package cross.field.StickMan;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.Date;

public class MainView extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    public static int BUTTON_H = 85;
    public static int BUTTON_W = 160;
    public static int BUTTON_X = 0;
    public static int BUTTON_Y = 683;
    public static final int END = 4;
    public static final int INIT_X = 144;
    public static final int INIT_Y = 384;
    public static final int PLAY = 1;
    public static final int RECORD = 3;
    public static final float SCROLL_SPEED = 15.0f;
    public static final int START = 0;
    private MainActivity activity;
    private BackGround bg;
    private int bg_h = 854;
    private int bg_w = 480;
    private int bg_x = 0;
    private int bg_y = 0;
    private Button button_jump;
    private int button_jump_h = TitleActivity.BUTTON_GROUP_HEIGHT;
    private int button_jump_w = 240;
    private int button_jump_x = this.button_slide_w;
    private int button_jump_y = 512;
    private Button button_menu;
    private int button_menu_h = 85;
    private int button_menu_w = 160;
    private int button_menu_x = 0;
    private int button_menu_y = 854;
    private Button button_ranking;
    private int button_ranking_h = 85;
    private int button_ranking_w = 160;
    private int button_ranking_x = 160;
    private int button_ranking_y = 854;
    private Button button_slide;
    private int button_slide_h = TitleActivity.BUTTON_GROUP_HEIGHT;
    private int button_slide_w = 240;
    private int button_slide_x = 0;
    private int button_slide_y = 512;
    private Button button_start;
    private int button_start_h = 85;
    private int button_start_w = 160;
    private int button_start_x = 320;
    private int button_start_y = 854;
    private Context context;
    private float dx = 0.0f;
    private float dy = 0.0f;
    private Graphics g;
    private GroundManager ground;
    private SurfaceHolder holder;
    private PlayerManager player;
    private Point point;
    private long point_score = 0;
    private boolean record_flag = false;
    private boolean record_today_flag = false;
    private int scene;
    private float[] score;
    private Thread thread;

    public void setScene(int scene2) {
        this.scene = scene2;
    }

    public int getScene() {
        return this.scene;
    }

    public void setScore(float score2, int i) {
        this.score[i] = score2;
    }

    public float getScore(int i) {
        return this.score[i];
    }

    public MainView(Context context2, SurfaceView sv) {
        super(context2);
        this.holder = sv.getHolder();
        this.holder.addCallback(this);
        this.holder.setFixedSize(getWidth(), getHeight());
        setFocusable(true);
        this.context = context2;
        setClickable(true);
        this.g = new Graphics(this.holder, context2);
        this.activity = (MainActivity) context2;
        this.score = new float[100];
        for (int i = 0; i < 100; i++) {
            this.score[i] = -1.0f;
        }
        setClickable(true);
        init();
    }

    public void init() {
        this.scene = 0;
        this.record_flag = false;
        this.record_today_flag = false;
        float dw = this.g.ratio_width;
        float dh = this.g.ratio_height;
        this.bg_x = 0;
        this.bg_y = 0;
        this.bg_w = (int) (480.0f * dw);
        this.bg_h = (int) (854.0f * dh);
        Graphics graphics = this.g;
        this.g.getClass();
        this.bg = new BackGround(graphics, 0, this.bg_x, this.bg_y, this.bg_w, this.bg_h);
        this.ground = new GroundManager(this.g, 0);
        this.point = new Point(this.g, 0, ((int) (20.0f * dh)) + 0, 30, 30);
        this.point_score = 0;
        this.player = new PlayerManager(this.g, 0);
        this.player.player.x = 144;
        this.button_slide_x = 0;
        this.button_slide_y = (int) (((double) dh) * 683.2d);
        this.button_slide_w = (int) ((480.0f * dw) / 2.0f);
        this.button_slide_h = (int) (((double) dh) * 85.4d);
        Context context2 = this.context;
        Graphics graphics2 = this.g;
        this.g.getClass();
        this.button_slide = new Button(context2, graphics2, 19, this.button_slide_x, this.button_slide_y, this.button_slide_w, this.button_slide_h, (int) (10.0f * dw), (int) (20.0f * dh));
        this.button_jump_x = this.button_slide_w;
        this.button_jump_y = (int) (((double) dh) * 683.2d);
        this.button_jump_w = (int) ((480.0f * dw) / 2.0f);
        this.button_jump_h = (int) (((double) dh) * 85.4d);
        Context context3 = this.context;
        Graphics graphics3 = this.g;
        this.g.getClass();
        this.button_jump = new Button(context3, graphics3, 18, this.button_jump_x, this.button_jump_y, this.button_jump_w, this.button_jump_h, (int) (10.0f * dw), (int) (20.0f * dh));
        this.button_menu_x = (int) (0.0f * dw);
        this.button_menu_y = (int) (((double) dh) * 854.0d);
        this.button_menu_w = (int) (160.0f * dw);
        this.button_menu_h = (int) (((double) dh) * 85.4d);
        Context context4 = this.context;
        Graphics graphics4 = this.g;
        this.g.getClass();
        this.button_menu = new Button(context4, graphics4, 20, this.button_menu_x, this.button_menu_y, this.button_menu_w, this.button_menu_h, 0, (int) (20.0f * dh));
        this.button_ranking_x = (int) (160.0f * dw);
        this.button_ranking_y = (int) (((double) dh) * 854.0d);
        this.button_ranking_w = (int) (160.0f * dw);
        this.button_ranking_h = (int) (((double) dh) * 85.4d);
        Context context5 = this.context;
        Graphics graphics5 = this.g;
        this.g.getClass();
        this.button_ranking = new Button(context5, graphics5, 22, this.button_ranking_x, this.button_ranking_y, this.button_ranking_w, this.button_ranking_h, 0, (int) (20.0f * dh));
        this.button_start_x = (int) (320.0f * dw);
        this.button_start_y = (int) (((double) dh) * 854.0d);
        this.button_start_w = (int) (160.0f * dw);
        this.button_start_h = (int) (((double) dh) * 85.4d);
        Context context6 = this.context;
        Graphics graphics6 = this.g;
        this.g.getClass();
        this.button_start = new Button(context6, graphics6, 21, this.button_start_x, this.button_start_y, this.button_start_w, this.button_start_h, 0, (int) (20.0f * dh));
    }

    public void run() {
        while (this.thread != null) {
            update();
            draw();
            sceneChange();
            try {
                Thread.sleep(55);
            } catch (Exception e) {
            }
        }
    }

    public void update() {
        switch (this.scene) {
            case 0:
                this.bg.action();
                this.ground.action();
                this.player.action();
                this.scene = 1;
                return;
            case 1:
                this.bg.action();
                this.ground.action();
                this.player.player.orientation(this.dx, this.dy);
                this.player.action();
                this.point_score += 10;
                return;
            case 2:
            case 3:
            default:
                return;
        }
    }

    public void draw() {
        this.g.lock();
        this.g.drawSquare(0, 0, this.g.disp_width, this.g.disp_height, -16777216, true);
        switch (this.scene) {
            case 0:
                this.bg.draw();
                this.ground.draw();
                this.player.draw();
                this.button_slide.draw();
                this.button_jump.draw();
                break;
            case 1:
                this.bg.draw();
                this.ground.draw();
                this.player.draw();
                this.point.drawPoint(this.point_score);
                this.button_slide.draw();
                this.button_jump.draw();
                break;
            case 3:
                this.bg.draw();
                this.ground.draw();
                this.player.draw();
                this.point.drawPoint(this.point_score);
                this.button_slide.draw();
                this.button_jump.draw();
                break;
            case 4:
                this.bg.draw();
                this.ground.draw();
                this.player.draw();
                this.point.drawPoint(this.point_score);
                this.button_slide.draw();
                this.button_jump.draw();
                break;
        }
        this.g.unlock();
    }

    public void sceneChange() {
        switch (this.scene) {
            case 0:
                this.player.sceneChange(this.scene);
                return;
            case 1:
                this.player.sceneChange(this.scene);
                if (this.player.player.y - this.player.player.h > this.g.disp_height) {
                    this.scene = 3;
                    Player player2 = this.player.player;
                    this.g.getClass();
                    player2.image_id = 3;
                }
                if (this.player.player.x < 0) {
                    this.scene = 3;
                    Player player3 = this.player.player;
                    this.g.getClass();
                    player3.image_id = 3;
                }
                if (this.player.player.x - this.player.player.w > this.g.disp_width) {
                    this.scene = 3;
                    Player player4 = this.player.player;
                    this.g.getClass();
                    player4.image_id = 3;
                }
                hitPB();
                return;
            case 2:
            default:
                return;
            case 3:
                this.player.sceneChange(this.scene);
                float[] sub1 = new float[101];
                for (int i = 0; i < 100; i++) {
                    sub1[i + 1] = this.score[i];
                }
                sub1[0] = (float) (this.point_score / 10);
                for (int i2 = 1; i2 < 101; i2++) {
                    for (int l = 1; l < 101; l++) {
                        if (sub1[l] > sub1[l - 1] && sub1[l - 1] != -1.0f) {
                            float sub2 = sub1[l];
                            sub1[l] = sub1[l - 1];
                            sub1[l - 1] = sub2;
                        }
                    }
                }
                for (int i3 = 0; i3 < 100; i3++) {
                    this.score[i3] = sub1[i3];
                }
                SharedPreferences pref = this.activity.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3);
                SharedPreferences.Editor editor = pref.edit();
                int best = (int) pref.getFloat("Score0", -1.0f);
                editor.putBoolean(TitleActivity.BEST_FLAG, false);
                if (best < ((int) (this.point_score / 10))) {
                    editor.putInt(TitleActivity.DAILY_RECORD, (int) (this.point_score / 10));
                    editor.putBoolean(TitleActivity.BEST_FLAG, true);
                    this.record_flag = true;
                    Date date = new Date();
                    long yesterday = pref.getLong(TitleActivity.DAILY_TIME, -1);
                    editor.putBoolean(TitleActivity.DAILY_FLAG, false);
                    if (yesterday == -1) {
                        editor.putInt(TitleActivity.DAILY_RECORD, (int) (this.point_score / 10));
                        editor.putLong(TitleActivity.DAILY_TIME, (long) Math.floor((double) (date.getTime() / 86400000)));
                    }
                } else {
                    Date date2 = new Date();
                    long yesterday2 = pref.getLong(TitleActivity.DAILY_TIME, -1);
                    editor.putBoolean(TitleActivity.DAILY_FLAG, false);
                    if (yesterday2 == -1) {
                        editor.putInt(TitleActivity.DAILY_RECORD, (int) (this.point_score / 10));
                        editor.putLong(TitleActivity.DAILY_TIME, (long) Math.floor((double) (date2.getTime() / 86400000)));
                        this.record_today_flag = true;
                    } else {
                        long yesterday3 = pref.getLong(TitleActivity.DAILY_TIME, -1);
                        long day = (long) Math.floor((double) (date2.getTime() / 86400000));
                        if (yesterday3 < day) {
                            editor.putLong(TitleActivity.DAILY_TIME, day);
                            editor.putInt(TitleActivity.DAILY_RECORD, -1);
                            editor.putBoolean(TitleActivity.DAILY_FLAG, true);
                        }
                        if (yesterday3 == day && pref.getInt(TitleActivity.DAILY_RECORD, (int) (this.point_score / 10)) < ((int) (this.point_score / 10))) {
                            editor.putInt(TitleActivity.DAILY_RECORD, (int) (this.point_score / 10));
                            editor.putBoolean(TitleActivity.DAILY_FLAG, true);
                            this.record_today_flag = true;
                        }
                    }
                }
                for (int i4 = 0; i4 < 100; i4++) {
                    editor.putFloat(TitleActivity.SCORE + i4, this.score[i4]);
                }
                editor.commit();
                this.scene = 4;
                return;
            case 4:
                this.activity.finish();
                return;
        }
    }

    public boolean hitPB() {
        this.player.player.fall();
        this.player.player = this.ground.hitGround(this.player.player);
        return false;
    }

    public boolean touchEvent(MotionEvent event) {
        boolean touch = false;
        if (this.button_jump.buttonEvent(event) != null && this.button_jump.buttonEvent(event).getAction() == 0 && this.player.player != null && this.scene == 1 && this.player.player.jump_count > 0) {
            this.player.player.jump_count--;
            this.player.player.jump();
        }
        if (this.button_slide.buttonEvent(event) != null) {
            if ((this.button_slide.buttonEvent(event).getAction() == 0 || this.button_slide.buttonEvent(event).getAction() == 2) && this.player.player != null && this.scene == 1 && this.ground.hitSlide(this.player.player)) {
                Player player2 = this.player.player;
                this.g.getClass();
                player2.image_id = 4;
                this.player.player.yhit = this.player.player.h / 2;
            }
            if ((this.button_slide.buttonEvent(event).getAction() == 1 || this.button_slide.alpha == 255) && this.player.player != null && this.scene == 1 && this.ground.hitSlide(this.player.player)) {
                Player player3 = this.player.player;
                this.g.getClass();
                player3.image_id = 1;
                this.player.player.yhit = 0;
            }
        } else if (this.player.player != null && this.scene == 1 && this.ground.hitSlide(this.player.player)) {
            Player player4 = this.player.player;
            this.g.getClass();
            player4.image_id = 1;
            this.player.player.yhit = 0;
        }
        if (this.button_menu.buttonEvent(event) != null && this.button_menu.buttonEvent(event).getAction() == 1) {
            SharedPreferences.Editor editor = this.activity.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3).edit();
            editor.putInt(TitleActivity.SCENE, 1);
            editor.commit();
            touch = true;
        }
        if (this.button_start.buttonEvent(event) != null && this.button_start.buttonEvent(event).getAction() == 1) {
            touch = false;
            init();
        }
        if (this.button_ranking.buttonEvent(event) != null && this.button_ranking.buttonEvent(event).getAction() == 1) {
            SharedPreferences.Editor editor2 = this.activity.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3).edit();
            editor2.putInt(TitleActivity.SCENE, 3);
            editor2.commit();
            touch = true;
        }
        switch (event.getAction()) {
        }
        return touch;
    }

    public void yesnodialog() {
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
        this.thread = null;
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
    }

    public void surfaceCreated(SurfaceHolder arg0) {
        this.thread = new Thread(this);
        this.thread.start();
    }
}
