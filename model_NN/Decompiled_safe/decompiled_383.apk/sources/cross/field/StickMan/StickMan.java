package cross.field.StickMan;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class StickMan extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, TitleActivity.class));
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
        finish();
    }
}
