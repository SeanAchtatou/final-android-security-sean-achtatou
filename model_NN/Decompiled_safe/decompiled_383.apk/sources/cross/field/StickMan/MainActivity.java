package cross.field.StickMan;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.AbsoluteLayout;
import android.widget.LinearLayout;

public class MainActivity extends Activity {
    public static final int ADLANTIS_HEIGHT = 85;
    public static final int ADLANTIS_WIDTH = 480;
    public static final int ADLANTIS_X = 0;
    public static final int ADLANTIS_Y = 768;
    public static final int VIEW_HEIGHT = 768;
    public static final int VIEW_WIDTH = 480;
    public static final int VIEW_X = 0;
    public static final int VIEW_Y = 0;
    public static final int WRAP_CONTENT = -2;
    public static final float XPERIA_HEIGHT = 854.0f;
    public static final float XPERIA_WIDTH = 480.0f;
    private int adlantis_height = 85;
    private int adlantis_width = 480;
    private int adlantis_x = 0;
    private int adlantis_y = 768;
    public int disp_height;
    public int disp_width;
    public float ratio_height;
    public float ratio_width;
    private MainView view;
    private int view_height = 768;
    private int view_width = 480;
    private int view_x = 0;
    private int view_y = 0;

    public void getDisplaySize() {
        Display disp = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        this.disp_width = disp.getWidth();
        this.disp_height = disp.getHeight();
    }

    public void getDisplayRatio() {
        getDisplaySize();
        this.ratio_width = ((float) this.disp_width) / 480.0f;
        this.ratio_height = ((float) this.disp_height) / 854.0f;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        getDisplaySize();
        getDisplayRatio();
        setContentView((int) R.layout.main);
        this.view_x = 0;
        this.view_x = (int) (((float) this.view_x) * this.ratio_width);
        this.view_y = 0;
        this.view_y = (int) (((float) this.view_y) * this.ratio_height);
        this.view_width = 480;
        this.view_width = (int) (((float) this.view_width) * this.ratio_width);
        this.view_height = 768;
        this.view_height = (int) (((float) this.view_height) * this.ratio_height);
        new LinearLayout(this);
        ((LinearLayout) findViewById(R.id.lnearLayout_surfaceView)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.view_width, this.view_height, this.view_x, this.view_y));
        this.view = new MainView(this, (SurfaceView) findViewById(R.id.surfaceView));
        this.adlantis_x = 0;
        this.adlantis_x = (int) (((float) this.adlantis_x) * this.ratio_width);
        this.adlantis_y = 768;
        this.adlantis_y = (int) (((float) this.adlantis_y) * this.ratio_height);
        this.adlantis_width = 480;
        this.adlantis_width = (int) (((float) this.adlantis_width) * this.ratio_width);
        this.adlantis_height = 85;
        this.adlantis_height = (int) (((float) this.adlantis_height) * this.ratio_height);
        new LinearLayout(this);
        ((LinearLayout) findViewById(R.id.linearLayout_Adlantis)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.adlantis_width, this.adlantis_height, this.adlantis_x, this.adlantis_y));
        SharedPreferences pref = getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3);
        for (int i = 0; i < 100; i++) {
            this.view.setScore(pref.getFloat(TitleActivity.SCORE + i, -1.0f), i);
        }
        for (int i2 = 1; i2 < 100; i2++) {
            for (int l = 1; l < 100; l++) {
                if ((this.view.getScore(l) > this.view.getScore(l - 1) || this.view.getScore(l - 1) == -1.0f) && this.view.getScore(l) != -1.0f) {
                    float sub = this.view.getScore(l);
                    this.view.setScore(this.view.getScore(l - 1), l);
                    this.view.setScore(sub, l - 1);
                }
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.view.touchEvent(event)) {
            finish();
        }
        switch (event.getAction()) {
        }
        return super.onTouchEvent(event);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    finish();
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }
}
