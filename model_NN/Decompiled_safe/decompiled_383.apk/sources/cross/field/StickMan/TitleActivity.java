package cross.field.StickMan;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.SpannableStringBuilder;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.crossfd.android.StickMan.utility.Constants;
import com.crossfd.android.StickMan.utility.RestWebServiceClient;
import com.crossfd.android.twitter.TwitterUtils;
import cross.field.more.MoreActivity;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class TitleActivity extends Activity {
    public static final int ADLANTIS_HEIGHT = 85;
    public static final int ADLANTIS_WIDTH = 480;
    public static final int ADLANTIS_X = 0;
    public static final int ADLANTIS_Y = 512;
    public static final String BEST_FLAG = "Best.Flag";
    public static final String BEST_RECORD = "Best.Record";
    public static final int BUTTON_GROUP_HEIGHT = 256;
    public static final int BUTTON_GROUP_WIDTH = 480;
    public static final int BUTTON_GROUP_X = 0;
    public static final int BUTTON_GROUP_Y = 597;
    public static final String DAILY_FLAG = "Daily.Flag";
    public static final String DAILY_RECORD = "Daily.Record";
    public static final String DAILY_TIME = "Daily.Time";
    public static final int MENU = 1;
    public static final String NAME = "Name";
    public static final int NAME_HEIGHT = -2;
    public static final int NAME_WIDTH = 480;
    public static final int NAME_X = 0;
    public static final int NAME_Y = 427;
    public static final int RANKING = 3;
    public static final String SCENE = "Scene";
    public static final String SCORE = "Score";
    public static final int START = 2;
    public static final int TITLE = 0;
    public static final int TITLE_HEIGHT = -2;
    public static final String TITLE_PREFERENCE = "TitlePreference";
    public static final int TITLE_WIDTH = 480;
    public static final int TITLE_X = 0;
    public static final int TITLE_Y = 170;
    public static final int WRAP_CONTENT = -2;
    public static final float XPERIA_HEIGHT = 854.0f;
    public static final float XPERIA_WIDTH = 480.0f;
    private int adlantis_height = 85;
    private int adlantis_width = 480;
    private int adlantis_x = 0;
    private int adlantis_y = 512;
    private int buttonGroup_height = BUTTON_GROUP_HEIGHT;
    private int buttonGroup_width = 480;
    private int buttonGroup_x = 0;
    private int buttonGroup_y = BUTTON_GROUP_Y;
    public int disp_height;
    public int disp_width;
    /* access modifiers changed from: private */
    public Intent intent;
    private TextView loginStatus;
    private int name_height = -2;
    private int name_width = 480;
    private int name_x = 0;
    private int name_y = NAME_Y;
    /* access modifiers changed from: private */
    public SharedPreferences prefs;
    public float ratio_height;
    public float ratio_width;
    private int title_height = -2;
    private int title_width = 480;
    private int title_x = 0;
    private int title_y = 170;

    public void getDisplaySize() {
        Display disp = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        this.disp_width = disp.getWidth();
        this.disp_height = disp.getHeight();
    }

    public void getDisplayRatio() {
        getDisplaySize();
        this.ratio_width = ((float) this.disp_width) / 480.0f;
        this.ratio_height = ((float) this.disp_height) / 854.0f;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        getDisplaySize();
        getDisplayRatio();
        setContentView(R.layout.title);
        this.title_x = (int) (0.0f * this.ratio_width);
        this.title_y = (int) (170.0f * this.ratio_height);
        this.title_width = (int) (480.0f * this.ratio_width);
        this.title_height = (int) (-2.0f * this.ratio_height);
        new LinearLayout(this);
        ((LinearLayout) findViewById(R.id.linearLayout_title)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.title_width, this.title_height, this.title_x, this.title_y));
        this.name_x = (int) (0.0f * this.ratio_width);
        this.name_y = (int) (427.0f * this.ratio_height);
        this.name_width = (int) (480.0f * this.ratio_width);
        this.name_height = (int) (-2.0f * this.ratio_height);
        new LinearLayout(this);
        ((LinearLayout) findViewById(R.id.linearLayout_name)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.name_width, this.name_height, this.name_x, this.name_y));
        this.buttonGroup_x = (int) (0.0f * this.ratio_width);
        this.buttonGroup_y = (int) (597.0f * this.ratio_height);
        this.buttonGroup_width = (int) (480.0f * this.ratio_width);
        this.buttonGroup_height = (int) (256.0f * this.ratio_height);
        new LinearLayout(this);
        ((LinearLayout) findViewById(R.id.linearLayout_button)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.buttonGroup_width, this.buttonGroup_height, this.buttonGroup_x, this.buttonGroup_y));
        new Button(this);
        ((Button) findViewById(R.id.button_start)).setLayoutParams(new LinearLayout.LayoutParams((int) (((double) this.buttonGroup_width) * 1.0d), (int) (85.4d * ((double) this.ratio_height))));
        new Button(this);
        ((Button) findViewById(R.id.button_ranking)).setLayoutParams(new LinearLayout.LayoutParams((int) (((double) this.buttonGroup_width) * 1.0d), (int) (85.4d * ((double) this.ratio_height))));
        new Button(this);
        ((Button) findViewById(R.id.button_more)).setLayoutParams(new LinearLayout.LayoutParams((int) (((double) this.buttonGroup_width) * 1.0d), (int) (85.4d * ((double) this.ratio_height))));
        SharedPreferences pref = getSharedPreferences(TITLE_PREFERENCE, 3);
        String name = pref.getString(NAME, "");
        new EditText(this);
        EditText edit = (EditText) findViewById(R.id.editText_edit);
        if (!name.equalsIgnoreCase("")) {
            edit.setText(name);
        }
        if (name.equalsIgnoreCase("")) {
            edit.setText("StickMan");
        }
        edit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean decision = false;
                if (actionId != 6) {
                    return false;
                }
                String str = ((SpannableStringBuilder) ((EditText) TitleActivity.this.findViewById(R.id.editText_edit)).getText()).toString();
                String oldName = TitleActivity.this.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3).getString(TitleActivity.NAME, "");
                if (str.equalsIgnoreCase(oldName)) {
                    decision = false;
                }
                if (!str.equalsIgnoreCase(oldName)) {
                    return TitleActivity.this.yesnoDialog();
                }
                return decision;
            }
        });
        final EditText editText = (EditText) findViewById(R.id.editText_edit);
        edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean flag) {
                if (!flag) {
                    ((InputMethodManager) TitleActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                editText.setFocusable(true);
                editText.setFocusableInTouchMode(true);
            }
        });
        SharedPreferences.Editor editor = pref.edit();
        for (int i = 0; i < 100; i++) {
            if (pref.getFloat(SCORE + i, -1.0f) == 0.0f) {
                editor.putFloat(SCORE + i, -1.0f);
            }
        }
        editor.commit();
        this.adlantis_x = (int) (0.0f * this.ratio_width);
        this.adlantis_y = (int) (512.0f * this.ratio_height);
        this.adlantis_width = (int) (480.0f * this.ratio_width);
        this.adlantis_height = (int) (85.0f * this.ratio_height);
        new LinearLayout(this);
        ((LinearLayout) findViewById(R.id.linearLayout_Adlantis)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.adlantis_width, this.adlantis_height, this.adlantis_x, this.adlantis_y));
    }

    public void onResume() {
        super.onResume();
        SharedPreferences pref = getSharedPreferences(TITLE_PREFERENCE, 3);
        boolean record = pref.getBoolean(BEST_FLAG, false);
        boolean daily = pref.getBoolean(DAILY_FLAG, false);
        if (record) {
            this.intent = new Intent(this, RankingActivity.class);
            new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle("NewRecord!").setMessage("Sending score").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    TitleActivity.this.startActivity(TitleActivity.this.intent);
                    TitleActivity.this.insert(0);
                }
            }).show();
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(BEST_FLAG, false);
            editor.commit();
        } else if (daily) {
            this.intent = new Intent(this, RankingActivity.class);
            new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle("Today's NewRecord!").setMessage("Sending score").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    TitleActivity.this.startActivity(TitleActivity.this.intent);
                    TitleActivity.this.insert(1);
                }
            }).show();
            SharedPreferences.Editor editor2 = pref.edit();
            editor2.putBoolean(DAILY_FLAG, false);
            editor2.commit();
        }
    }

    public void insert(int mode) {
        TelephonyManager tel = (TelephonyManager) getSystemService("phone");
        SharedPreferences pref = getSharedPreferences(TITLE_PREFERENCE, 3);
        String name = pref.getString(NAME, "NoData");
        String record = String.valueOf((int) pref.getFloat("Score0", -1.0f));
        if (mode == 0) {
            record = String.valueOf((int) pref.getFloat("Score0", -1.0f));
        }
        if (mode == 1) {
            record = String.valueOf(pref.getInt(DAILY_RECORD, -1));
        }
        RestWebServiceClient restClient = new RestWebServiceClient(Constants.URL_INSER_POINT);
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("user_name", name));
        nameValuePairs.add(new BasicNameValuePair("user_point", record));
        nameValuePairs.add(new BasicNameValuePair("country_code", getResources().getConfiguration().locale.getCountry()));
        nameValuePairs.add(new BasicNameValuePair("device_id", tel.getDeviceId()));
        try {
            if (!record.equalsIgnoreCase("-1") && !record.equalsIgnoreCase("0")) {
                restClient.webPost("", nameValuePairs);
            }
        } catch (Exception e) {
            showDialog(this, "Failure", "Transmission failure");
        }
    }

    public void buttonStart(View v) {
        new EditText(this);
        String str = ((SpannableStringBuilder) ((EditText) findViewById(R.id.editText_edit)).getText()).toString();
        if (str.length() != 0) {
            SharedPreferences.Editor editor = getSharedPreferences(TITLE_PREFERENCE, 0).edit();
            editor.putString(NAME, str);
            editor.commit();
            startActivity(new Intent(this, MainActivity.class));
            return;
        }
        showDialog(this, "Error", "Input name");
    }

    public void buttonRanking(View v) {
        new EditText(this);
        String str = ((SpannableStringBuilder) ((EditText) findViewById(R.id.editText_edit)).getText()).toString();
        if (str.length() != 0) {
            SharedPreferences.Editor editor = getSharedPreferences(TITLE_PREFERENCE, 0).edit();
            editor.putString(NAME, str);
            editor.commit();
            startActivity(new Intent(this, RankingActivity.class));
            return;
        }
        showDialog(this, "Error", "Input name");
    }

    private static void showDialog(Context context, String title, String text) {
        AlertDialog.Builder ad = new AlertDialog.Builder(context);
        ad.setTitle(title);
        ad.setMessage(text);
        ad.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
        ad.show();
    }

    public boolean yesnoDialog() {
        boolean decision = false;
        final EditText name = (EditText) findViewById(R.id.editText_edit);
        final String str = ((SpannableStringBuilder) name.getText()).toString();
        String newName = getSharedPreferences(TITLE_PREFERENCE, 3).getString(NAME, "");
        new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle("Look!").setMessage("Change name�H").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                SharedPreferences.Editor editor = TitleActivity.this.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 0).edit();
                editor.putString(TitleActivity.NAME, str);
                editor.commit();
                name.setFocusable(false);
                name.setFocusableInTouchMode(true);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                name.setText(TitleActivity.this.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3).getString(TitleActivity.NAME, ""));
                name.setFocusable(false);
                name.setFocusableInTouchMode(true);
            }
        }).show();
        if (newName.equalsIgnoreCase(str)) {
            decision = false;
        }
        if (!newName.equalsIgnoreCase(str)) {
            return true;
        }
        return decision;
    }

    public void buttonMore(View v) {
        startActivity(new Intent(this, MoreActivity.class));
    }

    public void updateLoginStatus() {
        this.loginStatus.setText("Logged into Twitter : " + TwitterUtils.isAuthenticated(this.prefs));
    }

    public void sendTweet(final String message) {
        new Thread() {
            public void run() {
                try {
                    TwitterUtils.sendTweet(TitleActivity.this.prefs, message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            event.getKeyCode();
        }
        return super.dispatchKeyEvent(event);
    }
}
