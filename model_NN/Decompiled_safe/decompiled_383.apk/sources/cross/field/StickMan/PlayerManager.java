package cross.field.StickMan;

public class PlayerManager {
    private Graphics g;
    public Player player;
    private int player_h = 60;
    private int player_w = 60;
    private int player_x = 0;
    private int player_y = 0;
    private int scene;

    public PlayerManager(Graphics g2, int scene2) {
        this.g = g2;
        this.scene = scene2;
        init();
    }

    public void init() {
        float dw = this.g.ratio_width;
        float dh = this.g.ratio_height;
        this.player_w = (int) (((float) this.player_w) * dw);
        this.player_h = (int) (((float) this.player_h) * dh);
        this.player_x = (int) (144.0f * this.g.ratio_width);
        this.player_y = (int) (384.0f * this.g.ratio_height);
        Graphics graphics = this.g;
        this.g.getClass();
        this.player = new Player(graphics, 1, this.player_x, this.player_y, this.player_w, this.player_h);
    }

    public void action() {
        this.player.action();
    }

    public void draw() {
        this.player.draw();
    }

    public void sceneChange(int scene2) {
        this.scene = scene2;
        this.player.sceneChange(scene2);
    }

    public void orientation(float dx, float dy) {
        this.player.orientation(dx, dy);
    }
}
