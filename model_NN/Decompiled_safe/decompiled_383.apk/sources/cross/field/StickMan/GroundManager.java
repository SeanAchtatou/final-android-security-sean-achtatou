package cross.field.StickMan;

import java.util.Random;

public class GroundManager {
    public static final int CONCAVE = 3;
    public static final int CONVEX = 4;
    public static final int FLAT = 2;
    public static final int GROUND_MAX = 20;
    public static final int INIT = 0;
    public static final int MODE_INTERVAL = 4;
    public static final int MODE_LIMIT = 1;
    public static final int SCROLL = 1;
    public static final int SCROLL_SPEED = -25;
    public Block[] block;
    private Graphics g;
    public Ground[] ground;
    public int ground_h = 20;
    public int ground_w = 80;
    private int height_limit = 0;
    private int mode;
    private int modeInterval = 0;
    private int modeLimit = 0;
    private Random random;
    private float scroll_speed;

    public GroundManager(Graphics g2, int scene) {
        this.g = g2;
        this.random = new Random();
        init();
    }

    public void init() {
        this.mode = 1;
        this.ground = new Ground[20];
        int xp = this.g.disp_width / 18;
        for (int i = 0; i < 20; i++) {
            Ground[] groundArr = this.ground;
            Graphics graphics = this.g;
            this.g.getClass();
            groundArr[i] = new Ground(graphics, 5, xp * i, (int) (this.g.ratio_height * 384.0f), xp, 768 - ((int) (this.g.ratio_height * 384.0f)));
        }
        this.scroll_speed = -25.0f * this.g.ratio_width;
        this.block = new Block[20];
    }

    public void action() {
        switch (this.mode) {
            case 0:
                init();
                return;
            case 1:
                scroll();
                return;
            default:
                return;
        }
    }

    public void scroll() {
        int state;
        for (int i = 0; i < 20; i++) {
            Ground ground2 = this.ground[i];
            ground2.x = (int) (((float) ground2.x) + this.scroll_speed);
            if (this.block[i] != null) {
                Block block2 = this.block[i];
                block2.x = (int) (((float) block2.x) + this.scroll_speed);
            }
            if (this.ground[i].x + this.ground[i].w <= 0) {
                this.ground[i].x += this.ground[i].w * 20;
                this.ground[i].y = (int) (384.0f * this.g.ratio_height);
                this.block[i] = null;
                if (this.modeInterval < 4 || this.random.nextInt(3) != 0) {
                    state = 2;
                    this.modeInterval++;
                } else {
                    state = this.random.nextInt(3) + 2;
                    this.modeInterval = 0;
                }
                switch (state) {
                    case 2:
                        this.height_limit = 0;
                        break;
                    case 3:
                        this.block[i] = new Block(this.g, 0, this.ground[i].x, (int) (((double) this.ground[i].y) - (((double) this.ground[i].w) * 2.5d)), this.ground[i].w, this.ground[i].w);
                        break;
                    case 4:
                        this.ground[i].y -= this.ground[i].w;
                        break;
                }
                this.ground[i].h = 768 - this.ground[i].y;
            }
        }
    }

    public void modeSet() {
    }

    public void draw() {
        for (int i = 0; i < 20; i++) {
            if (this.ground[i] != null) {
                this.ground[i].draw();
            }
            if (this.block[i] != null) {
                this.block[i].draw();
            }
        }
    }

    public Player hitGround(Player player) {
        for (int i = 0; i < 20; i++) {
            if (this.ground[i] != null && this.ground[i].x <= player.x + player.xoff + player.w) {
                int top1 = player.y + player.yoff + player.yhit;
                int bottom1 = top1 + player.h;
                int left1 = player.x + player.xoff + player.xhit;
                int right1 = left1 + ((int) (((double) player.xhit) * 1.0d));
                int top2 = this.ground[i].y;
                int bottom2 = top2 + this.ground[i].h;
                int left2 = this.ground[i].x;
                if (left1 <= left2 + this.ground[i].w && right1 >= left2 && bottom1 >= top2 && top1 <= bottom2) {
                    if (((float) player.y) - player.yspeed > ((float) this.ground[i].y)) {
                        player.x = this.ground[i].x - ((player.xoff + player.xhit) + ((int) (((double) player.xhit) * 1.0d)));
                    }
                    if (((float) player.y) - player.yspeed <= ((float) this.ground[i].y)) {
                        player.y = this.ground[i].y;
                        player.yspeed = 0.0f;
                        this.g.getClass();
                        player.image_id = 1;
                    }
                }
                if (this.block[i] != null) {
                    int top22 = this.block[i].y;
                    int bottom22 = top22 + this.block[i].h;
                    int left22 = this.block[i].x;
                    if (left1 <= left22 + this.block[i].w && right1 >= left22 && bottom1 >= top22 && top1 <= bottom22) {
                        if (((float) player.y) - player.yspeed > ((float) this.block[i].y)) {
                            player.x = this.block[i].x - ((player.xoff + player.xhit) + ((int) (((double) player.xhit) * 1.0d)));
                        }
                        if (((float) player.y) - player.yspeed <= ((float) this.block[i].y)) {
                            player.y = this.block[i].y;
                            player.yspeed = 0.0f;
                            this.g.getClass();
                            player.image_id = 1;
                        }
                    }
                }
            }
        }
        return player;
    }

    public boolean hitSlide(Player player) {
        boolean hit = false;
        for (int i = 0; i < 20; i++) {
            if (this.ground[i] != null && this.ground[i].x <= player.x + player.xoff + player.w) {
                int top1 = player.y + player.yoff + player.yhit;
                int bottom1 = top1 + player.h;
                int left1 = player.x + player.xoff + player.xhit;
                int right1 = left1 + ((int) (((double) player.xhit) * 1.0d));
                int top2 = this.ground[i].y;
                int bottom2 = top2 + this.ground[i].h;
                int left2 = this.ground[i].x;
                if (left1 <= left2 + this.ground[i].w && right1 >= left2 && bottom1 >= top2 && top1 <= bottom2) {
                    hit = true;
                }
            }
        }
        return hit;
    }

    public boolean hitGround(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2) {
        int top1 = y1;
        int bottom1 = y1 + h1;
        int right1 = x1 + w1;
        int top2 = y2;
        int bottom2 = y2 + h2;
        int left2 = x2;
        if (x1 > x2 + w2 || right1 < left2 || bottom1 < top2 || top1 > bottom2) {
            return false;
        }
        return true;
    }
}
