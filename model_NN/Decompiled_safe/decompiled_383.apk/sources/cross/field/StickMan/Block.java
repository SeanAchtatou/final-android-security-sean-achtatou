package cross.field.StickMan;

public class Block {
    private Graphics g;
    public int h;
    private int id;
    public int w;
    public int x;
    public int y;

    public Block(Graphics g2, int id2, int x2, int y2, int w2, int h2) {
        this.g = g2;
        this.x = x2;
        this.y = y2;
        this.w = w2;
        this.h = h2;
        setId(id2);
    }

    public void init(Graphics g2, int id2, int x2, int y2, int w2, int h2) {
        this.g = g2;
        this.x = x2;
        this.y = y2;
        this.w = w2;
        this.h = h2;
        setId(id2);
    }

    public void action() {
    }

    public void draw() {
        this.g.drawSquare(this.x, this.y, this.w, this.h, -16777216, true);
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public int getId() {
        return this.id;
    }
}
