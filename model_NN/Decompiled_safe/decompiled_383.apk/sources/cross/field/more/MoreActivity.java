package cross.field.more;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import cross.field.StickMan.R;

public class MoreActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(1024);
        setContentView((int) R.layout.more);
        WebView webView = (WebView) findViewById(R.id.webView_more);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("http://androida.me");
    }

    public void onResume() {
        super.onResume();
    }
}
