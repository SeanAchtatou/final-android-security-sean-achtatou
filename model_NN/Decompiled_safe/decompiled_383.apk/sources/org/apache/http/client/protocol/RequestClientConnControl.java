package org.apache.http.client.protocol;

import java.io.IOException;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.annotation.Immutable;
import org.apache.http.conn.ManagedClientConnection;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.protocol.HttpContext;

@Immutable
public class RequestClientConnControl implements HttpRequestInterceptor {
    private static final String PROXY_CONN_DIRECTIVE = "Proxy-Connection";

    public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
        if (request == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (request.getRequestLine().getMethod().equalsIgnoreCase("CONNECT")) {
            request.setHeader(PROXY_CONN_DIRECTIVE, "Keep-Alive");
        } else {
            ManagedClientConnection conn = (ManagedClientConnection) context.getAttribute("http.connection");
            if (conn == null) {
                throw new IllegalStateException("Client connection not specified in HTTP context");
            }
            HttpRoute route = conn.getRoute();
            if ((route.getHopCount() == 1 || route.isTunnelled()) && !request.containsHeader("Connection")) {
                request.addHeader("Connection", "Keep-Alive");
            }
            if (route.getHopCount() == 2 && !route.isTunnelled() && !request.containsHeader(PROXY_CONN_DIRECTIVE)) {
                request.addHeader(PROXY_CONN_DIRECTIVE, "Keep-Alive");
            }
        }
    }
}
