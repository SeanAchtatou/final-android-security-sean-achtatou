package org.apache.http.impl.conn.tsccm;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ClientConnectionOperator;
import org.apache.http.conn.ClientConnectionRequest;
import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.apache.http.conn.ManagedClientConnection;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.conn.DefaultClientConnectionOperator;
import org.apache.http.params.HttpParams;

public class ThreadSafeClientConnManager implements ClientConnectionManager {
    protected final ClientConnectionOperator connOperator;
    protected final AbstractConnPool connectionPool;
    /* access modifiers changed from: private */
    public final Log log = LogFactory.getLog(getClass());
    protected final SchemeRegistry schemeRegistry;

    public ThreadSafeClientConnManager(HttpParams params, SchemeRegistry schreg) {
        if (params == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else if (schreg == null) {
            throw new IllegalArgumentException("Scheme registry may not be null");
        } else {
            this.schemeRegistry = schreg;
            this.connOperator = createConnectionOperator(schreg);
            this.connectionPool = createConnectionPool(params);
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        try {
            shutdown();
        } finally {
            super.finalize();
        }
    }

    /* access modifiers changed from: protected */
    public AbstractConnPool createConnectionPool(HttpParams params) {
        return new ConnPoolByRoute(this.connOperator, params);
    }

    /* access modifiers changed from: protected */
    public ClientConnectionOperator createConnectionOperator(SchemeRegistry schreg) {
        return new DefaultClientConnectionOperator(schreg);
    }

    public SchemeRegistry getSchemeRegistry() {
        return this.schemeRegistry;
    }

    public ClientConnectionRequest requestConnection(final HttpRoute route, Object state) {
        final PoolEntryRequest poolRequest = this.connectionPool.requestPoolEntry(route, state);
        return new ClientConnectionRequest() {
            public void abortRequest() {
                poolRequest.abortRequest();
            }

            public ManagedClientConnection getConnection(long timeout, TimeUnit tunit) throws InterruptedException, ConnectionPoolTimeoutException {
                if (route == null) {
                    throw new IllegalArgumentException("Route may not be null.");
                }
                if (ThreadSafeClientConnManager.this.log.isDebugEnabled()) {
                    ThreadSafeClientConnManager.this.log.debug("ThreadSafeClientConnManager.getConnection: " + route + ", timeout = " + timeout);
                }
                return new BasicPooledConnAdapter(ThreadSafeClientConnManager.this, poolRequest.getPoolEntry(timeout, tunit));
            }
        };
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:40:0x007b=Splitter:B:40:0x007b, B:21:0x003e=Splitter:B:21:0x003e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void releaseConnection(org.apache.http.conn.ManagedClientConnection r11, long r12, java.util.concurrent.TimeUnit r14) {
        /*
            r10 = this;
            boolean r1 = r11 instanceof org.apache.http.impl.conn.tsccm.BasicPooledConnAdapter
            if (r1 != 0) goto L_0x000c
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r4 = "Connection class mismatch, connection not obtained from this manager."
            r1.<init>(r4)
            throw r1
        L_0x000c:
            r0 = r11
            org.apache.http.impl.conn.tsccm.BasicPooledConnAdapter r0 = (org.apache.http.impl.conn.tsccm.BasicPooledConnAdapter) r0
            r7 = r0
            org.apache.http.impl.conn.AbstractPoolEntry r1 = r7.getPoolEntry()
            if (r1 == 0) goto L_0x0024
            org.apache.http.conn.ClientConnectionManager r1 = r7.getManager()
            if (r1 == r10) goto L_0x0024
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r4 = "Connection not obtained from this manager."
            r1.<init>(r4)
            throw r1
        L_0x0024:
            monitor-enter(r7)
            org.apache.http.impl.conn.AbstractPoolEntry r2 = r7.getPoolEntry()     // Catch:{ all -> 0x005f }
            org.apache.http.impl.conn.tsccm.BasicPoolEntry r2 = (org.apache.http.impl.conn.tsccm.BasicPoolEntry) r2     // Catch:{ all -> 0x005f }
            if (r2 != 0) goto L_0x002f
            monitor-exit(r7)     // Catch:{ all -> 0x005f }
        L_0x002e:
            return
        L_0x002f:
            boolean r1 = r7.isOpen()     // Catch:{ IOException -> 0x006a }
            if (r1 == 0) goto L_0x003e
            boolean r1 = r7.isMarkedReusable()     // Catch:{ IOException -> 0x006a }
            if (r1 != 0) goto L_0x003e
            r7.shutdown()     // Catch:{ IOException -> 0x006a }
        L_0x003e:
            boolean r3 = r7.isMarkedReusable()     // Catch:{ all -> 0x005f }
            org.apache.commons.logging.Log r1 = r10.log     // Catch:{ all -> 0x005f }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x005f }
            if (r1 == 0) goto L_0x0053
            if (r3 == 0) goto L_0x0062
            org.apache.commons.logging.Log r1 = r10.log     // Catch:{ all -> 0x005f }
            java.lang.String r4 = "Released connection is reusable."
            r1.debug(r4)     // Catch:{ all -> 0x005f }
        L_0x0053:
            r7.detach()     // Catch:{ all -> 0x005f }
            org.apache.http.impl.conn.tsccm.AbstractConnPool r1 = r10.connectionPool     // Catch:{ all -> 0x005f }
            r4 = r12
            r6 = r14
            r1.freeEntry(r2, r3, r4, r6)     // Catch:{ all -> 0x005f }
        L_0x005d:
            monitor-exit(r7)     // Catch:{ all -> 0x005f }
            goto L_0x002e
        L_0x005f:
            r1 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x005f }
            throw r1
        L_0x0062:
            org.apache.commons.logging.Log r1 = r10.log     // Catch:{ all -> 0x005f }
            java.lang.String r4 = "Released connection is not reusable."
            r1.debug(r4)     // Catch:{ all -> 0x005f }
            goto L_0x0053
        L_0x006a:
            r1 = move-exception
            r8 = r1
            org.apache.commons.logging.Log r1 = r10.log     // Catch:{ all -> 0x00a3 }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x00a3 }
            if (r1 == 0) goto L_0x007b
            org.apache.commons.logging.Log r1 = r10.log     // Catch:{ all -> 0x00a3 }
            java.lang.String r4 = "Exception shutting down released connection."
            r1.debug(r4, r8)     // Catch:{ all -> 0x00a3 }
        L_0x007b:
            boolean r3 = r7.isMarkedReusable()     // Catch:{ all -> 0x005f }
            org.apache.commons.logging.Log r1 = r10.log     // Catch:{ all -> 0x005f }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x005f }
            if (r1 == 0) goto L_0x0090
            if (r3 == 0) goto L_0x009b
            org.apache.commons.logging.Log r1 = r10.log     // Catch:{ all -> 0x005f }
            java.lang.String r4 = "Released connection is reusable."
            r1.debug(r4)     // Catch:{ all -> 0x005f }
        L_0x0090:
            r7.detach()     // Catch:{ all -> 0x005f }
            org.apache.http.impl.conn.tsccm.AbstractConnPool r1 = r10.connectionPool     // Catch:{ all -> 0x005f }
            r4 = r12
            r6 = r14
            r1.freeEntry(r2, r3, r4, r6)     // Catch:{ all -> 0x005f }
            goto L_0x005d
        L_0x009b:
            org.apache.commons.logging.Log r1 = r10.log     // Catch:{ all -> 0x005f }
            java.lang.String r4 = "Released connection is not reusable."
            r1.debug(r4)     // Catch:{ all -> 0x005f }
            goto L_0x0090
        L_0x00a3:
            r1 = move-exception
            r9 = r1
            boolean r3 = r7.isMarkedReusable()     // Catch:{ all -> 0x005f }
            org.apache.commons.logging.Log r1 = r10.log     // Catch:{ all -> 0x005f }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x005f }
            if (r1 == 0) goto L_0x00ba
            if (r3 == 0) goto L_0x00c5
            org.apache.commons.logging.Log r1 = r10.log     // Catch:{ all -> 0x005f }
            java.lang.String r4 = "Released connection is reusable."
            r1.debug(r4)     // Catch:{ all -> 0x005f }
        L_0x00ba:
            r7.detach()     // Catch:{ all -> 0x005f }
            org.apache.http.impl.conn.tsccm.AbstractConnPool r1 = r10.connectionPool     // Catch:{ all -> 0x005f }
            r4 = r12
            r6 = r14
            r1.freeEntry(r2, r3, r4, r6)     // Catch:{ all -> 0x005f }
            throw r9     // Catch:{ all -> 0x005f }
        L_0x00c5:
            org.apache.commons.logging.Log r1 = r10.log     // Catch:{ all -> 0x005f }
            java.lang.String r4 = "Released connection is not reusable."
            r1.debug(r4)     // Catch:{ all -> 0x005f }
            goto L_0x00ba
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager.releaseConnection(org.apache.http.conn.ManagedClientConnection, long, java.util.concurrent.TimeUnit):void");
    }

    public void shutdown() {
        this.log.debug("Shutting down");
        this.connectionPool.shutdown();
    }

    public int getConnectionsInPool(HttpRoute route) {
        return ((ConnPoolByRoute) this.connectionPool).getConnectionsInPool(route);
    }

    public int getConnectionsInPool() {
        this.connectionPool.poolLock.lock();
        int count = this.connectionPool.numConnections;
        this.connectionPool.poolLock.unlock();
        return count;
    }

    public void closeIdleConnections(long idleTimeout, TimeUnit tunit) {
        if (this.log.isDebugEnabled()) {
            this.log.debug("Closing connections idle for " + idleTimeout + " " + tunit);
        }
        this.connectionPool.closeIdleConnections(idleTimeout, tunit);
        this.connectionPool.deleteClosedConnections();
    }

    public void closeExpiredConnections() {
        this.log.debug("Closing expired connections");
        this.connectionPool.closeExpiredConnections();
        this.connectionPool.deleteClosedConnections();
    }
}
