package com.tenapp.hoopersLite;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class MainMenu extends Activity {
    SharedPreferences.Editor achediter;
    SharedPreferences achvments;
    RateAlert alert;
    MediaPlayer bg_sound;
    ImageView exit;
    boolean flg = true;
    boolean flg_idle = true;
    Button help;
    Handler mHandler = new Handler();
    ImageView more;
    MediaPlayer mp;
    Button pro;
    ImageView rank_img;
    Button settings;
    Button start;
    ImageView test;

    public void onDestroy() {
        super.onDestroy();
        if (this.bg_sound != null) {
            this.bg_sound.stop();
        }
    }

    public void onCreate(Bundle b) {
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        super.onCreate(b);
        setContentView((int) R.layout.main);
        this.alert = new RateAlert(this);
        this.mp = MediaPlayer.create(getBaseContext(), (int) R.raw.click);
        Display display = getWindowManager().getDefaultDisplay();
        Settings.width = display.getWidth();
        Settings.height = display.getHeight();
        this.bg_sound = MediaPlayer.create(getBaseContext(), (int) R.raw.mainmenusound);
        if (this.bg_sound != null) {
            this.bg_sound.setLooping(true);
            this.bg_sound.start();
        }
        this.start = (Button) findViewById(R.id.play);
        this.pro = (Button) findViewById(R.id.buypro);
        this.more = (ImageView) findViewById(R.id.more);
        this.exit = (ImageView) findViewById(R.id.exit);
        this.help = (Button) findViewById(R.id.help);
        this.settings = (Button) findViewById(R.id.settings);
        this.test = (ImageView) findViewById(R.id.test1);
        this.rank_img = (ImageView) findViewById(R.id.rnk);
        EatingObjectsActivity.flg_ach1 = false;
        EatingObjectsActivity.flg_ach2 = false;
        EatingObjectsActivity.flg_ach3 = false;
        EatingObjectsActivity.flg_ach4 = false;
        Settings.tracker = GoogleAnalyticsTracker.getInstance();
        Settings.tracker.start("UA-25018212-1", 20, this);
        this.achvments = getSharedPreferences("R.values.Acheivements", 2);
        this.achediter = this.achvments.edit();
        this.test.setImageResource(R.drawable.ls1);
        int rank = this.achvments.getInt("Rank", 0);
        if (rank == 0) {
            this.rank_img.setBackgroundResource(R.drawable.none);
        } else if (rank == 1) {
            this.rank_img.setBackgroundResource(R.drawable.littlebaby);
        } else if (rank == 2) {
            this.rank_img.setBackgroundResource(R.drawable.toddler);
        } else if (rank == 3) {
            this.rank_img.setBackgroundResource(R.drawable.adolescent);
        } else if (rank == 4) {
            this.rank_img.setBackgroundResource(R.drawable.grownup);
        } else if (rank == 5) {
            this.rank_img.setBackgroundResource(R.drawable.adult);
        } else if (rank == 6) {
            this.rank_img.setBackgroundResource(R.drawable.wiseperson);
        } else if (rank == 7) {
            this.rank_img.setBackgroundResource(R.drawable.philosopher);
        } else if (rank == 8) {
            this.rank_img.setBackgroundResource(R.drawable.thinker);
        } else if (rank == 9) {
            this.rank_img.setBackgroundResource(R.drawable.wittyhooper);
        } else if (rank == 10) {
            this.rank_img.setBackgroundResource(R.drawable.philosopher);
        }
        this.start.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (MainMenu.this.mp != null) {
                    MainMenu.this.mp.start();
                }
                MainMenu.this.start.setBackgroundResource(R.drawable.playon);
                try {
                    Settings.tracker.trackEvent("Arcade/Puzzle", "HOOPERS", "clicked", 1);
                } catch (Exception e) {
                    Log.e("Error", "at line 100 MainMenu Exception is " + e);
                }
                Intent start = new Intent(MainMenu.this.getApplicationContext(), SelectWorld.class);
                MainMenu.this.finish();
                MainMenu.this.startActivity(start);
            }
        });
        this.help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (MainMenu.this.mp != null) {
                    MainMenu.this.mp.start();
                }
                MainMenu.this.help.setBackgroundResource(R.drawable.helpon);
                Intent start = new Intent(MainMenu.this.getApplicationContext(), Help.class);
                MainMenu.this.finish();
                MainMenu.this.startActivity(start);
            }
        });
        this.more.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (MainMenu.this.mp != null) {
                    MainMenu.this.mp.start();
                }
                Intent start = new Intent(MainMenu.this.getApplicationContext(), Moreapps.class);
                MainMenu.this.finish();
                MainMenu.this.startActivity(start);
            }
        });
        this.pro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (MainMenu.this.mp != null) {
                    MainMenu.this.mp.start();
                }
                MainMenu.this.pro.setBackgroundResource(R.drawable.buton);
                try {
                    Intent i = new Intent("android.intent.action.VIEW");
                    i.setData(Uri.parse("market://details?id=com.tenapp.hoopers"));
                    MainMenu.this.finish();
                    MainMenu.this.startActivity(i);
                } catch (Exception e) {
                }
            }
        });
        this.settings.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (MainMenu.this.mp != null) {
                    MainMenu.this.mp.start();
                }
                MainMenu.this.settings.setBackgroundResource(R.drawable.settingson);
                Intent start = new Intent(MainMenu.this.getApplicationContext(), SettingsChooser.class);
                MainMenu.this.finish();
                MainMenu.this.startActivity(start);
            }
        });
        this.exit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (MainMenu.this.mp != null) {
                    MainMenu.this.mp.start();
                }
                MainMenu.this.alert.show();
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            try {
                Settings.tracker.stop();
            } catch (Exception e) {
            }
            Log.w("Error", "IN Back");
            this.flg_idle = false;
            Process.killProcess(Process.myPid());
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    public class RateAlert extends Dialog implements View.OnClickListener {
        Button rating = ((Button) findViewById(R.id.rate));
        Button thanks = ((Button) findViewById(R.id.thanks));

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            if (keyCode != 4) {
                return super.onKeyDown(keyCode, event);
            }
            Intent i = new Intent(MainMenu.this.getApplicationContext(), MainMenu.class);
            MainMenu.this.finish();
            MainMenu.this.startActivity(i);
            return true;
        }

        public RateAlert(Context context) {
            super(context);
            requestWindowFeature(1);
            setContentView((int) R.layout.ratehoopers);
            this.rating.setOnClickListener(this);
            this.thanks.setOnClickListener(this);
        }

        public void onClick(View v) {
            if (v == this.rating) {
                if (MediaPlayer.create(MainMenu.this.getBaseContext(), (int) R.raw.click) != null) {
                }
                try {
                    Intent i = new Intent("android.intent.action.VIEW");
                    i.setData(Uri.parse("market://details?id=com.tenapp.hoopersLite"));
                    MainMenu.this.finish();
                    MainMenu.this.startActivity(i);
                } catch (Exception e) {
                }
            } else if (v == this.thanks) {
                MediaPlayer mp = MediaPlayer.create(MainMenu.this.getBaseContext(), (int) R.raw.click);
                if (mp != null) {
                    mp.start();
                }
                Process.killProcess(Process.myPid());
                MainMenu.this.finish();
            }
        }
    }
}
