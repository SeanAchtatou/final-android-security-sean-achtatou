package com.tenapp.hoopersLite;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ModeOneLevels extends Activity {
    MediaPlayer bg_sound;
    String caller = "1";
    SharedPreferences getpref;
    ImageView[] image_levels = new ImageView[30];
    ImageView[] image_stars = new ImageView[30];
    int level = 1;
    ImageView level10img;
    ImageView level10st;
    TextView level10tv;
    ImageView level11img;
    ImageView level11st;
    TextView level11tv;
    ImageView level12img;
    ImageView level12st;
    TextView level12tv;
    ImageView level13img;
    ImageView level13st;
    TextView level13tv;
    ImageView level14img;
    ImageView level14st;
    TextView level14tv;
    ImageView level15img;
    ImageView level15st;
    TextView level15tv;
    ImageView level16img;
    ImageView level16st;
    TextView level16tv;
    ImageView level17img;
    ImageView level17st;
    TextView level17tv;
    ImageView level18img;
    ImageView level18st;
    TextView level18tv;
    ImageView level19img;
    ImageView level19st;
    TextView level19tv;
    ImageView level1img;
    ImageView level1st;
    TextView level1tv;
    ImageView level20img;
    ImageView level20st;
    TextView level20tv;
    ImageView level21img;
    ImageView level21st;
    TextView level21tv;
    ImageView level22img;
    ImageView level22st;
    TextView level22tv;
    ImageView level23img;
    ImageView level23st;
    TextView level23tv;
    ImageView level24img;
    ImageView level24st;
    TextView level24tv;
    ImageView level25img;
    ImageView level25st;
    TextView level25tv;
    ImageView level26img;
    ImageView level26st;
    TextView level26tv;
    ImageView level27img;
    ImageView level27st;
    TextView level27tv;
    ImageView level28img;
    ImageView level28st;
    TextView level28tv;
    ImageView level29img;
    ImageView level29st;
    TextView level29tv;
    ImageView level2img;
    ImageView level2st;
    TextView level2tv;
    ImageView level30img;
    ImageView level30st;
    TextView level30tv;
    ImageView level3img;
    ImageView level3st;
    TextView level3tv;
    ImageView level4img;
    ImageView level4st;
    TextView level4tv;
    ImageView level5img;
    ImageView level5st;
    TextView level5tv;
    ImageView level6img;
    ImageView level6st;
    TextView level6tv;
    ImageView level7img;
    ImageView level7st;
    TextView level7tv;
    ImageView level8img;
    ImageView level8st;
    TextView level8tv;
    ImageView level9img;
    ImageView level9st;
    TextView level9tv;
    int[] locked_images = new int[30];
    int min = 0;
    String minn;
    MediaPlayer mp;
    int new_level = 1;
    int score = 0;
    int sec = 0;
    String secc;
    int[] stars = new int[30];
    TextView[] text_views = new TextView[30];
    Long[] time = new Long[30];

    public void onDestroy() {
        super.onDestroy();
        if (this.bg_sound != null) {
            this.bg_sound.stop();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            Intent i = new Intent(getApplicationContext(), MainMenu.class);
            finish();
            startActivity(i);
        }
        return super.onKeyDown(keyCode, event);
    }

    /* JADX WARN: Type inference failed for: r10v0, types: [com.tenapp.hoopersLite.ModeOneLevels, android.app.Activity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onCreate(android.os.Bundle r11) {
        /*
            r10 = this;
            android.view.Window r5 = r10.getWindow()
            r6 = 1024(0x400, float:1.435E-42)
            r7 = 1024(0x400, float:1.435E-42)
            r5.setFlags(r6, r7)
            r5 = 1
            r10.requestWindowFeature(r5)
            super.onCreate(r11)
            r5 = 2130903044(0x7f030004, float:1.7412895E38)
            r10.setContentView(r5)
            android.content.Context r5 = r10.getBaseContext()
            r6 = 2130968585(0x7f040009, float:1.7545828E38)
            android.media.MediaPlayer r5 = android.media.MediaPlayer.create(r5, r6)
            r10.bg_sound = r5
            android.media.MediaPlayer r5 = r10.bg_sound
            if (r5 == 0) goto L_0x0034
            android.media.MediaPlayer r5 = r10.bg_sound
            r6 = 1
            r5.setLooping(r6)
            android.media.MediaPlayer r5 = r10.bg_sound
            r5.start()
        L_0x0034:
            android.content.Context r5 = r10.getBaseContext()
            r6 = 2130968578(0x7f040002, float:1.7545814E38)
            android.media.MediaPlayer r5 = android.media.MediaPlayer.create(r5, r6)
            r10.mp = r5
            java.lang.String r5 = "R.values.W1Levels"
            r6 = 2
            android.content.SharedPreferences r5 = r10.getSharedPreferences(r5, r6)     // Catch:{ Exception -> 0x0afb }
            r10.getpref = r5     // Catch:{ Exception -> 0x0afb }
            android.content.SharedPreferences r5 = r10.getpref     // Catch:{ Exception -> 0x0afb }
            java.lang.String r6 = "Level"
            r7 = 1
            int r5 = r5.getInt(r6, r7)     // Catch:{ Exception -> 0x0afb }
            r10.level = r5     // Catch:{ Exception -> 0x0afb }
        L_0x0055:
            int r5 = r10.level
            r6 = 30
            if (r5 <= r6) goto L_0x005f
            r5 = 30
            r10.level = r5
        L_0x005f:
            r0 = 0
        L_0x0060:
            int r5 = r10.level
            if (r0 < r5) goto L_0x0937
            r0 = 0
        L_0x0065:
            int r5 = r10.level
            if (r0 < r5) goto L_0x0972
            int[] r5 = r10.locked_images
            r6 = 0
            r7 = 2130837576(0x7f020048, float:1.728011E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 1
            r7 = 2130837652(0x7f020094, float:1.7280264E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 2
            r7 = 2130837663(0x7f02009f, float:1.7280286E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 3
            r7 = 2130837665(0x7f0200a1, float:1.728029E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 4
            r7 = 2130837666(0x7f0200a2, float:1.7280293E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 5
            r7 = 2130837667(0x7f0200a3, float:1.7280295E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 6
            r7 = 2130837668(0x7f0200a4, float:1.7280297E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 7
            r7 = 2130837669(0x7f0200a5, float:1.7280299E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 8
            r7 = 2130837670(0x7f0200a6, float:1.72803E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 9
            r7 = 2130837642(0x7f02008a, float:1.7280244E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 10
            r7 = 2130837643(0x7f02008b, float:1.7280246E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 11
            r7 = 2130837644(0x7f02008c, float:1.7280248E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 12
            r7 = 2130837645(0x7f02008d, float:1.728025E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 13
            r7 = 2130837646(0x7f02008e, float:1.7280252E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 14
            r7 = 2130837647(0x7f02008f, float:1.7280254E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 15
            r7 = 2130837648(0x7f020090, float:1.7280256E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 16
            r7 = 2130837649(0x7f020091, float:1.7280258E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 17
            r7 = 2130837650(0x7f020092, float:1.728026E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 18
            r7 = 2130837651(0x7f020093, float:1.7280262E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 19
            r7 = 2130837653(0x7f020095, float:1.7280266E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 20
            r7 = 2130837654(0x7f020096, float:1.7280268E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 21
            r7 = 2130837655(0x7f020097, float:1.728027E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 22
            r7 = 2130837656(0x7f020098, float:1.7280272E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 23
            r7 = 2130837657(0x7f020099, float:1.7280274E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 24
            r7 = 2130837658(0x7f02009a, float:1.7280276E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 25
            r7 = 2130837659(0x7f02009b, float:1.7280278E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 26
            r7 = 2130837660(0x7f02009c, float:1.728028E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 27
            r7 = 2130837661(0x7f02009d, float:1.7280282E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 28
            r7 = 2130837662(0x7f02009e, float:1.7280284E38)
            r5[r6] = r7
            int[] r5 = r10.locked_images
            r6 = 29
            r7 = 2130837664(0x7f0200a0, float:1.7280288E38)
            r5[r6] = r7
            r5 = 2131296306(0x7f090032, float:1.8210525E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level1tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 0
            android.widget.TextView r7 = r10.level1tv
            r5[r6] = r7
            r5 = 2131296277(0x7f090015, float:1.8210466E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level2tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 1
            android.widget.TextView r7 = r10.level2tv
            r5[r6] = r7
            r5 = 2131296307(0x7f090033, float:1.8210527E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level3tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 2
            android.widget.TextView r7 = r10.level3tv
            r5[r6] = r7
            r5 = 2131296308(0x7f090034, float:1.821053E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level4tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 3
            android.widget.TextView r7 = r10.level4tv
            r5[r6] = r7
            r5 = 2131296309(0x7f090035, float:1.8210531E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level5tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 4
            android.widget.TextView r7 = r10.level5tv
            r5[r6] = r7
            r5 = 2131296310(0x7f090036, float:1.8210533E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level6tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 5
            android.widget.TextView r7 = r10.level6tv
            r5[r6] = r7
            r5 = 2131296311(0x7f090037, float:1.8210535E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level7tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 6
            android.widget.TextView r7 = r10.level7tv
            r5[r6] = r7
            r5 = 2131296312(0x7f090038, float:1.8210537E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level8tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 7
            android.widget.TextView r7 = r10.level8tv
            r5[r6] = r7
            r5 = 2131296313(0x7f090039, float:1.821054E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level9tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 8
            android.widget.TextView r7 = r10.level9tv
            r5[r6] = r7
            r5 = 2131296314(0x7f09003a, float:1.8210541E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level10tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 9
            android.widget.TextView r7 = r10.level10tv
            r5[r6] = r7
            r5 = 2131296315(0x7f09003b, float:1.8210543E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level11tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 10
            android.widget.TextView r7 = r10.level11tv
            r5[r6] = r7
            r5 = 2131296316(0x7f09003c, float:1.8210545E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level12tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 11
            android.widget.TextView r7 = r10.level12tv
            r5[r6] = r7
            r5 = 2131296317(0x7f09003d, float:1.8210547E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level13tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 12
            android.widget.TextView r7 = r10.level13tv
            r5[r6] = r7
            r5 = 2131296318(0x7f09003e, float:1.821055E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level14tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 13
            android.widget.TextView r7 = r10.level14tv
            r5[r6] = r7
            r5 = 2131296319(0x7f09003f, float:1.8210551E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level15tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 14
            android.widget.TextView r7 = r10.level15tv
            r5[r6] = r7
            r5 = 2131296320(0x7f090040, float:1.8210553E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level16tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 15
            android.widget.TextView r7 = r10.level16tv
            r5[r6] = r7
            r5 = 2131296321(0x7f090041, float:1.8210555E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level17tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 16
            android.widget.TextView r7 = r10.level17tv
            r5[r6] = r7
            r5 = 2131296322(0x7f090042, float:1.8210557E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level18tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 17
            android.widget.TextView r7 = r10.level18tv
            r5[r6] = r7
            r5 = 2131296323(0x7f090043, float:1.821056E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level19tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 18
            android.widget.TextView r7 = r10.level19tv
            r5[r6] = r7
            r5 = 2131296324(0x7f090044, float:1.8210561E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level20tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 19
            android.widget.TextView r7 = r10.level20tv
            r5[r6] = r7
            r5 = 2131296325(0x7f090045, float:1.8210563E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level21tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 20
            android.widget.TextView r7 = r10.level21tv
            r5[r6] = r7
            r5 = 2131296326(0x7f090046, float:1.8210566E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level22tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 21
            android.widget.TextView r7 = r10.level22tv
            r5[r6] = r7
            r5 = 2131296327(0x7f090047, float:1.8210568E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level23tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 22
            android.widget.TextView r7 = r10.level23tv
            r5[r6] = r7
            r5 = 2131296328(0x7f090048, float:1.821057E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level24tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 23
            android.widget.TextView r7 = r10.level24tv
            r5[r6] = r7
            r5 = 2131296329(0x7f090049, float:1.8210572E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level25tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 24
            android.widget.TextView r7 = r10.level25tv
            r5[r6] = r7
            r5 = 2131296330(0x7f09004a, float:1.8210574E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level26tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 25
            android.widget.TextView r7 = r10.level26tv
            r5[r6] = r7
            r5 = 2131296331(0x7f09004b, float:1.8210576E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level27tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 26
            android.widget.TextView r7 = r10.level27tv
            r5[r6] = r7
            r5 = 2131296332(0x7f09004c, float:1.8210578E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level28tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 27
            android.widget.TextView r7 = r10.level28tv
            r5[r6] = r7
            r5 = 2131296333(0x7f09004d, float:1.821058E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level29tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 28
            android.widget.TextView r7 = r10.level29tv
            r5[r6] = r7
            r5 = 2131296334(0x7f09004e, float:1.8210582E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.TextView r11 = (android.widget.TextView) r11
            r10.level30tv = r11
            android.widget.TextView[] r5 = r10.text_views
            r6 = 29
            android.widget.TextView r7 = r10.level30tv
            r5[r6] = r7
            r5 = 2131296275(0x7f090013, float:1.8210462E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level1img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 0
            android.widget.ImageView r7 = r10.level1img
            r5[r6] = r7
            r5 = 2131296276(0x7f090014, float:1.8210464E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level2img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 1
            android.widget.ImageView r7 = r10.level2img
            r5[r6] = r7
            r5 = 2131296278(0x7f090016, float:1.8210468E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level3img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 2
            android.widget.ImageView r7 = r10.level3img
            r5[r6] = r7
            r5 = 2131296279(0x7f090017, float:1.821047E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level4img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 3
            android.widget.ImageView r7 = r10.level4img
            r5[r6] = r7
            r5 = 2131296280(0x7f090018, float:1.8210472E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level5img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 4
            android.widget.ImageView r7 = r10.level5img
            r5[r6] = r7
            r5 = 2131296281(0x7f090019, float:1.8210474E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level6img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 5
            android.widget.ImageView r7 = r10.level6img
            r5[r6] = r7
            r5 = 2131296282(0x7f09001a, float:1.8210476E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level7img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 6
            android.widget.ImageView r7 = r10.level7img
            r5[r6] = r7
            r5 = 2131296283(0x7f09001b, float:1.8210478E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level8img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 7
            android.widget.ImageView r7 = r10.level8img
            r5[r6] = r7
            r5 = 2131296284(0x7f09001c, float:1.821048E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level9img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 8
            android.widget.ImageView r7 = r10.level9img
            r5[r6] = r7
            r5 = 2131296285(0x7f09001d, float:1.8210482E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level10img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 9
            android.widget.ImageView r7 = r10.level10img
            r5[r6] = r7
            r5 = 2131296286(0x7f09001e, float:1.8210484E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level11img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 10
            android.widget.ImageView r7 = r10.level11img
            r5[r6] = r7
            r5 = 2131296287(0x7f09001f, float:1.8210486E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level12img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 11
            android.widget.ImageView r7 = r10.level12img
            r5[r6] = r7
            r5 = 2131296288(0x7f090020, float:1.8210488E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level13img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 12
            android.widget.ImageView r7 = r10.level13img
            r5[r6] = r7
            r5 = 2131296289(0x7f090021, float:1.821049E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level14img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 13
            android.widget.ImageView r7 = r10.level14img
            r5[r6] = r7
            r5 = 2131296290(0x7f090022, float:1.8210493E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level15img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 14
            android.widget.ImageView r7 = r10.level15img
            r5[r6] = r7
            r5 = 2131296291(0x7f090023, float:1.8210495E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level16img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 15
            android.widget.ImageView r7 = r10.level16img
            r5[r6] = r7
            r5 = 2131296292(0x7f090024, float:1.8210497E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level17img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 16
            android.widget.ImageView r7 = r10.level17img
            r5[r6] = r7
            r5 = 2131296293(0x7f090025, float:1.8210499E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level18img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 17
            android.widget.ImageView r7 = r10.level18img
            r5[r6] = r7
            r5 = 2131296294(0x7f090026, float:1.82105E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level19img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 18
            android.widget.ImageView r7 = r10.level19img
            r5[r6] = r7
            r5 = 2131296295(0x7f090027, float:1.8210503E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level20img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 19
            android.widget.ImageView r7 = r10.level20img
            r5[r6] = r7
            r5 = 2131296296(0x7f090028, float:1.8210505E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level21img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 20
            android.widget.ImageView r7 = r10.level21img
            r5[r6] = r7
            r5 = 2131296297(0x7f090029, float:1.8210507E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level22img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 21
            android.widget.ImageView r7 = r10.level22img
            r5[r6] = r7
            r5 = 2131296298(0x7f09002a, float:1.8210509E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level23img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 22
            android.widget.ImageView r7 = r10.level23img
            r5[r6] = r7
            r5 = 2131296299(0x7f09002b, float:1.821051E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level24img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 23
            android.widget.ImageView r7 = r10.level24img
            r5[r6] = r7
            r5 = 2131296300(0x7f09002c, float:1.8210513E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level25img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 24
            android.widget.ImageView r7 = r10.level25img
            r5[r6] = r7
            r5 = 2131296301(0x7f09002d, float:1.8210515E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level26img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 25
            android.widget.ImageView r7 = r10.level26img
            r5[r6] = r7
            r5 = 2131296302(0x7f09002e, float:1.8210517E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level27img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 26
            android.widget.ImageView r7 = r10.level27img
            r5[r6] = r7
            r5 = 2131296303(0x7f09002f, float:1.8210519E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level28img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 27
            android.widget.ImageView r7 = r10.level28img
            r5[r6] = r7
            r5 = 2131296304(0x7f090030, float:1.821052E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level29img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 28
            android.widget.ImageView r7 = r10.level29img
            r5[r6] = r7
            r5 = 2131296305(0x7f090031, float:1.8210523E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level30img = r11
            android.widget.ImageView[] r5 = r10.image_levels
            r6 = 29
            android.widget.ImageView r7 = r10.level30img
            r5[r6] = r7
            r5 = 2131296335(0x7f09004f, float:1.8210584E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level1st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 0
            android.widget.ImageView r7 = r10.level1st
            r5[r6] = r7
            r5 = 2131296336(0x7f090050, float:1.8210586E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level2st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 1
            android.widget.ImageView r7 = r10.level2st
            r5[r6] = r7
            r5 = 2131296337(0x7f090051, float:1.8210588E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level3st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 2
            android.widget.ImageView r7 = r10.level3st
            r5[r6] = r7
            r5 = 2131296338(0x7f090052, float:1.821059E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level4st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 3
            android.widget.ImageView r7 = r10.level4st
            r5[r6] = r7
            r5 = 2131296339(0x7f090053, float:1.8210592E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level5st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 4
            android.widget.ImageView r7 = r10.level5st
            r5[r6] = r7
            r5 = 2131296340(0x7f090054, float:1.8210594E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level6st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 5
            android.widget.ImageView r7 = r10.level6st
            r5[r6] = r7
            r5 = 2131296341(0x7f090055, float:1.8210596E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level7st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 6
            android.widget.ImageView r7 = r10.level7st
            r5[r6] = r7
            r5 = 2131296342(0x7f090056, float:1.8210598E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level8st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 7
            android.widget.ImageView r7 = r10.level8st
            r5[r6] = r7
            r5 = 2131296343(0x7f090057, float:1.82106E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level9st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 8
            android.widget.ImageView r7 = r10.level9st
            r5[r6] = r7
            r5 = 2131296344(0x7f090058, float:1.8210602E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level10st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 9
            android.widget.ImageView r7 = r10.level10st
            r5[r6] = r7
            r5 = 2131296345(0x7f090059, float:1.8210604E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level11st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 10
            android.widget.ImageView r7 = r10.level11st
            r5[r6] = r7
            r5 = 2131296346(0x7f09005a, float:1.8210606E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level12st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 11
            android.widget.ImageView r7 = r10.level12st
            r5[r6] = r7
            r5 = 2131296347(0x7f09005b, float:1.8210608E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level13st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 12
            android.widget.ImageView r7 = r10.level13st
            r5[r6] = r7
            r5 = 2131296348(0x7f09005c, float:1.821061E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level14st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 13
            android.widget.ImageView r7 = r10.level14st
            r5[r6] = r7
            r5 = 2131296349(0x7f09005d, float:1.8210612E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level15st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 14
            android.widget.ImageView r7 = r10.level15st
            r5[r6] = r7
            r5 = 2131296350(0x7f09005e, float:1.8210614E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level16st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 15
            android.widget.ImageView r7 = r10.level16st
            r5[r6] = r7
            r5 = 2131296351(0x7f09005f, float:1.8210616E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level17st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 16
            android.widget.ImageView r7 = r10.level17st
            r5[r6] = r7
            r5 = 2131296352(0x7f090060, float:1.8210618E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level18st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 17
            android.widget.ImageView r7 = r10.level18st
            r5[r6] = r7
            r5 = 2131296353(0x7f090061, float:1.821062E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level19st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 18
            android.widget.ImageView r7 = r10.level19st
            r5[r6] = r7
            r5 = 2131296354(0x7f090062, float:1.8210622E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level20st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 19
            android.widget.ImageView r7 = r10.level20st
            r5[r6] = r7
            r5 = 2131296355(0x7f090063, float:1.8210624E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level21st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 20
            android.widget.ImageView r7 = r10.level21st
            r5[r6] = r7
            r5 = 2131296356(0x7f090064, float:1.8210626E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level22st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 21
            android.widget.ImageView r7 = r10.level22st
            r5[r6] = r7
            r5 = 2131296357(0x7f090065, float:1.8210628E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level23st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 22
            android.widget.ImageView r7 = r10.level23st
            r5[r6] = r7
            r5 = 2131296358(0x7f090066, float:1.821063E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level24st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 23
            android.widget.ImageView r7 = r10.level24st
            r5[r6] = r7
            r5 = 2131296359(0x7f090067, float:1.8210632E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level25st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 24
            android.widget.ImageView r7 = r10.level25st
            r5[r6] = r7
            r5 = 2131296360(0x7f090068, float:1.8210634E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level26st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 25
            android.widget.ImageView r7 = r10.level26st
            r5[r6] = r7
            r5 = 2131296361(0x7f090069, float:1.8210637E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level27st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 26
            android.widget.ImageView r7 = r10.level27st
            r5[r6] = r7
            r5 = 2131296362(0x7f09006a, float:1.8210639E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level28st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 27
            android.widget.ImageView r7 = r10.level28st
            r5[r6] = r7
            r5 = 2131296363(0x7f09006b, float:1.821064E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level29st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 28
            android.widget.ImageView r7 = r10.level29st
            r5[r6] = r7
            r5 = 2131296364(0x7f09006c, float:1.8210643E38)
            android.view.View r11 = r10.findViewById(r5)
            android.widget.ImageView r11 = (android.widget.ImageView) r11
            r10.level30st = r11
            android.widget.ImageView[] r5 = r10.image_stars
            r6 = 29
            android.widget.ImageView r7 = r10.level30st
            r5[r6] = r7
            r0 = 0
        L_0x0806:
            r5 = 29
            if (r0 <= r5) goto L_0x0992
            android.widget.ImageView r5 = r10.level1img
            com.tenapp.hoopersLite.ModeOneLevels$1 r6 = new com.tenapp.hoopersLite.ModeOneLevels$1
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level2img
            com.tenapp.hoopersLite.ModeOneLevels$2 r6 = new com.tenapp.hoopersLite.ModeOneLevels$2
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level3img
            com.tenapp.hoopersLite.ModeOneLevels$3 r6 = new com.tenapp.hoopersLite.ModeOneLevels$3
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level4img
            com.tenapp.hoopersLite.ModeOneLevels$4 r6 = new com.tenapp.hoopersLite.ModeOneLevels$4
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level5img
            com.tenapp.hoopersLite.ModeOneLevels$5 r6 = new com.tenapp.hoopersLite.ModeOneLevels$5
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level6img
            com.tenapp.hoopersLite.ModeOneLevels$6 r6 = new com.tenapp.hoopersLite.ModeOneLevels$6
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level7img
            com.tenapp.hoopersLite.ModeOneLevels$7 r6 = new com.tenapp.hoopersLite.ModeOneLevels$7
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level8img
            com.tenapp.hoopersLite.ModeOneLevels$8 r6 = new com.tenapp.hoopersLite.ModeOneLevels$8
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level9img
            com.tenapp.hoopersLite.ModeOneLevels$9 r6 = new com.tenapp.hoopersLite.ModeOneLevels$9
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level10img
            com.tenapp.hoopersLite.ModeOneLevels$10 r6 = new com.tenapp.hoopersLite.ModeOneLevels$10
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level11img
            com.tenapp.hoopersLite.ModeOneLevels$11 r6 = new com.tenapp.hoopersLite.ModeOneLevels$11
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level12img
            com.tenapp.hoopersLite.ModeOneLevels$12 r6 = new com.tenapp.hoopersLite.ModeOneLevels$12
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level13img
            com.tenapp.hoopersLite.ModeOneLevels$13 r6 = new com.tenapp.hoopersLite.ModeOneLevels$13
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level14img
            com.tenapp.hoopersLite.ModeOneLevels$14 r6 = new com.tenapp.hoopersLite.ModeOneLevels$14
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level15img
            com.tenapp.hoopersLite.ModeOneLevels$15 r6 = new com.tenapp.hoopersLite.ModeOneLevels$15
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level16img
            com.tenapp.hoopersLite.ModeOneLevels$16 r6 = new com.tenapp.hoopersLite.ModeOneLevels$16
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level17img
            com.tenapp.hoopersLite.ModeOneLevels$17 r6 = new com.tenapp.hoopersLite.ModeOneLevels$17
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level18img
            com.tenapp.hoopersLite.ModeOneLevels$18 r6 = new com.tenapp.hoopersLite.ModeOneLevels$18
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level19img
            com.tenapp.hoopersLite.ModeOneLevels$19 r6 = new com.tenapp.hoopersLite.ModeOneLevels$19
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level20img
            com.tenapp.hoopersLite.ModeOneLevels$20 r6 = new com.tenapp.hoopersLite.ModeOneLevels$20
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level21img
            com.tenapp.hoopersLite.ModeOneLevels$21 r6 = new com.tenapp.hoopersLite.ModeOneLevels$21
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level22img
            com.tenapp.hoopersLite.ModeOneLevels$22 r6 = new com.tenapp.hoopersLite.ModeOneLevels$22
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level23img
            com.tenapp.hoopersLite.ModeOneLevels$23 r6 = new com.tenapp.hoopersLite.ModeOneLevels$23
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level24img
            com.tenapp.hoopersLite.ModeOneLevels$24 r6 = new com.tenapp.hoopersLite.ModeOneLevels$24
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level25img
            com.tenapp.hoopersLite.ModeOneLevels$25 r6 = new com.tenapp.hoopersLite.ModeOneLevels$25
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level26img
            com.tenapp.hoopersLite.ModeOneLevels$26 r6 = new com.tenapp.hoopersLite.ModeOneLevels$26
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level27img
            com.tenapp.hoopersLite.ModeOneLevels$27 r6 = new com.tenapp.hoopersLite.ModeOneLevels$27
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level28img
            com.tenapp.hoopersLite.ModeOneLevels$28 r6 = new com.tenapp.hoopersLite.ModeOneLevels$28
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level29img
            com.tenapp.hoopersLite.ModeOneLevels$29 r6 = new com.tenapp.hoopersLite.ModeOneLevels$29
            r6.<init>()
            r5.setOnClickListener(r6)
            android.widget.ImageView r5 = r10.level30img
            com.tenapp.hoopersLite.ModeOneLevels$30 r6 = new com.tenapp.hoopersLite.ModeOneLevels$30
            r6.<init>()
            r5.setOnClickListener(r6)
            return
        L_0x0937:
            java.lang.Long[] r5 = r10.time
            android.content.SharedPreferences r6 = r10.getpref
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "level"
            r7.<init>(r8)
            int r8 = r0 + 1
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r8 = 0
            long r6 = r6.getLong(r7, r8)
            java.lang.Long r6 = java.lang.Long.valueOf(r6)
            r5[r0] = r6
            java.lang.String r5 = "Time"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.Long[] r7 = r10.time
            r7 = r7[r0]
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            android.util.Log.d(r5, r6)
            int r0 = r0 + 1
            goto L_0x0060
        L_0x0972:
            int[] r5 = r10.stars
            android.content.SharedPreferences r6 = r10.getpref
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "star"
            r7.<init>(r8)
            int r8 = r0 + 1
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r8 = 1
            int r6 = r6.getInt(r7, r8)
            r5[r0] = r6
            int r0 = r0 + 1
            goto L_0x0065
        L_0x0992:
            r5 = 0
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r5)
            r5 = 0
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r5)
            r5 = 0
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r5)
            int r5 = r10.level
            if (r0 >= r5) goto L_0x0acf
            java.lang.Long[] r5 = r10.time
            r5 = r5[r0]
            long r5 = r5.longValue()
            r7 = 0
            int r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r5 == 0) goto L_0x0a7f
            java.lang.Long[] r5 = r10.time
            r5 = r5[r0]
            long r5 = r5.longValue()
            float r5 = (float) r5
            r6 = 1148846080(0x447a0000, float:1000.0)
            float r5 = r5 / r6
            int r5 = (int) r5
            r10.sec = r5
            int[] r5 = r10.stars
            r5 = r5[r0]
            r6 = 1
            if (r5 != r6) goto L_0x0a42
            r5 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r5)
        L_0x09ce:
            int r5 = r10.sec
            int r5 = r5 / 60
            r10.min = r5
            int r5 = r10.sec
            int r5 = r5 % 60
            r10.sec = r5
            int r5 = r10.min
            r6 = 10
            if (r5 >= r6) goto L_0x0a5d
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "0"
            r5.<init>(r6)
            int r6 = r10.min
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r1 = r5.toString()
        L_0x09f1:
            int r5 = r10.sec
            r6 = 10
            if (r5 >= r6) goto L_0x0a6d
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "0"
            r5.<init>(r6)
            int r6 = r10.sec
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r10.secc = r5
        L_0x0a0a:
            android.widget.TextView[] r5 = r10.text_views
            r5 = r5[r0]
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "Time:"
            r6.<init>(r7)
            java.lang.StringBuilder r6 = r6.append(r1)
            java.lang.String r7 = ":"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = r10.secc
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            r5.setText(r6)
        L_0x0a2c:
            int r5 = r10.min
            if (r5 != 0) goto L_0x0a89
            int r5 = r10.sec
            if (r5 != 0) goto L_0x0a89
            android.widget.ImageView[] r5 = r10.image_stars
            r5 = r5[r0]
            r6 = 2130837760(0x7f020100, float:1.7280483E38)
            r5.setImageResource(r6)
        L_0x0a3e:
            int r0 = r0 + 1
            goto L_0x0806
        L_0x0a42:
            int[] r5 = r10.stars
            r5 = r5[r0]
            r6 = 2
            if (r5 != r6) goto L_0x0a4f
            r5 = 1
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r5)
            goto L_0x09ce
        L_0x0a4f:
            int[] r5 = r10.stars
            r5 = r5[r0]
            r6 = 3
            if (r5 != r6) goto L_0x09ce
            r5 = 1
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r5)
            goto L_0x09ce
        L_0x0a5d:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            int r6 = r10.min
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r1 = r5.toString()
            goto L_0x09f1
        L_0x0a6d:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            int r6 = r10.sec
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r10.secc = r5
            goto L_0x0a0a
        L_0x0a7f:
            android.widget.TextView[] r5 = r10.text_views
            r5 = r5[r0]
            java.lang.String r6 = "Time:-- --"
            r5.setText(r6)
            goto L_0x0a2c
        L_0x0a89:
            int r5 = r10.level
            r6 = 1
            int r5 = r5 - r6
            if (r0 >= r5) goto L_0x0ac3
            boolean r5 = r2.booleanValue()
            if (r5 == 0) goto L_0x0aa0
            android.widget.ImageView[] r5 = r10.image_stars
            r5 = r5[r0]
            r6 = 2130837761(0x7f020101, float:1.7280485E38)
            r5.setImageResource(r6)
            goto L_0x0a3e
        L_0x0aa0:
            boolean r5 = r4.booleanValue()
            if (r5 == 0) goto L_0x0ab1
            android.widget.ImageView[] r5 = r10.image_stars
            r5 = r5[r0]
            r6 = 2130837762(0x7f020102, float:1.7280487E38)
            r5.setImageResource(r6)
            goto L_0x0a3e
        L_0x0ab1:
            boolean r5 = r3.booleanValue()
            if (r5 == 0) goto L_0x0a3e
            android.widget.ImageView[] r5 = r10.image_stars
            r5 = r5[r0]
            r6 = 2130837763(0x7f020103, float:1.728049E38)
            r5.setImageResource(r6)
            goto L_0x0a3e
        L_0x0ac3:
            android.widget.ImageView[] r5 = r10.image_stars
            r5 = r5[r0]
            r6 = 2130837760(0x7f020100, float:1.7280483E38)
            r5.setImageResource(r6)
            goto L_0x0a3e
        L_0x0acf:
            java.lang.String r5 = "Level"
            java.lang.String r6 = "in else"
            android.util.Log.d(r5, r6)
            android.widget.ImageView[] r5 = r10.image_stars
            r5 = r5[r0]
            r6 = 4
            r5.setVisibility(r6)
            android.widget.TextView[] r5 = r10.text_views
            r5 = r5[r0]
            r6 = 4
            r5.setVisibility(r6)
            android.widget.ImageView[] r5 = r10.image_levels
            r5 = r5[r0]
            int[] r6 = r10.locked_images
            r6 = r6[r0]
            r5.setImageResource(r6)
            android.widget.ImageView[] r5 = r10.image_levels
            r5 = r5[r0]
            r6 = 0
            r5.setClickable(r6)
            goto L_0x0a3e
        L_0x0afb:
            r5 = move-exception
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tenapp.hoopersLite.ModeOneLevels.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: private */
    public void start_game() {
        if (this.new_level >= 11) {
            Toast.makeText(getApplicationContext(), "More Levels Available in Pro Version", 0).show();
        } else if (this.new_level == 1) {
            Intent i = new Intent(getApplicationContext(), Training.class);
            finish();
            startActivity(i);
        } else {
            Intent i2 = new Intent(getApplicationContext(), EatingObjectsActivity.class);
            Bundle b = new Bundle();
            b.putInt("level", this.new_level);
            b.putInt("score", 0);
            i2.putExtras(b);
            finish();
            startActivity(i2);
        }
    }

    /* access modifiers changed from: private */
    public void start_game(boolean x) {
        Toast.makeText(getApplicationContext(), " Level Locked", 0).show();
    }
}
