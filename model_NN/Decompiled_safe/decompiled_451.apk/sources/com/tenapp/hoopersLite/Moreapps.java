package com.tenapp.hoopersLite;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class Moreapps extends Activity {
    ImageButton HL;
    ImageButton HP;
    ImageButton MML;
    ImageButton MMP;
    ImageButton airflight;
    ImageButton airflightpro;
    Button backButton;
    ImageButton baseball;
    ImageButton baseballpro;
    MediaPlayer bg_sound;
    ImageButton birdhunting;
    ImageButton birdhuntingpro;
    MediaPlayer clicksound;
    ImageButton gun;
    ImageButton gunpro;
    ImageButton talkingben;
    ImageButton talkingbenpro;
    ImageButton tfb;
    ImageButton tfbpro;

    public void onDestroy() {
        super.onDestroy();
        if (this.bg_sound != null) {
            this.bg_sound.stop();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        Intent intent = new Intent(this, MainMenu.class);
        finish();
        startActivity(intent);
        return true;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.moreapps);
        getWindow().setFlags(1024, 1024);
        this.bg_sound = MediaPlayer.create(getBaseContext(), (int) R.raw.mainmenusound);
        if (this.bg_sound != null) {
            this.bg_sound.setLooping(true);
            this.bg_sound.start();
        }
        this.backButton = (Button) findViewById(R.id.back);
        this.birdhunting = (ImageButton) findViewById(R.id.angry);
        this.birdhunting.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.BirdHuntingLite"));
                Moreapps.this.startActivity(i);
            }
        });
        this.birdhuntingpro = (ImageButton) findViewById(R.id.angrypro);
        this.birdhuntingpro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.BirdHuntingPro"));
                Moreapps.this.startActivity(i);
            }
        });
        this.airflight = (ImageButton) findViewById(R.id.airflight);
        this.airflight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.coolLanding"));
                Moreapps.this.startActivity(i);
            }
        });
        this.airflightpro = (ImageButton) findViewById(R.id.airflightpro);
        this.airflightpro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.FlightControlPro"));
                Moreapps.this.startActivity(i);
            }
        });
        this.baseball = (ImageButton) findViewById(R.id.baseball);
        this.baseball.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.Oziapp.BassBall2011"));
                Moreapps.this.startActivity(i);
            }
        });
        this.baseballpro = (ImageButton) findViewById(R.id.baseballpro);
        this.baseballpro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.Oziapp.BassBall2011Pro"));
                Moreapps.this.startActivity(i);
            }
        });
        this.talkingben = (ImageButton) findViewById(R.id.dogsout);
        this.talkingben.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.talkingpoochlite"));
                Moreapps.this.startActivity(i);
            }
        });
        this.talkingbenpro = (ImageButton) findViewById(R.id.dogsoutpro);
        this.talkingbenpro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.talkingpoochpro"));
                Moreapps.this.startActivity(i);
            }
        });
        this.gun = (ImageButton) findViewById(R.id.gunnsmoke);
        this.gun.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.gunsmokelite"));
                Moreapps.this.startActivity(i);
            }
        });
        this.gunpro = (ImageButton) findViewById(R.id.gunnsmokepro);
        this.gunpro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.gunsmokepro"));
                Moreapps.this.startActivity(i);
            }
        });
        this.backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Moreapps.this.backButton.setBackgroundResource(R.drawable.backoff);
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent intent = new Intent(Moreapps.this, MainMenu.class);
                Moreapps.this.finish();
                Moreapps.this.startActivity(intent);
            }
        });
        this.tfb = (ImageButton) findViewById(R.id.tfb);
        this.tfb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.talkingbabylite"));
                Moreapps.this.startActivity(i);
            }
        });
        this.tfbpro = (ImageButton) findViewById(R.id.tfbp);
        this.tfbpro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.talkingbabypro"));
                Moreapps.this.startActivity(i);
            }
        });
        this.MML = (ImageButton) findViewById(R.id.mmlite);
        this.MML.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.mousemaniafree"));
                Moreapps.this.startActivity(i);
            }
        });
        this.MMP = (ImageButton) findViewById(R.id.mmpro);
        this.MMP.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Moreapps.this.clicksound != null) {
                    Moreapps.this.clicksound.start();
                }
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://details?id=com.oziapp.mousemania"));
                Moreapps.this.startActivity(i);
            }
        });
    }
}
