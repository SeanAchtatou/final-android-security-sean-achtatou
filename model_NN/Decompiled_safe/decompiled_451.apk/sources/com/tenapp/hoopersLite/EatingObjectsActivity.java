package com.tenapp.hoopersLite;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.AdView;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.resource.Achievement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EatingObjectsActivity extends Activity {
    static boolean display_test = true;
    static boolean flg_ach1 = false;
    static boolean flg_ach2 = false;
    static boolean flg_ach3 = false;
    static boolean flg_ach4 = false;
    static int level;
    static boolean path_met = false;
    static int score = 0;
    ArrayList<Eaters2> Hooppers;
    ArrayList<Food> Spikes;
    public AdView _ad;
    CustomView _panelView;
    SharedPreferences.Editor achediter;
    MediaPlayer acheivment_sound;
    SharedPreferences achvments;
    Bitmap bee = null;
    Bitmap bg_image = null;
    MediaPlayer bg_sound;
    Paint bigPaint = new Paint();
    Paint blackP = new Paint();
    Food blob1 = null;
    Food blob2 = null;
    ArrayList<Food> blobs;
    Bitmap blu1;
    Bitmap blu2;
    Bitmap blu3;
    Bitmap blu4;
    Bitmap blu5;
    Bitmap blu6;
    Bitmap blu7;
    Bitmap blu8;
    int c = 0;
    boolean check_for_third = true;
    boolean check_obstacle = true;
    boolean check_path_crossing_from_blobs = false;
    MediaPlayer collision;
    int counter_to_check_path_crosssing_from_all_blobs = 0;
    int counter_to_disappear = 0;
    int counter_to_game_over = 0;
    int counter_to_help = 0;
    Bitmap cow = null;
    CustomizeDialog customizeDialog;
    Bitmap devil = null;
    int disappear_blog_number = 4;
    float disappearing_x;
    float disappearing_y;
    boolean dont_add_more_points = false;
    Food downspike;
    Bitmap downspikeimg = null;
    boolean draw_blob_text = false;
    int draw_finger_counter = 0;
    boolean draw_shashka_text = false;
    float draw_shashka_text_x = -10.0f;
    float draw_shashka_text_y = -10.0f;
    boolean draw_star_text = false;
    MediaPlayer eat_sound;
    Food extraspike;
    Bitmap finger1 = null;
    Bitmap finger2 = null;
    boolean flg_pause = false;
    ArrayList<Food> food;
    boolean game_over = false;
    TryAgainAlert game_over_alert;
    MediaPlayer gameover_sound;
    int height;
    int high_score = 0;
    MediaPlayer hit;
    Bitmap horizontol_obs_image = null;
    Bitmap horizontol_obs_image_spikes = null;
    Food hspike;
    Bitmap hspikeimg = null;
    boolean if_object_touched = false;
    Bitmap imgbl1 = null;
    Bitmap imgbl11 = null;
    Bitmap imgbl2 = null;
    Bitmap imgbl22 = null;
    Bitmap imgbl3 = null;
    Bitmap imgbl33 = null;
    Food leftspike;
    Bitmap leftspikeimg = null;
    MediaPlayer level_complete_sound;
    SharedPreferences level_time_poster;
    SharedPreferences.Editor level_time_poster_editor;
    Bitmap longbarr = null;
    /* access modifiers changed from: private */
    public Handler m_Handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    if (EatingObjectsActivity.this.bg_sound != null) {
                        EatingObjectsActivity.this.bg_sound.setLooping(false);
                        EatingObjectsActivity.this.bg_sound.stop();
                    }
                    if (EatingObjectsActivity.this.gameover_sound != null && EatingObjectsActivity.this.play_sound) {
                        EatingObjectsActivity.this.gameover_sound.start();
                    }
                    EatingObjectsActivity.this.game_over_alert.show();
                    SharedPreferences check_highscore = EatingObjectsActivity.this.getSharedPreferences("R.values.Highscore", 1);
                    if (check_highscore.getInt("Score", 0) < EatingObjectsActivity.score) {
                        SharedPreferences.Editor prefsEditor = check_highscore.edit();
                        prefsEditor.putInt("Score", EatingObjectsActivity.score);
                        prefsEditor.commit();
                        return;
                    }
                    return;
                case 1:
                    if (EatingObjectsActivity.this.bg_sound != null) {
                        EatingObjectsActivity.this.bg_sound.setLooping(false);
                        EatingObjectsActivity.this.bg_sound.stop();
                    }
                    if (EatingObjectsActivity.level >= EatingObjectsActivity.this.level_time_poster.getInt("Level", 0) && EatingObjectsActivity.level <= 30) {
                        EatingObjectsActivity.this.level_time_poster_editor.putInt("Level", EatingObjectsActivity.level + 1);
                        EatingObjectsActivity.this.level_time_poster_editor.putLong("level" + EatingObjectsActivity.level, EatingObjectsActivity.this.miliseconds);
                    }
                    if (EatingObjectsActivity.this.food.size() == 3 || EatingObjectsActivity.this.food.size() == 2) {
                        EatingObjectsActivity.this.level_time_poster_editor.putInt("star" + EatingObjectsActivity.level, 1);
                    } else if (EatingObjectsActivity.this.food.size() == 1) {
                        EatingObjectsActivity.this.level_time_poster_editor.putInt("star" + EatingObjectsActivity.level, 2);
                    }
                    if (EatingObjectsActivity.this.food.size() == 0) {
                        EatingObjectsActivity.this.level_time_poster_editor.putInt("star" + EatingObjectsActivity.level, 3);
                    }
                    EatingObjectsActivity.this.level_time_poster_editor.commit();
                    EatingObjectsActivity.this.customizeDialog.msg = "Score: " + EatingObjectsActivity.score + "\n Time: " + EatingObjectsActivity.this.minutes_to_display + " : " + EatingObjectsActivity.this.seconds_to_display;
                    EatingObjectsActivity.this.customizeDialog.as.setText("Score: " + EatingObjectsActivity.score + "\nTime: " + EatingObjectsActivity.this.minutes_to_display + " : " + EatingObjectsActivity.this.seconds_to_display);
                    EatingObjectsActivity.this.customizeDialog.levelTxt.setText("Level " + EatingObjectsActivity.level + " Cleared");
                    if (!EatingObjectsActivity.this.one_or_two_blobs.booleanValue()) {
                        if (EatingObjectsActivity.this.food.size() == 3 || EatingObjectsActivity.this.food.size() == 2) {
                            EatingObjectsActivity.this.customizeDialog.im.setImageResource(R.drawable.star_1);
                            EatingObjectsActivity.this.customizeDialog.congr.setImageResource(R.drawable.goodjob);
                        } else if (EatingObjectsActivity.this.food.size() == 1) {
                            EatingObjectsActivity.this.customizeDialog.im.setImageResource(R.drawable.star_2);
                            EatingObjectsActivity.this.customizeDialog.congr.setImageResource(R.drawable.yourock);
                        }
                        if (EatingObjectsActivity.this.food.size() == 0) {
                            EatingObjectsActivity.this.customizeDialog.im.setImageResource(R.drawable.star_3);
                            EatingObjectsActivity.this.customizeDialog.congr.setImageResource(R.drawable.excellent);
                        }
                    } else {
                        if (EatingObjectsActivity.this.food.size() == 3 || EatingObjectsActivity.this.food.size() == 2) {
                            EatingObjectsActivity.this.customizeDialog.im.setImageResource(R.drawable.star_1);
                            EatingObjectsActivity.this.customizeDialog.congr.setImageResource(R.drawable.goodjob);
                        } else if (EatingObjectsActivity.this.food.size() == 1) {
                            EatingObjectsActivity.this.customizeDialog.im.setImageResource(R.drawable.star_1);
                            EatingObjectsActivity.this.customizeDialog.congr.setImageResource(R.drawable.yourock);
                        }
                        if (EatingObjectsActivity.this.food.size() == 0) {
                            EatingObjectsActivity.this.customizeDialog.im.setImageResource(R.drawable.star_2);
                            EatingObjectsActivity.this.customizeDialog.congr.setImageResource(R.drawable.excellent);
                        }
                    }
                    if (EatingObjectsActivity.this.level_complete_sound != null && EatingObjectsActivity.this.play_sound) {
                        EatingObjectsActivity.this.level_complete_sound.start();
                    }
                    if (EatingObjectsActivity.level >= 10) {
                        Toast.makeText(EatingObjectsActivity.this.getApplicationContext(), "Buy pro for more Levels", 0).show();
                        EatingObjectsActivity.this.customizeDialog.gunbtn2.setBackgroundResource(R.drawable.submitscorebtn);
                    }
                    EatingObjectsActivity.this.customizeDialog.show();
                    return;
                default:
                    return;
            }
        }
    };
    long miliseconds;
    int minutes;
    String minutes_to_display;
    boolean next_level = false;
    Eaters2 objTouched = null;
    Food obs1 = null;
    Food obs2 = null;
    Food obs3 = null;
    Food obs4 = null;
    Food obs5 = null;
    ArrayList<Food> obstacles;
    Boolean one_or_two_blobs = false;
    Bitmap org1;
    Bitmap org2;
    Bitmap org3;
    Bitmap org4;
    Bitmap org5;
    Bitmap org6;
    Bitmap org7;
    Bitmap org8;
    boolean path_crossing_from_blob = false;
    PauseAlert pause_alert;
    Bitmap pause_btn = null;
    long pause_start_time = 0;
    long pause_time;
    Bitmap pizza = null;
    boolean play_music = Settings.music;
    boolean play_sound = Settings.sounds;
    Bitmap ranked = null;
    Eaters2 red = null;
    Bitmap red1;
    Bitmap red2;
    Bitmap red3;
    Bitmap red4;
    Bitmap red5;
    Bitmap red6;
    Bitmap red7;
    Bitmap red8;
    Bitmap restart_btn = null;
    Food rightspike;
    Bitmap rightspikeimg = null;
    GameThread runGame;
    int seconds;
    String seconds_to_display;
    Paint shashkaPaint = new Paint();
    Paint shashkaPaint2 = new Paint();
    Paint shashkaPaint3 = new Paint();
    boolean show_disappear = true;
    boolean show_fps = Settings.fps;
    Bitmap star = null;
    Food star1 = null;
    Food star2 = null;
    Food star3 = null;
    int star_ratings = 0;
    boolean start_disappearing_blobs = false;
    float starting_point_x;
    float starting_point_y;
    long starts = System.currentTimeMillis();
    Bitmap str2 = null;
    int text_size = 8;
    Eaters2 third = null;
    Food upspike;
    Bitmap upspikeimg = null;
    Bitmap verticle_obs_image = null;
    Bitmap verticle_obs_image_spike = null;
    Food vspike;
    Bitmap vspikeimg = null;
    MediaPlayer wall_hit;
    Bitmap which_level = null;
    int width;
    Eaters2 yellow = null;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        Intent i = new Intent(getApplicationContext(), MainMenu.class);
        finish();
        startActivity(i);
        return true;
    }

    /* JADX WARN: Type inference failed for: r12v0, types: [com.tenapp.hoopersLite.EatingObjectsActivity, android.content.Context, android.app.Activity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onCreate(android.os.Bundle r13) {
        /*
            r12 = this;
            super.onCreate(r13)
            android.view.Window r0 = r12.getWindow()
            r1 = 1024(0x400, float:1.435E-42)
            r2 = 1024(0x400, float:1.435E-42)
            r0.setFlags(r1, r2)
            r0 = 1
            r12.requestWindowFeature(r0)
            com.tenapp.hoopersLite.EatingObjectsActivity$CustomView r0 = new com.tenapp.hoopersLite.EatingObjectsActivity$CustomView
            r0.<init>(r12)
            r12._panelView = r0
            android.content.Context r0 = r12.getBaseContext()
            r1 = 2130968577(0x7f040001, float:1.7545812E38)
            android.media.MediaPlayer r0 = android.media.MediaPlayer.create(r0, r1)
            r12.bg_sound = r0
            android.content.Context r0 = r12.getBaseContext()
            r1 = 2130968582(0x7f040006, float:1.7545822E38)
            android.media.MediaPlayer r0 = android.media.MediaPlayer.create(r0, r1)
            r12.eat_sound = r0
            android.content.Context r0 = r12.getBaseContext()
            r1 = 2130968581(0x7f040005, float:1.754582E38)
            android.media.MediaPlayer r0 = android.media.MediaPlayer.create(r0, r1)
            r12.hit = r0
            android.content.Context r0 = r12.getBaseContext()
            r1 = 2130968586(0x7f04000a, float:1.754583E38)
            android.media.MediaPlayer r0 = android.media.MediaPlayer.create(r0, r1)
            r12.wall_hit = r0
            android.content.Context r0 = r12.getBaseContext()
            r1 = 2130968584(0x7f040008, float:1.7545826E38)
            android.media.MediaPlayer r0 = android.media.MediaPlayer.create(r0, r1)
            r12.level_complete_sound = r0
            android.content.Context r0 = r12.getBaseContext()
            r1 = 2130968579(0x7f040003, float:1.7545816E38)
            android.media.MediaPlayer r0 = android.media.MediaPlayer.create(r0, r1)
            r12.collision = r0
            android.content.Context r0 = r12.getBaseContext()
            r1 = 2130968580(0x7f040004, float:1.7545818E38)
            android.media.MediaPlayer r0 = android.media.MediaPlayer.create(r0, r1)
            r12.gameover_sound = r0
            java.lang.String r0 = "R.values.Acheivements"
            r1 = 2
            android.content.SharedPreferences r0 = r12.getSharedPreferences(r0, r1)
            r12.achvments = r0
            android.content.SharedPreferences r0 = r12.achvments
            android.content.SharedPreferences$Editor r0 = r0.edit()
            r12.achediter = r0
            android.content.Intent r0 = r12.getIntent()
            android.os.Bundle r6 = r0.getExtras()
            java.lang.String r0 = "level"
            int r0 = r6.getInt(r0)
            com.tenapp.hoopersLite.EatingObjectsActivity.level = r0
            java.lang.String r0 = "score"
            int r0 = r6.getInt(r0)
            com.tenapp.hoopersLite.EatingObjectsActivity.score = r0
            android.view.WindowManager r0 = r12.getWindowManager()
            android.view.Display r7 = r0.getDefaultDisplay()
            int r0 = r7.getWidth()
            r12.width = r0
            int r0 = r7.getHeight()
            r12.height = r0
            com.tenapp.hoopersLite.EatingObjectsActivity$CustomizeDialog r0 = new com.tenapp.hoopersLite.EatingObjectsActivity$CustomizeDialog
            r0.<init>(r12)
            r12.customizeDialog = r0
            com.tenapp.hoopersLite.EatingObjectsActivity$TryAgainAlert r0 = new com.tenapp.hoopersLite.EatingObjectsActivity$TryAgainAlert
            r0.<init>(r12)
            r12.game_over_alert = r0
            com.tenapp.hoopersLite.EatingObjectsActivity$PauseAlert r0 = new com.tenapp.hoopersLite.EatingObjectsActivity$PauseAlert
            r0.<init>(r12)
            r12.pause_alert = r0
            int r0 = r12.width
            if (r0 <= 0) goto L_0x00ce
            int r0 = r12.height
            if (r0 > 0) goto L_0x00d6
        L_0x00ce:
            int r0 = com.tenapp.hoopersLite.Settings.width
            r12.width = r0
            int r0 = com.tenapp.hoopersLite.Settings.height
            r12.height = r0
        L_0x00d6:
            int r0 = r12.height
            r1 = 50
            int r0 = r0 - r1
            r12.height = r0
            java.lang.String r0 = "R.values.W1Levels"
            r1 = 2
            android.content.SharedPreferences r0 = r12.getSharedPreferences(r0, r1)     // Catch:{ Exception -> 0x05fd }
            r12.level_time_poster = r0     // Catch:{ Exception -> 0x05fd }
            android.content.SharedPreferences r0 = r12.level_time_poster     // Catch:{ Exception -> 0x05fd }
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ Exception -> 0x05fd }
            r12.level_time_poster_editor = r0     // Catch:{ Exception -> 0x05fd }
        L_0x00ee:
            java.lang.String r0 = "R.values.Highscore"
            r1 = 1
            android.content.SharedPreferences r9 = r12.getSharedPreferences(r0, r1)
            java.lang.String r0 = "Score"
            r1 = 0
            int r0 = r9.getInt(r0, r1)
            r12.high_score = r0
            android.graphics.Paint r0 = r12.blackP
            r1 = 1097859072(0x41700000, float:15.0)
            r0.setTextSize(r1)
            android.graphics.Paint r0 = r12.blackP
            r1 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0.setColor(r1)
            android.graphics.Paint r0 = r12.blackP
            android.graphics.Paint$Align r1 = android.graphics.Paint.Align.CENTER
            r0.setTextAlign(r1)
            android.graphics.Paint r0 = r12.blackP
            r1 = 1
            r0.setFakeBoldText(r1)
            android.graphics.Paint r0 = r12.blackP
            r1 = 1
            r0.setAntiAlias(r1)
            android.graphics.Paint r0 = r12.blackP
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL
            r0.setStyle(r1)
            android.graphics.Paint r0 = r12.blackP
            android.graphics.Typeface r1 = android.graphics.Typeface.SANS_SERIF
            r0.setTypeface(r1)
            android.graphics.Paint r0 = r12.shashkaPaint
            r1 = 1091567616(0x41100000, float:9.0)
            r0.setTextSize(r1)
            android.graphics.Paint r0 = r12.shashkaPaint
            r1 = -1
            r0.setColor(r1)
            android.graphics.Paint r0 = r12.shashkaPaint
            r1 = 1
            r0.setFakeBoldText(r1)
            android.graphics.Paint r0 = r12.shashkaPaint
            android.graphics.Paint$Align r1 = android.graphics.Paint.Align.LEFT
            r0.setTextAlign(r1)
            android.graphics.Paint r0 = r12.shashkaPaint
            r1 = 1
            r0.setAntiAlias(r1)
            android.graphics.Paint r0 = r12.shashkaPaint
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL
            r0.setStyle(r1)
            android.graphics.Paint r0 = r12.shashkaPaint
            android.graphics.Typeface r1 = android.graphics.Typeface.SANS_SERIF
            r0.setTypeface(r1)
            android.graphics.Paint r0 = r12.bigPaint
            r1 = 1094713344(0x41400000, float:12.0)
            r0.setTextSize(r1)
            android.graphics.Paint r0 = r12.bigPaint
            r1 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0.setColor(r1)
            android.graphics.Paint r0 = r12.bigPaint
            r1 = 1
            r0.setFakeBoldText(r1)
            android.graphics.Paint r0 = r12.bigPaint
            r1 = 1
            r0.setAntiAlias(r1)
            android.graphics.Paint r0 = r12.bigPaint
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL
            r0.setStyle(r1)
            android.graphics.Paint r0 = r12.bigPaint
            android.graphics.Typeface r1 = android.graphics.Typeface.SANS_SERIF
            r0.setTypeface(r1)
            android.graphics.Paint r0 = r12.shashkaPaint3
            r1 = 1094713344(0x41400000, float:12.0)
            r0.setTextSize(r1)
            android.graphics.Paint r0 = r12.shashkaPaint3
            r1 = -1
            r0.setColor(r1)
            android.graphics.Paint r0 = r12.shashkaPaint3
            r1 = 1
            r0.setFakeBoldText(r1)
            android.graphics.Paint r0 = r12.shashkaPaint3
            android.graphics.Paint$Align r1 = android.graphics.Paint.Align.LEFT
            r0.setTextAlign(r1)
            android.graphics.Paint r0 = r12.shashkaPaint3
            r1 = 1
            r0.setAntiAlias(r1)
            android.graphics.Paint r0 = r12.shashkaPaint3
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL
            r0.setStyle(r1)
            android.graphics.Paint r0 = r12.shashkaPaint3
            android.graphics.Typeface r1 = android.graphics.Typeface.SANS_SERIF
            r0.setTypeface(r1)
            android.graphics.Paint r0 = r12.shashkaPaint2
            r1 = 1091567616(0x41100000, float:9.0)
            r0.setTextSize(r1)
            android.graphics.Paint r0 = r12.shashkaPaint2
            r1 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0.setColor(r1)
            android.graphics.Paint r0 = r12.shashkaPaint2
            r1 = 1
            r0.setFakeBoldText(r1)
            android.graphics.Paint r0 = r12.shashkaPaint2
            android.graphics.Paint$Align r1 = android.graphics.Paint.Align.LEFT
            r0.setTextAlign(r1)
            android.graphics.Paint r0 = r12.shashkaPaint2
            r1 = 1
            r0.setAntiAlias(r1)
            android.graphics.Paint r0 = r12.shashkaPaint2
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL
            r0.setStyle(r1)
            android.graphics.Paint r0 = r12.shashkaPaint2
            android.graphics.Typeface r1 = android.graphics.Typeface.SANS_SERIF
            r0.setTypeface(r1)
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837724(0x7f0200dc, float:1.728041E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.pause_btn = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837745(0x7f0200f1, float:1.7280453E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.restart_btn = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837515(0x7f02000b, float:1.7279986E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.bg_image = r0
            android.graphics.Bitmap r0 = r12.bg_image
            int r1 = r12.width
            int r2 = r12.height
            r3 = 1
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createScaledBitmap(r0, r1, r2, r3)
            r12.bg_image = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837759(0x7f0200ff, float:1.7280481E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.star = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837767(0x7f020107, float:1.7280497E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.str2 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837518(0x7f02000e, float:1.7279992E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.imgbl1 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837519(0x7f02000f, float:1.7279994E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.imgbl11 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837520(0x7f020010, float:1.7279996E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.imgbl2 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837521(0x7f020011, float:1.7279998E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.imgbl22 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837522(0x7f020012, float:1.728E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.imgbl3 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837523(0x7f020013, float:1.7280002E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.imgbl33 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837776(0x7f020110, float:1.7280516E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.verticle_obs_image = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837556(0x7f020034, float:1.728007E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.horizontol_obs_image = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837572(0x7f020044, float:1.7280102E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.hspikeimg = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837777(0x7f020111, float:1.7280518E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.vspikeimg = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837547(0x7f02002b, float:1.7280051E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.finger1 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837548(0x7f02002c, float:1.7280053E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.finger2 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837575(0x7f020047, float:1.7280108E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.leftspikeimg = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837748(0x7f0200f4, float:1.7280459E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.rightspikeimg = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837775(0x7f02010f, float:1.7280514E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.upspikeimg = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837543(0x7f020027, float:1.7280043E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.downspikeimg = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837671(0x7f0200a7, float:1.7280303E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.longbarr = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837715(0x7f0200d3, float:1.7280392E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.org1 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837716(0x7f0200d4, float:1.7280394E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.org2 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837717(0x7f0200d5, float:1.7280396E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.org3 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837718(0x7f0200d6, float:1.7280398E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.org4 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837719(0x7f0200d7, float:1.72804E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.org5 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837720(0x7f0200d8, float:1.7280402E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.org6 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837721(0x7f0200d9, float:1.7280404E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.org7 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837722(0x7f0200da, float:1.7280406E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.org8 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837732(0x7f0200e4, float:1.7280426E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.ranked = r0
            android.content.Context r0 = r12.getBaseContext()
            r1 = 2130968576(0x7f040000, float:1.754581E38)
            android.media.MediaPlayer r0 = android.media.MediaPlayer.create(r0, r1)
            r12.acheivment_sound = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837736(0x7f0200e8, float:1.7280434E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.red1 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837737(0x7f0200e9, float:1.7280437E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.red2 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837738(0x7f0200ea, float:1.7280439E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.red3 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837739(0x7f0200eb, float:1.728044E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.red4 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837740(0x7f0200ec, float:1.7280443E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.red5 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837741(0x7f0200ed, float:1.7280445E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.red6 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837742(0x7f0200ee, float:1.7280447E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.red7 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837743(0x7f0200ef, float:1.7280449E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.red8 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837524(0x7f020014, float:1.7280005E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.blu1 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837525(0x7f020015, float:1.7280007E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.blu2 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837526(0x7f020016, float:1.7280009E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.blu3 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837527(0x7f020017, float:1.728001E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.blu4 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837528(0x7f020018, float:1.7280013E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.blu5 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837529(0x7f020019, float:1.7280015E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.blu6 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837530(0x7f02001a, float:1.7280017E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.blu7 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837531(0x7f02001b, float:1.7280019E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.blu8 = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837514(0x7f02000a, float:1.7279984E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.bee = r0
            com.tenapp.hoopersLite.Eaters2 r0 = new com.tenapp.hoopersLite.Eaters2
            android.graphics.Bitmap r1 = r12.bee
            r2 = 40
            r3 = 70
            java.lang.String r4 = "yellow"
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.yellow = r0
            com.tenapp.hoopersLite.Eaters2 r0 = r12.yellow
            r1 = 1
            r0.direction_y = r1
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837536(0x7f020020, float:1.7280029E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.cow = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837537(0x7f020021, float:1.728003E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.devil = r0
            com.tenapp.hoopersLite.Eaters2 r0 = new com.tenapp.hoopersLite.Eaters2
            android.graphics.Bitmap r1 = r12.cow
            int r2 = r12.width
            r3 = 40
            int r2 = r2 - r3
            int r3 = r12.height
            r4 = 80
            int r3 = r3 - r4
            java.lang.String r4 = "red"
            r5 = 0
            r0.<init>(r1, r2, r3, r4, r5)
            r12.red = r0
            com.tenapp.hoopersLite.Eaters2 r0 = r12.yellow
            com.tenapp.hoopersLite.Eaters2 r1 = r12.red
            r0.partner = r1
            com.tenapp.hoopersLite.Eaters2 r0 = r12.red
            com.tenapp.hoopersLite.Eaters2 r1 = r12.yellow
            r0.partner = r1
            com.tenapp.hoopersLite.Eaters2 r0 = r12.red
            r1 = 0
            r0.direction_y = r1
            com.tenapp.hoopersLite.Eaters2 r0 = new com.tenapp.hoopersLite.Eaters2
            android.graphics.Bitmap r1 = r12.devil
            int r2 = r12.width
            r3 = 40
            int r2 = r2 - r3
            r3 = 60
            java.lang.String r4 = "third"
            r5 = 0
            r0.<init>(r1, r2, r3, r4, r5)
            r12.third = r0
            com.tenapp.hoopersLite.Eaters2 r0 = r12.third
            com.tenapp.hoopersLite.Eaters2 r1 = r12.red
            r0.partner = r1
            com.tenapp.hoopersLite.Eaters2 r0 = r12.third
            r1 = 1
            r0.direction_y = r1
            com.tenapp.hoopersLite.Eaters2 r0 = r12.yellow
            com.tenapp.hoopersLite.Eaters2 r1 = r12.red
            float r1 = r1.x
            r0.Xf = r1
            com.tenapp.hoopersLite.Eaters2 r0 = r12.yellow
            com.tenapp.hoopersLite.Eaters2 r1 = r12.red
            float r1 = r1.y
            r0.Yf = r1
            com.tenapp.hoopersLite.Eaters2 r0 = r12.red
            com.tenapp.hoopersLite.Eaters2 r1 = r12.yellow
            float r1 = r1.x
            r0.Xf = r1
            com.tenapp.hoopersLite.Eaters2 r0 = r12.red
            com.tenapp.hoopersLite.Eaters2 r1 = r12.yellow
            float r1 = r1.y
            r0.Yf = r1
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.star
            int r2 = r12.width
            int r2 = r2 / 2
            r3 = 40
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 1120403456(0x42c80000, float:100.0)
            android.graphics.Bitmap r4 = r12.str2
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.star1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.star
            int r2 = r12.width
            int r2 = r2 / 2
            r3 = 40
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 1128792064(0x43480000, float:200.0)
            android.graphics.Bitmap r4 = r12.str2
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.star2 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.star
            int r2 = r12.width
            int r2 = r2 / 2
            r3 = 40
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 1133903872(0x43960000, float:300.0)
            android.graphics.Bitmap r4 = r12.str2
            r5 = 3
            r0.<init>(r1, r2, r3, r4, r5)
            r12.star3 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Hooppers = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r0 = r12.Hooppers
            com.tenapp.hoopersLite.Eaters2 r1 = r12.yellow
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r0 = r12.Hooppers
            com.tenapp.hoopersLite.Eaters2 r1 = r12.red
            r0.add(r1)
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 1
            if (r0 != r1) goto L_0x0615
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl3
            com.tenapp.hoopersLite.Food r2 = r12.star1
            float r2 = r2.x
            r3 = 1092616192(0x41200000, float:10.0)
            float r2 = r2 + r3
            com.tenapp.hoopersLite.Food r3 = r12.star2
            float r3 = r3.y
            com.tenapp.hoopersLite.Food r4 = r12.star2
            android.graphics.Bitmap r4 = r4.bitmap
            int r4 = r4.getHeight()
            float r4 = (float) r4
            float r3 = r3 + r4
            r4 = 1112014848(0x42480000, float:50.0)
            float r3 = r3 + r4
            android.graphics.Bitmap r4 = r12.imgbl33
            r5 = 3
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            com.tenapp.hoopersLite.Food r0 = r12.star3
            int r1 = r12.height
            r2 = 150(0x96, float:2.1E-43)
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837610(0x7f02006a, float:1.7280179E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
        L_0x05cc:
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            int r0 = r0.size()
            r1 = 2
            if (r0 < r1) goto L_0x1b8d
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r12.one_or_two_blobs = r0
        L_0x05dc:
            com.google.android.apps.analytics.GoogleAnalyticsTracker r0 = com.tenapp.hoopersLite.Settings.tracker     // Catch:{ Exception -> 0x1b96 }
            r1 = 1
            java.lang.String r2 = "Hoopers Pro"
            java.lang.String r3 = "HobTheNob"
            r4 = 2
            r0.setCustomVar(r1, r2, r3, r4)     // Catch:{ Exception -> 0x1b96 }
            com.google.android.apps.analytics.GoogleAnalyticsTracker r0 = com.tenapp.hoopersLite.Settings.tracker     // Catch:{ Exception -> 0x1b96 }
            java.lang.String r1 = "/HobTheNob"
            r0.trackPageView(r1)     // Catch:{ Exception -> 0x1b96 }
        L_0x05ee:
            r0 = 0
            com.tenapp.hoopersLite.Eaters2.next_level_ok_stop_moving = r0
            android.content.Intent r10 = r12.getIntent()     // Catch:{ Exception -> 0x1bae }
            android.os.Bundle r11 = r10.getExtras()     // Catch:{ Exception -> 0x1bae }
            r12.StartVersusPanel(r11)     // Catch:{ Exception -> 0x1bae }
        L_0x05fc:
            return
        L_0x05fd:
            r0 = move-exception
            r8 = r0
            java.lang.String r0 = "Error"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "at line 214 Exception is "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r0, r1)
            goto L_0x00ee
        L_0x0615:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 2
            if (r0 != r1) goto L_0x0695
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = r12.star3
            int r1 = r12.height
            r2 = 200(0xc8, float:2.8E-43)
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            com.tenapp.hoopersLite.Food r2 = r12.star2
            float r2 = r2.x
            r3 = 1092616192(0x41200000, float:10.0)
            float r2 = r2 + r3
            com.tenapp.hoopersLite.Food r3 = r12.star1
            float r3 = r3.y
            com.tenapp.hoopersLite.Food r4 = r12.star1
            android.graphics.Bitmap r4 = r4.bitmap
            int r4 = r4.getHeight()
            float r4 = (float) r4
            float r3 = r3 + r4
            r4 = 1112014848(0x42480000, float:50.0)
            float r3 = r3 + r4
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837621(0x7f020075, float:1.7280201E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x0695:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 3
            if (r0 != r1) goto L_0x06fa
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl2
            int r2 = r12.width
            r3 = 50
            int r2 = r2 - r3
            float r2 = (float) r2
            com.tenapp.hoopersLite.Food r3 = r12.star1
            float r3 = r3.y
            r4 = 1106247680(0x41f00000, float:30.0)
            float r3 = r3 + r4
            android.graphics.Bitmap r4 = r12.imgbl22
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837632(0x7f020080, float:1.7280224E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x06fa:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 4
            if (r0 != r1) goto L_0x0781
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1112014848(0x42480000, float:50.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            com.tenapp.hoopersLite.Food r2 = r12.star2
            float r2 = r2.x
            com.tenapp.hoopersLite.Food r3 = r12.star2
            float r3 = r3.y
            r4 = 1125515264(0x43160000, float:150.0)
            float r3 = r3 - r4
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837634(0x7f020082, float:1.7280228E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x0781:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 5
            if (r0 != r1) goto L_0x0807
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1112014848(0x42480000, float:50.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl3
            com.tenapp.hoopersLite.Food r2 = r12.star2
            float r2 = r2.x
            int r3 = r12.height
            r4 = 150(0x96, float:2.1E-43)
            int r3 = r3 - r4
            float r3 = (float) r3
            android.graphics.Bitmap r4 = r12.imgbl33
            r5 = 3
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837635(0x7f020083, float:1.728023E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x0807:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 6
            if (r0 != r1) goto L_0x0895
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1112014848(0x42480000, float:50.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            int r1 = r12.height
            r2 = 150(0x96, float:2.1E-43)
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            int r1 = r12.height
            r2 = 150(0x96, float:2.1E-43)
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            com.tenapp.hoopersLite.Food r2 = r12.star2
            float r2 = r2.x
            int r3 = r12.height
            r4 = 150(0x96, float:2.1E-43)
            int r3 = r3 - r4
            float r3 = (float) r3
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837636(0x7f020084, float:1.7280232E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x0895:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 7
            if (r0 != r1) goto L_0x091b
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1112014848(0x42480000, float:50.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            int r1 = r12.height
            r2 = 150(0x96, float:2.1E-43)
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            com.tenapp.hoopersLite.Food r2 = r12.star2
            float r2 = r2.x
            r3 = 1133903872(0x43960000, float:300.0)
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837637(0x7f020085, float:1.7280234E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x091b:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 8
            if (r0 != r1) goto L_0x099e
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1112014848(0x42480000, float:50.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1120403456(0x42c80000, float:100.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl2
            com.tenapp.hoopersLite.Food r2 = r12.star2
            float r2 = r2.x
            r3 = 1137508352(0x43cd0000, float:410.0)
            android.graphics.Bitmap r4 = r12.imgbl22
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837638(0x7f020086, float:1.7280236E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x099e:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 9
            if (r0 != r1) goto L_0x0a39
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1112014848(0x42480000, float:50.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1120403456(0x42c80000, float:100.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl3
            com.tenapp.hoopersLite.Food r2 = r12.star2
            float r2 = r2.x
            r3 = 1092616192(0x41200000, float:10.0)
            float r2 = r2 + r3
            r3 = 1132068864(0x437a0000, float:250.0)
            android.graphics.Bitmap r4 = r12.imgbl33
            r5 = 3
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.horizontol_obs_image
            r2 = 1124204544(0x43020000, float:130.0)
            r3 = 1137180672(0x43c80000, float:400.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.obs1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs1
            r0.add(r1)
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837639(0x7f020087, float:1.7280238E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x0a39:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 10
            if (r0 != r1) goto L_0x0ad8
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1120403456(0x42c80000, float:100.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            int r1 = r12.height
            r2 = 50
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            com.tenapp.hoopersLite.Food r2 = r12.star2
            float r2 = r2.x
            r3 = 1092616192(0x41200000, float:10.0)
            float r2 = r2 + r3
            r3 = 1128792064(0x43480000, float:200.0)
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.horizontol_obs_image
            r2 = 1124204544(0x43020000, float:130.0)
            r3 = 1137180672(0x43c80000, float:400.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.obs1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs1
            r0.add(r1)
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837611(0x7f02006b, float:1.728018E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x0ad8:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 11
            if (r0 != r1) goto L_0x0b77
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1120403456(0x42c80000, float:100.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            com.tenapp.hoopersLite.Food r2 = r12.star2
            float r2 = r2.x
            r3 = 1092616192(0x41200000, float:10.0)
            float r2 = r2 + r3
            int r3 = r12.height
            r4 = 50
            int r3 = r3 - r4
            float r3 = (float) r3
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.horizontol_obs_image
            r2 = 1124204544(0x43020000, float:130.0)
            r3 = 1128792064(0x43480000, float:200.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.obs1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs1
            r0.add(r1)
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837612(0x7f02006c, float:1.7280183E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x0b77:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 12
            if (r0 != r1) goto L_0x0c16
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1106247680(0x41f00000, float:30.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1126825984(0x432a0000, float:170.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1126825984(0x432a0000, float:170.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            int r1 = r12.height
            r2 = 100
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl3
            int r2 = r12.width
            r3 = 60
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 1132068864(0x437a0000, float:250.0)
            android.graphics.Bitmap r4 = r12.imgbl33
            r5 = 3
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.verticle_obs_image
            r2 = 1121714176(0x42dc0000, float:110.0)
            r3 = 1130758144(0x43660000, float:230.0)
            r4 = 0
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.obs1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs1
            r0.add(r1)
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837613(0x7f02006d, float:1.7280185E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x0c16:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 13
            if (r0 != r1) goto L_0x0cc7
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1126825984(0x432a0000, float:170.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1126825984(0x432a0000, float:170.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1126825984(0x432a0000, float:170.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1112014848(0x42480000, float:50.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1124204544(0x43020000, float:130.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1129447424(0x43520000, float:210.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl2
            int r2 = r12.width
            r3 = 60
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 1124859904(0x430c0000, float:140.0)
            android.graphics.Bitmap r4 = r12.imgbl22
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.verticle_obs_image
            r2 = 1125515264(0x43160000, float:150.0)
            r3 = 1117782016(0x42a00000, float:80.0)
            r4 = 0
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.obs1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.verticle_obs_image
            r2 = 1125515264(0x43160000, float:150.0)
            r3 = 1126170624(0x43200000, float:160.0)
            r4 = 0
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.obs2 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs2
            r0.add(r1)
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837614(0x7f02006e, float:1.7280187E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x0cc7:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 14
            if (r0 != r1) goto L_0x0d79
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1101004800(0x41a00000, float:20.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            int r1 = r12.width
            r2 = 50
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1126170624(0x43200000, float:160.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1126170624(0x43200000, float:160.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1126170624(0x43200000, float:160.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl2
            r2 = 1126170624(0x43200000, float:160.0)
            int r3 = r12.height
            r4 = 100
            int r3 = r3 - r4
            float r3 = (float) r3
            android.graphics.Bitmap r4 = r12.imgbl22
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.horizontol_obs_image
            r2 = 0
            r3 = 1133903872(0x43960000, float:300.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.obs1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.horizontol_obs_image
            r2 = 1132068864(0x437a0000, float:250.0)
            r3 = 1133903872(0x43960000, float:300.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.obs2 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs2
            r0.add(r1)
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837615(0x7f02006f, float:1.728019E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x0d79:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 15
            if (r0 != r1) goto L_0x0e2c
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1092616192(0x41200000, float:10.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1126170624(0x43200000, float:160.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            int r1 = r12.width
            r2 = 50
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1125515264(0x43160000, float:150.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1125515264(0x43160000, float:150.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1125515264(0x43160000, float:150.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl2
            r2 = 1126170624(0x43200000, float:160.0)
            int r3 = r12.height
            r4 = 100
            int r3 = r3 - r4
            float r3 = (float) r3
            android.graphics.Bitmap r4 = r12.imgbl22
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.horizontol_obs_image
            r2 = 1112014848(0x42480000, float:50.0)
            r3 = 1123024896(0x42f00000, float:120.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.obs1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.horizontol_obs_image
            r2 = 1130102784(0x435c0000, float:220.0)
            r3 = 1123024896(0x42f00000, float:120.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.obs2 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs2
            r0.add(r1)
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837616(0x7f020070, float:1.7280191E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x0e2c:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 16
            if (r0 != r1) goto L_0x0ead
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            int r2 = r12.width
            r3 = 50
            int r2 = r2 - r3
            float r2 = (float) r2
            com.tenapp.hoopersLite.Food r3 = r12.star1
            float r3 = r3.y
            r4 = 1106247680(0x41f00000, float:30.0)
            float r3 = r3 + r4
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl2
            r2 = 1112014848(0x42480000, float:50.0)
            int r3 = r12.height
            r4 = 100
            int r3 = r3 - r4
            float r3 = (float) r3
            android.graphics.Bitmap r4 = r12.imgbl22
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob2 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob2
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837617(0x7f020071, float:1.7280193E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x0ead:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 17
            if (r0 != r1) goto L_0x0f4d
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1112014848(0x42480000, float:50.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            com.tenapp.hoopersLite.Food r2 = r12.star2
            float r2 = r2.x
            int r3 = r12.height
            r4 = 150(0x96, float:2.1E-43)
            int r3 = r3 - r4
            float r3 = (float) r3
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl2
            com.tenapp.hoopersLite.Food r2 = r12.star2
            float r2 = r2.x
            r3 = 1125515264(0x43160000, float:150.0)
            android.graphics.Bitmap r4 = r12.imgbl22
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob2 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob2
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837618(0x7f020072, float:1.7280195E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x0f4d:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 18
            if (r0 != r1) goto L_0x0ff9
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1124859904(0x430c0000, float:140.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1101004800(0x41a00000, float:20.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            int r1 = r12.width
            r2 = 50
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            int r1 = r12.height
            r2 = 100
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            int r1 = r12.height
            r2 = 100
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            int r1 = r12.height
            r2 = 100
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            r2 = 1125515264(0x43160000, float:150.0)
            int r3 = r12.height
            r4 = 150(0x96, float:2.1E-43)
            int r3 = r3 - r4
            float r3 = (float) r3
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl2
            r2 = 1125515264(0x43160000, float:150.0)
            r3 = 1125515264(0x43160000, float:150.0)
            android.graphics.Bitmap r4 = r12.imgbl22
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob2 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob2
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837619(0x7f020073, float:1.7280197E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x0ff9:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 19
            if (r0 != r1) goto L_0x1091
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1120403456(0x42c80000, float:100.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1139802112(0x43f00000, float:480.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1134886912(0x43a50000, float:330.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            r2 = 1125187584(0x43110000, float:145.0)
            r3 = 1130758144(0x43660000, float:230.0)
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl2
            r2 = 1112014848(0x42480000, float:50.0)
            r3 = 1140129792(0x43f50000, float:490.0)
            android.graphics.Bitmap r4 = r12.imgbl22
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob2 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob2
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837620(0x7f020074, float:1.72802E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x1091:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 20
            if (r0 != r1) goto L_0x112d
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1112014848(0x42480000, float:50.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            int r1 = r12.width
            r2 = 50
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1120403456(0x42c80000, float:100.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1140457472(0x43fa0000, float:500.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1140457472(0x43fa0000, float:500.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            r2 = 1124859904(0x430c0000, float:140.0)
            r3 = 1130758144(0x43660000, float:230.0)
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl2
            r2 = 1124859904(0x430c0000, float:140.0)
            r3 = 1140129792(0x43f50000, float:490.0)
            android.graphics.Bitmap r4 = r12.imgbl22
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob2 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob2
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837622(0x7f020076, float:1.7280203E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x112d:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 21
            if (r0 != r1) goto L_0x11c3
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1120403456(0x42c80000, float:100.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1140457472(0x43fa0000, float:500.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            r2 = 1125187584(0x43110000, float:145.0)
            r3 = 1128792064(0x43480000, float:200.0)
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.hspikeimg
            r2 = 1123680256(0x42fa0000, float:125.0)
            r3 = 1125515264(0x43160000, float:150.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.hspike = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.hspike
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837623(0x7f020077, float:1.7280205E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x11c3:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 22
            if (r0 != r1) goto L_0x1274
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1109393408(0x42200000, float:40.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1119092736(0x42b40000, float:90.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            int r1 = r12.width
            r2 = 40
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            r2 = 1124532224(0x43070000, float:135.0)
            r3 = 1128792064(0x43480000, float:200.0)
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl2
            r2 = 1109393408(0x42200000, float:40.0)
            r3 = 1140129792(0x43f50000, float:490.0)
            android.graphics.Bitmap r4 = r12.imgbl22
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob2 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob2
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.vspikeimg
            r2 = 1126825984(0x432a0000, float:170.0)
            r3 = 1133248512(0x438c0000, float:280.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.vspike = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.vspike
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837624(0x7f020078, float:1.7280207E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x1274:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 23
            if (r0 != r1) goto L_0x1350
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1128792064(0x43480000, float:200.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1128792064(0x43480000, float:200.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1128792064(0x43480000, float:200.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1120403456(0x42c80000, float:100.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            r2 = 1124532224(0x43070000, float:135.0)
            r3 = 1130758144(0x43660000, float:230.0)
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl2
            r2 = 1109393408(0x42200000, float:40.0)
            r3 = 1140129792(0x43f50000, float:490.0)
            android.graphics.Bitmap r4 = r12.imgbl22
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob2 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob2
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.hspikeimg
            r2 = 1127481344(0x43340000, float:180.0)
            r3 = 1125515264(0x43160000, float:150.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.hspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.leftspikeimg
            r2 = 0
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.leftspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.rightspikeimg
            int r2 = r12.width
            android.graphics.Bitmap r3 = r12.rightspikeimg
            int r3 = r3.getWidth()
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.rightspike = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.hspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.leftspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.rightspike
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837625(0x7f020079, float:1.728021E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x1350:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 24
            if (r0 != r1) goto L_0x142c
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1112014848(0x42480000, float:50.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            r2 = 1124532224(0x43070000, float:135.0)
            r3 = 1120403456(0x42c80000, float:100.0)
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl2
            r2 = 1109393408(0x42200000, float:40.0)
            r3 = 1120403456(0x42c80000, float:100.0)
            android.graphics.Bitmap r4 = r12.imgbl22
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob2 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob2
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.upspikeimg
            r2 = 0
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.upspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.downspikeimg
            r2 = 0
            int r3 = r12.height
            android.graphics.Bitmap r4 = r12.downspikeimg
            int r4 = r4.getHeight()
            int r3 = r3 - r4
            float r3 = (float) r3
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.downspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.hspikeimg
            r2 = 1127481344(0x43340000, float:180.0)
            r3 = 1133903872(0x43960000, float:300.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.hspike = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.hspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.upspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.downspike
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837626(0x7f02007a, float:1.7280211E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x142c:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 25
            if (r0 != r1) goto L_0x1524
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1130758144(0x43660000, float:230.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1140457472(0x43fa0000, float:500.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            int r2 = r12.width
            r3 = 200(0xc8, float:2.8E-43)
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 1128792064(0x43480000, float:200.0)
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.upspikeimg
            r2 = 0
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.upspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.downspikeimg
            r2 = 0
            int r3 = r12.height
            android.graphics.Bitmap r4 = r12.downspikeimg
            int r4 = r4.getHeight()
            int r3 = r3 - r4
            float r3 = (float) r3
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.downspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.leftspikeimg
            r2 = 0
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.leftspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.rightspikeimg
            int r2 = r12.width
            android.graphics.Bitmap r3 = r12.rightspikeimg
            int r3 = r3.getWidth()
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.rightspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.hspikeimg
            r2 = 1127481344(0x43340000, float:180.0)
            r3 = 1133903872(0x43960000, float:300.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.hspike = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.hspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.upspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.downspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.rightspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.leftspike
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837627(0x7f02007b, float:1.7280213E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x1524:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 26
            if (r0 != r1) goto L_0x161b
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1140457472(0x43fa0000, float:500.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl3
            int r2 = r12.width
            r3 = 200(0xc8, float:2.8E-43)
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 1128792064(0x43480000, float:200.0)
            android.graphics.Bitmap r4 = r12.imgbl33
            r5 = 3
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.upspikeimg
            r2 = 0
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.upspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.downspikeimg
            r2 = 0
            int r3 = r12.height
            android.graphics.Bitmap r4 = r12.downspikeimg
            int r4 = r4.getHeight()
            int r3 = r3 - r4
            float r3 = (float) r3
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.downspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.leftspikeimg
            r2 = 0
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.leftspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.rightspikeimg
            int r2 = r12.width
            android.graphics.Bitmap r3 = r12.rightspikeimg
            int r3 = r3.getWidth()
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.rightspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.hspikeimg
            r2 = 1127481344(0x43340000, float:180.0)
            r3 = 1133903872(0x43960000, float:300.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.hspike = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.rightspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.leftspike
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.longbarr
            r2 = 1128792064(0x43480000, float:200.0)
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.obs1 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs1
            r0.add(r1)
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837628(0x7f02007c, float:1.7280215E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x161b:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 27
            if (r0 != r1) goto L_0x175a
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1116471296(0x428c0000, float:70.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            int r1 = r12.width
            r2 = 100
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1132068864(0x437a0000, float:250.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1128792064(0x43480000, float:200.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            int r2 = r12.width
            r3 = 150(0x96, float:2.1E-43)
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 1120403456(0x42c80000, float:100.0)
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl2
            int r2 = r12.width
            r3 = 350(0x15e, float:4.9E-43)
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r12.height
            r4 = 200(0xc8, float:2.8E-43)
            int r3 = r3 - r4
            float r3 = (float) r3
            android.graphics.Bitmap r4 = r12.imgbl22
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob2 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.upspikeimg
            r2 = 0
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.upspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.downspikeimg
            r2 = 0
            int r3 = r12.height
            android.graphics.Bitmap r4 = r12.downspikeimg
            int r4 = r4.getHeight()
            int r3 = r3 - r4
            float r3 = (float) r3
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.downspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.leftspikeimg
            r2 = 0
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.leftspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.rightspikeimg
            int r2 = r12.width
            android.graphics.Bitmap r3 = r12.rightspikeimg
            int r3 = r3.getWidth()
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.rightspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.hspikeimg
            r2 = 1123024896(0x42f00000, float:120.0)
            r3 = 1137180672(0x43c80000, float:400.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.hspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.hspikeimg
            r2 = 1123024896(0x42f00000, float:120.0)
            r3 = 1128792064(0x43480000, float:200.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.vspike = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.rightspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.leftspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.upspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.downspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.hspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.vspike
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.verticle_obs_image
            r2 = 1126170624(0x43200000, float:160.0)
            r3 = 1138819072(0x43e10000, float:450.0)
            r4 = 0
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.obs1 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs1
            r0.add(r1)
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837629(0x7f02007d, float:1.7280217E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x175a:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 28
            if (r0 != r1) goto L_0x18b1
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1116471296(0x428c0000, float:70.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            int r1 = r12.width
            r2 = 100
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1137180672(0x43c80000, float:400.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            int r2 = r12.width
            r3 = 150(0x96, float:2.1E-43)
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 1120403456(0x42c80000, float:100.0)
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.upspikeimg
            r2 = 0
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.upspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.downspikeimg
            r2 = 0
            int r3 = r12.height
            android.graphics.Bitmap r4 = r12.downspikeimg
            int r4 = r4.getHeight()
            int r3 = r3 - r4
            float r3 = (float) r3
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.downspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.leftspikeimg
            r2 = 0
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.leftspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.rightspikeimg
            int r2 = r12.width
            android.graphics.Bitmap r3 = r12.rightspikeimg
            int r3 = r3.getWidth()
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.rightspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.hspikeimg
            r2 = 1123024896(0x42f00000, float:120.0)
            r3 = 1137180672(0x43c80000, float:400.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.hspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.hspikeimg
            r2 = 1123024896(0x42f00000, float:120.0)
            r3 = 1128792064(0x43480000, float:200.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.vspike = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.rightspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.leftspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.upspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.downspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.hspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.vspike
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.verticle_obs_image
            r2 = 1126170624(0x43200000, float:160.0)
            r3 = 1138819072(0x43e10000, float:450.0)
            r4 = 0
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.obs1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.verticle_obs_image
            r2 = 1114636288(0x42700000, float:60.0)
            r3 = 1132068864(0x437a0000, float:250.0)
            r4 = 0
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.obs2 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.verticle_obs_image
            int r2 = r12.width
            r3 = 100
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 1132068864(0x437a0000, float:250.0)
            r4 = 0
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.obs3 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs3
            r0.add(r1)
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837630(0x7f02007e, float:1.728022E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x18b1:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 29
            if (r0 != r1) goto L_0x1a48
            java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r0 = r12.Hooppers
            r1 = 1
            r0.remove(r1)
            com.tenapp.hoopersLite.Eaters2 r0 = r12.red
            com.tenapp.hoopersLite.Eaters2 r1 = r12.yellow
            float r1 = r1.y
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r0 = r12.Hooppers
            com.tenapp.hoopersLite.Eaters2 r1 = r12.red
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1120403456(0x42c80000, float:100.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            int r1 = r12.width
            r2 = 130(0x82, float:1.82E-43)
            int r1 = r1 - r2
            float r1 = (float) r1
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1125515264(0x43160000, float:150.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1139474432(0x43eb0000, float:470.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1139474432(0x43eb0000, float:470.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            int r2 = r12.width
            r3 = 150(0x96, float:2.1E-43)
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 1120403456(0x42c80000, float:100.0)
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.upspikeimg
            r2 = 0
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.upspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.downspikeimg
            r2 = 0
            int r3 = r12.height
            android.graphics.Bitmap r4 = r12.downspikeimg
            int r4 = r4.getHeight()
            int r3 = r3 - r4
            float r3 = (float) r3
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.downspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.leftspikeimg
            r2 = 0
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.leftspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.rightspikeimg
            int r2 = r12.width
            android.graphics.Bitmap r3 = r12.rightspikeimg
            int r3 = r3.getWidth()
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.rightspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.hspikeimg
            r2 = 1123024896(0x42f00000, float:120.0)
            r3 = 1137180672(0x43c80000, float:400.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.hspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.hspikeimg
            r2 = 1123024896(0x42f00000, float:120.0)
            r3 = 1128792064(0x43480000, float:200.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.vspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.vspikeimg
            int r2 = r12.width
            r3 = 100
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 1132068864(0x437a0000, float:250.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.extraspike = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.rightspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.leftspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.upspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.downspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.hspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.vspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.extraspike
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.verticle_obs_image
            r2 = 1132920832(0x43870000, float:270.0)
            r3 = 1138819072(0x43e10000, float:450.0)
            r4 = 0
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.obs1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.verticle_obs_image
            r2 = 1112014848(0x42480000, float:50.0)
            r3 = 1138819072(0x43e10000, float:450.0)
            r4 = 0
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.obs2 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.verticle_obs_image
            r2 = 1120403456(0x42c80000, float:100.0)
            r3 = 1132068864(0x437a0000, float:250.0)
            r4 = 0
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.obs4 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.verticle_obs_image
            r2 = 1124859904(0x430c0000, float:140.0)
            r3 = 1120403456(0x42c80000, float:100.0)
            r4 = 0
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.obs5 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs4
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.obstacles
            com.tenapp.hoopersLite.Food r1 = r12.obs5
            r0.add(r1)
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837631(0x7f02007f, float:1.7280222E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x1a48:
            int r0 = com.tenapp.hoopersLite.EatingObjectsActivity.level
            r1 = 30
            if (r0 != r1) goto L_0x05cc
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.food = r0
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1130102784(0x435c0000, float:220.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1114636288(0x42700000, float:60.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1127481344(0x43340000, float:180.0)
            r0.x = r1
            com.tenapp.hoopersLite.Food r0 = r12.star1
            r1 = 1120403456(0x42c80000, float:100.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star2
            r1 = 1133903872(0x43960000, float:300.0)
            r0.y = r1
            com.tenapp.hoopersLite.Food r0 = r12.star3
            r1 = 1140457472(0x43fa0000, float:500.0)
            r0.y = r1
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star2
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.food
            com.tenapp.hoopersLite.Food r1 = r12.star3
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.blobs = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl1
            int r2 = r12.width
            r3 = 150(0x96, float:2.1E-43)
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 1124859904(0x430c0000, float:140.0)
            android.graphics.Bitmap r4 = r12.imgbl11
            r5 = 2
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob1 = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.imgbl2
            int r2 = r12.width
            r3 = 100
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 1132068864(0x437a0000, float:250.0)
            android.graphics.Bitmap r4 = r12.imgbl22
            r5 = 1
            r0.<init>(r1, r2, r3, r4, r5)
            r12.blob2 = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob1
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.blobs
            com.tenapp.hoopersLite.Food r1 = r12.blob2
            r0.add(r1)
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.upspikeimg
            r2 = 0
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.upspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.downspikeimg
            r2 = 0
            int r3 = r12.height
            android.graphics.Bitmap r4 = r12.downspikeimg
            int r4 = r4.getHeight()
            int r3 = r3 - r4
            float r3 = (float) r3
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.downspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.leftspikeimg
            r2 = 0
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.leftspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.rightspikeimg
            int r2 = r12.width
            android.graphics.Bitmap r3 = r12.rightspikeimg
            int r3 = r3.getWidth()
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 0
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.rightspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.hspikeimg
            r2 = 1123024896(0x42f00000, float:120.0)
            r3 = 1120403456(0x42c80000, float:100.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.hspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.hspikeimg
            r2 = 1123024896(0x42f00000, float:120.0)
            int r3 = r12.height
            r4 = 150(0x96, float:2.1E-43)
            int r3 = r3 - r4
            float r3 = (float) r3
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.vspike = r0
            com.tenapp.hoopersLite.Food r0 = new com.tenapp.hoopersLite.Food
            android.graphics.Bitmap r1 = r12.vspikeimg
            int r2 = r12.width
            r3 = 150(0x96, float:2.1E-43)
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 1132068864(0x437a0000, float:250.0)
            r4 = 0
            r0.<init>(r1, r2, r3, r4)
            r12.extraspike = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.Spikes = r0
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.rightspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.leftspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.upspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.downspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.hspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.vspike
            r0.add(r1)
            java.util.ArrayList<com.tenapp.hoopersLite.Food> r0 = r12.Spikes
            com.tenapp.hoopersLite.Food r1 = r12.extraspike
            r0.add(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.obstacles = r0
            android.content.res.Resources r0 = r12.getResources()
            r1 = 2130837633(0x7f020081, float:1.7280226E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r1)
            r12.which_level = r0
            goto L_0x05cc
        L_0x1b8d:
            r0 = 0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r12.one_or_two_blobs = r0
            goto L_0x05dc
        L_0x1b96:
            r0 = move-exception
            r8 = r0
            java.lang.String r0 = "Error"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "at line 1392 Exception is "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r0, r1)
            goto L_0x05ee
        L_0x1bae:
            r0 = move-exception
            goto L_0x05fc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tenapp.hoopersLite.EatingObjectsActivity.onCreate(android.os.Bundle):void");
    }

    public void onNewIntent(Intent newIntent) {
        Log.d("AFC Lite", "onNewIntent...");
        super.onNewIntent(newIntent);
        setIntent(newIntent);
        StartVersusPanel(getIntent().getExtras());
    }

    public void StartVersusPanel(Bundle levelBundle) {
        this._ad = new AdView(this);
        AdManager.setPublisherId("a14da0768272242");
        this._panelView.setLayoutParams(new WindowManager.LayoutParams(-1, this.height));
        FrameLayout rl = new FrameLayout(this);
        rl.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        rl.addView(this._panelView);
        LinearLayout li = new LinearLayout(this);
        LinearLayout.LayoutParams bottomNavParams = new LinearLayout.LayoutParams(-1, -1);
        li.setOrientation(1);
        li.setGravity(80);
        li.setLayoutParams(bottomNavParams);
        li.addView(this._ad);
        rl.addView(li);
        setContentView(rl);
    }

    public void onReceiveAd(AdView adView) {
    }

    public void onFailedToReceiveAd(AdView adView) {
    }

    public void onReceiveRefreshedAd(AdView adView) {
    }

    public void onFailedToReceiveRefreshedAd(AdView adView) {
    }

    class CustomView extends SurfaceView implements SurfaceHolder.Callback {
        public CustomView(Context context) {
            super(context);
            getHolder().addCallback(this);
        }

        public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        }

        public void surfaceCreated(SurfaceHolder arg0) {
            EatingObjectsActivity.this.runGame = new GameThread(getHolder(), this);
            EatingObjectsActivity.this.runGame.start();
            if (EatingObjectsActivity.this.play_music && EatingObjectsActivity.this.bg_sound != null) {
                EatingObjectsActivity.this.bg_sound.setLooping(true);
                EatingObjectsActivity.this.bg_sound.start();
            }
        }

        public void surfaceDestroyed(SurfaceHolder arg0) {
            boolean retry = true;
            EatingObjectsActivity.this.runGame.setRunning(false);
            if (EatingObjectsActivity.this.bg_sound != null) {
                EatingObjectsActivity.this.bg_sound.setLooping(false);
                EatingObjectsActivity.this.bg_sound.stop();
            }
            while (retry) {
                try {
                    EatingObjectsActivity.this.runGame.join();
                    retry = false;
                } catch (InterruptedException e) {
                    Log.e("Error", "at line 1570 Exception is " + e);
                }
            }
        }

        public void onDraw(Canvas cnv) {
            cnv.drawBitmap(EatingObjectsActivity.this.bg_image, 0.0f, 0.0f, (Paint) null);
            cnv.drawBitmap(EatingObjectsActivity.this.which_level, 10.0f, (float) ((EatingObjectsActivity.this.height - EatingObjectsActivity.this.which_level.getHeight()) - 5), (Paint) null);
            cnv.drawBitmap(EatingObjectsActivity.this.pause_btn, 0.0f, 60.0f, (Paint) null);
            cnv.drawBitmap(EatingObjectsActivity.this.restart_btn, (float) (EatingObjectsActivity.this.width - EatingObjectsActivity.this.restart_btn.getWidth()), 60.0f, (Paint) null);
            if ((EatingObjectsActivity.level == 1 || EatingObjectsActivity.level == 2) && !EatingObjectsActivity.this.game_over) {
                if (EatingObjectsActivity.path_met && !EatingObjectsActivity.this.path_crossing_from_blob) {
                    cnv.drawText("Wrong path. Path must pass from the blob.", 10.0f, (float) (EatingObjectsActivity.this.height - 40), EatingObjectsActivity.this.shashkaPaint3);
                }
                if (!EatingObjectsActivity.path_met && !EatingObjectsActivity.this.if_object_touched && !EatingObjectsActivity.this.game_over) {
                    cnv.drawText("Hoopers will eat when they are connected.", 10.0f, (float) (EatingObjectsActivity.this.height - 40), EatingObjectsActivity.this.shashkaPaint3);
                    cnv.drawText("Drag Me", EatingObjectsActivity.this.yellow.x - ((float) (EatingObjectsActivity.this.yellow.bitmap.getWidth() + 10)), EatingObjectsActivity.this.yellow.y, EatingObjectsActivity.this.shashkaPaint);
                }
                if (EatingObjectsActivity.this.if_object_touched) {
                    if (EatingObjectsActivity.this.counter_to_help < 40) {
                        cnv.drawText("Meet path by dragging any Hooper to eat all objects", 10.0f, (float) (EatingObjectsActivity.this.height - 40), EatingObjectsActivity.this.shashkaPaint3);
                        EatingObjectsActivity.this.counter_to_help++;
                    } else {
                        EatingObjectsActivity.this.counter_to_help = 0;
                    }
                    if (EatingObjectsActivity.this.draw_finger_counter < 10) {
                        if (FoodContains(1)) {
                            cnv.drawBitmap(EatingObjectsActivity.this.finger1, EatingObjectsActivity.this.star1.x - ((float) (EatingObjectsActivity.this.star1.bitmap.getWidth() / 2)), EatingObjectsActivity.this.star1.y + 10.0f, (Paint) null);
                            cnv.drawText("1", (8.0f + EatingObjectsActivity.this.star1.x) - ((float) (EatingObjectsActivity.this.star1.bitmap.getWidth() / 2)), EatingObjectsActivity.this.star1.y + 22.0f, EatingObjectsActivity.this.shashkaPaint2);
                        }
                        if (FoodContains(2)) {
                            cnv.drawBitmap(EatingObjectsActivity.this.finger1, EatingObjectsActivity.this.star2.x - ((float) (EatingObjectsActivity.this.star1.bitmap.getWidth() / 2)), EatingObjectsActivity.this.star2.y + 10.0f, (Paint) null);
                            cnv.drawText("2", (8.0f + EatingObjectsActivity.this.star2.x) - ((float) (EatingObjectsActivity.this.star2.bitmap.getWidth() / 2)), EatingObjectsActivity.this.star2.y + 22.0f, EatingObjectsActivity.this.shashkaPaint2);
                        }
                        if (FoodContains(3)) {
                            cnv.drawBitmap(EatingObjectsActivity.this.finger1, EatingObjectsActivity.this.star3.x - ((float) (EatingObjectsActivity.this.star1.bitmap.getWidth() / 2)), EatingObjectsActivity.this.star3.y + 10.0f, (Paint) null);
                            cnv.drawText("3", (8.0f + EatingObjectsActivity.this.star3.x) - ((float) (EatingObjectsActivity.this.star3.bitmap.getWidth() / 2)), EatingObjectsActivity.this.star3.y + 22.0f, EatingObjectsActivity.this.shashkaPaint2);
                        }
                        cnv.drawBitmap(EatingObjectsActivity.this.finger1, EatingObjectsActivity.this.blob1.x - ((float) EatingObjectsActivity.this.blob1.bitmap.getWidth()), EatingObjectsActivity.this.blob1.y + 10.0f, (Paint) null);
                        cnv.drawText("4", (24.0f + EatingObjectsActivity.this.blob1.x) - ((float) (EatingObjectsActivity.this.blob1.bitmap.getWidth() + 15)), EatingObjectsActivity.this.blob1.y + 22.0f, EatingObjectsActivity.this.shashkaPaint2);
                        cnv.drawBitmap(EatingObjectsActivity.this.finger1, EatingObjectsActivity.this.red.x - ((float) (EatingObjectsActivity.this.red.bitmap.getWidth() + 10)), EatingObjectsActivity.this.red.y - 10.0f, (Paint) null);
                        cnv.drawText("5", (30.0f + EatingObjectsActivity.this.red.x) - ((float) (EatingObjectsActivity.this.red.bitmap.getWidth() + EatingObjectsActivity.this.finger1.getWidth())), EatingObjectsActivity.this.red.y + 3.0f, EatingObjectsActivity.this.shashkaPaint2);
                        EatingObjectsActivity.this.draw_finger_counter++;
                    } else if (EatingObjectsActivity.this.draw_finger_counter < 10 || EatingObjectsActivity.this.draw_finger_counter > 20) {
                        EatingObjectsActivity.this.draw_finger_counter = 0;
                    } else {
                        if (FoodContains(1)) {
                            cnv.drawBitmap(EatingObjectsActivity.this.finger2, EatingObjectsActivity.this.star1.x - ((float) (EatingObjectsActivity.this.star1.bitmap.getWidth() / 2)), EatingObjectsActivity.this.star1.y + 10.0f, (Paint) null);
                            cnv.drawText("1", (8.0f + EatingObjectsActivity.this.star1.x) - ((float) (EatingObjectsActivity.this.star1.bitmap.getWidth() / 2)), EatingObjectsActivity.this.star1.y + 22.0f, EatingObjectsActivity.this.shashkaPaint2);
                        }
                        if (FoodContains(2)) {
                            cnv.drawBitmap(EatingObjectsActivity.this.finger2, EatingObjectsActivity.this.star2.x - ((float) (EatingObjectsActivity.this.star1.bitmap.getWidth() / 2)), EatingObjectsActivity.this.star2.y + 10.0f, (Paint) null);
                            cnv.drawText("2", (8.0f + EatingObjectsActivity.this.star2.x) - ((float) (EatingObjectsActivity.this.star2.bitmap.getWidth() / 2)), EatingObjectsActivity.this.star2.y + 22.0f, EatingObjectsActivity.this.shashkaPaint2);
                        }
                        if (FoodContains(3)) {
                            cnv.drawBitmap(EatingObjectsActivity.this.finger2, EatingObjectsActivity.this.star3.x - ((float) (EatingObjectsActivity.this.star1.bitmap.getWidth() / 2)), EatingObjectsActivity.this.star3.y + 10.0f, (Paint) null);
                            cnv.drawText("3", (8.0f + EatingObjectsActivity.this.star3.x) - ((float) (EatingObjectsActivity.this.star3.bitmap.getWidth() / 2)), EatingObjectsActivity.this.star3.y + 22.0f, EatingObjectsActivity.this.shashkaPaint2);
                        }
                        cnv.drawBitmap(EatingObjectsActivity.this.finger2, EatingObjectsActivity.this.blob1.x - ((float) EatingObjectsActivity.this.blob1.bitmap.getWidth()), EatingObjectsActivity.this.blob1.y + 10.0f, (Paint) null);
                        cnv.drawText("4", (24.0f + EatingObjectsActivity.this.blob1.x) - ((float) (EatingObjectsActivity.this.blob1.bitmap.getWidth() + 15)), EatingObjectsActivity.this.blob1.y + 22.0f, EatingObjectsActivity.this.shashkaPaint2);
                        cnv.drawBitmap(EatingObjectsActivity.this.finger2, EatingObjectsActivity.this.red.x - ((float) (EatingObjectsActivity.this.red.bitmap.getWidth() + 10)), EatingObjectsActivity.this.red.y - 10.0f, (Paint) null);
                        cnv.drawText("5", (30.0f + EatingObjectsActivity.this.red.x) - ((float) (EatingObjectsActivity.this.red.bitmap.getWidth() + EatingObjectsActivity.this.finger1.getWidth())), EatingObjectsActivity.this.red.y + 3.0f, EatingObjectsActivity.this.shashkaPaint2);
                        EatingObjectsActivity.this.draw_finger_counter++;
                        EatingObjectsActivity.this.draw_finger_counter++;
                    }
                }
            }
            if (EatingObjectsActivity.this.start_disappearing_blobs && EatingObjectsActivity.this.show_disappear) {
                if (EatingObjectsActivity.this.draw_shashka_text) {
                    if (EatingObjectsActivity.this.draw_star_text) {
                        EatingObjectsActivity.this.draw_blob_text = false;
                        if (EatingObjectsActivity.this.text_size < 8 || EatingObjectsActivity.this.text_size > 15) {
                            EatingObjectsActivity.this.shashkaPaint.setTextSize(8.0f);
                            EatingObjectsActivity.this.text_size = 8;
                            EatingObjectsActivity.this.draw_shashka_text_x = -10.0f;
                            EatingObjectsActivity.this.draw_shashka_text_y = -10.0f;
                            EatingObjectsActivity.this.draw_star_text = false;
                            EatingObjectsActivity.this.draw_shashka_text = false;
                        } else {
                            cnv.drawText("200+", EatingObjectsActivity.this.draw_shashka_text_x - 5.0f, EatingObjectsActivity.this.draw_shashka_text_y - 5.0f, EatingObjectsActivity.this.shashkaPaint);
                            EatingObjectsActivity.this.draw_shashka_text_y -= 1.0f;
                            EatingObjectsActivity.this.shashkaPaint.setTextSize((float) EatingObjectsActivity.this.text_size);
                            EatingObjectsActivity.this.text_size++;
                        }
                    }
                    if (EatingObjectsActivity.this.draw_blob_text) {
                        EatingObjectsActivity.this.draw_star_text = false;
                        if (EatingObjectsActivity.this.text_size < 8 || EatingObjectsActivity.this.text_size > 15) {
                            EatingObjectsActivity.this.shashkaPaint.setTextSize(8.0f);
                            EatingObjectsActivity.this.draw_shashka_text_x = -10.0f;
                            EatingObjectsActivity.this.draw_shashka_text_y = -10.0f;
                            EatingObjectsActivity.this.draw_shashka_text = false;
                            EatingObjectsActivity.this.draw_blob_text = false;
                            EatingObjectsActivity.this.text_size = 8;
                        } else {
                            cnv.drawText("250+", EatingObjectsActivity.this.draw_shashka_text_x - 5.0f, EatingObjectsActivity.this.draw_shashka_text_y - 5.0f, EatingObjectsActivity.this.shashkaPaint);
                            EatingObjectsActivity.this.draw_shashka_text_y -= 1.0f;
                            EatingObjectsActivity.this.shashkaPaint.setTextSize((float) EatingObjectsActivity.this.text_size);
                            EatingObjectsActivity.this.text_size++;
                        }
                    }
                }
                if (EatingObjectsActivity.this.disappear_blog_number == 1) {
                    if (EatingObjectsActivity.this.counter_to_disappear <= 15) {
                        cnv.drawBitmap(EatingObjectsActivity.this.red1, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 15 && EatingObjectsActivity.this.counter_to_disappear <= 30) {
                        cnv.drawBitmap(EatingObjectsActivity.this.red2, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 30 && EatingObjectsActivity.this.counter_to_disappear <= 45) {
                        cnv.drawBitmap(EatingObjectsActivity.this.red3, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 45 && EatingObjectsActivity.this.counter_to_disappear <= 60) {
                        cnv.drawBitmap(EatingObjectsActivity.this.red4, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 60 && EatingObjectsActivity.this.counter_to_disappear <= 75) {
                        cnv.drawBitmap(EatingObjectsActivity.this.red5, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 75 && EatingObjectsActivity.this.counter_to_disappear <= 90) {
                        cnv.drawBitmap(EatingObjectsActivity.this.red6, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 90 && EatingObjectsActivity.this.counter_to_disappear <= 100) {
                        cnv.drawBitmap(EatingObjectsActivity.this.red7, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 100 && EatingObjectsActivity.this.counter_to_disappear <= 110) {
                        cnv.drawBitmap(EatingObjectsActivity.this.red8, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    }
                } else if (EatingObjectsActivity.this.disappear_blog_number == 2) {
                    if (EatingObjectsActivity.this.counter_to_disappear <= 15) {
                        cnv.drawBitmap(EatingObjectsActivity.this.blu1, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 15 && EatingObjectsActivity.this.counter_to_disappear <= 30) {
                        cnv.drawBitmap(EatingObjectsActivity.this.blu2, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 30 && EatingObjectsActivity.this.counter_to_disappear <= 45) {
                        cnv.drawBitmap(EatingObjectsActivity.this.blu3, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 45 && EatingObjectsActivity.this.counter_to_disappear <= 60) {
                        cnv.drawBitmap(EatingObjectsActivity.this.blu4, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 60 && EatingObjectsActivity.this.counter_to_disappear <= 75) {
                        cnv.drawBitmap(EatingObjectsActivity.this.blu5, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 75 && EatingObjectsActivity.this.counter_to_disappear <= 90) {
                        cnv.drawBitmap(EatingObjectsActivity.this.blu6, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 90 && EatingObjectsActivity.this.counter_to_disappear <= 100) {
                        cnv.drawBitmap(EatingObjectsActivity.this.blu7, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 100 && EatingObjectsActivity.this.counter_to_disappear <= 110) {
                        cnv.drawBitmap(EatingObjectsActivity.this.blu8, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    }
                } else if (EatingObjectsActivity.this.disappear_blog_number == 3) {
                    if (EatingObjectsActivity.this.counter_to_disappear <= 15) {
                        cnv.drawBitmap(EatingObjectsActivity.this.org1, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 15 && EatingObjectsActivity.this.counter_to_disappear <= 30) {
                        cnv.drawBitmap(EatingObjectsActivity.this.org2, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 30 && EatingObjectsActivity.this.counter_to_disappear <= 45) {
                        cnv.drawBitmap(EatingObjectsActivity.this.org3, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 45 && EatingObjectsActivity.this.counter_to_disappear <= 60) {
                        cnv.drawBitmap(EatingObjectsActivity.this.org4, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 60 && EatingObjectsActivity.this.counter_to_disappear <= 75) {
                        cnv.drawBitmap(EatingObjectsActivity.this.org5, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 75 && EatingObjectsActivity.this.counter_to_disappear <= 90) {
                        cnv.drawBitmap(EatingObjectsActivity.this.org6, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 90 && EatingObjectsActivity.this.counter_to_disappear <= 100) {
                        cnv.drawBitmap(EatingObjectsActivity.this.org7, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    } else if (EatingObjectsActivity.this.counter_to_disappear >= 100 && EatingObjectsActivity.this.counter_to_disappear <= 110) {
                        cnv.drawBitmap(EatingObjectsActivity.this.org8, EatingObjectsActivity.this.disappearing_x, EatingObjectsActivity.this.disappearing_y, (Paint) null);
                        EatingObjectsActivity.this.counter_to_disappear++;
                    }
                }
                if (EatingObjectsActivity.this.blobs.size() <= 0 && EatingObjectsActivity.this.counter_to_disappear >= 110) {
                    EatingObjectsActivity.this.next_level = true;
                    EatingObjectsActivity.this.disappearing_x = 0.0f;
                    EatingObjectsActivity.this.disappearing_y = 0.0f;
                    EatingObjectsActivity.this.counter_to_disappear = 0;
                    EatingObjectsActivity.this.show_disappear = false;
                }
            }
            if ((EatingObjectsActivity.level >= 16 && EatingObjectsActivity.level <= 20) || EatingObjectsActivity.level == 22 || EatingObjectsActivity.level == 23 || EatingObjectsActivity.level == 24 || EatingObjectsActivity.level == 27) {
                EatingObjectsActivity.this.third.draw(cnv);
            }
            if (EatingObjectsActivity.this.obstacles.size() >= 1) {
                for (int i = 0; i < EatingObjectsActivity.this.obstacles.size(); i++) {
                    EatingObjectsActivity.this.obstacles.get(i).draw(cnv);
                }
            }
            cnv.drawText(new StringBuilder().append(EatingObjectsActivity.score).toString(), 55.0f, 35.0f, EatingObjectsActivity.this.blackP);
            if (EatingObjectsActivity.this.show_fps) {
                cnv.drawText("FPS: 25", (float) (EatingObjectsActivity.this.width - 30), (float) (EatingObjectsActivity.this.height - 15), EatingObjectsActivity.this.blackP);
            }
            cnv.drawText(EatingObjectsActivity.this.minutes_to_display + " : " + EatingObjectsActivity.this.seconds_to_display, 160.0f, 35.0f, EatingObjectsActivity.this.blackP);
            cnv.drawText(new StringBuilder().append(EatingObjectsActivity.this.high_score).toString(), 260.0f, 35.0f, EatingObjectsActivity.this.blackP);
            if (EatingObjectsActivity.this.food.size() >= 1) {
                for (int i2 = 0; i2 < EatingObjectsActivity.this.food.size(); i2++) {
                    EatingObjectsActivity.this.food.get(i2).draw(cnv);
                }
            }
            if (EatingObjectsActivity.this.Hooppers.size() >= 1) {
                for (int i3 = 0; i3 < EatingObjectsActivity.this.Hooppers.size(); i3++) {
                    EatingObjectsActivity.this.Hooppers.get(i3).draw(cnv);
                }
            }
            if (EatingObjectsActivity.this.blobs.size() >= 1 && EatingObjectsActivity.this.blobs != null) {
                for (int i4 = 0; i4 < EatingObjectsActivity.this.blobs.size(); i4++) {
                    EatingObjectsActivity.this.blobs.get(i4).draw(cnv);
                }
            }
            if (EatingObjectsActivity.this.Spikes.size() >= 1 && EatingObjectsActivity.this.Spikes != null) {
                for (int i5 = 0; i5 < EatingObjectsActivity.this.Spikes.size(); i5++) {
                    EatingObjectsActivity.this.Spikes.get(i5).draw(cnv);
                }
            }
            if (EatingObjectsActivity.flg_ach1 || EatingObjectsActivity.flg_ach2 || EatingObjectsActivity.flg_ach3 || EatingObjectsActivity.flg_ach4) {
                EatingObjectsActivity.this.flg_pause = true;
                if (EatingObjectsActivity.this.c >= 100 || !(EatingObjectsActivity.level == 2 || EatingObjectsActivity.level == 10 || EatingObjectsActivity.level == 21 || EatingObjectsActivity.level == 30)) {
                    EatingObjectsActivity.this.flg_pause = false;
                    EatingObjectsActivity.flg_ach1 = false;
                    EatingObjectsActivity.flg_ach2 = false;
                    EatingObjectsActivity.flg_ach3 = false;
                    EatingObjectsActivity.flg_ach4 = false;
                    EatingObjectsActivity.this.c = 0;
                    return;
                }
                cnv.drawBitmap(EatingObjectsActivity.this.ranked, 40.0f, 200.0f, (Paint) null);
                if (EatingObjectsActivity.flg_ach1) {
                    cnv.drawText("You rank is now Little Baby", 50.0f, (float) ((EatingObjectsActivity.this.ranked.getHeight() + 200) - 15), EatingObjectsActivity.this.bigPaint);
                } else if (EatingObjectsActivity.flg_ach2) {
                    cnv.drawText("You rank is now Toddler", 50.0f, (float) ((EatingObjectsActivity.this.ranked.getHeight() + 200) - 15), EatingObjectsActivity.this.bigPaint);
                } else if (EatingObjectsActivity.flg_ach3) {
                    cnv.drawText("You rank is now Adolescent", 50.0f, (float) ((EatingObjectsActivity.this.ranked.getHeight() + 200) - 15), EatingObjectsActivity.this.bigPaint);
                } else if (EatingObjectsActivity.flg_ach4) {
                    cnv.drawText("You rank is now Grown Up Punk", 50.0f, (float) ((EatingObjectsActivity.this.ranked.getHeight() + 200) - 15), EatingObjectsActivity.this.bigPaint);
                }
                EatingObjectsActivity.this.c++;
            }
        }

        private boolean FoodContains(int i) {
            for (int u = 0; u < EatingObjectsActivity.this.food.size(); u++) {
                if (EatingObjectsActivity.this.food.get(u).type == i) {
                    return true;
                }
            }
            return false;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:303:0x09d6, code lost:
            if (r14.this$0.yellow.y > (((float) r14.this$0.food.get(r0).bitmap.getHeight()) + r14.this$0.food.get(r0).y)) goto L_0x09d8;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void UpdatePhysics() {
            /*
                r14 = this;
                r13 = 20
                r12 = 1103626240(0x41c80000, float:25.0)
                r11 = 1097859072(0x41700000, float:15.0)
                r10 = 0
                r9 = 1
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.flg_pause
                if (r2 != 0) goto L_0x046b
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                long r3 = java.lang.System.currentTimeMillis()
                com.tenapp.hoopersLite.EatingObjectsActivity r5 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                long r5 = r5.starts
                com.tenapp.hoopersLite.EatingObjectsActivity r7 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                long r7 = r7.pause_time
                long r5 = r5 + r7
                long r3 = r3 - r5
                r2.miliseconds = r3
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                long r3 = r3.miliseconds
                float r3 = (float) r3
                r4 = 1148846080(0x447a0000, float:1000.0)
                float r3 = r3 / r4
                int r3 = (int) r3
                r2.seconds = r3
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r3 = r3.seconds
                int r3 = r3 / 60
                r2.minutes = r3
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r3 = r2.seconds
                int r3 = r3 % 60
                r2.seconds = r3
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r2 = r2.minutes
                r3 = 10
                if (r2 >= r3) goto L_0x046c
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                java.lang.String r4 = "0"
                r3.<init>(r4)
                com.tenapp.hoopersLite.EatingObjectsActivity r4 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r4 = r4.minutes
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r3 = r3.toString()
                r2.minutes_to_display = r3
            L_0x005e:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r2 = r2.seconds
                r3 = 10
                if (r2 >= r3) goto L_0x0485
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                java.lang.String r4 = "0"
                r3.<init>(r4)
                com.tenapp.hoopersLite.EatingObjectsActivity r4 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r4 = r4.seconds
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r3 = r3.toString()
                r2.seconds_to_display = r3
            L_0x007d:
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                r3 = 16
                if (r2 < r3) goto L_0x0087
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                if (r2 <= r13) goto L_0x009f
            L_0x0087:
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                r3 = 22
                if (r2 == r3) goto L_0x009f
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                r3 = 23
                if (r2 == r3) goto L_0x009f
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                r3 = 24
                if (r2 == r3) goto L_0x009f
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                r3 = 27
                if (r2 != r3) goto L_0x00a6
            L_0x009f:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.third
                r2.update_physics_default()
            L_0x00a6:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                int r2 = r2.size()
                if (r2 > 0) goto L_0x00ca
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.next_level
                if (r2 == 0) goto L_0x00ca
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.os.Handler r2 = r2.m_Handler
                r2.sendEmptyMessage(r9)
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.flg_pause = r9
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.EatingObjectsActivity$GameThread r2 = r2.runGame
                r2.setRunning(r10)
            L_0x00ca:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r3 = r3.red
                float r3 = r3.x
                float r3 = r3 - r12
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x0143
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r3 = r3.red
                float r3 = r3.x
                float r3 = r3 + r12
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 >= 0) goto L_0x0143
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r3 = r3.red
                float r3 = r3.y
                float r3 = r3 - r12
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x0143
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r3 = r3.red
                float r3 = r3.y
                float r3 = r3 + r12
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 >= 0) goto L_0x0143
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.game_over
                if (r2 != 0) goto L_0x0143
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.play_sound
                if (r2 == 0) goto L_0x0127
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.collision
                if (r2 == 0) goto L_0x0127
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.collision
                r2.start()
            L_0x0127:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.game_over = r9
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r10)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                r2.clear()
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                r2.remove(r10)
                com.tenapp.hoopersLite.EatingObjectsActivity.path_met = r10
            L_0x0143:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.Spikes
                int r2 = r2.size()
                if (r2 < r9) goto L_0x0158
                r0 = 0
            L_0x014e:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.Spikes
                int r2 = r2.size()
                if (r0 < r2) goto L_0x049c
            L_0x0158:
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                r3 = 23
                if (r2 == r3) goto L_0x0164
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                r3 = 26
                if (r2 != r3) goto L_0x01ba
            L_0x0164:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r2 = r2.x
                r3 = 1101004800(0x41a00000, float:20.0)
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 < 0) goto L_0x0186
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r2 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r3 = r3.width
                int r3 = r3 - r13
                float r3 = (float) r3
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x018f
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.game_over
                if (r2 != 0) goto L_0x018f
            L_0x0186:
                java.lang.String r2 = "red"
                r14.remove(r2)
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.game_over = r9
            L_0x018f:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.x
                r3 = 1101004800(0x41a00000, float:20.0)
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 < 0) goto L_0x01b1
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r3 = r3.width
                int r3 = r3 - r13
                float r3 = (float) r3
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x01ba
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.game_over
                if (r2 != 0) goto L_0x01ba
            L_0x01b1:
                java.lang.String r2 = "yellow"
                r14.remove(r2)
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.game_over = r9
            L_0x01ba:
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                r3 = 24
                if (r2 != r3) goto L_0x0216
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r2 = r2.y
                r3 = 1101004800(0x41a00000, float:20.0)
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 < 0) goto L_0x01e2
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r2 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r3 = r3.height
                int r3 = r3 - r13
                float r3 = (float) r3
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x01eb
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.game_over
                if (r2 != 0) goto L_0x01eb
            L_0x01e2:
                java.lang.String r2 = "red"
                r14.remove(r2)
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.game_over = r9
            L_0x01eb:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.y
                r3 = 1101004800(0x41a00000, float:20.0)
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 < 0) goto L_0x020d
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r3 = r3.height
                int r3 = r3 - r13
                float r3 = (float) r3
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x0216
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.game_over
                if (r2 != 0) goto L_0x0216
            L_0x020d:
                java.lang.String r2 = "yellow"
                r14.remove(r2)
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.game_over = r9
            L_0x0216:
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                r3 = 25
                if (r2 == r3) goto L_0x0222
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                r3 = 27
                if (r2 != r3) goto L_0x02ce
            L_0x0222:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r2 = r2.x
                r3 = 1101004800(0x41a00000, float:20.0)
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 < 0) goto L_0x0244
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r2 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r3 = r3.width
                int r3 = r3 - r13
                float r3 = (float) r3
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x024d
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.game_over
                if (r2 != 0) goto L_0x024d
            L_0x0244:
                java.lang.String r2 = "red"
                r14.remove(r2)
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.game_over = r9
            L_0x024d:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.x
                r3 = 1101004800(0x41a00000, float:20.0)
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 < 0) goto L_0x026f
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r3 = r3.width
                int r3 = r3 - r13
                float r3 = (float) r3
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x0278
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.game_over
                if (r2 != 0) goto L_0x0278
            L_0x026f:
                java.lang.String r2 = "yellow"
                r14.remove(r2)
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.game_over = r9
            L_0x0278:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r2 = r2.y
                r3 = 1101004800(0x41a00000, float:20.0)
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 < 0) goto L_0x029a
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r2 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r3 = r3.height
                int r3 = r3 - r13
                float r3 = (float) r3
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x02a3
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.game_over
                if (r2 != 0) goto L_0x02a3
            L_0x029a:
                java.lang.String r2 = "red"
                r14.remove(r2)
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.game_over = r9
            L_0x02a3:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.y
                r3 = 1101004800(0x41a00000, float:20.0)
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 < 0) goto L_0x02c5
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r3 = r3.height
                int r3 = r3 - r13
                float r3 = (float) r3
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x02ce
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.game_over
                if (r2 != 0) goto L_0x02ce
            L_0x02c5:
                java.lang.String r2 = "yellow"
                r14.remove(r2)
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.game_over = r9
            L_0x02ce:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.game_over
                if (r2 == 0) goto L_0x02e3
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r2 = r2.counter_to_game_over
                r3 = 50
                if (r2 < r3) goto L_0x0569
                r14.game_over()
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.counter_to_game_over = r10
            L_0x02e3:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                int r2 = r2.size()
                if (r2 < r9) goto L_0x02f8
                r0 = 0
            L_0x02ee:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                int r2 = r2.size()
                if (r0 < r2) goto L_0x0573
            L_0x02f8:
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                r3 = 16
                if (r2 < r3) goto L_0x0302
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                if (r2 <= r13) goto L_0x031a
            L_0x0302:
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                r3 = 22
                if (r2 == r3) goto L_0x031a
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                r3 = 23
                if (r2 == r3) goto L_0x031a
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                r3 = 24
                if (r2 == r3) goto L_0x031a
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.level
                r3 = 27
                if (r2 != r3) goto L_0x03ff
            L_0x031a:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                int r2 = r2.size()
                if (r2 < r9) goto L_0x032f
                r0 = 0
            L_0x0325:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                int r2 = r2.size()
                if (r0 < r2) goto L_0x072e
            L_0x032f:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r3 = r3.third
                float r3 = r3.x
                float r3 = r3 - r12
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x0397
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r3 = r3.third
                float r3 = r3.x
                float r3 = r3 + r12
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 >= 0) goto L_0x0397
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r3 = r3.third
                float r3 = r3.y
                float r3 = r3 - r12
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x0397
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r2 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r3 = r3.third
                float r3 = r3.y
                float r3 = r3 + r12
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 >= 0) goto L_0x0397
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.game_over
                if (r2 != 0) goto L_0x0397
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.play_sound
                if (r2 == 0) goto L_0x038c
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.collision
                if (r2 == 0) goto L_0x038c
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.collision
                r2.start()
            L_0x038c:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.game_over = r9
                java.lang.String r2 = "Yellow"
                r14.remove(r2)
                com.tenapp.hoopersLite.EatingObjectsActivity.path_met = r10
            L_0x0397:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.third
                float r2 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r3 = r3.red
                float r3 = r3.x
                float r3 = r3 - r12
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x03ff
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.third
                float r2 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r3 = r3.red
                float r3 = r3.x
                float r3 = r3 + r12
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 >= 0) goto L_0x03ff
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.third
                float r2 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r3 = r3.red
                float r3 = r3.y
                float r3 = r3 - r12
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x03ff
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.third
                float r2 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r3 = r3.red
                float r3 = r3.y
                float r3 = r3 + r12
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 >= 0) goto L_0x03ff
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.game_over
                if (r2 != 0) goto L_0x03ff
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.play_sound
                if (r2 == 0) goto L_0x03f4
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.collision
                if (r2 == 0) goto L_0x03f4
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.collision
                r2.start()
            L_0x03f4:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.game_over = r9
                java.lang.String r2 = "red"
                r14.remove(r2)
                com.tenapp.hoopersLite.EatingObjectsActivity.path_met = r10
            L_0x03ff:
                r0 = 0
            L_0x0400:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                int r2 = r2.size()
                if (r0 < r2) goto L_0x07e5
                boolean r2 = com.tenapp.hoopersLite.EatingObjectsActivity.path_met
                if (r2 == 0) goto L_0x0431
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.path_crossing_from_blob
                if (r2 != 0) goto L_0x0431
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.check_path_crossing_from_blobs
                if (r2 == 0) goto L_0x0431
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                boolean r2 = r2.type
                if (r2 == 0) goto L_0x08a1
                r1 = 0
            L_0x0423:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                int r2 = r2.size()
                if (r1 < r2) goto L_0x07f6
            L_0x042d:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.check_path_crossing_from_blobs = r10
            L_0x0431:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r2 = r2.counter_to_check_path_crosssing_from_all_blobs
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r3 = r3.blobs
                int r3 = r3.size()
                if (r2 != r3) goto L_0x0956
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.path_crossing_from_blob = r9
            L_0x0443:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.path_crossing_from_blob
                if (r2 == 0) goto L_0x0454
                r0 = 0
            L_0x044a:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                int r2 = r2.size()
                if (r0 < r2) goto L_0x095c
            L_0x0454:
                r0 = 0
            L_0x0455:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                int r2 = r2.size()
                if (r0 < r2) goto L_0x0aaa
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                int r2 = r2.size()
                if (r2 > 0) goto L_0x046b
                com.tenapp.hoopersLite.Eaters2.next_level_ok_stop_moving = r9
            L_0x046b:
                return
            L_0x046c:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                java.lang.String r4 = "0"
                r3.<init>(r4)
                com.tenapp.hoopersLite.EatingObjectsActivity r4 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r4 = r4.minutes
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r3 = r3.toString()
                r2.minutes_to_display = r3
                goto L_0x005e
            L_0x0485:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                com.tenapp.hoopersLite.EatingObjectsActivity r4 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r4 = r4.seconds
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r3 = r3.toString()
                r2.seconds_to_display = r3
                goto L_0x007d
            L_0x049c:
                r1 = 0
            L_0x049d:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                int r2 = r2.size()
                if (r1 < r2) goto L_0x04ab
            L_0x04a7:
                int r0 = r0 + 1
                goto L_0x014e
            L_0x04ab:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                float r3 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.Spikes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                float r2 = r2 - r11
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0565
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                float r3 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.Spikes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.Spikes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getWidth()
                float r2 = (float) r2
                float r2 = r2 + r4
                float r2 = r2 + r11
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0565
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                float r3 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.Spikes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                float r2 = r2 - r11
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0565
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                float r3 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.Spikes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.Spikes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getHeight()
                float r2 = (float) r2
                float r2 = r2 + r4
                float r2 = r2 + r11
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0565
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.play_sound
                if (r2 == 0) goto L_0x0556
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.wall_hit
                if (r2 == 0) goto L_0x0556
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.wall_hit
                r2.start()
            L_0x0556:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                r2.remove(r1)
                com.tenapp.hoopersLite.EatingObjectsActivity.path_met = r10
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.game_over = r9
                goto L_0x04a7
            L_0x0565:
                int r1 = r1 + 1
                goto L_0x049d
            L_0x0569:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r3 = r2.counter_to_game_over
                int r3 = r3 + 1
                r2.counter_to_game_over = r3
                goto L_0x02e3
            L_0x0573:
                r1 = 0
            L_0x0574:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                int r2 = r2.size()
                if (r1 < r2) goto L_0x0582
            L_0x057e:
                int r0 = r0 + 1
                goto L_0x02ee
            L_0x0582:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                float r3 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                float r2 = r2 - r11
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x072a
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                float r3 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getWidth()
                float r2 = (float) r2
                float r2 = r2 + r4
                float r2 = r2 + r11
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x072a
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                float r3 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                float r2 = r2 - r11
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x072a
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                float r3 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getHeight()
                float r2 = (float) r2
                float r2 = r2 + r4
                float r2 = r2 + r11
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x072a
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.play_sound
                if (r2 == 0) goto L_0x062d
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.wall_hit
                if (r2 == 0) goto L_0x062d
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.wall_hit
                r2.start()
            L_0x062d:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                r2.stopped_by_obstacle = r9
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                r2.stopped_by_obstacle = r9
                java.lang.String r3 = "Test"
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                java.lang.String r2 = " "
                r4.<init>(r2)
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                int r2 = r2.type
                java.lang.StringBuilder r2 = r4.append(r2)
                java.lang.String r4 = " true "
                java.lang.StringBuilder r2 = r2.append(r4)
                java.lang.String r2 = r2.toString()
                android.util.Log.d(r3, r2)
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                int r2 = r2.type
                if (r2 != r9) goto L_0x06eb
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                r2.vertical_obs = r9
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                r2.horizontal_obs = r10
                java.lang.String r3 = "Test"
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                java.lang.String r2 = " "
                r4.<init>(r2)
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                int r2 = r2.type
                java.lang.StringBuilder r2 = r4.append(r2)
                java.lang.String r4 = " vertocal "
                java.lang.StringBuilder r2 = r2.append(r4)
                java.lang.String r2 = r2.toString()
                android.util.Log.d(r3, r2)
            L_0x06b7:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                r2.increase_x = r10
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                r2.increase_y = r10
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                r2.decrease_x = r10
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                r2.decrease_y = r10
                com.tenapp.hoopersLite.EatingObjectsActivity.path_met = r10
                goto L_0x057e
            L_0x06eb:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                r2.horizontal_obs = r9
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                r2.vertical_obs = r10
                java.lang.String r3 = "Test"
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                java.lang.String r2 = " "
                r4.<init>(r2)
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                int r2 = r2.type
                java.lang.StringBuilder r2 = r4.append(r2)
                java.lang.String r4 = " Horizontal "
                java.lang.StringBuilder r2 = r2.append(r4)
                java.lang.String r2 = r2.toString()
                android.util.Log.d(r3, r2)
                goto L_0x06b7
            L_0x072a:
                int r1 = r1 + 1
                goto L_0x0574
            L_0x072e:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.third
                float r3 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                float r2 = r2 - r11
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x07e1
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.third
                float r3 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getWidth()
                float r2 = (float) r2
                float r2 = r2 + r4
                float r2 = r2 + r11
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x07e1
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.third
                float r3 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                float r2 = r2 - r11
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x07e1
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.third
                float r3 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.obstacles
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getHeight()
                float r2 = (float) r2
                float r2 = r2 + r4
                float r2 = r2 + r11
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x07e1
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.play_sound
                if (r2 == 0) goto L_0x07c1
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.wall_hit
                if (r2 == 0) goto L_0x07c1
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.wall_hit
                r2.start()
            L_0x07c1:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.third
                r2.stopped_by_obstacle = r9
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.third
                r2.increase_x = r10
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.third
                r2.increase_y = r10
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.third
                r2.decrease_x = r10
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.third
                r2.decrease_y = r10
                goto L_0x032f
            L_0x07e1:
                int r0 = r0 + 1
                goto L_0x0325
            L_0x07e5:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Eaters2> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Eaters2 r2 = (com.tenapp.hoopersLite.Eaters2) r2
                r2.update_physics()
                int r0 = r0 + 1
                goto L_0x0400
            L_0x07f6:
                r0 = 0
            L_0x07f7:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                int r2 = r2.size()
                if (r0 < r2) goto L_0x0807
                int r1 = r1 + 1
                goto L_0x0423
            L_0x0807:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posX
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r4 = 1109393408(0x42200000, float:40.0)
                float r2 = r2 - r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x089d
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posX
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r4 = 1109393408(0x42200000, float:40.0)
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x089d
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posY
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r4 = 1109393408(0x42200000, float:40.0)
                float r2 = r2 - r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x089d
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posY
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r4 = 1109393408(0x42200000, float:40.0)
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x089d
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r3 = r2.counter_to_check_path_crosssing_from_all_blobs
                int r3 = r3 + 1
                r2.counter_to_check_path_crosssing_from_all_blobs = r3
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                int r0 = r2.size()
            L_0x089d:
                int r0 = r0 + 1
                goto L_0x07f7
            L_0x08a1:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                boolean r2 = r2.type
                if (r2 == 0) goto L_0x042d
                r1 = 0
            L_0x08aa:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                int r2 = r2.size()
                if (r1 >= r2) goto L_0x042d
                r0 = 0
            L_0x08b5:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                int r2 = r2.size()
                if (r0 < r2) goto L_0x08c4
            L_0x08c1:
                int r1 = r1 + 1
                goto L_0x08aa
            L_0x08c4:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posX
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r4 = 1109393408(0x42200000, float:40.0)
                float r2 = r2 - r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0952
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posX
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r4 = 1109393408(0x42200000, float:40.0)
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0952
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posY
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r4 = 1109393408(0x42200000, float:40.0)
                float r2 = r2 - r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0952
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posY
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r4 = 1109393408(0x42200000, float:40.0)
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0952
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                int r3 = r2.counter_to_check_path_crosssing_from_all_blobs
                int r3 = r3 + 1
                r2.counter_to_check_path_crosssing_from_all_blobs = r3
                goto L_0x08c1
            L_0x0952:
                int r0 = r0 + 1
                goto L_0x08b5
            L_0x0956:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.path_crossing_from_blob = r10
                goto L_0x0443
            L_0x095c:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r3 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x09d8
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r3 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getWidth()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x09d8
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r3 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x09d8
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r3 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getHeight()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 <= 0) goto L_0x0a54
            L_0x09d8:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r3 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0aa6
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r3 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getWidth()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0aa6
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r3 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0aa6
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r3 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getHeight()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0aa6
            L_0x0a54:
                boolean r2 = com.tenapp.hoopersLite.EatingObjectsActivity.path_met
                if (r2 == 0) goto L_0x0aa6
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.play_sound
                if (r2 == 0) goto L_0x0a6b
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.eat_sound
                if (r2 == 0) goto L_0x0a6b
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.eat_sound
                r2.start()
            L_0x0a6b:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.draw_shashka_text = r9
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.draw_star_text = r9
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r3.draw_shashka_text_x = r2
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r3.draw_shashka_text_y = r2
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r3 = 8
                r2.text_size = r3
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                r2.remove(r0)
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.score
                int r2 = r2 + 200
                com.tenapp.hoopersLite.EatingObjectsActivity.score = r2
            L_0x0aa6:
                int r0 = r0 + 1
                goto L_0x044a
            L_0x0aaa:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r3 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r4 = 1092616192(0x41200000, float:10.0)
                float r2 = r2 - r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0bd3
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r3 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getWidth()
                float r2 = (float) r2
                float r2 = r2 + r4
                r4 = 1092616192(0x41200000, float:10.0)
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0bd3
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r3 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r4 = 1092616192(0x41200000, float:10.0)
                float r2 = r2 - r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0bd3
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.yellow
                float r3 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getHeight()
                float r2 = (float) r2
                float r2 = r2 + r4
                r4 = 1092616192(0x41200000, float:10.0)
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0bd3
                boolean r2 = com.tenapp.hoopersLite.EatingObjectsActivity.path_met
                if (r2 == 0) goto L_0x0bd3
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                int r2 = r2.size()
                r3 = 2
                if (r2 > r3) goto L_0x0d00
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.eat_sound
                if (r2 == 0) goto L_0x0b54
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.play_sound
                if (r2 == 0) goto L_0x0b54
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.eat_sound
                r2.start()
            L_0x0b54:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.start_disappearing_blobs = r9
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                int r2 = r2.type
                r3.disappear_blog_number = r2
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r3.disappearing_x = r2
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r3.disappearing_y = r2
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.counter_to_disappear = r10
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r3 = 8
                r2.text_size = r3
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.draw_shashka_text = r9
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.draw_blob_text = r9
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r3.draw_shashka_text_x = r2
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r3.draw_shashka_text_y = r2
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                r2.remove(r0)
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.path_crossing_from_blob = r10
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.counter_to_check_path_crosssing_from_all_blobs = r10
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.check_path_crossing_from_blobs = r9
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.score
                int r2 = r2 + 250
                com.tenapp.hoopersLite.EatingObjectsActivity.score = r2
            L_0x0bd3:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r3 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r4 = 1092616192(0x41200000, float:10.0)
                float r2 = r2 - r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0cfc
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r3 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.x
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getWidth()
                float r2 = (float) r2
                float r2 = r2 + r4
                r4 = 1092616192(0x41200000, float:10.0)
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0cfc
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r3 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r4 = 1092616192(0x41200000, float:10.0)
                float r2 = r2 - r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0cfc
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.Eaters2 r2 = r2.red
                float r3 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.y
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getHeight()
                float r2 = (float) r2
                float r2 = r2 + r4
                r4 = 1092616192(0x41200000, float:10.0)
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0cfc
                boolean r2 = com.tenapp.hoopersLite.EatingObjectsActivity.path_met
                if (r2 == 0) goto L_0x0cfc
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                int r2 = r2.size()
                r3 = 2
                if (r2 > r3) goto L_0x0d06
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.eat_sound
                if (r2 == 0) goto L_0x0c7d
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                boolean r2 = r2.play_sound
                if (r2 == 0) goto L_0x0c7d
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                android.media.MediaPlayer r2 = r2.eat_sound
                r2.start()
            L_0x0c7d:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.start_disappearing_blobs = r9
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                int r2 = r2.type
                r3.disappear_blog_number = r2
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r3.disappearing_x = r2
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r3.disappearing_y = r2
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.counter_to_disappear = r10
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r3 = 8
                r2.text_size = r3
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.draw_shashka_text = r9
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.draw_blob_text = r9
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r3.draw_shashka_text_x = r2
                com.tenapp.hoopersLite.EatingObjectsActivity r3 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r3.draw_shashka_text_y = r2
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                r2.remove(r0)
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.path_crossing_from_blob = r10
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.counter_to_check_path_crosssing_from_all_blobs = r10
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.check_path_crossing_from_blobs = r9
                int r2 = com.tenapp.hoopersLite.EatingObjectsActivity.score
                int r2 = r2 + 250
                com.tenapp.hoopersLite.EatingObjectsActivity.score = r2
            L_0x0cfc:
                int r0 = r0 + 1
                goto L_0x0455
            L_0x0d00:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.path_crossing_from_blob = r10
                goto L_0x0bd3
            L_0x0d06:
                com.tenapp.hoopersLite.EatingObjectsActivity r2 = com.tenapp.hoopersLite.EatingObjectsActivity.this
                r2.path_crossing_from_blob = r10
                goto L_0x0cfc
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tenapp.hoopersLite.EatingObjectsActivity.CustomView.UpdatePhysics():void");
        }

        private void remove(String string) {
            for (int i = 0; i < EatingObjectsActivity.this.Hooppers.size(); i++) {
                if (EatingObjectsActivity.this.Hooppers.get(i).name.equalsIgnoreCase(string)) {
                    EatingObjectsActivity.this.Hooppers.get(i).routes.clear();
                    EatingObjectsActivity.this.Hooppers.remove(i);
                }
            }
        }

        private void game_over() {
            EatingObjectsActivity.this.game_over = true;
            EatingObjectsActivity.this.game_over_alert.scores.setText("GAME OVER\nScore: " + EatingObjectsActivity.score + "\nTime: " + EatingObjectsActivity.this.minutes_to_display + " : " + EatingObjectsActivity.this.seconds_to_display);
            EatingObjectsActivity.this.m_Handler.sendEmptyMessage(0);
            EatingObjectsActivity.this.flg_pause = true;
            EatingObjectsActivity.this.runGame.setRunning(false);
        }

        private Eaters2 GetTouchedObject(float eventX, float eventY) {
            Eaters2 objTouched = null;
            int i = 0;
            while (true) {
                if (i >= EatingObjectsActivity.this.Hooppers.size()) {
                    break;
                }
                float x = EatingObjectsActivity.this.Hooppers.get(i).x;
                float y = EatingObjectsActivity.this.Hooppers.get(i).y;
                Bitmap bitmap = EatingObjectsActivity.this.Hooppers.get(i).bitmap;
                if (eventX >= (x - ((float) (bitmap.getWidth() / 2))) - 30.0f && eventX <= ((float) (bitmap.getWidth() / 2)) + x + 30.0f) {
                    if (eventY >= (y - ((float) (bitmap.getHeight() / 2))) - 30.0f && eventY <= ((float) (bitmap.getHeight() / 2)) + y + 30.0f) {
                        objTouched = EatingObjectsActivity.this.Hooppers.get(i);
                        EatingObjectsActivity.this.Hooppers.get(i).i_m_touched = true;
                        break;
                    }
                    objTouched = null;
                }
                i++;
            }
            if (((EatingObjectsActivity.level >= 16 && EatingObjectsActivity.level <= 20) || EatingObjectsActivity.level == 22 || EatingObjectsActivity.level == 23 || EatingObjectsActivity.level == 24 || EatingObjectsActivity.level == 27) && eventX >= (EatingObjectsActivity.this.third.x - ((float) (EatingObjectsActivity.this.third.bitmap.getWidth() / 2))) - 30.0f && eventX <= EatingObjectsActivity.this.third.x + ((float) (EatingObjectsActivity.this.third.bitmap.getWidth() / 2)) + 30.0f && eventY >= (EatingObjectsActivity.this.third.y - ((float) (EatingObjectsActivity.this.third.bitmap.getHeight() / 2))) - 30.0f && eventY <= EatingObjectsActivity.this.third.y + ((float) (EatingObjectsActivity.this.third.bitmap.getHeight() / 2)) + 30.0f) {
                objTouched = EatingObjectsActivity.this.third;
                EatingObjectsActivity.this.third.i_m_touched = true;
            }
            if (objTouched != null && !objTouched.name.equalsIgnoreCase("third")) {
                objTouched.type = true;
                objTouched.partner.type = false;
                objTouched.stopped_by_obstacle = false;
                objTouched.increase_x = false;
                objTouched.increase_y = false;
                objTouched.decrease_x = false;
                objTouched.decrease_y = false;
                objTouched.not_on_borders = true;
                if (EatingObjectsActivity.this.if_object_touched) {
                    EatingObjectsActivity.this.if_object_touched = false;
                } else {
                    EatingObjectsActivity.this.if_object_touched = true;
                }
            } else if (objTouched != null && objTouched.name.equalsIgnoreCase("third")) {
                objTouched.stopped_by_obstacle = false;
                objTouched.increase_x = false;
                objTouched.increase_y = false;
                objTouched.decrease_x = false;
                objTouched.decrease_y = false;
                objTouched.not_on_borders = true;
            }
            return objTouched;
        }

        public boolean onTouchEvent(MotionEvent evnt) {
            float temp_y;
            if (evnt.getAction() == 0) {
                if (evnt.getX() <= 5.0f || evnt.getX() >= ((float) (EatingObjectsActivity.this.pause_btn.getWidth() + 5))) {
                    if (evnt.getX() > ((float) ((EatingObjectsActivity.this.width - EatingObjectsActivity.this.restart_btn.getWidth()) - 5)) && evnt.getX() < ((float) EatingObjectsActivity.this.width) && evnt.getY() > 70.0f && evnt.getY() < ((float) (EatingObjectsActivity.this.restart_btn.getHeight() + 70))) {
                        MediaPlayer mp = MediaPlayer.create(EatingObjectsActivity.this.getBaseContext(), (int) R.raw.click);
                        if (mp != null) {
                            mp.start();
                        }
                        Intent start = new Intent(EatingObjectsActivity.this, EatingObjectsActivity.class);
                        Bundle b = new Bundle();
                        EatingObjectsActivity.path_met = false;
                        b.putInt("level", EatingObjectsActivity.level);
                        b.putInt("score", 0);
                        start.putExtras(b);
                        EatingObjectsActivity.this.finish();
                        EatingObjectsActivity.this.startActivity(start);
                    }
                } else if (evnt.getY() > 70.0f && evnt.getY() < ((float) (EatingObjectsActivity.this.pause_btn.getHeight() + 70))) {
                    MediaPlayer mp2 = MediaPlayer.create(EatingObjectsActivity.this.getBaseContext(), (int) R.raw.click);
                    if (mp2 != null) {
                        mp2.start();
                    }
                    EatingObjectsActivity.this.pause_alert.show();
                    EatingObjectsActivity.this.flg_pause = true;
                    EatingObjectsActivity.this.pause_start_time = System.currentTimeMillis();
                }
                EatingObjectsActivity.this.objTouched = GetTouchedObject(evnt.getX(), evnt.getY());
                if (EatingObjectsActivity.this.objTouched != null) {
                    EatingObjectsActivity.this.path_crossing_from_blob = false;
                    EatingObjectsActivity.this.check_path_crossing_from_blobs = false;
                    EatingObjectsActivity.this.counter_to_check_path_crosssing_from_all_blobs = 0;
                    if (EatingObjectsActivity.path_met && !EatingObjectsActivity.this.objTouched.name.equalsIgnoreCase("third")) {
                        EatingObjectsActivity.path_met = false;
                        EatingObjectsActivity.this.path_crossing_from_blob = false;
                        EatingObjectsActivity.this.objTouched.partner.routes.clear();
                        EatingObjectsActivity.this.objTouched.partner.routes.add(new RouteNates(EatingObjectsActivity.this.objTouched.x, EatingObjectsActivity.this.objTouched.y));
                        EatingObjectsActivity.this.objTouched.partner.increase_m = false;
                    }
                    EatingObjectsActivity.this.objTouched.mPath.reset();
                    EatingObjectsActivity.this.objTouched.routes = new ArrayList<>();
                    EatingObjectsActivity.this.objTouched.starting_x = evnt.getX();
                    EatingObjectsActivity.this.objTouched.starting_y = evnt.getY();
                    EatingObjectsActivity.this.objTouched.i_m_touched = true;
                    EatingObjectsActivity.this.objTouched.x_index = 0;
                    EatingObjectsActivity.this.objTouched.n = 0;
                    EatingObjectsActivity.this.objTouched.m = 1;
                    EatingObjectsActivity.this.objTouched.dont_draw_path = false;
                }
            }
            if (evnt.getAction() == 2 && EatingObjectsActivity.this.objTouched != null) {
                if (EatingObjectsActivity.this.objTouched.name.equalsIgnoreCase("third") && EatingObjectsActivity.this.check_for_third) {
                    EatingObjectsActivity.this.objTouched.routes.clear();
                    EatingObjectsActivity.this.check_for_third = false;
                }
                if (evnt.getY() < 80.0f) {
                    temp_y = 79.0f;
                } else {
                    temp_y = evnt.getY();
                }
                if (!EatingObjectsActivity.path_met || EatingObjectsActivity.this.objTouched.name.equalsIgnoreCase("third")) {
                    EatingObjectsActivity.this.objTouched.routes.add(new RouteNates(evnt.getX(), temp_y));
                    EatingObjectsActivity.this.objTouched.should_i_draw_path = true;
                }
            }
            if (evnt.getAction() == 1) {
                EatingObjectsActivity.this.check_for_third = true;
                if (EatingObjectsActivity.this.objTouched != null && EatingObjectsActivity.this.objTouched.routes.size() > 0) {
                    EatingObjectsActivity.this.if_object_touched = false;
                    EatingObjectsActivity.this.check_path_crossing_from_blobs = true;
                    EatingObjectsActivity.this.objTouched.routes = DouglasPeuckerReduction(EatingObjectsActivity.this.objTouched.routes, Double.valueOf(2.0d));
                    EatingObjectsActivity.this.objTouched.Xf = EatingObjectsActivity.this.objTouched.routes.get(0).posX;
                    EatingObjectsActivity.this.objTouched.Yf = EatingObjectsActivity.this.objTouched.routes.get(0).posY;
                    EatingObjectsActivity.this.objTouched.flgdelta = true;
                    EatingObjectsActivity.this.objTouched.ending_x = evnt.getX();
                    EatingObjectsActivity.this.objTouched.ending_y = evnt.getY();
                    if (EatingObjectsActivity.this.objTouched.ending_x > EatingObjectsActivity.this.objTouched.partner.x - 50.0f && EatingObjectsActivity.this.objTouched.ending_x < EatingObjectsActivity.this.objTouched.partner.x + 50.0f && EatingObjectsActivity.this.objTouched.ending_y > EatingObjectsActivity.this.objTouched.partner.y - 50.0f && EatingObjectsActivity.this.objTouched.ending_y < EatingObjectsActivity.this.objTouched.partner.y + 50.0f) {
                        EatingObjectsActivity.path_met = true;
                        EatingObjectsActivity.this.objTouched.routes.add(new RouteNates(EatingObjectsActivity.this.objTouched.partner.x, EatingObjectsActivity.this.objTouched.partner.y));
                        EatingObjectsActivity.this.objTouched.partner.Xf = EatingObjectsActivity.this.objTouched.routes.get(EatingObjectsActivity.this.objTouched.routes.size() - 1).posX;
                        EatingObjectsActivity.this.objTouched.partner.Yf = EatingObjectsActivity.this.objTouched.routes.get(EatingObjectsActivity.this.objTouched.routes.size() - 1).posY;
                        evnt.setLocation(0.0f, 0.0f);
                        EatingObjectsActivity.this.objTouched.should_i_draw_path = true;
                        EatingObjectsActivity.this.objTouched.partner.m = 1;
                        EatingObjectsActivity.this.objTouched.partner.x_index = EatingObjectsActivity.this.objTouched.routes.size() - 1;
                    }
                    if (EatingObjectsActivity.this.objTouched.name.equalsIgnoreCase("third")) {
                        EatingObjectsActivity.this.third.routes.remove(0);
                    }
                    EatingObjectsActivity.this.objTouched = null;
                }
            }
            return true;
        }

        /* Debug info: failed to restart local var, previous not found, register: 8 */
        public ArrayList<RouteNates> DouglasPeuckerReduction(ArrayList<RouteNates> Points, Double Tolerance) {
            if (Points == null || Points.size() < 3) {
                return Points;
            }
            int lastPoint = Points.size() - 1;
            List<Integer> pointIndexsToKeep = new ArrayList<>();
            pointIndexsToKeep.add(0);
            pointIndexsToKeep.add(Integer.valueOf(lastPoint));
            DouglasPeuckerReduction(Points, 0, lastPoint, Tolerance, pointIndexsToKeep);
            List<RouteNates> returnPoints = new ArrayList<>();
            Collections.sort(pointIndexsToKeep);
            for (Integer intValue : pointIndexsToKeep) {
                returnPoints.add(Points.get(intValue.intValue()));
            }
            return (ArrayList) returnPoints;
        }

        private void DouglasPeuckerReduction(List<RouteNates> points, int firstPoint, int lastPoint, Double tolerance, List<Integer> pointIndexsToKeep) {
            Double maxDistance = Double.valueOf(0.0d);
            int indexFarthest = 0;
            for (int index = firstPoint; index < lastPoint; index++) {
                Double distance = PerpendicularDistance(points.get(firstPoint), points.get(lastPoint), points.get(index));
                if (distance.doubleValue() > maxDistance.doubleValue()) {
                    maxDistance = distance;
                    indexFarthest = index;
                }
            }
            if (maxDistance.doubleValue() > tolerance.doubleValue() && indexFarthest != 0) {
                pointIndexsToKeep.add(Integer.valueOf(indexFarthest));
                DouglasPeuckerReduction(points, firstPoint, indexFarthest, tolerance, pointIndexsToKeep);
                DouglasPeuckerReduction(points, indexFarthest, lastPoint, tolerance, pointIndexsToKeep);
            }
        }

        public Double PerpendicularDistance(RouteNates Point1, RouteNates Point2, RouteNates Point) {
            return Double.valueOf((Double.valueOf(Math.abs(0.5d * ((double) ((((((Point1.posX * Point2.posY) + (Point2.posX * Point.posY)) + (Point.posX * Point1.posY)) - (Point2.posX * Point1.posY)) - (Point.posX * Point2.posY)) - (Point1.posX * Point.posY))))).doubleValue() / Double.valueOf(Math.sqrt(Math.pow((double) (Point1.posX - Point2.posX), 2.0d) + Math.pow((double) (Point1.posY - Point2.posY), 2.0d))).doubleValue()) * 2.0d);
        }
    }

    class GameThread extends Thread {
        private CustomView _panel;
        private SurfaceHolder _surfaceHolder;
        int fps = 25;
        float interpolation = 0.0f;
        int loops;
        int max_fps_skip = 5;
        long next_tick = System.currentTimeMillis();
        boolean run = true;
        int skip_ticks = (1000 / this.fps);

        public GameThread(SurfaceHolder surfaceHolder, CustomView panel) {
            this._surfaceHolder = surfaceHolder;
            this._panel = panel;
        }

        public void run() {
            while (this.run) {
                Canvas c = null;
                try {
                    c = this._surfaceHolder.lockCanvas(null);
                    this.loops = 0;
                    synchronized (this._surfaceHolder) {
                        while (System.currentTimeMillis() > this.next_tick && this.loops < this.max_fps_skip) {
                            this._panel.UpdatePhysics();
                            this.next_tick += (long) this.skip_ticks;
                            this.loops++;
                        }
                        this._panel.onDraw(c);
                    }
                    if (c != null) {
                        this._surfaceHolder.unlockCanvasAndPost(c);
                    }
                } catch (Exception e) {
                    try {
                        Log.e("Error", "at line 2537 Exception is " + e);
                    } finally {
                        if (c != null) {
                            this._surfaceHolder.unlockCanvasAndPost(c);
                        }
                    }
                }
            }
        }

        public void setRunning(boolean mrun) {
            this.run = mrun;
        }
    }

    public class CustomizeDialog extends Dialog implements View.OnClickListener {
        TextView as = ((TextView) findViewById(R.id.text));
        ImageView congr = ((ImageView) findViewById(R.id.cong));
        Button gunbtn1 = ((Button) findViewById(R.id.tryagain));
        Button gunbtn2 = ((Button) findViewById(R.id.nextlevel));
        Button gunbtn3 = ((Button) findViewById(R.id.mainmenu));
        Button gunbtn4;
        ImageView im = ((ImageView) findViewById(R.id.star));
        TextView levelTxt = ((TextView) findViewById(R.id.leveltext));
        String msg;
        Button okButton;

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            if (keyCode != 4) {
                return super.onKeyDown(keyCode, event);
            }
            Intent i = new Intent(EatingObjectsActivity.this.getApplicationContext(), MainMenu.class);
            EatingObjectsActivity.this.finish();
            EatingObjectsActivity.this.startActivity(i);
            return true;
        }

        public CustomizeDialog(Context context) {
            super(context);
            requestWindowFeature(1);
            requestWindowFeature(1);
            setContentView((int) R.layout.levelcomplete);
            this.gunbtn1.setOnClickListener(this);
            this.gunbtn2.setOnClickListener(this);
            this.gunbtn3.setOnClickListener(this);
            if (EatingObjectsActivity.level >= 10) {
                this.gunbtn2.setBackgroundResource(R.drawable.submitscorebtn);
            }
        }

        public void onClick(View v) {
            if (v == this.gunbtn1) {
                MediaPlayer mp = MediaPlayer.create(EatingObjectsActivity.this.getBaseContext(), (int) R.raw.click);
                if (mp != null) {
                    mp.start();
                }
                Intent start = new Intent(EatingObjectsActivity.this, EatingObjectsActivity.class);
                Bundle b = new Bundle();
                b.putInt("level", EatingObjectsActivity.level);
                b.putInt("score", 0);
                start.putExtras(b);
                EatingObjectsActivity.this.finish();
                EatingObjectsActivity.this.startActivity(start);
                dismiss();
            } else if (v == this.gunbtn2) {
                if (EatingObjectsActivity.this.bg_sound != null) {
                    EatingObjectsActivity.this.bg_sound.start();
                }
                MediaPlayer mp2 = MediaPlayer.create(EatingObjectsActivity.this.getBaseContext(), (int) R.raw.click);
                if (mp2 != null) {
                    mp2.start();
                }
                if (EatingObjectsActivity.level >= 10) {
                    Intent SScoreIntent = new Intent(EatingObjectsActivity.this, SubmitScore.class);
                    Bundle b2 = new Bundle();
                    b2.putInt("score", EatingObjectsActivity.score);
                    SScoreIntent.putExtras(b2);
                    EatingObjectsActivity.this.finish();
                    EatingObjectsActivity.this.startActivity(SScoreIntent);
                    return;
                }
                int rank = EatingObjectsActivity.this.achvments.getInt("Rank", 0);
                if (EatingObjectsActivity.level == 1 && rank == 0) {
                    EatingObjectsActivity.flg_ach1 = true;
                    try {
                        EatingObjectsActivity.this.achediter.putInt("Rank", 1);
                        EatingObjectsActivity.this.achediter.commit();
                        if (OpenFeint.isUserLoggedIn()) {
                            new Achievement("1152762").unlock(new Achievement.UnlockCB() {
                                public void onFailure(String exceptionMessage) {
                                    Toast.makeText(EatingObjectsActivity.this, "Error (" + exceptionMessage + ") unlocking achievement.", 0).show();
                                    EatingObjectsActivity.this.setResult(0);
                                }

                                public void onSuccess(boolean newUnlock) {
                                    EatingObjectsActivity.this.setResult(-1);
                                    Toast.makeText(EatingObjectsActivity.this, "New Acheivement Unlocked", 0).show();
                                    if (EatingObjectsActivity.this.acheivment_sound != null) {
                                        EatingObjectsActivity.this.acheivment_sound.start();
                                    }
                                }
                            });
                        }
                    } catch (Exception e) {
                    }
                } else if (EatingObjectsActivity.level == 9 && rank == 1) {
                    EatingObjectsActivity.flg_ach2 = true;
                    try {
                        EatingObjectsActivity.this.achediter.putInt("Rank", 2);
                        EatingObjectsActivity.this.achediter.commit();
                        if (OpenFeint.isUserLoggedIn()) {
                            new Achievement("1172702").unlock(new Achievement.UnlockCB() {
                                public void onFailure(String exceptionMessage) {
                                    Toast.makeText(EatingObjectsActivity.this, "Error (" + exceptionMessage + ") unlocking achievement.", 0).show();
                                    EatingObjectsActivity.this.setResult(0);
                                }

                                public void onSuccess(boolean newUnlock) {
                                    EatingObjectsActivity.this.setResult(-1);
                                    Toast.makeText(EatingObjectsActivity.this, "New Acheivement Unlocked", 0).show();
                                    if (EatingObjectsActivity.this.acheivment_sound != null) {
                                        EatingObjectsActivity.this.acheivment_sound.start();
                                    }
                                }
                            });
                        }
                    } catch (Exception e2) {
                    }
                } else if (EatingObjectsActivity.level == 20 && rank == 2) {
                    EatingObjectsActivity.flg_ach3 = true;
                    try {
                        EatingObjectsActivity.this.achediter.putInt("Rank", 3);
                        EatingObjectsActivity.this.achediter.commit();
                        if (OpenFeint.isUserLoggedIn()) {
                            new Achievement("1172712").unlock(new Achievement.UnlockCB() {
                                public void onFailure(String exceptionMessage) {
                                    Toast.makeText(EatingObjectsActivity.this, "Error (" + exceptionMessage + ") unlocking achievement.", 0).show();
                                    EatingObjectsActivity.this.setResult(0);
                                }

                                public void onSuccess(boolean newUnlock) {
                                    EatingObjectsActivity.this.setResult(-1);
                                    Toast.makeText(EatingObjectsActivity.this, "New Acheivement Unlocked", 0).show();
                                    if (EatingObjectsActivity.this.acheivment_sound != null) {
                                        EatingObjectsActivity.this.acheivment_sound.start();
                                    }
                                }
                            });
                        }
                    } catch (Exception e3) {
                    }
                }
                if (EatingObjectsActivity.level >= 10) {
                    Toast.makeText(EatingObjectsActivity.this.getApplicationContext(), "Buy Pro version for more fun.", 1).show();
                    return;
                }
                Intent start2 = new Intent(EatingObjectsActivity.this, EatingObjectsActivity.class);
                Bundle b3 = new Bundle();
                b3.putInt("level", EatingObjectsActivity.level + 1);
                b3.putInt("score", EatingObjectsActivity.score);
                EatingObjectsActivity.path_met = false;
                start2.putExtras(b3);
                EatingObjectsActivity.this.finish();
                EatingObjectsActivity.this.startActivity(start2);
                dismiss();
            } else if (v == this.gunbtn3) {
                MediaPlayer mp3 = MediaPlayer.create(EatingObjectsActivity.this.getBaseContext(), (int) R.raw.click);
                if (mp3 != null) {
                    mp3.start();
                }
                Intent start3 = new Intent(EatingObjectsActivity.this, MainMenu.class);
                EatingObjectsActivity.this.finish();
                EatingObjectsActivity.this.startActivity(start3);
                dismiss();
            }
        }
    }

    public class TryAgainAlert extends Dialog implements View.OnClickListener {
        Button mainmenu = ((Button) findViewById(R.id.mainmenu));
        TextView scores = ((TextView) findViewById(R.id.text));
        Button submit = ((Button) findViewById(R.id.submitscore));
        Button tryagain = ((Button) findViewById(R.id.trya));

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            if (keyCode != 4) {
                return super.onKeyDown(keyCode, event);
            }
            Intent i = new Intent(EatingObjectsActivity.this.getApplicationContext(), MainMenu.class);
            EatingObjectsActivity.this.finish();
            EatingObjectsActivity.this.startActivity(i);
            return true;
        }

        public TryAgainAlert(Context context) {
            super(context);
            requestWindowFeature(1);
            setContentView((int) R.layout.tryagainalert);
            this.tryagain.setOnClickListener(this);
            this.mainmenu.setOnClickListener(this);
            this.submit.setOnClickListener(this);
        }

        public void onClick(View v) {
            if (v == this.tryagain) {
                EatingObjectsActivity.path_met = false;
                MediaPlayer mp = MediaPlayer.create(EatingObjectsActivity.this.getBaseContext(), (int) R.raw.click);
                if (mp != null) {
                    mp.start();
                }
                Intent start = new Intent(EatingObjectsActivity.this, EatingObjectsActivity.class);
                Bundle b = new Bundle();
                EatingObjectsActivity.path_met = false;
                b.putInt("level", EatingObjectsActivity.level);
                b.putInt("score", 0);
                start.putExtras(b);
                EatingObjectsActivity.this.finish();
                EatingObjectsActivity.this.startActivity(start);
            } else if (v == this.mainmenu) {
                MediaPlayer mp2 = MediaPlayer.create(EatingObjectsActivity.this.getBaseContext(), (int) R.raw.click);
                if (mp2 != null) {
                    mp2.start();
                }
                Intent start2 = new Intent(EatingObjectsActivity.this, MainMenu.class);
                EatingObjectsActivity.this.finish();
                EatingObjectsActivity.this.startActivity(start2);
            } else if (v == this.submit) {
                MediaPlayer mp3 = MediaPlayer.create(EatingObjectsActivity.this.getBaseContext(), (int) R.raw.click);
                if (mp3 != null) {
                    mp3.start();
                }
                Intent SScoreIntent = new Intent(EatingObjectsActivity.this, SubmitScore.class);
                Bundle b2 = new Bundle();
                b2.putInt("score", EatingObjectsActivity.score);
                SScoreIntent.putExtras(b2);
                EatingObjectsActivity.this.finish();
                EatingObjectsActivity.this.startActivity(SScoreIntent);
            }
        }
    }

    public class PauseAlert extends Dialog implements View.OnClickListener {
        Button exit = ((Button) findViewById(R.id.exit));
        Button mainmenu = ((Button) findViewById(R.id.mainmenu));
        Button resume = ((Button) findViewById(R.id.resume));

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            if (keyCode != 4) {
                return super.onKeyDown(keyCode, event);
            }
            Intent i = new Intent(EatingObjectsActivity.this.getApplicationContext(), MainMenu.class);
            EatingObjectsActivity.this.finish();
            EatingObjectsActivity.this.startActivity(i);
            return true;
        }

        public PauseAlert(Context context) {
            super(context);
            requestWindowFeature(1);
            setContentView((int) R.layout.pausealert);
            this.resume.setOnClickListener(this);
            this.mainmenu.setOnClickListener(this);
            this.exit.setOnClickListener(this);
        }

        public void onClick(View v) {
            if (v == this.resume) {
                MediaPlayer mp = MediaPlayer.create(EatingObjectsActivity.this.getBaseContext(), (int) R.raw.click);
                if (mp != null) {
                    mp.start();
                }
                EatingObjectsActivity.this.flg_pause = false;
                EatingObjectsActivity.this.pause_time += System.currentTimeMillis() - EatingObjectsActivity.this.pause_start_time;
                dismiss();
            } else if (v == this.mainmenu) {
                MediaPlayer mp2 = MediaPlayer.create(EatingObjectsActivity.this.getBaseContext(), (int) R.raw.click);
                if (mp2 != null) {
                    mp2.start();
                }
                Intent start = new Intent(EatingObjectsActivity.this, MainMenu.class);
                EatingObjectsActivity.this.finish();
                EatingObjectsActivity.this.startActivity(start);
            } else if (v == this.exit && MediaPlayer.create(EatingObjectsActivity.this.getBaseContext(), (int) R.raw.click) != null) {
                EatingObjectsActivity.this.finish();
            }
        }
    }
}
