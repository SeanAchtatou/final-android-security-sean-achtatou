package com.tenapp.hoopersLite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.ViewSwitcher;

public class SelectWorld extends Activity implements ViewSwitcher.ViewFactory {
    MediaPlayer bg_sound;
    SharedPreferences getpref;
    int level = 0;
    Integer[] mImageIds = {Integer.valueOf((int) R.drawable.nob), Integer.valueOf((int) R.drawable.spacelocked), Integer.valueOf((int) R.drawable.waterworldlocked)};
    MediaPlayer mp;
    Button select;
    int which_world_is_unlocked;

    public void onDestroy() {
        super.onDestroy();
        if (this.bg_sound != null) {
            this.bg_sound.stop();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            Intent i = new Intent(getApplicationContext(), MainMenu.class);
            finish();
            startActivity(i);
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onCreate(Bundle b) {
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        super.onCreate(b);
        setContentView((int) R.layout.selectworld);
        this.mp = MediaPlayer.create(getBaseContext(), (int) R.raw.click);
        this.bg_sound = MediaPlayer.create(getBaseContext(), (int) R.raw.mainmenusound);
        if (this.bg_sound != null) {
            this.bg_sound.setLooping(true);
            this.bg_sound.start();
        }
        this.select = (Button) findViewById(R.id.selectworldbtn);
        this.getpref = getSharedPreferences("R.values.W1Levels", 2);
        this.which_world_is_unlocked = this.getpref.getInt("World", 0);
        if (this.which_world_is_unlocked == 2) {
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.space);
        } else if (this.which_world_is_unlocked == 3) {
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.space);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.waterworld);
        }
        Gallery gallery = (Gallery) findViewById(R.id.gallery);
        gallery.setAdapter((SpinnerAdapter) new ImageAdapter(this));
        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                SelectWorld.this.level = arg2;
                if (SelectWorld.this.level == 0) {
                    Intent i = new Intent(SelectWorld.this.getApplicationContext(), ModeOneLevels.class);
                    SelectWorld.this.finish();
                    SelectWorld.this.startActivity(i);
                } else if (SelectWorld.this.level == 1) {
                    if (SelectWorld.this.which_world_is_unlocked >= 2) {
                        Intent i2 = new Intent(SelectWorld.this.getApplicationContext(), CommingSoon.class);
                        SelectWorld.this.finish();
                        SelectWorld.this.startActivity(i2);
                    }
                } else if (SelectWorld.this.level == 2 && SelectWorld.this.which_world_is_unlocked >= 3) {
                    Intent i3 = new Intent(SelectWorld.this.getApplicationContext(), CommingSoon.class);
                    SelectWorld.this.finish();
                    SelectWorld.this.startActivity(i3);
                }
            }
        });
        this.select.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (SelectWorld.this.mp != null) {
                    SelectWorld.this.mp.start();
                }
                SelectWorld.this.select.setBackgroundResource(R.drawable.mainmenubtn2off);
                Intent i = new Intent(SelectWorld.this.getApplicationContext(), MainMenu.class);
                SelectWorld.this.finish();
                SelectWorld.this.startActivity(i);
            }
        });
    }

    public class ImageAdapter extends BaseAdapter {
        private Context ctx;

        public ImageAdapter(Context c) {
            this.ctx = c;
        }

        public int getCount() {
            return SelectWorld.this.mImageIds.length;
        }

        public Object getItem(int arg0) {
            SelectWorld.this.level = arg0;
            return Integer.valueOf(arg0);
        }

        public long getItemId(int arg0) {
            return (long) arg0;
        }

        public View getView(int arg0, View arg1, ViewGroup arg2) {
            ImageView iView = new ImageView(this.ctx);
            iView.setImageResource(SelectWorld.this.mImageIds[arg0].intValue());
            SelectWorld.this.level = arg0;
            iView.setScaleType(ImageView.ScaleType.FIT_XY);
            iView.setLayoutParams(new Gallery.LayoutParams(150, 150));
            return iView;
        }
    }

    public View makeView() {
        ImageView iView = new ImageView(this);
        iView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        iView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        iView.setBackgroundColor(-16777216);
        return iView;
    }
}
