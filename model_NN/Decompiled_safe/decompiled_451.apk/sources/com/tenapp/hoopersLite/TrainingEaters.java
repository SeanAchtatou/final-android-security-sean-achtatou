package com.tenapp.hoopersLite;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.Log;
import java.util.ArrayList;

public class TrainingEaters {
    static boolean next_level_ok_stop_moving = false;
    float Xf;
    float Xi;
    float Yf;
    float Yi;
    float angle;
    public Bitmap bitmap = null;
    Paint bitmapPaint;
    private float current_lineX = 0.0f;
    private float current_lineY = 0.0f;
    private DashPathEffect dashPath;
    boolean decrease_x = false;
    boolean decrease_y = false;
    float degree;
    float deltaX;
    float deltaY;
    boolean direction_x;
    boolean direction_y;
    boolean dont_draw_path;
    float ending_x;
    float ending_y;
    boolean flgNotMOVE_ON_LINE;
    boolean flgdelta = true;
    int height;
    boolean horizontal_obs = false;
    boolean i_m_touched = false;
    boolean increase_m = false;
    boolean increase_x = false;
    boolean increase_y = false;
    int m = 1;
    Paint mPaint;
    Path mPath;
    Matrix mat;
    int n = 0;
    public String name = null;
    boolean not_on_borders = true;
    TrainingEaters partner;
    ArrayList<RouteNates> routes;
    boolean should_i_draw_path = false;
    int size_of_PR;
    float speed = 3.0f;
    float starting_x;
    float starting_y;
    boolean stopped_by_obstacle = false;
    float temp_x;
    float temp_y;
    boolean type;
    boolean vertical_obs = false;
    int width;
    public float x;
    int x_index = 0;
    public float y;

    public TrainingEaters(Bitmap bee, int i, int j, String n2, boolean typ) {
        this.bitmap = bee;
        this.x = (float) i;
        this.y = (float) j;
        this.name = n2;
        this.routes = new ArrayList<>();
        this.mPath = new Path();
        this.mat = new Matrix();
        this.bitmapPaint = new Paint();
        this.type = typ;
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(true);
        this.mPaint.setDither(true);
        this.mPaint.setColor(-1);
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mPaint.setStrokeCap(Paint.Cap.BUTT);
        this.mPaint.setStrokeJoin(Paint.Join.MITER);
        this.dashPath = new DashPathEffect(new float[]{20.0f, 5.0f}, 1.0f);
        this.mPaint.setPathEffect(this.dashPath);
        if (this.width <= 0 || this.height <= 0) {
            this.width = Settings.width;
            this.height = Settings.height;
        }
        if (this.width <= 0 || this.height <= 0) {
            this.width = 450;
            this.height = 854;
        }
    }

    public float getx() {
        return this.x;
    }

    public float gety() {
        return this.y;
    }

    public void draw(Canvas c) {
        this.mat = new Matrix();
        this.mat.postRotate(this.angle, this.x, this.y);
        if (this.should_i_draw_path) {
            c.drawPath(this.mPath, this.mPaint);
        }
        c.drawBitmap(this.bitmap, this.x - ((float) (this.bitmap.getWidth() / 2)), this.y - ((float) (this.bitmap.getHeight() / 2)), (Paint) null);
    }

    public void update_physics() {
        if (next_level_ok_stop_moving) {
            this.mPath.reset();
        } else if (!Training.path_met) {
            update_physics_default();
        } else {
            update_physics_on_same_path();
        }
    }

    public void update_physics_default() {
        Log.d("CHECK34", "in default update physics");
        if (!next_level_ok_stop_moving) {
            if (this.x > ((float) (this.width - 10))) {
                this.decrease_x = true;
                this.increase_x = false;
                this.decrease_y = false;
                this.increase_y = false;
                Log.d("Checking XY", "X > width");
                this.not_on_borders = false;
            } else if (this.x < 10.0f) {
                this.decrease_x = false;
                this.increase_x = true;
                this.decrease_y = false;
                this.increase_y = false;
                Log.d("Checking XY", "X < 0");
                this.not_on_borders = false;
            }
            if (this.y < 80.0f) {
                this.decrease_x = false;
                this.increase_x = false;
                this.decrease_y = false;
                this.increase_y = true;
                this.not_on_borders = false;
                Log.d("Checking XY", "y < 0");
            } else if (this.y >= ((float) (this.height - 10))) {
                this.decrease_x = false;
                this.increase_x = false;
                this.decrease_y = true;
                this.increase_y = false;
                this.not_on_borders = false;
                Log.d("Checking XY", "y > height");
            }
            if (this.decrease_x) {
                this.x -= 1.0f;
            }
            if (this.increase_x) {
                this.x += 1.0f;
            }
            if (this.increase_y) {
                this.y += 1.0f;
            }
            if (this.decrease_y) {
                this.y -= 1.0f;
            }
            if (this.stopped_by_obstacle) {
                Log.d("Checking XY", "Stopped by obs");
                this.increase_x = false;
                this.decrease_x = false;
                this.increase_y = false;
                this.decrease_y = false;
                this.mPath.reset();
                if (this.vertical_obs) {
                    if (this.direction_y) {
                        this.x -= 1.0f;
                    } else if (!this.direction_y) {
                        this.x += 1.0f;
                    }
                } else if (this.direction_y) {
                    this.y -= 1.0f;
                } else if (!this.direction_y) {
                    this.y += 1.0f;
                }
                if (this.x > ((float) (this.width - 10))) {
                    this.decrease_x = true;
                    this.increase_x = false;
                    this.decrease_y = false;
                    this.increase_y = false;
                    Log.d("Checking XY", "X > width");
                    this.not_on_borders = false;
                    this.stopped_by_obstacle = false;
                } else if (this.x < 10.0f) {
                    this.decrease_x = false;
                    this.increase_x = true;
                    this.decrease_y = false;
                    this.increase_y = false;
                    Log.d("Checking XY", "X < 0");
                    this.not_on_borders = false;
                    this.stopped_by_obstacle = false;
                }
                if (this.y < 80.0f) {
                    this.decrease_x = false;
                    this.increase_x = false;
                    this.decrease_y = false;
                    this.increase_y = true;
                    this.not_on_borders = false;
                    this.stopped_by_obstacle = false;
                    Log.d("Checking XY", "y < 0");
                } else if (this.y >= ((float) (this.height - 10))) {
                    this.decrease_x = false;
                    this.increase_x = false;
                    this.decrease_y = true;
                    this.increase_y = false;
                    this.not_on_borders = false;
                    this.stopped_by_obstacle = false;
                    Log.d("Checking XY", "y > height");
                }
            }
            if (!this.stopped_by_obstacle && this.not_on_borders) {
                Log.d("Checking XY", "Not stopped by obs");
                this.increase_x = false;
                this.decrease_x = false;
                this.increase_y = false;
                this.decrease_y = false;
                if (!this.routes.isEmpty()) {
                    this.mPath.reset();
                    this.mPath.moveTo(this.x, this.y);
                    float linex = this.x;
                    float liney = this.y;
                    for (int i = this.n; i < this.routes.size(); i++) {
                        try {
                            RouteNates points = this.routes.get(i);
                            this.current_lineX = points.posX;
                            this.current_lineY = points.posY;
                            this.mPath.quadTo(linex, liney, (this.current_lineX + linex) / 2.0f, (this.current_lineY + liney) / 2.0f);
                            linex = this.current_lineX;
                            liney = this.current_lineY;
                        } catch (IndexOutOfBoundsException e) {
                            Log.e("Error", "at line 271 Exception is " + e);
                        }
                    }
                    if (this.x >= this.Xf - 1.0f && this.x <= this.Xf + 1.0f) {
                        if (this.x_index < this.routes.size()) {
                            RouteNates epoint = this.routes.get(this.x_index);
                            this.Xf = epoint.posX;
                            this.Yf = epoint.posY;
                            this.flgdelta = true;
                            if (this.x_index > 0) {
                                this.n++;
                            }
                            this.x_index++;
                        } else {
                            this.mPath.reset();
                            this.routes.clear();
                            this.flgNotMOVE_ON_LINE = true;
                            this.i_m_touched = false;
                            this.n = 0;
                        }
                    }
                    if (this.flgdelta) {
                        this.deltaX = this.Xf - this.x;
                        this.deltaY = this.Yf - this.y;
                        Log.w("x y XF YF", " " + this.x + " " + this.y + " " + this.Xf + " " + this.Yf);
                        this.degree = (float) Math.atan2((double) this.deltaY, (double) this.deltaX);
                        this.angle = (float) (((double) (this.degree * 180.0f)) / 3.14d);
                        this.flgdelta = false;
                    }
                    if (this.deltaX > 0.0f) {
                        if (this.deltaY > 0.0f) {
                            float d2 = (float) Math.sin((double) this.degree);
                            this.temp_y = this.y + d2;
                            if (this.temp_y >= this.y) {
                                this.direction_y = true;
                            } else {
                                this.direction_y = false;
                            }
                            this.y += d2;
                        } else {
                            float d22 = (float) (Math.sin((double) this.degree) * -1.0d);
                            this.temp_y = this.y - d22;
                            if (this.temp_y >= this.y) {
                                this.direction_y = true;
                            } else {
                                this.direction_y = false;
                            }
                            this.y -= d22;
                        }
                        float d1 = (float) Math.cos((double) this.degree);
                        this.temp_x = this.x + d1;
                        if (this.temp_x >= this.x) {
                            this.direction_x = true;
                        } else {
                            this.direction_x = false;
                        }
                        this.x += d1;
                    } else if (this.deltaX < 0.0f) {
                        if (this.deltaY > 0.0f) {
                            float d23 = (float) Math.sin((double) this.degree);
                            this.temp_y = this.y + d23;
                            if (this.temp_y >= this.y) {
                                this.direction_y = true;
                            } else {
                                this.direction_y = false;
                            }
                            this.y += d23;
                        } else {
                            float d24 = (float) (Math.sin((double) this.degree) * -1.0d);
                            this.Yi = this.y;
                            this.temp_y = this.y - d24;
                            if (this.temp_y >= this.y) {
                                this.direction_y = true;
                            } else {
                                this.direction_y = false;
                            }
                            this.y -= d24;
                        }
                    }
                    float d12 = (float) Math.cos((double) this.degree);
                    this.temp_x = this.x + d12;
                    if (this.temp_x >= this.x) {
                        this.direction_x = true;
                    } else {
                        this.direction_x = false;
                    }
                    this.x -= -1.0f * d12;
                }
            }
            if (this.routes.size() <= 0 && !this.i_m_touched) {
                this.routes.add(new RouteNates(this.partner.x, this.partner.y));
                this.should_i_draw_path = false;
            }
            if (this.name.equals("third") && this.routes.size() <= 0) {
                this.routes.add(new RouteNates(this.partner.x, this.partner.y));
                this.should_i_draw_path = false;
            }
        }
    }

    public void update_physics_on_same_path() {
        Log.d("CHECK34", "in connected update physics");
        if (this.type) {
            this.mPath.reset();
            this.partner.mPath.reset();
            this.mPath.moveTo(this.x, this.y);
            float linex = this.x;
            float liney = this.y;
            if (!Training.display_test) {
                for (int i = this.n; i < this.routes.size(); i++) {
                    try {
                        RouteNates points = this.routes.get(i);
                        this.current_lineX = points.posX;
                        this.current_lineY = points.posY;
                        this.mPath.quadTo(linex, liney, (this.current_lineX + linex) / 2.0f, (this.current_lineY + liney) / 2.0f);
                        linex = this.current_lineX;
                        liney = this.current_lineY;
                    } catch (IndexOutOfBoundsException e) {
                        Log.e("Error", "at line 474 Exception is " + e);
                    }
                }
            } else if (Training.display_test) {
                for (int i2 = this.n; i2 <= this.routes.size() - this.m; i2++) {
                    try {
                        RouteNates points2 = this.routes.get(i2);
                        this.current_lineX = points2.posX;
                        this.current_lineY = points2.posY;
                        this.mPath.quadTo(linex, liney, (this.current_lineX + linex) / 2.0f, (this.current_lineY + liney) / 2.0f);
                        linex = this.current_lineX;
                        liney = this.current_lineY;
                    } catch (IndexOutOfBoundsException e2) {
                        Log.e("Error", "at line 497 Exception is " + e2);
                    }
                }
                if (!this.dont_draw_path) {
                    this.mPath.quadTo(this.current_lineX, this.current_lineY, this.partner.x, this.partner.y);
                }
                if (this.n == this.routes.size() - this.m) {
                    this.dont_draw_path = true;
                }
                if (this.dont_draw_path) {
                    this.mPath = new Path();
                    this.mPath.moveTo(this.x, this.y);
                    this.mPath.lineTo(this.partner.x, this.partner.y);
                }
            }
            if (this.x >= this.Xf - 1.0f && this.x <= this.Xf + 1.0f) {
                if (this.x_index < this.routes.size()) {
                    RouteNates epoint = this.routes.get(this.x_index);
                    this.Xf = epoint.posX;
                    this.Yf = epoint.posY;
                    this.flgdelta = true;
                    if (this.x_index > 0) {
                        this.n++;
                    }
                    this.x_index++;
                } else {
                    this.mPath.reset();
                    this.routes.clear();
                    this.flgNotMOVE_ON_LINE = true;
                    this.i_m_touched = false;
                    this.n = 0;
                }
            }
            if (Training.display_test) {
                if (this.flgdelta) {
                    this.deltaX = this.Xf - this.x;
                    this.deltaY = this.Yf - this.y;
                    Log.w("x y XF YF", " " + this.x + " " + this.y + " " + this.Xf + " " + this.Yf);
                    this.degree = (float) Math.atan2((double) this.deltaY, (double) this.deltaX);
                    this.angle = (float) (((double) (this.degree * 180.0f)) / 3.14d);
                    this.flgdelta = false;
                }
                if (this.deltaX > 0.0f) {
                    if (this.deltaY > 0.0f) {
                        float d2 = (float) Math.sin((double) this.degree);
                        this.temp_y = this.y + d2;
                        if (this.temp_y >= this.y) {
                            this.direction_y = true;
                        } else {
                            this.direction_y = false;
                        }
                        this.y += d2;
                    } else {
                        float d22 = (float) (Math.sin((double) this.degree) * -1.0d);
                        this.temp_y = this.y - d22;
                        if (this.temp_y >= this.y) {
                            this.direction_y = true;
                        } else {
                            this.direction_y = false;
                        }
                        this.y -= d22;
                    }
                    float d1 = (float) Math.cos((double) this.degree);
                    this.temp_x = this.x + d1;
                    if (this.temp_x >= this.x) {
                        this.direction_x = true;
                    } else {
                        this.direction_x = false;
                    }
                    this.x += d1;
                } else if (this.deltaX < 0.0f) {
                    if (this.deltaY > 0.0f) {
                        float d23 = (float) Math.sin((double) this.degree);
                        this.temp_y = this.y + d23;
                        if (this.temp_y >= this.y) {
                            this.direction_y = true;
                        } else {
                            this.direction_y = false;
                        }
                        this.y += d23;
                    } else {
                        float d24 = (float) (Math.sin((double) this.degree) * -1.0d);
                        this.Yi = this.y;
                        this.temp_y = this.y - d24;
                        if (this.temp_y >= this.y) {
                            this.direction_y = true;
                        } else {
                            this.direction_y = false;
                        }
                        this.y -= d24;
                    }
                }
                float d12 = (float) Math.cos((double) this.degree);
                this.temp_x = this.x + d12;
                if (this.temp_x >= this.x) {
                    this.direction_x = true;
                } else {
                    this.direction_x = false;
                }
                this.x -= -1.0f * d12;
                return;
            }
            return;
        }
        if (!this.partner.routes.isEmpty()) {
            this.mPath.reset();
            this.mPath.moveTo(this.x, this.y);
            float f = this.x;
            float f2 = this.y;
            if (Training.display_test) {
                for (int i3 = this.partner.routes.size() - 1; i3 > 0; i3--) {
                    try {
                        RouteNates points3 = this.partner.routes.get(i3);
                        this.current_lineX = points3.posX;
                        this.current_lineY = points3.posY;
                        float linex2 = this.current_lineX;
                        float liney2 = this.current_lineY;
                    } catch (IndexOutOfBoundsException e3) {
                        Log.e("Error", "at line 658 Exception is " + e3);
                    }
                }
            }
        }
        if (this.x >= this.Xf - 1.0f && this.x <= this.Xf + 1.0f) {
            Log.e("Type2", "in xf yf");
            if (this.partner.routes.size() > 0) {
                Log.e("Index", "INdewx>0 " + this.x + " " + this.y + " xindex" + this.x_index + " Xf" + this.Xf + " " + this.Yf);
                RouteNates epoint2 = this.partner.routes.get(this.x_index);
                this.Xf = epoint2.posX;
                this.Yf = epoint2.posY;
                Log.e("Index", "INdewx>0 " + this.x + " " + this.y);
                this.flgdelta = true;
                this.x_index--;
                if (this.increase_m) {
                    this.partner.m++;
                }
                this.increase_m = true;
                Log.e("Check", String.valueOf(this.x) + " " + this.y + " " + this.Xf + " " + this.Yf);
            }
        }
        if (Training.display_test) {
            if (this.flgdelta) {
                this.deltaX = this.Xf - this.x;
                this.deltaY = this.Yf - this.y;
                this.degree = (float) Math.atan2((double) this.deltaY, (double) this.deltaX);
                this.angle = (float) (((double) (this.degree * 180.0f)) / 3.14d);
                this.flgdelta = false;
            }
            if (this.deltaX > 0.0f) {
                if (this.deltaY > 0.0f) {
                    float d25 = (float) Math.sin((double) this.degree);
                    this.temp_y = this.y + d25;
                    if (this.temp_y >= this.y) {
                        this.direction_y = true;
                    } else {
                        this.direction_y = false;
                    }
                    this.y += d25;
                } else {
                    float d26 = (float) (Math.sin((double) this.degree) * -1.0d);
                    this.temp_y = this.y - d26;
                    if (this.temp_y >= this.y) {
                        this.direction_y = true;
                    } else {
                        this.direction_y = false;
                    }
                    this.y -= d26;
                }
                float d13 = (float) Math.cos((double) this.degree);
                this.temp_x = this.x + d13;
                if (this.temp_x >= this.x) {
                    this.direction_x = true;
                } else {
                    this.direction_x = false;
                }
                this.x += d13;
            } else if (this.deltaX < 0.0f) {
                if (this.deltaY > 0.0f) {
                    float d27 = (float) Math.sin((double) this.degree);
                    this.temp_y = this.y + d27;
                    if (this.temp_y >= this.y) {
                        this.direction_y = true;
                    } else {
                        this.direction_y = false;
                    }
                    this.y += d27;
                } else {
                    float sin = (float) (Math.sin((double) this.degree) * -1.0d);
                    this.Yi = this.y;
                    float d28 = (float) (Math.sin((double) this.degree) * -1.0d);
                    this.Yi = this.y;
                    this.temp_y = this.y - d28;
                    if (this.temp_y >= this.y) {
                        this.direction_y = true;
                    } else {
                        this.direction_y = false;
                    }
                    this.y -= d28;
                }
            }
            float d14 = (float) Math.cos((double) this.degree);
            this.temp_x = this.x + d14;
            if (this.temp_x >= this.x) {
                this.direction_x = true;
            } else {
                this.direction_x = false;
            }
            this.x -= -1.0f * d14;
        }
    }
}
