package com.tenapp.hoopersLite;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

public class Help extends Activity {
    MediaPlayer bg_sound;
    int help_number = 1;
    ImageView image;
    MediaPlayer mp;
    ImageView text;

    public void onDestroy() {
        super.onDestroy();
        if (this.bg_sound != null) {
            this.bg_sound.stop();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        Intent i = new Intent(getApplicationContext(), MainMenu.class);
        finish();
        startActivity(i);
        return true;
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.help);
        this.text = (ImageView) findViewById(R.id.text);
        this.image = (ImageView) findViewById(R.id.imag);
        this.bg_sound = MediaPlayer.create(getBaseContext(), (int) R.raw.mainmenusound);
        if (this.bg_sound != null) {
            this.bg_sound.setLooping(true);
            this.bg_sound.start();
        }
        this.mp = MediaPlayer.create(getBaseContext(), (int) R.raw.click);
        this.image.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (Help.this.mp != null) {
                    Help.this.mp.start();
                }
                if (Help.this.help_number == 1) {
                    Help.this.help_number = 2;
                    Help.this.text.setImageResource(R.drawable.help2txt);
                    Help.this.image.setImageResource(R.drawable.help2img);
                } else if (Help.this.help_number == 2) {
                    Help.this.help_number = 3;
                    Help.this.text.setImageResource(R.drawable.help3txt);
                    Help.this.image.setImageResource(R.drawable.help3img);
                } else if (Help.this.help_number == 3) {
                    Help.this.help_number = 4;
                    Help.this.text.setImageResource(R.drawable.help4txt);
                    Help.this.image.setImageResource(R.drawable.help4img);
                } else if (Help.this.help_number == 4) {
                    Help.this.help_number = 5;
                    Help.this.text.setImageResource(R.drawable.help5txt);
                    Help.this.image.setImageResource(R.drawable.help5img);
                } else if (Help.this.help_number == 5) {
                    Help.this.help_number = 1;
                    Help.this.text.setImageResource(R.drawable.help1txt);
                    Help.this.image.setImageResource(R.drawable.help1img);
                }
            }
        });
    }
}
