package com.tenapp.hoopersLite;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;

public class SettingsChooser extends Activity {
    MediaPlayer bg_sound;
    boolean fps = Settings.fps;
    CheckBox fpsC;
    boolean music = Settings.music;
    CheckBox musicC;
    CheckBox soundC;
    boolean sounds = Settings.sounds;

    public void onDestroy() {
        super.onDestroy();
        if (this.bg_sound != null) {
            this.bg_sound.stop();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        Intent main = new Intent(this, MainMenu.class);
        finish();
        startActivity(main);
        return true;
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.settings);
        this.soundC = (CheckBox) findViewById(R.id.sounds);
        this.musicC = (CheckBox) findViewById(R.id.music);
        this.fpsC = (CheckBox) findViewById(R.id.fps);
        this.bg_sound = MediaPlayer.create(getBaseContext(), (int) R.raw.mainmenusound);
        if (this.bg_sound != null) {
            this.bg_sound.setLooping(true);
            this.bg_sound.start();
        }
        if (Settings.fps) {
            this.fpsC.setChecked(true);
        } else {
            this.fpsC.setChecked(false);
        }
        if (Settings.sounds) {
            this.soundC.setChecked(true);
        } else {
            this.soundC.setChecked(false);
        }
        if (Settings.music) {
            this.musicC.setChecked(true);
        } else {
            this.musicC.setChecked(false);
        }
        this.soundC.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    Settings.sounds = true;
                } else {
                    Settings.sounds = false;
                }
            }
        });
        this.musicC.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    Settings.music = true;
                } else {
                    Settings.music = false;
                }
            }
        });
        this.fpsC.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    Settings.fps = true;
                } else {
                    Settings.fps = false;
                }
            }
        });
    }
}
