package com.tenapp.hoopersLite;

public class RouteNates {
    float posX;
    float posY;

    public RouteNates(float x, float y) {
        this.posX = x;
        this.posY = y;
    }

    public float getPosX() {
        return this.posX;
    }

    public void setPosX(int posX2) {
        this.posX = (float) posX2;
    }

    public float getPosY() {
        return this.posY;
    }

    public void setPosY(int posY2) {
        this.posY = (float) posY2;
    }
}
