package com.tenapp.hoopersLite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Training extends Activity {
    static boolean display_test = true;
    static boolean path_met = false;
    static int score = 0;
    ArrayList<TrainingEaters> Hooppers;
    ArrayList<Food> Spikes;
    MediaPlayer acheivment_sound;
    Bitmap bee = null;
    Bitmap bg_image = null;
    MediaPlayer bg_sound;
    Paint blackP = new Paint();
    Food blob1 = null;
    Food blob2 = null;
    ArrayList<Food> blobs;
    Bitmap blu1;
    Bitmap blu2;
    Bitmap blu3;
    Bitmap blu4;
    Bitmap blu5;
    Bitmap blu6;
    Bitmap blu7;
    Bitmap blu8;
    boolean check_for_third = true;
    boolean check_obstacle = true;
    boolean check_path_crossing_from_blobs = false;
    Bitmap cmt_box = null;
    MediaPlayer collision;
    int count_help = 0;
    int counter_to_check_path_crosssing_from_all_blobs = 0;
    int counter_to_disappear = 0;
    int counter_to_game_over = 0;
    int counter_to_help = 0;
    Bitmap cow = null;
    Bitmap devil = null;
    int disappear_blog_number = 4;
    float disappearing_x;
    float disappearing_y;
    boolean dont_add_more_points = false;
    Food downspike;
    Bitmap downspikeimg = null;
    boolean draw_blob_text = false;
    int draw_finger_counter = 0;
    boolean draw_shashka_text = false;
    float draw_shashka_text_x = -10.0f;
    float draw_shashka_text_y = -10.0f;
    boolean draw_star_text = false;
    MediaPlayer eat_sound;
    Food extraspike;
    Bitmap finger1 = null;
    Bitmap finger2 = null;
    boolean flg_counter = false;
    boolean flg_pause = false;
    ArrayList<Food> food;
    boolean game_over = false;
    MediaPlayer gameover_sound;
    int height;
    boolean help2 = true;
    int help_number = 1;
    int high_score = 0;
    MediaPlayer hit;
    Bitmap horizontol_obs_image = null;
    Bitmap horizontol_obs_image_spikes = null;
    Food hspike;
    Bitmap hspikeimg = null;
    boolean if_object_touched = false;
    Bitmap imgbl1 = null;
    Bitmap imgbl11 = null;
    Bitmap imgbl2 = null;
    Bitmap imgbl22 = null;
    Bitmap imgbl3 = null;
    Bitmap imgbl33 = null;
    Food leftspike;
    Bitmap leftspikeimg = null;
    int level = 1;
    MediaPlayer level_complete_sound;
    Bitmap longbarr = null;
    long miliseconds;
    int minutes;
    String minutes_to_display;
    boolean next_level = false;
    TrainingEaters objTouched = null;
    Food obs1 = null;
    Food obs2 = null;
    Food obs3 = null;
    Food obs4 = null;
    Food obs5 = null;
    ArrayList<Food> obstacles;
    Boolean one_or_two_blobs = false;
    Bitmap org1;
    Bitmap org2;
    Bitmap org3;
    Bitmap org4;
    Bitmap org5;
    Bitmap org6;
    Bitmap org7;
    Bitmap org8;
    boolean path_crossing_from_blob = false;
    Bitmap pause_btn = null;
    long pause_start_time = 0;
    long pause_time;
    Bitmap pizza = null;
    boolean play_music = Settings.music;
    boolean play_sound = Settings.sounds;
    TrainingEaters red = null;
    Bitmap red1;
    Bitmap red2;
    Bitmap red3;
    Bitmap red4;
    Bitmap red5;
    Bitmap red6;
    Bitmap red7;
    Bitmap red8;
    Bitmap restart_btn = null;
    Food rightspike;
    Bitmap rightspikeimg = null;
    GameThread runGame;
    int seconds;
    String seconds_to_display;
    Paint shashkaPaint = new Paint();
    Paint shashkaPaint2 = new Paint();
    Paint shashkaPaint3 = new Paint();
    boolean show_disappear = true;
    boolean show_fps = Settings.fps;
    boolean show_help_text = false;
    Bitmap star = null;
    Food star1 = null;
    Food star2 = null;
    Food star3 = null;
    int star_ratings = 0;
    boolean start_disappearing_blobs = false;
    float starting_point_x;
    float starting_point_y;
    long starts = System.currentTimeMillis();
    Bitmap str2 = null;
    int text_size = 8;
    TrainingEaters third = null;
    Food upspike;
    Bitmap upspikeimg = null;
    Bitmap verticle_obs_image = null;
    Bitmap verticle_obs_image_spike = null;
    Food vspike;
    Bitmap vspikeimg = null;
    MediaPlayer wall_hit;
    Bitmap which_level = null;
    int width;
    TrainingEaters yellow = null;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        Intent i = new Intent(getApplicationContext(), MainMenu.class);
        finish();
        startActivity(i);
        return true;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView(new CustomView(this));
        this.bg_sound = MediaPlayer.create(getBaseContext(), (int) R.raw.bg_sound);
        this.eat_sound = MediaPlayer.create(getBaseContext(), (int) R.raw.hop);
        this.hit = MediaPlayer.create(getBaseContext(), (int) R.raw.hits);
        this.wall_hit = MediaPlayer.create(getBaseContext(), (int) R.raw.whit);
        this.level_complete_sound = MediaPlayer.create(getBaseContext(), (int) R.raw.lvl);
        this.collision = MediaPlayer.create(getBaseContext(), (int) R.raw.coll);
        this.gameover_sound = MediaPlayer.create(getBaseContext(), (int) R.raw.gamover);
        this.acheivment_sound = MediaPlayer.create(getBaseContext(), (int) R.raw.ach);
        Display display = getWindowManager().getDefaultDisplay();
        this.width = display.getWidth();
        this.height = display.getHeight();
        if (this.width <= 0 || this.height <= 0) {
            this.width = Settings.width;
            this.height = Settings.height;
        }
        this.blackP.setTextSize(15.0f);
        this.blackP.setColor(-16777216);
        this.blackP.setTextAlign(Paint.Align.CENTER);
        this.blackP.setFakeBoldText(true);
        this.blackP.setAntiAlias(true);
        this.blackP.setStyle(Paint.Style.FILL);
        this.blackP.setTypeface(Typeface.SANS_SERIF);
        this.shashkaPaint.setTextSize(9.0f);
        this.shashkaPaint.setColor(-1);
        this.shashkaPaint.setFakeBoldText(true);
        this.shashkaPaint.setTextAlign(Paint.Align.LEFT);
        this.shashkaPaint.setAntiAlias(true);
        this.shashkaPaint.setStyle(Paint.Style.FILL);
        this.shashkaPaint.setTypeface(Typeface.SANS_SERIF);
        this.shashkaPaint3.setTextSize(15.0f);
        this.shashkaPaint3.setColor(-16777216);
        this.shashkaPaint3.setFakeBoldText(true);
        this.shashkaPaint3.setTextAlign(Paint.Align.LEFT);
        this.shashkaPaint3.setAntiAlias(true);
        this.shashkaPaint3.setStyle(Paint.Style.FILL);
        this.shashkaPaint3.setTypeface(Typeface.SANS_SERIF);
        this.shashkaPaint2.setTextSize(9.0f);
        this.shashkaPaint2.setColor(-16777216);
        this.shashkaPaint2.setFakeBoldText(true);
        this.shashkaPaint2.setTextAlign(Paint.Align.LEFT);
        this.shashkaPaint2.setAntiAlias(true);
        this.shashkaPaint2.setStyle(Paint.Style.FILL);
        this.shashkaPaint2.setTypeface(Typeface.SANS_SERIF);
        this.bg_image = BitmapFactory.decodeResource(getResources(), R.drawable.traning_bg);
        this.cmt_box = BitmapFactory.decodeResource(getResources(), R.drawable.commentbox);
        this.finger1 = BitmapFactory.decodeResource(getResources(), R.drawable.finger1);
        this.finger2 = BitmapFactory.decodeResource(getResources(), R.drawable.finger2);
        this.bg_image = Bitmap.createScaledBitmap(this.bg_image, this.width, this.height, true);
        this.star = BitmapFactory.decodeResource(getResources(), R.drawable.star);
        this.str2 = BitmapFactory.decodeResource(getResources(), R.drawable.str2);
        this.imgbl1 = BitmapFactory.decodeResource(getResources(), R.drawable.blob1);
        this.imgbl11 = BitmapFactory.decodeResource(getResources(), R.drawable.blob11);
        this.imgbl2 = BitmapFactory.decodeResource(getResources(), R.drawable.blob2);
        this.imgbl22 = BitmapFactory.decodeResource(getResources(), R.drawable.blob22);
        this.imgbl3 = BitmapFactory.decodeResource(getResources(), R.drawable.blob3);
        this.imgbl33 = BitmapFactory.decodeResource(getResources(), R.drawable.blob33);
        this.verticle_obs_image = BitmapFactory.decodeResource(getResources(), R.drawable.vbrick);
        this.horizontol_obs_image = BitmapFactory.decodeResource(getResources(), R.drawable.hbrick);
        this.hspikeimg = BitmapFactory.decodeResource(getResources(), R.drawable.hspikes);
        this.vspikeimg = BitmapFactory.decodeResource(getResources(), R.drawable.vspikes);
        this.leftspikeimg = BitmapFactory.decodeResource(getResources(), R.drawable.leftspikee);
        this.rightspikeimg = BitmapFactory.decodeResource(getResources(), R.drawable.rightspikee);
        this.upspikeimg = BitmapFactory.decodeResource(getResources(), R.drawable.upspikee);
        this.downspikeimg = BitmapFactory.decodeResource(getResources(), R.drawable.downspikee);
        this.longbarr = BitmapFactory.decodeResource(getResources(), R.drawable.longbar);
        this.org1 = BitmapFactory.decodeResource(getResources(), R.drawable.org1);
        this.org2 = BitmapFactory.decodeResource(getResources(), R.drawable.org2);
        this.org3 = BitmapFactory.decodeResource(getResources(), R.drawable.org3);
        this.org4 = BitmapFactory.decodeResource(getResources(), R.drawable.org4);
        this.org5 = BitmapFactory.decodeResource(getResources(), R.drawable.org5);
        this.org6 = BitmapFactory.decodeResource(getResources(), R.drawable.org6);
        this.org7 = BitmapFactory.decodeResource(getResources(), R.drawable.org7);
        this.org8 = BitmapFactory.decodeResource(getResources(), R.drawable.org8);
        this.red1 = BitmapFactory.decodeResource(getResources(), R.drawable.red1);
        this.red2 = BitmapFactory.decodeResource(getResources(), R.drawable.red2);
        this.red3 = BitmapFactory.decodeResource(getResources(), R.drawable.red3);
        this.red4 = BitmapFactory.decodeResource(getResources(), R.drawable.red4);
        this.red5 = BitmapFactory.decodeResource(getResources(), R.drawable.red5);
        this.red6 = BitmapFactory.decodeResource(getResources(), R.drawable.red6);
        this.red7 = BitmapFactory.decodeResource(getResources(), R.drawable.red7);
        this.red8 = BitmapFactory.decodeResource(getResources(), R.drawable.red8);
        this.blu1 = BitmapFactory.decodeResource(getResources(), R.drawable.blu1);
        this.blu2 = BitmapFactory.decodeResource(getResources(), R.drawable.blu2);
        this.blu3 = BitmapFactory.decodeResource(getResources(), R.drawable.blu3);
        this.blu4 = BitmapFactory.decodeResource(getResources(), R.drawable.blu4);
        this.blu5 = BitmapFactory.decodeResource(getResources(), R.drawable.blu5);
        this.blu6 = BitmapFactory.decodeResource(getResources(), R.drawable.blu6);
        this.blu7 = BitmapFactory.decodeResource(getResources(), R.drawable.blu7);
        this.blu8 = BitmapFactory.decodeResource(getResources(), R.drawable.blu8);
        this.pause_btn = BitmapFactory.decodeResource(getResources(), R.drawable.skip);
        this.restart_btn = BitmapFactory.decodeResource(getResources(), R.drawable.restart);
        this.bee = BitmapFactory.decodeResource(getResources(), R.drawable.bee);
        this.yellow = new TrainingEaters(this.bee, 40, 70, "yellow", true);
        this.yellow.direction_y = true;
        this.cow = BitmapFactory.decodeResource(getResources(), R.drawable.cow);
        this.devil = BitmapFactory.decodeResource(getResources(), R.drawable.devil2);
        this.red = new TrainingEaters(this.cow, this.width - 40, this.height - 80, "red", false);
        this.yellow.partner = this.red;
        this.red.partner = this.yellow;
        this.red.direction_y = false;
        this.third = new TrainingEaters(this.devil, this.width - 40, 60, "third", false);
        this.third.partner = this.red;
        this.third.direction_y = true;
        this.yellow.Xf = this.red.x;
        this.yellow.Yf = this.red.y;
        this.red.Xf = this.yellow.x;
        this.red.Yf = this.yellow.y;
        this.star1 = new Food(this.star, (float) ((this.width / 2) - 40), 100.0f, this.str2, 1);
        this.star2 = new Food(this.star, (float) ((this.width / 2) - 40), 200.0f, this.str2, 2);
        this.star3 = new Food(this.star, (float) ((this.width / 2) - 40), 300.0f, this.str2, 3);
        this.Hooppers = new ArrayList<>();
        this.Hooppers.add(this.yellow);
        if (this.level == 1) {
            this.food = new ArrayList<>();
            this.food.add(this.star1);
            this.food.add(this.star2);
            this.Spikes = new ArrayList<>();
            this.blobs = new ArrayList<>();
            this.blob1 = new Food(this.imgbl3, this.star1.x + 10.0f, this.star2.y + ((float) this.star2.bitmap.getHeight()) + 50.0f, this.imgbl33, 3);
            this.blobs.add(this.blob1);
            this.obstacles = new ArrayList<>();
            this.star3.y = (float) (this.height - 200);
            this.food.add(this.star3);
            this.which_level = BitmapFactory.decodeResource(getResources(), R.drawable.levell1);
        }
        if (this.blobs.size() >= 2) {
            this.one_or_two_blobs = true;
        } else {
            this.one_or_two_blobs = false;
        }
        TrainingEaters.next_level_ok_stop_moving = false;
    }

    class CustomView extends SurfaceView implements SurfaceHolder.Callback {
        public CustomView(Context context) {
            super(context);
            getHolder().addCallback(this);
        }

        public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        }

        public void surfaceCreated(SurfaceHolder arg0) {
            Training.this.runGame = new GameThread(getHolder(), this);
            Training.this.runGame.start();
            if (Training.this.play_music && Training.this.bg_sound != null) {
                Training.this.bg_sound.setLooping(true);
                Training.this.bg_sound.start();
            }
        }

        public void surfaceDestroyed(SurfaceHolder arg0) {
            boolean retry = true;
            Training.this.runGame.setRunning(false);
            if (Training.this.bg_sound != null) {
                Training.this.bg_sound.setLooping(false);
                Training.this.bg_sound.stop();
            }
            while (retry) {
                try {
                    Training.this.runGame.join();
                    retry = false;
                } catch (InterruptedException e) {
                    Log.e("Error", "at line 1570 Exception is " + e);
                }
            }
        }

        public void onDraw(Canvas cnv) {
            cnv.drawBitmap(Training.this.bg_image, 0.0f, 0.0f, (Paint) null);
            cnv.drawBitmap(Training.this.cmt_box, 10.0f, (float) (Training.this.height - Training.this.cmt_box.getHeight()), (Paint) null);
            cnv.drawBitmap(Training.this.restart_btn, (float) (Training.this.width - Training.this.restart_btn.getWidth()), (float) (0.07d * ((double) Training.this.height)), (Paint) null);
            cnv.drawBitmap(Training.this.pause_btn, 0.0f, (float) (0.07d * ((double) Training.this.height)), (Paint) null);
            if (Training.this.help_number == 1 && Training.this.Hooppers.size() < 2) {
                cnv.drawText("Drag the Hooper by touching and giving", 20.0f, (float) (Training.this.height - 60), Training.this.shashkaPaint3);
                cnv.drawText("it direction.", 20.0f, (float) (Training.this.height - 48), Training.this.shashkaPaint3);
                cnv.drawText("Draging you fingure on the screen will give", 20.0f, (float) (Training.this.height - 36), Training.this.shashkaPaint3);
                cnv.drawText("it path.", 20.0f, (float) (Training.this.height - 24), Training.this.shashkaPaint3);
            } else if (Training.this.help_number == 2 && Training.this.Hooppers.size() < 2) {
                cnv.drawText("Well Done. Now direct it towards the", 20.0f, (float) (Training.this.height - 60), Training.this.shashkaPaint3);
                cnv.drawText("star to pick.", 20.0f, (float) (Training.this.height - 48), Training.this.shashkaPaint3);
                cnv.drawText("The more stars you pick more ponits", 20.0f, (float) (Training.this.height - 36), Training.this.shashkaPaint3);
                cnv.drawText("you will get.", 20.0f, (float) (Training.this.height - 24), Training.this.shashkaPaint3);
            } else if (Training.this.help_number == 3) {
                cnv.drawText("One Hooper cannot pick.", 20.0f, (float) (Training.this.height - 60), Training.this.shashkaPaint3);
                cnv.drawText("Two Hoopers must meet to pick.", 20.0f, (float) (Training.this.height - 48), Training.this.shashkaPaint3);
                cnv.drawText("Drag Hooper towards other.", 20.0f, (float) (Training.this.height - 36), Training.this.shashkaPaint3);
            } else if (Training.this.help_number == 4) {
                cnv.drawText("Meet their path in a way such that", 20.0f, (float) (Training.this.height - 60), Training.this.shashkaPaint3);
                cnv.drawText("stars comes in their path.", 20.0f, (float) (Training.this.height - 48), Training.this.shashkaPaint3);
            } else if (Training.this.help_number == 5) {
                cnv.drawText("Path must pass through the blob to pick ", 20.0f, (float) (Training.this.height - 60), Training.this.shashkaPaint3);
                cnv.drawText("the stars. ", 20.0f, (float) (Training.this.height - 48), Training.this.shashkaPaint3);
                cnv.drawText("Path must also be met with both Hoopers.", 20.0f, (float) (Training.this.height - 36), Training.this.shashkaPaint3);
            } else if (Training.this.help_number == 6) {
                cnv.drawText("If the hoopers collide game will over. ", 20.0f, (float) (Training.this.height - 60), Training.this.shashkaPaint3);
                cnv.drawText("Retry or skip training. ", 20.0f, (float) (Training.this.height - 48), Training.this.shashkaPaint3);
            } else if (Training.this.help_number == 7) {
                cnv.drawText("One star must be picked to eat blob.  ", 20.0f, (float) (Training.this.height - 60), Training.this.shashkaPaint3);
            } else if (Training.this.help_number == 8) {
                cnv.drawText("Well done.", 20.0f, (float) (Training.this.height - 60), Training.this.shashkaPaint3);
                cnv.drawText("You can now start the game or restart", 20.0f, (float) (Training.this.height - 48), Training.this.shashkaPaint3);
                cnv.drawText("training.", 20.0f, (float) (Training.this.height - 36), Training.this.shashkaPaint3);
            }
            if (Training.this.start_disappearing_blobs && Training.this.show_disappear) {
                if (Training.this.disappear_blog_number == 1) {
                    if (Training.this.counter_to_disappear <= 15) {
                        cnv.drawBitmap(Training.this.red1, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 15 && Training.this.counter_to_disappear <= 30) {
                        cnv.drawBitmap(Training.this.red2, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 30 && Training.this.counter_to_disappear <= 45) {
                        cnv.drawBitmap(Training.this.red3, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 45 && Training.this.counter_to_disappear <= 60) {
                        cnv.drawBitmap(Training.this.red4, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 60 && Training.this.counter_to_disappear <= 75) {
                        cnv.drawBitmap(Training.this.red5, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 75 && Training.this.counter_to_disappear <= 90) {
                        cnv.drawBitmap(Training.this.red6, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 90 && Training.this.counter_to_disappear <= 100) {
                        cnv.drawBitmap(Training.this.red7, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 100 && Training.this.counter_to_disappear <= 110) {
                        cnv.drawBitmap(Training.this.red8, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    }
                } else if (Training.this.disappear_blog_number == 2) {
                    if (Training.this.counter_to_disappear <= 15) {
                        cnv.drawBitmap(Training.this.blu1, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 15 && Training.this.counter_to_disappear <= 30) {
                        cnv.drawBitmap(Training.this.blu2, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 30 && Training.this.counter_to_disappear <= 45) {
                        cnv.drawBitmap(Training.this.blu3, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 45 && Training.this.counter_to_disappear <= 60) {
                        cnv.drawBitmap(Training.this.blu4, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 60 && Training.this.counter_to_disappear <= 75) {
                        cnv.drawBitmap(Training.this.blu5, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 75 && Training.this.counter_to_disappear <= 90) {
                        cnv.drawBitmap(Training.this.blu6, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 90 && Training.this.counter_to_disappear <= 100) {
                        cnv.drawBitmap(Training.this.blu7, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 100 && Training.this.counter_to_disappear <= 110) {
                        cnv.drawBitmap(Training.this.blu8, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    }
                } else if (Training.this.disappear_blog_number == 3) {
                    if (Training.this.counter_to_disappear <= 15) {
                        cnv.drawBitmap(Training.this.org1, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 15 && Training.this.counter_to_disappear <= 30) {
                        cnv.drawBitmap(Training.this.org2, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 30 && Training.this.counter_to_disappear <= 45) {
                        cnv.drawBitmap(Training.this.org3, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 45 && Training.this.counter_to_disappear <= 60) {
                        cnv.drawBitmap(Training.this.org4, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 60 && Training.this.counter_to_disappear <= 75) {
                        cnv.drawBitmap(Training.this.org5, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 75 && Training.this.counter_to_disappear <= 90) {
                        cnv.drawBitmap(Training.this.org6, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 90 && Training.this.counter_to_disappear <= 100) {
                        cnv.drawBitmap(Training.this.org7, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    } else if (Training.this.counter_to_disappear >= 100 && Training.this.counter_to_disappear <= 110) {
                        cnv.drawBitmap(Training.this.org8, Training.this.disappearing_x, Training.this.disappearing_y, (Paint) null);
                        Training.this.counter_to_disappear++;
                    }
                }
                if (Training.this.blobs.size() <= 0 && Training.this.counter_to_disappear >= 110) {
                    Training.this.next_level = true;
                    Training.this.disappearing_x = 0.0f;
                    Training.this.disappearing_y = 0.0f;
                    Training.this.counter_to_disappear = 0;
                    Training.this.show_disappear = false;
                }
            }
            if (Training.this.food.size() >= 1 && Training.this.help_number > 1) {
                for (int i = 0; i < Training.this.food.size(); i++) {
                    Training.this.food.get(i).draw(cnv);
                }
            }
            if (Training.this.Hooppers.size() >= 1) {
                for (int i2 = 0; i2 < Training.this.Hooppers.size(); i2++) {
                    Training.this.Hooppers.get(i2).draw(cnv);
                }
            }
            if (Training.this.blobs.size() >= 1 && Training.this.blobs != null && Training.this.help_number > 3) {
                for (int i3 = 0; i3 < Training.this.blobs.size(); i3++) {
                    Training.this.blobs.get(i3).draw(cnv);
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:120:0x036c, code lost:
            if (r10.this$0.yellow.y > (((float) r10.this$0.food.get(r0).bitmap.getHeight()) + r10.this$0.food.get(r0).y)) goto L_0x036e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:140:0x0486, code lost:
            if (r10.this$0.yellow.y > (((float) r10.this$0.food.get(r0).bitmap.getHeight()) + r10.this$0.food.get(r0).y)) goto L_0x0488;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:160:0x058e, code lost:
            if (r10.this$0.yellow.y > (((float) r10.this$0.food.get(r0).bitmap.getHeight()) + r10.this$0.food.get(r0).y)) goto L_0x0590;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void UpdatePhysics() {
            /*
                r10 = this;
                r9 = 8
                r8 = 1109393408(0x42200000, float:40.0)
                r7 = 1092616192(0x41200000, float:10.0)
                r6 = 1
                r5 = 0
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                boolean r2 = r2.flg_pause
                if (r2 != 0) goto L_0x018a
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                int r2 = r2.size()
                if (r2 > 0) goto L_0x0029
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                boolean r2 = r2.next_level
                if (r2 == 0) goto L_0x0029
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.flg_pause = r6
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.Training$GameThread r2 = r2.runGame
                r2.setRunning(r5)
            L_0x0029:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r2 = r2.x
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r3 = r3.red
                float r3 = r3.x
                r4 = 1103626240(0x41c80000, float:25.0)
                float r3 = r3 - r4
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x00af
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r2 = r2.x
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r3 = r3.red
                float r3 = r3.x
                r4 = 1103626240(0x41c80000, float:25.0)
                float r3 = r3 + r4
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 >= 0) goto L_0x00af
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r2 = r2.y
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r3 = r3.red
                float r3 = r3.y
                r4 = 1103626240(0x41c80000, float:25.0)
                float r3 = r3 - r4
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 <= 0) goto L_0x00af
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r2 = r2.y
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r3 = r3.red
                float r3 = r3.y
                r4 = 1103626240(0x41c80000, float:25.0)
                float r3 = r3 + r4
                int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                if (r2 >= 0) goto L_0x00af
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                boolean r2 = r2.game_over
                if (r2 != 0) goto L_0x00af
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                boolean r2 = r2.play_sound
                if (r2 == 0) goto L_0x008e
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                android.media.MediaPlayer r2 = r2.collision
                if (r2 == 0) goto L_0x008e
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                android.media.MediaPlayer r2 = r2.collision
                r2.start()
            L_0x008e:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.game_over = r6
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r3 = 6
                r2.help_number = r3
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.TrainingEaters> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r5)
                com.tenapp.hoopersLite.TrainingEaters r2 = (com.tenapp.hoopersLite.TrainingEaters) r2
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                r2.clear()
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.TrainingEaters> r2 = r2.Hooppers
                r2.remove(r5)
                com.tenapp.hoopersLite.Training.path_met = r5
            L_0x00af:
                r0 = 0
            L_0x00b0:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.TrainingEaters> r2 = r2.Hooppers
                int r2 = r2.size()
                if (r0 < r2) goto L_0x018b
                boolean r2 = com.tenapp.hoopersLite.Training.path_met
                if (r2 == 0) goto L_0x00e1
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                boolean r2 = r2.path_crossing_from_blob
                if (r2 != 0) goto L_0x00e1
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                boolean r2 = r2.check_path_crossing_from_blobs
                if (r2 == 0) goto L_0x00e1
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                boolean r2 = r2.type
                if (r2 == 0) goto L_0x023f
                r1 = 0
            L_0x00d3:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                int r2 = r2.size()
                if (r1 < r2) goto L_0x019c
            L_0x00dd:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.check_path_crossing_from_blobs = r5
            L_0x00e1:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                int r2 = r2.counter_to_check_path_crosssing_from_all_blobs
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r3 = r3.blobs
                int r3 = r3.size()
                if (r2 != r3) goto L_0x02ec
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.path_crossing_from_blob = r6
            L_0x00f3:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                boolean r2 = r2.path_crossing_from_blob
                if (r2 != 0) goto L_0x010b
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                int r2 = r2.help_number
                r3 = 2
                if (r2 != r3) goto L_0x010b
                r0 = 0
            L_0x0101:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                int r2 = r2.size()
                if (r0 < r2) goto L_0x02f2
            L_0x010b:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                boolean r2 = r2.flg_counter
                if (r2 == 0) goto L_0x0119
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                int r3 = r2.count_help
                int r3 = r3 + 1
                r2.count_help = r3
            L_0x0119:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                boolean r2 = r2.path_crossing_from_blob
                if (r2 != 0) goto L_0x0139
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                int r2 = r2.help_number
                r3 = 4
                if (r2 != r3) goto L_0x0139
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                int r2 = r2.count_help
                r3 = 50
                if (r2 <= r3) goto L_0x0139
                r0 = 0
            L_0x012f:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                int r2 = r2.size()
                if (r0 < r2) goto L_0x040c
            L_0x0139:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                boolean r2 = r2.path_crossing_from_blob
                if (r2 == 0) goto L_0x014a
                r0 = 0
            L_0x0140:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                int r2 = r2.size()
                if (r0 < r2) goto L_0x0514
            L_0x014a:
                r0 = 0
            L_0x014b:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                int r2 = r2.size()
                if (r0 < r2) goto L_0x065e
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                int r2 = r2.size()
                if (r2 > 0) goto L_0x0161
                com.tenapp.hoopersLite.TrainingEaters.next_level_ok_stop_moving = r6
            L_0x0161:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                int r2 = r2.x_index
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r3 = r3.yellow
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r3 = r3.routes
                int r3 = r3.size()
                if (r2 < r3) goto L_0x018a
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                boolean r2 = r2.help2
                if (r2 == 0) goto L_0x018a
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                int r2 = r2.x_index
                if (r2 == 0) goto L_0x018a
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r3 = 2
                r2.help_number = r3
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.help2 = r5
            L_0x018a:
                return
            L_0x018b:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.TrainingEaters> r2 = r2.Hooppers
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.TrainingEaters r2 = (com.tenapp.hoopersLite.TrainingEaters) r2
                r2.update_physics()
                int r0 = r0 + 1
                goto L_0x00b0
            L_0x019c:
                r0 = 0
            L_0x019d:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                int r2 = r2.size()
                if (r0 < r2) goto L_0x01ad
                int r1 = r1 + 1
                goto L_0x00d3
            L_0x01ad:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posX
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                float r2 = r2 - r8
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x023b
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posX
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                float r2 = r2 + r8
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x023b
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posY
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                float r2 = r2 - r8
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x023b
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posY
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                float r2 = r2 + r8
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x023b
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                int r3 = r2.counter_to_check_path_crosssing_from_all_blobs
                int r3 = r3 + 1
                r2.counter_to_check_path_crosssing_from_all_blobs = r3
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                int r0 = r2.size()
            L_0x023b:
                int r0 = r0 + 1
                goto L_0x019d
            L_0x023f:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                boolean r2 = r2.type
                if (r2 == 0) goto L_0x00dd
                r1 = 0
            L_0x0248:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                int r2 = r2.size()
                if (r1 >= r2) goto L_0x00dd
                r0 = 0
            L_0x0253:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                int r2 = r2.size()
                if (r0 < r2) goto L_0x0262
            L_0x025f:
                int r1 = r1 + 1
                goto L_0x0248
            L_0x0262:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posX
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                float r2 = r2 - r8
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x02e8
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posX
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                float r2 = r2 + r8
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x02e8
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posY
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                float r2 = r2 - r8
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x02e8
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                java.util.ArrayList<com.tenapp.hoopersLite.RouteNates> r2 = r2.routes
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.RouteNates r2 = (com.tenapp.hoopersLite.RouteNates) r2
                float r3 = r2.posY
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r1)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                float r2 = r2 + r8
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x02e8
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                int r3 = r2.counter_to_check_path_crosssing_from_all_blobs
                int r3 = r3 + 1
                r2.counter_to_check_path_crosssing_from_all_blobs = r3
                goto L_0x025f
            L_0x02e8:
                int r0 = r0 + 1
                goto L_0x0253
            L_0x02ec:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.path_crossing_from_blob = r5
                goto L_0x00f3
            L_0x02f2:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x036e
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getWidth()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x036e
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x036e
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getHeight()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 <= 0) goto L_0x03ea
            L_0x036e:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0408
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getWidth()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0408
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0408
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getHeight()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0408
            L_0x03ea:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r3 = 3
                r2.help_number = r3
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.TrainingEaters> r2 = r2.Hooppers
                int r2 = r2.size()
                if (r2 != r6) goto L_0x0408
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.TrainingEaters> r2 = r2.Hooppers
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r3 = r3.red
                r2.add(r3)
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.flg_counter = r6
            L_0x0408:
                int r0 = r0 + 1
                goto L_0x0101
            L_0x040c:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0488
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getWidth()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0488
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0488
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getHeight()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 <= 0) goto L_0x050b
            L_0x0488:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0510
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getWidth()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0510
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0510
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getHeight()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0510
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                int r2 = r2.help_number
                r3 = 4
                if (r2 != r3) goto L_0x0510
            L_0x050b:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r3 = 5
                r2.help_number = r3
            L_0x0510:
                int r0 = r0 + 1
                goto L_0x012f
            L_0x0514:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0590
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getWidth()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0590
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0590
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getHeight()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 <= 0) goto L_0x060c
            L_0x0590:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x065a
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getWidth()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x065a
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x065a
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getHeight()
                float r2 = (float) r2
                float r2 = r2 + r4
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x065a
            L_0x060c:
                boolean r2 = com.tenapp.hoopersLite.Training.path_met
                if (r2 == 0) goto L_0x065a
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                boolean r2 = r2.play_sound
                if (r2 == 0) goto L_0x0623
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                android.media.MediaPlayer r2 = r2.eat_sound
                if (r2 == 0) goto L_0x0623
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                android.media.MediaPlayer r2 = r2.eat_sound
                r2.start()
            L_0x0623:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.draw_shashka_text = r6
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.draw_star_text = r6
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r3.draw_shashka_text_x = r2
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r3.draw_shashka_text_y = r2
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.text_size = r9
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                r2.remove(r0)
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.show_help_text = r5
            L_0x065a:
                int r0 = r0 + 1
                goto L_0x0140
            L_0x065e:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                float r2 = r2 - r7
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0781
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getWidth()
                float r2 = (float) r2
                float r2 = r2 + r4
                float r2 = r2 + r7
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0781
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                float r2 = r2 - r7
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x0781
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.yellow
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getHeight()
                float r2 = (float) r2
                float r2 = r2 + r4
                float r2 = r2 + r7
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x0781
                boolean r2 = com.tenapp.hoopersLite.Training.path_met
                if (r2 == 0) goto L_0x0781
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                int r2 = r2.size()
                r3 = 2
                if (r2 > r3) goto L_0x08a8
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                android.media.MediaPlayer r2 = r2.eat_sound
                if (r2 == 0) goto L_0x0700
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                boolean r2 = r2.play_sound
                if (r2 == 0) goto L_0x0700
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                android.media.MediaPlayer r2 = r2.eat_sound
                r2.start()
            L_0x0700:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.start_disappearing_blobs = r6
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                int r2 = r2.type
                r3.disappear_blog_number = r2
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r3.disappearing_x = r2
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r3.disappearing_y = r2
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.counter_to_disappear = r5
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.text_size = r9
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.draw_shashka_text = r6
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.draw_blob_text = r6
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r3.draw_shashka_text_x = r2
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r3.draw_shashka_text_y = r2
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                r2.remove(r0)
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.path_crossing_from_blob = r5
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.counter_to_check_path_crosssing_from_all_blobs = r5
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.check_path_crossing_from_blobs = r6
                int r2 = com.tenapp.hoopersLite.Training.score
                int r2 = r2 + 250
                com.tenapp.hoopersLite.Training.score = r2
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.help_number = r9
            L_0x0781:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                float r2 = r2 - r7
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x08a4
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.x
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getWidth()
                float r2 = (float) r2
                float r2 = r2 + r4
                float r2 = r2 + r7
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x08a4
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                float r2 = r2 - r7
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 < 0) goto L_0x08a4
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.TrainingEaters r2 = r2.red
                float r3 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r4 = r2.y
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                android.graphics.Bitmap r2 = r2.bitmap
                int r2 = r2.getHeight()
                float r2 = (float) r2
                float r2 = r2 + r4
                float r2 = r2 + r7
                int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
                if (r2 > 0) goto L_0x08a4
                boolean r2 = com.tenapp.hoopersLite.Training.path_met
                if (r2 == 0) goto L_0x08a4
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.food
                int r2 = r2.size()
                r3 = 2
                if (r2 > r3) goto L_0x08b3
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                android.media.MediaPlayer r2 = r2.eat_sound
                if (r2 == 0) goto L_0x0823
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                boolean r2 = r2.play_sound
                if (r2 == 0) goto L_0x0823
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                android.media.MediaPlayer r2 = r2.eat_sound
                r2.start()
            L_0x0823:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.start_disappearing_blobs = r6
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                int r2 = r2.type
                r3.disappear_blog_number = r2
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r3.disappearing_x = r2
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r3.disappearing_y = r2
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.counter_to_disappear = r5
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.text_size = r9
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.draw_shashka_text = r6
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.draw_blob_text = r6
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.x
                r3.draw_shashka_text_x = r2
                com.tenapp.hoopersLite.Training r3 = com.tenapp.hoopersLite.Training.this
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                java.lang.Object r2 = r2.get(r0)
                com.tenapp.hoopersLite.Food r2 = (com.tenapp.hoopersLite.Food) r2
                float r2 = r2.y
                r3.draw_shashka_text_y = r2
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                java.util.ArrayList<com.tenapp.hoopersLite.Food> r2 = r2.blobs
                r2.remove(r0)
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.help_number = r9
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.path_crossing_from_blob = r5
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.counter_to_check_path_crosssing_from_all_blobs = r5
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.check_path_crossing_from_blobs = r6
                int r2 = com.tenapp.hoopersLite.Training.score
                int r2 = r2 + 250
                com.tenapp.hoopersLite.Training.score = r2
            L_0x08a4:
                int r0 = r0 + 1
                goto L_0x014b
            L_0x08a8:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.path_crossing_from_blob = r5
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r3 = 7
                r2.help_number = r3
                goto L_0x0781
            L_0x08b3:
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r2.path_crossing_from_blob = r5
                com.tenapp.hoopersLite.Training r2 = com.tenapp.hoopersLite.Training.this
                r3 = 7
                r2.help_number = r3
                goto L_0x08a4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tenapp.hoopersLite.Training.CustomView.UpdatePhysics():void");
        }

        private TrainingEaters GetTouchedObject(float eventX, float eventY) {
            TrainingEaters objTouched = null;
            int i = 0;
            while (true) {
                if (i >= Training.this.Hooppers.size()) {
                    break;
                }
                float x = Training.this.Hooppers.get(i).x;
                float y = Training.this.Hooppers.get(i).y;
                Bitmap bitmap = Training.this.Hooppers.get(i).bitmap;
                if (eventX >= (x - ((float) (bitmap.getWidth() / 2))) - 30.0f && eventX <= ((float) (bitmap.getWidth() / 2)) + x + 30.0f) {
                    if (eventY >= (y - ((float) (bitmap.getHeight() / 2))) - 30.0f && eventY <= ((float) (bitmap.getHeight() / 2)) + y + 30.0f) {
                        objTouched = Training.this.Hooppers.get(i);
                        Training.this.Hooppers.get(i).i_m_touched = true;
                        break;
                    }
                    objTouched = null;
                }
                i++;
            }
            if (((Training.this.level >= 16 && Training.this.level <= 20) || Training.this.level == 22 || Training.this.level == 23 || Training.this.level == 24 || Training.this.level == 27) && eventX >= (Training.this.third.x - ((float) (Training.this.third.bitmap.getWidth() / 2))) - 30.0f && eventX <= Training.this.third.x + ((float) (Training.this.third.bitmap.getWidth() / 2)) + 30.0f && eventY >= (Training.this.third.y - ((float) (Training.this.third.bitmap.getHeight() / 2))) - 30.0f && eventY <= Training.this.third.y + ((float) (Training.this.third.bitmap.getHeight() / 2)) + 30.0f) {
                objTouched = Training.this.third;
                Training.this.third.i_m_touched = true;
            }
            if (objTouched != null && !objTouched.name.equalsIgnoreCase("third")) {
                objTouched.type = true;
                objTouched.partner.type = false;
                objTouched.stopped_by_obstacle = false;
                objTouched.increase_x = false;
                objTouched.increase_y = false;
                objTouched.decrease_x = false;
                objTouched.decrease_y = false;
                objTouched.not_on_borders = true;
                if (Training.this.if_object_touched) {
                    Training.this.if_object_touched = false;
                } else {
                    Training.this.if_object_touched = true;
                }
            } else if (objTouched != null && objTouched.name.equalsIgnoreCase("third")) {
                objTouched.stopped_by_obstacle = false;
                objTouched.increase_x = false;
                objTouched.increase_y = false;
                objTouched.decrease_x = false;
                objTouched.decrease_y = false;
                objTouched.not_on_borders = true;
            }
            return objTouched;
        }

        public boolean onTouchEvent(MotionEvent evnt) {
            float temp_y;
            if (evnt.getAction() == 0) {
                if (evnt.getX() <= 0.0f || evnt.getX() >= ((float) (Training.this.pause_btn.getWidth() + 0))) {
                    if (evnt.getX() > ((float) ((Training.this.width - Training.this.restart_btn.getWidth()) - 5)) && evnt.getX() < ((float) Training.this.width) && evnt.getY() > ((float) (((double) Training.this.height) * 0.07d)) && evnt.getY() < ((float) (((double) Training.this.height) * 0.07d)) + ((float) Training.this.restart_btn.getHeight())) {
                        Intent i = new Intent(Training.this.getApplicationContext(), Training.class);
                        Training.path_met = false;
                        Training.this.path_crossing_from_blob = false;
                        Training.this.finish();
                        Training.this.startActivity(i);
                    }
                } else if (evnt.getY() > ((float) (((double) Training.this.height) * 0.07d)) && evnt.getY() < ((float) (((double) Training.this.height) * 0.07d)) + ((float) Training.this.pause_btn.getHeight())) {
                    Intent i2 = new Intent(Training.this.getApplicationContext(), EatingObjectsActivity.class);
                    Bundle b = new Bundle();
                    b.putInt("level", 1);
                    b.putInt("score", 0);
                    i2.putExtras(b);
                    Training.this.finish();
                    Training.this.startActivity(i2);
                }
                Training.this.objTouched = GetTouchedObject(evnt.getX(), evnt.getY());
                if (Training.this.objTouched != null) {
                    Training.this.path_crossing_from_blob = false;
                    Training.this.check_path_crossing_from_blobs = false;
                    Training.this.counter_to_check_path_crosssing_from_all_blobs = 0;
                    if (Training.path_met && !Training.this.objTouched.name.equalsIgnoreCase("third")) {
                        Training.path_met = false;
                        Training.this.path_crossing_from_blob = false;
                        Training.this.objTouched.partner.routes.clear();
                        Training.this.objTouched.partner.routes.add(new RouteNates(Training.this.objTouched.x, Training.this.objTouched.y));
                        Training.this.objTouched.partner.increase_m = false;
                    }
                    Training.this.objTouched.mPath.reset();
                    Training.this.objTouched.routes = new ArrayList<>();
                    Training.this.objTouched.starting_x = evnt.getX();
                    Training.this.objTouched.starting_y = evnt.getY();
                    Training.this.objTouched.i_m_touched = true;
                    Training.this.objTouched.x_index = 0;
                    Training.this.objTouched.n = 0;
                    Training.this.objTouched.m = 1;
                    Training.this.objTouched.dont_draw_path = false;
                }
            }
            if (evnt.getAction() == 2 && Training.this.objTouched != null) {
                if (Training.this.objTouched.name.equalsIgnoreCase("third") && Training.this.check_for_third) {
                    Training.this.objTouched.routes.clear();
                    Training.this.check_for_third = false;
                }
                if (evnt.getY() < 80.0f) {
                    temp_y = 79.0f;
                } else {
                    temp_y = evnt.getY();
                }
                if (!Training.path_met || Training.this.objTouched.name.equalsIgnoreCase("third")) {
                    Training.this.objTouched.routes.add(new RouteNates(evnt.getX(), temp_y));
                    Training.this.objTouched.should_i_draw_path = true;
                }
            }
            if (evnt.getAction() == 1) {
                Training.this.check_for_third = true;
                if (Training.this.objTouched != null && Training.this.objTouched.routes.size() > 0) {
                    Training.this.if_object_touched = false;
                    Training.this.check_path_crossing_from_blobs = true;
                    Training.this.objTouched.routes = DouglasPeuckerReduction(Training.this.objTouched.routes, Double.valueOf(2.0d));
                    Training.this.objTouched.Xf = Training.this.objTouched.routes.get(0).posX;
                    Training.this.objTouched.Yf = Training.this.objTouched.routes.get(0).posY;
                    Training.this.objTouched.flgdelta = true;
                    Training.this.objTouched.ending_x = evnt.getX();
                    Training.this.objTouched.ending_y = evnt.getY();
                    if (Training.this.objTouched.ending_x > Training.this.objTouched.partner.x - 50.0f && Training.this.objTouched.ending_x < Training.this.objTouched.partner.x + 50.0f && Training.this.objTouched.ending_y > Training.this.objTouched.partner.y - 50.0f && Training.this.objTouched.ending_y < Training.this.objTouched.partner.y + 50.0f) {
                        Training.path_met = true;
                        if (Training.this.help_number == 3) {
                            Training.this.help_number = 4;
                        }
                        Training.this.objTouched.routes.add(new RouteNates(Training.this.objTouched.partner.x, Training.this.objTouched.partner.y));
                        Training.this.objTouched.partner.Xf = Training.this.objTouched.routes.get(Training.this.objTouched.routes.size() - 1).posX;
                        Training.this.objTouched.partner.Yf = Training.this.objTouched.routes.get(Training.this.objTouched.routes.size() - 1).posY;
                        evnt.setLocation(0.0f, 0.0f);
                        Training.this.objTouched.should_i_draw_path = true;
                        Training.this.objTouched.partner.m = 1;
                        Training.this.objTouched.partner.x_index = Training.this.objTouched.routes.size() - 1;
                    }
                    Training.this.objTouched = null;
                }
            }
            return true;
        }

        /* Debug info: failed to restart local var, previous not found, register: 8 */
        public ArrayList<RouteNates> DouglasPeuckerReduction(ArrayList<RouteNates> Points, Double Tolerance) {
            if (Points == null || Points.size() < 3) {
                return Points;
            }
            int lastPoint = Points.size() - 1;
            List<Integer> pointIndexsToKeep = new ArrayList<>();
            pointIndexsToKeep.add(0);
            pointIndexsToKeep.add(Integer.valueOf(lastPoint));
            DouglasPeuckerReduction(Points, 0, lastPoint, Tolerance, pointIndexsToKeep);
            List<RouteNates> returnPoints = new ArrayList<>();
            Collections.sort(pointIndexsToKeep);
            for (Integer intValue : pointIndexsToKeep) {
                returnPoints.add(Points.get(intValue.intValue()));
            }
            return (ArrayList) returnPoints;
        }

        private void DouglasPeuckerReduction(List<RouteNates> points, int firstPoint, int lastPoint, Double tolerance, List<Integer> pointIndexsToKeep) {
            Double maxDistance = Double.valueOf(0.0d);
            int indexFarthest = 0;
            for (int index = firstPoint; index < lastPoint; index++) {
                Double distance = PerpendicularDistance(points.get(firstPoint), points.get(lastPoint), points.get(index));
                if (distance.doubleValue() > maxDistance.doubleValue()) {
                    maxDistance = distance;
                    indexFarthest = index;
                }
            }
            if (maxDistance.doubleValue() > tolerance.doubleValue() && indexFarthest != 0) {
                pointIndexsToKeep.add(Integer.valueOf(indexFarthest));
                DouglasPeuckerReduction(points, firstPoint, indexFarthest, tolerance, pointIndexsToKeep);
                DouglasPeuckerReduction(points, indexFarthest, lastPoint, tolerance, pointIndexsToKeep);
            }
        }

        public Double PerpendicularDistance(RouteNates Point1, RouteNates Point2, RouteNates Point) {
            return Double.valueOf((Double.valueOf(Math.abs(0.5d * ((double) ((((((Point1.posX * Point2.posY) + (Point2.posX * Point.posY)) + (Point.posX * Point1.posY)) - (Point2.posX * Point1.posY)) - (Point.posX * Point2.posY)) - (Point1.posX * Point.posY))))).doubleValue() / Double.valueOf(Math.sqrt(Math.pow((double) (Point1.posX - Point2.posX), 2.0d) + Math.pow((double) (Point1.posY - Point2.posY), 2.0d))).doubleValue()) * 2.0d);
        }
    }

    class GameThread extends Thread {
        private CustomView _panel;
        private SurfaceHolder _surfaceHolder;
        int fps = 25;
        float interpolation = 0.0f;
        int loops;
        int max_fps_skip = 5;
        long next_tick = System.currentTimeMillis();
        boolean run = true;
        int skip_ticks = (1000 / this.fps);

        public GameThread(SurfaceHolder surfaceHolder, CustomView panel) {
            this._surfaceHolder = surfaceHolder;
            this._panel = panel;
        }

        public void run() {
            while (this.run) {
                Canvas c = null;
                try {
                    c = this._surfaceHolder.lockCanvas(null);
                    this.loops = 0;
                    synchronized (this._surfaceHolder) {
                        while (System.currentTimeMillis() > this.next_tick && this.loops < this.max_fps_skip) {
                            this._panel.UpdatePhysics();
                            this.next_tick += (long) this.skip_ticks;
                            this.loops++;
                        }
                        this._panel.onDraw(c);
                    }
                    if (c != null) {
                        this._surfaceHolder.unlockCanvasAndPost(c);
                    }
                } catch (Exception e) {
                    try {
                        Log.e("Error", "at line 2537 Exception is " + e);
                    } finally {
                        if (c != null) {
                            this._surfaceHolder.unlockCanvasAndPost(c);
                        }
                    }
                }
            }
        }

        public void setRunning(boolean mrun) {
            this.run = mrun;
        }
    }
}
