package com.tenapp.hoopersLite;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class Food {
    public Bitmap bitmap;
    public Bitmap bitmap2;
    int counter = 0;
    boolean is_eaten = false;
    String my_eater = null;
    int type;
    float x;
    float y;

    public Food(Bitmap b, float xx, float yy, Bitmap b2) {
        this.bitmap = b;
        this.x = xx;
        this.y = yy;
        this.bitmap2 = b2;
    }

    public Food(Bitmap b, float xx, float yy, Bitmap b2, int Type) {
        this.bitmap = b;
        this.x = xx;
        this.y = yy;
        this.bitmap2 = b2;
        this.type = Type;
    }

    public void draw(Canvas c) {
        if (this.is_eaten) {
            return;
        }
        if (this.bitmap2 == null) {
            c.drawBitmap(this.bitmap, this.x, this.y, (Paint) null);
        } else if (this.counter <= 50) {
            c.drawBitmap(this.bitmap, this.x, this.y, (Paint) null);
            this.counter++;
        } else if (this.counter >= 50 && this.counter <= 100) {
            c.drawBitmap(this.bitmap2, this.x, this.y, (Paint) null);
            this.counter++;
        } else if (this.counter >= 100) {
            this.counter = 0;
        }
    }

    public void who_ate_me(String name) {
        this.my_eater = name;
        this.is_eaten = true;
    }
}
