package com.tenapp.hoopersLite;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import java.util.List;

public class Initializer extends Activity {
    static final String gameID = "338192";
    static final String gameKey = "2qbqQOyRgHjr1RjviOphg";
    static final String gameName = "Hoopers";
    static final String gameSecret = "ghweVpy1bFNm8Zq1wBL4kLjm6LARtBZXasnIwaLSA";

    public void onCreate(Bundle v) {
        super.onCreate(v);
        List<ActivityManager.RunningAppProcessInfo> activityes = ((ActivityManager) getSystemService("activity")).getRunningAppProcesses();
        for (int i = 0; i < activityes.size(); i++) {
            if (!activityes.get(i).processName.equals("com.tenapp.hoopersLite")) {
                Process.killProcess(activityes.get(i).pid);
            }
        }
        try {
            OpenFeint.initialize(getApplicationContext(), new OpenFeintSettings(gameName, gameKey, gameSecret, gameID), new OpenFeintDelegate() {
            });
        } catch (Exception e) {
            Log.d("Error", "In OF Initilizer " + e);
        }
        Intent i2 = new Intent(this, MainMenu.class);
        finish();
        startActivity(i2);
    }
}
