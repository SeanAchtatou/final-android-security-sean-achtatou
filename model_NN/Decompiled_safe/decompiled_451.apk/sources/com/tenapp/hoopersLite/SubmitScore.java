package com.tenapp.hoopersLite;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import com.openfeint.api.ui.Dashboard;

public class SubmitScore extends Activity {
    static final String gameID = "338192";
    static final String gameKey = "2qbqQOyRgHjr1RjviOphg";
    static final String gameName = "Hoopers";
    static final String gameSecret = "ghweVpy1bFNm8Zq1wBL4kLjm6LARtBZXasnIwaLSA";

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        Intent start = new Intent(this, MainMenu.class);
        finish();
        startActivity(start);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.submitscore);
        OpenFeint.initialize(this, new OpenFeintSettings(gameName, gameKey, gameSecret, gameID), new OpenFeintDelegate() {
        });
        new Score((long) getIntent().getExtras().getInt("score"), null).submitTo(new Leaderboard("859606"), new Score.SubmitToCB() {
            public void onSuccess(boolean newHighScore) {
                SubmitScore.this.setResult(-1);
            }

            public void onFailure(String exceptionMessage) {
                SubmitScore.this.setResult(0);
            }
        });
        Dashboard.openLeaderboard("859606");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
