package com.openfeint.internal.ui;

public abstract class WebViewCacheCallback {
    public abstract void failLoaded();

    public abstract void pathLoaded(String str);

    public void onTrackingNeeded() {
    }
}
