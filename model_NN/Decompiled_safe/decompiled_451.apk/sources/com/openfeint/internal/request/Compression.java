package com.openfeint.internal.request;

import com.openfeint.api.OpenFeintSettings;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

public class Compression {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$openfeint$internal$request$Compression$CompressionMethod;
    private static final byte[] MagicHeader = "OFZLHDR0".getBytes();
    private static String TAG = "Compression";

    private enum CompressionMethod {
        Default,
        Uncompressed,
        LegacyHeaderless
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$openfeint$internal$request$Compression$CompressionMethod() {
        int[] iArr = $SWITCH_TABLE$com$openfeint$internal$request$Compression$CompressionMethod;
        if (iArr == null) {
            iArr = new int[CompressionMethod.values().length];
            try {
                iArr[CompressionMethod.Default.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[CompressionMethod.LegacyHeaderless.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[CompressionMethod.Uncompressed.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$openfeint$internal$request$Compression$CompressionMethod = iArr;
        }
        return iArr;
    }

    public static byte[] compress(byte[] uncompressedData) {
        byte[] uploadData = uncompressedData;
        try {
            switch ($SWITCH_TABLE$com$openfeint$internal$request$Compression$CompressionMethod()[compressionMethod().ordinal()]) {
                case 1:
                    byte[] tenativeData = _compress(uncompressedData);
                    byte[] uncompressedSize = integerToLittleEndianByteArray(uncompressedData.length);
                    int compressedLength = tenativeData.length + MagicHeader.length + uncompressedSize.length;
                    if (compressedLength >= uncompressedData.length) {
                        OpenFeintInternal.log(TAG, "Using Default strategy: compression declined");
                        break;
                    } else {
                        uploadData = new byte[compressedLength];
                        System.arraycopy(MagicHeader, 0, uploadData, 0, MagicHeader.length);
                        System.arraycopy(uncompressedSize, 0, uploadData, MagicHeader.length, uncompressedSize.length);
                        System.arraycopy(tenativeData, 0, uploadData, MagicHeader.length + 4, tenativeData.length);
                        OpenFeintInternal.log(TAG, String.format("Using Default strategy: orig %d bytes, compressed %d bytes (%.2f%% of original size)", Integer.valueOf(uncompressedData.length), Integer.valueOf(compressedLength), Float.valueOf((((float) compressedLength) / ((float) uncompressedData.length)) * 100.0f)));
                        break;
                    }
                case 2:
                default:
                    OpenFeintInternal.log(TAG, "Using Uncompressed strategy");
                    break;
                case 3:
                    uploadData = _compress(uncompressedData);
                    OpenFeintInternal.log(TAG, String.format("Using Default strategy: orig %d bytes, compressed %d bytes (%.2f%% of original size)", Integer.valueOf(uncompressedData.length), Integer.valueOf(uploadData.length), Float.valueOf((((float) uploadData.length) / ((float) uncompressedData.length)) * 100.0f)));
                    break;
            }
            return uploadData;
        } catch (IOException e) {
            IOException iOException = e;
            return null;
        }
    }

    public static byte[] decompress(byte[] body) throws IOException {
        switch ($SWITCH_TABLE$com$openfeint$internal$request$Compression$CompressionMethod()[compressionMethod().ordinal()]) {
            case 1:
                int i = 0;
                if (MagicHeader.length < body.length) {
                    while (i < MagicHeader.length && MagicHeader[i] == body[i]) {
                        i++;
                    }
                }
                if (i != MagicHeader.length) {
                    return body;
                }
                int skip = MagicHeader.length + 4;
                return Util.toByteArray(new InflaterInputStream(new ByteArrayInputStream(body, skip, body.length - skip)));
            case 2:
            default:
                return body;
            case 3:
                return Util.toByteArray(new InflaterInputStream(new ByteArrayInputStream(body)));
        }
    }

    private static CompressionMethod compressionMethod() {
        String s = (String) OpenFeintInternal.getInstance().getSettings().get(OpenFeintSettings.SettingCloudStorageCompressionStrategy);
        if (s != null) {
            if (s.equals(OpenFeintSettings.CloudStorageCompressionStrategyLegacyHeaderlessCompression)) {
                return CompressionMethod.LegacyHeaderless;
            }
            if (s.equals(OpenFeintSettings.CloudStorageCompressionStrategyNoCompression)) {
                return CompressionMethod.Uncompressed;
            }
        }
        return CompressionMethod.Default;
    }

    private static byte[] _compress(byte[] data) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DeflaterOutputStream dos = new DeflaterOutputStream(baos);
        dos.write(data);
        dos.close();
        return baos.toByteArray();
    }

    private static byte[] integerToLittleEndianByteArray(int i) {
        return new byte[]{(byte) (i >> 0), (byte) (i >> 8), (byte) (i >> 16), (byte) (i >> 24)};
    }
}
