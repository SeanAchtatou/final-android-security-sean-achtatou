package com.openfeint.internal.request;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.openfeint.internal.OpenFeintInternal;
import com.tenapp.hoopersLite.R;

public abstract class BitmapRequest extends DownloadRequest {
    public void onSuccess(Bitmap responseBody) {
    }

    /* access modifiers changed from: protected */
    public void onSuccess(byte[] body) {
        Bitmap b = BitmapFactory.decodeByteArray(body, 0, body.length);
        if (b != null) {
            onSuccess(b);
        } else {
            onFailure(OpenFeintInternal.getRString(R.string.of_bitmap_decode_error));
        }
    }
}
