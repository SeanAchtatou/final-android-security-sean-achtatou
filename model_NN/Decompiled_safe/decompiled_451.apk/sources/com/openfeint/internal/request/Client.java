package com.openfeint.internal.request;

import android.os.Bundle;
import android.os.Handler;
import com.openfeint.internal.CookieStore;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.SyncedStore;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.protocol.HttpContext;

public class Client extends DefaultHttpClient {
    private static final int EXCESS_THREAD_LIFETIME = 30;
    private static final int MAX_THREADS = 4;
    private static final int MIN_THREADS = 2;
    private static final String TAG = "HTTPClient";
    private CookieStore mCookieStore;
    final ExecutorService mExecutor = new Executor();
    /* access modifiers changed from: private */
    public boolean mForceOffline;
    /* access modifiers changed from: private */
    public Handler mMainThreadHandler;
    /* access modifiers changed from: private */
    public Signer mSigner;

    public void saveInstanceState(Bundle outState) {
        this.mCookieStore.saveInstanceState(outState);
        outState.putBoolean("mForceOffline", this.mForceOffline);
    }

    public void restoreInstanceState(Bundle inState) {
        this.mCookieStore.restoreInstanceState(inState);
        this.mForceOffline = inState.getBoolean("mForceOffline");
    }

    public boolean toggleForceOffline() {
        if (this.mForceOffline) {
            this.mForceOffline = false;
            OpenFeintInternal.log(TAG, "forceOffline = FALSE");
        } else {
            this.mForceOffline = true;
            OpenFeintInternal.log(TAG, "forceOffline = TRUE");
        }
        return this.mForceOffline;
    }

    private static class GzipDecompressingEntity extends HttpEntityWrapper {
        public GzipDecompressingEntity(HttpEntity entity) {
            super(entity);
        }

        public InputStream getContent() throws IOException, IllegalStateException {
            return new GZIPInputStream(this.wrappedEntity.getContent());
        }

        public long getContentLength() {
            return -1;
        }
    }

    static final ClientConnectionManager makeCCM() {
        SchemeRegistry sr = new SchemeRegistry();
        sr.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        sr.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        return new ThreadSafeClientConnManager(new BasicHttpParams(), sr);
    }

    private final class Executor extends ThreadPoolExecutor {
        Executor() {
            super(2, 4, 30, TimeUnit.SECONDS, new LinkedBlockingQueue(), new RejectedExecutionHandler() {
                public void rejectedExecution(Runnable arg0, ThreadPoolExecutor arg1) {
                    OpenFeintInternal.log(Client.TAG, "Can't submit runnable " + arg0.toString());
                }
            });
        }
    }

    public Client(String key, String secret, SyncedStore prefs) {
        super(makeCCM(), new BasicHttpParams());
        this.mSigner = new Signer(key, secret);
        this.mMainThreadHandler = new Handler();
        this.mCookieStore = new CookieStore(prefs);
        setCookieStore(this.mCookieStore);
        addRequestInterceptor(new HttpRequestInterceptor() {
            public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
                if (!request.containsHeader("Accept-Encoding")) {
                    request.addHeader("Accept-Encoding", "gzip");
                }
            }
        });
        addResponseInterceptor(new HttpResponseInterceptor() {
            public void process(HttpResponse response, HttpContext context) throws HttpException, IOException {
                Header ceheader;
                HeaderElement[] codecs;
                HttpEntity entity = response.getEntity();
                if (entity != null && (ceheader = entity.getContentEncoding()) != null && (codecs = ceheader.getElements()) != null) {
                    for (HeaderElement name : codecs) {
                        if (name.getName().equalsIgnoreCase("gzip")) {
                            response.setEntity(new GzipDecompressingEntity(response.getEntity()));
                            return;
                        }
                    }
                }
            }
        });
    }

    public final void makeRequest(BaseRequest req) {
        makeRequest(req, req.timeout());
    }

    public final void makeRequest(final BaseRequest req, long timeoutMillis) {
        final Runnable onResponse = new Runnable() {
            public void run() {
                req.onResponse();
            }
        };
        final Runnable onTimeout = new Runnable() {
            public void run() {
                if (req.getResponse() == null && req.getFuture().cancel(true)) {
                    req.postTimeoutCleanup();
                    Client.this.mMainThreadHandler.post(onResponse);
                }
            }
        };
        final long j = timeoutMillis;
        final BaseRequest baseRequest = req;
        req.setFuture(this.mExecutor.submit(new Runnable() {
            public void run() {
                Client.this.mMainThreadHandler.postDelayed(onTimeout, j);
                baseRequest.sign(Client.this.mSigner);
                baseRequest.exec(Client.this.mForceOffline);
                Client.this.mMainThreadHandler.removeCallbacks(onTimeout);
                Client.this.mMainThreadHandler.post(onResponse);
            }
        }));
    }
}
