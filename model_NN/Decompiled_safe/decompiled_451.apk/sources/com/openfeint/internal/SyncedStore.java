package com.openfeint.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class SyncedStore {
    private static final String FILENAME = "of_prefs";
    private static final String TAG = "DistributedPrefs";
    private Context mContext;
    /* access modifiers changed from: private */
    public ReentrantReadWriteLock mLock = new ReentrantReadWriteLock();
    /* access modifiers changed from: private */
    public HashMap<String, String> mMap = new HashMap<>();

    public class Editor {
        public Editor() {
        }

        public void putString(String k, String v) {
            SyncedStore.this.mMap.put(k, v);
        }

        public void remove(String k) {
            SyncedStore.this.mMap.remove(k);
        }

        public Set<String> keySet() {
            return new HashSet(SyncedStore.this.mMap.keySet());
        }

        public void commit() {
            SyncedStore.this.save();
            SyncedStore.this.mLock.writeLock().unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public Editor edit() {
        this.mLock.writeLock().lock();
        return new Editor();
    }

    public class Reader {
        public Reader() {
        }

        public String getString(String k, String defValue) {
            String rv = (String) SyncedStore.this.mMap.get(k);
            return rv != null ? rv : defValue;
        }

        public Set<String> keySet() {
            return SyncedStore.this.mMap.keySet();
        }

        public void complete() {
            SyncedStore.this.mLock.readLock().unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public Reader read() {
        this.mLock.readLock().lock();
        return new Reader();
    }

    public SyncedStore(Context c) {
        this.mContext = c;
        load();
    }

    public void load() {
        this.mMap = null;
        boolean mustSaveAfterLoad = false;
        long start = System.currentTimeMillis();
        File myStore = this.mContext.getFileStreamPath(FILENAME);
        this.mLock.writeLock().lock();
        try {
            List<ApplicationInfo> apps = this.mContext.getPackageManager().getInstalledApplications(0);
            ApplicationInfo myInfo = null;
            Iterator<ApplicationInfo> it = apps.iterator();
            while (true) {
                if (it.hasNext()) {
                    ApplicationInfo ai = it.next();
                    if (ai.packageName.equals(this.mContext.getPackageName())) {
                        myInfo = ai;
                        break;
                    }
                } else {
                    break;
                }
            }
            String myStoreCPath = myStore.getCanonicalPath();
            if (myInfo != null) {
                if (myStoreCPath.startsWith(myInfo.dataDir)) {
                    String underDataDir = myStoreCPath.substring(myInfo.dataDir.length());
                    for (ApplicationInfo ai2 : apps) {
                        File file = new File(ai2.dataDir, underDataDir);
                        if (myStore.lastModified() < file.lastModified()) {
                            mustSaveAfterLoad = true;
                            myStore = file;
                        }
                    }
                    this.mMap = mapFromStore(myStore);
                }
            }
            if (this.mMap == null) {
                this.mMap = new HashMap<>();
            }
        } catch (IOException e) {
            OpenFeintInternal.log(TAG, "broken");
        } finally {
            this.mLock.writeLock().unlock();
        }
        if (mustSaveAfterLoad) {
            save();
        }
        OpenFeintInternal.log(TAG, "Loading prefs took " + new Long(System.currentTimeMillis() - start).toString() + " millis");
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x003c A[SYNTHETIC, Splitter:B:25:0x003c] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0041 A[Catch:{ IOException -> 0x0047 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x005c A[SYNTHETIC, Splitter:B:38:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x007b A[SYNTHETIC, Splitter:B:51:0x007b] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x009a A[SYNTHETIC, Splitter:B:64:0x009a] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00b1 A[SYNTHETIC, Splitter:B:73:0x00b1] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x00b5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.HashMap<java.lang.String, java.lang.String> mapFromStore(java.io.File r10) {
        /*
            r9 = this;
            r1 = 0
            r4 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0031, StreamCorruptedException -> 0x0051, IOException -> 0x0070, ClassNotFoundException -> 0x008f }
            r2.<init>(r10)     // Catch:{ FileNotFoundException -> 0x0031, StreamCorruptedException -> 0x0051, IOException -> 0x0070, ClassNotFoundException -> 0x008f }
            java.io.ObjectInputStream r5 = new java.io.ObjectInputStream     // Catch:{ FileNotFoundException -> 0x010a, StreamCorruptedException -> 0x00ff, IOException -> 0x00f4, ClassNotFoundException -> 0x00eb, all -> 0x00e4 }
            r5.<init>(r2)     // Catch:{ FileNotFoundException -> 0x010a, StreamCorruptedException -> 0x00ff, IOException -> 0x00f4, ClassNotFoundException -> 0x00eb, all -> 0x00e4 }
            java.lang.Object r3 = r5.readObject()     // Catch:{ FileNotFoundException -> 0x010f, StreamCorruptedException -> 0x0104, IOException -> 0x00f9, ClassNotFoundException -> 0x00ef, all -> 0x00e7 }
            if (r3 == 0) goto L_0x00c5
            boolean r6 = r3 instanceof java.util.HashMap     // Catch:{ FileNotFoundException -> 0x010f, StreamCorruptedException -> 0x0104, IOException -> 0x00f9, ClassNotFoundException -> 0x00ef, all -> 0x00e7 }
            if (r6 == 0) goto L_0x00c5
            java.util.HashMap r3 = (java.util.HashMap) r3     // Catch:{ FileNotFoundException -> 0x010f, StreamCorruptedException -> 0x0104, IOException -> 0x00f9, ClassNotFoundException -> 0x00ef, all -> 0x00e7 }
            if (r5 == 0) goto L_0x0021
            r5.close()     // Catch:{ IOException -> 0x0027 }
        L_0x001d:
            r4 = r5
            r1 = r2
            r6 = r3
        L_0x0020:
            return r6
        L_0x0021:
            if (r2 == 0) goto L_0x001d
            r2.close()     // Catch:{ IOException -> 0x0027 }
            goto L_0x001d
        L_0x0027:
            r6 = move-exception
            r0 = r6
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)
            goto L_0x001d
        L_0x0031:
            r6 = move-exception
            r0 = r6
        L_0x0033:
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "Couldn't open of_prefs"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)     // Catch:{ all -> 0x00ae }
            if (r4 == 0) goto L_0x0041
            r4.close()     // Catch:{ IOException -> 0x0047 }
        L_0x003f:
            r6 = 0
            goto L_0x0020
        L_0x0041:
            if (r1 == 0) goto L_0x003f
            r1.close()     // Catch:{ IOException -> 0x0047 }
            goto L_0x003f
        L_0x0047:
            r6 = move-exception
            r0 = r6
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)
            goto L_0x003f
        L_0x0051:
            r6 = move-exception
            r0 = r6
        L_0x0053:
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "StreamCorruptedException"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)     // Catch:{ all -> 0x00ae }
            if (r4 == 0) goto L_0x006a
            r4.close()     // Catch:{ IOException -> 0x0060 }
            goto L_0x003f
        L_0x0060:
            r6 = move-exception
            r0 = r6
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)
            goto L_0x003f
        L_0x006a:
            if (r1 == 0) goto L_0x003f
            r1.close()     // Catch:{ IOException -> 0x0060 }
            goto L_0x003f
        L_0x0070:
            r6 = move-exception
            r0 = r6
        L_0x0072:
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "IOException while reading"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)     // Catch:{ all -> 0x00ae }
            if (r4 == 0) goto L_0x0089
            r4.close()     // Catch:{ IOException -> 0x007f }
            goto L_0x003f
        L_0x007f:
            r6 = move-exception
            r0 = r6
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)
            goto L_0x003f
        L_0x0089:
            if (r1 == 0) goto L_0x003f
            r1.close()     // Catch:{ IOException -> 0x007f }
            goto L_0x003f
        L_0x008f:
            r6 = move-exception
            r0 = r6
        L_0x0091:
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "ClassNotFoundException"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)     // Catch:{ all -> 0x00ae }
            if (r4 == 0) goto L_0x00a8
            r4.close()     // Catch:{ IOException -> 0x009e }
            goto L_0x003f
        L_0x009e:
            r6 = move-exception
            r0 = r6
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)
            goto L_0x003f
        L_0x00a8:
            if (r1 == 0) goto L_0x003f
            r1.close()     // Catch:{ IOException -> 0x009e }
            goto L_0x003f
        L_0x00ae:
            r6 = move-exception
        L_0x00af:
            if (r4 == 0) goto L_0x00b5
            r4.close()     // Catch:{ IOException -> 0x00bb }
        L_0x00b4:
            throw r6
        L_0x00b5:
            if (r1 == 0) goto L_0x00b4
            r1.close()     // Catch:{ IOException -> 0x00bb }
            goto L_0x00b4
        L_0x00bb:
            r7 = move-exception
            r0 = r7
            java.lang.String r7 = "DistributedPrefs"
            java.lang.String r8 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r7, r8)
            goto L_0x00b4
        L_0x00c5:
            if (r5 == 0) goto L_0x00ce
            r5.close()     // Catch:{ IOException -> 0x00d7 }
            r4 = r5
            r1 = r2
            goto L_0x003f
        L_0x00ce:
            if (r2 == 0) goto L_0x00e0
            r2.close()     // Catch:{ IOException -> 0x00d7 }
            r4 = r5
            r1 = r2
            goto L_0x003f
        L_0x00d7:
            r6 = move-exception
            r0 = r6
            java.lang.String r6 = "DistributedPrefs"
            java.lang.String r7 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)
        L_0x00e0:
            r4 = r5
            r1 = r2
            goto L_0x003f
        L_0x00e4:
            r6 = move-exception
            r1 = r2
            goto L_0x00af
        L_0x00e7:
            r6 = move-exception
            r4 = r5
            r1 = r2
            goto L_0x00af
        L_0x00eb:
            r6 = move-exception
            r0 = r6
            r1 = r2
            goto L_0x0091
        L_0x00ef:
            r6 = move-exception
            r0 = r6
            r4 = r5
            r1 = r2
            goto L_0x0091
        L_0x00f4:
            r6 = move-exception
            r0 = r6
            r1 = r2
            goto L_0x0072
        L_0x00f9:
            r6 = move-exception
            r0 = r6
            r4 = r5
            r1 = r2
            goto L_0x0072
        L_0x00ff:
            r6 = move-exception
            r0 = r6
            r1 = r2
            goto L_0x0053
        L_0x0104:
            r6 = move-exception
            r0 = r6
            r4 = r5
            r1 = r2
            goto L_0x0053
        L_0x010a:
            r6 = move-exception
            r0 = r6
            r1 = r2
            goto L_0x0033
        L_0x010f:
            r6 = move-exception
            r0 = r6
            r4 = r5
            r1 = r2
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.SyncedStore.mapFromStore(java.io.File):java.util.HashMap");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0039 A[SYNTHETIC, Splitter:B:15:0x0039] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006d A[SYNTHETIC, Splitter:B:31:0x006d] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x007a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void save() {
        /*
            r7 = this;
            r3 = 0
            r1 = 0
            java.util.concurrent.locks.ReentrantReadWriteLock r4 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r4 = r4.readLock()
            r4.lock()
            android.content.Context r4 = r7.mContext     // Catch:{ IOException -> 0x002e }
            java.lang.String r5 = "of_prefs"
            r6 = 1
            java.io.FileOutputStream r3 = r4.openFileOutput(r5, r6)     // Catch:{ IOException -> 0x002e }
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x002e }
            r2.<init>(r3)     // Catch:{ IOException -> 0x002e }
            java.util.HashMap<java.lang.String, java.lang.String> r4 = r7.mMap     // Catch:{ IOException -> 0x00c7, all -> 0x00c4 }
            r2.writeObject(r4)     // Catch:{ IOException -> 0x00c7, all -> 0x00c4 }
            if (r2 == 0) goto L_0x009e
            r2.close()     // Catch:{ IOException -> 0x00a4 }
        L_0x0023:
            java.util.concurrent.locks.ReentrantReadWriteLock r4 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r4 = r4.readLock()
            r4.unlock()
            r1 = r2
        L_0x002d:
            return
        L_0x002e:
            r4 = move-exception
            r0 = r4
        L_0x0030:
            java.lang.String r4 = "DistributedPrefs"
            java.lang.String r5 = "Couldn't open of_prefs for writing"
            com.openfeint.internal.OpenFeintInternal.log(r4, r5)     // Catch:{ all -> 0x006a }
            if (r1 == 0) goto L_0x0046
            r1.close()     // Catch:{ IOException -> 0x004c }
        L_0x003c:
            java.util.concurrent.locks.ReentrantReadWriteLock r4 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r4 = r4.readLock()
            r4.unlock()
            goto L_0x002d
        L_0x0046:
            if (r3 == 0) goto L_0x003c
            r3.close()     // Catch:{ IOException -> 0x004c }
            goto L_0x003c
        L_0x004c:
            r4 = move-exception
            r0 = r4
            java.lang.String r4 = "DistributedPrefs"
            java.lang.String r5 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r4, r5)     // Catch:{ all -> 0x005f }
            java.util.concurrent.locks.ReentrantReadWriteLock r4 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r4 = r4.readLock()
            r4.unlock()
            goto L_0x002d
        L_0x005f:
            r4 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r5 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r5 = r5.readLock()
            r5.unlock()
            throw r4
        L_0x006a:
            r4 = move-exception
        L_0x006b:
            if (r1 == 0) goto L_0x007a
            r1.close()     // Catch:{ IOException -> 0x0080 }
        L_0x0070:
            java.util.concurrent.locks.ReentrantReadWriteLock r5 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r5 = r5.readLock()
            r5.unlock()
        L_0x0079:
            throw r4
        L_0x007a:
            if (r3 == 0) goto L_0x0070
            r3.close()     // Catch:{ IOException -> 0x0080 }
            goto L_0x0070
        L_0x0080:
            r5 = move-exception
            r0 = r5
            java.lang.String r5 = "DistributedPrefs"
            java.lang.String r6 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r5, r6)     // Catch:{ all -> 0x0093 }
            java.util.concurrent.locks.ReentrantReadWriteLock r5 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r5 = r5.readLock()
            r5.unlock()
            goto L_0x0079
        L_0x0093:
            r4 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r5 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r5 = r5.readLock()
            r5.unlock()
            throw r4
        L_0x009e:
            if (r3 == 0) goto L_0x0023
            r3.close()     // Catch:{ IOException -> 0x00a4 }
            goto L_0x0023
        L_0x00a4:
            r4 = move-exception
            r0 = r4
            java.lang.String r4 = "DistributedPrefs"
            java.lang.String r5 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r4, r5)     // Catch:{ all -> 0x00b9 }
            java.util.concurrent.locks.ReentrantReadWriteLock r4 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r4 = r4.readLock()
            r4.unlock()
            r1 = r2
            goto L_0x002d
        L_0x00b9:
            r4 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r5 = r7.mLock
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r5 = r5.readLock()
            r5.unlock()
            throw r4
        L_0x00c4:
            r4 = move-exception
            r1 = r2
            goto L_0x006b
        L_0x00c7:
            r4 = move-exception
            r0 = r4
            r1 = r2
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.SyncedStore.save():void");
    }
}
