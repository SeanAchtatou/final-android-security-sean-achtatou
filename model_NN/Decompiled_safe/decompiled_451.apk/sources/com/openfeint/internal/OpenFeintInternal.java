package com.openfeint.internal;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.webkit.CookieManager;
import com.openfeint.api.Notification;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.api.resource.CurrentUser;
import com.openfeint.api.resource.User;
import com.openfeint.internal.SyncedStore;
import com.openfeint.internal.db.DB;
import com.openfeint.internal.notifications.SimpleNotification;
import com.openfeint.internal.notifications.TwoLineNotification;
import com.openfeint.internal.offline.OfflineSupport;
import com.openfeint.internal.request.BaseRequest;
import com.openfeint.internal.request.BlobPostRequest;
import com.openfeint.internal.request.Client;
import com.openfeint.internal.request.GenericRequest;
import com.openfeint.internal.request.IRawRequestDelegate;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.request.OrderedArgList;
import com.openfeint.internal.request.RawRequest;
import com.openfeint.internal.request.multipart.ByteArrayPartSource;
import com.openfeint.internal.request.multipart.FilePartSource;
import com.openfeint.internal.request.multipart.PartSource;
import com.openfeint.internal.resource.BlobUploadParameters;
import com.openfeint.internal.resource.ServerException;
import com.openfeint.internal.ui.IntroFlow;
import com.openfeint.internal.ui.WebViewCache;
import com.tenapp.hoopersLite.R;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import org.apache.commons.codec.binary.Hex;
import org.apache.http.impl.client.AbstractHttpClient;
import org.codehaus.jackson.JsonFactory;

public class OpenFeintInternal {
    private static final boolean DEVELOPMENT_LOGGING_ENABLED = false;
    private static final String TAG = "OpenFeint";
    private static OpenFeintInternal sInstance;
    Analytics analytics;
    int mAppVersion = -1;
    /* access modifiers changed from: private */
    public boolean mApproved;
    /* access modifiers changed from: private */
    public boolean mBanned;
    Client mClient;
    private Context mContext;
    /* access modifiers changed from: private */
    public boolean mCreatingDeviceSession;
    private CurrentUser mCurrentUser;
    /* access modifiers changed from: private */
    public boolean mCurrentlyLoggingIn;
    /* access modifiers changed from: private */
    public boolean mDeclined;
    OpenFeintDelegate mDelegate;
    /* access modifiers changed from: private */
    public boolean mDeserializedAlready;
    /* access modifiers changed from: private */
    public boolean mDeviceSessionCreated;
    Properties mInternalProperties;
    private LoginDelegate mLoginDelegate;
    Handler mMainThreadHandler;
    /* access modifiers changed from: private */
    public Runnable mPostDeviceSessionRunnable;
    /* access modifiers changed from: private */
    public Runnable mPostLoginRunnable;
    private SyncedStore mPrefs;
    /* access modifiers changed from: private */
    public List<Runnable> mQueuedPostDeviceSessionRunnables;
    /* access modifiers changed from: private */
    public List<Runnable> mQueuedPostLoginRunnables;
    String mServerUrl;
    OpenFeintSettings mSettings;
    String mUDID;

    public interface IUploadDelegate {
        void fileUploadedTo(String str, boolean z);
    }

    public interface LoginDelegate {
        void login(User user);
    }

    public void setLoginDelegate(LoginDelegate delegate) {
        this.mLoginDelegate = delegate;
    }

    private void _saveInstanceState(Bundle outState) {
        if (this.mCurrentUser != null) {
            outState.putString("mCurrentUser", this.mCurrentUser.generate());
        }
        if (this.mClient != null) {
            this.mClient.saveInstanceState(outState);
        }
        outState.putBoolean("mCurrentlyLoggingIn", this.mCurrentlyLoggingIn);
        outState.putBoolean("mCreatingDeviceSession", this.mCreatingDeviceSession);
        outState.putBoolean("mDeviceSessionCreated", this.mDeviceSessionCreated);
        outState.putBoolean("mBanned", this.mBanned);
        outState.putBoolean("mApproved", this.mApproved);
        outState.putBoolean("mDeclined", this.mDeclined);
    }

    private void _restoreInstanceState(Bundle inState) {
        if (!this.mDeserializedAlready && inState != null) {
            this.mCurrentUser = (CurrentUser) userFromString(inState.getString("mCurrentUser"));
            if (this.mClient != null) {
                this.mClient.restoreInstanceState(inState);
            }
            this.mCurrentlyLoggingIn = inState.getBoolean("mCurrentlyLoggingIn");
            this.mCreatingDeviceSession = inState.getBoolean("mCreatingDeviceSession");
            this.mDeviceSessionCreated = inState.getBoolean("mDeviceSessionCreated");
            this.mBanned = inState.getBoolean("mBanned");
            this.mApproved = inState.getBoolean("mApproved");
            this.mDeclined = inState.getBoolean("mDeclined");
            this.mDeserializedAlready = true;
        }
    }

    public static void saveInstanceState(Bundle outState) {
        getInstance()._saveInstanceState(outState);
    }

    public static void restoreInstanceState(Bundle inState) {
        getInstance()._restoreInstanceState(inState);
    }

    public static OpenFeintInternal getInstance() {
        return sInstance;
    }

    public OpenFeintDelegate getDelegate() {
        return this.mDelegate;
    }

    public AbstractHttpClient getClient() {
        return this.mClient;
    }

    public SyncedStore getPrefs() {
        if (this.mPrefs == null) {
            this.mPrefs = new SyncedStore(getContext());
        }
        return this.mPrefs;
    }

    private void saveUser(SyncedStore.Editor e, User u) {
        e.putString("last_logged_in_user", u.generate());
    }

    private void clearUser(SyncedStore.Editor e) {
        e.remove("last_logged_in_user");
    }

    /* JADX INFO: finally extract failed */
    private User loadUser() {
        SyncedStore.Reader r = getPrefs().read();
        try {
            String urep = r.getString("last_logged_in_user", null);
            r.complete();
            return userFromString(urep);
        } catch (Throwable th) {
            r.complete();
            throw th;
        }
    }

    private static User userFromString(String urep) {
        if (urep == null) {
            return null;
        }
        try {
            Object responseBody = new JsonResourceParser(new JsonFactory().createJsonParser(new ByteArrayInputStream(urep.getBytes()))).parse();
            if (responseBody != null && (responseBody instanceof User)) {
                return (User) responseBody;
            }
        } catch (IOException e) {
        }
        return null;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    public void userLoggedIn(User loggedInUser) {
        this.mCurrentUser = new CurrentUser();
        this.mCurrentUser.shallowCopyAncestorType(loggedInUser);
        User lliu = lastLoggedInUser();
        if (lliu == null || !lliu.resourceID().equals(loggedInUser.resourceID())) {
            CookieManager.getInstance().removeAllCookie();
        }
        SyncedStore.Editor e = getPrefs().edit();
        try {
            e.putString("last_logged_in_server", getServerUrl());
            saveUserApproval(e);
            saveUser(e, loggedInUser);
            e.commit();
            if (this.mDelegate != null) {
                this.mDelegate.userLoggedIn(this.mCurrentUser);
            }
            if (this.mLoginDelegate != null) {
                this.mLoginDelegate.login(this.mCurrentUser);
            }
            if (this.mPostLoginRunnable != null) {
                this.mMainThreadHandler.post(this.mPostLoginRunnable);
                this.mPostLoginRunnable = null;
            }
            getAnalytics().markSessionOpen(true);
            OfflineSupport.setUserID(loggedInUser.resourceID());
        } catch (Throwable th) {
            e.commit();
            throw th;
        }
    }

    private void userLoggedOut() {
        User previousLocalUser = this.mCurrentUser;
        this.mCurrentUser = null;
        this.mDeviceSessionCreated = DEVELOPMENT_LOGGING_ENABLED;
        clearPrefs();
        if (this.mDelegate != null) {
            this.mDelegate.userLoggedOut(previousLocalUser);
        }
        getAnalytics().markSessionClose();
        OfflineSupport.setUserDeclined();
    }

    /* access modifiers changed from: private */
    public void clearPrefs() {
        SyncedStore.Editor e = getPrefs().edit();
        try {
            e.remove("last_logged_in_server");
            e.remove("last_logged_in_user_name");
            clearUser(e);
        } finally {
            e.commit();
        }
    }

    public void createUser(String userName, String email, String password, String passwordConfirmation, IRawRequestDelegate delegate) {
        OrderedArgList bootstrapArgs = new OrderedArgList();
        bootstrapArgs.put("user[name]", userName);
        bootstrapArgs.put("user[http_basic_credential_attributes][email]", email);
        bootstrapArgs.put("user[http_basic_credential_attributes][password]", password);
        bootstrapArgs.put("user[http_basic_credential_attributes][password_confirmation]", passwordConfirmation);
        RawRequest userCreate = new RawRequest(bootstrapArgs) {
            public String method() {
                return "POST";
            }

            public String path() {
                return "/xp/users.json";
            }

            public void onSuccess(Object responseBody) {
                OpenFeintInternal.this.userLoggedIn((User) responseBody);
            }
        };
        userCreate.setDelegate(delegate);
        _makeRequest(userCreate);
    }

    public static String getModelString() {
        return "p(" + Build.PRODUCT + ")/m(" + Build.MODEL + ")";
    }

    public static String getOSVersionString() {
        return "v" + Build.VERSION.RELEASE + " (" + Build.VERSION.INCREMENTAL + ")";
    }

    public static String getScreenInfo() {
        DisplayMetrics metrics = Util.getDisplayMetrics();
        return String.format("%dx%d (%f dpi)", Integer.valueOf(metrics.widthPixels), Integer.valueOf(metrics.heightPixels), Float.valueOf(metrics.density));
    }

    public static String getProcessorInfo() {
        String family = "unknown";
        try {
            Iterator<String> it = cat("/proc/cpuinfo").iterator();
            while (true) {
                if (it.hasNext()) {
                    String l = it.next();
                    if (l.startsWith("Processor\t")) {
                        family = l.split(":")[1].trim();
                        break;
                    }
                } else {
                    break;
                }
            }
            return String.format("family(%s) min(%s) max(%s)", family, cat("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq").get(0), cat("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq").get(0));
        } catch (Exception e) {
            return "family(unknown) min(unknown) max(unknown)";
        }
    }

    private static List<String> cat(String filename) {
        List<String> rv = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)), 8192);
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                rv.add(line);
            }
            br.close();
        } catch (Exception e) {
        }
        return rv;
    }

    public Map<String, Object> getDeviceParams() {
        HashMap<String, Object> device = new HashMap<>();
        device.put("identifier", getUDID());
        device.put("hardware", getModelString());
        device.put("os", getOSVersionString());
        device.put("screen_resolution", getScreenInfo());
        device.put("processor", getProcessorInfo());
        return device;
    }

    public void createDeviceSession() {
        if (!this.mCreatingDeviceSession && !this.mDeviceSessionCreated) {
            HashMap<String, Object> argMap = new HashMap<>();
            argMap.put("platform", "android");
            argMap.put("device", getDeviceParams());
            argMap.put("of-version", getOFVersion());
            argMap.put("game_version", Integer.toString(getAppVersion()));
            argMap.put("protocol_version", "1.0");
            OrderedArgList args = new OrderedArgList(argMap);
            this.mCreatingDeviceSession = true;
            _makeRequest(new RawRequest(args) {
                public String method() {
                    return "POST";
                }

                public String path() {
                    return "/xp/devices";
                }

                public boolean needsDeviceSession() {
                    return OpenFeintInternal.DEVELOPMENT_LOGGING_ENABLED;
                }

                public void onResponse(int responseCode, Object responseBody) {
                    OpenFeintInternal.this.mCreatingDeviceSession = OpenFeintInternal.DEVELOPMENT_LOGGING_ENABLED;
                    if (200 > responseCode || responseCode >= 300) {
                        OpenFeintInternal.this.mPostLoginRunnable = null;
                        OpenFeintInternal.this.showOfflineNotification(responseCode, responseBody);
                    } else {
                        OpenFeintInternal.this.mDeviceSessionCreated = true;
                        if (OpenFeintInternal.this.mPostDeviceSessionRunnable != null) {
                            OpenFeintInternal.log(TAG, "Launching post-device-session runnable now.");
                            OpenFeintInternal.this.mMainThreadHandler.post(OpenFeintInternal.this.mPostDeviceSessionRunnable);
                        }
                    }
                    if (OpenFeintInternal.this.mQueuedPostDeviceSessionRunnables != null) {
                        for (Runnable r : OpenFeintInternal.this.mQueuedPostDeviceSessionRunnables) {
                            OpenFeintInternal.this.mMainThreadHandler.post(r);
                        }
                    }
                    OpenFeintInternal.this.mPostDeviceSessionRunnable = null;
                    OpenFeintInternal.this.mQueuedPostDeviceSessionRunnables = null;
                }
            });
        }
    }

    public final void runOnUiThread(Runnable action) {
        this.mMainThreadHandler.post(action);
    }

    public void loginUser(String userName, String password, String userID, IRawRequestDelegate delegate) {
        if (!checkBan()) {
            if (this.mCreatingDeviceSession || !this.mDeviceSessionCreated) {
                if (!this.mCreatingDeviceSession) {
                    createDeviceSession();
                }
                log(TAG, "No device session yet - queueing login.");
                final String str = userName;
                final String str2 = password;
                final String str3 = userID;
                final IRawRequestDelegate iRawRequestDelegate = delegate;
                this.mPostDeviceSessionRunnable = new Runnable() {
                    public void run() {
                        OpenFeintInternal.this.loginUser(str, str2, str3, iRawRequestDelegate);
                    }
                };
                return;
            }
            boolean allowToast = true;
            OrderedArgList bootstrapArgs = new OrderedArgList();
            if (!(userName == null || password == null)) {
                bootstrapArgs.put("login", userName);
                bootstrapArgs.put("password", password);
                allowToast = DEVELOPMENT_LOGGING_ENABLED;
            }
            if (!(userID == null || password == null)) {
                bootstrapArgs.put("user_id", userID);
                bootstrapArgs.put("password", password);
                allowToast = DEVELOPMENT_LOGGING_ENABLED;
            }
            this.mCurrentlyLoggingIn = true;
            final boolean finalToast = allowToast;
            RawRequest userLogin = new RawRequest(bootstrapArgs) {
                public String method() {
                    return "POST";
                }

                public String path() {
                    return "/xp/sessions.json";
                }

                public void onResponse(int responseCode, Object responseBody) {
                    OpenFeintInternal.this.mCurrentlyLoggingIn = OpenFeintInternal.DEVELOPMENT_LOGGING_ENABLED;
                    if (200 <= responseCode && responseCode < 300) {
                        OpenFeintInternal.this.userLoggedIn((User) responseBody);
                        if (OpenFeintInternal.this.mPostLoginRunnable != null) {
                            OpenFeintInternal.log(TAG, "Launching post-login runnable now.");
                            OpenFeintInternal.this.mMainThreadHandler.post(OpenFeintInternal.this.mPostLoginRunnable);
                        }
                    } else if (finalToast) {
                        OpenFeintInternal.this.showOfflineNotification(responseCode, responseBody);
                    }
                    if (OpenFeintInternal.this.mQueuedPostLoginRunnables != null) {
                        for (Runnable r : OpenFeintInternal.this.mQueuedPostLoginRunnables) {
                            OpenFeintInternal.this.mMainThreadHandler.post(r);
                        }
                    }
                    OpenFeintInternal.this.mPostLoginRunnable = null;
                    OpenFeintInternal.this.mQueuedPostLoginRunnables = null;
                }
            };
            userLogin.setDelegate(delegate);
            _makeRequest(userLogin);
        }
    }

    public void submitIntent(final Intent intent, boolean spotlight) {
        this.mDeclined = DEVELOPMENT_LOGGING_ENABLED;
        Runnable r = new Runnable() {
            public void run() {
                intent.addFlags(268435456);
                OpenFeintInternal.this.getContext().startActivity(intent);
            }
        };
        if (!isUserLoggedIn()) {
            log(TAG, "Not logged in yet - queueing intent " + intent.toString() + " for now.");
            this.mPostLoginRunnable = r;
            if (!currentlyLoggingIn()) {
                login(spotlight);
                return;
            }
            return;
        }
        this.mMainThreadHandler.post(r);
    }

    public void logoutUser(IRawRequestDelegate delegate) {
        OrderedArgList bootstrapArgs = new OrderedArgList();
        bootstrapArgs.put("platform", "android");
        RawRequest userLogout = new RawRequest(bootstrapArgs) {
            public String method() {
                return "DELETE";
            }

            public String path() {
                return "/xp/sessions.json";
            }
        };
        userLogout.setDelegate(delegate);
        _makeRequest(userLogout);
        userLoggedOut();
    }

    public static void genericRequest(String path, String method, Map<String, Object> args, Map<String, Object> httpParams, IRawRequestDelegate delegate) {
        makeRequest(new GenericRequest(path, method, args, httpParams, delegate));
    }

    /* JADX INFO: finally extract failed */
    public void userApprovedFeint() {
        this.mApproved = true;
        this.mDeclined = DEVELOPMENT_LOGGING_ENABLED;
        SyncedStore.Editor e = getPrefs().edit();
        try {
            saveUserApproval(e);
            e.commit();
            launchIntroFlow(DEVELOPMENT_LOGGING_ENABLED);
            OfflineSupport.setUserTemporary();
        } catch (Throwable th) {
            e.commit();
            throw th;
        }
    }

    private void saveUserApproval(SyncedStore.Editor e) {
        e.remove(String.valueOf(getContext().getPackageName()) + ".of_declined");
    }

    /* JADX INFO: finally extract failed */
    public void userDeclinedFeint() {
        this.mApproved = DEVELOPMENT_LOGGING_ENABLED;
        this.mDeclined = true;
        SyncedStore.Editor e = getPrefs().edit();
        try {
            e.putString(String.valueOf(getContext().getPackageName()) + ".of_declined", "sadly");
            e.commit();
            OfflineSupport.setUserDeclined();
        } catch (Throwable th) {
            e.commit();
            throw th;
        }
    }

    public boolean currentlyLoggingIn() {
        if (this.mCurrentlyLoggingIn || this.mCreatingDeviceSession) {
            return true;
        }
        return DEVELOPMENT_LOGGING_ENABLED;
    }

    public CurrentUser getCurrentUser() {
        return this.mCurrentUser;
    }

    public boolean isUserLoggedIn() {
        if (getCurrentUser() != null) {
            return true;
        }
        return DEVELOPMENT_LOGGING_ENABLED;
    }

    public String getUDID() {
        if (this.mUDID == null) {
            this.mUDID = findUDID();
        }
        return this.mUDID;
    }

    public Properties getInternalProperties() {
        return this.mInternalProperties;
    }

    public String getServerUrl() {
        if (this.mServerUrl == null) {
            String raw = getInternalProperties().getProperty("server-url").toLowerCase().trim();
            if (raw.endsWith("/")) {
                this.mServerUrl = raw.substring(0, raw.length() - 1);
            } else {
                this.mServerUrl = raw;
            }
        }
        return this.mServerUrl;
    }

    public String getOFVersion() {
        return getInternalProperties().getProperty("of-version");
    }

    public String getAppName() {
        return this.mSettings.name;
    }

    public String getAppID() {
        return this.mSettings.id;
    }

    public Map<String, Object> getSettings() {
        return this.mSettings.settings;
    }

    public int getAppVersion() {
        if (this.mAppVersion == -1) {
            Context c = getContext();
            try {
                this.mAppVersion = c.getPackageManager().getPackageInfo(c.getPackageName(), 0).versionCode;
            } catch (Exception e) {
                this.mAppVersion = 0;
            }
        }
        return this.mAppVersion;
    }

    public Context getContext() {
        return this.mContext;
    }

    public void displayErrorDialog(CharSequence errorMessage) {
        SimpleNotification.show(errorMessage.toString(), Notification.Category.Foreground, Notification.Type.Error);
    }

    private String findUDID() {
        String androidID = Settings.Secure.getString(getContext().getContentResolver(), "android_id");
        if (androidID != null && !androidID.equals("9774d56d682e549c")) {
            return "android-id-" + androidID;
        }
        SyncedStore.Reader r = getPrefs().read();
        try {
            String androidID2 = r.getString("udid", null);
            if (androidID2 == null) {
                byte[] randomBytes = new byte[16];
                new Random().nextBytes(randomBytes);
                androidID2 = "android-emu-" + new String(Hex.encodeHex(randomBytes)).replace("\r\n", "");
                SyncedStore.Editor e = getPrefs().edit();
                try {
                    e.putString("udid", androidID2);
                } finally {
                    e.commit();
                }
            }
            return androidID2;
        } finally {
            r.complete();
        }
    }

    public static void makeRequest(BaseRequest req) {
        OpenFeintInternal ofi = getInstance();
        if (ofi == null) {
            ServerException e = new ServerException();
            e.exceptionClass = "NoFeint";
            e.message = "OpenFeint has not been initialized.";
            req.onResponse(0, e.generate().getBytes());
            return;
        }
        ofi._makeRequest(req);
    }

    /* access modifiers changed from: private */
    public final void _makeRequest(final BaseRequest req) {
        if (!isUserLoggedIn() && req.wantsLogin() && lastLoggedInUser() != null && isFeintServerReachable()) {
            login(DEVELOPMENT_LOGGING_ENABLED);
            if (this.mQueuedPostLoginRunnables == null) {
                this.mQueuedPostLoginRunnables = new ArrayList();
            }
            this.mQueuedPostLoginRunnables.add(new Runnable() {
                public void run() {
                    OpenFeintInternal.this.mClient.makeRequest(req);
                }
            });
        } else if (this.mDeviceSessionCreated || !req.needsDeviceSession()) {
            this.mClient.makeRequest(req);
        } else {
            createDeviceSession();
            if (this.mQueuedPostDeviceSessionRunnables == null) {
                this.mQueuedPostDeviceSessionRunnables = new ArrayList();
            }
            this.mQueuedPostDeviceSessionRunnables.add(new Runnable() {
                public void run() {
                    OpenFeintInternal.this.mClient.makeRequest(req);
                }
            });
        }
    }

    public void uploadFile(String xpApiPath, String filePath, String contentType, IUploadDelegate delegate) {
        String fileName = filePath;
        try {
            String[] parts = filePath.split("/");
            if (parts.length > 0) {
                fileName = parts[parts.length - 1];
            }
            uploadFile(xpApiPath, new FilePartSource(fileName, new File(filePath)), contentType, delegate);
        } catch (FileNotFoundException e) {
            delegate.fileUploadedTo("", DEVELOPMENT_LOGGING_ENABLED);
        }
    }

    public void uploadFile(String xpApiPath, String fileName, byte[] fileData, String contentType, IUploadDelegate delegate) {
        uploadFile(xpApiPath, new ByteArrayPartSource(fileName, fileData), contentType, delegate);
    }

    public void uploadFile(String xpApiPath, PartSource partSource, String contentType, IUploadDelegate delegate) {
        final String str = xpApiPath;
        final PartSource partSource2 = partSource;
        final String str2 = contentType;
        final IUploadDelegate iUploadDelegate = delegate;
        _makeRequest(new JSONRequest() {
            public boolean wantsLogin() {
                return true;
            }

            public String method() {
                return "POST";
            }

            public String path() {
                return str;
            }

            public void onSuccess(Object responseBody) {
                final BlobUploadParameters params = (BlobUploadParameters) responseBody;
                BlobPostRequest bp = new BlobPostRequest(params, partSource2, str2);
                if (iUploadDelegate != null) {
                    final IUploadDelegate iUploadDelegate = iUploadDelegate;
                    bp.setDelegate(new IRawRequestDelegate() {
                        public void onResponse(int responseCode, String responseBody) {
                            iUploadDelegate.fileUploadedTo(String.valueOf(params.action) + params.key, (200 > responseCode || responseCode >= 300) ? OpenFeintInternal.DEVELOPMENT_LOGGING_ENABLED : true);
                        }
                    });
                }
                OpenFeintInternal.this._makeRequest(bp);
            }

            public void onFailure(String reason) {
                if (iUploadDelegate != null) {
                    iUploadDelegate.fileUploadedTo("", OpenFeintInternal.DEVELOPMENT_LOGGING_ENABLED);
                }
            }
        });
    }

    public int getResource(String resourceName) {
        return getContext().getResources().getIdentifier(resourceName, null, getContext().getPackageName());
    }

    public static String getRString(int id) {
        return getInstance().getContext().getResources().getString(id);
    }

    public static void initializeWithoutLoggingIn(Context ctx, OpenFeintSettings settings, OpenFeintDelegate delegate) {
        validateManifest(ctx);
        if (sInstance == null) {
            sInstance = new OpenFeintInternal(settings, ctx);
        }
        sInstance.mDelegate = delegate;
        if (!sInstance.mDeclined) {
            String userID = sInstance.getUserID();
            if (userID == null) {
                OfflineSupport.setUserTemporary();
            } else {
                OfflineSupport.setUserID(userID);
            }
            sInstance.createDeviceSession();
            return;
        }
        OfflineSupport.setUserDeclined();
    }

    public static void initialize(Context ctx, OpenFeintSettings settings, OpenFeintDelegate delegate) {
        initializeWithoutLoggingIn(ctx, settings, delegate);
        OpenFeintInternal ofi = getInstance();
        if (ofi != null) {
            ofi.login(DEVELOPMENT_LOGGING_ENABLED);
        }
    }

    public void setDelegate(OpenFeintDelegate delegate) {
        this.mDelegate = delegate;
    }

    /* JADX INFO: Multiple debug info for r0v12 java.lang.String: [D('victory' boolean), D('n' java.lang.String)] */
    private static boolean validateManifest(Context appContext) {
        boolean victory;
        try {
            PackageInfo packageInfo = appContext.getPackageManager().getPackageInfo(appContext.getPackageName(), 1);
            for (String n : new String[]{"com.openfeint.api.ui.Dashboard", "com.openfeint.internal.ui.IntroFlow", "com.openfeint.internal.ui.Settings", "com.openfeint.internal.ui.NativeBrowser"}) {
                ActivityInfo[] activityInfoArr = packageInfo.activities;
                int length = activityInfoArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        victory = false;
                        break;
                    }
                    ActivityInfo ai = activityInfoArr[i];
                    if (!ai.name.equals(n)) {
                        i++;
                    } else if ((ai.configChanges & 128) == 0) {
                        Log.v(TAG, String.format("ActivityInfo for %s has the wrong configChanges.\nPlease consult README.txt for the correct configuration.", n));
                        return DEVELOPMENT_LOGGING_ENABLED;
                    } else {
                        victory = true;
                    }
                }
                if (!victory) {
                    Log.v(TAG, String.format("Couldn't find ActivityInfo for %s.\nPlease consult README.txt for the correct configuration.", n));
                    return DEVELOPMENT_LOGGING_ENABLED;
                }
            }
            for (String n2 : new String[]{"android.permission.INTERNET"}) {
                if (Util.noPermission(n2, appContext)) {
                    return DEVELOPMENT_LOGGING_ENABLED;
                }
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            Log.v(TAG, String.format("Couldn't find PackageInfo for %s.\nPlease initialize OF with an Activity that lives in your root package.", appContext.getPackageName()));
            return DEVELOPMENT_LOGGING_ENABLED;
        }
    }

    /* JADX INFO: finally extract failed */
    private OpenFeintInternal(OpenFeintSettings settings, Context ctx) {
        sInstance = this;
        this.mContext = ctx;
        this.mSettings = settings;
        SyncedStore.Reader r = getPrefs().read();
        try {
            this.mDeclined = r.getString(new StringBuilder(String.valueOf(getContext().getPackageName())).append(".of_declined").toString(), null) != null ? true : DEVELOPMENT_LOGGING_ENABLED;
            r.complete();
            this.mMainThreadHandler = new Handler();
            this.mInternalProperties = new Properties();
            this.mInternalProperties.put("server-url", "https://api.openfeint.com");
            this.mInternalProperties.put("of-version", "1.9.2");
            loadPropertiesFromXMLResource(this.mInternalProperties, getResource("@xml/openfeint_internal_settings"));
            Log.i(TAG, "Using OpenFeint version " + this.mInternalProperties.get("of-version") + " (" + this.mInternalProperties.get("server-url") + ")");
            Properties appProperties = new Properties();
            loadPropertiesFromXMLResource(appProperties, getResource("@xml/openfeint_app_settings"));
            this.mSettings.applyOverrides(appProperties);
            this.mSettings.verify();
            if (!Encryption.initialized()) {
                Encryption.init(this.mSettings.secret);
            }
            this.mClient = new Client(this.mSettings.key, this.mSettings.secret, getPrefs());
            Util.moveWebCache(ctx);
            WebViewCache.initialize(ctx);
            DB.createDB(ctx);
            WebViewCache.start();
            this.analytics = new Analytics();
        } catch (Throwable th) {
            r.complete();
            throw th;
        }
    }

    public Analytics getAnalytics() {
        return this.analytics;
    }

    public String getUserID() {
        User user = getCurrentUser();
        if (user != null) {
            return user.userID();
        }
        User user2 = lastLoggedInUser();
        if (user2 != null) {
            return user2.userID();
        }
        return null;
    }

    /* access modifiers changed from: private */
    public final User lastLoggedInUser() {
        User savedUser = loadUser();
        SyncedStore.Reader r = getPrefs().read();
        try {
            URL saved = new URL(getServerUrl());
            URL loaded = new URL(r.getString("last_logged_in_server", ""));
            if (savedUser == null || !saved.equals(loaded)) {
                return null;
            }
            r.complete();
            return savedUser;
        } catch (MalformedURLException e) {
        } finally {
            r.complete();
        }
    }

    public void login(final boolean spotlight) {
        this.mMainThreadHandler.post(new Runnable() {
            public void run() {
                if (!OpenFeintInternal.this.mDeclined && !OpenFeintInternal.this.mCurrentlyLoggingIn && !OpenFeintInternal.this.isUserLoggedIn()) {
                    OpenFeintInternal.this.mDeserializedAlready = true;
                    final User savedUser = OpenFeintInternal.this.lastLoggedInUser();
                    if (savedUser != null) {
                        OpenFeintInternal.log(OpenFeintInternal.TAG, "Logging in last known user: " + savedUser.name);
                        OpenFeintInternal openFeintInternal = OpenFeintInternal.this;
                        final boolean z = spotlight;
                        openFeintInternal.loginUser(null, null, null, new IRawRequestDelegate() {
                            public void onResponse(int responseCode, String responseBody) {
                                if (200 <= responseCode && responseCode < 300) {
                                    SimpleNotification.show("Welcome back " + savedUser.name, Notification.Category.Login, Notification.Type.Success);
                                } else if (403 == responseCode) {
                                    OpenFeintInternal.this.mBanned = true;
                                } else {
                                    OpenFeintInternal.this.launchIntroFlow(z);
                                }
                            }
                        });
                        return;
                    }
                    OpenFeintInternal.log(OpenFeintInternal.TAG, "No last user, launch intro flow");
                    OpenFeintInternal.this.clearPrefs();
                    OpenFeintInternal.this.launchIntroFlow(spotlight);
                }
            }
        });
    }

    private boolean checkBan() {
        if (!this.mBanned) {
            return DEVELOPMENT_LOGGING_ENABLED;
        }
        displayErrorDialog(getContext().getText(R.string.of_banned_dialog));
        return true;
    }

    public void launchIntroFlow(final boolean spotlight) {
        if (!checkBan()) {
            if (isFeintServerReachable()) {
                OpenFeintDelegate d = getDelegate();
                if (this.mApproved || d == null || !d.showCustomApprovalFlow(getContext())) {
                    Runnable r = new Runnable() {
                        public void run() {
                            Intent i = new Intent(OpenFeintInternal.this.getContext(), IntroFlow.class);
                            if (OpenFeintInternal.this.mApproved && spotlight) {
                                i.putExtra("content_name", "index?preapproved=true&spotlight=true");
                            } else if (spotlight) {
                                i.putExtra("content_name", "index?spotlight=true");
                            } else if (OpenFeintInternal.this.mApproved) {
                                i.putExtra("content_name", "index?preapproved=true");
                            }
                            i.addFlags(268435456);
                            OpenFeintInternal.this.getContext().startActivity(i);
                        }
                    };
                    if (this.mCreatingDeviceSession || !this.mDeviceSessionCreated) {
                        if (!this.mCreatingDeviceSession) {
                            createDeviceSession();
                        }
                        this.mPostDeviceSessionRunnable = r;
                        return;
                    }
                    r.run();
                    return;
                }
                return;
            }
            showOfflineNotification(0, "");
        }
    }

    /* access modifiers changed from: private */
    public void showOfflineNotification(int httpCode, Object responseBody) {
        Resources r = getContext().getResources();
        String serverMessage = r.getString(R.string.of_offline_notification_line2);
        if (httpCode != 0) {
            if (403 == httpCode) {
                this.mBanned = true;
            }
            if (responseBody instanceof ServerException) {
                serverMessage = ((ServerException) responseBody).message;
            }
        }
        TwoLineNotification.show(r.getString(R.string.of_offline_notification), serverMessage, Notification.Category.Foreground, Notification.Type.NetworkOffline);
        log("Reachability", "Unable to launch IntroFlow because: " + serverMessage);
    }

    private void loadPropertiesFromXMLResource(Properties defaults, int resourceID) {
        XmlResourceParser xml = null;
        try {
            xml = getContext().getResources().getXml(resourceID);
        } catch (Exception e) {
        }
        if (xml != null) {
            String k = null;
            try {
                int eventType = xml.getEventType();
                while (xml.getEventType() != 1) {
                    if (eventType == 2) {
                        k = xml.getName();
                    } else if (xml.getEventType() == 4) {
                        defaults.setProperty(k, xml.getText());
                    }
                    xml.next();
                    eventType = xml.getEventType();
                }
                xml.close();
            } catch (Exception e2) {
                throw new RuntimeException(e2);
            }
        }
    }

    public boolean isFeintServerReachable() {
        if (Util.noPermission("android.permission.ACCESS_NETWORK_STATE", getContext())) {
            return true;
        }
        NetworkInfo activeNetwork = ((ConnectivityManager) getContext().getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetwork == null || !activeNetwork.isConnected()) {
            return DEVELOPMENT_LOGGING_ENABLED;
        }
        return true;
    }

    public static void log(String tag, String message) {
    }
}
