package com.adwhirl.obj;

import com.qwapi.adclient.android.utils.Utils;

public class Ration implements Comparable<Ration> {
    public String key = Utils.EMPTY_STRING;
    public String key2 = Utils.EMPTY_STRING;
    public String name = Utils.EMPTY_STRING;
    public String nid = Utils.EMPTY_STRING;
    public int priority = 0;
    public int type = 0;
    public double weight = 0.0d;

    public int compareTo(Ration another) {
        int otherPriority = another.priority;
        if (this.priority < otherPriority) {
            return -1;
        }
        if (this.priority > otherPriority) {
            return 1;
        }
        return 0;
    }
}
