package com.adwhirl.adapters;

import android.app.Activity;
import android.location.Location;
import android.util.Log;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlTargeting;
import com.adwhirl.obj.Extra;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import com.inmobi.androidsdk.EducationType;
import com.inmobi.androidsdk.EthnicityType;
import com.inmobi.androidsdk.GenderType;
import com.inmobi.androidsdk.InMobiAdDelegate;
import com.inmobi.androidsdk.impl.InMobiAdView;
import java.util.Date;

public final class InMobiAdapter extends AdWhirlAdapter implements InMobiAdDelegate {
    public int adUnit = 9;
    private Extra extra = null;

    public InMobiAdapter(AdWhirlLayout adWhirlLayout, Ration ration) {
        super(adWhirlLayout, ration);
        this.extra = adWhirlLayout.extra;
    }

    public void handle() {
        Activity activity;
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null && (activity = adWhirlLayout.activityReference.get()) != null) {
            InMobiAdView.requestAdUnitWithDelegate(activity.getApplicationContext(), this, activity, this.adUnit).loadNewAd();
        }
    }

    public void adRequestCompleted(InMobiAdView adView) {
        Log.d(AdWhirlUtil.ADWHIRL, "InMobi success");
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.adWhirlManager.resetRollover();
            adWhirlLayout.handler.post(new AdWhirlLayout.ViewAdRunnable(adWhirlLayout, adView));
            adWhirlLayout.rotateThreadedDelayed();
            adView.stopReceivingNotifications();
        }
    }

    public void adRequestFailed(InMobiAdView adView) {
        Log.d(AdWhirlUtil.ADWHIRL, "InMobi failure");
        adView.stopReceivingNotifications();
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.rollover();
        }
    }

    public int age() {
        return AdWhirlTargeting.getAge();
    }

    public String areaCode() {
        return null;
    }

    public Location currentLocation() {
        return null;
    }

    public Date dateOfBirth() {
        return null;
    }

    public EducationType education() {
        return null;
    }

    public EthnicityType ethnicity() {
        return null;
    }

    public GenderType gender() {
        AdWhirlTargeting.Gender gender = AdWhirlTargeting.getGender();
        if (AdWhirlTargeting.Gender.MALE == gender) {
            return GenderType.G_M;
        }
        if (AdWhirlTargeting.Gender.FEMALE == gender) {
            return GenderType.G_F;
        }
        return GenderType.G_None;
    }

    public int income() {
        return 0;
    }

    public String interests() {
        return null;
    }

    public boolean isLocationInquiryAllowed() {
        if (this.extra.locationOn == 1) {
            return true;
        }
        return false;
    }

    public boolean isPublisherProvidingLocation() {
        return false;
    }

    public String keywords() {
        return AdWhirlTargeting.getKeywords();
    }

    public String postalCode() {
        return AdWhirlTargeting.getPostalCode();
    }

    public String searchString() {
        return null;
    }

    public String siteId() {
        return this.ration.key;
    }

    public boolean testMode() {
        return AdWhirlTargeting.getTestMode();
    }
}
