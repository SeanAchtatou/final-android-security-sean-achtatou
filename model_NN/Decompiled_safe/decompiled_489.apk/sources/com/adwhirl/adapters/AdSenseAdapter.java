package com.adwhirl.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ListView;
import android.widget.ScrollView;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlTargeting;
import com.adwhirl.obj.Extra;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import com.google.ads.AdSenseSpec;
import com.google.ads.AdSpec;
import com.google.ads.AdViewListener;
import com.google.ads.GoogleAdView;
import com.qwapi.adclient.android.requestparams.AdRequestParams;
import java.util.ArrayList;
import java.util.List;

public class AdSenseAdapter extends AdWhirlAdapter implements AdViewListener {
    private GoogleAdView adView;

    public AdSenseAdapter(AdWhirlLayout adWhirlLayout, Ration ration) {
        super(adWhirlLayout, ration);
    }

    public void handle() {
        String keywords;
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            String clientId = this.ration.key;
            if (clientId == null || !clientId.startsWith("ca-mb-app-pub-")) {
                Log.w(AdWhirlUtil.ADWHIRL, "Invalid AdSense client ID");
                adWhirlLayout.rollover();
            } else if (TextUtils.isEmpty(googleAdSenseCompanyName) || TextUtils.isEmpty(googleAdSenseAppName)) {
                Log.w(AdWhirlUtil.ADWHIRL, "AdSense company name and app name are required parameters");
                adWhirlLayout.rollover();
            } else {
                ExtendedAdSenseSpec spec = new ExtendedAdSenseSpec(clientId);
                spec.setCompanyName(googleAdSenseCompanyName);
                spec.setAppName(googleAdSenseAppName);
                if (!TextUtils.isEmpty(googleAdSenseChannel)) {
                    spec.setChannel(googleAdSenseChannel);
                }
                spec.setAdFormat(AdSenseSpec.AdFormat.FORMAT_320x50);
                spec.setAdTestEnabled(AdWhirlTargeting.getTestMode());
                this.adView = new GoogleAdView(adWhirlLayout.getContext());
                this.adView.setAdViewListener(this);
                Extra extra = adWhirlLayout.extra;
                spec.setColorBackground(rgbToHex(extra.bgRed, extra.bgGreen, extra.bgBlue));
                spec.setGender(AdWhirlTargeting.getGender());
                spec.setAge(AdWhirlTargeting.getAge());
                if (AdWhirlTargeting.getKeywordSet() != null) {
                    keywords = TextUtils.join(",", AdWhirlTargeting.getKeywordSet());
                } else {
                    keywords = AdWhirlTargeting.getKeywords();
                }
                if (!TextUtils.isEmpty(keywords)) {
                    spec.setKeywords(keywords);
                }
                boolean canExpand = true;
                ViewParent p = adWhirlLayout.getParent();
                if (p == null) {
                    canExpand = false;
                } else {
                    while (true) {
                        if (!(p instanceof ListView) && !(p instanceof ScrollView)) {
                            p = p.getParent();
                            if (p == null) {
                                break;
                            }
                        } else {
                            canExpand = false;
                        }
                    }
                    canExpand = false;
                }
                if (canExpand && googleAdSenseExpandDirection != null) {
                    try {
                        spec.setExpandDirection(AdSenseSpec.ExpandDirection.valueOf(googleAdSenseExpandDirection));
                    } catch (IllegalArgumentException e) {
                    }
                }
                this.adView.setVisibility(4);
                adWhirlLayout.addView(this.adView, new ViewGroup.LayoutParams(-2, -2));
                this.adView.showAds(spec);
            }
        }
    }

    public void onStartFetchAd() {
    }

    public void onFinishFetchAd() {
        Log.d(AdWhirlUtil.ADWHIRL, "AdSense success");
        this.adView.setAdViewListener(null);
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.removeView(this.adView);
            this.adView.setVisibility(0);
            adWhirlLayout.adWhirlManager.resetRollover();
            adWhirlLayout.handler.post(new AdWhirlLayout.ViewAdRunnable(adWhirlLayout, this.adView));
            adWhirlLayout.rotateThreadedDelayed();
        }
    }

    public void onClickAd() {
    }

    public void onAdFetchFailure() {
        Log.d(AdWhirlUtil.ADWHIRL, "AdSense failure");
        this.adView.setAdViewListener(null);
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.removeView(this.adView);
            adWhirlLayout.rollover();
        }
    }

    private String rgbToHex(int r, int g, int b) {
        String rHex = channelValueToHex(r);
        String gHex = channelValueToHex(g);
        String bHex = channelValueToHex(b);
        if (rHex == null || gHex == null || bHex == null) {
            return null;
        }
        return rHex + gHex + bHex;
    }

    private String channelValueToHex(int channelValue) {
        if (channelValue < 0 || channelValue > 255) {
            return null;
        }
        if (channelValue <= 15) {
            return AdRequestParams.ZERO + Integer.toHexString(channelValue);
        }
        return Integer.toHexString(channelValue);
    }

    class ExtendedAdSenseSpec extends AdSenseSpec {
        public int ageCode = -1;
        public int genderCode = -1;

        public ExtendedAdSenseSpec(String clientId) {
            super(clientId);
        }

        public void setAge(int age) {
            if (age <= 0) {
                this.ageCode = -1;
            } else if (age <= 17) {
                this.ageCode = 1000;
            } else if (age <= 24) {
                this.ageCode = 1001;
            } else if (age <= 34) {
                this.ageCode = 1002;
            } else if (age <= 44) {
                this.ageCode = 1003;
            } else if (age <= 54) {
                this.ageCode = 1004;
            } else if (age <= 64) {
                this.ageCode = 1005;
            } else {
                this.ageCode = 1006;
            }
        }

        public void setGender(AdWhirlTargeting.Gender gender) {
            if (gender == AdWhirlTargeting.Gender.MALE) {
                this.genderCode = 1;
            } else if (gender == AdWhirlTargeting.Gender.FEMALE) {
                this.genderCode = 2;
            } else {
                this.genderCode = -1;
            }
        }

        public List<AdSpec.Parameter> generateParameters(Context context) {
            List<AdSpec.Parameter> parameters = new ArrayList<>(super.generateParameters(context));
            if (this.ageCode != -1) {
                parameters.add(new AdSpec.Parameter("cust_age", Integer.toString(this.ageCode)));
            }
            if (this.genderCode != -1) {
                parameters.add(new AdSpec.Parameter("cust_gender", Integer.toString(this.genderCode)));
            }
            return parameters;
        }
    }
}
