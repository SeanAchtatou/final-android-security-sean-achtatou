package com.qwapi.adclient.android.service;

import com.qwapi.adclient.android.data.Ad;
import com.qwapi.adclient.android.utils.Utils;
import junit.framework.TestCase;

public class AdRequestTestCase extends TestCase {
    static final /* synthetic */ boolean $assertionsDisabled = (!AdRequestTestCase.class.desiredAssertionStatus());

    public AdRequestTestCase(String str) {
        super(str);
    }

    public void testSimpleCase() {
        Ad ad = AdXmlResponseConverter.getAdResponse(Utils.processUrl("http://stg-ad.qwapi.com/adserver/render?test=false&fmtver=2.0&plc=top&adm=b&udid=ANDROID_SIMULATOR_0&pid=3d1d1275c2d649729bd43fa5a50ee936&ua=Android_USQuattroSDK&fmt=xml&sid=people-fp2qqq96&dem=&geo=", "Android").getResponse().replace("&ic=1", "&amp;ic=1")).getAd(0);
        if (!$assertionsDisabled && ad == null) {
            throw new AssertionError();
        }
    }
}
