package com.qwapi.adclient.android.service;

import android.util.Log;
import com.qwapi.adclient.android.AdApiConfig;
import com.qwapi.adclient.android.AdApiConstants;
import com.qwapi.adclient.android.data.AdResponse;
import com.qwapi.adclient.android.requestparams.AdRequestParams;
import com.qwapi.adclient.android.utils.HttpResponse;
import com.qwapi.adclient.android.utils.Utils;

public class SingleAdRequestService implements AdRequestService {
    protected AdResponse _response = null;

    public int count() {
        if (this._response == null) {
            return 0;
        }
        return this._response.count();
    }

    public AdResponse getAd(AdRequestParams adRequestParams) {
        HttpResponse response;
        if (!(adRequestParams == null || (response = getResponse(adRequestParams, false, 0)) == null)) {
            String response2 = response.getResponse();
            Log.d(AdApiConstants.SDK, "response:" + response2);
            if (response2 != null && response2.length() > 0) {
                long currentTimeMillis = System.currentTimeMillis();
                this._response = AdXmlResponseConverter.getAdResponse(response2);
                Log.d(AdApiConstants.SDK, "ad returned in " + Long.toString(System.currentTimeMillis() - currentTimeMillis));
            }
        }
        return this._response;
    }

    /* access modifiers changed from: protected */
    public HttpResponse getResponse(AdRequestParams adRequestParams, boolean z, int i) {
        String str = AdApiConfig.getAPIUrl() + "render?" + adRequestParams.getQueryString(z, i);
        Log.d(AdApiConstants.SDK, "Url:" + str);
        return Utils.processUrl(str, adRequestParams.getUserAgent());
    }
}
