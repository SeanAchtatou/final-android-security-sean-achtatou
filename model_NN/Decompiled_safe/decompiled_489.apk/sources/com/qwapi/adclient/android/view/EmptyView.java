package com.qwapi.adclient.android.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

public class EmptyView extends View {
    public EmptyView(Context context) {
        super(context);
    }

    public EmptyView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    private int measureHeight(int i) {
        return 0;
    }

    private int measureWidth(int i) {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(measureWidth(i), measureHeight(i2));
    }
}
