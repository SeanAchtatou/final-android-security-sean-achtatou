package com.qwapi.adclient.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.qwapi.adclient.android.utils.HttpResponse;
import com.qwapi.adclient.android.utils.Utils;
import java.text.MessageFormat;

public class AdApiRegistration {
    public static final String PREFS_FILE_NAME = "QWAdApiPrefsFile";
    public static final String PREF_NAME = "userRegistered";
    public static final String REGISTER_APP_URL = "{0}logAction?advid={1}&udid={2}&sid={3}";

    public static boolean isRegistered(Context context) {
        return context.getSharedPreferences(PREFS_FILE_NAME, 0).getBoolean(PREF_NAME, false);
    }

    public static void registerApplication(Context context) {
        if (!isRegistered(context)) {
            final DeviceContext deviceContext = new DeviceContext(context);
            final String format = MessageFormat.format(REGISTER_APP_URL, AdApiConfig.getAPIUrl(), AdApiConfig.getPublihserId(), deviceContext.getDeviceId(), AdApiConfig.getDefaultSiteId());
            final SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_FILE_NAME, 0);
            new Thread(new Runnable() {
                public void run() {
                    HttpResponse processUrl;
                    try {
                        synchronized (AdApiRegistration.class) {
                            if (!sharedPreferences.getBoolean(AdApiRegistration.PREF_NAME, false) && (processUrl = Utils.processUrl(format, deviceContext.getUserAgent())) != null && processUrl.getReturnCode() == 200) {
                                SharedPreferences.Editor edit = sharedPreferences.edit();
                                edit.putBoolean(AdApiRegistration.PREF_NAME, true);
                                edit.commit();
                            }
                        }
                    } catch (Throwable th) {
                        Log.e(AdApiConstants.SDK, "Problem during registration:" + format, th);
                    }
                }
            }).start();
        }
    }
}
