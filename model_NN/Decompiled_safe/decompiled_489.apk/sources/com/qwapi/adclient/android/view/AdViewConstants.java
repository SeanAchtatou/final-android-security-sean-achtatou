package com.qwapi.adclient.android.view;

public class AdViewConstants {
    public static final String AMP = "&";
    public static final String EMPTY_DATA = "<div align=\"center\"><div>";
    public static final String EQUALS = "=";
    public static final String QUESTION = "?";
    public static final String TEXT_HTML = "text/html";
    public static final String UTF8 = "utf-8";
}
