package com.qwapi.adclient.android.requestparams;

import com.google.ads.AdActivity;
import com.qwapi.adclient.android.data.Ad;

public enum MediaType {
    banner,
    text,
    interstitial,
    expandable,
    animated;

    public String batchValue() {
        switch (ordinal()) {
            case 0:
                return "bs";
            default:
                return toString();
        }
    }

    public String toNamedString() {
        switch (ordinal()) {
            case 0:
                return "banner";
            case 1:
                return "text";
            case 2:
                return "interstitial";
            case 3:
                return Ad.AD_TYPE_EXPANDABLE_BANNER;
            case 4:
                return Ad.AD_TYPE_ANIMATED_BANNER;
            default:
                return null;
        }
    }

    public String toString() {
        switch (ordinal()) {
            case 0:
                return "b";
            case 1:
                return "t";
            case 2:
                return AdActivity.INTENT_ACTION_PARAM;
            case 3:
                return "sa";
            case 4:
                return "b";
            default:
                return null;
        }
    }
}
