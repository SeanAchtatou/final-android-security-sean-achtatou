package com.qwapi.adclient.android.data;

import android.util.Log;
import com.qwapi.adclient.android.utils.Utils;
import java.io.Serializable;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;

public class Ad implements Serializable {
    public static final String AD_TYPE_ANIMATED_BANNER = "animatedbanner";
    public static final String AD_TYPE_BANNER = "banner";
    public static final String AD_TYPE_EXPANDABLE_BANNER = "expandablebanner";
    public static final String AD_TYPE_INTERSTITIAL = "interstitial";
    public static final String AD_TYPE_TEXT = "text";
    private static final String ANCHOR_IMAGE_AD = "<html><head><style type=\"text/css\">body '{' margin:0px;padding:0px '}'</style></head><body><div align=\"center\"> <a href=\"{0}\"><img src=\"{1}\" alt=\"{2}\"  width=\"{3}\" height=\"{4}\" /></a></div>{5}</body></head>";
    private static final String ANCHOR_IMAGE_AD_WITH_ACTION = "<style type=\"text/css\">a:link'{'color:rgb({6});'}'</style><div align=\"center\"> <a href=\"{0}\"><img src=\"{1}\" alt=\"{2}\"  width=\"{3}\" height=\"{4}\" /></a><br /><br/><a href=\"{0}\">{7}</a> </div>";
    private static final String ANCHOR_TEXT_AD = "<html><head><style type=\"text/css\">body '{' margin:0px;padding:0px '}' a:link'{'color:rgb({2});'}'</style></head><body><div align=\"center\"><a href=\"{0}\">{1}</a></div>{3}</body></html>";
    private static final String ANCHOR_TEXT_AD_WITH_HEADLINE = "<html><head><style type='text/css'>body '{' margin:0px;padding:0px '}' a:link'{'color:rgb({3});'}'</style><div align=\"center\"><a href=\"{0}\"><b>{1}</b><br/>{2}</a></div>{4}</body></html>";
    private static final String PIXEL_TRACKING_URL_0 = "<img border=\"0\" src=\"";
    private static final String PIXEL_TRACKING_URL_1 = "\" height=\"1\" width=\"1\"/>";
    private String actionText;
    private String adText;
    private String adType;
    private String clickUrl;
    private Data data;
    private int dbid;
    private GregorianCalendar expiry = new GregorianCalendar();
    private boolean externallyHosted;
    private String id;
    private Image image;
    private int impressions = 1;
    private boolean isBot;
    private long lastServed = 0;
    private transient Status status;
    private Text text;
    private List<String> trackingPixels;

    public Ad() {
        this.expiry.add(12, 5);
    }

    private String getBannerXhtmlAdContent(String str) {
        return getBannerXhtmlAdContent(str, true);
    }

    private String getBannerXhtmlAdContent(String str, boolean z) {
        if (this.image == null) {
            return Utils.EMPTY_STRING;
        }
        return MessageFormat.format(this.actionText != null ? ANCHOR_IMAGE_AD : ANCHOR_IMAGE_AD, getBannerAdObjects(str, z));
    }

    private String getTextXhtmlAdContent(String str) {
        String str2 = null;
        if (getText() != null) {
            str2 = MessageFormat.format(getText().getHeadline() != null ? ANCHOR_TEXT_AD_WITH_HEADLINE : ANCHOR_TEXT_AD, getTextAdObjects(str));
        }
        return str2 == null ? Utils.EMPTY_STRING : str2;
    }

    public String getActionText() {
        return this.actionText;
    }

    public String getAdText() {
        return this.adText;
    }

    public String getAdType() {
        return this.adType;
    }

    public Object[] getBannerAdObjects(String str) {
        return getBannerAdObjects(str, true);
    }

    public Object[] getBannerAdObjects(String str, boolean z) {
        if (this.image == null) {
            return new Object[0];
        }
        if (this.actionText != null) {
            Object[] objArr = new Object[6];
            objArr[0] = this.clickUrl;
            objArr[1] = z ? this.image.getUrl() : this.image.getUrl() + "&width=" + this.image.getWidth() + "&height=225";
            objArr[2] = this.image.getAltText();
            objArr[3] = Utils.EMPTY_STRING + this.image.getWidth();
            objArr[4] = Utils.EMPTY_STRING + (z ? this.image.getHeight() : 225);
            objArr[5] = getPixelHtmlTags();
            return objArr;
        }
        Object[] objArr2 = new Object[6];
        objArr2[0] = this.clickUrl;
        objArr2[1] = z ? this.image.getUrl() : this.image.getUrl() + "&width=" + this.image.getWidth() + "&height=225";
        objArr2[2] = this.image.getAltText();
        objArr2[3] = Utils.EMPTY_STRING + this.image.getWidth();
        objArr2[4] = Utils.EMPTY_STRING + (z ? this.image.getHeight() : 225);
        objArr2[5] = getPixelHtmlTags();
        return objArr2;
    }

    public String getClickUrl() {
        return this.clickUrl;
    }

    public Data getData() {
        return this.data;
    }

    public int getDbId() {
        return this.dbid;
    }

    public long getExpires() {
        return this.expiry.getTime().getTime();
    }

    public String getId() {
        return this.id;
    }

    public Image getImage() {
        return this.image;
    }

    public int getImpressions() {
        return this.impressions;
    }

    public long getLastServed() {
        return this.lastServed;
    }

    public String getPixelHtmlTags() {
        if (this.trackingPixels == null) {
            return Utils.EMPTY_STRING;
        }
        StringBuilder sb = new StringBuilder();
        for (String append : this.trackingPixels) {
            sb.append(PIXEL_TRACKING_URL_0).append(append).append(PIXEL_TRACKING_URL_1);
        }
        return sb.toString();
    }

    public Status getStatus() {
        return this.status;
    }

    public Text getText() {
        return this.text;
    }

    public Object[] getTextAdObjects(String str) {
        if (this.text == null) {
            return new Object[0];
        }
        if (this.text.getHeadline() != null) {
            return new Object[]{this.clickUrl, this.text.getBodyText(), this.text.getHeadline(), str, getPixelHtmlTags()};
        }
        return new Object[]{this.clickUrl, this.text.getBodyText(), str, getPixelHtmlTags()};
    }

    public List<String> getTrackingPixels() {
        return this.trackingPixels;
    }

    public String getXhtmlAdContent(String str) {
        return getXhtmlAdContent(str, true);
    }

    public String getXhtmlAdContent(String str, boolean z) {
        return this.adType.indexOf("banner") != -1 ? getBannerXhtmlAdContent(str) : this.adType.indexOf("interstitial") != -1 ? getBannerXhtmlAdContent(str, z) : this.adType.equals("text") ? getTextXhtmlAdContent(str) : Utils.EMPTY_STRING;
    }

    public boolean isBot() {
        return this.isBot;
    }

    public boolean isExpired() {
        return this.expiry.before(new GregorianCalendar());
    }

    public boolean isExternallyHosted() {
        return this.externallyHosted;
    }

    public int served() {
        this.impressions--;
        return this.impressions;
    }

    public void setActionText(String str) {
        this.actionText = str;
    }

    public void setAdText(String str) {
        this.adText = str;
    }

    public void setAdType(String str) {
        this.adType = str;
    }

    public void setBot(boolean z) {
        this.isBot = z;
    }

    public void setClickUrl(String str) {
        this.clickUrl = str;
    }

    public void setData(Data data2) {
        this.data = data2;
    }

    public void setDbId(int i) {
        this.dbid = i;
    }

    public void setExpiry(long j) {
        this.expiry.setTimeInMillis(j);
    }

    public void setExpiry(String str) {
        try {
            this.expiry.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm Z").parse(str));
        } catch (Exception e) {
            Log.e("expiry parse error", e.getMessage());
        }
    }

    public void setExternallyHosted(boolean z) {
        this.externallyHosted = z;
    }

    public void setId(String str) {
        this.id = str;
    }

    public void setImage(Image image2) {
        this.image = image2;
    }

    public void setImpressionCount(int i) {
        int i2 = 1;
        if (i >= 1) {
            i2 = i;
        }
        this.impressions = i2;
    }

    public void setLastServed(int i) {
        this.lastServed = (long) i;
    }

    /* access modifiers changed from: protected */
    public void setStatus(Status status2) {
        this.status = status2;
    }

    public void setText(Text text2) {
        this.text = text2;
    }

    public void setTrackingPixels(List<String> list) {
        this.trackingPixels = list;
    }
}
