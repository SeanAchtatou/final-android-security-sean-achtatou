package com.qwapi.adclient.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.qwapi.adclient.android.AdApiConstants;
import com.qwapi.adclient.android.DeviceContext;
import com.qwapi.adclient.android.data.Ad;
import com.qwapi.adclient.android.utils.Utils;
import java.lang.ref.WeakReference;

public class AdWebView extends WebView {
    protected WeakReference<QWAdView> wAdView = null;

    private class AdWebViewClient extends WebViewClient {
        WeakReference<Ad> ad;
        WeakReference<EventDispatcher> wDispatacher;

        public AdWebViewClient(EventDispatcher eventDispatcher, Ad ad2) {
            this.wDispatacher = new WeakReference<>(eventDispatcher);
            this.ad = new WeakReference<>(ad2);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            try {
                this.wDispatacher.get().onAdClick(this.ad.get());
            } catch (Exception e) {
                Log.e(AdApiConstants.SDK, "Problem during onlick:" + e.getMessage());
            }
            String deviceId = DeviceContext.getDeviceId(webView.getContext());
            if (Utils.isGoodString(deviceId)) {
                Utils.invokeLandingPage(webView, str + "&udid=" + deviceId);
                return true;
            }
            Utils.invokeLandingPage(webView, str);
            return true;
        }
    }

    public AdWebView(Context context, AttributeSet attributeSet, Ad ad, EventDispatcher eventDispatcher, QWAdView qWAdView) {
        super(context, attributeSet);
        setBackgroundColor(0);
        init(ad, eventDispatcher, qWAdView);
    }

    public AdWebView(Context context, Ad ad, EventDispatcher eventDispatcher, QWAdView qWAdView) {
        super(context);
        init(ad, eventDispatcher, qWAdView);
    }

    private void init(Ad ad, EventDispatcher eventDispatcher, QWAdView qWAdView) {
        this.wAdView = new WeakReference<>(qWAdView);
        getSettings().setJavaScriptEnabled(true);
        setWebViewClient(new AdWebViewClient(eventDispatcher, ad));
        getSettings().setCacheMode(2);
        getSettings().setDefaultFontSize(12);
        load(ad);
        setBackgroundColor(0);
    }

    /* access modifiers changed from: protected */
    public void load(Ad ad) {
        String str;
        if (ad == null) {
            str = AdViewConstants.EMPTY_DATA;
        } else {
            String xhtmlAdContent = ad.getXhtmlAdContent(this.wAdView.get().getTextColor());
            Log.d("QuattroWirelessSDK/2.1 webview:", xhtmlAdContent);
            str = xhtmlAdContent;
        }
        loadDataWithBaseURL(null, str, AdViewConstants.TEXT_HTML, "utf-8", null);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeAllViews();
        destroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || !canGoBack()) {
            return super.onKeyDown(i, keyEvent);
        }
        goBack();
        return true;
    }
}
