package com.qwapi.adclient.android.requestparams;

public enum DisplayMode {
    normal,
    aged,
    autoRotate;

    public String toString() {
        switch (ordinal()) {
            case 0:
                return "Normal";
            case 1:
                return "Aged";
            case 2:
                return "Auto Rotate";
            default:
                return null;
        }
    }
}
