package com.qwapi.adclient.android.requestparams;

import com.google.ads.AdActivity;

public enum Gender {
    male,
    female;

    public String toString() {
        return ordinal() == 0 ? AdActivity.TYPE_PARAM : "f";
    }
}
