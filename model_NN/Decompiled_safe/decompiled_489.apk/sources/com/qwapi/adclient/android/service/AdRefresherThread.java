package com.qwapi.adclient.android.service;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.qwapi.adclient.android.requestparams.DisplayMode;
import com.qwapi.adclient.android.view.QWAdView;
import java.lang.ref.WeakReference;

public class AdRefresherThread extends Thread {
    private static final int MSG = 9999;
    /* access modifiers changed from: private */
    public int adInterval = 30;
    WeakReference<QWAdView> adView = null;
    /* access modifiers changed from: private */
    public DisplayMode displayMode = null;
    boolean first = true;
    private Looper looper = null;
    private Handler mRefreshHandler;
    private volatile boolean stopRequested = false;
    /* access modifiers changed from: private */
    public volatile boolean suspendAutoRefresh = false;

    public AdRefresherThread(QWAdView qWAdView, DisplayMode displayMode2, int i) {
        this.adView = new WeakReference<>(qWAdView);
        this.displayMode = displayMode2;
        this.adInterval = i;
    }

    public Handler getRefreshHandler() {
        return this.mRefreshHandler;
    }

    public void resumeThread() {
        this.suspendAutoRefresh = false;
    }

    public void run() {
        while (!this.stopRequested) {
            Looper.prepare();
            this.mRefreshHandler = new Handler() {
                boolean first = true;

                public void handleMessage(Message message) {
                    if (this.first) {
                        this.first = false;
                        if (AdRefresherThread.this.displayMode == DisplayMode.autoRotate) {
                            sendMessageDelayed(obtainMessage(AdRefresherThread.MSG), (long) (AdRefresherThread.this.adInterval * 1000));
                            return;
                        }
                    }
                    if (!AdRefresherThread.this.suspendAutoRefresh) {
                        AdRefresherThread.this.adView.get().prepareAd();
                        AdRefresherThread.this.adView.get().showNextAd();
                    }
                    if (AdRefresherThread.this.displayMode == DisplayMode.autoRotate) {
                        sendMessageDelayed(obtainMessage(AdRefresherThread.MSG), (long) (AdRefresherThread.this.adInterval * 1000));
                    }
                }
            };
            if (this.first) {
                this.mRefreshHandler.sendEmptyMessage(MSG);
                this.first = false;
            }
            this.looper = Looper.myLooper();
            Looper.loop();
        }
    }

    public void stopThread() {
        this.stopRequested = true;
        if (this.looper != null) {
            this.looper.quit();
        }
    }

    public void suspendThread() {
        this.suspendAutoRefresh = true;
    }
}
