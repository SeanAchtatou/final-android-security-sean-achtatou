package com.qwapi.adclient.android;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Build;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import com.qwapi.adclient.android.utils.Utils;
import java.util.Locale;

public class DeviceContext extends PhoneStateListener {
    private String androidId;
    private CellLocation cellLocation;
    private String countryCode;
    private String deviceId;
    private double lat = 0.0d;
    private double lng = 0.0d;
    private String networkOperator;
    private String networkOperatorName;
    private int networkType;
    private String phoneNumber;
    private int phoneType;
    private boolean wifiEnabled;

    public DeviceContext(Context context) {
        Location lastKnownLocation;
        TelephonyManager telephonyManager;
        if (context.checkPermission("android.permission.READ_PHONE_STATE", Binder.getCallingPid(), Binder.getCallingUid()) == 0 && (telephonyManager = (TelephonyManager) context.getSystemService("phone")) != null) {
            this.deviceId = telephonyManager.getDeviceId();
            this.phoneNumber = telephonyManager.getLine1Number();
            this.countryCode = telephonyManager.getNetworkCountryIso();
            this.networkOperator = telephonyManager.getNetworkOperator();
            this.networkType = telephonyManager.getNetworkType();
            this.phoneType = telephonyManager.getPhoneType();
            Log.d(AdApiConstants.SDK, "phoneNumber:" + this.phoneNumber);
            Log.d(AdApiConstants.SDK, "country code:" + this.countryCode);
            Log.d(AdApiConstants.SDK, "Network Operator:" + this.networkOperator);
            Log.d(AdApiConstants.SDK, "Network Type:" + this.networkType);
            Log.d(AdApiConstants.SDK, "Phone type:" + this.phoneType);
            if (context.checkPermission("android.permission.ACCESS_COARSE_LOCATION", Binder.getCallingPid(), Binder.getCallingUid()) == 0) {
            }
        }
        this.androidId = Settings.System.getString(context.getContentResolver(), "android_id");
        Log.d(AdApiConstants.SDK, "AndroidID = " + this.androidId);
        if (context.checkPermission("android.permission.ACCESS_FINE_LOCATION", Binder.getCallingPid(), Binder.getCallingUid()) == 0 && (lastKnownLocation = ((LocationManager) context.getSystemService("location")).getLastKnownLocation("gps")) != null) {
            this.lat = lastKnownLocation.getLatitude();
            this.lng = lastKnownLocation.getLongitude();
            Log.d(AdApiConstants.SDK, "Lat:" + this.lat);
            Log.d(AdApiConstants.SDK, "Lon:" + this.lng);
        }
    }

    public static String getDeviceId(Context context) {
        return Settings.System.getString(context.getContentResolver(), "android_id");
    }

    public String getAndroidId() {
        return this.androidId == null ? "ANDROID_SIMULATOR_0" : this.androidId;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public String getDeviceId() {
        return this.androidId == null ? "ANDROID_SIMULATOR_0" : this.androidId;
    }

    public double getLatitude() {
        return this.lat;
    }

    public double getLongitude() {
        return this.lng;
    }

    public String getNetworkOperator() {
        return this.networkOperator;
    }

    public String getNetworkOperatorName() {
        return this.networkOperatorName;
    }

    public int getNetworkType() {
        return this.networkType;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public int getPhoneType() {
        return this.phoneType;
    }

    public String getUserAgent() {
        return Utils.encode(new StringBuffer().append("Android_").append(Locale.getDefault().getCountry()).append("_").append(Build.BRAND).append("_").append(Build.MODEL).append("_").append(Build.PRODUCT).append("_").append(Build.VERSION.SDK).append("_").append(AdApiConstants.SDK).toString(), null);
    }

    public boolean isWifiEnabled() {
        return this.wifiEnabled;
    }

    public void onCellLocationChanged(CellLocation cellLocation2) {
        super.onCellLocationChanged(cellLocation2);
        this.cellLocation = cellLocation2;
        if (this.cellLocation != null) {
            try {
                GsmCellLocation gsmCellLocation = (GsmCellLocation) this.cellLocation;
                if (gsmCellLocation != null) {
                    Log.d(AdApiConstants.SDK, "CellId:" + gsmCellLocation.getCid());
                    Log.d(AdApiConstants.SDK, "cell Location Area Code:" + gsmCellLocation.getLac());
                }
            } catch (ClassCastException e) {
                Log.e(AdApiConstants.SDK, e.getMessage());
            }
        }
    }
}
