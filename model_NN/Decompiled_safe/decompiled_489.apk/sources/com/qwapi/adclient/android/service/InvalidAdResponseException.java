package com.qwapi.adclient.android.service;

public class InvalidAdResponseException extends Exception {
    private static final long serialVersionUID = 1;

    public InvalidAdResponseException() {
    }

    public InvalidAdResponseException(String str) {
        super(str);
    }

    public InvalidAdResponseException(String str, Throwable th) {
        super(str, th);
    }

    public InvalidAdResponseException(Throwable th) {
        super(th);
    }
}
