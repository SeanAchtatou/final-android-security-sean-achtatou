package com.qwapi.adclient.android.utils;

public class HttpResponse {
    private String response;
    private int returnCode;

    public HttpResponse(String str, int i) {
        this.response = str;
        this.returnCode = i;
    }

    public String getResponse() {
        return this.response;
    }

    public int getReturnCode() {
        return this.returnCode;
    }

    public void setResponse(String str) {
        this.response = str;
    }

    public void setReturnCode(int i) {
        this.returnCode = i;
    }
}
