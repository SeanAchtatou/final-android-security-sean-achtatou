package com.google.ads;

import android.content.Context;
import android.util.Log;
import com.google.ads.AdSpec;
import com.qwapi.adclient.android.view.AdViewConstants;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

final class AdUtil {
    private static final String LOGTAG = "Google.AdUtil";
    private static float density = -1.0f;

    private AdUtil() {
    }

    static int scalePixelsToDips(Context context, int size) {
        if (density < 0.0f) {
            density = context.getResources().getDisplayMetrics().density;
        }
        return (int) (((float) size) / density);
    }

    static int scaleDipsToPixels(Context context, int size) {
        if (density < 0.0f) {
            density = context.getResources().getDisplayMetrics().density;
        }
        return (int) (((float) size) * density);
    }

    static String generateJSONParameters(List<AdSpec.Parameter> parameters) {
        JSONObject jsonParams = new JSONObject();
        for (AdSpec.Parameter param : parameters) {
            String name = param.getName();
            String value = param.getValue();
            try {
                jsonParams = jsonParams.put(name, value);
            } catch (JSONException e) {
                Log.w(LOGTAG, "Error encoding JSON: " + name + AdViewConstants.EQUALS + value);
            }
        }
        return jsonParams.toString();
    }
}
