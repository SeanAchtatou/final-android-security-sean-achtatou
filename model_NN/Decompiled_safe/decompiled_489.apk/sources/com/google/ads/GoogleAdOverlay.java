package com.google.ads;

import android.content.Context;
import android.graphics.Rect;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import com.google.ads.AdSpec;
import com.google.ads.AdViewCommunicator;
import java.util.ArrayList;
import java.util.List;

class GoogleAdOverlay extends PopupWindow {
    private static final float BACKGROUND_DIM_AMOUNT = 0.5f;
    private FrameLayout mAdContainer;
    private View mParent;
    private WebView mWebView;

    public GoogleAdOverlay(Context context, View parent, WebView webView) {
        super(context);
        this.mParent = parent;
        this.mWebView = webView;
        init(context);
    }

    private void init(Context context) {
        setBackgroundDrawable(null);
        setFocusable(true);
        this.mAdContainer = new AdContainer(context);
        this.mAdContainer.setBackgroundDrawable(null);
        setContentView(this.mAdContainer);
    }

    private class AdContainer extends FrameLayout {
        public AdContainer(Context context) {
            super(context);
        }

        public boolean onTouchEvent(MotionEvent event) {
            int x = (int) event.getX();
            int y = (int) event.getY();
            if (event.getAction() == 0 && (x < 0 || x >= getWidth() || y < 0 || y >= getHeight())) {
                GoogleAdOverlay.this.sendOutsideTouchMessage(x, y);
                return true;
            } else if (event.getAction() != 4) {
                return super.onTouchEvent(event);
            } else {
                GoogleAdOverlay.this.sendOutsideTouchMessage(x, y);
                return true;
            }
        }

        public boolean dispatchKeyEvent(KeyEvent event) {
            if (event.getKeyCode() != 4 || event.getAction() != 0) {
                return super.dispatchKeyEvent(event);
            }
            GoogleAdOverlay.this.sendCloseMessage();
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void sendOutsideTouchMessage(int xPixels, int yPixels) {
        int[] viewLocation = new int[2];
        this.mWebView.getLocationInWindow(viewLocation);
        int leftPixels = viewLocation[0];
        int topPixels = viewLocation[1];
        Rect windowDimensions = new Rect();
        this.mParent.getWindowVisibleDisplayFrame(windowDimensions);
        Context context = this.mWebView.getContext();
        List<AdSpec.Parameter> dictionary = new ArrayList<>();
        dictionary.add(new AdSpec.Parameter("click_x", Integer.toString(AdUtil.scalePixelsToDips(context, xPixels + leftPixels))));
        dictionary.add(new AdSpec.Parameter("click_y", Integer.toString(AdUtil.scalePixelsToDips(context, yPixels + topPixels))));
        dictionary.add(new AdSpec.Parameter("ad_x", Integer.toString(AdUtil.scalePixelsToDips(context, leftPixels))));
        dictionary.add(new AdSpec.Parameter("ad_y", Integer.toString(AdUtil.scalePixelsToDips(context, topPixels))));
        dictionary.add(new AdSpec.Parameter("ad_width", Integer.toString(AdUtil.scalePixelsToDips(context, this.mWebView.getWidth()))));
        dictionary.add(new AdSpec.Parameter("ad_height", Integer.toString(AdUtil.scalePixelsToDips(context, this.mWebView.getHeight()))));
        dictionary.add(new AdSpec.Parameter("screen_width", Integer.toString(AdUtil.scalePixelsToDips(context, windowDimensions.width()))));
        dictionary.add(new AdSpec.Parameter("screen_height", Integer.toString(AdUtil.scalePixelsToDips(context, windowDimensions.height()))));
        AdViewCommunicator.sendJavaScriptMessage(this.mWebView, AdViewCommunicator.JsMessageAction.JS_OUTSIDE_CLICK_MESSAGE, dictionary);
    }

    /* access modifiers changed from: private */
    public void sendCloseMessage() {
        sendOutsideTouchMessage(-1, -1);
    }

    public void resize(int width, int height) {
        setWidth(AdUtil.scaleDipsToPixels(this.mParent.getContext(), width));
        setHeight(AdUtil.scaleDipsToPixels(this.mParent.getContext(), height));
    }

    public void removeAllViewsAndDismiss() {
        this.mAdContainer.removeAllViews();
        dismiss();
    }

    public void removeAllViews() {
        this.mAdContainer.removeAllViews();
    }

    public void addView(View view, int width, int height) {
        removeAllViews();
        this.mAdContainer.addView(view, width, height);
    }

    public void addView(View view) {
        removeAllViews();
        this.mAdContainer.addView(view);
    }

    public void addAndShowWebView(int leftPixels, int topPixels) {
        removeAllViews();
        this.mAdContainer.addView(this.mWebView);
        showAtLocation(this.mParent, 0, leftPixels, topPixels);
    }

    public void dimBackground() {
        WindowManager.LayoutParams lp = (WindowManager.LayoutParams) this.mAdContainer.getLayoutParams();
        if (lp != null) {
            lp.flags |= 2;
            lp.dimAmount = BACKGROUND_DIM_AMOUNT;
            ((WindowManager) this.mWebView.getContext().getSystemService("window")).updateViewLayout(this.mAdContainer, this.mAdContainer.getLayoutParams());
        }
    }
}
