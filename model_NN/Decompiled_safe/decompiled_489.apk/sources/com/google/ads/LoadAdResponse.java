package com.google.ads;

import android.util.Log;
import java.util.Map;

class LoadAdResponse implements AdResponse {
    private static final String LOGTAG = "LoadAdResponse";

    LoadAdResponse() {
    }

    public void run(Map<String, String> paramMap, GoogleAdView adView) {
        String adUrl = paramMap.get("src");
        if (adUrl == null) {
            Log.e(LOGTAG, "ERROR: src parameter not found in loadAdUrl");
        } else {
            adView.loadAdFromUrl(adUrl);
        }
    }
}
