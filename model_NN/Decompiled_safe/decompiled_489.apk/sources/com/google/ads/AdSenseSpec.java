package com.google.ads;

import android.content.Context;
import android.content.pm.PackageManager;
import com.adwhirl.util.AdWhirlUtil;
import com.google.ads.AdSpec;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdSenseSpec implements AdSpec {
    private static final String APP_URL_MODEL_WITH_SEPARATORS = ".android.";
    private static final String JS_PARAM = "afma-sdk-a-v1.7";
    private static final String PLATFORM = "Android";
    private boolean mAdTestEnabled = true;
    private AdType mAdType;
    private String mAlternateAdUrl;
    private String mAlternateColor;
    private String mAppName;
    private String mChannel;
    private String mClientId;
    private String mColorBackground = "000000";
    private String mColorBorder = "000000";
    private String mColorLink = "FFFFFF";
    private String mColorText = "FFFFFF";
    private String mColorUrl = "14b869";
    private String mCompanyName;
    private ExpandDirection mExpandDirection;
    private AdFormat mFormat = AdFormat.FORMAT_320x50;
    private String mKeywords;
    private String mWebEquivalentUrl;

    public AdSenseSpec(String clientId) {
        checkNotNullOrEmpty(clientId, "ClientId");
        this.mClientId = clientId;
    }

    private void checkNotNullOrEmpty(String param, String paramName) {
        if (param == null) {
            throw new NullPointerException(paramName + " cannot be null.");
        } else if (param.length() <= 0) {
            throw new IllegalArgumentException(paramName + " cannot be empty.");
        }
    }

    private void checkNotEmpty(String param, String paramName) {
        if (param != null && param.length() <= 0) {
            throw new IllegalArgumentException(paramName + " cannot be empty.");
        }
    }

    public List<AdSpec.Parameter> generateParameters(Context context) {
        if (this.mAppName == null) {
            throw new IllegalStateException("AppName must be set before calling generateParameters().");
        } else if (this.mCompanyName == null) {
            throw new IllegalStateException("CompanyName must be set before calling generateParameters().");
        } else {
            ArrayList<AdSpec.Parameter> params = new ArrayList<>();
            params.add(new AdSpec.Parameter("client", this.mClientId));
            params.add(new AdSpec.Parameter("app_name", generateAppUrl(context)));
            params.add(new AdSpec.Parameter("msid", context.getPackageName()));
            params.add(new AdSpec.Parameter("js", JS_PARAM));
            params.add(new AdSpec.Parameter("platform", PLATFORM));
            params.add(new AdSpec.Parameter("an", this.mAppName));
            params.add(new AdSpec.Parameter("cn", this.mCompanyName));
            params.add(new AdSpec.Parameter("hl", Locale.getDefault().getLanguage().trim().toLowerCase()));
            if (!this.mAdTestEnabled) {
                params.add(new AdSpec.Parameter("adtest", "off"));
            }
            if (this.mAdType != null) {
                params.add(new AdSpec.Parameter("ad_type", AdType.access$000(this.mAdType)));
            }
            if (this.mAlternateAdUrl != null) {
                params.add(new AdSpec.Parameter("alternate_ad_url", this.mAlternateAdUrl));
            }
            if (this.mAlternateColor != null) {
                params.add(new AdSpec.Parameter("alt_color", this.mAlternateColor));
            }
            if (this.mChannel != null) {
                params.add(new AdSpec.Parameter("channel", this.mChannel));
            }
            if (this.mColorBackground != null) {
                params.add(new AdSpec.Parameter("color_bg", this.mColorBackground));
            }
            if (this.mColorBorder != null) {
                params.add(new AdSpec.Parameter("color_border", this.mColorBorder));
            }
            if (this.mColorLink != null) {
                params.add(new AdSpec.Parameter("color_link", this.mColorLink));
            }
            if (this.mColorText != null) {
                params.add(new AdSpec.Parameter("color_text", this.mColorText));
            }
            if (this.mColorUrl != null) {
                params.add(new AdSpec.Parameter("color_url", this.mColorUrl));
            }
            if (this.mFormat != null) {
                params.add(new AdSpec.Parameter("format", this.mFormat.getFormatString()));
            }
            if (this.mKeywords != null) {
                params.add(new AdSpec.Parameter("kw", this.mKeywords));
            }
            if (this.mWebEquivalentUrl != null) {
                params.add(new AdSpec.Parameter("url", this.mWebEquivalentUrl));
            }
            if (this.mExpandDirection != null) {
                params.add(new AdSpec.Parameter("xdir", this.mExpandDirection.getFormatString()));
            }
            return params;
        }
    }

    public boolean getDebugMode() {
        return this.mAdTestEnabled;
    }

    public int getWidth() {
        return getAdFormat().getWidth();
    }

    public int getHeight() {
        return getAdFormat().getHeight();
    }

    private String generateAppUrl(Context context) {
        String packageName = context.getPackageName();
        try {
            return context.getPackageManager().getPackageInfo(packageName, 0).versionCode + APP_URL_MODEL_WITH_SEPARATORS + packageName;
        } catch (PackageManager.NameNotFoundException e) {
            return APP_URL_MODEL_WITH_SEPARATORS + packageName;
        }
    }

    public String getClientId() {
        return this.mClientId;
    }

    public AdSenseSpec setClientId(String clientId) {
        checkNotNullOrEmpty(clientId, "ClientId");
        this.mClientId = clientId;
        return this;
    }

    public boolean getAdTestEnabled() {
        return this.mAdTestEnabled;
    }

    public AdSenseSpec setAdTestEnabled(boolean enabled) {
        this.mAdTestEnabled = enabled;
        return this;
    }

    public AdType getAdType() {
        return this.mAdType;
    }

    public AdSenseSpec setAdType(AdType adType) {
        this.mAdType = adType;
        return this;
    }

    public String getAppName() {
        return this.mAppName;
    }

    public AdSenseSpec setAppName(String appName) {
        checkNotNullOrEmpty(appName, "AppName");
        this.mAppName = appName;
        return this;
    }

    public String getChannel() {
        return this.mChannel;
    }

    public AdSenseSpec setChannel(String channel) {
        checkNotEmpty(channel, "Channel");
        this.mChannel = channel;
        return this;
    }

    public String getAlternateAdUrl() {
        return this.mAlternateAdUrl;
    }

    public AdSenseSpec setAlternateAdUrl(String alternateAdUrl) {
        checkNotEmpty(alternateAdUrl, "AlternateAdUrl");
        this.mAlternateAdUrl = alternateAdUrl;
        return this;
    }

    public String getAlternateColor() {
        return this.mAlternateColor;
    }

    public AdSenseSpec setAlternateColor(String alternateColor) {
        checkNotEmpty(alternateColor, "AlternateColor");
        this.mAlternateColor = alternateColor;
        return this;
    }

    public String getColorBackground() {
        return this.mColorBackground;
    }

    public AdSenseSpec setColorBackground(String colorBackground) {
        checkNotEmpty(colorBackground, "ColorBackground");
        this.mColorBackground = colorBackground;
        return this;
    }

    public String getColorBorder() {
        return this.mColorBorder;
    }

    public AdSenseSpec setColorBorder(String colorBorder) {
        checkNotEmpty(colorBorder, "ColorBorder");
        this.mColorBorder = colorBorder;
        return this;
    }

    public String getColorLink() {
        return this.mColorLink;
    }

    public AdSenseSpec setColorLink(String colorLink) {
        checkNotEmpty(colorLink, "ColorLink");
        this.mColorLink = colorLink;
        return this;
    }

    public String getColorText() {
        return this.mColorText;
    }

    public AdSenseSpec setColorText(String colorText) {
        checkNotEmpty(colorText, "ColorText");
        this.mColorText = colorText;
        return this;
    }

    public String getColorUrl() {
        return this.mColorUrl;
    }

    public AdSenseSpec setColorUrl(String colorUrl) {
        checkNotEmpty(colorUrl, "ColorUrl");
        this.mColorUrl = colorUrl;
        return this;
    }

    public String getCompanyName() {
        return this.mCompanyName;
    }

    public AdSenseSpec setCompanyName(String companyName) {
        checkNotNullOrEmpty(companyName, "CompanyName");
        this.mCompanyName = companyName;
        return this;
    }

    public AdFormat getAdFormat() {
        if (this.mFormat == null) {
            return AdFormat.FORMAT_320x50;
        }
        return this.mFormat;
    }

    public AdSenseSpec setAdFormat(AdFormat format) {
        this.mFormat = format;
        return this;
    }

    public String getKeywords() {
        return this.mKeywords;
    }

    public AdSenseSpec setKeywords(String keywords) {
        checkNotEmpty(keywords, "Keywords");
        this.mKeywords = keywords;
        return this;
    }

    public String getWebEquivalentUrl() {
        return this.mWebEquivalentUrl;
    }

    public AdSenseSpec setWebEquivalentUrl(String webEquivalentUrl) {
        checkNotEmpty(webEquivalentUrl, "WebEquivalentUrl");
        this.mWebEquivalentUrl = webEquivalentUrl;
        return this;
    }

    public AdSenseSpec setExpandDirection(ExpandDirection direction) {
        this.mExpandDirection = direction;
        return this;
    }

    public ExpandDirection getExpandDirection() {
        return this.mExpandDirection;
    }

    public enum AdType {
        TEXT("text"),
        IMAGE("image"),
        TEXT_IMAGE("text_image");
        
        private String mTypeName;

        private AdType(String typeName) {
            this.mTypeName = typeName;
        }

        private String getTypeName() {
            return this.mTypeName;
        }
    }

    public enum AdFormat {
        FORMAT_320x50("320x50_mb", 320, 50),
        FORMAT_300x250("300x250_as", AdWhirlUtil.VERSION, 250);
        
        private String mFormatString;
        private int mHeight;
        private int mWidth;

        private AdFormat(String formatString, int width, int height) {
            this.mFormatString = formatString;
            this.mWidth = width;
            this.mHeight = height;
        }

        public String getFormatString() {
            return this.mFormatString;
        }

        public int getWidth() {
            return this.mWidth;
        }

        public int getHeight() {
            return this.mHeight;
        }
    }

    public enum ExpandDirection {
        TOP("t"),
        BOTTOM("b");
        
        private String mFormatString;

        private ExpandDirection(String directionString) {
            this.mFormatString = directionString;
        }

        /* access modifiers changed from: package-private */
        public String getFormatString() {
            return this.mFormatString;
        }
    }
}
