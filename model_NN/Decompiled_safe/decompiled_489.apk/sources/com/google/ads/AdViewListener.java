package com.google.ads;

public interface AdViewListener {
    void onClickAd();

    void onFinishFetchAd();

    void onStartFetchAd();
}
