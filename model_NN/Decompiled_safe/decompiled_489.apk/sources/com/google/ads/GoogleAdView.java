package com.google.ads;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ViewSwitcher;
import com.google.ads.AdSpec;
import java.io.IOException;
import java.util.List;
import java.util.Random;

public class GoogleAdView extends ViewSwitcher {
    private static final String AD_FOOTER = ";\n</script>\n<script type='text/javascript' src='http://pagead2.googlesyndication.com/pagead/afma_load_ads.js'></script>\n</body>\n</html>";
    private static final String AD_HEADER = "<html>\n<body marginwidth='0' marginheight='0'>\n<script type='text/javascript'>\nwindow.google_afma_request = ";
    private static final String AD_URL = "http://pagead2.googlesyndication.com/pagead/afma_load_ads.js";
    private static final String DEBUG_WATERMARK_FILE = "test_ad.png";
    private static final String DEFAULT_ENCODING = "utf8";
    private static final int DEFAULT_HEIGHT = 50;
    private static final String DEFAULT_MIME_TYPE = "text/html";
    private static final int DEFAULT_WIDTH = 320;
    private static final boolean ENABLE_LOCAL_URL_CAPTURE = true;
    private static final boolean ENABLE_REDIRECT_OPTIMIZATION = false;
    /* access modifiers changed from: private */
    public static final String[] LOCAL_URLS = {"about:blank"};
    private static final String LOGTAG = "GoogleAdView";
    public static final int MINIMUM_AUTO_REFRESH_SECONDS = 180;
    private static final String PARAM_AUTO_REFRESH = "ar";
    private static final String PARAM_CLICK_LATENCY = "pcl";
    private static final String PARAM_CLICK_STRING = "ai";
    private static final String PARAM_LATENCY = "prl";
    private static final String PARAM_PRIOR_CLICK_STRING = "pai";
    private static final String PARAM_SPACING_BOTTOM = "bsp";
    private static final String PARAM_SPACING_LEFT = "lsp";
    private static final String PARAM_SPACING_RIGHT = "rsp";
    private static final String PARAM_SPACING_TOP = "tsp";
    private static final String PARENT_STATE = "google_ad_view_parent_state";
    private static final int PROGRESS_BAR_INDEX = 0;
    /* access modifiers changed from: private */
    public static final String[] REDIRECT_DOMAINS = {"googleads.g.doubleclick.net", "googleadservices.com"};
    private static final int WEBVIEW_INDEX = 1;
    static final int WINDOW_EXPAND_DELAY = 150;
    static final int WINDOW_RETRACT_DELAY = 400;
    private int mAdHeight;
    private FrameLayout mAdViewHolder;
    /* access modifiers changed from: private */
    public AdViewListener mAdViewListener;
    private AdWebViewClient mAdWebViewClient;
    private int mAdWidth;
    /* access modifiers changed from: private */
    public int mAutoRefreshSecs;
    /* access modifiers changed from: private */
    public AdViewCommunicator mCommunicator;
    private Drawable mDebugDrawable;
    private boolean mDebugMode;
    /* access modifiers changed from: private */
    public boolean mExpanded;
    /* access modifiers changed from: private */
    public AdSpec mLastAdSpec;
    LatencyTracker mLatencyTracker;
    /* access modifiers changed from: private */
    public GoogleAdOverlay mOverlay;
    private WebViewPlaceHolder mPlaceHolder;
    LinearLayout mProgressBarLayout;
    private Runnable mRefreshRunnable;
    private boolean mShowAdsDeferred;
    /* access modifiers changed from: private */
    public WebView mWebView;

    public GoogleAdView(Context context) {
        super(context);
        init(context, new WebView(context), DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public GoogleAdView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, new WebView(context), DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    GoogleAdView(Context context, WebView webView) {
        super(context);
        init(context, webView, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    private void init(Context context, WebView webView, int width, int height) {
        ProgressBar progressBar = new ProgressBar(context, null, 16842873);
        progressBar.setIndeterminate(ENABLE_LOCAL_URL_CAPTURE);
        this.mProgressBarLayout = new LinearLayout(context);
        this.mProgressBarLayout.setGravity(17);
        this.mProgressBarLayout.addView(progressBar);
        addView(this.mProgressBarLayout, AdUtil.scaleDipsToPixels(context, width), AdUtil.scaleDipsToPixels(context, height));
        this.mCommunicator = new AdViewCommunicator(this);
        registerAdResponses();
        this.mWebView = webView;
        this.mAdWebViewClient = new AdWebViewClient();
        this.mWebView.setBackgroundColor(0);
        this.mWebView.setWebViewClient(this.mAdWebViewClient);
        this.mWebView.setInitialScale(AdUtil.scaleDipsToPixels(context, 100));
        this.mAdViewHolder = new FrameLayout(context);
        this.mAdViewHolder.setBackgroundColor(0);
        this.mAdViewHolder.setBackgroundDrawable(null);
        this.mAdViewHolder.addView(this.mWebView);
        addView(this.mAdViewHolder, AdUtil.scaleDipsToPixels(context, width), AdUtil.scaleDipsToPixels(context, height));
        this.mOverlay = new GoogleAdOverlay(context, this, webView);
        this.mPlaceHolder = new WebViewPlaceHolder(context);
        this.mWebView.setVerticalScrollBarEnabled(ENABLE_REDIRECT_OPTIMIZATION);
        this.mWebView.setHorizontalScrollBarEnabled(ENABLE_REDIRECT_OPTIMIZATION);
        ViewGroup.LayoutParams viewParams = this.mWebView.getLayoutParams();
        viewParams.width = -1;
        viewParams.height = -1;
        WebSettings settings = this.mWebView.getSettings();
        settings.setJavaScriptEnabled(ENABLE_LOCAL_URL_CAPTURE);
        settings.setSupportZoom(ENABLE_REDIRECT_OPTIMIZATION);
        settings.setCacheMode(0);
        this.mLatencyTracker = new LatencyTracker(context);
        this.mExpanded = ENABLE_REDIRECT_OPTIMIZATION;
        setOutAnimation(context, 17432577);
        setInAnimation(context, 17432576);
        this.mDebugMode = ENABLE_REDIRECT_OPTIMIZATION;
        this.mAutoRefreshSecs = -1;
        this.mRefreshRunnable = new RefreshRunnable();
    }

    private void registerAdResponses() {
        this.mCommunicator.registerAdResponse("/loadAdURL", new LoadAdResponse());
        this.mCommunicator.registerAdResponse("/resize", new ResizeResponse());
    }

    public AdViewListener getAdViewListener() {
        return this.mAdViewListener;
    }

    public void setAdViewListener(AdViewListener adViewListener) {
        this.mAdViewListener = adViewListener;
    }

    public int getAutoRefreshSeconds() {
        return this.mAutoRefreshSecs;
    }

    public void setAutoRefreshSeconds(int seconds) {
        if (seconds <= 0) {
            this.mAutoRefreshSecs = -1;
            return;
        }
        if (seconds < 180) {
            seconds = MINIMUM_AUTO_REFRESH_SECONDS;
        }
        this.mAutoRefreshSecs = seconds;
        scheduleAutoRefresh();
    }

    public void showAds(AdSpec adSpec) {
        showAds(adSpec, ENABLE_REDIRECT_OPTIMIZATION);
    }

    /* access modifiers changed from: private */
    public void showAds(AdSpec adSpec, boolean isAutoRefresh) {
        showAds(adSpec, isAutoRefresh, hasWindowFocus());
    }

    /* access modifiers changed from: package-private */
    public void showAds(AdSpec adSpec, boolean isAutoRefresh, boolean hasWindowFocus) {
        this.mShowAdsDeferred = ENABLE_REDIRECT_OPTIMIZATION;
        this.mLastAdSpec = adSpec;
        if (!hasWindowFocus) {
            this.mShowAdsDeferred = ENABLE_LOCAL_URL_CAPTURE;
            return;
        }
        if (this.mExpanded) {
            closeAdImmediately();
        }
        resize(adSpec.getWidth(), adSpec.getHeight());
        this.mDebugMode = adSpec.getDebugMode();
        String htmlString = generateHtml(adSpec, isAutoRefresh);
        if (this.mDebugMode) {
            Log.i(LOGTAG, "Fetching ad: " + htmlString);
            getDebugDrawable();
        }
        this.mWebView.loadData(htmlString, "text/html", DEFAULT_ENCODING);
        scheduleAutoRefresh();
    }

    /* access modifiers changed from: package-private */
    public void loadAdFromUrl(String address) {
        this.mWebView.loadUrl(address);
    }

    public void reset() {
        setDisplayedChild(0);
        this.mWebView.stopLoading();
        this.mWebView.clearView();
        this.mAdWebViewClient.reset();
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if (this.mShowAdsDeferred && hasWindowFocus && this.mLastAdSpec != null && !this.mExpanded) {
            showAds(this.mLastAdSpec, ENABLE_REDIRECT_OPTIMIZATION);
        }
        if (hasWindowFocus) {
            this.mLatencyTracker.onWindowGetFocus();
        }
        scheduleAutoRefresh();
    }

    public void setDisplayedChild(int child) {
        super.setDisplayedChild(child);
        if (!this.mDebugMode || child != 1) {
            setForeground(null);
        } else {
            setForeground(getDebugDrawable());
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Bundle savedState = new Bundle();
        Parcelable parentState = super.onSaveInstanceState();
        if (parentState != null) {
            savedState.putParcelable(PARENT_STATE, parentState);
        }
        this.mLatencyTracker.saveTransientState(savedState);
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcel) {
        Bundle savedState = (Bundle) parcel;
        this.mLatencyTracker.restoreTransientState(savedState);
        Parcelable parentState = savedState.getParcelable(PARENT_STATE);
        if (parentState != null) {
            super.onRestoreInstanceState(parentState);
        }
    }

    private void scheduleAutoRefresh() {
        removeCallbacks(this.mRefreshRunnable);
        if (this.mLastAdSpec != null && this.mAutoRefreshSecs > 0 && hasWindowFocus()) {
            postDelayed(this.mRefreshRunnable, (long) (this.mAutoRefreshSecs * 1000));
        }
    }

    /* access modifiers changed from: package-private */
    public void resize(int width, int height) {
        this.mAdWidth = width;
        this.mAdHeight = height;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            ViewGroup.LayoutParams layoutParams = getChildAt(i).getLayoutParams();
            layoutParams.width = AdUtil.scaleDipsToPixels(getContext(), width);
            layoutParams.height = AdUtil.scaleDipsToPixels(getContext(), height);
        }
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public String generateHtml(AdSpec adSpec, boolean isAutoRefresh) {
        List<AdSpec.Parameter> parameters = adSpec.generateParameters(getContext());
        Context context = getContext();
        int[] viewLocation = new int[2];
        this.mWebView.getLocationOnScreen(viewLocation);
        Rect windowDimensions = new Rect();
        getWindowVisibleDisplayFrame(windowDimensions);
        int leftPadding = viewLocation[0];
        int topPadding = viewLocation[1];
        int bottomPadding = (windowDimensions.height() - this.mWebView.getHeight()) - topPadding;
        int rightPadding = (windowDimensions.width() - this.mWebView.getWidth()) - leftPadding;
        parameters.add(new AdSpec.Parameter(PARAM_SPACING_TOP, Integer.toString(AdUtil.scalePixelsToDips(context, topPadding))));
        parameters.add(new AdSpec.Parameter(PARAM_SPACING_LEFT, Integer.toString(AdUtil.scalePixelsToDips(context, leftPadding))));
        parameters.add(new AdSpec.Parameter(PARAM_SPACING_BOTTOM, Integer.toString(AdUtil.scalePixelsToDips(context, bottomPadding))));
        parameters.add(new AdSpec.Parameter(PARAM_SPACING_RIGHT, Integer.toString(AdUtil.scalePixelsToDips(context, rightPadding))));
        if (isAutoRefresh) {
            parameters.add(new AdSpec.Parameter(PARAM_AUTO_REFRESH, Integer.toString(this.mAutoRefreshSecs)));
        }
        if (this.mLatencyTracker.hasAdFetchLatency()) {
            parameters.add(new AdSpec.Parameter(PARAM_LATENCY, Integer.toString(this.mLatencyTracker.getAdFetchLatency())));
        }
        if (this.mLatencyTracker.hasAdClickLatency()) {
            parameters.add(new AdSpec.Parameter(PARAM_CLICK_LATENCY, Integer.toString(this.mLatencyTracker.getAdClickLatency())));
        }
        if (this.mLatencyTracker.hasClickString()) {
            parameters.add(new AdSpec.Parameter(PARAM_PRIOR_CLICK_STRING, this.mLatencyTracker.getClickString()));
        }
        this.mLatencyTracker.clear();
        return AD_HEADER + AdUtil.generateJSONParameters(parameters) + AD_FOOTER;
    }

    private Drawable getDebugDrawable() {
        Class<Bitmap> cls = Bitmap.class;
        if (this.mDebugDrawable == null) {
            try {
                Bitmap debugBitmap = BitmapFactory.decodeStream(getContext().getAssets().open(DEBUG_WATERMARK_FILE));
                Class<Bitmap> cls2 = Bitmap.class;
                try {
                    cls2.getMethod("setDensity", Integer.TYPE).invoke(debugBitmap, 160);
                    this.mDebugDrawable = BitmapDrawable.class.getConstructor(Resources.class, Bitmap.class).newInstance(getContext().getResources(), debugBitmap);
                } catch (Exception e) {
                    this.mDebugDrawable = new BitmapDrawable(debugBitmap);
                }
            } catch (IOException e2) {
                Log.e(LOGTAG, "Error loading debug watermark", e2);
            }
        }
        return this.mDebugDrawable;
    }

    class AdWebViewClient extends WebViewClient {
        private boolean mStartedAdFetch;
        private Random random = new Random();

        AdWebViewClient() {
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            if (URLUtil.isDataUrl(url)) {
                this.mStartedAdFetch = GoogleAdView.ENABLE_LOCAL_URL_CAPTURE;
                GoogleAdView.this.mLatencyTracker.onAdFetchStart();
                if (GoogleAdView.this.mAdViewListener != null) {
                    GoogleAdView.this.mAdViewListener.onStartFetchAd();
                }
            }
        }

        public void onPageFinished(WebView view, String url) {
            patchDoubleClickBug();
            super.onPageFinished(view, url);
            if (this.mStartedAdFetch && !URLUtil.isDataUrl(url)) {
                this.mStartedAdFetch = GoogleAdView.ENABLE_REDIRECT_OPTIMIZATION;
                GoogleAdView.this.setDisplayedChild(1);
                GoogleAdView.this.mLatencyTracker.onAdFetchFinished();
                if (GoogleAdView.this.mAdViewListener != null) {
                    GoogleAdView.this.mAdViewListener.onFinishFetchAd();
                }
            }
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Uri uri = Uri.parse(url);
            if (AdViewCommunicator.isMessage(uri)) {
                GoogleAdView.this.mCommunicator.testAndForwardMessage(uri);
                return GoogleAdView.ENABLE_LOCAL_URL_CAPTURE;
            } else if (isBlacklisted(uri)) {
                return GoogleAdView.ENABLE_REDIRECT_OPTIMIZATION;
            } else {
                if (GoogleAdView.this.mAdViewListener != null) {
                    GoogleAdView.this.mAdViewListener.onClickAd();
                }
                GoogleAdView.this.mLatencyTracker.onAdClickStart(getClickString(uri));
                Intent intent = new Intent("android.intent.action.VIEW", AFMAUtil.a(uri));
                intent.addCategory("android.intent.category.BROWSABLE");
                GoogleAdView.this.getContext().startActivity(intent);
                return GoogleAdView.ENABLE_LOCAL_URL_CAPTURE;
            }
        }

        private boolean isBlacklisted(Uri uri) {
            String uriString = uri.toString();
            for (String equals : GoogleAdView.LOCAL_URLS) {
                if (uriString.equals(equals)) {
                    return GoogleAdView.ENABLE_LOCAL_URL_CAPTURE;
                }
            }
            return GoogleAdView.ENABLE_REDIRECT_OPTIMIZATION;
        }

        private boolean isRedirect(Uri uri) {
            String host = uri.getHost();
            for (int i = GoogleAdView.REDIRECT_DOMAINS.length - 1; i >= 0; i--) {
                if (host.endsWith(GoogleAdView.REDIRECT_DOMAINS[i])) {
                    return GoogleAdView.ENABLE_LOCAL_URL_CAPTURE;
                }
            }
            return GoogleAdView.ENABLE_REDIRECT_OPTIMIZATION;
        }

        private String getClickString(Uri uri) {
            if (!uri.isHierarchical()) {
                return null;
            }
            return uri.getQueryParameter(GoogleAdView.PARAM_CLICK_STRING);
        }

        /* access modifiers changed from: private */
        public void reset() {
            this.mStartedAdFetch = GoogleAdView.ENABLE_REDIRECT_OPTIMIZATION;
        }

        private void patchDoubleClickBug() {
            GoogleAdView.this.mWebView.loadUrl("javascript: document.body.style.margin = 0;");
        }
    }

    private void saveWebViewPicture() {
        Picture webPicture = this.mWebView.capturePicture();
        this.mPlaceHolder.setSize(this.mWebView.getWidth(), this.mWebView.getHeight());
        this.mPlaceHolder.setPicture(webPicture);
    }

    /* access modifiers changed from: package-private */
    public void expandAd(int top, int bottom, int left, int right) {
        if (!this.mExpanded) {
            this.mExpanded = ENABLE_LOCAL_URL_CAPTURE;
            Context context = getContext();
            int overlayWidth = this.mAdWidth;
            int overlayHeight = this.mAdHeight;
            saveWebViewPicture();
            int[] viewLocation = new int[2];
            this.mWebView.getLocationOnScreen(viewLocation);
            int overlayLeft = AdUtil.scalePixelsToDips(context, viewLocation[0]);
            int overlayTop = AdUtil.scalePixelsToDips(context, viewLocation[1]);
            this.mOverlay.resize(overlayWidth + left + right, overlayHeight + top + bottom);
            this.mAdViewHolder.removeAllViews();
            this.mAdViewHolder.addView(this.mPlaceHolder);
            this.mWebView.setVisibility(4);
            this.mOverlay.addAndShowWebView(AdUtil.scaleDipsToPixels(context, overlayLeft - left), AdUtil.scaleDipsToPixels(context, overlayTop - top));
            postDelayed(new WebViewExpandRunnable(), 150);
        }
    }

    /* access modifiers changed from: package-private */
    public void retractAd() {
        if (this.mExpanded) {
            saveWebViewPicture();
            this.mAdViewHolder.removeAllViews();
            this.mOverlay.removeAllViews();
            this.mAdViewHolder.addView(this.mWebView);
            this.mOverlay.addView(this.mPlaceHolder);
            resize(this.mAdWidth, this.mAdHeight);
            postDelayed(new WebViewRetractRunnable(), 400);
        }
    }

    /* access modifiers changed from: package-private */
    public void closeAdImmediately() {
        if (this.mExpanded) {
            this.mAdViewHolder.removeAllViews();
            this.mOverlay.removeAllViewsAndDismiss();
            this.mAdViewHolder.addView(this.mWebView);
            resize(this.mAdWidth, this.mAdHeight);
            this.mExpanded = ENABLE_REDIRECT_OPTIMIZATION;
        }
    }

    private class WebViewPlaceHolder extends View {
        private int mHeight;
        private Picture mTemporaryPicture;
        private int mWidth;

        public WebViewPlaceHolder(Context context) {
            super(context);
        }

        public void setPicture(Picture temporaryPicture) {
            this.mTemporaryPicture = temporaryPicture;
        }

        public void setSize(int width, int height) {
            this.mWidth = width;
            this.mHeight = height;
        }

        public Picture getPicture() {
            return this.mTemporaryPicture;
        }

        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawPicture(this.mTemporaryPicture, new Rect(0, 0, this.mWidth, this.mHeight));
        }
    }

    private class WebViewExpandRunnable implements Runnable {
        private WebViewExpandRunnable() {
        }

        public void run() {
            if (GoogleAdView.this.mExpanded) {
                GoogleAdView.this.mWebView.setVisibility(0);
                GoogleAdView.this.mOverlay.dimBackground();
            }
        }
    }

    private class WebViewRetractRunnable implements Runnable {
        private WebViewRetractRunnable() {
        }

        public void run() {
            if (GoogleAdView.this.mExpanded) {
                GoogleAdView.this.mOverlay.removeAllViewsAndDismiss();
                boolean unused = GoogleAdView.this.mExpanded = GoogleAdView.ENABLE_REDIRECT_OPTIMIZATION;
            }
        }
    }

    private class RefreshRunnable implements Runnable {
        private RefreshRunnable() {
        }

        public void run() {
            if (GoogleAdView.this.mLastAdSpec != null && GoogleAdView.this.hasWindowFocus()) {
                if (!GoogleAdView.this.mExpanded) {
                    GoogleAdView.this.showAds(GoogleAdView.this.mLastAdSpec, GoogleAdView.ENABLE_LOCAL_URL_CAPTURE);
                }
                if (GoogleAdView.this.mAutoRefreshSecs > 0) {
                    GoogleAdView.this.postDelayed(this, (long) (GoogleAdView.this.mAutoRefreshSecs * 1000));
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public WebView getWebView() {
        return this.mWebView;
    }

    /* access modifiers changed from: package-private */
    public AdViewCommunicator getCommunicator() {
        return this.mCommunicator;
    }

    /* access modifiers changed from: package-private */
    public boolean isExpanded() {
        return this.mExpanded;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (this.mExpanded) {
            closeAdImmediately();
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: package-private */
    public int getAdWidth() {
        return this.mAdWidth;
    }

    /* access modifiers changed from: package-private */
    public int getAdHeight() {
        return this.mAdHeight;
    }
}
