package com.google.ads;

import android.content.Context;
import com.google.ads.AdSpec;
import java.util.ArrayList;
import java.util.List;

public class DoubleClickSpec implements AdSpec {
    private static final int DOUBLECLICK_DEFAULT_HEIGHT = 50;
    private static final int DOUBLECLICK_DEFAULT_WIDTH = 320;
    private String mColorBackground;
    private String mKeyname;
    private SizeProfile mSizeProfile;

    public enum SizeProfile {
        T,
        S,
        M,
        L,
        XL
    }

    public DoubleClickSpec(String keyname) {
        this.mKeyname = keyname;
    }

    public List<AdSpec.Parameter> generateParameters(Context context) {
        ArrayList<AdSpec.Parameter> params = new ArrayList<>();
        params.add(new AdSpec.Parameter("k", this.mKeyname));
        if (this.mColorBackground != null) {
            params.add(new AdSpec.Parameter("color_bg", this.mColorBackground));
        }
        if (this.mSizeProfile != null) {
            params.add(new AdSpec.Parameter("sp", this.mSizeProfile.name().toLowerCase()));
        }
        return params;
    }

    public boolean getDebugMode() {
        return false;
    }

    public int getWidth() {
        return DOUBLECLICK_DEFAULT_WIDTH;
    }

    public int getHeight() {
        return DOUBLECLICK_DEFAULT_HEIGHT;
    }

    public String getKeyname() {
        return this.mKeyname;
    }

    public DoubleClickSpec setKeyname(String keyname) {
        this.mKeyname = keyname;
        return this;
    }

    public String getColorBackground() {
        return this.mColorBackground;
    }

    public DoubleClickSpec setColorBackground(String colorBackground) {
        this.mColorBackground = colorBackground;
        return this;
    }

    public SizeProfile getSizeProfile() {
        return this.mSizeProfile;
    }

    public DoubleClickSpec setSizeProfile(SizeProfile sizeProfile) {
        this.mSizeProfile = sizeProfile;
        return this;
    }
}
