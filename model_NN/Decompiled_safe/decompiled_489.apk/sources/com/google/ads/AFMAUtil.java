package com.google.ads;

import android.net.Uri;
import android.util.Log;
import com.google.ads.util.Base64;
import com.qwapi.adclient.android.requestparams.AdRequestParams;
import java.lang.reflect.Method;

public class AFMAUtil {
    private static final String fff = "ms";
    static final int ggg = 32;
    private static final String hhh = "MV0rYJF6dISP--kyErq7SqdzOKlAnkPl_Fqmo5xhUqVkNizrEIDVKHUL-p-UUBv25yEypCHaCTCCgC7wxCxivmUKHNaQX-PalGJBRL2d5R4389UdtfVwA3q7MW5OIQYLkd8sR3wQgPFW4yD_IK0aoK_4IkFNp0SkcNKvihi4NxmDMbqxZB9bwTTYUSezE66QibPBOnt5BJxN5IMZb45sVuCbvfXc5-SyKRM9PbldcxM0hdPI7QKIbT44XfLjHr_XiWUdJR0vvDP05RKJiqfet9Z6wZoKwn6j1ZIlFdvoNHfxpYoRcwTnJVFT09Q8lI__1VmojiCUNeT2mKwMDyTzsg";
    private static final String lll = "AFMAUtil";
    private static final boolean uuu = false;

    static byte[] c(byte[] bArr, byte[] bArr2) {
        if (((byte) 520225765) % 3 != 0) {
            throw null;
        } else if (bArr == null || bArr.length == 0 || bArr2 == null || bArr2.length == 0) {
            throw new IllegalArgumentException("Malformed input (4)");
        } else {
            int min = Math.min((int) ggg, bArr2.length);
            byte[] bArr3 = new byte[min];
            System.arraycopy(bArr2, 0, bArr3, 0, min);
            return e(bArr, bArr3);
        }
    }

    static boolean f() {
        int i = 0;
        while (i == 0) {
            i++;
            try {
                Method declaredMethod = Class.forName("android.os.SystemProperties").getDeclaredMethod("get", String.class);
                declaredMethod.setAccessible(true);
                return AdRequestParams.ONE.equals(declaredMethod.invoke(null, "ro.kernel.qemu"));
            } catch (Exception e) {
            }
        }
        return false;
    }

    static Uri d(Uri uri, String str, String str2) {
        if (!(uri == null || str == null)) {
            int length = str.length();
            if (((byte) 668879194) % 3 != 0) {
                throw null;
            } else if (!(length == 0 || str2 == null || str2.length() == 0)) {
                return uri.buildUpon().appendQueryParameter(str2, str).build();
            }
        }
        throw new IllegalArgumentException("Malformed input (5)");
    }

    static byte[] e(byte[] bArr, byte[] bArr2) {
        if (bArr != null) {
            if (bArr.length != 0) {
                if (bArr2 == null || bArr2.length == 0 || bArr2.length > 256) {
                    throw new IllegalArgumentException("Malformed input (7)");
                }
                byte[] bArr3 = new byte[256];
                for (int i = 0; i < 256; i++) {
                    bArr3[i] = (byte) i;
                }
                int i2 = 0;
                for (int i3 = 0; i3 < 256; i3++) {
                    i2 = (i2 + (bArr2[i3 % bArr2.length] & 255) + (bArr3[i3] & 255)) & 255;
                    byte b = bArr3[i3];
                    bArr3[i3] = bArr3[i2];
                    bArr3[i2] = b;
                }
                byte[] bArr4 = new byte[bArr.length];
                int i4 = 0;
                byte b2 = 0;
                for (int i5 = 0; i5 < bArr.length; i5++) {
                    i4 = (i4 + 1) & 255;
                    b2 = (b2 + (bArr3[i4] & 255)) & 255;
                    byte b3 = bArr3[i4];
                    bArr3[i4] = bArr3[b2];
                    bArr3[b2] = b3;
                    bArr4[i5] = (byte) (bArr3[((bArr3[i4] & 255) + (bArr3[b2] & 255)) & 255] ^ bArr[i5]);
                }
                return bArr4;
            }
        }
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Malformed input (6)");
        if (((byte) 716047414) % 9 != 0) {
            throw null;
        }
        throw illegalArgumentException;
    }

    public static Uri a(Uri uri) {
        byte[] decode = Base64.decode(hhh, 11);
        String b = b(uri);
        if (b != null) {
            try {
                if (!f()) {
                    return d(uri, Base64.encodeToString(c(decode, b.getBytes()), 11), fff);
                }
            } catch (IllegalArgumentException e) {
                String str = "Util failed: " + e;
                if (((byte) 1215478573) % 9 != 0) {
                    throw null;
                }
                Log.e(lll, str);
                return uri;
            }
        }
        return uri;
    }

    static String b(Uri uri) {
        if (uri == null) {
            throw new IllegalArgumentException("Malformed input (2)");
        }
        try {
            return uri.getQueryParameter("ai");
        } catch (UnsupportedOperationException e) {
            if (((byte) 2002686042) % 3 != 0) {
                throw null;
            }
            throw new IllegalArgumentException("Malformed input (3)");
        }
    }
}
