package me.ring.knou1.data;

import android.net.Uri;
import android.provider.BaseColumns;

public class Downloaded implements BaseColumns {
    public static final String COL_ARTIST = "artist";
    public static final String COL_CREATEDATE = "cdate";
    public static final String COL_KEY = "key";
    public static final String COL_RATING = "rating";
    public static final String COL_TITLE = "title";
    public static final Uri CONTENT_URI = Uri.parse("content://me.ring.knou1.rtdd/rtdd");

    private Downloaded() {
    }
}
