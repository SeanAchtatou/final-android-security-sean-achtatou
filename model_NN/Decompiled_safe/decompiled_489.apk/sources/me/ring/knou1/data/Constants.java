package me.ring.knou1.data;

import android.os.Environment;
import com.qwapi.adclient.android.utils.Utils;
import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Constants {
    public static final String APP_DATA_DIR = "/Ringsome/rt/";
    private static final String CACHE_DIR = "/Ringsome/rt/c/";
    private static final String IMAGE_CACHE_DIR = "/Ringsome/rt/i/";
    public static final String PKG_NAME = "me.ring.knou1";
    public static final String TAG = "ringsome";
    private static final String THUMBNAIL_DIR = "/Ringsome/rt/tn/";
    public static final String TOTAL_RINGTONE_COUNT = "http://ggapp.appspot.com/ringtone/counts/";

    public static String get_ThumbnailDir() {
        return String.valueOf(Environment.getExternalStorageDirectory().getPath()) + THUMBNAIL_DIR;
    }

    public static String get_ThumbnailPath(String key) {
        return String.valueOf(get_ThumbnailDir()) + key + ".png";
    }

    public static String get_CacheDir() {
        String retval = String.valueOf(Environment.getExternalStorageDirectory().getPath()) + CACHE_DIR;
        ensureDirExists(retval);
        return retval;
    }

    public static String get_ImageCacheDir() {
        String retval = String.valueOf(Environment.getExternalStorageDirectory().getPath()) + IMAGE_CACHE_DIR;
        ensureDirExists(retval);
        return retval;
    }

    private static void ensureDirExists(String path) {
        try {
            File f = new File(path);
            if (!f.exists()) {
                f.mkdirs();
            } else if (!f.isDirectory()) {
                f.delete();
                f.mkdirs();
            }
        } catch (Exception e) {
        }
    }

    public static String calcHash(String pInput) {
        try {
            MessageDigest lDigest = MessageDigest.getInstance("MD5");
            lDigest.update(pInput.getBytes());
            return String.format("%1$032X", new BigInteger(1, lDigest.digest()));
        } catch (NoSuchAlgorithmException e) {
            return Utils.EMPTY_STRING;
        }
    }
}
