package me.ring.knou1;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import me.ring.knou1.data.Constants;
import me.ring.knou1.task.DownloadTask;
import me.ring.knou1.task.MediaPlayerWrapper;
import me.ring.knou1.task.Ringtone;
import me.ring.knou1.task.RingtoneDetailTask;
import me.ring.knou1.task.ThumbnailTask;
import me.ring.knou1.utils.FileCacheMan;
import me.ring.knou1.utils.RingbowHelper;

public class RingtoneDetailActivity extends Activity implements RingtoneDetailTask.Listener, ThumbnailTask.Listener, MediaPlayerWrapper.Listener {
    private static final int DLG_LOADING = 10;
    private static final int GET_CODE = 0;
    private static final int MENU_SETRINGTONE = 1;
    private static final String P_ARTIST = "artist";
    private static final String P_IMAGEURL = "iurl";
    private static final String P_RATING = "rating";
    private static final String P_RINGTONEKEY = "rtkey";
    private static final String P_TITLE = "title";
    private static final int RING_CONNECTING = 2;
    private View mContentView = null;
    /* access modifiers changed from: private */
    public Button mPlayBtn = null;
    /* access modifiers changed from: private */
    public Button mPreviewBtn = null;
    ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public Ringtone mRingtone = null;
    /* access modifiers changed from: private */
    public Button mStopBtn = null;
    /* access modifiers changed from: private */
    public Button mStopPreviewBtn = null;
    private RingtoneDetailTask mTask = null;
    private Bitmap mThumbNail = null;

    public static void startActivity(Context ctx, String key, String title, String artist, int rating, String imgURL) {
        Intent intent = new Intent(ctx, RingtoneDetailActivity.class);
        intent.putExtra(P_RINGTONEKEY, key);
        intent.putExtra("title", title);
        intent.putExtra("artist", artist);
        intent.putExtra("rating", rating);
        intent.putExtra(P_IMAGEURL, imgURL);
        ctx.startActivity(intent);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.ringtone_detail);
        AdsView.createAdWhirl(this);
        this.mContentView = findViewById(R.id.ContentView);
        Bundle b = getIntent().getExtras();
        ((TextView) findViewById(R.id.Title)).setText(b.getString("title"));
        ((RatingBar) findViewById(R.id.Rating)).setRating((((float) b.getInt("rating")) / 100.0f) * 5.0f);
        ((TextView) findViewById(R.id.Artist)).setText(b.getString("artist"));
        String thumbNailURL = b.getString(P_IMAGEURL);
        if (thumbNailURL == null) {
            this.mThumbNail = BitmapFactory.decodeFile(Constants.get_ThumbnailPath(b.getString(P_RINGTONEKEY)));
        } else {
            this.mThumbNail = FileCacheMan.getImageCache(thumbNailURL);
        }
        if (this.mThumbNail != null) {
            ((ImageView) findViewById(R.id.Thumbnail)).setImageBitmap(this.mThumbNail);
        }
        this.mRingtone = (Ringtone) getLastNonConfigurationInstance();
        if (this.mRingtone == null) {
            this.mContentView.setVisibility(4);
            this.mTask = new RingtoneDetailTask(this);
            this.mTask.execute(getIntent().getExtras().getString(P_RINGTONEKEY));
        } else {
            updateRingtoneInfo();
            ((ImageView) findViewById(R.id.Thumbnail)).setImageBitmap(this.mRingtone.mBmp);
        }
        this.mPreviewBtn = (Button) findViewById(R.id.PreviewBtn);
        this.mPreviewBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RingtoneDetailActivity.this.showDialog(2);
                new Thread() {
                    public void run() {
                        ((RingbowApp) RingtoneDetailActivity.this.getApplication()).mPlayer.play(RingtoneDetailActivity.this, RingtoneDetailActivity.this.mRingtone.mLink);
                    }
                }.start();
            }
        });
        this.mStopPreviewBtn = (Button) findViewById(R.id.StopPreview);
        this.mStopPreviewBtn.setVisibility(8);
        this.mStopPreviewBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((RingbowApp) RingtoneDetailActivity.this.getApplication()).mPlayer.stop();
                RingtoneDetailActivity.this.mPreviewBtn.setVisibility(0);
                RingtoneDetailActivity.this.mStopPreviewBtn.setVisibility(8);
            }
        });
        ((Button) findViewById(R.id.DownloadBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DownloadActivity.startActivity(RingtoneDetailActivity.this, 0, RingtoneDetailActivity.this.mRingtone.mKey, RingtoneDetailActivity.this.mRingtone.mLink, RingtoneDetailActivity.this.mRingtone.mTitle, RingtoneDetailActivity.this.mRingtone.mArtist, RingtoneDetailActivity.this.mRingtone.mRating, RingtoneDetailActivity.this.mRingtone.mBmp);
            }
        });
        this.mPlayBtn = (Button) findViewById(R.id.PlayBtn);
        this.mPlayBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String path = DownloadTask.getDestFile(RingtoneDetailActivity.this.getIntent().getExtras().getString(RingtoneDetailActivity.P_RINGTONEKEY)).getAbsolutePath();
                RingtoneDetailActivity.this.mPlayBtn.setVisibility(8);
                RingtoneDetailActivity.this.mStopBtn.setVisibility(0);
                ((RingbowApp) RingtoneDetailActivity.this.getApplication()).mPlayer.play(RingtoneDetailActivity.this, path);
            }
        });
        this.mStopBtn = (Button) findViewById(R.id.StopBtn);
        this.mStopBtn.setVisibility(8);
        this.mStopBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((RingbowApp) RingtoneDetailActivity.this.getApplication()).mPlayer.stop();
                RingtoneDetailActivity.this.mPlayBtn.setVisibility(0);
                RingtoneDetailActivity.this.mStopBtn.setVisibility(8);
            }
        });
        ((Button) findViewById(R.id.SetBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RingtoneDetailActivity.this.showDialog(1);
            }
        });
        ((Button) findViewById(R.id.AssignBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ChooseContactActivity.startActivity(RingtoneDetailActivity.this, Ringtone.insertRingtone(RingtoneDetailActivity.this.getContentResolver(), DownloadTask.getDestFile(RingtoneDetailActivity.this.getIntent().getExtras().getString(RingtoneDetailActivity.P_RINGTONEKEY)).getAbsolutePath(), RingtoneDetailActivity.this.getIntent().getExtras().getString("title")));
            }
        });
        if (DownloadTask.isRingtoneExists(getIntent().getExtras().getString(P_RINGTONEKEY))) {
            findViewById(R.id.DownloadPanel).setVisibility(8);
            findViewById(R.id.SetPanel).setVisibility(0);
        } else {
            findViewById(R.id.DownloadPanel).setVisibility(0);
            findViewById(R.id.SetPanel).setVisibility(8);
        }
        ((Button) findViewById(R.id.MoreAuthorBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (RingtoneDetailActivity.this.mRingtone != null && RingtoneDetailActivity.this.mRingtone.mAuthor != null) {
                    String author = RingtoneDetailActivity.this.mRingtone.mAuthor;
                    RingtoneListActivity.startActivity(RingtoneDetailActivity.this, author, RingbowHelper.getByAuthor(author));
                }
            }
        });
        ((Button) findViewById(R.id.MoreArtistBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (RingtoneDetailActivity.this.mRingtone != null && RingtoneDetailActivity.this.mRingtone.mArtist != null) {
                    String artist = RingtoneDetailActivity.this.mRingtone.mArtist;
                    RingtoneListActivity.startActivity(RingtoneDetailActivity.this, artist, RingbowHelper.getByArtistURL(artist));
                }
            }
        });
        ((Button) findViewById(R.id.MoreCategoryBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (RingtoneDetailActivity.this.mRingtone != null && RingtoneDetailActivity.this.mRingtone.mCategory != null) {
                    String categoryName = RingtoneDetailActivity.this.mRingtone.mCategory;
                    RingtoneListActivity.startActivity(RingtoneDetailActivity.this, categoryName, RingbowHelper.getCategoryURL(categoryName));
                }
            }
        });
    }

    public Object onRetainNonConfigurationInstance() {
        if (this.mRingtone != null) {
            return this.mRingtone;
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public static Uri insertRingtone(ContentResolver cv, String path, String title) {
        ContentValues values = new ContentValues();
        values.put("title", title);
        values.put("_data", path);
        values.put("is_ringtone", (Boolean) true);
        values.put("is_notification", (Boolean) false);
        values.put("is_alarm", (Boolean) false);
        values.put("is_music", (Boolean) true);
        return cv.insert(MediaStore.Audio.Media.getContentUriForPath(path), values);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mRingtone = null;
        ((RingbowApp) getApplication()).mPlayer.unbind();
        super.onDestroy();
    }

    public void RDT_OnBegin() {
        showDialog(10);
    }

    public void RDT_OnFinished(boolean bError) {
        try {
            dismissDialog(10);
        } catch (Exception e) {
        }
        if (bError) {
            DownloadTask.isRingtoneExists(getIntent().getExtras().getString(P_RINGTONEKEY));
        }
    }

    public void RDT_OnRingtoneReady(Ringtone rt) {
        this.mRingtone = rt;
        updateRingtoneInfo();
        if (this.mThumbNail == null) {
            new ThumbnailTask(this).execute(rt, new Integer(0));
            return;
        }
        this.mRingtone.mBmp = this.mThumbNail;
    }

    private void updateRingtoneInfo() {
        Ringtone rt = this.mRingtone;
        this.mContentView.setVisibility(0);
        ((TextView) findViewById(R.id.Author)).setText(rt.mAuthor);
        ((TextView) findViewById(R.id.Category)).setText(rt.mCategory);
        ((TextView) findViewById(R.id.Downloads)).setText(new StringBuilder(String.valueOf(rt.mDownloads)).toString());
    }

    public void TT_OnBegin() {
    }

    public void TT_OnFinished(boolean bError) {
    }

    public void TT_OnThumbnailReady(int pos, Ringtone rt) {
        if (this.mRingtone != null) {
            ((ImageView) findViewById(R.id.Thumbnail)).setImageBitmap(rt.mBmp);
            this.mRingtone.mBmp = rt.mBmp;
        }
    }

    public void MPW_OnError() {
        this.mStopPreviewBtn.setVisibility(8);
        this.mPreviewBtn.setVisibility(0);
        this.mStopBtn.setVisibility(8);
        this.mPlayBtn.setVisibility(0);
    }

    public void MPW_OnPlayOver() {
        this.mStopPreviewBtn.setVisibility(8);
        this.mPreviewBtn.setVisibility(0);
        this.mStopBtn.setVisibility(8);
        this.mPlayBtn.setVisibility(0);
    }

    public void MPW_OnPlaystart() {
        Log.e("Ringtone Detial", "start play . ");
        removeDialog(2);
        this.mPreviewBtn.setVisibility(8);
        this.mStopPreviewBtn.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        String rtData;
        if (requestCode == 0 && resultCode == -1) {
            findViewById(R.id.DownloadPanel).setVisibility(8);
            findViewById(R.id.SetPanel).setVisibility(0);
            if (this.mTask != null && (rtData = this.mTask.getJSonResp()) != null) {
                FileCacheMan.putStringCache(getIntent().getExtras().getString(P_RINGTONEKEY), rtData);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new AlertDialog.Builder(this).setTitle("Set as").setItems((int) R.array.SET_RINGTONE_LIST, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                String key = RingtoneDetailActivity.this.getIntent().getExtras().getString(RingtoneDetailActivity.P_RINGTONEKEY);
                                String path = DownloadTask.getDestFile(key).getAbsolutePath();
                                Log.e("Ringtone detail", "key : " + key + " path" + path);
                                Uri uri = Ringtone.insertRingtone(RingtoneDetailActivity.this.getContentResolver(), path, RingtoneDetailActivity.this.getIntent().getExtras().getString("title"));
                                if (uri != null) {
                                    RingtoneManager.setActualDefaultRingtoneUri(RingtoneDetailActivity.this, 1, uri);
                                    Toast.makeText(RingtoneDetailActivity.this, String.valueOf(RingtoneDetailActivity.this.getIntent().getExtras().getString("title")) + " set as ringtone", 0).show();
                                    return;
                                }
                                return;
                            case 1:
                                Uri uri2 = Ringtone.insertNotification(RingtoneDetailActivity.this.getContentResolver(), DownloadTask.getDestFile(RingtoneDetailActivity.this.getIntent().getExtras().getString(RingtoneDetailActivity.P_RINGTONEKEY)).getAbsolutePath(), RingtoneDetailActivity.this.getIntent().getExtras().getString("title"));
                                if (uri2 != null) {
                                    RingtoneManager.setActualDefaultRingtoneUri(RingtoneDetailActivity.this, 2, uri2);
                                    Toast.makeText(RingtoneDetailActivity.this, String.valueOf(RingtoneDetailActivity.this.getIntent().getExtras().getString("title")) + " set as notification sound", 0).show();
                                    return;
                                }
                                return;
                            case 2:
                                Uri uri3 = Ringtone.insertAlarm(RingtoneDetailActivity.this.getContentResolver(), DownloadTask.getDestFile(RingtoneDetailActivity.this.getIntent().getExtras().getString(RingtoneDetailActivity.P_RINGTONEKEY)).getAbsolutePath(), RingtoneDetailActivity.this.getIntent().getExtras().getString("title"));
                                if (uri3 != null) {
                                    RingtoneManager.setActualDefaultRingtoneUri(RingtoneDetailActivity.this, 4, uri3);
                                    Toast.makeText(RingtoneDetailActivity.this, String.valueOf(RingtoneDetailActivity.this.getIntent().getExtras().getString("title")) + " set as alarm sound", 0).show();
                                    return;
                                }
                                return;
                            default:
                                return;
                        }
                    }
                }).create();
            case 2:
                this.mProgressDialog = new ProgressDialog(this);
                this.mProgressDialog.setMessage("Please wait while connecting...");
                this.mProgressDialog.setIndeterminate(true);
                this.mProgressDialog.setCancelable(true);
                return this.mProgressDialog;
            case 10:
                ProgressDialog dialog = new ProgressDialog(this);
                dialog.setTitle("A moment please...");
                dialog.setMessage("Retreiving data...");
                dialog.setIndeterminate(true);
                dialog.setCancelable(true);
                return dialog;
            default:
                return null;
        }
    }
}
