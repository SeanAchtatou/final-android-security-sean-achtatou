package me.ring.knou1;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.Toast;
import java.io.File;
import me.ring.knou1.data.Downloaded;
import me.ring.knou1.task.DownloadTask;

public class DownloadActivity extends Activity implements DownloadTask.Listener {
    private static final String P_ARTIST = "artist";
    private static final String P_KEY = "key";
    private static final String P_LINK = "link";
    private static final String P_RATING = "rating";
    private static final String P_TITLE = "title";
    private ProgressBar mProgBar = null;
    private DownloadTask mTask = null;

    public static void startActivity(Activity ctx, int resultCode, String key, String link, String title, String artist, int rating, Bitmap thumbnail) {
        Intent intent = new Intent(ctx, DownloadActivity.class);
        intent.putExtra("key", key);
        intent.putExtra(P_LINK, link);
        intent.putExtra("artist", artist);
        intent.putExtra("title", title);
        intent.putExtra("rating", rating);
        ctx.startActivityForResult(intent, resultCode);
        saveThumbnail(key, thumbnail);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.download);
        AdsView.createAdWhirl(this);
        Bundle b = getIntent().getExtras();
        String key = b.getString("key");
        String link = b.getString(P_LINK);
        this.mProgBar = (ProgressBar) findViewById(R.id.ProgressBar);
        this.mTask = new DownloadTask(this);
        this.mTask.execute(key, link);
    }

    public void onDestroy() {
        if (!(this.mTask == null || this.mTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.mTask.cancel(true);
            this.mTask = null;
            deleteTempFile();
        }
        super.onDestroy();
    }

    public void DT_OnBegin() {
        this.mProgBar.setProgress(0);
    }

    public void DT_OnFinished(boolean bError) {
        this.mTask = null;
        if (bError) {
            Toast.makeText(this, "Ringtone downloading failed!", 0).show();
            setResult(0, null);
            deleteTempFile();
        } else {
            this.mProgBar.setProgress(100);
            Bundle b = getIntent().getExtras();
            ContentValues values = new ContentValues();
            values.put("title", b.getString("title"));
            values.put("artist", b.getString("artist"));
            values.put("key", b.getString("key"));
            values.put("rating", Integer.valueOf(b.getInt("rating")));
            values.put(Downloaded.COL_CREATEDATE, Long.valueOf(System.currentTimeMillis()));
            getContentResolver().insert(Downloaded.CONTENT_URI, values);
            setResult(-1, null);
        }
        finish();
    }

    public void DT_OnProgressUpdated(Integer p) {
        this.mProgBar.setProgress(p.intValue());
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0050 A[SYNTHETIC, Splitter:B:20:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0059 A[SYNTHETIC, Splitter:B:25:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void saveThumbnail(java.lang.String r7, android.graphics.Bitmap r8) {
        /*
            if (r8 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream
            r2.<init>()
            android.graphics.Bitmap$CompressFormat r5 = android.graphics.Bitmap.CompressFormat.PNG
            r6 = 100
            r8.compress(r5, r6, r2)
            r3 = 0
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x004d, all -> 0x0056 }
            java.lang.String r5 = me.ring.knou1.data.Constants.get_ThumbnailDir()     // Catch:{ Exception -> 0x004d, all -> 0x0056 }
            r0.<init>(r5)     // Catch:{ Exception -> 0x004d, all -> 0x0056 }
            boolean r5 = r0.exists()     // Catch:{ Exception -> 0x004d, all -> 0x0056 }
            if (r5 != 0) goto L_0x0022
            r0.mkdirs()     // Catch:{ Exception -> 0x004d, all -> 0x0056 }
        L_0x0022:
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x004d, all -> 0x0056 }
            java.lang.String r5 = me.ring.knou1.data.Constants.get_ThumbnailPath(r7)     // Catch:{ Exception -> 0x004d, all -> 0x0056 }
            r1.<init>(r5)     // Catch:{ Exception -> 0x004d, all -> 0x0056 }
            boolean r5 = r1.exists()     // Catch:{ Exception -> 0x004d, all -> 0x0056 }
            if (r5 == 0) goto L_0x003a
            boolean r5 = r1.isFile()     // Catch:{ Exception -> 0x004d, all -> 0x0056 }
            if (r5 == 0) goto L_0x003a
            r1.delete()     // Catch:{ Exception -> 0x004d, all -> 0x0056 }
        L_0x003a:
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x004d, all -> 0x0056 }
            java.lang.String r5 = me.ring.knou1.data.Constants.get_ThumbnailPath(r7)     // Catch:{ Exception -> 0x004d, all -> 0x0056 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x004d, all -> 0x0056 }
            r2.writeTo(r4)     // Catch:{ Exception -> 0x0065, all -> 0x0062 }
            if (r4 == 0) goto L_0x0068
            r4.close()     // Catch:{ Exception -> 0x005d }
            r3 = r4
            goto L_0x0002
        L_0x004d:
            r5 = move-exception
        L_0x004e:
            if (r3 == 0) goto L_0x0002
            r3.close()     // Catch:{ Exception -> 0x0054 }
            goto L_0x0002
        L_0x0054:
            r5 = move-exception
            goto L_0x0002
        L_0x0056:
            r5 = move-exception
        L_0x0057:
            if (r3 == 0) goto L_0x005c
            r3.close()     // Catch:{ Exception -> 0x0060 }
        L_0x005c:
            throw r5
        L_0x005d:
            r5 = move-exception
            r3 = r4
            goto L_0x0002
        L_0x0060:
            r6 = move-exception
            goto L_0x005c
        L_0x0062:
            r5 = move-exception
            r3 = r4
            goto L_0x0057
        L_0x0065:
            r5 = move-exception
            r3 = r4
            goto L_0x004e
        L_0x0068:
            r3 = r4
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: me.ring.knou1.DownloadActivity.saveThumbnail(java.lang.String, android.graphics.Bitmap):void");
    }

    private void deleteTempFile() {
        File df = DownloadTask.getDestFile(getIntent().getExtras().getString("key"));
        if (df.exists() && df.isFile()) {
            df.delete();
            df.deleteOnExit();
        }
    }
}
