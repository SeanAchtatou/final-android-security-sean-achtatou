package me.ring.knou1;

public final class R {

    public static final class anim {
        public static final int footer_appear = 2130968576;
        public static final int footer_disappear = 2130968577;
        public static final int in = 2130968578;
        public static final int layout_slideup = 2130968579;
        public static final int out = 2130968580;
        public static final int slide_right = 2130968581;
    }

    public static final class array {
        public static final int MYDOWNLOAD_POPMENU_LIST = 2131099648;
        public static final int SET_RINGTONE_LIST = 2131099649;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int COLOR_BLACK = 2131034112;
        public static final int COLOR_TRANSPARENT = 2131034114;
        public static final int COLOR_WHITE = 2131034113;
    }

    public static final class drawable {
        public static final int bgimg = 2130837504;
        public static final int bkgnd = 2130837505;
        public static final int btn_back = 2130837506;
        public static final int btn_back_focused = 2130837507;
        public static final int btn_back_normal = 2130837508;
        public static final int btn_back_pressed = 2130837509;
        public static final int btn_default = 2130837510;
        public static final int btn_default_normal_disable_focused = 2130837511;
        public static final int btn_default_pressed = 2130837512;
        public static final int btn_default_selected = 2130837513;
        public static final int btn_forward = 2130837514;
        public static final int btn_forward_focused = 2130837515;
        public static final int btn_forward_normal = 2130837516;
        public static final int btn_forward_pressed = 2130837517;
        public static final int btnartist = 2130837518;
        public static final int btnauthor = 2130837519;
        public static final int btnleft = 2130837520;
        public static final int btnright = 2130837521;
        public static final int button_zoom_in = 2130837522;
        public static final int button_zoom_out = 2130837523;
        public static final int category = 2130837524;
        public static final int days = 2130837525;
        public static final int default_thumbnail = 2130837526;
        public static final int grid_line = 2130837570;
        public static final int ic_media_ff = 2130837527;
        public static final int ic_media_next = 2130837528;
        public static final int ic_media_pause = 2130837529;
        public static final int ic_media_play = 2130837530;
        public static final int ic_media_previous = 2130837531;
        public static final int ic_media_rew = 2130837532;
        public static final int ic_menu_artist = 2130837533;
        public static final int ic_menu_category = 2130837534;
        public static final int ic_menu_chart = 2130837535;
        public static final int ic_menu_download = 2130837536;
        public static final int ic_menu_editor = 2130837537;
        public static final int ic_menu_feature = 2130837538;
        public static final int ic_menu_search = 2130837539;
        public static final int ic_search = 2130837540;
        public static final int icon = 2130837541;
        public static final int leftbtn = 2130837542;
        public static final int logo = 2130837543;
        public static final int logo2 = 2130837544;
        public static final int marker_left = 2130837545;
        public static final int marker_left_focused = 2130837546;
        public static final int marker_left_normal = 2130837547;
        public static final int marker_left_pressed = 2130837548;
        public static final int marker_right = 2130837549;
        public static final int marker_right_focused = 2130837550;
        public static final int marker_right_normal = 2130837551;
        public static final int marker_right_pressed = 2130837552;
        public static final int menu_about = 2130837553;
        public static final int menu_reset = 2130837554;
        public static final int menu_save = 2130837555;
        public static final int menu_show_all_audio = 2130837556;
        public static final int month = 2130837557;
        public static final int playback_indicator = 2130837569;
        public static final int rightbtn = 2130837558;
        public static final int selection_border = 2130837568;
        public static final int star = 2130837559;
        public static final int timecode = 2130837571;
        public static final int timecode_shadow = 2130837572;
        public static final int type_alarm = 2130837560;
        public static final int type_bkgnd_alarm = 2130837573;
        public static final int type_bkgnd_music = 2130837576;
        public static final int type_bkgnd_notification = 2130837575;
        public static final int type_bkgnd_ringtone = 2130837574;
        public static final int type_bkgnd_unsupported = 2130837577;
        public static final int type_music = 2130837561;
        public static final int type_notification = 2130837562;
        public static final int type_ringtone = 2130837563;
        public static final int waveform_selected = 2130837565;
        public static final int waveform_unselected = 2130837566;
        public static final int waveform_unselected_bkgnd_overlay = 2130837567;
        public static final int weeks = 2130837564;
    }

    public static final class id {
        public static final int AdsView = 2131361795;
        public static final int Artist = 2131361819;
        public static final int AssignBtn = 2131361843;
        public static final int Author = 2131361832;
        public static final int BtnPanel = 2131361846;
        public static final int Category = 2131361833;
        public static final int ContentView = 2131361830;
        public static final int DownloadBtn = 2131361838;
        public static final int DownloadPanel = 2131361837;
        public static final int Downloads = 2131361831;
        public static final int Icon = 2131361823;
        public static final int Keyword = 2131361849;
        public static final int MoreArtistBtn = 2131361834;
        public static final int MoreAuthorBtn = 2131361835;
        public static final int MoreCategoryBtn = 2131361836;
        public static final int Msg = 2131361852;
        public static final int NextPage = 2131361847;
        public static final int PlayBtn = 2131361844;
        public static final int PrePage = 2131361848;
        public static final int PreviewBtn = 2131361839;
        public static final int ProgBar = 2131361851;
        public static final int ProgressBar = 2131361801;
        public static final int Rating = 2131361829;
        public static final int SearchBtn = 2131361850;
        public static final int SetBtn = 2131361842;
        public static final int SetPanel = 2131361841;
        public static final int StopBtn = 2131361845;
        public static final int StopPreview = 2131361840;
        public static final int Text = 2131361796;
        public static final int Thumbnail = 2131361817;
        public static final int Title = 2131361818;
        public static final int bkgnd = 2131361802;
        public static final int button_choose_contact = 2131361793;
        public static final int button_do_nothing = 2131361794;
        public static final int button_make_default = 2131361792;
        public static final int cancel = 2131361822;
        public static final int endmarker = 2131361805;
        public static final int endtext = 2131361815;
        public static final int ffwd = 2131361809;
        public static final int filename = 2131361821;
        public static final int info = 2131361806;
        public static final int mark_end = 2131361813;
        public static final int mark_start = 2131361812;
        public static final int menu_downloaded = 2131361854;
        public static final int menu_home = 2131361853;
        public static final int menu_sendto = 2131361855;
        public static final int play = 2131361807;
        public static final int record = 2131361824;
        public static final int rew = 2131361808;
        public static final int ringtone_type = 2131361820;
        public static final int row_album = 2131361828;
        public static final int row_artist = 2131361827;
        public static final int row_display_name = 2131361800;
        public static final int row_icon = 2131361825;
        public static final int row_ringtone = 2131361798;
        public static final int row_starred = 2131361799;
        public static final int row_title = 2131361826;
        public static final int save = 2131361816;
        public static final int search_filter = 2131361797;
        public static final int startmarker = 2131361804;
        public static final int starttext = 2131361814;
        public static final int waveform = 2131361803;
        public static final int zoom_in = 2131361810;
        public static final int zoom_out = 2131361811;
    }

    public static final class layout {
        public static final int after_save_action = 2130903040;
        public static final int category = 2130903041;
        public static final int category_item = 2130903042;
        public static final int choose_contact = 2130903043;
        public static final int contact_row = 2130903044;
        public static final int download = 2130903045;
        public static final int editor = 2130903046;
        public static final int featured = 2130903047;
        public static final int featured_item = 2130903048;
        public static final int file_save = 2130903049;
        public static final int home = 2130903050;
        public static final int home_list_item = 2130903051;
        public static final int media_select = 2130903052;
        public static final int media_select_row = 2130903053;
        public static final int mydownloads = 2130903054;
        public static final int mydownloads_item = 2130903055;
        public static final int ringtone_detail = 2130903056;
        public static final int ringtone_list = 2130903057;
        public static final int ringtone_list_item = 2130903058;
        public static final int search = 2130903059;
        public static final int starter = 2130903060;
        public static final int topartist = 2130903061;
        public static final int topdownload = 2130903062;
    }

    public static final class menu {
        public static final int artist = 2131296256;
    }

    public static final class string {
        public static final int ERR_NETWORK = 2131165186;
        public static final int alert_no_button = 2131165242;
        public static final int alert_ok_button = 2131165240;
        public static final int alert_title_failure = 2131165239;
        public static final int alert_title_success = 2131165238;
        public static final int alert_yes_button = 2131165241;
        public static final int app_name = 2131165184;
        public static final int artist_name = 2131165227;
        public static final int bad_extension_error = 2131165253;
        public static final int button_start_record_activity = 2131165188;
        public static final int choose_contact_ringtone_button = 2131165199;
        public static final int choose_contact_title = 2131165255;
        public static final int confirm_delete_non_ringdroid = 2131165234;
        public static final int confirm_delete_ringdroid = 2131165233;
        public static final int context_menu_delete = 2131165217;
        public static final int context_menu_edit = 2131165216;
        public static final int default_ringtone_success_message = 2131165244;
        public static final int delete_alarm = 2131165229;
        public static final int delete_audio = 2131165232;
        public static final int delete_cancel_button = 2131165236;
        public static final int delete_failed = 2131165237;
        public static final int delete_music = 2131165231;
        public static final int delete_notification = 2131165230;
        public static final int delete_ok_button = 2131165235;
        public static final int delete_ringtone = 2131165228;
        public static final int delete_tmp_error = 2131165247;
        public static final int do_nothing_with_ringtone_button = 2131165200;
        public static final int edit_intent = 2131165194;
        public static final int end_label = 2131165193;
        public static final int end_marker = 2131165204;
        public static final int ffwd = 2131165208;
        public static final int file_save_button_cancel = 2131165220;
        public static final int file_save_button_save = 2131165219;
        public static final int file_save_title = 2131165218;
        public static final int make_default_ringtone_button = 2131165198;
        public static final int menu_about = 2131165214;
        public static final int menu_reset = 2131165213;
        public static final int menu_save = 2131165212;
        public static final int menu_show_all_audio = 2131165215;
        public static final int no_extension_error = 2131165254;
        public static final int no_sdcard = 2131165190;
        public static final int no_space_error = 2131165252;
        public static final int no_unique_filename = 2131165246;
        public static final int play = 2131165206;
        public static final int play_error = 2131165245;
        public static final int progress_dialog_loading = 2131165201;
        public static final int progress_dialog_saving = 2131165202;
        public static final int read_error = 2131165251;
        public static final int record_error = 2131165248;
        public static final int rewind = 2131165205;
        public static final int ringtone_name_label = 2131165226;
        public static final int ringtone_type_label = 2131165221;
        public static final int save_button = 2131165211;
        public static final int save_success_message = 2131165243;
        public static final int sdcard_readonly = 2131165189;
        public static final int sdcard_shared = 2131165191;
        public static final int search_edit_box = 2131165187;
        public static final int set_default_notification = 2131165197;
        public static final int start_label = 2131165192;
        public static final int start_marker = 2131165203;
        public static final int stop = 2131165207;
        public static final int success_contact_ringtone = 2131165185;
        public static final int time_seconds = 2131165195;
        public static final int too_small_error = 2131165249;
        public static final int type_alarm = 2131165223;
        public static final int type_music = 2131165222;
        public static final int type_notification = 2131165224;
        public static final int type_ringtone = 2131165225;
        public static final int what_to_do_with_ringtone = 2131165196;
        public static final int write_error = 2131165250;
        public static final int zoom_in = 2131165209;
        public static final int zoom_out = 2131165210;
    }

    public static final class style {
        public static final int AudioFileInfoOverlayText = 2131230720;
        public static final int ChooseContactBackground = 2131230722;
        public static final int HorizontalDividerBottom = 2131230726;
        public static final int HorizontalDividerTop = 2131230725;
        public static final int ToolbarBackground = 2131230721;
        public static final int VerticalDividerLeft = 2131230723;
        public static final int VerticalDividerRight = 2131230724;
    }
}
