package me.ring.knou1.soundfile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import me.ring.knou1.soundfile.CheapSoundFile;

public class CheapMP3 extends CheapSoundFile {
    private static int[] BITRATES_MPEG1_L3;
    private static int[] BITRATES_MPEG2_L3;
    private static int[] SAMPLERATES_MPEG1_L3;
    private static int[] SAMPLERATES_MPEG2_L3;
    private int mAvgBitRate;
    private int mBitrateSum;
    private int mFileSize;
    private int[] mFrameGains;
    private int[] mFrameLens;
    private int[] mFrameOffsets;
    private int mGlobalChannels;
    private int mGlobalSampleRate;
    private int mMaxFrames;
    private int mMaxGain;
    private int mMinGain;
    private int mNumFrames;

    public static CheapSoundFile.Factory getFactory() {
        return new CheapSoundFile.Factory() {
            public CheapSoundFile create() {
                return new CheapMP3();
            }

            public String[] getSupportedExtensions() {
                return new String[]{"mp3"};
            }
        };
    }

    public int getNumFrames() {
        return this.mNumFrames;
    }

    public int[] getFrameOffsets() {
        return this.mFrameOffsets;
    }

    public int getSamplesPerFrame() {
        return 1152;
    }

    public int[] getFrameLens() {
        return this.mFrameLens;
    }

    public int[] getFrameGains() {
        return this.mFrameGains;
    }

    public int getFileSizeBytes() {
        return this.mFileSize;
    }

    public int getAvgBitrateKbps() {
        return this.mAvgBitRate;
    }

    public int getSampleRate() {
        return this.mGlobalSampleRate;
    }

    public int getChannels() {
        return this.mGlobalChannels;
    }

    public String getFiletype() {
        return "MP3";
    }

    public int getSeekableFrameOffset(int frame) {
        if (frame <= 0) {
            return 0;
        }
        if (frame >= this.mNumFrames) {
            return this.mFileSize;
        }
        return this.mFrameOffsets[frame];
    }

    public void ReadFile(File inputFile) throws FileNotFoundException, IOException {
        int mpgVersion;
        int bitRate;
        int sampleRate;
        int gain;
        super.ReadFile(inputFile);
        this.mNumFrames = 0;
        this.mMaxFrames = 64;
        this.mFrameOffsets = new int[this.mMaxFrames];
        this.mFrameLens = new int[this.mMaxFrames];
        this.mFrameGains = new int[this.mMaxFrames];
        this.mBitrateSum = 0;
        this.mMinGain = 255;
        this.mMaxGain = 0;
        this.mFileSize = (int) this.mInputFile.length();
        FileInputStream fileInputStream = new FileInputStream(this.mInputFile);
        int pos = 0;
        int offset = 0;
        byte[] buffer = new byte[12];
        while (pos < this.mFileSize - 12) {
            while (offset < 12) {
                offset += fileInputStream.read(buffer, offset, 12 - offset);
            }
            int bufferOffset = 0;
            while (bufferOffset < 12 && buffer[bufferOffset] != -1) {
                bufferOffset++;
            }
            if (this.mProgressListener != null && !this.mProgressListener.reportProgress((((double) pos) * 1.0d) / ((double) this.mFileSize))) {
                break;
            } else if (bufferOffset > 0) {
                for (int i = 0; i < 12 - bufferOffset; i++) {
                    buffer[i] = buffer[bufferOffset + i];
                }
                pos += bufferOffset;
                offset = 12 - bufferOffset;
            } else {
                if (buffer[1] == -6 || buffer[1] == -5) {
                    mpgVersion = 1;
                } else if (buffer[1] == -14 || buffer[1] == -13) {
                    mpgVersion = 2;
                } else {
                    for (int i2 = 0; i2 < 12 - 1; i2++) {
                        buffer[i2] = buffer[1 + i2];
                    }
                    pos++;
                    offset = 12 - 1;
                }
                if (mpgVersion == 1) {
                    bitRate = BITRATES_MPEG1_L3[(buffer[2] & 240) >> 4];
                    sampleRate = SAMPLERATES_MPEG1_L3[(buffer[2] & 12) >> 2];
                } else {
                    bitRate = BITRATES_MPEG2_L3[(buffer[2] & 240) >> 4];
                    sampleRate = SAMPLERATES_MPEG2_L3[(buffer[2] & 12) >> 2];
                }
                if (bitRate == 0 || sampleRate == 0) {
                    for (int i3 = 0; i3 < 12 - 2; i3++) {
                        buffer[i3] = buffer[2 + i3];
                    }
                    pos += 2;
                    offset = 12 - 2;
                } else {
                    this.mGlobalSampleRate = sampleRate;
                    int frameLen = (((bitRate * 144) * 1000) / sampleRate) + ((buffer[2] & 2) >> 1);
                    if ((buffer[3] & 192) == 192) {
                        this.mGlobalChannels = 1;
                        if (mpgVersion == 1) {
                            gain = ((buffer[10] & 1) << 7) + ((buffer[11] & 254) >> 1);
                        } else {
                            gain = ((buffer[9] & 3) << 6) + ((buffer[10] & 252) >> 2);
                        }
                    } else {
                        this.mGlobalChannels = 2;
                        if (mpgVersion == 1) {
                            gain = ((buffer[9] & Byte.MAX_VALUE) << 1) + ((buffer[10] & 128) >> 7);
                        } else {
                            gain = 0;
                        }
                    }
                    this.mBitrateSum = this.mBitrateSum + bitRate;
                    this.mFrameOffsets[this.mNumFrames] = pos;
                    this.mFrameLens[this.mNumFrames] = frameLen;
                    this.mFrameGains[this.mNumFrames] = gain;
                    if (gain < this.mMinGain) {
                        this.mMinGain = gain;
                    }
                    if (gain > this.mMaxGain) {
                        this.mMaxGain = gain;
                    }
                    this.mNumFrames = this.mNumFrames + 1;
                    if (this.mNumFrames == this.mMaxFrames) {
                        this.mAvgBitRate = this.mBitrateSum / this.mNumFrames;
                        int newMaxFrames = ((((this.mFileSize / this.mAvgBitRate) * sampleRate) / 144000) * 11) / 10;
                        if (newMaxFrames < this.mMaxFrames * 2) {
                            newMaxFrames = this.mMaxFrames * 2;
                        }
                        int[] newOffsets = new int[newMaxFrames];
                        int[] newLens = new int[newMaxFrames];
                        int[] newGains = new int[newMaxFrames];
                        for (int i4 = 0; i4 < this.mNumFrames; i4++) {
                            newOffsets[i4] = this.mFrameOffsets[i4];
                            newLens[i4] = this.mFrameLens[i4];
                            newGains[i4] = this.mFrameGains[i4];
                        }
                        this.mFrameOffsets = newOffsets;
                        this.mFrameLens = newLens;
                        this.mFrameGains = newGains;
                        this.mMaxFrames = newMaxFrames;
                    }
                    fileInputStream.skip((long) (frameLen - 12));
                    pos += frameLen;
                    offset = 0;
                }
            }
        }
        if (this.mNumFrames > 0) {
            this.mAvgBitRate = this.mBitrateSum / this.mNumFrames;
        } else {
            this.mAvgBitRate = 0;
        }
    }

    public void WriteFile(File outputFile, int startFrame, int numFrames) throws IOException {
        outputFile.createNewFile();
        FileInputStream in = new FileInputStream(this.mInputFile);
        FileOutputStream out = new FileOutputStream(outputFile);
        int maxFrameLen = 0;
        for (int i = 0; i < numFrames; i++) {
            if (this.mFrameLens[startFrame + i] > maxFrameLen) {
                maxFrameLen = this.mFrameLens[startFrame + i];
            }
        }
        byte[] buffer = new byte[maxFrameLen];
        int pos = 0;
        for (int i2 = 0; i2 < numFrames; i2++) {
            int skip = this.mFrameOffsets[startFrame + i2] - pos;
            int len = this.mFrameLens[startFrame + i2];
            if (skip > 0) {
                in.skip((long) skip);
                pos += skip;
            }
            in.read(buffer, 0, len);
            out.write(buffer, 0, len);
            pos += len;
        }
        in.close();
        out.close();
    }

    static {
        int[] iArr = new int[16];
        iArr[1] = 32;
        iArr[2] = 40;
        iArr[3] = 48;
        iArr[4] = 56;
        iArr[5] = 64;
        iArr[6] = 80;
        iArr[7] = 96;
        iArr[8] = 112;
        iArr[9] = 128;
        iArr[10] = 160;
        iArr[11] = 192;
        iArr[12] = 224;
        iArr[13] = 256;
        iArr[14] = 320;
        BITRATES_MPEG1_L3 = iArr;
        int[] iArr2 = new int[16];
        iArr2[1] = 8;
        iArr2[2] = 16;
        iArr2[3] = 24;
        iArr2[4] = 32;
        iArr2[5] = 40;
        iArr2[6] = 48;
        iArr2[7] = 56;
        iArr2[8] = 64;
        iArr2[9] = 80;
        iArr2[10] = 96;
        iArr2[11] = 112;
        iArr2[12] = 128;
        iArr2[13] = 144;
        iArr2[14] = 160;
        BITRATES_MPEG2_L3 = iArr2;
        int[] iArr3 = new int[4];
        iArr3[0] = 44100;
        iArr3[1] = 48000;
        iArr3[2] = 32000;
        SAMPLERATES_MPEG1_L3 = iArr3;
        int[] iArr4 = new int[4];
        iArr4[0] = 22050;
        iArr4[1] = 24000;
        iArr4[2] = 16000;
        SAMPLERATES_MPEG2_L3 = iArr4;
    }
}
