package me.ring.knou1.soundfile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import me.ring.knou1.soundfile.CheapSoundFile;

public class CheapAMR extends CheapSoundFile {
    private static int[] BLOCK_SIZES;
    private static int[] GAIN_FAC_MR515 = {28753, 2785, 6594, 7413, 10444, 1269, 4423, 1556, 12820, 2498, 4833, 2498, 7864, 1884, 3153, 1802, 20193, 3031, 5857, 4014, 8970, 1392, 4096, 655, 13926, 3112, 4669, 2703, 6553, 901, 2662, 655, 23511, 2457, 5079, 4096, 8560, 737, 4259, 2088, 12288, 1474, 4628, 1433, 7004, 737, 2252, 1228, 17326, 2334, 5816, 3686, 8601, 778, 3809, 614, 9256, 1761, 3522, 1966, 5529, 737, 3194, 778};
    private static int[] GRAY;
    private static int[] QUA_ENER_MR515;
    private static int[] QUA_GAIN_CODE = {159, -3776, -22731, 206, -3394, -20428, 268, -3005, -18088, 349, -2615, -15739, 419, -2345, -14113, 482, -2138, -12867, 554, -1932, -11629, 637, -1726, -10387, 733, -1518, -9139, 842, -1314, -7906, 969, -1106, -6656, 1114, -900, -5416, 1281, -694, -4173, 1473, -487, -2931, 1694, -281, -1688, 1948, -75, -445, 2241, 133, 801, 2577, 339, 2044, 2963, 545, 3285, 3408, 752, 4530, 3919, 958, 5772, 4507, 1165, 7016, 5183, 1371, 8259, 5960, 1577, 9501, 6855, 1784, 10745, 7883, 1991, 11988, 9065, 2197, 13231, 10425, 2404, 14474, 12510, 2673, 16096, 16263, 3060, 18429, 21142, 3448, 20763, 27485, 3836, 23097};
    private static int[] QUA_GAIN_PITCH;
    private int mBitRate;
    private int mFileSize;
    private int[] mFrameGains;
    private int[] mFrameLens;
    private int[] mFrameOffsets;
    private int mMaxFrames;
    private int mMaxGain;
    private int mMinGain;
    private int mNumFrames;
    private int mOffset;

    public static CheapSoundFile.Factory getFactory() {
        return new CheapSoundFile.Factory() {
            public CheapSoundFile create() {
                return new CheapAMR();
            }

            public String[] getSupportedExtensions() {
                return new String[]{"3gpp", "3gp", "amr"};
            }
        };
    }

    public int getNumFrames() {
        return this.mNumFrames;
    }

    public int getSamplesPerFrame() {
        return 40;
    }

    public int[] getFrameOffsets() {
        return this.mFrameOffsets;
    }

    public int[] getFrameLens() {
        return this.mFrameLens;
    }

    public int[] getFrameGains() {
        return this.mFrameGains;
    }

    public int getFileSizeBytes() {
        return this.mFileSize;
    }

    public int getAvgBitrateKbps() {
        return this.mBitRate;
    }

    public int getSampleRate() {
        return 8000;
    }

    public int getChannels() {
        return 1;
    }

    public String getFiletype() {
        return "AMR";
    }

    public void ReadFile(File inputFile) throws FileNotFoundException, IOException {
        super.ReadFile(inputFile);
        this.mNumFrames = 0;
        this.mMaxFrames = 64;
        this.mFrameOffsets = new int[this.mMaxFrames];
        this.mFrameLens = new int[this.mMaxFrames];
        this.mFrameGains = new int[this.mMaxFrames];
        this.mMinGain = 1000000000;
        this.mMaxGain = 0;
        this.mBitRate = 10;
        this.mOffset = 0;
        this.mFileSize = (int) this.mInputFile.length();
        if (this.mFileSize < 128) {
            throw new IOException("File too small to parse");
        }
        FileInputStream stream = new FileInputStream(this.mInputFile);
        byte[] header = new byte[12];
        stream.read(header, 0, 6);
        this.mOffset += 6;
        if (header[0] == 35 && header[1] == 33 && header[2] == 65 && header[3] == 77 && header[4] == 82 && header[5] == 10) {
            parseAMR(stream, this.mFileSize - 6);
        }
        stream.read(header, 6, 6);
        this.mOffset += 6;
        if (header[4] == 102 && header[5] == 116 && header[6] == 121 && header[7] == 112 && header[8] == 51 && header[9] == 103 && header[10] == 112 && header[11] == 52) {
            int boxLen = ((header[0] & 255) << 24) | ((header[1] & 255) << 16) | ((header[2] & 255) << 8) | (header[3] & 255);
            if (boxLen >= 4 && boxLen <= this.mFileSize - 8) {
                stream.skip((long) (boxLen - 12));
                this.mOffset += boxLen - 12;
            }
            parse3gpp(stream, this.mFileSize - boxLen);
        }
    }

    private void parse3gpp(InputStream stream, int maxLen) throws IOException {
        if (maxLen >= 8) {
            byte[] boxHeader = new byte[8];
            stream.read(boxHeader, 0, 8);
            this.mOffset += 8;
            int boxLen = ((boxHeader[0] & 255) << 24) | ((boxHeader[1] & 255) << 16) | ((boxHeader[2] & 255) << 8) | (boxHeader[3] & 255);
            if (boxLen > maxLen) {
                return;
            }
            if (boxHeader[4] == 109 && boxHeader[5] == 100 && boxHeader[6] == 97 && boxHeader[7] == 116) {
                parseAMR(stream, boxLen);
            } else {
                parse3gpp(stream, maxLen - boxLen);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void parseAMR(InputStream stream, int maxLen) throws IOException {
        int[] prevEner = new int[4];
        for (int i = 0; i < 4; i++) {
            prevEner[i] = 0;
        }
        int[] prevEnerMR122 = new int[4];
        for (int i2 = 0; i2 < 4; i2++) {
            prevEnerMR122[i2] = -2381;
        }
        int originalMaxLen = maxLen;
        int bytesTotal = 0;
        while (maxLen > 0) {
            int bytesConsumed = parseAMRFrame(stream, maxLen, prevEner);
            bytesTotal += bytesConsumed;
            maxLen -= bytesConsumed;
            if (this.mProgressListener != null && !this.mProgressListener.reportProgress((((double) bytesTotal) * 1.0d) / ((double) originalMaxLen))) {
                return;
            }
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:85:0x00c2 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int parseAMRFrame(java.io.InputStream r57, int r58, int[] r59) throws java.io.IOException {
        /*
            r56 = this;
            r0 = r56
            int r0 = r0.mOffset
            r22 = r0
            r4 = 1
            r0 = r4
            byte[] r0 = new byte[r0]
            r25 = r0
            r4 = 0
            r5 = 1
            r0 = r57
            r1 = r25
            r2 = r4
            r3 = r5
            r0.read(r1, r2, r3)
            r0 = r56
            int r0 = r0.mOffset
            r4 = r0
            int r4 = r4 + 1
            r0 = r4
            r1 = r56
            r1.mOffset = r0
            r4 = 0
            byte r4 = r25[r4]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 >> 3
            int r24 = r4 % 15
            r4 = 0
            byte r4 = r25[r4]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 >> 2
            r23 = r4 & 1
            int[] r4 = me.ring.knou1.soundfile.CheapAMR.BLOCK_SIZES
            r13 = r4[r24]
            int r4 = r13 + 1
            r0 = r4
            r1 = r58
            if (r0 <= r1) goto L_0x0043
            r4 = r58
        L_0x0042:
            return r4
        L_0x0043:
            if (r13 != 0) goto L_0x0047
            r4 = 1
            goto L_0x0042
        L_0x0047:
            r0 = r13
            byte[] r0 = new byte[r0]
            r46 = r0
            r4 = 0
            r0 = r57
            r1 = r46
            r2 = r4
            r3 = r13
            r0.read(r1, r2, r3)
            r0 = r56
            int r0 = r0.mOffset
            r4 = r0
            int r4 = r4 + r13
            r0 = r4
            r1 = r56
            r1.mOffset = r0
            int r4 = r13 * 8
            int[] r5 = new int[r4]
            r32 = 0
            byte r4 = r46[r32]
            r0 = r4
            r0 = r0 & 255(0xff, float:3.57E-43)
            r47 = r0
            r31 = 0
        L_0x0070:
            int r4 = r13 * 8
            r0 = r31
            r1 = r4
            if (r0 < r1) goto L_0x00a0
            switch(r24) {
                case 1: goto L_0x00c5;
                case 7: goto L_0x01da;
                default: goto L_0x007a;
            }
        L_0x007a:
            java.io.PrintStream r4 = java.lang.System.out
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "Unsupported frame type: "
            r5.<init>(r6)
            r0 = r5
            r1 = r24
            java.lang.StringBuilder r5 = r0.append(r1)
            java.lang.String r5 = r5.toString()
            r4.println(r5)
            int r4 = r13 + 1
            r5 = 1
            r0 = r56
            r1 = r22
            r2 = r4
            r3 = r5
            r0.addFrame(r1, r2, r3)
        L_0x009d:
            int r4 = r13 + 1
            goto L_0x0042
        L_0x00a0:
            r0 = r47
            r0 = r0 & 128(0x80, float:1.794E-43)
            r4 = r0
            int r4 = r4 >> 7
            r5[r31] = r4
            int r47 = r47 << 1
            r4 = r31 & 7
            r6 = 7
            if (r4 != r6) goto L_0x00c2
            int r4 = r13 * 8
            r6 = 1
            int r4 = r4 - r6
            r0 = r31
            r1 = r4
            if (r0 >= r1) goto L_0x00c2
            int r32 = r32 + 1
            byte r4 = r46[r32]
            r0 = r4
            r0 = r0 & 255(0xff, float:3.57E-43)
            r47 = r0
        L_0x00c2:
            int r31 = r31 + 1
            goto L_0x0070
        L_0x00c5:
            r4 = 5
            r0 = r4
            r1 = r56
            r1.mBitRate = r0
            r4 = 4
            r0 = r4
            int[] r0 = new int[r0]
            r28 = r0
            r4 = 0
            r6 = 24
            r6 = r5[r6]
            int r6 = r6 * 1
            r7 = 25
            r7 = r5[r7]
            int r7 = r7 * 2
            int r6 = r6 + r7
            r7 = 26
            r7 = r5[r7]
            int r7 = r7 * 4
            int r6 = r6 + r7
            r7 = 36
            r7 = r5[r7]
            int r7 = r7 * 8
            int r6 = r6 + r7
            r7 = 45
            r7 = r5[r7]
            int r7 = r7 * 16
            int r6 = r6 + r7
            r7 = 55
            r7 = r5[r7]
            int r7 = r7 * 32
            int r6 = r6 + r7
            r28[r4] = r6
            r4 = 1
            r6 = 27
            r6 = r5[r6]
            int r6 = r6 * 1
            r7 = 28
            r7 = r5[r7]
            int r7 = r7 * 2
            int r6 = r6 + r7
            r7 = 29
            r7 = r5[r7]
            int r7 = r7 * 4
            int r6 = r6 + r7
            r7 = 37
            r7 = r5[r7]
            int r7 = r7 * 8
            int r6 = r6 + r7
            r7 = 46
            r7 = r5[r7]
            int r7 = r7 * 16
            int r6 = r6 + r7
            r7 = 56
            r7 = r5[r7]
            int r7 = r7 * 32
            int r6 = r6 + r7
            r28[r4] = r6
            r4 = 2
            r6 = 30
            r6 = r5[r6]
            int r6 = r6 * 1
            r7 = 31
            r7 = r5[r7]
            int r7 = r7 * 2
            int r6 = r6 + r7
            r7 = 32
            r7 = r5[r7]
            int r7 = r7 * 4
            int r6 = r6 + r7
            r7 = 38
            r7 = r5[r7]
            int r7 = r7 * 8
            int r6 = r6 + r7
            r7 = 47
            r7 = r5[r7]
            int r7 = r7 * 16
            int r6 = r6 + r7
            r7 = 57
            r7 = r5[r7]
            int r7 = r7 * 32
            int r6 = r6 + r7
            r28[r4] = r6
            r4 = 3
            r6 = 33
            r6 = r5[r6]
            int r6 = r6 * 1
            r7 = 34
            r7 = r5[r7]
            int r7 = r7 * 2
            int r6 = r6 + r7
            r7 = 35
            r7 = r5[r7]
            int r7 = r7 * 4
            int r6 = r6 + r7
            r7 = 39
            r7 = r5[r7]
            int r7 = r7 * 8
            int r6 = r6 + r7
            r7 = 48
            r7 = r5[r7]
            int r7 = r7 * 16
            int r6 = r6 + r7
            r7 = 58
            r5 = r5[r7]
            int r5 = r5 * 32
            int r5 = r5 + r6
            r28[r4] = r5
            r31 = 0
        L_0x0183:
            r4 = 4
            r0 = r31
            r1 = r4
            if (r0 >= r1) goto L_0x009d
            r4 = 385963008(0x17015400, float:4.178817E-25)
            r5 = 0
            r5 = r59[r5]
            int r5 = r5 * 5571
            int r4 = r4 + r5
            r5 = 1
            r5 = r59[r5]
            int r5 = r5 * 4751
            int r4 = r4 + r5
            r5 = 2
            r5 = r59[r5]
            int r5 = r5 * 2785
            int r4 = r4 + r5
            r5 = 3
            r5 = r59[r5]
            int r5 = r5 * 1556
            int r4 = r4 + r5
            int r30 = r4 >> 15
            int[] r4 = me.ring.knou1.soundfile.CheapAMR.QUA_ENER_MR515
            r5 = r28[r31]
            r42 = r4[r5]
            int[] r4 = me.ring.knou1.soundfile.CheapAMR.GAIN_FAC_MR515
            r5 = r28[r31]
            r27 = r4[r5]
            r4 = 3
            r5 = 2
            r5 = r59[r5]
            r59[r4] = r5
            r4 = 2
            r5 = 1
            r5 = r59[r5]
            r59[r4] = r5
            r4 = 1
            r5 = 0
            r5 = r59[r5]
            r59[r4] = r5
            r4 = 0
            r59[r4] = r42
            int r4 = r30 * r27
            int r21 = r4 >> 24
            int r4 = r13 + 1
            r0 = r56
            r1 = r22
            r2 = r4
            r3 = r21
            r0.addFrame(r1, r2, r3)
            int r31 = r31 + 1
            goto L_0x0183
        L_0x01da:
            r4 = 12
            r0 = r4
            r1 = r56
            r1.mBitRate = r0
            r4 = 4
            int[] r6 = new int[r4]
            r4 = 4
            int[] r7 = new int[r4]
            r4 = 4
            int[] r8 = new int[r4]
            r4 = 4
            int[][] r9 = new int[r4][]
            r31 = 0
        L_0x01ef:
            r4 = 4
            r0 = r31
            r1 = r4
            if (r0 < r1) goto L_0x0338
            r4 = r56
            r4.getMR122Params(r5, r6, r7, r8, r9)
            r10 = 0
            r45 = 0
        L_0x01fd:
            r4 = 4
            r0 = r45
            r1 = r4
            if (r0 >= r1) goto L_0x009d
            r4 = 40
            int[] r14 = new int[r4]
            r31 = 0
        L_0x0209:
            r4 = 40
            r0 = r31
            r1 = r4
            if (r0 < r1) goto L_0x0342
            r34 = 0
        L_0x0212:
            r4 = 5
            r0 = r34
            r1 = r4
            if (r0 < r1) goto L_0x0349
            r33 = r6[r45]
            if (r45 == 0) goto L_0x0222
            r4 = 2
            r0 = r45
            r1 = r4
            if (r0 != r1) goto L_0x0393
        L_0x0222:
            r4 = 463(0x1cf, float:6.49E-43)
            r0 = r33
            r1 = r4
            if (r0 >= r1) goto L_0x038d
            int r4 = r33 + 5
            int r4 = r4 / 6
            int r10 = r4 + 17
        L_0x022f:
            int[] r4 = me.ring.knou1.soundfile.CheapAMR.QUA_GAIN_PITCH
            r5 = r7[r45]
            r4 = r4[r5]
            int r4 = r4 >> 2
            int r39 = r4 << 2
            r4 = 16383(0x3fff, float:2.2957E-41)
            r0 = r39
            r1 = r4
            if (r0 <= r1) goto L_0x03b8
            r39 = 32767(0x7fff, float:4.5916E-41)
        L_0x0242:
            r34 = r10
        L_0x0244:
            r4 = 40
            r0 = r34
            r1 = r4
            if (r0 < r1) goto L_0x03bc
            r16 = 0
            r34 = 0
        L_0x024f:
            r4 = 40
            r0 = r34
            r1 = r4
            if (r0 < r1) goto L_0x03cd
            r4 = 1073741823(0x3fffffff, float:1.9999999)
            r0 = r4
            r1 = r16
            if (r0 <= r1) goto L_0x0260
            if (r16 >= 0) goto L_0x03d8
        L_0x0260:
            r16 = 2147483647(0x7fffffff, float:NaN)
        L_0x0263:
            r4 = 32768(0x8000, float:4.5918E-41)
            int r4 = r4 + r16
            int r4 = r4 >> 16
            r5 = 52428(0xcccc, float:7.3467E-41)
            int r16 = r4 * r5
            r0 = r16
            double r0 = (double) r0
            r48 = r0
            double r48 = java.lang.Math.log(r48)
            r50 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r50 = java.lang.Math.log(r50)
            double r35 = r48 / r50
            r0 = r35
            int r0 = (int) r0
            r17 = r0
            r0 = r17
            double r0 = (double) r0
            r48 = r0
            double r48 = r35 - r48
            r50 = 4674736413210574848(0x40e0000000000000, double:32768.0)
            double r48 = r48 * r50
            r0 = r48
            int r0 = (int) r0
            r19 = r0
            r4 = 30
            int r4 = r17 - r4
            int r4 = r4 << 16
            int r5 = r19 * 2
            int r16 = r4 + r5
            r4 = 0
            r4 = r59[r4]
            int r4 = r4 * 44
            r5 = 1
            r5 = r59[r5]
            int r5 = r5 * 37
            int r4 = r4 + r5
            r5 = 2
            r5 = r59[r5]
            int r5 = r5 * 22
            int r4 = r4 + r5
            r5 = 3
            r5 = r59[r5]
            int r5 = r5 * 12
            int r15 = r4 + r5
            int r4 = r15 * 2
            r5 = 783741(0xbf57d, float:1.098255E-39)
            int r15 = r4 + r5
            int r4 = r15 - r16
            int r15 = r4 / 2
            int r18 = r15 >> 16
            int r4 = r15 >> 1
            int r5 = r18 << 15
            int r20 = r4 - r5
            r48 = 4611686018427387904(0x4000000000000000, double:2.0)
            r0 = r18
            double r0 = (double) r0
            r50 = r0
            r0 = r20
            double r0 = (double) r0
            r52 = r0
            r54 = 4674736413210574848(0x40e0000000000000, double:32768.0)
            double r52 = r52 / r54
            double r50 = r50 + r52
            double r48 = java.lang.Math.pow(r48, r50)
            r50 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            double r48 = r48 + r50
            r0 = r48
            int r0 = (int) r0
            r26 = r0
            r4 = 2047(0x7ff, float:2.868E-42)
            r0 = r26
            r1 = r4
            if (r0 > r1) goto L_0x03dc
            int r26 = r26 << 4
        L_0x02f2:
            r33 = r8[r45]
            int[] r4 = me.ring.knou1.soundfile.CheapAMR.QUA_GAIN_CODE
            int r5 = r33 * 3
            r4 = r4[r5]
            int r4 = r4 * r26
            int r4 = r4 >> 15
            int r29 = r4 << 1
            r0 = r29
            r0 = r0 & -32768(0xffffffffffff8000, float:NaN)
            r4 = r0
            if (r4 == 0) goto L_0x0309
            r29 = 32767(0x7fff, float:4.5916E-41)
        L_0x0309:
            r21 = r29
            int r4 = r13 + 1
            r0 = r56
            r1 = r22
            r2 = r4
            r3 = r21
            r0.addFrame(r1, r2, r3)
            int[] r4 = me.ring.knou1.soundfile.CheapAMR.QUA_GAIN_CODE
            int r5 = r33 * 3
            int r5 = r5 + 1
            r43 = r4[r5]
            r4 = 3
            r5 = 2
            r5 = r59[r5]
            r59[r4] = r5
            r4 = 2
            r5 = 1
            r5 = r59[r5]
            r59[r4] = r5
            r4 = 1
            r5 = 0
            r5 = r59[r5]
            r59[r4] = r5
            r4 = 0
            r59[r4] = r43
            int r45 = r45 + 1
            goto L_0x01fd
        L_0x0338:
            r4 = 10
            int[] r4 = new int[r4]
            r9[r31] = r4
            int r31 = r31 + 1
            goto L_0x01ef
        L_0x0342:
            r4 = 0
            r14[r31] = r4
            int r31 = r31 + 1
            goto L_0x0209
        L_0x0349:
            r4 = r9[r45]
            r4 = r4[r34]
            int r4 = r4 >> 3
            r4 = r4 & 1
            if (r4 != 0) goto L_0x038a
            r44 = 4096(0x1000, float:5.74E-42)
        L_0x0355:
            int[] r4 = me.ring.knou1.soundfile.CheapAMR.GRAY
            r5 = r9[r45]
            r5 = r5[r34]
            r5 = r5 & 7
            r4 = r4[r5]
            int r4 = r4 * 5
            int r40 = r34 + r4
            int[] r4 = me.ring.knou1.soundfile.CheapAMR.GRAY
            r5 = r9[r45]
            int r11 = r34 + 5
            r5 = r5[r11]
            r5 = r5 & 7
            r4 = r4[r5]
            int r4 = r4 * 5
            int r41 = r34 + r4
            r14[r40] = r44
            r0 = r41
            r1 = r40
            if (r0 >= r1) goto L_0x0380
            r0 = r44
            int r0 = -r0
            r44 = r0
        L_0x0380:
            r4 = r14[r41]
            int r4 = r4 + r44
            r14[r41] = r4
            int r34 = r34 + 1
            goto L_0x0212
        L_0x038a:
            r44 = -4096(0xfffffffffffff000, float:NaN)
            goto L_0x0355
        L_0x038d:
            r4 = 368(0x170, float:5.16E-43)
            int r10 = r33 - r4
            goto L_0x022f
        L_0x0393:
            r38 = 18
            r37 = 143(0x8f, float:2.0E-43)
            r4 = 5
            int r12 = r10 - r4
            r0 = r12
            r1 = r38
            if (r0 >= r1) goto L_0x03a1
            r12 = r38
        L_0x03a1:
            int r11 = r12 + 9
            r0 = r11
            r1 = r37
            if (r0 <= r1) goto L_0x03ae
            r11 = r37
            r4 = 9
            int r12 = r11 - r4
        L_0x03ae:
            int r4 = r33 + 5
            int r4 = r4 / 6
            int r4 = r4 + r12
            r5 = 1
            int r10 = r4 - r5
            goto L_0x022f
        L_0x03b8:
            int r39 = r39 * 2
            goto L_0x0242
        L_0x03bc:
            r4 = r14[r34]
            int r5 = r34 - r10
            r5 = r14[r5]
            int r5 = r5 * r39
            int r5 = r5 >> 15
            int r4 = r4 + r5
            r14[r34] = r4
            int r34 = r34 + 1
            goto L_0x0244
        L_0x03cd:
            r4 = r14[r34]
            r5 = r14[r34]
            int r4 = r4 * r5
            int r16 = r16 + r4
            int r34 = r34 + 1
            goto L_0x024f
        L_0x03d8:
            int r16 = r16 * 2
            goto L_0x0263
        L_0x03dc:
            r26 = 32767(0x7fff, float:4.5916E-41)
            goto L_0x02f2
        */
        throw new UnsupportedOperationException("Method not decompiled: me.ring.knou1.soundfile.CheapAMR.parseAMRFrame(java.io.InputStream, int, int[]):int");
    }

    /* access modifiers changed from: package-private */
    public void addFrame(int offset, int frameSize, int gain) {
        this.mFrameOffsets[this.mNumFrames] = offset;
        this.mFrameLens[this.mNumFrames] = frameSize;
        this.mFrameGains[this.mNumFrames] = gain;
        if (gain < this.mMinGain) {
            this.mMinGain = gain;
        }
        if (gain > this.mMaxGain) {
            this.mMaxGain = gain;
        }
        this.mNumFrames++;
        if (this.mNumFrames == this.mMaxFrames) {
            int newMaxFrames = this.mMaxFrames * 2;
            int[] newOffsets = new int[newMaxFrames];
            int[] newLens = new int[newMaxFrames];
            int[] newGains = new int[newMaxFrames];
            for (int i = 0; i < this.mNumFrames; i++) {
                newOffsets[i] = this.mFrameOffsets[i];
                newLens[i] = this.mFrameLens[i];
                newGains[i] = this.mFrameGains[i];
            }
            this.mFrameOffsets = newOffsets;
            this.mFrameLens = newLens;
            this.mFrameGains = newGains;
            this.mMaxFrames = newMaxFrames;
        }
    }

    public void WriteFile(File outputFile, int startFrame, int numFrames) throws IOException {
        outputFile.createNewFile();
        FileInputStream in = new FileInputStream(this.mInputFile);
        FileOutputStream out = new FileOutputStream(outputFile);
        out.write(new byte[]{35, 33, 65, 77, 82, 10}, 0, 6);
        int maxFrameLen = 0;
        for (int i = 0; i < numFrames; i++) {
            if (this.mFrameLens[startFrame + i] > maxFrameLen) {
                maxFrameLen = this.mFrameLens[startFrame + i];
            }
        }
        byte[] buffer = new byte[maxFrameLen];
        int pos = 0;
        for (int i2 = 0; i2 < numFrames; i2++) {
            int skip = this.mFrameOffsets[startFrame + i2] - pos;
            int len = this.mFrameLens[startFrame + i2];
            if (skip >= 0) {
                if (skip > 0) {
                    in.skip((long) skip);
                    pos += skip;
                }
                in.read(buffer, 0, len);
                out.write(buffer, 0, len);
                pos += len;
            }
        }
        in.close();
        out.close();
    }

    /* access modifiers changed from: package-private */
    public void getMR122Params(int[] bits, int[] adaptiveIndex, int[] adaptiveGain, int[] fixedGain, int[][] pulse) {
        adaptiveIndex[0] = (bits[45] * 1) + (bits[43] * 2) + (bits[41] * 4) + (bits[39] * 8) + (bits[37] * 16) + (bits[35] * 32) + (bits[33] * 64) + (bits[31] * 128) + (bits[29] * 256);
        adaptiveIndex[1] = (bits[242] * 1) + (bits[79] * 2) + (bits[77] * 4) + (bits[75] * 8) + (bits[73] * 16) + (bits[71] * 32);
        adaptiveIndex[2] = (bits[46] * 1) + (bits[44] * 2) + (bits[42] * 4) + (bits[40] * 8) + (bits[38] * 16) + (bits[36] * 32) + (bits[34] * 64) + (bits[32] * 128) + (bits[30] * 256);
        adaptiveIndex[3] = (bits[243] * 1) + (bits[80] * 2) + (bits[78] * 4) + (bits[76] * 8) + (bits[74] * 16) + (bits[72] * 32);
        adaptiveGain[0] = (bits[88] * 1) + (bits[55] * 2) + (bits[51] * 4) + (bits[47] * 8);
        adaptiveGain[1] = (bits[89] * 1) + (bits[56] * 2) + (bits[52] * 4) + (bits[48] * 8);
        adaptiveGain[2] = (bits[90] * 1) + (bits[57] * 2) + (bits[53] * 4) + (bits[49] * 8);
        adaptiveGain[3] = (bits[91] * 1) + (bits[58] * 2) + (bits[54] * 4) + (bits[50] * 8);
        fixedGain[0] = (bits[104] * 1) + (bits[92] * 2) + (bits[67] * 4) + (bits[63] * 8) + (bits[59] * 16);
        fixedGain[1] = (bits[105] * 1) + (bits[93] * 2) + (bits[68] * 4) + (bits[64] * 8) + (bits[60] * 16);
        fixedGain[2] = (bits[106] * 1) + (bits[94] * 2) + (bits[69] * 4) + (bits[65] * 8) + (bits[61] * 16);
        fixedGain[3] = (bits[107] * 1) + (bits[95] * 2) + (bits[70] * 4) + (bits[66] * 8) + (bits[62] * 16);
        pulse[0][0] = (bits[122] * 1) + (bits[123] * 2) + (bits[124] * 4) + (bits[96] * 8);
        pulse[0][1] = (bits[125] * 1) + (bits[126] * 2) + (bits[127] * 4) + (bits[100] * 8);
        pulse[0][2] = (bits[128] * 1) + (bits[129] * 2) + (bits[130] * 4) + (bits[108] * 8);
        pulse[0][3] = (bits[131] * 1) + (bits[132] * 2) + (bits[133] * 4) + (bits[112] * 8);
        pulse[0][4] = (bits[134] * 1) + (bits[135] * 2) + (bits[136] * 4) + (bits[116] * 8);
        pulse[0][5] = (bits[182] * 1) + (bits[183] * 2) + (bits[184] * 4);
        pulse[0][6] = (bits[185] * 1) + (bits[186] * 2) + (bits[187] * 4);
        pulse[0][7] = (bits[188] * 1) + (bits[189] * 2) + (bits[190] * 4);
        pulse[0][8] = (bits[191] * 1) + (bits[192] * 2) + (bits[193] * 4);
        pulse[0][9] = (bits[194] * 1) + (bits[195] * 2) + (bits[196] * 4);
        pulse[1][0] = (bits[137] * 1) + (bits[138] * 2) + (bits[139] * 4) + (bits[97] * 8);
        pulse[1][1] = (bits[140] * 1) + (bits[141] * 2) + (bits[142] * 4) + (bits[101] * 8);
        pulse[1][2] = (bits[143] * 1) + (bits[144] * 2) + (bits[145] * 4) + (bits[109] * 8);
        pulse[1][3] = (bits[146] * 1) + (bits[147] * 2) + (bits[148] * 4) + (bits[113] * 8);
        pulse[1][4] = (bits[149] * 1) + (bits[150] * 2) + (bits[151] * 4) + (bits[117] * 8);
        pulse[1][5] = (bits[197] * 1) + (bits[198] * 2) + (bits[199] * 4);
        pulse[1][6] = (bits[200] * 1) + (bits[201] * 2) + (bits[202] * 4);
        pulse[1][7] = (bits[203] * 1) + (bits[204] * 2) + (bits[205] * 4);
        pulse[1][8] = (bits[206] * 1) + (bits[207] * 2) + (bits[208] * 4);
        pulse[1][9] = (bits[209] * 1) + (bits[210] * 2) + (bits[211] * 4);
        pulse[2][0] = (bits[152] * 1) + (bits[153] * 2) + (bits[154] * 4) + (bits[98] * 8);
        pulse[2][1] = (bits[155] * 1) + (bits[156] * 2) + (bits[157] * 4) + (bits[102] * 8);
        pulse[2][2] = (bits[158] * 1) + (bits[159] * 2) + (bits[160] * 4) + (bits[110] * 8);
        pulse[2][3] = (bits[161] * 1) + (bits[162] * 2) + (bits[163] * 4) + (bits[114] * 8);
        pulse[2][4] = (bits[164] * 1) + (bits[165] * 2) + (bits[166] * 4) + (bits[118] * 8);
        pulse[2][5] = (bits[212] * 1) + (bits[213] * 2) + (bits[214] * 4);
        pulse[2][6] = (bits[215] * 1) + (bits[216] * 2) + (bits[217] * 4);
        pulse[2][7] = (bits[218] * 1) + (bits[219] * 2) + (bits[220] * 4);
        pulse[2][8] = (bits[221] * 1) + (bits[222] * 2) + (bits[223] * 4);
        pulse[2][9] = (bits[224] * 1) + (bits[225] * 2) + (bits[226] * 4);
        pulse[3][0] = (bits[167] * 1) + (bits[168] * 2) + (bits[169] * 4) + (bits[99] * 8);
        pulse[3][1] = (bits[170] * 1) + (bits[171] * 2) + (bits[172] * 4) + (bits[103] * 8);
        pulse[3][2] = (bits[173] * 1) + (bits[174] * 2) + (bits[175] * 4) + (bits[111] * 8);
        pulse[3][3] = (bits[176] * 1) + (bits[177] * 2) + (bits[178] * 4) + (bits[115] * 8);
        pulse[3][4] = (bits[179] * 1) + (bits[180] * 2) + (bits[181] * 4) + (bits[119] * 8);
        pulse[3][5] = (bits[227] * 1) + (bits[228] * 2) + (bits[229] * 4);
        pulse[3][6] = (bits[230] * 1) + (bits[231] * 2) + (bits[232] * 4);
        pulse[3][7] = (bits[233] * 1) + (bits[234] * 2) + (bits[235] * 4);
        pulse[3][8] = (bits[236] * 1) + (bits[237] * 2) + (bits[238] * 4);
        pulse[3][9] = (bits[239] * 1) + (bits[240] * 2) + (bits[241] * 4);
    }

    static {
        int[] iArr = new int[16];
        iArr[0] = 12;
        iArr[1] = 13;
        iArr[2] = 15;
        iArr[3] = 17;
        iArr[4] = 19;
        iArr[5] = 20;
        iArr[6] = 26;
        iArr[7] = 31;
        iArr[8] = 5;
        BLOCK_SIZES = iArr;
        int[] iArr2 = new int[65];
        iArr2[0] = 17333;
        iArr2[1] = -3431;
        iArr2[2] = 4235;
        iArr2[3] = 5276;
        iArr2[4] = 8325;
        iArr2[5] = -10422;
        iArr2[6] = 683;
        iArr2[7] = -8609;
        iArr2[8] = 10148;
        iArr2[9] = -4398;
        iArr2[10] = 1472;
        iArr2[11] = -4398;
        iArr2[12] = 5802;
        iArr2[13] = -6907;
        iArr2[14] = -2327;
        iArr2[15] = -7303;
        iArr2[16] = 14189;
        iArr2[17] = -2678;
        iArr2[18] = 3181;
        iArr2[19] = -180;
        iArr2[20] = 6972;
        iArr2[21] = -9599;
        iArr2[23] = -16305;
        iArr2[24] = 10884;
        iArr2[25] = -2444;
        iArr2[26] = 1165;
        iArr2[27] = -3697;
        iArr2[28] = 4180;
        iArr2[29] = -13468;
        iArr2[30] = -3833;
        iArr2[31] = -16305;
        iArr2[32] = 15543;
        iArr2[33] = -4546;
        iArr2[34] = 1913;
        iArr2[36] = 6556;
        iArr2[37] = -15255;
        iArr2[38] = 347;
        iArr2[39] = -5993;
        iArr2[40] = 9771;
        iArr2[41] = -9090;
        iArr2[42] = 1086;
        iArr2[43] = -9341;
        iArr2[44] = 4772;
        iArr2[45] = -15255;
        iArr2[46] = -5321;
        iArr2[47] = -10714;
        iArr2[48] = 12827;
        iArr2[49] = -5002;
        iArr2[50] = 3118;
        iArr2[51] = -938;
        iArr2[52] = 6598;
        iArr2[53] = -14774;
        iArr2[54] = -646;
        iArr2[55] = -16879;
        iArr2[56] = 7251;
        iArr2[57] = -7508;
        iArr2[58] = -1343;
        iArr2[59] = -6529;
        iArr2[60] = 2668;
        iArr2[61] = -15255;
        iArr2[62] = -2212;
        iArr2[63] = -2454;
        iArr2[64] = -14774;
        QUA_ENER_MR515 = iArr2;
        int[] iArr3 = new int[8];
        iArr3[1] = 1;
        iArr3[2] = 3;
        iArr3[3] = 2;
        iArr3[4] = 5;
        iArr3[5] = 6;
        iArr3[6] = 4;
        iArr3[7] = 7;
        GRAY = iArr3;
        int[] iArr4 = new int[16];
        iArr4[1] = 3277;
        iArr4[2] = 6556;
        iArr4[3] = 8192;
        iArr4[4] = 9830;
        iArr4[5] = 11469;
        iArr4[6] = 12288;
        iArr4[7] = 13107;
        iArr4[8] = 13926;
        iArr4[9] = 14746;
        iArr4[10] = 15565;
        iArr4[11] = 16384;
        iArr4[12] = 17203;
        iArr4[13] = 18022;
        iArr4[14] = 18842;
        iArr4[15] = 19661;
        QUA_GAIN_PITCH = iArr4;
    }
}
