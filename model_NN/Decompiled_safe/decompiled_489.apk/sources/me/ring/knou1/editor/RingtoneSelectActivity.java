package me.ring.knou1.editor;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.MergeCursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.qwapi.adclient.android.utils.Utils;
import java.io.File;
import java.util.ArrayList;
import me.ring.knou1.R;
import me.ring.knou1.data.Constants;
import me.ring.knou1.data.Downloaded;
import me.ring.knou1.soundfile.CheapSoundFile;

public class RingtoneSelectActivity extends ListActivity implements TextWatcher {
    private static final int CMD_DELETE = 4;
    private static final int CMD_EDIT = 3;
    private static final int CMD_SHOW_ALL = 2;
    private static final String[] EXTERNAL_COLUMNS = {"_id", "_data", Downloaded.COL_TITLE, Downloaded.COL_ARTIST, "album", "is_ringtone", "is_alarm", "is_notification", "is_music", "\"" + MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "\""};
    private static final String[] INTERNAL_COLUMNS = {"_id", "_data", Downloaded.COL_TITLE, Downloaded.COL_ARTIST, "album", "is_ringtone", "is_alarm", "is_notification", "is_music", "\"" + MediaStore.Audio.Media.INTERNAL_CONTENT_URI + "\""};
    private static final int REQUEST_CODE_EDIT = 1;
    private SimpleCursorAdapter mAdapter;
    private TextView mFilter;
    private boolean mShowAll;
    private boolean mWasGetContentIntent = false;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        this.mShowAll = false;
        String status = Environment.getExternalStorageState();
        if (status.equals("mounted_ro")) {
            showFinalAlert(getResources().getText(R.string.sdcard_readonly));
        } else if (status.equals("shared")) {
            showFinalAlert(getResources().getText(R.string.sdcard_shared));
        } else if (!status.equals("mounted")) {
            showFinalAlert(getResources().getText(R.string.no_sdcard));
        } else {
            Intent intent = getIntent();
            if (intent.getAction() != null) {
                this.mWasGetContentIntent = intent.getAction().equals("android.intent.action.GET_CONTENT");
            }
            setContentView((int) R.layout.media_select);
            ((Button) findViewById(R.id.record)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View clickedButton) {
                    RingtoneSelectActivity.this.onRecord();
                }
            });
            try {
                this.mAdapter = new SimpleCursorAdapter(this, R.layout.media_select_row, createCursor(Utils.EMPTY_STRING), new String[]{Downloaded.COL_ARTIST, "album", Downloaded.COL_TITLE, "_id"}, new int[]{R.id.row_artist, R.id.row_album, R.id.row_title, R.id.row_icon});
                setListAdapter(this.mAdapter);
                getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView parent, View view, int position, long id) {
                        RingtoneSelectActivity.this.startRingdroidEditor();
                    }
                });
            } catch (SecurityException e) {
                Log.e(Constants.PKG_NAME, e.toString());
            } catch (IllegalArgumentException e2) {
                Log.e(Constants.PKG_NAME, e2.toString());
            }
            this.mAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
                public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                    if (view.getId() != R.id.row_icon) {
                        return false;
                    }
                    RingtoneSelectActivity.this.setSoundIconFromCursor((ImageView) view, cursor);
                    return true;
                }
            });
            registerForContextMenu(getListView());
            this.mFilter = (TextView) findViewById(R.id.search_filter);
            if (this.mFilter != null) {
                this.mFilter.addTextChangedListener(this);
            }
        }
    }

    /* access modifiers changed from: private */
    public void setSoundIconFromCursor(ImageView view, Cursor cursor) {
        if (cursor.getInt(cursor.getColumnIndexOrThrow("is_ringtone")) != 0) {
            view.setImageResource(R.drawable.type_ringtone);
            ((View) view.getParent()).setBackgroundColor(getResources().getColor(R.drawable.type_bkgnd_ringtone));
        } else if (cursor.getInt(cursor.getColumnIndexOrThrow("is_alarm")) != 0) {
            view.setImageResource(R.drawable.type_alarm);
            ((View) view.getParent()).setBackgroundColor(getResources().getColor(R.drawable.type_bkgnd_alarm));
        } else if (cursor.getInt(cursor.getColumnIndexOrThrow("is_notification")) != 0) {
            view.setImageResource(R.drawable.type_notification);
            ((View) view.getParent()).setBackgroundColor(getResources().getColor(R.drawable.type_bkgnd_notification));
        } else if (cursor.getInt(cursor.getColumnIndexOrThrow("is_music")) != 0) {
            view.setImageResource(R.drawable.type_music);
            ((View) view.getParent()).setBackgroundColor(getResources().getColor(R.drawable.type_bkgnd_music));
        }
        if (!CheapSoundFile.isFilenameSupported(cursor.getString(cursor.getColumnIndexOrThrow("_data")))) {
            ((View) view.getParent()).setBackgroundColor(getResources().getColor(R.drawable.type_bkgnd_unsupported));
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent dataIntent) {
        if (requestCode == 1 && resultCode == -1) {
            setResult(-1, dataIntent);
            finish();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 2, 0, (int) R.string.menu_show_all_audio).setIcon((int) R.drawable.menu_show_all_audio);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean z;
        super.onPrepareOptionsMenu(menu);
        menu.findItem(2).setVisible(true);
        MenuItem findItem = menu.findItem(2);
        if (this.mShowAll) {
            z = false;
        } else {
            z = true;
        }
        findItem.setEnabled(z);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2:
                this.mShowAll = true;
                refreshListView();
                return true;
            default:
                return false;
        }
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        Cursor c = this.mAdapter.getCursor();
        menu.setHeaderTitle(c.getString(c.getColumnIndexOrThrow(Downloaded.COL_TITLE)));
        menu.add(0, 3, 0, (int) R.string.context_menu_edit);
        menu.add(0, 4, 0, (int) R.string.context_menu_delete);
    }

    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case 3:
                startRingdroidEditor();
                return true;
            case 4:
                confirmDelete();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void confirmDelete() {
        CharSequence message;
        CharSequence title;
        Cursor c = this.mAdapter.getCursor();
        int columnIndexOrThrow = c.getColumnIndexOrThrow(Downloaded.COL_ARTIST);
        if (c.getString(c.getColumnIndexOrThrow(Downloaded.COL_ARTIST)).equals(getResources().getText(R.string.artist_name))) {
            message = getResources().getText(R.string.confirm_delete_ringdroid);
        } else {
            message = getResources().getText(R.string.confirm_delete_non_ringdroid);
        }
        if (c.getInt(c.getColumnIndexOrThrow("is_ringtone")) != 0) {
            title = getResources().getText(R.string.delete_ringtone);
        } else if (c.getInt(c.getColumnIndexOrThrow("is_alarm")) != 0) {
            title = getResources().getText(R.string.delete_alarm);
        } else if (c.getInt(c.getColumnIndexOrThrow("is_notification")) != 0) {
            title = getResources().getText(R.string.delete_notification);
        } else if (c.getInt(c.getColumnIndexOrThrow("is_music")) != 0) {
            title = getResources().getText(R.string.delete_music);
        } else {
            title = getResources().getText(R.string.delete_audio);
        }
        new AlertDialog.Builder(this).setTitle(title).setMessage(message).setPositiveButton((int) R.string.delete_ok_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                RingtoneSelectActivity.this.onDelete();
            }
        }).setNegativeButton((int) R.string.delete_cancel_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).setCancelable(true).show();
    }

    /* access modifiers changed from: private */
    public void onDelete() {
        Cursor c = this.mAdapter.getCursor();
        String filename = c.getString(c.getColumnIndexOrThrow("_data"));
        int uriIndex = c.getColumnIndex("\"" + MediaStore.Audio.Media.INTERNAL_CONTENT_URI + "\"");
        if (uriIndex == -1) {
            uriIndex = c.getColumnIndex("\"" + MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "\"");
        }
        if (uriIndex == -1) {
            showFinalAlert(getResources().getText(R.string.delete_failed));
            return;
        }
        if (!new File(filename).delete()) {
            showFinalAlert(getResources().getText(R.string.delete_failed));
        }
        getContentResolver().delete(Uri.parse(String.valueOf(c.getString(uriIndex)) + "/" + c.getString(c.getColumnIndexOrThrow("_id"))), null, null);
    }

    private void showFinalAlert(CharSequence message) {
        new AlertDialog.Builder(this).setTitle(getResources().getText(R.string.alert_title_failure)).setMessage(message).setPositiveButton((int) R.string.alert_ok_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                RingtoneSelectActivity.this.finish();
            }
        }).setCancelable(false).show();
    }

    /* access modifiers changed from: private */
    public void onRecord() {
        try {
            Intent intent = new Intent("android.intent.action.EDIT", Uri.parse("record"));
            intent.putExtra("was_get_content_intent", this.mWasGetContentIntent);
            intent.setClass(this, RingtoneEditActivity.class);
            startActivityForResult(intent, 1);
        } catch (Exception e) {
            Log.e(Constants.PKG_NAME, "Couldn't start editor");
        }
    }

    /* access modifiers changed from: private */
    public void startRingdroidEditor() {
        Cursor c = this.mAdapter.getCursor();
        try {
            Intent intent = new Intent("android.intent.action.EDIT", Uri.parse(c.getString(c.getColumnIndexOrThrow("_data"))));
            intent.putExtra("was_get_content_intent", this.mWasGetContentIntent);
            intent.setClass(this, RingtoneEditActivity.class);
            startActivityForResult(intent, 1);
        } catch (Exception e) {
            Log.e(Constants.PKG_NAME, "Couldn't start editor");
        }
    }

    private Cursor getInternalAudioCursor(String selection, String[] selectionArgs) {
        return managedQuery(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, INTERNAL_COLUMNS, selection, selectionArgs, "title_key");
    }

    private Cursor getExternalAudioCursor(String selection, String[] selectionArgs) {
        return managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, EXTERNAL_COLUMNS, selection, selectionArgs, "title_key");
    }

    /* access modifiers changed from: package-private */
    public Cursor createCursor(String filter) {
        String selection;
        ArrayList<String> args = new ArrayList<>();
        if (this.mShowAll) {
            selection = "(_DATA LIKE ?)";
            args.add("%");
        } else {
            String selection2 = "(";
            String[] supportedExtensions = CheapSoundFile.getSupportedExtensions();
            int length = supportedExtensions.length;
            for (int i = 0; i < length; i++) {
                args.add("%." + supportedExtensions[i]);
                if (selection2.length() > 1) {
                    selection2 = String.valueOf(selection2) + " OR ";
                }
                selection2 = String.valueOf(selection2) + "(_DATA LIKE ?)";
            }
            selection = "(" + (String.valueOf(selection2) + ")") + ") AND (_DATA NOT LIKE ?)";
            args.add("%espeak-data/scratch%");
        }
        if (filter != null && filter.length() > 0) {
            String filter2 = "%" + filter + "%";
            selection = "(" + selection + " AND " + "((TITLE LIKE ?) OR (ARTIST LIKE ?) OR (ALBUM LIKE ?)))";
            args.add(filter2);
            args.add(filter2);
            args.add(filter2);
        }
        String[] argsArray = (String[]) args.toArray(new String[args.size()]);
        Cursor externalAudioCursor = getExternalAudioCursor(selection, argsArray);
        Cursor internalAudioCursor = getInternalAudioCursor(selection, argsArray);
        Cursor c = new MergeCursor(new Cursor[]{getExternalAudioCursor(selection, argsArray), getInternalAudioCursor(selection, argsArray)});
        startManagingCursor(c);
        return c;
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    public void afterTextChanged(Editable s) {
        refreshListView();
    }

    private void refreshListView() {
        this.mAdapter.changeCursor(createCursor(this.mFilter.getText().toString()));
    }
}
