package me.ring.knou1.editor;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import java.util.ArrayList;
import me.ring.knou1.R;

public class FileSaveDialog extends Dialog {
    public static final int FILE_KIND_ALARM = 1;
    public static final int FILE_KIND_MUSIC = 0;
    public static final int FILE_KIND_NOTIFICATION = 2;
    public static final int FILE_KIND_RINGTONE = 3;
    private View.OnClickListener cancelListener = new View.OnClickListener() {
        public void onClick(View view) {
            FileSaveDialog.this.dismiss();
        }
    };
    /* access modifiers changed from: private */
    public EditText mFilename;
    private String mOriginalName;
    private int mPreviousSelection;
    /* access modifiers changed from: private */
    public Message mResponse;
    private ArrayList<String> mTypeArray;
    /* access modifiers changed from: private */
    public Spinner mTypeSpinner;
    private View.OnClickListener saveListener = new View.OnClickListener() {
        public void onClick(View view) {
            FileSaveDialog.this.mResponse.obj = FileSaveDialog.this.mFilename.getText();
            FileSaveDialog.this.mResponse.arg1 = FileSaveDialog.this.mTypeSpinner.getSelectedItemPosition();
            FileSaveDialog.this.mResponse.sendToTarget();
            FileSaveDialog.this.dismiss();
        }
    };

    public static String KindToName(int kind) {
        switch (kind) {
            case 0:
                return "Music";
            case 1:
                return "Alarm";
            case 2:
                return "Notification";
            case 3:
                return "Ringtone";
            default:
                return "Unknown";
        }
    }

    public FileSaveDialog(Context context, Resources resources, String originalName, Message response) {
        super(context);
        setContentView((int) R.layout.file_save);
        setTitle(resources.getString(R.string.file_save_title));
        this.mTypeArray = new ArrayList<>();
        this.mTypeArray.add(resources.getString(R.string.type_music));
        this.mTypeArray.add(resources.getString(R.string.type_alarm));
        this.mTypeArray.add(resources.getString(R.string.type_notification));
        this.mTypeArray.add(resources.getString(R.string.type_ringtone));
        this.mFilename = (EditText) findViewById(R.id.filename);
        this.mOriginalName = originalName;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, 17367048, this.mTypeArray);
        adapter.setDropDownViewResource(17367049);
        this.mTypeSpinner = (Spinner) findViewById(R.id.ringtone_type);
        this.mTypeSpinner.setAdapter((SpinnerAdapter) adapter);
        this.mTypeSpinner.setSelection(3);
        this.mPreviousSelection = 3;
        setFilenameEditBoxFromName(false);
        this.mTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView parent, View v, int position, long id) {
                FileSaveDialog.this.setFilenameEditBoxFromName(true);
            }

            public void onNothingSelected(AdapterView parent) {
            }
        });
        ((Button) findViewById(R.id.save)).setOnClickListener(this.saveListener);
        ((Button) findViewById(R.id.cancel)).setOnClickListener(this.cancelListener);
        this.mResponse = response;
    }

    /* access modifiers changed from: private */
    public void setFilenameEditBoxFromName(boolean onlyIfNotEdited) {
        if (onlyIfNotEdited) {
            if (!(String.valueOf(this.mOriginalName) + " " + this.mTypeArray.get(this.mPreviousSelection)).contentEquals(this.mFilename.getText())) {
                return;
            }
        }
        int newSelection = this.mTypeSpinner.getSelectedItemPosition();
        this.mFilename.setText(String.valueOf(this.mOriginalName) + " " + this.mTypeArray.get(newSelection));
        this.mPreviousSelection = this.mTypeSpinner.getSelectedItemPosition();
    }
}
