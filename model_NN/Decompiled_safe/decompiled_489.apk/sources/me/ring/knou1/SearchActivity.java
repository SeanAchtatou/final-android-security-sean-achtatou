package me.ring.knou1;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.TextView;
import me.ring.knou1.utils.RingbowHelper;

public class SearchActivity extends Activity {
    /* access modifiers changed from: private */
    public TextView mKeywordTV = null;
    private ImageButton mSearchBtn = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.search);
        AdsView.createAdWhirl(this);
        this.mKeywordTV = (TextView) findViewById(R.id.Keyword);
        this.mSearchBtn = (ImageButton) findViewById(R.id.SearchBtn);
        this.mSearchBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String kw = SearchActivity.this.mKeywordTV.getText().toString();
                if (kw.length() > 0) {
                    String kw2 = kw.trim();
                    ((InputMethodManager) SearchActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(SearchActivity.this.mKeywordTV.getWindowToken(), 2);
                    RingtoneListActivity.startActivity(SearchActivity.this, "Search Results", RingbowHelper.getSearchURL(kw2));
                }
            }
        });
    }
}
