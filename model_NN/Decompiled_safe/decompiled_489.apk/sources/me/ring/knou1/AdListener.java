package me.ring.knou1;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.ViewGroup;
import com.google.ads.AdView;
import com.qwapi.adclient.android.data.Ad;
import com.qwapi.adclient.android.data.Status;
import com.qwapi.adclient.android.requestparams.AdRequestParams;
import com.qwapi.adclient.android.view.AdEventsListener;

public class AdListener implements AdEventsListener {
    private AdView adMobView;
    protected Handler hideHandler = new Handler() {
        public void handleMessage(Message msg) {
            AdListener.this.qwAdView.setVisibility(8);
        }
    };
    /* access modifiers changed from: private */
    public ViewGroup qwAdView;
    protected Handler showHandler = new Handler() {
        public void handleMessage(Message msg) {
            AdListener.this.qwAdView.setVisibility(0);
        }
    };

    public AdListener(Activity context) {
    }

    public void onAdClick(Context arg0, Ad arg1) {
    }

    public void onAdRequest(Context arg0, AdRequestParams arg1) {
    }

    public void onAdRequestFailed(Context arg0, AdRequestParams arg1, Status arg2) {
        Message.obtain(this.hideHandler, -1).sendToTarget();
    }

    public void onAdRequestSuccessful(Context arg0, AdRequestParams arg1, Ad arg2) {
        Message.obtain(this.showHandler, -1).sendToTarget();
    }

    public void onDisplayAd(Context arg0, Ad arg1) {
    }
}
