package me.ring.knou1;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.qwapi.adclient.android.utils.Utils;
import me.ring.knou1.data.Constants;
import me.ring.knou1.data.Downloaded;
import me.ring.knou1.task.DownloadTask;
import me.ring.knou1.task.Ringtone;

public class MyDownloadsActivity extends ListActivity {
    private static final int POPUP_MENU = 1;
    /* access modifiers changed from: private */
    public MyDownloadAdapter mAdapter = null;
    private boolean mAdapterSent = false;
    /* access modifiers changed from: private */
    public Cursor mCursor = null;
    /* access modifiers changed from: private */
    public Handler mReScanHandler = new Handler() {
        public void handleMessage(Message msg) {
            Cursor unused = MyDownloadsActivity.this.getMyDownloadCursor(MyDownloadsActivity.this.mAdapter.getQueryHandler());
        }
    };
    private BroadcastReceiver mScanListener = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            MyDownloadsActivity.this.mReScanHandler.sendEmptyMessage(0);
        }
    };
    /* access modifiers changed from: private */
    public int mSelectedPos = -1;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mydownloads);
        AdsView.createAdWhirl(this);
        IntentFilter f = new IntentFilter();
        f.addAction("android.intent.action.MEDIA_UNMOUNTED");
        f.addDataScheme("file");
        registerReceiver(this.mScanListener, f);
        this.mAdapter = (MyDownloadAdapter) getLastNonConfigurationInstance();
        if (this.mAdapter == null) {
            this.mAdapter = new MyDownloadAdapter(this, getApplication(), R.layout.mydownloads_item, this.mCursor, new String[0], new int[0]);
            setListAdapter(this.mAdapter);
            getMyDownloadCursor(this.mAdapter.getQueryHandler());
        } else {
            this.mAdapter.setActivity(this);
            setListAdapter(this.mAdapter);
            this.mCursor = this.mAdapter.getCursor();
            if (this.mCursor != null) {
                init(this.mCursor);
            } else {
                getMyDownloadCursor(this.mAdapter.getQueryHandler());
            }
        }
        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                Cursor c = (Cursor) MyDownloadsActivity.this.mAdapter.getItem(position);
                if (c == null) {
                    return true;
                }
                String selectedItemKey = c.getString(c.getColumnIndex(Downloaded.COL_KEY));
                if (selectedItemKey == null || selectedItemKey.length() <= 0) {
                    return true;
                }
                MyDownloadsActivity.this.mSelectedPos = position;
                MyDownloadsActivity.this.showDialog(1);
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Cursor c = (Cursor) this.mAdapter.getItem(position);
        if (c != null) {
            RingtoneDetailActivity.startActivity(this, c.getString(this.mAdapter.mKeyIdx), c.getString(this.mAdapter.mTitleIdx), c.getString(this.mAdapter.mArtistIdx), c.getInt(this.mAdapter.mRatingIdx), null);
        }
    }

    public Object onRetainNonConfigurationInstance() {
        this.mAdapterSent = true;
        return this.mAdapter;
    }

    public void onDestroy() {
        Cursor c;
        unregisterReceiver(this.mScanListener);
        if (!this.mAdapterSent && (c = this.mAdapter.getCursor()) != null) {
            c.close();
        }
        setListAdapter(null);
        this.mAdapter = null;
        super.onDestroy();
    }

    public void onPause() {
        this.mReScanHandler.removeCallbacksAndMessages(null);
        super.onPause();
    }

    /* access modifiers changed from: private */
    public void init(Cursor c) {
        if (this.mAdapter != null) {
            this.mAdapter.changeCursor(c);
        }
        if (this.mCursor == null) {
            this.mReScanHandler.sendEmptyMessageDelayed(0, 1000);
        }
    }

    /* access modifiers changed from: private */
    public Cursor getMyDownloadCursor(AsyncQueryHandler async) {
        String[] cols = {"_id", Downloaded.COL_TITLE, Downloaded.COL_ARTIST, Downloaded.COL_KEY, Downloaded.COL_RATING};
        if (async != null) {
            async.startQuery(0, null, Downloaded.CONTENT_URI, cols, null, null, "cdate DESC");
            return null;
        }
        try {
            ContentResolver resolver = getContentResolver();
            if (resolver != null) {
                return resolver.query(Downloaded.CONTENT_URI, cols, null, null, "cdate DESC");
            }
            return null;
        } catch (UnsupportedOperationException e) {
            return null;
        }
    }

    private static class MyDownloadAdapter extends SimpleCursorAdapter {
        /* access modifiers changed from: private */
        public MyDownloadsActivity mActivity;
        /* access modifiers changed from: private */
        public int mArtistIdx;
        /* access modifiers changed from: private */
        public int mKeyIdx;
        private AsyncQueryHandler mQueryHandler;
        /* access modifiers changed from: private */
        public int mRatingIdx;
        /* access modifiers changed from: private */
        public int mTitleIdx;

        static class ViewHolder {
            TextView mArtist;
            ImageView mIVThumbnail;
            RatingBar mRating;
            TextView mTVTitle;

            ViewHolder() {
            }
        }

        class QueryHandler extends AsyncQueryHandler {
            public QueryHandler(ContentResolver cr) {
                super(cr);
            }

            /* access modifiers changed from: protected */
            public void onQueryComplete(int token, Object cookie, Cursor cursor) {
                if (MyDownloadAdapter.this.mActivity != null) {
                    MyDownloadAdapter.this.mActivity.init(cursor);
                }
            }
        }

        public MyDownloadAdapter(MyDownloadsActivity activity, Context context, int layout, Cursor c, String[] from, int[] to) {
            super(context, layout, c, from, to);
            this.mActivity = activity;
            this.mQueryHandler = new QueryHandler(context.getContentResolver());
            getColumnIndices(c);
        }

        private void getColumnIndices(Cursor c) {
            if (c != null) {
                this.mKeyIdx = c.getColumnIndex(Downloaded.COL_KEY);
                this.mTitleIdx = c.getColumnIndex(Downloaded.COL_TITLE);
                this.mArtistIdx = c.getColumnIndex(Downloaded.COL_ARTIST);
                this.mRatingIdx = c.getColumnIndex(Downloaded.COL_RATING);
            }
        }

        public void setActivity(MyDownloadsActivity newActivity) {
            this.mActivity = newActivity;
        }

        public AsyncQueryHandler getQueryHandler() {
            return this.mQueryHandler;
        }

        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View v = super.newView(context, cursor, parent);
            ViewHolder vh = new ViewHolder();
            vh.mTVTitle = (TextView) v.findViewById(R.id.Title);
            vh.mIVThumbnail = (ImageView) v.findViewById(R.id.Thumbnail);
            vh.mArtist = (TextView) v.findViewById(R.id.Artist);
            vh.mRating = (RatingBar) v.findViewById(R.id.Rating);
            v.setTag(vh);
            return v;
        }

        public void bindView(View view, Context context, Cursor c) {
            ViewHolder vh = (ViewHolder) view.getTag();
            vh.mTVTitle.setText(c.getString(this.mTitleIdx));
            vh.mArtist.setText(c.getString(this.mArtistIdx));
            vh.mRating.setRating((((float) c.getInt(this.mRatingIdx)) / 100.0f) * 5.0f);
            Bitmap bmp = BitmapFactory.decodeFile(Constants.get_ThumbnailPath(c.getString(this.mKeyIdx)));
            if (bmp != null) {
                vh.mIVThumbnail.setImageBitmap(bmp);
            } else {
                vh.mIVThumbnail.setImageResource(R.drawable.default_thumbnail);
            }
        }

        public void changeCursor(Cursor cursor) {
            if (cursor != this.mActivity.mCursor) {
                this.mActivity.mCursor = cursor;
                getColumnIndices(cursor);
                super.changeCursor(cursor);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new AlertDialog.Builder(this).setTitle(Utils.EMPTY_STRING).setItems((int) R.array.MYDOWNLOAD_POPMENU_LIST, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Cursor c = (Cursor) MyDownloadsActivity.this.mAdapter.getItem(MyDownloadsActivity.this.mSelectedPos);
                                if (c != null) {
                                    RingtoneDetailActivity.startActivity(MyDownloadsActivity.this, c.getString(c.getColumnIndex(Downloaded.COL_KEY)), c.getString(c.getColumnIndex(Downloaded.COL_TITLE)), c.getString(c.getColumnIndex(Downloaded.COL_ARTIST)), c.getInt(c.getColumnIndex(Downloaded.COL_RATING)), null);
                                    return;
                                }
                                return;
                            case 1:
                                Cursor c2 = (Cursor) MyDownloadsActivity.this.mAdapter.getItem(MyDownloadsActivity.this.mSelectedPos);
                                if (c2 != null) {
                                    String selectedItemKey = c2.getString(c2.getColumnIndex(Downloaded.COL_KEY));
                                    Ringtone.deleteRingtone(MyDownloadsActivity.this.getContentResolver(), selectedItemKey, DownloadTask.getDestFile(selectedItemKey).getAbsolutePath());
                                    return;
                                }
                                return;
                            default:
                                return;
                        }
                    }
                }).create();
            default:
                return null;
        }
    }
}
