package me.ring.knou1.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.qwapi.adclient.android.utils.Utils;

public class SettingsHelper {
    private static final String PREF_NAME = "st";
    private static final String P_LASTKEYWORD = "ltkw";
    private static final String P_TOTALRT = "totalSum";

    public static String getTotalRingtoneCount(Context ctx) {
        return ctx.getSharedPreferences(PREF_NAME, 0).getString(P_TOTALRT, null);
    }

    public static void putTotalRingtoneCount(Context ctx, String val) {
        SharedPreferences.Editor sp = ctx.getSharedPreferences(PREF_NAME, 0).edit();
        sp.putString(P_TOTALRT, val);
        sp.commit();
    }

    public static String getLastKeyword(Context ctx) {
        return ctx.getSharedPreferences(PREF_NAME, 0).getString(P_LASTKEYWORD, Utils.EMPTY_STRING);
    }

    public static void putLastKeyword(Context ctx, String val) {
        SharedPreferences.Editor sp = ctx.getSharedPreferences(PREF_NAME, 0).edit();
        sp.putString(P_LASTKEYWORD, val);
        sp.commit();
    }
}
