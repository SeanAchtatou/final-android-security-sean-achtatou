package me.ring.knou1.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.File;
import me.ring.knou1.data.Constants;

public class FileCacheMan {
    private static final String IMAGE_CACHE_EXT = ".ic";
    public static final long ONE_DAY = 86400000;
    public static final long ONE_MONTH = 2592000000L;
    public static final long ONE_SECOND = 1000;
    public static final long ONE_WEEK = 604800000;
    private static final String TEXT_CACHE_EXT = ".tc";

    public static String getStringCache(String url, long expire) {
        String fName = String.valueOf(Constants.get_CacheDir()) + Constants.calcHash(url) + TEXT_CACHE_EXT;
        if (isExpired(fName, expire)) {
            return null;
        }
        return TextFileUtils.readText(fName);
    }

    public static void putStringCache(String url, String content) {
        String fName = String.valueOf(Constants.get_CacheDir()) + Constants.calcHash(url) + TEXT_CACHE_EXT;
        if (TextFileUtils.createText(fName)) {
            TextFileUtils.writeText(content, fName);
        }
    }

    public static Bitmap getImageCache(String url) {
        String fName = String.valueOf(Constants.get_ImageCacheDir()) + Constants.calcHash(url) + IMAGE_CACHE_EXT;
        if (isExpired(fName, ONE_MONTH)) {
            return null;
        }
        return BitmapFactory.decodeFile(fName);
    }

    public static void putImageCache(String url, Bitmap bmp) {
        saveImage(String.valueOf(Constants.get_ImageCacheDir()) + Constants.calcHash(url) + IMAGE_CACHE_EXT, bmp);
    }

    private static boolean isExpired(String path, long expire) {
        File f = new File(path);
        if (!f.exists()) {
            return true;
        }
        if (System.currentTimeMillis() - f.lastModified() < expire) {
            return false;
        }
        f.delete();
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036 A[SYNTHETIC, Splitter:B:17:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003f A[SYNTHETIC, Splitter:B:22:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void saveImage(java.lang.String r6, android.graphics.Bitmap r7) {
        /*
            if (r7 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.PNG
            r5 = 100
            r7.compress(r4, r5, r1)
            r2 = 0
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0033, all -> 0x003c }
            r0.<init>(r6)     // Catch:{ Exception -> 0x0033, all -> 0x003c }
            boolean r4 = r0.exists()     // Catch:{ Exception -> 0x0033, all -> 0x003c }
            if (r4 == 0) goto L_0x0024
            boolean r4 = r0.isFile()     // Catch:{ Exception -> 0x0033, all -> 0x003c }
            if (r4 == 0) goto L_0x0024
            r0.delete()     // Catch:{ Exception -> 0x0033, all -> 0x003c }
        L_0x0024:
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0033, all -> 0x003c }
            r3.<init>(r6)     // Catch:{ Exception -> 0x0033, all -> 0x003c }
            r1.writeTo(r3)     // Catch:{ Exception -> 0x004b, all -> 0x0048 }
            if (r3 == 0) goto L_0x004e
            r3.close()     // Catch:{ Exception -> 0x0043 }
            r2 = r3
            goto L_0x0002
        L_0x0033:
            r4 = move-exception
        L_0x0034:
            if (r2 == 0) goto L_0x0002
            r2.close()     // Catch:{ Exception -> 0x003a }
            goto L_0x0002
        L_0x003a:
            r4 = move-exception
            goto L_0x0002
        L_0x003c:
            r4 = move-exception
        L_0x003d:
            if (r2 == 0) goto L_0x0042
            r2.close()     // Catch:{ Exception -> 0x0046 }
        L_0x0042:
            throw r4
        L_0x0043:
            r4 = move-exception
            r2 = r3
            goto L_0x0002
        L_0x0046:
            r5 = move-exception
            goto L_0x0042
        L_0x0048:
            r4 = move-exception
            r2 = r3
            goto L_0x003d
        L_0x004b:
            r4 = move-exception
            r2 = r3
            goto L_0x0034
        L_0x004e:
            r2 = r3
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: me.ring.knou1.utils.FileCacheMan.saveImage(java.lang.String, android.graphics.Bitmap):void");
    }
}
