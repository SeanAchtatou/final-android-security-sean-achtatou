package me.ring.knou1.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;

public class TextFileUtils {
    public static boolean createText(String path) {
        try {
            File filename = new File(path);
            if (!filename.exists()) {
                filename.createNewFile();
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void deleteText(String path) {
        try {
            new RandomAccessFile(path, "rw").setLength(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readText(String path) {
        try {
            try {
                return new BufferedReader(new FileReader(new File(path))).readLine();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } catch (FileNotFoundException e2) {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0024 A[SYNTHETIC, Splitter:B:16:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0031 A[SYNTHETIC, Splitter:B:23:0x0031] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean writeText(java.lang.String r6, java.lang.String r7) {
        /*
            r3 = 0
            java.io.File r2 = new java.io.File
            r2.<init>(r7)
            java.io.RandomAccessFile r4 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x001d }
            java.lang.String r5 = "rw"
            r4.<init>(r2, r5)     // Catch:{ IOException -> 0x001d }
            r4.writeBytes(r6)     // Catch:{ IOException -> 0x003d, all -> 0x003a }
            if (r4 == 0) goto L_0x0015
            r4.close()     // Catch:{ IOException -> 0x0018 }
        L_0x0015:
            r5 = 1
            r3 = r4
        L_0x0017:
            return r5
        L_0x0018:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0015
        L_0x001d:
            r5 = move-exception
            r0 = r5
        L_0x001f:
            r0.printStackTrace()     // Catch:{ all -> 0x002e }
            if (r3 == 0) goto L_0x0027
            r3.close()     // Catch:{ IOException -> 0x0029 }
        L_0x0027:
            r5 = 0
            goto L_0x0017
        L_0x0029:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0027
        L_0x002e:
            r5 = move-exception
        L_0x002f:
            if (r3 == 0) goto L_0x0034
            r3.close()     // Catch:{ IOException -> 0x0035 }
        L_0x0034:
            throw r5
        L_0x0035:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0034
        L_0x003a:
            r5 = move-exception
            r3 = r4
            goto L_0x002f
        L_0x003d:
            r5 = move-exception
            r0 = r5
            r3 = r4
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: me.ring.knou1.utils.TextFileUtils.writeText(java.lang.String, java.lang.String):boolean");
    }
}
