package me.ring.knou1.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import me.ring.knou1.R;

public class AnimViewHelper {
    private Animation mAnimAppear;
    private Animation mAnimDisappear;
    private View mView = null;

    public AnimViewHelper(View rootView, Context ctx) {
        this.mView = rootView;
        this.mAnimDisappear = AnimationUtils.loadAnimation(ctx, R.anim.footer_disappear);
        this.mAnimAppear = AnimationUtils.loadAnimation(ctx, R.anim.footer_appear);
    }

    public void showFooter() {
        if (this.mView.getVisibility() != 0) {
            this.mView.setVisibility(0);
            this.mView.startAnimation(this.mAnimAppear);
        }
    }

    public void hideFooter() {
        if (this.mView.getVisibility() != 8) {
            this.mView.setVisibility(8);
            this.mView.startAnimation(this.mAnimDisappear);
        }
    }

    public void hideFooterNoAnim() {
        this.mView.setVisibility(8);
    }
}
