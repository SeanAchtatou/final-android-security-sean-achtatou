package me.ring.knou1.utils;

import com.qwapi.adclient.android.requestparams.AdRequestParams;
import java.security.MessageDigest;

public class HmacMd5 {
    private static final int HMAC_BLKL = 64;
    private static final int HMAC_MD5L = 16;
    private static final byte IPAD = 54;
    private static final byte OPAD = 92;
    private static final String[] hexDigits = {AdRequestParams.ZERO, AdRequestParams.ONE, "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    private static String byteToHexString(byte b) {
        int n = b;
        if (n < 0) {
            n += 256;
        }
        return String.valueOf(hexDigits[n / 16]) + hexDigits[n % 16];
    }

    public static String byteArrayToHexString(byte[] b) {
        StringBuffer resultSb = new StringBuffer();
        for (byte byteToHexString : b) {
            resultSb.append(byteToHexString(byteToHexString));
        }
        return resultSb.toString();
    }

    /* JADX INFO: Multiple debug info for r9v1 int: [D('key' java.lang.String), D('conLength' int)] */
    /* JADX INFO: Multiple debug info for r8v3 byte[]: [D('orgLength' int), D('conBytes' byte[])] */
    /* JADX INFO: Multiple debug info for r9v3 java.security.MessageDigest: [D('md' java.security.MessageDigest), D('conLength' int)] */
    /* JADX INFO: Multiple debug info for r1v4 byte[]: [D('i' int), D('res1' byte[])] */
    /* JADX INFO: Multiple debug info for r8v6 byte[]: [D('res' byte[]), D('i' int)] */
    /* JADX INFO: Multiple debug info for r8v7 java.lang.String: [D('res' byte[]), D('result' java.lang.String)] */
    public static String HMAC(String origin, String key) throws Exception {
        byte[] keyBytes = key.getBytes();
        byte[] orgBytes = origin.getBytes();
        byte[] iBytes = new byte[HMAC_BLKL];
        byte[] oBytes = new byte[80];
        int keyLength = keyBytes.length;
        int conLength = orgBytes.length + HMAC_BLKL;
        byte[] conBytes = new byte[conLength];
        for (int i = 0; i < HMAC_BLKL; i++) {
            if (i < keyLength) {
                iBytes[i] = (byte) (keyBytes[i] ^ IPAD);
                oBytes[i] = (byte) (keyBytes[i] ^ OPAD);
            } else {
                iBytes[i] = IPAD;
                oBytes[i] = OPAD;
            }
            conBytes[i] = iBytes[i];
        }
        for (int i2 = HMAC_BLKL; i2 < conLength; i2++) {
            conBytes[i2] = orgBytes[i2 - HMAC_BLKL];
        }
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] res1 = md.digest(conBytes);
        for (int i3 = HMAC_BLKL; i3 < 80; i3++) {
            oBytes[i3] = res1[i3 - HMAC_BLKL];
        }
        return byteArrayToHexString(md.digest(oBytes));
    }
}
