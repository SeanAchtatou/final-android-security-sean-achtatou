package me.ring.knou1.utils;

import com.qwapi.adclient.android.utils.Utils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import me.ring.knou1.AdsView;

public class RingbowHelper {
    public static final String SKEY = "IKJYTBVC";
    public static final String SUID = "308";

    public static final String getThumbnailURL(String imgPath) {
        if (imgPath.startsWith("http://")) {
            return imgPath;
        }
        return "http://s.heiguge.com/" + imgPath;
    }

    public static final String getCategoryListURL() {
        return "http://ggapp.appspot.com/category/list/?json=1";
    }

    public static final String getCategoryURL(String cateName) {
        return encAuthURLString("http://ggapp.appspot.com/ringtone/searchau/?category=" + encURLString(cateName) + "&json=1&count=10");
    }

    public static final String getHotMusicURL() {
        return "http://ggapp.appspot.com/ringtone/hot/bb1002/?json=1";
    }

    public static final String getMonthTopDownload() {
        return "http://ggapp.appspot.com/ringtone/hot/topdl/?json=1";
    }

    public static final String getDayTopDownload() {
        return "http://ggapp.appspot.com/ringtone/hot/topdl/?json=1&delta=1";
    }

    public static final String getWeeklyTopDownload() {
        return "http://ggapp.appspot.com/ringtone/hot/topdl/?json=1&delta=7";
    }

    public static final String getLatestAdded() {
        return "http://ggapp.appspot.com/ringtone/hot/newest/?json=1";
    }

    public static final String getPage(String reqURL, int start) {
        int pos = reqURL.indexOf("&auth=");
        if (pos > 0) {
            reqURL = reqURL.substring(0, pos);
        }
        return encAuthURLString(String.valueOf(reqURL) + "&count=10&start=" + (start * 10));
    }

    public static final String getByArtistURL(String artist) {
        return encAuthURLString("http://ggapp.appspot.com/ringtone/searchau/?q=" + encURLString(artist) + "&json=1&count=10");
    }

    public static final String getByArtistAndTitleURL(String artist, String title) {
        return encAuthURLString("http://ggapp.appspot.com/ringtone/searchau/?artist=" + encURLString(artist) + "&q=" + encURLString(title) + "&count=10&json=1");
    }

    public static final String getByAuthor(String author) {
        return encAuthURLString("http://ggapp.appspot.com/ringtone/searchau/?author=" + encURLString(author) + "&json=1&count=10");
    }

    public static final String getSearchURL(String keyword) {
        return encAuthURLString("http://ggapp.appspot.com/ringtone/searchau/?q=" + encURLString(keyword) + "&json=1&count=10");
    }

    public static final String getRingtoneURL(String key) {
        return "http://ggapp.appspot.com/ringtone/show/" + key + "?json=1";
    }

    private static final String encURLString(String s) {
        String retval = s;
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return retval;
        }
    }

    private static final String encAuthURLString(String s) {
        try {
            return String.valueOf(s) + "&uid=" + SUID + "&auth=" + HmacMd5.HMAC(String.valueOf(s) + "&uid=" + SUID + AdsView.ADID, SKEY);
        } catch (Exception e) {
            return Utils.EMPTY_STRING;
        }
    }
}
