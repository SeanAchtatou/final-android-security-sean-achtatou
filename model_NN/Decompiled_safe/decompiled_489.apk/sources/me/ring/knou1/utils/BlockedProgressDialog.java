package me.ring.knou1.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.KeyEvent;

public class BlockedProgressDialog extends ProgressDialog {
    public BlockedProgressDialog(Context context) {
        super(context);
    }

    public BlockedProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
