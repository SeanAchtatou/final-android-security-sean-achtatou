package me.ring.knou1;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.adwhirl.AdWhirlLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSenseSpec;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.GoogleAdView;
import com.qwapi.adclient.android.data.Ad;
import com.qwapi.adclient.android.data.Status;
import com.qwapi.adclient.android.requestparams.AdRequestParams;
import com.qwapi.adclient.android.requestparams.AnimationType;
import com.qwapi.adclient.android.requestparams.DisplayMode;
import com.qwapi.adclient.android.requestparams.MediaType;
import com.qwapi.adclient.android.requestparams.Placement;
import com.qwapi.adclient.android.view.AdEventsListener;
import com.qwapi.adclient.android.view.QWAdView;

public class AdsView {
    public static final String ADID = "07c5f1a23a7742e3b96dfaaf14dea8fb";
    private static final String APP_NAME = "Worara Ringtones";
    public static final String CHANNEL_ID_01 = "7174434902";
    public static final String CHANNEL_ID_02 = "9273120356";
    public static final String CHANNEL_ID_03 = "5718134625";
    private static final String CLIENT_ID = "ca-mb-app-pub-3102565455540132";
    private static final String COMPANY_NAME = "Xu Ying";
    private static final String KEYWORDS = "ringtones,search+ringtone,free+downloads+ringtone,mobile+ringtone,phones+ringtone";
    public static final String KW_MOSTVALUE = "game, job, shop, computer, girl, music, car";
    public static boolean mShowQuattroAd = true;

    public static void createAdMobAd(Activity activity, String keywords) {
        AdView adView = new AdView(activity, AdSize.BANNER, "a14c3bb5245b8fe");
        adView.setBackgroundColor(0);
        ((LinearLayout) activity.findViewById(R.id.AdsView)).addView(adView);
        adView.loadAd(new AdRequest());
    }

    public static void createAdWhirl(Activity activity) {
        try {
            ((LinearLayout) activity.findViewById(R.id.AdsView)).addView(new AdWhirlLayout(activity, ADID), new RelativeLayout.LayoutParams(-1, -2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void createAdsenseAds(Activity activity, String chanID) {
        GoogleAdView adsense = new GoogleAdView(activity);
        adsense.showAds(new AdSenseSpec(CLIENT_ID).setCompanyName(COMPANY_NAME).setAppName(APP_NAME).setKeywords(KEYWORDS).setChannel(chanID).setAdType(AdSenseSpec.AdType.TEXT_IMAGE).setAdTestEnabled(false));
        ViewGroup parentView = (ViewGroup) activity.findViewById(R.id.AdsView);
        parentView.setFocusable(false);
        parentView.addView(adsense);
    }

    public static void ScreateQWAd(Activity activity) {
        Activity activity2 = activity;
        new QWAdView(activity2, "AruRingtone-gb68rdpn", "df023fe8eaec4bdabd6e92763defb756", MediaType.banner, Placement.bottom, DisplayMode.autoRotate, 30, AnimationType.slide, new AdListener(activity), true);
    }

    public static void createQWAd(Activity activity) {
        if (mShowQuattroAd) {
            ViewGroup parentView = (ViewGroup) activity.findViewById(R.id.AdsView);
            parentView.addView(new QWAdView(activity, "Ringtone-g5er0xt9", "32cd2cd54d5a49aeb5f9c6f5c9c9e9ee", MediaType.banner, Placement.bottom, DisplayMode.autoRotate, 30, AnimationType.slide, new QWAdEventsListener(parentView), true));
        }
    }

    private static class QWAdEventsListener implements AdEventsListener {
        private ViewGroup mAdsView = null;

        public QWAdEventsListener(ViewGroup view) {
            this.mAdsView = view;
        }

        public void onAdClick(Context ctx, Ad ad) {
            Log.i("Snake", "onAdClick for Ad: " + ad.getAdType() + " : " + ad.getId());
        }

        public void onAdRequest(Context ctx, AdRequestParams params) {
            Log.i("Snake", "onAdRequest for RequestParams: " + params.toString());
        }

        public void onAdRequestFailed(Context ctx, AdRequestParams params, Status status) {
            Log.i("Snake", "onAdRequestFailed for RequestParams: " + params.toString() + " : " + status);
        }

        public void onAdRequestSuccessful(Context ctx, AdRequestParams params, Ad ad) {
            Log.i("Snake", "onAdRequestSuccessful for RequestParams: " + params.toString() + " : Ad: " + ad.getAdType() + " : " + ad.getId());
        }

        public void onDisplayAd(Context ctx, Ad ad) {
            Log.i("Snake", "onDisplayAd for Ad: " + ad.getAdType() + " : " + ad.getId());
        }
    }
}
