package me.ring.knou1;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import me.ring.knou1.task.QueryTask;
import me.ring.knou1.task.Ringtone;
import me.ring.knou1.task.ThumbnailTask;
import me.ring.knou1.utils.AnimViewHelper;
import me.ring.knou1.utils.BlockedProgressDialog;
import me.ring.knou1.utils.RingbowHelper;

public class ArtistRingtoneActivity extends ListActivity implements QueryTask.Listener, ThumbnailTask.Listener {
    private static final int DLG_LOADING = 1;
    public static final String P_REQURL = "rurl";
    public static final String P_VIEWTITLE = "vtitle";
    /* access modifiers changed from: private */
    public MyAdapter mAdapter = null;
    private AnimViewHelper mFooter = null;
    /* access modifiers changed from: private */
    public QueryTask mTask = null;
    /* access modifiers changed from: private */
    public HashMap<Integer, ThumbnailTask> mTaskTBs = new HashMap<>(6);

    public static void startActivity(Context ctx, String viewTitle, String reqURL) {
        Intent intent = new Intent(ctx, ArtistRingtoneActivity.class);
        intent.putExtra("rurl", reqURL);
        intent.putExtra("vtitle", viewTitle);
        ctx.startActivity(intent);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.ringtone_list);
        AdsView.createAdWhirl(this);
        this.mFooter = new AnimViewHelper(findViewById(R.id.BtnPanel), this);
        this.mFooter.hideFooterNoAnim();
        Bundle b = getIntent().getExtras();
        this.mAdapter = (MyAdapter) getLastNonConfigurationInstance();
        if (this.mAdapter == null) {
            this.mAdapter = new MyAdapter(this);
            setListAdapter(this.mAdapter);
            if (b != null) {
                this.mAdapter.setReqURL(b.getString("rurl"));
                this.mTask = new QueryTask(this);
                this.mTask.execute(this.mAdapter.getReqUrl());
            } else {
                return;
            }
        } else {
            this.mAdapter.mActivity = this;
            setListAdapter(this.mAdapter);
            this.mFooter.showFooter();
        }
        setTitle(b.getString("vtitle"));
        ((Button) findViewById(R.id.PrePage)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ArtistRingtoneActivity.this.mTask == null) {
                    Log.e("Ringsome", "list 1 ");
                    if (ArtistRingtoneActivity.this.mAdapter.previousPage()) {
                        Log.e("Ringsome", "list 2 ");
                        Enumeration<Integer> enu = Collections.enumeration(ArtistRingtoneActivity.this.mTaskTBs.keySet());
                        while (enu.hasMoreElements()) {
                            ((ThumbnailTask) ArtistRingtoneActivity.this.mTaskTBs.get(enu.nextElement())).cancel(true);
                        }
                        ArtistRingtoneActivity.this.mAdapter.clear();
                        ArtistRingtoneActivity.this.mTask = new QueryTask(ArtistRingtoneActivity.this);
                        ArtistRingtoneActivity.this.mTask.execute(RingbowHelper.getPage(ArtistRingtoneActivity.this.mAdapter.getReqUrl(), ArtistRingtoneActivity.this.mAdapter.nPage));
                    }
                }
            }
        });
        ((Button) findViewById(R.id.NextPage)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (ArtistRingtoneActivity.this.mTask == null) {
                    Log.e("Ringsome", "list 3 ");
                    if (ArtistRingtoneActivity.this.mAdapter.nextPage()) {
                        Log.e("Ringsome", "list 4 ");
                        Enumeration<Integer> enu = Collections.enumeration(ArtistRingtoneActivity.this.mTaskTBs.keySet());
                        while (enu.hasMoreElements()) {
                            ((ThumbnailTask) ArtistRingtoneActivity.this.mTaskTBs.get(enu.nextElement())).cancel(true);
                        }
                        ArtistRingtoneActivity.this.mTaskTBs.clear();
                        ArtistRingtoneActivity.this.mAdapter.clear();
                        ArtistRingtoneActivity.this.mTask = new QueryTask(ArtistRingtoneActivity.this);
                        ArtistRingtoneActivity.this.mTask.execute(RingbowHelper.getPage(ArtistRingtoneActivity.this.mAdapter.getReqUrl(), ArtistRingtoneActivity.this.mAdapter.nPage));
                    }
                }
            }
        });
    }

    public Object onRetainNonConfigurationInstance() {
        if (this.mAdapter == null || !this.mAdapter.bFinished) {
            return null;
        }
        return this.mAdapter;
    }

    public void onDestroy() {
        if (this.mTask != null) {
            this.mTask.cancel(true);
            this.mTask = null;
        }
        Enumeration<Integer> enu = Collections.enumeration(this.mTaskTBs.keySet());
        while (enu.hasMoreElements()) {
            this.mTaskTBs.get(enu.nextElement()).cancel(true);
        }
        this.mTaskTBs.clear();
        getListView().setAdapter((ListAdapter) null);
        this.mAdapter = null;
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Ringtone rt = this.mAdapter.getItem(position);
        if (rt != null) {
            RingtoneDetailActivity.startActivity(this, rt.mKey, rt.mTitle, rt.mArtist, rt.mRating, rt.mThumbnail);
        }
    }

    private static class MyAdapter extends BaseAdapter {
        boolean bFinished = false;
        /* access modifiers changed from: private */
        public ArtistRingtoneActivity mActivity = null;
        private String mReqURL = null;
        List<Ringtone> mRingtones = new LinkedList();
        /* access modifiers changed from: private */
        public int nPage = 0;

        public void setReqURL(String url) {
            this.mReqURL = url;
        }

        public String getReqUrl() {
            return this.mReqURL;
        }

        public MyAdapter(ArtistRingtoneActivity activity) {
            this.mActivity = activity;
        }

        private class ViewHolder {
            TextView mArtist;
            ImageView mIVThumbnail;
            RatingBar mRating;
            TextView mTVTitle;

            private ViewHolder() {
            }

            /* synthetic */ ViewHolder(MyAdapter myAdapter, ViewHolder viewHolder) {
                this();
            }
        }

        public int getCount() {
            return this.mRingtones.size();
        }

        public Ringtone getItem(int position) {
            try {
                return this.mRingtones.get(position);
            } catch (Exception e) {
                return null;
            }
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = LayoutInflater.from(this.mActivity).inflate((int) R.layout.ringtone_list_item, parent, false);
                ViewHolder vh = new ViewHolder(this, null);
                vh.mTVTitle = (TextView) view.findViewById(R.id.Title);
                vh.mIVThumbnail = (ImageView) view.findViewById(R.id.Thumbnail);
                vh.mArtist = (TextView) view.findViewById(R.id.Artist);
                vh.mRating = (RatingBar) view.findViewById(R.id.Rating);
                view.setTag(vh);
            } else {
                view = convertView;
            }
            ViewHolder vh2 = (ViewHolder) view.getTag();
            Ringtone rt = getItem(position);
            if (rt != null) {
                vh2.mTVTitle.setText(rt.mTitle);
                if (rt.mBmp != null) {
                    vh2.mIVThumbnail.setImageBitmap(rt.mBmp);
                } else {
                    vh2.mIVThumbnail.setImageResource(R.drawable.default_thumbnail);
                    Integer pos = new Integer(position);
                    ThumbnailTask t = new ThumbnailTask(this.mActivity);
                    t.execute(rt, pos);
                    this.mActivity.mTaskTBs.put(pos, t);
                }
                vh2.mArtist.setText(rt.mArtist);
                vh2.mRating.setRating((((float) rt.mRating) / 100.0f) * 5.0f);
            }
            return view;
        }

        public void add(Ringtone rt) {
            this.mRingtones.add(rt);
        }

        public void clear() {
            this.mRingtones.clear();
            notifyDataSetChanged();
        }

        public boolean nextPage() {
            if (this.mRingtones.size() % 10 != 0) {
                return false;
            }
            if (this.nPage == 99) {
                return false;
            }
            this.nPage++;
            return true;
        }

        public boolean previousPage() {
            if (this.nPage == 0) {
                return false;
            }
            this.nPage--;
            return true;
        }
    }

    public void QT_OnBegin() {
        this.mFooter.hideFooter();
        if (this.mAdapter != null) {
            this.mAdapter.bFinished = false;
        }
        showDialog(1);
    }

    public void QT_OnFinished(boolean bError) {
        removeDialog(1);
        this.mTask = null;
        if (bError) {
            Toast.makeText(this, getResources().getString(R.string.ERR_NETWORK), 0).show();
        } else if (this.mAdapter != null) {
            this.mAdapter.notifyDataSetChanged();
            this.mAdapter.bFinished = true;
            this.mFooter.showFooter();
        }
    }

    public void QT_OnRingtoneReady(Ringtone rt) {
        if (this.mAdapter != null) {
            this.mAdapter.add(rt);
        }
    }

    public void TT_OnBegin() {
    }

    public void TT_OnFinished(boolean bError) {
    }

    public void TT_OnThumbnailReady(int pos, Ringtone rt) {
        ListView listView = getListView();
        int firstPos = listView.getFirstVisiblePosition();
        int count = listView.getChildCount();
        if (pos >= firstPos && pos < firstPos + count) {
            ((MyAdapter.ViewHolder) listView.getChildAt(pos - firstPos).getTag()).mIVThumbnail.setImageBitmap(rt.mBmp);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                BlockedProgressDialog dialog = new BlockedProgressDialog(this);
                dialog.setTitle("A moment please...");
                dialog.setMessage("Retrieving data...");
                dialog.setIndeterminate(true);
                dialog.setCancelable(true);
                return dialog;
            default:
                return null;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.artist, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home /*2131361853*/:
                try {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:\"Dragon Lib\"")));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            case R.id.menu_downloaded /*2131361854*/:
                startActivity(new Intent().setClass(this, MyDownloadsActivity.class));
                return true;
            case R.id.menu_sendto /*2131361855*/:
                try {
                    Browser.sendString(this, "The app of " + getString(R.string.app_name) + " " + "provides many ringtones for you to enjoy online listening and download for free." + "Therefore I highly recommend it to you, sincerely hope you will love it. " + "http://market.android.com/details?id=" + getApplication().getPackageName() + " ");
                } catch (Exception e2) {
                }
                return true;
            default:
                return false;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            ((RingbowApp) getApplication()).mPlayer.stop();
        }
        return super.onKeyDown(keyCode, event);
    }
}
