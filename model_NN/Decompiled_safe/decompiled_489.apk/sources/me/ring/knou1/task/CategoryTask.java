package me.ring.knou1.task;

import android.os.AsyncTask;
import me.ring.knou1.utils.FileCacheMan;
import me.ring.knou1.utils.NetUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class CategoryTask extends AsyncTask<String, String, Integer> {
    private Listener mListener = null;

    public interface Listener {
        void CT_OnBegin();

        void CT_OnFinished(boolean z);

        void CT_OnUpdate(String str);
    }

    public CategoryTask(Listener listener) {
        this.mListener = listener;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(String... urls) {
        String categoryName;
        try {
            String url = urls[0];
            String jsonResp = FileCacheMan.getStringCache(url, FileCacheMan.ONE_WEEK);
            JSONArray items = null;
            if (jsonResp != null) {
                items = new JSONArray(jsonResp);
            }
            if (items == null) {
                String jsonResp2 = NetUtils.loadString(url);
                items = new JSONArray(jsonResp2);
                FileCacheMan.putStringCache(url, jsonResp2);
            }
            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                if (!(item == null || (categoryName = item.getString("name")) == null)) {
                    publishProgress(categoryName);
                }
            }
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.mListener != null) {
            this.mListener.CT_OnBegin();
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(String... categoryNames) {
        if (this.mListener != null) {
            this.mListener.CT_OnUpdate(categoryNames[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.mListener != null) {
            this.mListener.CT_OnFinished(result.intValue() == 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.mListener = null;
    }
}
