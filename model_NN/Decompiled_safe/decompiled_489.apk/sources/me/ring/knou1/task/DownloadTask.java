package me.ring.knou1.task;

import android.os.AsyncTask;
import android.os.Environment;
import java.io.File;
import me.ring.knou1.data.Constants;

public class DownloadTask extends AsyncTask<String, Integer, Integer> {
    private static final int IO_BUFFER_SIZE = 4096;
    private Listener mListener = null;

    public interface Listener {
        void DT_OnBegin();

        void DT_OnFinished(boolean z);

        void DT_OnProgressUpdated(Integer num);
    }

    public DownloadTask(Listener l) {
        this.mListener = l;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:110:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0152 A[SYNTHETIC, Splitter:B:70:0x0152] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0157 A[SYNTHETIC, Splitter:B:73:0x0157] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x016a A[SYNTHETIC, Splitter:B:81:0x016a] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x016f A[SYNTHETIC, Splitter:B:84:0x016f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Integer doInBackground(java.lang.String... r30) {
        /*
            r29 = this;
            r22 = 0
            r15 = r30[r22]
            r22 = 1
            r16 = r30[r22]
            java.io.File r9 = getDestFile(r15)
            boolean r22 = r9.exists()
            if (r22 == 0) goto L_0x001b
            boolean r22 = r9.isFile()
            if (r22 == 0) goto L_0x001b
            r9.delete()
        L_0x001b:
            boolean r22 = r9.exists()
            if (r22 == 0) goto L_0x0028
            r22 = 0
            java.lang.Integer r22 = java.lang.Integer.valueOf(r22)
        L_0x0027:
            return r22
        L_0x0028:
            r5 = 0
            r7 = 0
            r22 = 0
            java.lang.Long r11 = java.lang.Long.valueOf(r22)
            r9.createNewFile()     // Catch:{ Exception -> 0x0190 }
            java.net.URL r20 = new java.net.URL     // Catch:{ Exception -> 0x0190 }
            r0 = r20
            r1 = r16
            r0.<init>(r1)     // Catch:{ Exception -> 0x0190 }
            java.net.URLConnection r21 = r20.openConnection()     // Catch:{ Exception -> 0x0190 }
            java.util.Map r22 = r21.getHeaderFields()     // Catch:{ Exception -> 0x0190 }
            java.lang.String r23 = "content-length"
            java.lang.Object r13 = r22.get(r23)     // Catch:{ Exception -> 0x0190 }
            java.util.List r13 = (java.util.List) r13     // Catch:{ Exception -> 0x0190 }
            if (r13 == 0) goto L_0x0054
            boolean r22 = r13.isEmpty()     // Catch:{ Exception -> 0x0190 }
            if (r22 == 0) goto L_0x006f
        L_0x0054:
            r22 = 0
            java.lang.Integer r22 = java.lang.Integer.valueOf(r22)     // Catch:{ Exception -> 0x0190 }
            if (r5 == 0) goto L_0x005f
            r5.close()     // Catch:{ IOException -> 0x006a }
        L_0x005f:
            if (r7 == 0) goto L_0x0027
            r7.close()     // Catch:{ IOException -> 0x0065 }
            goto L_0x0027
        L_0x0065:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x0027
        L_0x006a:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x005f
        L_0x006f:
            r22 = 0
            r0 = r13
            r1 = r22
            java.lang.Object r18 = r0.get(r1)     // Catch:{ Exception -> 0x0190 }
            java.lang.String r18 = (java.lang.String) r18     // Catch:{ Exception -> 0x0190 }
            if (r18 != 0) goto L_0x0097
            r22 = 0
            java.lang.Integer r22 = java.lang.Integer.valueOf(r22)     // Catch:{ Exception -> 0x0190 }
            if (r5 == 0) goto L_0x0087
            r5.close()     // Catch:{ IOException -> 0x0092 }
        L_0x0087:
            if (r7 == 0) goto L_0x0027
            r7.close()     // Catch:{ IOException -> 0x008d }
            goto L_0x0027
        L_0x008d:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x0027
        L_0x0092:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x0087
        L_0x0097:
            long r22 = java.lang.Long.parseLong(r18)     // Catch:{ Exception -> 0x0190 }
            java.lang.Long r19 = java.lang.Long.valueOf(r22)     // Catch:{ Exception -> 0x0190 }
            java.io.BufferedInputStream r6 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0190 }
            java.io.InputStream r22 = r21.getInputStream()     // Catch:{ Exception -> 0x0190 }
            r23 = 4096(0x1000, float:5.74E-42)
            r0 = r6
            r1 = r22
            r2 = r23
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0190 }
            java.io.BufferedOutputStream r8 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x0194, all -> 0x0189 }
            java.io.FileOutputStream r22 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0194, all -> 0x0189 }
            r0 = r22
            r1 = r9
            r0.<init>(r1)     // Catch:{ Exception -> 0x0194, all -> 0x0189 }
            r0 = r8
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x0194, all -> 0x0189 }
            r17 = 0
            r22 = 4096(0x1000, float:5.74E-42)
            r0 = r22
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x012f, all -> 0x018c }
            r4 = r0
        L_0x00c8:
            int r17 = r6.read(r4)     // Catch:{ Exception -> 0x012f, all -> 0x018c }
            r22 = -1
            r0 = r17
            r1 = r22
            if (r0 != r1) goto L_0x00e9
            r8.flush()     // Catch:{ Exception -> 0x012f, all -> 0x018c }
            if (r6 == 0) goto L_0x00dc
            r6.close()     // Catch:{ IOException -> 0x017d }
        L_0x00dc:
            if (r8 == 0) goto L_0x00e1
            r8.close()     // Catch:{ IOException -> 0x0183 }
        L_0x00e1:
            r22 = 1
            java.lang.Integer r22 = java.lang.Integer.valueOf(r22)
            goto L_0x0027
        L_0x00e9:
            r22 = 0
            r0 = r8
            r1 = r4
            r2 = r22
            r3 = r17
            r0.write(r1, r2, r3)     // Catch:{ Exception -> 0x012f, all -> 0x018c }
            long r22 = r11.longValue()     // Catch:{ Exception -> 0x012f, all -> 0x018c }
            r0 = r17
            long r0 = (long) r0     // Catch:{ Exception -> 0x012f, all -> 0x018c }
            r24 = r0
            long r22 = r22 + r24
            java.lang.Long r11 = java.lang.Long.valueOf(r22)     // Catch:{ Exception -> 0x012f, all -> 0x018c }
            r22 = 1
            r0 = r22
            java.lang.Integer[] r0 = new java.lang.Integer[r0]     // Catch:{ Exception -> 0x012f, all -> 0x018c }
            r22 = r0
            r23 = 0
            java.lang.Integer r24 = new java.lang.Integer     // Catch:{ Exception -> 0x012f, all -> 0x018c }
            r25 = 100
            long r27 = r11.longValue()     // Catch:{ Exception -> 0x012f, all -> 0x018c }
            long r25 = r25 * r27
            long r27 = r19.longValue()     // Catch:{ Exception -> 0x012f, all -> 0x018c }
            long r25 = r25 / r27
            r0 = r25
            int r0 = (int) r0     // Catch:{ Exception -> 0x012f, all -> 0x018c }
            r25 = r0
            r24.<init>(r25)     // Catch:{ Exception -> 0x012f, all -> 0x018c }
            r22[r23] = r24     // Catch:{ Exception -> 0x012f, all -> 0x018c }
            r0 = r29
            r1 = r22
            r0.publishProgress(r1)     // Catch:{ Exception -> 0x012f, all -> 0x018c }
            goto L_0x00c8
        L_0x012f:
            r22 = move-exception
            r12 = r22
            r7 = r8
            r5 = r6
        L_0x0134:
            java.io.File r10 = getDestFile(r15)     // Catch:{ all -> 0x0167 }
            boolean r22 = r10.exists()     // Catch:{ all -> 0x0167 }
            if (r22 == 0) goto L_0x014a
            boolean r22 = r10.isFile()     // Catch:{ all -> 0x0167 }
            if (r22 == 0) goto L_0x014a
            r10.delete()     // Catch:{ all -> 0x0167 }
            r10.deleteOnExit()     // Catch:{ all -> 0x0167 }
        L_0x014a:
            r22 = 0
            java.lang.Integer r22 = java.lang.Integer.valueOf(r22)     // Catch:{ all -> 0x0167 }
            if (r5 == 0) goto L_0x0155
            r5.close()     // Catch:{ IOException -> 0x0162 }
        L_0x0155:
            if (r7 == 0) goto L_0x0027
            r7.close()     // Catch:{ IOException -> 0x015c }
            goto L_0x0027
        L_0x015c:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x0027
        L_0x0162:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x0155
        L_0x0167:
            r22 = move-exception
        L_0x0168:
            if (r5 == 0) goto L_0x016d
            r5.close()     // Catch:{ IOException -> 0x0173 }
        L_0x016d:
            if (r7 == 0) goto L_0x0172
            r7.close()     // Catch:{ IOException -> 0x0178 }
        L_0x0172:
            throw r22
        L_0x0173:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x016d
        L_0x0178:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x0172
        L_0x017d:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x00dc
        L_0x0183:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x00e1
        L_0x0189:
            r22 = move-exception
            r5 = r6
            goto L_0x0168
        L_0x018c:
            r22 = move-exception
            r7 = r8
            r5 = r6
            goto L_0x0168
        L_0x0190:
            r22 = move-exception
            r12 = r22
            goto L_0x0134
        L_0x0194:
            r22 = move-exception
            r12 = r22
            r5 = r6
            goto L_0x0134
        */
        throw new UnsupportedOperationException("Method not decompiled: me.ring.knou1.task.DownloadTask.doInBackground(java.lang.String[]):java.lang.Integer");
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.mListener != null) {
            this.mListener.DT_OnBegin();
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Integer... progres) {
        if (this.mListener != null) {
            this.mListener.DT_OnProgressUpdated(progres[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.mListener != null) {
            this.mListener.DT_OnFinished(result.intValue() == 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.mListener = null;
    }

    public static File getDestFile(String key) {
        File sdCard = Environment.getExternalStorageDirectory();
        File dataDir = new File(String.valueOf(sdCard.getPath()) + Constants.APP_DATA_DIR);
        if (!dataDir.exists()) {
            dataDir.mkdirs();
        }
        return new File(String.valueOf(sdCard.getPath()) + Constants.APP_DATA_DIR + key + ".mp3");
    }

    public static boolean isRingtoneExists(String key) {
        return new File(String.valueOf(Environment.getExternalStorageDirectory().getPath()) + Constants.APP_DATA_DIR + key + ".mp3").exists();
    }
}
