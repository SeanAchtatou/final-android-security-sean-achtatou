package me.ring.knou1.task;

import android.os.AsyncTask;
import me.ring.knou1.data.Downloaded;
import me.ring.knou1.utils.FileCacheMan;
import me.ring.knou1.utils.NetUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class HotMusicTask extends AsyncTask<String, Ringtone, Integer> {
    private Listener mListener = null;

    public interface Listener {
        void HMT_OnBegin();

        void HMT_OnFinished(boolean z);

        void HMT_OnReady(Ringtone ringtone);
    }

    public HotMusicTask(Listener l) {
        this.mListener = l;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(String... params) {
        String url = params[0];
        try {
            String jsonResp = FileCacheMan.getStringCache(url, FileCacheMan.ONE_WEEK);
            JSONArray items = null;
            if (jsonResp != null) {
                items = new JSONArray(jsonResp);
            }
            if (items == null) {
                String jsonResp2 = NetUtils.loadString(url);
                items = new JSONArray(jsonResp2);
                FileCacheMan.putStringCache(url, jsonResp2);
            }
            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                Ringtone rt = new Ringtone();
                rt.mTitle = item.getString("song");
                rt.mArtist = item.getString(Downloaded.COL_ARTIST);
                rt.mThumbnail = item.getString("image");
                publishProgress(rt);
            }
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.mListener != null) {
            this.mListener.HMT_OnBegin();
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Ringtone... hotmusics) {
        if (this.mListener != null) {
            this.mListener.HMT_OnReady(hotmusics[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.mListener != null) {
            this.mListener.HMT_OnFinished(result.intValue() == 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.mListener = null;
    }
}
