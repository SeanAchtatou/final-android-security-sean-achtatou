package me.ring.knou1.task;

import android.os.AsyncTask;
import java.text.SimpleDateFormat;
import me.ring.knou1.data.Downloaded;
import me.ring.knou1.utils.FileCacheMan;
import me.ring.knou1.utils.NetUtils;
import me.ring.knou1.utils.RingbowHelper;
import org.json.JSONObject;

public class RingtoneDetailTask extends AsyncTask<String, Ringtone, Integer> {
    private String mJSonResp = null;
    private Listener mListener = null;

    public interface Listener {
        void RDT_OnBegin();

        void RDT_OnFinished(boolean z);

        void RDT_OnRingtoneReady(Ringtone ringtone);
    }

    public RingtoneDetailTask(Listener listener) {
        this.mListener = listener;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(String... keys) {
        try {
            new SimpleDateFormat("yyyy/MM/dd hh:mm:ss Z");
            String jsonResp = FileCacheMan.getStringCache(keys[0], FileCacheMan.ONE_MONTH);
            JSONObject item = null;
            if (jsonResp != null) {
                item = new JSONObject(jsonResp);
            }
            if (item == null) {
                jsonResp = NetUtils.loadString(RingbowHelper.getRingtoneURL(keys[0]));
                item = new JSONObject(jsonResp);
                FileCacheMan.putStringCache(keys[0], jsonResp);
            }
            if (jsonResp != null) {
                FileCacheMan.putStringCache(keys[0], jsonResp);
            }
            Ringtone rt = new Ringtone();
            rt.mKey = keys[0];
            rt.mCategory = item.getString("category");
            rt.mRating = item.getInt(Downloaded.COL_RATING);
            rt.mAuthor = item.getString("author");
            rt.mArtist = item.getString(Downloaded.COL_ARTIST);
            rt.mThumbnail = item.getString("image");
            rt.mTitle = item.getString(Downloaded.COL_TITLE);
            rt.mDownloads = item.getInt("download");
            rt.mSize = item.getInt("size");
            rt.mLink = item.getString("mp3");
            rt.mDate = item.getString("date");
            this.mJSonResp = jsonResp;
            publishProgress(rt);
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.mListener != null) {
            this.mListener.RDT_OnBegin();
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Ringtone... ringtones) {
        if (this.mListener != null) {
            this.mListener.RDT_OnRingtoneReady(ringtones[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.mListener != null) {
            this.mListener.RDT_OnFinished(result.intValue() == 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.mListener = null;
    }

    public String getJSonResp() {
        return this.mJSonResp;
    }
}
