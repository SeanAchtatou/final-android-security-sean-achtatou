package me.ring.knou1.task;

import android.os.AsyncTask;
import com.qwapi.adclient.android.utils.Utils;
import java.text.SimpleDateFormat;
import me.ring.knou1.data.Downloaded;
import me.ring.knou1.utils.FileCacheMan;
import me.ring.knou1.utils.NetUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class QueryTask extends AsyncTask<String, Ringtone, Integer> {
    private Listener mListener = null;

    public interface Listener {
        void QT_OnBegin();

        void QT_OnFinished(boolean z);

        void QT_OnRingtoneReady(Ringtone ringtone);
    }

    public QueryTask(Listener listener) {
        this.mListener = listener;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(String... urls) {
        new SimpleDateFormat("yyyy/MM/dd hh:mm:ss Z");
        try {
            String jsonResp = FileCacheMan.getStringCache(urls[0], FileCacheMan.ONE_DAY);
            JSONArray items = null;
            if (jsonResp != null) {
                items = new JSONArray(jsonResp);
            }
            if (items == null) {
                String jsonResp2 = NetUtils.loadString(urls[0]);
                items = new JSONArray(jsonResp2);
                FileCacheMan.putStringCache(urls[0], jsonResp2);
            }
            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                Ringtone rt = new Ringtone();
                if (item.has(Downloaded.COL_RATING)) {
                    rt.mRating = item.getInt(Downloaded.COL_RATING);
                }
                if (item.has("author")) {
                    rt.mAuthor = item.getString("author");
                } else {
                    rt.mAuthor = Utils.EMPTY_STRING;
                }
                rt.mArtist = item.getString(Downloaded.COL_ARTIST);
                rt.mThumbnail = item.getString("image");
                if (item.has(Downloaded.COL_TITLE)) {
                    rt.mTitle = item.getString(Downloaded.COL_TITLE);
                } else if (item.has("song")) {
                    rt.mTitle = item.getString("song");
                }
                if (item.has("download")) {
                    rt.mDownloads = item.getInt("download");
                } else {
                    rt.mDownloads = 0;
                }
                if (item.has("size")) {
                    rt.mSize = item.getInt("size");
                } else {
                    rt.mSize = 0;
                }
                rt.mKey = item.getString(Downloaded.COL_KEY);
                if (item.has("date")) {
                    rt.mDate = item.getString("date");
                } else {
                    rt.mDate = Utils.EMPTY_STRING;
                }
                publishProgress(rt);
            }
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.mListener != null) {
            this.mListener.QT_OnBegin();
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Ringtone... ringtones) {
        if (this.mListener != null) {
            this.mListener.QT_OnRingtoneReady(ringtones[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.mListener != null) {
            this.mListener.QT_OnFinished(result.intValue() == 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.mListener = null;
    }
}
