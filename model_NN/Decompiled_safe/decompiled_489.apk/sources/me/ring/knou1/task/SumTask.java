package me.ring.knou1.task;

import android.content.Context;
import android.os.AsyncTask;
import me.ring.knou1.data.Constants;
import me.ring.knou1.utils.NetUtils;
import me.ring.knou1.utils.SettingsHelper;

public class SumTask extends AsyncTask<Object, String, Integer> {
    private Context mCtx = null;
    private String mLastTotal = null;
    private Listener mListener = null;

    public interface Listener {
        void ST_OnShowSum(String str);
    }

    public SumTask(Context ctx, Listener l) {
        this.mListener = l;
        this.mCtx = ctx;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(Object... arg0) {
        try {
            if (this.mLastTotal != null) {
                publishProgress(this.mLastTotal);
            }
            String[] tokens = NetUtils.loadString(Constants.TOTAL_RINGTONE_COUNT).split(" ");
            if (tokens[0].length() > 0) {
                publishProgress(tokens[0]);
                this.mLastTotal = tokens[0];
            }
        } catch (Exception e) {
            this.mLastTotal = null;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(String... sums) {
        if (this.mListener != null) {
            this.mListener.ST_OnShowSum(sums[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.mListener != null && this.mLastTotal != null) {
            SettingsHelper.putTotalRingtoneCount(this.mCtx, this.mLastTotal);
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.mLastTotal = SettingsHelper.getTotalRingtoneCount(this.mCtx);
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.mListener = null;
    }
}
