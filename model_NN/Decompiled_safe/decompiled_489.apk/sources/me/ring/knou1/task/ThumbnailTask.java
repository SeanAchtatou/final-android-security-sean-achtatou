package me.ring.knou1.task;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import me.ring.knou1.utils.FileCacheMan;
import me.ring.knou1.utils.NetUtils;
import me.ring.knou1.utils.RingbowHelper;

public class ThumbnailTask extends AsyncTask<Object, TaskParam, Integer> {
    private Listener mListener = null;

    public interface Listener {
        void TT_OnBegin();

        void TT_OnFinished(boolean z);

        void TT_OnThumbnailReady(int i, Ringtone ringtone);
    }

    protected static class TaskParam {
        int mPos;
        Ringtone mRingtone;

        public TaskParam(int pos, Ringtone rt) {
            this.mPos = pos;
            this.mRingtone = rt;
        }
    }

    public ThumbnailTask(Listener listener) {
        this.mListener = listener;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(Object... ringtones) {
        try {
            Integer pos = (Integer) ringtones[1];
            if (isCancelled()) {
                return 1;
            }
            Ringtone rt = (Ringtone) ringtones[0];
            if (rt == null) {
                return 0;
            }
            if (rt.mThumbnail == null || rt.mThumbnail.length() <= 0) {
                return 0;
            }
            if (rt.mThumbnail.equals("image/80fab5a5e9e25f5d9091975f8091cb1e.gif")) {
                return 0;
            }
            String url = RingbowHelper.getThumbnailURL(rt.mThumbnail);
            Bitmap bmp = FileCacheMan.getImageCache(url);
            if (bmp == null && (bmp = NetUtils.loadBitmap(url)) != null) {
                FileCacheMan.putImageCache(url, bmp);
            }
            if (bmp == null) {
                return 0;
            }
            rt.mBmp = bmp;
            publishProgress(new TaskParam(pos.intValue(), rt));
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.mListener != null) {
            this.mListener.TT_OnBegin();
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(TaskParam... params) {
        if (this.mListener != null) {
            this.mListener.TT_OnThumbnailReady(params[0].mPos, params[0].mRingtone);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.mListener != null) {
            this.mListener.TT_OnFinished(result.intValue() == 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.mListener = null;
    }
}
