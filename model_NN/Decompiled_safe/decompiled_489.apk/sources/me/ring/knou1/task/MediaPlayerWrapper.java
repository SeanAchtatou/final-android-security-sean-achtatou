package me.ring.knou1.task;

import android.media.MediaPlayer;
import java.io.IOException;

public class MediaPlayerWrapper {
    /* access modifiers changed from: private */
    public Listener mListener = null;
    private MediaPlayer mPlayer = null;

    public interface Listener {
        void MPW_OnError();

        void MPW_OnPlayOver();

        void MPW_OnPlaystart();
    }

    public boolean play(Listener l, String url) {
        stop();
        this.mListener = l;
        this.mPlayer = new MediaPlayer();
        this.mPlayer.setOnBufferingUpdateListener(new MyBufferingUpdateListener(this, null));
        this.mPlayer.setOnCompletionListener(new MyOnCompletionListener(this, null));
        this.mPlayer.setOnErrorListener(new MyOnErrorListener(this, null));
        this.mPlayer.setOnPreparedListener(new MyOnPreparedListener(this, null));
        try {
            this.mPlayer.setDataSource(url);
            this.mPlayer.prepare();
            this.mPlayer.start();
            return true;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return false;
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
            return false;
        } catch (IOException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public void stop() {
        if (this.mPlayer != null) {
            this.mPlayer.stop();
            this.mPlayer = null;
            this.mListener = null;
        }
    }

    public void unbind() {
        this.mListener = null;
    }

    private class MyBufferingUpdateListener implements MediaPlayer.OnBufferingUpdateListener {
        private MyBufferingUpdateListener() {
        }

        /* synthetic */ MyBufferingUpdateListener(MediaPlayerWrapper mediaPlayerWrapper, MyBufferingUpdateListener myBufferingUpdateListener) {
            this();
        }

        public void onBufferingUpdate(MediaPlayer arg0, int arg1) {
        }
    }

    private class MyOnCompletionListener implements MediaPlayer.OnCompletionListener {
        private MyOnCompletionListener() {
        }

        /* synthetic */ MyOnCompletionListener(MediaPlayerWrapper mediaPlayerWrapper, MyOnCompletionListener myOnCompletionListener) {
            this();
        }

        public void onCompletion(MediaPlayer arg0) {
            if (MediaPlayerWrapper.this.mListener != null) {
                MediaPlayerWrapper.this.mListener.MPW_OnPlayOver();
            }
        }
    }

    private class MyOnErrorListener implements MediaPlayer.OnErrorListener {
        private MyOnErrorListener() {
        }

        /* synthetic */ MyOnErrorListener(MediaPlayerWrapper mediaPlayerWrapper, MyOnErrorListener myOnErrorListener) {
            this();
        }

        public boolean onError(MediaPlayer mp, int what, int extra) {
            if (MediaPlayerWrapper.this.mListener == null) {
                return false;
            }
            MediaPlayerWrapper.this.mListener.MPW_OnError();
            return false;
        }
    }

    private class MyOnPreparedListener implements MediaPlayer.OnPreparedListener {
        private MyOnPreparedListener() {
        }

        /* synthetic */ MyOnPreparedListener(MediaPlayerWrapper mediaPlayerWrapper, MyOnPreparedListener myOnPreparedListener) {
            this();
        }

        public void onPrepared(MediaPlayer mp) {
            if (MediaPlayerWrapper.this.mListener != null) {
                MediaPlayerWrapper.this.mListener.MPW_OnPlaystart();
            }
        }
    }
}
