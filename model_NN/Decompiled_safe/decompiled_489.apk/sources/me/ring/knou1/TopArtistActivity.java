package me.ring.knou1;

import android.app.ListActivity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.Toast;
import java.util.ArrayList;
import me.ring.knou1.data.Downloaded;
import me.ring.knou1.utils.NetUtils;
import me.ring.knou1.utils.RingbowHelper;
import org.json.JSONArray;

public class TopArtistActivity extends ListActivity {
    /* access modifiers changed from: private */
    public MyData mData = null;
    private FetchLastFMListTask mTask = null;

    private static class MyData {
        ArrayAdapter<String> mAdapter;
        ArrayList<String> mNames = new ArrayList<>();
        ArrayList<String> mStrings = new ArrayList<>();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.topartist);
        AdsView.createAdWhirl(this);
        setTitle("Top Artists");
        this.mData = (MyData) getLastNonConfigurationInstance();
        if (this.mData == null) {
            this.mData = new MyData();
            this.mData.mAdapter = new ArrayAdapter<>(this, 17367043, this.mData.mStrings);
            getListView().setAdapter((ListAdapter) this.mData.mAdapter);
            this.mTask = new FetchLastFMListTask();
            this.mTask.execute("http://ggapp.appspot.com/ringtone/topartist/");
        } else {
            this.mData.mAdapter = new ArrayAdapter<>(this, 17367043, this.mData.mStrings);
            getListView().setAdapter((ListAdapter) this.mData.mAdapter);
        }
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                try {
                    RingtoneListActivity.startActivity(TopArtistActivity.this, "Search Results", RingbowHelper.getByArtistURL(TopArtistActivity.this.mData.mNames.get(position)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Object onRetainNonConfigurationInstance() {
        return this.mData;
    }

    public void onDestroy() {
        if (!(this.mTask == null || this.mTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.mTask.cancel(true);
        }
        super.onDestroy();
    }

    public class FetchLastFMListTask extends AsyncTask<String, Integer, Integer> {
        public FetchLastFMListTask() {
        }

        public Integer doInBackground(String... urls) {
            try {
                if (urls.length <= 0) {
                    return null;
                }
                JSONArray items = new JSONArray(NetUtils.loadString(urls[0]));
                for (int i = 0; i < items.length(); i++) {
                    TopArtistActivity.this.mData.mNames.add(items.getJSONObject(i).getString(Downloaded.COL_ARTIST));
                }
                return 1;
            } catch (Exception e) {
                return null;
            }
        }

        public void onPostExecute(Integer result) {
            if (result == null) {
                Toast.makeText(TopArtistActivity.this, "Network service is not available, please try again.", 0).show();
            } else if (result.intValue() == 1) {
                int i = 0;
                while (i < TopArtistActivity.this.mData.mNames.size()) {
                    try {
                        TopArtistActivity.this.mData.mAdapter.add(String.valueOf(i + 1) + ". " + TopArtistActivity.this.mData.mNames.get(i));
                        i++;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                }
            } else {
                Toast.makeText(TopArtistActivity.this, "Network service is not available, please try again.", 0).show();
            }
        }
    }
}
