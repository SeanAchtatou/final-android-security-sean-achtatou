package me.ring.knou1;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.LinkedList;
import java.util.List;
import me.ring.knou1.task.CategoryTask;
import me.ring.knou1.utils.RingbowHelper;

public class CategoryActivity extends ListActivity implements CategoryTask.Listener {
    private MyAdapter mAdapter = null;
    private CategoryTask mTask = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.category);
        AdsView.createAdWhirl(this);
        this.mAdapter = (MyAdapter) getLastNonConfigurationInstance();
        if (this.mAdapter == null) {
            this.mAdapter = new MyAdapter(this);
            getListView().setAdapter((ListAdapter) this.mAdapter);
            this.mTask = new CategoryTask(this);
            this.mTask.execute(RingbowHelper.getCategoryListURL());
            return;
        }
        this.mAdapter.mActivity = this;
        getListView().setAdapter((ListAdapter) this.mAdapter);
    }

    public void onDestroy() {
        if (this.mTask != null) {
            this.mTask.cancel(true);
            this.mTask = null;
        }
        getListView().setAdapter((ListAdapter) null);
        this.mAdapter = null;
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        String categoryName = this.mAdapter.getItem(position);
        RingtoneListActivity.startActivity(this, categoryName, RingbowHelper.getCategoryURL(categoryName));
    }

    protected static class MyAdapter extends BaseAdapter {
        /* access modifiers changed from: private */
        public CategoryActivity mActivity = null;
        private List<String> mCategoryList = new LinkedList();

        public MyAdapter(CategoryActivity activity) {
            this.mActivity = activity;
        }

        class ViewHolder {
            TextView mTVText;

            ViewHolder() {
            }
        }

        public int getCount() {
            return this.mCategoryList.size();
        }

        public String getItem(int pos) {
            try {
                return this.mCategoryList.get(pos);
            } catch (Exception e) {
                return null;
            }
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int pos, View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                view = LayoutInflater.from(this.mActivity).inflate((int) R.layout.category_item, parent, false);
                ViewHolder vh = new ViewHolder();
                vh.mTVText = (TextView) view.findViewById(R.id.Text);
                view.setTag(vh);
            }
            ViewHolder vh2 = (ViewHolder) view.getTag();
            String categoryName = getItem(pos);
            if (categoryName != null) {
                vh2.mTVText.setText(categoryName);
            }
            return view;
        }

        public void add(String categoryName) {
            this.mCategoryList.add(categoryName);
            notifyDataSetChanged();
        }
    }

    public void CT_OnBegin() {
    }

    public void CT_OnFinished(boolean bError) {
        this.mTask = null;
    }

    public void CT_OnUpdate(String categoryName) {
        if (this.mAdapter != null) {
            this.mAdapter.add(categoryName);
        }
    }
}
