package me.ring.knou1;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import me.ring.knou1.editor.RingtoneSelectActivity;

public class HomeActivity extends ListActivity {
    private MyAdapter mAdapter = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.home);
        this.mAdapter = new MyAdapter(this);
        getListView().setAdapter((ListAdapter) this.mAdapter);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int pos, long id) {
        switch (pos) {
            case 0:
                startActivity(new Intent(this, TopDownloadActivity.class));
                return;
            case 1:
                startActivity(new Intent(this, FeaturedActivity.class));
                return;
            case 2:
                startActivity(new Intent(this, TopArtistActivity.class));
                return;
            case 3:
                startActivity(new Intent(this, CategoryActivity.class));
                return;
            case 4:
                startActivity(new Intent(this, SearchActivity.class));
                return;
            case 5:
                startActivity(new Intent(this, MyDownloadsActivity.class));
                return;
            case 6:
                startActivity(new Intent(this, RingtoneSelectActivity.class));
                return;
            default:
                return;
        }
    }

    private static class MyAdapter extends BaseAdapter {
        private static final int[] mHomeIconList = {R.drawable.ic_menu_chart, R.drawable.ic_menu_feature, R.drawable.ic_menu_artist, R.drawable.ic_menu_category, R.drawable.ic_menu_search, R.drawable.ic_menu_download, R.drawable.ic_menu_editor};
        private static final String[] mHomeList = {"Download Charts", "Featured Ringtones", "Top Artists", "Browse Categories", "Search", "Your Downloads", "Ringtone Editor"};
        private Activity mActivity = null;

        public MyAdapter(Activity a) {
            this.mActivity = a;
        }

        public int getCount() {
            return mHomeList.length;
        }

        public String getItem(int pos) {
            return mHomeList[pos];
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        class ViewHolder {
            ImageView mIcon;
            TextView mTVText;

            ViewHolder() {
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int pos, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = LayoutInflater.from(this.mActivity).inflate((int) R.layout.home_list_item, parent, false);
                ViewHolder vh = new ViewHolder();
                vh.mTVText = (TextView) view.findViewById(R.id.Title);
                vh.mIcon = (ImageView) view.findViewById(R.id.Icon);
                view.setTag(vh);
            } else {
                view = convertView;
            }
            ViewHolder vh2 = (ViewHolder) view.getTag();
            vh2.mTVText.setText(getItem(pos));
            vh2.mIcon.setBackgroundResource(mHomeIconList[pos]);
            return view;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            ((RingbowApp) getApplication()).mPlayer.stop();
        }
        return super.onKeyDown(keyCode, event);
    }
}
