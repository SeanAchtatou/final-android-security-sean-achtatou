package me.ring.knou1;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;
import me.ring.knou1.utils.RingbowHelper;

public class TopDownloadActivity extends TabActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TabHost tabHost = getTabHost();
        Resources resources = getResources();
        Intent intent = new Intent(this, RingtoneListActivity.class);
        intent.putExtra("rurl", RingbowHelper.getPage(RingbowHelper.getDayTopDownload(), 0));
        intent.putExtra("vtitle", "Today");
        tabHost.addTab(tabHost.newTabSpec("Today").setIndicator("Today", resources.getDrawable(R.drawable.days)).setContent(intent));
        Intent intent2 = new Intent(this, RingtoneListActivity.class);
        intent2.putExtra("rurl", RingbowHelper.getPage(RingbowHelper.getWeeklyTopDownload(), 0));
        intent2.putExtra("vtitle", "This Week");
        tabHost.addTab(tabHost.newTabSpec("This Week").setIndicator("This Week", resources.getDrawable(R.drawable.weeks)).setContent(intent2));
        Intent intent3 = new Intent(this, RingtoneListActivity.class);
        intent3.putExtra("rurl", RingbowHelper.getPage(RingbowHelper.getMonthTopDownload(), 0));
        intent3.putExtra("vtitle", "This Mouth");
        tabHost.addTab(tabHost.newTabSpec("This Mouth").setIndicator("This Mouth", resources.getDrawable(R.drawable.month)).setContent(intent3));
    }
}
