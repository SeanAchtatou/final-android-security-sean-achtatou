package me.ring.knou1;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ProgressBar;
import android.widget.TextView;
import me.ring.knou1.task.SumTask;
import me.ring.knou1.utils.RingbowHelper;

public class StarterActivity extends Activity implements SumTask.Listener {
    /* access modifiers changed from: private */
    public static int MSG_PROGRESS = 1;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == StarterActivity.MSG_PROGRESS) {
                StarterActivity.this.mProgBar.setProgress(StarterActivity.this.mProgBar.getProgress() + 10);
                if (StarterActivity.this.mProgBar.getProgress() >= StarterActivity.this.mProgBar.getMax()) {
                    ArtistRingtoneActivity.startActivity(StarterActivity.this, "Kelly Clarkson Ringtone", RingbowHelper.getSearchURL("Kelly Clarkson"));
                    StarterActivity.this.finish();
                    return;
                }
                StarterActivity.this.mHandler.sendEmptyMessageDelayed(StarterActivity.MSG_PROGRESS, 100);
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressBar mProgBar = null;
    private SumTask mSumTask = null;
    private TextView mTVMsg = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.starter);
        this.mProgBar = (ProgressBar) findViewById(R.id.ProgBar);
        this.mProgBar.setMax(100);
        this.mTVMsg = (TextView) findViewById(R.id.Msg);
        this.mTVMsg.setVisibility(4);
        this.mSumTask = new SumTask(this, this);
        this.mSumTask.execute(new Object[0]);
        this.mHandler.sendEmptyMessageDelayed(MSG_PROGRESS, 100);
    }

    public void ST_OnShowSum(String sumStr) {
        try {
            Integer decode = Integer.decode(sumStr);
            this.mTVMsg.setText("Loading ringtones...");
            this.mTVMsg.setVisibility(0);
        } catch (Exception e) {
        }
    }
}
