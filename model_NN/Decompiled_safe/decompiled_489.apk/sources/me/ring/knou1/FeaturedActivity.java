package me.ring.knou1;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import me.ring.knou1.task.HotMusicTask;
import me.ring.knou1.task.Ringtone;
import me.ring.knou1.task.ThumbnailTask;
import me.ring.knou1.utils.RingbowHelper;

public class FeaturedActivity extends ListActivity implements HotMusicTask.Listener, ThumbnailTask.Listener {
    private static final int DLG_LOADING = 1;
    private MyAdapter mAdapter = null;
    private HotMusicTask mTask = null;
    /* access modifiers changed from: private */
    public HashMap<Integer, ThumbnailTask> mTaskTBs = new HashMap<>(6);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.featured);
        AdsView.createAdWhirl(this);
        this.mAdapter = (MyAdapter) getLastNonConfigurationInstance();
        if (this.mAdapter == null) {
            this.mAdapter = new MyAdapter(this);
            setListAdapter(this.mAdapter);
            this.mTask = new HotMusicTask(this);
            this.mTask.execute(RingbowHelper.getHotMusicURL());
            return;
        }
        this.mAdapter.mActivity = this;
        setListAdapter(this.mAdapter);
    }

    public Object onRetainNonConfigurationInstance() {
        if (this.mAdapter == null || !this.mAdapter.bFinished) {
            return null;
        }
        return this.mAdapter;
    }

    public void onDestroy() {
        if (this.mTask != null) {
            this.mTask.cancel(true);
            this.mTask = null;
        }
        Enumeration<Integer> enu = Collections.enumeration(this.mTaskTBs.keySet());
        while (enu.hasMoreElements()) {
            this.mTaskTBs.get(enu.nextElement()).cancel(true);
        }
        this.mTaskTBs.clear();
        getListView().setAdapter((ListAdapter) null);
        this.mAdapter = null;
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Ringtone rt = this.mAdapter.getItem(position);
        if (rt != null) {
            RingtoneListActivity.startActivity(this, rt.mTitle, RingbowHelper.getByArtistAndTitleURL(rt.mArtist, rt.mTitle));
        }
    }

    private static class MyAdapter extends BaseAdapter {
        boolean bFinished = false;
        /* access modifiers changed from: private */
        public FeaturedActivity mActivity = null;
        private List<Ringtone> mRingtones = new LinkedList();

        public MyAdapter(FeaturedActivity activity) {
            this.mActivity = activity;
        }

        private class ViewHolder {
            ImageView mIVThumbnail;
            TextView mTVArtist;
            TextView mTVSong;

            private ViewHolder() {
            }

            /* synthetic */ ViewHolder(MyAdapter myAdapter, ViewHolder viewHolder) {
                this();
            }
        }

        public int getCount() {
            return this.mRingtones.size();
        }

        public Ringtone getItem(int position) {
            try {
                return this.mRingtones.get(position);
            } catch (Exception e) {
                return null;
            }
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                view = LayoutInflater.from(this.mActivity).inflate((int) R.layout.featured_item, parent, false);
                ViewHolder vh = new ViewHolder(this, null);
                vh.mTVArtist = (TextView) view.findViewById(R.id.Artist);
                vh.mTVSong = (TextView) view.findViewById(R.id.Title);
                vh.mIVThumbnail = (ImageView) view.findViewById(R.id.Thumbnail);
                view.setTag(vh);
            }
            ViewHolder vh2 = (ViewHolder) view.getTag();
            Ringtone rt = getItem(position);
            if (rt != null) {
                vh2.mTVSong.setText(rt.mTitle);
                vh2.mTVArtist.setText(rt.mArtist);
                if (rt.mBmp != null) {
                    vh2.mIVThumbnail.setImageBitmap(rt.mBmp);
                } else {
                    vh2.mIVThumbnail.setImageResource(R.drawable.default_thumbnail);
                    Integer pos = new Integer(position);
                    ThumbnailTask t = new ThumbnailTask(this.mActivity);
                    t.execute(rt, pos);
                    this.mActivity.mTaskTBs.put(pos, t);
                }
            }
            return view;
        }

        public void add(Ringtone rt) {
            this.mRingtones.add(rt);
        }
    }

    public void HMT_OnBegin() {
        if (this.mAdapter != null) {
            this.mAdapter.bFinished = false;
        }
        showDialog(1);
    }

    public void HMT_OnFinished(boolean bError) {
        dismissDialog(1);
        this.mTask = null;
        if (bError) {
            Toast.makeText(this, getResources().getString(R.string.ERR_NETWORK), 0).show();
        } else if (this.mAdapter != null) {
            this.mAdapter.notifyDataSetChanged();
            this.mAdapter.bFinished = true;
        }
    }

    public void HMT_OnReady(Ringtone rt) {
        if (this.mAdapter != null) {
            this.mAdapter.add(rt);
        }
    }

    public void TT_OnBegin() {
    }

    public void TT_OnFinished(boolean bError) {
    }

    public void TT_OnThumbnailReady(int pos, Ringtone rt) {
        ListView listView = getListView();
        int firstPos = listView.getFirstVisiblePosition();
        int count = listView.getChildCount();
        if (pos >= firstPos && pos < firstPos + count) {
            ((MyAdapter.ViewHolder) listView.getChildAt(pos - firstPos).getTag()).mIVThumbnail.setImageBitmap(rt.mBmp);
        }
        this.mTaskTBs.remove(new Integer(pos));
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                ProgressDialog dialog = new ProgressDialog(this);
                dialog.setTitle("A moment please...");
                dialog.setMessage("Retreiving data...");
                dialog.setIndeterminate(true);
                dialog.setCancelable(true);
                return dialog;
            default:
                return null;
        }
    }
}
