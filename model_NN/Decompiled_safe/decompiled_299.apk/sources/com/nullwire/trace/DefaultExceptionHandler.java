package com.nullwire.trace;

import android.util.Log;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import java.util.Random;

public class DefaultExceptionHandler implements Thread.UncaughtExceptionHandler {
    private static final String TAG = "UNHANDLED_EXCEPTION";
    private Thread.UncaughtExceptionHandler defaultExceptionHandler;

    public DefaultExceptionHandler(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.defaultExceptionHandler = uncaughtExceptionHandler;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        try {
            String str = G.APP_VERSION + "-" + Integer.toString(new Random().nextInt(99999));
            Log.d(TAG, "Writing unhandled exception to: " + G.FILES_PATH + "/" + str + ".stacktrace");
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(G.FILES_PATH + "/" + str + ".stacktrace"));
            bufferedWriter.write(G.ANDROID_VERSION + "\n");
            bufferedWriter.write(G.PHONE_MODEL + "\n");
            bufferedWriter.write(stringWriter.toString());
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(TAG, stringWriter.toString());
        this.defaultExceptionHandler.uncaughtException(thread, th);
    }
}
