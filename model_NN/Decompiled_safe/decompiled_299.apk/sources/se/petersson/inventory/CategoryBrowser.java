package se.petersson.inventory;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.SearchRecentSuggestions;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.SimpleCursorAdapter;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import se.petersson.inventory.InventoryProvider;

public class CategoryBrowser extends ListActivity implements View.OnCreateContextMenuListener, SharedPreferences.OnSharedPreferenceChangeListener {
    private static final int ITEM_DELETE = 2;
    private static final int ITEM_GOOGLE_BOOKS = 5;
    private static final int ITEM_GOOGLE_IMAGES = 4;
    private static final int ITEM_LEND_TO = 7;
    private static final int ITEM_PRICES_PRISJAKT = 1;
    private static final int ITEM_SEARCH_AMAZON = 8;
    private static final int ITEM_SEARCH_GOOGLE = 3;
    private static final int ITEM_SEARCH_PRISJAKT = 0;
    private static final int ITEM_SHARE = 9;
    private static final int ITEM_SHOW_IMAGE = 6;
    private static final int ITEM_VIEW = 10;
    private static final int RES_CONTACT = 0;
    private static final int STYLE_CATEGORIES = 0;
    private static final int STYLE_CATEGORY = 2;
    private static final int STYLE_OWNERSHIP = 3;
    private static final int STYLE_PRICES = 4;
    private static final int STYLE_SEARCHRESULT = 1;
    private static final int STYLE_TAGS = 5;
    private View badge = null;
    private Boolean busy = false;
    /* access modifiers changed from: private */
    public DBAdapter db;
    long lendItem = -1;
    /* access modifiers changed from: private */
    public final ContactAccessor mContactAccessor = ContactAccessor.getInstance();
    private ListView mContent;
    final Handler mHandler = new Handler();
    private SharedPreferences mPreferences;
    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            CategoryBrowser.this.updateResultsInUi();
        }
    };
    private Cursor myList;
    private Boolean order = true;
    protected String parent = null;
    /* access modifiers changed from: private */
    public int presentationStyle;
    List<Map<String, String>> priceList = null;
    private String prices = null;
    private String queryAction = null;
    private int scrollPos = 0;
    private String sort = "name";
    private Button subcatButton;
    /* access modifiers changed from: private */
    public long tag = -2;
    private ImageButton tagsButton;
    private String title;
    private LinearLayout top;
    private String total;
    private String whenSelecting;

    public enum HitSort {
        ALPHA(R.string.sort_alpha, "name"),
        CHANGED(R.string.sort_changed, DBAdapter.KEY_CHANGED),
        CREATED(R.string.sort_created, DBAdapter.KEY_ROWID),
        NUMERAL(R.string.sort_numeral, DBAdapter.KEY_NUMERAL),
        NOTE(R.string.sort_note, DBAdapter.KEY_NOTE);
        
        final String arg;
        final int label;

        private HitSort(int label2, String arg2) {
            this.label = label2;
            this.arg = arg2;
        }

        public int label() {
            return this.label;
        }

        public String arg() {
            return this.arg;
        }
    }

    private void saveSort() {
        int i = 0;
        int val = 0;
        for (HitSort s : HitSort.values()) {
            if (this.sort.equals(s.arg)) {
                val = i;
            }
            i++;
        }
        if (!this.order.booleanValue()) {
            val += 10000;
        }
        ContentValues vals = new ContentValues();
        vals.put(DBAdapter.KEY_SORTING, Integer.valueOf(val));
        long key = -1;
        switch (this.presentationStyle) {
            case 2:
                Cursor c = this.db.getOptions("name=" + DatabaseUtils.sqlEscapeString(this.parent), null);
                if (c.moveToFirst()) {
                    int columnIndexOrThrow = c.getColumnIndexOrThrow(DBAdapter.KEY_ROWID);
                    key = c.getLong(c.getColumnIndexOrThrow(DBAdapter.KEY_ROWID));
                }
                c.close();
                break;
            case 5:
                if (this.tag >= 0) {
                    key = this.tag;
                    break;
                }
                break;
        }
        if (key != -1) {
            this.db.updateOption(key, vals);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (requestCode) {
            case 0:
                if (resultCode == -1) {
                    Uri contactData = intent.getData();
                    ContentValues vals = new ContentValues();
                    vals.put("state", (Integer) 2);
                    vals.put("owner", contactData.toString());
                    this.db.updateObject(this.lendItem, vals);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void restoreSort() {
        Cursor c;
        long key = -1;
        switch (this.presentationStyle) {
            case 2:
                Cursor c2 = this.db.getOptions("name=" + DatabaseUtils.sqlEscapeString(this.parent), null);
                if (c2.moveToFirst()) {
                    int columnIndexOrThrow = c2.getColumnIndexOrThrow(DBAdapter.KEY_ROWID);
                    key = c2.getLong(c2.getColumnIndexOrThrow(DBAdapter.KEY_ROWID));
                }
                c2.close();
                break;
            case DBAdapter.STATE_HIDE:
            case 4:
            default:
                return;
            case 5:
                if (this.tag >= 0) {
                    key = this.tag;
                    break;
                } else {
                    return;
                }
        }
        if (key >= 0 && (c = this.db.getOption(key)) != null) {
            long val = c.getLong(c.getColumnIndexOrThrow(DBAdapter.KEY_SORTING));
            c.close();
            if (val > 10000) {
                this.order = false;
                val -= 10000;
            } else {
                this.order = true;
            }
            int i = 0;
            for (HitSort s : HitSort.values()) {
                if (((long) i) == val) {
                    this.sort = s.arg;
                }
                i++;
            }
        }
    }

    public void onDestroy() {
        saveSort();
        if (this.myList != null && !this.myList.isClosed()) {
            this.myList.close();
        }
        super.onDestroy();
    }

    public void onCreate(Bundle savedInstanceState) {
        ZapApp.compat.hasOptionsMenu(this);
        requestWindowFeature(5);
        super.onCreate(savedInstanceState);
        this.mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.mPreferences.registerOnSharedPreferenceChangeListener(this);
        loadPreferences();
        Intent queryIntent = getIntent();
        this.queryAction = queryIntent.getAction();
        this.parent = queryIntent.getStringExtra("CategoryParent");
        this.prices = queryIntent.getStringExtra("LookupPrices");
        this.tag = queryIntent.getLongExtra("tag", -2);
        if (this.parent != null) {
            this.presentationStyle = 2;
        } else if ("android.intent.action.SEARCH".equals(this.queryAction)) {
            this.presentationStyle = 1;
        } else if (queryIntent.getLongExtra("ownershipState", -1024) != -1024) {
            this.presentationStyle = 3;
        } else if (this.tag > -2) {
            this.presentationStyle = 5;
        } else {
            this.presentationStyle = 0;
        }
        setContentView((int) R.layout.category_browser);
        this.subcatButton = (Button) findViewById(R.id.subcat_button);
        this.tagsButton = (ImageButton) findViewById(R.id.tags_button);
        this.mContent = getListView();
        this.db = ZapApp.getDB(this, false);
        restoreSort();
        doQuery(this.parent);
        getListView().setOnCreateContextMenuListener(this);
        this.top = (LinearLayout) findViewById(R.id.top);
    }

    private void loadPreferences() {
        this.mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.whenSelecting = this.mPreferences.getString("pref_when_selecting", "edit");
    }

    private void doQuery(final String parent2) {
        this.busy = true;
        this.mContent.setVisibility(ITEM_SEARCH_AMAZON);
        getWindow().setFeatureInt(5, -1);
        if (this.myList != null) {
            this.myList.close();
        }
        new Thread() {
            public void run() {
                CategoryBrowser.this.doHardWork(parent2);
                CategoryBrowser.this.mHandler.post(CategoryBrowser.this.mUpdateResults);
            }
        }.start();
    }

    class MySimpleCursorAdapter extends SimpleCursorAdapter {
        private Cursor c;
        private int clayout;
        DateFormat df;
        private SectionIndexer mIndexer = null;
        ArrayAdapter<CharSequence> statesAdapter;

        public MySimpleCursorAdapter(Context context, int layout, Cursor c2, String[] from, int[] to) {
            super(context, layout, c2, from, to);
            this.statesAdapter = ArrayAdapter.createFromResource(CategoryBrowser.this, R.array.states, 17367048);
            this.df = DateFormat.getDateTimeInstance();
            this.c = c2;
            this.clayout = layout;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            String firstPrice;
            String owner;
            View v = super.getView(position, convertView, parent);
            if (CategoryBrowser.this.presentationStyle == 5 && CategoryBrowser.this.tag == -1) {
                TextView tv = (TextView) v.findViewById(R.id.count);
                String val = this.c.getString(this.c.getColumnIndexOrThrow(DBAdapter.KEY_GROUP));
                if (val == null || val.length() == 0) {
                    tv.setText("0");
                }
            }
            if (this.clayout == R.layout.objectlist) {
                TextView tv2 = (TextView) v.findViewById(R.id.date);
                if (tv2 != null) {
                    tv2.setText(this.df.format(Long.valueOf(this.c.getLong(this.c.getColumnIndexOrThrow(DBAdapter.KEY_CHANGED)))));
                }
                String name = null;
                if (!(CategoryBrowser.this.presentationStyle == 3 || (owner = this.c.getString(this.c.getColumnIndexOrThrow("owner"))) == null)) {
                    name = String.valueOf(owner) + " no longer exists";
                    Cursor c2 = CategoryBrowser.this.getContentResolver().query(Uri.parse(owner), null, null, null, null);
                    if (c2 != null) {
                        if (c2.moveToFirst()) {
                            name = c2.getString(c2.getColumnIndexOrThrow(CategoryBrowser.this.mContactAccessor.getNameIndex()));
                        }
                        c2.close();
                    }
                }
                TextView tv3 = (TextView) v.findViewById(R.id.text2);
                if (tv3 != null) {
                    CharSequence txt = tv3.getText();
                    tv3.setVisibility((txt == null || txt.length() == 0) ? CategoryBrowser.ITEM_SEARCH_AMAZON : 0);
                }
                TableRow tr = (TableRow) v.findViewById(R.id.ownerrow);
                if (!(name == null || (tv3 = (TextView) v.findViewById(R.id.owner)) == null)) {
                    tv3.setText(name);
                }
                tr.setVisibility((name == null || tv3 == null) ? CategoryBrowser.ITEM_SEARCH_AMAZON : 0);
                TextView tv4 = (TextView) v.findViewById(R.id.rate);
                if (tv4 != null) {
                    int dbState = this.c.getInt(this.c.getColumnIndexOrThrow("state"));
                    tv4.setText(this.statesAdapter.getItem(dbState));
                    int color = -16777216;
                    switch (dbState) {
                        case 0:
                            color = Color.rgb(0, 170, 0);
                            break;
                        case 1:
                            color = -16776961;
                            break;
                        case 2:
                            color = -65536;
                            break;
                        case DBAdapter.STATE_HIDE:
                            color = -3355444;
                            break;
                        case 4:
                            color = -65281;
                            break;
                    }
                    tv4.setTextColor(color);
                    TextView tv5 = (TextView) v.findViewById(R.id.numeral);
                    tv5.setText(this.c.getString(this.c.getColumnIndexOrThrow(DBAdapter.KEY_NUMERAL)));
                    tv5.setBackgroundColor(-3355444);
                    TextView tv6 = (TextView) v.findViewById(R.id.price);
                    tv6.setText("");
                    if (CategoryBrowser.this.priceList != null) {
                        try {
                            JSONObject myPrices = ZapsInventory.pjLookup.lastJSONobject.getJSONObject(this.c.getString(this.c.getColumnIndexOrThrow("prisjaktid")));
                            if (!(myPrices == null || (firstPrice = myPrices.getString("price_str")) == null)) {
                                tv6.setText(firstPrice);
                            }
                        } catch (NullPointerException | JSONException e) {
                        }
                    }
                    ImageView iv = (ImageView) v.findViewById(R.id.photoView);
                    if (iv != null) {
                        String img = this.c.getString(this.c.getColumnIndexOrThrow(DBAdapter.KEY_IMAGE));
                        iv.setVisibility((img == null || img.length() <= 3) ? CategoryBrowser.ITEM_SEARCH_AMAZON : 0);
                    }
                }
            }
            return v;
        }
    }

    /* access modifiers changed from: private */
    public void updateResultsInUi() {
        int nitems;
        int myLayout;
        String[] dataMap;
        int[] myIds;
        JSONObject priceItem;
        int[] iArr = {R.id.title, R.id.count};
        if (this.myList == null || this.myList.isClosed()) {
            Toast.makeText(this, "Database problem", 1).show();
            finish();
            return;
        }
        String[] dataMap2 = {"category_name", "product_count"};
        ListView listView = getListView();
        if (this.myList.getCount() >= 1 || this.presentationStyle == 5) {
            int nPrices = 0;
            float sumPrices = 0.0f;
            if (this.prices != null) {
                JSONObject jo = ZapsInventory.pjLookup.lastJSONobject;
                if (jo != null) {
                    try {
                        JSONArray nameArray = jo.names();
                        JSONArray valArray = jo.toJSONArray(nameArray);
                        for (int j = 0; j < valArray.length(); j++) {
                            if (!nameArray.getString(j).equals("2") && (priceItem = jo.getJSONObject(nameArray.getString(j))) != null) {
                                nPrices++;
                                sumPrices += Float.valueOf(priceItem.getString(InventoryProvider.PricesColumns.PRICE)).floatValue();
                            }
                        }
                    } catch (JSONException e) {
                        Toast.makeText(this, R.string.label_nothing_to_show, 0).show();
                        finish();
                        return;
                    }
                }
                if (nPrices == 0) {
                    Toast.makeText(this, R.string.label_nothing_to_show, 0).show();
                    finish();
                    return;
                }
            }
            Boolean endless = false;
            switch (this.presentationStyle) {
                case 0:
                    this.title = r(R.string.title_categories);
                    nitems = this.myList.getCount();
                    setTitle(String.valueOf(this.title) + " (" + nitems + ")");
                    myLayout = R.layout.category_info;
                    dataMap = new String[]{"name", DBAdapter.KEY_NOTE, DBAdapter.KEY_COUNT};
                    myIds = new int[]{R.id.title, R.id.comment, R.id.count};
                    break;
                case 5:
                    if (this.tag != -1) {
                        Cursor taginfo = this.db.getOption(this.tag);
                        if (taginfo != null) {
                            nitems = this.myList.getCount();
                            String name = taginfo.getString(taginfo.getColumnIndexOrThrow("name"));
                            taginfo.close();
                            setTitle(String.valueOf(name) + " (" + nitems + ")");
                            myLayout = R.layout.objectlist;
                            dataMap = new String[]{"name", DBAdapter.KEY_NOTE, "state", DBAdapter.KEY_CHANGED};
                            myIds = new int[]{R.id.text1, R.id.text2, R.id.rate, R.id.date};
                            break;
                        } else {
                            Toast.makeText(this, "Database problem", 1).show();
                            finish();
                            return;
                        }
                    } else {
                        this.title = r(R.string.label_group);
                        nitems = this.myList.getCount();
                        setTitle(String.valueOf(this.title) + " (" + nitems + ")");
                        myLayout = R.layout.category_info;
                        dataMap = new String[]{"name", DBAdapter.KEY_NOTE, DBAdapter.KEY_COUNT};
                        myIds = new int[]{R.id.title, R.id.comment, R.id.count};
                        break;
                    }
                default:
                    this.title = this.parent;
                    String name2 = this.title;
                    switch (this.presentationStyle) {
                        case 1:
                            String queryString = getIntent().getStringExtra("query");
                            if (!"%".equals(queryString)) {
                                name2 = String.valueOf(r(R.string.label_search_results_for)) + ": " + getIntent().getStringExtra("query");
                                new SearchRecentSuggestions(getApplication(), "se.petersson.inventory.SuggestionProvider", 1).saveRecentQuery(queryString, null);
                                break;
                            } else {
                                name2 = r(R.string.label_list_everything);
                                break;
                            }
                        case 2:
                            name2 = this.parent;
                            break;
                        case DBAdapter.STATE_HIDE:
                            String ownerShip = getIntent().getStringExtra("ownership");
                            if (ownerShip != null && ownerShip.startsWith("content:")) {
                                name2 = String.valueOf(ownerShip) + " no longer exists";
                                Uri contact = Uri.parse(ownerShip);
                                Cursor c = getContentResolver().query(contact, null, null, null, null);
                                if (c != null) {
                                    if (c.moveToFirst()) {
                                        name2 = c.getString(c.getColumnIndexOrThrow(this.mContactAccessor.getNameIndex()));
                                    }
                                    c.close();
                                }
                                if (this.badge != null) {
                                    this.top.removeView(this.badge);
                                }
                                this.badge = this.mContactAccessor.getContactBadge(this, contact, listView);
                                if (this.badge != null) {
                                    this.top.addView(this.badge, 0);
                                    break;
                                }
                            } else {
                                name2 = r(R.string.me);
                                break;
                            }
                            break;
                    }
                    nitems = this.myList.getCount();
                    String name3 = String.valueOf(name2) + " (" + nitems + ")";
                    if (nPrices != 0) {
                        TextView tv = (TextView) findViewById(R.id.prisjaktHeader);
                        tv.setVisibility(0);
                        tv.setText(" Σ " + Math.round(sumPrices) + ":- (" + nPrices + ") " + getCurrency());
                    }
                    setTitle(name3);
                    myLayout = R.layout.objectlist;
                    dataMap = new String[]{"name", DBAdapter.KEY_NOTE, "state", DBAdapter.KEY_CHANGED};
                    myIds = new int[]{R.id.text1, R.id.text2, R.id.rate, R.id.date};
                    break;
            }
            ListAdapter la = new MySimpleCursorAdapter(this, myLayout, this.myList, dataMap, myIds);
            if (la == null || (la.getCount() == 0 && this.presentationStyle != 5)) {
                Toast.makeText(this, "No more categories found", 0).show();
                finish();
                return;
            }
            setListAdapter(la);
            getListView().setFastScrollEnabled(true);
            if (this.scrollPos > la.getCount()) {
                this.scrollPos = la.getCount();
            }
            getListView().setSelection(this.scrollPos);
            if (this.presentationStyle == 0) {
                this.subcatButton.setVisibility(0);
                this.subcatButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        CategoryBrowser.this.onSubcatClick(v);
                    }
                });
                this.tagsButton.setVisibility(0);
                this.tagsButton.setOnClickListener(new View.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                     arg types: [java.lang.String, int]
                     candidates:
                      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
                    public void onClick(View v) {
                        Intent tags = new Intent(CategoryBrowser.this, CategoryBrowser.class);
                        tags.putExtra("tag", -1L);
                        CategoryBrowser.this.startActivity(tags);
                    }
                });
            }
            this.mContent.setVisibility(0);
            getWindow().setFeatureInt(5, -2);
            if (endless.booleanValue()) {
                setTitle(String.valueOf(this.title) + " (" + this.myList.getCount() + "/" + this.total + ")");
            }
            TextView empty = (TextView) findViewById(R.id.empty_tags);
            if (empty != null && this.presentationStyle == 5 && this.tag == -1) {
                empty.setVisibility(nitems == 0 ? 0 : ITEM_SEARCH_AMAZON);
            }
            this.busy = null;
            return;
        }
        Toast.makeText(this, R.string.label_nothing_to_show, 0).show();
        finish();
    }

    private String getCurrency() {
        String server = PrisjaktLookup.jsonserver;
        if (server.contains("prisjakt.nu")) {
            return "SEK";
        }
        if (server.contains(".no")) {
            return "NOK";
        }
        if (server.contains(".nz")) {
            return "NZD";
        }
        return "";
    }

    public void onSubcatClick(View view) {
        final Cursor subcats = this.db.getStateUserSum(null);
        final OwnershipCursorAdapter a = new OwnershipCursorAdapter(this, R.layout.category_info, subcats, new String[]{"owner", DBAdapter.KEY_COUNT}, new int[]{R.id.title, R.id.count});
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.label_select_subcategory);
        builder.setAdapter(a, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int position) {
                Intent mint = new Intent(CategoryBrowser.this, CategoryBrowser.class);
                Cursor c = (Cursor) a.getItem(position);
                dialog.dismiss();
                mint.putExtra("ownershipState", c.getLong(c.getColumnIndexOrThrow("state")));
                mint.putExtra("ownership", c.getString(c.getColumnIndexOrThrow("owner")));
                CategoryBrowser.this.startActivity(mint);
                subcats.close();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                subcats.close();
            }
        });
        builder.show();
    }

    class OwnershipCursorAdapter extends SimpleCursorAdapter {
        private Cursor c;
        ArrayAdapter<CharSequence> statesAdapter;

        public OwnershipCursorAdapter(Context context, int layout, Cursor c2, String[] from, int[] to) {
            super(context, layout, c2, from, to);
            this.statesAdapter = ArrayAdapter.createFromResource(CategoryBrowser.this, R.array.states, 17367048);
            this.c = c2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = super.getView(position, convertView, parent);
            String owner = this.c.getString(this.c.getColumnIndexOrThrow("owner"));
            String name = CategoryBrowser.this.r(R.string.me);
            if (owner != null && owner.startsWith("content")) {
                name = String.valueOf(owner) + " no longer exists";
                Cursor c2 = CategoryBrowser.this.getContentResolver().query(Uri.parse(owner), null, null, null, null);
                if (c2 != null) {
                    if (c2.moveToFirst()) {
                        name = c2.getString(c2.getColumnIndexOrThrow(CategoryBrowser.this.mContactAccessor.getNameIndex()));
                    }
                    c2.close();
                }
            }
            TextView tv = (TextView) v.findViewById(R.id.title);
            tv.setText(name);
            tv.setTag(owner);
            TextView tv2 = (TextView) v.findViewById(R.id.comment);
            if (tv2 != null) {
                int dbState = this.c.getInt(this.c.getColumnIndexOrThrow("state"));
                tv2.setText(this.statesAdapter.getItem(dbState));
                int color = -16777216;
                switch (dbState) {
                    case 0:
                        color = Color.rgb(0, 170, 0);
                        break;
                    case 1:
                        color = -16776961;
                        break;
                    case 2:
                        color = -65536;
                        break;
                    case DBAdapter.STATE_HIDE:
                        color = -3355444;
                        break;
                    case 4:
                        color = -65281;
                        break;
                }
                tv2.setTextColor(color);
                tv2.setTag(Integer.valueOf(dbState));
            }
            return v;
        }
    }

    public void onPathClick(View view) {
    }

    private String noCase() {
        if (this.sort.equals("name") || this.sort.equals(DBAdapter.KEY_NOTE)) {
            return " COLLATE NOCASE";
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void doHardWork(String parent2) {
        String subset;
        String all;
        this.db = ZapApp.getDB(this, false);
        if (this.db != null) {
            String sortorder = String.valueOf(this.sort) + noCase() + " " + (this.order.booleanValue() ? "asc" : "desc");
            switch (this.presentationStyle) {
                case 0:
                    try {
                        this.myList = this.db.getCategoriesSum(sortorder);
                        break;
                    } catch (IllegalStateException e) {
                        Log.e("Inventory(doHardWork)", "Failed to get CategoriesSum: " + e.getMessage());
                        this.myList = null;
                        break;
                    }
                case 1:
                    String queryString = getIntent().getStringExtra("query");
                    this.myList = this.db.getObjects("name like " + DatabaseUtils.sqlEscapeString("%" + queryString + "%") + " or " + DBAdapter.KEY_NOTE + " like " + DatabaseUtils.sqlEscapeString("%" + queryString + "%") + " or " + DBAdapter.KEY_BARCODE + " like " + DatabaseUtils.sqlEscapeString("%" + queryString + "%"), sortorder);
                    break;
                case 2:
                    this.myList = this.db.getObjects("category=" + DatabaseUtils.sqlEscapeString(parent2), sortorder);
                    break;
                case DBAdapter.STATE_HIDE:
                    String subset2 = "state=" + Long.valueOf(getIntent().getLongExtra("ownershipState", 0));
                    String ownerShip = getIntent().getStringExtra("ownership");
                    if (ownerShip == null || !ownerShip.startsWith("content:")) {
                        subset = String.valueOf(subset2) + " AND owner is null ";
                    } else {
                        subset = String.valueOf(subset2) + " AND owner = " + DatabaseUtils.sqlEscapeString(ownerShip);
                    }
                    this.myList = this.db.getObjects(subset, sortorder);
                    break;
                case 5:
                    if (this.tag != -1) {
                        this.myList = this.db.getObjects("grp like " + DatabaseUtils.sqlEscapeString("%," + this.tag + ",%"), sortorder);
                        break;
                    } else {
                        this.myList = this.db.getTagsSum(sortorder);
                        break;
                    }
            }
            if (this.prices != null) {
                int pjIdx = this.myList.getColumnIndexOrThrow("prisjaktid");
                if (this.myList.moveToFirst()) {
                    String all2 = this.myList.getString(pjIdx);
                    if (all2 == null) {
                        all = "";
                    } else {
                        all = all2.concat(",");
                    }
                    while (this.myList.moveToNext()) {
                        String pjId = this.myList.getString(pjIdx);
                        if (pjId != null && pjId.length() > 0) {
                            all = all.concat(String.valueOf(pjId) + ",");
                        }
                    }
                    if (all.length() > 0) {
                        this.priceList = ZapsInventory.pjLookup.itemHeader(all);
                        return;
                    }
                    this.priceList = null;
                    ZapsInventory.pjLookup.lastJSONobject = null;
                    return;
                }
                return;
            }
            ZapsInventory.pjLookup.lastJSONobject = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (position < this.myList.getCount()) {
            this.scrollPos = position;
            Cursor c = (Cursor) getListAdapter().getItem(position);
            switch (this.presentationStyle) {
                case 0:
                    Intent self = new Intent(this, CategoryBrowser.class);
                    self.putExtra("CategoryParent", c.getString(c.getColumnIndexOrThrow("name")));
                    startActivity(self);
                    return;
                case 5:
                    if (this.tag == -1) {
                        String val = c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_GROUP));
                        if (val == null || val.length() < 1) {
                            Toast.makeText(this, (int) R.string.msg_tag_empty, 1).show();
                            return;
                        }
                        Intent self2 = new Intent(this, CategoryBrowser.class);
                        self2.putExtra("tag", c.getLong(c.getColumnIndexOrThrow(DBAdapter.KEY_ROWID)));
                        startActivity(self2);
                        return;
                    }
                    break;
            }
            Intent edit = new Intent(this, "edit".equals(this.whenSelecting) ? EditObject.class : ViewObject.class);
            edit.putExtra("objectId", Long.valueOf(c.getLong(c.getColumnIndexOrThrow(DBAdapter.KEY_ROWID))));
            startActivity(edit);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("scrollPos", this.scrollPos);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle inState) {
        if (inState != null) {
            this.scrollPos = inState.getInt("scrollPos");
        }
    }

    public boolean onSearchRequested() {
        startSearch(null, false, null, false);
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.presentationStyle == 5 && this.tag == -1) {
            MenuItem mi = menu.add(0, -4, 0, r(R.string.menu_title_add));
            mi.setIcon(17301555);
            ZapApp.compat.prepActionMenu(mi);
            return true;
        }
        for (HitSort s : HitSort.values()) {
            if (!(this.presentationStyle == 0 && (s.label == R.string.sort_changed || s.label == R.string.sort_numeral || s.label == R.string.sort_note))) {
                menu.add(0, s.ordinal(), 0, r(s.label)).setChecked(s.arg == this.sort);
            }
        }
        menu.add(0, -1, 0, (int) R.string.menu_reverse);
        switch (this.presentationStyle) {
            case 1:
            case 2:
            case DBAdapter.STATE_HIDE:
            case 5:
                if (this.prices == null) {
                    MenuItem mi2 = menu.add(0, -2, 0, r(R.string.label_prices_prisjakt));
                    mi2.setIcon((int) R.drawable.logo_prisjakt_s);
                    ZapApp.compat.prepActionMenu(mi2);
                    break;
                }
                break;
        }
        switch (this.presentationStyle) {
            case 0:
            case 2:
                MenuItem mi3 = menu.add(0, -3, 0, r(R.string.label_mail_export));
                mi3.setIcon(17301584);
                ZapApp.compat.prepActionMenu(mi3);
                break;
        }
        if (this.presentationStyle == 2) {
            MenuItem mi4 = menu.add(0, -5, 0, r(R.string.menu_title_add));
            mi4.setIcon(17301555);
            ZapApp.compat.prepActionMenu(mi4);
        }
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        for (HitSort s : HitSort.values()) {
            MenuItem mi = menu.findItem(s.ordinal());
            if (mi != null) {
                mi.setTitle(String.valueOf(r(s.label)) + (s.arg == this.sort ? " √" : ""));
            }
        }
        MenuItem mi2 = menu.findItem(-1);
        if (mi2 == null) {
            return true;
        }
        mi2.setTitle(String.valueOf(r(R.string.menu_reverse)) + (this.order.booleanValue() ? " " : " √"));
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == 16908332) {
            Intent intent = new Intent(this, ZapsInventory.class);
            intent.addFlags(67108864);
            startActivity(intent);
            return true;
        } else if (id == -5) {
            Intent intent2 = new Intent(this, EditObject.class);
            intent2.putExtra(DBAdapter.KEY_CATEGORY, this.parent);
            startActivity(intent2);
            return true;
        } else {
            if (id == -4) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                EditText editText = new EditText(this);
                builder.setTitle(R.string.menu_title_add);
                builder.setView(editText);
                builder.setNeutralButton(17039360, (DialogInterface.OnClickListener) null);
                final EditText editText2 = editText;
                builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String value = editText2.getText().toString();
                        if (value == null || value.length() != 0) {
                            Cursor old = CategoryBrowser.this.db.getOptions("name = " + DatabaseUtils.sqlEscapeString(value), null);
                            int count = old.getCount();
                            if (old != null && old.getCount() <= 0) {
                                ContentValues tagVals = new ContentValues();
                                tagVals.put("name", value);
                                if (CategoryBrowser.this.db.insertGroup(tagVals) == -1) {
                                    Toast.makeText(CategoryBrowser.this, "Failed to create new tag " + value, 1).show();
                                } else {
                                    CategoryBrowser.this.saneRequery();
                                }
                                EditObject.flushTagMap();
                            }
                        }
                    }
                });
                builder.show();
            }
            if (id == -2) {
                Intent priceIntent = getIntent();
                priceIntent.putExtra("LookupPrices", "true");
                startActivity(priceIntent);
                return true;
            } else if (id == -3) {
                String path = Environment.getExternalStorageDirectory() + "/export.xml";
                if (new XmlFileExporter(this).Export(path, this.db, String.valueOf(this.sort) + noCase() + " " + (this.order.booleanValue() ? "asc" : "desc"), this.parent)) {
                    Intent intent3 = new Intent("android.intent.action.SEND");
                    intent3.setType("message/rfc822");
                    String subject = r(R.string.app_name);
                    if (this.parent != null) {
                        subject = String.valueOf(subject) + " " + this.parent;
                    }
                    intent3.putExtra("android.intent.extra.SUBJECT", subject);
                    intent3.putExtra("android.intent.extra.TEXT", r(R.string.mail_export_body));
                    intent3.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(path)));
                    startActivity(Intent.createChooser(intent3, "Zap's SendMail"));
                    return true;
                }
                Toast.makeText(this, R.string.msg_export_failed, 1).show();
                return false;
            } else {
                if (id == -1) {
                    this.order = Boolean.valueOf(!this.order.booleanValue());
                } else {
                    HitSort[] values = HitSort.values();
                    int length = values.length;
                    for (int i = 0; i < length; i++) {
                        HitSort s = values[i];
                        if (s.ordinal() == id) {
                            this.sort = s.arg;
                        }
                    }
                }
                doQuery(this.parent);
                return true;
            }
        }
    }

    /* access modifiers changed from: private */
    public void saneRequery() {
        if (this.myList != null && !this.busy.booleanValue()) {
            doQuery(this.parent);
        }
    }

    public void onResume() {
        super.onResume();
        this.db = ZapApp.getDB(this, false);
        saneRequery();
        loadPreferences();
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (this.presentationStyle != 0 && menuInfo != null) {
            if (!(this.presentationStyle == 5 && this.tag == -1)) {
                Cursor c = (Cursor) getListAdapter().getItem(((AdapterView.AdapterContextMenuInfo) menuInfo).position);
                if (c != null) {
                    String name = c.getString(c.getColumnIndexOrThrow("name"));
                    String prisjaktid = c.getString(c.getColumnIndexOrThrow("prisjaktid"));
                    String barcode = c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_BARCODE));
                    menu.setHeaderTitle(name);
                    if ("edit".equals(this.whenSelecting)) {
                        menu.add(0, (int) ITEM_VIEW, 0, (int) R.string.label_overview);
                    } else {
                        menu.add(0, (int) ITEM_VIEW, 0, (int) R.string.label_edit);
                    }
                    menu.add(0, (int) ITEM_LEND_TO, 0, (int) R.string.label_lend_to);
                    if (prisjaktid == null) {
                        menu.add(0, 0, 0, (int) R.string.label_search_prisjakt);
                    } else {
                        menu.add(0, 1, 0, (int) R.string.label_prices_prisjakt);
                    }
                    String img = c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_IMAGE));
                    if (img != null && img.length() > 3) {
                        menu.add(0, (int) ITEM_SHOW_IMAGE, 0, (int) R.string.label_show_image);
                    }
                    menu.add(0, 3, 0, (int) R.string.label_search_google);
                    menu.add(0, 4, 0, (int) R.string.label_google_images);
                    if (barcode != null && barcode.length() > 11 && barcode.matches("[0-9]*")) {
                        menu.add(0, (int) ITEM_SEARCH_AMAZON, 0, (int) R.string.label_search_amazon);
                    }
                    menu.add(0, (int) ITEM_SHARE, 0, (int) R.string.label_share);
                } else {
                    return;
                }
            }
            menu.add(0, 2, 0, (int) R.string.label_item_delete);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onContextItemSelected(MenuItem item) {
        boolean success;
        if (this.presentationStyle == 0) {
            return true;
        }
        AdapterView.AdapterContextMenuInfo ami = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Cursor c = (Cursor) getListAdapter().getItem(ami.position);
        if (c == null) {
            return true;
        }
        String name = c.getString(c.getColumnIndexOrThrow("name"));
        String images = "";
        this.scrollPos = ami.position;
        switch (item.getItemId()) {
            case 0:
            case 1:
                Intent intent = new Intent("se.petersson.prisjakt.ZapsPrisjakt.SEARCH");
                ComponentName cn = intent.resolveActivity(getPackageManager());
                if (cn == null) {
                    Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=Prisjakt"));
                    if (intent2.resolveActivity(getPackageManager()) == null) {
                        Toast.makeText(this, R.string.error_no_market_manual_prisjakt, 1).show();
                        return true;
                    }
                    startActivity(intent2);
                    Toast.makeText(this, R.string.hint_install_prisjakt, 1).show();
                    return true;
                }
                String prisjaktid = c.getString(c.getColumnIndexOrThrow("prisjaktid"));
                intent.putExtra("se.petersson.prisjakt.itemName", "Prisjakt: " + name);
                switch (item.getItemId()) {
                    case 0:
                        intent.setAction("android.intent.action.SEARCH");
                        intent.setComponent(cn);
                        intent.putExtra("query", name);
                        break;
                    case 1:
                        intent.putExtra("se.petersson.prisjakt.ListType", "ItemDetails");
                        intent.putExtra("se.petersson.prisjakt.itemId", prisjaktid);
                        break;
                }
                startActivity(intent);
                return true;
            case 2:
                if (this.presentationStyle == 5 && this.tag == -1) {
                    success = this.db.deleteGroup(Long.valueOf(c.getLong(c.getColumnIndexOrThrow(DBAdapter.KEY_ROWID))).longValue());
                    EditObject.flushTagMap();
                } else {
                    success = this.db.deleteObject(Long.valueOf(c.getLong(c.getColumnIndexOrThrow(DBAdapter.KEY_ROWID))).longValue());
                }
                if (success) {
                    Toast.makeText(this, R.string.msg_delete_succeeded, 0).show();
                    saneRequery();
                } else {
                    Toast.makeText(this, R.string.msg_delete_failed, 1).show();
                }
                return true;
            case DBAdapter.STATE_HIDE:
                break;
            case 4:
                images = "site=images&";
                break;
            case 5:
                showWebDialog("http://books.google.com/books?vid=ISBN" + URLEncoder.encode(c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_BARCODE)).substring(3)), name);
                return true;
            case ITEM_SHOW_IMAGE /*6*/:
                EditObject.showUrl(this, c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_IMAGE)));
                return true;
            case ITEM_LEND_TO /*7*/:
                Intent pick = this.mContactAccessor.getContactPickerIntent();
                this.lendItem = c.getLong(c.getColumnIndexOrThrow(DBAdapter.KEY_ROWID));
                startActivityForResult(pick, 0);
                return true;
            case ITEM_SEARCH_AMAZON /*8*/:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=" + URLEncoder.encode(c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_BARCODE))))));
                return true;
            case ITEM_SHARE /*9*/:
                Intent intent3 = new Intent("android.intent.action.SEND");
                intent3.setType("text/plain");
                try {
                    String prisjaktid2 = c.getString(c.getColumnIndexOrThrow("prisjaktid"));
                    if (prisjaktid2 != null && prisjaktid2.length() > 0) {
                        intent3.putExtra("prisjaktid", prisjaktid2);
                    }
                    String barcode = c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_BARCODE));
                    if (barcode != null) {
                        intent3.putExtra(DBAdapter.KEY_BARCODE, barcode);
                    }
                    intent3.putExtra("android.intent.extra.SUBJECT", name);
                    intent3.putExtra("android.intent.extra.TEXT", c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_NOTE)));
                    try {
                        startActivity(Intent.createChooser(intent3, getText(R.string.label_share_via)));
                    } catch (ActivityNotFoundException ex) {
                        Toast.makeText(this, ex.getMessage(), 0).show();
                    }
                    return true;
                } catch (NullPointerException e) {
                    Toast.makeText(this, "Sorry, can't do that..", 0).show();
                    return true;
                }
            case ITEM_VIEW /*10*/:
                Intent intent4 = new Intent(this, "edit".equals(this.whenSelecting) ? ViewObject.class : EditObject.class);
                intent4.putExtra("objectId", Long.valueOf(c.getLong(c.getColumnIndexOrThrow(DBAdapter.KEY_ROWID))));
                startActivity(intent4);
                return true;
            default:
                return true;
        }
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.google.com/m/search?" + images + "q=" + URLEncoder.encode(name))));
        return true;
    }

    private void showWebDialog(String webUrl, String title2) {
        ImageView throbber = new ImageView(this);
        throbber.setImageResource(R.drawable.ic_popup_sync_1);
        RotateAnimation rotate = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 1, 0.5f);
        rotate.setDuration(600);
        rotate.setRepeatMode(1);
        rotate.setRepeatCount(-1);
        final FrameLayout fl = new FrameLayout(this);
        final WebView i = new WebView(this);
        final String safeUrl = webUrl.replace(" ", "%20");
        i.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                fl.bringChildToFront(i);
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.equals(safeUrl)) {
                    return false;
                }
                Intent i = new Intent("android.intent.action.VIEW", Uri.parse(url));
                if (i.resolveActivity(this.getPackageManager()) == null) {
                    Toast.makeText(this, String.valueOf(CategoryBrowser.this.r(R.string.error_no_url_handler)) + url, 1).show();
                    return true;
                }
                CategoryBrowser.this.startActivity(i);
                return true;
            }
        });
        i.loadUrl(safeUrl);
        i.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        fl.addView(throbber);
        throbber.startAnimation(rotate);
        fl.addView(i);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title2);
        builder.setView(fl);
        builder.setNeutralButton((CharSequence) null, (DialogInterface.OnClickListener) null);
        builder.setPositiveButton((CharSequence) null, (DialogInterface.OnClickListener) null);
        final AlertDialog d = builder.show();
        i.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                int msg = R.string.msg_network_error;
                if (errorCode == 404) {
                    msg = R.string.msg_google_book_not_found;
                }
                Toast.makeText(this, msg, 1).show();
                d.cancel();
            }
        });
    }

    /* access modifiers changed from: private */
    public String r(int id) {
        return getResources().getString(id);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        loadPreferences();
    }
}
