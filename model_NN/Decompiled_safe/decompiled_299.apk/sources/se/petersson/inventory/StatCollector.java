package se.petersson.inventory;

import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

public class StatCollector {
    public static int calls = 0;
    public static Location ll;
    private static PrisjaktLookup pjl = new PrisjaktLookup();
    private static int sent = 0;

    public static void pushStats(Context context, String owner, String version) {
        if (calls != 0 || sent <= 0) {
            String peek = "&device=" + enc(Build.DEVICE) + "&finger=" + enc(Build.FINGERPRINT) + "&version=" + version;
            if (ll != null) {
                peek = String.valueOf(peek) + "&lat=" + ll.getLatitude() + "&long=" + ll.getLongitude() + "&acc=" + ll.getAccuracy();
            }
            if (owner != null) {
                peek = String.valueOf(peek) + "&owner=" + enc(owner);
            }
            TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService("phone");
            String imsi = mTelephonyMgr.getSubscriberId();
            String imei = mTelephonyMgr.getDeviceId();
            final PrisjaktLookup pjl2 = new PrisjaktLookup();
            final String info = "imsi=" + enc(imsi) + "&imei=" + enc(imei) + "&calls=" + calls + peek;
            new Thread() {
                public void run() {
                    PrisjaktLookup.this.phoneHome(info, "");
                }
            }.start();
            sent++;
        }
    }

    public static void badBarcode(Context context, String barcode, String owner) {
        String peek = "&device=" + Build.DEVICE + "&finger=" + Build.FINGERPRINT;
        if (ll != null) {
            peek = String.valueOf(peek) + "&lat=" + ll.getLatitude() + "&long=" + ll.getLongitude() + "&acc=" + ll.getAccuracy();
        }
        if (owner != null) {
            peek = String.valueOf(peek) + "&owner=" + enc(owner);
        }
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService("phone");
        final String info = "imsi=" + enc(mTelephonyMgr.getSubscriberId()) + "&imei=" + enc(mTelephonyMgr.getDeviceId()) + "&barcode=" + enc(barcode) + peek;
        new Thread() {
            public void run() {
                new PrisjaktLookup().phoneHome(info, "");
            }
        }.start();
    }

    private static String enc(String str) {
        if (str == null) {
            return "";
        }
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.e("enc", "Bad encoding: " + e.getMessage());
            return "";
        }
    }

    public static void communityBarcodeStore(Context context, String barcode, String name, String category, String owner) {
        final String info = String.valueOf("&barcode=" + enc(barcode) + "&name=" + enc(name) + "&category=" + enc(category) + "&account=" + enc(owner)) + "&imei=" + enc(((TelephonyManager) context.getSystemService("phone")).getDeviceId());
        new Thread() {
            public void run() {
                new PrisjaktLookup().community("action=store" + info);
            }
        }.start();
    }

    public static List<Map<String, String>> communityBarcodeLookup(Context context, String barcode, String owner) {
        return pjl.community("action=lookup" + (String.valueOf("&barcode=" + enc(barcode) + "&account=" + enc(owner)) + "&imei=" + enc(((TelephonyManager) context.getSystemService("phone")).getDeviceId())));
    }
}
