package se.petersson.inventory;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;

public abstract class ContactAccessor {
    private static ContactAccessor sInstance;

    public abstract View getContactBadge(Context context, Uri uri, ViewGroup viewGroup);

    public abstract Intent getContactPickerIntent();

    public abstract String getNameIndex();

    public static ContactAccessor getInstance() {
        String className;
        if (sInstance == null) {
            if (Integer.parseInt(Build.VERSION.SDK) < 5) {
                className = "se.petersson.inventory.ContactAccessorOldApi";
            } else {
                className = "se.petersson.inventory.ContactAccessorNewApi";
            }
            try {
                sInstance = (ContactAccessor) Class.forName(className).asSubclass(ContactAccessor.class).newInstance();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return sInstance;
    }
}
