package se.petersson.inventory;

import java.io.ByteArrayOutputStream;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class LikePHP {
    private static /* synthetic */ int[] $SWITCH_TABLE$se$petersson$inventory$LikePHP$hash_alg;
    private static char[] map1 = new char[64];

    public enum hash_alg {
        md5,
        sha1,
        sha256,
        sha384,
        sha512
    }

    static /* synthetic */ int[] $SWITCH_TABLE$se$petersson$inventory$LikePHP$hash_alg() {
        int[] iArr = $SWITCH_TABLE$se$petersson$inventory$LikePHP$hash_alg;
        if (iArr == null) {
            iArr = new int[hash_alg.values().length];
            try {
                iArr[hash_alg.md5.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[hash_alg.sha1.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[hash_alg.sha256.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[hash_alg.sha384.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[hash_alg.sha512.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            $SWITCH_TABLE$se$petersson$inventory$LikePHP$hash_alg = iArr;
        }
        return iArr;
    }

    public static String hash_hmac(hash_alg alg, String plaintext, String salt) throws Exception {
        String algorithm;
        switch ($SWITCH_TABLE$se$petersson$inventory$LikePHP$hash_alg()[alg.ordinal()]) {
            case 1:
                algorithm = "HmacMD5";
                break;
            case 2:
                algorithm = "HmacSHA1";
                break;
            case DBAdapter.STATE_HIDE:
                algorithm = "HmacSHA256";
                break;
            case 4:
                algorithm = "HmacSHA384";
                break;
            case 5:
                algorithm = "HmacSHA512";
                break;
            default:
                algorithm = "invalid";
                break;
        }
        SecretKeySpec secretKeySpec = new SecretKeySpec(salt.getBytes(), algorithm);
        Mac mac = Mac.getInstance(algorithm);
        mac.init(secretKeySpec);
        return new String(bin2hex(mac.doFinal(plaintext.getBytes())));
    }

    public static byte[] hash_hmac_raw(hash_alg alg, String plaintext, String salt) throws Exception {
        String algorithm;
        switch ($SWITCH_TABLE$se$petersson$inventory$LikePHP$hash_alg()[alg.ordinal()]) {
            case 1:
                algorithm = "HmacMD5";
                break;
            case 2:
                algorithm = "HmacSHA1";
                break;
            case DBAdapter.STATE_HIDE:
                algorithm = "HmacSHA256";
                break;
            case 4:
                algorithm = "HmacSHA384";
                break;
            case 5:
                algorithm = "HmacSHA512";
                break;
            default:
                algorithm = "invalid";
                break;
        }
        SecretKeySpec secretKeySpec = new SecretKeySpec(salt.getBytes(), algorithm);
        Mac mac = Mac.getInstance(algorithm);
        mac.init(secretKeySpec);
        return mac.doFinal(plaintext.getBytes());
    }

    public static String bin2hex(byte[] raw) {
        return HexUtils.convert(raw);
    }

    public static class HexUtils {
        public static final int[] DEC;

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: int[]} */
        /* JADX WARNING: Multi-variable type inference failed */
        static {
            /*
                r7 = 13
                r6 = 12
                r5 = 11
                r4 = 10
                r3 = -1
                r0 = 256(0x100, float:3.59E-43)
                int[] r0 = new int[r0]
                r1 = 0
                r0[r1] = r3
                r1 = 1
                r0[r1] = r3
                r1 = 2
                r0[r1] = r3
                r1 = 3
                r0[r1] = r3
                r1 = 4
                r0[r1] = r3
                r1 = 5
                r0[r1] = r3
                r1 = 6
                r0[r1] = r3
                r1 = 7
                r0[r1] = r3
                r1 = 8
                r0[r1] = r3
                r1 = 9
                r0[r1] = r3
                r0[r4] = r3
                r0[r5] = r3
                r0[r6] = r3
                r0[r7] = r3
                r1 = 14
                r0[r1] = r3
                r1 = 15
                r0[r1] = r3
                r1 = 16
                r0[r1] = r3
                r1 = 17
                r0[r1] = r3
                r1 = 18
                r0[r1] = r3
                r1 = 19
                r0[r1] = r3
                r1 = 20
                r0[r1] = r3
                r1 = 21
                r0[r1] = r3
                r1 = 22
                r0[r1] = r3
                r1 = 23
                r0[r1] = r3
                r1 = 24
                r0[r1] = r3
                r1 = 25
                r0[r1] = r3
                r1 = 26
                r0[r1] = r3
                r1 = 27
                r0[r1] = r3
                r1 = 28
                r0[r1] = r3
                r1 = 29
                r0[r1] = r3
                r1 = 30
                r0[r1] = r3
                r1 = 31
                r0[r1] = r3
                r1 = 32
                r0[r1] = r3
                r1 = 33
                r0[r1] = r3
                r1 = 34
                r0[r1] = r3
                r1 = 35
                r0[r1] = r3
                r1 = 36
                r0[r1] = r3
                r1 = 37
                r0[r1] = r3
                r1 = 38
                r0[r1] = r3
                r1 = 39
                r0[r1] = r3
                r1 = 40
                r0[r1] = r3
                r1 = 41
                r0[r1] = r3
                r1 = 42
                r0[r1] = r3
                r1 = 43
                r0[r1] = r3
                r1 = 44
                r0[r1] = r3
                r1 = 45
                r0[r1] = r3
                r1 = 46
                r0[r1] = r3
                r1 = 47
                r0[r1] = r3
                r1 = 49
                r2 = 1
                r0[r1] = r2
                r1 = 50
                r2 = 2
                r0[r1] = r2
                r1 = 51
                r2 = 3
                r0[r1] = r2
                r1 = 52
                r2 = 4
                r0[r1] = r2
                r1 = 53
                r2 = 5
                r0[r1] = r2
                r1 = 54
                r2 = 6
                r0[r1] = r2
                r1 = 55
                r2 = 7
                r0[r1] = r2
                r1 = 56
                r2 = 8
                r0[r1] = r2
                r1 = 57
                r2 = 9
                r0[r1] = r2
                r1 = 58
                r0[r1] = r3
                r1 = 59
                r0[r1] = r3
                r1 = 60
                r0[r1] = r3
                r1 = 61
                r0[r1] = r3
                r1 = 62
                r0[r1] = r3
                r1 = 63
                r0[r1] = r3
                r1 = 64
                r0[r1] = r3
                r1 = 65
                r0[r1] = r4
                r1 = 66
                r0[r1] = r5
                r1 = 67
                r0[r1] = r6
                r1 = 68
                r0[r1] = r7
                r1 = 69
                r2 = 14
                r0[r1] = r2
                r1 = 70
                r2 = 15
                r0[r1] = r2
                r1 = 71
                r0[r1] = r3
                r1 = 72
                r0[r1] = r3
                r1 = 73
                r0[r1] = r3
                r1 = 74
                r0[r1] = r3
                r1 = 75
                r0[r1] = r3
                r1 = 76
                r0[r1] = r3
                r1 = 77
                r0[r1] = r3
                r1 = 78
                r0[r1] = r3
                r1 = 79
                r0[r1] = r3
                r1 = 80
                r0[r1] = r3
                r1 = 81
                r0[r1] = r3
                r1 = 82
                r0[r1] = r3
                r1 = 83
                r0[r1] = r3
                r1 = 84
                r0[r1] = r3
                r1 = 85
                r0[r1] = r3
                r1 = 86
                r0[r1] = r3
                r1 = 87
                r0[r1] = r3
                r1 = 88
                r0[r1] = r3
                r1 = 89
                r0[r1] = r3
                r1 = 90
                r0[r1] = r3
                r1 = 91
                r0[r1] = r3
                r1 = 92
                r0[r1] = r3
                r1 = 93
                r0[r1] = r3
                r1 = 94
                r0[r1] = r3
                r1 = 95
                r0[r1] = r3
                r1 = 96
                r0[r1] = r3
                r1 = 97
                r0[r1] = r4
                r1 = 98
                r0[r1] = r5
                r1 = 99
                r0[r1] = r6
                r1 = 100
                r0[r1] = r7
                r1 = 101(0x65, float:1.42E-43)
                r2 = 14
                r0[r1] = r2
                r1 = 102(0x66, float:1.43E-43)
                r2 = 15
                r0[r1] = r2
                r1 = 103(0x67, float:1.44E-43)
                r0[r1] = r3
                r1 = 104(0x68, float:1.46E-43)
                r0[r1] = r3
                r1 = 105(0x69, float:1.47E-43)
                r0[r1] = r3
                r1 = 106(0x6a, float:1.49E-43)
                r0[r1] = r3
                r1 = 107(0x6b, float:1.5E-43)
                r0[r1] = r3
                r1 = 108(0x6c, float:1.51E-43)
                r0[r1] = r3
                r1 = 109(0x6d, float:1.53E-43)
                r0[r1] = r3
                r1 = 110(0x6e, float:1.54E-43)
                r0[r1] = r3
                r1 = 111(0x6f, float:1.56E-43)
                r0[r1] = r3
                r1 = 112(0x70, float:1.57E-43)
                r0[r1] = r3
                r1 = 113(0x71, float:1.58E-43)
                r0[r1] = r3
                r1 = 114(0x72, float:1.6E-43)
                r0[r1] = r3
                r1 = 115(0x73, float:1.61E-43)
                r0[r1] = r3
                r1 = 116(0x74, float:1.63E-43)
                r0[r1] = r3
                r1 = 117(0x75, float:1.64E-43)
                r0[r1] = r3
                r1 = 118(0x76, float:1.65E-43)
                r0[r1] = r3
                r1 = 119(0x77, float:1.67E-43)
                r0[r1] = r3
                r1 = 120(0x78, float:1.68E-43)
                r0[r1] = r3
                r1 = 121(0x79, float:1.7E-43)
                r0[r1] = r3
                r1 = 122(0x7a, float:1.71E-43)
                r0[r1] = r3
                r1 = 123(0x7b, float:1.72E-43)
                r0[r1] = r3
                r1 = 124(0x7c, float:1.74E-43)
                r0[r1] = r3
                r1 = 125(0x7d, float:1.75E-43)
                r0[r1] = r3
                r1 = 126(0x7e, float:1.77E-43)
                r0[r1] = r3
                r1 = 127(0x7f, float:1.78E-43)
                r0[r1] = r3
                r1 = 128(0x80, float:1.794E-43)
                r0[r1] = r3
                r1 = 129(0x81, float:1.81E-43)
                r0[r1] = r3
                r1 = 130(0x82, float:1.82E-43)
                r0[r1] = r3
                r1 = 131(0x83, float:1.84E-43)
                r0[r1] = r3
                r1 = 132(0x84, float:1.85E-43)
                r0[r1] = r3
                r1 = 133(0x85, float:1.86E-43)
                r0[r1] = r3
                r1 = 134(0x86, float:1.88E-43)
                r0[r1] = r3
                r1 = 135(0x87, float:1.89E-43)
                r0[r1] = r3
                r1 = 136(0x88, float:1.9E-43)
                r0[r1] = r3
                r1 = 137(0x89, float:1.92E-43)
                r0[r1] = r3
                r1 = 138(0x8a, float:1.93E-43)
                r0[r1] = r3
                r1 = 139(0x8b, float:1.95E-43)
                r0[r1] = r3
                r1 = 140(0x8c, float:1.96E-43)
                r0[r1] = r3
                r1 = 141(0x8d, float:1.98E-43)
                r0[r1] = r3
                r1 = 142(0x8e, float:1.99E-43)
                r0[r1] = r3
                r1 = 143(0x8f, float:2.0E-43)
                r0[r1] = r3
                r1 = 144(0x90, float:2.02E-43)
                r0[r1] = r3
                r1 = 145(0x91, float:2.03E-43)
                r0[r1] = r3
                r1 = 146(0x92, float:2.05E-43)
                r0[r1] = r3
                r1 = 147(0x93, float:2.06E-43)
                r0[r1] = r3
                r1 = 148(0x94, float:2.07E-43)
                r0[r1] = r3
                r1 = 149(0x95, float:2.09E-43)
                r0[r1] = r3
                r1 = 150(0x96, float:2.1E-43)
                r0[r1] = r3
                r1 = 151(0x97, float:2.12E-43)
                r0[r1] = r3
                r1 = 152(0x98, float:2.13E-43)
                r0[r1] = r3
                r1 = 153(0x99, float:2.14E-43)
                r0[r1] = r3
                r1 = 154(0x9a, float:2.16E-43)
                r0[r1] = r3
                r1 = 155(0x9b, float:2.17E-43)
                r0[r1] = r3
                r1 = 156(0x9c, float:2.19E-43)
                r0[r1] = r3
                r1 = 157(0x9d, float:2.2E-43)
                r0[r1] = r3
                r1 = 158(0x9e, float:2.21E-43)
                r0[r1] = r3
                r1 = 159(0x9f, float:2.23E-43)
                r0[r1] = r3
                r1 = 160(0xa0, float:2.24E-43)
                r0[r1] = r3
                r1 = 161(0xa1, float:2.26E-43)
                r0[r1] = r3
                r1 = 162(0xa2, float:2.27E-43)
                r0[r1] = r3
                r1 = 163(0xa3, float:2.28E-43)
                r0[r1] = r3
                r1 = 164(0xa4, float:2.3E-43)
                r0[r1] = r3
                r1 = 165(0xa5, float:2.31E-43)
                r0[r1] = r3
                r1 = 166(0xa6, float:2.33E-43)
                r0[r1] = r3
                r1 = 167(0xa7, float:2.34E-43)
                r0[r1] = r3
                r1 = 168(0xa8, float:2.35E-43)
                r0[r1] = r3
                r1 = 169(0xa9, float:2.37E-43)
                r0[r1] = r3
                r1 = 170(0xaa, float:2.38E-43)
                r0[r1] = r3
                r1 = 171(0xab, float:2.4E-43)
                r0[r1] = r3
                r1 = 172(0xac, float:2.41E-43)
                r0[r1] = r3
                r1 = 173(0xad, float:2.42E-43)
                r0[r1] = r3
                r1 = 174(0xae, float:2.44E-43)
                r0[r1] = r3
                r1 = 175(0xaf, float:2.45E-43)
                r0[r1] = r3
                r1 = 176(0xb0, float:2.47E-43)
                r0[r1] = r3
                r1 = 177(0xb1, float:2.48E-43)
                r0[r1] = r3
                r1 = 178(0xb2, float:2.5E-43)
                r0[r1] = r3
                r1 = 179(0xb3, float:2.51E-43)
                r0[r1] = r3
                r1 = 180(0xb4, float:2.52E-43)
                r0[r1] = r3
                r1 = 181(0xb5, float:2.54E-43)
                r0[r1] = r3
                r1 = 182(0xb6, float:2.55E-43)
                r0[r1] = r3
                r1 = 183(0xb7, float:2.56E-43)
                r0[r1] = r3
                r1 = 184(0xb8, float:2.58E-43)
                r0[r1] = r3
                r1 = 185(0xb9, float:2.59E-43)
                r0[r1] = r3
                r1 = 186(0xba, float:2.6E-43)
                r0[r1] = r3
                r1 = 187(0xbb, float:2.62E-43)
                r0[r1] = r3
                r1 = 188(0xbc, float:2.63E-43)
                r0[r1] = r3
                r1 = 189(0xbd, float:2.65E-43)
                r0[r1] = r3
                r1 = 190(0xbe, float:2.66E-43)
                r0[r1] = r3
                r1 = 191(0xbf, float:2.68E-43)
                r0[r1] = r3
                r1 = 192(0xc0, float:2.69E-43)
                r0[r1] = r3
                r1 = 193(0xc1, float:2.7E-43)
                r0[r1] = r3
                r1 = 194(0xc2, float:2.72E-43)
                r0[r1] = r3
                r1 = 195(0xc3, float:2.73E-43)
                r0[r1] = r3
                r1 = 196(0xc4, float:2.75E-43)
                r0[r1] = r3
                r1 = 197(0xc5, float:2.76E-43)
                r0[r1] = r3
                r1 = 198(0xc6, float:2.77E-43)
                r0[r1] = r3
                r1 = 199(0xc7, float:2.79E-43)
                r0[r1] = r3
                r1 = 200(0xc8, float:2.8E-43)
                r0[r1] = r3
                r1 = 201(0xc9, float:2.82E-43)
                r0[r1] = r3
                r1 = 202(0xca, float:2.83E-43)
                r0[r1] = r3
                r1 = 203(0xcb, float:2.84E-43)
                r0[r1] = r3
                r1 = 204(0xcc, float:2.86E-43)
                r0[r1] = r3
                r1 = 205(0xcd, float:2.87E-43)
                r0[r1] = r3
                r1 = 206(0xce, float:2.89E-43)
                r0[r1] = r3
                r1 = 207(0xcf, float:2.9E-43)
                r0[r1] = r3
                r1 = 208(0xd0, float:2.91E-43)
                r0[r1] = r3
                r1 = 209(0xd1, float:2.93E-43)
                r0[r1] = r3
                r1 = 210(0xd2, float:2.94E-43)
                r0[r1] = r3
                r1 = 211(0xd3, float:2.96E-43)
                r0[r1] = r3
                r1 = 212(0xd4, float:2.97E-43)
                r0[r1] = r3
                r1 = 213(0xd5, float:2.98E-43)
                r0[r1] = r3
                r1 = 214(0xd6, float:3.0E-43)
                r0[r1] = r3
                r1 = 215(0xd7, float:3.01E-43)
                r0[r1] = r3
                r1 = 216(0xd8, float:3.03E-43)
                r0[r1] = r3
                r1 = 217(0xd9, float:3.04E-43)
                r0[r1] = r3
                r1 = 218(0xda, float:3.05E-43)
                r0[r1] = r3
                r1 = 219(0xdb, float:3.07E-43)
                r0[r1] = r3
                r1 = 220(0xdc, float:3.08E-43)
                r0[r1] = r3
                r1 = 221(0xdd, float:3.1E-43)
                r0[r1] = r3
                r1 = 222(0xde, float:3.11E-43)
                r0[r1] = r3
                r1 = 223(0xdf, float:3.12E-43)
                r0[r1] = r3
                r1 = 224(0xe0, float:3.14E-43)
                r0[r1] = r3
                r1 = 225(0xe1, float:3.15E-43)
                r0[r1] = r3
                r1 = 226(0xe2, float:3.17E-43)
                r0[r1] = r3
                r1 = 227(0xe3, float:3.18E-43)
                r0[r1] = r3
                r1 = 228(0xe4, float:3.2E-43)
                r0[r1] = r3
                r1 = 229(0xe5, float:3.21E-43)
                r0[r1] = r3
                r1 = 230(0xe6, float:3.22E-43)
                r0[r1] = r3
                r1 = 231(0xe7, float:3.24E-43)
                r0[r1] = r3
                r1 = 232(0xe8, float:3.25E-43)
                r0[r1] = r3
                r1 = 233(0xe9, float:3.27E-43)
                r0[r1] = r3
                r1 = 234(0xea, float:3.28E-43)
                r0[r1] = r3
                r1 = 235(0xeb, float:3.3E-43)
                r0[r1] = r3
                r1 = 236(0xec, float:3.31E-43)
                r0[r1] = r3
                r1 = 237(0xed, float:3.32E-43)
                r0[r1] = r3
                r1 = 238(0xee, float:3.34E-43)
                r0[r1] = r3
                r1 = 239(0xef, float:3.35E-43)
                r0[r1] = r3
                r1 = 240(0xf0, float:3.36E-43)
                r0[r1] = r3
                r1 = 241(0xf1, float:3.38E-43)
                r0[r1] = r3
                r1 = 242(0xf2, float:3.39E-43)
                r0[r1] = r3
                r1 = 243(0xf3, float:3.4E-43)
                r0[r1] = r3
                r1 = 244(0xf4, float:3.42E-43)
                r0[r1] = r3
                r1 = 245(0xf5, float:3.43E-43)
                r0[r1] = r3
                r1 = 246(0xf6, float:3.45E-43)
                r0[r1] = r3
                r1 = 247(0xf7, float:3.46E-43)
                r0[r1] = r3
                r1 = 248(0xf8, float:3.48E-43)
                r0[r1] = r3
                r1 = 249(0xf9, float:3.49E-43)
                r0[r1] = r3
                r1 = 250(0xfa, float:3.5E-43)
                r0[r1] = r3
                r1 = 251(0xfb, float:3.52E-43)
                r0[r1] = r3
                r1 = 252(0xfc, float:3.53E-43)
                r0[r1] = r3
                r1 = 253(0xfd, float:3.55E-43)
                r0[r1] = r3
                r1 = 254(0xfe, float:3.56E-43)
                r0[r1] = r3
                r1 = 255(0xff, float:3.57E-43)
                r0[r1] = r3
                se.petersson.inventory.LikePHP.HexUtils.DEC = r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: se.petersson.inventory.LikePHP.HexUtils.<clinit>():void");
        }

        public static byte[] convert(String digits) {
            byte b;
            int i;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            for (int i2 = 0; i2 < digits.length(); i2 += 2) {
                char c1 = digits.charAt(i2);
                if (i2 + 1 >= digits.length()) {
                    throw new IllegalArgumentException("hexUtil.odd");
                }
                char c2 = digits.charAt(i2 + 1);
                if (c1 >= '0' && c1 <= '9') {
                    b = (byte) (((c1 - '0') * 16) + 0);
                } else if (c1 >= 'a' && c1 <= 'f') {
                    b = (byte) ((((c1 - 'a') + 10) * 16) + 0);
                } else if (c1 < 'A' || c1 > 'F') {
                    throw new IllegalArgumentException("hexUtil.bad");
                } else {
                    b = (byte) ((((c1 - 'A') + 10) * 16) + 0);
                }
                if (c2 >= '0' && c2 <= '9') {
                    i = c2 - '0';
                } else if (c2 >= 'a' && c2 <= 'f') {
                    i = (c2 - 'a') + 10;
                } else if (c2 < 'A' || c2 > 'F') {
                    throw new IllegalArgumentException("hexUtil.bad");
                } else {
                    i = (c2 - 'A') + 10;
                }
                baos.write((byte) (i + b));
            }
            return baos.toByteArray();
        }

        public static String convert(byte[] bytes) {
            StringBuffer sb = new StringBuffer(bytes.length * 2);
            for (int i = 0; i < bytes.length; i++) {
                sb.append(convertDigit(bytes[i] >> 4));
                sb.append(convertDigit(bytes[i] & 15));
            }
            return sb.toString();
        }

        public static int convert2Int(byte[] hex) {
            if (hex.length < 4) {
                return 0;
            }
            if (DEC[hex[0]] < 0) {
                throw new IllegalArgumentException("hexUtil.bad");
            }
            int len = DEC[hex[0]] << 4;
            if (DEC[hex[1]] < 0) {
                throw new IllegalArgumentException("hexUtil.bad");
            }
            int len2 = (len + DEC[hex[1]]) << 4;
            if (DEC[hex[2]] < 0) {
                throw new IllegalArgumentException("hexUtil.bad");
            }
            int len3 = (len2 + DEC[hex[2]]) << 4;
            if (DEC[hex[3]] >= 0) {
                return len3 + DEC[hex[3]];
            }
            throw new IllegalArgumentException("hexUtil.bad");
        }

        private static char convertDigit(int value) {
            int value2 = value & 15;
            if (value2 >= 10) {
                return (char) ((value2 - 10) + 97);
            }
            return (char) (value2 + 48);
        }
    }

    static {
        int i;
        int i2 = 0;
        char c = 'A';
        while (true) {
            i = i2;
            if (c > 'Z') {
                break;
            }
            i2 = i + 1;
            map1[i] = c;
            c = (char) (c + 1);
        }
        char c2 = 'a';
        while (c2 <= 'z') {
            map1[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = '0';
        while (c3 <= '9') {
            map1[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        int i3 = i + 1;
        map1[i] = '+';
        int i4 = i3 + 1;
        map1[i3] = '/';
    }

    public static char[] base64_encode(String in) {
        return base64_encode(in.getBytes());
    }

    /* JADX INFO: Multiple debug info for r0v8 int: [D('i0' int), D('o1' int)] */
    /* JADX INFO: Multiple debug info for r1v6 int: [D('i1' int), D('o2' int)] */
    /* JADX INFO: Multiple debug info for r2v9 int: [D('o3' int), D('i2' int)] */
    public static char[] base64_encode(byte[] in) {
        int ip;
        int i1;
        int ip2;
        int iLen = in.length;
        int oDataLen = ((iLen * 4) + 2) / 3;
        char[] out = new char[(((iLen + 2) / 3) * 4)];
        int iEnd = 0 + iLen;
        int op = 0;
        int op2 = 0;
        while (op2 < iEnd) {
            int ip3 = op2 + 1;
            int i0 = in[op2] & 255;
            if (ip3 < iEnd) {
                int ip4 = ip3 + 1;
                i1 = in[ip3] & 255;
                ip = ip4;
            } else {
                ip = ip3;
                i1 = 0;
            }
            if (ip < iEnd) {
                int i = in[ip] & 255;
                ip++;
                ip2 = i;
            } else {
                ip2 = 0;
            }
            int o0 = i0 >>> 2;
            int o1 = ((i0 & 3) << 4) | (i1 >>> 4);
            int o2 = ((i1 & 15) << 2) | (ip2 >>> 6);
            int i2 = ip2 & 63;
            int op3 = op + 1;
            out[op] = map1[o0];
            int op4 = op3 + 1;
            out[op3] = map1[o1];
            out[op4] = op4 < oDataLen ? map1[o2] : '=';
            int op5 = op4 + 1;
            out[op5] = op5 < oDataLen ? map1[i2] : '=';
            op = op5 + 1;
            op2 = ip;
        }
        return out;
    }
}
