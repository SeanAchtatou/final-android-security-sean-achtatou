package se.petersson.inventory;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Contacts;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

public class ContactAccessorOldApi extends ContactAccessor {
    public Intent getContactPickerIntent() {
        return new Intent("android.intent.action.PICK", Contacts.People.CONTENT_URI);
    }

    public String getNameIndex() {
        return "name";
    }

    public View getContactBadge(Context ctx, Uri contact, ViewGroup parent) {
        final Activity myAct = (Activity) ctx;
        Button b = new Button(ctx);
        String name = null;
        Cursor c = myAct.getContentResolver().query(contact, null, null, null, null);
        if (c != null) {
            if (c.moveToFirst()) {
                name = c.getString(c.getColumnIndexOrThrow(getNameIndex()));
            }
            c.close();
        }
        if (name == null) {
            name = String.valueOf(contact.toString()) + " no longer exists";
        }
        b.setCompoundDrawablesWithIntrinsicBounds(17301652, 0, 0, 0);
        b.setGravity(16);
        b.setCompoundDrawablePadding(10);
        b.setText(name);
        b.setTag(contact);
        b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    myAct.startActivity(new Intent("android.intent.action.VIEW", (Uri) v.getTag()));
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(myAct, e.getMessage(), 1).show();
                }
            }
        });
        return b;
    }
}
