package se.petersson.inventory;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;

public class MaintenanceActivity extends Activity {
    /* access modifiers changed from: private */
    public String TAG = "Maintenance";
    private Button clearDbButton;
    private Button exportDbToSdButton;
    private Button importDbFromSdButton;
    private Button tidyCategoriesButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.maintenance);
        this.exportDbToSdButton = (Button) findViewById(R.id.exportdbtosdbutton);
        this.exportDbToSdButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i(MaintenanceActivity.this.TAG, "exporting database to external storage");
                new AlertDialog.Builder(MaintenanceActivity.this).setMessage((int) R.string.confirm_backup_overwrite).setPositiveButton((int) R.string.confirm_yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (MaintenanceActivity.this.isExternalStorageAvail()) {
                            Log.i(MaintenanceActivity.this.TAG, "importing database from external storage, and resetting database");
                            new ExportDatabaseTask(MaintenanceActivity.this, null).execute(new Void[0]);
                            MaintenanceActivity.this.finish();
                            return;
                        }
                        Toast.makeText(MaintenanceActivity.this, (int) R.string.msg_export_no_storage, 0).show();
                    }
                }).setNegativeButton((int) R.string.confirm_no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show();
            }
        });
        this.importDbFromSdButton = (Button) findViewById(R.id.importdbfromsdbutton);
        this.importDbFromSdButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new AlertDialog.Builder(MaintenanceActivity.this).setMessage((int) R.string.confirm_database_overwrite).setPositiveButton((int) R.string.confirm_yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (MaintenanceActivity.this.isExternalStorageAvail()) {
                            Log.i(MaintenanceActivity.this.TAG, "importing database from external storage, and resetting database");
                            new ImportDatabaseTask(MaintenanceActivity.this, null).execute(new Void[0]);
                            SystemClock.sleep(1000);
                            MaintenanceActivity.this.finish();
                            return;
                        }
                        Toast.makeText(MaintenanceActivity.this, (int) R.string.msg_import_no_storage, 0).show();
                    }
                }).setNegativeButton((int) R.string.confirm_no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show();
            }
        });
        this.importDbFromSdButton.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
                new AlertDialog.Builder(MaintenanceActivity.this).setMessage((int) R.string.confirm_merge_database).setPositiveButton((int) R.string.confirm_yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (MaintenanceActivity.this.isExternalStorageAvail()) {
                            Log.i(MaintenanceActivity.this.TAG, "merge database from external storage");
                            new MergeDatabaseTask(MaintenanceActivity.this, null).execute(new Void[0]);
                            return;
                        }
                        Toast.makeText(MaintenanceActivity.this, (int) R.string.msg_import_no_storage, 0).show();
                    }
                }).setNegativeButton((int) R.string.confirm_no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show();
                return true;
            }
        });
        this.clearDbButton = (Button) findViewById(R.id.cleardbutton);
        this.clearDbButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new AlertDialog.Builder(MaintenanceActivity.this).setMessage((int) R.string.confirm_empty_database).setPositiveButton((int) R.string.confirm_yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Log.i(MaintenanceActivity.this.TAG, "deleting database");
                        DBAdapter db = ZapApp.getDB(MaintenanceActivity.this, false);
                        if (db != null) {
                            if (db.deleteAllDataYesImSure(true)) {
                                Toast.makeText(MaintenanceActivity.this, (int) R.string.msg_data_deleted, 0).show();
                            } else {
                                Toast.makeText(MaintenanceActivity.this, (int) R.string.msg_data_delete_failed, 0).show();
                            }
                        }
                        MaintenanceActivity.this.finish();
                    }
                }).setNegativeButton((int) R.string.confirm_no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Toast.makeText(MaintenanceActivity.this, (int) R.string.msg_aborted, 0).show();
                    }
                }).show();
            }
        });
        this.tidyCategoriesButton = (Button) findViewById(R.id.tidyCbutton);
        this.tidyCategoriesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i(MaintenanceActivity.this.TAG, " removing empty categories");
                int n = ZapApp.getDB(MaintenanceActivity.this, false).flushEmptyCategories();
                Toast.makeText(MaintenanceActivity.this, MaintenanceActivity.this.getResources().getString(R.string.msg_tidy_categories, Integer.valueOf(n)), 0).show();
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean isExternalStorageAvail() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    private class ExportDatabaseTask extends AsyncTask<Void, Void, Boolean> {
        private final ProgressDialog dialog;

        private ExportDatabaseTask() {
            this.dialog = new ProgressDialog(MaintenanceActivity.this);
        }

        /* synthetic */ ExportDatabaseTask(MaintenanceActivity maintenanceActivity, ExportDatabaseTask exportDatabaseTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.dialog.setMessage(MaintenanceActivity.this.getResources().getString(R.string.title_exporting));
            this.dialog.show();
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Void... args) {
            ZapApp.closeDB(false);
            File dbFile = new File(Environment.getDataDirectory() + "/data/se.petersson.inventory/databases/" + "inventory");
            File exportDir = new File(Environment.getExternalStorageDirectory(), InventoryProvider.AUTHORITY);
            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }
            File file = new File(exportDir, dbFile.getName());
            try {
                file.createNewFile();
                MaintenanceActivity.copyFile(dbFile, file);
                return true;
            } catch (IOException e) {
                IOException e2 = e;
                Log.e(MaintenanceActivity.this.TAG, e2.getMessage(), e2);
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean success) {
            this.dialog.isShowing();
            if (success.booleanValue()) {
                Toast.makeText(MaintenanceActivity.this, (int) R.string.msg_backup_success, 0).show();
            } else {
                Toast.makeText(MaintenanceActivity.this, (int) R.string.msg_backup_failure, 0).show();
            }
        }
    }

    private class MergeDatabaseTask extends AsyncTask<Void, Void, String> {
        private final ProgressDialog dialog;

        private MergeDatabaseTask() {
            this.dialog = new ProgressDialog(MaintenanceActivity.this);
        }

        /* synthetic */ MergeDatabaseTask(MaintenanceActivity maintenanceActivity, MergeDatabaseTask mergeDatabaseTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.dialog.setMessage(MaintenanceActivity.this.getResources().getString(R.string.title_merging));
            this.dialog.setProgressStyle(0);
            this.dialog.show();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... args) {
            Cursor hit;
            File dbBackupFile = new File(Environment.getExternalStorageDirectory() + "/se.petersson.inventory/inventory");
            if (!dbBackupFile.exists()) {
                return MaintenanceActivity.this.getResources().getString(R.string.error_no_backup);
            }
            if (!dbBackupFile.canRead()) {
                return MaintenanceActivity.this.getResources().getString(R.string.error_backup_unreadable);
            }
            try {
                DBAdapter current = ZapApp.getDB(MaintenanceActivity.this, false);
                Cursor c = ZapApp.getDB(MaintenanceActivity.this, true).getObjects(null, null);
                if (c == null) {
                    return "No items in backup";
                }
                int n = c.getCount();
                for (int i = 0; i < n; i++) {
                    if (c.moveToPosition(i)) {
                        String barcode = c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_BARCODE));
                        if (barcode == null || barcode.length() <= 0) {
                            hit = current.getObjects("name = " + DatabaseUtils.sqlEscapeString(c.getString(c.getColumnIndexOrThrow("name"))), null);
                        } else {
                            hit = current.getObjects("barcode = " + DatabaseUtils.sqlEscapeString(barcode), null);
                        }
                        if (hit == null || hit.getCount() == 0) {
                            int state = c.getInt(c.getColumnIndexOrThrow("state"));
                            Map<String, String> map = new HashMap<>();
                            map.put("name", c.getString(c.getColumnIndexOrThrow("name")));
                            map.put(DBAdapter.KEY_NOTE, c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_NOTE)));
                            map.put("prisjaktid", c.getString(c.getColumnIndexOrThrow("prisjaktid")));
                            map.put(DBAdapter.KEY_IMAGE, c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_IMAGE)));
                            map.put(DBAdapter.KEY_CATEGORY, c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_CATEGORY)));
                            ZapsInventory.saveData(MaintenanceActivity.this, current, barcode, state, map, false, null, c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_NUMERAL)));
                        }
                        if (hit != null) {
                            hit.close();
                        }
                    }
                }
                c.close();
                ZapApp.closeDB(true);
                return null;
            } catch (Exception e) {
                Exception e2 = e;
                Log.e(MaintenanceActivity.this.TAG, e2.getMessage(), e2);
                return e2.getMessage();
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String errMsg) {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            if (errMsg == null) {
                Toast.makeText(MaintenanceActivity.this, (int) R.string.msg_merge_success, 0).show();
            } else {
                Toast.makeText(MaintenanceActivity.this, String.valueOf(MaintenanceActivity.this.getResources().getString(R.string.msg_merge_failure)) + ": " + errMsg, 0).show();
            }
        }
    }

    private class ImportDatabaseTask extends AsyncTask<Void, Void, String> {
        private final ProgressDialog dialog;

        private ImportDatabaseTask() {
            this.dialog = new ProgressDialog(MaintenanceActivity.this);
        }

        /* synthetic */ ImportDatabaseTask(MaintenanceActivity maintenanceActivity, ImportDatabaseTask importDatabaseTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.dialog.setMessage(MaintenanceActivity.this.getResources().getString(R.string.title_importing));
            this.dialog.setProgressStyle(0);
            this.dialog.show();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... args) {
            File dbBackupFile = new File(Environment.getExternalStorageDirectory() + "/se.petersson.inventory/inventory");
            if (!dbBackupFile.exists()) {
                return MaintenanceActivity.this.getResources().getString(R.string.error_no_backup);
            }
            if (!dbBackupFile.canRead()) {
                return MaintenanceActivity.this.getResources().getString(R.string.error_backup_unreadable);
            }
            ZapApp.closeDB(false);
            File dbFile = new File(Environment.getDataDirectory() + "/data/se.petersson.inventory/databases/" + "inventory");
            if (dbFile.exists()) {
                dbFile.delete();
            }
            try {
                ZapApp.closeDB(true);
                dbFile.createNewFile();
                MaintenanceActivity.copyFile(dbBackupFile, dbFile);
                return null;
            } catch (IOException e) {
                IOException e2 = e;
                Log.e(MaintenanceActivity.this.TAG, e2.getMessage(), e2);
                return e2.getMessage();
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String errMsg) {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            if (errMsg == null) {
                Toast.makeText(MaintenanceActivity.this, (int) R.string.msg_restore_success, 0).show();
            } else {
                Toast.makeText(MaintenanceActivity.this, String.valueOf(MaintenanceActivity.this.getResources().getString(R.string.msg_restore_failure)) + ": " + errMsg, 0).show();
            }
        }
    }

    public static void copyFile(File src, File dst) throws IOException {
        FileChannel inChannel = new FileInputStream(src).getChannel();
        FileChannel outChannel = new FileOutputStream(dst).getChannel();
        try {
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } finally {
            if (inChannel != null) {
                inChannel.close();
            }
            if (outChannel != null) {
                outChannel.close();
            }
        }
    }
}
