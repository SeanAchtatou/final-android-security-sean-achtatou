package se.petersson.inventory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public class PrisjaktLookup {
    private static String homeserver = "http://home.petersson.se/android/inventory/";
    public static String jsonserver = "http://www.prisjakt.nu/extern/extern_data.php?";
    private static String tag = "PrisjaktLookup";
    public String errMsg = null;
    public JSONObject lastJSONobject = null;

    public List<Map<String, String>> search(String query, String sort) {
        return json_lookup("output=json&action=search_prod&p=", query, sort);
    }

    public List<Map<String, String>> barcode(String query, String sort) {
        return json_lookup("output=json&action=search_prod_code&p=", query == null ? "" : query, sort);
    }

    public List<Map<String, String>> itemDetails(String query) {
        return json_lookup("output=json&action=product_prices&p=", query, null);
    }

    public List<Map<String, String>> itemHeader(String query) {
        return json_lookup("output=json&action=product_details&p=", query, null);
    }

    public List<Map<String, String>> relatedItems(String query) {
        return json_lookup("output=json&action=related_prod&p=", query, null);
    }

    public List<Map<String, String>> itemReviews(String query) {
        return json_lookup("output=json&action=user_reviews&start=0&max=50&p=", query, null);
    }

    public List<Map<String, String>> itemClosest(String query, String loc) {
        return json_lookup("output=json&action=find_stores&" + loc + "&p=", query, null);
    }

    public List<Map<String, String>> storesClosest(String query, String loc) {
        return json_lookup("output=json&action=find_stores&" + loc + "&store_id=", query, null);
    }

    public List<Map<String, String>> subCategories(String query, int start, String sort) {
        String str;
        String str2 = "output=json&action=list_category" + (start == 0 ? "" : "&start=" + start) + "&p=";
        if (query == null) {
            str = "0";
        } else {
            str = query;
        }
        return json_lookup(str2, str, sort);
    }

    public List<Map<String, String>> phoneHome(String args, String query) {
        return json_lookup(String.valueOf(homeserver) + "phoneHome.php?output=json&" + args, query, null);
    }

    public List<Map<String, String>> community(String query) {
        return json_lookup(String.valueOf(homeserver) + "community.php?" + query, "", null);
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    is.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x014a, code lost:
        r25 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x014b, code lost:
        r5 = r25;
        r1.errMsg = "State problem. Try later.\n" + r5.getMessage();
        r5.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x01ad, code lost:
        r25 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x01ae, code lost:
        r5 = r25;
        r1.errMsg = "Protocol error from server. Try later.\n" + r5.getMessage();
        r5.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0202, code lost:
        r25 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0203, code lost:
        r5 = r25;
        r1.errMsg = "Unable to contact server. Do you have a working network connection?\n" + r5.getMessage();
        r5.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0234, code lost:
        r25 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0235, code lost:
        r5 = r25;
        r1.errMsg = "Network error. Try again.\n" + r5.getMessage();
        r5.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x02ac, code lost:
        r25 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x02ad, code lost:
        r5 = r25;
        r1.errMsg = "Format error in server data. Try later.\n" + r5.getMessage();
        r5.printStackTrace();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x014a A[ExcHandler: IllegalStateException (r25v30 'e' java.lang.IllegalStateException A[CUSTOM_DECLARE]), Splitter:B:15:0x00e7] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x01ad A[ExcHandler: ClientProtocolException (r25v26 'e' org.apache.http.client.ClientProtocolException A[CUSTOM_DECLARE]), Splitter:B:15:0x00e7] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0202 A[ExcHandler: UnknownHostException (r25v22 'e' java.net.UnknownHostException A[CUSTOM_DECLARE]), Splitter:B:15:0x00e7] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0234 A[ExcHandler: IOException (r25v18 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:15:0x00e7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<java.util.Map<java.lang.String, java.lang.String>> json_lookup(java.lang.String r29, java.lang.String r30, java.lang.String r31) {
        /*
            r28 = this;
            org.apache.http.impl.client.DefaultHttpClient r10 = new org.apache.http.impl.client.DefaultHttpClient
            r10.<init>()
            java.lang.String r22 = se.petersson.inventory.PrisjaktLookup.jsonserver
            java.lang.String r25 = "http"
            r0 = r29
            r1 = r25
            boolean r25 = r0.startsWith(r1)
            if (r25 == 0) goto L_0x0114
            java.lang.String r22 = ""
        L_0x0015:
            java.lang.String r23 = ""
            java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x011c }
            java.lang.String r26 = java.lang.String.valueOf(r29)     // Catch:{ UnsupportedEncodingException -> 0x011c }
            r25.<init>(r26)     // Catch:{ UnsupportedEncodingException -> 0x011c }
            java.lang.String r26 = "UTF-8"
            r0 = r30
            r1 = r26
            java.lang.String r26 = java.net.URLEncoder.encode(r0, r1)     // Catch:{ UnsupportedEncodingException -> 0x011c }
            java.lang.StringBuilder r25 = r25.append(r26)     // Catch:{ UnsupportedEncodingException -> 0x011c }
            java.lang.String r23 = r25.toString()     // Catch:{ UnsupportedEncodingException -> 0x011c }
        L_0x0032:
            if (r31 == 0) goto L_0x004f
            java.lang.StringBuilder r25 = new java.lang.StringBuilder
            java.lang.String r26 = java.lang.String.valueOf(r23)
            r25.<init>(r26)
            java.lang.String r26 = "&sort="
            java.lang.StringBuilder r25 = r25.append(r26)
            r0 = r25
            r1 = r31
            java.lang.StringBuilder r25 = r0.append(r1)
            java.lang.String r23 = r25.toString()
        L_0x004f:
            java.lang.StringBuilder r25 = new java.lang.StringBuilder
            java.lang.String r26 = java.lang.String.valueOf(r23)
            r25.<init>(r26)
            java.lang.String r26 = "&compress=gzencode"
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r23 = r25.toString()
            org.apache.http.client.methods.HttpGet r11 = new org.apache.http.client.methods.HttpGet
            java.lang.StringBuilder r25 = new java.lang.StringBuilder
            java.lang.String r26 = java.lang.String.valueOf(r22)
            r25.<init>(r26)
            r0 = r25
            r1 = r23
            java.lang.StringBuilder r25 = r0.append(r1)
            java.lang.String r25 = r25.toString()
            r0 = r11
            r1 = r25
            r0.<init>(r1)
            se.petersson.inventory.LikePHP$hash_alg r25 = se.petersson.inventory.LikePHP.hash_alg.sha1     // Catch:{ Exception -> 0x0139 }
            java.lang.String r26 = "I1j0Qj9XS95b6ydgfad2EZpg4"
            r0 = r25
            r1 = r23
            r2 = r26
            java.lang.String r9 = se.petersson.inventory.LikePHP.hash_hmac(r0, r1, r2)     // Catch:{ Exception -> 0x0139 }
            java.lang.String r25 = r9.toString()     // Catch:{ Exception -> 0x0139 }
            char[] r8 = se.petersson.inventory.LikePHP.base64_encode(r25)     // Catch:{ Exception -> 0x0139 }
            java.lang.String r25 = "X-Prisjakt-Auth"
            java.lang.StringBuilder r26 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0139 }
            java.lang.String r27 = "25:"
            r26.<init>(r27)     // Catch:{ Exception -> 0x0139 }
            java.lang.String r27 = new java.lang.String     // Catch:{ Exception -> 0x0139 }
            r0 = r27
            r1 = r8
            r0.<init>(r1)     // Catch:{ Exception -> 0x0139 }
            java.lang.StringBuilder r26 = r26.append(r27)     // Catch:{ Exception -> 0x0139 }
            java.lang.String r26 = r26.toString()     // Catch:{ Exception -> 0x0139 }
            r0 = r11
            r1 = r25
            r2 = r26
            r0.addHeader(r1, r2)     // Catch:{ Exception -> 0x0139 }
        L_0x00b6:
            java.lang.String r25 = se.petersson.inventory.PrisjaktLookup.jsonserver
            r0 = r22
            r1 = r25
            boolean r25 = r0.contains(r1)
            if (r25 == 0) goto L_0x00e2
            java.lang.String r25 = se.petersson.inventory.PrisjaktLookup.tag
            java.lang.StringBuilder r26 = new java.lang.StringBuilder
            java.lang.String r27 = "JSON lookup: "
            r26.<init>(r27)
            r0 = r26
            r1 = r22
            java.lang.StringBuilder r26 = r0.append(r1)
            r0 = r26
            r1 = r23
            java.lang.StringBuilder r26 = r0.append(r1)
            java.lang.String r26 = r26.toString()
            android.util.Log.d(r25, r26)
        L_0x00e2:
            java.util.ArrayList r18 = new java.util.ArrayList
            r18.<init>()
            org.apache.http.HttpResponse r20 = r10.execute(r11)     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            org.apache.http.StatusLine r25 = r20.getStatusLine()     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            int r25 = r25.getStatusCode()     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            switch(r25) {
                case 200: goto L_0x016a;
                case 401: goto L_0x0141;
                default: goto L_0x00f6;
            }     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
        L_0x00f6:
            java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            java.lang.String r26 = "Unexpected server response. Try upgrading, wait a bit or read Help & Tips.\n"
            r25.<init>(r26)     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            org.apache.http.StatusLine r26 = r20.getStatusLine()     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            java.lang.String r26 = r26.toString()     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            java.lang.StringBuilder r25 = r25.append(r26)     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            java.lang.String r25 = r25.toString()     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            r0 = r25
            r1 = r28
            r1.errMsg = r0     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
        L_0x0113:
            return r18
        L_0x0114:
            int r25 = se.petersson.inventory.StatCollector.calls
            int r25 = r25 + 1
            se.petersson.inventory.StatCollector.calls = r25
            goto L_0x0015
        L_0x011c:
            r25 = move-exception
            r5 = r25
            java.lang.String r25 = se.petersson.inventory.PrisjaktLookup.tag
            java.lang.StringBuilder r26 = new java.lang.StringBuilder
            java.lang.String r27 = "Bad encoding: "
            r26.<init>(r27)
            java.lang.String r27 = r5.getMessage()
            java.lang.StringBuilder r26 = r26.append(r27)
            java.lang.String r26 = r26.toString()
            android.util.Log.e(r25, r26)
            goto L_0x0032
        L_0x0139:
            r25 = move-exception
            r6 = r25
            r6.printStackTrace()
            goto L_0x00b6
        L_0x0141:
            java.lang.String r25 = "Incorrect authentication from server. Application may need upgrade."
            r0 = r25
            r1 = r28
            r1.errMsg = r0     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            goto L_0x0113
        L_0x014a:
            r25 = move-exception
            r5 = r25
            java.lang.StringBuilder r25 = new java.lang.StringBuilder
            java.lang.String r26 = "State problem. Try later.\n"
            r25.<init>(r26)
            java.lang.String r26 = r5.getMessage()
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r25 = r25.toString()
            r0 = r25
            r1 = r28
            r1.errMsg = r0
            r5.printStackTrace()
            goto L_0x0113
        L_0x016a:
            org.apache.http.HttpEntity r7 = r20.getEntity()     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            if (r7 == 0) goto L_0x0113
            org.apache.http.HttpEntity r25 = r20.getEntity()     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            java.io.InputStream r13 = r25.getContent()     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            java.lang.String r25 = "Content-Encoding"
            r0 = r20
            r1 = r25
            org.apache.http.Header r3 = r0.getFirstHeader(r1)     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            if (r3 == 0) goto L_0x0196
            java.lang.String r25 = r3.getValue()     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            java.lang.String r26 = "gzip"
            boolean r25 = r25.equalsIgnoreCase(r26)     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            if (r25 == 0) goto L_0x0196
            java.util.zip.GZIPInputStream r14 = new java.util.zip.GZIPInputStream     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            r14.<init>(r13)     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            r13 = r14
        L_0x0196:
            java.lang.String r21 = convertStreamToString(r13)     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            r13.close()     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            int r25 = r21.length()     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            if (r25 != 0) goto L_0x01ce
            java.lang.String r25 = "Unexpected empty message from server. Probably due to maintenance - try again later."
            r0 = r25
            r1 = r28
            r1.errMsg = r0     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            goto L_0x0113
        L_0x01ad:
            r25 = move-exception
            r5 = r25
            java.lang.StringBuilder r25 = new java.lang.StringBuilder
            java.lang.String r26 = "Protocol error from server. Try later.\n"
            r25.<init>(r26)
            java.lang.String r26 = r5.getMessage()
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r25 = r25.toString()
            r0 = r25
            r1 = r28
            r1.errMsg = r0
            r5.printStackTrace()
            goto L_0x0113
        L_0x01ce:
            r25 = 0
            r0 = r21
            r1 = r25
            char r25 = r0.charAt(r1)     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            switch(r25) {
                case 91: goto L_0x0255;
                case 123: goto L_0x0223;
                default: goto L_0x01db;
            }     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
        L_0x01db:
            java.lang.String r25 = "No match found"
            r0 = r21
            r1 = r25
            boolean r25 = r0.contains(r1)     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            if (r25 != 0) goto L_0x0113
            java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            java.lang.String r26 = "Unexpected server message: "
            r25.<init>(r26)     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            r0 = r25
            r1 = r21
            java.lang.StringBuilder r25 = r0.append(r1)     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            java.lang.String r25 = r25.toString()     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            r0 = r25
            r1 = r28
            r1.errMsg = r0     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            goto L_0x0113
        L_0x0202:
            r25 = move-exception
            r5 = r25
            java.lang.StringBuilder r25 = new java.lang.StringBuilder
            java.lang.String r26 = "Unable to contact server. Do you have a working network connection?\n"
            r25.<init>(r26)
            java.lang.String r26 = r5.getMessage()
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r25 = r25.toString()
            r0 = r25
            r1 = r28
            r1.errMsg = r0
            r5.printStackTrace()
            goto L_0x0113
        L_0x0223:
            org.json.JSONObject r25 = new org.json.JSONObject     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            r0 = r25
            r1 = r21
            r0.<init>(r1)     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            r0 = r25
            r1 = r28
            r1.lastJSONobject = r0     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            goto L_0x0113
        L_0x0234:
            r25 = move-exception
            r5 = r25
            java.lang.StringBuilder r25 = new java.lang.StringBuilder
            java.lang.String r26 = "Network error. Try again.\n"
            r25.<init>(r26)
            java.lang.String r26 = r5.getMessage()
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r25 = r25.toString()
            r0 = r25
            r1 = r28
            r1.errMsg = r0
            r5.printStackTrace()
            goto L_0x0113
        L_0x0255:
            org.json.JSONArray r17 = new org.json.JSONArray     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            r0 = r17
            r1 = r21
            r0.<init>(r1)     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            r12 = 0
        L_0x025f:
            int r25 = r17.length()     // Catch:{ IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234, JSONException -> 0x02ac }
            r0 = r12
            r1 = r25
            if (r0 >= r1) goto L_0x0113
            r0 = r17
            r1 = r12
            org.json.JSONObject r16 = r0.getJSONObject(r1)     // Catch:{ JSONException -> 0x02cd, IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234 }
            org.json.JSONArray r19 = r16.names()     // Catch:{ JSONException -> 0x02cd, IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234 }
            r0 = r16
            r1 = r19
            org.json.JSONArray r24 = r0.toJSONArray(r1)     // Catch:{ JSONException -> 0x02cd, IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234 }
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ JSONException -> 0x02cd, IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234 }
            r4.<init>()     // Catch:{ JSONException -> 0x02cd, IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234 }
            r15 = 0
        L_0x0281:
            int r25 = r24.length()     // Catch:{ JSONException -> 0x02cd, IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234 }
            r0 = r15
            r1 = r25
            if (r0 < r1) goto L_0x0293
            r0 = r18
            r1 = r4
            r0.add(r1)     // Catch:{ JSONException -> 0x02cd, IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234 }
        L_0x0290:
            int r12 = r12 + 1
            goto L_0x025f
        L_0x0293:
            r0 = r19
            r1 = r15
            java.lang.String r25 = r0.getString(r1)     // Catch:{ JSONException -> 0x02cd, IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234 }
            r0 = r24
            r1 = r15
            java.lang.String r26 = r0.getString(r1)     // Catch:{ JSONException -> 0x02cd, IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234 }
            r0 = r4
            r1 = r25
            r2 = r26
            r0.put(r1, r2)     // Catch:{ JSONException -> 0x02cd, IllegalStateException -> 0x014a, ClientProtocolException -> 0x01ad, UnknownHostException -> 0x0202, IOException -> 0x0234 }
            int r15 = r15 + 1
            goto L_0x0281
        L_0x02ac:
            r25 = move-exception
            r5 = r25
            java.lang.StringBuilder r25 = new java.lang.StringBuilder
            java.lang.String r26 = "Format error in server data. Try later.\n"
            r25.<init>(r26)
            java.lang.String r26 = r5.getMessage()
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r25 = r25.toString()
            r0 = r25
            r1 = r28
            r1.errMsg = r0
            r5.printStackTrace()
            goto L_0x0113
        L_0x02cd:
            r25 = move-exception
            goto L_0x0290
        */
        throw new UnsupportedOperationException("Method not decompiled: se.petersson.inventory.PrisjaktLookup.json_lookup(java.lang.String, java.lang.String, java.lang.String):java.util.List");
    }
}
