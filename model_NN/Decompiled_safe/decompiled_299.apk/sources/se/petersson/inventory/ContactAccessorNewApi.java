package se.petersson.inventory;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.CharArrayBuffer;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.View;
import android.view.ViewGroup;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import android.widget.Toast;
import java.io.InputStream;

public class ContactAccessorNewApi extends ContactAccessor {
    static final String[] CONTACTS_SUMMARY_PROJECTION;
    static final int SUMMARY_HAS_PHONE_COLUMN_INDEX = 7;
    static final int SUMMARY_ID_COLUMN_INDEX = 0;
    static final int SUMMARY_LOOKUP_KEY = 6;
    static final int SUMMARY_NAME_COLUMN_INDEX = 1;
    static final int SUMMARY_PHOTO_ID_COLUMN_INDEX = 5;
    static final int SUMMARY_PRESENCE_STATUS_COLUMN_INDEX = 4;
    static final int SUMMARY_STARRED_COLUMN_INDEX = 2;
    static final int SUMMARY_TIMES_CONTACTED_COLUMN_INDEX = 3;

    public Intent getContactPickerIntent() {
        return new Intent("android.intent.action.PICK", ContactsContract.Contacts.CONTENT_URI);
    }

    public String getNameIndex() {
        return "display_name";
    }

    static {
        String[] strArr = new String[8];
        strArr[0] = DBAdapter.KEY_ROWID;
        strArr[1] = "display_name";
        strArr[2] = "starred";
        strArr[3] = "times_contacted";
        strArr[SUMMARY_PRESENCE_STATUS_COLUMN_INDEX] = "contact_presence";
        strArr[SUMMARY_PHOTO_ID_COLUMN_INDEX] = "photo_id";
        strArr[SUMMARY_LOOKUP_KEY] = "lookup";
        strArr[SUMMARY_HAS_PHONE_COLUMN_INDEX] = "has_phone_number";
        CONTACTS_SUMMARY_PROJECTION = strArr;
    }

    /* Debug info: failed to restart local var, previous not found, register: 22 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getContactBadge(Context ctx, Uri contact, ViewGroup parent) {
        Bitmap contactPhoto;
        try {
            Cursor c = ctx.getContentResolver().query(contact, CONTACTS_SUMMARY_PROJECTION, "((display_name NOTNULL) AND (has_phone_number=1) AND (display_name != '' ))", null, "display_name COLLATE LOCALIZED ASC");
            if (c == null || !c.moveToFirst()) {
                return null;
            }
            View view = ((Activity) ctx).getLayoutInflater().inflate(R.layout.quick_contacts, parent, false);
            ContactListItemCache cache = new ContactListItemCache();
            cache.nameView = (TextView) view.findViewById(R.id.name);
            cache.photoView = (QuickContactBadge) view.findViewById(R.id.badge);
            view.setTag(cache);
            c.copyStringToBuffer(1, cache.nameBuffer);
            cache.nameView.setText(cache.nameBuffer.data, 0, cache.nameBuffer.sizeCopied);
            long contactId = c.getLong(0);
            cache.photoView.assignContactUri(ContactsContract.Contacts.getLookupUri(contactId, c.getString(SUMMARY_LOOKUP_KEY)));
            InputStream photo = ContactsContract.Contacts.openContactPhotoInputStream(ctx.getContentResolver(), ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId));
            if (!(photo == null || (contactPhoto = BitmapFactory.decodeStream(photo)) == null)) {
                cache.photoView.setImageBitmap(contactPhoto);
            }
            return view;
        } catch (IllegalArgumentException e) {
            Toast.makeText(ctx, "Sorry, you seem to have nonstandard contacts: " + e.getMessage(), 1).show();
            return null;
        }
    }

    static final class ContactListItemCache {
        public CharArrayBuffer nameBuffer = new CharArrayBuffer(128);
        public TextView nameView;
        public QuickContactBadge photoView;

        ContactListItemCache() {
        }
    }
}
