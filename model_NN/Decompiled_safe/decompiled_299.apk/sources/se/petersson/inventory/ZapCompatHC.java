package se.petersson.inventory;

import android.app.Activity;
import android.view.MenuItem;

public class ZapCompatHC extends ZapCompat {
    public void prepActionMenu(MenuItem mi) {
        mi.setShowAsAction(5);
    }

    public void hasOptionsMenu(Activity ctx) {
        ctx.getWindow().requestFeature(8);
    }
}
