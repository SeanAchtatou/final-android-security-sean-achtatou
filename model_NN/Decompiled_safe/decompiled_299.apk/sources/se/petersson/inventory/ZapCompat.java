package se.petersson.inventory;

import android.app.Activity;
import android.os.Build;
import android.view.MenuItem;

public abstract class ZapCompat {
    private static ZapCompat sInstance;

    public abstract void hasOptionsMenu(Activity activity);

    public abstract void prepActionMenu(MenuItem menuItem);

    public static ZapCompat getInstance() {
        String className;
        if (sInstance == null) {
            if (Integer.parseInt(Build.VERSION.SDK) < 11) {
                className = "se.petersson.inventory.ZapCompatPreHC";
            } else {
                className = "se.petersson.inventory.ZapCompatHC";
            }
            try {
                sInstance = (ZapCompat) Class.forName(className).asSubclass(ZapCompat.class).newInstance();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return sInstance;
    }
}
