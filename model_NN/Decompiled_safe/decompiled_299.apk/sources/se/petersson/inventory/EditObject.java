package se.petersson.inventory;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import se.petersson.inventory.InventoryProvider;

public class EditObject extends Activity implements SharedPreferences.OnSharedPreferenceChangeListener {
    static final int DATE_DIALOG_ID = 1;
    private static final int MENU_ADD_BARCODE = 0;
    private static final int MENU_APPLY_TEMPLATE = 7;
    private static final int MENU_EDIT_BARCODE = 5;
    private static final int MENU_PRISJAKT_PRICES = 2;
    private static final int MENU_PRISJAKT_SEARCH = 3;
    private static final int MENU_REEVAL_BARCODE = 6;
    private static final int MENU_SEARCH_BARCODE = 4;
    protected static final int RES_CONTACT = 0;
    protected static final int RES_SCAN = 1;
    protected static final int RES_SELECT_PHOTO = 2;
    protected static final int RES_TAKE_PHOTO = 3;
    protected static PrisjaktLookup pjLookup = new PrisjaktLookup();
    private static Map<Long, String> tagMapper = null;
    private TextView barcode;
    private String barcodeSupport;
    /* access modifiers changed from: private */
    public CheckBox catCheckbox;
    /* access modifiers changed from: private */
    public EditText catNew;
    private Spinner category;
    /* access modifiers changed from: private */
    public Cursor categoryCursor = null;
    private TextView changed;
    private boolean communityBarcode = false;
    /* access modifiers changed from: private */
    public TextView created;
    /* access modifiers changed from: private */
    public DBAdapter db;
    private String defaultNumeral = "1";
    private String defaultOwner = null;
    private int defaultState = 1;
    DateFormat df = DateFormat.getDateTimeInstance();
    /* access modifiers changed from: private */
    public final ContactAccessor mContactAccessor = ContactAccessor.getInstance();
    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar c = Calendar.getInstance();
            c.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
            Long t = Long.valueOf(c.getTimeInMillis());
            EditObject.this.created.setTag(t);
            EditObject.this.created.setText(EditObject.this.df.format(t));
        }
    };
    final Handler mHandler = new Handler();
    private SharedPreferences mPreferences;
    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            EditObject.this.updateResultsInUi();
        }
    };
    private List<Map<String, String>> myList;
    /* access modifiers changed from: private */
    public Uri myPicture;
    /* access modifiers changed from: private */
    public EditText name;
    /* access modifiers changed from: private */
    public EditText note;
    /* access modifiers changed from: private */
    public Button numeral;
    private Cursor object = null;
    private long objectId;
    private String overrideCategory = null;
    private Button owner;
    private TextView ownerLabel;
    /* access modifiers changed from: private */
    public TextView photo;
    private ImageButton photoView;
    private TextView prisjaktId;
    private Button revert;
    private Button save;
    /* access modifiers changed from: private */
    public Spinner state;
    /* access modifiers changed from: private */
    public Button tags;
    /* access modifiers changed from: private */
    public boolean virgin = true;

    public void onCreate(Bundle savedInstanceState) {
        ZapApp.compat.hasOptionsMenu(this);
        requestWindowFeature(MENU_EDIT_BARCODE);
        super.onCreate(savedInstanceState);
        setResult(-1);
        this.mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.mPreferences.registerOnSharedPreferenceChangeListener(this);
        loadPreferences();
        setContentView((int) R.layout.editobject);
        this.objectId = getIntent().getLongExtra("objectId", -1);
        String findPrisjaktId = getIntent().getStringExtra("prisjaktid");
        this.db = ZapApp.getDB(this, false);
        if (this.objectId != -1) {
            this.object = this.db.getObject(this.objectId);
            if (!this.object.moveToFirst()) {
                Toast.makeText(this, String.valueOf(r(R.string.msg_find_failed)) + " " + this.objectId, 1).show();
                finish();
                return;
            }
            setTitle((int) R.string.label_edit);
        } else if (findPrisjaktId != null) {
            this.object = this.db.getObjects("prisjaktid = '" + findPrisjaktId + "'", null);
            if (this.object == null) {
                Toast.makeText(this, String.valueOf(r(R.string.msg_find_failed)) + " " + findPrisjaktId, 0).show();
                setTitle((int) R.string.title_create_object);
            } else if (!this.object.moveToFirst()) {
                this.object = null;
                setTitle((int) R.string.title_create_object);
            } else {
                setTitle((int) R.string.label_edit);
            }
        } else {
            setTitle((int) R.string.title_create_object);
        }
        this.ownerLabel = (TextView) findViewById(R.id.ownerLabel);
        this.state = (Spinner) findViewById(R.id.state);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.states, 17367048);
        adapter.setDropDownViewResource(17367049);
        this.state.setAdapter((SpinnerAdapter) adapter);
        this.state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                EditObject.this.adjustOwnerLabel();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.category = (Spinner) findViewById(R.id.category);
        this.categoryCursor = this.db.getCategories("name asc");
        SimpleCursorAdapter catAdapter = new SimpleCursorAdapter(this, 17367048, this.categoryCursor, new String[]{"name"}, new int[]{16908308});
        catAdapter.setDropDownViewResource(17367049);
        this.category.setAdapter((SpinnerAdapter) catAdapter);
        this.category.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                EditObject.this.virgin = false;
                return false;
            }
        });
        this.category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (!EditObject.this.virgin) {
                    EditObject.this.catCheckbox.setChecked(false);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.owner = (Button) findViewById(R.id.owner);
        this.owner.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    EditObject.this.startActivityForResult(EditObject.this.mContactAccessor.getContactPickerIntent(), 0);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(EditObject.this, "Sorry, your device is unable to pick contacts.\n" + e.getMessage(), 1).show();
                }
            }
        });
        this.numeral = (Button) findViewById(R.id.numeral);
        this.numeral.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LayoutInflater inflater = LayoutInflater.from(EditObject.this);
                AlertDialog.Builder builder = new AlertDialog.Builder(EditObject.this);
                View qs = inflater.inflate((int) R.layout.editnumeral, (ViewGroup) null);
                final NumberPicker np = (NumberPicker) qs.findViewById(R.id.number);
                String strVal = (String) EditObject.this.numeral.getTag();
                Float fVal = Float.valueOf(1.0f);
                try {
                    fVal = Float.valueOf(strVal);
                } catch (NumberFormatException e) {
                } catch (NullPointerException e2) {
                } finally {
                    np.setCurrent(fVal);
                }
                qs.measure(-2, -2);
                builder.setView(qs);
                builder.setInverseBackgroundForced(true);
                builder.setTitle((int) R.string.label_numeral);
                builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String val = np.getCurrentString();
                        EditObject.this.numeral.setText(val);
                        EditObject.this.numeral.setTag(val);
                    }
                });
                builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
                builder.create().show();
            }
        });
        this.tags = (Button) findViewById(R.id.tags);
        this.tags.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditObject.this);
                final Set<Long> currentTags = (HashSet) EditObject.this.tags.getTag();
                Cursor myTags = EditObject.this.db.getTagsSum("name");
                int nTags = myTags.getCount();
                if (nTags == 0) {
                    Toast.makeText(EditObject.this, (int) R.string.msg_empty_tags, 0).show();
                    return;
                }
                CharSequence[] _options = new CharSequence[nTags];
                boolean[] _selections = new boolean[nTags];
                final Long[] lookup = new Long[nTags];
                for (int i = 0; i < nTags; i++) {
                    if (myTags.moveToPosition(i)) {
                        _options[i] = myTags.getString(myTags.getColumnIndexOrThrow("name"));
                        Long key = Long.valueOf(myTags.getLong(myTags.getColumnIndexOrThrow(DBAdapter.KEY_ROWID)));
                        lookup[i] = key;
                        _selections[i] = currentTags.contains(key);
                    }
                }
                myTags.close();
                builder.setTitle((int) R.string.label_group);
                builder.setMultiChoiceItems(_options, _selections, new DialogInterface.OnMultiChoiceClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton, boolean isChecked) {
                        if (isChecked) {
                            currentTags.add(lookup[whichButton]);
                        } else {
                            currentTags.remove(lookup[whichButton]);
                        }
                    }
                });
                builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        EditObject.this.showTags(currentTags);
                        EditObject.this.tags.setTag(currentTags);
                    }
                });
                builder.setNegativeButton((CharSequence) null, (DialogInterface.OnClickListener) null);
                AlertDialog m_AlertDialog = builder.create();
                if (Integer.parseInt(Build.VERSION.SDK) < EditObject.MENU_EDIT_BARCODE) {
                    m_AlertDialog.getListView().setBackgroundColor(-7829368);
                }
                m_AlertDialog.show();
            }
        });
        this.photo = (TextView) findViewById(R.id.photo);
        this.photoView = (ImageButton) findViewById(R.id.photoSelect);
        this.photoView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent("android.intent.action.GET_CONTENT");
                i.setType("image/*");
                try {
                    EditObject.this.startActivityForResult(i, 2);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(EditObject.this, "Sorry, your device can not select in image: " + e.getMessage(), 1).show();
                }
            }
        });
        this.photoView = (ImageButton) findViewById(R.id.photoTake);
        this.photoView.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View v) {
                Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
                ContentValues values = new ContentValues();
                values.put(InventoryProvider.AppWidgetsColumns.TITLE, "Inventory picture");
                values.put("description", "User snapshot of an item");
                String msg = "";
                try {
                    EditObject.this.myPicture = EditObject.this.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                } catch (IllegalStateException e) {
                    msg = " " + e.getMessage();
                } catch (UnsupportedOperationException e2) {
                    msg = " " + e2.getMessage();
                }
                if (EditObject.this.myPicture == null) {
                    Toast.makeText(EditObject.this, "Failed to prepare image store for camera. Looks like a firmware problem. Please use camera separately and import for gallery for now." + msg, 1).show();
                    return;
                }
                i.putExtra("output", EditObject.this.myPicture);
                i.putExtra("android.intent.extra.videoQuality", 1);
                i.putExtra("return-data", true);
                try {
                    EditObject.this.startActivityForResult(i, 3);
                } catch (ActivityNotFoundException e3) {
                    Toast.makeText(EditObject.this, "Hmm, looks like you have no camera. Giving up...", 0).show();
                }
            }
        });
        this.photoView = (ImageButton) findViewById(R.id.photoEdit);
        this.photoView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditObject.this);
                final EditText editor = new EditText(EditObject.this);
                editor.setText((String) EditObject.this.photo.getText());
                builder.setTitle((int) R.string.label_edit_image_url);
                builder.setView(editor);
                builder.setNeutralButton(17039360, (DialogInterface.OnClickListener) null);
                builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        EditObject.this.setPhoto(editor.getText().toString());
                    }
                });
                builder.show();
            }
        });
        this.photoView = (ImageButton) findViewById(R.id.photoView);
        this.photoView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditObject.this.showUrl(EditObject.this.photo.getText().toString());
            }
        });
        this.save = (Button) findViewById(R.id.save);
        this.save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!EditObject.this.saveData()) {
                    Toast.makeText(EditObject.this, (int) R.string.msg_save_failed, 1).show();
                    return;
                }
                EditObject.this.setResult(-1);
                EditObject.this.finish();
            }
        });
        this.revert = (Button) findViewById(R.id.revert);
        this.revert.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditObject.this.setResult(0);
                EditObject.this.finish();
            }
        });
        this.name = (EditText) findViewById(R.id.name);
        this.name.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable arg0) {
                EditObject.this.saveEnable();
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        this.note = (EditText) findViewById(R.id.note);
        this.created = (TextView) findViewById(R.id.created);
        this.changed = (TextView) findViewById(R.id.changed);
        this.catCheckbox = (CheckBox) findViewById(R.id.catcheckbox);
        this.catCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked && EditObject.this.categoryCursor.getCount() == 0) {
                    Toast.makeText(EditObject.this, (int) R.string.msg_no_category_enter_new, 0).show();
                    EditObject.this.catCheckbox.setChecked(true);
                }
            }
        });
        this.prisjaktId = (TextView) findViewById(R.id.prisjaktid);
        this.barcode = (TextView) findViewById(R.id.barcode);
        this.catNew = (EditText) findViewById(R.id.newCat);
        this.catNew.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable arg0) {
                EditObject.this.catCheckbox.setChecked(EditObject.this.catNew.getText().length() != 0);
                EditObject.this.saveEnable();
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        this.created.setLongClickable(true);
        this.created.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                if (((Long) v.getTag()) == null) {
                    return false;
                }
                EditObject.this.removeDialog(1);
                EditObject.this.showDialog(1);
                return true;
            }
        });
        displayData();
        getWindow().setSoftInputMode(3);
        getWindow().setFeatureInt(MENU_EDIT_BARCODE, -2);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                Calendar c = Calendar.getInstance();
                Long ctime = (Long) this.created.getTag();
                if (ctime != null) {
                    c.setTimeInMillis(ctime.longValue());
                }
                return new DatePickerDialog(this, this.mDateSetListener, c.get(1), c.get(2), c.get(MENU_EDIT_BARCODE));
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void saveEnable() {
        if (this.name.length() == 0 || ((!this.catCheckbox.isChecked() || this.catNew.length() == 0) && this.categoryCursor.getCount() <= 0)) {
            this.save.setEnabled(false);
        } else {
            this.save.setEnabled(true);
        }
    }

    /* access modifiers changed from: private */
    public void adjustOwnerLabel() {
        View sel;
        int label = R.string.label_owner;
        if (this.state != null && (sel = this.state.getSelectedView()) != null) {
            switch (this.state.getPositionForView(sel)) {
                case 2:
                    label = R.string.label_lent_to;
                    break;
            }
            this.ownerLabel.setText(label);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (requestCode) {
            case 0:
                if (resultCode == -1) {
                    Uri contactData = intent.getData();
                    Cursor c = getContentResolver().query(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String name2 = c.getString(c.getColumnIndexOrThrow(this.mContactAccessor.getNameIndex()));
                        this.owner.setText(name2);
                        this.owner.setTag(contactData);
                        Toast.makeText(this, String.valueOf(r(R.string.msg_selected)) + ": " + name2, 0).show();
                    }
                    c.close();
                    return;
                }
                Toast.makeText(this, (int) R.string.msg_no_selecting_mine, 0).show();
                this.owner.setText((int) R.string.me);
                this.owner.setTag(null);
                return;
            case 1:
                if (resultCode == -1) {
                    setBarcode(intent.getStringExtra("SCAN_RESULT"));
                    this.communityBarcode = true;
                    return;
                }
                Toast.makeText(this, (int) R.string.msg_aborted_barcode_scanning, 0).show();
                return;
            case 2:
                if (resultCode == -1) {
                    setPhoto(intent.getData().toString());
                    return;
                }
                Toast.makeText(this, "Aborted photo selection", 0).show();
                setPhoto(null);
                return;
            case DBAdapter.STATE_HIDE:
                if (resultCode != -1) {
                    Toast.makeText(this, "Aborted taking photo", 0).show();
                    setPhoto(null);
                    return;
                } else if (this.myPicture != null) {
                    setPhoto(this.myPicture.toString());
                    return;
                } else {
                    Toast.makeText(this, "Sorry, your firmware seems to have a camera bug and isn't supported for now. Until you upgrade you will need to use the camera separately and just select the picture instead.", 1).show();
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void displayData() {
        if (this.object == null) {
            Intent base = getIntent();
            String defName = base.getStringExtra("android.intent.extra.SUBJECT");
            if (defName != null) {
                this.name.setText(defName);
            }
            String defText = base.getStringExtra("android.intent.extra.TEXT");
            if (defText != null) {
                this.note.setText(defText);
            }
            setNumeral(base.getStringExtra(DBAdapter.KEY_NUMERAL));
            String defPrisjaktId = base.getStringExtra("prisjaktid");
            setPrisjakt(defPrisjaktId);
            String barcode2 = base.getStringExtra(DBAdapter.KEY_BARCODE);
            setBarcode(barcode2);
            this.state.setSelection(this.defaultState);
            setOwner(this.defaultOwner);
            this.save.setEnabled(this.name.getText().length() != 0 && (!this.catCheckbox.isChecked() || this.catNew.getText().length() != 0));
            String defCat = base.getStringExtra(DBAdapter.KEY_CATEGORY);
            if (this.overrideCategory != null) {
                defCat = this.overrideCategory;
            }
            setCategory(defCat);
            if (barcode2 != null && defPrisjaktId == null) {
                this.communityBarcode = true;
            }
            setTags("");
            String defImage = base.getStringExtra(DBAdapter.KEY_IMAGE);
            if (defImage != null) {
                setPhoto(defImage);
            } else if (defPrisjaktId == null || defPrisjaktId.length() <= 1 || ZapsInventory.jsonServer == null) {
                setPhoto(null);
            } else {
                setPhoto(String.valueOf(ZapsInventory.jsonServer) + "bilder/bild.php?size=800&p=" + defPrisjaktId);
            }
        } else {
            this.name.setText(this.object.getString(this.object.getColumnIndexOrThrow("name")));
            this.note.setText(this.object.getString(this.object.getColumnIndexOrThrow(DBAdapter.KEY_NOTE)));
            this.state.setSelection(this.object.getInt(this.object.getColumnIndexOrThrow("state")));
            Long ctime = Long.valueOf(this.object.getLong(this.object.getColumnIndexOrThrow(DBAdapter.KEY_CREATED)));
            this.created.setText(this.df.format(ctime));
            this.created.setTag(ctime);
            this.changed.setText(this.df.format(Long.valueOf(this.object.getLong(this.object.getColumnIndexOrThrow(DBAdapter.KEY_CHANGED)))));
            setOwner(this.object.getString(this.object.getColumnIndexOrThrow("owner")));
            setCategory(this.object.getString(this.object.getColumnIndexOrThrow(DBAdapter.KEY_CATEGORY)));
            String pjId = this.object.getString(this.object.getColumnIndexOrThrow("prisjaktid"));
            setPrisjakt(pjId);
            setBarcode(this.object.getString(this.object.getColumnIndexOrThrow(DBAdapter.KEY_BARCODE)));
            setNumeral(this.object.getString(this.object.getColumnIndexOrThrow(DBAdapter.KEY_NUMERAL)));
            setTags(this.object.getString(this.object.getColumnIndexOrThrow(DBAdapter.KEY_GROUP)));
            String dbPhoto = this.object.getString(this.object.getColumnIndexOrThrow(DBAdapter.KEY_IMAGE));
            if ((dbPhoto == null || dbPhoto.equals("")) && pjId != null && pjId.matches("[0-9]*")) {
                dbPhoto = String.valueOf(ZapsInventory.jsonServer) + "bilder/bild.php?size=800&p=" + pjId;
            }
            setPhoto(dbPhoto);
        }
    }

    private void setOwner(String ownerUri) {
        if (ownerUri != null) {
            this.owner.setTag(ownerUri);
            String name2 = String.valueOf(ownerUri) + " no longer exists!";
            Cursor c = getContentResolver().query(Uri.parse(ownerUri), null, null, null, null);
            if (c != null) {
                if (c.moveToFirst()) {
                    name2 = c.getString(c.getColumnIndexOrThrow(this.mContactAccessor.getNameIndex()));
                }
                c.close();
            }
            this.owner.setText(name2);
            return;
        }
        this.owner.setTag(null);
        this.owner.setText((int) R.string.me);
    }

    /* access modifiers changed from: private */
    public void setCategory(String candidate) {
        int candidatePos = -1;
        if (this.categoryCursor.moveToFirst()) {
            do {
                if (this.categoryCursor.getString(this.categoryCursor.getColumnIndexOrThrow("name")).equals(candidate)) {
                    candidatePos = this.categoryCursor.getPosition();
                }
                if (candidatePos != -1) {
                    break;
                }
            } while (this.categoryCursor.moveToNext());
        }
        if (candidatePos == -1) {
            this.catNew.setText(candidate);
        } else {
            this.category.setSelection(candidatePos);
        }
        this.catCheckbox.setChecked(candidatePos == -1);
    }

    private void setTags(String strTags) {
        Set<Long> myTags = tagsStringToSet(strTags);
        showTags(myTags);
        this.tags.setTag(myTags);
    }

    public static Set<Long> tagsStringToSet(String strTags) {
        Set<Long> myTags = new HashSet<>();
        myTags.clear();
        if (strTags != null) {
            for (String part : strTags.split(",")) {
                try {
                    myTags.add(Long.valueOf(part));
                } catch (NumberFormatException e) {
                }
            }
        }
        return myTags;
    }

    private String getTags() {
        String strTags = ",";
        for (Long tag : (Set) this.tags.getTag()) {
            strTags = strTags.concat(String.valueOf(tag.toString()) + ",");
        }
        return strTags;
    }

    /* access modifiers changed from: private */
    public void showTags(Set<Long> tagSet) {
        this.tags.setText(tagsSetToString(this.db, tagSet));
    }

    public static void flushTagMap() {
        tagMapper = null;
    }

    private static void makeMap(DBAdapter db2) {
        tagMapper = new HashMap();
        Cursor c = db2.getTagsSum("name");
        int nTags = c.getCount();
        for (int i = 0; i < nTags; i++) {
            if (c.moveToPosition(i)) {
                tagMapper.put(Long.valueOf(c.getLong(c.getColumnIndexOrThrow(DBAdapter.KEY_ROWID))), c.getString(c.getColumnIndexOrThrow("name")));
            }
        }
        c.close();
    }

    public static String tagsSetToString(DBAdapter db2, Set<Long> tagSet) {
        String msg = "";
        if (tagMapper == null) {
            makeMap(db2);
        }
        for (Long key : tagMapper.keySet()) {
            if (tagSet.contains(key)) {
                msg = msg.concat(String.valueOf(tagMapper.get(key)) + " ");
            }
        }
        if (msg == "") {
            msg = "-";
        }
        return msg.trim();
    }

    private void setPrisjakt(String id) {
        if (id != null) {
            if (id.matches("[0-9]*")) {
                this.prisjaktId.setText(String.valueOf(r(R.string.label_prisjakt)) + ": " + id);
            } else if (id.startsWith("GB:")) {
                this.prisjaktId.setText("GoogleBooks: " + id.substring(3));
            } else if (id.startsWith("AZ:")) {
                this.prisjaktId.setText("Amazon: " + id.substring(3));
            } else if (id.startsWith("AL:")) {
                this.prisjaktId.setText("AdLibris: " + id.substring(3));
            } else {
                this.prisjaktId.setText(id);
            }
            this.prisjaktId.setTag(id);
        }
    }

    /* access modifiers changed from: private */
    public void setNumeral(String defNumeral) {
        if (defNumeral == null) {
            defNumeral = this.defaultNumeral;
        }
        this.numeral.setText(defNumeral);
        this.numeral.setTag(defNumeral);
    }

    /* access modifiers changed from: private */
    public void setBarcode(String defBarcode) {
        if (defBarcode != null) {
            this.barcode.setText(String.valueOf(r(R.string.label_barcode)) + ": " + defBarcode);
            this.barcode.setTag(defBarcode);
        }
    }

    /* access modifiers changed from: private */
    public void setPhoto(String path) {
        this.photo.setText(path);
        this.photoView.setEnabled(path != null && path.length() > 0);
    }

    /* access modifiers changed from: private */
    public static Bitmap fetchBitmap(String urlString) {
        try {
            return BitmapFactory.decodeStream(fetch(urlString));
        } catch (MalformedURLException e) {
            return null;
        } catch (IOException e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        if (this.myPicture != null) {
            outState.putString("mypicture", this.myPicture.toString());
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle inState) {
        if (inState != null) {
            try {
                String p = inState.getString("mypicture");
                if (p != null) {
                    this.myPicture = Uri.parse(p);
                }
            } catch (Exception e) {
                Toast.makeText(this, "Failed to restore picture reference", 0).show();
            }
        }
    }

    private static InputStream fetch(String urlString) throws MalformedURLException, IOException {
        return new DefaultHttpClient().execute(new HttpGet(urlString)).getEntity().getContent();
    }

    /* JADX INFO: Multiple debug info for r9v1 java.lang.String: [D('webUrl' java.lang.String), D('safeUrl' java.lang.String)] */
    private static void showImageDialog(String webUrl, Context context) {
        ImageView throbber = new ImageView(context);
        throbber.setImageResource(R.drawable.ic_popup_sync_1);
        RotateAnimation rotate = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 1, 0.5f);
        rotate.setDuration(600);
        rotate.setRepeatMode(1);
        rotate.setRepeatCount(-1);
        final LinearLayout fl = new LinearLayout(context);
        final ImageView i = new ImageView(context);
        final Context ctx = context;
        final String safeUrl = webUrl.replace(" ", "%20");
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(1);
        final ImageView imageView = throbber;
        final RotateAnimation rotateAnimation = rotate;
        final Handler handler = new Handler() {
            public void handleMessage(Message message) {
                imageView.setVisibility(8);
                rotateAnimation.setRepeatCount(0);
                i.setAdjustViewBounds(true);
                if (((Bitmap) message.obj) == null) {
                    Toast.makeText(ctx, (int) R.string.msg_network_error, 0).show();
                    try {
                        d.dismiss();
                    } catch (Exception e) {
                    }
                }
                i.setImageBitmap((Bitmap) message.obj);
                i.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                fl.removeView(imageView);
                fl.addView(i);
            }
        };
        new Thread() {
            public void run() {
                handler.sendMessage(handler.obtainMessage(1, EditObject.fetchBitmap(safeUrl)));
            }
        }.start();
        i.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        fl.addView(throbber);
        throbber.startAnimation(rotate);
        fl.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        d.setContentView(fl);
        i.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    /* access modifiers changed from: private */
    public void showUrl(String imageUrl) {
        showUrl(this, imageUrl);
    }

    public static void showUrl(Context ctx, String imageUrl) {
        if (imageUrl == null || imageUrl.length() < MENU_SEARCH_BARCODE) {
            Toast.makeText(ctx, "Nothing to show", 0).show();
        }
        ImageView i = new ImageView(ctx);
        final Dialog d = new Dialog(ctx);
        d.requestWindowFeature(1);
        try {
            if (imageUrl.startsWith("http", 0)) {
                showImageDialog(imageUrl, ctx);
                return;
            }
            i.setImageBitmap(getBitmap(ctx, imageUrl));
            i.setScaleType(ImageView.ScaleType.FIT_XY);
            i.setAdjustViewBounds(true);
            i.setBackgroundColor(-1);
            i.setClickable(true);
            i.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    d.dismiss();
                }
            });
            i.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            d.setContentView(i);
            d.setCanceledOnTouchOutside(true);
            d.show();
        } catch (Exception e) {
            Toast.makeText(ctx, "Failed to show image", 0).show();
        }
    }

    public static Bitmap getBitmap(Context context, String photoUriPath) throws Exception {
        InputStream photoStream = context.getContentResolver().openInputStream(Uri.parse(photoUriPath));
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        Bitmap photoBitmap = BitmapFactory.decodeStream(photoStream, null, options);
        int h = photoBitmap.getHeight();
        int w = photoBitmap.getWidth();
        if (w > h && w > 1024) {
            double ratio = 1024.0d / ((double) w);
            w = 1024;
            h = (int) (((double) h) * ratio);
        } else if (h > w && h > 1024) {
            double ratio2 = 1024.0d / ((double) h);
            h = 1024;
            w = (int) (((double) w) * ratio2);
        }
        Bitmap scaled = Bitmap.createScaledBitmap(photoBitmap, w, h, true);
        photoBitmap.recycle();
        return scaled;
    }

    private String r(int id) {
        return getResources().getString(id);
    }

    /* access modifiers changed from: protected */
    public boolean saveData() {
        String catString;
        boolean retval;
        ContentValues vals = new ContentValues();
        vals.put("name", this.name.getText().toString());
        vals.put(DBAdapter.KEY_NOTE, this.note.getText().toString());
        vals.put("state", Integer.valueOf(this.state.getPositionForView(this.state.getSelectedView())));
        vals.put(DBAdapter.KEY_BARCODE, (String) this.barcode.getTag());
        vals.put("prisjaktid", (String) this.prisjaktId.getTag());
        vals.put(DBAdapter.KEY_GROUP, getTags());
        vals.put(DBAdapter.KEY_IMAGE, this.photo.getText().toString());
        String ownerUri = null;
        if (this.owner.getTag() != null) {
            ownerUri = this.owner.getTag().toString();
        }
        vals.put("owner", ownerUri);
        vals.put(DBAdapter.KEY_CHANGED, Long.valueOf(System.currentTimeMillis()));
        String strNum = (String) this.numeral.getTag();
        if (strNum == null) {
            strNum = "1";
        }
        vals.put(DBAdapter.KEY_NUMERAL, strNum);
        if (!this.catCheckbox.isChecked() || this.catNew.getText().length() <= 0) {
            catString = ((TextView) this.category.getSelectedView()).getText().toString();
        } else {
            ContentValues catVals = new ContentValues();
            catString = this.catNew.getText().toString();
            catVals.put("name", catString);
            if (this.db.insertCategory(catVals) == -1) {
                Toast.makeText(this, "Failed to create new category " + catString, 1).show();
            }
        }
        vals.put(DBAdapter.KEY_CATEGORY, catString);
        if (this.objectId == -1) {
            vals.put(DBAdapter.KEY_CREATED, Long.valueOf(System.currentTimeMillis()));
            retval = this.db.insertObject(vals) != -1;
        } else {
            Long ctime = (Long) this.created.getTag();
            if (ctime != null) {
                vals.put(DBAdapter.KEY_CREATED, ctime);
            }
            retval = this.db.updateObject(this.objectId, vals);
        }
        if (this.communityBarcode) {
            StatCollector.communityBarcodeStore(this, (String) this.barcode.getTag(), this.name.getText().toString(), catString, ZapsInventory.email);
        }
        return retval;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuItem newitem = menu.add(0, 0, 0, (int) R.string.menu_set_barcode);
        newitem.setIcon(17301555);
        ZapApp.compat.prepActionMenu(newitem);
        if (this.prisjaktId.getTag() != null) {
            menu.add(0, 2, 0, (int) R.string.label_prices_prisjakt).setIcon((int) R.drawable.logo_prisjakt_s);
        } else {
            menu.add(0, 3, 0, (int) R.string.label_search_prisjakt).setIcon((int) R.drawable.logo_prisjakt_s);
        }
        if (this.barcode.getTag() != null) {
            MenuItem searchBarcode = menu.add(0, (int) MENU_SEARCH_BARCODE, 0, (int) R.string.label_search_barcode);
            searchBarcode.setIcon(17301600);
            ZapApp.compat.prepActionMenu(searchBarcode);
            MenuItem reevalBarcode = menu.add(0, (int) MENU_REEVAL_BARCODE, 0, (int) R.string.label_reeval_barcode);
            reevalBarcode.setIcon(17301580);
            ZapApp.compat.prepActionMenu(reevalBarcode);
        }
        menu.add(0, (int) MENU_EDIT_BARCODE, 0, (int) R.string.menu_edit_barcode).setIcon(17301566);
        menu.add(0, (int) MENU_APPLY_TEMPLATE, 0, (int) R.string.menu_apply_template).setIcon(17301515);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean z;
        MenuItem mi = menu.findItem(3);
        if (mi != null) {
            mi.setEnabled(this.name.getText().length() > 0);
        }
        MenuItem mi2 = menu.findItem(MENU_SEARCH_BARCODE);
        if (mi2 != null) {
            if (this.barcode.getTag() != null) {
                z = true;
            } else {
                z = false;
            }
            mi2.setEnabled(z);
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                if ("products".equals(this.barcodeSupport)) {
                    intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
                }
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, 1);
                    break;
                } else {
                    Intent mint = new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=zxing"));
                    if (mint.resolveActivity(getPackageManager()) == null) {
                        if (Build.MODEL.equals("google_sdk")) {
                            Intent fake = new Intent();
                            fake.putExtra("SCAN_RESULT", "5025322412367");
                            onActivityResult(1, -1, fake);
                        } else {
                            Toast.makeText(this, R.string.error_no_market_manual_barcode, 1).show();
                        }
                        return false;
                    }
                    startActivity(mint);
                    Toast.makeText(this, R.string.hint_install_barcode, 1).show();
                    return false;
                }
            case 2:
            case DBAdapter.STATE_HIDE:
                Intent intent2 = new Intent("se.petersson.prisjakt.ZapsPrisjakt.SEARCH");
                ComponentName cn = intent2.resolveActivity(getPackageManager());
                if (cn == null) {
                    Intent mint2 = new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=Prisjakt"));
                    if (mint2.resolveActivity(getPackageManager()) == null) {
                        Toast.makeText(this, R.string.error_no_market_manual_prisjakt, 1).show();
                        return true;
                    }
                    startActivity(mint2);
                    Toast.makeText(this, R.string.hint_install_prisjakt, 1).show();
                    return true;
                }
                String prisjaktid = (String) this.prisjaktId.getTag();
                String nameSearch = this.name.getText().toString();
                intent2.putExtra("se.petersson.prisjakt.itemName", "Prisjakt: " + nameSearch);
                switch (item.getItemId()) {
                    case 2:
                        intent2.putExtra("se.petersson.prisjakt.ListType", "ItemDetails");
                        intent2.putExtra("se.petersson.prisjakt.itemId", prisjaktid);
                        break;
                    case DBAdapter.STATE_HIDE:
                        intent2.setAction("android.intent.action.SEARCH");
                        intent2.setComponent(cn);
                        intent2.putExtra("query", nameSearch);
                        break;
                }
                startActivity(intent2);
                return true;
            case MENU_SEARCH_BARCODE /*4*/:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.google.com/m/search?q=" + URLEncoder.encode(this.barcode.getTag().toString()))));
                break;
            case MENU_EDIT_BARCODE /*5*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                EditText editText = new EditText(this);
                editText.setText((String) this.barcode.getTag());
                builder.setTitle((int) R.string.menu_edit_barcode);
                if ("products".equals(this.barcodeSupport)) {
                    editText.setInputType(2);
                }
                builder.setView(editText);
                builder.setNeutralButton(17039360, (DialogInterface.OnClickListener) null);
                final EditText editText2 = editText;
                builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String value = editText2.getText().toString();
                        if (value != null && value.length() == 0) {
                            value = null;
                        }
                        EditObject.this.setBarcode(value);
                    }
                });
                builder.show();
                return true;
            case MENU_REEVAL_BARCODE /*6*/:
                getWindow().setFeatureInt(MENU_EDIT_BARCODE, -1);
                new Thread() {
                    public void run() {
                        EditObject.this.doHardWork();
                        EditObject.this.mHandler.post(EditObject.this.mUpdateResults);
                    }
                }.start();
                return true;
            case MENU_APPLY_TEMPLATE /*7*/:
                Cursor templateTag = this.db.getOptions("kind = 1 AND name = " + DatabaseUtils.sqlEscapeString("Template"), null);
                if (templateTag != null && templateTag.getCount() == 1) {
                    if (templateTag.moveToFirst()) {
                        int tag = templateTag.getInt(templateTag.getColumnIndexOrThrow(DBAdapter.KEY_ROWID));
                        templateTag.close();
                        Cursor templates = this.db.getObjects("grp like " + DatabaseUtils.sqlEscapeString("%," + tag + ",%"), "name COLLATE NOCASE ASC");
                        if (templates != null && templates.getCount() >= 1) {
                            SimpleCursorAdapter a = new SimpleCursorAdapter(this, 17367044, templates, new String[]{"name", DBAdapter.KEY_CATEGORY}, new int[]{16908308, 16908309});
                            AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                            builder2.setTitle((int) R.string.label_select_template);
                            final SimpleCursorAdapter simpleCursorAdapter = a;
                            final Cursor cursor = templates;
                            builder2.setAdapter(a, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int position) {
                                    Cursor c = (Cursor) simpleCursorAdapter.getItem(position);
                                    dialog.dismiss();
                                    EditObject.this.state.setSelection((int) c.getLong(c.getColumnIndexOrThrow("state")));
                                    String val = c.getString(c.getColumnIndexOrThrow("name"));
                                    if (!"".equals(val)) {
                                        EditObject.this.name.setText(val);
                                    }
                                    String val2 = c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_NOTE));
                                    if (!"".equals(val2)) {
                                        EditObject.this.note.setText(val2);
                                    }
                                    EditObject.this.setCategory(c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_CATEGORY)));
                                    EditObject.this.setNumeral(c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_NUMERAL)));
                                    cursor.close();
                                }
                            });
                            final Cursor cursor2 = templates;
                            builder2.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                public void onCancel(DialogInterface dialog) {
                                    cursor2.close();
                                }
                            });
                            builder2.show();
                            break;
                        } else {
                            Toast.makeText(this, R.string.msg_need_templates, 1).show();
                            if (templates != null) {
                                templates.close();
                            }
                            return true;
                        }
                    } else {
                        return true;
                    }
                } else {
                    Toast.makeText(this, R.string.msg_need_templates, 1).show();
                    if (templateTag != null) {
                        templateTag.close();
                    }
                    return true;
                }
                break;
            case 16908332:
                Intent intent3 = new Intent(this, ZapsInventory.class);
                intent3.addFlags(67108864);
                startActivity(intent3);
                return true;
        }
        return true;
    }

    public void OnResume() {
        this.db = ZapApp.getDB(this, false);
        loadPreferences();
        adjustOwnerLabel();
    }

    /* access modifiers changed from: private */
    public void doHardWork() {
        this.myList = null;
        if (ZapsInventory.jsonServer != null && ZapsInventory.jsonServer.startsWith("http")) {
            PrisjaktLookup.jsonserver = String.valueOf(ZapsInventory.jsonServer) + "extern/extern_data.php?";
            this.myList = Collections.synchronizedList(pjLookup.barcode(this.barcode.getTag().toString(), "rank"));
        }
        if (this.myList == null || this.myList.isEmpty()) {
            this.myList = Collections.synchronizedList(StatCollector.communityBarcodeLookup(this, this.barcode.getTag().toString(), ZapsInventory.email));
        }
    }

    /* access modifiers changed from: private */
    public void updateResultsInUi() {
        if (this.myList == null || this.myList.isEmpty()) {
            Toast.makeText(this, (int) R.string.msg_no_match_make_it_up, 0).show();
        } else {
            String pjImg = null;
            Map<String, String> item = this.myList.get(0);
            String value = (String) item.get("name");
            if (value != null) {
                this.name.setText(value);
            }
            String value2 = item.get(DBAdapter.KEY_NOTE);
            if (value2 != null) {
                this.note.setText(value2);
            }
            String value3 = item.get("id");
            if (value3 != null) {
                setPrisjakt(value3);
                if (value3.matches("[0-9]*")) {
                    pjImg = String.valueOf(ZapsInventory.jsonServer) + "bilder/bild.php?size=800&p=" + value3;
                }
            }
            String value4 = item.get(DBAdapter.KEY_IMAGE);
            if (value4 != null) {
                setPhoto(value4);
            } else if (pjImg != null) {
                setPhoto(pjImg);
            }
            Toast.makeText(this, (int) R.string.msg_match_found_confirm, 0).show();
        }
        getWindow().setFeatureInt(MENU_EDIT_BARCODE, -2);
    }

    public void onDestroy() {
        if (this.object != null) {
            this.object.close();
        }
        if (this.categoryCursor != null) {
            this.categoryCursor.close();
        }
        super.onDestroy();
    }

    private void loadPreferences() {
        this.mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.defaultState = Integer.valueOf(this.mPreferences.getString(getString(R.string.pref_default_state), "1")).intValue();
        this.defaultOwner = this.mPreferences.getString("pref_default_owner", null);
        this.defaultNumeral = this.mPreferences.getString("pref_initial_numeral", "1");
        if ("".equals(this.defaultOwner)) {
            this.defaultOwner = null;
        }
        this.overrideCategory = this.mPreferences.getString("pref_override_category", null);
        if ("".equals(this.overrideCategory)) {
            this.overrideCategory = null;
        }
        this.barcodeSupport = this.mPreferences.getString(getString(R.string.pref_barcode_support), "products");
    }

    public void onSharedPreferenceChanged(SharedPreferences arg0, String arg1) {
        loadPreferences();
    }
}
