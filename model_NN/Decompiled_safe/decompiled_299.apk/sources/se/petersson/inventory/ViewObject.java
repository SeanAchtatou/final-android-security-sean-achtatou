package se.petersson.inventory;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Set;

public class ViewObject extends Activity {
    private final int MENU_EDIT = 0;
    private TextView barcode;
    private TextView category;
    private Cursor categoryCursor = null;
    private TextView changed;
    private TextView created;
    private DBAdapter db;
    DateFormat df = DateFormat.getDateTimeInstance();
    /* access modifiers changed from: private */
    public Drawable headerDrawable = null;
    /* access modifiers changed from: private */
    public ImageView headerImage = null;
    private final ContactAccessor mContactAccessor = ContactAccessor.getInstance();
    final Handler mHandler = new Handler();
    final Runnable mUpdateHeader = new Runnable() {
        public void run() {
            ViewObject.this.updateHeaderInUi();
        }
    };
    private TextView note;
    private TextView numeral;
    private Cursor object = null;
    private long objectId;
    private TextView owner;
    private TextView ownerLabel;
    private TextView prisjaktId;
    private TextView state;
    private int stateId;
    private TextView tags;

    public void onCreate(Bundle savedInstanceState) {
        ZapApp.compat.hasOptionsMenu(this);
        requestWindowFeature(5);
        super.onCreate(savedInstanceState);
        setResult(-1);
        setContentView((int) R.layout.viewobject);
        this.objectId = getIntent().getLongExtra("objectId", -1);
        if (this.objectId == -1) {
            Toast.makeText(this, "Item not found", 0).show();
            finish();
            return;
        }
        setTitle((int) R.string.label_overview);
        this.ownerLabel = (TextView) findViewById(R.id.ownerLabel);
        this.state = (TextView) findViewById(R.id.state);
        this.note = (TextView) findViewById(R.id.note);
        this.created = (TextView) findViewById(R.id.created);
        this.changed = (TextView) findViewById(R.id.changed);
        this.prisjaktId = (TextView) findViewById(R.id.prisjaktid);
        this.barcode = (TextView) findViewById(R.id.barcode);
        this.owner = (TextView) findViewById(R.id.owner);
        this.category = (TextView) findViewById(R.id.category);
        this.numeral = (TextView) findViewById(R.id.numeral);
        this.tags = (TextView) findViewById(R.id.tags);
        getWindow().setSoftInputMode(3);
        getWindow().setFeatureInt(5, -2);
    }

    public void onStart() {
        super.onStart();
        this.db = ZapApp.getDB(this, false);
        this.object = this.db.getObject(this.objectId);
        if (!this.object.moveToFirst()) {
            Toast.makeText(this, String.valueOf(r(R.string.msg_find_failed)) + " " + this.objectId, 1).show();
            finish();
            return;
        }
        displayData();
    }

    private void adjustOwnerLabel() {
        int label = R.string.label_owner;
        switch (this.stateId) {
            case 2:
                label = R.string.label_lent_to;
                break;
        }
        this.ownerLabel.setText(label);
    }

    /* access modifiers changed from: protected */
    public void displayData() {
        ArrayAdapter<CharSequence> statesAdapter = ArrayAdapter.createFromResource(this, R.array.states, 17367048);
        setTitle(String.valueOf(this.object.getString(this.object.getColumnIndexOrThrow(DBAdapter.KEY_CATEGORY))) + ": " + this.object.getString(this.object.getColumnIndexOrThrow("name")));
        this.note.setText(this.object.getString(this.object.getColumnIndexOrThrow(DBAdapter.KEY_NOTE)));
        this.stateId = this.object.getInt(this.object.getColumnIndexOrThrow("state"));
        this.state.setText(statesAdapter.getItem(this.stateId).toString());
        adjustOwnerLabel();
        Long ctime = Long.valueOf(this.object.getLong(this.object.getColumnIndexOrThrow(DBAdapter.KEY_CREATED)));
        this.created.setText(this.df.format(ctime));
        this.created.setTag(ctime);
        this.changed.setText(this.df.format(Long.valueOf(this.object.getLong(this.object.getColumnIndexOrThrow(DBAdapter.KEY_CHANGED)))));
        setOwner(this.object.getString(this.object.getColumnIndexOrThrow("owner")));
        setPrisjakt(this.object.getString(this.object.getColumnIndexOrThrow("prisjaktid")));
        setBarcode(this.object.getString(this.object.getColumnIndexOrThrow(DBAdapter.KEY_BARCODE)));
        setNumeral(this.object.getString(this.object.getColumnIndexOrThrow(DBAdapter.KEY_NUMERAL)));
        setTags(this.object.getString(this.object.getColumnIndexOrThrow(DBAdapter.KEY_GROUP)));
        showPhoto(this.object.getString(this.object.getColumnIndexOrThrow(DBAdapter.KEY_IMAGE)));
    }

    private void setOwner(String ownerUri) {
        if (ownerUri != null) {
            this.owner.setTag(ownerUri);
            String name = String.valueOf(ownerUri) + " no longer exists!";
            Cursor c = getContentResolver().query(Uri.parse(ownerUri), null, null, null, null);
            if (c != null) {
                if (c.moveToFirst()) {
                    name = c.getString(c.getColumnIndexOrThrow(this.mContactAccessor.getNameIndex()));
                }
                c.close();
            }
            this.owner.setText(name);
            return;
        }
        this.owner.setTag(null);
        this.owner.setText((int) R.string.me);
    }

    private void setTags(String strTags) {
        Set<Long> myTags = EditObject.tagsStringToSet(strTags);
        showTags(myTags);
        this.tags.setTag(myTags);
    }

    private void showTags(Set<Long> tagSet) {
        this.tags.setText(EditObject.tagsSetToString(this.db, tagSet));
    }

    /* access modifiers changed from: private */
    public void updateHeaderInUi() {
        if (this.headerImage != null && this.headerDrawable != null) {
            this.headerImage.setImageDrawable(this.headerDrawable);
        }
    }

    private void showPhoto(String url) {
        this.headerImage = (ImageView) findViewById(R.id.photo);
        if (url != null && !url.equals("")) {
            final String imageUrl = url;
            new Thread() {
                public void run() {
                    try {
                        if (!imageUrl.startsWith("http")) {
                            try {
                                ViewObject.this.headerImage.setImageBitmap(EditObject.getBitmap(ViewObject.this, imageUrl));
                            } catch (Exception e) {
                            }
                        } else {
                            URL url = new URL(imageUrl);
                            int n = 0;
                            do {
                                ViewObject.this.headerDrawable = Drawable.createFromStream(url.openStream(), "test");
                                if (ViewObject.this.headerDrawable == null || ViewObject.this.headerDrawable.getIntrinsicHeight() < 1) {
                                    n++;
                                }
                            } while (n < 5);
                        }
                        ViewObject.this.mHandler.post(ViewObject.this.mUpdateHeader);
                    } catch (IOException e2) {
                        e2.getStackTrace();
                    }
                }
            }.start();
        }
    }

    private void setPrisjakt(String id) {
        if (id != null) {
            if (id.matches("[0-9]*")) {
                this.prisjaktId.setText(String.valueOf(r(R.string.label_prisjakt)) + ": " + id);
            } else if (id.startsWith("GB:")) {
                this.prisjaktId.setText("GoogleBooks: " + id.substring(3));
            } else if (id.startsWith("AZ:")) {
                this.prisjaktId.setText("Amazon: " + id.substring(3));
            } else if (id.startsWith("AL:")) {
                this.prisjaktId.setText("AdLibris: " + id.substring(3));
            } else {
                this.prisjaktId.setText(id);
            }
            this.prisjaktId.setTag(id);
        }
    }

    private void setNumeral(String defNumeral) {
        this.numeral.setText(defNumeral);
        this.numeral.setTag(defNumeral);
    }

    private void setBarcode(String defBarcode) {
        if (defBarcode != null) {
            this.barcode.setText(String.valueOf(r(R.string.label_barcode)) + ": " + defBarcode);
            this.barcode.setTag(defBarcode);
        }
    }

    private String r(int id) {
        return getResources().getString(id);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuItem edit = menu.add(0, 0, 0, (int) R.string.label_edit);
        edit.setIcon(17301566);
        ZapApp.compat.prepActionMenu(edit);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                Intent intent = new Intent(this, ZapsInventory.class);
                intent.addFlags(67108864);
                startActivity(intent);
                return true;
            case 0:
                Intent intent2 = new Intent(this, EditObject.class);
                intent2.putExtra("objectId", this.objectId);
                startActivity(intent2);
                break;
        }
        return true;
    }

    public void onDestroy() {
        if (this.object != null) {
            this.object.close();
        }
        if (this.categoryCursor != null) {
            this.categoryCursor.close();
        }
        super.onDestroy();
    }
}
