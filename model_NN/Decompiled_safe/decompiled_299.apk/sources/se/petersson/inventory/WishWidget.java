package se.petersson.inventory;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

public class WishWidget extends AppWidgetProvider {
    public static String ACTION_WIDGET_CONFIGURE = "ConfigureWidget";
    public static String ACTION_WIDGET_RECEIVER = "ActionReceiverWidget";
    private static final long ACT_NEXT = 1;
    private static final long ACT_PREV = 0;
    private static PrisjaktLookup pjl = new PrisjaktLookup();
    private SharedPreferences mPreferences = null;

    private String init(Context context) {
        this.mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (this.mPreferences.getString(context.getString(R.string.pref_title_lookup), "null").equals("null")) {
            return "Server not set - can't update";
        }
        return null;
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        String failMsg;
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.wish_widget);
        if (this.mPreferences != null || (failMsg = init(context)) == null) {
            Intent prev = new Intent(context, WishWidget.class);
            prev.setAction(ACTION_WIDGET_RECEIVER);
            prev.putExtra("action", "ACT_PREV");
            Intent next = new Intent(context, WishWidget.class);
            next.setAction(ACTION_WIDGET_RECEIVER);
            next.putExtra("action", "ACT_NEXT");
            remoteViews.setOnClickPendingIntent(R.id.button_left, PendingIntent.getBroadcast(context, 0, prev, 268435456));
            remoteViews.setOnClickPendingIntent(R.id.button_right, PendingIntent.getBroadcast(context, 1, next, 268435456));
            appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);
            return;
        }
        remoteViews.setTextViewText(R.id.text_info, failMsg);
        appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);
    }

    public void onReceive(Context context, Intent intent) {
        if ("android.appwidget.action.APPWIDGET_DELETED".equals(intent.getAction())) {
            int appWidgetId = intent.getExtras().getInt("appWidgetId", 0);
            if (appWidgetId != 0) {
                onDeleted(context, new int[]{appWidgetId});
                return;
            }
            return;
        }
        if (intent.getAction().equals(ACTION_WIDGET_RECEIVER)) {
            String myAction = "null";
            try {
                myAction = intent.getStringExtra("action");
            } catch (NullPointerException e) {
                Log.e("Error", "msg = null");
            }
            Toast.makeText(context, "Action=" + myAction + intent.getExtras().toString(), 0).show();
        }
        super.onReceive(context, intent);
    }

    public static RemoteViews buildUpdate(UpdateService updateService, Uri appWidgetUri) {
        return null;
    }
}
