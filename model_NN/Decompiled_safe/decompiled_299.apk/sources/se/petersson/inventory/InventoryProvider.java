package se.petersson.inventory;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

public class InventoryProvider extends ContentProvider {
    private static final int APPWIDGETS = 101;
    private static final int APPWIDGETS_ID = 102;
    private static final int APPWIDGETS_PRICES = 103;
    public static final String AUTHORITY = "se.petersson.inventory";
    private static final boolean LOGD = true;
    private static final int PRICES = 201;
    private static final int PRICES_ID = 202;
    private static final String TABLE_APPWIDGETS = "appwidgets";
    private static final String TABLE_PRICES = "prices";
    private static final String TAG = "InventoryProvider";
    private static final UriMatcher sUriMatcher = new UriMatcher(-1);
    private DatabaseHelper mOpenHelper;

    public static class AppWidgets implements BaseColumns, AppWidgetsColumns {
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/appwidget";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/appwidget";
        public static final Uri CONTENT_URI = Uri.parse("content://se.petersson.inventory/appwidgets");
        public static final String TWIG_FORECAST_AT = "forecast_at";
        public static final String TWIG_PRICES = "prices";
    }

    public interface AppWidgetsColumns {
        public static final String CONFIGURED = "configured";
        public static final int CONFIGURED_TRUE = 1;
        public static final String COUNTRY_CODE = "countryCode";
        public static final String CURRENCY = "currency";
        public static final int CURRENCY_NZD = 2;
        public static final int CURRENCY_SEK = 1;
        public static final String LAST_UPDATED = "lastUpdated";
        public static final String OWNER = "owner";
        public static final String STATE = "state";
        public static final String TITLE = "title";
    }

    public static class Prices implements BaseColumns, PricesColumns {
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/price";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/price";
        public static final Uri CONTENT_URI = Uri.parse("content://se.petersson.inventory/prices");
    }

    public interface PricesColumns {
        public static final String APPWIDGET_ID = "widgetId";
        public static final String INVENTORYID = "inventoryid";
        public static final String NAME = "name";
        public static final String PJID = "prisjaktid";
        public static final String PRICE = "price";
        public static final String URL = "url";
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        private static final String DATABASE_NAME = "prices.db";
        private static final int DATABASE_VERSION = 3;
        private static final int VER_ADD_METAR = 3;
        private static final int VER_ORIGINAL = 2;

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 3);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE appwidgets (_id INTEGER PRIMARY KEY,title TEXT,state INTEGER,owner STRING,currency STRING,lastUpdated INTEGER,countryCode TEXT,configured INTEGER);");
            db.execSQL("CREATE TABLE prices (_id INTEGER PRIMARY KEY AUTOINCREMENT,widgetId INTEGER,inventoryid INTEGER,prisjaktid STRING,price INTEGER,name TEXT,url TEXT);");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            int version = oldVersion;
            switch (version) {
                case 2:
                    db.execSQL("ALTER TABLE appwidgets ADD COLUMN countryCode TEXT");
                    version = 3;
                    break;
            }
            if (version != 3) {
                Log.w(InventoryProvider.TAG, "Destroying old data during upgrade.");
                db.execSQL("DROP TABLE IF EXISTS appwidgets");
                db.execSQL("DROP TABLE IF EXISTS prices");
                onCreate(db);
            }
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String selection2;
        Log.d(TAG, "delete() with uri=" + uri);
        SQLiteDatabase db = this.mOpenHelper.getWritableDatabase();
        switch (sUriMatcher.match(uri)) {
            case APPWIDGETS /*101*/:
                return db.delete(TABLE_APPWIDGETS, selection, selectionArgs);
            case APPWIDGETS_ID /*102*/:
                long appWidgetId = Long.parseLong(uri.getPathSegments().get(1));
                return db.delete(TABLE_APPWIDGETS, "_id=" + appWidgetId, null) + db.delete("prices", "widgetId=" + appWidgetId, null);
            case APPWIDGETS_PRICES /*103*/:
                long appWidgetId2 = Long.parseLong(uri.getPathSegments().get(1));
                if (selection == null) {
                    selection2 = "";
                } else {
                    selection2 = "(" + selection + ") AND ";
                }
                return db.delete("prices", String.valueOf(selection2) + "widgetId=" + appWidgetId2, selectionArgs);
            case PRICES /*201*/:
                return db.delete("prices", selection, selectionArgs);
            default:
                throw new UnsupportedOperationException();
        }
    }

    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case APPWIDGETS /*101*/:
                return AppWidgets.CONTENT_TYPE;
            case APPWIDGETS_ID /*102*/:
                return AppWidgets.CONTENT_ITEM_TYPE;
            case APPWIDGETS_PRICES /*103*/:
                return Prices.CONTENT_TYPE;
            case PRICES /*201*/:
                return Prices.CONTENT_TYPE;
            case PRICES_ID /*202*/:
                return Prices.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 11 */
    public Uri insert(Uri uri, ContentValues values) {
        Log.d(TAG, "insert() with uri=" + uri);
        SQLiteDatabase db = this.mOpenHelper.getWritableDatabase();
        switch (sUriMatcher.match(uri)) {
            case APPWIDGETS /*101*/:
                long rowId = db.insert(TABLE_APPWIDGETS, AppWidgetsColumns.TITLE, values);
                if (rowId != -1) {
                    return ContentUris.withAppendedId(AppWidgets.CONTENT_URI, rowId);
                }
                return null;
            case APPWIDGETS_PRICES /*103*/:
                values.put(PricesColumns.APPWIDGET_ID, Long.valueOf(Long.parseLong(uri.getPathSegments().get(1))));
                long rowId2 = db.insert("prices", "name", values);
                if (rowId2 != -1) {
                    return ContentUris.withAppendedId(AppWidgets.CONTENT_URI, rowId2);
                }
                return null;
            case PRICES /*201*/:
                long rowId3 = db.insert("prices", "name", values);
                if (rowId3 != -1) {
                    return ContentUris.withAppendedId(Prices.CONTENT_URI, rowId3);
                }
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public boolean onCreate() {
        this.mOpenHelper = new DatabaseHelper(getContext());
        return LOGD;
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.d(TAG, "query() with uri=" + uri);
        SQLiteDatabase db = this.mOpenHelper.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        switch (sUriMatcher.match(uri)) {
            case APPWIDGETS /*101*/:
                qb.setTables(TABLE_APPWIDGETS);
                break;
            case APPWIDGETS_ID /*102*/:
                qb.setTables(TABLE_APPWIDGETS);
                qb.appendWhere("_id=" + uri.getPathSegments().get(1));
                break;
            case APPWIDGETS_PRICES /*103*/:
                qb.setTables("prices");
                qb.appendWhere("widgetId=" + uri.getPathSegments().get(1));
                sortOrder = "inventoryid ASC";
                break;
            case PRICES /*201*/:
                qb.setTables("prices");
                break;
            case PRICES_ID /*202*/:
                qb.setTables("prices");
                qb.appendWhere("_id=" + uri.getPathSegments().get(1));
                break;
        }
        return qb.query(db, projection, selection, selectionArgs, null, null, sortOrder, null);
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Log.d(TAG, "update() with uri=" + uri);
        SQLiteDatabase db = this.mOpenHelper.getWritableDatabase();
        switch (sUriMatcher.match(uri)) {
            case APPWIDGETS /*101*/:
                return db.update(TABLE_APPWIDGETS, values, selection, selectionArgs);
            case APPWIDGETS_ID /*102*/:
                return db.update(TABLE_APPWIDGETS, values, "_id=" + Long.parseLong(uri.getPathSegments().get(1)), null);
            case PRICES /*201*/:
                return db.update("prices", values, selection, selectionArgs);
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        sUriMatcher.addURI(AUTHORITY, TABLE_APPWIDGETS, APPWIDGETS);
        sUriMatcher.addURI(AUTHORITY, "appwidgets/#", APPWIDGETS_ID);
        sUriMatcher.addURI(AUTHORITY, "appwidgets/#/prices", APPWIDGETS_PRICES);
        sUriMatcher.addURI(AUTHORITY, "prices", PRICES);
        sUriMatcher.addURI(AUTHORITY, "prices/#", PRICES_ID);
    }
}
