package se.petersson.inventory;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.preference.EditTextPreference;
import android.util.AttributeSet;
import android.widget.SimpleCursorAdapter;

public class CategoryPreference extends EditTextPreference {
    /* access modifiers changed from: private */
    public Context ctx;

    public CategoryPreference(Context context) {
        super(context);
        this.ctx = context;
    }

    public CategoryPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.ctx = context;
    }

    public CategoryPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.ctx = context;
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        int nCats;
        final DBAdapter db = ZapApp.getDB(this.ctx, false);
        try {
            nCats = db.countCategories();
        } catch (Exception e) {
            nCats = 0;
        }
        if (nCats > 0) {
            builder.setNeutralButton((int) R.string.label_select, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    final SimpleCursorAdapter catAdapter = new SimpleCursorAdapter(CategoryPreference.this.ctx, 17367050, db.getCategories("name asc"), new String[]{"name"}, new int[]{16908308});
                    AlertDialog.Builder builder = new AlertDialog.Builder(CategoryPreference.this.ctx);
                    builder.setTitle((int) R.string.label_category);
                    builder.setAdapter(catAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int position) {
                            Cursor c = (Cursor) catAdapter.getItem(position);
                            dialog.dismiss();
                            CategoryPreference.this.setText(c.getString(c.getColumnIndexOrThrow("name")));
                            CategoryPreference.this.onClick(CategoryPreference.this.getDialog(), -1);
                        }
                    });
                    builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            CategoryPreference.this.setText(null);
                            CategoryPreference.this.onClick(CategoryPreference.this.getDialog(), -1);
                        }
                    });
                    builder.show();
                }
            });
        }
    }
}
