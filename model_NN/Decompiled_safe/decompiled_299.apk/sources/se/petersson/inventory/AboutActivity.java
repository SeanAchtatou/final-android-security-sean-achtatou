package se.petersson.inventory;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;

public class AboutActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(3);
        setContentView((int) R.layout.about);
        getWindow().setFeatureDrawableResource(3, R.drawable.icon);
        try {
            setTitle(String.valueOf(getResources().getString(R.string.app_name)) + " " + getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
        }
    }
}
