package se.petersson.inventory;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

public class ZapApp extends Application {
    static String TAG = "Zap App";
    public static ZapCompat compat = null;
    private static DBAdapter[] db = new DBAdapter[2];

    public void onCreate() {
        super.onCreate();
        compat = ZapCompat.getInstance();
    }

    public static DBAdapter getDB(Context ctx, Boolean onSD) {
        int idx = onSD.booleanValue() ? (char) 1 : 0;
        if (db[idx] == null) {
            db[idx] = new DBAdapter(ctx, onSD.booleanValue());
            db[idx].open();
            if (db[idx] == null) {
                db[idx] = new DBAdapter(ctx, onSD.booleanValue());
                db[idx].open();
            }
            if (db[idx] == null) {
                Toast.makeText(ctx, "Sorry, your database is severely broken...", 1).show();
            }
        }
        return db[idx];
    }

    public static void closeDB(Boolean onSD) {
        int idx = onSD.booleanValue() ? (char) 1 : 0;
        if (db[idx] != null) {
            db[idx].close();
            db[idx] = null;
        }
    }
}
