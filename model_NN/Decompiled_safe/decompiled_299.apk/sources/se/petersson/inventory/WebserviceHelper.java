package se.petersson.inventory;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import se.petersson.inventory.InventoryProvider;

public class WebserviceHelper {
    public static final String COUNTRY_US = "US";
    private static final String[] PROJECTION_APPWIDGET = {"name", "prisjaktid", DBAdapter.KEY_ROWID};
    private static final String TAG = "ForcastHelper";
    static final long WEBSERVICE_TIMEOUT = 30000;
    private static HttpClient sClient = new DefaultHttpClient();
    private static String sUserAgent = null;

    public static void prepareUserAgent(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            sUserAgent = String.format("FakeAgentForNow", info.packageName, info.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Couldn't find package information in PackageManager", e);
        }
    }

    public static Reader queryApi(String url) throws Exception {
        if (sUserAgent == null) {
            throw new Exception("Must prepare user agent string");
        }
        HttpGet request = new HttpGet(url);
        request.setHeader("User-Agent", sUserAgent);
        try {
            HttpResponse response = sClient.execute(request);
            Log.d(TAG, "Request returned status " + response.getStatusLine());
            return new InputStreamReader(response.getEntity().getContent());
        } catch (IOException e) {
            throw new Exception("Problem calling forecast API", e);
        }
    }

    /* JADX INFO: Multiple debug info for r7v1 android.database.Cursor: [D('context' android.content.Context), D('cursor' android.database.Cursor)] */
    /* JADX INFO: Multiple debug info for r7v5 java.util.List<java.util.Map<java.lang.String, java.lang.String>>: [D('list' java.util.List<java.util.Map<java.lang.String, java.lang.String>>), D('pjl' se.petersson.inventory.PrisjaktLookup)] */
    public static void updatePrices(Context context, Uri appWidgetUri) throws Exception {
        String pjIDs;
        String pjid;
        if (sUserAgent == null) {
            prepareUserAgent(context);
        }
        Uri appWidgetPrices = Uri.withAppendedPath(appWidgetUri, InventoryProvider.AppWidgets.TWIG_PRICES);
        ContentResolver resolver = context.getContentResolver();
        DBAdapter db = new DBAdapter(context, false);
        Cursor cursor = null;
        try {
            cursor = db.getObjects("owner is null AND state = 2", DBAdapter.KEY_CHANGED);
            if (cursor == null || !cursor.moveToFirst() || (pjid = cursor.getString(cursor.getColumnIndexOrThrow("prisjaktid"))) == null) {
                pjIDs = "";
            } else {
                pjIDs = String.valueOf("") + pjid.concat(",");
            }
            Log.d(TAG, "find pjIDs=" + pjIDs);
            PrisjaktLookup pjl = new PrisjaktLookup();
            PrisjaktLookup.jsonserver = "http://prisjakt.nu/extern/extern_data.php?";
            List<Map<String, String>> list = pjl.itemDetails(pjIDs);
            resolver.delete(appWidgetPrices, null, null);
            ContentValues values = new ContentValues();
            for (Map<String, String> item : list) {
                Log.d(TAG, "inserting item " + item.toString());
                values.clear();
                values.put("name", (String) item.get("name"));
                resolver.insert(appWidgetPrices, values);
            }
            values.clear();
            values.put("LAST_UPDATED", Long.valueOf(System.currentTimeMillis()));
            resolver.update(appWidgetUri, values, null, null);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
