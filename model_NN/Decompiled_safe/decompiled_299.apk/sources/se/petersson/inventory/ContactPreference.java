package se.petersson.inventory;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.preference.RingtonePreference;
import android.util.AttributeSet;

public class ContactPreference extends RingtonePreference {
    private final ContactAccessor mContactAccessor = ContactAccessor.getInstance();
    private OnPhonePickedListener mPhonePickedListener;

    public interface OnPhonePickedListener {
        void onPhonePicked(Uri uri);
    }

    public ContactPreference(Context context, AttributeSet a) {
        super(context, a);
    }

    public ContactPreference(Context context) {
        super(context);
    }

    public void setOnPhonePickedListener(OnPhonePickedListener listener) {
        if (listener != null) {
            this.mPhonePickedListener = listener;
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareRingtonePickerIntent(Intent intent) {
        Intent tpl = this.mContactAccessor.getContactPickerIntent();
        intent.setAction(tpl.getAction());
        intent.setData(tpl.getData());
        intent.setFlags(16777216);
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!super.onActivityResult(requestCode, resultCode, data)) {
            return true;
        }
        if (data != null) {
            Uri uri = data.getData();
            if (callChangeListener(uri != null ? uri.toString() : "")) {
                onSaveRingtone(uri);
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSaveRingtone(Uri uri) {
        super.onSaveRingtone(uri);
        if (this.mPhonePickedListener != null) {
            this.mPhonePickedListener.onPhonePicked(uri);
        }
    }
}
