package se.petersson.inventory;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Contacts;
import android.provider.SearchRecentSuggestions;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.nullwire.trace.ExceptionHandler;
import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import se.petersson.inventory.InventoryProvider;

public class ZapsInventory extends ListActivity implements SharedPreferences.OnSharedPreferenceChangeListener, View.OnCreateContextMenuListener {
    private static final int MENU_ITEM_ABOUT = 3;
    private static final int MENU_ITEM_CHANGES = 4;
    private static final int MENU_ITEM_CLEAR_HISTORY = 5;
    private static final int MENU_ITEM_MAINT = 6;
    private static final int MENU_ITEM_NEW = 1;
    private static final int MENU_ITEM_SETTINGS = 2;
    private static final int RES_EDIT = 1;
    private static final int RES_SCAN = 0;
    protected static String email;
    public static String jsonServer;
    protected static PrisjaktLookup pjLookup = new PrisjaktLookup();
    /* access modifiers changed from: private */
    public String barcode = null;
    private String barcodeSupport;
    private boolean batchMode = false;
    private DBAdapter db;
    private String defaultNumeral = "1";
    private String defaultOwner = null;
    private int defaultState = 1;
    /* access modifiers changed from: private */
    public boolean doLookup = true;
    final Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public SharedPreferences mPreferences;
    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            ZapsInventory.this.updateResultsInUi(-1);
        }
    };
    private boolean manualBarcode = false;
    MyPhoneStateListener mpsl = null;
    private List<Map<String, String>> myList;
    private String overrideCategory = null;
    private String scannedHit;
    /* access modifiers changed from: private */
    public String searchedBarcode = null;
    private String verName = "";
    private String whenSelecting;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void onCreate(Bundle savedInstanceState) {
        ExceptionHandler.register(this, "http://home.petersson.se/android/inventory/server.php");
        ZapApp.compat.hasOptionsMenu(this);
        requestWindowFeature(MENU_ITEM_CLEAR_HISTORY);
        super.onCreate(savedInstanceState);
        this.mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.mPreferences.registerOnSharedPreferenceChangeListener(this);
        email = findOwner(this);
        try {
            this.verName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
        }
        setContentView((int) R.layout.main);
        setTitle(String.valueOf(r(R.string.app_name)) + " " + this.verName);
        ListView listView = getListView();
        View header = getLayoutInflater().inflate((int) R.layout.mainheader, (ViewGroup) listView, false);
        header.setBackgroundColor(-180075452);
        ((ImageView) header.findViewById(R.id.photo)).setImageResource(R.drawable.icon);
        TextView tv = (TextView) header.findViewById(R.id.header);
        tv.setText((int) R.string.app_name);
        tv.setTextColor(-1);
        listView.addHeaderView(header, null, true);
        getListView().setOnCreateContextMenuListener(this);
        getWindow().setFeatureInt(MENU_ITEM_CLEAR_HISTORY, -2);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        switch (((AdapterView.AdapterContextMenuInfo) menuInfo).position) {
            case 3:
                menu.add(0, 0, 0, (int) R.string.label_batchscanning);
                return;
            case MENU_ITEM_CHANGES /*4*/:
                menu.add(0, 1, 0, (int) R.string.label_feedback_attach);
                menu.add(0, 2, 0, (int) R.string.label_send_log);
                return;
            default:
                return;
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position) {
            case 3:
                this.batchMode = true;
                startScanner();
                break;
            case MENU_ITEM_CHANGES /*4*/:
                switch (item.getItemId()) {
                    case 1:
                        File f = new File(Environment.getExternalStorageDirectory() + "/se.petersson.inventory/inventory");
                        if (f.exists()) {
                            Intent sendIntent = new Intent("android.intent.action.SEND");
                            sendIntent.setType("application/octet-stream");
                            String r = r(R.string.app_name);
                            sendIntent.putExtra("android.intent.extra.EMAIL", new String[]{"jonas@petersson.se"});
                            sendIntent.putExtra("android.intent.extra.SUBJECT", r(R.string.mail_subject));
                            sendIntent.putExtra("android.intent.extra.TEXT", r(R.string.mail_body));
                            sendIntent.putExtra("android.intent.extra.STREAM", Uri.fromFile(f));
                            startActivity(Intent.createChooser(sendIntent, "Zap's SendMail"));
                            return true;
                        }
                        Toast.makeText(this, (int) R.string.msg_create_backup, 1).show();
                        return false;
                    case 2:
                        Intent intent = new Intent("android.intent.action.MAIN");
                        intent.addCategory("android.intent.category.LAUNCHER");
                        intent.setClassName("org.jtb.alogcat", "org.jtb.alogcat.LogActivity");
                        if (getPackageManager().queryIntentActivities(intent, 65536).size() == 0) {
                            Intent mint = new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=alogcat"));
                            if (mint.resolveActivity(getPackageManager()) == null) {
                                Toast.makeText(this, (int) R.string.error_no_market_manual_alogcat, 1).show();
                                return true;
                            }
                            startActivity(mint);
                            Toast.makeText(this, (int) R.string.hint_install_alogcat, 1).show();
                            return true;
                        }
                        Toast.makeText(this, (int) R.string.hint_send_mail, 1).show();
                        startActivity(intent);
                        return false;
                }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position) {
            case 0:
                Intent all = new Intent(this, CategoryBrowser.class);
                all.setAction("android.intent.action.SEARCH");
                all.putExtra("query", "%");
                startActivity(all);
                return;
            case 1:
                onSearchRequested();
                return;
            case 2:
                startActivity(new Intent(this, CategoryBrowser.class));
                return;
            case 3:
                startScanner();
                return;
            case MENU_ITEM_CHANGES /*4*/:
                Intent sendIntent = new Intent("android.intent.action.SEND");
                sendIntent.setType("message/rfc822");
                sendIntent.putExtra("android.intent.extra.EMAIL", new String[]{"jonas@petersson.se"});
                sendIntent.putExtra("android.intent.extra.SUBJECT", r(R.string.mail_subject));
                sendIntent.putExtra("android.intent.extra.TEXT", r(R.string.mail_body));
                startActivity(Intent.createChooser(sendIntent, "Zap's SendMail"));
                return;
            case MENU_ITEM_CLEAR_HISTORY /*5*/:
                String peek = "?device=" + Build.DEVICE + "&finger=" + Build.FINGERPRINT;
                String owner = findOwner(this);
                if (owner != null) {
                    peek = String.valueOf(peek) + "&owner=" + owner;
                }
                showWebDialog("http://home.petersson.se/android/inventory/" + peek, r(R.string.label_help_tips));
                return;
            default:
                return;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        Float fVal;
        String out;
        super.onActivityResult(requestCode, resultCode, intent);
        switch (requestCode) {
            case 0:
                if (resultCode == -1) {
                    this.barcode = intent.getStringExtra("SCAN_RESULT");
                    if (this.barcode == null) {
                        Toast.makeText(this, "Barcode scanner didn't return a barcode even though it claims the scan was OK. Looks broken, please upgrade and try again!", 1).show();
                        return;
                    }
                    this.doLookup = false;
                    String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                    if ("products".equals(this.barcodeSupport) || "any_lookup_any".equals(this.barcodeSupport)) {
                        this.doLookup = true;
                    } else if (format != null && (format.startsWith("UPC") || format.startsWith("EAN"))) {
                        this.doLookup = true;
                    }
                    this.db = ZapApp.getDB(this, null);
                    Cursor c = this.db.getObjects("barcode = " + DatabaseUtils.sqlEscapeString(this.barcode), null);
                    if (c == null) {
                        Toast.makeText(this, "Database access failed - aborting", 1).show();
                        return;
                    } else if (c.getCount() > 0 && c.moveToFirst()) {
                        Long id = Long.valueOf(c.getLong(c.getColumnIndexOrThrow(DBAdapter.KEY_ROWID)));
                        if ("select".equals(this.scannedHit)) {
                            Toast.makeText(this, R.string.msg_edit_existing, 0).show();
                            Intent intent2 = new Intent(this, "edit".equals(this.whenSelecting) ? EditObject.class : ViewObject.class);
                            intent2.putExtra("objectId", id);
                            startActivityForResult(intent2, 1);
                            return;
                        }
                        ContentValues vals = new ContentValues();
                        String strNum = c.getString(c.getColumnIndexOrThrow(DBAdapter.KEY_NUMERAL));
                        String name = c.getString(c.getColumnIndexOrThrow("name"));
                        Float fVal2 = Float.valueOf(1.0f);
                        DecimalFormat decimalFormat = new DecimalFormat("###.##");
                        try {
                            fVal = Float.valueOf(Float.valueOf(decimalFormat.parse(strNum).floatValue()).floatValue() + ((float) ("decrease".equals(this.scannedHit) ? -1 : 1)));
                        } catch (NumberFormatException e) {
                            fVal = Float.valueOf(fVal2.floatValue() + ((float) ("decrease".equals(this.scannedHit) ? -1 : 1)));
                        } catch (NullPointerException e2) {
                            fVal = Float.valueOf(fVal2.floatValue() + ((float) ("decrease".equals(this.scannedHit) ? -1 : 1)));
                        } catch (ParseException e3) {
                            e3.printStackTrace();
                            fVal = Float.valueOf(fVal2.floatValue() + ((float) ("decrease".equals(this.scannedHit) ? -1 : 1)));
                        } catch (Throwable th) {
                            Float fVal3 = Float.valueOf(fVal2.floatValue() + ((float) ("decrease".equals(this.scannedHit) ? -1 : 1)));
                            throw th;
                        }
                        try {
                            out = decimalFormat.format(fVal);
                        } catch (IllegalArgumentException e4) {
                            out = "1";
                        }
                        if (out.endsWith(".00")) {
                            out = out.substring(0, out.length() - 3);
                        }
                        vals.put(DBAdapter.KEY_NUMERAL, out);
                        if (Boolean.valueOf(this.db.updateObject(id.longValue(), vals)).booleanValue()) {
                            Toast.makeText(this, String.valueOf(name) + "\n" + r(R.string.label_numeral) + ": " + strNum + " => " + out, 0).show();
                            if (this.batchMode) {
                                startScanner();
                                return;
                            }
                            return;
                        }
                        Toast.makeText(this, "Sorry, failed to change numeral. Aborting!", 1).show();
                        return;
                    } else if (c.getCount() == 0) {
                        getWindow().setFeatureInt(MENU_ITEM_CLEAR_HISTORY, -1);
                        new Thread() {
                            public void run() {
                                if (ZapsInventory.this.doLookup) {
                                    ZapsInventory.this.doHardWork(ZapsInventory.this.barcode);
                                }
                                ZapsInventory.this.searchedBarcode = ZapsInventory.this.barcode;
                                ZapsInventory.this.mHandler.post(ZapsInventory.this.mUpdateResults);
                            }
                        }.start();
                        return;
                    } else {
                        Toast.makeText(this, "Database matched " + c.getCount() + " item(s) - should not happen.", 1).show();
                        return;
                    }
                } else if (resultCode == 0) {
                    Toast.makeText(this, R.string.label_aborted_barcode, 0).show();
                    this.batchMode = false;
                    getWindow().setFeatureInt(MENU_ITEM_CLEAR_HISTORY, -2);
                    return;
                } else {
                    return;
                }
            case 1:
                getWindow().setFeatureInt(MENU_ITEM_CLEAR_HISTORY, -2);
                if (this.batchMode) {
                    startScanner();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private boolean startScanner() {
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        if ("products".equals(this.barcodeSupport)) {
            intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
        }
        if (intent.resolveActivity(getPackageManager()) == null) {
            Intent mint = new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=zxing"));
            if (mint.resolveActivity(getPackageManager()) == null) {
                if (Build.MODEL.equals("google_sdk")) {
                    Intent fake = new Intent();
                    fake.putExtra("SCAN_RESULT", "5025322412367");
                    onActivityResult(0, -1, fake);
                } else {
                    Toast.makeText(this, (int) R.string.error_no_market_manual_barcode, 1).show();
                }
                return true;
            }
            startActivity(mint);
            Toast.makeText(this, (int) R.string.hint_install_barcode, 1).show();
            return true;
        }
        try {
            startActivityForResult(intent, 0);
        } catch (NullPointerException e) {
            Toast.makeText(this, "Failed to start barcode scanner. Try reinstalling it.", 1).show();
        }
        return false;
    }

    /* JADX INFO: Multiple debug info for r7v22 android.content.ContentValues: [D('catVals' android.content.ContentValues), D('old' android.database.Cursor)] */
    public static boolean saveData(Context ctx, DBAdapter db2, String barcode2, int defaultState2, Map<String, String> item, boolean doCommunity, String defaultOwner2, String defaultNumeral2) {
        ContentValues vals = new ContentValues();
        vals.put("name", item.get("name"));
        vals.put(DBAdapter.KEY_NOTE, item.get(DBAdapter.KEY_NOTE));
        vals.put("state", Integer.valueOf(defaultState2));
        if (defaultOwner2 != null) {
            vals.put("owner", defaultOwner2);
        }
        vals.put(DBAdapter.KEY_BARCODE, barcode2);
        vals.put("prisjaktid", item.get("id"));
        vals.put(DBAdapter.KEY_IMAGE, item.get(DBAdapter.KEY_IMAGE));
        vals.put(DBAdapter.KEY_CHANGED, Long.valueOf(System.currentTimeMillis()));
        vals.put(DBAdapter.KEY_NUMERAL, defaultNumeral2);
        String cat = item.get(DBAdapter.KEY_CATEGORY);
        if (cat == null) {
            cat = "Unknown";
        }
        vals.put(DBAdapter.KEY_CATEGORY, cat);
        vals.put(DBAdapter.KEY_CREATED, Long.valueOf(System.currentTimeMillis()));
        long newObjectId = db2.insertObject(vals);
        Cursor old = db2.getOptions("name = " + DatabaseUtils.sqlEscapeString(cat), null);
        if (old == null || old.getCount() == 0) {
            if (old != null) {
                old.close();
            }
            ContentValues catVals = new ContentValues();
            catVals.put("name", item.get(DBAdapter.KEY_CATEGORY));
            if (db2.insertCategory(catVals) == -1) {
                if (doCommunity) {
                    Toast.makeText(ctx, "Database problem", 0).show();
                }
                return false;
            }
        } else if (old != null) {
            old.close();
        }
        boolean retval = newObjectId != -1;
        if (doCommunity) {
            StatCollector.communityBarcodeStore(ctx, barcode2, item.get("name"), item.get(DBAdapter.KEY_CATEGORY), email);
        }
        return retval;
    }

    private CharSequence[] cast(int max) {
        CharSequence[] out = new CharSequence[max];
        for (int i = 0; i < max; i++) {
            out[i] = (CharSequence) this.myList.get(i).get("name");
        }
        return out;
    }

    /* access modifiers changed from: private */
    public void updateResultsInUi(int presel) {
        Intent intent = new Intent(this, EditObject.class);
        intent.putExtra(DBAdapter.KEY_BARCODE, this.searchedBarcode);
        if (this.myList == null || this.myList.isEmpty()) {
            Toast.makeText(this, R.string.msg_no_match_make_it_up, 0).show();
        } else {
            if (presel == -1) {
                int idhits = 0;
                int i = 0;
                while (i < this.myList.size() && ((String) this.myList.get(i).get("id")) != null) {
                    idhits++;
                    i++;
                }
                if (idhits > 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Candidates");
                    builder.setItems(cast(idhits), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            if (item < 0) {
                                Toast.makeText(ZapsInventory.this, (int) R.string.label_aborted_barcode, 0).show();
                            } else {
                                ZapsInventory.this.updateResultsInUi(item);
                            }
                        }
                    });
                    builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            Toast.makeText(ZapsInventory.this, (int) R.string.label_aborted_barcode, 0).show();
                            ZapsInventory.this.getWindow().setFeatureInt(ZapsInventory.MENU_ITEM_CLEAR_HISTORY, -2);
                        }
                    });
                    try {
                        builder.show();
                        return;
                    } catch (Exception e) {
                        Log.w("Inventory::updateResultsInUi()", "Ignoring exception: " + e.getMessage());
                        return;
                    }
                } else {
                    presel = 0;
                }
            }
            if (presel >= this.myList.size()) {
                presel = this.myList.size() - 1;
            }
            Map<String, String> item = this.myList.get(presel);
            if (this.overrideCategory != null) {
                item.put(DBAdapter.KEY_CATEGORY, this.overrideCategory);
            }
            if (this.batchMode) {
                saveData(this, this.db, this.searchedBarcode, this.defaultState, item, true, this.defaultOwner, this.defaultNumeral);
                Toast.makeText(this, (CharSequence) item.get("name"), 0).show();
                startScanner();
                return;
            }
            intent.putExtra("android.intent.extra.SUBJECT", (String) item.get("name"));
            intent.putExtra("android.intent.extra.TEXT", (String) item.get(DBAdapter.KEY_NOTE));
            intent.putExtra("prisjaktid", (String) item.get("id"));
            intent.putExtra(DBAdapter.KEY_CATEGORY, (String) item.get(DBAdapter.KEY_CATEGORY));
            intent.putExtra(InventoryProvider.PricesColumns.PRICE, (String) item.get(InventoryProvider.PricesColumns.PRICE));
            intent.putExtra(DBAdapter.KEY_IMAGE, (String) item.get(DBAdapter.KEY_IMAGE));
            Toast.makeText(this, R.string.msg_match_found_confirm, 0).show();
        }
        startActivityForResult(intent, 1);
    }

    /* access modifiers changed from: protected */
    public void doHardWork(String barcode2) {
        this.myList = null;
        if (jsonServer != null && jsonServer.startsWith("http")) {
            PrisjaktLookup.jsonserver = String.valueOf(jsonServer) + "extern/extern_data.php?";
            this.myList = Collections.synchronizedList(pjLookup.barcode(barcode2, "rank"));
        }
        if (this.myList == null || this.myList.isEmpty()) {
            this.myList = Collections.synchronizedList(StatCollector.communityBarcodeLookup(this, barcode2, email));
        }
        if (this.myList == null || this.myList.isEmpty()) {
            try {
                StatCollector.badBarcode(this, barcode2, findOwner(this));
            } catch (Exception e) {
            }
        }
    }

    public String findOwner(Context context) {
        if (email != null) {
            return email;
        }
        if (Integer.parseInt(Build.VERSION.SDK) >= MENU_ITEM_CLEAR_HISTORY) {
            try {
                Cursor c = context.getContentResolver().query(Uri.parse("content://com.android.contacts/settings"), new String[]{"account_name"}, null, null, null);
                if (c == null || !c.moveToFirst()) {
                    return null;
                }
                int i = 0;
                while (true) {
                    int i2 = i;
                    email = c.getString(c.getColumnIndexOrThrow("account_name"));
                    i = i2 + 1;
                    if (i2 >= 20 || ((email != null && email.contains("@")) || !c.moveToNext())) {
                        c.close();
                    }
                }
                c.close();
                return email;
            } catch (Exception e) {
            }
        } else {
            Cursor c2 = getContentResolver().query(Contacts.People.CONTENT_URI, new String[]{DBAdapter.KEY_ROWID, "_sync_account"}, null, null, null);
            if (c2 == null) {
                return null;
            }
            if (!c2.moveToFirst()) {
                c2.close();
                return null;
            }
            int i3 = 0;
            while (true) {
                int i4 = i3;
                try {
                    email = c2.getString(c2.getColumnIndexOrThrow("_sync_account"));
                    i3 = i4 + 1;
                    if (i4 >= 20) {
                        break;
                    }
                    try {
                        if ((email != null && email.contains("@")) || !c2.moveToNext()) {
                            break;
                        }
                    } catch (Exception e2) {
                    }
                } catch (Exception e3) {
                }
            }
            c2.close();
            return email;
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        loadPreferences();
    }

    class MyPhoneStateListener extends PhoneStateListener {
        MyPhoneStateListener() {
        }

        public void onServiceStateChanged(ServiceState serviceState) {
            String siteString;
            super.onServiceStateChanged(serviceState);
            String oper = serviceState.getOperatorNumeric();
            if (oper != null && ZapsInventory.jsonServer.equals("null")) {
                if (oper.startsWith("240")) {
                    siteString = "http://www.prisjakt.nu/";
                } else if (oper.startsWith("242")) {
                    siteString = "http://www.prisjakt.no/";
                } else {
                    siteString = "http://pricespy.co.nz/";
                }
                ((TelephonyManager) ZapsInventory.this.getApplicationContext().getSystemService("phone")).listen(ZapsInventory.this.mpsl, 0);
                SharedPreferences.Editor editor = ZapsInventory.this.mPreferences.edit();
                editor.putString(ZapsInventory.this.getString(R.string.pref_title_lookup), siteString);
                editor.commit();
                ZapsInventory.jsonServer = siteString;
                Toast.makeText(ZapsInventory.this, String.valueOf(ZapsInventory.this.r(R.string.label_auto_setsite)) + ": " + siteString, 1).show();
                ZapsInventory.this.mpsl = null;
            }
        }
    }

    private void loadPreferences() {
        jsonServer = this.mPreferences.getString(getString(R.string.pref_title_lookup), "null");
        this.manualBarcode = this.mPreferences.getBoolean(getString(R.string.pref_title_new_item_init), false);
        this.barcodeSupport = this.mPreferences.getString(getString(R.string.pref_barcode_support), "products");
        this.defaultState = Integer.valueOf(this.mPreferences.getString(getString(R.string.pref_default_state), "1")).intValue();
        this.defaultOwner = this.mPreferences.getString("pref_default_owner", null);
        this.defaultNumeral = this.mPreferences.getString("pref_initial_numeral", "1");
        if ("".equals(this.defaultOwner)) {
            this.defaultOwner = null;
        }
        this.whenSelecting = this.mPreferences.getString("pref_when_selecting", "edit");
        this.scannedHit = this.mPreferences.getString("pref_scanned_hit", "select");
        this.overrideCategory = this.mPreferences.getString("pref_override_category", null);
        if ("".equals(this.overrideCategory)) {
            this.overrideCategory = null;
        }
        if (jsonServer.equals("null")) {
            Toast.makeText(this, (int) R.string.msg_guessing_site, 0).show();
            if (this.mpsl == null) {
                this.mpsl = new MyPhoneStateListener();
                ((TelephonyManager) getApplicationContext().getSystemService("phone")).listen(this.mpsl, 1);
                return;
            }
            return;
        }
        PrisjaktLookup.jsonserver = String.valueOf(jsonServer) + "extern/extern_data.php?";
    }

    /* access modifiers changed from: private */
    public String r(int id) {
        return getResources().getString(id);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuItem newitem = menu.add(0, 1, 0, (int) R.string.menu_title_add);
        newitem.setIcon(17301555);
        ZapApp.compat.prepActionMenu(newitem);
        MenuItem settings = menu.add(0, 2, 0, (int) R.string.menu_title_settings);
        settings.setIcon(17301577);
        ZapApp.compat.prepActionMenu(settings);
        MenuItem maint = menu.add(0, (int) MENU_ITEM_MAINT, 0, (int) R.string.menu_title_maint);
        maint.setIcon(17301570);
        ZapApp.compat.prepActionMenu(maint);
        menu.add(0, 3, 0, (int) R.string.menu_title_about).setIcon(17301569);
        menu.add(0, (int) MENU_ITEM_CHANGES, 0, (int) R.string.menu_title_changes).setIcon(17301578);
        menu.add(0, (int) MENU_ITEM_CLEAR_HISTORY, 0, (int) R.string.menu_title_clear_history).setIcon(17301564);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                if (!this.manualBarcode) {
                    startActivity(new Intent(this, EditObject.class));
                    break;
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    final EditText editor = new EditText(this);
                    if ("products".equals(this.barcodeSupport)) {
                        editor.setInputType(2);
                    }
                    builder.setTitle((int) R.string.menu_enter_barcode);
                    builder.setView(editor);
                    builder.setNeutralButton(17039360, (DialogInterface.OnClickListener) null);
                    builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            final String value = editor.getText().toString();
                            if (value != null && value.length() == 0) {
                                ZapsInventory.this.startActivity(new Intent(ZapsInventory.this, EditObject.class));
                            }
                            ZapsInventory.this.getWindow().setFeatureInt(ZapsInventory.MENU_ITEM_CLEAR_HISTORY, -1);
                            new Thread() {
                                public void run() {
                                    ZapsInventory.this.doHardWork(value);
                                    ZapsInventory.this.searchedBarcode = value;
                                    ZapsInventory.this.mHandler.post(ZapsInventory.this.mUpdateResults);
                                }
                            }.start();
                        }
                    });
                    builder.show();
                    break;
                }
            case 2:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case 3:
                startActivity(new Intent(this, AboutActivity.class));
                break;
            case MENU_ITEM_CHANGES /*4*/:
                showWebDialog("http://home.petersson.se/android/inventory/changelog", r(R.string.menu_title_changes));
                break;
            case MENU_ITEM_CLEAR_HISTORY /*5*/:
                new SearchRecentSuggestions(getApplication(), "se.petersson.inventory.SuggestionProvider", 1).clearHistory();
                Toast.makeText(this, (int) R.string.msg_cleared_history, 0).show();
                break;
            case MENU_ITEM_MAINT /*6*/:
                startActivity(new Intent(this, MaintenanceActivity.class));
                break;
        }
        return true;
    }

    private void showWebDialog(String webUrl, String title) {
        ImageView throbber = new ImageView(this);
        throbber.setImageResource(R.drawable.ic_popup_sync_1);
        RotateAnimation rotate = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 1, 0.5f);
        rotate.setDuration(600);
        rotate.setRepeatMode(1);
        rotate.setRepeatCount(-1);
        final FrameLayout frameLayout = new FrameLayout(this);
        final WebView webView = new WebView(this);
        String peek = "?device=" + Build.DEVICE + "&finger=" + Build.FINGERPRINT + "&version=" + this.verName;
        try {
            String peek2 = String.valueOf(peek) + "&owner=" + findOwner(this);
            TelephonyManager mTelephonyMgr = (TelephonyManager) getSystemService("phone");
            peek = String.valueOf(peek2) + "&imsi=" + mTelephonyMgr.getSubscriberId() + "&imei=" + mTelephonyMgr.getDeviceId();
        } catch (Exception e) {
        }
        final String safeUrl = String.valueOf(webUrl.replace(" ", "%20")) + peek;
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                frameLayout.bringChildToFront(webView);
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.equals(safeUrl)) {
                    return false;
                }
                Intent i = new Intent("android.intent.action.VIEW", Uri.parse(url));
                if (i.resolveActivity(this.getPackageManager()) == null) {
                    Toast.makeText(this, String.valueOf(ZapsInventory.this.r(R.string.error_no_url_handler)) + url, 1).show();
                    return true;
                }
                this.startActivity(i);
                return true;
            }
        });
        webView.loadUrl(safeUrl);
        webView.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        frameLayout.addView(throbber);
        throbber.startAnimation(rotate);
        frameLayout.addView(webView);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setView(frameLayout);
        builder.setNeutralButton((CharSequence) null, (DialogInterface.OnClickListener) null);
        builder.setPositiveButton((CharSequence) null, (DialogInterface.OnClickListener) null);
        final String str = safeUrl;
        final AlertDialog show = builder.show();
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.equals(str)) {
                    return false;
                }
                Intent i = new Intent("android.intent.action.VIEW", Uri.parse(url));
                if (i.resolveActivity(this.getPackageManager()) == null) {
                    Toast.makeText(this, String.valueOf(ZapsInventory.this.r(R.string.error_no_url_handler)) + url, 1).show();
                    return true;
                }
                this.startActivity(i);
                return true;
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(this, (int) R.string.msg_network_error, 1).show();
                try {
                    show.cancel();
                } catch (IllegalArgumentException e) {
                }
            }
        });
    }

    public void onResume() {
        super.onResume();
        this.db = ZapApp.getDB(this, false);
        List<Map<String, Object>> topList = new ArrayList<>();
        Map<String, Object> searchdict = new HashMap<>();
        searchdict.put("icon", 17301583);
        searchdict.put("text", String.valueOf(r(R.string.menu_title_search)) + " (" + (this.db != null ? Integer.valueOf(this.db.countObjects(null)) : "-") + ")");
        topList.add(searchdict);
        Map<String, Object> catdict = new HashMap<>();
        catdict.put("icon", 17301556);
        catdict.put("text", r(R.string.label_categories));
        topList.add(catdict);
        Map<String, Object> bardict = new HashMap<>();
        bardict.put("icon", Integer.valueOf((int) R.drawable.barcodescanner));
        bardict.put("text", r(R.string.label_read_barcode));
        topList.add(bardict);
        Map<String, Object> wheredict = new HashMap<>();
        wheredict.put("icon", 17301584);
        wheredict.put("text", r(R.string.label_feedback));
        topList.add(wheredict);
        Map<String, Object> helpdict = new HashMap<>();
        helpdict.put("icon", 17301568);
        helpdict.put("text", r(R.string.label_help_tips));
        topList.add(helpdict);
        setListAdapter(new SimpleAdapter(getApplication(), topList, R.layout.mainheader, new String[]{"icon", "text"}, new int[]{R.id.photo, R.id.header}));
        loadPreferences();
        if (!this.mPreferences.getString("version", "").equals(this.verName)) {
            showWebDialog("http://home.petersson.se/android/inventory/changelog", r(R.string.menu_title_changes));
            SharedPreferences.Editor editor = this.mPreferences.edit();
            editor.putString("version", this.verName);
            editor.commit();
            try {
                StatCollector.pushStats(this, findOwner(this), this.verName);
            } catch (Exception e) {
            }
        }
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            StatCollector.pushStats(this, findOwner(this), this.verName);
        } catch (Exception e) {
        }
    }
}
