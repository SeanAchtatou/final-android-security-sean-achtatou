package se.petersson.inventory;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import java.io.File;

public class DBAdapter {
    private static final String CREATE_TABLE_OBJECTS = "CREATE TABLE IF NOT EXISTS objects (_id INTEGER PRIMARY KEY AUTOINCREMENT, state INTEGER not null, name TEXT NOT NULL, created INTEGER NOT NULL, changed INTEGER, grp INTEGER, category INTEGER, place INTEGER, owner INTEGER, numeral REAL default 1,note TEXT, barcode TEXT, prisjaktid TEXT, image TEXT);";
    private static final String CREATE_TABLE_OPTIONS = "CREATE TABLE IF NOT EXISTS options (_id INTEGER PRIMARY KEY AUTOINCREMENT, kind INTEGER not null, name TEXT NOT NULL, note TEXT, sorting INTEGER, latlonacc TEXT);";
    protected static final String DATABASE_NAME = "inventory";
    private static final int DATABASE_VERSION = 2;
    public static final String KEY_BARCODE = "barcode";
    public static final String KEY_CATEGORY = "category";
    public static final String KEY_CHANGED = "changed";
    public static final String KEY_COUNT = "hits";
    public static final String KEY_CREATED = "created";
    public static final String KEY_GROUP = "grp";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_KIND = "kind";
    public static final String KEY_LATLONACC = "latlonacc";
    public static final String KEY_NAME = "name";
    public static final String KEY_NOTE = "note";
    public static final String KEY_NUMERAL = "numeral";
    public static final String KEY_OWNER = "owner";
    public static final String KEY_PLACE = "place";
    public static final String KEY_PRISJAKTID = "prisjaktid";
    public static final String KEY_ROWID = "_id";
    public static final String KEY_SORTING = "sorting";
    public static final String KEY_STATE = "state";
    public static final int KIND_CATEGORY = 0;
    public static final int KIND_GROUP = 1;
    public static final int STATE_HAVE = 0;
    public static final int STATE_HIDE = 3;
    public static final int STATE_LENT = 2;
    public static final int STATE_WANT = 1;
    private static final String TABLE_OBJECTS = "objects";
    private static final String TABLE_OPTIONS = "options";
    private static final String TAG = "DBAdapter";
    private DatabaseHelper DBHelper;
    private final Context context;
    private SQLiteDatabase db;

    public DBAdapter(Context ctx, boolean onSD) {
        this.context = ctx;
        this.DBHelper = new DatabaseHelper(this.context, onSD);
    }

    private static class DatabaseHelper {
        Context mContext;
        SQLiteDatabase mDB;
        DatabaseHelper mDbHelper;
        boolean sd = false;

        DatabaseHelper(Context context, boolean onSD) {
            this.sd = onSD;
            this.mContext = context;
        }

        public void open() {
            File dbFile;
            if (!this.sd || !Environment.getExternalStorageState().equals("mounted")) {
                dbFile = this.mContext.getDatabasePath(DBAdapter.DATABASE_NAME);
            } else {
                dbFile = new File(Environment.getExternalStorageDirectory(), "se.petersson.inventory/inventory");
            }
            if (dbFile.exists()) {
                Log.i("SQLiteHelper", "Opening database at " + dbFile);
                this.mDB = SQLiteDatabase.openOrCreateDatabase(dbFile, (SQLiteDatabase.CursorFactory) null);
                if (2 > this.mDB.getVersion()) {
                    upgrade();
                    return;
                }
                return;
            }
            File dbDir = new File(dbFile.getParent());
            if (!dbDir.exists()) {
                dbDir.mkdirs();
            }
            Log.i("SQLiteHelper", "Creating database at " + dbFile);
            this.mDB = SQLiteDatabase.openOrCreateDatabase(dbFile, (SQLiteDatabase.CursorFactory) null);
            create();
        }

        public void close() {
            this.mDB.close();
        }

        public void create() {
            Log.w(DBAdapter.TAG, "Creating objects table...");
            this.mDB.execSQL(DBAdapter.CREATE_TABLE_OBJECTS);
            Log.w(DBAdapter.TAG, "Creating options table...");
            this.mDB.execSQL(DBAdapter.CREATE_TABLE_OPTIONS);
        }

        private void myError(String msg) {
            if (this.mContext != null) {
                Toast.makeText(this.mContext, msg, 0).show();
            }
            Log.e(DBAdapter.TAG, msg);
        }

        private void myError(int r) {
            if (this.mContext != null) {
                myError(this.mContext.getResources().getString(r));
            }
        }

        public void upgrade() {
            Log.w(DBAdapter.TAG, "Upgrading database from version " + this.mDB.getVersion() + " to " + 2 + ", data is preservered");
            if (this.mDB.getVersion() < 2) {
                try {
                    this.mDB.execSQL("ALTER TABLE objects ADD COLUMN numeral REAL default 1");
                    myError((int) R.string.msg_database_upgraded);
                } catch (SQLiteException e) {
                    SQLiteException e2 = e;
                    if (e2.getMessage().contains("duplicate column")) {
                        try {
                            this.mDB.execSQL("PRAGMA user_version=2");
                            myError("Corrected version confusion");
                        } catch (SQLiteException e3) {
                            myError("Failed to correct version mismatch: " + e3.getMessage());
                        }
                    } else {
                        myError("Upgrade confusion - have you manually altered the database?\n" + e2.getMessage());
                    }
                }
            }
            ZapApp.closeDB(Boolean.valueOf(this.sd));
        }

        public SQLiteDatabase getWritableDatabase() {
            open();
            return this.mDB;
        }
    }

    public DBAdapter open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        if (this.db == null) {
            return null;
        }
        return this;
    }

    public void close() {
        this.DBHelper.close();
    }

    public long insertCategory(ContentValues initialValues) {
        return insertOption(0, initialValues);
    }

    public long insertGroup(ContentValues initialValues) {
        return insertOption(1, initialValues);
    }

    public long insertOption(int kind, ContentValues initialValues) {
        initialValues.put(KEY_KIND, Integer.valueOf(kind));
        Log.d(TAG, "inserting option with: " + initialValues.toString());
        return this.db.insert(TABLE_OPTIONS, null, initialValues);
    }

    public long insertObject(ContentValues initialValues) {
        Log.d(TAG, "inserting object with: " + initialValues.toString());
        return this.db.insert(TABLE_OBJECTS, null, initialValues);
    }

    public boolean deleteGroup(long rowID) {
        return deleteOption(1, rowID);
    }

    public boolean deleteCategory(int rowID) {
        return deleteOption(0, (long) rowID);
    }

    public boolean deleteOption(int kind, long rowId) {
        Log.d(TAG, "deleting object #" + rowId);
        return this.db.delete(TABLE_OPTIONS, new StringBuilder("_id=").append(rowId).append(" AND ").append(KEY_KIND).append("=").append(kind).toString(), null) > 0;
    }

    public boolean deleteObject(long rowId) {
        Log.d(TAG, "deleting object #" + rowId);
        return this.db.delete(TABLE_OBJECTS, new StringBuilder("_id=").append(rowId).toString(), null) > 0;
    }

    public int countObjects(String match) {
        try {
            Cursor c = this.db.query(TABLE_OBJECTS, new String[]{KEY_ROWID}, match, null, null, null, null);
            int count = c.getCount();
            c.close();
            Log.d(TAG, "counted " + count + " objects");
            return count;
        } catch (IllegalStateException e) {
            Log.w(TAG, "Failed to count objects (will return 0): " + e.getMessage());
            return 0;
        }
    }

    public int countCategories() {
        return countOptions("kind = 0");
    }

    public int countOptions(String match) {
        Cursor c = this.db.query(TABLE_OPTIONS, new String[]{KEY_ROWID}, match, null, null, null, null);
        if (c == null) {
            return 0;
        }
        int count = c.getCount();
        c.close();
        Log.d(TAG, "counted " + count + " objects");
        return count;
    }

    public Cursor getObjects(String match, String orderBy) {
        if (!this.db.isOpen()) {
            Log.w(TAG, "Tried to get objects in a closed database - returning null for now.");
            return null;
        }
        String myOrder = orderBy;
        if (myOrder != null && myOrder.startsWith("name ")) {
            myOrder = "sort_" + orderBy;
        }
        return this.db.query(TABLE_OBJECTS, new String[]{KEY_ROWID, "name", KEY_CREATED, KEY_CHANGED, "state", KEY_GROUP, KEY_CATEGORY, KEY_PLACE, "owner", KEY_BARCODE, "prisjaktid", KEY_NOTE, KEY_NUMERAL, KEY_IMAGE, "CASE lower(substr(name,1,2)) WHEN 'a ' THEN substr(name,3) ELSE CASE lower(substr(name,1,4)) WHEN 'the ' THEN substr(name,5) ELSE CASE lower(substr(name,1,3)) WHEN 'le ' THEN substr(name,4) ELSE CASE lower(substr(name,1,3)) WHEN 'la ' THEN substr(name,4) ELSE CASE lower(substr(name,1,3)) WHEN 'el ' THEN substr(name,4) ELSE name END END END END END AS sort_name"}, match, null, null, null, myOrder);
    }

    public Cursor getCategoriesSum(String orderBy) {
        return this.db.query("options,objects", new String[]{"options._id as _id", "options.name as name", "options.note as note", "count(*) as hits"}, "kind=0 and objects.category = options.name", null, "options.name", null, orderBy);
    }

    public Cursor getTagsSum(String orderBy) {
        return this.db.query("options left outer join objects on objects.grp like '%,'||options._id||',%'", new String[]{"options._id as _id", "options.name as name", "options.note as note", "count(*) as hits", "objects.grp as grp"}, "kind=1", null, "options._id", null, orderBy);
    }

    public Cursor getStateUserSum(String orderBy) {
        return this.db.query(TABLE_OBJECTS, new String[]{KEY_ROWID, "owner", "state", "count(*) as hits"}, null, null, "owner, state", null, orderBy);
    }

    public Cursor getCategories(String orderBy) {
        return getOptions("kind = 0", orderBy);
    }

    public Cursor getGroups(String orderBy) {
        return getOptions("kind = 1", orderBy);
    }

    public Cursor getOptions(String match, String orderBy) {
        return this.db.query(TABLE_OPTIONS, new String[]{KEY_ROWID, "name", KEY_KIND, KEY_NOTE}, match, null, null, null, orderBy);
    }

    public Cursor getObject(long rowID) throws SQLException {
        Cursor mCursor = this.db.query(true, TABLE_OBJECTS, new String[]{KEY_ROWID, "name", KEY_CREATED, KEY_CHANGED, "state", KEY_GROUP, KEY_CATEGORY, KEY_PLACE, "owner", KEY_BARCODE, "prisjaktid", KEY_NOTE, KEY_IMAGE, KEY_NUMERAL}, "_id=" + rowID, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor getTag(long rowID) {
        Cursor c = getOption(rowID);
        if (c == null || c.getLong(c.getColumnIndexOrThrow(KEY_KIND)) == 1) {
            return c;
        }
        return null;
    }

    public Cursor getOption(long rowID) throws SQLException {
        Cursor mCursor = this.db.query(true, TABLE_OPTIONS, new String[]{KEY_ROWID, "name", KEY_KIND, KEY_NOTE, KEY_SORTING}, "_id=" + rowID, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public boolean updateObject(long rowId, ContentValues args) {
        Log.d(TAG, "updating object #" + rowId + " with: " + args.toString());
        return this.db.update(TABLE_OBJECTS, args, new StringBuilder("_id=").append(rowId).toString(), null) > 0;
    }

    public boolean updateOption(long rowId, ContentValues args) {
        Log.d(TAG, "updating option #" + rowId + " with: " + args.toString());
        return this.db.update(TABLE_OPTIONS, args, new StringBuilder("_id=").append(rowId).toString(), null) > 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00fb A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int flushEmptyCategories() {
        /*
            r15 = this;
            r10 = 0
            android.database.sqlite.SQLiteDatabase r0 = r15.db
            java.lang.String r1 = "options left outer join objects on (objects.category = options.name)"
            r2 = 3
            java.lang.String[] r2 = new java.lang.String[r2]
            r3 = 0
            java.lang.String r4 = "options._id as _id"
            r2[r3] = r4
            r3 = 1
            java.lang.String r4 = "options.name as name"
            r2[r3] = r4
            r3 = 2
            java.lang.String r4 = "objects._id as flag"
            r2[r3] = r4
            java.lang.String r3 = "kind = 0"
            r4 = 0
            java.lang.String r5 = "options.name"
            r6 = 0
            r7 = 0
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5, r6, r7)
            if (r8 == 0) goto L_0x0050
            boolean r0 = r8.moveToFirst()
            if (r0 == 0) goto L_0x0050
        L_0x002a:
            java.lang.String r0 = "flag"
            int r0 = r8.getColumnIndexOrThrow(r0)
            java.lang.String r11 = r8.getString(r0)
            if (r11 != 0) goto L_0x004a
            java.lang.String r0 = "_id"
            int r0 = r8.getColumnIndexOrThrow(r0)
            int r0 = r8.getInt(r0)
            java.lang.Integer r13 = java.lang.Integer.valueOf(r0)
            if (r10 != 0) goto L_0x00ce
            java.lang.String r10 = r13.toString()
        L_0x004a:
            boolean r0 = r8.moveToNext()
            if (r0 != 0) goto L_0x002a
        L_0x0050:
            if (r8 == 0) goto L_0x0055
            r8.close()
        L_0x0055:
            android.database.sqlite.SQLiteDatabase r0 = r15.db
            java.lang.String r1 = "options"
            r2 = 3
            java.lang.String[] r2 = new java.lang.String[r2]
            r3 = 0
            java.lang.String r4 = "count(*) as n"
            r2[r3] = r4
            r3 = 1
            java.lang.String r4 = "name"
            r2[r3] = r4
            r3 = 2
            java.lang.String r4 = "group_concat(_id) as dups"
            r2[r3] = r4
            java.lang.String r3 = "kind = 0"
            r4 = 0
            java.lang.String r5 = "name"
            java.lang.String r6 = "n > 1"
            r7 = 0
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5, r6, r7)
            if (r8 == 0) goto L_0x00a8
            boolean r0 = r8.moveToFirst()
            if (r0 == 0) goto L_0x00a8
        L_0x007f:
            java.lang.String r0 = "dups"
            int r0 = r8.getColumnIndexOrThrow(r0)
            java.lang.String r9 = r8.getString(r0)
            if (r9 == 0) goto L_0x00a2
            java.lang.String r0 = ","
            int r0 = r9.indexOf(r0)
            java.lang.Integer r14 = java.lang.Integer.valueOf(r0)
            int r0 = r14.intValue()
            int r0 = r0 + 1
            java.lang.String r12 = r9.substring(r0)
            if (r10 != 0) goto L_0x00e7
            r10 = r12
        L_0x00a2:
            boolean r0 = r8.moveToNext()
            if (r0 != 0) goto L_0x007f
        L_0x00a8:
            if (r8 == 0) goto L_0x00ad
            r8.close()
        L_0x00ad:
            if (r10 == 0) goto L_0x00fb
            android.database.sqlite.SQLiteDatabase r0 = r15.db
            java.lang.String r1 = "options"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "_id IN ("
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r3 = ")"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r3 = 0
            int r0 = r0.delete(r1, r2, r3)
        L_0x00cd:
            return r0
        L_0x00ce:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = ","
            r0.<init>(r1)
            java.lang.String r1 = r13.toString()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r10 = r10.concat(r0)
            goto L_0x004a
        L_0x00e7:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = ","
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r12)
            java.lang.String r0 = r0.toString()
            java.lang.String r10 = r10.concat(r0)
            goto L_0x00a2
        L_0x00fb:
            r0 = 0
            goto L_0x00cd
        */
        throw new UnsupportedOperationException("Method not decompiled: se.petersson.inventory.DBAdapter.flushEmptyCategories():int");
    }

    public boolean deleteAllDataYesImSure(boolean actually) {
        if (!actually) {
            return false;
        }
        try {
            this.db = this.DBHelper.getWritableDatabase();
            Log.w(TAG, "Removing all from objects table...");
            this.db.execSQL("delete from objects");
            Log.w(TAG, "Removing all from options table...");
            this.db.execSQL("delete from options");
            return true;
        } catch (IllegalStateException e) {
            Log.w(TAG, "Delete exception:" + e.getMessage());
            return false;
        }
    }
}
