package se.petersson.inventory;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.text.format.Time;
import android.util.Log;
import android.widget.RemoteViews;
import java.util.LinkedList;
import java.util.Queue;
import se.petersson.inventory.InventoryProvider;

public class UpdateService extends Service implements Runnable {
    public static final String ACTION_UPDATE_ALL = "se.petersson.inventory.UPDATE_ALL";
    private static final int COL_CONFIGURED = 0;
    private static final int COL_LAST_UPDATED = 1;
    private static final long PRICE_CACHE_THROTTLE = 10800000;
    private static final String[] PROJECTION_APPWIDGETS = {InventoryProvider.AppWidgetsColumns.CONFIGURED, InventoryProvider.AppWidgetsColumns.LAST_UPDATED};
    private static final String TAG = "UpdateService";
    private static final long UPDATE_INTERVAL = 21600000;
    private static final long UPDATE_THROTTLE = 1800000;
    private static final long UPDATE_TRIGGER_EARLY = 600000;
    private static Queue<Integer> sAppWidgetIds = new LinkedList();
    private static Object sLock = new Object();
    private static boolean sThreadRunning = false;

    public static void requestUpdate(int[] appWidgetIds) {
        synchronized (sLock) {
            for (int appWidgetId : appWidgetIds) {
                sAppWidgetIds.add(Integer.valueOf(appWidgetId));
            }
        }
    }

    private static boolean hasMoreUpdates() {
        boolean hasMore;
        synchronized (sLock) {
            hasMore = !sAppWidgetIds.isEmpty();
            if (!hasMore) {
                sThreadRunning = false;
            }
        }
        return hasMore;
    }

    private static int getNextUpdate() {
        synchronized (sLock) {
            if (sAppWidgetIds.peek() == null) {
                return 0;
            }
            int intValue = sAppWidgetIds.poll().intValue();
            return intValue;
        }
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        if (ACTION_UPDATE_ALL.equals(intent.getAction())) {
            Log.d(TAG, "Requested UPDATE_ALL action");
            requestUpdate(AppWidgetManager.getInstance(this).getAppWidgetIds(new ComponentName(this, WishWidget.class)));
        }
        synchronized (sLock) {
            if (!sThreadRunning) {
                sThreadRunning = true;
                new Thread(this).start();
            }
        }
    }

    public void run() {
        Log.d(TAG, "Processing thread started");
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        ContentResolver resolver = getContentResolver();
        long now = System.currentTimeMillis();
        while (hasMoreUpdates()) {
            int appWidgetId = getNextUpdate();
            Uri appWidgetUri = ContentUris.withAppendedId(InventoryProvider.AppWidgets.CONTENT_URI, (long) appWidgetId);
            Cursor cursor = null;
            boolean isConfigured = false;
            boolean shouldUpdate = false;
            try {
                cursor = resolver.query(appWidgetUri, PROJECTION_APPWIDGETS, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    isConfigured = cursor.getInt(0) == 1;
                    long lastUpdated = cursor.getLong(1);
                    Log.d(TAG, "Delta since last forecast update is " + ((now - lastUpdated) / 60000) + " min");
                    shouldUpdate = Math.abs(now - lastUpdated) > PRICE_CACHE_THROTTLE;
                }
                if (!isConfigured) {
                    Log.d(TAG, "Not configured yet, so skipping update");
                } else {
                    if (shouldUpdate) {
                        try {
                            WebserviceHelper.updatePrices(this, appWidgetUri);
                        } catch (Exception e) {
                            Log.e(TAG, "Problem parsing prices", e);
                        }
                    }
                    RemoteViews updateViews = null;
                    if (appWidgetManager.getAppWidgetInfo(appWidgetId).provider.getClassName().equals(WishWidget.class.getName())) {
                        updateViews = WishWidget.buildUpdate(this, appWidgetUri);
                    }
                    if (updateViews != null) {
                        appWidgetManager.updateAppWidget(appWidgetId, updateViews);
                    }
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        Time time = new Time();
        time.set(System.currentTimeMillis() + UPDATE_INTERVAL + UPDATE_TRIGGER_EARLY);
        time.hour -= time.hour % 6;
        time.minute = 0;
        time.second = 0;
        long nextUpdate = time.toMillis(false) - UPDATE_TRIGGER_EARLY;
        long nowMillis = System.currentTimeMillis();
        if (nextUpdate - nowMillis < UPDATE_THROTTLE) {
            Log.d(TAG, "Calculated next update too early, throttling for a few minutes");
            nextUpdate = nowMillis + UPDATE_THROTTLE;
        }
        Log.d(TAG, "Requesting next update at " + nextUpdate + ", in " + ((nextUpdate - nowMillis) / 60000) + " min");
        Intent intent = new Intent(ACTION_UPDATE_ALL);
        intent.setClass(this, UpdateService.class);
        ((AlarmManager) getSystemService("alarm")).set(0, nextUpdate, PendingIntent.getService(this, 0, intent, 0));
        stopSelf();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
