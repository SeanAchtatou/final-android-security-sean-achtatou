package se.petersson.inventory;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.BadParcelableException;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.widget.Toast;

public class SettingsActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private final ContactAccessor mContactAccessor = ContactAccessor.getInstance();

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        refreshSettings();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
        } catch (BadParcelableException e) {
            Toast.makeText(this, "Your phone firmware seems unable to handle this. Upgrade may help.\n" + e.getMessage(), 1).show();
        }
        String value = null;
        if (resultCode == -1) {
            Uri contactData = data.getData();
            Cursor c = getContentResolver().query(contactData, null, null, null, null);
            if (c.moveToFirst()) {
                String string = c.getString(c.getColumnIndexOrThrow(this.mContactAccessor.getNameIndex()));
                value = contactData.toString();
            }
            c.close();
        } else {
            Toast.makeText(this, (int) R.string.msg_no_selecting_mine, 0).show();
        }
        SharedPreferences.Editor editor = getPreferenceManager().getSharedPreferences().edit();
        editor.putString("pref_default_owner", value);
        editor.commit();
    }

    private String r(int id) {
        return getResources().getString(id);
    }

    private void refreshSettings() {
        Uri contactData;
        Cursor c;
        Resources resources = getResources();
        ListPreference lookup = (ListPreference) findPreference(resources.getString(R.string.pref_title_lookup));
        if (lookup != null) {
            lookup.setSummary(lookup.getEntry());
        }
        ListPreference defaultState = (ListPreference) findPreference(resources.getString(R.string.pref_default_state));
        if (defaultState != null) {
            defaultState.setSummary(defaultState.getEntry());
        }
        SharedPreferences mPreferences = getPreferenceManager().getSharedPreferences();
        String default_owner = mPreferences.getString("pref_default_owner", null);
        ContactPreference owner = (ContactPreference) findPreference("pref_default_owner");
        String name = r(R.string.me);
        String value = default_owner;
        if (!(value == null || (c = getContentResolver().query((contactData = Uri.parse(value)), null, null, null, null)) == null)) {
            if (c.moveToFirst()) {
                name = c.getString(c.getColumnIndexOrThrow(this.mContactAccessor.getNameIndex()));
                String value2 = contactData.toString();
            }
            c.close();
        }
        owner.setSummary(name);
        EditTextPreference overrideCategory = (EditTextPreference) findPreference("pref_override_category");
        String hint = r(R.string.pref_no_override_category);
        if (overrideCategory != null) {
            String cat = overrideCategory.getText();
            if (cat != null && cat.length() > 0) {
                hint = "'" + cat + "'";
            }
            overrideCategory.setSummary(hint);
        }
        EditTextPreference numeralPref = (EditTextPreference) findPreference("pref_initial_numeral");
        String string = mPreferences.getString("pref_initial_numeral", "1");
        numeralPref.setSummary(numeralPref.getText());
        ListPreference barcodeSupport = (ListPreference) findPreference(resources.getString(R.string.pref_barcode_support));
        barcodeSupport.setSummary(barcodeSupport.getEntry());
        ListPreference whenSelecting = (ListPreference) findPreference("pref_when_selecting");
        whenSelecting.setSummary(whenSelecting.getEntry());
        ListPreference scannedHit = (ListPreference) findPreference("pref_scanned_hit");
        scannedHit.setSummary(scannedHit.getEntry());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setTitle(String.valueOf(getResources().getString(R.string.label_settings_for)) + " " + getResources().getString(R.string.app_name) + " " + getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
            addPreferencesFromResource(R.xml.preferences);
            getPreferenceScreen().removePreference(getPreferenceScreen().findPreference("version"));
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        } catch (PackageManager.NameNotFoundException e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        refreshSettings();
    }
}
