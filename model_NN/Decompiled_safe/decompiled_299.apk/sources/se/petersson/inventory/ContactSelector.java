package se.petersson.inventory;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.Contacts;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ContactSelector extends Activity {
    public static final int PICK_CONTACT = 1;
    private Button btnContacts;
    private TextView txtContacts;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.contactselector);
        this.btnContacts = (Button) findViewById(R.id.btn_contacts);
        this.txtContacts = (TextView) findViewById(R.id.txt_contacts);
        this.btnContacts.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ContactSelector.this.startActivityForResult(new Intent("android.intent.action.PICK", Contacts.People.CONTENT_URI), 1);
            }
        });
    }

    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case 1:
                if (resultCode == -1) {
                    Cursor c = getContentResolver().query(data.getData(), null, null, null, null);
                    if (c.moveToFirst()) {
                        this.txtContacts.setText(c.getString(c.getColumnIndexOrThrow("name")));
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }
}
