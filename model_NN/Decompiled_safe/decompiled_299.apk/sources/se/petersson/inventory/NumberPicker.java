package se.petersson.inventory;

import android.content.Context;
import android.os.Handler;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.NumberKeyListener;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.text.DecimalFormat;

public class NumberPicker extends LinearLayout implements View.OnClickListener, View.OnFocusChangeListener, View.OnLongClickListener {
    /* access modifiers changed from: private */
    public static final char[] DIGIT_CHARACTERS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '.'};
    private static final String TAG = "NumberPicker";
    public static final Formatter TWO_DIGIT_FORMATTER = new Formatter() {
        final Object[] mArgs = new Object[1];
        final StringBuilder mBuilder = new StringBuilder();
        final java.util.Formatter mFmt = new java.util.Formatter(this.mBuilder);

        public String toString(Float value) {
            String out;
            try {
                out = new DecimalFormat("###.##").format(value);
            } catch (IllegalArgumentException e) {
                out = "1";
            }
            if (out.endsWith(".00")) {
                return out.substring(0, out.length() - 3);
            }
            return out;
        }
    };
    protected Float mCurrent;
    /* access modifiers changed from: private */
    public boolean mDecrement;
    private NumberPickerButton mDecrementButton;
    /* access modifiers changed from: private */
    public String[] mDisplayedValues;
    private Formatter mFormatter;
    /* access modifiers changed from: private */
    public final Handler mHandler;
    /* access modifiers changed from: private */
    public boolean mIncrement;
    private NumberPickerButton mIncrementButton;
    private OnChangedListener mListener;
    /* access modifiers changed from: private */
    public final InputFilter mNumberInputFilter;
    protected Float mPrevious;
    private final Runnable mRunnable;
    /* access modifiers changed from: private */
    public long mSpeed;
    private final EditText mText;

    public interface Formatter {
        String toString(Float f);
    }

    public interface OnChangedListener {
        void onChanged(NumberPicker numberPicker, Float f, Float f2);
    }

    public NumberPicker(Context context) {
        this(context, null);
    }

    public NumberPicker(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, se.petersson.inventory.NumberPicker, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public NumberPicker(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        this.mRunnable = new Runnable() {
            public void run() {
                if (NumberPicker.this.mIncrement) {
                    NumberPicker.this.changeCurrent(Float.valueOf(NumberPicker.this.mCurrent.floatValue() + 1.0f));
                    NumberPicker.this.mHandler.postDelayed(this, NumberPicker.this.mSpeed);
                } else if (NumberPicker.this.mDecrement) {
                    NumberPicker.this.changeCurrent(Float.valueOf(NumberPicker.this.mCurrent.floatValue() - 1.0f));
                    NumberPicker.this.mHandler.postDelayed(this, NumberPicker.this.mSpeed);
                }
            }
        };
        this.mFormatter = TWO_DIGIT_FORMATTER;
        this.mSpeed = 300;
        setOrientation(1);
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.number_picker, (ViewGroup) this, true);
        this.mHandler = new Handler();
        InputFilter inputFilter = new NumberPickerInputFilter(this, null);
        this.mNumberInputFilter = new NumberRangeKeyListener(this, null);
        this.mIncrementButton = (NumberPickerButton) findViewById(R.id.increment);
        this.mIncrementButton.setOnClickListener(this);
        this.mIncrementButton.setOnLongClickListener(this);
        this.mIncrementButton.setNumberPicker(this);
        this.mDecrementButton = (NumberPickerButton) findViewById(R.id.decrement);
        this.mDecrementButton.setOnClickListener(this);
        this.mDecrementButton.setOnLongClickListener(this);
        this.mDecrementButton.setNumberPicker(this);
        this.mText = (EditText) findViewById(R.id.timepicker_input);
        this.mText.setOnFocusChangeListener(this);
        this.mText.setFilters(new InputFilter[]{inputFilter});
        this.mText.setRawInputType(2);
        if (!isEnabled()) {
            setEnabled(false);
        }
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        this.mIncrementButton.setEnabled(enabled);
        this.mDecrementButton.setEnabled(enabled);
        this.mText.setEnabled(enabled);
    }

    public void setOnChangeListener(OnChangedListener listener) {
        this.mListener = listener;
    }

    public void setFormatter(Formatter formatter) {
        this.mFormatter = formatter;
    }

    public void setRange(int start, int end) {
        updateView();
    }

    public void setCurrent(Float current) {
        this.mCurrent = current;
        updateView();
    }

    public void setSpeed(long speed) {
        this.mSpeed = speed;
    }

    public void onClick(View v) {
        validateInput(this.mText);
        if (!this.mText.hasFocus()) {
            this.mText.requestFocus();
        }
        if (R.id.increment == v.getId()) {
            changeCurrent(Float.valueOf(this.mCurrent.floatValue() + 1.0f));
        } else if (R.id.decrement == v.getId()) {
            changeCurrent(Float.valueOf(this.mCurrent.floatValue() - 1.0f));
        }
    }

    private String formatNumber(Float mCurrent2) {
        if (this.mFormatter != null) {
            return this.mFormatter.toString(mCurrent2);
        }
        return String.valueOf(mCurrent2);
    }

    /* access modifiers changed from: protected */
    public void changeCurrent(Float current) {
        this.mPrevious = this.mCurrent;
        this.mCurrent = current;
        notifyChange();
        updateView();
    }

    /* access modifiers changed from: protected */
    public void notifyChange() {
        if (this.mListener != null) {
            this.mListener.onChanged(this, this.mPrevious, this.mCurrent);
        }
    }

    /* access modifiers changed from: protected */
    public void updateView() {
        this.mText.setText(formatNumber(this.mCurrent));
        this.mText.setSelection(this.mText.getText().length());
    }

    private void validateCurrentView(CharSequence str) {
        Float val = this.mPrevious;
        try {
            Float val2 = Float.valueOf(str.toString());
            if (this.mCurrent != val2) {
                this.mPrevious = this.mCurrent;
                this.mCurrent = val2;
                notifyChange();
            }
        } catch (NumberFormatException e) {
            if (this.mCurrent != val) {
                this.mPrevious = this.mCurrent;
                this.mCurrent = val;
                notifyChange();
            }
        } catch (NullPointerException e2) {
            if (this.mCurrent != val) {
                this.mPrevious = this.mCurrent;
                this.mCurrent = val;
                notifyChange();
            }
        } catch (Throwable th) {
            if (this.mCurrent != val) {
                this.mPrevious = this.mCurrent;
                this.mCurrent = val;
                notifyChange();
            }
            throw th;
        }
        updateView();
    }

    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
            validateInput(v);
        }
    }

    private void validateInput(View v) {
        String str = String.valueOf(((TextView) v).getText());
        if ("".equals(str)) {
            updateView();
        } else {
            validateCurrentView(str);
        }
    }

    public boolean onLongClick(View v) {
        this.mText.clearFocus();
        if (R.id.increment == v.getId()) {
            this.mIncrement = true;
            this.mHandler.post(this.mRunnable);
        } else if (R.id.decrement == v.getId()) {
            this.mDecrement = true;
            this.mHandler.post(this.mRunnable);
        }
        return true;
    }

    public void cancelIncrement() {
        this.mIncrement = false;
    }

    public void cancelDecrement() {
        this.mDecrement = false;
    }

    private class NumberPickerInputFilter implements InputFilter {
        private NumberPickerInputFilter() {
        }

        /* synthetic */ NumberPickerInputFilter(NumberPicker numberPicker, NumberPickerInputFilter numberPickerInputFilter) {
            this();
        }

        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (NumberPicker.this.mDisplayedValues == null) {
                return NumberPicker.this.mNumberInputFilter.filter(source, start, end, dest, dstart, dend);
            }
            CharSequence filtered = String.valueOf(source.subSequence(start, end));
            String str = String.valueOf(String.valueOf(String.valueOf(dest.subSequence(0, dstart))) + ((Object) filtered) + ((Object) dest.subSequence(dend, dest.length()))).toLowerCase();
            for (String val : NumberPicker.this.mDisplayedValues) {
                if (val.toLowerCase().startsWith(str)) {
                    return filtered;
                }
            }
            return "";
        }
    }

    private class NumberRangeKeyListener extends NumberKeyListener {
        private NumberRangeKeyListener() {
        }

        /* synthetic */ NumberRangeKeyListener(NumberPicker numberPicker, NumberRangeKeyListener numberRangeKeyListener) {
            this();
        }

        public int getInputType() {
            return 12290;
        }

        /* access modifiers changed from: protected */
        public char[] getAcceptedChars() {
            return NumberPicker.DIGIT_CHARACTERS;
        }

        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            CharSequence filtered = super.filter(source, start, end, dest, dstart, dend);
            if (filtered == null) {
                filtered = source.subSequence(start, end);
            }
            String result = String.valueOf(String.valueOf(dest.subSequence(0, dstart))) + ((Object) filtered) + ((Object) dest.subSequence(dend, dest.length()));
            if ("".equals(result)) {
                return result;
            }
            return filtered;
        }
    }

    public Float getCurrent() {
        return this.mCurrent;
    }

    public String getCurrentString() {
        return this.mText.getText().toString();
    }
}
