package se.petersson.inventory;

import android.content.SearchRecentSuggestionsProvider;

public class SearchSuggestionSampleProvider extends SearchRecentSuggestionsProvider {
    static final String AUTHORITY = "se.petersson.inventory.SuggestionProvider";
    static final int MODE = 1;

    public SearchSuggestionSampleProvider() {
        setupSuggestions(AUTHORITY, 1);
    }
}
