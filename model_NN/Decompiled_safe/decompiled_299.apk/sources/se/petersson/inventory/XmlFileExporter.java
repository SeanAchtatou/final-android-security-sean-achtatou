package se.petersson.inventory;

import android.content.Context;
import android.widget.ArrayAdapter;
import java.io.IOException;
import org.xmlpull.v1.XmlSerializer;

public class XmlFileExporter {
    private Context ctx;
    private final ContactAccessor mContactAccessor = ContactAccessor.getInstance();
    private ArrayAdapter<CharSequence> statesAdapter;
    final String tag = toString();

    public XmlFileExporter(Context ctx2) {
        this.ctx = ctx2;
        this.statesAdapter = ArrayAdapter.createFromResource(ctx2, R.array.states, 17367048);
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0061 A[Catch:{ Exception -> 0x00b1 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean Export(java.lang.String r12, se.petersson.inventory.DBAdapter r13, java.lang.String r14, java.lang.String r15) {
        /*
            r11 = this;
            r10 = 0
            r9 = 1
            java.io.File r4 = new java.io.File
            r4.<init>(r12)
            r4.createNewFile()     // Catch:{ IOException -> 0x0076 }
        L_0x000a:
            r2 = 0
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0091 }
            r3.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0091 }
            org.xmlpull.v1.XmlSerializer r5 = android.util.Xml.newSerializer()
            java.lang.String r6 = "UTF-8"
            r5.setOutput(r3, r6)     // Catch:{ Exception -> 0x00b1 }
            r6 = 0
            r7 = 1
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)     // Catch:{ Exception -> 0x00b1 }
            r5.startDocument(r6, r7)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r6 = "http://xmlpull.org/v1/doc/features.html#indent-output"
            r7 = 1
            r5.setFeature(r6, r7)     // Catch:{ Exception -> 0x00b1 }
            r6 = 0
            java.lang.String r7 = "Workbook"
            r5.startTag(r6, r7)     // Catch:{ Exception -> 0x00b1 }
            r6 = 0
            java.lang.String r7 = "xmlns"
            java.lang.String r8 = "urn:schemas-microsoft-com:office:spreadsheet"
            r5.attribute(r6, r7, r8)     // Catch:{ Exception -> 0x00b1 }
            r6 = 0
            java.lang.String r7 = "xmlns:ss"
            java.lang.String r8 = "urn:schemas-microsoft-com:office:spreadsheet"
            r5.attribute(r6, r7, r8)     // Catch:{ Exception -> 0x00b1 }
            if (r15 != 0) goto L_0x00ad
            android.database.Cursor r0 = r13.getCategories(r14)     // Catch:{ Exception -> 0x00b1 }
            if (r0 == 0) goto L_0x005f
            boolean r6 = r0.moveToFirst()     // Catch:{ Exception -> 0x00b1 }
            if (r6 == 0) goto L_0x005f
        L_0x004c:
            java.lang.String r6 = "name"
            int r6 = r0.getColumnIndexOrThrow(r6)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r6 = r0.getString(r6)     // Catch:{ Exception -> 0x00b1 }
            r11.serializeCategory(r5, r13, r6, r14)     // Catch:{ Exception -> 0x00b1 }
            boolean r6 = r0.moveToNext()     // Catch:{ Exception -> 0x00b1 }
            if (r6 != 0) goto L_0x004c
        L_0x005f:
            if (r0 == 0) goto L_0x0064
            r0.close()     // Catch:{ Exception -> 0x00b1 }
        L_0x0064:
            r6 = 0
            java.lang.String r7 = "Workbook"
            r5.endTag(r6, r7)     // Catch:{ Exception -> 0x00b1 }
            r5.endDocument()     // Catch:{ Exception -> 0x00b1 }
            r5.flush()     // Catch:{ Exception -> 0x00b1 }
            r3.close()     // Catch:{ Exception -> 0x00b1 }
            r2 = r3
            r6 = r9
        L_0x0075:
            return r6
        L_0x0076:
            r1 = move-exception
            java.lang.String r6 = r11.tag
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "IOException in createNewFile() method:"
            r7.<init>(r8)
            java.lang.String r8 = r1.toString()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            android.util.Log.e(r6, r7)
            goto L_0x000a
        L_0x0091:
            r6 = move-exception
            r1 = r6
            java.lang.String r6 = r11.tag
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "FileNotFoundException: can't create FileOutputStream: "
            r7.<init>(r8)
            java.lang.String r8 = r1.toString()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            android.util.Log.e(r6, r7)
            r6 = r10
            goto L_0x0075
        L_0x00ad:
            r11.serializeCategory(r5, r13, r15, r14)     // Catch:{ Exception -> 0x00b1 }
            goto L_0x0064
        L_0x00b1:
            r6 = move-exception
            r1 = r6
            java.lang.String r6 = r11.tag
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "Exception while creating xml file:"
            r7.<init>(r8)
            java.lang.String r8 = r1.toString()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            android.util.Log.e(r6, r7)
            r2 = r3
            r6 = r10
            goto L_0x0075
        */
        throw new UnsupportedOperationException("Method not decompiled: se.petersson.inventory.XmlFileExporter.Export(java.lang.String, se.petersson.inventory.DBAdapter, java.lang.String, java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0095  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void serializeCategory(org.xmlpull.v1.XmlSerializer r20, se.petersson.inventory.DBAdapter r21, java.lang.String r22, java.lang.String r23) throws java.lang.IllegalArgumentException, java.lang.IllegalStateException, java.io.IOException {
        /*
            r19 = this;
            r4 = 0
            java.lang.String r5 = "ss:Worksheet"
            r0 = r20
            r1 = r4
            r2 = r5
            r0.startTag(r1, r2)
            r4 = 0
            java.lang.String r5 = "ss:Name"
            r0 = r20
            r1 = r4
            r2 = r5
            r3 = r22
            r0.attribute(r1, r2, r3)
            r4 = 0
            java.lang.String r5 = "Table"
            r0 = r20
            r1 = r4
            r2 = r5
            r0.startTag(r1, r2)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "category="
            r4.<init>(r5)
            java.lang.String r5 = android.database.DatabaseUtils.sqlEscapeString(r22)
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r15 = r4.toString()
            r0 = r21
            r1 = r15
            r2 = r23
            android.database.Cursor r14 = r0.getObjects(r1, r2)
            if (r14 == 0) goto L_0x0093
            boolean r4 = r14.moveToFirst()
            if (r4 == 0) goto L_0x0093
        L_0x0044:
            r4 = 0
            java.lang.String r5 = "Row"
            r0 = r20
            r1 = r4
            r2 = r5
            r0.startTag(r1, r2)
            r4 = 8
            java.lang.String[] r13 = new java.lang.String[r4]
            r4 = 0
            java.lang.String r5 = "name"
            r13[r4] = r5
            r4 = 1
            java.lang.String r5 = "note"
            r13[r4] = r5
            r4 = 2
            java.lang.String r5 = "state"
            r13[r4] = r5
            r4 = 3
            java.lang.String r5 = "barcode"
            r13[r4] = r5
            r4 = 4
            java.lang.String r5 = "numeral"
            r13[r4] = r5
            r4 = 5
            java.lang.String r5 = "owner"
            r13[r4] = r5
            r4 = 6
            java.lang.String r5 = "image"
            r13[r4] = r5
            r4 = 7
            java.lang.String r5 = "grp"
            r13[r4] = r5
            int r15 = r13.length
            r4 = 0
            r18 = r4
        L_0x007e:
            r0 = r18
            r1 = r15
            if (r0 < r1) goto L_0x00ad
            r4 = 0
            java.lang.String r5 = "Row"
            r0 = r20
            r1 = r4
            r2 = r5
            r0.endTag(r1, r2)
            boolean r4 = r14.moveToNext()
            if (r4 != 0) goto L_0x0044
        L_0x0093:
            if (r14 == 0) goto L_0x0098
            r14.close()
        L_0x0098:
            r4 = 0
            java.lang.String r5 = "Table"
            r0 = r20
            r1 = r4
            r2 = r5
            r0.endTag(r1, r2)
            r4 = 0
            java.lang.String r5 = "ss:Worksheet"
            r0 = r20
            r1 = r4
            r2 = r5
            r0.endTag(r1, r2)
            return
        L_0x00ad:
            r12 = r13[r18]
            java.lang.String r4 = "numeral"
            boolean r4 = r12.equals(r4)
            if (r4 == 0) goto L_0x013f
            java.lang.String r4 = "Number"
            r16 = r4
        L_0x00bb:
            int r4 = r14.getColumnIndexOrThrow(r12)
            java.lang.String r17 = r14.getString(r4)
            java.lang.String r4 = "owner"
            boolean r4 = r12.equals(r4)
            if (r4 == 0) goto L_0x00fc
            if (r17 == 0) goto L_0x00fc
            r0 = r19
            android.content.Context r0 = r0.ctx
            r4 = r0
            android.content.ContentResolver r4 = r4.getContentResolver()
            android.net.Uri r5 = android.net.Uri.parse(r17)
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r10 = r4.query(r5, r6, r7, r8, r9)
            boolean r4 = r10.moveToFirst()
            if (r4 == 0) goto L_0x00f9
            r0 = r19
            se.petersson.inventory.ContactAccessor r0 = r0.mContactAccessor
            r4 = r0
            java.lang.String r4 = r4.getNameIndex()
            int r4 = r10.getColumnIndexOrThrow(r4)
            java.lang.String r17 = r10.getString(r4)
        L_0x00f9:
            r10.close()
        L_0x00fc:
            java.lang.String r4 = "state"
            boolean r4 = r12.equals(r4)
            if (r4 == 0) goto L_0x011b
            java.lang.Integer r4 = java.lang.Integer.valueOf(r17)
            int r11 = r4.intValue()
            r0 = r19
            android.widget.ArrayAdapter<java.lang.CharSequence> r0 = r0.statesAdapter
            r4 = r0
            java.lang.Object r22 = r4.getItem(r11)
            java.lang.CharSequence r22 = (java.lang.CharSequence) r22
            java.lang.String r17 = r22.toString()
        L_0x011b:
            java.lang.String r4 = "grp"
            boolean r4 = r12.equals(r4)
            if (r4 == 0) goto L_0x012e
            java.util.Set r4 = se.petersson.inventory.EditObject.tagsStringToSet(r17)
            r0 = r21
            r1 = r4
            java.lang.String r17 = se.petersson.inventory.EditObject.tagsSetToString(r0, r1)
        L_0x012e:
            r0 = r19
            r1 = r20
            r2 = r17
            r3 = r16
            r0.serializeCell(r1, r2, r3)
            int r4 = r18 + 1
            r18 = r4
            goto L_0x007e
        L_0x013f:
            java.lang.String r4 = "String"
            r16 = r4
            goto L_0x00bb
        */
        throw new UnsupportedOperationException("Method not decompiled: se.petersson.inventory.XmlFileExporter.serializeCategory(org.xmlpull.v1.XmlSerializer, se.petersson.inventory.DBAdapter, java.lang.String, java.lang.String):void");
    }

    private void serializeCell(XmlSerializer serializer, String value, String type) throws IllegalArgumentException, IllegalStateException, IOException {
        serializer.startTag(null, "Cell");
        serializer.startTag(null, "Data");
        serializer.attribute(null, "ss:Type", type);
        if (value != null) {
            serializer.text(value);
        }
        serializer.endTag(null, "Data");
        serializer.endTag(null, "Cell");
    }
}
