package wall.bt.wp.P115;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import com.xl.util.GenUtil;

public class MoreApk extends Activity {
    private String strURL = "";
    private WebView wv_Page;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.file_web_view);
        this.strURL = getResources().getString(R.string.webapkurl);
        GenUtil.systemPrintln("FileWebView strURL = " + this.strURL);
        if (this.strURL.toLowerCase().indexOf("http:") < 0) {
            this.strURL = "http://" + this.strURL;
        }
        this.wv_Page = (WebView) findViewById(R.id.wv_page);
        this.wv_Page.getSettings().setJavaScriptEnabled(true);
        this.wv_Page.clearCache(true);
        this.wv_Page.setBackgroundColor(-16777216);
        GenUtil.systemPrintln("strURL = " + this.strURL);
        this.wv_Page.loadUrl(this.strURL);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.wv_Page.canGoBack()) {
            this.wv_Page.goBack();
            return true;
        }
        quitSystem();
        return true;
    }

    public void quitSystem() {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.txt_quit_title)).setMessage(getResources().getString(R.string.txt_quit_body)).setIcon((int) R.drawable.icon).setPositiveButton(getResources().getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MoreApk.this.finish();
            }
        }).setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    public boolean onCreateOptionsMenu(Menu paramMenu) {
        getMenuInflater().inflate(R.menu.apk_menu, paramMenu);
        return super.onCreateOptionsMenu(paramMenu);
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        super.onOptionsItemSelected(paramMenuItem);
        switch (paramMenuItem.getItemId()) {
            case R.id.refresh /*2131230783*/:
                this.wv_Page.reload();
                return true;
            default:
                return true;
        }
    }
}
