package com.google.gson;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;

final class bg {
    private bg() {
    }

    static Class<?> a(Class<?> cls) {
        return Array.newInstance(cls, 0).getClass();
    }

    static boolean a(Type type) {
        return type instanceof Class ? ((Class) type).isArray() : type instanceof GenericArrayType;
    }

    static Class<?> b(Type type) {
        Type type2;
        while (true) {
            Type type3 = type2;
            if (type3 instanceof Class) {
                return (Class) type3;
            }
            if (type3 instanceof ParameterizedType) {
                type2 = ((ParameterizedType) type3).getRawType();
            } else if (type3 instanceof GenericArrayType) {
                return a(b(((GenericArrayType) type3).getGenericComponentType()));
            } else {
                if (type3 instanceof WildcardType) {
                    type2 = ((WildcardType) type3).getUpperBounds()[0];
                } else {
                    throw new IllegalArgumentException("Type '" + type3 + "' is not a Class, " + "ParameterizedType, or GenericArrayType. Can't extract class.");
                }
            }
        }
    }
}
