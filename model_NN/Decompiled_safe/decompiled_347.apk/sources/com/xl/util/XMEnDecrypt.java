package com.xl.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class XMEnDecrypt {
    public static boolean XMEncryptFile(String strOldFile, String strNewFile) {
        File oldFile = new File(strOldFile);
        if (!oldFile.exists() || !oldFile.isFile()) {
            return false;
        }
        File newFile = new File(strNewFile);
        try {
            FileInputStream finstream = new FileInputStream(oldFile);
            FileOutputStream foutStream = new FileOutputStream(newFile);
            byte[] btBuffer = new byte[4096];
            while (true) {
                int iLen = finstream.read(btBuffer);
                if (iLen != -1) {
                    byte[] btTmp = new byte[4096];
                    for (int iIndex = 0; iIndex < iLen; iIndex++) {
                        btTmp[iIndex] = (byte) (((btBuffer[iIndex] & 15) << 4) | (((btBuffer[iIndex] & 240) >> 4) & 15));
                    }
                    foutStream.write(btTmp, 0, iLen);
                } else {
                    finstream.close();
                    foutStream.close();
                    return true;
                }
            }
        } catch (Exception e) {
            Exception exc = e;
            return false;
        }
    }

    public static boolean XMDecryptFile(String strOldFile, String strNewFile) {
        File oldFile = new File(strOldFile);
        if (!oldFile.exists() || !oldFile.isFile()) {
            return false;
        }
        File newFile = new File(strNewFile);
        if (!newFile.exists()) {
            return false;
        }
        try {
            FileInputStream finstream = new FileInputStream(oldFile);
            FileOutputStream foutStream = new FileOutputStream(newFile);
            byte[] btBuffer = new byte[4096];
            while (true) {
                int iLen = finstream.read(btBuffer);
                if (iLen != -1) {
                    byte[] btTmp = new byte[4096];
                    for (int iIndex = 0; iIndex < iLen; iIndex++) {
                        btTmp[iIndex] = (byte) (((btBuffer[iIndex] & 15) << 4) | (((btBuffer[iIndex] & 240) >> 4) & 15));
                    }
                    foutStream.write(btTmp, 0, iLen);
                } else {
                    finstream.close();
                    foutStream.close();
                    return true;
                }
            }
        } catch (Exception e) {
            Exception exc = e;
            return false;
        }
    }

    public static byte[] XMEncryptString(byte[] btOld) {
        if (btOld.length <= 0) {
            return null;
        }
        try {
            byte[] btBuffer = new byte[btOld.length];
            for (int iIndex = 0; iIndex < btOld.length; iIndex++) {
                btBuffer[iIndex] = (byte) (((btOld[iIndex] & 15) << 4) | (((btOld[iIndex] & 240) >> 4) & 15));
            }
            return btBuffer;
        } catch (Exception e) {
            return null;
        }
    }

    public static byte[] XMDecryptString(byte[] btOld) {
        if (btOld.length <= 0) {
            return null;
        }
        try {
            byte[] btBuffer = new byte[btOld.length];
            for (int iIndex = 0; iIndex < btOld.length; iIndex++) {
                btBuffer[iIndex] = (byte) (((btOld[iIndex] & 15) << 4) | (((btOld[iIndex] & 240) >> 4) & 15));
            }
            return btBuffer;
        } catch (Exception e) {
            return null;
        }
    }
}
