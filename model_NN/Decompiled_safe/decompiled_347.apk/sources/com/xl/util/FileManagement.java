package com.xl.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import java.io.File;
import java.util.HashMap;
import wall.bt.wp.P115.R;

public class FileManagement extends Activity {
    private ListView lvFile;
    /* access modifiers changed from: private */
    public TextView tvDir;
    /* access modifiers changed from: private */
    public XMUtility xmUtility = new XMUtility();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.file_list);
        this.tvDir = (TextView) findViewById(R.id.tvPathText);
        this.tvDir.setText("/sdcard");
        this.lvFile = (ListView) findViewById(R.id.listview);
        this.lvFile.setAdapter((ListAdapter) new SimpleAdapter(this, this.xmUtility.getFileDir("/sdcard"), R.layout.list_row, new String[]{"icon", "fname"}, new int[]{R.id.img, R.id.filename}));
        this.lvFile.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                FileManagement.this.getFoldList(FileManagement.this.xmUtility.arr.get(arg2));
            }
        });
        this.lvFile.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                HashMap hashMap = (HashMap) arg0.getSelectedItem();
                String strDir = (String) hashMap.get("fdir");
                boolean bFold = Boolean.parseBoolean(hashMap.get("bfold").toString());
                FileManagement.this.tvDir.setText(strDir);
                if (!bFold) {
                    FileManagement.this.onExit(strDir);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void getFoldList(HashMap<String, Object> map) {
        String strDir = (String) map.get("fdir");
        if (strDir.length() <= 0) {
            return;
        }
        if (Boolean.parseBoolean(map.get("bfold").toString())) {
            this.tvDir.setText(strDir);
            this.xmUtility.getFileDir(strDir);
            this.lvFile.invalidateViews();
            return;
        }
        onExit(strDir);
    }

    /* access modifiers changed from: protected */
    public void getFoldList(String strDir) {
        String strParent;
        if (strDir.length() > 0 && (strParent = new File(strDir).getParent()) != "/") {
            this.tvDir.setText(strParent);
            this.xmUtility.getFileDir(strParent);
            this.lvFile.invalidateViews();
        }
    }

    /* access modifiers changed from: protected */
    public void delList(HashMap<String, Object> map) {
        if (map != null) {
            String strDir = (String) map.get("fdir");
            if (strDir.length() > 1) {
                boolean bFold = Boolean.parseBoolean(map.get("bfold").toString());
                File file = new File(strDir);
                if (bFold) {
                    if (file.exists() && file.isDirectory()) {
                        file.delete();
                    }
                } else if (file.exists() && file.isFile()) {
                    file.delete();
                }
                String strCurDir = this.xmUtility.getCurDirectory();
                this.tvDir.setText(strCurDir);
                this.xmUtility.getFileDir(strCurDir);
                this.lvFile.invalidateViews();
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            String strDir = this.xmUtility.getCurDirectory();
            if (!strDir.equalsIgnoreCase("/sdcard")) {
                getFoldList(strDir);
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onExit(String strFile) {
        Intent intent = getIntent();
        Bundle bound = intent.getExtras();
        bound.putString("SelFile", strFile);
        intent.putExtras(bound);
        setResult(-1, intent);
        finish();
    }

    /* access modifiers changed from: protected */
    public void openFile(String strFile) {
    }
}
