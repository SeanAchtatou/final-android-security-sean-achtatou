package random.display.provider.flickr;

import android.graphics.Bitmap;
import android.util.Log;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import random.display.provider.AbstractImageProvider;
import random.display.utils.HttpClientUtils;

public class FlickrImageProvider extends AbstractImageProvider {
    private static final String GETSIZE_URL = "http://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=%s&photo_id=%s&format=json";
    private static final String LARGE = "Large";
    public static final int LARGE_FIRST = 0;
    private static final String MEDIUM = "Medium";
    private static final String NONSTANDARD = "Medium 640";
    public static final int NONSTRANDARD_FIRST = 1;
    private static final String SEARCH_URL = "http://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%s&text=%s&sort=%s&format=json";
    private String apiKey = null;
    private JSONArray cache;
    private String currentImageId;
    private String currentKeyword;
    private int fetchCount = 0;
    private int fetchType;
    private String lastUrl;

    /* renamed from: random  reason: collision with root package name */
    private Random f0random = new Random(System.currentTimeMillis());

    public String getApiKey() {
        return this.apiKey;
    }

    public void setApiKey(String apiKey2) {
        this.apiKey = apiKey2;
    }

    public int getFetchType() {
        return this.fetchType;
    }

    public void setFetchType(int fetchType2) {
        this.fetchType = fetchType2;
    }

    public void initialize() {
        if (getApiKey() == null) {
            setApiKey("c0e4cc7e03c3613dc1592d3c7991b424");
        }
    }

    public Bitmap downloadImage(String[] keywords) {
        try {
            if (this.cache == null || this.fetchCount >= 5) {
                String strKeyword = keywords[this.f0random.nextInt(keywords.length)];
                this.currentKeyword = strKeyword;
                Log.d("FlickrImageProvider", "choosed keyword: " + strKeyword);
                this.cache = listPhotos(strKeyword);
                this.fetchCount = 0;
            } else {
                this.fetchCount++;
            }
            this.currentImageId = this.cache.getJSONObject(this.f0random.nextInt(this.cache.length())).getString("id");
            String strUrl = getUrl(listPhotoSize(this.currentImageId));
            this.lastUrl = strUrl;
            Log.d("FlickrImageProvider", "download image from: " + strUrl);
            Bitmap bm = HttpClientUtils.downloadImage("GET", strUrl);
            if (bm == null && getImageProviderListener() != null) {
                getImageProviderListener().onDownloadFailed();
            }
            return bm;
        } catch (Exception e) {
            Exception e2 = e;
            Log.e("FlickrImageProvider", "list photo with " + "", e2);
            e2.printStackTrace();
            return null;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    private String getUrl(JSONArray sizes) throws JSONException {
        HashMap<String, String> hmSizes = new HashMap<>();
        for (int i = 0; i < sizes.length(); i++) {
            JSONObject jsSize = sizes.getJSONObject(i);
            hmSizes.put(jsSize.getString("label"), jsSize.getString("source"));
        }
        if (1 == getFetchType()) {
            if (hmSizes.containsKey(NONSTANDARD)) {
                return (String) hmSizes.get(NONSTANDARD);
            }
            if (hmSizes.containsKey(LARGE)) {
                return (String) hmSizes.get(LARGE);
            }
            if (hmSizes.containsKey(MEDIUM)) {
                return (String) hmSizes.get(MEDIUM);
            }
            return null;
        } else if (hmSizes.containsKey(LARGE)) {
            return (String) hmSizes.get(LARGE);
        } else {
            if (hmSizes.containsKey(NONSTANDARD)) {
                return (String) hmSizes.get(NONSTANDARD);
            }
            if (hmSizes.containsKey(MEDIUM)) {
                return (String) hmSizes.get(MEDIUM);
            }
            return null;
        }
    }

    private JSONArray listPhotoSize(String id) throws IOException, JSONException {
        String strJSON = HttpClientUtils.doGet(String.format(GETSIZE_URL, getApiKey(), id)).substring("jsonFlickrApi(".length());
        JSONObject jsRoot = new JSONObject(strJSON.substring(0, strJSON.length() - 1));
        if ("ok".equals(jsRoot.getString("stat"))) {
            return jsRoot.getJSONObject("sizes").getJSONArray("size");
        }
        Log.e("FlickrImageProvider", "list photo size with id " + id + " failed, code: " + jsRoot.getString("err"));
        return null;
    }

    private JSONArray listPhotos(String keyword) throws IOException, JSONException {
        String strJSON = HttpClientUtils.doGet(String.format(SEARCH_URL, getApiKey(), URLEncoder.encode(keyword), getSortBy())).substring("jsonFlickrApi(".length());
        JSONObject jsRoot = new JSONObject(strJSON.substring(0, strJSON.length() - 1));
        if ("ok".equals(jsRoot.getString("stat"))) {
            return jsRoot.getJSONObject("photos").getJSONArray("photo");
        }
        Log.e("FlickrImageProvider", "list photo with " + keyword + " failed, code: " + jsRoot.getString("err"));
        return null;
    }

    private String getSortBy() {
        float randomNum = this.f0random.nextFloat();
        if (((double) randomNum) < 0.6d) {
            return "relevance";
        }
        if (((double) randomNum) < 0.75d) {
            return "interestingness-asc";
        }
        return "date-posted-desc";
    }

    public String getCurrentKeyword() {
        return this.currentKeyword;
    }

    public String getCurrentSource() {
        return "flickr";
    }

    public String getCurrentUrl() {
        return this.lastUrl;
    }

    public String getCurrentImageId() {
        return this.currentImageId;
    }
}
