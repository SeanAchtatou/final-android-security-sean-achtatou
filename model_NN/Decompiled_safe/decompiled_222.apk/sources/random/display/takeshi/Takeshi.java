package random.display.takeshi;

import android.util.Log;
import java.util.Random;
import random.display.base.AbstractRandomDisplayActivity;
import random.display.provider.ImageProvider;
import random.display.provider.flickr.FlickrImageProvider;
import random.display.provider.picasa.PicasaImageProvider;

public class Takeshi extends AbstractRandomDisplayActivity {
    private FlickrImageProvider fip = new FlickrImageProvider();
    private PicasaImageProvider pip = new PicasaImageProvider();

    /* renamed from: random  reason: collision with root package name */
    private Random f2random = new Random(System.currentTimeMillis());

    public Takeshi() {
        this.fip.setKeywords(new String[]{"金城武 -P -護照 -飯店 -三菱 -捷運 -回家 -plurk -合照 -小金 -噢 -P127099 -麻油雞", "Kaneshiro Takeshi -EA -figure -ISO -plurk"});
        this.pip.setKeywords(new String[]{"\"金城武\" -\"聲蕙\" -\"小胖\" -\"拍照\" -\"肉鬆\" -\"室友\" -\"臉上\" -\"Ruby Wang\"", "\"kaneshiro\" \"takeshi\""});
    }

    /* access modifiers changed from: protected */
    public String getStorageFolderName() {
        return "/takeshi/";
    }

    /* access modifiers changed from: protected */
    public String[] getKeywords() {
        return null;
    }

    /* access modifiers changed from: protected */
    public ImageProvider getImageProvider() {
        if (this.f2random.nextBoolean()) {
            Log.i("Takeshi", "use flickr");
            return this.fip;
        }
        Log.i("Takeshi", "use picasa");
        return this.pip;
    }

    /* access modifiers changed from: protected */
    public String getAdmobId() {
        return "a14e4cce22cee79";
    }

    /* access modifiers changed from: protected */
    public int getAdmobFilter() {
        return 2;
    }
}
