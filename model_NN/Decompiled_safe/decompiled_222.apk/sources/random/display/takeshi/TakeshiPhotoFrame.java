package random.display.takeshi;

import random.display.provider.ImageProvider;
import random.display.provider.SDStorageImageProvider;
import random.display.widget.AbstractPhotoFrame;

public class TakeshiPhotoFrame extends AbstractPhotoFrame {
    private static SDStorageImageProvider m_ImageProvider = new SDStorageImageProvider("/takeshi/");

    /* access modifiers changed from: protected */
    public String getActivityName() {
        return "random.display.takeshi/random.display.takeshi.Takeshi";
    }

    /* access modifiers changed from: protected */
    public int getWaitTimeout() {
        return 20000;
    }

    /* access modifiers changed from: protected */
    public ImageProvider getImageProvider() {
        return m_ImageProvider;
    }
}
