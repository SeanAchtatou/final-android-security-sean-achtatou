package random.display.log;

import java.util.ArrayList;
import java.util.List;

public class UsageLogger {
    private static UsageLogger m_Instance;
    private List<UsageLog> m_Queue = new ArrayList();
    /* access modifiers changed from: private */
    public boolean m_Run;
    private Thread m_UploadThread;

    public static UsageLogger getInstance() {
        synchronized (UsageLogger.class) {
            if (m_Instance == null) {
                m_Instance = new UsageLogger();
                m_Instance.run();
            }
        }
        return m_Instance;
    }

    public void addUsageLog(UsageLog log) {
        synchronized (this) {
            this.m_Queue.add(log);
        }
    }

    public void stop() {
        this.m_Run = false;
    }

    public void run() {
        this.m_Run = true;
        this.m_UploadThread = new Thread(new Runnable() {
            public void run() {
                while (UsageLogger.this.m_Run) {
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    UsageLogger.this.doUpload();
                }
            }
        });
        this.m_UploadThread.start();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r4.hasNext() != false) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        r7.m_Queue.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0023, code lost:
        monitor-exit(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        random.display.utils.HttpClientUtils.doPost("http://randomdisplay.isgreat.org/stat.php", "data=" + java.net.URLEncoder.encode(r1.toString()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0041, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0042, code lost:
        android.util.Log.e("UsageLogger", "post to server error", r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r1.put(r4.next().toJSON());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005c, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        android.util.Log.e("UsageLogger", "convert to json error", r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000c, code lost:
        r1 = new org.json.JSONArray();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0011, code lost:
        monitor-enter(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r4 = r7.m_Queue.iterator();
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void doUpload() {
        /*
            r7 = this;
            monitor-enter(r7)
            java.util.List<random.display.log.UsageLog> r4 = r7.m_Queue     // Catch:{ all -> 0x004b }
            int r4 = r4.size()     // Catch:{ all -> 0x004b }
            if (r4 != 0) goto L_0x000b
            monitor-exit(r7)     // Catch:{ all -> 0x004b }
        L_0x000a:
            return
        L_0x000b:
            monitor-exit(r7)     // Catch:{ all -> 0x004b }
            org.json.JSONArray r1 = new org.json.JSONArray
            r1.<init>()
            monitor-enter(r7)
            java.util.List<random.display.log.UsageLog> r4 = r7.m_Queue     // Catch:{ all -> 0x0066 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x0066 }
        L_0x0018:
            boolean r5 = r4.hasNext()     // Catch:{ all -> 0x0066 }
            if (r5 != 0) goto L_0x004e
            java.util.List<random.display.log.UsageLog> r4 = r7.m_Queue     // Catch:{ all -> 0x0066 }
            r4.clear()     // Catch:{ all -> 0x0066 }
            monitor-exit(r7)     // Catch:{ all -> 0x0066 }
            java.lang.String r2 = r1.toString()
            java.lang.String r4 = "http://randomdisplay.isgreat.org/stat.php"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0041 }
            java.lang.String r6 = "data="
            r5.<init>(r6)     // Catch:{ IOException -> 0x0041 }
            java.lang.String r6 = java.net.URLEncoder.encode(r2)     // Catch:{ IOException -> 0x0041 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0041 }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x0041 }
            random.display.utils.HttpClientUtils.doPost(r4, r5)     // Catch:{ IOException -> 0x0041 }
            goto L_0x000a
        L_0x0041:
            r4 = move-exception
            r0 = r4
            java.lang.String r4 = "UsageLogger"
            java.lang.String r5 = "post to server error"
            android.util.Log.e(r4, r5, r0)
            goto L_0x000a
        L_0x004b:
            r4 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x004b }
            throw r4
        L_0x004e:
            java.lang.Object r3 = r4.next()     // Catch:{ all -> 0x0066 }
            random.display.log.UsageLog r3 = (random.display.log.UsageLog) r3     // Catch:{ all -> 0x0066 }
            org.json.JSONObject r5 = r3.toJSON()     // Catch:{ JSONException -> 0x005c }
            r1.put(r5)     // Catch:{ JSONException -> 0x005c }
            goto L_0x0018
        L_0x005c:
            r5 = move-exception
            r0 = r5
            java.lang.String r5 = "UsageLogger"
            java.lang.String r6 = "convert to json error"
            android.util.Log.e(r5, r6, r0)     // Catch:{ all -> 0x0066 }
            goto L_0x0018
        L_0x0066:
            r4 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0066 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: random.display.log.UsageLogger.doUpload():void");
    }
}
