package random.display.puzzle;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import random.display.takeshi.R;

public class Answer extends Activity {
    private static final String TAG = "Answer";
    private ImageView answerView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.answer);
        String answerPath = getIntent().getExtras().getString(TAG);
        Log.d(TAG, "answerPath: " + answerPath);
        this.answerView = (ImageView) findViewById(R.id.answerView);
        this.answerView.setImageBitmap(BitmapFactory.decodeFile(answerPath));
    }
}
