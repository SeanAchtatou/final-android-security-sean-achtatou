package random.display.puzzle;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public abstract class AbstractPuzzleView extends View implements View.OnTouchListener {
    private int[] arrayDst;
    private Rect[] arraySrc;
    private int index = -1;
    private Bitmap pic;
    private int selectedIndex = -1;

    /* access modifiers changed from: protected */
    public abstract int[] getArrayDst();

    /* access modifiers changed from: protected */
    public abstract Rect[] getArraySrc();

    public int getSelectedIndex() {
        return this.selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex2) {
        if (-1 != this.selectedIndex) {
            invalidate(this.arraySrc[this.selectedIndex]);
        }
        this.selectedIndex = selectedIndex2;
        if (-1 != this.selectedIndex) {
            invalidate(this.arraySrc[this.selectedIndex]);
        }
    }

    public AbstractPuzzleView(Context context) {
        super(context);
        setOnTouchListener(this);
    }

    public void setBitmap(Bitmap pic2) {
        this.pic = pic2;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.arraySrc = getArraySrc();
        this.arrayDst = getArrayDst();
        scalePic(w, h);
        super.onSizeChanged(w, h, oldw, oldh);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private void scalePic(int w, int h) {
        Matrix matrix = new Matrix();
        matrix.postScale(((float) w) / ((float) this.pic.getWidth()), ((float) h) / ((float) this.pic.getHeight()));
        this.pic = Bitmap.createBitmap(this.pic, 0, 0, this.pic.getWidth(), this.pic.getHeight(), matrix, true);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.pic != null) {
            Paint paint = new Paint();
            paint.setColor(-65536);
            paint.setAntiAlias(true);
            paint.setStrokeWidth(2.0f);
            paint.setStyle(Paint.Style.STROKE);
            for (int i = 0; i < this.arraySrc.length; i++) {
                canvas.drawBitmap(this.pic, this.arraySrc[this.arrayDst[i]], this.arraySrc[i], (Paint) null);
            }
            if (-1 != this.selectedIndex) {
                canvas.drawRect(this.arraySrc[this.selectedIndex], paint);
            }
            canvas.save(31);
        }
    }

    public void move(int a, int b) {
        int tmp = this.arrayDst[a];
        this.arrayDst[a] = this.arrayDst[b];
        this.arrayDst[b] = tmp;
        invalidate();
    }

    public int getIndex(int x, int y) {
        for (int i = 0; i < this.arraySrc.length; i++) {
            if (this.arraySrc[i].contains(x, y)) {
                return i;
            }
        }
        return -1;
    }

    public boolean isMatch() {
        for (int i = 0; i < this.arrayDst.length; i++) {
            if (this.arrayDst[i] != i) {
                return false;
            }
        }
        return true;
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0) {
            if (-1 == this.index) {
                this.index = getIndex((int) event.getX(), (int) event.getY());
            } else {
                move(this.index, getIndex((int) event.getX(), (int) event.getY()));
                this.index = -1;
                if (isMatch()) {
                    Toast.makeText(getContext(), "Match!!", 0).show();
                }
            }
            setSelectedIndex(this.index);
        }
        return false;
    }
}
