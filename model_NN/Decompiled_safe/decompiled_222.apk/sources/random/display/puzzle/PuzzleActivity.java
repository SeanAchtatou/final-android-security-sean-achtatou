package random.display.puzzle;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.io.File;
import random.display.provider.flickr.FlickrImageProvider;
import random.display.takeshi.R;

public class PuzzleActivity extends Activity {
    private static final String TAG = "PuzzleActivity";
    private static int nextCount = 0;
    /* access modifiers changed from: private */
    public LinearLayout admobContainer;
    private String admobId;
    private FrameLayout mainLayout;
    /* access modifiers changed from: private */
    public boolean mode3x3 = true;
    private Bitmap pic;
    /* access modifiers changed from: private */
    public String puzzleSrcPath;
    private AbstractPuzzleView puzzleView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.puzzle);
        this.puzzleSrcPath = getIntent().getExtras().getString("PuzzleSrc");
        this.admobId = getIntent().getExtras().getString("AdmobId");
        this.mainLayout = (FrameLayout) findViewById(R.id.puzzleLayout);
        this.admobContainer = (LinearLayout) findViewById(R.id.admodLayout);
        showPuzzleModeDialog();
    }

    private String getPuzzleSrc() {
        File[] allFiles = new File(this.puzzleSrcPath).listFiles();
        int i = nextCount;
        nextCount = i + 1;
        return allFiles[i % allFiles.length].getAbsolutePath();
    }

    private void showPuzzleModeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select mode");
        builder.setItems(new CharSequence[]{"3x3", "4x4"}, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int item) {
                switch (item) {
                    case FlickrImageProvider.LARGE_FIRST:
                        PuzzleActivity.this.mode3x3 = true;
                        break;
                    case 1:
                        PuzzleActivity.this.mode3x3 = false;
                        break;
                    default:
                        PuzzleActivity.this.finish();
                        break;
                }
                PuzzleActivity.this.playPuzzle();
            }
        });
        builder.create().show();
    }

    public void loadPuzzleSrc() {
        delAnswerPic();
        if (this.pic != null) {
            this.pic.recycle();
        }
        this.pic = BitmapFactory.decodeFile(getPuzzleSrc());
    }

    /* access modifiers changed from: private */
    public void playPuzzle() {
        loadPuzzleSrc();
        admodInit();
        if (this.mode3x3) {
            Log.i(TAG, "mode 3x3");
            puzzle3x3Initial();
        } else {
            Log.i(TAG, "mode 4x4");
            puzzle4x4Initial();
        }
        savePic(this.pic);
    }

    private void puzzle3x3Initial() {
        this.mainLayout.removeAllViews();
        this.puzzleView = null;
        this.puzzleView = new Puzzle3x3(this);
        this.puzzleView.setBitmap(this.pic);
        this.mainLayout.addView(this.puzzleView);
    }

    private void puzzle4x4Initial() {
        this.mainLayout.removeAllViews();
        this.puzzleView = null;
        this.puzzleView = new Puzzle4x4(this);
        this.puzzleView.setBitmap(this.pic);
        this.mainLayout.addView(this.puzzleView);
    }

    private void admodInit() {
        AdView adView = new AdView(this, AdSize.BANNER, this.admobId);
        this.admobContainer.removeAllViews();
        this.admobContainer.addView(adView);
        AdRequest adRequest = new AdRequest();
        adView.setAdListener(new AdListener() {
            public void onDismissScreen(Ad arg0) {
                PuzzleActivity.this.admobContainer.setVisibility(8);
            }

            public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
                PuzzleActivity.this.admobContainer.setVisibility(8);
            }

            public void onLeaveApplication(Ad arg0) {
                PuzzleActivity.this.admobContainer.setVisibility(8);
            }

            public void onPresentScreen(Ad arg0) {
                PuzzleActivity.this.admobContainer.setVisibility(0);
            }

            public void onReceiveAd(Ad arg0) {
                PuzzleActivity.this.admobContainer.setVisibility(0);
            }
        });
        adView.loadAd(adRequest);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, getText(R.string.show_anwser));
        menu.add(0, 1, 1, getText(R.string.play_next_puzzle));
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case FlickrImageProvider.LARGE_FIRST:
                Intent i = new Intent(this, Answer.class);
                i.putExtra("Answer", String.valueOf(this.puzzleSrcPath) + "/answer.png");
                startActivity(i);
                break;
            case 1:
                playPuzzle();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void savePic(Bitmap pic2) {
        if (pic2 != null) {
            final Bitmap clonedPic = pic2.copy(pic2.getConfig(), false);
            new Thread(new Runnable() {
                /* JADX WARNING: Removed duplicated region for block: B:19:0x006a A[SYNTHETIC, Splitter:B:19:0x006a] */
                /* JADX WARNING: Removed duplicated region for block: B:26:0x007b A[SYNTHETIC, Splitter:B:26:0x007b] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r7 = this;
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder
                        random.display.puzzle.PuzzleActivity r5 = random.display.puzzle.PuzzleActivity.this
                        java.lang.String r5 = r5.puzzleSrcPath
                        java.lang.String r5 = java.lang.String.valueOf(r5)
                        r4.<init>(r5)
                        java.lang.String r5 = "/answer.png"
                        java.lang.StringBuilder r4 = r4.append(r5)
                        java.lang.String r3 = r4.toString()
                        if (r3 == 0) goto L_0x001f
                        android.graphics.Bitmap r4 = r0
                        if (r4 != 0) goto L_0x0020
                    L_0x001f:
                        return
                    L_0x0020:
                        r1 = 0
                        java.lang.String r4 = "PuzzleActivity"
                        java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x004f }
                        java.lang.String r6 = "trying to save image to "
                        r5.<init>(r6)     // Catch:{ FileNotFoundException -> 0x004f }
                        java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ FileNotFoundException -> 0x004f }
                        java.lang.String r5 = r5.toString()     // Catch:{ FileNotFoundException -> 0x004f }
                        android.util.Log.d(r4, r5)     // Catch:{ FileNotFoundException -> 0x004f }
                        java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x004f }
                        r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x004f }
                        android.graphics.Bitmap r4 = r0     // Catch:{ FileNotFoundException -> 0x0091, all -> 0x008e }
                        android.graphics.Bitmap$CompressFormat r5 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ FileNotFoundException -> 0x0091, all -> 0x008e }
                        r6 = 90
                        r4.compress(r5, r6, r2)     // Catch:{ FileNotFoundException -> 0x0091, all -> 0x008e }
                        if (r2 == 0) goto L_0x0048
                        r2.close()     // Catch:{ IOException -> 0x0089 }
                    L_0x0048:
                        android.graphics.Bitmap r4 = r0
                        r4.recycle()
                        r1 = r2
                        goto L_0x001f
                    L_0x004f:
                        r4 = move-exception
                        r0 = r4
                    L_0x0051:
                        java.lang.String r4 = "PuzzleActivity"
                        java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0078 }
                        java.lang.String r6 = "error to save image to "
                        r5.<init>(r6)     // Catch:{ all -> 0x0078 }
                        java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ all -> 0x0078 }
                        java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0078 }
                        android.util.Log.e(r4, r5, r0)     // Catch:{ all -> 0x0078 }
                        r0.printStackTrace()     // Catch:{ all -> 0x0078 }
                        if (r1 == 0) goto L_0x006d
                        r1.close()     // Catch:{ IOException -> 0x0073 }
                    L_0x006d:
                        android.graphics.Bitmap r4 = r0
                        r4.recycle()
                        goto L_0x001f
                    L_0x0073:
                        r0 = move-exception
                        r0.printStackTrace()
                        goto L_0x006d
                    L_0x0078:
                        r4 = move-exception
                    L_0x0079:
                        if (r1 == 0) goto L_0x007e
                        r1.close()     // Catch:{ IOException -> 0x0084 }
                    L_0x007e:
                        android.graphics.Bitmap r5 = r0
                        r5.recycle()
                        throw r4
                    L_0x0084:
                        r0 = move-exception
                        r0.printStackTrace()
                        goto L_0x007e
                    L_0x0089:
                        r0 = move-exception
                        r0.printStackTrace()
                        goto L_0x0048
                    L_0x008e:
                        r4 = move-exception
                        r1 = r2
                        goto L_0x0079
                    L_0x0091:
                        r4 = move-exception
                        r0 = r4
                        r1 = r2
                        goto L_0x0051
                    */
                    throw new UnsupportedOperationException("Method not decompiled: random.display.puzzle.PuzzleActivity.AnonymousClass3.run():void");
                }
            }).start();
        }
    }

    private void delAnswerPic() {
        File delFile = new File(String.valueOf(this.puzzleSrcPath) + "/answer.png");
        if (delFile.exists()) {
            delFile.delete();
        }
    }
}
