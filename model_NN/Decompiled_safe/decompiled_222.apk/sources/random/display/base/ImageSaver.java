package random.display.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ImageSaver {
    public static final int ACTIVITY_INTENT_CROPING_ID = 1;
    private Activity mainActivity;
    /* access modifiers changed from: private */
    public boolean saveAsPuzzleSrc;
    private ImageSaverEventListener savingEventListener;
    /* access modifiers changed from: private */
    public boolean setAsWallpaper;
    private String storageFolderName;
    private String storagePath = null;

    public interface ImageSaverEventListener {
        void onBeforeSaving();

        void onBeforeWallpaperCropping();

        void onNoSDCardError();

        void onSaved(String str);

        void onUnableToSetWallpaper();

        void onWallpaperError();

        void onWallpaperSet();
    }

    public Activity getMainActivity() {
        return this.mainActivity;
    }

    public void setMainActivity(Activity mainActivity2) {
        this.mainActivity = mainActivity2;
    }

    public String getStorageFolderName() {
        return this.storageFolderName;
    }

    public void setStorageFolderName(String storageFolderName2) {
        this.storageFolderName = storageFolderName2;
    }

    public ImageSaverEventListener getSavingEventListener() {
        return this.savingEventListener;
    }

    public void setSavingEventListener(ImageSaverEventListener savingEventListener2) {
        this.savingEventListener = savingEventListener2;
    }

    /* access modifiers changed from: private */
    public synchronized String getStoragePath() {
        String str;
        if (this.storagePath == null) {
            if (Environment.getExternalStorageState().equals("removed")) {
                if (getSavingEventListener() != null) {
                    getSavingEventListener().onNoSDCardError();
                }
                str = null;
            } else {
                File vSDCard = Environment.getExternalStorageDirectory();
                File vPath = new File(String.valueOf(vSDCard.getParent()) + "/" + vSDCard.getName() + getStorageFolderName());
                if (!vPath.exists()) {
                    vPath.mkdirs();
                }
                this.storagePath = vPath.getAbsolutePath();
            }
        }
        str = this.storagePath;
        return str;
    }

    public void setWallpaper(Bitmap pic, Context context) {
        if (pic != null) {
            if (pic.getWidth() >= 320 && pic.getHeight() >= 320) {
                savePic(pic, context, true, false);
            } else if (getSavingEventListener() != null) {
                getSavingEventListener().onUnableToSetWallpaper();
            }
        }
    }

    public void savePic(Bitmap pic, Context context, boolean wallpaper, boolean puzzle) {
        if (pic != null) {
            final Bitmap clonedPic = pic.copy(pic.getConfig(), false);
            this.setAsWallpaper = wallpaper;
            this.saveAsPuzzleSrc = puzzle;
            new Thread(new Runnable() {
                /* JADX WARNING: Removed duplicated region for block: B:37:0x0131 A[SYNTHETIC, Splitter:B:37:0x0131] */
                /* JADX WARNING: Removed duplicated region for block: B:44:0x0143 A[SYNTHETIC, Splitter:B:44:0x0143] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r7 = this;
                        random.display.base.ImageSaver r4 = random.display.base.ImageSaver.this
                        java.lang.String r3 = r4.getStoragePath()
                        if (r3 == 0) goto L_0x000c
                        android.graphics.Bitmap r4 = r0
                        if (r4 != 0) goto L_0x000d
                    L_0x000c:
                        return
                    L_0x000d:
                        random.display.base.ImageSaver r4 = random.display.base.ImageSaver.this
                        boolean r4 = r4.setAsWallpaper
                        if (r4 == 0) goto L_0x0086
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder
                        java.lang.String r5 = java.lang.String.valueOf(r3)
                        r4.<init>(r5)
                        java.lang.String r5 = "/wallpaper.png"
                        java.lang.StringBuilder r4 = r4.append(r5)
                        java.lang.String r3 = r4.toString()
                        random.display.base.ImageSaver r4 = random.display.base.ImageSaver.this
                        random.display.base.ImageSaver$ImageSaverEventListener r4 = r4.getSavingEventListener()
                        if (r4 == 0) goto L_0x0039
                        random.display.base.ImageSaver r4 = random.display.base.ImageSaver.this
                        random.display.base.ImageSaver$ImageSaverEventListener r4 = r4.getSavingEventListener()
                        r4.onBeforeWallpaperCropping()
                    L_0x0039:
                        r1 = 0
                        java.lang.String r4 = "ImageSaver"
                        java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0116 }
                        java.lang.String r6 = "trying to save image to "
                        r5.<init>(r6)     // Catch:{ FileNotFoundException -> 0x0116 }
                        java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ FileNotFoundException -> 0x0116 }
                        java.lang.String r5 = r5.toString()     // Catch:{ FileNotFoundException -> 0x0116 }
                        android.util.Log.d(r4, r5)     // Catch:{ FileNotFoundException -> 0x0116 }
                        java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0116 }
                        r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x0116 }
                        android.graphics.Bitmap r4 = r0     // Catch:{ FileNotFoundException -> 0x015a, all -> 0x0157 }
                        android.graphics.Bitmap$CompressFormat r5 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ FileNotFoundException -> 0x015a, all -> 0x0157 }
                        r6 = 90
                        r4.compress(r5, r6, r2)     // Catch:{ FileNotFoundException -> 0x015a, all -> 0x0157 }
                        random.display.base.ImageSaver r4 = random.display.base.ImageSaver.this     // Catch:{ FileNotFoundException -> 0x015a, all -> 0x0157 }
                        boolean r4 = r4.setAsWallpaper     // Catch:{ FileNotFoundException -> 0x015a, all -> 0x0157 }
                        if (r4 != 0) goto L_0x0075
                        random.display.base.ImageSaver r4 = random.display.base.ImageSaver.this     // Catch:{ FileNotFoundException -> 0x015a, all -> 0x0157 }
                        random.display.base.ImageSaver$ImageSaverEventListener r4 = r4.getSavingEventListener()     // Catch:{ FileNotFoundException -> 0x015a, all -> 0x0157 }
                        if (r4 == 0) goto L_0x0075
                        random.display.base.ImageSaver r4 = random.display.base.ImageSaver.this     // Catch:{ FileNotFoundException -> 0x015a, all -> 0x0157 }
                        random.display.base.ImageSaver$ImageSaverEventListener r4 = r4.getSavingEventListener()     // Catch:{ FileNotFoundException -> 0x015a, all -> 0x0157 }
                        r4.onSaved(r3)     // Catch:{ FileNotFoundException -> 0x015a, all -> 0x0157 }
                    L_0x0075:
                        random.display.base.ImageSaver r4 = random.display.base.ImageSaver.this     // Catch:{ FileNotFoundException -> 0x015a, all -> 0x0157 }
                        r4.onPictureSaved(r3)     // Catch:{ FileNotFoundException -> 0x015a, all -> 0x0157 }
                        if (r2 == 0) goto L_0x007f
                        r2.close()     // Catch:{ IOException -> 0x0151 }
                    L_0x007f:
                        android.graphics.Bitmap r4 = r0
                        r4.recycle()
                        r1 = r2
                        goto L_0x000c
                    L_0x0086:
                        random.display.base.ImageSaver r4 = random.display.base.ImageSaver.this
                        boolean r4 = r4.saveAsPuzzleSrc
                        if (r4 == 0) goto L_0x00de
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder
                        java.lang.String r5 = java.lang.String.valueOf(r3)
                        r4.<init>(r5)
                        java.lang.String r5 = "/puzzle"
                        java.lang.StringBuilder r4 = r4.append(r5)
                        java.lang.String r3 = r4.toString()
                        random.display.base.ImageSaver r4 = random.display.base.ImageSaver.this
                        r4.makePuzzleFolder(r3)
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder
                        java.lang.String r5 = java.lang.String.valueOf(r3)
                        r4.<init>(r5)
                        java.lang.String r5 = "/"
                        java.lang.StringBuilder r4 = r4.append(r5)
                        java.util.UUID r5 = java.util.UUID.randomUUID()
                        java.lang.String r5 = r5.toString()
                        java.lang.StringBuilder r4 = r4.append(r5)
                        java.lang.String r5 = ".png"
                        java.lang.StringBuilder r4 = r4.append(r5)
                        java.lang.String r3 = r4.toString()
                        random.display.base.ImageSaver r4 = random.display.base.ImageSaver.this
                        random.display.base.ImageSaver$ImageSaverEventListener r4 = r4.getSavingEventListener()
                        if (r4 == 0) goto L_0x0039
                        random.display.base.ImageSaver r4 = random.display.base.ImageSaver.this
                        random.display.base.ImageSaver$ImageSaverEventListener r4 = r4.getSavingEventListener()
                        r4.onBeforeWallpaperCropping()
                        goto L_0x0039
                    L_0x00de:
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder
                        java.lang.String r5 = java.lang.String.valueOf(r3)
                        r4.<init>(r5)
                        java.lang.String r5 = "/"
                        java.lang.StringBuilder r4 = r4.append(r5)
                        java.util.UUID r5 = java.util.UUID.randomUUID()
                        java.lang.String r5 = r5.toString()
                        java.lang.StringBuilder r4 = r4.append(r5)
                        java.lang.String r5 = ".png"
                        java.lang.StringBuilder r4 = r4.append(r5)
                        java.lang.String r3 = r4.toString()
                        random.display.base.ImageSaver r4 = random.display.base.ImageSaver.this
                        random.display.base.ImageSaver$ImageSaverEventListener r4 = r4.getSavingEventListener()
                        if (r4 == 0) goto L_0x0039
                        random.display.base.ImageSaver r4 = random.display.base.ImageSaver.this
                        random.display.base.ImageSaver$ImageSaverEventListener r4 = r4.getSavingEventListener()
                        r4.onBeforeSaving()
                        goto L_0x0039
                    L_0x0116:
                        r4 = move-exception
                        r0 = r4
                    L_0x0118:
                        java.lang.String r4 = "ImageSaver"
                        java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0140 }
                        java.lang.String r6 = "error to save image to "
                        r5.<init>(r6)     // Catch:{ all -> 0x0140 }
                        java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ all -> 0x0140 }
                        java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0140 }
                        android.util.Log.e(r4, r5, r0)     // Catch:{ all -> 0x0140 }
                        r0.printStackTrace()     // Catch:{ all -> 0x0140 }
                        if (r1 == 0) goto L_0x0134
                        r1.close()     // Catch:{ IOException -> 0x013b }
                    L_0x0134:
                        android.graphics.Bitmap r4 = r0
                        r4.recycle()
                        goto L_0x000c
                    L_0x013b:
                        r0 = move-exception
                        r0.printStackTrace()
                        goto L_0x0134
                    L_0x0140:
                        r4 = move-exception
                    L_0x0141:
                        if (r1 == 0) goto L_0x0146
                        r1.close()     // Catch:{ IOException -> 0x014c }
                    L_0x0146:
                        android.graphics.Bitmap r5 = r0
                        r5.recycle()
                        throw r4
                    L_0x014c:
                        r0 = move-exception
                        r0.printStackTrace()
                        goto L_0x0146
                    L_0x0151:
                        r0 = move-exception
                        r0.printStackTrace()
                        goto L_0x007f
                    L_0x0157:
                        r4 = move-exception
                        r1 = r2
                        goto L_0x0141
                    L_0x015a:
                        r4 = move-exception
                        r0 = r4
                        r1 = r2
                        goto L_0x0118
                    */
                    throw new UnsupportedOperationException("Method not decompiled: random.display.base.ImageSaver.AnonymousClass1.run():void");
                }
            }).start();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void onPictureSaved(String src) {
        if (this.setAsWallpaper) {
            File f = new File(src);
            Intent intent = new Intent("com.android.camera.action.CROP");
            intent.setDataAndType(Uri.fromFile(f), "image/*");
            int w = getMainActivity().getWallpaperDesiredMinimumWidth();
            int h = getMainActivity().getWallpaperDesiredMinimumHeight();
            intent.putExtra("outputX", w);
            intent.putExtra("outputY", h);
            intent.putExtra("aspectX", w);
            intent.putExtra("aspectY", h);
            intent.putExtra("crop", true);
            intent.putExtra("scale", true);
            intent.putExtra("output", Uri.fromFile(f));
            intent.putExtra("noFaceDetection", true);
            getMainActivity().startActivityForResult(intent, 1);
        }
    }

    /* JADX INFO: finally extract failed */
    public void processCropingIntentResult(int requestCode, int resultCode, Intent data) {
        if (1 == requestCode) {
            if (-1 != resultCode) {
                delWallpaper();
                return;
            }
            String strPath = getStoragePath();
            if (strPath != null) {
                Bitmap bm = BitmapFactory.decodeFile(new File(String.valueOf(strPath) + "/" + "wallpaper.png").getAbsolutePath());
                try {
                    doSetWallpaper(bm);
                    bm.recycle();
                    delWallpaper();
                } catch (Throwable th) {
                    bm.recycle();
                    throw th;
                }
            }
        }
    }

    private void doSetWallpaper(Bitmap bm) {
        if (bm != null) {
            try {
                getMainActivity().setWallpaper(bm);
                if (getSavingEventListener() != null) {
                    getSavingEventListener().onWallpaperSet();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                if (getSavingEventListener() != null) {
                    getSavingEventListener().onWallpaperError();
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                if (getSavingEventListener() != null) {
                    getSavingEventListener().onWallpaperError();
                }
            }
        } else if (getSavingEventListener() != null) {
            getSavingEventListener().onWallpaperError();
        }
    }

    /* access modifiers changed from: private */
    public void makePuzzleFolder(String src) {
        File puzzleFolderChceker = new File(src);
        if (!puzzleFolderChceker.exists()) {
            puzzleFolderChceker.mkdir();
        }
    }

    private void delWallpaper() {
        String strPath = getStoragePath();
        if (strPath != null) {
            new File(String.valueOf(strPath) + "/" + "wallpaper.png").delete();
        }
    }
}
