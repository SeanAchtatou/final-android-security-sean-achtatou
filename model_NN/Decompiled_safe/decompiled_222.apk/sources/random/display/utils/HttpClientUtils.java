package random.display.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class HttpClientUtils {
    public static void fillRequestProperties(HttpURLConnection con, Map<String, String> requestProperties) {
        for (String key : requestProperties.keySet()) {
            con.setRequestProperty(key, requestProperties.get(key));
        }
    }

    public static String doGet(String u) throws IOException {
        return doGet(u, null);
    }

    public static String doGet(String u, Map<String, String> requestProperties) throws IOException {
        return doRequest("GET", u, null, requestProperties);
    }

    public static String doRequest(String method, String u, String args, Map<String, String> requestProperties) throws IOException {
        HttpURLConnection con = (HttpURLConnection) new URL(u).openConnection();
        con.setRequestMethod(method);
        if (requestProperties != null) {
            fillRequestProperties(con, requestProperties);
        }
        if (args != null && "POST".equals(method)) {
            con.setDoInput(true);
        }
        con.setDoOutput(true);
        con.connect();
        if (args != null && "POST".equals(method)) {
            DataOutputStream dos = new DataOutputStream(con.getOutputStream());
            dos.write(args.getBytes());
            dos.flush();
            dos.close();
        }
        Log.i("HttpClientUtils", String.valueOf(method) + " [" + u + "] : " + con.getResponseMessage());
        InputStream inData = con.getInputStream();
        try {
            StringBuilder sbRet = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(inData));
            for (String strLine = br.readLine(); strLine != null; strLine = br.readLine()) {
                sbRet.append(strLine);
            }
            return sbRet.toString();
        } finally {
            inData.close();
            con.disconnect();
        }
    }

    public static String doPost(String u) throws IOException {
        return doPost(u, null, null);
    }

    public static String doPost(String u, String args) throws IOException {
        return doPost(u, args, null);
    }

    public static String doPost(String u, String args, Map<String, String> requestProperties) throws IOException {
        return doRequest("POST", u, args, requestProperties);
    }

    public static Bitmap downloadImage(String method, String u) throws IOException {
        if (method == null || u == null) {
            return null;
        }
        return downloadImage(method, u, null);
    }

    public static Bitmap downloadImage(String method, String u, String args) throws IOException {
        return downloadImage(method, u, args, null);
    }

    public static Bitmap downloadImage(String method, String u, String args, Map<String, String> requestProperties) throws IOException {
        HttpURLConnection con = (HttpURLConnection) new URL(u).openConnection();
        con.setRequestMethod(method);
        if (requestProperties != null) {
            fillRequestProperties(con, requestProperties);
        }
        if (args != null && "POST".equals(method)) {
            con.setDoInput(true);
        }
        con.setDoOutput(true);
        con.connect();
        if (args != null && "POST".equals(method)) {
            DataOutputStream dos = new DataOutputStream(con.getOutputStream());
            dos.write(args.getBytes());
            dos.flush();
            dos.close();
        }
        BufferedInputStream inData = new BufferedInputStream(con.getInputStream());
        try {
            return BitmapFactory.decodeStream(inData);
        } finally {
            inData.close();
            con.disconnect();
        }
    }
}
