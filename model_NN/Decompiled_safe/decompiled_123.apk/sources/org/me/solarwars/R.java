package org.me.solarwars;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int arrowleft = 2130837504;
        public static final int arrowright = 2130837505;
        public static final int birdy_ad = 2130837506;
        public static final int bomb_it_ad = 2130837507;
        public static final int cancel = 2130837508;
        public static final int endturn = 2130837509;
        public static final int fleet = 2130837510;
        public static final int fleet_left = 2130837511;
        public static final int fleet_right = 2130837512;
        public static final int fleetimg_0 = 2130837513;
        public static final int fleetimg_1 = 2130837514;
        public static final int fleetimg_2 = 2130837515;
        public static final int fleetimg_3 = 2130837516;
        public static final int fleetimg_4 = 2130837517;
        public static final int fleetimg_5 = 2130837518;
        public static final int fleetimg_6 = 2130837519;
        public static final int fleetimg_7 = 2130837520;
        public static final int fleetimg_8 = 2130837521;
        public static final int fleetimg_9 = 2130837522;
        public static final int frogs_jump_ad = 2130837523;
        public static final int icon = 2130837524;
        public static final int make_fleet = 2130837525;
        public static final int makefleet = 2130837526;
        public static final int menu_load = 2130837527;
        public static final int menu_save = 2130837528;
        public static final int menuback = 2130837529;
        public static final int ok = 2130837530;
        public static final int pirate_islands_ad = 2130837531;
        public static final int push_off_0 = 2130837532;
        public static final int push_off_1 = 2130837533;
        public static final int push_off_2 = 2130837534;
        public static final int push_off_3 = 2130837535;
        public static final int push_off_4 = 2130837536;
        public static final int push_off_5 = 2130837537;
        public static final int push_off_6 = 2130837538;
        public static final int push_off_7 = 2130837539;
        public static final int push_off_8 = 2130837540;
        public static final int push_on_0 = 2130837541;
        public static final int push_on_1 = 2130837542;
        public static final int push_on_2 = 2130837543;
        public static final int push_on_3 = 2130837544;
        public static final int push_on_4 = 2130837545;
        public static final int push_on_5 = 2130837546;
        public static final int push_on_6 = 2130837547;
        public static final int push_on_7 = 2130837548;
        public static final int push_on_8 = 2130837549;
        public static final int rotateme_ad = 2130837550;
        public static final int set_target = 2130837551;
        public static final int solar = 2130837552;
        public static final int solar_01 = 2130837553;
        public static final int solar_02 = 2130837554;
        public static final int solar_03 = 2130837555;
        public static final int solar_04 = 2130837556;
        public static final int solar_05 = 2130837557;
        public static final int solar_06 = 2130837558;
        public static final int solar_left = 2130837559;
        public static final int solar_right = 2130837560;
        public static final int tank_warriors_ad = 2130837561;
        public static final int twitter = 2130837562;
        public static final int uback = 2130837563;
    }

    public static final class id {
        public static final int ad = 2131034122;
        public static final int create_army_cancel = 2131034120;
        public static final int create_army_ok = 2131034121;
        public static final int gotomobilsoft = 2131034123;
        public static final int image = 2131034113;
        public static final int layout_root = 2131034112;
        public static final int layout_root2 = 2131034114;
        public static final int layout_root3 = 2131034116;
        public static final int max_units = 2131034118;
        public static final int menubtn = 2131034124;
        public static final int unit_count = 2131034119;
        public static final int unit_count_planet = 2131034117;
        public static final int units_slider = 2131034115;
    }

    public static final class layout {
        public static final int createarmy_layout = 2130903040;
        public static final int main = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2130968576;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
