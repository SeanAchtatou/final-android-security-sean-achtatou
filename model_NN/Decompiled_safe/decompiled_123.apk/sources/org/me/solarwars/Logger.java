package org.me.solarwars;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class Logger {
    static Logger instance = null;

    public void writelog(String msg) {
        writeToFile(msg, "gamecanvas.txt");
    }

    private void writeToFile(String stacktrace, String filename) {
        try {
            BufferedWriter bos = new BufferedWriter(new FileWriter("/sdcard" + filename));
            bos.write(stacktrace);
            bos.flush();
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Logger getInstance() {
        if (instance == null) {
            instance = new Logger();
        }
        return instance;
    }
}
