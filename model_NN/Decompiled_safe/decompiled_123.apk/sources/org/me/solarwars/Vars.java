package org.me.solarwars;

import java.util.ArrayList;
import java.util.Random;
import org.me.solarwars.Player;

public class Vars {
    static Vars instance = null;
    public int ABOUT_BTN;
    public int ADD_AI_PLAYER;
    public int ADD_SHIP_TO_FLEET;
    public int CANCEL_MAKE_FLEET;
    public int CLOSE_ABOUT;
    public int CLOSE_ENDGAME;
    public int END_TURN;
    public int FLEET_SLIDER;
    public int LOAD_GAME;
    public int MAKE_FLEET;
    public int MAP_SIZE_ADD;
    public int MAP_SIZE_BTN;
    public int MAP_SIZE_SUB;
    public int MAX_SHIP_TO_FLEET;
    public int NEXT_FLEET;
    public int NEXT_PLANET;
    public int OK_MAKE_FLEET;
    public int OPEN_ABOUT_URL;
    public int OTHER_APP;
    public int PLAYERS_NUM_BTN;
    public int PLAY_BTN_ID;
    public int PREV_FLEET;
    public int PREV_PLANET;
    public int SAVE_CANCEL;
    public int SAVE_NO;
    public int SAVE_YES;
    public int SET_TARGET;
    public int SUB_AI_PLAYER;
    public int SUB_SHIP_TO_FLEET;
    ArrayList<BattleReport> battleReportList;
    ArrayList<gButton> buttonList;
    int currPlayer;
    ArrayList<Fleet> fleetList;
    public GameState gameState;
    ArrayList<Planet> planetList;
    ArrayList<Player> playerList;
    Random random;
    int scaleFactor;
    int screenHeight;
    ArrayList<ScreenMsg> screenMessages;
    int screenWidth;
    int selectedFleet;
    int selectedPlanet;
    int tutorial_msg_count;
    String winner;

    public enum GameState {
        Menu,
        Game,
        MakeFleet,
        About,
        EndGame,
        SaveGameDlg
    }

    public Vars() {
        this.tutorial_msg_count = 40;
        this.buttonList = null;
        this.playerList = null;
        this.fleetList = null;
        this.battleReportList = null;
        this.planetList = null;
        this.screenMessages = null;
        this.winner = "";
        this.PLAY_BTN_ID = 100;
        this.ADD_AI_PLAYER = 101;
        this.SUB_AI_PLAYER = 102;
        this.MAP_SIZE_ADD = 103;
        this.MAP_SIZE_SUB = 104;
        this.LOAD_GAME = 105;
        this.PLAYERS_NUM_BTN = 106;
        this.MAP_SIZE_BTN = 107;
        this.ABOUT_BTN = 108;
        this.OTHER_APP = 109;
        this.END_TURN = 200;
        this.NEXT_FLEET = 201;
        this.NEXT_PLANET = 202;
        this.PREV_FLEET = 203;
        this.PREV_PLANET = 204;
        this.MAKE_FLEET = 205;
        this.SET_TARGET = 206;
        this.OK_MAKE_FLEET = 207;
        this.CANCEL_MAKE_FLEET = 208;
        this.ADD_SHIP_TO_FLEET = 300;
        this.SUB_SHIP_TO_FLEET = 301;
        this.MAX_SHIP_TO_FLEET = 302;
        this.OPEN_ABOUT_URL = 400;
        this.CLOSE_ABOUT = 401;
        this.CLOSE_ENDGAME = 500;
        this.SAVE_YES = 600;
        this.SAVE_NO = 601;
        this.SAVE_CANCEL = 602;
        this.FLEET_SLIDER = 10000;
        this.buttonList = new ArrayList<>();
        this.buttonList.clear();
        this.playerList = new ArrayList<>();
        this.playerList.clear();
        this.fleetList = new ArrayList<>();
        this.fleetList.clear();
        this.battleReportList = new ArrayList<>();
        this.battleReportList.clear();
        this.planetList = new ArrayList<>();
        this.planetList.clear();
        this.screenMessages = new ArrayList<>();
        this.screenMessages.clear();
    }

    public void addButton(gButton b) {
        this.buttonList.add(b);
    }

    public ArrayList<gButton> getButtonList() {
        return this.buttonList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public gButton getButton(int b) {
        if (b >= 0 && b < this.buttonList.size()) {
            return this.buttonList.get(b);
        }
        System.out.println("button null!!!!!");
        return null;
    }

    public void setCurrPlayer(int cp) {
        this.currPlayer = cp;
    }

    public int getCurrPlayer() {
        return this.currPlayer;
    }

    public ArrayList<Planet> getPlanetList() {
        return this.planetList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Planet getPlanet(int i) {
        int size = this.planetList.size();
        if (i >= 0 && i < size) {
            return this.planetList.get(i);
        }
        System.out.println("planet null!!!!");
        return null;
    }

    public ArrayList<Player> getPlayerList() {
        return this.playerList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public Player getPlayer(int i) {
        if (i >= 0 && i < this.playerList.size()) {
            return this.playerList.get(i);
        }
        System.out.println("player null!!!!!");
        return null;
    }

    public Player getPlayerById(int id) {
        for (int i = 0; i < this.playerList.size(); i++) {
            if (id == getPlayer(i).getId()) {
                return getPlayer(i);
            }
        }
        return null;
    }

    public ArrayList<BattleReport> getBattleReportList() {
        return this.battleReportList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public BattleReport getBattleReport(int i) {
        if (i < 0 || i >= this.battleReportList.size()) {
            return null;
        }
        return this.battleReportList.get(i);
    }

    public ArrayList<Fleet> getFleetList() {
        return this.fleetList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Fleet getFleet(int i) {
        if (i < 0 || i >= this.fleetList.size()) {
            return null;
        }
        return this.fleetList.get(i);
    }

    public ArrayList<ScreenMsg> getScreenMsgList() {
        return this.screenMessages;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public ScreenMsg getScreenMsg(int i) {
        if (i < 0 || i >= this.screenMessages.size()) {
            return null;
        }
        return this.screenMessages.get(i);
    }

    public void addScreenMsg(ScreenMsg str) {
        this.screenMessages.add(str);
    }

    public void flushMessages() {
    }

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public void selectFleet(int i) {
        this.selectedFleet = i;
        this.selectedPlanet = -1;
        getScreenMsgList().clear();
        addTutorialMsg("Select destination by tapping on target");
    }

    public void addTutorialMsg(String m) {
        if (this.tutorial_msg_count > 0) {
            ScreenMsg msg = new ScreenMsg();
            msg.msg = m;
            msg.color = -1;
            getScreenMsgList().add(msg);
            this.tutorial_msg_count--;
        }
    }

    public void selectPlanet(int p) {
        this.selectedFleet = -1;
        this.selectedPlanet = p;
        getScreenMsgList().clear();
        addTutorialMsg("Create fleet by tapping a planet again");
        addTutorialMsg("   or by click on CREATE FLEET button");
    }

    public int getSelectedFleet() {
        return this.selectedFleet;
    }

    public int getSelectedPlanet() {
        return this.selectedPlanet;
    }

    public void init() {
        this.random = new Random();
        this.scaleFactor = 1;
        this.selectedFleet = -1;
        this.selectedPlanet = -1;
    }

    public void setScaleFactor(int sf) {
        this.scaleFactor = sf;
    }

    public int getScaleFactor() {
        return this.scaleFactor;
    }

    public int GetNextInt(int max) {
        if (max == 0) {
            max = 1;
        }
        return Math.abs(this.random.nextInt() % max);
    }

    public void screenSize(int w, int h) {
        this.screenWidth = w;
        this.screenHeight = h;
    }

    public static Vars getInstance() {
        if (instance == null) {
            instance = new Vars();
            instance.init();
        }
        return instance;
    }

    public int findPlayerById(int id) {
        for (int i = 0; i < this.playerList.size(); i++) {
            if (this.playerList.get(i).getId() == id) {
                return i;
            }
        }
        return -1;
    }

    public void makeFleet(int planet_index, int ships) {
        Fleet f = new Fleet();
        f.setShips(ships);
        Planet p = getPlanet(planet_index);
        f.SetPos(p.getPx(), p.getPy());
        f.setDestPos(p.getPx(), p.getPy());
        f.setOwnerID(p.getOwnerID());
        f.setSize(this.scaleFactor * 50, this.scaleFactor * 50);
        p.setShips(p.getShips() - ships);
        this.fleetList.add(f);
    }

    public void hideAllButtons() {
        if (this.buttonList != null) {
            for (int i = 0; i < this.buttonList.size(); i++) {
                this.buttonList.get(i).hide();
            }
        }
    }

    public void setupNewGame(int playerNum, String mapSize) {
        this.currPlayer = 0;
        CMap map = CMap.getInstance();
        if (mapSize.equals("small")) {
            map.setMapSize(800, 800);
            map.generate(GetNextInt(10) + 10);
        } else if (mapSize.equals("medium")) {
            map.setMapSize(1100, 1100);
            map.generate(GetNextInt(20) + 20);
        } else if (mapSize.equals("big")) {
            map.setMapSize(1700, 1700);
            map.generate(GetNextInt(30) + 30);
        }
        this.screenMessages.clear();
        this.fleetList.clear();
        getPlayerList().clear();
        for (int i = 0; i < playerNum + 1; i++) {
            Player p = new Player();
            if (i == 0) {
                p.setPlayerType(Player.PlayerType.Human);
                p.setName("Player");
            } else {
                p.setPlayerType(Player.PlayerType.AI);
                p.setName("CompAI " + i);
            }
            p.setId(i);
            boolean done = false;
            int pcount = getInstance().getPlanetList().size();
            while (!done) {
                int rand = GetNextInt(pcount);
                if (getPlanet(rand).getOwnerID() < 0) {
                    getPlanet(rand).setOwnerID(p.getId());
                    done = true;
                }
            }
            getInstance().getPlayerList().add(p);
        }
        this.currPlayer = 0;
        int curr_planet = 0;
        int i2 = 0;
        while (true) {
            if (i2 >= this.planetList.size()) {
                break;
            } else if (getPlanet(i2).getOwnerID() == getPlayer(0).getId()) {
                curr_planet = i2;
                break;
            } else {
                i2++;
            }
        }
        CMap.getInstance().centerOnPoint((int) getPlanet(curr_planet).getPx(), (int) getPlanet(curr_planet).getPy());
        selectPlanet(curr_planet);
    }

    public boolean isEndGame() {
        int players_in_game = 0;
        int[] count = new int[this.playerList.size()];
        for (int p = 0; p < this.playerList.size(); p++) {
            count[p] = 0;
            for (int i = 0; i < this.planetList.size(); i++) {
                if (getPlayer(p).getId() == getPlanet(i).getOwnerID()) {
                    count[p] = count[p] + 1;
                }
            }
            for (int f = 0; f < this.fleetList.size(); f++) {
                if (getFleet(f).getOwnerID() == getPlayer(p).getId()) {
                    count[p] = count[p] + 1;
                }
            }
            if (count[p] > 0) {
                players_in_game++;
                this.winner = getPlayer(p).getName();
            }
        }
        if (players_in_game < 2) {
            return true;
        }
        return false;
    }

    public String getWinner() {
        return this.winner;
    }

    public void UpdatePlanets() {
        for (int i = 0; i < this.planetList.size(); i++) {
            int ns = getPlanet(i).getProduction();
            if (getPlanet(i).getOwnerID() < 0) {
                getPlanet(i).setShips(getPlanet(i).getShips() + (ns / 2));
            } else {
                getPlayerById(getPlanet(i).getOwnerID()).ship_build += ns;
                getPlanet(i).setShips(getPlanet(i).getShips() + ns);
            }
        }
    }

    public void MakeAIMoves() {
        for (int p = 0; p < getPlayerList().size(); p++) {
            if (getPlayer(p).getPlayerType() != Player.PlayerType.Human) {
                for (int i = 0; i < getPlanetList().size(); i++) {
                    int player_id = getPlayer(p).getId();
                    if (getPlanet(i).getOwnerID() == player_id && GetNextInt(15) == 0 && getPlanet(i).getShips() > getPlanet(i).getProduction() * 2) {
                        makeFleet(i, (getPlanet(i).getShips() / 2) + GetNextInt(getPlanet(i).getShips() / 2));
                        boolean flag = false;
                        int count = 0;
                        double dest_x = 0.0d;
                        double dest_y = 0.0d;
                        while (!flag) {
                            flag = true;
                            int dest_planet = GetNextInt(getPlanetList().size());
                            if (dest_planet == i) {
                                flag = false;
                            }
                            if (count < 8 && getPlanet(dest_planet).getOwnerID() == player_id) {
                                flag = false;
                                if (count < 0) {
                                    count = 0;
                                }
                            }
                            if (count < 6 && getPlanet(dest_planet).getOwnerID() != player_id) {
                                flag = false;
                            }
                            double px = getPlanet(dest_planet).getPx();
                            double py = getPlanet(dest_planet).getPy();
                            if (Math.sqrt(((px - getPlanet(i).getPx()) * (px - getPlanet(i).getPx())) + ((py - getPlanet(i).getPy()) * (py - getPlanet(i).getPy()))) > ((double) CMap.getInstance().GetWidth()) * 0.5d && count < 10) {
                                flag = false;
                            }
                            count++;
                            double planet_size = getPlanet(dest_planet).getWidth();
                            dest_x = px + (planet_size / 2.0d);
                            dest_y = py + (planet_size / 2.0d);
                        }
                        if (getFleet(getFleetList().size() - 1) != null) {
                            getFleet(getFleetList().size() - 1).setDestPos(dest_x, dest_y);
                        }
                    }
                }
            }
        }
    }

    public void selectNextPlanet() {
        int cp;
        int cp2 = this.selectedPlanet + 1;
        if (cp2 < 0) {
            cp2 = 0;
        }
        if (cp2 >= this.planetList.size()) {
            cp2 = 0;
        }
        int currPlayerId = getPlayer(this.currPlayer).getId();
        int i = 0;
        while (true) {
            if (i >= this.planetList.size()) {
                break;
            } else if (getPlanet(cp).getOwnerID() == currPlayerId) {
                selectPlanet(cp);
                break;
            } else {
                cp++;
                if (cp >= this.planetList.size()) {
                    cp = 0;
                }
                i++;
            }
        }
        showUI(true);
    }

    public void selectPrevPlanet() {
        int cp;
        int cp2 = this.selectedPlanet - 1;
        if (cp2 < 0) {
            cp2 = this.planetList.size() - 1;
        }
        int currPlayerId = getPlayer(this.currPlayer).getId();
        int i = 0;
        while (true) {
            if (i >= this.planetList.size()) {
                break;
            } else if (getPlanet(cp).getOwnerID() == currPlayerId) {
                selectPlanet(cp);
                break;
            } else {
                cp--;
                if (cp < 0) {
                    cp = this.planetList.size() - 1;
                }
                i++;
            }
        }
        showUI(true);
    }

    public void selectNextFleet() {
        int cf;
        int cf2 = this.selectedFleet + 1;
        if (cf2 < 0) {
            cf2 = 0;
        }
        if (cf2 >= this.fleetList.size()) {
            cf2 = 0;
        }
        int currPlayerId = getPlayer(this.currPlayer).getId();
        int i = 0;
        while (true) {
            if (i >= this.fleetList.size()) {
                break;
            } else if (getFleet(cf).getOwnerID() == currPlayerId) {
                selectFleet(cf);
                break;
            } else {
                cf++;
                if (cf >= this.fleetList.size()) {
                    cf = 0;
                }
                i++;
            }
        }
        showUI(true);
    }

    public void selectPrevFleet() {
        int cf;
        if (this.fleetList.size() > 0) {
            int cf2 = this.selectedFleet - 1;
            if (cf2 < 0 && (cf2 = this.fleetList.size() - 1) < 0) {
                cf2 = 0;
            }
            if (getPlayer(this.currPlayer) != null) {
                int currPlayerId = getPlayer(this.currPlayer).getId();
                int i = 0;
                while (true) {
                    if (i < this.fleetList.size()) {
                        if (getFleet(cf) != null && getFleet(cf).getOwnerID() == currPlayerId) {
                            selectFleet(cf);
                            break;
                        }
                        cf--;
                        if (cf >= this.fleetList.size()) {
                            cf = this.fleetList.size() - 1;
                        }
                        i++;
                    } else {
                        break;
                    }
                }
                showUI(true);
            }
        }
    }

    public Player getHumanPlayer() {
        for (int i = 0; i < this.playerList.size(); i++) {
            if (getPlayer(i).getPlayerType() == Player.PlayerType.Human) {
                return getPlayer(i);
            }
        }
        return null;
    }

    public void MoveFleets() {
        String name1;
        String name2;
        for (int i = 0; i < 4; i++) {
            for (int f = 0; f < getFleetList().size(); f++) {
                Fleet fleet = getFleet(f);
                if (!(fleet.getPx() == fleet.getDestPx() && fleet.getPy() == fleet.getDestPy())) {
                    double r = Math.sqrt(((fleet.getPx() - fleet.getDestPx()) * (fleet.getPx() - fleet.getDestPx())) + ((fleet.getPy() - fleet.getDestPy()) * (fleet.getPy() - fleet.getDestPy())));
                    if (r < ((double) fleet.getSpeed())) {
                        fleet.SetPos(fleet.getDestPx(), fleet.getDestPy());
                    }
                    fleet.SetPos(fleet.getPx() - (((double) (fleet.speed / 4)) * ((fleet.getPx() - fleet.getDestPx()) / r)), fleet.getPy() - (((double) (fleet.speed / 4)) * ((fleet.getPy() - fleet.getDestPy()) / r)));
                }
                if (fleet.getPx() == fleet.getDestPx() && fleet.getPy() == fleet.getDestPy()) {
                    int fx = (int) fleet.getPx();
                    int fy = (int) fleet.getPy();
                    for (int k = 0; k < this.planetList.size(); k++) {
                        if (((double) fx) >= getPlanet(k).getPx() && ((double) fx) <= getPlanet(k).getPx() + getPlanet(k).getWidth() && ((double) fy) >= getPlanet(k).getPy() && ((double) fy) <= getPlanet(k).getPy() + getPlanet(k).getWidth()) {
                            if (fleet.getOwnerID() == getPlanet(k).getOwnerID()) {
                                getPlanet(k).setShips(getPlanet(k).getShips() + fleet.getShips());
                            } else {
                                BattleReport br = new BattleReport();
                                int ps = getPlanet(k).getShips();
                                int fs = fleet.getShips();
                                int pr = GetNextInt(ps);
                                int fr = GetNextInt(fs);
                                if (ps < fs) {
                                    pr /= 2;
                                }
                                if (ps >= fs) {
                                    fr /= 2;
                                }
                                br.setPos((int) (getPlanet(k).getPx() + (getPlanet(k).getWidth() / 2.0d)), (int) (getPlanet(k).getPy() + (getPlanet(k).getWidth() / 2.0d)));
                                int id_planet = getPlanet(k).getOwnerID();
                                if (fr > pr) {
                                    getPlanet(k).setOwnerID(fleet.getOwnerID());
                                    getPlanet(k).setShips(fleet.ships - ps);
                                    if (getPlanet(k).getShips() <= 0) {
                                        getPlanet(k).setShips(1);
                                        getPlayerById(fleet.getOwnerID()).setPlanetConquered(getPlayerById(fleet.getOwnerID()).getPlanetConquered() + 1);
                                    }
                                } else {
                                    getPlanet(k).setShips(getPlanet(k).getShips() - fleet.getShips());
                                    if (getPlanet(k).getShips() < 0) {
                                        getPlanet(k).setShips(0);
                                    }
                                }
                                br.player_id1 = id_planet;
                                br.player_id2 = fleet.getOwnerID();
                                br.result = getPlanet(k).getOwnerID();
                                br.human_result = -1;
                                if ((getHumanPlayer().getId() == br.player_id1 && br.result == br.player_id1) || (getHumanPlayer().getId() == br.player_id2 && br.result == br.player_id2)) {
                                    br.human_result = 1;
                                } else {
                                    if ((getHumanPlayer().getId() == br.player_id1 && br.result != br.player_id1) || (getHumanPlayer().getId() == br.player_id2 && br.result != br.player_id2)) {
                                        br.human_result = 0;
                                    }
                                }
                                if (getPlayerById(br.player_id1) != null) {
                                    name1 = String.valueOf("") + getPlayerById(br.player_id1).getName();
                                } else {
                                    name1 = String.valueOf("") + "Aliens";
                                }
                                if (getPlayerById(br.player_id2) != null) {
                                    name2 = String.valueOf("") + getInstance().getPlayerById(br.player_id2).getName();
                                } else {
                                    name2 = String.valueOf("") + "Aliens";
                                }
                                br.str1 = String.valueOf(name1) + " vs " + name2;
                                br.str2 = "";
                                if (br.result == br.player_id1) {
                                    br.str2 = String.valueOf(br.str2) + name1;
                                } else {
                                    br.str2 = String.valueOf(br.str2) + name2;
                                }
                                br.str2 = String.valueOf(br.str2) + " wins";
                                this.battleReportList.add(br);
                                ScreenMsg msg = new ScreenMsg();
                                msg.msg = String.valueOf(br.str1) + ", " + br.str2;
                                msg.color = -7829368;
                                if (br.human_result == 1) {
                                    msg.color = -16711936;
                                }
                                if (br.human_result == 0) {
                                    msg.color = -65536;
                                }
                                getScreenMsgList().add(msg);
                            }
                            fleet.setShips(0);
                        }
                    }
                }
            }
            int f2 = 0;
            while (f2 < this.fleetList.size()) {
                if (getFleet(f2).getShips() <= 0) {
                    this.fleetList.remove(getFleet(f2));
                    f2 = 0;
                }
                f2++;
            }
        }
    }

    public void showUI(boolean show) {
        if (this.buttonList != null) {
            for (int i = 0; i < this.buttonList.size(); i++) {
                if (getButton(i) != null) {
                    getButton(i).hide();
                }
            }
            if (!show) {
                return;
            }
            if (this.gameState == GameState.Menu) {
                showControls(new int[]{this.ABOUT_BTN, this.PLAY_BTN_ID, this.ADD_AI_PLAYER, this.SUB_AI_PLAYER, this.MAP_SIZE_ADD, this.MAP_SIZE_SUB, this.LOAD_GAME, this.PLAYERS_NUM_BTN, this.MAP_SIZE_BTN, this.OTHER_APP}, null);
            } else if (this.gameState == GameState.Game) {
                boolean make_fleet = false;
                int pid = getPlayer(this.currPlayer).getId();
                if (this.selectedPlanet >= 0 && getPlanet(this.selectedPlanet).getOwnerID() == pid) {
                    make_fleet = true;
                }
                if (0 != 0) {
                    showControls(new int[]{this.END_TURN, this.NEXT_FLEET, this.NEXT_PLANET, this.PREV_FLEET, this.PREV_PLANET, this.SET_TARGET}, null);
                } else if (make_fleet) {
                    showControls(new int[]{this.END_TURN, this.NEXT_FLEET, this.NEXT_PLANET, this.PREV_FLEET, this.PREV_PLANET, this.MAKE_FLEET}, null);
                } else {
                    showControls(new int[]{this.END_TURN, this.NEXT_FLEET, this.NEXT_PLANET, this.PREV_FLEET, this.PREV_PLANET}, null);
                }
            } else if (this.gameState == GameState.MakeFleet) {
                showControls(new int[]{this.OK_MAKE_FLEET, this.CANCEL_MAKE_FLEET, this.ADD_SHIP_TO_FLEET, this.SUB_SHIP_TO_FLEET, this.MAX_SHIP_TO_FLEET}, new int[]{this.FLEET_SLIDER});
            } else if (this.gameState == GameState.About) {
                showControls(new int[]{this.OPEN_ABOUT_URL, this.CLOSE_ABOUT}, null);
            } else if (this.gameState == GameState.EndGame) {
                showControls(new int[]{this.CLOSE_ENDGAME}, null);
            } else if (this.gameState == GameState.SaveGameDlg) {
                showControls(new int[]{this.SAVE_YES, this.SAVE_NO, this.SAVE_CANCEL}, null);
            }
        }
    }

    public void showControls(int[] showbtn, int[] sliders) {
        if (showbtn != null) {
            for (int b = 0; b < this.buttonList.size(); b++) {
                for (int k = 0; k < showbtn.length; k++) {
                    if (getButton(b) != null && getButton(b).getId() == showbtn[k]) {
                        getButton(b).show();
                    }
                }
            }
        }
    }
}
