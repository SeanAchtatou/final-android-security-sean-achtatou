package org.me.solarwars;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

public class EndGameCanvas extends View {
    gButton closeBtn;
    Paint paint;
    int[] planet_conquered;
    String[] player_names;
    int[] ships_build;
    Sites site;
    String siteUrl;
    gButton urlButton;

    enum Sites {
        Handango
    }

    public EndGameCanvas(Context context) {
        super(context);
        this.site = Sites.Handango;
        this.siteUrl = "";
        this.paint = null;
        this.paint = new Paint();
        initUI();
    }

    public void initUI() {
        this.closeBtn = new gButton();
        this.closeBtn.setText("Close");
        this.closeBtn.setId(Vars.getInstance().CLOSE_ENDGAME);
        Vars.getInstance().addButton(this.closeBtn);
    }

    public void layoutUI() {
        this.closeBtn.setSize((int) (((double) getWidth()) * 0.6d), Vars.getInstance().getScaleFactor() * 60);
        this.closeBtn.setPosition((int) (((double) getWidth()) * 0.2d), getHeight() - ((int) (((double) this.closeBtn.getHeight()) * 1.8d)));
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        layoutUI();
    }

    public void initCanvas() {
        int cnt = Vars.getInstance().getPlayerList().size();
        this.player_names = new String[cnt];
        this.ships_build = new int[cnt];
        this.planet_conquered = new int[cnt];
        for (int i = 0; i < Vars.getInstance().getPlayerList().size(); i++) {
            this.player_names[i] = Vars.getInstance().getPlayer(i).getName();
            this.ships_build[i] = Vars.getInstance().getPlayer(i).getPlanetConquered();
            this.planet_conquered[i] = Vars.getInstance().getPlayer(i).getShipsBuild();
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.paint.setColor(Color.rgb(0, 0, 20));
        this.paint.setAntiAlias(true);
        this.paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), this.paint);
        this.paint.setTextSize(26.0f);
        this.paint.setColor(-256);
        if (Vars.getInstance().getWinner().equals(Vars.getInstance().getHumanPlayer().getName())) {
            canvas.drawText("Congratulations!", (float) ((getWidth() / 2) - (((int) this.paint.measureText("Congratulations!")) / 2)), (float) 45, this.paint);
        } else {
            canvas.drawText("You have been eliminated", (float) ((getWidth() / 2) - (((int) this.paint.measureText("You have been eliminated")) / 2)), (float) 45, this.paint);
        }
        int text_y = (int) (((float) 45) + this.paint.getTextSize() + ((float) (Vars.getInstance().getScaleFactor() * 15)));
        this.paint.setTextSize(26.0f);
        this.paint.setColor(-1);
        String str = "Mighty " + Vars.getInstance().getWinner();
        canvas.drawText(str, (float) ((getWidth() / 2) - (((int) this.paint.measureText(str)) / 2)), (float) text_y, this.paint);
        int text_y2 = (int) (((float) text_y) + this.paint.getTextSize() + ((float) (Vars.getInstance().getScaleFactor() * 15)));
        canvas.drawText(" conquered galaxy!", (float) ((getWidth() / 2) - (((int) this.paint.measureText(" conquered galaxy!")) / 2)), (float) text_y2, this.paint);
        int text_y3 = (int) (((float) text_y2) + this.paint.getTextSize() + ((float) (Vars.getInstance().getScaleFactor() * 10)));
        this.paint.setTextSize(14.0f);
        int text_y4 = (int) (((float) text_y3) + this.paint.getTextSize() + ((float) (Vars.getInstance().getScaleFactor() * 10)));
        this.paint.setColor(-1);
        canvas.drawText("Name", 10.0f, (float) text_y4, this.paint);
        canvas.drawText("Solars conquered", (float) (getWidth() / 3), (float) text_y4, this.paint);
        canvas.drawText("Ships built", (float) ((getWidth() * 3) / 4), (float) text_y4, this.paint);
        for (int i = 0; i < Vars.getInstance().getPlayerList().size(); i++) {
            text_y4 = (int) (((float) text_y4) + this.paint.getTextSize() + ((float) (Vars.getInstance().getScaleFactor() * 6)));
            setColorById(i);
            canvas.drawText(Vars.getInstance().getPlayer(i).getName(), 10.0f, (float) text_y4, this.paint);
            canvas.drawText(new StringBuilder().append(Vars.getInstance().getPlayer(i).getPlanetConquered()).toString(), (float) (getWidth() / 2), (float) text_y4, this.paint);
            canvas.drawText(new StringBuilder().append(Vars.getInstance().getPlayer(i).getShipsBuild()).toString(), (float) ((getWidth() * 4) / 5), (float) text_y4, this.paint);
        }
        for (int i2 = 0; i2 < Vars.getInstance().getButtonList().size(); i2++) {
            Vars.getInstance().getButton(i2).draw(canvas, this.paint);
        }
    }

    public void setColorById(int id) {
        if (id < 0) {
            this.paint.setColor(-7829368);
        } else if (id == 0) {
            this.paint.setColor(-16711936);
        } else if (id == 1) {
            this.paint.setColor(-65536);
        } else if (id == 2) {
            this.paint.setColor(-16776961);
        } else if (id == 3) {
            this.paint.setColor(Color.rgb(187, 8, 181));
        } else if (id == 4) {
            this.paint.setColor(Color.rgb(113, 207, 207));
        } else if (id == 5) {
            this.paint.setColor(Color.rgb(158, 154, 39));
        } else if (id == 6) {
            this.paint.setColor(Color.rgb(246, 149, 219));
        } else if (id == 7) {
            this.paint.setColor(Color.rgb(230, 230, 230));
        } else if (id == 8) {
            this.paint.setColor(Color.rgb(183, 25, 0));
        } else {
            this.paint.setColor(Color.rgb(70, 70, 70));
        }
    }

    public void mouseDown(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                postInvalidate();
                return;
            }
        }
    }

    public void mouseUp(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                postInvalidate();
                return;
            }
        }
    }

    public void mouseDrag(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
        }
        if (pressed != 0) {
            postInvalidate();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        postInvalidate();
        return true;
    }

    public void handleCommand(int id) {
        if (id == Vars.getInstance().CLOSE_ENDGAME) {
            SolarActivity.getInstance().showMenu();
        }
    }
}
