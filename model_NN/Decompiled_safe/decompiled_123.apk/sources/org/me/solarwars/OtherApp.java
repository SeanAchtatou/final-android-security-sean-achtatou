package org.me.solarwars;

import java.util.Random;

public class OtherApp {
    static OtherApp instance = null;
    public String[] button_text = {"  Try Rotate Me", "  Try Frogs Jump"};
    public int current;
    public String[] images = {"rotateme_ad", "frogs_jump_ad"};
    Random r = new Random();
    public String[] urls = {"market://details?id=softkos.rotateme", "market://details?id=sofkos.frogsjump"};

    public OtherApp() {
        random();
    }

    public void random() {
        this.current = this.r.nextInt(this.button_text.length);
    }

    public static OtherApp getInstance() {
        if (instance == null) {
            instance = new OtherApp();
        }
        return instance;
    }
}
