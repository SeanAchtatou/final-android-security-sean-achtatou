package org.me.solarwars;

public class BattleReport {
    int human_result;
    String player1_name;
    String player2_name;
    int player_id1;
    int player_id2;
    int px;
    int py;
    int result;
    String str1;
    String str2;

    public void setNames(String s1, String s2) {
        this.player1_name = s1;
        this.player2_name = s2;
    }

    public BattleReport() {
        this.human_result = -1;
        this.human_result = -1;
    }

    public void setPos(int x, int y) {
        this.px = x;
        this.py = y;
    }

    public void init(int p1, int p2, int res) {
        this.player_id1 = p1;
        this.player_id2 = p2;
        this.result = res;
    }
}
