package org.me.solarwars;

import android.graphics.drawable.Drawable;

public class ImageLoader {
    static ImageLoader instance = null;
    public int ARROW_LEFT = 38;
    public int ARROW_RIGHT = 39;
    public int BIDY_AD = 49;
    public int CANCEL = 47;
    public int ENDTURNBTN = 37;
    public int FLEET = 44;
    public int FLEETIMG_00 = 9;
    public int FLEETIMG_01 = 10;
    public int FLEETIMG_02 = 11;
    public int FLEETIMG_04 = 13;
    public int FLEETIMG_05 = 14;
    public int FLEETIMG_06 = 15;
    public int FLEETIMG_07 = 16;
    public int FLEETIMG_08 = 17;
    public int FLEETIMG_09 = 18;
    public int FLEETIMG_12 = 9;
    public int FLEET_LEFT = 42;
    public int FLEET_RIGHT = 43;
    public int MAKEFLEET = 48;
    public int MAKE_FLEET = 7;
    public int MENU_BACK = 0;
    public int OK = 46;
    public int PUSH_OFF_0 = 19;
    public int PUSH_OFF_1 = 20;
    public int PUSH_OFF_2 = 21;
    public int PUSH_OFF_3 = 22;
    public int PUSH_OFF_4 = 23;
    public int PUSH_OFF_5 = 24;
    public int PUSH_OFF_6 = 25;
    public int PUSH_OFF_7 = 26;
    public int PUSH_OFF_8 = 27;
    public int PUSH_ON_0 = 28;
    public int PUSH_ON_1 = 29;
    public int PUSH_ON_2 = 30;
    public int PUSH_ON_3 = 31;
    public int PUSH_ON_4 = 32;
    public int PUSH_ON_5 = 33;
    public int PUSH_ON_6 = 34;
    public int PUSH_ON_7 = 35;
    public int PUSH_ON_8 = 36;
    public int SET_TARGET = 8;
    public int SOLAR = 45;
    public int SOLAR_01 = 1;
    public int SOLAR_02 = 2;
    public int SOLAR_03 = 3;
    public int SOLAR_04 = 4;
    public int SOLAR_05 = 5;
    public int SOLAR_LEFT = 40;
    public int SOLAR_RIGHT = 41;
    public int UBACK = 6;
    Drawable[] images;
    int[] img_id = {R.drawable.menuback, R.drawable.solar_01, R.drawable.solar_02, R.drawable.solar_03, R.drawable.solar_04, R.drawable.solar_05, R.drawable.uback, R.drawable.make_fleet, R.drawable.set_target, R.drawable.fleetimg_0, R.drawable.fleetimg_1, R.drawable.fleetimg_2, R.drawable.fleetimg_3, R.drawable.fleetimg_4, R.drawable.fleetimg_5, R.drawable.fleetimg_6, R.drawable.fleetimg_7, R.drawable.fleetimg_8, R.drawable.fleetimg_9, R.drawable.push_off_0, R.drawable.push_off_1, R.drawable.push_off_2, R.drawable.push_off_3, R.drawable.push_off_4, R.drawable.push_off_5, R.drawable.push_off_6, R.drawable.push_off_7, R.drawable.push_off_8, R.drawable.push_on_0, R.drawable.push_on_1, R.drawable.push_on_2, R.drawable.push_on_3, R.drawable.push_on_4, R.drawable.push_on_5, R.drawable.push_on_6, R.drawable.push_on_7, R.drawable.push_on_8, R.drawable.endturn, R.drawable.arrowleft, R.drawable.arrowright, R.drawable.solar_left, R.drawable.solar_right, R.drawable.fleet_left, R.drawable.fleet_right, R.drawable.fleet, R.drawable.solar, R.drawable.ok, R.drawable.cancel, R.drawable.makefleet, R.drawable.frogs_jump_ad};

    public static ImageLoader getInstance() {
        if (instance == null) {
            instance = new ImageLoader();
        }
        return instance;
    }

    public void LoadImages() {
        int len = this.img_id.length;
        this.images = new Drawable[50];
        for (int i = 0; i < len; i++) {
            this.images[i] = SolarActivity.getInstance().getResources().getDrawable(this.img_id[i]);
        }
    }

    public Drawable getImage(int img) {
        if (img < this.images.length) {
            return this.images[img];
        }
        return null;
    }
}
