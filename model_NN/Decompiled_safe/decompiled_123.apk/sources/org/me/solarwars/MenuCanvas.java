package org.me.solarwars;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;

public class MenuCanvas extends View {
    gButton aboutBtn;
    gButton addAiPlayerBtn;
    int aiPlayerNum = 3;
    gButton aiPlayerNumBtn;
    gButton loadGameBtn;
    String mapSize = "medium";
    gButton mapSizeAddBtn;
    gButton mapSizeNumBtn;
    gButton mapSizeSubBtn;
    gButton otherAppBtn;
    Paint paint = new Paint();
    gButton playBtn;
    gButton subAiPlayerBtn;

    public MenuCanvas(Context context) {
        super(context);
        initUI();
    }

    public void initUI() {
        this.playBtn = new gButton();
        this.playBtn.setText("New game");
        this.playBtn.setId(Vars.getInstance().PLAY_BTN_ID);
        Vars.getInstance().addButton(this.playBtn);
        this.aboutBtn = new gButton();
        this.aboutBtn.setText("More free apps");
        this.aboutBtn.setId(Vars.getInstance().ABOUT_BTN);
        Vars.getInstance().addButton(this.aboutBtn);
        this.otherAppBtn = new gButton();
        this.otherAppBtn.setText("   Frogs jump");
        this.otherAppBtn.setId(Vars.getInstance().OTHER_APP);
        this.otherAppBtn.setImage(ImageLoader.getInstance().BIDY_AD);
        Vars.getInstance().addButton(this.otherAppBtn);
        this.loadGameBtn = new gButton();
        this.loadGameBtn.setText("Continue game");
        this.loadGameBtn.setId(Vars.getInstance().LOAD_GAME);
        Vars.getInstance().addButton(this.loadGameBtn);
        this.addAiPlayerBtn = new gButton();
        this.addAiPlayerBtn.setId(Vars.getInstance().ADD_AI_PLAYER);
        this.addAiPlayerBtn.setImage(ImageLoader.getInstance().ARROW_RIGHT);
        Vars.getInstance().addButton(this.addAiPlayerBtn);
        this.subAiPlayerBtn = new gButton();
        this.subAiPlayerBtn.setId(Vars.getInstance().SUB_AI_PLAYER);
        this.subAiPlayerBtn.setImage(ImageLoader.getInstance().ARROW_LEFT);
        Vars.getInstance().addButton(this.subAiPlayerBtn);
        this.aiPlayerNumBtn = new gButton();
        this.aiPlayerNumBtn.setEnabled(false);
        this.aiPlayerNumBtn.setId(Vars.getInstance().PLAYERS_NUM_BTN);
        this.aiPlayerNumBtn.setText("AI players: " + this.aiPlayerNum);
        Vars.getInstance().addButton(this.aiPlayerNumBtn);
        this.mapSizeAddBtn = new gButton();
        this.mapSizeAddBtn.setId(Vars.getInstance().MAP_SIZE_ADD);
        this.mapSizeAddBtn.setImage(ImageLoader.getInstance().ARROW_RIGHT);
        Vars.getInstance().addButton(this.mapSizeAddBtn);
        this.mapSizeSubBtn = new gButton();
        this.mapSizeSubBtn.setId(Vars.getInstance().MAP_SIZE_SUB);
        this.mapSizeSubBtn.setImage(ImageLoader.getInstance().ARROW_LEFT);
        Vars.getInstance().addButton(this.mapSizeSubBtn);
        this.mapSizeNumBtn = new gButton();
        this.mapSizeNumBtn.setEnabled(false);
        this.mapSizeNumBtn.setId(Vars.getInstance().MAP_SIZE_BTN);
        this.mapSizeNumBtn.setText("Map: " + this.mapSize);
        Vars.getInstance().addButton(this.mapSizeNumBtn);
    }

    public void layoutUI() {
        this.playBtn.setSize(238, 50);
        this.playBtn.setPosition((getWidth() / 2) - (this.playBtn.getWidth() / 2), getHeight() - ((int) (((double) this.playBtn.getHeight()) * 1.2d)));
        this.loadGameBtn.setSize(238, 50);
        this.loadGameBtn.setPosition((getWidth() / 2) - (this.playBtn.getWidth() / 2), this.playBtn.getPy() - ((int) (((double) this.loadGameBtn.getHeight()) * 1.1d)));
        this.aboutBtn.setSize(238, 50);
        this.aboutBtn.setPosition((getWidth() / 2) - (this.loadGameBtn.getWidth() / 2), this.loadGameBtn.getPy() - ((int) (((double) this.aboutBtn.getHeight()) * 1.1d)));
        this.otherAppBtn.setSize(238, 50);
        this.otherAppBtn.setPosition((getWidth() / 2) - (this.otherAppBtn.getWidth() / 2), this.aboutBtn.getPy() - ((int) (((double) this.otherAppBtn.getHeight()) * 1.1d)));
        int by = (int) (((double) getHeight()) / 3.8d);
        this.subAiPlayerBtn.setSize(50, 50);
        this.addAiPlayerBtn.setSize(50, 50);
        this.addAiPlayerBtn.setPosition((int) (((double) getWidth()) - (((double) this.addAiPlayerBtn.getWidth()) * 1.1d)), by);
        this.subAiPlayerBtn.setPosition((int) (((double) getWidth()) - (((double) (this.addAiPlayerBtn.getWidth() * 2)) * 1.1d)), by);
        this.aiPlayerNumBtn.setSize(getWidth() - ((int) (((double) 50) * 2.4d)), 50);
        this.aiPlayerNumBtn.setPosition((int) (((double) this.addAiPlayerBtn.getWidth()) * 0.1d), by);
        this.mapSizeAddBtn.setSize(50, 50);
        this.mapSizeSubBtn.setSize(50, 50);
        int y1 = this.subAiPlayerBtn.getPy() + ((int) (((double) this.subAiPlayerBtn.getHeight()) * 1.1d));
        this.mapSizeAddBtn.setPosition((int) (((double) getWidth()) - (((double) this.addAiPlayerBtn.getWidth()) * 1.1d)), y1);
        this.mapSizeSubBtn.setPosition((int) (((double) getWidth()) - (((double) (this.addAiPlayerBtn.getWidth() * 2)) * 1.1d)), y1);
        this.mapSizeNumBtn.setSize(getWidth() - ((int) (((double) 50) * 2.4d)), 50);
        this.mapSizeNumBtn.setPosition((int) (((double) this.addAiPlayerBtn.getWidth()) * 0.1d), this.mapSizeSubBtn.getPy());
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        layoutUI();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.paint.setColor(-16777216);
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), this.paint);
        ImageLoader.getInstance().getImage(ImageLoader.getInstance().UBACK).setBounds(0, 0, getWidth(), getHeight());
        ImageLoader.getInstance().getImage(ImageLoader.getInstance().UBACK).draw(canvas);
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            Vars.getInstance().getButtonList().get(i).draw(canvas, this.paint);
        }
        this.paint.setTextSize(50.0f);
        this.paint.setColor(-256);
        canvas.drawText(Strings.appName, (float) ((getWidth() / 2) - (((int) this.paint.measureText(Strings.appName)) / 2)), 60.0f, this.paint);
    }

    public void handleCommand(int id) {
        if (id == Vars.getInstance().PLAY_BTN_ID) {
            Vars.getInstance().setupNewGame(this.aiPlayerNum, this.mapSize);
            SolarActivity.getInstance().showGame();
        }
        if (id == Vars.getInstance().LOAD_GAME && FileWR.getInstance().loadGame()) {
            SolarActivity.getInstance().showGame();
        }
        if (id == Vars.getInstance().ADD_AI_PLAYER) {
            this.aiPlayerNum++;
            if (this.aiPlayerNum > 8) {
                this.aiPlayerNum = 8;
            }
            this.aiPlayerNumBtn.setText("AI players: " + this.aiPlayerNum);
        }
        if (id == Vars.getInstance().SUB_AI_PLAYER) {
            this.aiPlayerNum--;
            if (this.aiPlayerNum < 1) {
                this.aiPlayerNum = 1;
            }
            this.aiPlayerNumBtn.setText("AI players: " + this.aiPlayerNum);
        }
        if (id == Vars.getInstance().MAP_SIZE_ADD) {
            if (this.mapSize.equals("small")) {
                this.mapSize = "medium";
            } else if (this.mapSize.equals("medium")) {
                this.mapSize = "big";
            } else if (this.mapSize.equals("big")) {
                this.mapSize = "small";
            }
            this.mapSizeNumBtn.setText("Map: " + this.mapSize);
        }
        if (id == Vars.getInstance().MAP_SIZE_SUB) {
            if (this.mapSize.equals("small")) {
                this.mapSize = "big";
            } else if (this.mapSize.equals("medium")) {
                this.mapSize = "small";
            } else if (this.mapSize.equals("big")) {
                this.mapSize = "medium";
            }
            this.mapSizeNumBtn.setText("Map: " + this.mapSize);
        }
        if (id == Vars.getInstance().ABOUT_BTN) {
            SolarActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.mobilsoft.pl")));
        }
        if (id == Vars.getInstance().OTHER_APP) {
            SolarActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=sofkos.frogsjump")));
        }
        invalidate();
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            int pressed = 0;
            for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
                pressed |= Vars.getInstance().getButtonList().get(i).mouseDown(x, y);
                if (pressed != 0) {
                    invalidate();
                    return true;
                }
            }
        } else if (2 == event.getAction()) {
            int pressed2 = 0;
            for (int i2 = 0; i2 < Vars.getInstance().getButtonList().size(); i2++) {
                pressed2 |= Vars.getInstance().getButtonList().get(i2).mouseDrag(x, y);
            }
            if (pressed2 != 0) {
                invalidate();
                return true;
            }
        } else if (1 == event.getAction()) {
            int pressed3 = 0;
            for (int i3 = 0; i3 < Vars.getInstance().getButtonList().size(); i3++) {
                pressed3 |= Vars.getInstance().getButtonList().get(i3).mouseUp(x, y);
                if (pressed3 != 0) {
                    handleCommand(Vars.getInstance().getButtonList().get(i3).getId());
                    invalidate();
                    return true;
                }
            }
        }
        invalidate();
        return true;
    }
}
