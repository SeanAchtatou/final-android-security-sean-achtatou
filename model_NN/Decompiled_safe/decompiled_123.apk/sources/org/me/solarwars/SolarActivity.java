package org.me.solarwars;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import org.me.solarwars.Vars;

public class SolarActivity extends Activity {
    static SolarActivity instance = null;
    EndGameCanvas endGameCanvas = null;
    GameCanvas gameCanvas = null;
    LinearLayout gameLayout;
    MenuCanvas menuCanvas = null;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        instance = this;
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Vars.getInstance().screenSize(dm.widthPixels, dm.heightPixels);
        ImageLoader.getInstance().LoadImages();
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        this.menuCanvas = new MenuCanvas(getApplicationContext());
        this.endGameCanvas = new EndGameCanvas(getApplicationContext());
        showMenu();
    }

    public void showMenu() {
        Vars.getInstance().gameState = Vars.GameState.Menu;
        Vars.getInstance().hideAllButtons();
        Vars.getInstance().showUI(true);
        setContentView(this.menuCanvas);
    }

    public void showGame() {
        if (this.gameCanvas == null) {
            this.gameLayout = new LinearLayout(this);
            this.gameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            this.gameLayout.setOrientation(1);
            this.gameCanvas = new GameCanvas(this);
            AdView adView = new AdView(this, AdSize.BANNER, "a14c11cecc71b9d");
            adView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            adView.loadAd(new AdRequest());
            this.gameCanvas.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            this.gameLayout.addView(adView);
            this.gameLayout.addView(this.gameCanvas);
        }
        Vars.getInstance().gameState = Vars.GameState.Game;
        Vars.getInstance().hideAllButtons();
        Vars.getInstance().showUI(true);
        setContentView(this.gameLayout);
    }

    public void showEndGame() {
        Vars.getInstance().gameState = Vars.GameState.EndGame;
        Vars.getInstance().hideAllButtons();
        this.endGameCanvas.initCanvas();
        Vars.getInstance().showUI(true);
        setContentView(this.endGameCanvas);
    }

    public static SolarActivity getInstance() {
        return instance;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && (Vars.getInstance().gameState == Vars.GameState.Game || Vars.getInstance().gameState == Vars.GameState.MakeFleet || Vars.getInstance().gameState == Vars.GameState.About || Vars.getInstance().gameState == Vars.GameState.SaveGameDlg || Vars.getInstance().gameState == Vars.GameState.EndGame)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyUp(keyCode, event);
        }
        if (Vars.getInstance().gameState == Vars.GameState.Game) {
            askForSaveAndShowMenu();
        } else if (Vars.getInstance().gameState == Vars.GameState.SaveGameDlg || Vars.getInstance().gameState == Vars.GameState.About) {
            showMenu();
        } else if (Vars.getInstance().gameState == Vars.GameState.EndGame) {
            showMenu();
        } else if (Vars.getInstance().gameState == Vars.GameState.MakeFleet) {
            showGame();
        }
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2000, 0, "Save game").setIcon((int) R.drawable.menu_save);
        menu.add(0, 2001, 0, "Load game").setIcon((int) R.drawable.menu_load);
        menu.add(1, 2002, 1, "Cancel");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2000:
                saveGame();
                return true;
            case 2001:
                loadGame();
                return true;
            case 2002:
                return true;
            default:
                return false;
        }
    }

    public void saveGame() {
        if (Vars.getInstance().gameState == Vars.GameState.Game) {
            FileWR.getInstance().saveGame();
            showInfoDialog("Game saved", "Save game");
            this.gameCanvas.postInvalidate();
        }
    }

    public void loadGame() {
        if (Vars.getInstance().gameState == Vars.GameState.Game && FileWR.getInstance().loadGame()) {
            showInfoDialog("Game loaded", "Load game");
            this.gameCanvas.postInvalidate();
        }
        if (Vars.getInstance().gameState == Vars.GameState.Menu && FileWR.getInstance().loadGame()) {
            getInstance().showGame();
        }
    }

    public void showInfoDialog(String msg, String title) {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
        alertbox.setTitle(title);
        alertbox.setMessage(msg);
        alertbox.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        alertbox.show();
    }

    public void askForSaveAndShowMenu() {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
        alertbox.setTitle("Save game");
        alertbox.setMessage("Do you want save current game?");
        alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                FileWR.getInstance().saveGame();
                SolarActivity.getInstance().showMenu();
            }
        });
        alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                SolarActivity.getInstance().showMenu();
            }
        });
        alertbox.show();
    }
}
