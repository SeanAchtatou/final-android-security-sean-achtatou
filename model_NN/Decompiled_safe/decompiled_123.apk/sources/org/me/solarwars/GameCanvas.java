package org.me.solarwars;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import java.util.ArrayList;
import org.me.solarwars.Player;
import org.me.solarwars.Vars;

public class GameCanvas extends View {
    static GameCanvas instance = null;
    Arrow arrow;
    Button closeDialogButton = null;
    Dialog createArmyDialog = null;
    gButton endTurnBtn;
    gButton makeFleetBtn;
    Button maxShipsButton = null;
    int mouseDownX = 0;
    int mouseDownY = 0;
    gButton nextFleetBtn;
    gButton nextPlanetBtn;
    Button okCreateArmyButton = null;
    Paint paint;
    int panelSize = 126;
    gButton prevFleetBtn;
    gButton prevPlanetBtn;
    String prodString = "";
    gButton setTargetBtn;
    int sliderCount = 0;
    TextView unitCount = null;
    TextView unitCountPlanet = null;
    SeekBar unitsSlider = null;
    int universeBackColor;
    Vars vars;
    int vpx;
    int vpy;

    public GameCanvas(Context context) {
        super(context);
        instance = this;
        this.paint = new Paint();
        this.arrow = new Arrow();
        this.universeBackColor = Color.rgb(0, 0, 20);
        CMap.getInstance().SetScreenSize(Vars.getInstance().getScreenWidth(), Vars.getInstance().getScreenHeight() - this.panelSize);
        initUI();
    }

    public void initUI() {
        this.endTurnBtn = new gButton();
        this.endTurnBtn.setImage(ImageLoader.getInstance().ENDTURNBTN);
        this.endTurnBtn.setId(Vars.getInstance().END_TURN);
        Vars.getInstance().addButton(this.endTurnBtn);
        this.nextFleetBtn = new gButton();
        this.nextFleetBtn.setImage(ImageLoader.getInstance().FLEET_RIGHT);
        this.nextFleetBtn.setId(Vars.getInstance().NEXT_FLEET);
        Vars.getInstance().addButton(this.nextFleetBtn);
        this.prevFleetBtn = new gButton();
        this.prevFleetBtn.setImage(ImageLoader.getInstance().FLEET_LEFT);
        this.prevFleetBtn.setId(Vars.getInstance().PREV_FLEET);
        Vars.getInstance().addButton(this.prevFleetBtn);
        this.nextPlanetBtn = new gButton();
        this.nextPlanetBtn.setImage(ImageLoader.getInstance().SOLAR_RIGHT);
        this.nextPlanetBtn.setId(Vars.getInstance().NEXT_PLANET);
        Vars.getInstance().addButton(this.nextPlanetBtn);
        this.prevPlanetBtn = new gButton();
        this.prevPlanetBtn.setImage(ImageLoader.getInstance().SOLAR_LEFT);
        this.prevPlanetBtn.setId(Vars.getInstance().PREV_PLANET);
        Vars.getInstance().addButton(this.prevPlanetBtn);
        this.makeFleetBtn = new gButton();
        this.makeFleetBtn.setImage(ImageLoader.getInstance().MAKE_FLEET);
        this.makeFleetBtn.setId(Vars.getInstance().MAKE_FLEET);
        Vars.getInstance().addButton(this.makeFleetBtn);
        this.setTargetBtn = new gButton();
        this.setTargetBtn.setImage(ImageLoader.getInstance().SET_TARGET);
        this.setTargetBtn.setId(Vars.getInstance().SET_TARGET);
        Vars.getInstance().addButton(this.setTargetBtn);
    }

    public void layoutUI() {
        this.endTurnBtn.setSize(62, 62);
        this.endTurnBtn.setPosition((getWidth() - 62) - 1, (getHeight() - 62) - 1);
        this.nextFleetBtn.setSize(62, 62);
        this.nextFleetBtn.setPosition((this.endTurnBtn.getPx() - 62) - 1, (getHeight() - 62) - 1);
        this.prevFleetBtn.setSize(62, 62);
        this.prevFleetBtn.setPosition((this.nextFleetBtn.getPx() - 62) - 1, (getHeight() - 62) - 1);
        this.nextPlanetBtn.setSize(62, 62);
        int i = 62 + 1;
        this.nextPlanetBtn.setPosition((this.endTurnBtn.getPx() - 62) - 1, getHeight() - (2 * 63));
        this.prevPlanetBtn.setSize(62, 62);
        int i2 = 62 + 1;
        this.prevPlanetBtn.setPosition((this.nextPlanetBtn.getPx() - 62) - 1, getHeight() - (2 * 63));
        this.makeFleetBtn.setSize(62, 62);
        int i3 = 62 + 1;
        this.makeFleetBtn.setPosition((getWidth() - 62) - 1, getHeight() - (2 * 63));
        this.setTargetBtn.setSize(62, 62);
        int i4 = 62 + 1;
        this.setTargetBtn.setPosition((getWidth() - 62) - 1, getHeight() - (2 * 63));
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        layoutUI();
        invalidate();
    }

    public void drawPlanets(Canvas canvas) {
        if (Vars.getInstance().getPlanetList() != null) {
            int plist_size = Vars.getInstance().getPlanetList().size();
            this.paint.setTextSize(12.0f);
            if (Vars.getInstance().getPlanetList() != null && plist_size > 0) {
                for (int i = 0; i < plist_size; i++) {
                    if (Vars.getInstance().getPlanet(i) != null) {
                        int ptype = Vars.getInstance().getPlanet(i).GetPlanetType();
                        int planet_size = (int) Vars.getInstance().getPlanet(i).getWidth();
                        int xx = ((int) Vars.getInstance().getPlanet(i).getPx()) - this.vpx;
                        int yy = ((int) Vars.getInstance().getPlanet(i).getPy()) - this.vpy;
                        if (xx <= getWidth() && yy <= getHeight() && (planet_size * 2) + xx >= 0 && (planet_size * 2) + yy >= 0) {
                            drawImage(canvas, ImageLoader.getInstance().SOLAR_01 + ptype, xx, yy, planet_size, planet_size);
                            setColorById(Vars.getInstance().getPlanet(i).getOwnerID());
                            canvas.drawText(Vars.getInstance().getPlanet(i).getName(), (float) (xx + 40), (float) (yy + 8), this.paint);
                            this.prodString = "Prod: " + Vars.getInstance().getPlanet(i).getProduction();
                            canvas.drawText(this.prodString, (float) (xx + 40), ((float) yy) + this.paint.getTextSize() + 12.0f, this.paint);
                            if (Vars.getInstance().getPlanet(i).getOwnerID() < 0 || Vars.getInstance().getPlayerById(Vars.getInstance().getPlanet(i).getOwnerID()).getPlayerType() != Player.PlayerType.Human) {
                                this.prodString = "Ships: ?";
                            } else {
                                this.prodString = "Ships: " + String.valueOf(Vars.getInstance().getPlanet(i).getShips());
                            }
                            canvas.drawText(this.prodString, (float) (xx + 40), ((float) yy) + (2.0f * (this.paint.getTextSize() + 8.0f)), this.paint);
                            if (Vars.getInstance().getSelectedPlanet() == i) {
                                this.paint.setColor(-1);
                                int s = planet_size / 4;
                                canvas.drawLine((float) xx, (float) yy, (float) (xx + s), (float) yy, this.paint);
                                canvas.drawLine((float) xx, (float) yy, (float) xx, (float) (yy + s), this.paint);
                                canvas.drawLine((float) xx, (float) (yy + planet_size), (float) (xx + s), (float) (yy + planet_size), this.paint);
                                canvas.drawLine((float) xx, (float) ((yy + planet_size) - s), (float) xx, (float) (yy + planet_size), this.paint);
                                canvas.drawLine((float) (xx + planet_size), (float) yy, (float) ((xx - s) + planet_size), (float) yy, this.paint);
                                canvas.drawLine((float) (xx + planet_size), (float) yy, (float) (xx + planet_size), (float) (yy + s), this.paint);
                                canvas.drawLine((float) (xx + planet_size), (float) (yy + planet_size), (float) ((xx - s) + planet_size), (float) (yy + planet_size), this.paint);
                                canvas.drawLine((float) (xx + planet_size), (float) ((yy + planet_size) - s), (float) (xx + planet_size), (float) (yy + planet_size), this.paint);
                            }
                        }
                    }
                }
            }
        }
    }

    public void setColorById(int id) {
        if (id < 0) {
            this.paint.setColor(-7829368);
        } else if (id == 0) {
            this.paint.setColor(-16711936);
        } else if (id == 1) {
            this.paint.setColor(-65536);
        } else if (id == 2) {
            this.paint.setColor(-16776961);
        } else if (id == 3) {
            this.paint.setColor(Color.rgb(187, 8, 181));
        } else if (id == 4) {
            this.paint.setColor(Color.rgb(113, 207, 207));
        } else if (id == 5) {
            this.paint.setColor(Color.rgb(158, 154, 39));
        } else if (id == 6) {
            this.paint.setColor(Color.rgb(246, 149, 219));
        } else if (id == 7) {
            this.paint.setColor(Color.rgb(230, 230, 230));
        } else if (id == 8) {
            this.paint.setColor(Color.rgb(183, 25, 0));
        } else {
            this.paint.setColor(Color.rgb(70, 70, 70));
        }
    }

    public int CalcAngle(int px, int py, int x2, int y2) {
        int angle;
        double c = Math.sqrt((double) (((x2 - px) * (x2 - px)) + ((y2 - py) * (y2 - py))));
        if (x2 < px) {
            angle = ((int) ((180.0d * Math.asin(((double) (py - y2)) / c)) / 3.141592653589793d)) + 90;
        } else {
            angle = ((int) ((180.0d * Math.asin(((double) (y2 - py)) / c)) / 3.141592653589793d)) - 90;
        }
        while (angle < 0) {
            angle += 360;
        }
        while (angle >= 360) {
            angle -= 360;
        }
        return angle;
    }

    public void drawArrows(Canvas canvas, int x1, int y1, int x2, int y2) {
        this.arrow.SetAngle(CalcAngle(x2 - this.vpx, y2 - this.vpy, x1 - this.vpx, y1 - this.vpy));
        if (Math.abs(x2 - x1) >= 2 || Math.abs(y2 - y1) >= 2) {
            double size = 1.5d * ((double) this.arrow.GetSize());
            double ds = Math.sqrt((double) (((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1))));
            double vx = (((double) (x2 - x1)) * size) / ds;
            double vy = (((double) (y2 - y1)) * size) / ds;
            int count = (int) (((double) (x2 - x1)) / vx);
            for (int a = 1; a < count; a++) {
                this.arrow.SetDrawingOffset((int) (((double) x1) + (((double) a) * vx)), (int) (((double) y1) + (((double) a) * vy)));
                this.arrow.draw(canvas, this.paint);
            }
        }
    }

    public void drawFleets(Canvas canvas) {
        this.paint.setTextSize(12.0f);
        for (int i = 0; i < Vars.getInstance().getFleetList().size(); i++) {
            int fleet_size = (int) Vars.getInstance().getFleet(i).getWidth();
            int xx = ((int) Vars.getInstance().getFleet(i).getPx()) - this.vpx;
            int yy = ((int) Vars.getInstance().getFleet(i).getPy()) - this.vpy;
            int dxx = ((int) Vars.getInstance().getFleet(i).getDestPx()) - this.vpx;
            int dyy = ((int) Vars.getInstance().getFleet(i).getDestPy()) - this.vpy;
            setColorById(Vars.getInstance().getFleet(i).getOwnerID());
            drawArrows(canvas, xx, yy, dxx, dyy);
            canvas.drawOval(new RectF((float) (dxx - (16 / 6)), (float) (dyy - (16 / 6)), (float) ((dxx - (16 / 6)) + (16 / 3)), (float) ((dyy - (16 / 6)) + (16 / 3))), this.paint);
            drawImage(canvas, ImageLoader.getInstance().FLEETIMG_00 + Vars.getInstance().getFleet(i).getOwnerID(), xx - (fleet_size / 2), yy - (fleet_size / 2), fleet_size, fleet_size);
            this.paint.setColor(-1);
            this.prodString = "?";
            if (Vars.getInstance().getPlayerById(Vars.getInstance().getFleet(i).getOwnerID()).getPlayerType() == Player.PlayerType.Human) {
                this.prodString = new StringBuilder().append(Vars.getInstance().getFleet(i).getShips()).toString();
            }
            canvas.drawText(this.prodString, (float) (xx - (fleet_size / 3)), (float) (yy - (fleet_size / 4)), this.paint);
            if (Vars.getInstance().getSelectedFleet() == i) {
                this.paint.setColor(-1);
                int s = fleet_size / 4;
                int xx2 = xx - (fleet_size / 2);
                int yy2 = yy - (fleet_size / 2);
                canvas.drawLine((float) xx2, (float) yy2, (float) (xx2 + s), (float) yy2, this.paint);
                canvas.drawLine((float) xx2, (float) yy2, (float) xx2, (float) (yy2 + s), this.paint);
                canvas.drawLine((float) xx2, (float) (yy2 + fleet_size), (float) (xx2 + s), (float) (yy2 + fleet_size), this.paint);
                canvas.drawLine((float) xx2, (float) ((yy2 + fleet_size) - s), (float) xx2, (float) (yy2 + fleet_size), this.paint);
                canvas.drawLine((float) (xx2 + fleet_size), (float) yy2, (float) ((xx2 - s) + fleet_size), (float) yy2, this.paint);
                canvas.drawLine((float) (xx2 + fleet_size), (float) yy2, (float) (xx2 + fleet_size), (float) (yy2 + s), this.paint);
                canvas.drawLine((float) (xx2 + fleet_size), (float) (yy2 + fleet_size), (float) ((xx2 - s) + fleet_size), (float) (yy2 + fleet_size), this.paint);
                canvas.drawLine((float) (xx2 + fleet_size), (float) ((yy2 + fleet_size) - s), (float) (xx2 + fleet_size), (float) (yy2 + fleet_size), this.paint);
                int xx3 = xx2 + (fleet_size / 2);
                int i2 = (fleet_size / 2) + yy2;
            }
        }
    }

    public void EndTurn() {
        Vars.getInstance().getScreenMsgList().clear();
        Vars.getInstance().getBattleReportList().clear();
        Vars.getInstance().selectFleet(-1);
        Vars.getInstance().selectPlanet(-1);
        Vars.getInstance().UpdatePlanets();
        Vars.getInstance().MakeAIMoves();
        Vars.getInstance().MoveFleets();
        if (Vars.getInstance().isEndGame()) {
            SolarActivity.getInstance().showEndGame();
            return;
        }
        Vars.getInstance().showUI(true);
        invalidate();
    }

    public void updateVp() {
        this.vpx = CMap.getInstance().GetVpx();
        this.vpy = CMap.getInstance().GetVpy();
    }

    public void drawBattleReportsMap(Canvas canvas) {
        int brcount = Vars.getInstance().getBattleReportList().size();
        for (int i = 0; i < brcount; i++) {
            BattleReport br = Vars.getInstance().getBattleReport(i);
            int sizex = Math.max((int) this.paint.measureText(br.str1), (int) this.paint.measureText(br.str2)) + 10;
            int sizey = (int) (((double) this.paint.getTextSize()) * 2.6d);
            this.paint.setStyle(Paint.Style.FILL);
            this.paint.setColor(-16776961);
            if (br.human_result == 1) {
                this.paint.setColor(-16711936);
            } else if (br.human_result == 0) {
                this.paint.setColor(-65536);
            }
            canvas.drawRect((float) (br.px - this.vpx), (float) (br.py - this.vpy), (float) ((br.px - this.vpx) + sizex), (float) ((br.py - this.vpy) + sizey), this.paint);
            this.paint.setStyle(Paint.Style.STROKE);
            this.paint.setColor(-1);
            canvas.drawRect((float) (br.px - this.vpx), (float) (br.py - this.vpy), (float) ((br.px - this.vpx) + sizex), (float) ((br.py - this.vpy) + sizey), this.paint);
            canvas.drawText(br.str1, (float) ((br.px - this.vpx) + 5), ((float) (br.py - this.vpy)) + this.paint.getTextSize() + 2.0f, this.paint);
            canvas.drawText(br.str2, (float) ((br.px - this.vpx) + 5), ((float) (br.py - this.vpy)) + (this.paint.getTextSize() * 2.0f) + 4.0f, this.paint);
        }
    }

    public void drawScreenMessages(Canvas canvas) {
        this.paint.setTextSize(12.0f);
        int th = (int) (this.paint.getTextSize() + ((float) (Vars.getInstance().getScaleFactor() * 3)));
        int y = (getHeight() - this.panelSize) - (Vars.getInstance().getScaleFactor() * 6);
        int yoffset = 0;
        for (int i = Vars.getInstance().getScreenMsgList().size() - 1; i >= 0; i--) {
            this.paint.setColor(Vars.getInstance().getScreenMsg(i).color);
            canvas.drawText(Vars.getInstance().getScreenMsg(i).msg, 10.0f, (float) (y - yoffset), this.paint);
            yoffset += th;
        }
    }

    public void drawGame(Canvas canvas) {
        updateVp();
        this.paint.setColor(this.universeBackColor);
        this.paint.setAntiAlias(true);
        this.paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), this.paint);
        drawImage(canvas, ImageLoader.getInstance().UBACK, (-this.vpx) / 3, (-this.vpy) / 3, ((-this.vpx) / 3) + CMap.getInstance().GetWidth(), ((-this.vpy) / 3) + CMap.getInstance().GetHeight());
        drawPlanets(canvas);
        drawFleets(canvas);
        drawBattleReportsMap(canvas);
        drawScreenMessages(canvas);
        this.paint.setStyle(Paint.Style.FILL);
        this.paint.setColor(-16777216);
        canvas.drawRect(0.0f, (float) (getHeight() - this.panelSize), (float) getHeight(), (float) getHeight(), this.paint);
        drawMiniMap(canvas);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.paint == null) {
            this.paint = new Paint();
        }
        if (canvas != null) {
            drawGame(canvas);
            for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
                Vars.getInstance().getButton(i).draw(canvas, this.paint);
            }
        }
    }

    public void drawImage(Canvas canvas, int image, int x, int y, int w, int h) {
        if (ImageLoader.getInstance().getImage(image) != null) {
            ImageLoader.getInstance().getImage(image).setBounds(x, y, x + w, y + h);
            ImageLoader.getInstance().getImage(image).draw(canvas);
        }
    }

    public void drawMiniMap(Canvas g) {
        int x = Vars.getInstance().getScaleFactor();
        int y = (getHeight() - this.panelSize) + Vars.getInstance().getScaleFactor();
        int w = this.panelSize - (Vars.getInstance().getScaleFactor() * 2);
        int h = w;
        drawImage(g, ImageLoader.getInstance().UBACK, x, y, w, h);
        int map_w = CMap.getInstance().GetWidth();
        int map_h = CMap.getInstance().GetHeight();
        Vars.getInstance().getPlanetList();
        if (Vars.getInstance().getPlanetList() != null) {
            for (int i = 0; i < Vars.getInstance().getPlanetList().size(); i++) {
                int px = (int) ((((double) w) * Vars.getInstance().getPlanet(i).getPx()) / ((double) map_w));
                int py = (int) ((((double) h) * Vars.getInstance().getPlanet(i).getPy()) / ((double) map_h));
                this.paint.setColor(-3355444);
                int ownerid = Vars.getInstance().getPlanet(i).getOwnerID();
                int size = Vars.getInstance().getScaleFactor() * 2;
                if (ownerid < 0) {
                    this.paint.setColor(-12303292);
                } else {
                    size = Vars.getInstance().getScaleFactor() * 3;
                }
                setColorById(ownerid);
                g.drawRect((float) (px + x), (float) (py + y), (float) (px + x + size), (float) (py + y + size), this.paint);
            }
        }
        ArrayList<Fleet> flist = Vars.getInstance().getFleetList();
        if (flist != null) {
            for (int i2 = 0; i2 < flist.size(); i2++) {
                if (flist.get(i2) != null) {
                    int px2 = (int) ((((double) w) * Vars.getInstance().getFleet(i2).getPx()) / ((double) map_w));
                    int py2 = (int) ((((double) h) * Vars.getInstance().getFleet(i2).getPy()) / ((double) map_h));
                    this.paint.setColor(-3355444);
                    int ownerid2 = Vars.getInstance().getFleet(i2).getOwnerID();
                    int size2 = Vars.getInstance().getScaleFactor() * 1;
                    if (ownerid2 < 0) {
                        this.paint.setColor(-12303292);
                    } else {
                        size2 = Vars.getInstance().getScaleFactor() * 2;
                    }
                    setColorById(ownerid2);
                    g.drawRect((float) (px2 + x), (float) (py2 + y), (float) (px2 + x + size2), (float) (py2 + y + size2), this.paint);
                }
            }
        }
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setColor(-7829368);
        g.drawRect((float) x, (float) y, (float) (x + w), (float) (y + h), this.paint);
        double x_scale = ((double) w) / ((double) CMap.getInstance().GetWidth());
        double y_scale = ((double) h) / ((double) CMap.getInstance().GetHeight());
        this.paint.setColor(-1);
        int mw = (int) (((double) CMap.getInstance().getScreenWidth()) * x_scale);
        int mh = (int) (((double) CMap.getInstance().getScreenHeight()) * y_scale);
        int sx = ((int) (((double) CMap.getInstance().GetVpx()) * x_scale)) + x;
        int sy = ((int) (((double) CMap.getInstance().GetVpy()) * y_scale)) + y;
        g.drawRect((float) sx, (float) sy, (float) ((sx + mw) - 1), (float) ((sy + mh) - 1), this.paint);
    }

    public void checkPlanetSelection(int x, int y) {
        boolean selected = false;
        int last_selected = Vars.getInstance().getSelectedPlanet();
        ArrayList plist = Vars.getInstance().getPlanetList();
        int i = 0;
        while (true) {
            if (i < plist.size()) {
                if (((double) x) > Vars.getInstance().getPlanet(i).getPx() - ((double) this.vpx) && ((double) x) < (Vars.getInstance().getPlanet(i).getPx() - ((double) this.vpx)) + Vars.getInstance().getPlanet(i).getWidth() && ((double) y) > Vars.getInstance().getPlanet(i).getPy() - ((double) this.vpy) && ((double) y) < (Vars.getInstance().getPlanet(i).getPy() - ((double) this.vpy)) + Vars.getInstance().getPlanet(i).getHeight()) {
                    Vars.getInstance().selectPlanet(i);
                    selected = true;
                    break;
                }
                i++;
            } else {
                break;
            }
        }
        Vars.getInstance().showUI(true);
        if (selected && last_selected >= 0 && last_selected == Vars.getInstance().getSelectedPlanet()) {
            if (Vars.getInstance().getPlayerList().get(Vars.getInstance().getCurrPlayer()).getId() == Vars.getInstance().getPlanet(last_selected).getOwnerID()) {
                openCreateArmyDialog();
            }
        }
    }

    public boolean checkFleetSelection(int x, int y) {
        int fsize = Vars.getInstance().getFleetList().size();
        int i = 0;
        while (i < fsize) {
            int fx = (int) Vars.getInstance().getFleet(i).getPx();
            int fy = (int) Vars.getInstance().getFleet(i).getPy();
            int fw = (int) Vars.getInstance().getFleet(i).getWidth();
            int fh = (int) Vars.getInstance().getFleet(i).getHeight();
            if (x <= (fx - this.vpx) - (fw / 2) || x >= (fx - this.vpx) + (fw / 2) || y <= (fy - this.vpy) - (fh / 2) || y >= (fy - this.vpy) + (fh / 2)) {
                i++;
            } else {
                Vars.getInstance().selectFleet(i);
                Vars.getInstance().showUI(true);
                return true;
            }
        }
        return false;
    }

    public void minimapMouseMove(int x, int y) {
        int spacer = Vars.getInstance().getScaleFactor();
        int scaleFactor = Vars.getInstance().getScaleFactor();
        int height = (getHeight() - this.panelSize) + spacer;
        int mw = this.panelSize - (spacer * 2);
        int mh = mw;
        if (y > getHeight() - this.panelSize) {
            int width = mw;
            int height2 = mh;
            int mapWidth = CMap.getInstance().GetWidth();
            int mapHeight = CMap.getInstance().GetHeight();
            if (y > (getHeight() - spacer) - height2 && y < getHeight() - spacer && x > spacer && x < spacer + width) {
                int dy = y - ((getHeight() - spacer) - height2);
                float pdx = (((float) (x - spacer)) / ((float) width)) - ((((float) getWidth()) / ((float) mapWidth)) / 2.0f);
                int nvpx = (int) (((float) mapWidth) * pdx);
                int nvpy = (int) (((float) mapHeight) * ((((float) dy) / ((float) height2)) - ((((float) getHeight()) / ((float) mapHeight)) / 4.0f)));
                if (nvpx < 0) {
                    nvpx = 0;
                }
                if (nvpx > mapWidth - getWidth()) {
                    nvpx = mapWidth - getWidth();
                }
                if (nvpy < 0) {
                    nvpy = 0;
                }
                if (nvpy > (mapHeight - getHeight()) + this.panelSize) {
                    nvpy = (mapHeight - getHeight()) + this.panelSize;
                }
                CMap.getInstance().SetVp(nvpx, nvpy);
            }
        }
    }

    public void mouseDown(int x, int y) {
        this.mouseDownX = x;
        this.mouseDownY = y;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
        if (Vars.getInstance().gameState == Vars.GameState.Game) {
            if (y < getHeight() - this.panelSize) {
                CMap.getInstance().startScroll(x, y);
            } else {
                minimapMouseMove(x, y);
            }
            updateVp();
        }
    }

    public void mouseDrag(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
        if (Vars.getInstance().gameState == Vars.GameState.Game) {
            if (y < getHeight() - this.panelSize) {
                CMap.getInstance().scroll(x, y);
            } else {
                minimapMouseMove(x, y);
            }
            updateVp();
        }
    }

    public void mouseUp(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
        if (Vars.getInstance().gameState == Vars.GameState.Game) {
            updateVp();
            double panned = Math.sqrt((double) (((x - this.mouseDownX) * (x - this.mouseDownX)) + ((y - this.mouseDownY) * (y - this.mouseDownY))));
            int lastSelectedFleet = Vars.getInstance().getSelectedFleet();
            if (panned < ((double) (Vars.getInstance().getScaleFactor() * 12))) {
                if (y < getHeight() - this.panelSize && !checkFleetSelection(x, y) && Vars.getInstance().getSelectedFleet() < 0) {
                    checkPlanetSelection(x, y);
                }
                int sf = Vars.getInstance().getSelectedFleet();
                if (lastSelectedFleet == sf && y < getHeight() - this.panelSize && sf >= 0) {
                    if (Vars.getInstance().getFleet(sf).getOwnerID() == Vars.getInstance().getPlayer(Vars.getInstance().getCurrPlayer()).getId()) {
                        Vars.getInstance().getFleet(sf).setDestPos((double) (this.vpx + x), (double) (this.vpy + y));
                        Vars.getInstance().getScreenMsgList().clear();
                        Vars.getInstance().addTutorialMsg("Click on End turn button to move fleets");
                        Vars.getInstance().selectFleet(-1);
                    }
                }
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (Vars.getInstance().gameState == Vars.GameState.Game) {
            updateVp();
        }
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    public void centerOnSelectedFleet() {
        int sf = Vars.getInstance().getSelectedFleet();
        if (sf >= 0 && Vars.getInstance().getFleet(sf) != null) {
            CMap.getInstance().centerOnPoint((int) Vars.getInstance().getFleet(sf).getPx(), (int) Vars.getInstance().getFleet(sf).getPy());
        }
    }

    public void centerOnSelectedPlanet() {
        int sp = Vars.getInstance().getSelectedPlanet();
        if (sp >= 0 && Vars.getInstance().getPlanet(sp) != null) {
            CMap.getInstance().centerOnPoint((int) Vars.getInstance().getPlanet(sp).getPx(), (int) Vars.getInstance().getPlanet(sp).getPy());
        }
    }

    public void handleCommand(int id) {
        if (id == Vars.getInstance().END_TURN) {
            EndTurn();
        } else if (id == Vars.getInstance().NEXT_FLEET) {
            Vars.getInstance().selectNextFleet();
            centerOnSelectedFleet();
        } else if (id == Vars.getInstance().PREV_FLEET) {
            Vars.getInstance().selectPrevFleet();
            centerOnSelectedFleet();
        } else if (id == Vars.getInstance().NEXT_PLANET) {
            Vars.getInstance().selectNextPlanet();
            centerOnSelectedPlanet();
        } else if (id == Vars.getInstance().PREV_PLANET) {
            Vars.getInstance().selectPrevPlanet();
            centerOnSelectedPlanet();
        } else if (id == Vars.getInstance().MAKE_FLEET) {
            openCreateArmyDialog();
        } else {
            int i = Vars.getInstance().SET_TARGET;
        }
    }

    public static GameCanvas getInstance() {
        return instance;
    }

    public Dialog getCreateArmyDialog() {
        return this.createArmyDialog;
    }

    public void updateSliderCount() {
        this.unitCount.setText("  Fleet: " + this.sliderCount);
        this.unitCountPlanet.setText("Planet: " + (this.unitsSlider.getMax() - this.sliderCount) + " ");
    }

    public void openCreateArmyDialog() {
        this.createArmyDialog = new Dialog(SolarActivity.getInstance());
        this.createArmyDialog.setContentView((int) R.layout.createarmy_layout);
        this.createArmyDialog.setTitle("Create army");
        this.unitCount = (TextView) this.createArmyDialog.findViewById(R.id.unit_count);
        this.unitCountPlanet = (TextView) this.createArmyDialog.findViewById(R.id.unit_count_planet);
        this.unitsSlider = (SeekBar) this.createArmyDialog.findViewById(R.id.units_slider);
        ((ImageView) this.createArmyDialog.findViewById(R.id.image)).setImageResource(R.drawable.makefleet);
        int curr_planet = Vars.getInstance().getSelectedPlanet();
        if (curr_planet >= 0) {
            this.unitsSlider.setMax(Vars.getInstance().getPlanet(curr_planet).getShips());
            this.unitsSlider.setProgress(Vars.getInstance().getPlanet(curr_planet).getShips() / 3);
            this.sliderCount = Vars.getInstance().getPlanet(curr_planet).getShips() / 3;
            updateSliderCount();
        }
        this.unitsSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                GameCanvas.this.sliderCount = progress;
                GameCanvas.this.updateSliderCount();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        this.createArmyDialog.show();
        this.okCreateArmyButton = (Button) this.createArmyDialog.findViewById(R.id.create_army_ok);
        this.okCreateArmyButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Vars.getInstance().makeFleet(Vars.getInstance().getSelectedPlanet(), GameCanvas.this.sliderCount);
                GameCanvas.this.getCreateArmyDialog().dismiss();
                GameCanvas.getInstance().postInvalidate();
            }
        });
        this.closeDialogButton = (Button) this.createArmyDialog.findViewById(R.id.create_army_cancel);
        this.closeDialogButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                GameCanvas.this.getCreateArmyDialog().dismiss();
            }
        });
        this.maxShipsButton = (Button) this.createArmyDialog.findViewById(R.id.max_units);
        this.maxShipsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                GameCanvas.this.sliderCount = GameCanvas.this.unitsSlider.getMax();
                GameCanvas.this.unitsSlider.setProgress(GameCanvas.this.sliderCount);
                GameCanvas.this.updateSliderCount();
            }
        });
    }
}
