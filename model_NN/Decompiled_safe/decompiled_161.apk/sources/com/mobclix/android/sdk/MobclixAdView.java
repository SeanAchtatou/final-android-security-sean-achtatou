package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ViewFlipper;
import com.mobclix.android.sdk.Mobclix;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Timer;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class MobclixAdView extends ViewFlipper {
    private static final long MINIMUM_REFRESH_TIME = 15000;
    static int debugOrdinal = 0;
    static HashMap<String, Long> lastAutoplayTime = new HashMap<>();
    private String TAG = "MobclixAdView";
    MobclixCreative ad = null;
    String adCode = "";
    /* access modifiers changed from: private */
    public String adSpace = "";
    private Thread adThread;
    int allowAutoplay = -1;
    int backgroundColor = 0;
    Context context;
    Mobclix controller = null;
    JSONArray creatives = null;
    final AdResponseHandler handler = new AdResponseHandler(this, null);
    float height;
    /* access modifiers changed from: private */
    public MobclixInstrumentation instrumentation = MobclixInstrumentation.getInstance();
    String instrumentationGroup = null;
    private boolean isManuallyPaused = false;
    HashSet<MobclixAdViewListener> listeners = new HashSet<>();
    Object lock = new Object();
    int nCreative = 0;
    int ordinal = 0;
    MobclixCreative prevAd = null;
    String rawResponse = null;
    final RemoteConfigReadyHandler rcHandler = new RemoteConfigReadyHandler(this, null);
    /* access modifiers changed from: private */
    public long refreshRate = 0;
    String requestedAdUrlForAdView = null;
    int requireUserInteraction = -1;
    boolean restored = false;
    boolean rotate = true;
    float scale = 1.0f;
    String size;
    boolean testMode = false;
    private Timer timer = null;
    float width;

    MobclixAdView(Context a, String s) {
        super(a);
        this.context = a;
        this.size = s;
        try {
            initialize((Activity) a);
        } catch (Exception e) {
        }
    }

    MobclixAdView(Context a, String s, AttributeSet attrs) {
        super(a, attrs);
        this.context = a;
        this.size = s;
        String colorString = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "background");
        if (colorString != null) {
            this.backgroundColor = Color.parseColor(colorString);
        }
        try {
            initialize((Activity) a);
        } catch (Exception e) {
        }
    }

    public void setRequestedAdUrlForAdView(String url) {
        this.requestedAdUrlForAdView = url;
    }

    public String getRequestedAdUrlForAdView() {
        return this.requestedAdUrlForAdView;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 7) {
                try {
                    super.onDetachedFromWindow();
                    super.stopFlipping();
                } catch (IllegalArgumentException e) {
                    Log.w(this.TAG, "Android project  issue 6191  workaround.");
                    super.stopFlipping();
                } catch (Throwable th) {
                    super.stopFlipping();
                    throw th;
                }
            } else {
                super.onDetachedFromWindow();
            }
        } catch (Exception e2) {
            super.onDetachedFromWindow();
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        stopFetchAdRequestTimer();
        if (this.ad != null) {
            this.ad.onStop();
        }
        super.finalize();
    }

    public boolean addMobclixAdViewListener(MobclixAdViewListener l) {
        if (l == null) {
            return false;
        }
        return this.listeners.add(l);
    }

    public boolean removeMobclixAdViewListener(MobclixAdViewListener l) {
        return this.listeners.remove(l);
    }

    public void setLayoutParams(ViewGroup.LayoutParams params) {
        int formatWidth = Integer.parseInt(this.size.split("x")[0]);
        int formatHeight = Integer.parseInt(this.size.split("x")[1]);
        if (params.width == formatWidth && params.height == formatHeight) {
            this.width = (float) formatWidth;
            this.height = (float) formatHeight;
        } else {
            this.width = TypedValue.applyDimension(1, (float) formatWidth, getResources().getDisplayMetrics());
            this.height = TypedValue.applyDimension(1, (float) formatHeight, getResources().getDisplayMetrics());
            this.scale = getResources().getDisplayMetrics().density;
        }
        params.width = (int) this.width;
        params.height = (int) this.height;
        super.setLayoutParams(params);
    }

    public void setRefreshTime(long rate) {
        stopFetchAdRequestTimer();
        this.refreshRate = rate;
        if (this.refreshRate >= 0) {
            if (this.refreshRate < MINIMUM_REFRESH_TIME) {
                this.refreshRate = MINIMUM_REFRESH_TIME;
            }
            try {
                this.timer = new Timer();
                this.timer.scheduleAtFixedRate(new FetchAdResponseThread(this.handler), this.refreshRate, this.refreshRate);
            } catch (Exception e) {
            }
        }
    }

    public void setAllowAutoplay(boolean auto) {
        this.allowAutoplay = auto ? 1 : 0;
    }

    public void setRichMediaRequiresUserInteraction(boolean user) {
        this.requireUserInteraction = user ? 1 : 0;
    }

    public void setShouldRotate(boolean shouldRotate) {
        this.rotate = shouldRotate;
    }

    public void setTestMode(boolean t) {
        this.testMode = t;
    }

    public void setAdSpace(String a) {
        this.adSpace = a;
    }

    public boolean allowAutoplay() {
        return this.allowAutoplay == 1;
    }

    public boolean richMediaRequiresUserInteraction() {
        return this.requireUserInteraction == 1;
    }

    public boolean shouldRotate() {
        return this.rotate;
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (!hasWindowFocus) {
            try {
                stopFetchAdRequestTimer();
            } catch (Exception e) {
            }
            try {
                this.controller.location.stopLocation();
            } catch (Exception e2) {
            }
            try {
                if (this.ad != null) {
                    this.ad.onPause();
                }
            } catch (Exception e3) {
            }
        } else if (this.ad != null) {
            if (!this.isManuallyPaused) {
                resume();
            }
            if (this.ad != null) {
                this.ad.onResume();
            }
        }
    }

    /* access modifiers changed from: private */
    public void stopFetchAdRequestTimer() {
        synchronized (this) {
            if (this.timer != null) {
                this.timer.cancel();
                this.timer.purge();
                this.timer = null;
                System.gc();
            }
        }
    }

    public void pause() {
        this.isManuallyPaused = true;
        stopFetchAdRequestTimer();
    }

    public void resume() {
        if (this.controller.isEnabled(this.size)) {
            if (this.refreshRate != 0) {
                setRefreshTime(this.refreshRate);
            } else {
                setRefreshTime(this.controller.getRefreshTime(this.size));
            }
        }
        this.isManuallyPaused = false;
    }

    private void initialize(Activity a) {
        synchronized (this.lock) {
            this.instrumentationGroup = String.valueOf(MobclixInstrumentation.ADVIEW) + "_" + this.size + "_" + (debugOrdinal + 1);
            debugOrdinal++;
        }
        try {
            Mobclix.onCreate(a);
            this.controller = Mobclix.getInstance();
            if (this.controller.getUserAgent().equals("null")) {
                WebSettings settings = new WebView(getContext()).getSettings();
                try {
                    this.controller.setUserAgent((String) settings.getClass().getDeclaredMethod("getUserAgentString", new Class[0]).invoke(settings, new Object[0]));
                } catch (Exception e) {
                    this.controller.setUserAgent("Mozilla/5.0 (Linux; U; Android 1.1; en-us; dream) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2");
                }
            }
        } catch (Exception e2) {
        }
        for (String s : Mobclix.MC_AD_SIZES) {
            if (!lastAutoplayTime.containsKey(s)) {
                lastAutoplayTime.put(s, null);
            }
        }
        setBackgroundColor(this.backgroundColor);
        CookieManager.getInstance().setAcceptCookie(true);
        try {
            this.adSpace = getTag().toString();
        } catch (Exception e3) {
        }
        try {
            Class<?> clazz = Class.forName("com.admob.android.ads.AdManager");
            clazz.getMethod("setTestDevices", String[].class).invoke(null, new String[]{(String) clazz.getField("TEST_EMULATOR").get(null)});
        } catch (Exception e4) {
        }
        if (this.controller != null) {
            new Thread(new WaitForRemoteConfigThread(this, null)).start();
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        super.onSaveInstanceState();
        stopFetchAdRequestTimer();
        if (this.ad == null) {
            return null;
        }
        this.ad.onStop();
        Bundle savedInstanceState = new Bundle();
        savedInstanceState.putString("response", this.rawResponse);
        savedInstanceState.putInt("nCreative", this.nCreative);
        return savedInstanceState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(View.BaseSavedState.EMPTY_STATE);
        try {
            this.rawResponse = ((Bundle) state).getString("response");
            this.nCreative = ((Bundle) state).getInt("nCreative");
            if (!this.rawResponse.equals("")) {
                try {
                    this.creatives = new JSONObject(this.rawResponse).getJSONArray("creatives");
                    this.creatives = new JSONArray(this.rawResponse);
                } catch (Exception e) {
                }
                restoreAd();
                this.restored = true;
            }
        } catch (Exception e2) {
        }
    }

    public void getNextAd(String params) {
        this.ad = null;
        if (this.creatives == null) {
            getAd(params);
            return;
        }
        this.nCreative++;
        if (this.nCreative >= this.creatives.length()) {
            getAd(params);
            return;
        }
        try {
            this.ad = new MobclixCreative(this, this.creatives.getJSONObject(this.nCreative), false);
            if (!this.ad.isInitialized()) {
                getNextAd("");
            }
        } catch (Exception e) {
            getNextAd("");
        }
    }

    public void getAd() {
        if (this.adThread != null) {
            this.adThread.interrupt();
            this.adThread = null;
        }
        this.adThread = new Thread(new FetchAdResponseThread(this.handler));
        this.adThread.start();
    }

    /* access modifiers changed from: package-private */
    public void getAd(String params) {
        if (this.adThread != null) {
            this.adThread.interrupt();
            this.adThread = null;
        }
        String instrPath = this.instrumentation.benchmarkStart(this.instrumentation.startGroup(this.instrumentationGroup, MobclixInstrumentation.ADVIEW), "start_request");
        FetchAdResponseThread fetchAdResponseThread = new FetchAdResponseThread(this.handler);
        fetchAdResponseThread.setNextRequestParams(params);
        this.adThread = new Thread(fetchAdResponseThread);
        this.adThread.start();
        String instrPath2 = this.instrumentation.benchmarkFinishPath(instrPath);
    }

    private void restoreAd() {
        try {
            Mobclix.onCreate((Activity) getContext());
        } catch (Exception e) {
        }
        try {
            if (this.adThread != null) {
                this.adThread.interrupt();
            }
            if (this.controller.isEnabled(this.size)) {
                this.ad = new MobclixCreative(this, this.creatives.getJSONObject(this.nCreative), true);
                setRefreshTime(this.controller.getRefreshTime(this.size));
                return;
            }
            Iterator<MobclixAdViewListener> it = this.listeners.iterator();
            while (it.hasNext()) {
                MobclixAdViewListener listener = it.next();
                if (listener != null) {
                    listener.onFailedLoad(this, MobclixAdViewListener.ADSIZE_DISABLED);
                }
            }
        } catch (Exception e2) {
        }
    }

    public void cancelAd() {
        synchronized (this) {
            if (this.adThread != null) {
                this.adThread.interrupt();
            }
        }
        stopFetchAdRequestTimer();
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int visibility) {
        if (visibility == 0 && this.ad != null && !this.ad.trackingPixelsFired) {
            this.ad.fireOnShowTrackingPixels();
        }
    }

    private class WaitForRemoteConfigThread implements Runnable {
        private WaitForRemoteConfigThread() {
        }

        /* synthetic */ WaitForRemoteConfigThread(MobclixAdView mobclixAdView, WaitForRemoteConfigThread waitForRemoteConfigThread) {
            this();
        }

        public void run() {
            Long t = Long.valueOf(System.currentTimeMillis());
            do {
                try {
                    if (MobclixAdView.this.controller.isRemoteConfigSet() == 1) {
                        break;
                    }
                } catch (Exception e) {
                    return;
                }
            } while (System.currentTimeMillis() - t.longValue() < 10000);
            MobclixAdView.this.rcHandler.sendEmptyMessage(0);
        }
    }

    private class RemoteConfigReadyHandler extends Handler {
        private RemoteConfigReadyHandler() {
        }

        /* synthetic */ RemoteConfigReadyHandler(MobclixAdView mobclixAdView, RemoteConfigReadyHandler remoteConfigReadyHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            if (!MobclixAdView.this.restored) {
                if (MobclixAdView.this.controller.isEnabled(MobclixAdView.this.size)) {
                    if (MobclixAdView.this.allowAutoplay == -1) {
                        MobclixAdView.this.setAllowAutoplay(MobclixAdView.this.controller.shouldAutoplay(MobclixAdView.this.size));
                    }
                    if (MobclixAdView.this.requireUserInteraction == -1) {
                        MobclixAdView.this.setRichMediaRequiresUserInteraction(MobclixAdView.this.controller.rmRequireUserInteraction(MobclixAdView.this.size));
                    }
                    if (MobclixAdView.this.refreshRate != 0) {
                        MobclixAdView.this.setRefreshTime(MobclixAdView.this.refreshRate);
                    } else if (MobclixAdView.this.controller.getRefreshTime(MobclixAdView.this.size) >= MobclixAdView.MINIMUM_REFRESH_TIME) {
                        MobclixAdView.this.setRefreshTime(MobclixAdView.this.controller.getRefreshTime(MobclixAdView.this.size));
                    }
                    MobclixAdView.this.getAd();
                    return;
                }
                Iterator<MobclixAdViewListener> it = MobclixAdView.this.listeners.iterator();
                while (it.hasNext()) {
                    MobclixAdViewListener listener = it.next();
                    if (listener != null) {
                        listener.onFailedLoad(MobclixAdView.this, MobclixAdViewListener.ADSIZE_DISABLED);
                    }
                }
            }
        }
    }

    private class FetchAdResponseThread extends Mobclix.FetchResponseThread {
        String nextRequestParams = "";

        FetchAdResponseThread(Handler h) {
            super("", h);
        }

        public void run() {
            try {
                if (!((ActivityManager) MobclixAdView.this.context.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getPackageName().equals(MobclixAdView.this.context.getPackageName())) {
                    MobclixAdView.this.ordinal = 0;
                    MobclixAdView.this.stopFetchAdRequestTimer();
                    MobclixAdView.this.controller.location.stopLocation();
                    return;
                }
            } catch (Exception e) {
            }
            setUrl(getAdUrl());
            super.run();
        }

        /* access modifiers changed from: package-private */
        public void setNextRequestParams(String p) {
            if (p == null) {
                p = "";
            }
            this.nextRequestParams = p;
        }

        private String getAdUrl() {
            String instrPath = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.startGroup(MobclixAdView.this.instrumentationGroup, MobclixInstrumentation.ADVIEW), "build_request");
            StringBuffer data = new StringBuffer();
            StringBuffer keywordsBuffer = new StringBuffer();
            String keywords = "";
            StringBuffer queryBuffer = new StringBuffer();
            try {
                String instrPath2 = MobclixAdView.this.instrumentation.benchmarkStart(instrPath, "ad_feed_id_params");
                data.append(MobclixAdView.this.controller.getAdServer());
                data.append("?p=android");
                if (MobclixAdView.this.adCode.equals("")) {
                    data.append("&i=").append(URLEncoder.encode(MobclixAdView.this.controller.getApplicationId(), "UTF-8"));
                    data.append("&s=").append(URLEncoder.encode(MobclixAdView.this.size, "UTF-8"));
                } else {
                    data.append("&a=").append(URLEncoder.encode(MobclixAdView.this.adCode, "UTF-8"));
                }
                String instrPath3 = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath2), "software_env");
                data.append("&av=").append(URLEncoder.encode(MobclixAdView.this.controller.getApplicationVersion(), "UTF-8"));
                data.append("&u=").append(URLEncoder.encode(MobclixAdView.this.controller.getDeviceId(), "UTF-8"));
                data.append("&andid=").append(URLEncoder.encode(MobclixAdView.this.controller.getAndroidId(), "UTF-8"));
                data.append("&v=").append(URLEncoder.encode(MobclixAdView.this.controller.getMobclixVersion()));
                data.append("&ct=").append(URLEncoder.encode(MobclixAdView.this.controller.getConnectionType()));
                instrPath = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath3), "hardware_env");
                data.append("&dm=").append(URLEncoder.encode(MobclixAdView.this.controller.getDeviceModel(), "UTF-8"));
                data.append("&hwdm=").append(URLEncoder.encode(MobclixAdView.this.controller.getDeviceHardwareModel(), "UTF-8"));
                data.append("&sv=").append(URLEncoder.encode(MobclixAdView.this.controller.getAndroidVersion(), "UTF-8"));
                data.append("&ua=").append(URLEncoder.encode(MobclixAdView.this.controller.getUserAgent(), "UTF-8"));
                if (MobclixAdView.this.controller.isRooted()) {
                    data.append("&jb=1");
                } else {
                    data.append("&jb=0");
                }
                String instrPath4 = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath), "ad_view_state_id_params");
                data.append("&o=").append(MobclixAdView.this.ordinal);
                MobclixAdView.this.ordinal++;
                if (MobclixAdView.this.allowAutoplay == 1) {
                    data.append("&ap=1");
                } else {
                    data.append("&ap=0");
                }
                if (MobclixAdView.this.adSpace != null && !MobclixAdView.this.adSpace.equals("")) {
                    data.append("&as=").append(URLEncoder.encode(MobclixAdView.this.adSpace));
                }
                if (MobclixAdView.this.testMode) {
                    data.append("&t=1");
                }
                String instrPath5 = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath4), "geo_lo");
                if (!MobclixAdView.this.controller.getGPS().equals("null")) {
                    data.append("&ll=").append(URLEncoder.encode(MobclixAdView.this.controller.getGPS(), "UTF-8"));
                }
                data.append("&l=").append(URLEncoder.encode(MobclixAdView.this.controller.getLocale(), "UTF-8"));
                String instrPath6 = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath5), "keywords");
                Iterator<MobclixAdViewListener> it = MobclixAdView.this.listeners.iterator();
                while (it.hasNext()) {
                    MobclixAdViewListener listener = it.next();
                    if (listener != null) {
                        keywords = listener.keywords();
                    }
                    if (keywords == null) {
                        keywords = "";
                    }
                    if (!keywords.equals("")) {
                        if (keywordsBuffer.length() == 0) {
                            keywordsBuffer.append("&k=").append(URLEncoder.encode(keywords, "UTF-8"));
                        } else {
                            keywordsBuffer.append("%2C").append(URLEncoder.encode(keywords, "UTF-8"));
                        }
                    }
                    String query = listener.query();
                    if (query == null) {
                        query = "";
                    }
                    if (!query.equals("")) {
                        if (queryBuffer.length() == 0) {
                            queryBuffer.append("&q=").append(URLEncoder.encode(query, "UTF-8"));
                        } else {
                            queryBuffer.append("%2B").append(URLEncoder.encode(query, "UTF-8"));
                        }
                    }
                }
                if (keywordsBuffer.length() > 0) {
                    data.append(keywordsBuffer);
                }
                String instrPath7 = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath6), "query");
                if (queryBuffer.length() > 0) {
                    data.append(queryBuffer);
                }
                String instrPath8 = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath7), "query");
                if (MobclixAdView.this.requestedAdUrlForAdView != null && !MobclixAdView.this.requestedAdUrlForAdView.equals("")) {
                    data.append("&adurl=").append(URLEncoder.encode(MobclixAdView.this.requestedAdUrlForAdView, "UTF-8"));
                }
                if (!this.nextRequestParams.equals("")) {
                    data.append(this.nextRequestParams);
                }
                this.nextRequestParams = "";
                String instrPath9 = MobclixAdView.this.instrumentation.benchmarkFinishPath(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath8));
                MobclixAdView.this.instrumentation.addInfo(data.toString(), "request_url", MobclixAdView.this.instrumentationGroup);
                return data.toString();
            } catch (Exception e) {
                String instrPath10 = MobclixAdView.this.instrumentation.benchmarkFinishPath(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath));
                MobclixAdView.this.instrumentation.finishGroup(MobclixAdView.this.instrumentationGroup);
                return "";
            }
        }
    }

    private class AdResponseHandler extends Handler {
        private AdResponseHandler() {
        }

        /* synthetic */ AdResponseHandler(MobclixAdView mobclixAdView, AdResponseHandler adResponseHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            String type = msg.getData().getString("type");
            if (type.equals("success")) {
                String instrPath = MobclixAdView.this.instrumentation.benchmarkStart(MobclixAdView.this.instrumentation.startGroup(MobclixAdView.this.instrumentationGroup, MobclixInstrumentation.ADVIEW), "handle_response");
                if (MobclixAdView.this.ad != null) {
                    MobclixAdView.this.prevAd = MobclixAdView.this.ad;
                }
                try {
                    String instrPath2 = MobclixAdView.this.instrumentation.benchmarkStart(instrPath, "a_decode_json");
                    MobclixAdView.this.rawResponse = msg.getData().getString("response");
                    MobclixAdView.this.instrumentation.addInfo(MobclixAdView.this.rawResponse, "raw_json", MobclixAdView.this.instrumentationGroup);
                    JSONObject response = new JSONObject(MobclixAdView.this.rawResponse);
                    MobclixAdView.this.creatives = response.getJSONArray("creatives");
                    if (MobclixAdView.this.creatives.length() >= 1) {
                        MobclixAdView.this.nCreative = 0;
                        MobclixAdView.this.instrumentation.addInfo(MobclixAdView.this.creatives.getJSONObject(MobclixAdView.this.nCreative), "decoded_json", MobclixAdView.this.instrumentationGroup);
                        MobclixAdView.this.ad = new MobclixCreative(MobclixAdView.this, MobclixAdView.this.creatives.getJSONObject(MobclixAdView.this.nCreative), false);
                        if (!MobclixAdView.this.ad.isInitialized()) {
                            MobclixAdView.this.getNextAd("");
                        }
                    }
                    String instrPath3 = MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath2);
                } catch (Exception e) {
                    String instrPath4 = MobclixAdView.this.instrumentation.benchmarkFinishPath(MobclixAdView.this.instrumentation.benchmarkFinishPath(instrPath));
                    MobclixAdView.this.instrumentation.finishGroup(MobclixAdView.this.instrumentationGroup);
                    MobclixAdView.this.getNextAd("");
                }
            } else if (type.equals("failure")) {
                int errorCode = msg.getData().getInt("errorCode");
                Iterator<MobclixAdViewListener> it = MobclixAdView.this.listeners.iterator();
                while (it.hasNext()) {
                    MobclixAdViewListener listener = it.next();
                    if (listener != null) {
                        listener.onFailedLoad(MobclixAdView.this, errorCode);
                    }
                }
            }
        }
    }
}
