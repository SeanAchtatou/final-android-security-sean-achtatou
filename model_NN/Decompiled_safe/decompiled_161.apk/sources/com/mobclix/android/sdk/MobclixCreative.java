package com.mobclix.android.sdk;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.webkit.CookieSyncManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.google.ads.AdActivity;
import com.mobclix.android.sdk.Mobclix;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class MobclixCreative extends ViewFlipper {
    /* access modifiers changed from: private */
    public static boolean isPlaying = false;
    private final String TAG = "MobclixCreative";
    /* access modifiers changed from: private */
    public Action action;
    /* access modifiers changed from: private */
    public Stack<Thread> asyncRequestThreads = new Stack<>();
    private String creativeId = "";
    /* access modifiers changed from: private */
    public Thread customAdThread;
    final ResourceResponseHandler handler = new ResourceResponseHandler();
    private boolean hasAutoplayed = false;
    private boolean initialized = false;
    private MobclixInstrumentation instrumentation = MobclixInstrumentation.getInstance();
    /* access modifiers changed from: private */
    public boolean loop = true;
    /* access modifiers changed from: private */
    public int numPages = 1;
    private ArrayList<String> onLoadUrls = new ArrayList<>();
    private ArrayList<String> onTouchUrls = new ArrayList<>();
    final PageCycleHandler pageCycleHandler = new PageCycleHandler();
    /* access modifiers changed from: private */
    public Timer pageCycleTimer = null;
    MobclixAdView parentAdView;
    boolean trackingPixelsFired = false;
    private int transitionTime = 3000;
    private String transitionType = "none";
    private String type = "";
    /* access modifiers changed from: private */
    public int visiblePage = 0;

    MobclixCreative(MobclixAdView a, JSONObject responseObject, boolean ap) {
        super(a.getContext());
        this.parentAdView = a;
        String instrPath = this.instrumentation.benchmarkStart(this.instrumentation.startGroup(this.parentAdView.instrumentationGroup, MobclixInstrumentation.ADVIEW), "handle_response");
        if (responseObject == null) {
            addView(new CustomAdPage(this));
            this.numPages = 1;
            this.type = "customAd";
            this.initialized = true;
            return;
        }
        String instrPath2 = this.instrumentation.benchmarkStart(instrPath, "b_build_models");
        JSONObject creative = responseObject;
        try {
            JSONObject eventUrls = creative.getJSONObject("eventUrls");
            try {
                JSONArray t = eventUrls.getJSONArray("onShow");
                for (int i = 0; i < t.length(); i++) {
                    this.onLoadUrls.add(t.getString(i));
                }
            } catch (Exception e) {
            }
            JSONArray t2 = eventUrls.getJSONArray("onTouch");
            for (int i2 = 0; i2 < t2.length(); i2++) {
                this.onTouchUrls.add(t2.getString(i2));
            }
        } catch (Exception e2) {
        }
        try {
            instrPath = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkStart(this.instrumentation.benchmarkFinishPath(instrPath2), "c_build_creative"), "a_determine_type");
            JSONObject properties = creative.getJSONObject("props");
            try {
                this.creativeId = creative.getString("id");
            } catch (JSONException e3) {
            }
            this.type = creative.getString("type");
            String instrPath3 = this.instrumentation.benchmarkFinishPath(instrPath);
            try {
                this.hasAutoplayed = ap;
                this.action = new Action(creative.getJSONObject("action"), this);
            } catch (Exception e4) {
                this.action = new Action(this);
            }
            instrPath = this.instrumentation.benchmarkStart(instrPath3, "b_get_view");
            if (this.type.equals(AdActivity.HTML_PARAM)) {
                addView(new HTMLPage(properties.getString(AdActivity.HTML_PARAM), this));
                this.numPages = 1;
                this.initialized = true;
            } else if (this.type.equals("openallocation")) {
                addView(new OpenAllocationPage(properties, this));
                this.numPages = 1;
                this.initialized = true;
            } else {
                try {
                    this.transitionType = properties.getString("transitionType");
                } catch (JSONException e5) {
                }
                setAnimationType(this, this.transitionType);
                try {
                    this.transitionTime = (int) (properties.getDouble("transitionTime") * 1000.0d);
                } catch (JSONException e6) {
                }
                if (this.transitionTime == 0) {
                    this.transitionTime = 3000;
                }
                try {
                    this.loop = properties.getBoolean("loop");
                } catch (JSONException e7) {
                }
                if (this.type.equals("image")) {
                    JSONArray t3 = properties.getJSONArray("images");
                    this.numPages = t3.length();
                    for (int i3 = 0; i3 < t3.length(); i3++) {
                        addView(new ImagePage(t3.getString(i3), this));
                    }
                } else if (this.type.equals("text")) {
                    JSONArray t4 = properties.getJSONArray("texts");
                    this.numPages = t4.length();
                    for (int i4 = 0; i4 < t4.length(); i4++) {
                        addView(new TextPage(t4.getJSONObject(i4), this));
                    }
                }
                String instrPath4 = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkFinishPath(instrPath), "f_load_ad_creative");
                runNextAsyncRequest();
                String instrPath5 = this.instrumentation.benchmarkFinishPath(instrPath4);
                this.initialized = true;
            }
        } catch (JSONException e8) {
            String instrPath6 = this.instrumentation.benchmarkFinishPath(this.instrumentation.benchmarkFinishPath(this.instrumentation.benchmarkFinishPath(instrPath)));
            this.instrumentation.finishGroup(this.parentAdView.instrumentationGroup);
        }
    }

    public String getType() {
        return this.type;
    }

    public String getCreativeId() {
        return this.creativeId;
    }

    public boolean getHasAutoplayed() {
        return this.hasAutoplayed;
    }

    public boolean isInitialized() {
        return this.initialized;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 7) {
                try {
                    super.onDetachedFromWindow();
                    super.stopFlipping();
                } catch (IllegalArgumentException e) {
                    Log.w("MobclixCreative", "Android project  issue 6191  workaround.");
                    super.stopFlipping();
                } catch (Throwable th) {
                    super.stopFlipping();
                    throw th;
                }
            } else {
                super.onDetachedFromWindow();
            }
        } catch (Exception e2) {
            super.onDetachedFromWindow();
        }
    }

    /* access modifiers changed from: private */
    public int dp(int p) {
        return (int) (this.parentAdView.scale * ((float) p));
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public void onPause() {
        synchronized (this) {
            if (this.pageCycleTimer != null) {
                this.pageCycleTimer.cancel();
                this.pageCycleTimer.purge();
            }
        }
        try {
            if (getCurrentView().getClass() == HTMLPage.class) {
                MobclixJavascriptInterface i = ((HTMLPage) getCurrentView()).webview.getJavascriptInterface();
                i.pauseListeners();
                if (!i.expanded) {
                    i.adWillBecomeHidden();
                }
            }
        } catch (Exception e) {
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public void onResume() {
        synchronized (this) {
            if (this.pageCycleTimer != null) {
                this.pageCycleTimer.cancel();
                this.pageCycleTimer.purge();
                this.pageCycleTimer = new Timer();
                this.pageCycleTimer.scheduleAtFixedRate(new PageCycleThread(), (long) this.transitionTime, (long) this.transitionTime);
            }
        }
        try {
            if (getCurrentView().getClass() == HTMLPage.class) {
                MobclixJavascriptInterface i = ((HTMLPage) getCurrentView()).webview.getJavascriptInterface();
                i.resumeListeners();
                if (!i.expanded && i.expanderActivity == null) {
                    i.adDidReturnFromHidden();
                }
                i.expanded = false;
            }
        } catch (Exception e) {
        }
    }

    public void onStop() {
        onPause();
        try {
            if (getCurrentView().getClass() == HTMLPage.class) {
                ((HTMLPage) getCurrentView()).webview.getJavascriptInterface().adWillTerminate();
            }
        } catch (Exception e) {
        }
    }

    public boolean onTouchEvent(MotionEvent e) {
        try {
            if (this.type.equals(AdActivity.HTML_PARAM)) {
                return super.onTouchEvent(e);
            }
            if (e.getAction() == 0) {
                fireOnTouchTrackingPixels();
                return this.action.act();
            }
            return false;
        } catch (Exception e2) {
        }
    }

    public void runNextAsyncRequest() {
        if (!this.asyncRequestThreads.isEmpty()) {
            this.asyncRequestThreads.pop().start();
            return;
        }
        String instrPath = this.instrumentation.benchmarkStart(this.instrumentation.startGroup(this.parentAdView.instrumentationGroup, MobclixInstrumentation.ADVIEW), "handle_response");
        this.visiblePage = 0;
        String instrPath2 = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkStart(this.instrumentation.benchmarkStart(instrPath, "c_build_creative"), "b_get_view"), "c_deque_view");
        while (this.parentAdView.getChildCount() > 1) {
            if (this.parentAdView.getChildAt(0) == this.parentAdView.prevAd) {
                this.parentAdView.removeViewAt(1);
            } else {
                this.parentAdView.removeViewAt(0);
            }
            System.gc();
        }
        String instrPath3 = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkFinishPath(this.instrumentation.benchmarkFinishPath(instrPath2)), "e_add_view");
        this.parentAdView.addView(this);
        String instrPath4 = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkFinishPath(this.instrumentation.benchmarkFinishPath(instrPath3)), "d_bring_onscreen");
        if (this.parentAdView.prevAd != null) {
            this.parentAdView.prevAd.onStop();
            if (this.parentAdView.rotate) {
                setAnimationType(this.parentAdView, "flipRight");
            }
        }
        this.parentAdView.showNext();
        String instrPath5 = this.instrumentation.benchmarkFinishPath(instrPath4);
        if (this.numPages > 1) {
            onPause();
            this.pageCycleTimer = new Timer();
            this.pageCycleTimer.scheduleAtFixedRate(new PageCycleThread(), (long) this.transitionTime, (long) this.transitionTime);
        }
        String instrPath6 = this.instrumentation.benchmarkStart(instrPath5, "e_trigger_events");
        if (this.parentAdView.getVisibility() == 0) {
            fireOnShowTrackingPixels();
        }
        String instrPath7 = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkFinishPath(instrPath6), "f_notify_delegates");
        Iterator<MobclixAdViewListener> it = this.parentAdView.listeners.iterator();
        while (it.hasNext()) {
            MobclixAdViewListener listener = it.next();
            if (listener != null) {
                listener.onSuccessfulLoad(this.parentAdView);
            }
        }
        String instrPath8 = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkFinishPath(instrPath7), "h_handle_autoplay");
        if (this.action != null && this.action.getAutoplay() && this.parentAdView.allowAutoplay() && !this.hasAutoplayed && !isPlaying && MobclixAdView.lastAutoplayTime.get(this.parentAdView.size).longValue() + Mobclix.getInstance().getAutoplayInterval(this.parentAdView.size).longValue() < System.currentTimeMillis()) {
            this.hasAutoplayed = true;
            this.action.act();
            MobclixAdView.lastAutoplayTime.put(this.parentAdView.size, Long.valueOf(System.currentTimeMillis()));
        }
        String instrPath9 = this.instrumentation.benchmarkFinishPath(this.instrumentation.benchmarkFinishPath(instrPath8));
        this.instrumentation.finishGroup(this.parentAdView.instrumentationGroup);
    }

    /* access modifiers changed from: package-private */
    public void fireOnShowTrackingPixels() {
        Iterator<String> it = this.onLoadUrls.iterator();
        while (it.hasNext()) {
            new Thread(new Mobclix.FetchImageThread(it.next(), new Mobclix.BitmapHandler())).start();
        }
        this.trackingPixelsFired = true;
    }

    /* access modifiers changed from: package-private */
    public void fireOnTouchTrackingPixels() {
        Iterator<String> it = this.onTouchUrls.iterator();
        while (it.hasNext()) {
            new Thread(new Mobclix.FetchImageThread(it.next(), new Mobclix.BitmapHandler())).start();
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v6, types: [android.view.animation.Animation] */
    /* JADX WARN: Type inference failed for: r2v49, types: [com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation] */
    /* JADX WARN: Type inference failed for: r2v50, types: [com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation] */
    /* JADX WARN: Type inference failed for: r2v51, types: [android.view.animation.TranslateAnimation] */
    /* JADX WARN: Type inference failed for: r2v52, types: [android.view.animation.TranslateAnimation] */
    /* JADX WARN: Type inference failed for: r2v53, types: [android.view.animation.TranslateAnimation] */
    /* JADX WARN: Type inference failed for: r0v8 */
    /* JADX WARN: Type inference failed for: r0v9 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    public void setAnimationType(android.widget.ViewFlipper r12, java.lang.String r13) {
        /*
            r11 = this;
            if (r13 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.lang.String r2 = "fade"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x002c
            android.view.animation.AlphaAnimation r0 = new android.view.animation.AlphaAnimation
            r2 = 1065353216(0x3f800000, float:1.0)
            r3 = 0
            r0.<init>(r2, r3)
            android.view.animation.AlphaAnimation r1 = new android.view.animation.AlphaAnimation
            r2 = 0
            r3 = 1065353216(0x3f800000, float:1.0)
            r1.<init>(r2, r3)
        L_0x001b:
            r2 = 300(0x12c, double:1.48E-321)
            r0.setDuration(r2)
            r2 = 300(0x12c, double:1.48E-321)
            r1.setDuration(r2)
            r12.setOutAnimation(r0)
            r12.setInAnimation(r1)
            goto L_0x0002
        L_0x002c:
            java.lang.String r2 = "slideRight"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x0051
            android.view.animation.TranslateAnimation r0 = new android.view.animation.TranslateAnimation
            r1 = 2
            r2 = 0
            r3 = 2
            r4 = 1065353216(0x3f800000, float:1.0)
            r5 = 2
            r6 = 0
            r7 = 2
            r8 = 0
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            android.view.animation.TranslateAnimation r1 = new android.view.animation.TranslateAnimation
            r2 = 2
            r3 = -1082130432(0xffffffffbf800000, float:-1.0)
            r4 = 2
            r5 = 0
            r6 = 2
            r7 = 0
            r8 = 2
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x001b
        L_0x0051:
            java.lang.String r2 = "slideLeft"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x0077
            android.view.animation.TranslateAnimation r0 = new android.view.animation.TranslateAnimation
            r3 = 2
            r4 = 0
            r5 = 2
            r6 = -1082130432(0xffffffffbf800000, float:-1.0)
            r7 = 2
            r8 = 0
            r9 = 2
            r10 = 0
            r2 = r0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
            android.view.animation.TranslateAnimation r1 = new android.view.animation.TranslateAnimation
            r2 = 2
            r3 = 1065353216(0x3f800000, float:1.0)
            r4 = 2
            r5 = 0
            r6 = 2
            r7 = 0
            r8 = 2
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x001b
        L_0x0077:
            java.lang.String r2 = "slideUp"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x009e
            android.view.animation.TranslateAnimation r0 = new android.view.animation.TranslateAnimation
            r3 = 2
            r4 = 0
            r5 = 2
            r6 = 0
            r7 = 2
            r8 = 0
            r9 = 2
            r10 = -1082130432(0xffffffffbf800000, float:-1.0)
            r2 = r0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
            android.view.animation.TranslateAnimation r1 = new android.view.animation.TranslateAnimation
            r2 = 2
            r3 = 0
            r4 = 2
            r5 = 0
            r6 = 2
            r7 = 1065353216(0x3f800000, float:1.0)
            r8 = 2
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x001b
        L_0x009e:
            java.lang.String r2 = "slideDown"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x00c5
            android.view.animation.TranslateAnimation r0 = new android.view.animation.TranslateAnimation
            r3 = 2
            r4 = 0
            r5 = 2
            r6 = 0
            r7 = 2
            r8 = 0
            r9 = 2
            r10 = 1065353216(0x3f800000, float:1.0)
            r2 = r0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
            android.view.animation.TranslateAnimation r1 = new android.view.animation.TranslateAnimation
            r2 = 2
            r3 = 0
            r4 = 2
            r5 = 0
            r6 = 2
            r7 = -1082130432(0xffffffffbf800000, float:-1.0)
            r8 = 2
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x001b
        L_0x00c5:
            java.lang.String r2 = "flipRight"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x0117
            com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation r0 = new com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation
            r4 = 0
            r5 = 1119092736(0x42b40000, float:90.0)
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getWidth()
            float r2 = (float) r2
            r3 = 1073741824(0x40000000, float:2.0)
            float r6 = r2 / r3
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getHeight()
            float r2 = (float) r2
            r3 = 1073741824(0x40000000, float:2.0)
            float r7 = r2 / r3
            r8 = 0
            r9 = 1
            r2 = r0
            r3 = r11
            r2.<init>(r4, r5, r6, r7, r8, r9)
            com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation r1 = new com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation
            r3 = -1028390912(0xffffffffc2b40000, float:-90.0)
            r4 = 0
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getWidth()
            float r2 = (float) r2
            r5 = 1073741824(0x40000000, float:2.0)
            float r5 = r2 / r5
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getHeight()
            float r2 = (float) r2
            r6 = 1073741824(0x40000000, float:2.0)
            float r6 = r2 / r6
            r7 = 0
            r8 = 0
            r2 = r11
            r1.<init>(r3, r4, r5, r6, r7, r8)
            r2 = 300(0x12c, double:1.48E-321)
            r1.setStartOffset(r2)
            goto L_0x001b
        L_0x0117:
            java.lang.String r2 = "flipLeft"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x0002
            com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation r0 = new com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation
            r4 = 0
            r5 = -1028390912(0xffffffffc2b40000, float:-90.0)
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getWidth()
            float r2 = (float) r2
            r3 = 1073741824(0x40000000, float:2.0)
            float r6 = r2 / r3
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getHeight()
            float r2 = (float) r2
            r3 = 1073741824(0x40000000, float:2.0)
            float r7 = r2 / r3
            r8 = 0
            r9 = 1
            r2 = r0
            r3 = r11
            r2.<init>(r4, r5, r6, r7, r8, r9)
            com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation r1 = new com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation
            r3 = 1119092736(0x42b40000, float:90.0)
            r4 = 0
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getWidth()
            float r2 = (float) r2
            r5 = 1073741824(0x40000000, float:2.0)
            float r5 = r2 / r5
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getHeight()
            float r2 = (float) r2
            r6 = 1073741824(0x40000000, float:2.0)
            float r6 = r2 / r6
            r7 = 0
            r8 = 0
            r2 = r11
            r1.<init>(r3, r4, r5, r6, r7, r8)
            r2 = 300(0x12c, double:1.48E-321)
            r1.setStartOffset(r2)
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixCreative.setAnimationType(android.widget.ViewFlipper, java.lang.String):void");
    }

    class ResourceResponseHandler extends Handler {
        ResourceResponseHandler() {
        }

        public void handleMessage(Message msg) {
            MobclixCreative.this.runNextAsyncRequest();
        }
    }

    class PageCycleHandler extends Handler {
        PageCycleHandler() {
        }

        public void handleMessage(Message msg) {
            int nextPage = MobclixCreative.this.visiblePage + 1;
            if (nextPage >= MobclixCreative.this.numPages) {
                if (!MobclixCreative.this.loop) {
                    MobclixCreative.this.pageCycleTimer.cancel();
                    return;
                }
                nextPage = 0;
            }
            MobclixCreative.this.visiblePage = nextPage;
            MobclixCreative.this.showNext();
        }
    }

    class PageCycleThread extends TimerTask {
        PageCycleThread() {
        }

        public void run() {
            MobclixCreative.this.pageCycleHandler.sendEmptyMessage(0);
        }
    }

    private class CustomAdThread implements Runnable {
        private String url;

        CustomAdThread(String fetchUrl) {
            this.url = fetchUrl;
        }

        /* JADX WARN: Type inference failed for: r2v3, types: [java.net.URLConnection] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r4 = this;
                r1 = 0
                java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                java.lang.String r3 = r4.url     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                r2.<init>(r3)     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                java.net.URLConnection r2 = r2.openConnection()     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                r0 = r2
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                r1 = r0
                java.lang.String r2 = "GET"
                r1.setRequestMethod(r2)     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                java.lang.String r2 = "User-Agent"
                com.mobclix.android.sdk.MobclixCreative r3 = com.mobclix.android.sdk.MobclixCreative.this     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                com.mobclix.android.sdk.MobclixAdView r3 = r3.parentAdView     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                com.mobclix.android.sdk.Mobclix r3 = r3.controller     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                java.lang.String r3 = r3.getUserAgent()     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                r1.setRequestProperty(r2, r3)     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                r1.connect()     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                r1.disconnect()
            L_0x002a:
                return
            L_0x002b:
                r2 = move-exception
                r1.disconnect()
                goto L_0x002a
            L_0x0030:
                r2 = move-exception
                r1.disconnect()
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixCreative.CustomAdThread.run():void");
        }
    }

    class Page extends RelativeLayout {
        protected HashMap<String, Integer> alignmentMap = new HashMap<>();
        protected int layer;
        protected MobclixCreative parentCreative;
        protected int resourceId;
        protected String type;

        Page(MobclixCreative c) {
            super(c.getContext());
            this.alignmentMap.put("center", 17);
            this.alignmentMap.put("left", 19);
            this.alignmentMap.put("right", 21);
            this.parentCreative = c;
        }

        public MobclixCreative getParentCreative() {
            return this.parentCreative;
        }

        public void setResourceId(int r) {
            this.resourceId = r;
        }

        public void setLayer(int l) {
            this.layer = l;
        }

        public void setType(String t) {
            this.type = t;
        }

        public int getResourceId() {
            return this.resourceId;
        }

        public int getLayer() {
            return this.layer;
        }

        public String getType() {
            return this.type;
        }

        public int getColorFromJSON(JSONObject c) {
            try {
                return Color.argb(c.getInt("a"), c.getInt("r"), c.getInt("g"), c.getInt("b"));
            } catch (JSONException e) {
                return 0;
            }
        }
    }

    private class TextPage extends Page {
        private String bAlign = "center";
        private int bColor = -16776961;
        private String bText = "";
        private TextView bTextView;
        private int bgColor = -1;
        private String bgImgUrl = "null";
        private String hAlign = "center";
        private int hColor = -16776961;
        private String hText = "";
        private TextView hTextView;
        private String leftIconUrl = "null";
        /* access modifiers changed from: private */
        public ImageView leftIconView;
        private String rightIconUrl = "null";
        /* access modifiers changed from: private */
        public ImageView rightIconView;

        TextPage(JSONObject resource, MobclixCreative p) {
            super(p);
            try {
                this.bgColor = getColorFromJSON(resource.getJSONObject("bgColor"));
            } catch (JSONException e) {
            }
            try {
                this.bgImgUrl = resource.getString("bgImg");
            } catch (JSONException e2) {
            }
            try {
                this.leftIconUrl = resource.getString("leftIcon");
            } catch (JSONException e3) {
            }
            if (this.leftIconUrl.equals("")) {
                this.leftIconUrl = "null";
            }
            try {
                this.rightIconUrl = resource.getString("rightIcon");
            } catch (JSONException e4) {
            }
            if (this.rightIconUrl.equals("")) {
                this.rightIconUrl = "null";
            }
            try {
                JSONObject text = resource.getJSONObject("headerText");
                try {
                    this.hAlign = text.getString("alignment");
                } catch (JSONException e5) {
                }
                try {
                    this.hText = text.getString("text");
                } catch (JSONException e6) {
                }
                if (this.hText.equals("null")) {
                    this.hText = "";
                }
                this.hColor = getColorFromJSON(text.getJSONObject("color"));
            } catch (JSONException e7) {
            }
            try {
                JSONObject text2 = resource.getJSONObject("bodyText");
                try {
                    this.bAlign = text2.getString("alignment");
                } catch (JSONException e8) {
                }
                try {
                    this.bText = text2.getString("text");
                } catch (JSONException e9) {
                }
                if (this.bText.equals("null")) {
                    this.bText = "";
                }
                this.bColor = getColorFromJSON(text2.getJSONObject("color"));
            } catch (JSONException e10) {
            }
            createLayout();
            loadIcons();
            if (!this.bgImgUrl.equals("null")) {
                loadBackgroundImage();
            }
        }

        public void createLayout() {
            RelativeLayout.LayoutParams textLayoutParams = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams leftIconLayoutParams = new RelativeLayout.LayoutParams(MobclixCreative.this.dp(48), MobclixCreative.this.dp(48));
            leftIconLayoutParams.addRule(15);
            RelativeLayout.LayoutParams rightIconLayoutParams = new RelativeLayout.LayoutParams(MobclixCreative.this.dp(48), MobclixCreative.this.dp(48));
            rightIconLayoutParams.addRule(15);
            if (this.leftIconUrl.equals("null") && this.rightIconUrl.equals("null")) {
                textLayoutParams.setMargins(MobclixCreative.this.dp(5), 0, MobclixCreative.this.dp(5), 0);
            } else if (!this.leftIconUrl.equals("null") && this.rightIconUrl.equals("null")) {
                textLayoutParams.setMargins(MobclixCreative.this.dp(60), 0, MobclixCreative.this.dp(5), 0);
                leftIconLayoutParams.addRule(9);
                leftIconLayoutParams.setMargins(MobclixCreative.this.dp(5), 0, 0, 0);
            } else if (!this.leftIconUrl.equals("null") || this.rightIconUrl.equals("null")) {
                textLayoutParams.setMargins(MobclixCreative.this.dp(60), 0, MobclixCreative.this.dp(60), 0);
                leftIconLayoutParams.addRule(9);
                leftIconLayoutParams.setMargins(MobclixCreative.this.dp(5), 0, 0, 0);
                rightIconLayoutParams.addRule(11);
                rightIconLayoutParams.setMargins(0, 0, MobclixCreative.this.dp(5), 0);
            } else {
                textLayoutParams.setMargins(MobclixCreative.this.dp(5), 0, MobclixCreative.this.dp(60), 0);
                rightIconLayoutParams.addRule(11);
                rightIconLayoutParams.setMargins(0, 0, MobclixCreative.this.dp(5), 0);
            }
            LinearLayout textLayout = new LinearLayout(this.parentCreative.parentAdView.getContext());
            textLayout.setOrientation(1);
            textLayout.setLayoutParams(textLayoutParams);
            textLayout.setGravity(16);
            if (!this.hText.equals("")) {
                this.hTextView = new TextView(MobclixCreative.this.parentAdView.getContext());
                this.hTextView.setGravity(((Integer) this.alignmentMap.get(this.hAlign)).intValue());
                this.hTextView.setText(Html.fromHtml("<b>" + this.hText + "</b>"));
                this.hTextView.setTextColor(this.hColor);
                textLayout.addView(this.hTextView);
            }
            if (!this.bText.equals("")) {
                this.bTextView = new TextView(MobclixCreative.this.parentAdView.getContext());
                this.bTextView.setGravity(((Integer) this.alignmentMap.get(this.bAlign)).intValue());
                this.bTextView.setText(this.bText);
                this.bTextView.setTextColor(this.bColor);
                textLayout.addView(this.bTextView);
            }
            addView(textLayout);
            if (!this.leftIconUrl.equals("null")) {
                this.leftIconView = new ImageView(MobclixCreative.this.parentAdView.getContext());
                this.leftIconView.setLayoutParams(leftIconLayoutParams);
                addView(this.leftIconView);
            }
            if (!this.rightIconUrl.equals("null")) {
                this.rightIconView = new ImageView(MobclixCreative.this.parentAdView.getContext());
                this.rightIconView.setLayoutParams(rightIconLayoutParams);
                addView(this.rightIconView);
            }
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            setBackgroundColor(this.bgColor);
        }

        public void loadIcons() {
            if (!this.leftIconUrl.equals("null")) {
                this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.leftIconUrl, new Mobclix.BitmapHandler() {
                    public void handleMessage(Message m) {
                        if (this.bmImg != null) {
                            TextPage.this.leftIconView.setImageBitmap(this.bmImg);
                        }
                        TextPage.this.getParentCreative().handler.sendEmptyMessage(0);
                    }
                })));
            }
            if (!this.rightIconUrl.equals("null")) {
                this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.rightIconUrl, new Mobclix.BitmapHandler() {
                    public void handleMessage(Message m) {
                        if (this.bmImg != null) {
                            TextPage.this.rightIconView.setImageBitmap(this.bmImg);
                        }
                        TextPage.this.getParentCreative().handler.sendEmptyMessage(0);
                    }
                })));
            }
        }

        public void loadBackgroundImage() {
            this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.bgImgUrl, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        TextPage.this.setBackgroundDrawable(new BitmapDrawable(this.bmImg));
                    }
                    TextPage.this.getParentCreative().handler.sendEmptyMessage(0);
                }
            })));
        }
    }

    private class ImagePage extends Page {
        private String imgUrl;
        /* access modifiers changed from: private */
        public ImageView imgView;

        ImagePage(String url, MobclixCreative c) {
            super(c);
            this.imgUrl = url;
            createLayout();
            loadImage();
        }

        public void createLayout() {
            RelativeLayout.LayoutParams imgLayoutParams = new RelativeLayout.LayoutParams(-1, -1);
            imgLayoutParams.addRule(15);
            this.imgView = new ImageView(MobclixCreative.this.parentAdView.getContext());
            this.imgView.setLayoutParams(imgLayoutParams);
            addView(this.imgView);
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        }

        public void loadImage() {
            this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.imgUrl, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        ImagePage.this.imgView.setImageBitmap(this.bmImg);
                    }
                    ImagePage.this.getParentCreative().handler.sendEmptyMessage(0);
                }
            })));
        }
    }

    class MobclixWebView extends WebView {
        private String html = null;
        MobclixJavascriptInterface jsInterface;
        boolean loaded = false;
        MobclixAdView parentAdView;

        public MobclixWebView(MobclixAdView adview) {
            super(adview.getContext());
            this.parentAdView = adview;
        }

        public void setJavascriptInterface(MobclixJavascriptInterface jsInt) {
            this.jsInterface = jsInt;
        }

        public MobclixJavascriptInterface getJavascriptInterface() {
            return this.jsInterface;
        }

        public void setAdHtml(String h) {
            this.html = h;
        }

        public void loadAd() {
            try {
                loadDataWithBaseURL(null, this.html, "text/html", "utf-8", null);
            } catch (Exception e) {
            }
        }

        public void triggerOnTouchUrls() {
            try {
                ((HTMLPage) getParent()).parentCreative.action.act();
            } catch (Exception e) {
            }
        }
    }

    private class HTMLPage extends Page {
        private String html;
        private MobclixJavascriptInterface jsInterface;
        /* access modifiers changed from: private */
        public MobclixWebView webview;

        HTMLPage(String h, MobclixCreative c) {
            super(c);
            this.html = h;
            try {
                createLayout();
            } catch (Exception e) {
            }
        }

        public void createLayout() {
            this.webview = new MobclixWebView(MobclixCreative.this.parentAdView);
            this.jsInterface = new MobclixJavascriptInterface(this.webview);
            this.webview.setHorizontalScrollBarEnabled(true);
            this.webview.setVerticalScrollBarEnabled(true);
            this.webview.setScrollBarStyle(33554432);
            this.webview.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            this.webview.addJavascriptInterface(this.jsInterface, "MOBCLIX");
            this.webview.setJavascriptInterface(this.jsInterface);
            WebSettings settings = this.webview.getSettings();
            settings.setSupportZoom(true);
            settings.setJavaScriptEnabled(true);
            try {
                settings.getClass().getDeclaredMethod("setBuiltInZoomControls", Boolean.TYPE).invoke(settings, false);
            } catch (Exception e) {
            }
            this.webview.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Uri uri = Uri.parse(url);
                    if (uri.getScheme().equals("tel") || uri.getScheme().equals("mailto")) {
                        HTMLPage.this.parentCreative.action.type = "null";
                        HTMLPage.this.parentCreative.action.act();
                        if (HTMLPage.this.webview.getJavascriptInterface().expanderActivity != null) {
                            HTMLPage.this.webview.getJavascriptInterface().expanderActivity.wasAdActivity = true;
                        }
                        HTMLPage.this.getContext().startActivity(new Intent("android.intent.action.VIEW", uri));
                        return true;
                    } else if (uri.getScheme().equals("sms")) {
                        HTMLPage.this.parentCreative.action.type = "null";
                        HTMLPage.this.parentCreative.action.act();
                        if (HTMLPage.this.webview.getJavascriptInterface().expanderActivity != null) {
                            HTMLPage.this.webview.getJavascriptInterface().expanderActivity.wasAdActivity = true;
                        }
                        String[] smsUrl = url.split(":");
                        String tmp = String.valueOf(smsUrl[0]) + "://";
                        for (int i = 1; i < smsUrl.length; i++) {
                            tmp = String.valueOf(tmp) + smsUrl[i];
                        }
                        String body = Uri.parse(tmp).getQueryParameter("body");
                        Intent mIntent = new Intent("android.intent.action.VIEW", Uri.parse(url.split("\\?")[0]));
                        mIntent.putExtra("sms_body", body);
                        HTMLPage.this.getContext().startActivity(mIntent);
                        return true;
                    } else {
                        String shouldOpenInNewWindow = uri.getQueryParameter("shouldOpenInNewWindow");
                        if (shouldOpenInNewWindow == null) {
                            shouldOpenInNewWindow = "";
                        } else {
                            shouldOpenInNewWindow.toLowerCase();
                        }
                        if ((HTMLPage.this.webview.getJavascriptInterface().expanderActivity != null || shouldOpenInNewWindow.equals("no")) && !shouldOpenInNewWindow.equals("yes")) {
                            HTMLPage.this.webview.loadUrl(url);
                        } else {
                            if (HTMLPage.this.webview.getJavascriptInterface().expanderActivity != null) {
                                HTMLPage.this.webview.getJavascriptInterface().expanderActivity.wasAdActivity = true;
                            }
                            HTMLPage.this.getContext().startActivity(new Intent("android.intent.action.VIEW", uri));
                            HTMLPage.this.parentCreative.action.type = "null";
                            HTMLPage.this.parentCreative.action.act();
                        }
                        return true;
                    }
                }

                public void onPageFinished(WebView view, String url) {
                    CookieSyncManager.getInstance().sync();
                    if (!HTMLPage.this.webview.loaded) {
                        HTMLPage.this.getParentCreative().handler.sendEmptyMessage(0);
                    }
                    HTMLPage.this.webview.loaded = true;
                }
            });
            this.webview.setWebChromeClient(new WebChromeClient() {
                public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                    Log.v("MobclixCreative", "ALERT: " + url + ", " + message);
                    result.confirm();
                    return true;
                }
            });
            this.webview.setAdHtml(this.html);
            this.webview.loadAd();
            this.webview.setInitialScale(MobclixCreative.this.dp(100));
            this.webview.setFocusable(true);
            addView(this.webview);
        }

        public void onDetachedFromWindow() {
            this.jsInterface.pauseListeners();
        }
    }

    private class CustomAdPage extends Page {
        private ImageView imgView;

        CustomAdPage(MobclixCreative c) {
            super(c);
            createLayout();
        }

        public void createLayout() {
            try {
                Bitmap myBitmap = BitmapFactory.decodeStream(this.parentCreative.parentAdView.context.openFileInput(String.valueOf(this.parentCreative.parentAdView.size) + "_mc_cached_custom_ad.png"));
                RelativeLayout.LayoutParams imgLayoutParams = new RelativeLayout.LayoutParams(-1, -1);
                imgLayoutParams.addRule(15);
                this.imgView = new ImageView(MobclixCreative.this.parentAdView.getContext());
                this.imgView.setLayoutParams(imgLayoutParams);
                this.imgView.setImageBitmap(myBitmap);
                addView(this.imgView);
                setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            } catch (Exception e) {
            }
            getParentCreative().handler.sendEmptyMessage(0);
        }
    }

    private class OpenAllocationPage extends Page {
        RelativeLayout adMobAdView;
        String network = "openadmob";
        String params = null;

        OpenAllocationPage(JSONObject p, MobclixCreative c) {
            super(c);
            int openAllocationCode;
            try {
                this.network = p.getString("network");
            } catch (Exception e) {
            }
            try {
                StringBuffer paramsBuffer = new StringBuffer();
                JSONObject pp = p.getJSONObject("params");
                Iterator<?> i = pp.keys();
                while (i.hasNext()) {
                    String k = i.next().toString();
                    String v = pp.get(k).toString();
                    paramsBuffer.append("&").append(k);
                    paramsBuffer.append("=").append(v);
                }
                this.params = paramsBuffer.toString();
            } catch (Exception e2) {
            }
            boolean eventConsumed = false;
            if (this.network.equals("openadmob")) {
                openAllocationCode = MobclixAdViewListener.SUBALLOCATION_ADMOB;
            } else if (this.network.equals("opengoogle")) {
                openAllocationCode = MobclixAdViewListener.SUBALLOCATION_GOOGLE;
            } else {
                openAllocationCode = MobclixAdViewListener.SUBALLOCATION_OTHER;
            }
            Iterator<MobclixAdViewListener> it = MobclixCreative.this.parentAdView.listeners.iterator();
            while (it.hasNext()) {
                MobclixAdViewListener listener = it.next();
                if (listener != null) {
                    eventConsumed = eventConsumed || listener.onOpenAllocationLoad(MobclixCreative.this.parentAdView, openAllocationCode);
                }
            }
            if (!eventConsumed && openAllocationCode == -750) {
                adMobAllocation();
            }
        }

        /* access modifiers changed from: package-private */
        public void adMobAllocation() {
            try {
                Class<?> AdMobAdViewClass = Class.forName("com.admob.android.ads.AdView");
                this.adMobAdView = (RelativeLayout) AdMobAdViewClass.getConstructor(Activity.class).newInstance((Activity) this.parentCreative.getContext());
                Object adMobAdListener = null;
                Class<?> AdMobAdListenerClass = null;
                try {
                    AdMobAdListenerClass = Class.forName("com.admob.android.ads.AdListener");
                    AdMobInvocationHandler adMobInvocationHandler = new AdMobInvocationHandler();
                    adMobAdListener = AdMobAdListenerClass.cast(Proxy.newProxyInstance(AdMobAdListenerClass.getClassLoader(), new Class[]{AdMobAdListenerClass}, adMobInvocationHandler));
                } catch (Exception e) {
                    Log.v("MobclixCreative", e.toString());
                }
                try {
                    AdMobAdViewClass.getMethod("setAdListener", AdMobAdListenerClass).invoke(this.adMobAdView, adMobAdListener);
                    StringBuilder keywordsBuffer = new StringBuilder();
                    StringBuilder queryBuffer = new StringBuilder();
                    Iterator<MobclixAdViewListener> it = MobclixCreative.this.parentAdView.listeners.iterator();
                    while (it.hasNext()) {
                        MobclixAdViewListener listener = it.next();
                        if (listener != null) {
                            String keywords = listener.keywords();
                            if (keywords == null) {
                                keywords = "";
                            }
                            if (!keywords.equals("")) {
                                keywordsBuffer.append(keywords).append(" ");
                            }
                            String query = listener.query();
                            if (query == null) {
                                query = "";
                            }
                            if (!query.equals("")) {
                                queryBuffer.append(query).append(" ");
                            }
                        }
                    }
                    if (keywordsBuffer.length() > 0) {
                        AdMobAdViewClass.getMethod("setKeywords", String.class).invoke(this.adMobAdView, keywordsBuffer.toString());
                    }
                    if (queryBuffer.length() > 0) {
                        AdMobAdViewClass.getMethod("setSearchQuery", String.class).invoke(this.adMobAdView, queryBuffer.toString());
                    }
                } catch (Exception e2) {
                    Log.v("MobclixCreative", e2.toString());
                }
                this.adMobAdView.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Iterator<MobclixAdViewListener> it = MobclixCreative.this.parentAdView.listeners.iterator();
                        while (it.hasNext()) {
                            MobclixAdViewListener listener = it.next();
                            if (listener != null) {
                                listener.onAdClick(MobclixCreative.this.parentAdView);
                            }
                        }
                    }
                });
                this.adMobAdView.setVisibility(4);
                this.parentCreative.parentAdView.addView(this.adMobAdView);
            } catch (Exception e3) {
            }
        }

        public class AdMobInvocationHandler implements InvocationHandler {
            public AdMobInvocationHandler() {
            }

            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                OpenAllocationPage.this.parentCreative.parentAdView.removeView(OpenAllocationPage.this.adMobAdView);
                if (method.getName().equals("onReceiveAd")) {
                    OpenAllocationPage.this.addView(OpenAllocationPage.this.adMobAdView);
                    OpenAllocationPage.this.adMobAdView.setVisibility(0);
                    OpenAllocationPage.this.getParentCreative().handler.sendEmptyMessage(0);
                    return null;
                }
                if (OpenAllocationPage.this.params == null) {
                    OpenAllocationPage.this.params = "";
                }
                OpenAllocationPage.this.parentCreative.parentAdView.getNextAd(OpenAllocationPage.this.params);
                return null;
            }
        }
    }

    class Action {
        boolean autoplay = false;
        String browserType = "standard";
        String cachedHTML = "";
        Bitmap cachedImageBitmap = null;
        int cachedImageHeight;
        int cachedImageWidth;
        private ArrayList<String> onShowUrls = new ArrayList<>();
        private ArrayList<String> onTouchUrls = new ArrayList<>();
        /* access modifiers changed from: private */
        public MobclixCreative parentCreative;
        private JSONObject rawJSON;
        String type = "null";
        String url = "";

        Action(MobclixCreative creative) {
            this.parentCreative = creative;
        }

        Action(JSONObject action, MobclixCreative creative) {
            try {
                this.rawJSON = action;
                this.parentCreative = creative;
                this.type = action.getString("type");
                if (action.has("autoplay")) {
                    this.autoplay = action.getBoolean("autoplay");
                }
                try {
                    JSONObject eventUrls = action.getJSONObject("eventUrls");
                    JSONArray t = eventUrls.getJSONArray("onShow");
                    for (int i = 0; i < t.length(); i++) {
                        this.onShowUrls.add(t.getString(i));
                    }
                    JSONArray t2 = eventUrls.getJSONArray("onTouch");
                    for (int i2 = 0; i2 < t2.length(); i2++) {
                        this.onTouchUrls.add(t2.getString(i2));
                    }
                } catch (Exception e) {
                }
                if (this.type.equals("url")) {
                    try {
                        this.url = action.getString("url");
                    } catch (JSONException e2) {
                    }
                    try {
                        this.browserType = action.getString("browserType");
                    } catch (JSONException e3) {
                    }
                    try {
                        if (action.getBoolean("preload")) {
                            loadPreload();
                        }
                    } catch (JSONException e4) {
                    }
                } else if (this.type.equals(AdActivity.HTML_PARAM)) {
                    try {
                        this.url = action.getString("baseUrl");
                    } catch (JSONException e5) {
                    }
                    try {
                        this.cachedHTML = action.getString(AdActivity.HTML_PARAM);
                    } catch (JSONException e6) {
                    }
                    try {
                        this.browserType = action.getString("browserType");
                    } catch (JSONException e7) {
                    }
                }
            } catch (JSONException e8) {
            }
        }

        public boolean getAutoplay() {
            return this.autoplay;
        }

        /* access modifiers changed from: package-private */
        public void loadPreload() {
            this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchResponseThread(this.url, new Handler() {
                public void handleMessage(Message m) {
                    String type = m.getData().getString("type");
                    if (type.equals("success")) {
                        Action.this.cachedHTML = m.getData().getString("response");
                    } else if (type.equals("failure")) {
                        Action.this.cachedHTML = "";
                    }
                    Action.this.parentCreative.handler.sendEmptyMessage(0);
                }
            })));
        }

        public boolean act() {
            try {
                Iterator<String> it = this.onShowUrls.iterator();
                while (it.hasNext()) {
                    new Thread(new Mobclix.FetchImageThread(it.next(), new Mobclix.BitmapHandler())).start();
                }
            } catch (Exception e) {
            }
            Iterator<MobclixAdViewListener> it2 = MobclixCreative.this.parentAdView.listeners.iterator();
            while (it2.hasNext()) {
                MobclixAdViewListener listener = it2.next();
                if (listener != null) {
                    listener.onAdClick(MobclixCreative.this.parentAdView);
                }
            }
            MobclixCreative.isPlaying = true;
            if (this.type.equals("url") || this.type.equals(AdActivity.HTML_PARAM)) {
                loadUrl();
            } else if (this.type.equals("video")) {
                Intent mIntent = new Intent();
                String packageName = MobclixCreative.this.parentAdView.getContext().getPackageName();
                mIntent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "video").putExtra(String.valueOf(packageName) + ".data", this.rawJSON.toString());
                MobclixCreative.this.getContext().startActivity(mIntent);
            }
            MobclixCreative.isPlaying = false;
            return true;
        }

        /* access modifiers changed from: package-private */
        public void loadUrl() {
            try {
                if (!this.url.equals("")) {
                    Uri uri = Uri.parse(this.url);
                    String[] tmp = this.url.split("mobclix://");
                    if (tmp.length <= 1) {
                        tmp = this.url.split("mobclix%3A%2F%2F");
                        if (tmp.length <= 1) {
                            for (int i = 0; i < Mobclix.getInstance().getNativeUrls().size(); i++) {
                                if (uri.getHost().equals(Mobclix.getInstance().getNativeUrls().get(i))) {
                                    MobclixCreative.this.getContext().startActivity(new Intent("android.intent.action.VIEW", uri));
                                    return;
                                }
                            }
                            if (!this.cachedHTML.equals("")) {
                                Intent mIntent = new Intent();
                                String packageName = MobclixCreative.this.parentAdView.getContext().getPackageName();
                                try {
                                    this.rawJSON.put("cachedHTML", this.cachedHTML);
                                } catch (JSONException e) {
                                }
                                mIntent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "browser").putExtra(String.valueOf(packageName) + ".data", this.rawJSON.toString());
                                MobclixCreative.this.getContext().startActivity(mIntent);
                            } else if (this.browserType.equals("minimal")) {
                                Intent mIntent2 = new Intent();
                                String packageName2 = MobclixCreative.this.parentAdView.getContext().getPackageName();
                                mIntent2.setClassName(packageName2, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName2) + ".type", "browser").putExtra(String.valueOf(packageName2) + ".data", this.rawJSON.toString());
                                MobclixCreative.this.getContext().startActivity(mIntent2);
                                return;
                            } else {
                                MobclixCreative.this.getContext().startActivity(new Intent("android.intent.action.VIEW", uri));
                            }
                            CookieSyncManager.getInstance().sync();
                            return;
                        }
                    }
                    String customAdString = tmp[1];
                    MobclixCreative.this.customAdThread = new Thread(new CustomAdThread(this.url));
                    MobclixCreative.this.customAdThread.start();
                    Iterator<MobclixAdViewListener> it = MobclixCreative.this.parentAdView.listeners.iterator();
                    while (it.hasNext()) {
                        MobclixAdViewListener listener = it.next();
                        if (listener != null) {
                            listener.onCustomAdTouchThrough(MobclixCreative.this.parentAdView, customAdString);
                        }
                    }
                }
            } catch (Exception e2) {
            }
        }
    }

    private class Rotate3dAnimation extends Animation {
        private Camera mCamera;
        private final float mCenterX;
        private final float mCenterY;
        private final float mDepthZ;
        private final float mFromDegrees;
        private final boolean mReverse;
        private final float mToDegrees;

        public Rotate3dAnimation(float fromDegrees, float toDegrees, float centerX, float centerY, float depthZ, boolean reverse) {
            this.mFromDegrees = fromDegrees;
            this.mToDegrees = toDegrees;
            this.mCenterX = centerX;
            this.mCenterY = centerY;
            this.mDepthZ = depthZ;
            this.mReverse = reverse;
        }

        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
            this.mCamera = new Camera();
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float interpolatedTime, Transformation t) {
            float fromDegrees = this.mFromDegrees;
            float degrees = fromDegrees + ((this.mToDegrees - fromDegrees) * interpolatedTime);
            float centerX = this.mCenterX;
            float centerY = this.mCenterY;
            Camera camera = this.mCamera;
            Matrix matrix = t.getMatrix();
            camera.save();
            if (this.mReverse) {
                camera.translate(0.0f, 0.0f, this.mDepthZ * interpolatedTime);
            } else {
                camera.translate(0.0f, 0.0f, this.mDepthZ * (1.0f - interpolatedTime));
            }
            camera.rotateY(degrees);
            camera.getMatrix(matrix);
            camera.restore();
            matrix.preTranslate(-centerX, -centerY);
            matrix.postTranslate(centerX, centerY);
        }
    }
}
