package com.mobclix.android.sdk;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import com.mobclix.android.sdk.Mobclix;
import java.io.FileOutputStream;
import java.net.URLEncoder;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: MobclixConfig */
class FetchRemoteConfig extends AsyncTask<String, Integer, JSONArray> {
    private static String TAG = "MobclixConfig";
    private static boolean running = false;
    Mobclix c = Mobclix.getInstance();
    private MobclixInstrumentation instrumentation = MobclixInstrumentation.getInstance();
    private String url;

    FetchRemoteConfig() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:134:0x088d A[LOOP:1: B:23:0x0153->B:134:0x088d, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x0a7f  */
    /* JADX WARNING: Removed duplicated region for block: B:217:0x0167 A[EDGE_INSN: B:217:0x0167->B:25:0x0167 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.json.JSONArray doInBackground(java.lang.String... r41) {
        /*
            r40 = this;
            boolean r34 = com.mobclix.android.sdk.FetchRemoteConfig.running
            if (r34 == 0) goto L_0x0007
            r34 = 0
        L_0x0006:
            return r34
        L_0x0007:
            r34 = 1
            com.mobclix.android.sdk.FetchRemoteConfig.running = r34
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            if (r34 != 0) goto L_0x001d
            com.mobclix.android.sdk.MobclixInstrumentation r34 = com.mobclix.android.sdk.MobclixInstrumentation.getInstance()
            r0 = r34
            r1 = r40
            r1.instrumentation = r0
        L_0x001d:
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            java.lang.String r35 = com.mobclix.android.sdk.MobclixInstrumentation.STARTUP
            java.lang.String r22 = r34.startGroup(r35)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            java.lang.String r35 = "config"
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            java.lang.String r35 = "update_session"
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r34 = r0
            r34.updateSession()
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)
            r6 = 0
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            java.lang.String r35 = "update"
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            java.lang.String r35 = "parse_cache"
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            java.lang.String r35 = "load_misc_settings"
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)
            java.lang.String r34 = "deviceId"
            boolean r34 = com.mobclix.android.sdk.Mobclix.hasPref(r34)     // Catch:{ Exception -> 0x02ba }
            if (r34 == 0) goto L_0x0299
            java.lang.String r34 = "deviceId"
            java.lang.String r25 = com.mobclix.android.sdk.Mobclix.getPref(r34)     // Catch:{ Exception -> 0x02ba }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x02ba }
            r34 = r0
            r0 = r34
            java.lang.String r0 = r0.deviceId     // Catch:{ Exception -> 0x02ba }
            r34 = r0
            r0 = r25
            r1 = r34
            boolean r34 = r0.equals(r1)     // Catch:{ Exception -> 0x02ba }
            if (r34 != 0) goto L_0x00cb
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x02ba }
            r34 = r0
            r0 = r25
            r1 = r34
            r1.previousDeviceId = r0     // Catch:{ Exception -> 0x02ba }
        L_0x00cb:
            java.lang.String r34 = "idleTimeout"
            boolean r34 = com.mobclix.android.sdk.Mobclix.hasPref(r34)
            if (r34 == 0) goto L_0x00e9
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r34 = r0
            java.lang.String r35 = "idleTimeout"
            java.lang.String r35 = com.mobclix.android.sdk.Mobclix.getPref(r35)
            int r35 = java.lang.Integer.parseInt(r35)
            r0 = r35
            r1 = r34
            r1.idleTimeout = r0
        L_0x00e9:
            java.lang.String r34 = "pollTime"
            boolean r34 = com.mobclix.android.sdk.Mobclix.hasPref(r34)
            if (r34 == 0) goto L_0x0107
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r34 = r0
            java.lang.String r35 = "pollTime"
            java.lang.String r35 = com.mobclix.android.sdk.Mobclix.getPref(r35)
            int r35 = java.lang.Integer.parseInt(r35)
            r0 = r35
            r1 = r34
            r1.pollTime = r0
        L_0x0107:
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            java.lang.String r35 = "load_adunits"
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)
            java.lang.String[] r34 = com.mobclix.android.sdk.Mobclix.MC_AD_SIZES
            r0 = r34
            int r0 = r0.length
            r35 = r0
            r36 = 0
        L_0x0130:
            r0 = r36
            r1 = r35
            if (r0 < r1) goto L_0x02bd
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)
            r7 = 1
        L_0x0153:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r34 = r0
            r0 = r34
            int r0 = r0.remoteConfigSet
            r34 = r0
            r35 = 1
            r0 = r34
            r1 = r35
            if (r0 != r1) goto L_0x0471
        L_0x0167:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r34 = r0
            r0 = r34
            int r0 = r0.remoteConfigSet
            r34 = r0
            r35 = 1
            r0 = r34
            r1 = r35
            if (r0 == r1) goto L_0x0259
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            java.lang.String r35 = "parse_cache"
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            java.lang.String r35 = "load_urls"
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)
            java.lang.String r34 = "ConfigServer"
            boolean r34 = com.mobclix.android.sdk.Mobclix.hasPref(r34)
            if (r34 == 0) goto L_0x01b9
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r34 = r0
            java.lang.String r35 = "ConfigServer"
            java.lang.String r35 = com.mobclix.android.sdk.Mobclix.getPref(r35)
            r0 = r35
            r1 = r34
            r1.configServer = r0
        L_0x01b9:
            java.lang.String r34 = "AdServer"
            boolean r34 = com.mobclix.android.sdk.Mobclix.hasPref(r34)
            if (r34 == 0) goto L_0x01d3
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r34 = r0
            java.lang.String r35 = "AdServer"
            java.lang.String r35 = com.mobclix.android.sdk.Mobclix.getPref(r35)
            r0 = r35
            r1 = r34
            r1.adServer = r0
        L_0x01d3:
            java.lang.String r34 = "AnalyticsServer"
            boolean r34 = com.mobclix.android.sdk.Mobclix.hasPref(r34)
            if (r34 == 0) goto L_0x01ed
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r34 = r0
            java.lang.String r35 = "AnalyticsServer"
            java.lang.String r35 = com.mobclix.android.sdk.Mobclix.getPref(r35)
            r0 = r35
            r1 = r34
            r1.analyticsServer = r0
        L_0x01ed:
            java.lang.String r34 = "VcServer"
            boolean r34 = com.mobclix.android.sdk.Mobclix.hasPref(r34)
            if (r34 == 0) goto L_0x0207
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r34 = r0
            java.lang.String r35 = "VcServer"
            java.lang.String r35 = com.mobclix.android.sdk.Mobclix.getPref(r35)
            r0 = r35
            r1 = r34
            r1.vcServer = r0
        L_0x0207:
            java.lang.String r34 = "FeedbackServer"
            boolean r34 = com.mobclix.android.sdk.Mobclix.hasPref(r34)
            if (r34 == 0) goto L_0x0221
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r34 = r0
            java.lang.String r35 = "FeedbackServer"
            java.lang.String r35 = com.mobclix.android.sdk.Mobclix.getPref(r35)
            r0 = r35
            r1 = r34
            r1.feedbackServer = r0
        L_0x0221:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r34 = r0
            r35 = 1
            r0 = r35
            r1 = r34
            r1.remoteConfigSet = r0
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r34 = r0
            r35 = 1
            r0 = r35
            r1 = r34
            r1.isOfflineSession = r0
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)
        L_0x0259:
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            java.lang.String r35 = com.mobclix.android.sdk.MobclixInstrumentation.STARTUP
            r34.finishGroup(r35)
            com.mobclix.android.sdk.Mobclix.sync()
            r34 = 0
            com.mobclix.android.sdk.FetchRemoteConfig.running = r34
            r34 = r6
            goto L_0x0006
        L_0x0299:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x02ba }
            r34 = r0
            r35 = 1
            r0 = r35
            r1 = r34
            r1.isNewUser = r0     // Catch:{ Exception -> 0x02ba }
            java.lang.String r34 = "deviceId"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x02ba }
            r35 = r0
            r0 = r35
            java.lang.String r0 = r0.deviceId     // Catch:{ Exception -> 0x02ba }
            r35 = r0
            com.mobclix.android.sdk.Mobclix.addPref(r34, r35)     // Catch:{ Exception -> 0x02ba }
            goto L_0x00cb
        L_0x02ba:
            r34 = move-exception
            goto L_0x00cb
        L_0x02bd:
            r29 = r34[r36]
            boolean r37 = com.mobclix.android.sdk.Mobclix.hasPref(r29)
            if (r37 == 0) goto L_0x040a
            java.lang.String r10 = com.mobclix.android.sdk.Mobclix.getPref(r29)
            java.lang.String r37 = ","
            r0 = r10
            r1 = r37
            java.lang.String[] r13 = r0.split(r1)
            java.util.HashMap<java.lang.String, java.lang.Boolean> r37 = com.mobclix.android.sdk.Mobclix.enabled     // Catch:{ Exception -> 0x039b }
            r38 = 0
            r38 = r13[r38]     // Catch:{ Exception -> 0x039b }
            java.lang.String r39 = "true"
            boolean r38 = r38.equals(r39)     // Catch:{ Exception -> 0x039b }
            java.lang.Boolean r38 = java.lang.Boolean.valueOf(r38)     // Catch:{ Exception -> 0x039b }
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)     // Catch:{ Exception -> 0x039b }
        L_0x02eb:
            java.util.HashMap<java.lang.String, java.lang.Long> r37 = com.mobclix.android.sdk.Mobclix.refreshTime     // Catch:{ Exception -> 0x03b1 }
            r38 = 1
            r38 = r13[r38]     // Catch:{ Exception -> 0x03b1 }
            long r38 = java.lang.Long.parseLong(r38)     // Catch:{ Exception -> 0x03b1 }
            java.lang.Long r38 = java.lang.Long.valueOf(r38)     // Catch:{ Exception -> 0x03b1 }
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)     // Catch:{ Exception -> 0x03b1 }
        L_0x0302:
            java.util.HashMap<java.lang.String, java.lang.Boolean> r37 = com.mobclix.android.sdk.Mobclix.autoplay     // Catch:{ Exception -> 0x03c7 }
            r38 = 2
            r38 = r13[r38]     // Catch:{ Exception -> 0x03c7 }
            java.lang.String r39 = "true"
            boolean r38 = r38.equals(r39)     // Catch:{ Exception -> 0x03c7 }
            java.lang.Boolean r38 = java.lang.Boolean.valueOf(r38)     // Catch:{ Exception -> 0x03c7 }
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)     // Catch:{ Exception -> 0x03c7 }
        L_0x031b:
            java.util.HashMap<java.lang.String, java.lang.Long> r37 = com.mobclix.android.sdk.Mobclix.autoplayInterval     // Catch:{ Exception -> 0x03dd }
            r38 = 3
            r38 = r13[r38]     // Catch:{ Exception -> 0x03dd }
            long r38 = java.lang.Long.parseLong(r38)     // Catch:{ Exception -> 0x03dd }
            java.lang.Long r38 = java.lang.Long.valueOf(r38)     // Catch:{ Exception -> 0x03dd }
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)     // Catch:{ Exception -> 0x03dd }
        L_0x0332:
            java.util.HashMap<java.lang.String, java.lang.Boolean> r37 = com.mobclix.android.sdk.Mobclix.rmRequireUser     // Catch:{ Exception -> 0x03f4 }
            r38 = 4
            r38 = r13[r38]     // Catch:{ Exception -> 0x03f4 }
            java.lang.String r39 = "true"
            boolean r38 = r38.equals(r39)     // Catch:{ Exception -> 0x03f4 }
            java.lang.Boolean r38 = java.lang.Boolean.valueOf(r38)     // Catch:{ Exception -> 0x03f4 }
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)     // Catch:{ Exception -> 0x03f4 }
        L_0x034b:
            java.util.HashMap<java.lang.String, java.lang.Boolean> r37 = com.mobclix.android.sdk.Mobclix.customAdSet
            r38 = 0
            java.lang.Boolean r38 = java.lang.Boolean.valueOf(r38)
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)
            java.lang.StringBuilder r37 = new java.lang.StringBuilder
            java.lang.String r38 = java.lang.String.valueOf(r29)
            r37.<init>(r38)
            java.lang.String r38 = "CustomAdUrl"
            java.lang.StringBuilder r37 = r37.append(r38)
            java.lang.String r37 = r37.toString()
            boolean r37 = com.mobclix.android.sdk.Mobclix.hasPref(r37)
            if (r37 == 0) goto L_0x0462
            java.util.HashMap<java.lang.String, java.lang.String> r37 = com.mobclix.android.sdk.Mobclix.customAdUrl
            java.lang.StringBuilder r38 = new java.lang.StringBuilder
            java.lang.String r39 = java.lang.String.valueOf(r29)
            r38.<init>(r39)
            java.lang.String r39 = "CustomAdUrl"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r38 = r38.toString()
            java.lang.String r38 = com.mobclix.android.sdk.Mobclix.getPref(r38)
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)
        L_0x0397:
            int r36 = r36 + 1
            goto L_0x0130
        L_0x039b:
            r37 = move-exception
            r16 = r37
            java.util.HashMap<java.lang.String, java.lang.Boolean> r37 = com.mobclix.android.sdk.Mobclix.enabled
            r38 = 1
            java.lang.Boolean r38 = java.lang.Boolean.valueOf(r38)
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)
            goto L_0x02eb
        L_0x03b1:
            r37 = move-exception
            r16 = r37
            java.util.HashMap<java.lang.String, java.lang.Long> r37 = com.mobclix.android.sdk.Mobclix.refreshTime
            r38 = 30000(0x7530, double:1.4822E-319)
            java.lang.Long r38 = java.lang.Long.valueOf(r38)
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)
            goto L_0x0302
        L_0x03c7:
            r37 = move-exception
            r16 = r37
            java.util.HashMap<java.lang.String, java.lang.Boolean> r37 = com.mobclix.android.sdk.Mobclix.autoplay
            r38 = 0
            java.lang.Boolean r38 = java.lang.Boolean.valueOf(r38)
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)
            goto L_0x031b
        L_0x03dd:
            r37 = move-exception
            r16 = r37
            java.util.HashMap<java.lang.String, java.lang.Long> r37 = com.mobclix.android.sdk.Mobclix.autoplayInterval
            r38 = 120000(0x1d4c0, double:5.9288E-319)
            java.lang.Long r38 = java.lang.Long.valueOf(r38)
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)
            goto L_0x0332
        L_0x03f4:
            r37 = move-exception
            r16 = r37
            java.util.HashMap<java.lang.String, java.lang.Boolean> r37 = com.mobclix.android.sdk.Mobclix.rmRequireUser
            r38 = 1
            java.lang.Boolean r38 = java.lang.Boolean.valueOf(r38)
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)
            goto L_0x034b
        L_0x040a:
            java.util.HashMap<java.lang.String, java.lang.Boolean> r37 = com.mobclix.android.sdk.Mobclix.enabled
            r38 = 1
            java.lang.Boolean r38 = java.lang.Boolean.valueOf(r38)
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)
            java.util.HashMap<java.lang.String, java.lang.Long> r37 = com.mobclix.android.sdk.Mobclix.refreshTime
            r38 = 30000(0x7530, double:1.4822E-319)
            java.lang.Long r38 = java.lang.Long.valueOf(r38)
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)
            java.util.HashMap<java.lang.String, java.lang.Boolean> r37 = com.mobclix.android.sdk.Mobclix.autoplay
            r38 = 0
            java.lang.Boolean r38 = java.lang.Boolean.valueOf(r38)
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)
            java.util.HashMap<java.lang.String, java.lang.Long> r37 = com.mobclix.android.sdk.Mobclix.autoplayInterval
            r38 = 120000(0x1d4c0, double:5.9288E-319)
            java.lang.Long r38 = java.lang.Long.valueOf(r38)
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)
            java.util.HashMap<java.lang.String, java.lang.Boolean> r37 = com.mobclix.android.sdk.Mobclix.rmRequireUser
            r38 = 1
            java.lang.Boolean r38 = java.lang.Boolean.valueOf(r38)
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)
            goto L_0x034b
        L_0x0462:
            java.util.HashMap<java.lang.String, java.lang.String> r37 = com.mobclix.android.sdk.Mobclix.customAdUrl
            java.lang.String r38 = ""
            r0 = r37
            r1 = r29
            r2 = r38
            r0.put(r1, r2)
            goto L_0x0397
        L_0x0471:
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            java.lang.StringBuilder r35 = new java.lang.StringBuilder
            java.lang.String r36 = "attempt_"
            r35.<init>(r36)
            r0 = r35
            r1 = r7
            java.lang.StringBuilder r35 = r0.append(r1)
            java.lang.String r35 = r35.toString()
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            java.lang.String r35 = "build_request"
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)
            r34 = 1
            r0 = r7
            r1 = r34
            if (r0 != r1) goto L_0x0891
            r34 = 1
        L_0x04ae:
            r0 = r40
            r1 = r34
            java.lang.String r34 = r0.getConfigUrl(r1)
            r0 = r34
            r1 = r40
            r1.url = r0
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            java.lang.String r35 = "send_request"
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)
            java.lang.String r27 = ""
            r11 = 0
            r9 = 0
            com.mobclix.android.sdk.Mobclix$MobclixHttpClient r18 = new com.mobclix.android.sdk.Mobclix$MobclixHttpClient     // Catch:{ Exception -> 0x0b8a, all -> 0x0b86 }
            r0 = r40
            java.lang.String r0 = r0.url     // Catch:{ Exception -> 0x0b8a, all -> 0x0b86 }
            r34 = r0
            r0 = r18
            r1 = r34
            r0.<init>(r1)     // Catch:{ Exception -> 0x0b8a, all -> 0x0b86 }
            org.apache.http.HttpResponse r20 = r18.execute()     // Catch:{ Exception -> 0x0b8a, all -> 0x0b86 }
            org.apache.http.HttpEntity r19 = r20.getEntity()     // Catch:{ Exception -> 0x0b8a, all -> 0x0b86 }
            org.apache.http.StatusLine r34 = r20.getStatusLine()     // Catch:{ Exception -> 0x0b8a, all -> 0x0b86 }
            int r28 = r34.getStatusCode()     // Catch:{ Exception -> 0x0b8a, all -> 0x0b86 }
            r34 = 200(0xc8, float:2.8E-43)
            r0 = r28
            r1 = r34
            if (r0 != r1) goto L_0x0b42
            java.io.BufferedReader r8 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0b8a, all -> 0x0b86 }
            java.io.InputStreamReader r34 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0b8a, all -> 0x0b86 }
            java.io.InputStream r35 = r19.getContent()     // Catch:{ Exception -> 0x0b8a, all -> 0x0b86 }
            r34.<init>(r35)     // Catch:{ Exception -> 0x0b8a, all -> 0x0b86 }
            r35 = 8000(0x1f40, float:1.121E-41)
            r0 = r8
            r1 = r34
            r2 = r35
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0b8a, all -> 0x0b86 }
            java.lang.String r30 = r8.readLine()     // Catch:{ Exception -> 0x0a40 }
        L_0x0520:
            if (r30 != 0) goto L_0x0895
            r19.consumeContent()     // Catch:{ Exception -> 0x0a40 }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x0a40 }
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)     // Catch:{ Exception -> 0x0a40 }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x0a40 }
            r34 = r0
            java.lang.String r35 = "handle_response"
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)     // Catch:{ Exception -> 0x0a40 }
            java.lang.String r34 = ""
            r0 = r27
            r1 = r34
            boolean r34 = r0.equals(r1)     // Catch:{ Exception -> 0x0a40 }
            if (r34 != 0) goto L_0x0862
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            java.lang.String r35 = "decode_json"
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            org.json.JSONObject r12 = new org.json.JSONObject     // Catch:{ Exception -> 0x0a2d }
            r0 = r12
            r1 = r27
            r0.<init>(r1)     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            java.lang.String r35 = "save_json"
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            java.lang.String r35 = "raw_config_json"
            java.lang.String r36 = com.mobclix.android.sdk.MobclixInstrumentation.STARTUP     // Catch:{ Exception -> 0x0a2d }
            r0 = r34
            r1 = r27
            r2 = r35
            r3 = r36
            r0.addInfo(r1, r2, r3)     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            java.lang.String r35 = "decoded_config_json"
            java.lang.String r36 = com.mobclix.android.sdk.MobclixInstrumentation.STARTUP     // Catch:{ Exception -> 0x0a2d }
            r0 = r34
            r1 = r12
            r2 = r35
            r3 = r36
            r0.addInfo(r1, r2, r3)     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            java.lang.String r35 = "load_config"
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "urls"
            r0 = r12
            r1 = r34
            org.json.JSONObject r31 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            java.lang.String r35 = "config"
            r0 = r31
            r1 = r35
            java.lang.String r35 = r0.getString(r1)     // Catch:{ Exception -> 0x0a2d }
            r0 = r35
            r1 = r34
            r1.configServer = r0     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            java.lang.String r35 = "ads"
            r0 = r31
            r1 = r35
            java.lang.String r35 = r0.getString(r1)     // Catch:{ Exception -> 0x0a2d }
            r0 = r35
            r1 = r34
            r1.adServer = r0     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            java.lang.String r35 = "analytics"
            r0 = r31
            r1 = r35
            java.lang.String r35 = r0.getString(r1)     // Catch:{ Exception -> 0x0a2d }
            r0 = r35
            r1 = r34
            r1.analyticsServer = r0     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            java.lang.String r35 = "vc"
            r0 = r31
            r1 = r35
            java.lang.String r35 = r0.getString(r1)     // Catch:{ Exception -> 0x0a2d }
            r0 = r35
            r1 = r34
            r1.vcServer = r0     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            java.lang.String r35 = "feedback"
            r0 = r31
            r1 = r35
            java.lang.String r35 = r0.getString(r1)     // Catch:{ Exception -> 0x0a2d }
            r0 = r35
            r1 = r34
            r1.feedbackServer = r0     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            java.lang.String r35 = "debug"
            r0 = r31
            r1 = r35
            java.lang.String r35 = r0.getString(r1)     // Catch:{ Exception -> 0x0a2d }
            r0 = r35
            r1 = r34
            r1.debugServer = r0     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            java.lang.String r35 = "idle_timeout"
            r0 = r12
            r1 = r35
            int r35 = r0.getInt(r1)     // Catch:{ Exception -> 0x0a2d }
            r0 = r35
            int r0 = r0 * 1000
            r35 = r0
            r0 = r35
            r1 = r34
            r1.idleTimeout = r0     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0b96 }
            r34 = r0
            java.lang.String r35 = "poll_time"
            r0 = r12
            r1 = r35
            int r35 = r0.getInt(r1)     // Catch:{ Exception -> 0x0b96 }
            r0 = r35
            int r0 = r0 * 1000
            r35 = r0
            r0 = r35
            r1 = r34
            r1.pollTime = r0     // Catch:{ Exception -> 0x0b96 }
        L_0x0697:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            r0 = r35
            int r0 = r0.pollTime     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r36 = r0
            r0 = r36
            int r0 = r0.idleTimeout     // Catch:{ Exception -> 0x0a2d }
            r36 = r0
            int r35 = java.lang.Math.min(r35, r36)     // Catch:{ Exception -> 0x0a2d }
            r0 = r35
            r1 = r34
            r1.pollTime = r0     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            java.lang.String r35 = "set_default_values"
            r0 = r34
            r1 = r22
            r2 = r35
            java.lang.String r22 = r0.benchmarkStart(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            java.util.HashMap r26 = new java.util.HashMap     // Catch:{ Exception -> 0x0a2d }
            r26.<init>()     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "ConfigServer"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            r0 = r35
            java.lang.String r0 = r0.configServer     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            r0 = r26
            r1 = r34
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "AdServer"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            r0 = r35
            java.lang.String r0 = r0.adServer     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            r0 = r26
            r1 = r34
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "AnalyticsServer"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            r0 = r35
            java.lang.String r0 = r0.analyticsServer     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            r0 = r26
            r1 = r34
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "VcServer"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            r0 = r35
            java.lang.String r0 = r0.vcServer     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            r0 = r26
            r1 = r34
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "FeedbackServer"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            r0 = r35
            java.lang.String r0 = r0.feedbackServer     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            r0 = r26
            r1 = r34
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "idleTimeout"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            r0 = r35
            int r0 = r0.idleTimeout     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            java.lang.String r35 = java.lang.Integer.toString(r35)     // Catch:{ Exception -> 0x0a2d }
            r0 = r26
            r1 = r34
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "pollTime"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            r0 = r35
            int r0 = r0.pollTime     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            java.lang.String r35 = java.lang.Integer.toString(r35)     // Catch:{ Exception -> 0x0a2d }
            r0 = r26
            r1 = r34
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "ad_units"
            r0 = r12
            r1 = r34
            org.json.JSONArray r5 = r0.getJSONArray(r1)     // Catch:{ Exception -> 0x0a2d }
            r21 = 0
        L_0x078a:
            int r34 = r5.length()     // Catch:{ Exception -> 0x0a2d }
            r0 = r21
            r1 = r34
            if (r0 < r1) goto L_0x08b0
            java.lang.String r34 = "debug_config"
            r0 = r12
            r1 = r34
            org.json.JSONObject r15 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0b24 }
            java.lang.String[] r34 = com.mobclix.android.sdk.MobclixInstrumentation.MC_DEBUG_CATS     // Catch:{ Exception -> 0x0b24 }
            r0 = r34
            int r0 = r0.length     // Catch:{ Exception -> 0x0b24 }
            r35 = r0
            r36 = 0
        L_0x07a6:
            r0 = r36
            r1 = r35
            if (r0 < r1) goto L_0x0ab7
            java.util.Iterator r14 = r15.keys()     // Catch:{ Exception -> 0x0b24 }
        L_0x07b0:
            boolean r34 = r14.hasNext()     // Catch:{ Exception -> 0x0b24 }
            if (r34 != 0) goto L_0x0ad3
        L_0x07b6:
            java.lang.String r34 = "app_alerts"
            r0 = r12
            r1 = r34
            org.json.JSONArray r6 = r0.getJSONArray(r1)     // Catch:{ Exception -> 0x0b93 }
        L_0x07bf:
            java.lang.String r34 = "native_urls"
            r0 = r12
            r1 = r34
            org.json.JSONArray r24 = r0.getJSONArray(r1)     // Catch:{ Exception -> 0x0b90 }
            r21 = 0
        L_0x07ca:
            int r34 = r24.length()     // Catch:{ Exception -> 0x0b90 }
            r0 = r21
            r1 = r34
            if (r0 < r1) goto L_0x0b27
        L_0x07d4:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            r0 = r34
            java.lang.String r0 = r0.previousDeviceId     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            if (r34 == 0) goto L_0x07f3
            java.lang.String r34 = "deviceId"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            r0 = r35
            java.lang.String r0 = r0.deviceId     // Catch:{ Exception -> 0x0a2d }
            r35 = r0
            com.mobclix.android.sdk.Mobclix.addPref(r34, r35)     // Catch:{ Exception -> 0x0a2d }
        L_0x07f3:
            java.lang.String r34 = "offlineSessions"
            java.lang.String r35 = "0"
            r0 = r26
            r1 = r34
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "totalSessionTime"
            java.lang.String r35 = "0"
            r0 = r26
            r1 = r34
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "totalIdleTime"
            java.lang.String r35 = "0"
            r0 = r26
            r1 = r34
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            com.mobclix.android.sdk.Mobclix.addPref(r26)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "MCReferralData"
            boolean r34 = com.mobclix.android.sdk.Mobclix.hasPref(r34)     // Catch:{ Exception -> 0x0a2d }
            if (r34 == 0) goto L_0x082a
            java.lang.String r34 = "MCReferralData"
            com.mobclix.android.sdk.Mobclix.removePref(r34)     // Catch:{ Exception -> 0x0a2d }
        L_0x082a:
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            r35 = 0
            r0 = r35
            r1 = r34
            r1.isOfflineSession = r0     // Catch:{ Exception -> 0x0a2d }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a2d }
            r34 = r0
            r35 = 1
            r0 = r35
            r1 = r34
            r1.remoteConfigSet = r0     // Catch:{ Exception -> 0x0a2d }
        L_0x0862:
            r8.close()     // Catch:{ Exception -> 0x0b75 }
        L_0x0865:
            if (r11 == 0) goto L_0x086a
            r11.disconnect()
        L_0x086a:
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r34 = r0
            r0 = r34
            r1 = r22
            java.lang.String r22 = r0.benchmarkFinishPath(r1)
            r34 = 1
            r0 = r7
            r1 = r34
            if (r0 > r1) goto L_0x0167
            int r7 = r7 + 1
            goto L_0x0153
        L_0x0891:
            r34 = 0
            goto L_0x04ae
        L_0x0895:
            java.lang.StringBuilder r34 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a40 }
            java.lang.String r35 = java.lang.String.valueOf(r27)     // Catch:{ Exception -> 0x0a40 }
            r34.<init>(r35)     // Catch:{ Exception -> 0x0a40 }
            r0 = r34
            r1 = r30
            java.lang.StringBuilder r34 = r0.append(r1)     // Catch:{ Exception -> 0x0a40 }
            java.lang.String r27 = r34.toString()     // Catch:{ Exception -> 0x0a40 }
            java.lang.String r30 = r8.readLine()     // Catch:{ Exception -> 0x0a40 }
            goto L_0x0520
        L_0x08b0:
            r0 = r5
            r1 = r21
            org.json.JSONObject r4 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "size"
            r0 = r4
            r1 = r34
            java.lang.String r29 = r0.getString(r1)     // Catch:{ Exception -> 0x0a2d }
            java.util.HashMap<java.lang.String, java.lang.Boolean> r34 = com.mobclix.android.sdk.Mobclix.enabled     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = "enabled"
            r0 = r4
            r1 = r35
            boolean r35 = r0.getBoolean(r1)     // Catch:{ Exception -> 0x0a2d }
            java.lang.Boolean r35 = java.lang.Boolean.valueOf(r35)     // Catch:{ Exception -> 0x0a2d }
            r0 = r34
            r1 = r29
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "refresh"
            r0 = r4
            r1 = r34
            long r34 = r0.getLong(r1)     // Catch:{ Exception -> 0x0a2d }
            r36 = -1
            int r34 = (r34 > r36 ? 1 : (r34 == r36 ? 0 : -1))
            if (r34 != 0) goto L_0x0a0f
            java.util.HashMap<java.lang.String, java.lang.Long> r34 = com.mobclix.android.sdk.Mobclix.refreshTime     // Catch:{ Exception -> 0x0a2d }
            r35 = -1
            java.lang.Long r35 = java.lang.Long.valueOf(r35)     // Catch:{ Exception -> 0x0a2d }
            r0 = r34
            r1 = r29
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
        L_0x08f8:
            java.util.HashMap<java.lang.String, java.lang.Boolean> r34 = com.mobclix.android.sdk.Mobclix.autoplay     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = "autoplay"
            r0 = r4
            r1 = r35
            boolean r35 = r0.getBoolean(r1)     // Catch:{ Exception -> 0x0a2d }
            java.lang.Boolean r35 = java.lang.Boolean.valueOf(r35)     // Catch:{ Exception -> 0x0a2d }
            r0 = r34
            r1 = r29
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "autoplay_interval"
            r0 = r4
            r1 = r34
            long r34 = r0.getLong(r1)     // Catch:{ Exception -> 0x0a2d }
            r36 = -1
            int r34 = (r34 > r36 ? 1 : (r34 == r36 ? 0 : -1))
            if (r34 != 0) goto L_0x0a5b
            java.util.HashMap<java.lang.String, java.lang.Long> r34 = com.mobclix.android.sdk.Mobclix.autoplayInterval     // Catch:{ Exception -> 0x0a2d }
            r35 = -1
            java.lang.Long r35 = java.lang.Long.valueOf(r35)     // Catch:{ Exception -> 0x0a2d }
            r0 = r34
            r1 = r29
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
        L_0x0930:
            java.util.HashMap<java.lang.String, java.lang.Boolean> r34 = com.mobclix.android.sdk.Mobclix.rmRequireUser     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = "autoplay"
            r0 = r4
            r1 = r35
            boolean r35 = r0.getBoolean(r1)     // Catch:{ Exception -> 0x0a2d }
            java.lang.Boolean r35 = java.lang.Boolean.valueOf(r35)     // Catch:{ Exception -> 0x0a2d }
            r0 = r34
            r1 = r29
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            java.lang.StringBuilder r34 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a2d }
            java.util.HashMap<java.lang.String, java.lang.Boolean> r35 = com.mobclix.android.sdk.Mobclix.enabled     // Catch:{ Exception -> 0x0a2d }
            r0 = r35
            r1 = r29
            java.lang.Object r41 = r0.get(r1)     // Catch:{ Exception -> 0x0a2d }
            java.lang.Boolean r41 = (java.lang.Boolean) r41     // Catch:{ Exception -> 0x0a2d }
            boolean r35 = r41.booleanValue()     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = java.lang.Boolean.toString(r35)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = java.lang.String.valueOf(r35)     // Catch:{ Exception -> 0x0a2d }
            r34.<init>(r35)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = ","
            java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x0a2d }
            java.util.HashMap<java.lang.String, java.lang.Long> r35 = com.mobclix.android.sdk.Mobclix.refreshTime     // Catch:{ Exception -> 0x0a2d }
            r0 = r35
            r1 = r29
            java.lang.Object r41 = r0.get(r1)     // Catch:{ Exception -> 0x0a2d }
            java.lang.Long r41 = (java.lang.Long) r41     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = r41.toString()     // Catch:{ Exception -> 0x0a2d }
            java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = ","
            java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x0a2d }
            java.util.HashMap<java.lang.String, java.lang.Boolean> r35 = com.mobclix.android.sdk.Mobclix.autoplay     // Catch:{ Exception -> 0x0a2d }
            r0 = r35
            r1 = r29
            java.lang.Object r41 = r0.get(r1)     // Catch:{ Exception -> 0x0a2d }
            java.lang.Boolean r41 = (java.lang.Boolean) r41     // Catch:{ Exception -> 0x0a2d }
            boolean r35 = r41.booleanValue()     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = java.lang.Boolean.toString(r35)     // Catch:{ Exception -> 0x0a2d }
            java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = ","
            java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x0a2d }
            java.util.HashMap<java.lang.String, java.lang.Long> r35 = com.mobclix.android.sdk.Mobclix.autoplayInterval     // Catch:{ Exception -> 0x0a2d }
            r0 = r35
            r1 = r29
            java.lang.Object r41 = r0.get(r1)     // Catch:{ Exception -> 0x0a2d }
            java.lang.Long r41 = (java.lang.Long) r41     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = r41.toString()     // Catch:{ Exception -> 0x0a2d }
            java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = ","
            java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x0a2d }
            java.util.HashMap<java.lang.String, java.lang.Boolean> r35 = com.mobclix.android.sdk.Mobclix.rmRequireUser     // Catch:{ Exception -> 0x0a2d }
            r0 = r35
            r1 = r29
            java.lang.Object r41 = r0.get(r1)     // Catch:{ Exception -> 0x0a2d }
            java.lang.Boolean r41 = (java.lang.Boolean) r41     // Catch:{ Exception -> 0x0a2d }
            boolean r35 = r41.booleanValue()     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = java.lang.Boolean.toString(r35)     // Catch:{ Exception -> 0x0a2d }
            java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = r34.toString()     // Catch:{ Exception -> 0x0a2d }
            r0 = r26
            r1 = r29
            r2 = r34
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r34 = "customAdUrl"
            r0 = r4
            r1 = r34
            java.lang.String r10 = r0.getString(r1)     // Catch:{ Exception -> 0x0aa5 }
            java.util.HashMap<java.lang.String, java.lang.String> r34 = com.mobclix.android.sdk.Mobclix.customAdUrl     // Catch:{ Exception -> 0x0aa5 }
            r0 = r34
            r1 = r29
            java.lang.Object r34 = r0.get(r1)     // Catch:{ Exception -> 0x0aa5 }
            r0 = r10
            r1 = r34
            boolean r34 = r0.equals(r1)     // Catch:{ Exception -> 0x0aa5 }
            if (r34 == 0) goto L_0x0a83
            java.util.HashMap<java.lang.String, java.lang.String> r34 = com.mobclix.android.sdk.Mobclix.customAdUrl     // Catch:{ Exception -> 0x0aa5 }
            java.lang.String r35 = ""
            r0 = r34
            r1 = r29
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0aa5 }
        L_0x0a0b:
            int r21 = r21 + 1
            goto L_0x078a
        L_0x0a0f:
            java.util.HashMap<java.lang.String, java.lang.Long> r34 = com.mobclix.android.sdk.Mobclix.refreshTime     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = "refresh"
            r0 = r4
            r1 = r35
            long r35 = r0.getLong(r1)     // Catch:{ Exception -> 0x0a2d }
            r37 = 1000(0x3e8, double:4.94E-321)
            long r35 = r35 * r37
            java.lang.Long r35 = java.lang.Long.valueOf(r35)     // Catch:{ Exception -> 0x0a2d }
            r0 = r34
            r1 = r29
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            goto L_0x08f8
        L_0x0a2d:
            r34 = move-exception
            r16 = r34
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0a40 }
            r34 = r0
            r35 = -1
            r0 = r35
            r1 = r34
            r1.remoteConfigSet = r0     // Catch:{ Exception -> 0x0a40 }
            goto L_0x0862
        L_0x0a40:
            r34 = move-exception
            r16 = r34
        L_0x0a43:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ all -> 0x0a79 }
            r34 = r0
            r35 = -1
            r0 = r35
            r1 = r34
            r1.remoteConfigSet = r0     // Catch:{ all -> 0x0a79 }
            r8.close()     // Catch:{ Exception -> 0x0b53 }
        L_0x0a54:
            if (r11 == 0) goto L_0x086a
            r11.disconnect()
            goto L_0x086a
        L_0x0a5b:
            java.util.HashMap<java.lang.String, java.lang.Long> r34 = com.mobclix.android.sdk.Mobclix.autoplayInterval     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = "autoplay_interval"
            r0 = r4
            r1 = r35
            long r35 = r0.getLong(r1)     // Catch:{ Exception -> 0x0a2d }
            r37 = 1000(0x3e8, double:4.94E-321)
            long r35 = r35 * r37
            java.lang.Long r35 = java.lang.Long.valueOf(r35)     // Catch:{ Exception -> 0x0a2d }
            r0 = r34
            r1 = r29
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            goto L_0x0930
        L_0x0a79:
            r34 = move-exception
        L_0x0a7a:
            r8.close()     // Catch:{ Exception -> 0x0b64 }
        L_0x0a7d:
            if (r11 == 0) goto L_0x0a82
            r11.disconnect()
        L_0x0a82:
            throw r34
        L_0x0a83:
            java.lang.StringBuilder r34 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0aa5 }
            java.lang.String r35 = java.lang.String.valueOf(r29)     // Catch:{ Exception -> 0x0aa5 }
            r34.<init>(r35)     // Catch:{ Exception -> 0x0aa5 }
            java.lang.String r35 = "CustomAdUrl"
            java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x0aa5 }
            java.lang.String r34 = r34.toString()     // Catch:{ Exception -> 0x0aa5 }
            com.mobclix.android.sdk.Mobclix.removePref(r34)     // Catch:{ Exception -> 0x0aa5 }
            java.util.HashMap<java.lang.String, java.lang.String> r34 = com.mobclix.android.sdk.Mobclix.customAdUrl     // Catch:{ Exception -> 0x0aa5 }
            r0 = r34
            r1 = r29
            r2 = r10
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0aa5 }
            goto L_0x0a0b
        L_0x0aa5:
            r34 = move-exception
            r16 = r34
            java.util.HashMap<java.lang.String, java.lang.String> r34 = com.mobclix.android.sdk.Mobclix.customAdUrl     // Catch:{ Exception -> 0x0a2d }
            java.lang.String r35 = ""
            r0 = r34
            r1 = r29
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0a2d }
            goto L_0x0a0b
        L_0x0ab7:
            r32 = r34[r36]     // Catch:{ Exception -> 0x0b24 }
            java.lang.StringBuilder r37 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b24 }
            java.lang.String r38 = "debug_"
            r37.<init>(r38)     // Catch:{ Exception -> 0x0b24 }
            r0 = r37
            r1 = r32
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ Exception -> 0x0b24 }
            java.lang.String r37 = r37.toString()     // Catch:{ Exception -> 0x0b24 }
            com.mobclix.android.sdk.Mobclix.removePref(r37)     // Catch:{ Exception -> 0x0b24 }
            int r36 = r36 + 1
            goto L_0x07a6
        L_0x0ad3:
            java.lang.Object r23 = r14.next()     // Catch:{ Exception -> 0x0b07 }
            java.lang.String r23 = (java.lang.String) r23     // Catch:{ Exception -> 0x0b07 }
            r0 = r15
            r1 = r23
            java.lang.String r33 = r0.getString(r1)     // Catch:{ Exception -> 0x0b07 }
            java.util.HashMap<java.lang.String, java.lang.String> r34 = com.mobclix.android.sdk.Mobclix.debugConfig     // Catch:{ Exception -> 0x0b07 }
            r0 = r34
            r1 = r23
            r2 = r33
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0b07 }
            java.lang.StringBuilder r34 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b07 }
            java.lang.String r35 = "debug_"
            r34.<init>(r35)     // Catch:{ Exception -> 0x0b07 }
            r0 = r34
            r1 = r23
            java.lang.StringBuilder r34 = r0.append(r1)     // Catch:{ Exception -> 0x0b07 }
            java.lang.String r34 = r34.toString()     // Catch:{ Exception -> 0x0b07 }
            r0 = r34
            r1 = r33
            com.mobclix.android.sdk.Mobclix.addPref(r0, r1)     // Catch:{ Exception -> 0x0b07 }
            goto L_0x07b0
        L_0x0b07:
            r34 = move-exception
            r17 = r34
            java.lang.String r34 = com.mobclix.android.sdk.FetchRemoteConfig.TAG     // Catch:{ Exception -> 0x0b24 }
            java.lang.StringBuilder r35 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b24 }
            java.lang.String r36 = "ERROR: "
            r35.<init>(r36)     // Catch:{ Exception -> 0x0b24 }
            java.lang.String r36 = r17.toString()     // Catch:{ Exception -> 0x0b24 }
            java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x0b24 }
            java.lang.String r35 = r35.toString()     // Catch:{ Exception -> 0x0b24 }
            android.util.Log.v(r34, r35)     // Catch:{ Exception -> 0x0b24 }
            goto L_0x07b0
        L_0x0b24:
            r34 = move-exception
            goto L_0x07b6
        L_0x0b27:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0b90 }
            r34 = r0
            r0 = r34
            java.util.List<java.lang.String> r0 = r0.nativeUrls     // Catch:{ Exception -> 0x0b90 }
            r34 = r0
            r0 = r24
            r1 = r21
            java.lang.String r35 = r0.getString(r1)     // Catch:{ Exception -> 0x0b90 }
            r34.add(r35)     // Catch:{ Exception -> 0x0b90 }
            int r21 = r21 + 1
            goto L_0x07ca
        L_0x0b42:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0b8a, all -> 0x0b86 }
            r34 = r0
            r35 = -1
            r0 = r35
            r1 = r34
            r1.remoteConfigSet = r0     // Catch:{ Exception -> 0x0b8a, all -> 0x0b86 }
            r8 = r9
            goto L_0x0862
        L_0x0b53:
            r16 = move-exception
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r34 = r0
            r35 = -1
            r0 = r35
            r1 = r34
            r1.remoteConfigSet = r0
            goto L_0x0a54
        L_0x0b64:
            r16 = move-exception
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r35 = r0
            r36 = -1
            r0 = r36
            r1 = r35
            r1.remoteConfigSet = r0
            goto L_0x0a7d
        L_0x0b75:
            r16 = move-exception
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r34 = r0
            r35 = -1
            r0 = r35
            r1 = r34
            r1.remoteConfigSet = r0
            goto L_0x0865
        L_0x0b86:
            r34 = move-exception
            r8 = r9
            goto L_0x0a7a
        L_0x0b8a:
            r34 = move-exception
            r16 = r34
            r8 = r9
            goto L_0x0a43
        L_0x0b90:
            r34 = move-exception
            goto L_0x07d4
        L_0x0b93:
            r34 = move-exception
            goto L_0x07bf
        L_0x0b96:
            r34 = move-exception
            goto L_0x0697
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.FetchRemoteConfig.doInBackground(java.lang.String[]):org.json.JSONArray");
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(JSONArray appAlerts) {
        if (appAlerts != null) {
            for (int i = 0; i < appAlerts.length(); i++) {
                try {
                    JSONObject a = appAlerts.getJSONObject(i);
                    String id = a.getString("id");
                    if (id != null && !id.equals("")) {
                        int timesShown = 0;
                        long lastTimeShown = 0;
                        String appAlertPref = Mobclix.getPref("MCAppAlert" + id);
                        if (!appAlertPref.equals("")) {
                            String[] appAlertPrefs = appAlertPref.split(",");
                            try {
                                timesShown = Integer.parseInt(appAlertPrefs[0]);
                            } catch (Exception e) {
                            }
                            try {
                                lastTimeShown = Long.parseLong(appAlertPrefs[1]);
                            } catch (Exception e2) {
                            }
                        }
                        String title = a.getString("title");
                        if (title != null && !title.equals("") && !title.equals("null")) {
                            String message = null;
                            try {
                                message = a.getString("message");
                                if (message == null || message.equals("") || message.equals("null")) {
                                    message = null;
                                }
                            } catch (Exception e3) {
                            }
                            int maxDisplays = 0;
                            try {
                                maxDisplays = a.getInt("max_displays");
                            } catch (Exception e4) {
                            }
                            if (maxDisplays == 0 || timesShown < maxDisplays) {
                                long displayInterval = 0;
                                try {
                                    displayInterval = ((long) a.getInt("display_interval")) * 1000;
                                } catch (Exception e5) {
                                }
                                if (displayInterval != 0) {
                                    if (lastTimeShown + displayInterval >= System.currentTimeMillis()) {
                                    }
                                }
                                JSONArray versions = a.getJSONArray("target_versions");
                                boolean targeted = false;
                                for (int j = 0; j < versions.length(); j++) {
                                    try {
                                        String v = versions.getString(j).split("\\*")[0];
                                        if (this.c.getApplicationVersion().substring(0, v.length()).equals(v)) {
                                            targeted = true;
                                        }
                                    } catch (Exception e6) {
                                    }
                                }
                                if (targeted) {
                                    String actionButton = null;
                                    try {
                                        actionButton = a.getString("action_button");
                                        if (actionButton == null || actionButton.equals("") || actionButton.equals("null")) {
                                            actionButton = null;
                                        }
                                    } catch (Exception e7) {
                                    }
                                    String actionUrl = null;
                                    try {
                                        actionUrl = a.getString("action_url");
                                        if (actionUrl == null || actionUrl.equals("") || actionUrl.equals("null")) {
                                            actionUrl = null;
                                        }
                                    } catch (Exception e8) {
                                    }
                                    if (actionUrl == null || actionButton != null) {
                                        String dismissButton = null;
                                        try {
                                            dismissButton = a.getString("dismiss_button");
                                            if (dismissButton == null || dismissButton.equals("") || dismissButton.equals("null")) {
                                                dismissButton = null;
                                            }
                                        } catch (Exception e9) {
                                        }
                                        if (actionUrl != null || dismissButton != null) {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(this.c.getContext());
                                            builder.setTitle(title);
                                            builder.setCancelable(false);
                                            if (message != null) {
                                                builder.setMessage(message);
                                            }
                                            if (actionUrl != null) {
                                                builder.setPositiveButton(actionButton, new Mobclix.ObjectOnClickListener(actionUrl) {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        FetchRemoteConfig.this.c.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse((String) this.obj1)));
                                                    }
                                                });
                                            }
                                            if (dismissButton != null) {
                                                builder.setNegativeButton(dismissButton, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });
                                            }
                                            builder.create().show();
                                            Mobclix.addPref("MCAppAlert" + id, String.valueOf(Integer.toString(timesShown + 1)) + "," + Long.toString(System.currentTimeMillis()));
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e10) {
                }
            }
        }
    }

    private String getConfigUrl(boolean usePref) {
        String configServer = this.c.configServer;
        StringBuffer data = new StringBuffer();
        if (Mobclix.hasPref("ConfigServer") && usePref) {
            configServer = Mobclix.getPref("ConfigServer");
        }
        data.append(configServer);
        data.append("?p=android");
        data.append("&a=").append(URLEncoder.encode(this.c.getApplicationId(), "UTF-8"));
        data.append("&m=").append(URLEncoder.encode(this.c.getMobclixVersion()));
        data.append("&v=").append(URLEncoder.encode(this.c.getApplicationVersion(), "UTF-8"));
        data.append("&d=").append(URLEncoder.encode(this.c.getDeviceId(), "UTF-8"));
        data.append("&dm=").append(URLEncoder.encode(this.c.getDeviceModel(), "UTF-8"));
        data.append("&dv=").append(URLEncoder.encode(this.c.getAndroidVersion(), "UTF-8"));
        data.append("&hwdm=").append(URLEncoder.encode(this.c.getDeviceHardwareModel(), "UTF-8"));
        data.append("&g=").append(URLEncoder.encode(this.c.getConnectionType(), "UTF-8"));
        if (!this.c.getGPS().equals("null")) {
            data.append("&ll=").append(URLEncoder.encode(this.c.getGPS(), "UTF-8"));
        }
        if (Mobclix.hasPref("offlineSessions")) {
            try {
                data.append("&off=").append(Mobclix.getPref("offlineSessions"));
            } catch (Exception e) {
            }
        }
        if (Mobclix.hasPref("totalSessionTime")) {
            try {
                data.append("&st=").append(Mobclix.getPref("totalSessionTime"));
            } catch (Exception e2) {
            }
        }
        if (Mobclix.hasPref("totalIdleTime")) {
            try {
                data.append("&it=").append(Mobclix.getPref("totalIdleTime"));
            } catch (Exception e3) {
            }
        }
        try {
            if (this.c.previousDeviceId != null) {
                data.append("&pd=").append(URLEncoder.encode(this.c.previousDeviceId, "UTF-8"));
            }
            data.append("&mcc=").append(URLEncoder.encode(this.c.getMcc(), "UTF-8"));
            data.append("&mnc=").append(URLEncoder.encode(this.c.getMnc(), "UTF-8"));
            if (this.c.isNewUser) {
                data.append("&new=true");
            }
            try {
                if (Mobclix.hasPref("MCReferralData")) {
                    String referral = Mobclix.getPref("MCReferralData");
                    if (!referral.equals("")) {
                        data.append("&r=").append(referral);
                    }
                }
            } catch (Exception e4) {
            }
            return data.toString();
        } catch (Exception e5) {
            return "";
        }
    }

    private void downloadCustomImages() {
        for (String s : Mobclix.MC_AD_SIZES) {
            String customAdUrl = Mobclix.customAdUrl.get(s);
            if (!customAdUrl.equals("")) {
                try {
                    DefaultHttpClient httpClient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet(customAdUrl);
                    httpGet.setHeader("Cookie", Mobclix.getCookieStringFromCookieManager(customAdUrl));
                    HttpEntity httpEntity = httpClient.execute(httpGet).getEntity();
                    Mobclix.syncCookiesToCookieManager(httpClient.getCookieStore(), customAdUrl);
                    Bitmap bmImg = BitmapFactory.decodeStream(httpEntity.getContent());
                    FileOutputStream fos = this.c.getContext().openFileOutput(String.valueOf(s) + "_mc_cached_custom_ad.png", 0);
                    bmImg.compress(Bitmap.CompressFormat.PNG, 90, fos);
                    fos.close();
                    Mobclix.addPref(String.valueOf(s) + "CustomAdUrl", Mobclix.customAdUrl.get(s));
                    Mobclix.customAdSet.put(s, true);
                } catch (Exception e) {
                }
            } else {
                Mobclix.customAdSet.put(s, true);
            }
        }
    }
}
