package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.ArcShape;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Browser;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.VideoView;
import com.mobclix.android.sdk.Mobclix;
import com.mobclix.android.sdk.MobclixCreative;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MobclixBrowserActivity extends Activity {
    private static final int TYPE_ADD_CONTACT = 8;
    private static final int TYPE_BROWSER = 2;
    private static final int TYPE_CAMERA = 4;
    private static final int TYPE_EXPANDER = 3;
    private static final int TYPE_GALLERY = 5;
    private static final int TYPE_GALLERY_TO_SERVER = 6;
    private static final int TYPE_GET_CONTACT = 7;
    private static final int TYPE_OFFER = 1;
    private static final int TYPE_VIDEO = 0;
    private final int MENU_BOOKMARK = 0;
    private final int MENU_CLOSE = 2;
    private final int MENU_FORWARD = 1;
    private String TAG = "mobclix-browser";
    /* access modifiers changed from: private */
    public LinkedList<Thread> asyncRequestThreads = new LinkedList<>();
    private String data = "";
    boolean firstOpen = true;
    private Intent forwardingIntent = null;
    /* access modifiers changed from: private */
    public ResourceResponseHandler handler = new ResourceResponseHandler();
    Uri imageUri;
    FrameLayout mFrame;
    private float scale = 1.0f;
    ScreenReceiver screenReceiver;
    private int type;
    /* access modifiers changed from: private */
    public View view;

    /* access modifiers changed from: private */
    public int dp(int p) {
        return (int) (this.scale * ((float) p));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.scale = getResources().getDisplayMetrics().density;
        Bundle extras = getIntent().getExtras();
        this.data = extras.getString(String.valueOf(getPackageName()) + ".data");
        this.mFrame = new FrameLayout(this);
        this.mFrame.setLayoutParams(new TableLayout.LayoutParams(-1, -1));
        if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("video")) {
            this.type = 0;
            requestWindowFeature(1);
            setRequestedOrientation(0);
            this.view = new MobclixVideoView(this, this.data);
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("browser")) {
            this.type = 2;
            requestWindowFeature(2);
            this.view = new MobclixBrowser(this, this.data);
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("expander")) {
            this.type = 3;
            requestWindowFeature(1);
            getWindow().setFlags(1024, 1024);
            this.view = new MobclixExpander(this, this.data);
            IntentFilter filter = new IntentFilter("android.intent.action.SCREEN_ON");
            filter.addAction("android.intent.action.SCREEN_OFF");
            this.screenReceiver = new ScreenReceiver();
            registerReceiver(this.screenReceiver, filter);
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("camera")) {
            this.type = 4;
            ContentValues values = new ContentValues();
            values.put("title", "camera.jpg");
            values.put("description", "Image capture by camera");
            try {
                this.imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                this.forwardingIntent = new Intent("android.media.action.IMAGE_CAPTURE");
                this.forwardingIntent.putExtra("output", this.imageUri);
                this.forwardingIntent.putExtra("android.intent.extra.videoQuality", 0);
                return;
            } catch (Exception e) {
                return;
            }
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("gallery")) {
            this.type = TYPE_GALLERY;
            this.forwardingIntent = new Intent();
            this.forwardingIntent.setType("image/*");
            this.forwardingIntent.setAction("android.intent.action.GET_CONTENT");
            return;
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("sendToServer")) {
            this.type = TYPE_GALLERY_TO_SERVER;
            this.forwardingIntent = new Intent();
            this.forwardingIntent.setType("image/*");
            this.forwardingIntent.setAction("android.intent.action.GET_CONTENT");
            return;
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("contact")) {
            this.type = TYPE_GET_CONTACT;
            return;
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("addContact")) {
            this.type = 8;
            try {
                this.forwardingIntent = MobclixContacts.getInstance().getAddContactIntent(new JSONObject(extras.getString(String.valueOf(getPackageName()) + ".data")));
                return;
            } catch (Exception e2) {
                Mobclix.getInstance().cameraWebview.getJavascriptInterface().contactCanceled("Error getting contact.");
                finish();
                return;
            }
        }
        setContentView(this.mFrame);
        this.mFrame.addView(this.view);
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void onStart() {
        super.onStart();
        switch (this.type) {
            case 0:
                if (!((MobclixVideoView) this.view).loadComplete) {
                    runNextAsyncRequest();
                    ((MobclixVideoView) this.view).mProgressDialog = ProgressDialog.show(this, "", "Loading...", true, true);
                    ((MobclixVideoView) this.view).mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            MobclixBrowserActivity.this.finish();
                        }
                    });
                    return;
                }
                ((MobclixVideoView) this.view).mBackgroundImage.setVisibility(0);
                ((MobclixVideoView) this.view).mVideoView.start();
                return;
            default:
                return;
        }
    }

    public void onResume() {
        super.onResume();
        switch (this.type) {
            case 3:
                ((MobclixExpander) this.view).webview.getJavascriptInterface().resumeListeners();
                ((MobclixExpander) this.view).wasAdActivity = false;
                if (!this.firstOpen) {
                    ((MobclixExpander) this.view).webview.getJavascriptInterface().adDidReturnFromHidden();
                }
                this.firstOpen = false;
                return;
            case 4:
                if (Mobclix.getInstance().cameraWebview == null) {
                    finish();
                    return;
                } else if (this.forwardingIntent != null) {
                    startActivityForResult(this.forwardingIntent, this.type);
                    return;
                } else {
                    finish();
                    return;
                }
            case TYPE_GALLERY /*5*/:
                if (this.forwardingIntent != null) {
                    startActivityForResult(Intent.createChooser(this.forwardingIntent, "Select Picture"), this.type);
                    return;
                } else {
                    finish();
                    return;
                }
            case TYPE_GALLERY_TO_SERVER /*6*/:
                if (this.forwardingIntent != null) {
                    startActivityForResult(Intent.createChooser(this.forwardingIntent, "Select Picture"), this.type);
                    return;
                } else {
                    finish();
                    return;
                }
            case TYPE_GET_CONTACT /*7*/:
                startActivityForResult(MobclixContacts.getInstance().getPickContactIntent(), this.type);
                return;
            case 8:
                try {
                    startActivityForResult(this.forwardingIntent, this.type);
                    return;
                } catch (Exception e) {
                    Mobclix.getInstance().cameraWebview.getJavascriptInterface().contactCanceled("Error getting contact.");
                    finish();
                    return;
                }
            default:
                return;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data2) {
        Bitmap scaledPic;
        Bitmap scaledPic2;
        if (Mobclix.getInstance().cameraWebview == null) {
            finish();
        }
        if (requestCode == 4) {
            if (resultCode == -1) {
                try {
                    Bitmap pic = BitmapFactory.decodeFile(convertImageUriToFile(this.imageUri, this).getAbsolutePath());
                    int height = Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoHeight;
                    int width = Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoWidth;
                    if (height == 0 || width == 0) {
                        scaledPic2 = pic;
                    } else {
                        scaledPic2 = Bitmap.createScaledBitmap(pic, Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoWidth, Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoHeight, true);
                    }
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    scaledPic2.compress(Bitmap.CompressFormat.JPEG, 10, bos);
                    Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoTaken(Base64.encodeBytes(bos.toByteArray()));
                    bos.close();
                    System.gc();
                } catch (Exception e) {
                    Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoCanceled(e.toString());
                    Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoCanceled("Error processing photo.");
                }
            } else if (resultCode == 0) {
                Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoCanceled("User canceled.");
            }
        } else if (requestCode == TYPE_GALLERY) {
            if (resultCode == -1) {
                try {
                    Uri selectedImageUri = data2.getData();
                    String filemanagerstring = selectedImageUri.getPath();
                    String selectedImagePath = getPath(selectedImageUri);
                    if (selectedImagePath == null) {
                        selectedImagePath = filemanagerstring;
                    }
                    Uri parse = Uri.parse(selectedImagePath);
                    File in = new File(selectedImagePath);
                    Bitmap pic2 = BitmapFactory.decodeFile(in.getAbsolutePath());
                    int height2 = Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoHeight;
                    int width2 = Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoWidth;
                    if (height2 == 0 || width2 == 0) {
                        scaledPic = pic2;
                    } else {
                        scaledPic = Bitmap.createScaledBitmap(pic2, Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoWidth, Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoHeight, true);
                    }
                    ByteArrayOutputStream bos2 = new ByteArrayOutputStream();
                    scaledPic.compress(Bitmap.CompressFormat.JPEG, 10, bos2);
                    Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoTaken(Base64.encodeBytes(bos2.toByteArray()));
                    bos2.close();
                    System.gc();
                    in.delete();
                } catch (Exception e2) {
                    Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoCanceled(e2.toString());
                    Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoCanceled("Error processing photo.");
                }
            } else if (resultCode == 0) {
                Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoCanceled("User canceled.");
            }
        } else if (requestCode == TYPE_GALLERY_TO_SERVER) {
            if (resultCode == -1) {
                try {
                    Uri selectedImageUri2 = data2.getData();
                    String filemanagerstring2 = selectedImageUri2.getPath();
                    String selectedImagePath2 = getPath(selectedImageUri2);
                    if (selectedImagePath2 == null) {
                        selectedImagePath2 = filemanagerstring2;
                    }
                    Mobclix.getInstance().cameraWebview.getJavascriptInterface().sendImageToServer(selectedImagePath2);
                } catch (Exception e3) {
                    Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoCanceled(e3.toString());
                    Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoCanceled("Error processing photo.");
                }
            } else if (resultCode == 0) {
                Mobclix.getInstance().cameraWebview.getJavascriptInterface().photoCanceled("User canceled.");
            }
        } else if (requestCode == TYPE_GET_CONTACT) {
            if (resultCode == -1) {
                try {
                    Mobclix.getInstance().cameraWebview.getJavascriptInterface().contactPicked(data2.getData());
                } catch (Exception e4) {
                    Mobclix.getInstance().cameraWebview.getJavascriptInterface().contactCanceled("Error getting contact.");
                }
            } else if (resultCode == 0) {
                Mobclix.getInstance().cameraWebview.getJavascriptInterface().contactCanceled("User canceled.");
            }
        } else if (requestCode == 8) {
            if (resultCode == -1) {
                try {
                    Mobclix.getInstance().cameraWebview.getJavascriptInterface().contactAdded();
                } catch (Exception e5) {
                    Mobclix.getInstance().cameraWebview.getJavascriptInterface().contactCanceled("Error getting contact.");
                }
            } else if (resultCode == 0) {
                Mobclix.getInstance().cameraWebview.getJavascriptInterface().contactCanceled("User canceled.");
            }
        }
        Mobclix.getInstance().cameraWebview = null;
        finish();
    }

    public String getPath(Uri uri) {
        Cursor cursor = managedQuery(uri, new String[]{"_data"}, null, null, null);
        if (cursor == null) {
            return null;
        }
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static File convertImageUriToFile(Uri imageUri2, Activity activity) {
        Cursor cursor = null;
        try {
            cursor = activity.managedQuery(imageUri2, new String[]{"_data", "_id", "orientation"}, null, null, null);
            int file_ColumnIndex = cursor.getColumnIndexOrThrow("_data");
            int orientation_ColumnIndex = cursor.getColumnIndexOrThrow("orientation");
            if (cursor.moveToFirst()) {
                String string = cursor.getString(orientation_ColumnIndex);
                File file = new File(cursor.getString(file_ColumnIndex));
            }
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        if (this.imageUri != null) {
            outState.putParcelable("imageUri", this.imageUri);
        }
        super.onSaveInstanceState(outState);
    }

    public void onRestoreInstanceState(Bundle inState) {
        if (inState.containsKey("imageUri")) {
            this.imageUri = (Uri) inState.getParcelable("imageUri");
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public void onStop() {
        super.onStop();
        switch (this.type) {
            case 3:
                if (((MobclixExpander) this.view).wasAdActivity) {
                    ((MobclixExpander) this.view).webview.getJavascriptInterface().adWillBecomeHidden();
                    return;
                } else if (((MobclixExpander) this.view).open) {
                    ((MobclixExpander) this.view).exit(1);
                    ((MobclixExpander) this.view).webview.getJavascriptInterface().adWillBecomeHidden();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(this.screenReceiver);
        } catch (Exception e) {
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public void onPause() {
        super.onPause();
        switch (this.type) {
            case 0:
                ((MobclixVideoView) this.view).mProgressDialog.dismiss();
                return;
            case 1:
            case 2:
            default:
                return;
            case 3:
                ((MobclixExpander) this.view).webview.getJavascriptInterface().pauseListeners();
                return;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (this.type) {
            case 0:
                if (keyCode == 4) {
                    finish();
                }
                return super.onKeyDown(keyCode, event);
            case 1:
            default:
                return super.onKeyDown(keyCode, event);
            case 2:
                if (keyCode != 4 || !((MobclixBrowser) this.view).getWebView().canGoBack()) {
                    return super.onKeyDown(keyCode, event);
                }
                ((MobclixBrowser) this.view).getWebView().goBack();
                return true;
            case 3:
                if (keyCode != 4) {
                    return super.onKeyDown(keyCode, event);
                }
                if (((MobclixExpander) this.view).webview.canGoBack()) {
                    ((MobclixExpander) this.view).webview.loadAd();
                }
                ((MobclixExpander) this.view).exit(500);
                return true;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        switch (this.type) {
            case 2:
                super.onCreateOptionsMenu(menu);
                menu.add(0, 0, 0, "Bookmark").setIcon(17301555);
                menu.add(0, 1, 0, "Forward").setIcon(17301565);
                menu.add(0, 2, 0, "Close").setIcon(17301560);
                return true;
            default:
                return false;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (this.type) {
            case 2:
                switch (item.getItemId()) {
                    case 0:
                        Browser.saveBookmark(this, ((MobclixBrowser) this.view).getWebView().getTitle(), ((MobclixBrowser) this.view).getWebView().getUrl());
                        return true;
                    case 1:
                        if (((MobclixBrowser) this.view).getWebView().canGoForward()) {
                            ((MobclixBrowser) this.view).getWebView().goForward();
                        }
                        return true;
                    case 2:
                        finish();
                        return true;
                    default:
                        return super.onContextItemSelected(item);
                }
            default:
                return false;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public void runNextAsyncRequest() {
        if (!this.asyncRequestThreads.isEmpty()) {
            this.asyncRequestThreads.removeFirst().start();
            return;
        }
        switch (this.type) {
            case 0:
                ((MobclixVideoView) this.view).loadComplete = true;
                ((MobclixVideoView) this.view).mProgressDialog.dismiss();
                ((MobclixVideoView) this.view).createAdButtonBanner();
                ((MobclixVideoView) this.view).mVideoView.setVisibility(0);
                ((MobclixVideoView) this.view).mVideoView.start();
                return;
            default:
                return;
        }
    }

    class ResourceResponseHandler extends Handler {
        ResourceResponseHandler() {
        }

        public void handleMessage(Message msg) {
            MobclixBrowserActivity.this.runNextAsyncRequest();
        }
    }

    private class MobclixVideoView extends RelativeLayout implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
        private Activity activity;
        private LinearLayout adButtonBanner = null;
        private LinearLayout adButtons = null;
        private ArrayList<String> buttonImageUrls = new ArrayList<>();
        /* access modifiers changed from: private */
        public ArrayList<Bitmap> buttonImages = new ArrayList<>();
        private ArrayList<String> buttonUrls = new ArrayList<>();
        private String landingUrl = "";
        /* access modifiers changed from: private */
        public boolean loadComplete = false;
        /* access modifiers changed from: private */
        public ImageView mBackgroundImage;
        private MediaController mMediaController;
        /* access modifiers changed from: private */
        public ProgressDialog mProgressDialog = null;
        /* access modifiers changed from: private */
        public VideoView mVideoView;
        private String tagline = "";
        private String taglineImageUrl = "";
        /* access modifiers changed from: private */
        public ImageView taglineImageView = null;
        private ArrayList<String> trackingUrls = new ArrayList<>();
        /* access modifiers changed from: private */
        public boolean videoLoaded = false;
        private String videoUrl;

        MobclixVideoView(Activity a, String data) {
            super(a);
            this.activity = a;
            try {
                JSONObject responseObject = new JSONObject(data);
                try {
                    this.videoUrl = responseObject.getString("videoUrl");
                } catch (Exception e) {
                }
                try {
                    this.landingUrl = responseObject.getString("landingUrl");
                } catch (Exception e2) {
                }
                try {
                    this.tagline = responseObject.getString("tagline");
                } catch (Exception e3) {
                }
                try {
                    this.taglineImageUrl = responseObject.getString("taglineImageUrl");
                } catch (Exception e4) {
                }
                try {
                    JSONArray buttons = responseObject.getJSONArray("buttons");
                    for (int i = 0; i < buttons.length(); i++) {
                        this.buttonImageUrls.add(buttons.getJSONObject(i).getString("imageUrl"));
                        this.buttonUrls.add(buttons.getJSONObject(i).getString("url"));
                    }
                } catch (Exception e5) {
                }
                JSONArray tracking = responseObject.getJSONArray("trackingUrls");
                for (int i2 = 0; i2 < tracking.length(); i2++) {
                    this.trackingUrls.add(tracking.getString(i2));
                }
            } catch (Exception e6) {
            }
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            loadVideoAd();
        }

        public void onPrepared(MediaPlayer mp) {
            this.videoLoaded = true;
            this.mBackgroundImage.setVisibility(8);
            this.mProgressDialog.dismiss();
        }

        public boolean onError(MediaPlayer mp, int what, int extra) {
            this.mProgressDialog.dismiss();
            removeView(this.mVideoView);
            return true;
        }

        public void onCompletion(MediaPlayer mp) {
            this.mBackgroundImage.setVisibility(0);
        }

        public void loadVideoAd() {
            MobclixBrowserActivity.this.getWindow().setFlags(1024, 1024);
            RelativeLayout.LayoutParams paramsCenter = new RelativeLayout.LayoutParams(-1, -1);
            paramsCenter.addRule(13);
            this.mVideoView = new VideoView(this.activity);
            this.mVideoView.setId(1337);
            this.mVideoView.setLayoutParams(paramsCenter);
            this.mMediaController = new MediaController(this.activity);
            this.mMediaController.setAnchorView(this.mVideoView);
            this.mVideoView.setVideoURI(Uri.parse(this.videoUrl));
            this.mVideoView.setMediaController(this.mMediaController);
            this.mVideoView.setOnPreparedListener(this);
            this.mVideoView.setOnErrorListener(this);
            this.mVideoView.setOnCompletionListener(this);
            this.mVideoView.setVisibility(4);
            addView(this.mVideoView);
            this.mBackgroundImage = new ImageView(this.activity);
            this.mBackgroundImage.setLayoutParams(paramsCenter);
            this.mBackgroundImage.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (MobclixVideoView.this.videoLoaded) {
                        MobclixVideoView.this.mBackgroundImage.setVisibility(8);
                        if (!MobclixVideoView.this.mVideoView.isPlaying()) {
                            MobclixVideoView.this.mVideoView.start();
                        }
                    }
                }
            });
            addView(this.mBackgroundImage);
            if ((!this.taglineImageUrl.equals("null") && !this.taglineImageUrl.equals("")) || !this.tagline.equals("")) {
                LinearLayout taglineWrap = new LinearLayout(this.activity);
                RelativeLayout.LayoutParams taglineLayout = new RelativeLayout.LayoutParams(-2, -2);
                taglineLayout.addRule(11);
                taglineWrap.setLayoutParams(taglineLayout);
                taglineWrap.setBackgroundColor(Color.parseColor("#CC666666"));
                taglineWrap.setPadding(MobclixBrowserActivity.this.dp(4), MobclixBrowserActivity.this.dp(4), MobclixBrowserActivity.this.dp(4), MobclixBrowserActivity.this.dp(4));
                if (this.taglineImageUrl.equals("null") || this.taglineImageUrl.equals("")) {
                    TextView taglineView = new TextView(this.activity);
                    taglineView.setText(this.tagline);
                    taglineWrap.addView(taglineView);
                } else {
                    this.taglineImageView = new ImageView(this.activity);
                    taglineWrap.addView(this.taglineImageView);
                    loadTaglineImage();
                }
                addView(taglineWrap);
            }
            MobclixBrowserActivity.this.setContentView(this);
            Iterator<String> it = this.buttonImageUrls.iterator();
            while (it.hasNext()) {
                loadButtonImage(it.next());
            }
            if (!this.landingUrl.equals("")) {
                loadBackgroundImage();
            }
            Iterator<String> it2 = this.trackingUrls.iterator();
            while (it2.hasNext()) {
                loadTrackingImage(it2.next());
            }
        }

        public void createAdButtonBanner() {
            if (this.buttonImages.size() != 0) {
                this.adButtonBanner = new LinearLayout(this.activity);
                this.adButtonBanner.setOrientation(1);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
                layoutParams.addRule(12);
                this.adButtonBanner.setLayoutParams(layoutParams);
                this.adButtonBanner.setBackgroundColor(Color.parseColor("#CC666666"));
                ImageView divider = new ImageView(this.activity);
                divider.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                divider.setBackgroundResource(17301524);
                this.adButtonBanner.addView(divider);
                this.adButtons = new LinearLayout(this.activity);
                this.adButtons.setPadding(0, MobclixBrowserActivity.this.dp(4), 0, 0);
                for (int i = 0; i < this.buttonImages.size(); i++) {
                    ImageView b = new ImageView(this.activity);
                    Bitmap bmImg = this.buttonImages.get(i);
                    LinearLayout.LayoutParams buttonLayout = new LinearLayout.LayoutParams(MobclixBrowserActivity.this.dp(bmImg.getWidth()), MobclixBrowserActivity.this.dp(bmImg.getHeight()));
                    buttonLayout.weight = 1.0f;
                    b.setLayoutParams(buttonLayout);
                    b.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    b.setImageBitmap(bmImg);
                    b.setOnClickListener(new ButtonOnClickListener(this.activity, this.buttonUrls.get(i)));
                    this.adButtons.addView(b);
                }
                this.adButtonBanner.addView(this.adButtons);
                addView(this.adButtonBanner);
            }
        }

        public void loadBackgroundImage() {
            MobclixBrowserActivity.this.asyncRequestThreads.add(new Thread(new Mobclix.FetchImageThread(this.landingUrl, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        MobclixVideoView.this.mBackgroundImage.setImageBitmap(this.bmImg);
                    }
                    MobclixBrowserActivity.this.handler.sendEmptyMessage(0);
                }
            })));
        }

        public void loadTaglineImage() {
            MobclixBrowserActivity.this.asyncRequestThreads.add(new Thread(new Mobclix.FetchImageThread(this.taglineImageUrl, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        int rawWidth = this.bmImg.getWidth();
                        int rawHeight = this.bmImg.getHeight();
                        MobclixVideoView.this.taglineImageView.setLayoutParams(new LinearLayout.LayoutParams(MobclixBrowserActivity.this.dp(rawWidth), MobclixBrowserActivity.this.dp(rawHeight)));
                        MobclixVideoView.this.taglineImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        MobclixVideoView.this.taglineImageView.setImageBitmap(this.bmImg);
                    }
                    MobclixBrowserActivity.this.handler.sendEmptyMessage(0);
                }
            })));
        }

        public void loadButtonImage(String url) {
            MobclixBrowserActivity.this.asyncRequestThreads.add(new Thread(new Mobclix.FetchImageThread(url, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        MobclixVideoView.this.buttonImages.add(this.bmImg);
                    }
                    MobclixBrowserActivity.this.handler.sendEmptyMessage(0);
                }
            })));
        }

        public void loadTrackingImage(String url) {
            MobclixBrowserActivity.this.asyncRequestThreads.add(new Thread(new Mobclix.FetchImageThread(url, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    MobclixBrowserActivity.this.handler.sendEmptyMessage(0);
                }
            })));
        }

        class ButtonOnClickListener implements View.OnClickListener {
            private Context context;
            private String url;

            public ButtonOnClickListener(Context c, String u) {
                this.context = c;
                this.url = u;
            }

            public void onClick(View arg0) {
                this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.url)));
            }
        }
    }

    private class MobclixBrowser extends RelativeLayout {
        /* access modifiers changed from: private */
        public Activity activity;
        private String browserType = "standard";
        private String cachedHTML = "";
        private WebView mWebView;
        private String url;

        MobclixBrowser(Activity a, String data) {
            super(a);
            this.activity = a;
            try {
                JSONObject responseObject = new JSONObject(data);
                try {
                    this.url = responseObject.getString("url");
                } catch (Exception e) {
                    this.url = "http://www.mobclix.com";
                }
                try {
                    this.cachedHTML = responseObject.getString("cachedHTML");
                } catch (Exception e2) {
                    this.cachedHTML = "";
                }
                try {
                    this.browserType = responseObject.getString("browserType");
                } catch (Exception e3) {
                    Exception exc = e3;
                    this.browserType = "standard";
                }
            } catch (JSONException e4) {
                this.url = "http://www.mobclix.com";
            }
            this.mWebView = new WebView(a);
            MobclixBrowserActivity.this.setProgressBarVisibility(true);
            this.mWebView.getSettings().setJavaScriptEnabled(true);
            this.mWebView.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Uri uri = Uri.parse(url);
                    if (uri.getScheme().equals("http") || uri.getScheme().equals("https") || uri.getScheme() == null) {
                        view.loadUrl(url);
                        return true;
                    }
                    MobclixBrowserActivity.this.startActivity(new Intent("android.intent.action.VIEW", uri));
                    return true;
                }

                public void onPageFinished(WebView view, String url) {
                }
            });
            this.mWebView.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView view, int progress) {
                    MobclixBrowser.this.activity.setProgress(progress * 100);
                }
            });
            this.mWebView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            if (this.cachedHTML.equals("")) {
                this.mWebView.loadUrl(this.url);
            } else {
                this.mWebView.loadDataWithBaseURL(this.url, this.cachedHTML, "text/html", "utf-8", null);
            }
            addView(this.mWebView);
            if (this.browserType.equals("minimal")) {
                RelativeLayout.LayoutParams closeButtonLayoutParams = new RelativeLayout.LayoutParams(MobclixBrowserActivity.this.dp(30), MobclixBrowserActivity.this.dp(30));
                closeButtonLayoutParams.setMargins(MobclixBrowserActivity.this.dp(MobclixBrowserActivity.TYPE_GALLERY), MobclixBrowserActivity.this.dp(MobclixBrowserActivity.TYPE_GALLERY), 0, 0);
                ImageView closeButton = new ImageView(MobclixBrowserActivity.this);
                closeButton.setLayoutParams(closeButtonLayoutParams);
                closeButton.setImageResource(17301594);
                ShapeDrawable backgroundCircle = new ShapeDrawable(new ArcShape(0.0f, 360.0f));
                backgroundCircle.setPadding(MobclixBrowserActivity.this.dp(-7), MobclixBrowserActivity.this.dp(-7), MobclixBrowserActivity.this.dp(-7), MobclixBrowserActivity.this.dp(-7));
                closeButton.setBackgroundDrawable(backgroundCircle);
                closeButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        MobclixBrowserActivity.this.finish();
                    }
                });
                addView(closeButton);
            }
        }

        /* access modifiers changed from: private */
        public WebView getWebView() {
            return this.mWebView;
        }
    }

    class MobclixExpander extends LinearLayout implements Animation.AnimationListener {
        static final int DEFAULT_DURATION = 500;
        private Activity activity;
        View hSpacer;
        int iHeight = 0;
        int iLeftMargin = 0;
        int iTopMargin = 0;
        int iWidth = 0;
        boolean open = true;
        int screenHeight = 0;
        int screenWidth = 0;
        int statusBarHeight = 0;
        int titleBarHeight = 0;
        View vSpacer;
        boolean wasAdActivity = false;
        MobclixCreative.MobclixWebView webview;
        ViewGroup webviewContainer;
        int windowFlags;

        MobclixExpander(Activity a, String data) {
            super(a);
            this.activity = a;
            int duration = DEFAULT_DURATION;
            int fTopMargin = 0;
            int fLeftMargin = 0;
            int fWidth = 0;
            int fHeight = 0;
            try {
                JSONObject jSONObject = new JSONObject(data);
                try {
                    this.statusBarHeight = jSONObject.getInt("statusBarHeight");
                } catch (Exception e) {
                }
                try {
                    this.iTopMargin = jSONObject.getInt("topMargin");
                } catch (Exception e2) {
                }
                try {
                    this.iLeftMargin = jSONObject.getInt("leftMargin");
                } catch (Exception e3) {
                }
                try {
                    fTopMargin = jSONObject.getInt("fTopMargin");
                } catch (Exception e4) {
                }
                try {
                    fLeftMargin = jSONObject.getInt("fLeftMargin");
                } catch (Exception e5) {
                }
                try {
                    fWidth = jSONObject.getInt("fWidth");
                } catch (Exception e6) {
                }
                try {
                    fHeight = jSONObject.getInt("fHeight");
                } catch (Exception e7) {
                }
                try {
                    duration = jSONObject.getInt("duration");
                } catch (Exception e8) {
                }
            } catch (JSONException e9) {
                this.activity.finish();
            }
            Rect rectgle = new Rect();
            this.webview = Mobclix.getInstance().webview;
            this.webview.getJavascriptInterface().expanderActivity = this;
            Window ww = ((Activity) this.webview.getContext()).getWindow();
            ww.getDecorView().getWindowVisibleDisplayFrame(rectgle);
            this.windowFlags = ww.getAttributes().flags;
            ww.setFlags(1024, 1024);
            DisplayMetrics dm = new DisplayMetrics();
            this.activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
            int orientation = (this.activity.getWindowManager().getDefaultDisplay().getOrientation() + 1) % 2;
            this.screenWidth = dm.widthPixels;
            this.screenHeight = dm.heightPixels;
            this.activity.setRequestedOrientation(orientation);
            setBackgroundColor(Color.argb(128, 256, 256, 256));
            setOrientation(1);
            setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
            setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    MobclixExpander.this.exit(MobclixExpander.DEFAULT_DURATION);
                }
            });
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(1, this.iTopMargin);
            this.vSpacer = new View(this.activity);
            this.vSpacer.setLayoutParams(layoutParams);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
            LinearLayout hWrapper = new LinearLayout(this.activity);
            hWrapper.setLayoutParams(layoutParams2);
            LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(this.iLeftMargin, 1);
            this.hSpacer = new View(this.activity);
            this.hSpacer.setLayoutParams(layoutParams3);
            this.webviewContainer = (ViewGroup) this.webview.getParent();
            this.webviewContainer.removeView(this.webview);
            this.iWidth = this.webview.getWidth();
            this.iHeight = this.webview.getHeight();
            this.webview.setLayoutParams(new LinearLayout.LayoutParams(this.iWidth, this.iHeight));
            addView(this.vSpacer);
            addView(hWrapper);
            hWrapper.addView(this.hSpacer);
            hWrapper.addView(this.webview);
            expand(this.iLeftMargin, fLeftMargin, this.iTopMargin, fTopMargin, fWidth, fHeight, duration, this);
        }

        public synchronized void exit(int duration) {
            if (Mobclix.getInstance().webview != null) {
                this.webview.getJavascriptInterface().expanderActivity = null;
                Mobclix.getInstance().webview = null;
                this.open = false;
                this.webview.getJavascriptInterface().adWillContract();
                expand(this.iLeftMargin, this.iTopMargin, this.iWidth, this.iHeight, duration, this);
            }
        }

        public void expand(int fLeftMargin, int fTopMargin, int width, int height, int duration, Animation.AnimationListener l) {
            expand(this.hSpacer.getWidth(), fLeftMargin, this.vSpacer.getHeight(), fTopMargin, width, height, duration, l);
        }

        public void expand(int leftMargin, int fLeftMargin, int topMargin, int fTopMargin, int width, int height, int duration, Animation.AnimationListener l) {
            if (duration < 0) {
                duration = 0;
            }
            if (duration > 3000) {
                duration = 3000;
            }
            if (this.open) {
                if (topMargin < 0) {
                    topMargin = 0;
                }
                if (leftMargin < 0) {
                    leftMargin = 0;
                }
                if (fTopMargin < 0) {
                    fTopMargin = 0;
                }
                if (fLeftMargin < 0) {
                    fLeftMargin = 0;
                }
                if (width < 0) {
                    width = 1;
                }
                if (height < 0) {
                    height = 1;
                }
                DisplayMetrics dm = new DisplayMetrics();
                MobclixBrowserActivity.this.getWindowManager().getDefaultDisplay().getMetrics(dm);
                int screenWidth2 = dm.widthPixels;
                int screenHeight2 = dm.heightPixels;
                if (width > screenWidth2) {
                    width = screenWidth2;
                    fLeftMargin = 0;
                } else if (width + fLeftMargin > screenWidth2) {
                    fLeftMargin = screenWidth2 - width;
                }
                if (height > screenHeight2) {
                    height = screenHeight2;
                    fTopMargin = 0;
                } else if (height + fTopMargin > screenHeight2) {
                    fTopMargin = screenHeight2 - height;
                }
            }
            Animation vExpand = new ExpandAnimation(this.vSpacer, 1.0f, 1.0f, (float) topMargin, (float) fTopMargin);
            vExpand.setDuration((long) duration);
            Animation hExpand = new ExpandAnimation(this.hSpacer, (float) leftMargin, (float) fLeftMargin, 1.0f, 1.0f);
            hExpand.setDuration((long) duration);
            Animation webviewExpand = new ExpandAnimation(this.webview, (float) this.webview.getWidth(), (float) width, (float) this.webview.getHeight(), (float) height);
            webviewExpand.setDuration((long) duration);
            webviewExpand.setAnimationListener(this);
            this.vSpacer.startAnimation(vExpand);
            this.hSpacer.startAnimation(hExpand);
            this.webview.startAnimation(webviewExpand);
        }

        public void onAnimationEnd(Animation animation) {
            if (!this.open) {
                Window w = ((Activity) this.webview.getContext()).getWindow();
                WindowManager.LayoutParams wlp = w.getAttributes();
                wlp.flags = this.windowFlags;
                w.setAttributes(wlp);
                ((ViewGroup) this.webview.getParent()).removeView(this.webview);
                this.webviewContainer.addView(this.webview);
                MobclixBrowserActivity.this.unregisterReceiver(MobclixBrowserActivity.this.screenReceiver);
                this.activity.finish();
                return;
            }
            this.webview.getJavascriptInterface().resumeListeners();
            this.webview.getJavascriptInterface().adFinishedResizing();
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    class ExpandAnimation extends Animation {
        float dHeight;
        float dWidth;
        float height;
        View view;
        float width;

        ExpandAnimation(View v, float startWidth, float finalWidth, float startHeight, float finalHeight) {
            this.view = v;
            this.height = startHeight;
            this.dHeight = finalHeight - this.height;
            this.width = startWidth;
            this.dWidth = finalWidth - this.width;
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            ViewGroup.LayoutParams lp = this.view.getLayoutParams();
            lp.height = (int) (this.height + (this.dHeight * interpolatedTime));
            lp.width = (int) (this.width + (this.dWidth * interpolatedTime));
            this.view.setLayoutParams(lp);
        }
    }

    public class ScreenReceiver extends BroadcastReceiver {
        public boolean isScreenOn = false;

        public ScreenReceiver() {
        }

        /* Debug info: failed to restart local var, previous not found, register: 2 */
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
                ((MobclixExpander) MobclixBrowserActivity.this.view).webview.getJavascriptInterface().adWillBecomeHidden();
            } else {
                intent.getAction().equals("android.intent.action.SCREEN_ON");
            }
        }
    }
}
