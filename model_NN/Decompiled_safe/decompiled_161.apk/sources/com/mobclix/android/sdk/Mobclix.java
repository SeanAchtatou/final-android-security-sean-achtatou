package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.google.ads.AdActivity;
import com.mobclix.android.sdk.MobclixAnalytics;
import com.mobclix.android.sdk.MobclixCreative;
import com.mobclix.android.sdk.MobclixLocation;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

public final class Mobclix {
    static final boolean DEBUG = false;
    public static final int LOG_LEVEL_DEBUG = 1;
    public static final int LOG_LEVEL_ERROR = 8;
    public static final int LOG_LEVEL_FATAL = 16;
    public static final int LOG_LEVEL_INFO = 2;
    public static final int LOG_LEVEL_WARN = 4;
    static final String[] MC_AD_SIZES = {"320x50", "300x250"};
    static final String MC_CUSTOM_AD_FILENAME = "_mc_cached_custom_ad.png";
    static final String MC_CUSTOM_AD_PREF = "CustomAdUrl";
    static final String MC_KEY_CONNECTION_TYPE = "g";
    private static final String MC_KEY_EVENT_DESCRIPTION = "ed";
    private static final String MC_KEY_EVENT_LOG_LEVEL = "el";
    private static final String MC_KEY_EVENT_NAME = "en";
    private static final String MC_KEY_EVENT_PROCESS_NAME = "ep";
    private static final String MC_KEY_EVENT_STOP = "es";
    private static final String MC_KEY_EVENT_THREAD_ID = "et";
    static final String MC_KEY_LATITUDE_LONGITUDE = "ll";
    static final String MC_KEY_SESSION_ID = "id";
    private static final String MC_KEY_TIMESTAMP = "ts";
    static final String MC_LIBRARY_VERSION = "2.2.1";
    private static final String MC_TAG = "mobclix-controller";
    static final String PREFS_CONFIG = ".MCConfig";
    static HashMap<String, Boolean> autoplay = new HashMap<>();
    static HashMap<String, Long> autoplayInterval = new HashMap<>();
    /* access modifiers changed from: private */
    public static final Mobclix controller = new Mobclix();
    static HashMap<String, Boolean> customAdSet = new HashMap<>();
    static HashMap<String, String> customAdUrl = new HashMap<>();
    static HashMap<String, String> debugConfig = new HashMap<>();
    static HashMap<String, Boolean> enabled = new HashMap<>();
    private static boolean isInitialized = DEBUG;
    static HashMap<String, Long> refreshTime = new HashMap<>();
    static HashMap<String, Boolean> rmRequireUser = new HashMap<>();
    String adServer = "http://ads.mobclix.com/";
    String analyticsServer = "http://data.mobclix.com/post/sendData";
    private String androidId = "null";
    private String androidVersion = "null";
    private String applicationId = "null";
    private String applicationVersion = "null";
    /* access modifiers changed from: private */
    public int batteryLevel = -1;
    MobclixCreative.MobclixWebView cameraWebview;
    String configServer = "http://data.mobclix.com/post/config";
    private String connectionType = "null";
    /* access modifiers changed from: private */
    public Context context;
    String debugServer = "http://data.mobclix.com/post/debug";
    private String deviceHardwareModel = "null";
    String deviceId = "null";
    private String deviceModel = "null";
    String feedbackServer = "http://data.mobclix.com/post/feedback";
    private boolean haveLocationPermission = DEBUG;
    int idleTimeout = 120000;
    private MobclixInstrumentation instrumentation = MobclixInstrumentation.getInstance();
    private boolean isInSession = DEBUG;
    boolean isNewUser = DEBUG;
    boolean isOfflineSession = DEBUG;
    private boolean isTopTask = DEBUG;
    private String language = "null";
    /* access modifiers changed from: private */
    public String latitude = "null";
    private String locale = "null";
    MobclixLocation location = new MobclixLocation();
    private Criteria locationCriteria;
    private Handler locationHandler;
    private int logLevel = 16;
    /* access modifiers changed from: private */
    public String longitude = "null";
    private Handler mHandler;
    private String mcc = "null";
    private String mnc = "null";
    List<String> nativeUrls = new ArrayList();
    HashMap<String, Boolean> permissions = new HashMap<>();
    private String platform = "android";
    int pollTime = 30000;
    String previousDeviceId = null;
    int remoteConfigSet = 0;
    private boolean rooted = DEBUG;
    JSONObject session = new JSONObject();
    private long sessionEndTime = 0;
    private Timer sessionPollingTimer = new Timer();
    private long sessionStartTime = 0;
    private SharedPreferences sharedPrefs = null;
    private long totalIdleTime = 0;
    private String userAgent = "null";
    String vcServer = "http://vc.mobclix.com";
    MobclixCreative.MobclixWebView webview;

    /* access modifiers changed from: package-private */
    public void setContext(Activity a) {
        this.context = a;
    }

    /* access modifiers changed from: package-private */
    public Context getContext() {
        return this.context;
    }

    public String getApplicationId() {
        return this.applicationId == null ? "null" : this.applicationId;
    }

    /* access modifiers changed from: package-private */
    public String getPlatform() {
        return this.platform == null ? "null" : this.platform;
    }

    /* access modifiers changed from: package-private */
    public String getAndroidVersion() {
        return this.androidVersion == null ? "null" : this.androidVersion;
    }

    /* access modifiers changed from: package-private */
    public String getApplicationVersion() {
        return this.applicationVersion == null ? "null" : this.applicationVersion;
    }

    /* access modifiers changed from: package-private */
    public String getDeviceId() {
        return this.deviceId == null ? "null" : this.deviceId;
    }

    /* access modifiers changed from: package-private */
    public String getAndroidId() {
        return this.androidId == null ? "null" : this.androidId;
    }

    /* access modifiers changed from: package-private */
    public String getDeviceModel() {
        return this.deviceModel == null ? "null" : this.deviceModel;
    }

    /* access modifiers changed from: package-private */
    public String getDeviceHardwareModel() {
        return this.deviceHardwareModel == null ? "null" : this.deviceHardwareModel;
    }

    /* access modifiers changed from: package-private */
    public String getConnectionType() {
        return this.connectionType == null ? "null" : this.connectionType;
    }

    /* access modifiers changed from: package-private */
    public String getLatitude() {
        return this.latitude == null ? "null" : this.latitude;
    }

    /* access modifiers changed from: package-private */
    public String getLongitude() {
        return this.longitude == null ? "null" : this.longitude;
    }

    /* access modifiers changed from: package-private */
    public String getGPS() {
        if (getLatitude().equals("null") || getLongitude().equals("null")) {
            return "null";
        }
        return String.valueOf(getLatitude()) + "," + getLongitude();
    }

    /* access modifiers changed from: package-private */
    public String getLocale() {
        return this.locale == null ? "null" : this.locale;
    }

    /* access modifiers changed from: package-private */
    public String getLanguage() {
        return this.language == null ? "null" : this.language;
    }

    /* access modifiers changed from: package-private */
    public String getMcc() {
        return this.mcc == null ? "null" : this.mcc;
    }

    /* access modifiers changed from: package-private */
    public String getMnc() {
        return this.mnc == null ? "null" : this.mnc;
    }

    /* access modifiers changed from: package-private */
    public int getBatteryLevel() {
        return this.batteryLevel;
    }

    /* access modifiers changed from: package-private */
    public String getMobclixVersion() {
        return MC_LIBRARY_VERSION;
    }

    /* access modifiers changed from: package-private */
    public int getLogLevel() {
        return this.logLevel;
    }

    /* access modifiers changed from: package-private */
    public boolean isTopTask() {
        return this.isTopTask;
    }

    /* access modifiers changed from: package-private */
    public boolean isRooted() {
        return this.rooted;
    }

    /* access modifiers changed from: package-private */
    public boolean hasLocationPermission() {
        return this.haveLocationPermission;
    }

    /* access modifiers changed from: package-private */
    public String getDebugConfig(String tag) {
        try {
            return debugConfig.get(tag);
        } catch (Exception e) {
            return "";
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isEnabled(String size) {
        try {
            return enabled.get(size).booleanValue();
        } catch (Exception e) {
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public long getRefreshTime(String size) {
        try {
            return refreshTime.get(size).longValue();
        } catch (Exception e) {
            return -1;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean shouldAutoplay(String size) {
        try {
            return autoplay.get(size).booleanValue();
        } catch (Exception e) {
            return DEBUG;
        }
    }

    /* access modifiers changed from: package-private */
    public Long getAutoplayInterval(String size) {
        try {
            return autoplayInterval.get(size);
        } catch (Exception e) {
            return 0L;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean rmRequireUserInteraction(String size) {
        try {
            return rmRequireUser.get(size).booleanValue();
        } catch (Exception e) {
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public String getCustomAdUrl(String size) {
        try {
            return customAdUrl.get(size);
        } catch (Exception e) {
            return "";
        }
    }

    /* access modifiers changed from: package-private */
    public String getDebugServer() {
        return this.debugServer;
    }

    /* access modifiers changed from: package-private */
    public String getAdServer() {
        return this.adServer;
    }

    /* access modifiers changed from: package-private */
    public String getConfigServer() {
        return this.configServer;
    }

    /* access modifiers changed from: package-private */
    public String getAnalyticsServer() {
        return this.analyticsServer;
    }

    /* access modifiers changed from: package-private */
    public String getVcServer() {
        return this.vcServer;
    }

    /* access modifiers changed from: package-private */
    public String getFeedbackServer() {
        return this.feedbackServer;
    }

    /* access modifiers changed from: package-private */
    public List<String> getNativeUrls() {
        return this.nativeUrls;
    }

    /* access modifiers changed from: package-private */
    public int isRemoteConfigSet() {
        return this.remoteConfigSet;
    }

    /* access modifiers changed from: package-private */
    public String getUserAgent() {
        if (this.userAgent.equals("null") && hasPref("UserAgent")) {
            this.userAgent = getPref("UserAgent");
        }
        return this.userAgent;
    }

    /* access modifiers changed from: package-private */
    public void setUserAgent(String u) {
        this.userAgent = u;
        addPref("UserAgent", u);
    }

    static HashMap<String, String> getAllPref() {
        try {
            return (HashMap) controller.sharedPrefs.getAll();
        } catch (Exception e) {
            return new HashMap<>();
        }
    }

    static String getPref(String k) {
        try {
            return controller.sharedPrefs.getString(k, "");
        } catch (Exception e) {
            return "";
        }
    }

    static boolean hasPref(String k) {
        try {
            return controller.sharedPrefs.contains(k);
        } catch (Exception e) {
            return DEBUG;
        }
    }

    static void addPref(String k, String v) {
        try {
            SharedPreferences.Editor spe = controller.sharedPrefs.edit();
            spe.putString(k, v);
            spe.commit();
        } catch (Exception e) {
        }
    }

    static void addPref(Map<String, String> m) {
        try {
            SharedPreferences.Editor spe = controller.sharedPrefs.edit();
            for (Map.Entry<String, String> pair : m.entrySet()) {
                spe.putString((String) pair.getKey(), (String) pair.getValue());
            }
            spe.commit();
        } catch (Exception e) {
        }
    }

    static void removePref(String k) {
        try {
            SharedPreferences.Editor spe = controller.sharedPrefs.edit();
            spe.remove(k);
            spe.commit();
        } catch (Exception e) {
        }
    }

    static void clearPref() {
        try {
            SharedPreferences.Editor spe = controller.sharedPrefs.edit();
            spe.clear();
            spe.commit();
        } catch (Exception e) {
        }
    }

    private static String sha1(String string) {
        byte[] bArr = new byte[40];
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(string.getBytes(), 0, string.length());
            byte[] shaHash = md.digest();
            StringBuffer hexString = new StringBuffer();
            for (byte b : shaHash) {
                hexString.append(Integer.toHexString(b & 255));
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public void updateSession() {
        updateConnectivity();
        if (this.haveLocationPermission) {
            this.locationHandler.sendEmptyMessage(0);
        }
        try {
            this.session.put(MC_KEY_TIMESTAMP, System.currentTimeMillis());
            String loc = getGPS();
            if (!loc.equals("null")) {
                this.session.put(MC_KEY_LATITUDE_LONGITUDE, loc);
            } else {
                this.session.remove(MC_KEY_LATITUDE_LONGITUDE);
            }
            this.session.put(MC_KEY_CONNECTION_TYPE, this.connectionType);
        } catch (JSONException e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void updateLocation() {
        this.location.getLocation(this.context, new MobclixLocation.LocationResult() {
            public void gotLocation(Location location) {
                try {
                    Mobclix.this.latitude = Double.toString(location.getLatitude());
                    Mobclix.this.longitude = Double.toString(location.getLongitude());
                } catch (Exception e) {
                }
            }
        });
    }

    private void updateConnectivity() {
        NetworkInfo network_info;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) this.context.getSystemService("connectivity");
            String network_type = AdActivity.URL_PARAM;
            if (this.permissions.get("android.permission.ACCESS_NETWORK_STATE").booleanValue() && (network_info = connectivityManager.getActiveNetworkInfo()) != null) {
                network_type = network_info.getTypeName();
            }
            if (network_type.equals("WI_FI") || network_type.equals("WIFI")) {
                this.connectionType = "wifi";
            } else if (network_type.equals("MOBILE")) {
                this.connectionType = Integer.toString(((TelephonyManager) this.context.getSystemService("phone")).getNetworkType());
            } else {
                this.connectionType = "null";
            }
            if (this.connectionType == null) {
                this.connectionType = "null";
            }
        } catch (Exception e) {
            this.connectionType = "null";
        }
    }

    private Mobclix() {
    }

    public static Mobclix getInstance() {
        return controller;
    }

    public static final synchronized void onCreate(Activity a) {
        synchronized (Mobclix.class) {
            if (a != null) {
                try {
                    controller.context = a.getApplicationContext();
                } catch (Exception e) {
                }
            }
            if (!isInitialized) {
                try {
                    controller.initialize(a);
                } catch (MobclixPermissionException e2) {
                    throw e2;
                } catch (Exception e3) {
                }
            }
            controller.handleSessionStatus(true);
        }
        return;
    }

    private void initialize(Activity a) {
        initialize(a, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r21v0, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v129, resolved type: android.telephony.TelephonyManager} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void initialize(android.app.Activity r23, java.lang.String r24) {
        /*
            r22 = this;
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = r23.getPackageName()
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r3.<init>(r4)
            java.lang.String r4 = ".MCConfig"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r4 = 0
            r0 = r23
            r1 = r3
            r2 = r4
            android.content.SharedPreferences r3 = r0.getSharedPreferences(r1, r2)
            r0 = r3
            r1 = r22
            r1.sharedPrefs = r0
            java.lang.String[] r3 = com.mobclix.android.sdk.MobclixInstrumentation.MC_DEBUG_CATS
            int r4 = r3.length
            r5 = 0
        L_0x0029:
            if (r5 < r4) goto L_0x0085
            r0 = r22
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r3 = r0
            if (r3 != 0) goto L_0x003b
            com.mobclix.android.sdk.MobclixInstrumentation r3 = com.mobclix.android.sdk.MobclixInstrumentation.getInstance()
            r0 = r3
            r1 = r22
            r1.instrumentation = r0
        L_0x003b:
            r0 = r22
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r3 = r0
            java.lang.String r4 = com.mobclix.android.sdk.MobclixInstrumentation.STARTUP
            java.lang.String r13 = r3.startGroup(r4)
            r0 = r22
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r3 = r0
            java.lang.String r4 = "init"
            java.lang.String r13 = r3.benchmarkStart(r13, r4)
            r0 = r22
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r3 = r0
            java.lang.String r4 = "environment"
            java.lang.String r13 = r3.benchmarkStart(r13, r4)
            java.lang.String r17 = ""
            r10 = 0
            java.lang.String r17 = r23.getPackageName()     // Catch:{ Exception -> 0x0593 }
        L_0x0063:
            android.content.pm.PackageManager r3 = r23.getPackageManager()     // Catch:{ NameNotFoundException -> 0x00c2 }
            r4 = 128(0x80, float:1.794E-43)
            r0 = r3
            r1 = r17
            r2 = r4
            android.content.pm.ApplicationInfo r10 = r0.getApplicationInfo(r1, r2)     // Catch:{ NameNotFoundException -> 0x00c2 }
        L_0x0071:
            if (r24 != 0) goto L_0x00d6
            android.os.Bundle r3 = r10.metaData     // Catch:{ NullPointerException -> 0x00cc }
            java.lang.String r4 = "com.mobclix.APPLICATION_ID"
            java.lang.String r24 = r3.getString(r4)     // Catch:{ NullPointerException -> 0x00cc }
            if (r24 != 0) goto L_0x00d6
            android.content.res.Resources$NotFoundException r3 = new android.content.res.Resources$NotFoundException
            java.lang.String r4 = "com.mobclix.APPLICATION_ID not found in the Android Manifest xml."
            r3.<init>(r4)
            throw r3
        L_0x0085:
            r21 = r3[r5]
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "debug_"
            r6.<init>(r7)
            r0 = r6
            r1 = r21
            java.lang.StringBuilder r6 = r0.append(r1)
            java.lang.String r6 = r6.toString()
            boolean r6 = hasPref(r6)
            if (r6 == 0) goto L_0x00be
            java.util.HashMap<java.lang.String, java.lang.String> r6 = com.mobclix.android.sdk.Mobclix.debugConfig
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "debug_"
            r7.<init>(r8)
            r0 = r7
            r1 = r21
            java.lang.StringBuilder r7 = r0.append(r1)
            java.lang.String r7 = r7.toString()
            java.lang.String r7 = getPref(r7)
            r0 = r6
            r1 = r21
            r2 = r7
            r0.put(r1, r2)
        L_0x00be:
            int r5 = r5 + 1
            goto L_0x0029
        L_0x00c2:
            r3 = move-exception
            r12 = r3
            java.lang.String r3 = "mobclix-controller"
            java.lang.String r4 = "Application Key Started"
            android.util.Log.e(r3, r4)
            goto L_0x0071
        L_0x00cc:
            r3 = move-exception
            r12 = r3
            android.content.res.Resources$NotFoundException r3 = new android.content.res.Resources$NotFoundException
            java.lang.String r4 = "com.mobclix.APPLICATION_ID not found in the Android Manifest xml."
            r3.<init>(r4)
            throw r3
        L_0x00d6:
            r0 = r24
            r1 = r22
            r1.applicationId = r0
            r15 = 0
            android.os.Bundle r3 = r10.metaData     // Catch:{ Exception -> 0x05b1 }
            java.lang.String r4 = "com.mobclix.LOG_LEVEL"
            java.lang.String r15 = r3.getString(r4)     // Catch:{ Exception -> 0x05b1 }
        L_0x00e5:
            r14 = 16
            if (r15 == 0) goto L_0x00f2
            java.lang.String r3 = "debug"
            boolean r3 = r15.equalsIgnoreCase(r3)
            if (r3 == 0) goto L_0x0166
            r14 = 1
        L_0x00f2:
            r0 = r14
            r1 = r22
            r1.logLevel = r0
            r0 = r23
            r1 = r22
            r1.context = r0
            r0 = r22
            java.lang.String r0 = r0.applicationId
            r3 = r0
            if (r3 == 0) goto L_0x0111
            r0 = r22
            java.lang.String r0 = r0.applicationId
            r3 = r0
            java.lang.String r4 = ""
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0118
        L_0x0111:
            java.lang.String r3 = "null"
            r0 = r3
            r1 = r22
            r1.applicationId = r0
        L_0x0118:
            java.lang.String r3 = android.os.Build.VERSION.RELEASE
            r0 = r3
            r1 = r22
            r1.androidVersion = r0
            r0 = r22
            java.lang.String r0 = r0.androidVersion
            r3 = r0
            if (r3 == 0) goto L_0x0133
            r0 = r22
            java.lang.String r0 = r0.androidVersion
            r3 = r0
            java.lang.String r4 = ""
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x013a
        L_0x0133:
            java.lang.String r3 = "null"
            r0 = r3
            r1 = r22
            r1.androidVersion = r0
        L_0x013a:
            java.lang.String r9 = ""
            r0 = r22
            android.content.Context r0 = r0.context
            r3 = r0
            android.content.pm.PackageManager r18 = r3.getPackageManager()
            r0 = r22
            android.content.Context r0 = r0.context     // Catch:{ Exception -> 0x05ae }
            r3 = r0
            java.lang.String r9 = r3.getPackageName()     // Catch:{ Exception -> 0x05ae }
        L_0x014e:
            java.lang.String r3 = "android.permission.GET_TASKS"
            r0 = r18
            r1 = r3
            r2 = r9
            int r3 = r0.checkPermission(r1, r2)
            if (r3 == 0) goto L_0x0193
            com.mobclix.android.sdk.Mobclix$MobclixPermissionException r3 = new com.mobclix.android.sdk.Mobclix$MobclixPermissionException
            java.lang.String r4 = "Missing required permission GET_TASKS."
            r0 = r3
            r1 = r22
            r2 = r4
            r0.<init>(r2)
            throw r3
        L_0x0166:
            java.lang.String r3 = "info"
            boolean r3 = r15.equalsIgnoreCase(r3)
            if (r3 == 0) goto L_0x0170
            r14 = 2
            goto L_0x00f2
        L_0x0170:
            java.lang.String r3 = "warn"
            boolean r3 = r15.equalsIgnoreCase(r3)
            if (r3 == 0) goto L_0x017b
            r14 = 4
            goto L_0x00f2
        L_0x017b:
            java.lang.String r3 = "error"
            boolean r3 = r15.equalsIgnoreCase(r3)
            if (r3 == 0) goto L_0x0187
            r14 = 8
            goto L_0x00f2
        L_0x0187:
            java.lang.String r3 = "fatal"
            boolean r3 = r15.equalsIgnoreCase(r3)
            if (r3 == 0) goto L_0x00f2
            r14 = 16
            goto L_0x00f2
        L_0x0193:
            r0 = r22
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r3 = r0
            java.lang.String r4 = "android.permission.GET_TASKS"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r3.put(r4, r5)
            java.lang.String r3 = "android.permission.INTERNET"
            r0 = r18
            r1 = r3
            r2 = r9
            int r3 = r0.checkPermission(r1, r2)
            if (r3 == 0) goto L_0x01ba
            com.mobclix.android.sdk.Mobclix$MobclixPermissionException r3 = new com.mobclix.android.sdk.Mobclix$MobclixPermissionException
            java.lang.String r4 = "Missing required permission INTERNET."
            r0 = r3
            r1 = r22
            r2 = r4
            r0.<init>(r2)
            throw r3
        L_0x01ba:
            r0 = r22
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r3 = r0
            java.lang.String r4 = "android.permission.INTERNET"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r3.put(r4, r5)
            java.lang.String r3 = "android.permission.READ_PHONE_STATE"
            r0 = r18
            r1 = r3
            r2 = r9
            int r3 = r0.checkPermission(r1, r2)
            if (r3 == 0) goto L_0x01e1
            com.mobclix.android.sdk.Mobclix$MobclixPermissionException r3 = new com.mobclix.android.sdk.Mobclix$MobclixPermissionException
            java.lang.String r4 = "Missing required permission READ_PHONE_STATE."
            r0 = r3
            r1 = r22
            r2 = r4
            r0.<init>(r2)
            throw r3
        L_0x01e1:
            r0 = r22
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r3 = r0
            java.lang.String r4 = "android.permission.READ_PHONE_STATE"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r3.put(r4, r5)
            java.lang.String r3 = "android.permission.BATTERY_STATS"
            r0 = r18
            r1 = r3
            r2 = r9
            int r3 = r0.checkPermission(r1, r2)     // Catch:{ Exception -> 0x05ab }
            if (r3 != 0) goto L_0x0213
            r0 = r22
            android.content.Context r0 = r0.context     // Catch:{ Exception -> 0x05ab }
            r3 = r0
            com.mobclix.android.sdk.Mobclix$2 r4 = new com.mobclix.android.sdk.Mobclix$2     // Catch:{ Exception -> 0x05ab }
            r0 = r4
            r1 = r22
            r0.<init>()     // Catch:{ Exception -> 0x05ab }
            android.content.IntentFilter r5 = new android.content.IntentFilter     // Catch:{ Exception -> 0x05ab }
            java.lang.String r6 = "android.intent.action.BATTERY_CHANGED"
            r5.<init>(r6)     // Catch:{ Exception -> 0x05ab }
            r3.registerReceiver(r4, r5)     // Catch:{ Exception -> 0x05ab }
        L_0x0213:
            r0 = r22
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions     // Catch:{ Exception -> 0x05ab }
            r3 = r0
            java.lang.String r4 = "android.permission.BATTERY_STATS"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x05ab }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x05ab }
        L_0x0222:
            java.lang.String r3 = "android.permission.CAMERA"
            r0 = r18
            r1 = r3
            r2 = r9
            int r3 = r0.checkPermission(r1, r2)
            if (r3 != 0) goto L_0x023d
            r0 = r22
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r3 = r0
            java.lang.String r4 = "android.permission.CAMERA"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r3.put(r4, r5)
        L_0x023d:
            java.lang.String r3 = "android.permission.READ_CALENDAR"
            r0 = r18
            r1 = r3
            r2 = r9
            int r3 = r0.checkPermission(r1, r2)
            if (r3 != 0) goto L_0x0258
            r0 = r22
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r3 = r0
            java.lang.String r4 = "android.permission.READ_CALENDAR"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r3.put(r4, r5)
        L_0x0258:
            java.lang.String r3 = "android.permission.WRITE_CALENDAR"
            r0 = r18
            r1 = r3
            r2 = r9
            int r3 = r0.checkPermission(r1, r2)
            if (r3 != 0) goto L_0x0273
            r0 = r22
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r3 = r0
            java.lang.String r4 = "android.permission.WRITE_CALENDAR"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r3.put(r4, r5)
        L_0x0273:
            java.lang.String r3 = "android.permission.READ_CONTACTS"
            r0 = r18
            r1 = r3
            r2 = r9
            int r3 = r0.checkPermission(r1, r2)
            if (r3 != 0) goto L_0x028e
            r0 = r22
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r3 = r0
            java.lang.String r4 = "android.permission.READ_CONTACTS"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r3.put(r4, r5)
        L_0x028e:
            java.lang.String r3 = "android.permission.WRITE_CONTACTS"
            r0 = r18
            r1 = r3
            r2 = r9
            int r3 = r0.checkPermission(r1, r2)
            if (r3 != 0) goto L_0x02a9
            r0 = r22
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r3 = r0
            java.lang.String r4 = "android.permission.WRITE_CONTACTS"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r3.put(r4, r5)
        L_0x02a9:
            java.lang.String r3 = "android.permission.GET_ACCOUNTS"
            r0 = r18
            r1 = r3
            r2 = r9
            int r3 = r0.checkPermission(r1, r2)
            if (r3 != 0) goto L_0x02c4
            r0 = r22
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r3 = r0
            java.lang.String r4 = "android.permission.GET_ACCOUNTS"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r3.put(r4, r5)
        L_0x02c4:
            java.lang.String r3 = "android.permission.VIBRATE"
            r0 = r18
            r1 = r3
            r2 = r9
            int r3 = r0.checkPermission(r1, r2)
            if (r3 != 0) goto L_0x02df
            r0 = r22
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r3 = r0
            java.lang.String r4 = "android.permission.VIBRATE"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r3.put(r4, r5)
        L_0x02df:
            java.lang.String r3 = "android.permission.ACCESS_FINE_LOCATION"
            r0 = r18
            r1 = r3
            r2 = r9
            int r3 = r0.checkPermission(r1, r2)     // Catch:{ Exception -> 0x0568 }
            if (r3 != 0) goto L_0x0532
            android.location.Criteria r3 = new android.location.Criteria     // Catch:{ Exception -> 0x0568 }
            r3.<init>()     // Catch:{ Exception -> 0x0568 }
            r0 = r3
            r1 = r22
            r1.locationCriteria = r0     // Catch:{ Exception -> 0x0568 }
            r0 = r22
            android.location.Criteria r0 = r0.locationCriteria     // Catch:{ Exception -> 0x0568 }
            r3 = r0
            r4 = 1
            r3.setAccuracy(r4)     // Catch:{ Exception -> 0x0568 }
            r3 = 1
            r0 = r3
            r1 = r22
            r1.haveLocationPermission = r0     // Catch:{ Exception -> 0x0568 }
            r0 = r22
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions     // Catch:{ Exception -> 0x0568 }
            r3 = r0
            java.lang.String r4 = "android.permission.ACCESS_FINE_LOCATION"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0568 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0568 }
        L_0x0313:
            java.lang.String r3 = "android.permission.ACCESS_NETWORK_STATE"
            r0 = r18
            r1 = r3
            r2 = r9
            int r3 = r0.checkPermission(r1, r2)     // Catch:{ Exception -> 0x0568 }
            if (r3 != 0) goto L_0x032e
            r0 = r22
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions     // Catch:{ Exception -> 0x0568 }
            r3 = r0
            java.lang.String r4 = "android.permission.ACCESS_NETWORK_STATE"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0568 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0568 }
        L_0x032e:
            r20 = 0
            r0 = r22
            android.content.Context r0 = r0.context     // Catch:{ Exception -> 0x05a8 }
            r3 = r0
            java.lang.String r4 = "phone"
            java.lang.Object r21 = r3.getSystemService(r4)     // Catch:{ Exception -> 0x05a8 }
            r0 = r21
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Exception -> 0x05a8 }
            r20 = r0
        L_0x0341:
            r3 = 0
            r0 = r18
            r1 = r9
            r2 = r3
            android.content.pm.PackageInfo r3 = r0.getPackageInfo(r1, r2)     // Catch:{ Exception -> 0x05a5 }
            java.lang.String r3 = r3.versionName     // Catch:{ Exception -> 0x05a5 }
            r0 = r3
            r1 = r22
            r1.applicationVersion = r0     // Catch:{ Exception -> 0x05a5 }
        L_0x0351:
            r0 = r22
            java.lang.String r0 = r0.applicationVersion
            r3 = r0
            if (r3 == 0) goto L_0x0365
            r0 = r22
            java.lang.String r0 = r0.applicationVersion
            r3 = r0
            java.lang.String r4 = ""
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x036c
        L_0x0365:
            java.lang.String r3 = "null"
            r0 = r3
            r1 = r22
            r1.applicationVersion = r0
        L_0x036c:
            android.content.ContentResolver r3 = r23.getContentResolver()     // Catch:{ Exception -> 0x0573 }
            java.lang.String r4 = "android_id"
            java.lang.String r3 = android.provider.Settings.System.getString(r3, r4)     // Catch:{ Exception -> 0x0573 }
            r0 = r3
            r1 = r22
            r1.androidId = r0     // Catch:{ Exception -> 0x0573 }
        L_0x037b:
            r0 = r22
            java.lang.String r0 = r0.androidId
            r3 = r0
            if (r3 == 0) goto L_0x038f
            r0 = r22
            java.lang.String r0 = r0.androidId
            r3 = r0
            java.lang.String r4 = ""
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0396
        L_0x038f:
            java.lang.String r3 = "null"
            r0 = r3
            r1 = r22
            r1.androidId = r0
        L_0x0396:
            java.lang.String r3 = r20.getDeviceId()     // Catch:{ Exception -> 0x05a2 }
            r0 = r3
            r1 = r22
            r1.deviceId = r0     // Catch:{ Exception -> 0x05a2 }
        L_0x039f:
            r0 = r22
            java.lang.String r0 = r0.deviceId
            r3 = r0
            if (r3 == 0) goto L_0x03b3
            r0 = r22
            java.lang.String r0 = r0.deviceId
            r3 = r0
            java.lang.String r4 = ""
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x03bd
        L_0x03b3:
            r0 = r22
            java.lang.String r0 = r0.androidId
            r3 = r0
            r0 = r3
            r1 = r22
            r1.deviceId = r0
        L_0x03bd:
            java.lang.String r3 = android.os.Build.MODEL     // Catch:{ Exception -> 0x059f }
            r0 = r3
            r1 = r22
            r1.deviceModel = r0     // Catch:{ Exception -> 0x059f }
        L_0x03c4:
            r0 = r22
            java.lang.String r0 = r0.deviceModel
            r3 = r0
            if (r3 == 0) goto L_0x03d8
            r0 = r22
            java.lang.String r0 = r0.deviceModel
            r3 = r0
            java.lang.String r4 = ""
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x03df
        L_0x03d8:
            java.lang.String r3 = "null"
            r0 = r3
            r1 = r22
            r1.deviceModel = r0
        L_0x03df:
            java.lang.String r3 = android.os.Build.DEVICE     // Catch:{ Exception -> 0x059c }
            r0 = r3
            r1 = r22
            r1.deviceHardwareModel = r0     // Catch:{ Exception -> 0x059c }
        L_0x03e6:
            r0 = r22
            java.lang.String r0 = r0.deviceHardwareModel
            r3 = r0
            if (r3 == 0) goto L_0x03fa
            r0 = r22
            java.lang.String r0 = r0.deviceHardwareModel
            r3 = r0
            java.lang.String r4 = ""
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0401
        L_0x03fa:
            java.lang.String r3 = "null"
            r0 = r3
            r1 = r22
            r1.deviceHardwareModel = r0
        L_0x0401:
            java.lang.Runtime r3 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x0589 }
            java.lang.String r4 = "su"
            java.lang.Process r19 = r3.exec(r4)     // Catch:{ Exception -> 0x0589 }
            r3 = 1
            r0 = r3
            r1 = r22
            r1.rooted = r0     // Catch:{ Exception -> 0x0589 }
        L_0x0411:
            java.lang.String r16 = r20.getNetworkOperator()     // Catch:{ Exception -> 0x0599 }
            if (r16 == 0) goto L_0x0433
            r3 = 0
            r4 = 3
            r0 = r16
            r1 = r3
            r2 = r4
            java.lang.String r3 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x0599 }
            r0 = r3
            r1 = r22
            r1.mcc = r0     // Catch:{ Exception -> 0x0599 }
            r3 = 3
            r0 = r16
            r1 = r3
            java.lang.String r3 = r0.substring(r1)     // Catch:{ Exception -> 0x0599 }
            r0 = r3
            r1 = r22
            r1.mnc = r0     // Catch:{ Exception -> 0x0599 }
        L_0x0433:
            r0 = r22
            java.lang.String r0 = r0.mcc
            r3 = r0
            if (r3 == 0) goto L_0x0447
            r0 = r22
            java.lang.String r0 = r0.mcc
            r3 = r0
            java.lang.String r4 = ""
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x044e
        L_0x0447:
            java.lang.String r3 = "null"
            r0 = r3
            r1 = r22
            r1.mcc = r0
        L_0x044e:
            r0 = r22
            java.lang.String r0 = r0.mnc
            r3 = r0
            if (r3 == 0) goto L_0x0462
            r0 = r22
            java.lang.String r0 = r0.mnc
            r3 = r0
            java.lang.String r4 = ""
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0469
        L_0x0462:
            java.lang.String r3 = "null"
            r0 = r3
            r1 = r22
            r1.mnc = r0
        L_0x0469:
            java.util.Locale r11 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x0596 }
            java.lang.String r3 = r11.toString()     // Catch:{ Exception -> 0x0596 }
            r0 = r3
            r1 = r22
            r1.locale = r0     // Catch:{ Exception -> 0x0596 }
            java.lang.String r3 = r11.getLanguage()     // Catch:{ Exception -> 0x0596 }
            r0 = r3
            r1 = r22
            r1.language = r0     // Catch:{ Exception -> 0x0596 }
        L_0x047f:
            r0 = r22
            java.lang.String r0 = r0.locale
            r3 = r0
            if (r3 == 0) goto L_0x0493
            r0 = r22
            java.lang.String r0 = r0.locale
            r3 = r0
            java.lang.String r4 = ""
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x049a
        L_0x0493:
            java.lang.String r3 = "null"
            r0 = r3
            r1 = r22
            r1.locale = r0
        L_0x049a:
            r0 = r22
            java.lang.String r0 = r0.language
            r3 = r0
            if (r3 == 0) goto L_0x04ae
            r0 = r22
            java.lang.String r0 = r0.language
            r3 = r0
            java.lang.String r4 = ""
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x04b5
        L_0x04ae:
            java.lang.String r3 = "null"
            r0 = r3
            r1 = r22
            r1.language = r0
        L_0x04b5:
            r0 = r22
            android.content.Context r0 = r0.context
            r3 = r0
            android.webkit.CookieSyncManager.createInstance(r3)
            com.mobclix.android.sdk.Mobclix$3 r3 = new com.mobclix.android.sdk.Mobclix$3
            r0 = r3
            r1 = r22
            r0.<init>()
            r0 = r3
            r1 = r22
            r1.locationHandler = r0
            r0 = r22
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r3 = r0
            java.lang.String r13 = r3.benchmarkFinishPath(r13)
            r0 = r22
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r3 = r0
            java.lang.String r4 = "session"
            java.lang.String r13 = r3.benchmarkStart(r13, r4)
            com.mobclix.android.sdk.Mobclix$MobclixConfigHandler r3 = new com.mobclix.android.sdk.Mobclix$MobclixConfigHandler
            r0 = r3
            r1 = r22
            r0.<init>()
            r0 = r3
            r1 = r22
            r1.mHandler = r0
            r0 = r22
            int r0 = r0.pollTime
            r3 = r0
            r0 = r22
            int r0 = r0.idleTimeout
            r4 = r0
            int r3 = java.lang.Math.min(r3, r4)
            r0 = r3
            r1 = r22
            r1.pollTime = r0
            r0 = r22
            java.util.Timer r0 = r0.sessionPollingTimer
            r3 = r0
            com.mobclix.android.sdk.Mobclix$SessionPolling r4 = new com.mobclix.android.sdk.Mobclix$SessionPolling
            r5 = 0
            r0 = r4
            r1 = r22
            r2 = r5
            r0.<init>(r1, r2)
            r0 = r22
            int r0 = r0.pollTime
            r5 = r0
            long r5 = (long) r5
            r0 = r22
            int r0 = r0.pollTime
            r7 = r0
            long r7 = (long) r7
            r3.scheduleAtFixedRate(r4, r5, r7)
            r3 = 1
            com.mobclix.android.sdk.Mobclix.isInitialized = r3
            r0 = r22
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r3 = r0
            java.lang.String r13 = r3.benchmarkFinishPath(r13)
            r0 = r22
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r3 = r0
            java.lang.String r13 = r3.benchmarkFinishPath(r13)
            return
        L_0x0532:
            java.lang.String r3 = "android.permission.ACCESS_COARSE_LOCATION"
            r0 = r18
            r1 = r3
            r2 = r9
            int r3 = r0.checkPermission(r1, r2)     // Catch:{ Exception -> 0x0568 }
            if (r3 != 0) goto L_0x056b
            android.location.Criteria r3 = new android.location.Criteria     // Catch:{ Exception -> 0x0568 }
            r3.<init>()     // Catch:{ Exception -> 0x0568 }
            r0 = r3
            r1 = r22
            r1.locationCriteria = r0     // Catch:{ Exception -> 0x0568 }
            r0 = r22
            android.location.Criteria r0 = r0.locationCriteria     // Catch:{ Exception -> 0x0568 }
            r3 = r0
            r4 = 2
            r3.setAccuracy(r4)     // Catch:{ Exception -> 0x0568 }
            r3 = 1
            r0 = r3
            r1 = r22
            r1.haveLocationPermission = r0     // Catch:{ Exception -> 0x0568 }
            r0 = r22
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions     // Catch:{ Exception -> 0x0568 }
            r3 = r0
            java.lang.String r4 = "android.permission.ACCESS_COARSE_LOCATION"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0568 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0568 }
            goto L_0x0313
        L_0x0568:
            r3 = move-exception
            goto L_0x032e
        L_0x056b:
            r3 = 0
            r0 = r3
            r1 = r22
            r1.haveLocationPermission = r0     // Catch:{ Exception -> 0x0568 }
            goto L_0x0313
        L_0x0573:
            r3 = move-exception
            r12 = r3
            android.content.ContentResolver r3 = r23.getContentResolver()     // Catch:{ Exception -> 0x0586 }
            java.lang.String r4 = "android_id"
            java.lang.String r3 = android.provider.Settings.System.getString(r3, r4)     // Catch:{ Exception -> 0x0586 }
            r0 = r3
            r1 = r22
            r1.androidId = r0     // Catch:{ Exception -> 0x0586 }
            goto L_0x037b
        L_0x0586:
            r3 = move-exception
            goto L_0x037b
        L_0x0589:
            r3 = move-exception
            r12 = r3
            r3 = 0
            r0 = r3
            r1 = r22
            r1.rooted = r0
            goto L_0x0411
        L_0x0593:
            r3 = move-exception
            goto L_0x0063
        L_0x0596:
            r3 = move-exception
            goto L_0x047f
        L_0x0599:
            r3 = move-exception
            goto L_0x0433
        L_0x059c:
            r3 = move-exception
            goto L_0x03e6
        L_0x059f:
            r3 = move-exception
            goto L_0x03c4
        L_0x05a2:
            r3 = move-exception
            goto L_0x039f
        L_0x05a5:
            r3 = move-exception
            goto L_0x0351
        L_0x05a8:
            r3 = move-exception
            goto L_0x0341
        L_0x05ab:
            r3 = move-exception
            goto L_0x0222
        L_0x05ae:
            r3 = move-exception
            goto L_0x014e
        L_0x05b1:
            r3 = move-exception
            goto L_0x00e5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.Mobclix.initialize(android.app.Activity, java.lang.String):void");
    }

    public static final synchronized void onStop(Activity a) {
        synchronized (Mobclix.class) {
            controller.handleSessionStatus(DEBUG);
        }
    }

    class MobclixConfigHandler extends Handler {
        MobclixConfigHandler() {
        }

        public void handleMessage(Message msg) {
            Mobclix.this.createNewSession();
        }
    }

    public static final void logEvent(int eventLogLevel, String processName, String eventName, String description, boolean stopProcess) {
        if (!isInitialized) {
            Log.v(MC_TAG, "logEvent failed - You must initialize Mobclix by calling Mobclix.onCreate(this).");
        } else if (eventLogLevel >= controller.logLevel) {
            String logString = String.valueOf(processName) + ", " + eventName + ": " + description;
            switch (eventLogLevel) {
                case 1:
                    Log.d("Mobclix", logString);
                    break;
                case 2:
                    Log.i("Mobclix", logString);
                    break;
                case 4:
                    Log.w("Mobclix", logString);
                    break;
                case 8:
                    Log.e("Mobclix", logString);
                    break;
                case 16:
                    Log.e("Mobclix", logString);
                    break;
            }
            try {
                JSONObject event = new JSONObject(controller.session, new String[]{MC_KEY_TIMESTAMP, MC_KEY_LATITUDE_LONGITUDE, MC_KEY_CONNECTION_TYPE, MC_KEY_SESSION_ID});
                event.put(MC_KEY_EVENT_LOG_LEVEL, Integer.toString(eventLogLevel));
                event.put(MC_KEY_EVENT_PROCESS_NAME, URLEncoder.encode(processName, "UTF-8"));
                event.put(MC_KEY_EVENT_NAME, URLEncoder.encode(eventName, "UTF-8"));
                event.put(MC_KEY_EVENT_DESCRIPTION, URLEncoder.encode(description, "UTF-8"));
                event.put(MC_KEY_EVENT_THREAD_ID, Long.toString(Thread.currentThread().getId()));
                event.put(MC_KEY_EVENT_STOP, stopProcess ? "1" : "0");
                new Thread(new MobclixAnalytics.LogEvent(event)).start();
            } catch (Exception e) {
            }
        }
    }

    public static final void sync() {
        if (!isInitialized) {
            Log.v(MC_TAG, "sync failed - You must initialize Mobclix by calling Mobclix.onCreate(this).");
        } else if (MobclixAnalytics.getSyncStatus() == MobclixAnalytics.SYNC_READY) {
            new Thread(new MobclixAnalytics.Sync()).start();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void createNewSession() {
        String instrPath = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkStart(this.instrumentation.startGroup(MobclixInstrumentation.STARTUP), "session"), "init");
        long ts = System.currentTimeMillis();
        String sessionId = sha1(String.valueOf(this.deviceId) + ts);
        this.isTopTask = true;
        this.sessionStartTime = ts;
        this.sessionEndTime = 0;
        this.totalIdleTime = 0;
        this.isInSession = true;
        try {
            this.session.put(MC_KEY_SESSION_ID, URLEncoder.encode(sessionId, "UTF-8"));
        } catch (Exception e) {
        }
        String instrPath2 = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkFinishPath(instrPath), "config");
        this.remoteConfigSet = 0;
        new FetchRemoteConfig().execute(new String[0]);
        String instrPath3 = this.instrumentation.benchmarkFinishPath(this.instrumentation.benchmarkFinishPath(instrPath2));
    }

    private class SessionPolling extends TimerTask {
        private SessionPolling() {
        }

        /* synthetic */ SessionPolling(Mobclix mobclix, SessionPolling sessionPolling) {
            this();
        }

        public synchronized void run() {
            Mobclix.this.handleSessionStatus(isTopTask());
        }

        public boolean isTopTask() {
            try {
                return ((ActivityManager) Mobclix.this.context.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getPackageName().equals(Mobclix.this.context.getPackageName());
            } catch (Exception e) {
                return Mobclix.DEBUG;
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void handleSessionStatus(boolean r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            if (r7 == 0) goto L_0x002a
            boolean r2 = r6.isTopTask     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            if (r2 == 0) goto L_0x000d
        L_0x000b:
            monitor-exit(r6)
            return
        L_0x000d:
            boolean r2 = r6.isInSession     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            if (r2 != 0) goto L_0x001a
            android.os.Handler r2 = r6.mHandler     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            r3 = 0
            r2.sendEmptyMessage(r3)     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            goto L_0x000b
        L_0x0018:
            r2 = move-exception
            goto L_0x000b
        L_0x001a:
            long r2 = r6.totalIdleTime     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            long r4 = r6.sessionEndTime     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            long r4 = r0 - r4
            long r2 = r2 + r4
            r6.totalIdleTime = r2     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            r2 = 1
            r6.isTopTask = r2     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            goto L_0x000b
        L_0x0027:
            r2 = move-exception
            monitor-exit(r6)
            throw r2
        L_0x002a:
            boolean r2 = r6.isTopTask     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            if (r2 != 0) goto L_0x0041
            long r2 = r6.sessionEndTime     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            long r2 = r0 - r2
            int r4 = r6.idleTimeout     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            long r4 = (long) r4     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x000b
            boolean r2 = r6.isInSession     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            if (r2 == 0) goto L_0x000b
            r6.endSession()     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            goto L_0x000b
        L_0x0041:
            r6.sessionEndTime = r0     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            r2 = 0
            r6.isTopTask = r2     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            com.mobclix.android.sdk.MobclixLocation r2 = r6.location     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            r2.stopLocation()     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.Mobclix.handleSessionStatus(boolean):void");
    }

    private void endSession() {
        try {
            if (this.isInSession) {
                long sessionTime = this.sessionEndTime - this.sessionStartTime;
                if (hasPref("totalSessionTime")) {
                    try {
                        sessionTime += Long.parseLong(getPref("totalSessionTime"));
                    } catch (Exception e) {
                    }
                }
                if (hasPref("totalIdleTime")) {
                    try {
                        this.totalIdleTime += Long.parseLong(getPref("totalIdleTime"));
                    } catch (Exception e2) {
                    }
                }
                HashMap<String, String> sessionStats = new HashMap<>();
                sessionStats.put("totalSessionTime", Long.toString(sessionTime));
                sessionStats.put("totalIdleTime", Long.toString(this.totalIdleTime));
                if (this.isOfflineSession) {
                    int offlineSessions = 1;
                    if (hasPref("offlineSessions")) {
                        try {
                            offlineSessions = 1 + Integer.parseInt(getPref("offlineSessions"));
                        } catch (Exception e3) {
                        }
                    }
                    sessionStats.put("offlineSessions", Long.toString((long) offlineSessions));
                }
                addPref(sessionStats);
                this.isInSession = DEBUG;
                this.isTopTask = DEBUG;
                this.sessionStartTime = 0;
                this.sessionEndTime = 0;
                this.totalIdleTime = 0;
            }
        } catch (Exception e4) {
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        endSession();
    }

    class MobclixPermissionException extends RuntimeException {
        private static final long serialVersionUID = -2362572513974509340L;

        MobclixPermissionException(String detailMessage) {
            super(detailMessage);
        }
    }

    static String getCookieStringFromCookieManager(String url) {
        try {
            return CookieManager.getInstance().getCookie(url);
        } catch (Exception e) {
            return "";
        }
    }

    static void syncCookiesToCookieManager(CookieStore cs, String url) {
        try {
            CookieManager cookieManager = CookieManager.getInstance();
            List<Cookie> cookies = cs.getCookies();
            StringBuffer cookieStringBuffer = new StringBuffer();
            if (!cookies.isEmpty()) {
                for (int i = 0; i < cookies.size(); i++) {
                    Cookie c = (Cookie) cookies.get(i);
                    cookieStringBuffer.append(c.getName()).append("=").append(c.getValue());
                    if (c.getExpiryDate() != null) {
                        cookieStringBuffer.append("; expires=").append(new SimpleDateFormat("E, dd-MMM-yyyy HH:mm:ss").format(c.getExpiryDate())).append(" GMT");
                    }
                    if (c.getPath() != null) {
                        cookieStringBuffer.append("; path=").append(c.getPath());
                    }
                    if (c.getDomain() != null) {
                        cookieStringBuffer.append("; domain=").append(c.getDomain());
                    }
                    cookieManager.setCookie(url, cookieStringBuffer.toString());
                }
                CookieSyncManager.getInstance().sync();
                CookieSyncManager.getInstance().stopSync();
            }
        } catch (Exception e) {
        }
    }

    static class MobclixHttpClient extends DefaultHttpClient {
        HttpGet httpGet = new HttpGet(this.url);
        String url;

        public MobclixHttpClient(String u) {
            this.url = u;
            this.httpGet.setHeader("Cookie", Mobclix.getCookieStringFromCookieManager(this.url));
            this.httpGet.setHeader("User-Agent", Mobclix.controller.getUserAgent());
        }

        public HttpResponse execute() throws ClientProtocolException, IOException {
            try {
                HttpResponse httpResponse = Mobclix.super.execute(this.httpGet);
                Mobclix.syncCookiesToCookieManager(getCookieStore(), this.url);
                return httpResponse;
            } catch (Throwable th) {
                return null;
            }
        }
    }

    static class BitmapHandler extends Handler {
        protected Bitmap bmImg = null;
        protected Object state = null;

        BitmapHandler() {
        }

        public void setBitmap(Bitmap bm) {
            this.bmImg = bm;
        }

        public void setState(Object o) {
            this.state = o;
        }
    }

    static class FetchImageThread implements Runnable {
        private Bitmap bmImg;
        private BitmapHandler handler;
        private String imageUrl;

        FetchImageThread(String url, BitmapHandler h) {
            this.imageUrl = url;
            this.handler = h;
        }

        public void run() {
            try {
                HttpEntity httpEntity = new MobclixHttpClient(this.imageUrl).execute().getEntity();
                this.bmImg = BitmapFactory.decodeStream(httpEntity.getContent());
                httpEntity.consumeContent();
                this.handler.setBitmap(this.bmImg);
            } catch (Throwable th) {
            }
            this.handler.sendEmptyMessage(0);
        }
    }

    static class FetchResponseThread extends TimerTask implements Runnable {
        private Handler handler;
        private String url;

        FetchResponseThread(String u, Handler h) {
            this.url = u;
            this.handler = h;
        }

        public void run() {
            int errorCode;
            Mobclix.controller.updateSession();
            if (this.url.equals("")) {
                sendErrorCode(MobclixAdViewListener.UNAVAILABLE);
            }
            String response = "";
            BufferedReader br = null;
            try {
                HttpResponse httpResponse = new MobclixHttpClient(this.url).execute();
                HttpEntity httpEntity = httpResponse.getEntity();
                int responseCode = httpResponse.getStatusLine().getStatusCode();
                if ((responseCode != 200 && responseCode != 251) || httpEntity == null) {
                    switch (responseCode) {
                        case 251:
                            String sub = httpResponse.getFirstHeader("X-Mobclix-Suballocation").getValue();
                            if (sub == null) {
                                errorCode = MobclixAdViewListener.UNAVAILABLE;
                            } else {
                                errorCode = Integer.parseInt(sub);
                            }
                            sendErrorCode(errorCode);
                            break;
                        default:
                            errorCode = MobclixAdViewListener.UNAVAILABLE;
                            sendErrorCode(errorCode);
                            break;
                    }
                } else {
                    BufferedReader br2 = new BufferedReader(new InputStreamReader(httpEntity.getContent()), 8000);
                    try {
                        for (String tmp = br2.readLine(); tmp != null; tmp = br2.readLine()) {
                            response = String.valueOf(response) + tmp;
                        }
                        httpEntity.consumeContent();
                        if (!response.equals("")) {
                            Message msg = new Message();
                            Bundle bundle = new Bundle();
                            bundle.putString("type", "success");
                            bundle.putString("response", response);
                            msg.setData(bundle);
                            this.handler.sendMessage(msg);
                            br = br2;
                        } else {
                            br = br2;
                        }
                    } catch (Throwable th) {
                        th = th;
                        br = br2;
                        try {
                            br.close();
                        } catch (Exception e) {
                        }
                        throw th;
                    }
                }
                try {
                    br.close();
                    return;
                } catch (Exception e2) {
                    return;
                }
            } catch (Throwable th2) {
            }
            try {
                sendErrorCode(MobclixAdViewListener.UNAVAILABLE);
                try {
                    br.close();
                } catch (Exception e3) {
                }
            } catch (Throwable th3) {
                th = th3;
                br.close();
                throw th;
            }
        }

        /* access modifiers changed from: package-private */
        public void setUrl(String u) {
            this.url = u;
        }

        private void sendErrorCode(int errorCode) {
            Message msg = new Message();
            Bundle bundle = new Bundle();
            bundle.putString("type", "failure");
            bundle.putInt("errorCode", errorCode);
            msg.setData(bundle);
            this.handler.sendMessage(msg);
        }
    }

    static class ObjectOnClickListener implements DialogInterface.OnClickListener {
        Object obj1;
        Object obj2;
        Object obj3;

        public ObjectOnClickListener(Object o) {
            this.obj1 = o;
        }

        public ObjectOnClickListener(Object o, Object o2) {
            this.obj1 = o;
            this.obj2 = o2;
        }

        public ObjectOnClickListener(Object o, Object o2, Object o3) {
            this.obj1 = o;
            this.obj2 = o2;
            this.obj3 = o3;
        }

        public void onClick(DialogInterface dialog, int which) {
        }
    }
}
