package com.mobclix.android.sdk;

import android.app.Activity;
import java.util.Map;

public class MobclixDemographics {
    public static String AreaCode = "dac";
    public static String Birthdate = "dbd";
    public static String City = "dci";
    public static String Country = "dco";
    public static String DMACode = "ddc";
    public static String DatingGender = "ddg";
    public static String Education = "dec";
    public static int EducationBachelorsDegree = 4;
    public static int EducationDoctoralDegree = 6;
    public static int EducationHighSchool = 1;
    public static int EducationInCollege = 3;
    public static int EducationMastersDegree = 5;
    public static int EducationSomeCollege = 2;
    public static int EducationUnknown = 0;
    public static String Ethnicity = "den";
    public static int EthnicityAsian = 2;
    public static int EthnicityBlack = 3;
    public static int EthnicityHispanic = 4;
    public static int EthnicityMixed = 1;
    public static int EthnicityNativeAmerican = 5;
    public static int EthnicityUnknown = 0;
    public static int EthnicityWhite = 6;
    public static String Gender = "dg";
    public static int GenderBoth = 3;
    public static int GenderFemale = 2;
    public static int GenderMale = 1;
    public static int GenderUnknown = 0;
    public static String Income = "dic";
    public static int MaritalMarried = 3;
    public static int MaritalSingleAvailable = 1;
    public static int MaritalSingleUnavailable = 2;
    public static String MaritalStatus = "dms";
    public static int MaritalUnknown = 0;
    public static String MetroCode = "dmc";
    public static String PostalCode = "dpo";
    public static String Region = "drg";
    public static String Religion = "drl";
    public static int ReligionBuddhism = 1;
    public static int ReligionChristianity = 2;
    public static int ReligionHinduism = 3;
    public static int ReligionIslam = 4;
    public static int ReligionJudaism = 5;
    public static int ReligionOther = 7;
    public static int ReligionUnaffiliated = 6;
    public static int ReligionUnknown = 0;
    private static String TAG = "mobclixDemographics";
    static Mobclix controller = Mobclix.getInstance();

    public static void sendDemographics(Activity a, Map<String, Object> d) {
        sendDemographics(a, d, null);
    }

    /* JADX INFO: Multiple debug info for r2v36 java.lang.String: [D('isValid' boolean), D('temp' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r2v42 java.lang.String: [D('isValid' boolean), D('temp' java.lang.String)] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x01a9  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x00af A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void sendDemographics(android.app.Activity r11, java.util.Map<java.lang.String, java.lang.Object> r12, com.mobclix.android.sdk.MobclixFeedback.Listener r13) {
        /*
            if (r12 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            com.mobclix.android.sdk.Mobclix.onCreate(r11)
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            java.lang.String r1 = com.mobclix.android.sdk.MobclixDemographics.Birthdate
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r4.put(r1, r2)
            java.lang.String r1 = com.mobclix.android.sdk.MobclixDemographics.Education
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r4.put(r1, r2)
            java.lang.String r1 = com.mobclix.android.sdk.MobclixDemographics.Ethnicity
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r4.put(r1, r2)
            java.lang.String r1 = com.mobclix.android.sdk.MobclixDemographics.Gender
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r4.put(r1, r2)
            java.lang.String r1 = com.mobclix.android.sdk.MobclixDemographics.DatingGender
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r4.put(r1, r2)
            java.lang.String r1 = com.mobclix.android.sdk.MobclixDemographics.Income
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r4.put(r1, r2)
            java.lang.String r1 = com.mobclix.android.sdk.MobclixDemographics.MaritalStatus
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r4.put(r1, r2)
            java.lang.String r1 = com.mobclix.android.sdk.MobclixDemographics.Religion
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r4.put(r1, r2)
            java.lang.String r1 = com.mobclix.android.sdk.MobclixDemographics.AreaCode
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r4.put(r1, r2)
            java.lang.String r1 = com.mobclix.android.sdk.MobclixDemographics.City
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r4.put(r1, r2)
            java.lang.String r1 = com.mobclix.android.sdk.MobclixDemographics.Country
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r4.put(r1, r2)
            java.lang.String r1 = com.mobclix.android.sdk.MobclixDemographics.DMACode
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r4.put(r1, r2)
            java.lang.String r1 = com.mobclix.android.sdk.MobclixDemographics.MetroCode
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r4.put(r1, r2)
            java.lang.String r1 = com.mobclix.android.sdk.MobclixDemographics.PostalCode
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r4.put(r1, r2)
            java.lang.String r1 = com.mobclix.android.sdk.MobclixDemographics.Region
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r4.put(r1, r2)
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            r2 = 0
            java.util.Set r3 = r12.keySet()
            java.util.Iterator r9 = r3.iterator()
        L_0x00af:
            boolean r2 = r9.hasNext()
            if (r2 != 0) goto L_0x0190
            com.mobclix.android.sdk.Mobclix r12 = com.mobclix.android.sdk.MobclixDemographics.controller
            java.lang.String r3 = r12.getFeedbackServer()
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.String r12 = "p=android&t=demo"
            r2.append(r12)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r12 = "&a="
            java.lang.StringBuffer r12 = r2.append(r12)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            com.mobclix.android.sdk.Mobclix r4 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r4 = r4.getApplicationId()     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r5 = "UTF-8"
            java.lang.String r4 = java.net.URLEncoder.encode(r4, r5)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            r12.append(r4)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r12 = "&v="
            java.lang.StringBuffer r12 = r2.append(r12)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            com.mobclix.android.sdk.Mobclix r4 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r4 = r4.getApplicationVersion()     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r5 = "UTF-8"
            java.lang.String r4 = java.net.URLEncoder.encode(r4, r5)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            r12.append(r4)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r12 = "&m="
            java.lang.StringBuffer r12 = r2.append(r12)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            com.mobclix.android.sdk.Mobclix r4 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r4 = r4.getMobclixVersion()     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r5 = "UTF-8"
            java.lang.String r4 = java.net.URLEncoder.encode(r4, r5)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            r12.append(r4)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r12 = "&d="
            java.lang.StringBuffer r12 = r2.append(r12)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            com.mobclix.android.sdk.Mobclix r4 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r4 = r4.getDeviceId()     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r5 = "UTF-8"
            java.lang.String r4 = java.net.URLEncoder.encode(r4, r5)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            r12.append(r4)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r12 = "&dt="
            java.lang.StringBuffer r12 = r2.append(r12)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            com.mobclix.android.sdk.Mobclix r4 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r4 = r4.getDeviceModel()     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r5 = "UTF-8"
            java.lang.String r4 = java.net.URLEncoder.encode(r4, r5)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            r12.append(r4)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r12 = "&os="
            java.lang.StringBuffer r12 = r2.append(r12)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            com.mobclix.android.sdk.Mobclix r4 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r4 = r4.getAndroidVersion()     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r5 = "UTF-8"
            java.lang.String r4 = java.net.URLEncoder.encode(r4, r5)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            r12.append(r4)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            com.mobclix.android.sdk.Mobclix r12 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r12 = r12.getGPS()     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r4 = "null"
            boolean r4 = r12.equals(r4)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            if (r4 != 0) goto L_0x016f
            java.lang.String r4 = "UTF-8"
            java.lang.String r12 = java.net.URLEncoder.encode(r12, r4)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r4 = "&gps="
            java.lang.StringBuffer r4 = r2.append(r4)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            r5 = 0
            r6 = 48
            int r7 = r12.length()     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            int r6 = java.lang.Math.min(r6, r7)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r5 = r12.substring(r5, r6)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            r4.append(r5)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
        L_0x016f:
            java.util.Set r12 = r1.keySet()     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.util.Iterator r4 = r12.iterator()     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
        L_0x0177:
            boolean r12 = r4.hasNext()     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            if (r12 != 0) goto L_0x02b6
            java.lang.Thread r12 = new java.lang.Thread
            com.mobclix.android.sdk.MobclixUtility$POSTThread r1 = new com.mobclix.android.sdk.MobclixUtility$POSTThread
            java.lang.String r2 = r2.toString()
            r1.<init>(r3, r2, r11, r13)
            r12.<init>(r1)
            r12.run()
            goto L_0x0002
        L_0x0190:
            java.lang.Object r3 = r9.next()
            java.lang.String r3 = (java.lang.String) r3
            java.lang.String r6 = ""
            r2 = 0
            java.lang.Object r7 = r12.get(r3)
            if (r7 == 0) goto L_0x02e9
            java.lang.Object r5 = r4.get(r3)
            if (r5 != 0) goto L_0x01ae
            r2 = 0
            r5 = r6
        L_0x01a7:
            if (r2 == 0) goto L_0x00af
            r1.put(r3, r5)
            goto L_0x00af
        L_0x01ae:
            java.lang.Class r5 = r7.getClass()
            java.lang.Class<java.util.Date> r8 = java.util.Date.class
            if (r5 != r8) goto L_0x01cd
            java.lang.String r5 = com.mobclix.android.sdk.MobclixDemographics.Birthdate
            boolean r5 = r3.equals(r5)
            if (r5 == 0) goto L_0x02e9
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat
            java.lang.String r5 = "yyyyMMdd"
            r2.<init>(r5)
            java.util.Date r7 = (java.util.Date) r7
            java.lang.String r5 = r2.format(r7)
            r2 = 1
            goto L_0x01a7
        L_0x01cd:
            java.lang.Class r5 = r7.getClass()
            java.lang.Class<java.lang.String> r8 = java.lang.String.class
            if (r5 != r8) goto L_0x022a
            java.lang.String r5 = com.mobclix.android.sdk.MobclixDemographics.City
            boolean r5 = r3.equals(r5)
            if (r5 != 0) goto L_0x0215
            java.lang.String r5 = com.mobclix.android.sdk.MobclixDemographics.Country
            boolean r5 = r3.equals(r5)
            if (r5 != 0) goto L_0x0215
            java.lang.String r5 = com.mobclix.android.sdk.MobclixDemographics.PostalCode
            boolean r5 = r3.equals(r5)
            if (r5 != 0) goto L_0x0215
            java.lang.String r5 = com.mobclix.android.sdk.MobclixDemographics.Region
            boolean r5 = r3.equals(r5)
            if (r5 != 0) goto L_0x0215
            java.lang.String r5 = com.mobclix.android.sdk.MobclixDemographics.AreaCode
            boolean r5 = r3.equals(r5)
            if (r5 != 0) goto L_0x0215
            java.lang.String r5 = com.mobclix.android.sdk.MobclixDemographics.DMACode
            boolean r5 = r3.equals(r5)
            if (r5 != 0) goto L_0x0215
            java.lang.String r5 = com.mobclix.android.sdk.MobclixDemographics.Income
            boolean r5 = r3.equals(r5)
            if (r5 != 0) goto L_0x0215
            java.lang.String r5 = com.mobclix.android.sdk.MobclixDemographics.MetroCode
            boolean r5 = r3.equals(r5)
            if (r5 == 0) goto L_0x02e9
        L_0x0215:
            r0 = r7
            java.lang.String r0 = (java.lang.String) r0
            r2 = r0
            java.lang.String r5 = r2.trim()
            java.lang.String r2 = ""
            boolean r2 = r5.equals(r2)
            if (r2 == 0) goto L_0x0227
            r2 = 0
            goto L_0x01a7
        L_0x0227:
            r2 = 1
            goto L_0x01a7
        L_0x022a:
            java.lang.Class r5 = r7.getClass()
            java.lang.Class<java.lang.Integer> r8 = java.lang.Integer.class
            if (r5 != r8) goto L_0x02e9
            java.lang.String r5 = com.mobclix.android.sdk.MobclixDemographics.AreaCode
            boolean r5 = r3.equals(r5)
            if (r5 != 0) goto L_0x0252
            java.lang.String r5 = com.mobclix.android.sdk.MobclixDemographics.DMACode
            boolean r5 = r3.equals(r5)
            if (r5 != 0) goto L_0x0252
            java.lang.String r5 = com.mobclix.android.sdk.MobclixDemographics.Income
            boolean r5 = r3.equals(r5)
            if (r5 != 0) goto L_0x0252
            java.lang.String r5 = com.mobclix.android.sdk.MobclixDemographics.MetroCode
            boolean r5 = r3.equals(r5)
            if (r5 == 0) goto L_0x026a
        L_0x0252:
            java.lang.Integer r7 = (java.lang.Integer) r7
            java.lang.String r2 = r7.toString()
            java.lang.String r5 = r2.trim()
            java.lang.String r2 = ""
            boolean r2 = r5.equals(r2)
            if (r2 == 0) goto L_0x0267
            r2 = 0
            goto L_0x01a7
        L_0x0267:
            r2 = 1
            goto L_0x01a7
        L_0x026a:
            r5 = 0
            java.lang.String r8 = com.mobclix.android.sdk.MobclixDemographics.Gender
            boolean r8 = r3.equals(r8)
            if (r8 == 0) goto L_0x0288
            r5 = 2
        L_0x0274:
            r0 = r7
            java.lang.Integer r0 = (java.lang.Integer) r0
            r8 = r0
            int r8 = r8.intValue()
            r10 = 1
            if (r10 > r8) goto L_0x02e9
            if (r8 > r5) goto L_0x02e9
            java.lang.String r5 = r7.toString()
            r2 = 1
            goto L_0x01a7
        L_0x0288:
            java.lang.String r8 = com.mobclix.android.sdk.MobclixDemographics.DatingGender
            boolean r8 = r3.equals(r8)
            if (r8 != 0) goto L_0x0298
            java.lang.String r8 = com.mobclix.android.sdk.MobclixDemographics.MaritalStatus
            boolean r8 = r3.equals(r8)
            if (r8 == 0) goto L_0x029a
        L_0x0298:
            r5 = 3
            goto L_0x0274
        L_0x029a:
            java.lang.String r8 = com.mobclix.android.sdk.MobclixDemographics.Education
            boolean r8 = r3.equals(r8)
            if (r8 != 0) goto L_0x02aa
            java.lang.String r8 = com.mobclix.android.sdk.MobclixDemographics.Ethnicity
            boolean r8 = r3.equals(r8)
            if (r8 == 0) goto L_0x02ac
        L_0x02aa:
            r5 = 6
            goto L_0x0274
        L_0x02ac:
            java.lang.String r8 = com.mobclix.android.sdk.MobclixDemographics.Religion
            boolean r8 = r3.equals(r8)
            if (r8 == 0) goto L_0x0274
            r5 = 7
            goto L_0x0274
        L_0x02b6:
            java.lang.Object r12 = r4.next()     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r6 = "&"
            r5.<init>(r6)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.StringBuilder r5 = r5.append(r12)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r6 = "="
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r5 = r5.toString()     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.StringBuffer r5 = r2.append(r5)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.Object r12 = r1.get(r12)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            java.lang.String r6 = "UTF-8"
            java.lang.String r12 = java.net.URLEncoder.encode(r12, r6)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            r5.append(r12)     // Catch:{ UnsupportedEncodingException -> 0x02e6 }
            goto L_0x0177
        L_0x02e6:
            r11 = move-exception
            goto L_0x0002
        L_0x02e9:
            r5 = r6
            goto L_0x01a7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixDemographics.sendDemographics(android.app.Activity, java.util.Map, com.mobclix.android.sdk.MobclixFeedback$Listener):void");
    }
}
