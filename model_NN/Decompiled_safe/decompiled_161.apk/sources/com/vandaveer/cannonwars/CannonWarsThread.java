package com.vandaveer.cannonwars;

import android.graphics.Canvas;

class CannonWarsThread extends Thread {
    private CannonWarsPanel _panel;
    public boolean _pause = false;
    private boolean _run = false;
    long beforeTime = 0;
    private long delay = 13;
    long sleepTime = 0;

    public CannonWarsThread(CannonWarsPanel panel) {
        this._panel = panel;
    }

    public void setRunning(boolean run) {
        this._run = run;
    }

    public boolean isRunning() {
        return this._run;
    }

    public void run() {
        while (this._run) {
            if (!this._pause) {
                Canvas c = null;
                try {
                    c = this._panel.getHolder().lockCanvas(null);
                    synchronized (this._panel.getHolder()) {
                        this._panel.updatePhysics();
                        this._panel.checkForWinners();
                        this.beforeTime = System.currentTimeMillis();
                        this._panel.onDraw(c);
                        this.sleepTime = this.delay - (System.currentTimeMillis() - this.beforeTime);
                        if (this.sleepTime > 0) {
                            sleepFor(this.sleepTime);
                        }
                    }
                } finally {
                    if (c != null) {
                        this._panel.getHolder().unlockCanvasAndPost(c);
                    }
                }
            }
        }
    }

    public void Pause(boolean pauseGame) {
        this._pause = pauseGame;
    }

    private void sleepFor(long sleep_time) {
        try {
            Thread.sleep(sleep_time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
