package com.vandaveer.cannonwars;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

public class Keypad extends Dialog {
    protected static final String TAG = "CannonWarsPro";
    private final CannonWars Act;
    private int gameLevel;
    private final View[] keys = new View[12];

    public Keypad(Context context, CannonWars tsAct, int level) {
        super(context);
        this.Act = tsAct;
        this.gameLevel = level;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.keypad);
        findViews();
        setListeners();
    }

    /* access modifiers changed from: private */
    public void returnResult(int level) {
        this.Act.startGame(level);
        dismiss();
    }

    private void findViews() {
        findViewById(R.id.keypad);
        this.keys[0] = findViewById(R.id.keypad_1);
        this.keys[1] = findViewById(R.id.keypad_2);
        this.keys[2] = findViewById(R.id.keypad_3);
        this.keys[3] = findViewById(R.id.keypad_4);
        this.keys[4] = findViewById(R.id.keypad_5);
        this.keys[5] = findViewById(R.id.keypad_6);
        this.keys[6] = findViewById(R.id.keypad_7);
        this.keys[7] = findViewById(R.id.keypad_8);
        this.keys[8] = findViewById(R.id.keypad_9);
        this.keys[9] = findViewById(R.id.keypad_10);
        this.keys[10] = findViewById(R.id.keypad_11);
        this.keys[11] = findViewById(R.id.keypad_12);
        for (int i = 1; i < this.keys.length + 1; i++) {
            if (this.gameLevel >= i) {
                this.keys[i - 1].setEnabled(true);
            } else {
                this.keys[i - 1].setEnabled(false);
            }
        }
    }

    private void setListeners() {
        for (int i = 0; i < this.keys.length; i++) {
            final int t = i + 1;
            this.keys[i].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Keypad.this.returnResult(t);
                }
            });
        }
    }
}
