package com.vandaveer.cannonwars;

import android.content.Context;
import android.net.ParseException;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

public final class HighScores {
    /* JADX INFO: Multiple debug info for r5v6 java.lang.String: [D('context' android.content.Context), D('url' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v2 org.apache.http.params.BasicHttpParams: [D('httpParameters' org.apache.http.params.HttpParams), D('queryString' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v3 org.apache.http.protocol.BasicHttpContext: [D('httpParameters' org.apache.http.params.HttpParams), D('localContext' org.apache.http.protocol.HttpContext)] */
    /* JADX INFO: Multiple debug info for r5v15 org.apache.http.HttpResponse: [D('httpClient' org.apache.http.client.HttpClient), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r6v4 java.lang.String: [D('rawScores' java.lang.String), D('localContext' org.apache.http.protocol.HttpContext)] */
    /* JADX INFO: Multiple debug info for r5v21 java.lang.String: [D('oAes' com.vandaveer.cannonwars.Crypto), D('rawScores' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v22 java.lang.String: [D('scores' java.lang.String), D('rawScores' java.lang.String)] */
    public static String getServerResponse(Context context, String queryString) {
        try {
            HttpGet httpGet = new HttpGet(String.valueOf(context.getString(R.string.highScoreServer)) + queryString);
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, 3000);
            HttpConnectionParams.setSoTimeout(httpParameters, 5000);
            HttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpContext localContext = new BasicHttpContext();
            httpGet.getParams().setBooleanParameter("http.protocol.expect-continue", false);
            HttpContext localContext2 = getResponseBody(httpClient.execute(httpGet, localContext));
            return new Crypto().decryptString(localContext2.substring(localContext2.indexOf("<record>") + 8, localContext2.indexOf("</record>")));
        } catch (Exception ex) {
            Log.d("Air Defense", new StringBuilder().append(ex.getStackTrace()).toString());
            return null;
        }
    }

    /* JADX INFO: finally extract failed */
    public static String _getResponseBody(HttpEntity entity) throws IOException, ParseException {
        if (entity == null) {
            throw new IllegalArgumentException("HTTP entity may not be null");
        }
        InputStream instream = entity.getContent();
        if (instream == null) {
            return "";
        }
        if (entity.getContentLength() > 2147483647L) {
            throw new IllegalArgumentException("HTTP entity too large to be buffered in memory");
        }
        String charset = getContentCharSet(entity);
        if (charset == null) {
            charset = "ISO-8859-1";
        }
        Reader reader = new InputStreamReader(instream, charset);
        StringBuilder buffer = new StringBuilder();
        try {
            char[] tmp = new char[1024];
            while (true) {
                int l = reader.read(tmp);
                if (l == -1) {
                    reader.close();
                    return buffer.toString();
                }
                buffer.append(tmp, 0, l);
            }
        } catch (Throwable th) {
            reader.close();
            throw th;
        }
    }

    public static String getContentCharSet(HttpEntity entity) throws ParseException {
        NameValuePair param;
        if (entity == null) {
            throw new IllegalArgumentException("HTTP entity may not be null");
        } else if (entity.getContentType() == null) {
            return null;
        } else {
            HeaderElement[] values = entity.getContentType().getElements();
            if (values.length <= 0 || (param = values[0].getParameterByName("charset")) == null) {
                return null;
            }
            return param.getValue();
        }
    }

    public static String getResponseBody(HttpResponse response) {
        HttpEntity entity = null;
        try {
            entity = response.getEntity();
            return _getResponseBody(entity);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            if (entity == null) {
                return null;
            }
            try {
                entity.consumeContent();
                return null;
            } catch (IOException e3) {
                return null;
            }
        }
    }

    public static void postServerQueryString(Context context, String queryString) {
        try {
            HttpGet httpGet = new HttpGet(String.valueOf(context.getString(R.string.highScoreServer)) + queryString);
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, 3000);
            HttpConnectionParams.setSoTimeout(httpParameters, 5000);
            HttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpContext localContext = new BasicHttpContext();
            httpGet.getParams().setBooleanParameter("http.protocol.expect-continue", false);
            httpClient.execute(httpGet, localContext);
        } catch (Exception e) {
        }
    }
}
