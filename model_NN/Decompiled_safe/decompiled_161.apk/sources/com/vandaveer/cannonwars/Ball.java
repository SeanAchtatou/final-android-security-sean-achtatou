package com.vandaveer.cannonwars;

public class Ball {
    public float locationX = 0.0f;
    public float locationY = 0.0f;
    public float radius = 0.0f;
    public float speedX = 0.0f;
    public float speedY = 0.0f;

    public Ball(float r) {
        this.radius = r;
    }
}
