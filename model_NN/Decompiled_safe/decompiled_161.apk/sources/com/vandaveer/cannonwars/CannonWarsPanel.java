package com.vandaveer.cannonwars;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.SoundPool;
import android.net.Uri;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;
import com.vandaveer.cannonwars.Graphic;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

public class CannonWarsPanel extends SurfaceView implements SurfaceHolder.Callback {
    static final String PRO_VERSION = "market://details?id=com.vandaveer.cannonwars_pro";
    private static float SCALE_HEIGHT = 320.0f;
    private static float SCALE_WIDTH = 480.0f;
    private static float fireDisplayTime = 300.0f;
    private static int playerTouchedArrowPadding = 20;
    private final int ARROW_TIME_DELAY = 200;
    private Map<Integer, Bitmap> _bitmapCache = new HashMap();
    private SoundPool _soundPool;
    private CannonWarsThread _thread;
    float archAdjustMent;
    private Graphic arrowD;
    float arrowDX;
    private Graphic arrowL;
    float arrowLX;
    private Graphic arrowR;
    float arrowRX;
    private float arrowRightLeftY;
    private Graphic arrowU;
    float arrowUX;
    private float arrowUpDownY;
    private Graphic background;
    private Ball ball;
    private float ballPaddingX;
    private float ballPaddingY;
    int ballSpeed = 0;
    float ballXPos;
    float ballYPos;
    private ArrayList<Graphic> bldFireL = new ArrayList<>();
    private ArrayList<Graphic> bldFireR = new ArrayList<>();
    private Bitmap bmBldFireL;
    private Bitmap bmBldFireR;
    private Bitmap bmBombL;
    private Bitmap bmBombR;
    private Bitmap bmBuilding;
    private Bitmap bmExp0;
    private Bitmap bmExp1;
    private Bitmap bmExp2;
    private Bitmap bmpArrowD;
    private Bitmap bmpArrowL;
    private Bitmap bmpArrowR;
    private Bitmap bmpArrowU;
    private Bitmap bmpExit;
    private Bitmap bmpFire;
    private Bitmap bmpMute;
    private Bitmap bmpPlaneL;
    private Bitmap bmpPlaneR;
    private Bitmap bmpRedArrowD;
    private Bitmap bmpRedArrowL;
    private Bitmap bmpRedArrowR;
    private Bitmap bmpRedArrowU;
    private Bitmap bmpRedFire;
    private Bitmap bmpUnMute;
    private float bombXSpeed;
    private float bombYSpeed;
    private ArrayList<Graphic> bombs = new ArrayList<>();
    float buildingHeight;
    float buildingWidth;
    private ArrayList<Graphic> buildings = new ArrayList<>();
    int cannonHeight;
    float cannonHitOffset;
    int cannonWidth;
    private int chanceOfBombDrop;
    private int chanceOfPlane;
    private Graphic computerCannon;
    float computerCannonXPos;
    float computerCannonYPos;
    boolean computerFireCannon;
    private boolean downArrowBeingPushed;
    private float exitX;
    private float exitY;
    private long explosionDisplayTime = 50;
    private ArrayList<Graphic> explosions = new ArrayList<>();
    boolean fireCannon;
    private float fireX;
    private float fireY;
    public int gameLevel;
    private boolean gameOver = false;
    private float groundY;
    int height = 0;
    float heightDisplayX;
    float heightSpeedDisplayY;
    float highScoreY;
    public int highscore;
    private Graphic imgExit;
    private Graphic imgFire;
    private Graphic imgMute;
    private long lastArrowTime = -1;
    float lastComputerBallX;
    float lastComputerBallY;
    private long lastTimePlayerCannonFired;
    private boolean leftArrowBeingPushed;
    /* access modifiers changed from: private */
    public int level;
    public int lowestHighScore = 0;
    float maxBuildingHeight;
    private int maxBuildings;
    private float maxPlaneSpeed;
    public String message;
    private int minBallHeight;
    float minBuildingHeight;
    private float minPlaneSpeed;
    private float minPlaneY;
    private float muteX;
    private float muteY;
    private float offScreenX;
    float planeHeight;
    final float planeIncrementSpeed = 0.2f;
    float planeWidth;
    private ArrayList<Graphic> planes = new ArrayList<>();
    public boolean playSound = true;
    private Graphic playerCannon;
    private Graphic playerCannonFire;
    float playerCannonXPos;
    float playerCannonYPos;
    private boolean rightArrowBeingPushed;
    public int score = 0;
    float scoreTextHeight;
    float scoreX;
    float scoreY;
    private int screenHeight;
    private int screenWidth;
    private int sndBuildingHit = 0;
    private int sndCannonFire = 0;
    private int sndCannonHit = 0;
    private int sndGroundHit = 0;
    float speedDisplayX;
    float toolBarHeight;
    private int totPlaneBombsDroped = 0;
    private int totPlanes = 1;
    private boolean upArrowBeingPushed;
    float zArch;

    public CannonWarsPanel(Context context, int gameLevel2) {
        super(context);
        fillBitmapCache();
        requestFocus();
        setFocusableInTouchMode(true);
        loadSounds();
        getHolder().addCallback(this);
        this._thread = new CannonWarsThread(this);
        setFocusable(true);
        this.level = gameLevel2;
    }

    private void fillBitmapCache() {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.planel), BitmapFactory.decodeResource(getResources(), R.drawable.planel, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.planer), BitmapFactory.decodeResource(getResources(), R.drawable.planer, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.bombl), BitmapFactory.decodeResource(getResources(), R.drawable.bombl, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.bombr), BitmapFactory.decodeResource(getResources(), R.drawable.bombr, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.bg), BitmapFactory.decodeResource(getResources(), R.drawable.bg, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.cannonr), BitmapFactory.decodeResource(getResources(), R.drawable.cannonr, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.cannonl), BitmapFactory.decodeResource(getResources(), R.drawable.cannonl, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.b1), BitmapFactory.decodeResource(getResources(), R.drawable.b1, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.exp0), BitmapFactory.decodeResource(getResources(), R.drawable.exp0, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.exp1), BitmapFactory.decodeResource(getResources(), R.drawable.exp1, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.exp2), BitmapFactory.decodeResource(getResources(), R.drawable.exp2, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.arrowd), BitmapFactory.decodeResource(getResources(), R.drawable.arrowd, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.arrowu), BitmapFactory.decodeResource(getResources(), R.drawable.arrowu, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.arrowl), BitmapFactory.decodeResource(getResources(), R.drawable.arrowl, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.arrowr), BitmapFactory.decodeResource(getResources(), R.drawable.arrowr, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.redarrowd), BitmapFactory.decodeResource(getResources(), R.drawable.redarrowd, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.redarrowu), BitmapFactory.decodeResource(getResources(), R.drawable.redarrowu, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.redarrowl), BitmapFactory.decodeResource(getResources(), R.drawable.redarrowl, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.redarrowr), BitmapFactory.decodeResource(getResources(), R.drawable.redarrowr, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.playercannonfire), BitmapFactory.decodeResource(getResources(), R.drawable.playercannonfire, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.fire), BitmapFactory.decodeResource(getResources(), R.drawable.fire, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.redfire), BitmapFactory.decodeResource(getResources(), R.drawable.redfire, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.firel2), BitmapFactory.decodeResource(getResources(), R.drawable.firel2, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.firer2), BitmapFactory.decodeResource(getResources(), R.drawable.firer2, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.exit), BitmapFactory.decodeResource(getResources(), R.drawable.exit, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.mute), BitmapFactory.decodeResource(getResources(), R.drawable.mute, opt));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.unmute), BitmapFactory.decodeResource(getResources(), R.drawable.unmute, opt));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r12) {
        /*
            r11 = this;
            r10 = 100
            r9 = 0
            r8 = 1
            android.view.SurfaceHolder r7 = r11.getHolder()
            monitor-enter(r7)
            java.lang.String r0 = r11.message     // Catch:{ all -> 0x014c }
            if (r0 != 0) goto L_0x0011
            com.vandaveer.cannonwars.Graphic r0 = r11.background     // Catch:{ all -> 0x014c }
            if (r0 != 0) goto L_0x0014
        L_0x0011:
            monitor-exit(r7)     // Catch:{ all -> 0x014c }
            r0 = r8
        L_0x0013:
            return r0
        L_0x0014:
            int r6 = r12.getAction()     // Catch:{ all -> 0x014c }
            float r2 = r12.getX()     // Catch:{ all -> 0x014c }
            float r3 = r12.getY()     // Catch:{ all -> 0x014c }
            r0 = 0
            r11.PauseGame(r0)     // Catch:{ all -> 0x014c }
            switch(r6) {
                case 0: goto L_0x002a;
                case 1: goto L_0x0120;
                default: goto L_0x0027;
            }     // Catch:{ all -> 0x014c }
        L_0x0027:
            monitor-exit(r7)     // Catch:{ all -> 0x014c }
            r0 = r8
            goto L_0x0013
        L_0x002a:
            com.vandaveer.cannonwars.Graphic r1 = r11.imgExit     // Catch:{ all -> 0x014c }
            r4 = 0
            r5 = 0
            r0 = r11
            boolean r0 = r0.PlayerTouchedGraphic(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x014c }
            if (r0 == 0) goto L_0x003b
            r11.promptToExitGame()     // Catch:{ all -> 0x014c }
            monitor-exit(r7)     // Catch:{ all -> 0x014c }
            r0 = r8
            goto L_0x0013
        L_0x003b:
            com.vandaveer.cannonwars.Graphic r1 = r11.imgMute     // Catch:{ all -> 0x014c }
            r4 = 0
            r5 = 0
            r0 = r11
            boolean r0 = r0.PlayerTouchedGraphic(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x014c }
            if (r0 == 0) goto L_0x0053
            boolean r0 = r11.playSound     // Catch:{ all -> 0x014c }
            if (r0 == 0) goto L_0x0051
            r0 = r9
        L_0x004b:
            r11.ToggleSound(r0)     // Catch:{ all -> 0x014c }
            monitor-exit(r7)     // Catch:{ all -> 0x014c }
            r0 = r8
            goto L_0x0013
        L_0x0051:
            r0 = r8
            goto L_0x004b
        L_0x0053:
            com.vandaveer.cannonwars.Graphic r1 = r11.imgFire     // Catch:{ all -> 0x014c }
            int r4 = com.vandaveer.cannonwars.CannonWarsPanel.playerTouchedArrowPadding     // Catch:{ all -> 0x014c }
            int r5 = com.vandaveer.cannonwars.CannonWarsPanel.playerTouchedArrowPadding     // Catch:{ all -> 0x014c }
            r0 = r11
            boolean r0 = r0.PlayerTouchedGraphic(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x014c }
            if (r0 == 0) goto L_0x0092
            com.vandaveer.cannonwars.Graphic r0 = r11.imgFire     // Catch:{ all -> 0x014c }
            android.graphics.Bitmap r1 = r11.bmpRedFire     // Catch:{ all -> 0x014c }
            r0.bitmap = r1     // Catch:{ all -> 0x014c }
            boolean r0 = r11.fireCannon     // Catch:{ all -> 0x014c }
            if (r0 != 0) goto L_0x008f
            boolean r0 = r11.computerFireCannon     // Catch:{ all -> 0x014c }
            if (r0 != 0) goto L_0x008f
            float r0 = r11.offScreenX     // Catch:{ all -> 0x014c }
            r11.ballXPos = r0     // Catch:{ all -> 0x014c }
            int r0 = r11.ballSpeed     // Catch:{ all -> 0x014c }
            if (r0 != 0) goto L_0x007f
            java.lang.String r0 = "Speed must be greater than zero. Touch the arrows buttons below to increase speed and height."
            r1 = 0
            r11.showToastMessage(r0, r1)     // Catch:{ all -> 0x014c }
            monitor-exit(r7)     // Catch:{ all -> 0x014c }
            r0 = r8
            goto L_0x0013
        L_0x007f:
            int r0 = r11.height     // Catch:{ all -> 0x014c }
            if (r0 != 0) goto L_0x008c
            java.lang.String r0 = "Height must be greater than zero. Touch the arrow buttons below to increase speed and height."
            r1 = 0
            r11.showToastMessage(r0, r1)     // Catch:{ all -> 0x014c }
            monitor-exit(r7)     // Catch:{ all -> 0x014c }
            r0 = r8
            goto L_0x0013
        L_0x008c:
            r0 = 1
            r11.fireCannon = r0     // Catch:{ all -> 0x014c }
        L_0x008f:
            monitor-exit(r7)     // Catch:{ all -> 0x014c }
            r0 = r8
            goto L_0x0013
        L_0x0092:
            com.vandaveer.cannonwars.Graphic r1 = r11.arrowL     // Catch:{ all -> 0x014c }
            int r4 = com.vandaveer.cannonwars.CannonWarsPanel.playerTouchedArrowPadding     // Catch:{ all -> 0x014c }
            int r5 = com.vandaveer.cannonwars.CannonWarsPanel.playerTouchedArrowPadding     // Catch:{ all -> 0x014c }
            r0 = r11
            boolean r0 = r0.PlayerTouchedGraphic(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x014c }
            if (r0 == 0) goto L_0x00b5
            com.vandaveer.cannonwars.Graphic r0 = r11.arrowL     // Catch:{ all -> 0x014c }
            android.graphics.Bitmap r1 = r11.bmpRedArrowL     // Catch:{ all -> 0x014c }
            r0.bitmap = r1     // Catch:{ all -> 0x014c }
            int r0 = r11.ballSpeed     // Catch:{ all -> 0x014c }
            if (r0 <= 0) goto L_0x00ae
            int r0 = r11.ballSpeed     // Catch:{ all -> 0x014c }
            int r0 = r0 - r8
            r11.ballSpeed = r0     // Catch:{ all -> 0x014c }
        L_0x00ae:
            r0 = 1
            r11.leftArrowBeingPushed = r0     // Catch:{ all -> 0x014c }
            monitor-exit(r7)     // Catch:{ all -> 0x014c }
            r0 = r8
            goto L_0x0013
        L_0x00b5:
            com.vandaveer.cannonwars.Graphic r1 = r11.arrowR     // Catch:{ all -> 0x014c }
            int r4 = com.vandaveer.cannonwars.CannonWarsPanel.playerTouchedArrowPadding     // Catch:{ all -> 0x014c }
            int r5 = com.vandaveer.cannonwars.CannonWarsPanel.playerTouchedArrowPadding     // Catch:{ all -> 0x014c }
            r0 = r11
            boolean r0 = r0.PlayerTouchedGraphic(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x014c }
            if (r0 == 0) goto L_0x00d9
            com.vandaveer.cannonwars.Graphic r0 = r11.arrowR     // Catch:{ all -> 0x014c }
            android.graphics.Bitmap r1 = r11.bmpRedArrowR     // Catch:{ all -> 0x014c }
            r0.bitmap = r1     // Catch:{ all -> 0x014c }
            int r0 = r11.ballSpeed     // Catch:{ all -> 0x014c }
            if (r0 >= r10) goto L_0x00d2
            int r0 = r11.ballSpeed     // Catch:{ all -> 0x014c }
            int r0 = r0 + 1
            r11.ballSpeed = r0     // Catch:{ all -> 0x014c }
        L_0x00d2:
            r0 = 1
            r11.rightArrowBeingPushed = r0     // Catch:{ all -> 0x014c }
            monitor-exit(r7)     // Catch:{ all -> 0x014c }
            r0 = r8
            goto L_0x0013
        L_0x00d9:
            com.vandaveer.cannonwars.Graphic r1 = r11.arrowU     // Catch:{ all -> 0x014c }
            int r4 = com.vandaveer.cannonwars.CannonWarsPanel.playerTouchedArrowPadding     // Catch:{ all -> 0x014c }
            int r5 = com.vandaveer.cannonwars.CannonWarsPanel.playerTouchedArrowPadding     // Catch:{ all -> 0x014c }
            r0 = r11
            boolean r0 = r0.PlayerTouchedGraphic(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x014c }
            if (r0 == 0) goto L_0x00fd
            com.vandaveer.cannonwars.Graphic r0 = r11.arrowU     // Catch:{ all -> 0x014c }
            android.graphics.Bitmap r1 = r11.bmpRedArrowU     // Catch:{ all -> 0x014c }
            r0.bitmap = r1     // Catch:{ all -> 0x014c }
            int r0 = r11.height     // Catch:{ all -> 0x014c }
            if (r0 >= r10) goto L_0x00f6
            int r0 = r11.height     // Catch:{ all -> 0x014c }
            int r0 = r0 + 1
            r11.height = r0     // Catch:{ all -> 0x014c }
        L_0x00f6:
            r0 = 1
            r11.upArrowBeingPushed = r0     // Catch:{ all -> 0x014c }
            monitor-exit(r7)     // Catch:{ all -> 0x014c }
            r0 = r8
            goto L_0x0013
        L_0x00fd:
            com.vandaveer.cannonwars.Graphic r1 = r11.arrowD     // Catch:{ all -> 0x014c }
            int r4 = com.vandaveer.cannonwars.CannonWarsPanel.playerTouchedArrowPadding     // Catch:{ all -> 0x014c }
            int r5 = com.vandaveer.cannonwars.CannonWarsPanel.playerTouchedArrowPadding     // Catch:{ all -> 0x014c }
            r0 = r11
            boolean r0 = r0.PlayerTouchedGraphic(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x014c }
            if (r0 == 0) goto L_0x0027
            com.vandaveer.cannonwars.Graphic r0 = r11.arrowD     // Catch:{ all -> 0x014c }
            android.graphics.Bitmap r1 = r11.bmpRedArrowD     // Catch:{ all -> 0x014c }
            r0.bitmap = r1     // Catch:{ all -> 0x014c }
            int r0 = r11.height     // Catch:{ all -> 0x014c }
            if (r0 <= 0) goto L_0x0119
            int r0 = r11.height     // Catch:{ all -> 0x014c }
            int r0 = r0 - r8
            r11.height = r0     // Catch:{ all -> 0x014c }
        L_0x0119:
            r0 = 1
            r11.downArrowBeingPushed = r0     // Catch:{ all -> 0x014c }
            monitor-exit(r7)     // Catch:{ all -> 0x014c }
            r0 = r8
            goto L_0x0013
        L_0x0120:
            com.vandaveer.cannonwars.Graphic r0 = r11.arrowL     // Catch:{ all -> 0x014c }
            android.graphics.Bitmap r1 = r11.bmpArrowL     // Catch:{ all -> 0x014c }
            r0.bitmap = r1     // Catch:{ all -> 0x014c }
            com.vandaveer.cannonwars.Graphic r0 = r11.arrowR     // Catch:{ all -> 0x014c }
            android.graphics.Bitmap r1 = r11.bmpArrowR     // Catch:{ all -> 0x014c }
            r0.bitmap = r1     // Catch:{ all -> 0x014c }
            com.vandaveer.cannonwars.Graphic r0 = r11.arrowU     // Catch:{ all -> 0x014c }
            android.graphics.Bitmap r1 = r11.bmpArrowU     // Catch:{ all -> 0x014c }
            r0.bitmap = r1     // Catch:{ all -> 0x014c }
            com.vandaveer.cannonwars.Graphic r0 = r11.arrowD     // Catch:{ all -> 0x014c }
            android.graphics.Bitmap r1 = r11.bmpArrowD     // Catch:{ all -> 0x014c }
            r0.bitmap = r1     // Catch:{ all -> 0x014c }
            com.vandaveer.cannonwars.Graphic r0 = r11.imgFire     // Catch:{ all -> 0x014c }
            android.graphics.Bitmap r1 = r11.bmpFire     // Catch:{ all -> 0x014c }
            r0.bitmap = r1     // Catch:{ all -> 0x014c }
            r0 = 0
            r11.leftArrowBeingPushed = r0     // Catch:{ all -> 0x014c }
            r0 = 0
            r11.rightArrowBeingPushed = r0     // Catch:{ all -> 0x014c }
            r0 = 0
            r11.upArrowBeingPushed = r0     // Catch:{ all -> 0x014c }
            r0 = 0
            r11.downArrowBeingPushed = r0     // Catch:{ all -> 0x014c }
            goto L_0x0027
        L_0x014c:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x014c }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vandaveer.cannonwars.CannonWarsPanel.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void updatePhysics() {
        if (this.background != null && this.message == null) {
            if (this.fireCannon || this.computerFireCannon) {
                FireCannon();
            }
            for (int i = 0; i < this.planes.size(); i++) {
                float planeX = this.planes.get(i).locationX;
                if (planeX < 0.0f - this.planeWidth || planeX > ((float) this.screenWidth) + this.planeWidth) {
                    this.planes.remove(i);
                    if (this.totPlaneBombsDroped >= this.totPlanes) {
                        this.totPlaneBombsDroped--;
                    }
                } else {
                    this.planes.get(i).locationX += this.planes.get(i).getSpeed().getX();
                    if (Math.abs(new Random().nextInt() % this.chanceOfBombDrop) == 0) {
                        DropPlaneBombs();
                    }
                }
            }
            for (int i2 = 0; i2 < this.bombs.size(); i2++) {
                Graphic.Speed speed = this.bombs.get(i2).getSpeed();
                this.bombs.get(i2).locationX += speed.getX();
                this.bombs.get(i2).locationY += speed.getY();
            }
        }
    }

    private void DropPlaneBombs() {
        if (this.totPlaneBombsDroped < this.totPlanes) {
            int bombSizeLen = this.bombs.size();
            int planeSizeLen = this.planes.size();
            if (planeSizeLen > 0 && bombSizeLen < this.totPlanes) {
                Graphic bomb = null;
                int randBomb = new Random().nextInt(planeSizeLen);
                float planeX = this.planes.get(randBomb).locationX;
                if (randBomb <= planeSizeLen) {
                    if (this.planes.get(randBomb).getSpeed().getX() > 0.0f) {
                        if (planeX < this.playerCannonXPos) {
                            bomb = new Graphic(this.bmBombR);
                            bomb.getSpeed().setX(this.bombXSpeed);
                        }
                    } else if (planeX < this.computerCannonXPos + ((float) this.cannonWidth)) {
                        bomb = new Graphic(this.bmBombL);
                        bomb.getSpeed().setX(-this.bombXSpeed);
                    }
                    if (bomb != null) {
                        bomb.locationX = planeX;
                        bomb.locationY = ((float) this.planes.get(randBomb).bitmap.getHeight()) + this.planes.get(randBomb).locationY;
                        bomb.getSpeed().setY(this.bombYSpeed);
                        if (bombSizeLen <= 0) {
                            this.bombs.add(bomb);
                        } else if (bomb.locationY < this.bombs.get(this.bombs.size() - 1).locationY - this.offScreenX) {
                            this.bombs.add(bomb);
                        }
                        this.totPlaneBombsDroped++;
                    }
                }
            }
        }
    }

    public void checkForWinners() {
        if (this.background != null) {
            if (!this.gameOver) {
                if (this.computerCannonXPos > 0.0f) {
                    if (CannonBallHitsCannon()) {
                        PlaySound(this.sndCannonHit);
                        return;
                    } else if (this.ballYPos > this.toolBarHeight - this.ball.radius) {
                        PlaySound(this.sndGroundHit);
                        this.ball.locationY = this.toolBarHeight - getScaleHeight(25);
                        CreateBallExplosion(false);
                        this.ball.locationX = this.offScreenX;
                        return;
                    } else {
                        int buildingSize = this.buildings.size();
                        for (int i = 0; i < buildingSize; i++) {
                            if (CannonBallHitsBuilding(this.buildings.get(i))) {
                                this.ball.locationX = this.offScreenX;
                                PlaySound(this.sndBuildingHit);
                                if (this.fireCannon) {
                                    this.computerFireCannon = true;
                                    return;
                                }
                                return;
                            }
                        }
                    }
                }
                if (!this.computerFireCannon) {
                    for (int i2 = 0; i2 < this.planes.size(); i2++) {
                        if (BallCollide(this.planes.get(i2))) {
                            PlaySound(this.sndGroundHit);
                            CreateExplosion(this.planes.get(i2), true);
                            this.planes.remove(i2);
                            this.ball.locationX = this.offScreenX;
                            this.score++;
                            CheckLevel();
                        }
                    }
                }
                for (int i3 = 0; i3 < this.bombs.size(); i3++) {
                    if (BallCollide(this.bombs.get(i3))) {
                        PlaySound(this.sndGroundHit);
                        CreateExplosion(this.bombs.get(i3), false);
                        this.ball.locationX = this.offScreenX;
                        this.bombs.remove(i3);
                        this.score++;
                        CheckLevel();
                    }
                }
                for (int i4 = 0; i4 < this.bombs.size(); i4++) {
                    if (((float) this.bombs.get(i4).bitmap.getHeight()) + this.bombs.get(i4).locationY > this.groundY) {
                        PlaySound(this.sndCannonHit);
                        Graphic exp0 = new Graphic(this.bmExp0);
                        exp0.locationX = this.bombs.get(i4).locationX;
                        exp0.locationY = this.bombs.get(i4).locationY;
                        this.explosions.add(exp0);
                        this.bombs.remove(i4);
                        this.ball.locationX = this.offScreenX;
                    } else if (Collide(this.bombs.get(i4), this.playerCannon)) {
                        PlaySound(this.sndCannonHit);
                        CreateExplosion(this.playerCannon, true);
                        this.bombs.remove(i4);
                        this.playerCannon.locationX = this.offScreenX;
                        this.ball.locationX = this.offScreenX;
                        this.gameOver = true;
                    }
                }
            } else if (this.explosions.size() <= 0) {
                PauseGame(true);
                if (this.score <= this.lowestHighScore || this.lowestHighScore == 0) {
                    PromptForNewGame(true);
                    return;
                }
                final Context context = getContext();
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        new EnterHighScore(context, this).show();
                    }
                });
            }
        }
    }

    private boolean Collide(Graphic first, Graphic second) {
        float x1 = first.locationX;
        float y1 = first.locationY;
        float w1 = (float) first.bitmap.getWidth();
        float h1 = (float) first.bitmap.getHeight();
        float x2 = second.locationX;
        float y2 = second.locationY;
        float h2 = (float) second.bitmap.getHeight();
        float w2 = (float) second.bitmap.getWidth();
        if (x1 + w1 <= x2 || x1 >= x2 + w2 || y1 >= y2 + h2 || y1 + h1 <= y2) {
            return false;
        }
        return true;
    }

    private boolean BallCollide(Graphic graphic) {
        if (this.ball.locationX + this.ball.radius <= graphic.locationX - this.ballPaddingX || this.ball.locationX - this.ball.radius >= graphic.locationX + ((float) graphic.bitmap.getWidth()) + this.ballPaddingX || this.ball.locationY + this.ball.radius <= graphic.locationY - this.ballPaddingY || this.ball.locationY - this.ball.radius >= graphic.locationY + ((float) graphic.bitmap.getHeight()) + this.ballPaddingY) {
            return false;
        }
        return true;
    }

    private boolean CannonBallHitsCannon() {
        if (this.fireCannon) {
            if (this.ballXPos + this.ball.radius > this.computerCannonXPos - this.cannonHitOffset && this.ballXPos < this.computerCannonXPos + ((float) this.cannonWidth) + this.cannonHitOffset && this.ballYPos > this.computerCannonYPos) {
                this.ball.locationX = this.offScreenX;
                CreateExplosion(this.computerCannon, true);
                this.computerCannon.locationX = this.offScreenX;
                this.score++;
                CheckLevel();
                return true;
            }
        } else if (this.ballXPos + this.ball.radius > this.playerCannonXPos - this.cannonHitOffset && this.ballXPos < this.playerCannonXPos + ((float) this.cannonWidth) + this.cannonHitOffset && this.ballYPos > this.playerCannonYPos) {
            this.ball.locationX = this.offScreenX;
            CreateExplosion(this.playerCannon, true);
            this.playerCannon.locationX = this.offScreenX;
            this.gameOver = true;
            return true;
        }
        return false;
    }

    private boolean CannonBallHitsBuilding(Graphic build) {
        if (this.ballYPos > this.toolBarHeight + this.ball.radius) {
            return false;
        }
        if (this.ball.locationX + this.ball.radius <= build.locationX || this.ball.locationX - this.ball.radius >= build.locationX + ((float) build.bitmap.getWidth()) || this.ball.locationY + this.ball.radius <= build.locationY || this.ball.locationY - this.ball.radius >= build.locationY + ((float) build.bitmap.getHeight())) {
            return false;
        }
        if (this.ballXPos > build.locationX + this.buildingWidth) {
            this.ballXPos = (build.locationX + this.buildingWidth) - (this.ball.radius * 2.0f);
        }
        if (this.ballYPos > this.toolBarHeight - this.ball.radius) {
            this.ballXPos = build.locationX + this.ball.radius;
            this.ballYPos = this.toolBarHeight - this.ball.radius;
        }
        CreateBuildingExplosion(this.ballXPos, this.ballYPos, build);
        return true;
    }

    public void onDraw(Canvas canvas) {
        Graphic plane;
        if (this.background == null) {
            SetUpImages();
            ToggleSound(this.playSound);
            newGame();
            return;
        }
        canvas.drawBitmap(this.background.bitmap, 0.0f, 0.0f, (Paint) null);
        if (this.message == null) {
            if (this.planes.size() < this.totPlanes && !this.gameOver) {
                Random random = new Random();
                if (Math.abs(random.nextInt() % this.chanceOfPlane) == 0) {
                    if (Math.abs(random.nextInt() % 2) == 0) {
                        plane = new Graphic(this.bmpPlaneR);
                        plane.locationX = -this.planeWidth;
                        plane.getSpeed().setX(this.minPlaneSpeed + (((float) Math.random()) * this.maxPlaneSpeed));
                    } else {
                        plane = new Graphic(this.bmpPlaneL);
                        plane.locationX = (float) this.screenWidth;
                        plane.getSpeed().setX(-(this.minPlaneSpeed + (((float) Math.random()) * this.maxPlaneSpeed)));
                    }
                    if (this.planes.size() == 0) {
                        plane.locationY = this.minPlaneY;
                    } else {
                        float lowestPlaneY = this.planes.get(this.planes.size() - 1).locationY + this.planeHeight;
                        if (lowestPlaneY > ((float) (this.screenHeight / 2))) {
                            lowestPlaneY = this.minPlaneY;
                        }
                        plane.locationY = lowestPlaneY;
                    }
                    this.planes.add(plane);
                    if (this.totPlaneBombsDroped > 0) {
                        this.totPlaneBombsDroped = this.totPlaneBombsDroped - 1;
                    }
                }
            }
            Iterator<Graphic> it = this.buildings.iterator();
            while (it.hasNext()) {
                Graphic building = it.next();
                canvas.drawBitmap(building.bitmap, building.locationX, building.locationY, (Paint) null);
            }
            Iterator<Graphic> it2 = this.bombs.iterator();
            while (it2.hasNext()) {
                Graphic bomb = it2.next();
                canvas.drawBitmap(bomb.bitmap, bomb.locationX, bomb.locationY, (Paint) null);
            }
            canvas.drawBitmap(this.playerCannon.bitmap, this.playerCannon.locationX, this.playerCannon.locationY, (Paint) null);
            canvas.drawBitmap(this.computerCannon.bitmap, this.computerCannon.locationX, this.computerCannon.locationY, (Paint) null);
            if (this.ballXPos > 0.0f) {
                if (((float) (System.currentTimeMillis() - this.lastTimePlayerCannonFired)) < fireDisplayTime) {
                    if (this.fireCannon) {
                        this.playerCannonFire.locationX = (this.playerCannon.locationX + ((float) this.cannonWidth)) - getScaleWidth(10);
                    } else {
                        this.playerCannonFire.locationX = this.computerCannon.locationX - getScaleWidth(10);
                    }
                    this.playerCannonFire.locationY = this.playerCannon.locationY - ((float) (this.playerCannonFire.bitmap.getHeight() / 2));
                    canvas.drawBitmap(this.playerCannonFire.bitmap, this.playerCannonFire.locationX, this.playerCannonFire.locationY - getScaleHeight(2), (Paint) null);
                }
                Paint mPaint = new Paint();
                mPaint.setColor(-256);
                mPaint.setShadowLayer(this.ball.radius - getScaleWidth(2), getScaleWidth(2), getScaleHeight(1), -16777216);
                mPaint.setAntiAlias(true);
                canvas.drawCircle(this.ball.locationX, this.ball.locationY, this.ball.radius, mPaint);
            }
            if (this.computerCannon.locationX != this.offScreenX || this.explosions.size() != 0) {
                Iterator<Graphic> it3 = this.planes.iterator();
                while (it3.hasNext()) {
                    Graphic plane2 = it3.next();
                    canvas.drawBitmap(plane2.bitmap, plane2.locationX, plane2.locationY, (Paint) null);
                }
                Iterator<Graphic> it4 = this.bldFireL.iterator();
                while (it4.hasNext()) {
                    Graphic buildingLeftFire = it4.next();
                    canvas.drawBitmap(buildingLeftFire.bitmap, buildingLeftFire.locationX, buildingLeftFire.locationY, (Paint) null);
                }
                Iterator<Graphic> it5 = this.bldFireR.iterator();
                while (it5.hasNext()) {
                    Graphic buildingRightFire = it5.next();
                    canvas.drawBitmap(buildingRightFire.bitmap, buildingRightFire.locationX, buildingRightFire.locationY, (Paint) null);
                }
                Paint paint = new Paint();
                paint.setColor(-7829368);
                Canvas canvas2 = canvas;
                canvas2.drawRect(0.0f, this.toolBarHeight, (float) getWidth(), (float) getHeight(), paint);
                paint.setColor(-1);
                paint.setStrokeWidth(2.0f);
                canvas.drawLine(0.0f, this.toolBarHeight - 1.0f, (float) getWidth(), this.toolBarHeight - 1.0f, paint);
                canvas.drawBitmap(this.arrowL.bitmap, this.arrowLX, this.arrowRightLeftY, (Paint) null);
                canvas.drawBitmap(this.arrowR.bitmap, this.arrowRX, this.arrowRightLeftY, (Paint) null);
                canvas.drawBitmap(this.arrowU.bitmap, this.arrowUX, this.arrowUpDownY, (Paint) null);
                canvas.drawBitmap(this.arrowD.bitmap, this.arrowDX, this.arrowUpDownY, (Paint) null);
                canvas.drawBitmap(this.imgFire.bitmap, this.fireX, this.fireY, (Paint) null);
                canvas.drawBitmap(this.imgExit.bitmap, this.exitX, this.exitY, (Paint) null);
                canvas.drawBitmap(this.imgMute.bitmap, this.muteX, this.muteY, (Paint) null);
                paint.setTextSize(this.scoreTextHeight);
                paint.setColor(-1);
                paint.setAntiAlias(true);
                paint.setTypeface(Typeface.SANS_SERIF);
                canvas.drawText("Score: " + this.score, this.scoreX, this.scoreY, paint);
                if (this.score > this.highscore) {
                    this.highscore = this.score;
                }
                canvas.drawText("High Score: " + this.highscore, this.scoreX, this.highScoreY, paint);
                canvas.drawText("Height: " + this.height, this.heightDisplayX, this.heightSpeedDisplayY, paint);
                if (this.height < 10) {
                    this.speedDisplayX = this.heightDisplayX + getScaleWidth(62);
                } else if (this.height <= 9 || this.height >= 100) {
                    this.speedDisplayX = this.heightDisplayX + getScaleWidth(73);
                } else {
                    this.speedDisplayX = this.heightDisplayX + getScaleWidth(67);
                }
                canvas.drawText("Speed: " + this.ballSpeed, this.speedDisplayX, this.heightSpeedDisplayY, paint);
                if (this.explosions.size() > 0) {
                    int expSize = this.explosions.size() - 1;
                    canvas.drawBitmap(this.explosions.get(expSize).bitmap, this.explosions.get(expSize).locationX, this.explosions.get(expSize).locationY, (Paint) null);
                    if (System.currentTimeMillis() > this.explosions.get(expSize).displayTime) {
                        this.explosions.remove(expSize);
                    }
                }
                long thisTime = System.currentTimeMillis();
                if (thisTime - this.lastArrowTime > 200) {
                    this.lastArrowTime = thisTime;
                    if (this.leftArrowBeingPushed) {
                        this.arrowL.bitmap = this.bmpRedArrowL;
                        if (this.ballSpeed > 0) {
                            this.ballSpeed = this.ballSpeed - 1;
                        }
                    }
                    if (this.rightArrowBeingPushed) {
                        this.arrowR.bitmap = this.bmpRedArrowR;
                        if (this.ballSpeed < 100) {
                            this.ballSpeed = this.ballSpeed + 1;
                        }
                    }
                    if (this.upArrowBeingPushed) {
                        this.arrowU.bitmap = this.bmpRedArrowU;
                        if (this.height < 100) {
                            this.height = this.height + 1;
                        }
                    }
                    if (this.downArrowBeingPushed) {
                        this.arrowD.bitmap = this.bmpRedArrowD;
                        if (this.height > 0) {
                            this.height = this.height - 1;
                        }
                    }
                }
            } else if (!this.gameOver) {
                createNewScene();
            }
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height2) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (!this._thread.isAlive()) {
            this._thread = new CannonWarsThread(this);
        }
        this._thread.setRunning(true);
        try {
            this._thread.start();
        } catch (IllegalThreadStateException e) {
            this._thread.resume();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        this._thread.setRunning(false);
        while (retry) {
            try {
                this._thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
        Log.i("thread", "Thread terminated...");
    }

    public void PauseGame(boolean pause) {
        this._thread.Pause(pause);
    }

    public void PromptForNewGame(final boolean displayGameOverMessage) {
        final Context context = getContext();
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
                if (displayGameOverMessage) {
                    alertbox.setMessage((int) R.string.game_over_text);
                } else {
                    alertbox.setMessage("Score has been submitted. Play Again?");
                }
                alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        CannonWarsPanel.this.level = 1;
                        CannonWarsPanel.this.newGame();
                    }
                });
                alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        CannonWarsPanel.this.upSell();
                    }
                });
                alertbox.show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void upSell() {
        final Context context = getContext();
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
                alertbox.setMessage("Want Cannon Wars Pro?");
                final Context context = context;
                alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        ((Activity) context).startActivity(new Intent("android.intent.action.VIEW", Uri.parse(CannonWarsPanel.PRO_VERSION)));
                        CannonWarsPanel.this.EndGame();
                    }
                });
                alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        CannonWarsPanel.this.EndGame();
                    }
                });
                alertbox.show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void EndGame() {
        ((Activity) getContext()).finish();
    }

    /* access modifiers changed from: private */
    public void newGame() {
        this.score = 0;
        this.gameOver = false;
        ChangeLevel(this.level);
        PauseGame(false);
    }

    private float getScaleHeight(int factor) {
        return (((float) factor) / SCALE_HEIGHT) * ((float) this.screenHeight);
    }

    private float getScaleWidth(int factor) {
        return (((float) factor) / SCALE_WIDTH) * ((float) this.screenWidth);
    }

    private void createNewScene() {
        this.bldFireL.clear();
        this.bldFireR.clear();
        this.buildings.clear();
        this.zArch = 0.0f;
        this.ballSpeed = 1;
        this.height = 1;
        this.lastComputerBallX = 0.0f;
        this.lastComputerBallY = 0.0f;
        this.bombs.clear();
        this.planes.clear();
        this.explosions.clear();
        Random generator = new Random();
        float minPlayerBuildingX = 0.0f;
        float maxPlayerBuildingX = 0.0f;
        for (int i = 0; i < this.maxBuildings; i++) {
            switch (this.maxBuildings) {
                case 1:
                    minPlayerBuildingX = getScaleWidth(85);
                    maxPlayerBuildingX = (((float) this.screenWidth) - getScaleWidth(85)) - this.buildingWidth;
                    break;
                case 2:
                    if (i != 0) {
                        minPlayerBuildingX = this.buildings.get(i - 1).locationX + this.buildingWidth + getScaleWidth(5);
                        maxPlayerBuildingX = (((float) this.screenWidth) - getScaleWidth(85)) - this.buildingWidth;
                        break;
                    } else {
                        minPlayerBuildingX = getScaleWidth(90);
                        maxPlayerBuildingX = minPlayerBuildingX + (this.buildingWidth * ((float) (i + 1)));
                        break;
                    }
                case R.styleable.com_admob_android_ads_AdView_keywords /*3*/:
                    if (i != 0) {
                        if (i != 1) {
                            minPlayerBuildingX = this.buildings.get(i - 1).locationX + this.buildingWidth + ((float) (this.cannonWidth * 3));
                            maxPlayerBuildingX = ((float) this.screenWidth) - this.buildingWidth;
                            break;
                        } else {
                            minPlayerBuildingX = this.buildings.get(i - 1).locationX + this.buildingWidth + getScaleWidth(5);
                            maxPlayerBuildingX = minPlayerBuildingX + (this.buildingWidth * 2.0f);
                            break;
                        }
                    } else {
                        minPlayerBuildingX = getScaleWidth(90);
                        maxPlayerBuildingX = minPlayerBuildingX + (this.buildingWidth * ((float) (i + 1)));
                        break;
                    }
            }
            Graphic graphic = new Graphic(this.bmBuilding);
            graphic.locationX = (generator.nextFloat() * (maxPlayerBuildingX - minPlayerBuildingX)) + minPlayerBuildingX;
            graphic.locationY = (generator.nextFloat() * (this.maxBuildingHeight - this.minBuildingHeight)) + this.minBuildingHeight;
            this.buildings.add(graphic);
        }
        this.playerCannonXPos = ((((this.buildings.get(0).locationX - ((float) this.cannonWidth)) - getScaleWidth(40)) - ((float) this.cannonWidth)) * generator.nextFloat()) + ((float) this.cannonWidth);
        this.playerCannonYPos = this.toolBarHeight - ((float) this.cannonHeight);
        this.playerCannon.locationX = this.playerCannonXPos;
        this.playerCannon.locationY = this.playerCannonYPos;
        this.ball.locationX = this.offScreenX;
        if (this.maxBuildings < 3) {
            this.computerCannonXPos = this.buildings.get(this.maxBuildings - 1).locationX + this.buildingWidth + ((float) this.cannonWidth) + getScaleWidth(20) + (generator.nextFloat() * (((float) (this.screenWidth - this.cannonWidth)) - (((this.buildings.get(this.maxBuildings - 1).locationX + this.buildingWidth) + ((float) this.cannonWidth)) + getScaleWidth(20))));
        } else {
            this.computerCannonXPos = (this.buildings.get(1).locationX + this.buildings.get(2).locationX) / 2.0f;
        }
        if (this.computerCannonXPos + ((float) this.cannonWidth) > ((float) this.screenWidth)) {
            this.computerCannonXPos = (float) (this.screenWidth - this.cannonWidth);
        }
        this.computerCannonYPos = this.toolBarHeight - ((float) this.cannonHeight);
        this.computerCannon.locationX = this.computerCannonXPos;
        this.computerCannon.locationY = this.computerCannonYPos;
    }

    private void CreateExplosion(Graphic graphic, boolean bigExplosion) {
        float startX = graphic.locationX;
        float startY = graphic.locationY;
        if (bigExplosion) {
            Graphic exp0 = new Graphic(this.bmExp0);
            exp0.locationX = startX;
            exp0.locationY = startY;
            exp0.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 6);
            this.explosions.add(exp0);
            Graphic exp1 = new Graphic(this.bmExp1);
            exp1.locationX = startX;
            exp1.locationY = startY;
            exp1.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 5);
            this.explosions.add(exp1);
            Graphic exp2 = new Graphic(this.bmExp2);
            exp2.locationX = startX;
            exp2.locationY = startY;
            exp2.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 4);
            this.explosions.add(exp2);
            Graphic exp3 = new Graphic(this.bmExp2);
            exp3.locationX = startX;
            exp3.locationY = startY;
            exp3.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 3);
            this.explosions.add(exp3);
            Graphic exp4 = new Graphic(this.bmExp1);
            exp4.locationX = startX;
            exp4.locationY = startY;
            exp4.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 2);
            this.explosions.add(exp4);
            Graphic exp5 = new Graphic(this.bmExp0);
            exp5.locationX = startX;
            exp5.locationY = startY;
            exp5.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 1);
            this.explosions.add(exp5);
            return;
        }
        Graphic exp02 = new Graphic(this.bmExp0);
        exp02.locationX = startX;
        exp02.locationY = startY;
        exp02.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 3);
        this.explosions.add(exp02);
        Graphic exp12 = new Graphic(this.bmExp1);
        exp12.locationX = startX;
        exp12.locationY = startY;
        exp12.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 2);
        this.explosions.add(exp12);
        Graphic exp22 = new Graphic(this.bmExp2);
        exp22.locationX = startX;
        exp22.locationY = startY;
        exp22.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 1);
        this.explosions.add(exp22);
    }

    private void CreateBallExplosion(boolean bigExplosion) {
        float startX = this.ball.locationX - this.ball.radius;
        float startY = this.ball.locationY - this.ball.radius;
        if (bigExplosion) {
            Graphic exp0 = new Graphic(this.bmExp0);
            exp0.locationX = startX;
            exp0.locationY = startY;
            exp0.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 6);
            this.explosions.add(exp0);
            Graphic exp1 = new Graphic(this.bmExp1);
            exp1.locationX = startX;
            exp1.locationY = startY;
            exp1.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 5);
            this.explosions.add(exp1);
            Graphic exp2 = new Graphic(this.bmExp2);
            exp2.locationX = startX;
            exp2.locationY = startY;
            exp2.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 4);
            this.explosions.add(exp2);
            Graphic exp3 = new Graphic(this.bmExp2);
            exp3.locationX = startX;
            exp3.locationY = startY;
            exp3.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 3);
            this.explosions.add(exp3);
            Graphic exp4 = new Graphic(this.bmExp1);
            exp4.locationX = startX;
            exp4.locationY = startY;
            exp4.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 2);
            this.explosions.add(exp4);
            Graphic exp5 = new Graphic(this.bmExp0);
            exp5.locationX = startX;
            exp5.locationY = startY;
            exp5.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 1);
            this.explosions.add(exp5);
            return;
        }
        Graphic exp02 = new Graphic(this.bmExp0);
        exp02.locationX = startX;
        exp02.locationY = startY;
        exp02.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 3);
        this.explosions.add(exp02);
        Graphic exp12 = new Graphic(this.bmExp1);
        exp12.locationX = startX;
        exp12.locationY = startY;
        exp12.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 2);
        this.explosions.add(exp12);
        Graphic exp22 = new Graphic(this.bmExp2);
        exp22.locationX = startX;
        exp22.locationY = startY;
        exp22.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 1);
        this.explosions.add(exp22);
    }

    private void CreateBuildingExplosion(float startX, float startY, Graphic build) {
        Graphic exp0 = new Graphic(this.bmExp0);
        exp0.locationX = startX;
        exp0.locationY = startY;
        exp0.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 6);
        this.explosions.add(exp0);
        Graphic exp1 = new Graphic(this.bmExp1);
        exp1.locationX = startX;
        exp1.locationY = startY;
        exp1.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 5);
        this.explosions.add(exp1);
        Graphic exp2 = new Graphic(this.bmExp2);
        exp2.locationX = startX;
        exp2.locationY = startY;
        exp2.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 4);
        this.explosions.add(exp2);
        Graphic exp3 = new Graphic(this.bmExp2);
        exp3.locationX = startX;
        exp3.locationY = startY;
        exp3.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 3);
        this.explosions.add(exp3);
        Graphic exp4 = new Graphic(this.bmExp1);
        exp4.locationX = startX;
        exp4.locationY = startY;
        exp4.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 2);
        this.explosions.add(exp4);
        Graphic exp5 = new Graphic(this.bmExp0);
        exp5.locationX = startX;
        exp5.locationY = startY;
        exp5.displayTime = System.currentTimeMillis() + (this.explosionDisplayTime * 1);
        this.explosions.add(exp5);
        if (startX < build.locationX + (this.buildingWidth / 2.0f)) {
            Graphic buildingFire = new Graphic(this.bmBldFireL);
            buildingFire.locationX = startX;
            buildingFire.locationY = startY;
            this.bldFireL.add(buildingFire);
            return;
        }
        Graphic buildingFire2 = new Graphic(this.bmBldFireR);
        buildingFire2.locationX = startX;
        buildingFire2.locationY = startY;
        this.bldFireR.add(buildingFire2);
    }

    private boolean PlayerTouchedGraphic(Graphic graphic, float touchX, float touchY, int xBuffer, int yBuffer) {
        if (graphic != null) {
            float graphicWidth = (float) graphic.bitmap.getWidth();
            float graphicHeight = (float) graphic.bitmap.getHeight();
            if (touchX <= graphic.locationX - ((float) xBuffer) || touchX >= graphic.locationX + graphicWidth + ((float) xBuffer) || touchY <= graphic.locationY - ((float) yBuffer) || touchY >= graphic.locationY + graphicHeight + ((float) yBuffer)) {
                return false;
            }
            return true;
        }
        return false;
    }

    private void showToastMessage(final String msg, final boolean pauseGame) {
        final Context context = getContext();
        PauseGame(pauseGame);
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                Toast toast = Toast.makeText(context, msg, 0);
                toast.setGravity(16, 0, 0);
                toast.show();
                CannonWarsPanel.this.PauseGame(pauseGame);
            }
        });
    }

    private void DisplayMessage(String msg) {
        this.message = msg;
        final Context context = getContext();
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                ((ShowMessage) new ShowMessage(context, this)).display();
            }
        });
    }

    private void ResetForNextLevel() {
        createNewScene();
    }

    private void ChangeLevel(int lvl) {
        float speedScale = ((float) this.screenWidth) / SCALE_WIDTH;
        switch (lvl) {
            case 1:
                this.maxBuildings = 1;
                this.chanceOfPlane = 5000;
                this.chanceOfBombDrop = 5000;
                this.totPlanes = 1;
                this.minPlaneSpeed = 1.0f * speedScale;
                this.maxPlaneSpeed = 1.2f * speedScale;
                this.bombXSpeed = 0.5f * speedScale;
                this.bombYSpeed = 1.0f * speedScale;
                DisplayMessage("Level 1");
                break;
            case 2:
                this.maxBuildings = 2;
                this.chanceOfPlane = 4000;
                this.totPlanes = 1;
                this.chanceOfBombDrop = 4000;
                this.minPlaneSpeed = 1.2f * speedScale;
                this.maxPlaneSpeed = 1.5f * speedScale;
                this.bombXSpeed = 0.5f * speedScale;
                this.bombYSpeed = 1.0f * speedScale;
                DisplayMessage("Level 2");
                break;
            case R.styleable.com_admob_android_ads_AdView_keywords /*3*/:
                this.maxBuildings = 2;
                this.chanceOfPlane = 3000;
                this.totPlanes = 1;
                this.chanceOfBombDrop = 3000;
                this.minPlaneSpeed = 1.5f * speedScale;
                this.maxPlaneSpeed = 1.7f * speedScale;
                this.bombXSpeed = 1.0f * speedScale;
                this.bombYSpeed = 1.2f * speedScale;
                DisplayMessage("Level 3");
                this.level = 3;
                break;
            case 4:
                this.maxBuildings = 2;
                this.chanceOfPlane = 2000;
                this.chanceOfBombDrop = 1000;
                this.totPlanes = 1;
                this.minPlaneSpeed = 1.7f * speedScale;
                this.maxPlaneSpeed = 2.0f * speedScale;
                this.bombXSpeed = 1.0f * speedScale;
                this.bombYSpeed = 1.2f * speedScale;
                DisplayMessage("Level 4");
                this.level = 4;
                break;
            case 5:
                this.maxBuildings = 3;
                this.chanceOfPlane = 1000;
                this.chanceOfBombDrop = 1000;
                this.totPlanes = 2;
                this.minPlaneSpeed = 2.0f * speedScale;
                this.maxPlaneSpeed = 2.2f * speedScale;
                this.bombXSpeed = 1.0f * speedScale;
                this.bombYSpeed = 1.2f * speedScale;
                DisplayMessage("Level 5");
                this.level = 5;
                break;
            case 6:
                this.maxBuildings = 3;
                this.chanceOfPlane = 1000;
                this.chanceOfBombDrop = 1000;
                this.totPlanes = 2;
                this.minPlaneSpeed = 2.0f * speedScale;
                this.maxPlaneSpeed = 2.2f * speedScale;
                this.bombXSpeed = 1.2f * speedScale;
                this.bombYSpeed = 1.2f * speedScale;
                DisplayMessage("Level 6");
                this.level = 6;
                break;
            case 7:
                this.maxBuildings = 3;
                this.chanceOfPlane = 900;
                this.chanceOfBombDrop = 900;
                this.totPlanes = 3;
                this.minPlaneSpeed = 2.2f * speedScale;
                this.maxPlaneSpeed = 2.4f * speedScale;
                this.bombXSpeed = 1.2f * speedScale;
                this.bombYSpeed = 1.2f * speedScale;
                DisplayMessage("Level 7");
                this.level = 7;
                break;
            case 8:
                this.maxBuildings = 3;
                this.chanceOfPlane = 900;
                this.totPlanes = 3;
                this.chanceOfBombDrop = 800;
                this.minPlaneSpeed = 2.2f * speedScale;
                this.maxPlaneSpeed = 2.4f * speedScale;
                this.bombXSpeed = 1.2f * speedScale;
                this.bombYSpeed = 1.3f * speedScale;
                DisplayMessage("Level 8");
                this.level = 8;
                break;
            case 9:
                this.maxBuildings = 3;
                this.chanceOfPlane = 800;
                this.chanceOfBombDrop = 750;
                this.totPlanes = 3;
                this.minPlaneSpeed = 2.2f * speedScale;
                this.maxPlaneSpeed = 2.4f * speedScale;
                this.bombXSpeed = 1.2f * speedScale;
                this.bombYSpeed = 1.3f * speedScale;
                DisplayMessage("Level 9");
                this.level = 9;
                break;
            case 10:
                this.maxBuildings = 3;
                this.chanceOfPlane = 750;
                this.chanceOfBombDrop = 750;
                this.totPlanes = 3;
                this.minPlaneSpeed = 2.2f * speedScale;
                this.maxPlaneSpeed = 2.4f * speedScale;
                this.bombXSpeed = 1.3f * speedScale;
                this.bombYSpeed = 1.5f * speedScale;
                DisplayMessage("Level 10");
                this.level = 10;
                break;
            case 11:
                this.maxBuildings = 3;
                this.chanceOfPlane = 700;
                this.chanceOfBombDrop = 700;
                this.totPlanes = 3;
                this.minPlaneSpeed = 2.2f * speedScale;
                this.maxPlaneSpeed = 2.4f * speedScale;
                this.bombXSpeed = 1.3f * speedScale;
                this.bombYSpeed = 1.5f * speedScale;
                DisplayMessage("Level 11");
                this.level = 11;
                break;
            case 12:
                this.maxBuildings = 3;
                this.chanceOfPlane = 500;
                this.chanceOfBombDrop = 500;
                this.totPlanes = 3;
                this.minPlaneSpeed = 2.2f * speedScale;
                this.maxPlaneSpeed = 2.4f * speedScale;
                this.bombXSpeed = 1.3f * speedScale;
                this.bombYSpeed = 1.5f * speedScale;
                DisplayMessage("Level 12");
                this.level = 12;
                break;
        }
        ResetForNextLevel();
        this.level = lvl;
        if (this.level > this.gameLevel) {
            this.gameLevel = this.level;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void CheckLevel() {
        if (this.message == null) {
            switch (this.score) {
                case 8:
                    if (this.level == 1) {
                        ChangeLevel(2);
                        return;
                    }
                    return;
                case 15:
                    if (this.level == 2) {
                        ChangeLevel(3);
                        return;
                    }
                    return;
                case 25:
                    if (this.level == 3) {
                        ChangeLevel(4);
                        return;
                    }
                    return;
                case 50:
                    if (this.level == 4) {
                        ChangeLevel(5);
                        return;
                    }
                    return;
                case 65:
                    if (this.level == 5) {
                        ChangeLevel(6);
                        return;
                    }
                    return;
                case 75:
                    if (this.level == 6) {
                        ChangeLevel(7);
                        return;
                    }
                    return;
                case 100:
                    if (this.level == 7) {
                        ChangeLevel(8);
                        break;
                    }
                    break;
                case 120:
                    break;
                case 130:
                    if (this.level == 9) {
                        ChangeLevel(10);
                        return;
                    }
                    return;
                case 140:
                    if (this.level == 10) {
                        ChangeLevel(11);
                        return;
                    }
                    return;
                case 150:
                    if (this.level == 11) {
                        ChangeLevel(12);
                        return;
                    }
                    return;
                default:
                    return;
            }
            if (this.level == 8) {
                ChangeLevel(9);
            }
        }
    }

    private void SetUpImages() {
        this.screenWidth = getWidth();
        this.screenHeight = getHeight();
        this.archAdjustMent = getScaleWidth(200);
        this.minBallHeight = (int) getScaleHeight(-1000);
        this.background = new Graphic(Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.bg)), getWidth(), getHeight(), true));
        this.bmBuilding = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.b1)), (int) getScaleWidth(49), (int) getScaleHeight(251), true);
        this.buildingWidth = (float) this.bmBuilding.getWidth();
        this.buildingHeight = (float) this.bmBuilding.getHeight();
        this.minPlaneY = getScaleHeight(65);
        this.playerCannon = new Graphic(Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.cannonr)), (int) getScaleWidth(36), (int) getScaleHeight(38), true));
        this.cannonWidth = this.playerCannon.bitmap.getWidth();
        this.cannonHeight = this.playerCannon.bitmap.getHeight();
        this.offScreenX = ((float) (0 - this.cannonWidth)) - getScaleWidth(30);
        this.playerCannonFire = new Graphic(Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.playercannonfire)), (int) getScaleWidth(18), (int) getScaleHeight(20), true));
        this.computerCannon = new Graphic(Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.cannonl)), (int) getScaleWidth(36), (int) getScaleHeight(38), true));
        this.minBuildingHeight = ((float) this.screenHeight) - this.buildingHeight;
        this.maxBuildingHeight = this.buildingHeight - getScaleHeight(100);
        this.ball = new Ball(getScaleWidth(5));
        this.ballPaddingX = getScaleWidth(5);
        this.ballPaddingY = getScaleHeight(5);
        this.scoreX = ((float) this.screenWidth) - getScaleWidth(100);
        this.scoreY = getScaleHeight(25);
        this.highScoreY = getScaleHeight(40);
        this.bmpArrowL = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.arrowl)), (int) getScaleWidth(20), (int) getScaleHeight(22), true);
        this.bmpArrowR = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.arrowr)), (int) getScaleWidth(20), (int) getScaleHeight(22), true);
        this.bmpArrowU = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.arrowu)), (int) getScaleWidth(20), (int) getScaleHeight(24), true);
        this.bmpArrowD = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.arrowd)), (int) getScaleWidth(20), (int) getScaleHeight(24), true);
        this.bmBombL = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.bombl)), (int) getScaleWidth(14), (int) getScaleHeight(20), true);
        this.bmBombR = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.bombr)), (int) getScaleWidth(14), (int) getScaleHeight(20), true);
        this.bmExp0 = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.exp0)), (int) getScaleWidth(25), (int) getScaleHeight(23), true);
        this.bmExp1 = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.exp1)), (int) getScaleWidth(45), (int) getScaleHeight(42), true);
        this.bmExp2 = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.exp2)), (int) getScaleWidth(60), (int) getScaleHeight(56), true);
        this.bmpFire = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.fire)), (int) getScaleWidth(66), (int) getScaleHeight(21), true);
        this.bmpPlaneL = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.planel)), (int) getScaleWidth(64), (int) getScaleHeight(18), true);
        this.bmpPlaneR = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.planer)), (int) getScaleWidth(64), (int) getScaleHeight(18), true);
        this.planeWidth = (float) this.bmpPlaneL.getWidth();
        this.planeHeight = (float) this.bmpPlaneL.getHeight();
        this.bmBldFireL = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.firel2)), (int) getScaleWidth(18), (int) getScaleHeight(38), true);
        this.bmBldFireR = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.firer2)), (int) getScaleWidth(18), (int) getScaleHeight(38), true);
        this.bmpRedArrowL = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.redarrowl)), (int) getScaleWidth(20), (int) getScaleHeight(22), true);
        this.bmpRedArrowR = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.redarrowr)), (int) getScaleWidth(20), (int) getScaleHeight(22), true);
        this.bmpRedArrowU = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.redarrowu)), (int) getScaleWidth(20), (int) getScaleHeight(24), true);
        this.bmpRedArrowD = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.redarrowd)), (int) getScaleWidth(20), (int) getScaleHeight(24), true);
        this.bmpRedFire = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.redfire)), (int) getScaleWidth(66), (int) getScaleHeight(21), true);
        this.bmpExit = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.exit)), (int) getScaleWidth(30), (int) getScaleHeight(30), true);
        this.imgExit = new Graphic(this.bmpExit);
        this.exitX = ((float) this.screenWidth) - getScaleWidth(160);
        this.exitY = ((float) this.screenHeight) - getScaleHeight(36);
        this.imgExit.locationX = this.exitX;
        this.imgExit.locationY = this.exitY;
        this.bmpMute = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.mute)), (int) getScaleWidth(30), (int) getScaleHeight(30), true);
        this.bmpUnMute = Bitmap.createScaledBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.unmute)), (int) getScaleWidth(30), (int) getScaleHeight(30), true);
        this.imgMute = new Graphic(this.bmpMute);
        this.muteX = this.exitX + getScaleWidth(40);
        this.muteY = this.exitY;
        this.imgMute.locationX = this.muteX;
        this.imgMute.locationY = this.muteY;
        this.arrowL = new Graphic(this.bmpArrowL);
        this.arrowR = new Graphic(this.bmpArrowR);
        this.arrowU = new Graphic(this.bmpArrowU);
        this.arrowD = new Graphic(this.bmpArrowD);
        this.imgFire = new Graphic(this.bmpFire);
        this.arrowL.bitmap = this.bmpArrowL;
        this.arrowR.bitmap = this.bmpArrowR;
        this.arrowU.bitmap = this.bmpArrowU;
        this.arrowD.bitmap = this.bmpArrowD;
        this.toolBarHeight = ((float) getHeight()) - getScaleHeight(40);
        this.groundY = this.toolBarHeight;
        this.arrowRightLeftY = this.toolBarHeight + getScaleHeight(9);
        this.arrowUpDownY = this.toolBarHeight + getScaleHeight(8);
        this.arrowLX = getScaleWidth(10);
        this.arrowL.locationX = this.arrowLX;
        this.arrowL.locationY = this.arrowRightLeftY;
        this.arrowRX = getScaleWidth(58);
        this.arrowR.locationX = this.arrowRX;
        this.arrowR.locationY = this.arrowRightLeftY;
        this.arrowUX = getScaleWidth(102);
        this.arrowU.locationX = this.arrowUX;
        this.arrowU.locationY = this.arrowUpDownY;
        this.arrowDX = getScaleWidth(145);
        this.arrowD.locationX = this.arrowDX;
        this.arrowD.locationY = this.arrowUpDownY;
        this.fireX = ((float) this.screenWidth) - getScaleWidth(75);
        this.fireY = ((float) this.screenHeight) - getScaleHeight(31);
        this.imgFire.locationX = this.fireX;
        this.imgFire.locationY = this.fireY;
        this.heightSpeedDisplayY = this.arrowUpDownY + getScaleHeight(15);
        this.heightDisplayX = this.arrowDX + getScaleWidth(35);
        this.speedDisplayX = this.heightDisplayX + getScaleWidth(55);
        this.cannonHitOffset = getScaleWidth(5);
        this.scoreTextHeight = getScaleHeight(15);
    }

    private void promptToExitGame() {
        PauseGame(true);
        final Context context = getContext();
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
                alertbox.setMessage("  Exit Game?  ");
                alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        CannonWarsPanel.this.EndGame();
                    }
                });
                alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        CannonWarsPanel.this.PauseGame(false);
                    }
                });
                alertbox.show();
            }
        });
    }

    private void FireCannon() {
        if (this.ballSpeed == 0) {
            this.fireCannon = false;
            this.computerFireCannon = false;
            this.ballXPos = this.offScreenX;
            return;
        }
        float ballXSpeed = this.ball.speedX;
        float ballYSpeed = this.ball.speedY;
        if (this.ballXPos == this.offScreenX) {
            this.lastTimePlayerCannonFired = System.currentTimeMillis();
            PlaySound(this.sndCannonFire);
            float speedAdjustFaxtor = getScaleWidth(2);
            this.zArch = 0.0f;
            if (this.fireCannon) {
                this.ball.locationX = ((this.playerCannonXPos + ((float) this.cannonWidth)) - getScaleWidth(5)) + this.ball.radius;
                this.ball.locationY = ((this.playerCannonYPos - (this.ball.radius / 2.0f)) - getScaleHeight(5)) + this.ball.radius;
                ballXSpeed = ((float) this.ballSpeed) / speedAdjustFaxtor;
                ballYSpeed = ((float) (-this.height)) / speedAdjustFaxtor;
            } else {
                this.ball.locationX = (this.computerCannonXPos + getScaleWidth(2)) - this.ball.radius;
                this.ball.locationY = ((this.playerCannonYPos - (this.ball.radius / 2.0f)) - getScaleHeight(5)) + this.ball.radius;
                Random generator = new Random();
                int tolerance = 15;
                float buildingProximityAdjustment = 0.0f;
                if (this.maxBuildings == 3 && this.ballXPos < this.buildings.get(1).locationX && this.lastComputerBallY < 0.0f) {
                    buildingProximityAdjustment = this.lastComputerBallY * 1.2f;
                    tolerance = 20;
                }
                ballXSpeed = ((generator.nextFloat() * ((float) ((this.ballSpeed + tolerance) - this.ballSpeed))) + ((float) this.ballSpeed)) / speedAdjustFaxtor;
                ballYSpeed = (-(((generator.nextFloat() * ((float) ((this.height + tolerance) - this.height))) + ((float) this.height)) + buildingProximityAdjustment)) / speedAdjustFaxtor;
                this.lastComputerBallX = ballXSpeed;
                this.lastComputerBallY = ballYSpeed;
            }
        }
        this.ballXPos = this.ball.locationX;
        this.ballYPos = this.ball.locationY;
        if (this.ballYPos + this.ball.radius > ((float) this.screenHeight) || this.ballXPos + this.ball.radius > ((float) this.screenWidth) || this.ballXPos < this.ball.radius || this.ballYPos < ((float) this.minBallHeight)) {
            PlaySound(this.sndGroundHit);
            if (this.fireCannon) {
                this.fireCannon = false;
                if (this.computerCannon.locationX != this.offScreenX) {
                    this.computerFireCannon = true;
                }
            } else {
                this.computerFireCannon = false;
            }
            this.ball.speedX = 0.0f;
            this.ball.locationX = this.offScreenX;
            return;
        }
        float f = ballXSpeed - 1.0f;
        this.ball.speedX = ballXSpeed;
        float f2 = ballYSpeed - 1.0f;
        this.ball.speedY = ballYSpeed;
        float speedScale = ((float) this.screenWidth) / SCALE_WIDTH;
        this.zArch += 1.0f;
        if (this.fireCannon) {
            this.ball.locationX = this.ballXPos + this.ball.speedX;
            this.ballYPos += ((this.zArch * this.zArch) / this.archAdjustMent) - (((float) this.height) / speedScale);
            this.ball.locationY = this.ballYPos + this.ball.speedY;
            return;
        }
        this.ball.locationX = this.ballXPos - this.ball.speedX;
        this.ballYPos += ((this.zArch * this.zArch) / this.archAdjustMent) - (((float) this.height) / speedScale);
        this.ball.locationY = this.ballYPos + this.ball.speedY;
    }

    public void ToggleSound(boolean plySnd) {
        this.playSound = plySnd;
        if (this.playSound) {
            this.imgMute.bitmap = this.bmpMute;
            return;
        }
        this.imgMute.bitmap = this.bmpUnMute;
    }

    private void loadSounds() {
        if (this._soundPool != null) {
            this._soundPool.release();
            this._soundPool = null;
        }
        this._soundPool = new SoundPool(16, 3, 100);
        this.sndCannonFire = this._soundPool.load(getContext(), R.raw.cannon_fire, 0);
        this.sndBuildingHit = this._soundPool.load(getContext(), R.raw.cannon_ball_hits_building, 0);
        this.sndCannonHit = this._soundPool.load(getContext(), R.raw.cannon_ball_hits_cannon, 0);
        this.sndGroundHit = this._soundPool.load(getContext(), R.raw.bomb_hits_ground, 0);
    }

    private void PlaySound(int snd) {
        if (this.playSound) {
            try {
                this._soundPool.play(snd, 1.0f, 1.0f, 0, 0, 1.0f);
            } catch (Exception e) {
                loadSounds();
            }
        }
    }
}
