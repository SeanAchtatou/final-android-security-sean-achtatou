package com.vandaveer.cannonwars;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.TextView;
import java.util.Timer;
import java.util.TimerTask;

public class ShowMessage extends Dialog {
    private static final int WAIT_TIME = 3000;
    private View MessageView;
    /* access modifiers changed from: private */
    public CannonWarsPanel _panel;
    Handler blinkHandler = new Handler() {
        public void handleMessage(Message msg) {
            TextView msgView = (TextView) ShowMessage.this.findViewById(R.id.message_msg);
            switch (msg.what) {
                case 0:
                    msgView.setTextColor(-1);
                    break;
                case 1:
                    msgView.setTextColor(Color.rgb(6, 54, 103));
                    break;
            }
            super.handleMessage(msg);
        }
    };
    Handler hand = new Handler();
    ShowMessage hr;
    Timer myTimer;

    public ShowMessage(Context context, CannonWarsPanel panel) {
        super(context);
        this._panel = panel;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.show_message);
        this.MessageView = findViewById(R.id.show_message);
        ((TextView) findViewById(R.id.message_msg)).setText(this._panel.message);
        this.MessageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShowMessage.this._panel.message = null;
                ShowMessage.this.dismiss();
            }
        });
    }

    public void display() {
        show();
        this.myTimer = null;
        this.myTimer = new Timer();
        this.myTimer.schedule(new TimerTask() {
            public void run() {
                ShowMessage.this._panel.message = null;
                ShowMessage.this.dismiss();
            }
        }, 3000);
        for (int i = 0; i < 8; i++) {
            Message msg = new Message();
            if (i % 2 == 0) {
                msg.what = 0;
            } else {
                msg.what = 1;
            }
            this.blinkHandler.sendMessageDelayed(msg, (long) (i * 400));
        }
    }
}
