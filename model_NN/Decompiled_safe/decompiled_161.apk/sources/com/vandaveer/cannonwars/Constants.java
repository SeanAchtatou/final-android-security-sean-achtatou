package com.vandaveer.cannonwars;

public class Constants {
    public static final String GET_HIGH_SCORE_TABLE = "?gid=4&a=g";
    public static final String GET_LOWEST_HIGH_SCORE = "?gid=4&a=ls";
    public static final String HIGH_SCORE = "hs";
    public static final String HIGH_SCORES_QUERY_STRING = "?gid=4&a=p&s=";
    public static final String LEVEL = "level";
    public static final String PLAY_SOUND = "play_sound";
    public static final String PREFERENCE_NAME = "cannonwars";
}
