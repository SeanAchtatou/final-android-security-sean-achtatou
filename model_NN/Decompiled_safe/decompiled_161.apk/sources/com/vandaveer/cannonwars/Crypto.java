package com.vandaveer.cannonwars;

import android.util.Log;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Crypto {
    private static String CIPHER_ALGORITHM = "AES";
    private static String CIPHER_TRANSFORMATION = "AES/CBC/PKCS5Padding";
    private static String MESSAGEDIGEST_ALGORITHM = "MD5";
    public static final String TAG = "ad";
    private static Cipher aesCipher;
    private static IvParameterSpec ivParameterSpec;
    private static String pass = "!sakflsfjak*1#90";
    private static SecretKey secretKey;
    private byte[] passwordKey;

    public Crypto() {
        try {
            aesCipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "No such algorithm " + CIPHER_ALGORITHM, e);
        } catch (NoSuchPaddingException e2) {
            Log.e(TAG, "No such padding PKCS5", e2);
        }
    }

    public String encryptString(String plainText) {
        String initVector = generateIV();
        this.passwordKey = encodeDigest(pass);
        secretKey = new SecretKeySpec(this.passwordKey, CIPHER_ALGORITHM);
        ivParameterSpec = new IvParameterSpec(initVector.getBytes());
        return String.valueOf(initVector) + new String(Base64Coder.encode(encrypt(plainText.getBytes())));
    }

    public String decryptString(String cipherText) {
        String initVector = cipherText.substring(0, 16);
        String cipherText2 = cipherText.substring(16);
        this.passwordKey = encodeDigest(pass);
        secretKey = new SecretKeySpec(this.passwordKey, CIPHER_ALGORITHM);
        ivParameterSpec = new IvParameterSpec(initVector.getBytes());
        return new String(decrypt(Base64Coder.decode(cipherText2)));
    }

    private byte[] encrypt(byte[] clearData) {
        try {
            aesCipher.init(1, secretKey, ivParameterSpec);
            try {
                return aesCipher.doFinal(clearData);
            } catch (IllegalBlockSizeException e) {
                Log.e(TAG, "Illegal block size", e);
                return null;
            } catch (BadPaddingException e2) {
                Log.e(TAG, "Bad padding", e2);
                return null;
            }
        } catch (InvalidKeyException e3) {
            Log.e(TAG, "Invalid key", e3);
            return null;
        } catch (InvalidAlgorithmParameterException e4) {
            Log.e(TAG, "Invalid algorithm " + CIPHER_ALGORITHM, e4);
            return null;
        }
    }

    private byte[] decrypt(byte[] cipherData) {
        try {
            aesCipher.init(2, secretKey, ivParameterSpec);
            try {
                return aesCipher.doFinal(cipherData);
            } catch (IllegalBlockSizeException e) {
                Log.e(TAG, "Illegal block size", e);
                return null;
            } catch (BadPaddingException e2) {
                Log.e(TAG, "Bad padding", e2);
                return null;
            }
        } catch (InvalidKeyException e3) {
            Log.e(TAG, "Invalid key", e3);
            return null;
        } catch (InvalidAlgorithmParameterException e4) {
            Log.e(TAG, "Invalid algorithm " + CIPHER_ALGORITHM, e4);
            return null;
        }
    }

    private byte[] encodeDigest(String text) {
        try {
            return MessageDigest.getInstance(MESSAGEDIGEST_ALGORITHM).digest(text.getBytes());
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "No such algorithm " + MESSAGEDIGEST_ALGORITHM, e);
            return null;
        }
    }

    private String generateIV() {
        Random generator = new Random();
        String iv = "";
        for (int i = 0; i < 16; i++) {
            iv = String.valueOf(iv) + Integer.toString(generator.nextInt(9));
        }
        return iv;
    }
}
