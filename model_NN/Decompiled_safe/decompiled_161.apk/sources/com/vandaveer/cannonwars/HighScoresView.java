package com.vandaveer.cannonwars;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;

public class HighScoresView extends Dialog {
    CannonWars cannonWars;
    public boolean connectedToServer = false;
    private View highScoreView;
    private String rawServerScores;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.highscores);
        this.highScoreView = findViewById(R.id.highscores);
        populateHighScoreTable(this.rawServerScores);
        this.highScoreView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HighScoresView.this.cannonWars.setUpMainScreen();
                HighScoresView.this.dismiss();
            }
        });
    }

    public HighScoresView(Context gameContext, String serverScores, CannonWars cw) {
        super(gameContext);
        this.cannonWars = cw;
        this.rawServerScores = serverScores;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        this.cannonWars.setUpMainScreen();
        dismiss();
        return super.onKeyDown(keyCode, event);
    }

    /* Debug info: failed to restart local var, previous not found, register: 13 */
    private boolean populateHighScoreTable(String rawServerScores2) {
        try {
            ArrayList<String> parsedHighScore = new ArrayList<>();
            ArrayList<String> parsedHighScoreName = new ArrayList<>();
            ArrayList<String> parsedHighScoreCountry = new ArrayList<>();
            String[] parsedServerScoreLine = rawServerScores2.split(";");
            int j = 0;
            for (int i = 0; i < parsedServerScoreLine.length; i++) {
                switch (j) {
                    case 0:
                        parsedHighScore.add(parsedServerScoreLine[i]);
                        break;
                    case 1:
                        parsedHighScoreName.add(parsedServerScoreLine[i]);
                        break;
                    case 2:
                        parsedHighScoreCountry.add(parsedServerScoreLine[i]);
                        j = 0;
                        continue;
                }
                j++;
            }
            TextView[] highScoreViews = {(TextView) findViewById(R.id.hs_1_score), (TextView) findViewById(R.id.hs_2_score), (TextView) findViewById(R.id.hs_3_score), (TextView) findViewById(R.id.hs_4_score), (TextView) findViewById(R.id.hs_5_score), (TextView) findViewById(R.id.hs_6_score), (TextView) findViewById(R.id.hs_7_score), (TextView) findViewById(R.id.hs_8_score), (TextView) findViewById(R.id.hs_9_score), (TextView) findViewById(R.id.hs_10_score)};
            TextView[] highScoreNameViews = {(TextView) findViewById(R.id.hs_1_name), (TextView) findViewById(R.id.hs_2_name), (TextView) findViewById(R.id.hs_3_name), (TextView) findViewById(R.id.hs_4_name), (TextView) findViewById(R.id.hs_5_name), (TextView) findViewById(R.id.hs_6_name), (TextView) findViewById(R.id.hs_7_name), (TextView) findViewById(R.id.hs_8_name), (TextView) findViewById(R.id.hs_9_name), (TextView) findViewById(R.id.hs_10_name)};
            TextView[] highScoreCountryViews = {(TextView) findViewById(R.id.hs_1_country), (TextView) findViewById(R.id.hs_2_country), (TextView) findViewById(R.id.hs_3_country), (TextView) findViewById(R.id.hs_4_country), (TextView) findViewById(R.id.hs_5_country), (TextView) findViewById(R.id.hs_6_country), (TextView) findViewById(R.id.hs_7_country), (TextView) findViewById(R.id.hs_8_country), (TextView) findViewById(R.id.hs_9_country), (TextView) findViewById(R.id.hs_10_country)};
            for (int i2 = 0; i2 < parsedHighScore.size(); i2++) {
                highScoreViews[i2].setText((CharSequence) parsedHighScore.get(i2));
                highScoreNameViews[i2].setText((CharSequence) parsedHighScoreName.get(i2));
                highScoreCountryViews[i2].setText((CharSequence) parsedHighScoreCountry.get(i2));
            }
            return true;
        } catch (Exception e) {
            Exception exc = e;
            return false;
        }
    }
}
