package com.vandaveer.cannonwars;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

public class Prefs extends Dialog {
    private CannonWars _cw;
    private boolean _playSound;
    private View highScoreView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.settings);
        this.highScoreView = findViewById(R.id.highscores);
        this.highScoreView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Prefs.this.CloseView();
            }
        });
        findViewById(R.id.pref_ok_button).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Prefs.this.CloseView();
            }
        });
        ((CheckBox) findViewById(R.id.checkBoxSound)).setChecked(this._playSound);
    }

    /* access modifiers changed from: private */
    public void CloseView() {
        this._cw.ToggleSound(((CheckBox) findViewById(R.id.checkBoxSound)).isChecked());
        dismiss();
    }

    public Prefs(Context context, CannonWars ad, boolean playSound) {
        super(context);
        this._cw = ad;
        this._playSound = playSound;
    }
}
