package com.vandaveer.cannonwars;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class EnterHighScore extends Dialog {
    /* access modifiers changed from: private */
    public CannonWarsPanel cannonPanel;

    public EnterHighScore(Context context, CannonWarsPanel panel) {
        super(context);
        this.cannonPanel = panel;
    }

    /* access modifiers changed from: private */
    public void submitHighScore() {
        Context context = getContext();
        EditText edittext = (EditText) findViewById(R.id.edittext);
        String iso = ((TelephonyManager) context.getSystemService("phone")).getNetworkCountryIso();
        try {
            Settings.System.getString(context.getContentResolver(), "android_id");
        } catch (Exception e) {
        }
        String scores = new Crypto().encryptString(String.valueOf(edittext.getText().toString()) + ";" + this.cannonPanel.score + ";" + iso + ";" + "unknown");
        try {
            scores = URLEncoder.encode(scores, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        HighScores.postServerQueryString(context, Constants.HIGH_SCORES_QUERY_STRING + scores);
        this.cannonPanel.PromptForNewGame(false);
        dismiss();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.enter_high_score);
        findViewById(R.id.enter_high_score_ok_btn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EnterHighScore.this.submitHighScore();
            }
        });
        ((EditText) findViewById(R.id.edittext)).setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 0 || keyCode != 66) {
                    return false;
                }
                EnterHighScore.this.submitHighScore();
                return true;
            }
        });
        findViewById(R.id.enter_high_score_cancel_btn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EnterHighScore.this.cannonPanel.PromptForNewGame(true);
                EnterHighScore.this.dismiss();
            }
        });
    }
}
