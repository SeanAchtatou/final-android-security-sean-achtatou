package com.vandaveer.cannonwars;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

public class CustomDialog extends Dialog {
    private String _msg;
    private View _view;
    public boolean connectedToServer = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.custom_dialog);
        this._view = findViewById(R.id.custom_dialog);
        this._view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CustomDialog.this.dismiss();
            }
        });
        findViewById(R.id.custom_dialog_ok_button).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CustomDialog.this.dismiss();
            }
        });
        ((TextView) findViewById(R.id.custom_dialog_text)).setText(this._msg);
    }

    public CustomDialog(Context gameContext, String msg) {
        super(gameContext);
        this._msg = msg;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        dismiss();
        return super.onKeyDown(keyCode, event);
    }
}
