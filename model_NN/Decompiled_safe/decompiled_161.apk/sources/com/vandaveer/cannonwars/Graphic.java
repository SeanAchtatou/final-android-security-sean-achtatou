package com.vandaveer.cannonwars;

import android.graphics.Bitmap;

public class Graphic {
    private Speed _speed;
    private String _type;
    public Bitmap bitmap;
    public long displayTime;
    public float locationX = 0.0f;
    public float locationY = 0.0f;
    public float speedX = 0.0f;
    public float speedY = 0.0f;

    public float getTouchedX() {
        return this.locationX + ((float) (this.bitmap.getWidth() / 2));
    }

    public void setX(float value) {
        this.locationX = value;
    }

    public float getTouchedY() {
        return this.locationY + ((float) (this.bitmap.getHeight() / 2));
    }

    public void setTouchedY(float value) {
        this.locationY = value - ((float) (this.bitmap.getHeight() / 2));
    }

    public class Speed {
        private float _x = 0.0f;
        private float _y = 0.0f;

        public Speed() {
        }

        public float getX() {
            return this._x;
        }

        public void setX(float speed) {
            this._x = speed;
        }

        public float getY() {
            return this._y;
        }

        public void setY(float speed) {
            this._y = speed;
        }

        public String toString() {
            return "Speed: x: " + this._x + " | y: " + this._y;
        }
    }

    public Graphic(Bitmap bp) {
        this.bitmap = bp;
        this._speed = new Speed();
    }

    public Speed getSpeed() {
        return this._speed;
    }

    public void setType(String type) {
        this._type = type;
    }

    public String getType() {
        return this._type;
    }
}
