package com.vandaveer.cannonwars;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class Game extends Activity {
    CannonWarsPanel panel;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(0);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        Bundle bundle = getIntent().getExtras();
        int lowestHighScore = bundle.getInt("lowestHighScore");
        this.panel = new CannonWarsPanel(this, bundle.getInt(Constants.LEVEL));
        this.panel.highscore = getSharedPreferences(Constants.PREFERENCE_NAME, 0).getInt(Constants.HIGH_SCORE, 0);
        this.panel.playSound = getSharedPreferences(Constants.PREFERENCE_NAME, 0).getBoolean(Constants.PLAY_SOUND, true);
        this.panel.gameLevel = getSharedPreferences(Constants.PREFERENCE_NAME, 0).getInt(Constants.LEVEL, 0);
        this.panel.lowestHighScore = lowestHighScore;
        setContentView(this.panel);
        ShowAd();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.panel != null) {
            getSharedPreferences(Constants.PREFERENCE_NAME, 0).edit().putInt(Constants.HIGH_SCORE, this.panel.highscore).commit();
            getSharedPreferences(Constants.PREFERENCE_NAME, 0).edit().putInt(Constants.LEVEL, this.panel.gameLevel).commit();
            getSharedPreferences(Constants.PREFERENCE_NAME, 0).edit().putBoolean(Constants.PLAY_SOUND, this.panel.playSound).commit();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.panel == null) {
            return false;
        }
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onMenuOpened(int featureId, Menu menu) {
        this.panel.PauseGame(true);
        MenuItem item = menu.findItem(R.id.sound);
        if (this.panel.playSound) {
            item.setTitle((int) R.string.settings_sound_mute);
        } else {
            item.setTitle((int) R.string.settings_sound_unmute);
        }
        super.onMenuOpened(featureId, menu);
        return true;
    }

    public void onOptionsMenuClosed(Menu menu) {
        this.panel.PauseGame(false);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        this.panel.PauseGame(true);
        switch (item.getItemId()) {
            case R.id.end_game /*2131230781*/:
                finish();
                return true;
            case R.id.resume_game /*2131230782*/:
                this.panel.PauseGame(false);
                return true;
            case R.id.sound /*2131230783*/:
                if (this.panel.playSound) {
                    item.setTitle((int) R.string.settings_sound_unmute);
                    this.panel.ToggleSound(false);
                } else {
                    item.setTitle((int) R.string.settings_sound_mute);
                    this.panel.ToggleSound(true);
                }
                this.panel.PauseGame(false);
                return true;
            default:
                return false;
        }
    }

    public void ShowAd() {
        try {
            LinearLayout layout = new LinearLayout(this);
            layout.setOrientation(1);
            RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(-2, -2);
            adParams.addRule(9);
            addContentView(layout, new ViewGroup.LayoutParams(-1, -1));
            AdView adView = new AdView(this, AdSize.BANNER, "a14d01035408c8c");
            layout.addView(adView, adParams);
            adView.loadAd(new AdRequest());
        } catch (Exception e) {
        }
    }
}
