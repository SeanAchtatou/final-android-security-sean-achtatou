package com.vandaveer.cannonwars;

public class Base64Coder {
    private static char[] map1 = new char[64];
    private static byte[] map2 = new byte[128];
    private static final String systemLineSeparator = System.getProperty("line.separator");

    static {
        int i;
        int i2 = 0;
        char c = 'A';
        while (true) {
            i = i2;
            if (c > 'Z') {
                break;
            }
            i2 = i + 1;
            map1[i] = c;
            c = (char) (c + 1);
        }
        char c2 = 'a';
        while (c2 <= 'z') {
            map1[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = '0';
        while (c3 <= '9') {
            map1[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        int i3 = i + 1;
        map1[i] = '+';
        int i4 = i3 + 1;
        map1[i3] = '/';
        for (int i5 = 0; i5 < map2.length; i5++) {
            map2[i5] = -1;
        }
        for (int i6 = 0; i6 < 64; i6++) {
            map2[map1[i6]] = (byte) i6;
        }
    }

    public static String encodeString(String s) {
        return new String(encode(s.getBytes()));
    }

    public static String encodeLines(byte[] in) {
        return encodeLines(in, 0, in.length, 76, systemLineSeparator);
    }

    public static String encodeLines(byte[] in, int iOff, int iLen, int lineLen, String lineSeparator) {
        int blockLen = (lineLen * 3) / 4;
        if (blockLen <= 0) {
            throw new IllegalArgumentException();
        }
        StringBuilder buf = new StringBuilder((((iLen + 2) / 3) * 4) + (lineSeparator.length() * (((iLen + blockLen) - 1) / blockLen)));
        int ip = 0;
        while (ip < iLen) {
            int l = Math.min(iLen - ip, blockLen);
            buf.append(encode(in, iOff + ip, l));
            buf.append(lineSeparator);
            ip += l;
        }
        return buf.toString();
    }

    public static char[] encode(byte[] in) {
        return encode(in, 0, in.length);
    }

    public static char[] encode(byte[] in, int iLen) {
        return encode(in, 0, iLen);
    }

    /* JADX INFO: Multiple debug info for r0v5 int: [D('oLen' int), D('ip' int)] */
    /* JADX INFO: Multiple debug info for r11v8 int: [D('o1' int), D('i0' int)] */
    /* JADX INFO: Multiple debug info for r12v6 int: [D('i1' int), D('o2' int)] */
    /* JADX INFO: Multiple debug info for r0v9 int: [D('i2' int), D('o3' int)] */
    public static char[] encode(byte[] in, int iOff, int iLen) {
        int ip;
        int i1;
        int ip2;
        int oDataLen = ((iLen * 4) + 2) / 3;
        char[] out = new char[(((iLen + 2) / 3) * 4)];
        int iEnd = iOff + iLen;
        int op = 0;
        int op2 = iOff;
        while (op2 < iEnd) {
            int ip3 = op2 + 1;
            int i0 = in[op2] & 255;
            if (ip3 < iEnd) {
                int ip4 = ip3 + 1;
                i1 = in[ip3] & 255;
                ip = ip4;
            } else {
                ip = ip3;
                i1 = 0;
            }
            if (ip < iEnd) {
                int i = in[ip] & 255;
                ip++;
                ip2 = i;
            } else {
                ip2 = 0;
            }
            int o0 = i0 >>> 2;
            int i02 = ((i0 & 3) << 4) | (i1 >>> 4);
            int o2 = ((i1 & 15) << 2) | (ip2 >>> 6);
            int o3 = ip2 & 63;
            int op3 = op + 1;
            out[op] = map1[o0];
            int op4 = op3 + 1;
            out[op3] = map1[i02];
            out[op4] = op4 < oDataLen ? map1[o2] : '=';
            int op5 = op4 + 1;
            out[op5] = op5 < oDataLen ? map1[o3] : '=';
            op = op5 + 1;
            op2 = ip;
        }
        return out;
    }

    public static String decodeString(String s) {
        return new String(decode(s));
    }

    public static byte[] decodeLines(String s) {
        char[] buf = new char[s.length()];
        int p = 0;
        for (int ip = 0; ip < s.length(); ip++) {
            char c = s.charAt(ip);
            if (!(c == ' ' || c == 13 || c == 10 || c == 9)) {
                buf[p] = c;
                p++;
            }
        }
        return decode(buf, 0, p);
    }

    public static byte[] decode(String s) {
        return decode(s.toCharArray());
    }

    public static byte[] decode(char[] in) {
        return decode(in, 0, in.length);
    }

    /* JADX INFO: Multiple debug info for r10v4 char: [D('ip' int), D('i0' int)] */
    /* JADX INFO: Multiple debug info for r11v3 char: [D('i1' int), D('ip' int)] */
    /* JADX INFO: Multiple debug info for r10v6 byte: [D('b0' int), D('i0' int)] */
    /* JADX INFO: Multiple debug info for r11v4 byte: [D('i1' int), D('b1' int)] */
    /* JADX INFO: Multiple debug info for r0v6 byte: [D('i2' int), D('b2' int)] */
    /* JADX INFO: Multiple debug info for r1v3 byte: [D('i3' int), D('b3' int)] */
    /* JADX INFO: Multiple debug info for r10v9 int: [D('b0' int), D('o0' int)] */
    /* JADX INFO: Multiple debug info for r11v7 int: [D('o1' int), D('b1' int)] */
    /* JADX INFO: Multiple debug info for r0v9 byte: [D('b2' int), D('o2' int)] */
    /* JADX INFO: Multiple debug info for r1v4 int: [D('b3' int), D('op' int)] */
    public static byte[] decode(char[] in, int iOff, int iLen) {
        int ip;
        char c;
        int ip2;
        int o1;
        if (iLen % 4 != 0) {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }
        while (iLen > 0 && in[(iOff + iLen) - 1] == '=') {
            iLen--;
        }
        int oLen = (iLen * 3) / 4;
        byte[] out = new byte[oLen];
        int iEnd = iOff + iLen;
        int op = 0;
        int op2 = iOff;
        while (op2 < iEnd) {
            int ip3 = op2 + 1;
            char c2 = in[op2];
            int ip4 = ip3 + 1;
            char c3 = in[ip3];
            if (ip4 < iEnd) {
                int ip5 = ip4 + 1;
                c = in[ip4];
                ip = ip5;
            } else {
                ip = ip4;
                c = 'A';
            }
            if (ip < iEnd) {
                int i = in[ip];
                ip++;
                ip2 = i;
            } else {
                ip2 = 65;
            }
            if (c2 > 127 || c3 > 127 || c > 127 || ip2 > 127) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            byte b = map2[c2];
            byte b2 = map2[c3];
            byte b3 = map2[c];
            byte b4 = map2[ip2];
            if (b < 0 || b2 < 0 || b3 < 0 || b4 < 0) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int o0 = (b << 2) | (b2 >>> 4);
            int b1 = ((b2 & 15) << 4) | (b3 >>> 2);
            int o2 = ((b3 & 3) << 6) | b4;
            int op3 = op + 1;
            out[op] = (byte) o0;
            if (op3 < oLen) {
                out[op3] = (byte) b1;
                o1 = op3 + 1;
            } else {
                o1 = op3;
            }
            if (o1 < oLen) {
                out[o1] = (byte) o2;
                op = o1 + 1;
                op2 = ip;
            } else {
                op = o1;
                op2 = ip;
            }
        }
        return out;
    }

    private Base64Coder() {
    }
}
