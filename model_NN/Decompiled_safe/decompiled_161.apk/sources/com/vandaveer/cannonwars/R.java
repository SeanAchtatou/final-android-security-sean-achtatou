package com.vandaveer.cannonwars;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class color {
        public static final int background = 2131034112;
    }

    public static final class drawable {
        public static final int arrowd = 2130837504;
        public static final int arrowl = 2130837505;
        public static final int arrowr = 2130837506;
        public static final int arrowu = 2130837507;
        public static final int b1 = 2130837508;
        public static final int bg = 2130837509;
        public static final int bgsplash = 2130837510;
        public static final int bombl = 2130837511;
        public static final int bombr = 2130837512;
        public static final int btn_blue = 2130837513;
        public static final int cannonball = 2130837514;
        public static final int cannonl = 2130837515;
        public static final int cannonr = 2130837516;
        public static final int exit = 2130837517;
        public static final int exp0 = 2130837518;
        public static final int exp1 = 2130837519;
        public static final int exp2 = 2130837520;
        public static final int fire = 2130837521;
        public static final int firel2 = 2130837522;
        public static final int firer2 = 2130837523;
        public static final int icon = 2130837524;
        public static final int mute = 2130837525;
        public static final int mybutton_background = 2130837526;
        public static final int planel = 2130837527;
        public static final int planer = 2130837528;
        public static final int playercannonfire = 2130837529;
        public static final int redarrowd = 2130837530;
        public static final int redarrowl = 2130837531;
        public static final int redarrowr = 2130837532;
        public static final int redarrowu = 2130837533;
        public static final int redfire = 2130837534;
        public static final int unmute = 2130837535;
    }

    public static final class id {
        public static final int checkBoxSound = 2131230777;
        public static final int choose_button = 2131230772;
        public static final int custom_dialog = 2131230720;
        public static final int custom_dialog_ok_button = 2131230722;
        public static final int custom_dialog_text = 2131230721;
        public static final int edittext = 2131230725;
        public static final int end_game = 2131230781;
        public static final int enter_high_score_cancel_btn = 2131230727;
        public static final int enter_high_score_ok_btn = 2131230726;
        public static final int exit_button = 2131230776;
        public static final int highscores = 2131230723;
        public static final int highscores_button = 2131230774;
        public static final int hs_10_country = 2131230757;
        public static final int hs_10_name = 2131230756;
        public static final int hs_10_score = 2131230755;
        public static final int hs_1_country = 2131230730;
        public static final int hs_1_name = 2131230729;
        public static final int hs_1_score = 2131230728;
        public static final int hs_2_country = 2131230733;
        public static final int hs_2_name = 2131230732;
        public static final int hs_2_score = 2131230731;
        public static final int hs_3_country = 2131230736;
        public static final int hs_3_name = 2131230735;
        public static final int hs_3_score = 2131230734;
        public static final int hs_4_country = 2131230739;
        public static final int hs_4_name = 2131230738;
        public static final int hs_4_score = 2131230737;
        public static final int hs_5_country = 2131230742;
        public static final int hs_5_name = 2131230741;
        public static final int hs_5_score = 2131230740;
        public static final int hs_6_country = 2131230745;
        public static final int hs_6_name = 2131230744;
        public static final int hs_6_score = 2131230743;
        public static final int hs_7_country = 2131230748;
        public static final int hs_7_name = 2131230747;
        public static final int hs_7_score = 2131230746;
        public static final int hs_8_country = 2131230751;
        public static final int hs_8_name = 2131230750;
        public static final int hs_8_score = 2131230749;
        public static final int hs_9_country = 2131230754;
        public static final int hs_9_name = 2131230753;
        public static final int hs_9_score = 2131230752;
        public static final int hs_title = 2131230724;
        public static final int instructions_button = 2131230775;
        public static final int keypad = 2131230758;
        public static final int keypad_1 = 2131230759;
        public static final int keypad_10 = 2131230768;
        public static final int keypad_11 = 2131230769;
        public static final int keypad_12 = 2131230770;
        public static final int keypad_2 = 2131230760;
        public static final int keypad_3 = 2131230761;
        public static final int keypad_4 = 2131230762;
        public static final int keypad_5 = 2131230763;
        public static final int keypad_6 = 2131230764;
        public static final int keypad_7 = 2131230765;
        public static final int keypad_8 = 2131230766;
        public static final int keypad_9 = 2131230767;
        public static final int message_msg = 2131230780;
        public static final int new_button = 2131230771;
        public static final int pref_ok_button = 2131230778;
        public static final int resume_game = 2131230782;
        public static final int settings_button = 2131230773;
        public static final int show_message = 2131230779;
        public static final int sound = 2131230783;
    }

    public static final class layout {
        public static final int custom_dialog = 2130903040;
        public static final int enter_high_score = 2130903041;
        public static final int highscores = 2130903042;
        public static final int keypad = 2130903043;
        public static final int main = 2130903044;
        public static final int settings = 2130903045;
        public static final int show_message = 2130903046;
        public static final int splash = 2130903047;
    }

    public static final class menu {
        public static final int menu = 2131165184;
    }

    public static final class raw {
        public static final int bomb_hits_ground = 2130968576;
        public static final int cannon_ball_hits_building = 2130968577;
        public static final int cannon_ball_hits_cannon = 2130968578;
        public static final int cannon_fire = 2130968579;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int choose_level_label = 2131099652;
        public static final int choose_level_title = 2131099663;
        public static final int exit_label = 2131099654;
        public static final int game_over_text = 2131099659;
        public static final int highScoreServer = 2131099648;
        public static final int instructions_label = 2131099653;
        public static final int instructions_text = 2131099664;
        public static final int main_title = 2131099650;
        public static final int new_game_label = 2131099651;
        public static final int settings_end_game_label = 2131099655;
        public static final int settings_resume_game_label = 2131099656;
        public static final int settings_sound_mute = 2131099657;
        public static final int settings_sound_unmute = 2131099658;
        public static final int settings_title = 2131099660;
        public static final int sound_summary = 2131099662;
        public static final int sound_title = 2131099661;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
