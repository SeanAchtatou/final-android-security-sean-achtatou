package com.vandaveer.cannonwars;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;

public class CannonWars extends Activity implements View.OnClickListener {
    CannonWarsPanel cannonWarsPanel;
    public ProgressDialog loadingDialog;
    private int lowestHighScore = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(0);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        showHighScoresView();
    }

    private void showHighScoresView() {
        setContentView((int) R.layout.splash);
        new InitAsyncTask(this, null).execute(new Void[0]);
        this.loadingDialog = new ProgressDialog(this);
        this.loadingDialog.setCancelable(true);
        this.loadingDialog.setMessage("Loading...");
        this.loadingDialog.show();
    }

    public void ToggleSound(boolean playSound) {
        getSharedPreferences(Constants.PREFERENCE_NAME, 0).edit().putBoolean(Constants.PLAY_SOUND, playSound).commit();
    }

    public void setUpMainScreen() {
        setContentView((int) R.layout.main);
        findViewById(R.id.new_button).setOnClickListener(this);
        findViewById(R.id.choose_button).setOnClickListener(this);
        findViewById(R.id.settings_button).setOnClickListener(this);
        findViewById(R.id.instructions_button).setOnClickListener(this);
        findViewById(R.id.highscores_button).setOnClickListener(this);
        findViewById(R.id.exit_button).setOnClickListener(this);
    }

    public void showHighScores(String rawServerScores) {
        if (rawServerScores != null) {
            String slowestHighScore = HighScores.getServerResponse(this, Constants.GET_LOWEST_HIGH_SCORE);
            if (slowestHighScore != null) {
                this.lowestHighScore = Integer.parseInt(slowestHighScore);
                new HighScoresView(this, rawServerScores, this).show();
                return;
            }
            setUpMainScreen();
            return;
        }
        setUpMainScreen();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.new_button /*2131230771*/:
                startGame(1);
                return;
            case R.id.choose_button /*2131230772*/:
                try {
                    new Keypad(this, this, getSharedPreferences(Constants.PREFERENCE_NAME, 0).getInt(Constants.LEVEL, 1)).show();
                    return;
                } catch (Exception e) {
                    return;
                }
            case R.id.settings_button /*2131230773*/:
                new Prefs(this, this, getSharedPreferences(Constants.PREFERENCE_NAME, 0).getBoolean(Constants.PLAY_SOUND, true)).show();
                return;
            case R.id.highscores_button /*2131230774*/:
                showHighScoresView();
                return;
            case R.id.instructions_button /*2131230775*/:
                new CustomDialog(this, getString(R.string.instructions_text)).show();
                return;
            case R.id.exit_button /*2131230776*/:
                finish();
                return;
            default:
                return;
        }
    }

    public void startGame(int level) {
        Bundle bundle = new Bundle();
        bundle.putInt("lowestHighScore", this.lowestHighScore);
        bundle.putInt(Constants.LEVEL, level);
        Intent i = new Intent(this, Game.class);
        i.putExtras(bundle);
        startActivityForResult(i, 0);
    }

    private class InitAsyncTask extends AsyncTask<Void, Void, String> {
        private InitAsyncTask() {
        }

        /* synthetic */ InitAsyncTask(CannonWars cannonWars, InitAsyncTask initAsyncTask) {
            this();
        }

        public String doInBackground(Void... params) {
            return HighScores.getServerResponse(CannonWars.this.getBaseContext(), Constants.GET_HIGH_SCORE_TABLE);
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String scores) {
            if (scores != null) {
                CannonWars.this.showHighScores(scores);
            } else {
                CannonWars.this.setUpMainScreen();
            }
            if (CannonWars.this.loadingDialog != null) {
                CannonWars.this.loadingDialog.dismiss();
            }
        }
    }
}
