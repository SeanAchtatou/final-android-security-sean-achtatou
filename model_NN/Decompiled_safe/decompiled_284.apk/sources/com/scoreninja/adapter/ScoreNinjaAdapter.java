package com.scoreninja.adapter;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

public final class ScoreNinjaAdapter {
    private static final String ACTIVITY_CLASS = "com.scoreninja.ScoreNinja";
    private static final String KEY_APP_ID = "appid";
    private static final String KEY_COMMENTS = "comments";
    private static final String KEY_NAME = "name";
    private static final String KEY_NEW_SCORE = "newscore";
    private static final String KEY_SIGNATURE = "signature";
    private static final String KEY_SUB_BOARD = "subboard";
    private static final String KEY_TITLE = "title";
    static final String NEVER_ASK_PREF = "neverask";
    public static final String PACKAGE = "com.scoreninja";
    static final String PREFS_NAME = "ScoreNinjaPrefs";
    private static final int REQUEST_CODE = 987656789;
    private static final String SERVICE_CLASS = "com.scoreninja.ScoreNinjaService";
    private final String appId;
    private final Activity parent;
    private final RequestSigner signer;

    public ScoreNinjaAdapter(Context context, String appId2, String privateKey) {
        this.parent = (Activity) context;
        this.appId = appId2;
        this.signer = new RequestSigner(privateKey);
    }

    public void show() {
        show(0);
    }

    public void show(int newScore) {
        show(newScore, null, null);
    }

    public void show(int newScore, String titleText, String subBoard) {
        if (isInstalled(this.parent)) {
            Intent i = new Intent();
            i.setAction("android.intent.action.VIEW");
            i.addCategory("android.intent.category.DEFAULT");
            i.setFlags(69206016);
            i.setClassName(PACKAGE, ACTIVITY_CLASS);
            i.putExtra(KEY_APP_ID, this.appId);
            if (titleText != null) {
                i.putExtra(KEY_TITLE, titleText);
            }
            i.putExtra(KEY_NEW_SCORE, newScore);
            if (subBoard != null) {
                i.putExtra(KEY_SUB_BOARD, subBoard);
            }
            i.putExtra(KEY_SIGNATURE, this.signer.getSignature(String.valueOf(this.appId) + newScore + (subBoard != null ? subBoard : "")));
            try {
                this.parent.startActivityIfNeeded(i, REQUEST_CODE);
            } catch (ActivityNotFoundException e) {
            }
        } else if (!neverAskAgain(this.parent)) {
            new ScoreNinjaInstallAlert(this.parent).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == -1 && requestCode == REQUEST_CODE && intent != null) {
            int newScore = intent.getIntExtra(KEY_NEW_SCORE, 0);
            String signature = intent.getStringExtra(KEY_SIGNATURE);
            String subBoard = intent.getStringExtra(KEY_SUB_BOARD);
            String scoreSignature = this.signer.getSignature(String.valueOf(this.appId) + newScore + (subBoard != null ? subBoard : ""));
            if (newScore >= 1 && scoreSignature.equals(signature)) {
                Intent newIntent = new Intent();
                newIntent.setClassName(PACKAGE, SERVICE_CLASS);
                newIntent.setAction("android.intent.action.SEND");
                newIntent.putExtra(KEY_APP_ID, this.appId);
                newIntent.putExtra(KEY_NEW_SCORE, newScore);
                if (subBoard != null) {
                    newIntent.putExtra(KEY_SUB_BOARD, subBoard);
                }
                if (!intent.hasExtra(KEY_NAME) || !intent.hasExtra(KEY_COMMENTS)) {
                    newIntent.putExtra(KEY_SIGNATURE, scoreSignature);
                } else {
                    String name = intent.getStringExtra(KEY_NAME);
                    String comments = intent.getStringExtra(KEY_COMMENTS);
                    newIntent.putExtra(KEY_NAME, name);
                    newIntent.putExtra(KEY_COMMENTS, comments);
                    newIntent.putExtra(KEY_SIGNATURE, this.signer.getSignature(String.valueOf(this.appId) + newScore + (subBoard != null ? subBoard : "") + name + comments));
                }
                this.parent.startService(newIntent);
            }
        }
    }

    public static boolean neverAskAgain(Context ctx) {
        return ctx.getSharedPreferences(PREFS_NAME, 0).getBoolean(NEVER_ASK_PREF, false);
    }

    public static boolean isInstalled(Context ctx) {
        try {
            Context createPackageContext = ctx.createPackageContext(PACKAGE, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
