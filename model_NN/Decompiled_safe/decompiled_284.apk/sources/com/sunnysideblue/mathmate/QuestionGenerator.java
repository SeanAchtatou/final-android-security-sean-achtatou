package com.sunnysideblue.mathmate;

import android.content.Context;
import com.scoreloop.client.android.core.controller.RequestControllerException;
import com.scoreloop.client.android.ui.component.base.Constant;
import java.util.ArrayList;
import java.util.Random;

public class QuestionGenerator {
    private static int ADD_MASK = 8;
    public static final int CHAL_MODE_OFF = 0;
    public static final int CHAL_MODE_ON = 1;
    private static int DIV_MASK = 1;
    private static int MUL_MASK = 2;
    private static int SUB_MASK = 4;
    private static int ZERO_MASK = 0;
    static int cntSameRightWrong = 0;
    static int gameLevel = 0;
    static int prevRightWrong = 0;
    boolean adds = false;
    boolean divs = false;
    boolean muls = false;
    private OneQuestion oneQ;
    Prefs optionPrefs = new Prefs();
    ArrayList<OneQuestion> questionList = new ArrayList<>();
    boolean subs = false;

    QuestionGenerator(Context cxt, int chal_mode) {
        if (chal_mode == 1) {
            this.adds = true;
            this.subs = true;
            this.muls = true;
            this.divs = false;
            return;
        }
        this.adds = Prefs.getAddition(cxt);
        this.subs = Prefs.getSubtraction(cxt);
        this.muls = Prefs.getMultiplication(cxt);
        this.divs = Prefs.getDivision(cxt);
    }

    public static int randNextInt(int low, int high) {
        return Math.min(low, high) + new Random().nextInt(Math.abs(high - low));
    }

    public int rightWrongFlagGen() {
        int returnVal;
        int returnVal2 = 0;
        switch (((int) (50.0d * Math.random())) + 1) {
            case 1:
            case 3:
            case 5:
            case 7:
            case Constant.LIST_ITEM_TYPE_CHALLENGE_STAKE_AND_MODE:
            case 12:
            case 14:
            case 16:
            case 18:
            case 20:
            case Constant.LIST_ITEM_TYPE_SCORE_HIGHLIGHTED:
            case Constant.LIST_ITEM_TYPE_STANDARD:
            case Constant.LIST_ITEM_TYPE_USER_ADD_BUDDIES:
            case Constant.LIST_ITEM_TYPE_USER_DETAIL:
            case Constant.LIST_ITEM_TYPE_X_COUNT:
            case 32:
            case 34:
            case 36:
            case 38:
            case RequestControllerException.CODE_BUDDY_ADD_REQUEST_ALREADY_ADDED:
            case RequestControllerException.CODE_BUDDY_REMOVE_REQUEST_ALREADY_REMOVED:
            case 43:
            case 45:
            case 47:
            case 49:
                returnVal2 = 0;
                break;
            case 2:
            case 4:
            case 6:
            case 8:
            case 10:
            case 11:
            case 13:
            case 15:
            case 17:
            case 19:
            case Constant.LIST_ITEM_TYPE_SCORE_SUBMIT_LOCAL:
            case Constant.LIST_ITEM_TYPE_USER:
            case Constant.LIST_ITEM_TYPE_USER_ADD_BUDDY:
            case Constant.LIST_ITEM_TYPE_USER_FIND_MATCH:
            case 30:
            case 31:
            case 33:
            case 35:
            case 37:
            case 39:
            case 42:
            case 44:
            case 46:
            case 48:
            case ListGameString.FIFTYITEMS:
                returnVal2 = 1;
                break;
        }
        if (prevRightWrong == returnVal) {
            cntSameRightWrong++;
        }
        if (cntSameRightWrong > 3) {
            returnVal = returnVal == 0 ? 1 : 0;
            cntSameRightWrong = 0;
        }
        prevRightWrong = returnVal;
        return returnVal;
    }

    public void qGenerator(int noOfItems, int gameLevel2, int chall_mode) {
        int mask = 0;
        int no = 0;
        gameLevel = gameLevel2;
        int[] opArray = {ZERO_MASK, ADD_MASK, SUB_MASK, MUL_MASK, DIV_MASK};
        if (this.adds) {
            mask = 0 + ADD_MASK;
        }
        if (this.subs) {
            mask += SUB_MASK;
        }
        if (this.muls) {
            mask += MUL_MASK;
        }
        if (this.divs) {
            mask += DIV_MASK;
        }
        while (no < noOfItems) {
            int opCode = ((int) (4.0d * Math.random())) + 1;
            switch (gameLevel2) {
                case 0:
                    if ((opArray[opCode] & mask) != opArray[opCode]) {
                        break;
                    } else {
                        this.oneQ = new OneQuestion(((int) (9.0d * Math.random())) + 1, opCode, ((int) (9.0d * Math.random())) + 1, rightWrongFlagGen(), randNextInt(1, 10));
                        this.questionList.add(this.oneQ);
                        no++;
                        break;
                    }
                case 1:
                    if ((opArray[opCode] & mask) != opArray[opCode]) {
                        break;
                    } else {
                        int tempFirstNo = randNextInt(10, 99);
                        if (opCode == 3) {
                            this.oneQ = new OneQuestion(((int) (9.0d * Math.random())) + 1, opCode, ((int) (9.0d * Math.random())) + 1, rightWrongFlagGen(), randNextInt(1, 10));
                        } else {
                            this.oneQ = new OneQuestion(tempFirstNo, opCode, ((int) (9.0d * Math.random())) + 1, rightWrongFlagGen(), randNextInt(1, 10));
                        }
                        this.questionList.add(this.oneQ);
                        no++;
                        break;
                    }
                case 2:
                    if ((opArray[opCode] & mask) != opArray[opCode]) {
                        break;
                    } else {
                        int tempFirstNo2 = randNextInt(10, 99);
                        int tempSecondNo = randNextInt(10, 99);
                        if (opCode == 3) {
                            this.oneQ = new OneQuestion(tempFirstNo2, opCode, randNextInt(1, 9), rightWrongFlagGen(), randNextInt(1, 10));
                        } else {
                            this.oneQ = new OneQuestion(tempFirstNo2, opCode, tempSecondNo, rightWrongFlagGen(), randNextInt(1, 10));
                        }
                        this.questionList.add(this.oneQ);
                        no++;
                        break;
                    }
            }
        }
    }

    public ArrayList<OneQuestion> getQuestions() {
        return this.questionList;
    }

    public class OneQuestion {
        int arithOp = 0;
        int firstNo = 0;
        int result = 0;
        int rightwrongFlag = 0;
        int secondNo = 0;
        int wrongAnsPattern = 0;

        OneQuestion(int firstNo2, int arithOp2, int secondNo2, int rightwrongFlag2, int wrongAnsPattern2) {
            this.firstNo = firstNo2;
            this.arithOp = arithOp2;
            this.secondNo = secondNo2;
            this.rightwrongFlag = rightwrongFlag2;
            this.wrongAnsPattern = wrongAnsPattern2;
            genAlgorithm();
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        private void genAlgorithm() {
            int tempFirstNo = this.firstNo;
            int tempSecondNo = this.secondNo;
            if (this.rightwrongFlag == 1) {
                switch (this.arithOp) {
                    case 1:
                        this.result = this.firstNo + this.secondNo;
                        return;
                    case 2:
                        this.result = this.firstNo - this.secondNo;
                        return;
                    case 3:
                        this.result = this.firstNo * this.secondNo;
                        return;
                    case 4:
                        int tempResult = tempFirstNo * tempSecondNo;
                        if (tempResult == 0) {
                            tempFirstNo += ((int) (Math.random() * 9.0d)) + 1;
                            tempResult = tempFirstNo * tempSecondNo;
                        }
                        this.result = tempFirstNo;
                        this.firstNo = tempResult;
                        this.secondNo = tempSecondNo;
                        return;
                    default:
                        return;
                }
            } else {
                switch (this.arithOp) {
                    case 1:
                        switch (this.wrongAnsPattern) {
                            case 1:
                            case 6:
                                this.result = (this.firstNo + this.secondNo) - 1;
                                return;
                            case 2:
                            case 7:
                                this.result = this.firstNo + this.secondNo + 1;
                                return;
                            case 3:
                            case 8:
                                this.result = this.firstNo + this.secondNo + 2;
                                return;
                            case 4:
                            case Constant.LIST_ITEM_TYPE_CHALLENGE_STAKE_AND_MODE:
                                this.result = (this.firstNo + this.secondNo) - 2;
                                return;
                            case 5:
                            case 10:
                                this.result = this.firstNo * this.secondNo;
                                return;
                            default:
                                return;
                        }
                    case 2:
                        switch (this.wrongAnsPattern) {
                            case 1:
                            case 6:
                                this.result = (this.firstNo - this.secondNo) - 1;
                                return;
                            case 2:
                            case 7:
                                this.result = (this.firstNo - this.secondNo) + 1;
                                return;
                            case 3:
                            case 8:
                                if (this.firstNo - this.secondNo == 0) {
                                    this.result = (this.firstNo - this.secondNo) + 1;
                                    return;
                                } else {
                                    this.result = (this.firstNo - this.secondNo) * -1;
                                    return;
                                }
                            case 4:
                            case Constant.LIST_ITEM_TYPE_CHALLENGE_STAKE_AND_MODE:
                                this.result = this.firstNo - (this.secondNo * -1);
                                return;
                            case 5:
                            case 10:
                                this.result = (this.firstNo - this.secondNo) + 2;
                                return;
                            default:
                                return;
                        }
                    case 3:
                        switch (this.wrongAnsPattern) {
                            case 1:
                            case 6:
                                if (QuestionGenerator.gameLevel == 2) {
                                    this.result = (this.firstNo * this.secondNo) + 100;
                                    return;
                                } else if (QuestionGenerator.gameLevel == 1) {
                                    this.result = (this.firstNo * this.secondNo) + 10;
                                    return;
                                } else {
                                    this.result = (this.firstNo + 1) * this.secondNo;
                                    return;
                                }
                            case 2:
                            case 7:
                                if (QuestionGenerator.gameLevel == 2) {
                                    this.result = (this.firstNo + 10) * (this.secondNo - 10);
                                    return;
                                } else if (QuestionGenerator.gameLevel == 1) {
                                    this.result = (this.firstNo + 10) * this.secondNo;
                                    return;
                                } else {
                                    this.result = this.firstNo * (this.secondNo + 1);
                                    return;
                                }
                            case 3:
                            case 8:
                                if (QuestionGenerator.gameLevel == 2) {
                                    this.result = (this.firstNo * this.secondNo) - 100;
                                    return;
                                } else if (QuestionGenerator.gameLevel == 1) {
                                    this.result = (this.firstNo * this.secondNo) - 10;
                                    return;
                                } else {
                                    this.result = this.firstNo * (this.secondNo + this.secondNo);
                                    return;
                                }
                            case 4:
                            case Constant.LIST_ITEM_TYPE_CHALLENGE_STAKE_AND_MODE:
                                if (QuestionGenerator.gameLevel == 2) {
                                    this.result = this.firstNo * (this.secondNo - 10);
                                    return;
                                } else if (QuestionGenerator.gameLevel == 1) {
                                    this.result = (this.firstNo - 10) * this.secondNo;
                                    return;
                                } else {
                                    this.result = this.firstNo * (this.secondNo - 1);
                                    return;
                                }
                            case 5:
                            case 10:
                                this.result = Integer.parseInt(String.valueOf(String.valueOf(this.firstNo)) + String.valueOf(this.secondNo));
                                return;
                            default:
                                return;
                        }
                    case 4:
                        int tempResult2 = tempFirstNo * tempSecondNo;
                        if (tempResult2 == 0) {
                            tempFirstNo += ((int) (Math.random() * 9.0d)) + 1;
                            tempResult2 = tempFirstNo * tempSecondNo;
                        }
                        this.result = tempFirstNo;
                        this.firstNo = tempResult2;
                        this.secondNo = tempSecondNo;
                        try {
                            switch (this.wrongAnsPattern) {
                                case 1:
                                case 6:
                                    this.result = (this.firstNo / this.secondNo) - 1;
                                    return;
                                case 2:
                                case 7:
                                    this.result = (this.firstNo / this.secondNo) + ((int) (3.0d * Math.random())) + 1;
                                    return;
                                case 3:
                                case 8:
                                    this.result = (this.firstNo / this.secondNo) + this.secondNo;
                                    return;
                                case 4:
                                case Constant.LIST_ITEM_TYPE_CHALLENGE_STAKE_AND_MODE:
                                    this.result = (this.firstNo / this.secondNo) + 1;
                                    return;
                                case 5:
                                case 10:
                                    this.result = this.firstNo / (this.firstNo / this.secondNo);
                                    return;
                                default:
                                    return;
                            }
                        } catch (Exception e) {
                            this.result = 0;
                            return;
                        }
                        this.result = 0;
                        return;
                    default:
                        return;
                }
            }
        }
    }
}
