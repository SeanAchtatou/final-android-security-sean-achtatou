package com.sunnysideblue.mathmate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.scoreloop.client.android.ui.EntryScreenActivity;
import com.scoreloop.client.android.ui.component.base.Constant;

public class Game extends Activity implements View.OnClickListener {
    static final int CHALLENGE_MODE_ON = 1;
    static final int DIALOG_ABOUT_ID = 1;
    static final int DIALOG_CHALLENGE_ID = 3;
    static final int DIALOG_EXIT_ID = 0;
    static final int DIALOG_NEWGAME_ID = 2;
    static final String EASY_LABEL = "EASY";
    static final String EASY_TIME = "0:00:30.00";
    static final int GAME_CHALLENGE_MODE_REQUEST = 7;
    static final int GAME_CHALL_MODE_CLEAR = 20;
    static final int GAME_CHAL_MODE = 888;
    static final int GAME_COUNTDOWN_CHALL_MODE = 11;
    static final int GAME_COUNTDOWN_NORMAL_MODE = 10;
    static final int GAME_PLAYGROUND_REQUEST = 2;
    static final int GAME_TRAIN_MODE = 999;
    static final String HARD_LABEL = "HARD";
    static final String HARD_TIME = "0:00:60.00";
    static final String MEDIUM_LABEL = "MEDIUM";
    static final String MEDIUM_TIME = "0:00:45.00";
    private static final String TAG = "Math Mate";
    static int gameLevel = 0;
    Prefs challengemodePrefs = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        this.challengemodePrefs = new Prefs();
        findViewById(R.id.option_button).setOnClickListener(this);
        findViewById(R.id.new_button).setOnClickListener(this);
        findViewById(R.id.about_button).setOnClickListener(this);
        findViewById(R.id.challenge_mod_button).setOnClickListener(this);
        findViewById(R.id.scoreloop).setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scoreloop /*2131558421*/:
                startActivity(new Intent(this, EntryScreenActivity.class));
                return;
            case R.id.new_button /*2131558422*/:
                openNewGameDialog();
                return;
            case R.id.challenge_mod_button /*2131558423*/:
                chalModeStart();
                return;
            case R.id.option_button /*2131558424*/:
                startActivity(new Intent(this, Prefs.class));
                finish();
                return;
            case R.id.about_button /*2131558425*/:
                showDialog(1);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                View layout = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.custom_dialog, (ViewGroup) findViewById(R.id.layout_root));
                ((TextView) layout.findViewById(R.id.text1)).setText(R.string.dialog_exit_msg);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setView(layout);
                builder.setTitle("Exit the Game");
                builder.setIcon(R.drawable.icon_mathmate);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Game.this.setResult(-1, Game.this.getIntent());
                        Game.this.finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                return builder.create();
            case 1:
                View layout2 = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.custom_dialog, (ViewGroup) findViewById(R.id.layout_root));
                TextView text2 = (TextView) layout2.findViewById(R.id.text2);
                text2.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/ubuntu_titling_rg_bold.ttf"));
                text2.setText("Created by SunnySide Blue. 2011\n\n\nPlease feel free to send us any feedback for future updates!\n\ncontact@sunnysideblue.com\n\nVisit us at\nwww.sunnysideblue.com");
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setView(layout2);
                builder2.setTitle("About Game");
                builder2.setIcon(R.drawable.icon_mathmate);
                builder2.setPositiveButton("Email Us", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Intent emailIntent = new Intent("android.intent.action.SEND");
                        emailIntent.setType("plain/text");
                        emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{"contact@sunnysideblue.com"});
                        emailIntent.putExtra("android.intent.extra.SUBJECT", Game.TAG);
                        emailIntent.putExtra("android.intent.extra.TEXT", "To developer");
                        Game.this.startActivity(Intent.createChooser(emailIntent, "Send Email"));
                        Game.this.finish();
                    }
                });
                builder2.setNegativeButton("4 LAWS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Game.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.campuscrusade.com/fourlawseng.htm")));
                        Game.this.finish();
                    }
                });
                builder2.setOnKeyListener(null);
                return builder2.create();
            case 2:
            default:
                return null;
            case 3:
                String challengeTime = null;
                String level = "";
                switch (this.challengemodePrefs.getPrefLevel(getApplicationContext())) {
                    case 0:
                        level = "EASY";
                        break;
                    case 1:
                        level = "MEDIUM";
                        break;
                    case 2:
                        level = "HARD";
                        break;
                }
                if (level.endsWith("EASY")) {
                    challengeTime = EASY_TIME;
                } else if (level.endsWith("MEDIUM")) {
                    challengeTime = MEDIUM_TIME;
                } else if (level.endsWith("HARD")) {
                    challengeTime = HARD_TIME;
                }
                View layout3 = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.custom_dialog, (ViewGroup) findViewById(R.id.layout_root));
                SpannableStringBuilder sb = new SpannableStringBuilder();
                sb.append((CharSequence) "Current level: ");
                String str = "\"" + level + "\"";
                sb.append((CharSequence) str);
                int end = 15 + str.length();
                sb.setSpan(new ForegroundColorSpan(-65536), 15, end, 33);
                sb.setSpan(new StyleSpan(1), 15, end, 33);
                sb.append((CharSequence) " \n");
                sb.append((CharSequence) "\n\n");
                sb.append((CharSequence) "To reach the next level\n");
                sb.append((CharSequence) "get less than four wrong\n");
                sb.append((CharSequence) "within ");
                String str2 = "\"" + challengeTime + "\"";
                sb.append((CharSequence) str2);
                int start = end + 59;
                int end2 = start + str2.length();
                sb.setSpan(new StyleSpan(3), start, end2, 33);
                sb.append((CharSequence) "\n\nGood Luck!!");
                int start2 = end2 + 1;
                sb.setSpan(new ForegroundColorSpan(-256), start2, start2 + "\n\nGood Luck!!".length(), 33);
                ((TextView) layout3.findViewById(R.id.text1)).setText(sb);
                AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
                builder3.setView(layout3);
                builder3.setTitle("Challenge Mode");
                builder3.setIcon(R.drawable.icon_mathmate);
                builder3.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Game.this.chalModeStart();
                    }
                });
                builder3.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                return builder3.create();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case 4:
                setResult(-1, getIntent());
                finish();
                break;
            case Constant.LIST_ITEM_TYPE_STANDARD:
            case 66:
                break;
            default:
                return super.onKeyDown(keyCode, event);
        }
        return true;
    }

    private void startChallengeGame() {
        MainApplication.setGamePlaySessionMode(3);
        Intent intent = new Intent(this, ListGameString.class);
        intent.putExtra(ListGameString.KEY_CHALLENGE_MODE, 1);
        intent.putExtra(ListGameString.KEY_CHAL_MODE_LEVEL, this.challengemodePrefs.getPrefLevel(getApplicationContext()));
        intent.putExtra(ListGameString.KEY_CHAL_MODE_CHANCE, this.challengemodePrefs.getPrefChance(getApplicationContext()));
        startActivityForResult(intent, 7);
        finish();
    }

    private void startGame(int i) {
        MainApplication.setGamePlaySessionMode(Integer.valueOf(i));
        Intent intent = new Intent(this, ListGameString.class);
        intent.putExtra(ListGameString.KEY_LEVEL, i);
        startActivityForResult(intent, 2);
        finish();
    }

    private void openNewGameDialog() {
        new AlertDialog.Builder(this).setItems((int) R.array.level, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                Game.this.trainModeStart(i);
            }
        }).show();
    }

    public void trainModeStart(int i) {
        gameLevel = i;
        startGame(gameLevel);
    }

    public void chalModeStart() {
        startChallengeGame();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1) {
            if (requestCode == 2) {
                finish();
            }
            if (requestCode == 7) {
                finish();
            }
            if (requestCode == 222) {
                showDialog(3);
                finish();
            }
        }
    }
}
