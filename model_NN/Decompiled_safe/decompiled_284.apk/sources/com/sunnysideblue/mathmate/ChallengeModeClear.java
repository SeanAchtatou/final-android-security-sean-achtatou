package com.sunnysideblue.mathmate;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import com.scoreloop.client.android.ui.component.base.Constant;

public class ChallengeModeClear extends Activity implements View.OnClickListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.hall_of_fame_game_claer);
        findViewById(R.id.complete_clear_button).setOnClickListener(this);
        TextView tv = (TextView) findViewById(R.id.genius_name_text);
        TextView tv2 = (TextView) findViewById(R.id.hall_of_fame_text);
        Prefs pref = new Prefs();
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/ubuntu_titling_rg_bold.ttf");
        tv.setTypeface(face);
        tv.setRawInputType(1);
        tv2.setTypeface(face);
        tv2.setRawInputType(1);
        tv.setText(pref.getPrefName(getApplicationContext()));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.complete_clear_button /*2131558416*/:
                startActivity(new Intent(this, Game.class));
                finish();
                return;
            default:
                return;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case 4:
                startActivity(new Intent(this, Game.class));
                finish();
                return true;
            case Constant.LIST_ITEM_TYPE_STANDARD:
            case 66:
                return super.onKeyDown(keyCode, event);
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
