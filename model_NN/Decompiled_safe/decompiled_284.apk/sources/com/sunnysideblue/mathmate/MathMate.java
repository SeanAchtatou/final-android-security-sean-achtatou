package com.sunnysideblue.mathmate;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class MathMate extends Activity {
    static final int MAIN_MENU_REQUEST = 0;
    static boolean isFocustoToast = false;
    GoogleAnalyticsTracker tracker;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.title);
        showStudioLogo();
        runningTimer();
    }

    private void runningTimer() {
        new CountDownTimer(3000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                MathMate.this.startActivityForResult(new Intent(MathMate.this.getApplicationContext(), Game.class), 0);
                MathMate.this.finish();
            }
        }.start();
    }

    public void showStudioLogo() {
        View layout = ((LayoutInflater) getApplicationContext().getSystemService("layout_inflater")).inflate((int) R.layout.studio_intro, (ViewGroup) findViewById(R.id.toast_studio_intro_layout_root));
        ImageView image = (ImageView) layout.findViewById(R.id.studio_intro_image);
        image.setImageResource(R.drawable.company_logo_new);
        image.setBackgroundColor(-1);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(17, 0, 0);
        toast.setDuration(0);
        toast.setView(layout);
        toast.show();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1 && requestCode == 0) {
            finish();
        }
    }
}
