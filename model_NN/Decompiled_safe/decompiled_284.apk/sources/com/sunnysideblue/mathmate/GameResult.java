package com.sunnysideblue.mathmate;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.LeaderboardsScreenActivity;
import com.scoreloop.client.android.ui.OnScoreSubmitObserver;
import com.scoreloop.client.android.ui.PostScoreOverlayActivity;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.scoreloop.client.android.ui.ShowResultOverlayActivity;
import com.scoreninja.adapter.ScoreNinjaAdapter;

public class GameResult extends Activity implements View.OnClickListener, OnScoreSubmitObserver {
    public static final String CHALLENGE_BUTTON = "com.sunnysideblue.mathmate.challengeButton";
    public static final String CHALLENGE_MODE = "com.sunnysideblue.mathmate.challengemode";
    private static final int DIALOG_PROGRESS = 12;
    private static final int LEADERBOARD_SHOW = 2;
    public static final int POPUP_CHALLENGEMODE_DIALOG = 888;
    private static final int POST_SCORE = 1;
    public static final int RESULT_BACK_TO_ABOVE = 222;
    public static final int RESULT_GAME_RESET = 999;
    public static final String RESULT_TEXT = "com.sunnysideblue.mathmate.resulttext";
    private static final int SHOW_RESULT = 0;
    public static final String TRAIN_GAME_LEVEL = "com.sunnysideblue.mathmate.traingamelevel";
    public static final String WORLD_SCORE = "com.sunnysideblue.mathmate.worldscore";
    private int _submitStatus;
    private int challMode = 0;
    private String levelLabel = "";
    private String resultButton = "";
    private String resultText = "";
    private ScoreNinjaAdapter scoreNinjaAdapter;
    private int trainGameLevel = 0;
    private long worldScore = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        String resultText2 = getIntent().getStringExtra(RESULT_TEXT);
        this.worldScore = getIntent().getLongExtra(WORLD_SCORE, 0);
        this.resultButton = getIntent().getStringExtra(CHALLENGE_BUTTON);
        this.challMode = getIntent().getIntExtra(CHALLENGE_MODE, 0);
        this.trainGameLevel = getIntent().getIntExtra(TRAIN_GAME_LEVEL, 0);
        setContentView((int) R.layout.game_result);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/ubuntu_titling_rg_bold.ttf");
        TextView txt1 = (TextView) findViewById(R.id.text1);
        txt1.setTypeface(face);
        if (this.resultButton.equals("Main\nMenu")) {
            txt1.setText("Congraturations!!");
        } else {
            txt1.setText("RESULT");
        }
        TextView txt2 = (TextView) findViewById(R.id.text2);
        txt2.setTypeface(face);
        txt2.setText(resultText2);
        Button oneMoreTimeButton = (Button) findViewById(R.id.restart);
        oneMoreTimeButton.setTypeface(face);
        if (this.challMode == 1) {
            oneMoreTimeButton.setText(this.resultButton);
        } else {
            oneMoreTimeButton.setText("Retry");
        }
        oneMoreTimeButton.setOnClickListener(this);
        if (this.challMode == 0 || this.resultButton.equals("Main\nMenu")) {
            Button rightButton = (Button) findViewById(R.id.highscores);
            rightButton.setTypeface(face);
            rightButton.setText("Submit\nScore");
            rightButton.setOnClickListener(this);
        } else {
            Button rightButton2 = (Button) findViewById(R.id.highscores);
            rightButton2.setTypeface(face);
            rightButton2.setText("Game\nReset");
            rightButton2.setOnClickListener(this);
        }
        if (this.challMode == 0 && MainApplication.getGamePlaySessionMode() == null) {
            throw new IllegalStateException("no mode received");
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.restart /*2131558411*/:
                if (this.resultButton.equals("Main\nMenu")) {
                    MainApplication.setGameLevel(0);
                    goBackMenu();
                    return;
                } else if (this.resultButton.equals("Retry")) {
                    MainApplication.setRetryGameFlag(true);
                    if (this.challMode == 0) {
                        MainApplication.setGameLevel(this.trainGameLevel);
                    }
                    setResult(-1, getIntent());
                    finish();
                    return;
                } else if (this.resultButton.equals("Next")) {
                    MainApplication.setRetryGameFlag(false);
                    MainApplication.setGameLevel(this.trainGameLevel);
                    setResult(POPUP_CHALLENGEMODE_DIALOG, getIntent());
                    finish();
                    return;
                } else {
                    return;
                }
            case R.id.highscores /*2131558412*/:
                if (this.challMode == 0 || this.resultButton.equals("Main\nMenu")) {
                    showDialog(12);
                    goWorldHighScores();
                    return;
                }
                setResult(RESULT_GAME_RESET, getIntent());
                finish();
                return;
            default:
                return;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (this.resultButton.equals("Main\nMenu")) {
                MainApplication.setGameLevel(0);
            }
            goBackMenu();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void goBackMenu() {
        if (this.resultButton.equals("Main\nMenu")) {
            MainApplication.setFinalSocre(0L);
        }
        setResult(222, getIntent());
        finish();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 0:
                if (this._submitStatus != 4) {
                    startActivityForResult(new Intent(this, PostScoreOverlayActivity.class), 1);
                    return;
                } else {
                    finish();
                    return;
                }
            case 1:
                Intent intent = new Intent(this, LeaderboardsScreenActivity.class);
                intent.putExtra("mode", this.trainGameLevel);
                startActivityForResult(intent, 2);
                return;
            case 2:
                if (this.resultButton.equals("Main\nMenu")) {
                    MainApplication.setGameLevel(0);
                }
                goBackMenu();
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.ScoreloopManager.onGamePlayEnded(com.scoreloop.client.android.core.model.Score, java.lang.Boolean):void
     arg types: [com.scoreloop.client.android.core.model.Score, int]
     candidates:
      com.scoreloop.client.android.ui.ScoreloopManager.onGamePlayEnded(java.lang.Double, java.lang.Integer):void
      com.scoreloop.client.android.ui.ScoreloopManager.onGamePlayEnded(com.scoreloop.client.android.core.model.Score, java.lang.Boolean):void */
    public void goWorldHighScores() {
        Score score = new Score(Double.valueOf(Double.parseDouble(new StringBuilder(String.valueOf(this.worldScore)).toString())), null);
        score.setMode(MainApplication.getGamePlaySessionMode());
        ScoreloopManagerSingleton.get().onGamePlayEnded(score, (Boolean) false);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 12:
                return createProgressDialog();
            default:
                return null;
        }
    }

    private Dialog createProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Submitting score in progress");
        return dialog;
    }

    public void onScoreSubmit(int status, Exception error) {
        dismissDialog(12);
        this._submitStatus = status;
        startActivityForResult(new Intent(this, ShowResultOverlayActivity.class), 0);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        ScoreloopManagerSingleton.get().setOnScoreSubmitObserver(null);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ScoreloopManagerSingleton.get().setOnScoreSubmitObserver(this);
    }
}
