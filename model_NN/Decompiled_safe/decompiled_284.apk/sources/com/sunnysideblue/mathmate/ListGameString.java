package com.sunnysideblue.mathmate;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.sunnysideblue.mathmate.QuestionGenerator;
import java.util.ArrayList;

public class ListGameString extends ListActivity implements View.OnClickListener {
    public static final int CHAL_MODE_CHANCE = 1;
    public static final int CHAL_MODE_OFF = 0;
    public static final int CHAL_MODE_ON = 1;
    private static final int DIALOG_CHALLENGE_ID = 4;
    private static final int DIALOG_EXIT_GAME = 1;
    private static final int DIALOG_GAME_CLEAR = 2;
    private static final int DIALOG_GAME_RESULT = 0;
    private static final int DIALOG_HALL_OF_FAME = 3;
    public static final int EASY_LEVEL = 0;
    public static final String EASY_LEVEL_LABEL = "EASY";
    public static final long EASY_TIME = 30000;
    private static final int EXPECT_WRONG_ANSWER = 4;
    static final int FALSE = 0;
    public static final int FIFTYITEMS = 50;
    public static final int GAME_CHALLMODE_RESTART_GAME_OK = 333;
    static final int GAME_CHALL_MODE_CLEAR = 20;
    static final int GAME_COUNTDOWN_CHALL_MODE = 11;
    static final int GAME_COUNTDOWN_NORMAL_MODE = 10;
    static final int GAME_RESULT_MODE = 20;
    public static final int GAME_TRAINMODE_RESTART_GAME_OK = 444;
    public static final int HARD_LEVEL = 2;
    public static final String HARD_LEVEL_LABEL = "HARD";
    public static final long HARD_TIME = 60000;
    public static final int HUNDREDITEMS = 100;
    public static final String KEY_CHALLENGE_MODE = "com.sunnysideblue.mathmate.challenge_mode";
    public static final String KEY_CHAL_MODE_CHANCE = "com.sunnysideblue.mathmate.challenge_mode_chance";
    public static final String KEY_CHAL_MODE_LEVEL = "com.sunnysideblue.mathmate.challenge_mode_level";
    public static final String KEY_LEVEL = "com.sunnysideblue.mathmate.level";
    public static final int LEVEL_10 = 0;
    public static final int LEVEL_100 = 4;
    public static final int LEVEL_20 = 1;
    public static final int LEVEL_50 = 2;
    public static final int LEVEL_70 = 3;
    public static final int MEDIUM_LEVEL = 1;
    public static final String MEDIUM_LEVEL_LABEL = "MEDIUM";
    public static final long MEDIUM_TIME = 45000;
    public static final int POPUP_CHAL_DIALOG = 222;
    private static final String RESET_TIME = "0:0:00.00";
    public static final int RESULT_BACK_TO_ABOVE = 222;
    public static final int SEVENTYITEMS = 70;
    public static final int TENITEMS = 10;
    static final int TRUE = 1;
    public static final int TWENTYITEMS = 20;
    static String chkAns = "";
    static int counter = 3;
    static String gameButteonText = "One More Time";
    static boolean gameClear = false;
    static boolean key_Back_flag = false;
    static LinearLayout layout = null;
    static int listCnt = 0;
    static ArrayList<String> queSet = null;
    static String resultMessage = "";
    static int resumeFlag = 0;
    static int rightwrong = 0;
    int[] arrayImage;
    int challengeModeChance = 0;
    int challengeModeFlag = 0;
    Prefs challengemodePrefs = null;
    Chronometer chronoTimer = null;
    boolean dialogOpen = false;
    public String easyTimeRecord = "";
    public long easyTime_ChalMode = 0;
    String finalResultTime = RESET_TIME;
    public long finalTime_ChalMode = 0;
    int gameLevel = 0;
    public String hardTimeRecord = "";
    public long hardTime_ChalMode = 0;
    int imageCnt = 0;
    int itemCnt = 0;
    private String levelLabel = "";
    long levelTimeLimit = 0;
    public String mediumTimeRecord = "";
    public long mediumTime_ChalMode = 0;
    MediaPlayer mp = null;
    int noOfItems = 0;
    int oneFlag = 0;
    ArrayList<QuestionGenerator.OneQuestion> oneq = null;
    Prefs optionPrefs = null;
    private boolean popUpResult;
    TextView tv = null;
    int wrongAnsNo = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.scroll_view_3);
        ((TextView) findViewById(R.id.itemcount_label)).setTextColor(-16777216);
        this.challengemodePrefs = new Prefs();
        this.challengeModeFlag = getIntent().getIntExtra(KEY_CHALLENGE_MODE, 0);
        if (this.challengeModeFlag != 1) {
            startGame();
        } else if (!MainApplication.getRetryGameFlag()) {
            showDialog(4);
        } else {
            startGame();
        }
    }

    public void startGame() {
        this.optionPrefs = new Prefs();
        this.challengeModeFlag = getIntent().getIntExtra(KEY_CHALLENGE_MODE, 0);
        this.challengeModeChance = this.optionPrefs.getPrefChance(this);
        if (this.challengeModeFlag == 1) {
            this.gameLevel = this.optionPrefs.getPrefLevel(this);
            this.noOfItems = 20;
        } else {
            this.gameLevel = getIntent().getIntExtra(KEY_LEVEL, 0);
            this.noOfItems = Integer.parseInt(Prefs.getNoofItems(this));
        }
        switch (this.gameLevel) {
            case 0:
                this.levelLabel = EASY_LEVEL_LABEL;
                this.levelTimeLimit = 30000;
                break;
            case 1:
                this.levelLabel = MEDIUM_LEVEL_LABEL;
                this.levelTimeLimit = MEDIUM_TIME;
                break;
            case 2:
                this.levelLabel = HARD_LEVEL_LABEL;
                this.levelTimeLimit = HARD_TIME;
                break;
        }
        TextView tempLevel = (TextView) findViewById(R.id.templevel);
        tempLevel.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/ubuntu_titling_rg_bold.ttf"));
        tempLevel.setText("    Level: " + this.levelLabel);
        ((Button) findViewById(R.id.right)).setOnClickListener(this);
        ((Button) findViewById(R.id.wrong)).setOnClickListener(this);
        setListAdapter(new IconicAdapter());
        buildQuestionSet(this.noOfItems, this.gameLevel);
        listCnt = queSet.size();
        this.arrayImage = new int[(this.noOfItems + 3)];
        initialList();
        this.chronoTimer = new Chronometer(this);
        this.mp = MediaPlayer.create(this, (int) R.raw.stomp);
        this.popUpResult = false;
        startActivity(new Intent(getApplicationContext(), CountDown.class));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    public void initialList() {
        for (int i = 0; i < 3; i++) {
            addQuestionList(queSet.get(listCnt - 1));
            listCnt--;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1 && requestCode == 20) {
            restartGame();
            finish();
        }
        if (resultCode == 888) {
            restartGame();
            finish();
        }
        if (resultCode == 222) {
            updatePrefData();
            startActivity(new Intent(getApplicationContext(), Game.class));
            finish();
        }
        if (resultCode == 999) {
            if (this.challengeModeFlag == 1) {
                this.gameLevel = 0;
                this.finalResultTime = RESET_TIME;
                this.challengeModeChance = 1;
                MainApplication.setGameLevel(this.gameLevel);
                updatePrefData();
            }
            restartGame();
            finish();
        }
    }

    public void onClick(View v) {
        if (this.itemCnt < this.noOfItems + 3) {
            switch (v.getId()) {
                case R.id.right /*2131558417*/:
                    rightwrong = 1;
                    questionPuller();
                    return;
                case R.id.wrong /*2131558418*/:
                    rightwrong = 0;
                    questionPuller();
                    return;
                default:
                    return;
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case 4:
                key_Back_flag = true;
                showDialog(1);
                return true;
            case Constant.LIST_ITEM_TYPE_STANDARD:
            case 66:
                return super.onKeyDown(keyCode, event);
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                Dialog dialog = new Dialog(getApplicationContext());
                dialog.setContentView(R.layout.custom_dialog);
                dialog.setTitle("Custom Dialog");
                ((TextView) dialog.findViewById(R.id.text1)).setText(resultMessage);
                ((ImageView) dialog.findViewById(R.id.image)).setImageResource(R.drawable.icon_mathmate);
                return dialog;
            case 1:
                View layout2 = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.custom_dialog, (ViewGroup) findViewById(R.id.layout_root));
                ((TextView) layout2.findViewById(R.id.text1)).setText(R.string.dialog_backtomenu_msg);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setView(layout2);
                builder.setTitle("Exit or Restart the Game");
                builder.setIcon(R.drawable.icon_mathmate);
                builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MainApplication.setRetryGameFlag(false);
                        ListGameString.this.goBackMenu();
                    }
                });
                builder.setNegativeButton("Restart", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MainApplication.setRetryGameFlag(true);
                        ListGameString.this.restartGame();
                        ListGameString.this.finish();
                    }
                });
                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        ListGameString.this.showDialog(1);
                    }
                });
                return builder.create();
            case 2:
                View layout3 = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.custom_dialog, (ViewGroup) findViewById(R.id.layout_root));
                layout3.setBackgroundColor(-65536);
                ((TextView) layout3.findViewById(R.id.text1)).setText(resultMessage);
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setView(layout3);
                builder2.setTitle("Challenge Mode CLEAR!!");
                builder2.setIcon(R.drawable.icon_mathmate);
                builder2.setPositiveButton("Congratulation!!", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ListGameString.this.gameLevel = 0;
                        ListGameString.this.finalResultTime = ListGameString.RESET_TIME;
                        ListGameString.this.challengeModeChance = 1;
                        ListGameString.this.showDialog(3);
                        dialog.dismiss();
                    }
                });
                builder2.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        if (ListGameString.this.dialogOpen) {
                            ListGameString.this.showElapsedTime();
                        }
                    }
                });
                return builder2.create();
            case 3:
                View textEntryView = LayoutInflater.from(this).inflate(R.layout.alert_dialog_text_entry, (ViewGroup) null);
                final View view = textEntryView;
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.icon_mathmate).setTitle("Enter \"Genius\" name").setView(textEntryView).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ListGameString.this.getApplicationContext()).edit();
                        editor.putString("Name", String.valueOf(ListGameString.this.optionPrefs.getPrefName(ListGameString.this.getApplicationContext())) + "\n" + ((EditText) view.findViewById(R.id.geniusname_edit)).getText().toString());
                        editor.commit();
                        ListGameString.this.startActivity(new Intent(ListGameString.this.getApplicationContext(), ChallengeModeClear.class));
                        ListGameString.this.finish();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ListGameString.this.showDialog(1);
                        dialog.dismiss();
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        ListGameString.this.showDialog(3);
                    }
                }).create();
            case 4:
                String challengeTime = null;
                String level = "";
                int getLevel = this.challengemodePrefs.getPrefLevel(getApplicationContext());
                switch (getLevel) {
                    case 0:
                        level = EASY_LEVEL_LABEL;
                        break;
                    case 1:
                        level = MEDIUM_LEVEL_LABEL;
                        break;
                    case 2:
                        level = HARD_LEVEL_LABEL;
                        break;
                }
                MainApplication.setGameLevel(getLevel);
                if (level.endsWith(EASY_LEVEL_LABEL)) {
                    challengeTime = "0:00:30.00";
                } else if (level.endsWith(MEDIUM_LEVEL_LABEL)) {
                    challengeTime = "0:00:45.00";
                } else if (level.endsWith(HARD_LEVEL_LABEL)) {
                    challengeTime = "0:00:60.00";
                }
                View layout4 = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.custom_dialog, (ViewGroup) findViewById(R.id.layout_root));
                SpannableStringBuilder sb = new SpannableStringBuilder();
                sb.append((CharSequence) "Current level: ");
                String str = "\"" + level + "\"";
                sb.append((CharSequence) str);
                int end = 15 + str.length();
                sb.setSpan(new ForegroundColorSpan(-65536), 15, end, 33);
                sb.setSpan(new StyleSpan(1), 15, end, 33);
                sb.append((CharSequence) " \n");
                sb.append((CharSequence) "\n\n");
                sb.append((CharSequence) "To reach the next level\n");
                sb.append((CharSequence) "get less than four wrong\n");
                sb.append((CharSequence) "within ");
                String str2 = "\"" + challengeTime + "\"";
                sb.append((CharSequence) str2);
                int start = end + 59;
                int end2 = start + str2.length();
                sb.setSpan(new StyleSpan(3), start, end2, 33);
                sb.append((CharSequence) "\n\nGood Luck!!");
                int start2 = end2 + 1;
                sb.setSpan(new ForegroundColorSpan(-256), start2, start2 + "\n\nGood Luck!!".length(), 33);
                TextView text5 = (TextView) layout4.findViewById(R.id.text1);
                text5.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/ubuntu_titling_rg_bold.ttf"));
                text5.setTextSize(17.0f);
                text5.setText(sb);
                AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
                builder3.setView(layout4);
                builder3.setTitle("Challenge Mode");
                builder3.setIcon(R.drawable.icon_mathmate);
                builder3.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ListGameString.this.startGame();
                    }
                });
                builder3.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ListGameString.this.goBackMenu();
                    }
                });
                builder3.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        ListGameString.this.goBackMenu();
                    }
                });
                return builder3.create();
            default:
                return null;
        }
    }

    public void goBackMenu() {
        updatePrefData();
        startActivity(new Intent(getApplicationContext(), Game.class));
        finish();
    }

    public void questionPuller() {
        chkAns = this.oneq.get(listCnt).rightwrongFlag == rightwrong ? "   RIGHT" : "   WRONG";
        if (chkAns.endsWith("RIGHT")) {
            this.arrayImage[this.imageCnt] = R.drawable.o;
        } else {
            this.wrongAnsNo++;
            this.arrayImage[this.imageCnt] = R.drawable.x;
            if (Prefs.getShake(this)) {
                findViewById(16908298).startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            }
            if (Prefs.getVibrator(this)) {
                ((Vibrator) getSystemService("vibrator")).vibrate(50);
            }
            if (Prefs.getSound(this)) {
                this.mp.setLooping(false);
                this.mp.start();
            }
        }
        if (listCnt > 0) {
            addQuestionList(queSet.get(listCnt - 1));
            listCnt--;
            this.imageCnt++;
            return;
        }
        ((ArrayAdapter) getListAdapter()).remove(queSet.get(2));
        ((ArrayAdapter) getListAdapter()).add(queSet.get(2));
        resultMessage = new CalculateElapsedTime(this.challengeModeFlag).getDialogMessage();
        showElapsedTime();
    }

    public void showElapsedTime() {
        this.dialogOpen = true;
        if (this.challengeModeChance == 1) {
            this.finalResultTime = RESET_TIME;
        }
        if (this.challengeModeChance == 0 && this.gameLevel == 2) {
            this.finalResultTime = RESET_TIME;
            if (!this.popUpResult) {
                this.popUpResult = true;
                Intent intent = new Intent(getApplicationContext(), GameResult.class);
                intent.putExtra(GameResult.RESULT_TEXT, resultMessage);
                intent.putExtra(GameResult.WORLD_SCORE, this.finalTime_ChalMode);
                intent.putExtra(GameResult.CHALLENGE_MODE, this.challengeModeFlag);
                intent.putExtra(GameResult.CHALLENGE_BUTTON, gameButteonText);
                intent.putExtra(GameResult.TRAIN_GAME_LEVEL, 3);
                startActivityForResult(intent, 20);
                this.gameLevel = 0;
                this.finalResultTime = RESET_TIME;
                this.challengeModeChance = 1;
            }
        } else if (!this.popUpResult) {
            this.popUpResult = true;
            Intent intent2 = new Intent(getApplicationContext(), GameResult.class);
            intent2.putExtra(GameResult.RESULT_TEXT, resultMessage);
            intent2.putExtra(GameResult.WORLD_SCORE, new CalculateElapsedTime(this.challengeModeFlag).getScore());
            intent2.putExtra(GameResult.CHALLENGE_MODE, this.challengeModeFlag);
            intent2.putExtra(GameResult.CHALLENGE_BUTTON, gameButteonText);
            intent2.putExtra(GameResult.TRAIN_GAME_LEVEL, this.gameLevel);
            startActivityForResult(intent2, 20);
        }
    }

    public void restartGame() {
        startActivity(getIntent());
        finish();
    }

    public void addQuestionList(String oneQue) {
        TextView itemCountLabel = (TextView) findViewById(R.id.itemcount_label);
        itemCountLabel.setTextColor(-1);
        TextView itemCount = (TextView) findViewById(R.id.itemcount);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/ubuntu_titling_rg_bold.ttf");
        itemCount.setTypeface(face);
        itemCountLabel.setTypeface(face);
        itemCount.setText(String.valueOf(String.valueOf(this.itemCnt - 1)) + " / " + String.valueOf(this.noOfItems));
        ((ArrayAdapter) getListAdapter()).add(oneQue);
        this.oneFlag++;
        this.itemCnt++;
    }

    public ArrayList<String> buildQuestionSet(int noOfItems2, int gameLevel2) {
        String opChar = "";
        QuestionGenerator qGen = new QuestionGenerator(this, this.challengeModeFlag);
        qGen.qGenerator(noOfItems2, gameLevel2, this.challengeModeFlag);
        this.oneq = qGen.getQuestions();
        queSet = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            queSet.add("");
        }
        for (int i2 = 0; i2 < this.oneq.size(); i2++) {
            switch (this.oneq.get(i2).arithOp) {
                case 1:
                    opChar = "+";
                    break;
                case 2:
                    opChar = " -";
                    break;
                case 3:
                    opChar = "x";
                    break;
                case 4:
                    opChar = " /";
                    break;
            }
            queSet.add(String.valueOf(this.oneq.get(i2).firstNo) + " " + opChar + " " + this.oneq.get(i2).secondNo + "  =  " + this.oneq.get(i2).result);
        }
        return queSet;
    }

    public void onPause() {
        super.onPause();
        updatePrefData();
    }

    public void onStop() {
        super.onStop();
        updatePrefData();
    }

    public void onResume() {
        super.onResume();
        key_Back_flag = false;
    }

    public void updatePrefData() {
        if (this.challengeModeFlag == 1) {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
            editor.putInt("Level", MainApplication.getGameLevel());
            if (!key_Back_flag) {
                editor.putString("TimeRecord", this.finalResultTime);
            }
            editor.putInt("Chance", this.challengeModeChance);
            editor.putString("EasyTimeRecord", MainApplication.getEasyTimeRecord());
            editor.putString("MediumTimeRecord", MainApplication.getMediumTimeRecord());
            editor.putString("HardTimeRecord", MainApplication.getHardTimeRecord());
            editor.putLong("HardTimeMillis", MainApplication.getFinalScore().longValue());
            editor.commit();
        }
    }

    class IconicAdapter extends ArrayAdapter<String> {
        private final int MAX_CHARCTERS = 18;

        IconicAdapter() {
            super(ListGameString.this, (int) R.layout.row_layout_listview, new ArrayList());
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = ListGameString.this.getLayoutInflater().inflate((int) R.layout.row_layout_listview, parent, false);
            }
            ((ListView) ListGameString.this.findViewById(16908298)).setSelected(false);
            TextView label = (TextView) row.findViewById(R.id.question_label);
            ImageView icon = (ImageView) row.findViewById(R.id.result_icon);
            if (ListGameString.queSet.get((ListGameString.queSet.size() - 1) - position).length() > 18) {
                label.setTextSize(35.0f);
            } else {
                label.setTextSize(40.0f);
            }
            if (position == 0 && position == ListGameString.this.imageCnt && ListGameString.this.arrayImage[ListGameString.this.imageCnt] == 0) {
                icon.setBackgroundResource(R.layout.custom_list_style);
                label.setTextColor(-60918);
                label.setBackgroundResource(R.layout.custom_list_style);
            }
            label.setText(ListGameString.queSet.get((ListGameString.queSet.size() - 1) - position));
            icon.setImageResource(ListGameString.this.arrayImage[position]);
            return row;
        }
    }

    public class CalculateElapsedTime {
        int challModeFlag = 0;
        String dialogMessage = "";
        long elapsedMillis = 0;
        int elapsedSecs = 0;
        long elapsed_millisec = 0;
        long elapsed_second = 0;
        long finalMillis = 0;
        int finalSecs = 0;
        int hour = 0;
        long millisec = 0;
        int min = 0;
        long penaltyTime = 0;
        long second = 0;

        CalculateElapsedTime(int challModeFlag2) {
            this.challModeFlag = challModeFlag2;
            this.elapsedMillis = SystemClock.elapsedRealtime() - ListGameString.this.chronoTimer.getBase();
            this.elapsed_millisec = this.elapsedMillis % 1000;
            this.elapsed_second = this.elapsedMillis / 1000;
            this.elapsedSecs = (int) (this.elapsedMillis / 1000);
            this.penaltyTime = (long) (ListGameString.this.wrongAnsNo * 5000);
            this.finalMillis = this.elapsedMillis + this.penaltyTime;
            this.finalSecs = this.elapsedSecs + ((int) (this.penaltyTime / 1000));
            this.millisec = this.finalMillis % 1000;
            this.second = this.finalMillis / 1000;
            if (this.second >= 60) {
                this.min = ((int) this.second) / 60;
                this.second %= 60;
            }
            if (this.min > -60) {
                this.hour = this.min / 60;
                this.min %= 60;
            }
        }

        public String getDialogMessage() {
            String str_Min;
            String str_Sec;
            String challModeComment;
            if (this.min >= 10 || this.min <= 0) {
                str_Min = new StringBuilder(String.valueOf(this.min)).toString();
            } else {
                str_Min = "0" + this.min;
            }
            if (this.second >= 10 || this.second <= 0) {
                str_Sec = new StringBuilder(String.valueOf(this.second)).toString();
            } else {
                str_Sec = "0" + this.second;
            }
            ListGameString.this.finalResultTime = String.valueOf(this.hour) + ":" + str_Min + ":" + str_Sec + "." + this.millisec;
            if (this.challModeFlag == 1) {
                if (ListGameString.this.levelTimeLimit >= this.finalMillis && ListGameString.this.wrongAnsNo <= 4) {
                    if (ListGameString.this.challengeModeChance != 0) {
                        ListGameString.this.challengeModeChance--;
                    }
                    ListGameString.gameButteonText = "One More Time";
                    challModeComment = "Good Job!! You need \"" + ListGameString.this.challengeModeChance + "\"" + " time more challeneg to go next level";
                    if (ListGameString.this.challengeModeChance == 0) {
                        switch (ListGameString.this.gameLevel) {
                            case 0:
                                ListGameString.this.gameLevel = 1;
                                MainApplication.setGameLevel(ListGameString.this.gameLevel);
                                challModeComment = "       Good Job!! Level complete!\n\n";
                                ListGameString.this.challengeModeChance = 1;
                                ListGameString.gameButteonText = "Next";
                                ListGameString.this.easyTimeRecord = ListGameString.this.finalResultTime;
                                ListGameString.this.easyTime_ChalMode = this.finalMillis;
                                MainApplication.setFinalScoreZero();
                                MainApplication.setEasyTimeRecord(ListGameString.this.finalResultTime);
                                MainApplication.setFinalSocre(Long.valueOf(this.finalMillis));
                                ListGameString.this.updatePrefData();
                                break;
                            case 1:
                                ListGameString.this.gameLevel = 2;
                                MainApplication.setGameLevel(ListGameString.this.gameLevel);
                                challModeComment = "       Good Job!! Level complete!\n\n";
                                ListGameString.this.challengeModeChance = 1;
                                ListGameString.gameButteonText = "Next";
                                Prefs dataPrfs = new Prefs();
                                ListGameString.this.easyTimeRecord = dataPrfs.getPrefEsayTimeRecord(ListGameString.this);
                                ListGameString.this.mediumTimeRecord = ListGameString.this.finalResultTime;
                                ListGameString.this.easyTime_ChalMode = dataPrfs.getPrefEasyTimeChallMode(ListGameString.this).longValue();
                                ListGameString.this.mediumTime_ChalMode = this.finalMillis;
                                MainApplication.setMediumTimeRecord(ListGameString.this.finalResultTime);
                                MainApplication.setFinalSocre(Long.valueOf(this.finalMillis));
                                ListGameString.this.updatePrefData();
                                break;
                            case 2:
                                challModeComment = "    Challenge Mode Completed!!";
                                ListGameString.gameButteonText = "Main\nMenu";
                                Prefs dataPrfs2 = new Prefs();
                                ListGameString.this.easyTimeRecord = dataPrfs2.getPrefEsayTimeRecord(ListGameString.this);
                                ListGameString.this.mediumTimeRecord = dataPrfs2.getPrefMediumTimeRecord(ListGameString.this);
                                ListGameString.this.hardTimeRecord = ListGameString.this.finalResultTime;
                                ListGameString.this.easyTime_ChalMode = dataPrfs2.getPrefEasyTimeChallMode(ListGameString.this).longValue();
                                ListGameString.this.mediumTime_ChalMode = dataPrfs2.getPrefMediumTimeChallMode(ListGameString.this).longValue();
                                ListGameString.this.hardTime_ChalMode = this.finalMillis;
                                MainApplication.setHardTimeRecord(ListGameString.this.finalResultTime);
                                MainApplication.setFinalSocre(Long.valueOf(this.finalMillis));
                                ListGameString.this.updatePrefData();
                                break;
                        }
                    }
                } else {
                    challModeComment = "          Try Harder!!";
                    ListGameString.gameButteonText = "Retry";
                    MainApplication.setGameLevel(ListGameString.this.gameLevel);
                    Prefs dataPrfs3 = new Prefs();
                    ListGameString.this.easyTimeRecord = dataPrfs3.getPrefEsayTimeRecord(ListGameString.this);
                    ListGameString.this.mediumTimeRecord = dataPrfs3.getPrefMediumTimeRecord(ListGameString.this);
                    ListGameString.this.hardTimeRecord = dataPrfs3.getPrefHardTimeRecord(ListGameString.this);
                    ListGameString.this.easyTime_ChalMode = dataPrfs3.getPrefEasyTimeChallMode(ListGameString.this).longValue();
                    ListGameString.this.mediumTime_ChalMode = dataPrfs3.getPrefMediumTimeChallMode(ListGameString.this).longValue();
                    ListGameString.this.hardTime_ChalMode = dataPrfs3.getPrefHardTimeChallMode(ListGameString.this).longValue();
                    ListGameString.this.updatePrefData();
                }
                if (ListGameString.gameButteonText.equals("Main\nMenu")) {
                    Prefs dataPrfs4 = new Prefs();
                    ListGameString.this.finalTime_ChalMode = dataPrfs4.getPrefHardTimeChallMode(ListGameString.this).longValue();
                    this.dialogMessage = String.valueOf("\n               Summary Time\n\n         Easy    level: " + dataPrfs4.getPrefEsayTimeRecord(ListGameString.this) + "\n" + "       Medium level: " + dataPrfs4.getPrefMediumTimeRecord(ListGameString.this) + "\n" + "         Hard    level: " + dataPrfs4.getPrefHardTimeRecord(ListGameString.this) + "\n" + "\n               Final Score: " + ListGameString.this.finalTime_ChalMode + "\n") + "\n" + challModeComment;
                } else {
                    this.dialogMessage = String.valueOf(resultMessage()) + "\n" + challModeComment;
                }
            } else {
                ListGameString.gameButteonText = "Retry";
                this.dialogMessage = resultMessage();
            }
            return this.dialogMessage;
        }

        public String resultMessage() {
            return "\n          Elapsed Time: " + this.elapsed_second + "." + this.elapsed_millisec + " [sec]\n\n" + "        * Wrong Answers: " + ListGameString.this.wrongAnsNo + " x 5 sec \n" + "        = Penalty Time : " + ((int) (this.penaltyTime / 1000)) + " [sec]\n\n" + "          Final Result: " + ListGameString.this.finalResultTime + "\n";
        }

        public long getScore() {
            return this.finalMillis;
        }
    }
}
