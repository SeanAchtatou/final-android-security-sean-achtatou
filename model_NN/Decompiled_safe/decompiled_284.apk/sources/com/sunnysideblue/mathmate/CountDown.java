package com.sunnysideblue.mathmate;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.widget.TextView;
import java.util.concurrent.atomic.AtomicBoolean;

public class CountDown extends Activity {
    static final int GAME_COUNTDOWN = 10;
    static final int GAME_COUNTDOWN_CHALL_MODE = 11;
    int cnt = 3;
    TextView countdown = null;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (CountDown.this.cnt > 0) {
                CountDown.this.showCountDown();
                return;
            }
            CountDown.this.countdown.setTextSize(200.0f);
            CountDown.this.countdown.setText("Go!");
            CountDown.this.setResult(-1, CountDown.this.getIntent());
            CountDown.this.finish();
        }
    };
    AtomicBoolean isRunning = new AtomicBoolean(false);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.translucent_countdown);
    }

    public void onStart() {
        super.onStart();
        this.countdown = (TextView) findViewById(R.id.tranlucent_text);
        Thread background = new Thread(new Runnable() {
            public void run() {
                int i = 0;
                while (i < 4) {
                    try {
                        if (CountDown.this.isRunning.get()) {
                            Thread.sleep(700);
                            CountDown.this.handler.sendMessage(CountDown.this.handler.obtainMessage());
                            i++;
                        } else {
                            return;
                        }
                    } catch (Throwable th) {
                        return;
                    }
                }
            }
        });
        this.isRunning.set(true);
        background.start();
    }

    public void onStop() {
        super.onStop();
        this.isRunning.set(false);
    }

    public void showCountDown() {
        this.countdown.setText(new StringBuilder(String.valueOf(this.cnt)).toString());
        this.cnt--;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case 4:
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }
}
