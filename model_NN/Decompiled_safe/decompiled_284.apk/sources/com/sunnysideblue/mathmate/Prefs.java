package com.sunnysideblue.mathmate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.KeyEvent;

public class Prefs extends PreferenceActivity {
    private static final String ADDITION_KEY = "checkbox_preference_addition";
    private static final String DIVISION_KEY = "checkbox_preference_division";
    private static final String MULTIPLICATION_KEY = "checkbox_preference_multiplication";
    private static final String NOOFITEMS_KEY = "list_preference_noofitems";
    static final String PREF_FILE = "whatsworng_pref";
    private static final String SHAKE_KEY = "checkbox_preference_shake";
    private static final String SOUND_KEY = "checkbox_preference_sound";
    private static final String SUBTRACTION_KEY = "checkbox_preference_subtraction";
    private static final String VIBRATOR_KEY = "checkbox_preference_vibrator";
    private static final boolean addition_default = true;
    static int chance = 0;
    private static final int default_chance = 1;
    private static final long default_finaltime = 0;
    private static final int default_level = 0;
    private static final String default_name = "";
    private static final String default_timeRec = "0:0:00.00";
    private static final boolean division_default = false;
    static int level = 0;
    private static final boolean multiplication_default = true;
    private static final String noofitems_default = "20";
    private static final boolean shake_default = true;
    private static final boolean sound_default = true;
    private static final boolean subtraction_default = true;
    static String timeRec = default_name;
    private static final boolean vibrator_default = true;
    SharedPreferences pref = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            goBackMenu();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void goBackMenu() {
        startActivity(new Intent(getApplicationContext(), Game.class));
        finish();
    }

    public static boolean getVibrator(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(VIBRATOR_KEY, true);
    }

    public static boolean getShake(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SHAKE_KEY, true);
    }

    public static boolean getSound(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SOUND_KEY, true);
    }

    public static boolean getAddition(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(ADDITION_KEY, true);
    }

    public static boolean getSubtraction(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SUBTRACTION_KEY, true);
    }

    public static boolean getMultiplication(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(MULTIPLICATION_KEY, true);
    }

    public static boolean getDivision(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(DIVISION_KEY, division_default);
    }

    public static String getNoofItems(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(NOOFITEMS_KEY, noofitems_default);
    }

    public int getPrefLevel(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("Level", 0);
    }

    public String getPreftimeRec(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("TimeRecord", default_timeRec);
    }

    public int getPrefChance(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("Chance", 1);
    }

    public String getPrefName(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("Name", default_name);
    }

    public Long getPrefEasyTimeChallMode(Context context) {
        return Long.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getLong("EasyTimeMillis", default_finaltime));
    }

    public Long getPrefMediumTimeChallMode(Context context) {
        return Long.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getLong("MediumTimeMillis", default_finaltime));
    }

    public Long getPrefHardTimeChallMode(Context context) {
        return Long.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getLong("HardTimeMillis", default_finaltime));
    }

    public String getPrefEsayTimeRecord(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("EasyTimeRecord", default_timeRec);
    }

    public String getPrefMediumTimeRecord(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("MediumTimeRecord", default_timeRec);
    }

    public String getPrefHardTimeRecord(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("HardTimeRecord", default_timeRec);
    }
}
