package com.sunnysideblue.mathmate;

import android.app.Application;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;

public class MainApplication extends Application {
    static final String EXTRA_MODE = "extraMode";
    private static String _easyTimeRecord = "";
    private static long _finalScore = 0;
    private static int _gameLevel = 0;
    private static Integer _gamePlaySessionMode;
    private static GamePlaySessionStatus _gamePlaySessionStatus;
    private static String _hardTimeRecord = "";
    private static String _mediumTimeRecord = "";
    public static String easyTimeRecord = "";
    public static long easyTime_ChalMode = 0;
    public static String hardTimeRecord = "";
    public static long hardTime_ChalMode = 0;
    public static String mediumTimeRecord = "";
    public static long mediumTime_ChalMode = 0;
    private static boolean retryFlag = false;
    private String gameScret = "n5mSHXMK6RrCsumwLeu0Oq8PNJpLA0NFc6WhUtqjgdwgf9d0xfODsA==";

    enum GamePlaySessionStatus {
        CHALLENGE,
        NONE,
        NORMAL
    }

    static void setGamePlaySessionMode(Integer mode) {
        _gamePlaySessionMode = mode;
    }

    static Integer getGamePlaySessionMode() {
        return _gamePlaySessionMode;
    }

    static void setRetryGameFlag(boolean flag) {
        retryFlag = flag;
    }

    static boolean getRetryGameFlag() {
        return retryFlag;
    }

    static void setGameLevel(int level) {
        _gameLevel = level;
    }

    static int getGameLevel() {
        return _gameLevel;
    }

    static void setEasyTimeRecord(String easyTime) {
        _easyTimeRecord = easyTime;
    }

    static String getEasyTimeRecord() {
        return _easyTimeRecord;
    }

    static void setMediumTimeRecord(String mediumTime) {
        _mediumTimeRecord = mediumTime;
    }

    static String getMediumTimeRecord() {
        return _mediumTimeRecord;
    }

    static void setHardTimeRecord(String hardTime) {
        _hardTimeRecord = hardTime;
    }

    static String getHardTimeRecord() {
        return _hardTimeRecord;
    }

    static void setFinalSocre(Long score) {
        _finalScore += score.longValue();
    }

    static Long getFinalScore() {
        return Long.valueOf(_finalScore);
    }

    static void setFinalScoreZero() {
        _finalScore = 0;
    }

    public void onCreate() {
        super.onCreate();
        ScoreloopManagerSingleton.init(this, this.gameScret);
    }

    public void onTerminate() {
        super.onTerminate();
        ScoreloopManagerSingleton.destroy();
    }
}
