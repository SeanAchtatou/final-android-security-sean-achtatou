package com.scoreloop.client.android.ui.component.game;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.sunnysideblue.mathmate.R;

public class GameDetailListItem extends BaseListItem {
    private final Game _game;

    public GameDetailListItem(Context context, Drawable drawable, Game game) {
        super(context, drawable, game.getDescription());
        this._game = game;
    }

    public int getType() {
        return 13;
    }

    public View getView(View view, ViewGroup parent) {
        if (view == null) {
            view = getLayoutInflater().inflate((int) R.layout.sl_list_item_game_detail, (ViewGroup) null);
        }
        TextView detail = (TextView) view.findViewById(R.id.sl_list_item_game_detail_text);
        String description = this._game.getDescription();
        if (description == null) {
            description = this._game.getPublisherName();
        }
        if (description != null) {
            detail.setText(description.replace("\r", ""));
        }
        return view;
    }

    public boolean isEnabled() {
        return false;
    }
}
