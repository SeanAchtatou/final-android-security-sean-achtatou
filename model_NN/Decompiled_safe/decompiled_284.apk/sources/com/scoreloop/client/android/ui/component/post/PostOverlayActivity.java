package com.scoreloop.client.android.ui.component.post;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import com.scoreloop.client.android.core.controller.MessageController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerException;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.SocialProviderController;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.model.Entity;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.ui.framework.BaseActivity;
import com.sunnysideblue.mathmate.R;
import java.util.HashMap;
import java.util.Map;

public class PostOverlayActivity extends Activity implements RequestControllerObserver, CompoundButton.OnCheckedChangeListener, SocialProviderControllerObserver {
    private static final int DIALOG_CONNECT_FAILED = 101;
    private static final int DIALOG_ERROR_NETWORK = 100;
    private static final int DIALOG_PROGRESS = 102;
    private static Entity _messageTarget = null;
    /* access modifiers changed from: private */
    public final Map<CheckBox, SocialProvider> _checkboxToProviderMap = new HashMap();
    private Handler _handler = new Handler();
    /* access modifiers changed from: private */
    public MessageController _messageController;
    /* access modifiers changed from: private */
    public EditText _messageEditText;
    private Button _noButton;
    private Button _postButton;
    private final Map<SocialProvider, CheckBox> _providerToCheckboxMap = new HashMap();

    public static void setMessageTarget(Entity messageTarget) {
        _messageTarget = messageTarget;
    }

    public static boolean isPosted(Context context, Entity messageTarget) {
        return false;
    }

    public static void setPosted(Context context, Entity messageTarget) {
    }

    private void addCheckbox(String socialProviderId, int checkboxId) {
        SocialProvider provider = SocialProvider.getSocialProviderForIdentifier(socialProviderId);
        CheckBox checkBox = (CheckBox) findViewById(checkboxId);
        this._checkboxToProviderMap.put(checkBox, provider);
        this._providerToCheckboxMap.put(provider, checkBox);
        checkBox.setOnCheckedChangeListener(this);
    }

    /* access modifiers changed from: private */
    public void blockUI(boolean block) {
        if (block) {
            showDialog(DIALOG_PROGRESS);
        } else {
            dismissDialog(DIALOG_PROGRESS);
        }
        boolean enabled = !block;
        this._postButton.setEnabled(enabled);
        this._noButton.setEnabled(enabled);
        this._messageEditText.setEnabled(enabled);
        for (CheckBox checkBox : this._checkboxToProviderMap.keySet()) {
            checkBox.setEnabled(enabled);
        }
    }

    private Dialog createErrorDialog(int messageResId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(messageResId));
        Dialog dialog = builder.create();
        dialog.getWindow().requestFeature(1);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    private Dialog createProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("progress");
        return dialog;
    }

    /* access modifiers changed from: protected */
    public Entity getMessageTarget() {
        return _messageTarget;
    }

    /* access modifiers changed from: protected */
    public String getPostText() {
        return "Achievement";
    }

    public void onBackPressed() {
    }

    public void onCheckedChanged(CompoundButton button, boolean isChecked) {
        if (isChecked) {
            final SocialProvider provider = this._checkboxToProviderMap.get((CheckBox) button);
            if (!provider.isUserConnected(Session.getCurrentSession().getUser())) {
                blockUI(true);
                this._handler.post(new Runnable() {
                    public void run() {
                        SocialProviderController.getSocialProviderController(null, PostOverlayActivity.this, provider).connect(PostOverlayActivity.this);
                    }
                });
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sl_post);
        Entity messageTarget = getMessageTarget();
        if (messageTarget == null || messageTarget.getIdentifier() == null) {
            finish();
            return;
        }
        this._messageController = new MessageController(this);
        ((TextView) findViewById(R.id.sl_post_text)).setText(String.format(getString(R.string.sl_format_post), getPostText()));
        this._noButton = (Button) findViewById(R.id.cancel_button);
        this._noButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PostOverlayActivity.this.finish();
            }
        });
        this._messageEditText = (EditText) findViewById(R.id.message_edittext);
        this._postButton = (Button) findViewById(R.id.ok_button);
        this._postButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PostOverlayActivity.this._messageController.setTarget(PostOverlayActivity.this.getMessageTarget());
                for (CheckBox checkBox : PostOverlayActivity.this._checkboxToProviderMap.keySet()) {
                    SocialProvider provider = (SocialProvider) PostOverlayActivity.this._checkboxToProviderMap.get(checkBox);
                    if (checkBox.isChecked()) {
                        PostOverlayActivity.this._messageController.addReceiverWithUsers(provider, new Object[0]);
                    }
                }
                PostOverlayActivity.this._messageController.setText(PostOverlayActivity.this._messageEditText.getText().toString());
                if (PostOverlayActivity.this._messageController.isSubmitAllowed()) {
                    PostOverlayActivity.this.blockUI(true);
                    PostOverlayActivity.this._messageController.submitMessage();
                }
            }
        });
        addCheckbox(SocialProvider.FACEBOOK_IDENTIFIER, R.id.facebook_checkbox);
        addCheckbox(SocialProvider.MYSPACE_IDENTIFIER, R.id.myspace_checkbox);
        addCheckbox(SocialProvider.TWITTER_IDENTIFIER, R.id.twitter_checkbox);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 100:
                return createErrorDialog(R.string.sl_error_message_network);
            case DIALOG_CONNECT_FAILED /*101*/:
                return createErrorDialog(R.string.sl_error_message_connect_failed);
            case DIALOG_PROGRESS /*102*/:
                return createProgressDialog();
            default:
                return null;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return false;
    }

    public void requestControllerDidFail(RequestController aRequestController, Exception anException) {
        if (aRequestController == this._messageController) {
            blockUI(false);
            for (SocialProvider provider : SocialProvider.getSupportedProviders()) {
                updateProviderCheckbox(provider);
            }
            if (!(anException instanceof RequestControllerException) || ((RequestControllerException) anException).getErrorCode() != 110) {
                showDialog(100);
            } else {
                showDialog(DIALOG_CONNECT_FAILED);
            }
        }
    }

    public void requestControllerDidReceiveResponse(RequestController aRequestController) {
        if (aRequestController == this._messageController) {
            dismissDialog(DIALOG_PROGRESS);
            BaseActivity.showToast(this, String.format(getResources().getString(R.string.sl_format_posted), getPostText()));
            setPosted(getApplicationContext(), getMessageTarget());
            finish();
        }
    }

    public void socialProviderControllerDidCancel(SocialProviderController controller) {
        blockUI(false);
        updateProviderCheckbox(controller.getSocialProvider());
    }

    public void socialProviderControllerDidEnterInvalidCredentials(SocialProviderController controller) {
        blockUI(false);
        updateProviderCheckbox(controller.getSocialProvider());
        showDialog(DIALOG_CONNECT_FAILED);
    }

    public void socialProviderControllerDidFail(SocialProviderController controller, Throwable error) {
        blockUI(false);
        updateProviderCheckbox(controller.getSocialProvider());
        showDialog(100);
    }

    public void socialProviderControllerDidSucceed(SocialProviderController controller) {
        blockUI(false);
        updateProviderCheckbox(controller.getSocialProvider());
    }

    private void updateProviderCheckbox(SocialProvider provider) {
        if (!provider.isUserConnected(Session.getCurrentSession().getUser())) {
            this._providerToCheckboxMap.get(provider).setChecked(false);
        }
    }
}
