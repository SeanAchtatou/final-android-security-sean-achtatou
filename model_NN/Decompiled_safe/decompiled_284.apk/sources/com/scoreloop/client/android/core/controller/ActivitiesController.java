package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import com.scoreloop.client.android.core.PublishedFor__2_1_0;
import com.scoreloop.client.android.core.PublishedFor__2_2_0;
import com.scoreloop.client.android.core.model.Activity;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.sunnysideblue.mathmate.ListGameString;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;

public class ActivitiesController extends RequestController {
    private List<Activity> c;

    private static class a extends Request {
        private final Game a;
        private final b b;
        private final User c;

        public a(RequestCompletionCallback requestCompletionCallback, Game game, User user, b bVar) {
            super(requestCompletionCallback);
            this.c = user;
            this.b = bVar;
            this.a = game;
        }

        public String a() {
            switch (this.b) {
                case BUDDY:
                    if (this.c == null) {
                        throw new IllegalStateException("internal error: no _user set");
                    }
                    return String.format("/service/users/%s/buddies/last_activities", this.c.getIdentifier());
                case GAME:
                    if (this.a == null) {
                        throw new IllegalStateException("internal error: no _game set");
                    }
                    return String.format("/service/games/%s/activities", this.a.getIdentifier());
                case USER:
                    if (this.c == null) {
                        throw new IllegalStateException("internal error: no _user set");
                    }
                    return String.format("/service/users/%s/activities", this.c.getIdentifier());
                case COMMUNITY:
                    return "/service/activities";
                default:
                    return null;
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    private enum b {
        BUDDY,
        COMMUNITY,
        GAME,
        USER
    }

    @PublishedFor__1_0_0
    public ActivitiesController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    @PublishedFor__1_0_0
    public ActivitiesController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.c = Collections.emptyList();
    }

    private void a(List<Activity> list) {
        this.c = Collections.unmodifiableList(list);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        if (response.f() == 200) {
            ArrayList arrayList = new ArrayList();
            JSONArray d = response.d();
            Session h = h();
            for (int i = 0; i < d.length(); i++) {
                arrayList.add(new Activity(h, d.getJSONObject(i).getJSONObject(Activity.a)));
            }
            a(arrayList);
            return true;
        }
        throw new IllegalStateException("invalid response status: " + response.f());
    }

    @PublishedFor__1_0_0
    public List<Activity> getActivities() {
        return this.c;
    }

    @PublishedFor__2_1_0
    public void loadActivitiesForGame(Game game) {
        if (game == null || game.getIdentifier() == null) {
            throw new IllegalArgumentException("invalid game argument");
        }
        a_();
        a aVar = new a(g(), game, null, b.GAME);
        aVar.a((long) ListGameString.HARD_TIME);
        b(aVar);
    }

    @PublishedFor__1_1_0
    public void loadActivitiesForUser(User user) {
        if (user == null) {
            throw new IllegalArgumentException("invalid user argument");
        }
        a_();
        a aVar = new a(g(), null, user, b.USER);
        aVar.a((long) ListGameString.HARD_TIME);
        b(aVar);
    }

    @PublishedFor__1_0_0
    public void loadBuddyActivities() {
        a_();
        a aVar = new a(g(), null, i(), b.BUDDY);
        aVar.a((long) ListGameString.HARD_TIME);
        b(aVar);
    }

    @PublishedFor__2_2_0
    public void loadCommunityActivities() {
        a_();
        a aVar = new a(g(), null, null, b.COMMUNITY);
        aVar.a((long) ListGameString.HARD_TIME);
        b(aVar);
    }

    @PublishedFor__1_0_0
    public void loadGameActivities() {
        if (getGame() == null) {
            throw new IllegalArgumentException("using loadGameActivities does not make sense without gameID being set on AcitiviesController instance");
        }
        a_();
        a aVar = new a(g(), getGame(), null, b.GAME);
        aVar.a((long) ListGameString.HARD_TIME);
        b(aVar);
    }
}
