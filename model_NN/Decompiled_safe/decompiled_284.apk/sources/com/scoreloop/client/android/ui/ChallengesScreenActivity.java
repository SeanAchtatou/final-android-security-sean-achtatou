package com.scoreloop.client.android.ui;

import android.os.Bundle;
import com.scoreloop.client.android.ui.component.base.Configuration;
import com.scoreloop.client.android.ui.framework.ScreenActivity;

public class ChallengesScreenActivity extends ScreenActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StandardScoreloopManager manager = StandardScoreloopManager.getFactory(ScoreloopManagerSingleton.get());
        if (!manager.getConfiguration().isFeatureEnabled(Configuration.Feature.CHALLENGE)) {
            finish();
        } else {
            display(manager.createChallengeScreenDescription(null), savedInstanceState);
        }
    }
}
