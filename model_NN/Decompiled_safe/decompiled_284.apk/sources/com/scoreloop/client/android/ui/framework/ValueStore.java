package com.scoreloop.client.android.ui.framework;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ValueStore {
    private static final String PATH_SEPARATOR = "/";
    private final Map<String, Date> _dates = new HashMap();
    private ValueSourceFactory _factory;
    private Map<String, List<WeakReference<Observer>>> _observerMap;
    private List<ValueSource> _valueSources;
    private final Map<String, Object> _values = new HashMap();

    public interface Observer {
        void onValueChanged(ValueStore valueStore, String str, Object obj, Object obj2);

        void onValueSetDirty(ValueStore valueStore, String str);
    }

    private interface ObserverContinuation {
        void withObserver(Observer observer);
    }

    public enum RetrievalMode {
        NOT_DIRTY,
        NOT_OLDER_THAN
    }

    public interface ValueSource {
        boolean isRetrieving();

        void retrieve(ValueStore valueStore);

        void supportedKeys(Set<String> set);
    }

    public interface ValueSourceFactory {
        ValueSource getValueSourceForKeyInStore(String str, ValueStore valueStore);
    }

    private static class Node {
        String firstKey;
        String restKey;
        ValueStore restStore;

        private Node() {
        }

        /* synthetic */ Node(Node node) {
            this();
        }
    }

    public static String concatenateKeys(String... keys) {
        if (keys.length == 0) {
            return null;
        }
        if (keys.length == 1) {
            return keys[0];
        }
        StringBuilder builder = new StringBuilder();
        builder.append(keys[0]);
        for (int i = 1; i < keys.length; i++) {
            builder.append(PATH_SEPARATOR);
            builder.append(keys[i]);
        }
        return builder.toString();
    }

    private static boolean isPlainKey(String key) {
        return key.indexOf(PATH_SEPARATOR) == -1;
    }

    private static String[] splitKeyComponents(String key) {
        return key.split(PATH_SEPARATOR, 2);
    }

    public void addObserver(String key, Observer observer) {
        if (isPlainKey(key)) {
            List<WeakReference<Observer>> observers = getObserverMap().get(key);
            if (observers == null) {
                observers = new ArrayList<>();
                this._observerMap.put(key, observers);
            }
            observers.add(new WeakReference(observer));
            return;
        }
        Node node = getNode(key, true);
        node.restStore.addObserver(node.restKey, observer);
    }

    public void addValueSources(ValueSource... sources) {
        Collections.addAll(getValueSources(), sources);
    }

    /* access modifiers changed from: package-private */
    public void copyFromOtherForKeys(ValueStore otherValues, Set<String> keys) {
        for (String key : keys) {
            putValue(key, otherValues.getValue(key));
        }
    }

    private void forAllObservers(String key, ObserverContinuation continuation) {
        List<WeakReference<Observer>> observers = getObserverMap().get(key);
        if (observers != null) {
            for (WeakReference<Observer> weakObserver : new ArrayList<>(observers)) {
                Observer observer = (Observer) weakObserver.get();
                if (observer != null) {
                    continuation.withObserver(observer);
                } else {
                    observers.remove(weakObserver);
                }
            }
        }
    }

    private Node getNode(String key, boolean doCreate) {
        Node node = new Node(null);
        String[] components = splitKeyComponents(key);
        node.firstKey = components[0];
        node.restKey = components[1];
        node.restStore = (ValueStore) this._values.get(node.firstKey);
        if (node.restStore == null && doCreate) {
            node.restStore = new ValueStore();
            this._values.put(node.firstKey, node.restStore);
        }
        return node;
    }

    private Map<String, List<WeakReference<Observer>>> getObserverMap() {
        if (this._observerMap == null) {
            this._observerMap = new HashMap();
        }
        return this._observerMap;
    }

    public <T> T getValue(String key) {
        return getValue(key, null);
    }

    public <T> T getValue(String key, T defaultIfNotFound) {
        if (!isPlainKey(key)) {
            Node node = getNode(key, false);
            if (node.restStore != null) {
                return node.restStore.getValue(node.restKey, defaultIfNotFound);
            }
        } else if (this._values.containsKey(key)) {
            return this._values.get(key);
        }
        return defaultIfNotFound;
    }

    private ValueSource getValueSource(String key) {
        Set<String> keys = new HashSet<>();
        for (ValueSource source : getValueSources()) {
            source.supportedKeys(keys);
            if (keys.contains(key)) {
                return source;
            }
        }
        if (this._factory == null) {
            return null;
        }
        ValueSource valueSource = this._factory.getValueSourceForKeyInStore(key, this);
        if (valueSource != null) {
            addValueSources(valueSource);
        }
        return valueSource;
    }

    private List<ValueSource> getValueSources() {
        if (this._valueSources == null) {
            this._valueSources = new ArrayList();
        }
        return this._valueSources;
    }

    private void invokeChangedObservers(final String key, final Object oldValue, final Object newValue) {
        forAllObservers(key, new ObserverContinuation() {
            public void withObserver(Observer observer) {
                observer.onValueChanged(ValueStore.this, key, oldValue, newValue);
            }
        });
    }

    private void invokeSetDirtyObservers(final String key) {
        forAllObservers(key, new ObserverContinuation() {
            public void withObserver(Observer observer) {
                observer.onValueSetDirty(ValueStore.this, key);
            }
        });
    }

    private boolean isClean(String key, RetrievalMode mode, Object argument) {
        Date date = this._dates.get(key);
        if (mode == RetrievalMode.NOT_DIRTY) {
            if (date != null) {
                return true;
            }
            return false;
        } else if (mode != RetrievalMode.NOT_OLDER_THAN) {
            return true;
        } else {
            if (date == null) {
                return false;
            }
            Date reference = new Date();
            reference.setTime(reference.getTime() - ((Long) argument).longValue());
            return !date.before(reference);
        }
    }

    public boolean isDirty(String key) {
        if (isPlainKey(key)) {
            return this._dates.get(key) == null;
        }
        Node node = getNode(key, false);
        if (node.restStore != null) {
            return node.restStore.isDirty(node.restKey);
        }
        return false;
    }

    public void putValue(String key, Object newValue) {
        if (isPlainKey(key)) {
            Object oldValue = this._values.get(key);
            this._values.put(key, newValue);
            this._dates.put(key, new Date());
            if (oldValue != newValue) {
                invokeChangedObservers(key, oldValue, newValue);
                return;
            }
            return;
        }
        Node node = getNode(key, true);
        node.restStore.putValue(node.restKey, newValue);
    }

    public void removeObserver(String key, Observer anObserver) {
        if (isPlainKey(key)) {
            List<WeakReference<Observer>> observers = getObserverMap().get(key);
            if (observers != null) {
                for (WeakReference<Observer> weakObserver : new ArrayList<>(observers)) {
                    Observer observer = (Observer) weakObserver.get();
                    if (observer == null || observer == anObserver) {
                        observers.remove(weakObserver);
                    }
                }
                return;
            }
            return;
        }
        Node node = getNode(key, false);
        if (node.restStore != null) {
            node.restStore.removeObserver(node.restKey, anObserver);
        }
    }

    public boolean retrieveValue(String key, RetrievalMode mode, Object argument) {
        if (!isPlainKey(key)) {
            Node node = getNode(key, false);
            if (node.restStore != null) {
                return node.restStore.retrieveValue(node.restKey, mode, argument);
            }
            return false;
        } else if (isClean(key, mode, argument)) {
            return true;
        } else {
            ValueSource source = getValueSource(key);
            if (source != null && !source.isRetrieving()) {
                source.retrieve(this);
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void runObserverForKeys(ValueStore oldValues, Set<String> keys, Observer observer) {
        ValueStore oldRestStore;
        Object oldValue;
        for (String key : keys) {
            if (isPlainKey(key)) {
                observer.onValueSetDirty(this, key);
                if (oldValues != null) {
                    oldValue = oldValues.getValue(key);
                } else {
                    oldValue = null;
                }
                Object newValue = getValue(key);
                if (oldValue != newValue) {
                    observer.onValueChanged(this, key, oldValue, newValue);
                }
            } else {
                Node node = getNode(key, false);
                if (node.restStore != null) {
                    if (oldValues != null) {
                        oldRestStore = (ValueStore) oldValues.getValue(node.firstKey);
                    } else {
                        oldRestStore = null;
                    }
                    node.restStore.runObserverForKeys(oldRestStore, Collections.singleton(node.restKey), observer);
                }
            }
        }
    }

    public void setAllDirty() {
        this._dates.clear();
        for (String key : this._values.keySet()) {
            Object value = this._values.get(key);
            if (value instanceof ValueStore) {
                ((ValueStore) value).setAllDirty();
            } else {
                invokeSetDirtyObservers(key);
            }
        }
    }

    public void setDirty(String key) {
        if (isPlainKey(key)) {
            this._dates.put(key, null);
            invokeSetDirtyObservers(key);
            return;
        }
        Node node = getNode(key, false);
        if (node.restStore != null) {
            node.restStore.setDirty(node.restKey);
        }
    }

    public void setValueSourceFactroy(ValueSourceFactory factory) {
        this._factory = factory;
    }

    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append(super.toString());
        buffer.append(" [");
        boolean isFirst = true;
        for (String key : this._values.keySet()) {
            if (!isFirst) {
                buffer.append(", ");
            }
            buffer.append(key);
            buffer.append("=");
            Object value = this._values.get(key);
            buffer.append(value != null ? value.toString() : "NULL");
            if (this._dates.get(key) == null) {
                buffer.append("(*)");
            }
            isFirst = false;
        }
        buffer.append("]");
        return buffer.toString();
    }
}
