package com.scoreloop.client.android.ui.component.achievement;

import android.content.Intent;
import android.os.Bundle;
import com.scoreloop.client.android.core.controller.AchievementsController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.model.Achievement;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.post.PostOverlayActivity;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;

public class AchievementListActivity extends ComponentListActivity<AchievementListItem> {
    private AchievementsController _achievementsController;

    /* access modifiers changed from: private */
    public void onAchievements() {
        BaseListAdapter<AchievementListItem> adapter = getBaseListAdapter();
        adapter.clear();
        boolean isSessionUser = isSessionUser();
        for (Achievement achievement : this._achievementsController.getAchievements()) {
            adapter.add(new AchievementListItem(this, achievement, isSessionUser));
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new BaseListAdapter(this));
        if (isSessionUser()) {
            this._achievementsController = ((AchievementsEngine) getActivityArguments().getValue(Constant.ACHIEVEMENTS_ENGINE)).getAchievementsController();
        } else {
            this._achievementsController = new AchievementsController(getRequestControllerObserver());
        }
    }

    public void onListItemClick(AchievementListItem item) {
        Achievement achievement = item.getAchievement();
        if (item.isEnabled() && !PostOverlayActivity.isPosted(getApplicationContext(), achievement)) {
            Intent intent = new Intent(this, PostOverlayActivity.class);
            PostOverlayActivity.setMessageTarget(achievement);
            startActivity(intent);
        }
    }

    public void onRefresh(int flags) {
        if (isSessionUser()) {
            showSpinner();
            ((AchievementsEngine) getActivityArguments().getValue(Constant.ACHIEVEMENTS_ENGINE)).submitAchievements(true, new Runnable() {
                public void run() {
                    AchievementListActivity.this.hideSpinner();
                    AchievementListActivity.this.onAchievements();
                }
            });
            return;
        }
        showSpinnerFor(this._achievementsController);
        this._achievementsController.setUser(getUser());
        this._achievementsController.loadAchievements();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        setNeedsRefresh();
    }

    public void requestControllerDidReceiveResponseSafe(RequestController aRequestController) {
        onAchievements();
    }
}
