package com.scoreloop.client.android.ui.component.base;

import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.framework.ScreenDescription;

public interface Factory {
    ScreenDescription createAchievementScreenDescription(User user);

    ScreenDescription createChallengeAcceptScreenDescription(Challenge challenge);

    ScreenDescription createChallengeCreateScreenDescription(User user, Integer num);

    ScreenDescription createChallengePaymentScreenDescription();

    ScreenDescription createChallengeScreenDescription(User user);

    ScreenDescription createEntryScreenDescription();

    ScreenDescription createGameDetailScreenDescription(Game game);

    ScreenDescription createGameScreenDescription(User user, int i);

    ScreenDescription createMarketScreenDescription(User user);

    ScreenDescription createNewsScreenDescription();

    ScreenDescription createProfileSettingsPictureScreenDescription(User user);

    ScreenDescription createProfileSettingsScreenDescription(User user);

    ScreenDescription createScoreScreenDescription(Game game, Integer num, Integer num2);

    ScreenDescription createUserAddBuddyScreenDescription();

    ScreenDescription createUserDetailScreenDescription(User user, Boolean bool);

    ScreenDescription createUserScreenDescription(User user);
}
