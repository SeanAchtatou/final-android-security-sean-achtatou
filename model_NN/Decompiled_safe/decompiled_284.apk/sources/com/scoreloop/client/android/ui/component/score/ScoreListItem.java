package com.scoreloop.client.android.ui.component.score;

import android.graphics.drawable.Drawable;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.scoreloop.client.android.ui.component.base.StringFormatter;
import com.sunnysideblue.mathmate.R;

public class ScoreListItem extends StandardListItem<Score> {
    private boolean _isEnabled;

    public ScoreListItem(ComponentActivity activity, Score score, boolean isEnabled) {
        super(activity, null, StringFormatter.getScoreTitle(activity, score), StringFormatter.formatLeaderboardsScore(score, activity.getConfiguration()), score);
        this._isEnabled = isEnabled;
    }

    /* access modifiers changed from: protected */
    public String getImageUrl() {
        User user = ((Score) getTarget()).getUser();
        if (user == null) {
            user = Session.getCurrentSession().getUser();
        }
        return user.getImageUrl();
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_score;
    }

    /* access modifiers changed from: protected */
    public int getSubTitleId() {
        return R.id.sl_list_item_score_result;
    }

    /* access modifiers changed from: protected */
    public int getTitleId() {
        return R.id.sl_list_item_score_title;
    }

    public int getType() {
        return 19;
    }

    public Drawable getDrawable() {
        return getContext().getResources().getDrawable(R.drawable.sl_icon_user);
    }

    public boolean isEnabled() {
        return this._isEnabled;
    }
}
