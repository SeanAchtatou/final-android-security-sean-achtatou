package com.scoreloop.client.android.core.util;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;

public class OAuthBuilder {
    private final Random a = new Random();
    private String b;
    private URL c;
    private String d;
    private Map<String, String> e;
    private String f;

    private String a() {
        return Long.toString(this.a.nextLong());
    }

    private String a(String str, String str2, String str3) {
        Object[] objArr = new Object[2];
        objArr[0] = c(str2);
        objArr[1] = str3 != null ? str3 : "";
        try {
            return c(a(str.getBytes("UTF-8"), String.format("%s&%s", objArr).getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e2) {
            throw new IllegalStateException(e2);
        }
    }

    private static String a(byte[] bArr, byte[] bArr2) {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr2, "HmacSHA1");
            Mac instance = Mac.getInstance("HmacSHA1");
            instance.init(secretKeySpec);
            return Base64.a(instance.doFinal(bArr));
        } catch (Exception e2) {
            throw new IllegalStateException("Failed to generate HMAC : " + e2.getMessage());
        }
    }

    private static List<String> a(Map<String, String> map) {
        if (map == null) {
            throw new IllegalArgumentException();
        }
        ArrayList arrayList = new ArrayList(map.keySet());
        Collections.sort(arrayList);
        return arrayList;
    }

    private String b() {
        return new Long(System.currentTimeMillis() / 1000).toString();
    }

    private String c() {
        return this.d;
    }

    private static String c(String str) {
        return URLEncoder.encode(str);
    }

    private void c(String str, String str2) {
        if (d() == null || f() == null || c() == null) {
            throw new IllegalArgumentException();
        }
        i();
        String a2 = a(h(), str, str2);
        Logger.a("signature", a2);
        this.e.put("oauth_signature", a2);
    }

    private String d() {
        return this.b;
    }

    private void d(String str) {
        if (str == null) {
            throw new IllegalArgumentException();
        } else if (this.b != null) {
            throw new IllegalStateException("must be called only once");
        } else {
            this.b = str.toUpperCase();
        }
    }

    private Map<String, String> e() {
        return this.e;
    }

    private URL f() {
        return this.c;
    }

    private String g() {
        return this.f;
    }

    private String h() {
        StringBuilder sb = new StringBuilder();
        sb.append(d());
        sb.append("&");
        sb.append(c(f().toString()));
        sb.append("&");
        boolean z = true;
        Iterator<String> it = a(e()).iterator();
        while (true) {
            boolean z2 = z;
            if (it.hasNext()) {
                String next = it.next();
                if (!z2) {
                    sb.append("%26");
                }
                sb.append(c(next));
                sb.append("%3D");
                sb.append(c(e().get(next)));
                z = false;
            } else {
                String sb2 = sb.toString();
                Logger.a("signature base", sb2);
                return sb2;
            }
        }
    }

    private void i() {
        if (this.e != null) {
            throw new IllegalStateException("must be called only once");
        }
        this.e = new HashMap();
        this.e.put("oauth_consumer_key", c());
        this.e.put("oauth_nonce", a());
        this.e.put("oauth_signature_method", "HMAC-SHA1");
        this.e.put("oauth_timestamp", b());
        this.e.put("oauth_version", "1.0");
        if (g() != null) {
            this.e.put("oauth_token", g());
        }
    }

    public URL a(String str, Map<String, String> map) {
        this.e = map;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        boolean z = true;
        Iterator<String> it = a(e()).iterator();
        while (true) {
            boolean z2 = z;
            if (it.hasNext()) {
                String next = it.next();
                if (z2) {
                    sb.append("?");
                } else {
                    sb.append("&");
                }
                sb.append(next);
                sb.append("=");
                sb.append(e().get(next));
                z = false;
            } else {
                try {
                    return new URL(sb.toString());
                } catch (MalformedURLException e2) {
                    throw new IllegalStateException(e2);
                }
            }
        }
    }

    public HttpGet a(String str, String str2) {
        d("get");
        c(str, str2);
        try {
            return new HttpGet(a(f().toString(), e()).toURI());
        } catch (URISyntaxException e2) {
            throw new IllegalStateException(e2);
        }
    }

    public void a(String str) {
        if (str == null) {
            throw new IllegalArgumentException();
        } else if (this.d != null) {
            throw new IllegalStateException("must be called only once");
        } else {
            this.d = str;
        }
    }

    public void a(URL url) {
        if (url == null) {
            throw new IllegalArgumentException();
        } else if (this.c != null) {
            throw new IllegalStateException("must be called only once");
        } else {
            this.c = url;
        }
    }

    public HttpPut b(String str, String str2) {
        d("put");
        c(str, str2);
        try {
            return new HttpPut(a(f().toString(), e()).toURI());
        } catch (URISyntaxException e2) {
            throw new IllegalStateException(e2);
        }
    }

    public void b(String str) {
        if (str == null) {
            throw new IllegalArgumentException();
        } else if (this.f != null) {
            throw new IllegalStateException("must be called only once");
        } else {
            this.f = str;
        }
    }
}
