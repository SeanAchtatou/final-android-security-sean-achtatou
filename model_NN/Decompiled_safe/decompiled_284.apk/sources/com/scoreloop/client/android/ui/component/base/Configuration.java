package com.scoreloop.client.android.ui.component.base;

import android.content.Context;
import android.util.Log;
import com.scoreloop.client.android.core.controller.AchievementsController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.model.Client;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.ScoreFormatter;
import com.scoreloop.client.android.core.model.Session;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.IllegalFormatException;
import java.util.Properties;

public class Configuration {
    private static final String ACHIVEMENT_INITIAL_SYNC = "ui.feature.achievement.forceSync";
    private static final String FORMAT_MONEY_KEY = "ui.format.money";
    private static final String FORMAT_SCORE_CHALLENGES = "ui.format.score.challenges";
    private static final String FORMAT_SCORE_KEY = "ui.format.score";
    private static final String FORMAT_SCORE_LEADERBOARD = "ui.format.score.leaderboard";
    private static final String FORMAT_SCORE_RESULT_KEY = "ui.format.score.result";
    private static final String FORMAT_SCORE_SOCIAL_NETWORK_POST = "ui.format.score.socialnetworkpost";
    private static final String RES_MODES_NAME = "ui.res.modes.name";
    private boolean _achievementForceInitialSync = true;
    private ScoreFormatter.ScoreFormatKey _challengesScoreFormat;
    private ScoreFormatter.ScoreFormatKey _leaderboardScoreFormat;
    private String[] _modesNames;
    private int _modesResId;
    private String _moneyFormat = "%.2f %s";
    private ScoreFormatter _scoreFormatter;
    private String _scoreResultFormat;
    private ScoreFormatter.ScoreFormatKey _socialNetworkPostScoreFormat;

    static class ConfigurationException extends IllegalStateException {
        private static final String SCORELOOP_UI = "ScoreloopUI";
        private static final long serialVersionUID = 1;

        ConfigurationException(String message) {
            super(message);
            Log.e("ScoreloopUI", "=====================================================================================");
            Log.e("ScoreloopUI", "scoreloop.properties file verification error. Please resolve any issues first!");
            Log.e("ScoreloopUI", message);
        }
    }

    public enum Feature {
        ACHIEVEMENT("ui.feature.achievement", false),
        ADDRESS_BOOK("ui.feature.address_book", true),
        CHALLENGE("ui.feature.challenge", false),
        NEWS("ui.feature.news", false);
        
        private boolean _isEnabled = true;
        private String _propertyName;

        private Feature(String propertyName, boolean preset) {
            this._propertyName = propertyName;
            this._isEnabled = preset;
        }

        /* access modifiers changed from: package-private */
        public String getPropertyName() {
            return this._propertyName;
        }

        /* access modifiers changed from: package-private */
        public boolean isEnabled() {
            return this._isEnabled;
        }

        /* access modifiers changed from: package-private */
        public void setEnabled(boolean value) {
            this._isEnabled = value;
        }
    }

    public Configuration(Context context, Session session) {
        Properties properties = Client.getProperties(context);
        Feature[] features = Feature.values();
        for (Feature feature : features) {
            String property = feature.getPropertyName();
            String value = properties.getProperty(property);
            if (value != null) {
                feature.setEnabled(verifyBooleanProperty(value.trim(), property));
            }
        }
        String value2 = properties.getProperty(ACHIVEMENT_INITIAL_SYNC);
        if (value2 != null) {
            this._achievementForceInitialSync = verifyBooleanProperty(value2.trim(), ACHIVEMENT_INITIAL_SYNC);
        }
        this._scoreResultFormat = properties.getProperty(FORMAT_SCORE_RESULT_KEY);
        if (this._scoreResultFormat != null) {
            this._scoreResultFormat = this._scoreResultFormat.trim();
        }
        this._scoreFormatter = new ScoreFormatter(properties.getProperty(FORMAT_SCORE_KEY));
        this._leaderboardScoreFormat = loadScoreFormatProperty(properties, FORMAT_SCORE_LEADERBOARD);
        this._challengesScoreFormat = loadScoreFormatProperty(properties, FORMAT_SCORE_CHALLENGES);
        this._socialNetworkPostScoreFormat = loadScoreFormatProperty(properties, FORMAT_SCORE_SOCIAL_NETWORK_POST);
        Game game = session.getGame();
        if (game == null || !game.hasModes()) {
            this._modesNames = new String[0];
        } else {
            int minMode = game.getMinMode().intValue();
            int modeCount = game.getModeCount().intValue();
            this._modesNames = new String[modeCount];
            for (int i = minMode; i < minMode + modeCount; i++) {
                this._modesNames[i] = this._scoreFormatter.formatScore(new Score(null, Collections.singletonMap(Game.CONTEXT_KEY_MODE, Integer.valueOf(i))), ScoreFormatter.ScoreFormatKey.ModeOnlyFormat);
            }
        }
        this._moneyFormat = properties.getProperty(FORMAT_MONEY_KEY, this._moneyFormat).trim();
        String modesResName = properties.getProperty(RES_MODES_NAME);
        if (modesResName != null) {
            this._modesResId = context.getResources().getIdentifier(modesResName.trim(), "array", context.getPackageName());
            Log.i("test", "Type: " + context.getResources().getResourceTypeName(this._modesResId));
        }
        verifyConfiguration(context, session);
    }

    public int getModesResId() {
        return this._modesResId;
    }

    public String[] getModesNames() {
        return this._modesNames;
    }

    public String getMoneyFormat() {
        return this._moneyFormat;
    }

    public boolean isAchievementForceInitialSync() {
        return this._achievementForceInitialSync;
    }

    public ScoreFormatter.ScoreFormatKey getLeaderboardScoreFormat() {
        return this._leaderboardScoreFormat;
    }

    public ScoreFormatter.ScoreFormatKey getChallengesScoreFormat() {
        return this._challengesScoreFormat;
    }

    public ScoreFormatter.ScoreFormatKey getSocialNetworkPostScoreFormat() {
        return this._socialNetworkPostScoreFormat;
    }

    public String getScoreResultFormat() {
        return this._scoreResultFormat;
    }

    public ScoreFormatter getScoreFormatter() {
        return this._scoreFormatter;
    }

    public boolean isFeatureEnabled(Feature feature) {
        return feature.isEnabled();
    }

    private ScoreFormatter.ScoreFormatKey loadScoreFormatProperty(Properties properties, String propertyName) {
        String format = properties.getProperty(propertyName);
        ScoreFormatter.ScoreFormatKey scoreFormatKey = null;
        if (format == null || (scoreFormatKey = ScoreFormatter.ScoreFormatKey.parse(format)) != null) {
            return scoreFormatKey;
        }
        throw new ConfigurationException("invalid " + propertyName + " value (unrecognized format key): " + format);
    }

    private boolean verifyBooleanProperty(String value, String property) {
        if (value.equalsIgnoreCase("false")) {
            return false;
        }
        if (value.equalsIgnoreCase("true")) {
            return true;
        }
        throw new ConfigurationException("property " + property + " must be either 'true' or 'false'");
    }

    /* access modifiers changed from: protected */
    public void verifyConfiguration(Context context, Session session) {
        if (!Feature.access$2(Feature.ACHIEVEMENT) || new AchievementsController(new RequestControllerObserver() {
            public void requestControllerDidFail(RequestController aRequestController, Exception anException) {
            }

            public void requestControllerDidReceiveResponse(RequestController aRequestController) {
            }
        }).getAwardList() != null) {
            Game game = session.getGame();
            if (game != null && game.hasModes()) {
                int modeCount = game.getModeCount().intValue();
                if (this._modesResId == 0) {
                    int minMode = session.getGame().getMinMode().intValue();
                    String[] modesNames = this._scoreFormatter.getDefinedModesNames(minMode, modeCount);
                    for (int i = 0; i < modesNames.length; i++) {
                        if (modesNames[i] == null) {
                            throw new ConfigurationException("no name configured for mode " + (minMode + i) + " - check " + FORMAT_SCORE_KEY);
                        }
                    }
                } else {
                    String[] modeStrings = context.getResources().getStringArray(this._modesResId);
                    if (modeStrings == null || modeStrings.length != modeCount) {
                        throw new ConfigurationException("your modes string array must have exactily " + modeCount + " entries!");
                    }
                }
            }
            try {
                String.format(this._moneyFormat, BigDecimal.ONE, "$");
            } catch (IllegalFormatException e) {
                throw new ConfigurationException("invalid ui.format.money value: must contain valid %f and %s specifiers in that order. " + e.getLocalizedMessage());
            }
        } else {
            throw new ConfigurationException("when you enable the achievement feature you also have to provide an SLAwards.bundle in the assets folder");
        }
    }
}
