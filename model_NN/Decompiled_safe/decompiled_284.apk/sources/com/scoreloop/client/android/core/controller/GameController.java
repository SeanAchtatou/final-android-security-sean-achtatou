package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__2_1_0;
import com.scoreloop.client.android.core.model.Entity;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import org.json.JSONObject;

public class GameController extends RequestController {
    private Game c;

    private class a extends Request {
        private final Entity b;

        public a(RequestCompletionCallback requestCompletionCallback, Entity entity) {
            super(requestCompletionCallback);
            this.b = entity;
        }

        public String a() {
            return String.format("/service/games/%s", this.b.getIdentifier());
        }

        public JSONObject b() {
            return new JSONObject();
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    @PublishedFor__2_1_0
    public GameController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    @PublishedFor__2_1_0
    public GameController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.c = null;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        if (response.f() != 200 || response.a() == null) {
            throw new Exception("Request failed");
        }
        getGame().a(((JSONObject) response.a()).getJSONObject(Game.a));
        return true;
    }

    @PublishedFor__2_1_0
    public Game getGame() {
        if (this.c == null) {
            this.c = h().getGame();
            if (this.c == null) {
                throw new IllegalStateException("no session game available");
            }
        }
        return this.c;
    }

    @PublishedFor__2_1_0
    public void loadGame() {
        a_();
        a aVar = new a(g(), this.c);
        aVar.a(600000);
        b(aVar);
    }

    @PublishedFor__2_1_0
    public void setGame(Entity entity) {
        if (entity == null) {
            this.c = null;
        } else if (!Game.a.equals(entity.a())) {
            throw new IllegalArgumentException("entity must be of type Game");
        } else if (!(entity instanceof Game)) {
            throw new IllegalArgumentException("In this release Entity must be a Game instance");
        } else {
            this.c = (Game) entity;
        }
    }
}
