package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import org.json.JSONObject;
import org.json.JSONTokener;

/* compiled from: DeveloperNotice */
final class q {
    private static boolean a = false;

    q() {
    }

    public static void a(Context context) {
        byte[] b;
        String string;
        if (!a) {
            a = true;
            if (AdManager.getUserId(context) == null) {
                try {
                    String a2 = u.a(context, null, null, 0);
                    o a3 = f.a("http://api.admob.com/v1/pubcode/android_sdk_emulator_notice" + "?" + a2, "developer_message", AdManager.getUserId(context));
                    if (a3.e() && (b = a3.b()) != null && (string = new JSONObject(new JSONTokener(new String(b))).getString("data")) != null && !string.equals("")) {
                        Log.w(AdManager.LOG, string);
                    }
                } catch (Exception e) {
                    if (Log.isLoggable(AdManager.LOG, 2)) {
                        Log.v(AdManager.LOG, "Unhandled exception retrieving developer message.", e);
                    }
                }
            }
        }
    }
}
