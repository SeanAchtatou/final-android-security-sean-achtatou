package com.admob.android.ads;

import android.util.Log;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/* compiled from: AdMobURLConnector */
final class r extends o {
    private HttpURLConnection m;
    private URL n;

    public r(String str, String str2, String str3, n nVar, int i, Map<String, String> map, String str4) {
        super(str2, str3, nVar, i, map, str4);
        try {
            this.n = new URL(str);
            this.i = this.n;
        } catch (MalformedURLException e) {
            this.n = null;
            this.c = e;
        }
        this.m = null;
        this.e = 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00e4 A[SYNTHETIC, Splitter:B:40:0x00e4] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01a2 A[SYNTHETIC, Splitter:B:78:0x01a2] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean e() {
        /*
            r10 = this;
            r8 = 0
            r7 = 1
            r6 = 0
            java.net.URL r0 = r10.n
            if (r0 != 0) goto L_0x0026
            com.admob.android.ads.n r0 = r10.h
            if (r0 == 0) goto L_0x0017
            com.admob.android.ads.n r0 = r10.h
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r2 = "url was null"
            r1.<init>(r2)
            r0.a(r10, r1)
        L_0x0017:
            r0 = r6
        L_0x0018:
            if (r0 != 0) goto L_0x0025
            com.admob.android.ads.n r1 = r10.h
            if (r1 == 0) goto L_0x0025
            com.admob.android.ads.n r1 = r10.h
            java.lang.Exception r2 = r10.c
            r1.a(r10, r2)
        L_0x0025:
            return r0
        L_0x0026:
            java.net.HttpURLConnection.setFollowRedirects(r7)
            r2 = r6
        L_0x002a:
            int r0 = r10.e
            int r1 = r10.f
            if (r0 >= r1) goto L_0x01bd
            if (r2 != 0) goto L_0x01bd
            java.lang.String r0 = "AdMobSDK"
            r1 = 2
            boolean r0 = android.util.Log.isLoggable(r0, r1)
            if (r0 == 0) goto L_0x0061
            java.lang.String r0 = "AdMobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "attempt "
            java.lang.StringBuilder r1 = r1.append(r3)
            int r3 = r10.e
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = " to connect to url "
            java.lang.StringBuilder r1 = r1.append(r3)
            java.net.URL r3 = r10.n
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r0, r1)
        L_0x0061:
            r10.h()     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.net.URL r0 = r10.n     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            r10.m = r0     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.net.HttpURLConnection r0 = r10.m     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            if (r0 == 0) goto L_0x01ba
            java.net.HttpURLConnection r0 = r10.m     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.lang.String r1 = "User-Agent"
            java.lang.String r3 = a()     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.lang.String r0 = r10.g     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            if (r0 == 0) goto L_0x008a
            java.net.HttpURLConnection r0 = r10.m     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.lang.String r1 = "X-ADMOB-ISU"
            java.lang.String r3 = r10.g     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
        L_0x008a:
            java.net.HttpURLConnection r0 = r10.m     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            int r1 = r10.b     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.net.HttpURLConnection r0 = r10.m     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            int r1 = r10.b     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.util.Map r0 = r10.d     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            if (r0 == 0) goto L_0x00f4
            java.util.Map r0 = r10.d     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.util.Set r0 = r0.keySet()     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
        L_0x00a6:
            boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            if (r0 == 0) goto L_0x00f4
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            if (r0 == 0) goto L_0x00a6
            java.util.Map r1 = r10.d     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            if (r1 == 0) goto L_0x00a6
            java.net.HttpURLConnection r4 = r10.m     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            r4.addRequestProperty(r0, r1)     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            goto L_0x00a6
        L_0x00c4:
            r0 = move-exception
            r1 = r8
        L_0x00c6:
            java.lang.String r2 = "AdMobSDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x01ae }
            r3.<init>()     // Catch:{ all -> 0x01ae }
            java.lang.String r4 = "could not open connection to url "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x01ae }
            java.net.URL r4 = r10.n     // Catch:{ all -> 0x01ae }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x01ae }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x01ae }
            android.util.Log.w(r2, r3, r0)     // Catch:{ all -> 0x01ae }
            r10.c = r0     // Catch:{ all -> 0x01ae }
            if (r1 == 0) goto L_0x00e7
            r1.close()     // Catch:{ Exception -> 0x01a9 }
        L_0x00e7:
            r10.h()
            r0 = r6
        L_0x00eb:
            int r1 = r10.e
            int r1 = r1 + 1
            r10.e = r1
            r2 = r0
            goto L_0x002a
        L_0x00f4:
            java.lang.String r0 = r10.l     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            if (r0 == 0) goto L_0x017e
            java.net.HttpURLConnection r0 = r10.m     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.lang.String r1 = "POST"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.net.HttpURLConnection r0 = r10.m     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            r1 = 1
            r0.setDoOutput(r1)     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.net.HttpURLConnection r0 = r10.m     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.lang.String r1 = "Content-Type"
            java.lang.String r3 = r10.a     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.net.HttpURLConnection r0 = r10.m     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.lang.String r1 = "Content-Length"
            java.lang.String r3 = r10.l     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            int r3 = r3.length()     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.net.HttpURLConnection r0 = r10.m     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.io.OutputStream r0 = r0.getOutputStream()     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.io.BufferedWriter r1 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.io.OutputStreamWriter r3 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            r3.<init>(r0)     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            r0 = 4096(0x1000, float:5.74E-42)
            r1.<init>(r3, r0)     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            java.lang.String r0 = r10.l     // Catch:{ Exception -> 0x01b5 }
            r1.write(r0)     // Catch:{ Exception -> 0x01b5 }
            r1.close()     // Catch:{ Exception -> 0x01b5 }
            r0 = r8
        L_0x013a:
            java.net.HttpURLConnection r1 = r10.m     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            int r1 = r1.getResponseCode()     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            r3 = 200(0xc8, float:2.8E-43)
            if (r1 < r3) goto L_0x01b8
            r3 = 300(0x12c, float:4.2E-43)
            if (r1 >= r3) goto L_0x01b8
            java.net.HttpURLConnection r1 = r10.m     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            java.net.URL r1 = r1.getURL()     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            r10.i = r1     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            boolean r1 = r10.k     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            if (r1 == 0) goto L_0x018b
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            java.net.HttpURLConnection r2 = r10.m     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            java.io.InputStream r2 = r2.getInputStream()     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            r3 = 4096(0x1000, float:5.74E-42)
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            r2 = 4096(0x1000, float:5.74E-42)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            r4 = 4096(0x1000, float:5.74E-42)
            r3.<init>(r4)     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
        L_0x016c:
            int r4 = r1.read(r2)     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            r5 = -1
            if (r4 == r5) goto L_0x0185
            r5 = 0
            r3.write(r2, r5, r4)     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            goto L_0x016c
        L_0x0178:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00c6
        L_0x017e:
            java.net.HttpURLConnection r0 = r10.m     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            r0.connect()     // Catch:{ Exception -> 0x00c4, all -> 0x019e }
            r0 = r8
            goto L_0x013a
        L_0x0185:
            byte[] r1 = r3.toByteArray()     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            r10.j = r1     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
        L_0x018b:
            com.admob.android.ads.n r1 = r10.h     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            if (r1 == 0) goto L_0x0194
            com.admob.android.ads.n r1 = r10.h     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            r1.a(r10)     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
        L_0x0194:
            r1 = r7
        L_0x0195:
            r10.h()     // Catch:{ Exception -> 0x0178, all -> 0x01b0 }
            r10.h()
            r0 = r1
            goto L_0x00eb
        L_0x019e:
            r0 = move-exception
            r1 = r8
        L_0x01a0:
            if (r1 == 0) goto L_0x01a5
            r1.close()     // Catch:{ Exception -> 0x01ac }
        L_0x01a5:
            r10.h()
            throw r0
        L_0x01a9:
            r0 = move-exception
            goto L_0x00e7
        L_0x01ac:
            r1 = move-exception
            goto L_0x01a5
        L_0x01ae:
            r0 = move-exception
            goto L_0x01a0
        L_0x01b0:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x01a0
        L_0x01b5:
            r0 = move-exception
            goto L_0x00c6
        L_0x01b8:
            r1 = r2
            goto L_0x0195
        L_0x01ba:
            r0 = r8
            r1 = r2
            goto L_0x0195
        L_0x01bd:
            r0 = r2
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.r.e():boolean");
    }

    private void h() {
        if (this.m != null) {
            this.m.disconnect();
            this.m = null;
        }
    }

    public final void f() {
        h();
        this.h = null;
    }

    public final void run() {
        try {
            e();
        } catch (Exception e) {
            if (Log.isLoggable(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "exception caught in AdMobURLConnector.run(), " + e.getMessage());
            }
        }
    }
}
