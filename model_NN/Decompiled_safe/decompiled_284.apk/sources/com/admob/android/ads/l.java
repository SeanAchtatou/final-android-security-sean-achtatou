package com.admob.android.ads;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.admob.android.ads.b;
import com.admob.android.ads.c;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: AdContainer */
final class l extends RelativeLayout implements Animation.AnimationListener, b.f, c.a, p {
    private static float i = -1.0f;
    private static d j = null;
    protected b a;
    protected ProgressBar b;
    /* access modifiers changed from: private */
    public final AdView c;
    private Vector<String> d;
    private View e;
    private long f = -1;
    private boolean g;
    private View h;

    public l(b bVar, Context context, AdView adView) {
        super(context);
        this.c = adView;
        setId(1);
        i = getResources().getDisplayMetrics().density;
        setBackgroundDrawable(context.getResources().getDrawable(17301602));
        Drawable drawable = context.getResources().getDrawable(17301602);
        drawable.setAlpha(128);
        this.e = new View(context);
        this.e.setBackgroundDrawable(drawable);
        this.e.setVisibility(4);
        addView(this.e, new RelativeLayout.LayoutParams(-1, -1));
        a((b) null);
    }

    /* access modifiers changed from: package-private */
    public final void a(b bVar) {
        this.a = bVar;
        if (bVar == null) {
            setFocusable(false);
            setClickable(false);
            return;
        }
        bVar.a((b.f) this);
        setFocusable(true);
        setClickable(true);
    }

    /* access modifiers changed from: protected */
    public final b e() {
        return this.a;
    }

    static float f() {
        return i;
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        if (this.a != null) {
            this.a.h();
            this.a = null;
        }
    }

    public final void a(View view, RelativeLayout.LayoutParams layoutParams) {
        if (view != null && view != this.h) {
            this.h = view;
            this.b = new ProgressBar(getContext());
            this.b.setIndeterminate(true);
            this.b.setId(2);
            if (layoutParams != null) {
                this.b.setLayoutParams(layoutParams);
            }
            this.b.setVisibility(4);
            post(new b(this));
        }
    }

    /* compiled from: AdContainer */
    private static class b implements Runnable {
        private WeakReference<l> a;

        public b(l lVar) {
            this.a = new WeakReference<>(lVar);
        }

        public final void run() {
            try {
                l lVar = this.a.get();
                if (lVar != null) {
                    lVar.addView(lVar.b);
                }
            } catch (Exception e) {
                if (Log.isLoggable(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "exception caught in AdContainer post run(), " + e.getMessage());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        if (isPressed() || isFocused()) {
            canvas.clipRect(3, 3, getWidth() - 3, getHeight() - 3);
        }
        super.onDraw(canvas);
        if (this.f == -1) {
            this.f = SystemClock.uptimeMillis();
            if (this.a != null) {
                this.a.i();
            }
        }
    }

    public final void d() {
        post(new c(this));
    }

    /* compiled from: AdContainer */
    private static class c implements Runnable {
        private WeakReference<l> a;

        public c(l lVar) {
            this.a = new WeakReference<>(lVar);
        }

        public final void run() {
            try {
                l lVar = this.a.get();
                if (lVar != null) {
                    lVar.h();
                }
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void h() {
        this.g = false;
        if (this.b != null) {
            this.b.setVisibility(4);
        }
        if (this.h != null) {
            this.h.setVisibility(0);
        }
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (Log.isLoggable(AdManager.LOG, 2)) {
            Log.v(AdManager.LOG, "onKeyDown: keyCode=" + i2);
        }
        if (i2 == 66 || i2 == 23) {
            this.d = a(keyEvent, this.d);
            setPressed(true);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public final boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (Log.isLoggable(AdManager.LOG, 2)) {
            Log.v(AdManager.LOG, "onKeyUp: keyCode=" + i2);
        }
        if (i() && (i2 == 66 || i2 == 23)) {
            this.d = a(keyEvent, this.d);
            j();
        }
        setPressed(false);
        return super.onKeyUp(i2, keyEvent);
    }

    public final void c() {
        Vector vector = new Vector();
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            vector.add(getChildAt(i2));
        }
        if (j == null) {
            j = new d();
        }
        Collections.sort(vector, j);
        for (int size = vector.size() - 1; size >= 0; size--) {
            if (indexOfChild((View) vector.elementAt(size)) != size) {
                bringChildToFront((View) vector.elementAt(size));
            }
        }
        this.e.bringToFront();
    }

    /* compiled from: AdContainer */
    static class d implements Comparator<View> {
        d() {
        }

        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            float a = g.a((View) obj);
            float a2 = g.a((View) obj2);
            if (a < a2) {
                return -1;
            }
            return a > a2 ? 1 : 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x007a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean dispatchTouchEvent(android.view.MotionEvent r8) {
        /*
            r7 = this;
            r6 = 2
            r5 = 0
            r4 = 1
            int r0 = r8.getAction()
            java.lang.String r1 = "AdMobSDK"
            boolean r1 = android.util.Log.isLoggable(r1, r6)
            if (r1 == 0) goto L_0x0043
            java.lang.String r1 = "AdMobSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "dispatchTouchEvent: action="
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = " x="
            java.lang.StringBuilder r2 = r2.append(r3)
            float r3 = r8.getX()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " y="
            java.lang.StringBuilder r2 = r2.append(r3)
            float r3 = r8.getY()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.v(r1, r2)
        L_0x0043:
            boolean r1 = r7.i()
            if (r1 == 0) goto L_0x0079
            com.admob.android.ads.b r1 = r7.a
            if (r1 == 0) goto L_0x0098
            com.admob.android.ads.b r1 = r7.a
            android.graphics.Rect r1 = r1.g()
            com.admob.android.ads.b r2 = r7.a
            android.graphics.Rect r1 = r2.a(r1)
            float r2 = r8.getX()
            int r2 = (int) r2
            float r3 = r8.getY()
            int r3 = (int) r3
            boolean r1 = r1.contains(r2, r3)
            if (r1 != 0) goto L_0x0098
            r1 = r5
        L_0x006a:
            if (r1 == 0) goto L_0x0074
            java.util.Vector<java.lang.String> r2 = r7.d
            java.util.Vector r2 = r7.a(r8, r4, r2)
            r7.d = r2
        L_0x0074:
            if (r0 != 0) goto L_0x007a
            r7.setPressed(r1)
        L_0x0079:
            return r4
        L_0x007a:
            if (r0 != r6) goto L_0x0080
            r7.setPressed(r1)
            goto L_0x0079
        L_0x0080:
            if (r0 != r4) goto L_0x0091
            boolean r0 = r7.isPressed()
            if (r0 == 0) goto L_0x008d
            if (r1 == 0) goto L_0x008d
            r7.j()
        L_0x008d:
            r7.setPressed(r5)
            goto L_0x0079
        L_0x0091:
            r1 = 3
            if (r0 != r1) goto L_0x0079
            r7.setPressed(r5)
            goto L_0x0079
        L_0x0098:
            r1 = r4
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.l.dispatchTouchEvent(android.view.MotionEvent):boolean");
    }

    private boolean i() {
        if (this.a == null || SystemClock.uptimeMillis() - this.f <= this.a.c()) {
            return false;
        }
        return true;
    }

    public final boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (Log.isLoggable(AdManager.LOG, 2)) {
            Log.v(AdManager.LOG, "dispatchTrackballEvent: action=" + motionEvent.getAction());
        }
        if (i()) {
            this.d = a(motionEvent, true, this.d);
            if (motionEvent.getAction() == 0) {
                setPressed(true);
            } else if (motionEvent.getAction() == 1) {
                if (hasFocus()) {
                    j();
                }
                setPressed(false);
            }
        }
        return super.onTrackballEvent(motionEvent);
    }

    private void j() {
        if (this.a != null && isPressed()) {
            setPressed(false);
            if (!this.g) {
                this.g = true;
                JSONObject k = k();
                if (this.h != null) {
                    AnimationSet animationSet = new AnimationSet(true);
                    float width = ((float) this.h.getWidth()) / 2.0f;
                    float height = ((float) this.h.getHeight()) / 2.0f;
                    ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, width, height);
                    scaleAnimation.setDuration(200);
                    animationSet.addAnimation(scaleAnimation);
                    ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.2f, 0.001f, 1.2f, 0.001f, width, height);
                    scaleAnimation2.setDuration(299);
                    scaleAnimation2.setStartOffset(200);
                    scaleAnimation2.setAnimationListener(this);
                    animationSet.addAnimation(scaleAnimation2);
                    postDelayed(new a(k, this), 500);
                    this.h.startAnimation(animationSet);
                    return;
                }
                this.a.a(k);
                if (this.c != null) {
                    this.c.performClick();
                }
            }
        }
    }

    /* compiled from: AdContainer */
    protected static class a extends Thread {
        private JSONObject a;
        private WeakReference<l> b;

        public a(JSONObject jSONObject, l lVar) {
            this.a = jSONObject;
            this.b = new WeakReference<>(lVar);
        }

        public final void run() {
            try {
                l lVar = this.b.get();
                if (lVar != null && lVar.a != null) {
                    lVar.a.a(this.a);
                    if (lVar.c != null) {
                        lVar.c.performClick();
                    }
                }
            } catch (Exception e) {
                if (Log.isLoggable(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "exception caught in AdClickThread.run(), " + e.getMessage());
                }
            }
        }
    }

    private JSONObject k() {
        JSONObject jSONObject;
        Exception e2;
        try {
            JSONObject jSONObject2 = new JSONObject();
            a(this, jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            try {
                jSONObject3.put("interactions", jSONObject2);
                return jSONObject3;
            } catch (Exception e3) {
                e2 = e3;
                jSONObject = jSONObject3;
                Log.w(AdManager.LOG, "Exception while processing interaction history.", e2);
                return jSONObject;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            jSONObject = null;
            e2 = exc;
            Log.w(AdManager.LOG, "Exception while processing interaction history.", e2);
            return jSONObject;
        }
    }

    private static void a(View view, JSONObject jSONObject) {
        if (view instanceof p) {
            p pVar = (p) view;
            JSONObject a2 = pVar.a();
            String b2 = pVar.b();
            if (!(a2 == null || b2 == null)) {
                try {
                    jSONObject.put(b2, a2);
                } catch (Exception e2) {
                }
            }
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                a(viewGroup.getChildAt(i2), jSONObject);
            }
        }
    }

    private static Vector<String> a(int i2, int i3, int i4, long j2, Vector<String> vector) {
        Vector<String> vector2;
        String format;
        if (vector == null) {
            vector2 = new Vector<>();
        } else {
            vector2 = vector;
        }
        float f2 = ((float) j2) / 1000.0f;
        if (i3 == -1 || i4 == -1) {
            format = String.format("{%d,%f}", Integer.valueOf(i2), Float.valueOf(f2));
        } else {
            format = String.format("{%d,%d,%d,%f}", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Float.valueOf(f2));
        }
        vector2.add(format);
        if (Log.isLoggable(AdManager.LOG, 2)) {
            Log.v(AdManager.LOG, "recordEvent:" + format);
        }
        return vector2;
    }

    private Vector<String> a(MotionEvent motionEvent, boolean z, Vector<String> vector) {
        int action = motionEvent.getAction();
        long eventTime = motionEvent.getEventTime() - this.f;
        if (action != 0 && action != 1) {
            return vector;
        }
        return a(action == 1 ? 1 : 0, (int) motionEvent.getX(), (int) motionEvent.getY(), eventTime, vector);
    }

    private Vector<String> a(KeyEvent keyEvent, Vector<String> vector) {
        int action = keyEvent.getAction();
        long eventTime = keyEvent.getEventTime() - this.f;
        if (action != 0 && action != 1) {
            return vector;
        }
        return a(action == 1 ? 1 : 0, -1, -1, eventTime, vector);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
    }

    public final String b() {
        return "container";
    }

    public final JSONObject a() {
        JSONObject jSONObject = null;
        if (this.d != null) {
            jSONObject = new JSONObject();
            try {
                jSONObject.put("touches", new JSONArray((Collection) this.d));
            } catch (Exception e2) {
            }
        }
        return jSONObject;
    }

    public final void setPressed(boolean z) {
        if ((!this.g || !z) && isPressed() != z) {
            if (z) {
                if (this.e != null) {
                    this.e.bringToFront();
                }
                this.e.setVisibility(0);
            } else {
                this.e.setVisibility(4);
            }
            super.setPressed(z);
            invalidate();
        }
    }
}
