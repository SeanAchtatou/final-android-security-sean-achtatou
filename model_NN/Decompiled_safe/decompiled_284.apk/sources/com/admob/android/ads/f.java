package com.admob.android.ads;

import java.util.Map;
import org.json.JSONObject;

/* compiled from: AdMobConnectorFactory */
public final class f {
    private static boolean a = false;

    public static o a(String str, String str2, String str3, n nVar, int i, Map<String, String> map, String str4) {
        return new r(str, str2, str3, nVar, i, null, str4);
    }

    public static o a(String str, String str2, String str3, JSONObject jSONObject) {
        o a2 = a(str, str2, str3, null, 5000, null, jSONObject == null ? null : jSONObject.toString());
        a2.a("application/json");
        return a2;
    }

    public static o a(String str, String str2, String str3, n nVar) {
        return a(str, str2, str3, nVar, 5000, null, null);
    }

    public static o a(String str, String str2, String str3, n nVar, int i) {
        o a2 = a(str, null, str3, nVar, 5000, null, null);
        if (a2 != null) {
            a2.a(1);
        }
        return a2;
    }

    public static o a(String str, String str2, String str3) {
        return a(str, str2, str3, (n) null);
    }
}
