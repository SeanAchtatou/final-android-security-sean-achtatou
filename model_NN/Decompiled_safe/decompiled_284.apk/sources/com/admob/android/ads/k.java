package com.admob.android.ads;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* compiled from: SkewAnimation */
public final class k extends Animation {
    private float[] a;
    private float[] b;

    public k(float[] fArr, float[] fArr2, PointF pointF) {
        this.a = fArr;
        this.b = fArr2;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        if (((double) f) < 0.0d || ((double) f) > 1.0d) {
            transformation.setTransformationType(Transformation.TYPE_IDENTITY);
            return;
        }
        Matrix matrix = transformation.getMatrix();
        float[] fArr = new float[this.a.length];
        for (int i = 0; i < this.a.length; i++) {
            fArr[i] = this.a[i] + ((this.b[i] - this.a[i]) * f);
        }
        matrix.setSkew(this.a[0], this.a[1]);
        transformation.setTransformationType(Transformation.TYPE_MATRIX);
    }
}
