package com.google.gson;

import java.lang.reflect.Type;

class bb {
    protected final Type a;
    protected final Class<?> b;

    bb(Type type) {
        this.a = type;
        this.b = bg.b(type);
    }

    public final Type a() {
        return this.a;
    }

    public final Class<?> b() {
        return this.b;
    }

    public final boolean c() {
        return bg.a((Type) this.b);
    }

    public final boolean d() {
        return au.a(au.b(this.b));
    }
}
