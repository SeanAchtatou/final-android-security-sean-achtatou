package net.leieuncretino.tturn;

public final class R {

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class color {
        public static final int help_text_default = 2131034115;
        public static final int help_title_default = 2131034114;
        public static final int text_default = 2131034112;
        public static final int title_default = 2131034113;
    }

    public static final class drawable {
        public static final int eightball = 2130837504;
        public static final int flip = 2130837505;
        public static final int help = 2130837506;
        public static final int help_icon = 2130837507;
        public static final int hockey = 2130837508;
        public static final int icon = 2130837509;
        public static final int ittext = 2130837510;
        public static final int phoings = 2130837511;
        public static final int tturn = 2130837512;
    }

    public static final class id {
        public static final int BANNER = 2130968576;
        public static final int IAB_BANNER = 2130968578;
        public static final int IAB_LEADERBOARD = 2130968579;
        public static final int IAB_MRECT = 2130968577;
        public static final int TextView01 = 2130968581;
        public static final int TextView02 = 2130968595;
        public static final int ad1 = 2130968597;
        public static final int content = 2130968580;
        public static final int help_ads_eightball = 2130968591;
        public static final int help_ads_flip = 2130968587;
        public static final int help_ads_hockey = 2130968583;
        public static final int help_ads_ittext = 2130968593;
        public static final int help_ads_phoings = 2130968585;
        public static final int help_ads_tturn = 2130968589;
        public static final int ihelp_ads_eightball = 2130968592;
        public static final int ihelp_ads_flip = 2130968588;
        public static final int ihelp_ads_hockey = 2130968584;
        public static final int ihelp_ads_ittext = 2130968594;
        public static final int ihelp_ads_phoings = 2130968586;
        public static final int ihelp_ads_tturn = 2130968590;
        public static final int nine_render = 2130968596;
        public static final int scrllvwNo1 = 2130968582;
        public static final int tris_render = 2130968598;
    }

    public static final class layout {
        public static final int help = 2130903040;
        public static final int help_ads = 2130903041;
        public static final int help_blind = 2130903042;
        public static final int help_nine = 2130903043;
        public static final int help_tic = 2130903044;
        public static final int help_trick = 2130903045;
        public static final int main = 2130903046;
        public static final int nine = 2130903047;
        public static final int tris = 2130903048;
    }

    public static final class string {
        public static final int app_name = 2131099657;
        public static final int black = 2131099659;
        public static final int blackstart = 2131099682;
        public static final int draw = 2131099661;
        public static final int hello = 2131099656;
        public static final int help = 2131099648;
        public static final int help_ads = 2131099671;
        public static final int help_ads_eightball = 2131099654;
        public static final int help_ads_flip = 2131099652;
        public static final int help_ads_hockey = 2131099650;
        public static final int help_ads_ittext = 2131099655;
        public static final int help_ads_phoings = 2131099651;
        public static final int help_ads_title = 2131099649;
        public static final int help_ads_tturn = 2131099653;
        public static final int help_blind = 2131099678;
        public static final int help_blind_help = 2131099680;
        public static final int help_blind_title = 2131099679;
        public static final int help_nine = 2131099670;
        public static final int help_nine_help = 2131099677;
        public static final int help_nine_title = 2131099676;
        public static final int help_tic = 2131099668;
        public static final int help_tic_help = 2131099673;
        public static final int help_tic_title = 2131099672;
        public static final int help_trick = 2131099669;
        public static final int help_trick_help = 2131099675;
        public static final int help_trick_title = 2131099674;
        public static final int in = 2131099663;
        public static final int moves = 2131099664;
        public static final int phone = 2131099667;
        public static final int start = 2131099666;
        public static final int tap = 2131099665;
        public static final int the = 2131099658;
        public static final int white = 2131099660;
        public static final int whitestart = 2131099681;
        public static final int win = 2131099662;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }
}
