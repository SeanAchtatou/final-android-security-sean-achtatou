package net.leieuncretino.tturn.ai;

import android.util.Log;
import net.leieuncretino.tturn.Const;

public class Ai implements Const {
    public int nodeCount;
    public Node rootNode;

    public Ai() {
    }

    public Ai(Board b) {
        this.rootNode = new Node(new Board(b, b.mode, b.misere));
    }

    public int Move() {
        int max = -10;
        Node bestNode = new Node(this.rootNode.board.mode, this.rootNode.board.misere);
        for (int x = 0; x <= 8; x++) {
            if (this.rootNode.board.canMove(x)) {
                Node n = new Node(this.rootNode);
                if (n.board.SetMove(x)) {
                    this.rootNode.AddChildren(n);
                    n.moveBox = x;
                    int val = minimaxAB(n, true, 4, -10, 10);
                    if (val >= max) {
                        max = val;
                        bestNode = n;
                    }
                    Log.d("NODE :", n.toString());
                }
            }
        }
        return bestNode.moveBox;
    }

    private int minimaxAB(Node node, boolean min, int depth, int alpha, int beta) {
        int bp = boardPoint(node);
        if (bp != -1 || depth == 0) {
            node.point = bp;
            return bp;
        } else if (min) {
            for (int x = 0; x <= 8; x++) {
                if (node.board.canMove(x)) {
                    Node n = new Node(node);
                    if (n.board.SetMove(x)) {
                        node.AddChildren(n);
                        n.moveBox = x;
                        int val = minimaxAB(n, false, depth - 1, alpha, beta);
                        if (val < beta) {
                            beta = val;
                            n.parent.point = val;
                        }
                        if (beta == -2) {
                            return beta;
                        }
                    } else {
                        continue;
                    }
                }
            }
            return beta;
        } else if (min) {
            return -100;
        } else {
            for (int x2 = 0; x2 <= 8; x2++) {
                if (node.board.canMove(x2)) {
                    Node n2 = new Node(node);
                    if (n2.board.SetMove(x2)) {
                        node.AddChildren(n2);
                        n2.moveBox = x2;
                        int val2 = minimaxAB(n2, true, depth - 1, alpha, beta);
                        if (val2 > alpha) {
                            alpha = val2;
                            n2.parent.point = val2;
                        }
                        if (alpha == 2) {
                            return alpha;
                        }
                    } else {
                        continue;
                    }
                }
            }
            return alpha;
        }
    }

    private int boardPoint(Node n) {
        if (n.board.checkCondition() == 1) {
            return -2;
        }
        if (n.board.checkCondition() == 2) {
            return 2;
        }
        if (n.board.checkCondition() == 255) {
            return 0;
        }
        return -1;
    }
}
