package net.leieuncretino.tturn.ai;

import net.leieuncretino.tturn.Const;

public class Board implements Const {
    private int condition = -1;
    private int[] intBoard = new int[9];
    private int[] intBoardTurns = new int[9];
    private int intMoveSymbol = 2;
    public boolean misere = false;
    public int mode = 1;
    public int moveCount = 0;

    public Board(int[][] intBoard2, int[][] intBoardTurns2, int moveCount2, int intMoveSymbol2, int mode2, boolean misere2) {
        this.moveCount = moveCount2;
        this.intMoveSymbol = intMoveSymbol2;
        this.mode = mode2;
        this.misere = misere2;
        for (int x = 0; x <= 8; x++) {
            this.intBoard[x] = intBoard2[x % 3][x / 3];
            this.intBoardTurns[x] = intBoardTurns2[x % 3][x / 3];
        }
    }

    public Board(int mode2, boolean misere2) {
        this.mode = mode2;
        this.misere = misere2;
        for (int x = 0; x <= 8; x++) {
            this.intBoard[x] = 0;
            this.intBoardTurns[x] = 0;
        }
    }

    public Board(int[] intBoard2, int mode2, boolean misere2) {
        this.mode = mode2;
        this.misere = misere2;
        for (int x = 0; x <= 8; x++) {
            this.intBoard[x] = intBoard2[x];
            this.intBoardTurns[x] = this.intBoardTurns[x];
        }
    }

    public Board(Board board, int mode2, boolean misere2) {
        this.mode = mode2;
        this.misere = misere2;
        this.moveCount = board.moveCount;
        this.intMoveSymbol = board.intMoveSymbol;
        System.arraycopy(board.intBoard, 0, this.intBoard, 0, 9);
        System.arraycopy(board.intBoardTurns, 0, this.intBoardTurns, 0, 9);
    }

    public void copy(Board board, int mode2, boolean misere2) {
        this.mode = mode2;
        this.misere = misere2;
        this.moveCount = board.moveCount;
        this.intMoveSymbol = board.intMoveSymbol;
        System.arraycopy(board.intBoard, 0, this.intBoard, 0, 9);
        System.arraycopy(board.intBoardTurns, 0, this.intBoardTurns, 0, 9);
    }

    public boolean SetMove(int boxNo) {
        int i;
        if (this.intBoard[boxNo] != 0) {
            return false;
        }
        this.intBoard[boxNo] = this.intMoveSymbol;
        if (this.mode == 2) {
            if (this.misere) {
                this.intBoardTurns[boxNo] = 7;
                for (int x = 0; x <= 8; x++) {
                    if (this.intBoardTurns[x] > 0) {
                        int[] iArr = this.intBoardTurns;
                        iArr[x] = iArr[x] - 1;
                        if (this.intBoardTurns[x] <= 0) {
                            int[] iArr2 = this.intBoard;
                            if (this.intBoard[x] == 2) {
                                i = 1;
                            } else {
                                i = 2;
                            }
                            iArr2[x] = i;
                        }
                    } else if (this.intBoardTurns[x] < 0) {
                        this.intBoardTurns[x] = 0;
                        this.intBoard[x] = 0;
                    }
                }
            } else {
                this.intBoardTurns[boxNo] = 7;
                for (int x2 = 0; x2 <= 8; x2++) {
                    int[] iArr3 = this.intBoardTurns;
                    iArr3[x2] = iArr3[x2] - 1;
                    if (this.intBoardTurns[x2] <= 0) {
                        this.intBoard[x2] = 0;
                    }
                }
            }
        }
        if (this.intMoveSymbol == 1) {
            this.intMoveSymbol = 2;
        } else {
            this.intMoveSymbol = 1;
        }
        this.moveCount++;
        return true;
    }

    public int checkCondition() {
        if (this.condition < 0) {
            this.condition = internalCheckCondition();
        }
        if (this.misere && this.condition != 255) {
            if (this.condition == 2) {
                this.condition = 1;
            } else {
                this.condition = 2;
            }
        }
        return this.condition;
    }

    public int internalCheckCondition() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        if (((this.intBoard[0] == this.intBoard[1]) & (this.intBoard[1] == this.intBoard[2])) && (this.intBoard[2] != 0)) {
            return this.intBoard[0];
        }
        if (this.intBoard[3] == this.intBoard[4]) {
            z = true;
        } else {
            z = false;
        }
        if ((z & (this.intBoard[4] == this.intBoard[5])) && (this.intBoard[5] != 0)) {
            return this.intBoard[3];
        }
        if (this.intBoard[6] == this.intBoard[7]) {
            z2 = true;
        } else {
            z2 = false;
        }
        if ((z2 & (this.intBoard[7] == this.intBoard[8])) && (this.intBoard[8] != 0)) {
            return this.intBoard[6];
        }
        if (this.intBoard[0] == this.intBoard[3]) {
            z3 = true;
        } else {
            z3 = false;
        }
        if ((z3 & (this.intBoard[3] == this.intBoard[6])) && (this.intBoard[6] != 0)) {
            return this.intBoard[0];
        }
        if (this.intBoard[1] == this.intBoard[4]) {
            z4 = true;
        } else {
            z4 = false;
        }
        if ((z4 & (this.intBoard[4] == this.intBoard[7])) && (this.intBoard[7] != 0)) {
            return this.intBoard[1];
        }
        if (this.intBoard[2] == this.intBoard[5]) {
            z5 = true;
        } else {
            z5 = false;
        }
        if ((z5 & (this.intBoard[5] == this.intBoard[8])) && (this.intBoard[8] != 0)) {
            return this.intBoard[2];
        }
        if (this.intBoard[0] == this.intBoard[4]) {
            z6 = true;
        } else {
            z6 = false;
        }
        if ((z6 & (this.intBoard[4] == this.intBoard[8])) && (this.intBoard[8] != 0)) {
            return this.intBoard[0];
        }
        if (this.intBoard[2] == this.intBoard[4]) {
            z7 = true;
        } else {
            z7 = false;
        }
        if ((z7 & (this.intBoard[4] == this.intBoard[6])) && (this.intBoard[6] != 0)) {
            return this.intBoard[2];
        }
        for (int x = 0; x < 9; x++) {
            if (this.intBoard[x] == 0) {
                return 0;
            }
        }
        return 255;
    }

    public String toString() {
        String rv;
        String rv2 = "";
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                String rv3 = String.valueOf(rv2) + "|";
                if (this.intBoard[(x * 3) + y] == 0) {
                    rv = String.valueOf(rv3) + " " + ((x * 3) + y) + " ";
                } else if (this.intBoard[(x * 3) + y] == 1) {
                    rv = String.valueOf(rv3) + "[W]";
                } else {
                    rv = String.valueOf(rv3) + "[B]";
                }
                rv2 = String.valueOf(rv) + "|";
            }
            rv2 = String.valueOf(rv2) + newline;
        }
        return rv2;
    }

    public int weight() {
        return 0;
    }

    public boolean canMove(int pos) {
        return this.intBoard[pos] == 0;
    }
}
