package net.leieuncretino.tturn.ai;

import java.util.ArrayList;
import net.leieuncretino.tturn.Const;

public class Node implements Const {
    public Board board;
    public ArrayList<Node> childrenNode = new ArrayList<>();
    public int moveBox;
    public Node parent;
    public int point;

    public Node(int mode, boolean misere) {
        this.board = new Board(mode, misere);
    }

    public Node(Board board2) {
        this.board = board2;
    }

    public Node(Node toCopy) {
        this.point = toCopy.point;
        this.board = new Board(toCopy.board, toCopy.board.mode, toCopy.board.misere);
    }

    public void AddChildren(Node node) {
        this.childrenNode.add(node);
        node.Parent(this);
    }

    private void Parent(Node node) {
        this.parent = node;
    }

    public void copy(Node n) {
        this.point = n.point;
        this.board.copy(n.board, n.board.mode, n.board.misere);
    }

    public String toString() {
        return "Point " + this.point + newline + "moveBox " + this.moveBox + newline + this.board.toString() + newline;
    }
}
