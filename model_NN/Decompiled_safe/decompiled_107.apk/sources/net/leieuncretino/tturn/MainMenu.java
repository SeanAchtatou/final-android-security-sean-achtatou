package net.leieuncretino.tturn;

import android.content.Intent;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.ui.activity.BaseGameActivity;

public class MainMenu extends BaseGameActivity implements Scene.IOnSceneTouchListener, Const {
    private static final int CAMERA_HEIGHT = 854;
    private static final int CAMERA_WIDTH = 480;
    public static final int REQUEST_NINE = 2;
    public static final int REQUEST_TRIS = 1;
    public static final boolean debug = false;
    public static Sprite singleSprite;
    public static Sprite twoSprite;
    private Texture mBackgroundTexture;
    private TextureRegion mBackgroundTextureRegion;
    private Camera mCamera;
    private TextureRegion mSingleTextureRegion;
    private Texture mTexture;
    private TextureRegion mTwoTextureRegion;
    public boolean misere = false;
    public int startedGame = 0;

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public Engine onLoadEngine() {
        this.mCamera = new Camera(0.0f, 0.0f, 480.0f, 854.0f);
        return new Engine(new EngineOptions(true, EngineOptions.ScreenOrientation.PORTRAIT, new RatioResolutionPolicy(480.0f, 854.0f), this.mCamera).setNeedsSound(true));
    }

    public void onLoadResources() {
        this.mTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TextureRegionFactory.setAssetBasePath("gfx/");
        this.mSingleTextureRegion = TextureRegionFactory.createFromAsset(this.mTexture, this, "btn_single.png", 0, 0);
        this.mTwoTextureRegion = TextureRegionFactory.createFromAsset(this.mTexture, this, "btn_two.png", 0, 256);
        this.mBackgroundTexture = new Texture(512, 1024, TextureOptions.DEFAULT);
        this.mBackgroundTextureRegion = TextureRegionFactory.createFromAsset(this.mBackgroundTexture, this, "main_menu_a.jpg", 0, 0);
        this.mEngine.getTextureManager().loadTextures(this.mBackgroundTexture, this.mTexture);
    }

    public Scene onLoadScene() {
        this.mEngine.registerUpdateHandler(new FPSLogger());
        Scene scene = new Scene(2);
        scene.setBackgroundEnabled(false);
        scene.getLayer(0).addEntity(new Sprite(0.0f, 0.0f, this.mBackgroundTextureRegion));
        singleSprite = new Sprite(40.0f, 200.0f, 400.0f, 200.0f, this.mSingleTextureRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                setVisible(false);
                MainMenu.twoSprite.setVisible(false);
                MainMenu.this.singleOff();
                if (MainMenu.this.startedGame == 1) {
                    MainMenu.this.realTicTacToe(1);
                } else {
                    MainMenu.this.realTrickTacToe(1);
                }
                return true;
            }
        };
        twoSprite = new Sprite(40.0f, 500.0f, 400.0f, 200.0f, this.mTwoTextureRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                setVisible(false);
                MainMenu.singleSprite.setVisible(false);
                MainMenu.this.singleOff();
                if (MainMenu.this.startedGame == 1) {
                    MainMenu.this.realTicTacToe(2);
                } else {
                    MainMenu.this.realTrickTacToe(2);
                }
                return true;
            }
        };
        scene.setOnSceneTouchListener(this);
        return scene;
    }

    public void onLoadComplete() {
    }

    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        boolean z;
        if (pSceneTouchEvent.getAction() == 1) {
            if (pSceneTouchEvent.getY() > 320.0f && pSceneTouchEvent.getY() < 430.0f) {
                if (pSceneTouchEvent.getX() > 240.0f) {
                    z = true;
                } else {
                    z = false;
                }
                ticTacToe(z);
            } else if (pSceneTouchEvent.getY() > 430.0f && pSceneTouchEvent.getY() < 540.0f) {
                blind();
            } else if (pSceneTouchEvent.getY() > 540.0f && pSceneTouchEvent.getY() < 650.0f) {
                trickTacToe(pSceneTouchEvent.getX() > 240.0f);
            } else if (pSceneTouchEvent.getY() > 650.0f && pSceneTouchEvent.getY() < 760.0f) {
                nine();
            } else if (pSceneTouchEvent.getY() > 760.0f) {
                help();
            }
        }
        return true;
    }

    public void ticTacToe(boolean lose) {
        this.startedGame = 1;
        this.misere = lose;
        singleOn();
    }

    public void trickTacToe(boolean lose) {
        this.startedGame = 2;
        this.misere = lose;
        singleOn();
    }

    public void realTicTacToe(int pMode) {
        Intent i = new Intent(this, Tris.class);
        i.putExtra("game", 1);
        i.putExtra("plMode", pMode);
        i.putExtra("misere", this.misere);
        startActivityForResult(i, 1);
    }

    public void realTrickTacToe(int pMode) {
        Intent i = new Intent(this, Tris.class);
        i.putExtra("game", 2);
        i.putExtra("plMode", pMode);
        i.putExtra("misere", this.misere);
        startActivityForResult(i, 1);
    }

    public void nine() {
        startActivityForResult(new Intent(this, Nine.class), 1);
    }

    public void blind() {
        startActivityForResult(new Intent(this, Blind.class), 1);
    }

    public void help() {
        startActivity(new Intent(this, Help.class));
    }

    public void singleOn() {
        runOnUpdateThread(new Runnable() {
            public void run() {
                MainMenu.singleSprite.setVisible(true);
                MainMenu.this.mEngine.getScene().getLayer(1).addEntity(MainMenu.singleSprite);
                MainMenu.this.mEngine.getScene().registerTouchArea(MainMenu.singleSprite);
                MainMenu.twoSprite.setVisible(true);
                MainMenu.this.mEngine.getScene().getLayer(1).addEntity(MainMenu.twoSprite);
                MainMenu.this.mEngine.getScene().registerTouchArea(MainMenu.twoSprite);
            }
        });
    }

    public void singleOff() {
        runOnUpdateThread(new Runnable() {
            public void run() {
                MainMenu.singleSprite.setVisible(false);
                MainMenu.this.mEngine.getScene().getLayer(1).removeEntity(MainMenu.singleSprite);
                MainMenu.this.mEngine.getScene().unregisterTouchArea(MainMenu.singleSprite);
                MainMenu.twoSprite.setVisible(false);
                MainMenu.this.mEngine.getScene().getLayer(1).removeEntity(MainMenu.twoSprite);
                MainMenu.this.mEngine.getScene().unregisterTouchArea(MainMenu.twoSprite);
            }
        });
    }
}
