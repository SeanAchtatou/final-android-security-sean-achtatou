package net.leieuncretino.tturn;

public interface Const {
    public static final int BLACK = 2;
    public static final int DRAW = 255;
    public static final int EMPTY = 0;
    public static final int GAME_NINE = 3;
    public static final int GAME_TIC = 1;
    public static final int GAME_TRICK = 2;
    public static final int PLMODE_SINGLE = 1;
    public static final int PLMODE_TWO = 2;
    public static final int STATE_CHOOSE_COLOR = 0;
    public static final int STATE_PLAY = 1;
    public static final int STATE_RESULT = 2;
    public static final int STATE_SET_BLACK = 21;
    public static final int STATE_SET_WHITE = 11;
    public static final int STATE_WAITSET_BLACK = 20;
    public static final int STATE_WAITSET_WHITE = 10;
    public static final int WHITE = 1;
    public static final String newline = System.getProperty("line.separator");
}
