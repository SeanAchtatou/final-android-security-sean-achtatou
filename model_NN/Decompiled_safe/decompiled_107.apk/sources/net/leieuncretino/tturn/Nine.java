package net.leieuncretino.tturn;

import android.graphics.Typeface;
import android.os.Bundle;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Random;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.ui.activity.LayoutGameActivity;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.HorizontalAlign;

public class Nine extends LayoutGameActivity implements Scene.IOnSceneTouchListener, IUpdateHandler, AdListener {
    public static final int BLACK = 2;
    public static final int BL_EN_X = 460;
    public static final int BL_ST_X = 260;
    public static final int BOARDS_SIZE = 130;
    public static final int BOARDS_X = 0;
    public static final int BOARDS_Y = 0;
    public static final int BOARD_SIZE = 111;
    public static final int BOARD_SMALL = 43;
    public static final int BOARD_X = 5;
    public static final int BOARD_Y = 405;
    private static final int CAMERA_HEIGHT = 853;
    private static final int CAMERA_WIDTH = 480;
    public static final int COL_EN_Y = 340;
    public static final int COL_ST_Y = 140;
    public static final int DRAW = 255;
    public static final int EMPTY = 0;
    public static final int LEVELS = 6;
    public static final int LEV_BACK = 0;
    public static final int LEV_BOARDS = 1;
    public static final int LEV_BOARDS_PIECES = 2;
    public static final int LEV_MSG = 5;
    public static final int LEV_SPRITE = 3;
    public static final int LEV_TEXT = 4;
    public static final int STATE_CHOOSE_COLOR = 0;
    public static final int STATE_PLAY = 1;
    public static final int STATE_RESULT = 2;
    public static final int WHITE = 1;
    public static final int WH_EN_X = 220;
    public static final int WH_ST_X = 20;
    public static final boolean debug = false;
    AdView ad;
    public Board board = null;
    public Board[][] boards = ((Board[][]) Array.newInstance(Board.class, 3, 3));
    public Sprite colorMarker = null;
    public int currentPlayer = 0;
    public Sound[] draw = new Sound[2];
    public Sound error = null;
    public long lastTap = 0;
    private Font mAliceFont;
    private Texture mAliceFontTexture;
    private Texture mBackgroundTexture;
    private TextureRegion mBackgroundTextureRegion;
    /* access modifiers changed from: private */
    public TextureRegion mBlackTextureRegion;
    /* access modifiers changed from: private */
    public TextureRegion mBoardOffTextureRegion;
    /* access modifiers changed from: private */
    public TextureRegion mBoardTextureRegion;
    private Camera mCamera;
    public Sprite mChooseBlack;
    public Sprite mChooseWhite;
    private Font mFont;
    private Texture mFontTexture;
    private Texture mTexture;
    /* access modifiers changed from: private */
    public TextureRegion mWhiteTextureRegion;
    public int moves = 0;
    public String msg = "";
    public boolean selectBoard = false;
    public Sound[] sndPiece = new Sound[3];
    public Sound start = null;
    public int state;
    private Text tapMsg = null;
    public String title = "";
    int[][][] tris;
    public boolean waitMsg = false;
    public Sound win = null;

    public Nine() {
        int[] iArr = new int[2];
        iArr[1] = 1;
        int[] iArr2 = new int[2];
        iArr2[1] = 2;
        int[][] iArr3 = {new int[2], iArr, iArr2};
        int[] iArr4 = new int[2];
        iArr4[0] = 1;
        int[][] iArr5 = {iArr4, new int[]{1, 1}, new int[]{1, 2}};
        int[] iArr6 = new int[2];
        iArr6[0] = 2;
        int[][] iArr7 = {iArr6, new int[]{2, 1}, new int[]{2, 2}};
        int[] iArr8 = new int[2];
        iArr8[0] = 1;
        int[] iArr9 = new int[2];
        iArr9[0] = 2;
        int[][] iArr10 = {new int[2], iArr8, iArr9};
        int[] iArr11 = new int[2];
        iArr11[1] = 1;
        int[][] iArr12 = {iArr11, new int[]{1, 1}, new int[]{2, 1}};
        int[] iArr13 = new int[2];
        iArr13[1] = 2;
        int[] iArr14 = new int[2];
        iArr14[1] = 2;
        int[] iArr15 = new int[2];
        iArr15[0] = 2;
        this.tris = new int[][][]{iArr3, iArr5, iArr7, iArr10, iArr12, new int[][]{iArr13, new int[]{1, 2}, new int[]{2, 2}}, new int[][]{new int[2], new int[]{1, 1}, new int[]{2, 2}}, new int[][]{iArr14, new int[]{1, 1}, iArr15}};
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle pSavedInstanceState) {
        super.onCreate(pSavedInstanceState);
        this.ad = (AdView) findViewById(R.id.ad1);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.ad != null) {
            this.ad.setEnabled(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.ad != null) {
            this.ad.setEnabled(true);
            this.ad.loadAd(new AdRequest());
        }
    }

    /* access modifiers changed from: protected */
    public int getLayoutID() {
        return R.layout.nine;
    }

    /* access modifiers changed from: protected */
    public int getRenderSurfaceViewID() {
        return R.id.nine_render;
    }

    private class Board {
        public int[][] board;
        public Sprite checkBoard;
        public int col;
        public Sprite[][] pieces;
        public int row;
        public boolean selected;
        public Board selectedBoard;

        private Board() {
            this.selected = false;
            this.board = (int[][]) Array.newInstance(Integer.TYPE, 3, 3);
            this.pieces = new Sprite[][]{new Sprite[3], new Sprite[3], new Sprite[3]};
        }

        /* synthetic */ Board(Nine nine, Board board2) {
            this();
        }

        public int freeSpaces() {
            int count = 0;
            for (int c = 0; c < 3; c++) {
                for (int r = 0; r < 3; r++) {
                    if (this.board[c][r] == 0) {
                        count++;
                    }
                }
            }
            return count;
        }

        public void putSmallPiece(Scene scene, int c, int r, int color) {
            this.board[c][r] = color;
            if (this.pieces[c][r] != null) {
                scene.getLayer(2).removeEntity(this.pieces[c][r]);
            }
            if (color == 1) {
                this.pieces[c][r] = new Sprite((float) ((this.col * Nine.BOARDS_SIZE) + 0 + (c * 43)), (float) ((this.row * Nine.BOARDS_SIZE) + 0 + (r * 43)), 43.0f, 43.0f, Nine.this.mWhiteTextureRegion);
                scene.getLayer(2).addEntity(this.pieces[c][r]);
            } else if (color == 2) {
                this.pieces[c][r] = new Sprite((float) ((this.col * Nine.BOARDS_SIZE) + 0 + (c * 43)), (float) ((this.row * Nine.BOARDS_SIZE) + 0 + (r * 43)), 43.0f, 43.0f, Nine.this.mBlackTextureRegion);
                scene.getLayer(2).addEntity(this.pieces[c][r]);
            }
        }

        public void clear(Scene scene) {
            for (int c = 0; c < 3; c++) {
                for (int r = 0; r < 3; r++) {
                    this.board[c][r] = 0;
                    if (this.pieces[c][r] != null) {
                        scene.getLayer(2).removeEntity(this.pieces[c][r]);
                        this.pieces[c][r] = null;
                    }
                }
            }
        }

        public void setSelected(boolean selected2, Scene scene) {
            if (this.selected != selected2 || this.checkBoard == null) {
                if (this.checkBoard != null) {
                    scene.getLayer(1).removeEntity(this.checkBoard);
                }
                if (selected2) {
                    this.checkBoard = new Sprite((float) ((this.col * Nine.BOARDS_SIZE) + 0), (float) ((this.row * Nine.BOARDS_SIZE) + 0), 130.0f, 130.0f, Nine.this.mBoardTextureRegion);
                } else {
                    this.checkBoard = new Sprite((float) ((this.col * Nine.BOARDS_SIZE) + 0), (float) ((this.row * Nine.BOARDS_SIZE) + 0), 130.0f, 130.0f, Nine.this.mBoardOffTextureRegion);
                }
                scene.getLayer(1).addEntity(this.checkBoard);
                this.selected = selected2;
            }
        }
    }

    public void displaySelected() {
        if (this.board == null) {
            this.board = new Board(this, null);
        }
        for (int c = 0; c < 3; c++) {
            for (int r = 0; r < 3; r++) {
                this.board.board[c][r] = 0;
                if (this.board.pieces[c][r] != null) {
                    this.mEngine.getScene().getLayer(3).removeEntity(this.board.pieces[c][r]);
                    this.board.pieces[c][r] = null;
                }
                if (this.boards[c][r].selected) {
                    this.board.selectedBoard = this.boards[c][r];
                }
            }
        }
        for (int c2 = 0; c2 < 3; c2++) {
            for (int r2 = 0; r2 < 3; r2++) {
                this.board.board[c2][r2] = this.board.selectedBoard.board[c2][r2];
                if (this.board.board[c2][r2] == 1) {
                    this.board.pieces[c2][r2] = new Sprite((float) ((c2 * BOARD_SIZE) + 5 + 5), (float) ((r2 * BOARD_SIZE) + BOARD_Y + 5), 101.0f, 101.0f, this.mWhiteTextureRegion);
                } else if (this.board.board[c2][r2] == 2) {
                    this.board.pieces[c2][r2] = new Sprite((float) ((c2 * BOARD_SIZE) + 5 + 5), (float) ((r2 * BOARD_SIZE) + BOARD_Y + 5), 101.0f, 101.0f, this.mBlackTextureRegion);
                }
                if (this.board.board[c2][r2] != 0) {
                    this.mEngine.getScene().getLayer(3).addEntity(this.board.pieces[c2][r2]);
                }
            }
        }
    }

    public Engine onLoadEngine() {
        this.mCamera = new Camera(0.0f, 0.0f, 480.0f, 853.0f);
        return new Engine(new EngineOptions(true, EngineOptions.ScreenOrientation.PORTRAIT, new RatioResolutionPolicy(480.0f, 853.0f), this.mCamera).setNeedsSound(true));
    }

    public void onLoadResources() {
        this.mTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TextureRegionFactory.setAssetBasePath("gfx/");
        this.mBlackTextureRegion = TextureRegionFactory.createFromAsset(this.mTexture, this, "black.png", 0, 0);
        this.mWhiteTextureRegion = TextureRegionFactory.createFromAsset(this.mTexture, this, "white.png", 256, 0);
        this.mBoardTextureRegion = TextureRegionFactory.createFromAsset(this.mTexture, this, "sprite_board.jpg", 0, 256);
        this.mBoardOffTextureRegion = TextureRegionFactory.createFromAsset(this.mTexture, this, "sprite_board_off.jpg", 256, 256);
        this.mBackgroundTexture = new Texture(512, 1024, TextureOptions.DEFAULT);
        this.mBackgroundTextureRegion = TextureRegionFactory.createFromAsset(this.mBackgroundTexture, this, "board9.jpg", 0, 0);
        this.mFontTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFont = FontFactory.createStroke(this.mFontTexture, Typeface.create(Typeface.DEFAULT, 1), 48.0f, true, -16777216, 1, -1);
        this.mEngine.getTextureManager().loadTexture(this.mFontTexture);
        FontFactory.setAssetBasePath("fonts/");
        this.mAliceFontTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mAliceFont = FontFactory.createStrokeFromAsset(this.mAliceFontTexture, this, "Alice_in_Wonderland_3.ttf", 70.0f, true, -1, 1, -16777216);
        SoundFactory.setAssetBasePath("snd/");
        try {
            this.sndPiece[0] = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "pezzo1.ogg");
            this.sndPiece[1] = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "pezzo2.ogg");
            this.sndPiece[2] = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "pezzo3.ogg");
            this.draw[0] = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "draw.ogg");
            this.draw[1] = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "draw2.ogg");
            this.error = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "error1.ogg");
            this.win = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "win.ogg");
            this.start = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "roll.ogg");
        } catch (IOException e) {
            Debug.e("Error", e);
        }
        this.mEngine.getTextureManager().loadTextures(this.mFontTexture, this.mAliceFontTexture, this.mBackgroundTexture, this.mTexture);
        this.mEngine.getFontManager().loadFonts(this.mAliceFont, this.mFont);
    }

    public Scene onLoadScene() {
        this.mEngine.registerUpdateHandler(new FPSLogger());
        Scene scene = new Scene(6);
        scene.setBackgroundEnabled(false);
        scene.getLayer(0).addEntity(new Sprite(0.0f, 0.0f, this.mBackgroundTextureRegion));
        for (int c = 0; c < 3; c++) {
            for (int r = 0; r < 3; r++) {
                this.boards[c][r] = new Board(this, null);
                this.boards[c][r].col = c;
                this.boards[c][r].row = r;
                this.boards[c][r].selected = false;
                if (c == 1 && r == 1) {
                    this.boards[c][r].selected = true;
                }
                this.boards[c][r].clear(scene);
                this.boards[c][r].setSelected(this.boards[c][r].selected, scene);
            }
        }
        this.board = new Board(this, null);
        displaySelected();
        scene.registerUpdateHandler(this);
        scene.setOnSceneTouchListener(this);
        return scene;
    }

    public void onLoadComplete() {
        newGame();
    }

    public void doMsg(String msg2) {
        this.waitMsg = true;
        this.tapMsg = new Text(60.0f, 400.0f, this.mAliceFont, msg2, HorizontalAlign.CENTER);
        this.mEngine.getScene().getLayer(5).addEntity(this.tapMsg);
    }

    public void undoMsg() {
        this.waitMsg = false;
        if (this.tapMsg != null) {
            this.mEngine.getScene().getLayer(5).removeEntity(this.tapMsg);
        }
    }

    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        if (pSceneTouchEvent.getAction() != 0 || !avoidFastTap()) {
            return false;
        }
        if (this.waitMsg) {
            if (this.state == 0) {
                if (pSceneTouchEvent.getY() <= 140.0f || pSceneTouchEvent.getY() >= 340.0f) {
                    return false;
                }
                if (pSceneTouchEvent.getX() > 20.0f && pSceneTouchEvent.getX() < 220.0f) {
                    avoidFast();
                    startGame(1);
                    sndStart();
                    undoMsg();
                    undoChooseColor();
                    return false;
                } else if (pSceneTouchEvent.getX() <= 260.0f || pSceneTouchEvent.getX() >= 460.0f) {
                    return false;
                } else {
                    avoidFast();
                    startGame(2);
                    sndStart();
                    undoMsg();
                    undoChooseColor();
                    return false;
                }
            } else if (this.state != 2) {
                return false;
            } else {
                undoMsg();
                newGame();
                return false;
            }
        } else if (this.state != 1) {
            return false;
        } else {
            float x = pSceneTouchEvent.getX();
            float y = pSceneTouchEvent.getY();
            if (y >= 405.0f) {
                checkClick(((int) (x - 5.0f)) / BOARD_SIZE, ((int) (y - 405.0f)) / BOARD_SIZE);
                return false;
            } else if (!this.selectBoard) {
                return false;
            } else {
                checkClickBoard(((int) (x - 0.0f)) / BOARDS_SIZE, ((int) (y - 0.0f)) / BOARDS_SIZE);
                return false;
            }
        }
    }

    public boolean avoidFastTap() {
        return System.currentTimeMillis() - this.lastTap > 500;
    }

    public void avoidFast() {
        this.lastTap = System.currentTimeMillis();
    }

    public void doChooseColor() {
        this.mChooseBlack = new Sprite(260.0f, 140.0f, 200.0f, 200.0f, this.mBlackTextureRegion);
        this.mChooseWhite = new Sprite(20.0f, 140.0f, 200.0f, 200.0f, this.mWhiteTextureRegion);
        this.mEngine.getScene().getLayer(3).addEntity(this.mChooseWhite);
        this.mEngine.getScene().getLayer(3).addEntity(this.mChooseBlack);
    }

    public void undoChooseColor() {
        this.mEngine.getScene().getLayer(3).removeEntity(this.mChooseWhite);
        this.mEngine.getScene().getLayer(3).removeEntity(this.mChooseBlack);
        this.mChooseBlack = null;
        this.mChooseWhite = null;
    }

    public void newGame() {
        this.currentPlayer = 0;
        this.moves = 0;
        this.msg = getString(R.string.start);
        doMsg(this.msg);
        this.state = 0;
        avoidFast();
        runOnUpdateThread(new Runnable() {
            public void run() {
                for (int c = 0; c < 3; c++) {
                    for (int r = 0; r < 3; r++) {
                        Nine.this.boards[c][r].clear(Nine.this.mEngine.getScene());
                    }
                }
                for (int c2 = 0; c2 < 3; c2++) {
                    for (int r2 = 0; r2 < 3; r2++) {
                        if (Nine.this.board.pieces[c2][r2] != null) {
                            Nine.this.mEngine.getScene().getLayer(3).removeEntity(Nine.this.board.pieces[c2][r2]);
                        }
                        Nine.this.board.board[c2][r2] = 0;
                        if (Nine.this.boards[c2][r2].selected) {
                            Nine.this.board.selectedBoard = Nine.this.boards[c2][r2];
                        }
                    }
                }
                Nine.this.doChooseColor();
            }
        });
    }

    public void startGame(int color) {
        this.state = 1;
        this.currentPlayer = color;
        doRunMarker(color);
        this.selectBoard = true;
    }

    public void swapPlayer() {
        this.currentPlayer = this.currentPlayer == 1 ? 2 : 1;
        doMarker(this.currentPlayer);
    }

    public void checkClick(int col, int row) {
        if (col >= 0 && col < 3 && row >= 0 && row < 3) {
            avoidFast();
            if (this.board.board[col][row] != 0) {
                sndError();
                return;
            }
            this.moves++;
            this.board.board[col][row] = this.currentPlayer;
            place(this.board, col, row, this.currentPlayer);
        }
    }

    public void checkClickBoard(final int col, final int row) {
        if (col >= 0 && col < 3 && row >= 0 && row < 3) {
            avoidFast();
            if (this.boards[col][row].selected) {
                sndError();
            } else {
                runOnUpdateThread(new Runnable() {
                    public void run() {
                        Nine.this.swapBoardTo(col, row);
                    }
                });
            }
        }
    }

    public void swapBoardTo(int col, int row) {
        if (this.board.selectedBoard != null) {
            this.board.selectedBoard.setSelected(false, this.mEngine.getScene());
        }
        this.boards[col][row].setSelected(true, this.mEngine.getScene());
        displaySelected();
        sndBoard();
    }

    public void place(Board board2, int col, int row, int color) {
        final Board board3 = board2;
        final int i = col;
        final int i2 = row;
        final int i3 = color;
        runOnUpdateThread(new Runnable() {
            public void run() {
                if (board3.pieces[i][i2] != null) {
                    Nine.this.mEngine.getScene().getLayer(3).removeEntity(board3.pieces[i][i2]);
                }
                if (i3 == 2) {
                    board3.pieces[i][i2] = new Sprite((float) ((i * Nine.BOARD_SIZE) + 5 + 5), (float) ((i2 * Nine.BOARD_SIZE) + Nine.BOARD_Y + 5), 101.0f, 101.0f, Nine.this.mBlackTextureRegion);
                } else {
                    board3.pieces[i][i2] = new Sprite((float) ((i * Nine.BOARD_SIZE) + 5 + 5), (float) ((i2 * Nine.BOARD_SIZE) + Nine.BOARD_Y + 5), 101.0f, 101.0f, Nine.this.mWhiteTextureRegion);
                }
                Nine.this.mEngine.getScene().getLayer(3).addEntity(board3.pieces[i][i2]);
                board3.selectedBoard.putSmallPiece(Nine.this.mEngine.getScene(), i, i2, i3);
                Nine.this.sndPiece();
                Nine.this.swapPlayer();
                Nine.this.checkEnd();
                if (Nine.this.state != 2) {
                    Nine.this.checkDraw();
                }
                if (Nine.this.state != 2) {
                    Nine.this.checkSwitchBoard(board3, i, i2, i3);
                }
            }
        });
    }

    public void checkDraw() {
        int free = 0;
        for (int c = 0; c < 3; c++) {
            for (int r = 0; r < 3; r++) {
                free += this.boards[c][r].freeSpaces();
            }
        }
        if (free == 0) {
            sndDraw();
            avoidFast();
            this.state = 2;
            doMsg(String.valueOf(String.valueOf(getString(R.string.draw)) + "\n") + getString(R.string.tap));
        }
    }

    public void checkSwitchBoard(Board board2, int col, int row, int color) {
        if (this.boards[col][row].freeSpaces() > 0) {
            swapBoardTo(col, row);
            this.selectBoard = false;
            return;
        }
        swapBoardTo(col, row);
        this.selectBoard = true;
    }

    public void doRunMarker(final int color) {
        runOnUpdateThread(new Runnable() {
            public void run() {
                Nine.this.doMarker(color);
            }
        });
    }

    public void doMarker(int color) {
        if (this.colorMarker != null) {
            this.mEngine.getScene().getLayer(3).removeEntity(this.colorMarker);
        }
        if (color == 2) {
            this.colorMarker = new Sprite(360.0f, 410.0f, 100.0f, 100.0f, this.mBlackTextureRegion);
        } else {
            this.colorMarker = new Sprite(360.0f, 410.0f, 100.0f, 100.0f, this.mWhiteTextureRegion);
        }
        this.mEngine.getScene().getLayer(3).addEntity(this.colorMarker);
    }

    public int freeSpaces() {
        int count = 0;
        for (int c = 0; c < 3; c++) {
            for (int r = 0; r < 3; r++) {
                if (this.board.board[c][r] == 0) {
                    count++;
                }
            }
        }
        return count;
    }

    public void checkEnd() {
        int color;
        int winner = 0;
        for (int i = 0; i < this.tris.length; i++) {
            if (winner == 0 && (color = checkById(i)) != 0) {
                winner = color;
            }
        }
        if (winner == 2) {
            avoidFast();
            this.state = 2;
            doMsg(String.valueOf(String.valueOf(String.valueOf(String.valueOf(getString(R.string.the)) + " " + getString(R.string.black) + " ") + getString(R.string.win) + "\n" + getString(R.string.in) + " ") + Integer.toString(this.moves) + " " + getString(R.string.moves) + "\n") + getString(R.string.tap));
            sndWin();
        } else if (winner == 1) {
            avoidFast();
            this.state = 2;
            doMsg(String.valueOf(String.valueOf(String.valueOf(String.valueOf(getString(R.string.the)) + " " + getString(R.string.white) + " ") + getString(R.string.win) + "\n" + getString(R.string.in) + " ") + Integer.toString(this.moves) + " " + getString(R.string.moves) + "\n") + getString(R.string.tap));
            sndWin();
        }
    }

    public int checkTris(int row1, int col1, int row2, int col2, int row3, int col3) {
        int pos1 = this.board.board[row1][col1];
        int pos2 = this.board.board[row2][col2];
        int pos3 = this.board.board[row3][col3];
        if (pos1 == pos2 && pos1 == pos3 && pos1 != 0) {
            return pos1;
        }
        return 0;
    }

    public int checkById(int id) {
        return checkTris(this.tris[id][0][0], this.tris[id][0][1], this.tris[id][1][0], this.tris[id][1][1], this.tris[id][2][0], this.tris[id][2][1]);
    }

    public void onUpdate(float pSecondsElapsed) {
    }

    public void reset() {
    }

    public void sndWin() {
        this.win.play();
    }

    public void sndError() {
        this.error.play();
    }

    public void sndStart() {
        this.start.play();
    }

    public void sndDraw() {
        this.draw[new Random().nextInt(2)].play();
    }

    public void sndPiece() {
        this.sndPiece[new Random().nextInt(3)].play();
    }

    public void sndBoard() {
        sndPiece();
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad arg0) {
    }
}
