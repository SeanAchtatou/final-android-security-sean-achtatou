package net.leieuncretino.tturn;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TextView;

public class Help extends TabActivity {
    private int ADD_NEW_TAB = 1;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.help);
        TabHost tabs = (TabHost) findViewById(16908306);
        tabs.setup();
        Resources res = getResources();
        tabs.addTab(tabs.newTabSpec("help_tic").setIndicator(getString(R.string.help_tic), res.getDrawable(R.drawable.help)).setContent(new Intent().setClass(this, HelpTic.class)));
        tabs.addTab(tabs.newTabSpec("help_tic").setIndicator(getString(R.string.help_blind), res.getDrawable(R.drawable.help)).setContent(new Intent().setClass(this, HelpBlind.class)));
        tabs.addTab(tabs.newTabSpec("help_tic").setIndicator(getString(R.string.help_trick), res.getDrawable(R.drawable.help)).setContent(new Intent().setClass(this, HelpTrick.class)));
        tabs.addTab(tabs.newTabSpec("help_tic").setIndicator(getString(R.string.help_nine), res.getDrawable(R.drawable.help)).setContent(new Intent().setClass(this, HelpNine.class)));
        tabs.addTab(tabs.newTabSpec("help_tic").setIndicator(getString(R.string.help_ads_title), res.getDrawable(R.drawable.help)).setContent(new Intent().setClass(this, HelpAds.class)));
    }

    public static class HelpTic extends Activity {
        public void onCreate(Bundle icicle) {
            super.onCreate(icicle);
            setContentView((int) R.layout.help_tic);
        }
    }

    public static class HelpBlind extends Activity {
        public void onCreate(Bundle icicle) {
            super.onCreate(icicle);
            setContentView((int) R.layout.help_blind);
        }
    }

    public static class HelpTrick extends Activity {
        public void onCreate(Bundle icicle) {
            super.onCreate(icicle);
            setContentView((int) R.layout.help_trick);
        }
    }

    public static class HelpNine extends Activity {
        public void onCreate(Bundle icicle) {
            super.onCreate(icicle);
            setContentView((int) R.layout.help_nine);
        }
    }

    public static class HelpAds extends Activity {
        public void onCreate(Bundle icicle) {
            super.onCreate(icicle);
            setContentView((int) R.layout.help_ads);
            ((TextView) findViewById(R.id.help_ads_tturn)).setVisibility(8);
            ((ImageButton) findViewById(R.id.ihelp_ads_tturn)).setVisibility(8);
        }

        public void doTturn(View v) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=net.leieuncretino.tturn")));
        }

        public void doPhoings(View v) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=net.leieuncretino.phoings")));
        }

        public void doFlip(View v) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=net.leieuncretino.beta.flip")));
        }

        public void doEightball(View v) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=net.leieuncretino.eightball")));
        }

        public void doHockey(View v) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=net.leieuncretino.beta.hockey")));
        }

        public void doIttext(View v) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=net.leieuncretino.beta.ittext")));
        }
    }
}
