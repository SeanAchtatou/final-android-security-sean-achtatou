package net.leieuncretino.tturn;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Random;
import net.leieuncretino.tturn.ai.Ai;
import net.leieuncretino.tturn.ai.Board;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.ui.activity.LayoutGameActivity;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.HorizontalAlign;
import org.anddev.andengine.util.constants.TimeConstants;

public class Blind extends LayoutGameActivity implements Scene.IOnSceneTouchListener, IUpdateHandler, AdListener, Const {
    public static final int BL_EN_X = 460;
    public static final int BL_ST_X = 280;
    public static final int BOARD_SIZE = 140;
    public static final int BOARD_X = 30;
    public static final int BOARD_Y = 320;
    private static final int CAMERA_HEIGHT = 853;
    private static final int CAMERA_WIDTH = 480;
    public static final int CELL_SPAN = 140;
    public static final int CELL_ST_X = 80;
    public static final int CELL_ST_Y = 370;
    public static final int COL_EN_Y = 300;
    public static final int COL_ST_Y = 120;
    public static final int LEVELS = 4;
    public static final int LEV_BACK = 0;
    public static final int LEV_MSG = 3;
    public static final int LEV_SPRITE = 1;
    public static final int LEV_TEXT = 2;
    public static final int WH_EN_X = 200;
    public static final int WH_ST_X = 20;
    public static final int WON_B_X = 360;
    public static final int WON_W_X = 60;
    public static final int WON_Y = 120;
    public static final boolean debug = false;
    AdView ad;
    public Ai ai = null;
    public int[][] blackMoves;
    public int[][] board = ((int[][]) Array.newInstance(Integer.TYPE, 3, 3));
    public int[][] boardTurns = ((int[][]) Array.newInstance(Integer.TYPE, 3, 3));
    public Sprite colorMarker = null;
    public int currentPlayer = 0;
    public Sound[] draw = new Sound[2];
    public Sound error = null;
    public long lastTap = 0;
    private Font mAliceFont;
    private Texture mAliceFontTexture;
    private Texture mBackgroundTexture;
    private TextureRegion mBackgroundTextureRegion;
    /* access modifiers changed from: private */
    public TextureRegion mBlackTextureRegion;
    private Camera mCamera;
    private Font mFont;
    private Texture mFontTexture;
    private Texture mTexture;
    private TextureRegion mTitleTextureRegion;
    /* access modifiers changed from: private */
    public TextureRegion mWhiteTextureRegion;
    public int mode = 1;
    public int moves = 0;
    public String msg = "";
    public Sprite[][] pieces = {new Sprite[3], new Sprite[3], new Sprite[3]};
    public int plMode;
    int placedMoves = 0;
    public Sound[] sndPiece = new Sound[3];
    public Sound start = null;
    public int state;
    private Text tapMsg = null;
    public String title = "";
    int[][][] tris;
    private ChangeableText txtMoves = null;
    private ChangeableText[][] txtNumbers = ((ChangeableText[][]) Array.newInstance(ChangeableText.class, 3, 3));
    private ChangeableText txtWonBlack = null;
    private ChangeableText txtWonWhite = null;
    public boolean waitMsg = false;
    public int[][] whiteMoves;
    public Sound win = null;
    public int wonBlack = 0;
    public int wonWhite = 0;

    public Blind() {
        int[] iArr = new int[2];
        iArr[1] = 1;
        int[] iArr2 = new int[2];
        iArr2[1] = 2;
        int[][] iArr3 = {new int[2], iArr, iArr2};
        int[] iArr4 = new int[2];
        iArr4[0] = 1;
        int[][] iArr5 = {iArr4, new int[]{1, 1}, new int[]{1, 2}};
        int[] iArr6 = new int[2];
        iArr6[0] = 2;
        int[][] iArr7 = {iArr6, new int[]{2, 1}, new int[]{2, 2}};
        int[] iArr8 = new int[2];
        iArr8[0] = 1;
        int[] iArr9 = new int[2];
        iArr9[0] = 2;
        int[][] iArr10 = {new int[2], iArr8, iArr9};
        int[] iArr11 = new int[2];
        iArr11[1] = 1;
        int[][] iArr12 = {iArr11, new int[]{1, 1}, new int[]{2, 1}};
        int[] iArr13 = new int[2];
        iArr13[1] = 2;
        int[] iArr14 = new int[2];
        iArr14[1] = 2;
        int[] iArr15 = new int[2];
        iArr15[0] = 2;
        this.tris = new int[][][]{iArr3, iArr5, iArr7, iArr10, iArr12, new int[][]{iArr13, new int[]{1, 2}, new int[]{2, 2}}, new int[][]{new int[2], new int[]{1, 1}, new int[]{2, 2}}, new int[][]{iArr14, new int[]{1, 1}, iArr15}};
        this.whiteMoves = (int[][]) Array.newInstance(Integer.TYPE, 3, 3);
        this.blackMoves = (int[][]) Array.newInstance(Integer.TYPE, 3, 3);
    }

    public boolean isSinglePlayer() {
        return this.plMode == 1;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle pSavedInstanceState) {
        super.onCreate(pSavedInstanceState);
        Intent i = getIntent();
        this.mode = i.getIntExtra("game", 1);
        this.plMode = i.getIntExtra("plMode", 2);
        this.ad = (AdView) findViewById(R.id.ad1);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.ad != null) {
            this.ad.setEnabled(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.ad != null) {
            this.ad.setEnabled(true);
            this.ad.loadAd(new AdRequest());
        }
    }

    /* access modifiers changed from: protected */
    public int getLayoutID() {
        return R.layout.tris;
    }

    /* access modifiers changed from: protected */
    public int getRenderSurfaceViewID() {
        return R.id.tris_render;
    }

    public boolean isTic() {
        return this.mode == 1;
    }

    public Engine onLoadEngine() {
        this.mCamera = new Camera(0.0f, 0.0f, 480.0f, 853.0f);
        return new Engine(new EngineOptions(true, EngineOptions.ScreenOrientation.PORTRAIT, new RatioResolutionPolicy(480.0f, 853.0f), this.mCamera).setNeedsSound(true));
    }

    public void onLoadResources() {
        this.mTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TextureRegionFactory.setAssetBasePath("gfx/");
        this.mBlackTextureRegion = TextureRegionFactory.createFromAsset(this.mTexture, this, "black.png", 0, 0);
        this.mWhiteTextureRegion = TextureRegionFactory.createFromAsset(this.mTexture, this, "white.png", 256, 0);
        this.mTitleTextureRegion = TextureRegionFactory.createFromAsset(this.mTexture, this, "blind.png", 0, 256);
        this.mBackgroundTexture = new Texture(512, 1024, TextureOptions.DEFAULT);
        this.mBackgroundTextureRegion = TextureRegionFactory.createFromAsset(this.mBackgroundTexture, this, "board1.jpg", 0, 0);
        this.mFontTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFont = FontFactory.createStroke(this.mFontTexture, Typeface.create(Typeface.DEFAULT, 1), 48.0f, true, -16777216, 1, -1);
        this.mEngine.getTextureManager().loadTexture(this.mFontTexture);
        FontFactory.setAssetBasePath("fonts/");
        this.mAliceFontTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mAliceFont = FontFactory.createStrokeFromAsset(this.mAliceFontTexture, this, "Alice_in_Wonderland_3.ttf", 70.0f, true, -16777216, 1, -1);
        SoundFactory.setAssetBasePath("snd/");
        try {
            this.sndPiece[0] = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "pezzo1.ogg");
            this.sndPiece[1] = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "pezzo2.ogg");
            this.sndPiece[2] = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "pezzo3.ogg");
            this.draw[0] = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "draw.ogg");
            this.draw[1] = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "draw2.ogg");
            this.error = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "error1.ogg");
            this.win = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "win.ogg");
            this.start = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "roll.ogg");
        } catch (IOException e) {
            Debug.e("Error", e);
        }
        this.mEngine.getTextureManager().loadTextures(this.mFontTexture, this.mAliceFontTexture, this.mBackgroundTexture, this.mTexture);
        this.mEngine.getFontManager().loadFonts(this.mAliceFont, this.mFont);
    }

    public Scene onLoadScene() {
        this.mEngine.registerUpdateHandler(new FPSLogger());
        Scene scene = new Scene(4);
        scene.setBackgroundEnabled(false);
        scene.getLayer(0).addEntity(new Sprite(0.0f, 0.0f, this.mBackgroundTextureRegion));
        this.txtWonBlack = new ChangeableText(360.0f, 120.0f, this.mAliceFont, "0", "000".length());
        this.txtWonWhite = new ChangeableText(60.0f, 120.0f, this.mAliceFont, "0", "000".length());
        for (int c = 0; c < 3; c++) {
            for (int r = 0; r < 3; r++) {
                this.txtNumbers[c][r] = new ChangeableText((float) ((c * 140) + 80), (float) ((r * 140) + CELL_ST_Y), this.mFont, " ", "0".length());
                scene.getLayer(2).addEntity(this.txtNumbers[c][r]);
            }
        }
        scene.getLayer(2).addEntity(this.txtWonWhite);
        scene.getLayer(2).addEntity(this.txtWonBlack);
        scene.getLayer(2).addEntity(new Sprite(0.0f, 20.0f, this.mTitleTextureRegion));
        scene.registerUpdateHandler(this);
        scene.setOnSceneTouchListener(this);
        return scene;
    }

    public void onLoadComplete() {
        newGame();
    }

    public void doMsg(String msg2) {
        this.waitMsg = true;
        this.tapMsg = new Text(60.0f, 400.0f, this.mAliceFont, msg2, HorizontalAlign.CENTER);
        this.mEngine.getScene().getLayer(3).addEntity(this.tapMsg);
    }

    public void undoMsg() {
        this.waitMsg = false;
        if (this.tapMsg != null) {
            this.mEngine.getScene().getLayer(3).removeEntity(this.tapMsg);
        }
    }

    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        if (pSceneTouchEvent.getAction() != 0 || !avoidFastTap()) {
            return false;
        }
        if (this.state == 0) {
            if (pSceneTouchEvent.getY() <= 120.0f || pSceneTouchEvent.getY() >= 300.0f) {
                return false;
            }
            if (pSceneTouchEvent.getX() > 20.0f && pSceneTouchEvent.getX() < 200.0f) {
                avoidFast();
                startGame(1);
                sndStart();
                undoMsg();
                return false;
            } else if (pSceneTouchEvent.getX() <= 280.0f || pSceneTouchEvent.getX() >= 460.0f) {
                return false;
            } else {
                avoidFast();
                startGame(2);
                sndStart();
                undoMsg();
                return false;
            }
        } else if (this.state == 2) {
            undoMsg();
            newGame();
            return false;
        } else if (this.state == 10) {
            undoMsg();
            this.placedMoves = 1;
            this.state = 11;
            avoidFast();
            return false;
        } else if (this.state == 11) {
            if (checkPlaceMove(this.whiteMoves, ((int) (pSceneTouchEvent.getX() - 30.0f)) / 140, ((int) (pSceneTouchEvent.getY() - 320.0f)) / 140, this.placedMoves)) {
                this.placedMoves++;
            }
            if (this.placedMoves == 10) {
                clearText();
                this.state = 20;
                this.msg = getString(R.string.blackstart);
                if (isSinglePlayer()) {
                    this.msg = String.valueOf(getString(R.string.phone)) + this.msg;
                }
                doMsg(this.msg);
            }
            avoidFast();
            return false;
        } else if (this.state == 20) {
            undoMsg();
            this.placedMoves = 1;
            this.state = 21;
            avoidFast();
            return false;
        } else if (this.state == 21) {
            if (checkPlaceMove(this.blackMoves, ((int) (pSceneTouchEvent.getX() - 30.0f)) / 140, ((int) (pSceneTouchEvent.getY() - 320.0f)) / 140, this.placedMoves)) {
                this.placedMoves++;
            }
            if (this.placedMoves == 10) {
                clearText();
                runGame();
            }
            avoidFast();
            return false;
        } else if (this.state != 1) {
            return false;
        } else {
            if (this.currentPlayer == 1) {
                placeBest(this.whiteMoves);
                return false;
            }
            placeBest(this.blackMoves);
            return false;
        }
    }

    public boolean avoidFastTap() {
        return System.currentTimeMillis() - this.lastTap > 200;
    }

    public void avoidFast() {
        this.lastTap = System.currentTimeMillis();
    }

    public void newGame() {
        this.msg = getString(R.string.whitestart);
        if (isSinglePlayer()) {
            this.msg = String.valueOf(getString(R.string.phone)) + this.msg;
        }
        doMsg(this.msg);
        this.state = 10;
        avoidFast();
        clearMoves();
        runOnUpdateThread(new Runnable() {
            public void run() {
                for (int c = 0; c < 3; c++) {
                    for (int r = 0; r < 3; r++) {
                        Blind.this.board[c][r] = 0;
                        Blind.this.boardTurns[c][r] = 0;
                        if (Blind.this.pieces[c][r] != null) {
                            Blind.this.mEngine.getScene().getLayer(1).removeEntity(Blind.this.pieces[c][r]);
                        }
                        Blind.this.pieces[c][r] = null;
                    }
                }
            }
        });
    }

    public void runGame() {
        this.currentPlayer = 0;
        this.moves = 0;
        this.msg = getString(R.string.start);
        if (isSinglePlayer()) {
            this.msg = String.valueOf(getString(R.string.phone)) + this.msg;
        }
        doMsg(this.msg);
        this.state = 0;
        avoidFast();
        runOnUpdateThread(new Runnable() {
            public void run() {
                for (int c = 0; c < 3; c++) {
                    for (int r = 0; r < 3; r++) {
                        Blind.this.board[c][r] = 0;
                        Blind.this.boardTurns[c][r] = 0;
                        if (Blind.this.pieces[c][r] != null) {
                            Blind.this.mEngine.getScene().getLayer(1).removeEntity(Blind.this.pieces[c][r]);
                        }
                        Blind.this.pieces[c][r] = null;
                    }
                }
            }
        });
    }

    public void startGame(int color) {
        this.state = 1;
        doMarker(color);
        this.currentPlayer = color;
        if (isSinglePlayer() && this.currentPlayer == 2) {
            playBlackFirst();
        }
    }

    public void swapPlayer() {
        this.currentPlayer = this.currentPlayer == 1 ? 2 : 1;
        doMarker(this.currentPlayer);
    }

    public void checkClick(int col, int row) {
        if (col >= 0 && col < 3 && row >= 0 && row < 3) {
            avoidFast();
            if (this.board[col][row] != 0) {
                sndError();
            } else if (isTic()) {
                this.moves++;
                this.board[col][row] = this.currentPlayer;
                place(col, row, this.currentPlayer);
                swapPlayer();
                checkEnd();
            } else {
                this.moves++;
                this.board[col][row] = this.currentPlayer;
                this.boardTurns[col][row] = 7;
                place(col, row, this.currentPlayer);
                decreaseAndClean();
                swapPlayer();
                checkEnd();
            }
        }
    }

    public void place(final int col, final int row, final int color) {
        runOnUpdateThread(new Runnable() {
            public void run() {
                if (Blind.this.pieces[col][row] != null) {
                    Blind.this.mEngine.getScene().getLayer(1).removeEntity(Blind.this.pieces[col][row]);
                }
                if (color == 2) {
                    Blind.this.pieces[col][row] = new Sprite((float) ((col * 140) + 30 + 5), (float) ((row * 140) + 320 + 5), 130.0f, 130.0f, Blind.this.mBlackTextureRegion);
                } else {
                    Blind.this.pieces[col][row] = new Sprite((float) ((col * 140) + 30 + 5), (float) ((row * 140) + 320 + 5), 130.0f, 130.0f, Blind.this.mWhiteTextureRegion);
                }
                Blind.this.mEngine.getScene().getLayer(1).addEntity(Blind.this.pieces[col][row]);
                Blind.this.sndPiece();
            }
        });
    }

    public void decreaseAndClean() {
        for (int c = 0; c < 3; c++) {
            for (int r = 0; r < 3; r++) {
                if (this.boardTurns[c][r] > 0) {
                    int[] iArr = this.boardTurns[c];
                    iArr[r] = iArr[r] - 1;
                    if (this.boardTurns[c][r] == 0) {
                        this.board[c][r] = 0;
                    }
                }
            }
        }
        runOnUpdateThread(new Runnable() {
            public void run() {
                for (int c = 0; c < 3; c++) {
                    for (int r = 0; r < 3; r++) {
                        if (Blind.this.boardTurns[c][r] == 0) {
                            if (Blind.this.pieces[c][r] != null) {
                                Blind.this.mEngine.getScene().getLayer(1).removeEntity(Blind.this.pieces[c][r]);
                            }
                            Blind.this.pieces[c][r] = null;
                        }
                    }
                }
            }
        });
    }

    public void doMarker(final int color) {
        runOnUpdateThread(new Runnable() {
            public void run() {
                if (Blind.this.colorMarker != null) {
                    Blind.this.mEngine.getScene().getLayer(1).removeEntity(Blind.this.colorMarker);
                }
                if (color == 2) {
                    Blind.this.colorMarker = new Sprite(210.0f, 260.0f, 60.0f, 60.0f, Blind.this.mBlackTextureRegion);
                } else {
                    Blind.this.colorMarker = new Sprite(210.0f, 260.0f, 60.0f, 60.0f, Blind.this.mWhiteTextureRegion);
                }
                Blind.this.mEngine.getScene().getLayer(1).addEntity(Blind.this.colorMarker);
            }
        });
    }

    public int freeSpaces() {
        int count = 0;
        for (int c = 0; c < 3; c++) {
            for (int r = 0; r < 3; r++) {
                if (this.board[c][r] == 0) {
                    count++;
                }
            }
        }
        return count;
    }

    public void checkEnd() {
        int color;
        int winner = 0;
        for (int i = 0; i < this.tris.length; i++) {
            if (winner == 0 && (color = checkById(i)) != 0) {
                winner = color;
            }
        }
        if (winner == 2) {
            this.wonBlack++;
            avoidFast();
            this.state = 2;
            updatePoints();
            doMsg(String.valueOf(String.valueOf(String.valueOf(String.valueOf(getString(R.string.the)) + " " + getString(R.string.black) + " ") + getString(R.string.win) + "\n" + getString(R.string.in) + " ") + Integer.toString(this.moves) + " " + getString(R.string.moves) + "\n") + getString(R.string.tap));
            sndWin();
        } else if (winner == 1) {
            this.wonWhite++;
            avoidFast();
            this.state = 2;
            updatePoints();
            doMsg(String.valueOf(String.valueOf(String.valueOf(String.valueOf(getString(R.string.the)) + " " + getString(R.string.white) + " ") + getString(R.string.win) + "\n" + getString(R.string.in) + " ") + Integer.toString(this.moves) + " " + getString(R.string.moves) + "\n") + getString(R.string.tap));
            sndWin();
        } else if (freeSpaces() == 0) {
            sndDraw();
            avoidFast();
            this.state = 2;
            updatePoints();
            doMsg(String.valueOf(String.valueOf(getString(R.string.draw)) + "\n") + getString(R.string.tap));
        } else if (isSinglePlayer() && this.currentPlayer == 2) {
            playBlack();
        }
    }

    public void playBlack() {
        this.ai = new Ai(new Board(this.board, this.boardTurns, this.moves, this.currentPlayer, this.mode, false));
        int nextMove = this.ai.Move();
        checkClick(nextMove % 3, nextMove / 3);
    }

    public void playBlackFirst() {
        float f = new Random().nextFloat();
        if (((double) f) < 0.5d) {
            checkClick(1, 1);
        } else if (((double) f) < 0.6d) {
            checkClick(0, 0);
        } else if (((double) f) < 0.7d) {
            checkClick(0, 2);
        } else if (((double) f) < 0.8d) {
            checkClick(2, 0);
        } else if (((double) f) < 0.9d) {
            checkClick(2, 2);
        } else if (((double) f) < 0.925d) {
            checkClick(0, 1);
        } else if (((double) f) < 0.95d) {
            checkClick(1, 0);
        } else if (((double) f) < 0.975d) {
            checkClick(1, 2);
        } else {
            checkClick(2, 1);
        }
    }

    public int checkTris(int row1, int col1, int row2, int col2, int row3, int col3) {
        int pos1 = this.board[row1][col1];
        int pos2 = this.board[row2][col2];
        int pos3 = this.board[row3][col3];
        if (pos1 == pos2 && pos1 == pos3 && pos1 != 0) {
            return pos1;
        }
        return 0;
    }

    public int checkById(int id) {
        return checkTris(this.tris[id][0][0], this.tris[id][0][1], this.tris[id][1][0], this.tris[id][1][1], this.tris[id][2][0], this.tris[id][2][1]);
    }

    public void updatePoints() {
        this.txtWonBlack.setText(Integer.toString(this.wonBlack));
        this.txtWonWhite.setText(Integer.toString(this.wonWhite));
    }

    public void onUpdate(float pSecondsElapsed) {
    }

    public void reset() {
    }

    public void sndWin() {
        this.win.play();
    }

    public void sndError() {
        this.error.play();
    }

    public void sndStart() {
        this.start.play();
    }

    public void sndDraw() {
        this.draw[new Random().nextInt(2)].play();
    }

    public void sndPiece() {
        this.sndPiece[new Random().nextInt(3)].play();
    }

    public void clearText() {
        for (int c = 0; c < 3; c++) {
            for (int r = 0; r < 3; r++) {
                this.txtNumbers[c][r].setText(" ");
                this.txtNumbers[c][r].setVisible(false);
            }
        }
    }

    public void clearMoves() {
        for (int c = 0; c < 3; c++) {
            for (int r = 0; r < 3; r++) {
                this.whiteMoves[c][r] = 0;
                this.blackMoves[c][r] = 0;
            }
        }
    }

    public void placeBest(int[][] moves2) {
        int bc = 0;
        int br = 0;
        int best = TimeConstants.MILLISECONDSPERSECOND;
        for (int c = 0; c < 3; c++) {
            for (int r = 0; r < 3; r++) {
                if (this.board[c][r] == 0 && moves2[c][r] < best) {
                    best = moves2[c][r];
                    bc = c;
                    br = r;
                }
            }
        }
        checkClick(bc, br);
    }

    public boolean checkPlaceMove(int[][] moves2, int c, int r, int position) {
        if (c < 0 || c >= 3 || r < 0 || r >= 3) {
            return false;
        }
        if (moves2[c][r] > 0) {
            sndError();
            return false;
        }
        moves2[c][r] = position;
        this.txtNumbers[c][r].setText(Integer.toString(position));
        this.txtNumbers[c][r].setVisible(true);
        return true;
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad arg0) {
    }
}
