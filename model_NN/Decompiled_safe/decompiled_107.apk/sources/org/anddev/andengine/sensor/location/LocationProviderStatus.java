package org.anddev.andengine.sensor.location;

public enum LocationProviderStatus {
    AVAILABLE,
    OUT_OF_SERVICE,
    TEMPORARILY_UNAVAILABLE
}
