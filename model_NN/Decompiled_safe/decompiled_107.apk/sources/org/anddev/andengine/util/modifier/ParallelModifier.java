package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.util.ModifierUtils;

public class ParallelModifier<T> extends BaseModifier<T> {
    private final float mDuration;
    /* access modifiers changed from: private */
    public boolean mFinishedCached;
    private final IModifier<T>[] mModifiers;

    public ParallelModifier(IModifier... pModifiers) throws IllegalArgumentException {
        this(null, pModifiers);
    }

    public ParallelModifier(IModifier.IModifierListener<T> pModifierListener, IModifier<T>... pModifiers) throws IllegalArgumentException {
        super(pModifierListener);
        if (pModifiers.length == 0) {
            throw new IllegalArgumentException("pModifiers must not be empty!");
        }
        this.mModifiers = pModifiers;
        IModifier<T> shapeModifierWithLongestDuration = ModifierUtils.getModifierWithLongestDuration(pModifiers);
        this.mDuration = shapeModifierWithLongestDuration.getDuration();
        shapeModifierWithLongestDuration.setModifierListener(new InternalModifierListener(this, null));
    }

    protected ParallelModifier(ParallelModifier parallelModifier) {
        super(parallelModifier.mModifierListener);
        IModifier[] otherModifiers = parallelModifier.mModifiers;
        this.mModifiers = new IModifier[otherModifiers.length];
        IModifier[] shapeModifiers = this.mModifiers;
        for (int i = shapeModifiers.length - 1; i >= 0; i--) {
            shapeModifiers[i] = otherModifiers[i].clone();
        }
        IModifier<T> shapeModifierWithLongestDuration = ModifierUtils.getModifierWithLongestDuration(shapeModifiers);
        this.mDuration = shapeModifierWithLongestDuration.getDuration();
        shapeModifierWithLongestDuration.setModifierListener(new InternalModifierListener(this, null));
    }

    public ParallelModifier<T> clone() {
        return new ParallelModifier<>(this);
    }

    public float getDuration() {
        return this.mDuration;
    }

    public void onUpdate(float pSecondsElapsed, T pItem) {
        this.mFinishedCached = false;
        IModifier[] shapeModifiers = this.mModifiers;
        int i = shapeModifiers.length - 1;
        while (i >= 0) {
            shapeModifiers[i].onUpdate(pSecondsElapsed, pItem);
            if (!this.mFinishedCached) {
                i--;
            } else {
                return;
            }
        }
        this.mFinishedCached = false;
    }

    public void reset() {
        this.mFinished = false;
        IModifier[] shapeModifiers = this.mModifiers;
        for (int i = shapeModifiers.length - 1; i >= 0; i--) {
            shapeModifiers[i].reset();
        }
    }

    private class InternalModifierListener implements IModifier.IModifierListener<T> {
        private InternalModifierListener() {
        }

        /* synthetic */ InternalModifierListener(ParallelModifier parallelModifier, InternalModifierListener internalModifierListener) {
            this();
        }

        public void onModifierFinished(IModifier<T> iModifier, T pItem) {
            ParallelModifier.this.mFinished = true;
            ParallelModifier.this.mFinishedCached = true;
            if (ParallelModifier.this.mModifierListener != null) {
                ParallelModifier.this.mModifierListener.onModifierFinished(ParallelModifier.this, pItem);
            }
        }
    }
}
