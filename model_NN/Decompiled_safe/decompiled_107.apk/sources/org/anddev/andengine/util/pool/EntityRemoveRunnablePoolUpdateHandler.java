package org.anddev.andengine.util.pool;

public class EntityRemoveRunnablePoolUpdateHandler extends RunnablePoolUpdateHandler<EntityRemoveRunnablePoolItem> {
    /* access modifiers changed from: protected */
    public EntityRemoveRunnablePoolItem onAllocatePoolItem() {
        return new EntityRemoveRunnablePoolItem();
    }
}
