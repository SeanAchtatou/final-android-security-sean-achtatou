package org.anddev.andengine.util;

import android.util.Log;
import org.anddev.andengine.util.constants.Constants;

public class Debug implements Constants {
    private static DebugLevel DEBUGLEVEL = DebugLevel.VERBOSE;

    public static void setDebugLevel(DebugLevel pDebugLevel) {
        if (pDebugLevel == null) {
            throw new IllegalArgumentException("pDebugLevel must not be null!");
        }
        DEBUGLEVEL = pDebugLevel;
    }

    public static DebugLevel getDebugLevel() {
        return DEBUGLEVEL;
    }

    public static void v(String pMessage) {
        v(pMessage, null);
    }

    public static void v(String pMessage, Throwable pThrowable) {
        if (DebugLevel.access$2(DEBUGLEVEL, DebugLevel.VERBOSE)) {
            Log.v(Constants.DEBUGTAG, pMessage, pThrowable);
        }
    }

    public static void d(String pMessage) {
        d(pMessage, null);
    }

    public static void d(String pMessage, Throwable pThrowable) {
        if (DebugLevel.access$2(DEBUGLEVEL, DebugLevel.DEBUG)) {
            Log.d(Constants.DEBUGTAG, pMessage, pThrowable);
        }
    }

    public static void i(String pMessage) {
        i(pMessage, null);
    }

    public static void i(String pMessage, Throwable pThrowable) {
        if (DebugLevel.access$2(DEBUGLEVEL, DebugLevel.INFO)) {
            Log.i(Constants.DEBUGTAG, pMessage, pThrowable);
        }
    }

    public static void w(String pMessage) {
        w(pMessage, null);
    }

    public static void w(Throwable pThrowable) {
        w(Constants.DEBUGTAG, pThrowable);
    }

    public static void w(String pMessage, Throwable pThrowable) {
        if (!DebugLevel.access$2(DEBUGLEVEL, DebugLevel.WARNING)) {
            return;
        }
        if (pThrowable == null) {
            Log.w(Constants.DEBUGTAG, pMessage, new Exception());
        } else {
            Log.w(Constants.DEBUGTAG, pMessage, pThrowable);
        }
    }

    public static void e(String pMessage) {
        e(pMessage, null);
    }

    public static void e(Throwable pThrowable) {
        e(Constants.DEBUGTAG, pThrowable);
    }

    public static void e(String pMessage, Throwable pThrowable) {
        if (!DebugLevel.access$2(DEBUGLEVEL, DebugLevel.ERROR)) {
            return;
        }
        if (pThrowable == null) {
            Log.e(Constants.DEBUGTAG, pMessage, new Exception());
        } else {
            Log.e(Constants.DEBUGTAG, pMessage, pThrowable);
        }
    }

    public enum DebugLevel implements Comparable<DebugLevel> {
        NONE,
        ERROR,
        WARNING,
        INFO,
        DEBUG,
        VERBOSE;
        
        public static DebugLevel ALL = VERBOSE;

        private boolean isSameOrLessThan(DebugLevel pDebugLevel) {
            return compareTo(pDebugLevel) >= 0;
        }
    }
}
