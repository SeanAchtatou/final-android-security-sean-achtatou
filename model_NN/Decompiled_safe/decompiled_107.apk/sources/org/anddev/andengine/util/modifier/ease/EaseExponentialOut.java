package org.anddev.andengine.util.modifier.ease;

public class EaseExponentialOut implements IEaseFunction {
    private static EaseExponentialOut INSTANCE;

    private EaseExponentialOut() {
    }

    public static EaseExponentialOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseExponentialOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        return (float) (pSecondsElapsed == pDuration ? (double) (pMinValue + pMaxValue) : (((double) pMaxValue) * ((-Math.pow(2.0d, (double) ((-10.0f * pSecondsElapsed) / pDuration))) + 1.0d)) + ((double) pMinValue));
    }
}
