package org.anddev.andengine.util;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketUtils {
    public static final String SOCKETEXCEPTION_MESSAGE_SOCKET_CLOSED = "socket closed";
    public static final String SOCKETEXCEPTION_MESSAGE_SOCKET_IS_CLOSED = "Socket is closed";

    public static void closeSocket(Socket pSocket) {
        if (pSocket != null && !pSocket.isClosed()) {
            try {
                pSocket.close();
            } catch (IOException e) {
                System.err.println(e.getStackTrace());
            }
        }
    }

    public static void closeSocket(ServerSocket pSocket) {
        if (pSocket != null && !pSocket.isClosed()) {
            try {
                pSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
