package org.anddev.andengine.util;

import android.content.Context;
import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;

public class FileUtils {
    public static void copyToExternalStorage(Context pContext, int pSourceResourceID, String pFilename) throws FileNotFoundException {
        copyToExternalStorage(pContext, pContext.getResources().openRawResource(pSourceResourceID), pFilename);
    }

    public static void copyToExternalStorage(Context pContext, String pSourceAssetPath, String pFilename) throws IOException {
        copyToExternalStorage(pContext, pContext.getAssets().open(pSourceAssetPath), pFilename);
    }

    public static void copyToExternalStorage(Context pContext, InputStream pInputStream, String pFilename) throws FileNotFoundException {
        if (isExternalStorageWriteable()) {
            StreamUtils.copyAndClose(pInputStream, new FileOutputStream(getAbsolutePathOnExternalStorage(pContext, pFilename)));
            return;
        }
        throw new IllegalStateException("External Storage is not writeable.");
    }

    public static boolean isFileExistingOnExternalStorage(Context pContext, String pFilename) {
        if (isExternalStorageReadable()) {
            return new File(getAbsolutePathOnExternalStorage(pContext, pFilename)).exists();
        }
        throw new IllegalStateException("External Storage is not readable.");
    }

    public static boolean isDirectoryExistingOnExternalStorage(Context pContext, String pDirectory) {
        if (isExternalStorageReadable()) {
            return new File(getAbsolutePathOnExternalStorage(pContext, pDirectory)).exists();
        }
        throw new IllegalStateException("External Storage is not readable.");
    }

    public static boolean ensureDirectoriesExistOnExternalStorage(Context pContext, String pDirectory) {
        if (isDirectoryExistingOnExternalStorage(pContext, pDirectory)) {
            return true;
        }
        if (isExternalStorageWriteable()) {
            return new File(getAbsolutePathOnExternalStorage(pContext, pDirectory)).mkdirs();
        }
        throw new IllegalStateException("External Storage is not writeable.");
    }

    public static InputStream openOnExternalStorage(Context pContext, String pFilename) throws FileNotFoundException {
        return new FileInputStream(getAbsolutePathOnExternalStorage(pContext, pFilename));
    }

    public static String[] getDirectoryListOnExternalStorage(Context pContext, String pFilename) throws FileNotFoundException {
        return new File(getAbsolutePathOnExternalStorage(pContext, pFilename)).list();
    }

    public static String[] getDirectoryListOnExternalStorage(Context pContext, String pFilename, FilenameFilter pFilenameFilter) throws FileNotFoundException {
        return new File(getAbsolutePathOnExternalStorage(pContext, pFilename)).list(pFilenameFilter);
    }

    public static String getAbsolutePathOnExternalStorage(Context pContext, String pFilename) {
        return Environment.getExternalStorageDirectory() + "/Android/data/" + pContext.getApplicationInfo().packageName + "/files/" + pFilename;
    }

    public static boolean isExternalStorageWriteable() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return state.equals("mounted") || state.equals("mounted_ro");
    }

    public static void copyFile(File pIn, File pOut) throws IOException {
        FileInputStream fis = new FileInputStream(pIn);
        FileOutputStream fos = new FileOutputStream(pOut);
        try {
            StreamUtils.copy(fis, fos);
        } finally {
            StreamUtils.closeStream(fis);
            StreamUtils.closeStream(fos);
        }
    }

    public static boolean deleteDirectory(File pFileOrDirectory) {
        if (pFileOrDirectory.isDirectory()) {
            for (String file : pFileOrDirectory.list()) {
                if (!deleteDirectory(new File(pFileOrDirectory, file))) {
                    return false;
                }
            }
        }
        return pFileOrDirectory.delete();
    }
}
