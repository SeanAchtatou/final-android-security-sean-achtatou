package org.anddev.andengine.util;

import android.content.Context;
import android.content.SharedPreferences;
import org.anddev.andengine.util.constants.Constants;

public class SimplePreferences implements Constants {
    private static SharedPreferences.Editor EDITORINSTANCE;
    private static SharedPreferences INSTANCE;
    private static final String PREFERENCES_NAME = null;

    public static SharedPreferences getInstance(Context ctx) {
        if (INSTANCE == null) {
            INSTANCE = ctx.getSharedPreferences(PREFERENCES_NAME, 0);
        }
        return INSTANCE;
    }

    public static SharedPreferences.Editor getEditorInstance(Context ctx) {
        if (EDITORINSTANCE == null) {
            EDITORINSTANCE = getInstance(ctx).edit();
        }
        return EDITORINSTANCE;
    }

    public static int incrementAccessCount(Context pCtx, String pKey) {
        return incrementAccessCount(pCtx, pKey, 1);
    }

    public static int incrementAccessCount(Context pCtx, String pKey, int pIncrement) {
        SharedPreferences prefs = getInstance(pCtx);
        int newAccessCount = prefs.getInt(pKey, 0) + pIncrement;
        prefs.edit().putInt(pKey, newAccessCount).commit();
        return newAccessCount;
    }

    public static int getAccessCount(Context pCtx, String pKey) {
        return getInstance(pCtx).getInt(pKey, 0);
    }
}
