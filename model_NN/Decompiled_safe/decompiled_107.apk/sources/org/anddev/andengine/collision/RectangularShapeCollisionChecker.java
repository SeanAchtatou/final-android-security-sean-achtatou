package org.anddev.andengine.collision;

import org.anddev.andengine.entity.shape.RectangularShape;
import org.anddev.andengine.util.MathUtils;

public class RectangularShapeCollisionChecker extends ShapeCollisionChecker {
    private static final int RECTANGULARSHAPE_VERTEX_COUNT = 4;
    private static final float[] VERTICES_COLLISION_TMP_A = new float[8];
    private static final float[] VERTICES_COLLISION_TMP_B = new float[8];
    private static final float[] VERTICES_CONTAINS_TMP = new float[8];

    public static boolean checkContains(RectangularShape pRectangularShape, float pX, float pY) {
        fillVertices(pRectangularShape, VERTICES_CONTAINS_TMP);
        return ShapeCollisionChecker.checkContains(VERTICES_CONTAINS_TMP, 8, pX, pY);
    }

    public static boolean checkCollision(RectangularShape pRectangularShapeA, RectangularShape pRectangularShapeB) {
        if (pRectangularShapeA.getRotation() != 0.0f || pRectangularShapeB.getRotation() != 0.0f || pRectangularShapeA.isScaled() || pRectangularShapeB.isScaled()) {
            fillVertices(pRectangularShapeA, VERTICES_COLLISION_TMP_A);
            fillVertices(pRectangularShapeB, VERTICES_COLLISION_TMP_B);
            return ShapeCollisionChecker.checkCollision(8, 8, VERTICES_COLLISION_TMP_A, VERTICES_COLLISION_TMP_B);
        }
        float aLeft = pRectangularShapeA.getX();
        float aTop = pRectangularShapeA.getY();
        float bLeft = pRectangularShapeB.getX();
        float bTop = pRectangularShapeB.getY();
        return BaseCollisionChecker.checkAxisAlignedRectangleCollision(aLeft, aTop, pRectangularShapeA.getWidth() + aLeft, pRectangularShapeA.getHeight() + aTop, bLeft, bTop, pRectangularShapeB.getWidth() + bLeft, pRectangularShapeB.getHeight() + bTop);
    }

    public static void fillVertices(RectangularShape pRectangularShape, float[] pVertices) {
        float left = pRectangularShape.getX();
        float top = pRectangularShape.getY();
        float right = pRectangularShape.getWidth() + left;
        float bottom = pRectangularShape.getHeight() + top;
        pVertices[0] = left;
        pVertices[1] = top;
        pVertices[2] = right;
        pVertices[3] = top;
        pVertices[4] = right;
        pVertices[5] = bottom;
        pVertices[6] = left;
        pVertices[7] = bottom;
        MathUtils.rotateAndScaleAroundCenter(pVertices, pRectangularShape.getRotation(), left + pRectangularShape.getRotationCenterX(), top + pRectangularShape.getRotationCenterY(), pRectangularShape.getScaleX(), pRectangularShape.getScaleY(), left + pRectangularShape.getScaleCenterX(), top + pRectangularShape.getScaleCenterY());
    }
}
