package org.anddev.andengine.engine.options;

public class RenderOptions {
    private boolean mDisableExtensionVertexBufferObjects = false;

    public RenderOptions enableExtensionVertexBufferObjects() {
        return setDisableExtensionVertexBufferObjects(false);
    }

    public RenderOptions disableExtensionVertexBufferObjects() {
        return setDisableExtensionVertexBufferObjects(true);
    }

    public RenderOptions setDisableExtensionVertexBufferObjects(boolean pDisableExtensionVertexBufferObjects) {
        this.mDisableExtensionVertexBufferObjects = pDisableExtensionVertexBufferObjects;
        return this;
    }

    public boolean isDisableExtensionVertexBufferObjects() {
        return this.mDisableExtensionVertexBufferObjects;
    }
}
