package org.anddev.andengine.engine.handler;

public interface IUpdateHandler {
    void onUpdate(float f);

    void reset();
}
