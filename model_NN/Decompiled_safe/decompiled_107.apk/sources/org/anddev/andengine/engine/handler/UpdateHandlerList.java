package org.anddev.andengine.engine.handler;

import java.util.ArrayList;

public class UpdateHandlerList extends ArrayList<IUpdateHandler> implements IUpdateHandler {
    private static final long serialVersionUID = -8842562717687229277L;

    public void onUpdate(float pSecondsElapsed) {
        for (int i = size() - 1; i >= 0; i--) {
            ((IUpdateHandler) get(i)).onUpdate(pSecondsElapsed);
        }
    }

    public void reset() {
        for (int i = size() - 1; i >= 0; i--) {
            ((IUpdateHandler) get(i)).reset();
        }
    }
}
