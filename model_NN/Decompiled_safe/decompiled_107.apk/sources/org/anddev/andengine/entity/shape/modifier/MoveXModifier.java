package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class MoveXModifier extends SingleValueSpanShapeModifier {
    public MoveXModifier(float pDuration, float pFromX, float pToX) {
        this(pDuration, pFromX, pToX, null, IEaseFunction.DEFAULT);
    }

    public MoveXModifier(float pDuration, float pFromX, float pToX, IEaseFunction pEaseFunction) {
        this(pDuration, pFromX, pToX, null, pEaseFunction);
    }

    public MoveXModifier(float pDuration, float pFromX, float pToX, IShapeModifier.IShapeModifierListener pShapeModifierListener) {
        super(pDuration, pFromX, pToX, pShapeModifierListener, IEaseFunction.DEFAULT);
    }

    public MoveXModifier(float pDuration, float pFromX, float pToX, IShapeModifier.IShapeModifierListener pShapeModifierListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromX, pToX, pShapeModifierListener, pEaseFunction);
    }

    protected MoveXModifier(MoveXModifier pMoveXModifier) {
        super(pMoveXModifier);
    }

    public MoveXModifier clone() {
        return new MoveXModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(IShape pShape, float pX) {
        pShape.setPosition(pX, pShape.getY());
    }

    /* access modifiers changed from: protected */
    public void onSetValue(IShape pShape, float pPercentageDone, float pX) {
        pShape.setPosition(pX, pShape.getY());
    }
}
