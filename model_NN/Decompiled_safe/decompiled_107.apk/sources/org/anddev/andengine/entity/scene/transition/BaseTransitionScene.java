package org.anddev.andengine.entity.scene.transition;

import org.anddev.andengine.entity.scene.Scene;

public abstract class BaseTransitionScene extends Scene {
    protected final float mDuration;
    protected final Scene mNewScene;
    protected final Scene mOldScene;

    protected BaseTransitionScene(int pLayerCount, float pDuration, Scene pOldScene, Scene pNewScene) {
        super(pLayerCount);
        if (pOldScene == pNewScene) {
            throw new IllegalArgumentException("New scene must be different from old scene!");
        }
        this.mDuration = pDuration;
        this.mNewScene = pOldScene;
        this.mOldScene = pNewScene;
    }
}
