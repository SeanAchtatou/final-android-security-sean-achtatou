package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.util.modifier.BaseSingleValueSpanModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public abstract class SingleValueSpanShapeModifier extends BaseSingleValueSpanModifier<IShape> implements IShapeModifier {
    public SingleValueSpanShapeModifier(float pDuration, float pFromValue, float pToValue) {
        super(pDuration, pFromValue, pToValue);
    }

    public SingleValueSpanShapeModifier(float pDuration, float pFromValue, float pToValue, IEaseFunction pEaseFunction) {
        super(pDuration, pFromValue, pToValue, pEaseFunction);
    }

    public SingleValueSpanShapeModifier(float pDuration, float pFromValue, float pToValue, IShapeModifier.IShapeModifierListener pShapeModifierListener) {
        super(pDuration, pFromValue, pToValue, pShapeModifierListener);
    }

    public SingleValueSpanShapeModifier(float pDuration, float pFromValue, float pToValue, IShapeModifier.IShapeModifierListener pShapeModifierListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromValue, pToValue, pShapeModifierListener, pEaseFunction);
    }

    protected SingleValueSpanShapeModifier(SingleValueSpanShapeModifier pSingleValueSpanShapeModifier) {
        super(pSingleValueSpanShapeModifier);
    }
}
