package org.anddev.andengine.entity;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;

public abstract class Entity implements IEntity {
    private boolean mIgnoreUpdate;
    private boolean mVisible = true;
    private int mZIndex = 0;

    /* access modifiers changed from: protected */
    public abstract void onManagedDraw(GL10 gl10, Camera camera);

    /* access modifiers changed from: protected */
    public abstract void onManagedUpdate(float f);

    public Entity() {
    }

    public Entity(int pZIndex) {
        this.mZIndex = pZIndex;
    }

    public boolean isVisible() {
        return this.mVisible;
    }

    public void setVisible(boolean pVisible) {
        this.mVisible = pVisible;
    }

    public boolean isIgnoreUpdate() {
        return this.mIgnoreUpdate;
    }

    public void setIgnoreUpdate(boolean pIgnoreUpdate) {
        this.mIgnoreUpdate = pIgnoreUpdate;
    }

    public int getZIndex() {
        return this.mZIndex;
    }

    public void setZIndex(int pZIndex) {
        this.mZIndex = pZIndex;
    }

    public final void onDraw(GL10 pGL, Camera pCamera) {
        if (this.mVisible) {
            onManagedDraw(pGL, pCamera);
        }
    }

    public final void onUpdate(float pSecondsElapsed) {
        if (!this.mIgnoreUpdate) {
            onManagedUpdate(pSecondsElapsed);
        }
    }

    public void reset() {
        this.mVisible = true;
        this.mIgnoreUpdate = false;
    }
}
