package org.anddev.andengine.entity.shape.util;

import android.util.FloatMath;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.util.MathUtils;

public class ShapeUtils {
    @Deprecated
    public void setVelocityRespectingRotation(IShape pShape, float pVelocityX, float pVelocityY) {
        float rotationRad = MathUtils.degToRad(pShape.getRotation());
        float sin = FloatMath.sin(rotationRad);
        float cos = FloatMath.cos(rotationRad);
        pShape.setVelocity(((-pVelocityY) * sin) + (cos * pVelocityX), (cos * pVelocityY) + (sin * pVelocityX));
    }

    @Deprecated
    public void accelerateRespectingRotation(IShape pShape, float pAccelerationX, float pAccelerationY) {
        float rotationRad = MathUtils.degToRad(pShape.getRotation());
        float sin = FloatMath.sin(rotationRad);
        float cos = FloatMath.cos(rotationRad);
        pShape.setAcceleration(((-pAccelerationY) * sin) + (cos * pAccelerationX), (cos * pAccelerationY) + (sin * pAccelerationX));
    }
}
