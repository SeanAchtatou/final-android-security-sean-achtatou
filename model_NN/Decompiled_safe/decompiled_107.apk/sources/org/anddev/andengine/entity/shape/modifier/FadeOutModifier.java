package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class FadeOutModifier extends AlphaModifier {
    public FadeOutModifier(float pDuration) {
        super(pDuration, 1.0f, 0.0f, IEaseFunction.DEFAULT);
    }

    public FadeOutModifier(float pDuration, IEaseFunction pEaseFunction) {
        super(pDuration, 1.0f, 0.0f, pEaseFunction);
    }

    public FadeOutModifier(float pDuration, IShapeModifier.IShapeModifierListener pShapeModifierListener) {
        super(pDuration, 1.0f, 0.0f, pShapeModifierListener, IEaseFunction.DEFAULT);
    }

    public FadeOutModifier(float pDuration, IShapeModifier.IShapeModifierListener pShapeModifierListener, IEaseFunction pEaseFunction) {
        super(pDuration, 1.0f, 0.0f, pShapeModifierListener, pEaseFunction);
    }

    protected FadeOutModifier(FadeOutModifier pFadeOutModifier) {
        super(pFadeOutModifier);
    }

    public FadeOutModifier clone() {
        return new FadeOutModifier(this);
    }
}
