package org.anddev.andengine.entity.layer;

import java.util.ArrayList;
import java.util.Comparator;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.util.IEntityMatcher;

public class DynamicCapacityLayer extends BaseLayer {
    private static final int CAPACITY_DEFAULT = 10;
    private final ArrayList<IEntity> mEntities;

    public DynamicCapacityLayer() {
        this(10);
    }

    public DynamicCapacityLayer(int pExpectedCapacity) {
        this.mEntities = new ArrayList<>(pExpectedCapacity);
    }

    public IEntity getEntity(int pIndex) {
        return this.mEntities.get(pIndex);
    }

    public int getEntityCount() {
        return this.mEntities.size();
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 pGL, Camera pCamera) {
        ArrayList<IEntity> entities = this.mEntities;
        int entityCount = entities.size();
        for (int i = 0; i < entityCount; i++) {
            entities.get(i).onDraw(pGL, pCamera);
        }
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float pSecondsElapsed) {
        ArrayList<IEntity> entities = this.mEntities;
        int entityCount = entities.size();
        for (int i = 0; i < entityCount; i++) {
            entities.get(i).onUpdate(pSecondsElapsed);
        }
    }

    public void reset() {
        super.reset();
        ArrayList<IEntity> entities = this.mEntities;
        for (int i = entities.size() - 1; i >= 0; i--) {
            entities.get(i).reset();
        }
    }

    public void clear() {
        this.mEntities.clear();
    }

    public void addEntity(IEntity pEntity) {
        this.mEntities.add(pEntity);
    }

    public boolean removeEntity(IEntity pEntity) {
        return this.mEntities.remove(pEntity);
    }

    public IEntity removeEntity(int pIndex) {
        return this.mEntities.remove(pIndex);
    }

    public boolean removeEntity(IEntityMatcher pEntityMatcher) {
        ArrayList<IEntity> entities = this.mEntities;
        for (int i = entities.size() - 1; i >= 0; i--) {
            if (pEntityMatcher.matches(entities.get(i))) {
                entities.remove(i);
                return true;
            }
        }
        return false;
    }

    public IEntity findEntity(IEntityMatcher pEntityMatcher) {
        ArrayList<IEntity> entities = this.mEntities;
        for (int i = entities.size() - 1; i >= 0; i--) {
            IEntity entity = entities.get(i);
            if (pEntityMatcher.matches(entity)) {
                return entity;
            }
        }
        return null;
    }

    public void sortEntities() {
        ZIndexSorter.getInstance().sort(this.mEntities);
    }

    public void sortEntities(Comparator<IEntity> pEntityComparator) {
        ZIndexSorter.getInstance().sort(this.mEntities, pEntityComparator);
    }

    public IEntity replaceEntity(int pEntityIndex, IEntity pEntity) {
        return this.mEntities.set(pEntityIndex, pEntity);
    }

    public void setEntity(int pEntityIndex, IEntity pEntity) {
        if (pEntityIndex == this.mEntities.size()) {
            addEntity(pEntity);
        } else {
            this.mEntities.set(pEntityIndex, pEntity);
        }
    }

    public void swapEntities(int pEntityIndexA, int pEntityIndexB) {
        ArrayList<IEntity> entities = this.mEntities;
        entities.set(pEntityIndexA, entities.set(pEntityIndexB, entities.get(pEntityIndexA)));
    }
}
