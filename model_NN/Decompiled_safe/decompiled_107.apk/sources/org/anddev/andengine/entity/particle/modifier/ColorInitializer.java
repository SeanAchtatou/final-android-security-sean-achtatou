package org.anddev.andengine.entity.particle.modifier;

public class ColorInitializer extends BaseTripleValueInitializer {
    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: org.anddev.andengine.entity.particle.Particle.setColor(float, float, float):void in method: org.anddev.andengine.entity.particle.modifier.ColorInitializer.onInitializeParticle(org.anddev.andengine.entity.particle.Particle, float, float, float):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: org.anddev.andengine.entity.particle.Particle.setColor(float, float, float):void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:540)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    protected void onInitializeParticle(org.anddev.andengine.entity.particle.Particle r1, float r2, float r3, float r4) {
        /*
            r0 = this;
            r1.setColor(r2, r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.entity.particle.modifier.ColorInitializer.onInitializeParticle(org.anddev.andengine.entity.particle.Particle, float, float, float):void");
    }

    public ColorInitializer(float pRed, float pGreen, float pBlue) {
        super(pRed, pRed, pGreen, pGreen, pBlue, pBlue);
    }

    public ColorInitializer(float pMinRed, float pMaxRed, float pMinGreen, float pMaxGreen, float pMinBlue, float pMaxBlue) {
        super(pMinRed, pMaxRed, pMinGreen, pMaxGreen, pMinBlue, pMaxBlue);
    }
}
