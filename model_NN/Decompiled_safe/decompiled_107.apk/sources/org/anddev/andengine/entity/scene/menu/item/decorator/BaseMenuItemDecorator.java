package org.anddev.andengine.entity.scene.menu.item.decorator;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.scene.menu.item.IMenuItem;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.util.modifier.IModifier;

public abstract class BaseMenuItemDecorator implements IMenuItem {
    private final IMenuItem mMenuItem;

    /* access modifiers changed from: protected */
    public abstract void onMenuItemReset(IMenuItem iMenuItem);

    /* access modifiers changed from: protected */
    public abstract void onMenuItemSelected(IMenuItem iMenuItem);

    /* access modifiers changed from: protected */
    public abstract void onMenuItemUnselected(IMenuItem iMenuItem);

    public BaseMenuItemDecorator(IMenuItem pMenuItem) {
        this.mMenuItem = pMenuItem;
    }

    public int getID() {
        return this.mMenuItem.getID();
    }

    public final void onSelected() {
        this.mMenuItem.onSelected();
        onMenuItemSelected(this.mMenuItem);
    }

    public final void onUnselected() {
        this.mMenuItem.onUnselected();
        onMenuItemUnselected(this.mMenuItem);
    }

    public void accelerate(float pAccelerationX, float pAccelerationY) {
        this.mMenuItem.accelerate(pAccelerationX, pAccelerationY);
    }

    public void addShapeModifier(IModifier<IShape> pShapeModifier) {
        this.mMenuItem.addShapeModifier(pShapeModifier);
    }

    public void clearShapeModifiers() {
        this.mMenuItem.clearShapeModifiers();
    }

    public boolean collidesWith(IShape pOtherShape) {
        return this.mMenuItem.collidesWith(pOtherShape);
    }

    public float getAccelerationX() {
        return this.mMenuItem.getAccelerationX();
    }

    public float getAccelerationY() {
        return this.mMenuItem.getAccelerationY();
    }

    public float getAlpha() {
        return this.mMenuItem.getAlpha();
    }

    public float getAngularVelocity() {
        return this.mMenuItem.getAngularVelocity();
    }

    public float getBaseHeight() {
        return this.mMenuItem.getBaseHeight();
    }

    public float getBaseWidth() {
        return this.mMenuItem.getBaseWidth();
    }

    public float getBaseX() {
        return this.mMenuItem.getBaseX();
    }

    public float getBaseY() {
        return this.mMenuItem.getBaseY();
    }

    public float getBlue() {
        return this.mMenuItem.getBlue();
    }

    public float getGreen() {
        return this.mMenuItem.getGreen();
    }

    public float getHeight() {
        return this.mMenuItem.getHeight();
    }

    public float getHeightScaled() {
        return this.mMenuItem.getHeightScaled();
    }

    public float getRed() {
        return this.mMenuItem.getRed();
    }

    public float getRotation() {
        return this.mMenuItem.getRotation();
    }

    public float getRotationCenterX() {
        return this.mMenuItem.getRotationCenterX();
    }

    public float getRotationCenterY() {
        return this.mMenuItem.getRotationCenterY();
    }

    public float getScaleCenterX() {
        return this.mMenuItem.getScaleCenterX();
    }

    public float getScaleCenterY() {
        return this.mMenuItem.getScaleCenterY();
    }

    public float getScaleX() {
        return this.mMenuItem.getScaleX();
    }

    public float getScaleY() {
        return this.mMenuItem.getScaleY();
    }

    public float[] getSceneCenterCoordinates() {
        return this.mMenuItem.getSceneCenterCoordinates();
    }

    public float getVelocityX() {
        return this.mMenuItem.getVelocityX();
    }

    public float getVelocityY() {
        return this.mMenuItem.getVelocityY();
    }

    public float getWidth() {
        return this.mMenuItem.getWidth();
    }

    public float getWidthScaled() {
        return this.mMenuItem.getWidthScaled();
    }

    public float getX() {
        return this.mMenuItem.getX();
    }

    public float getY() {
        return this.mMenuItem.getY();
    }

    public boolean isCullingEnabled() {
        return this.mMenuItem.isCullingEnabled();
    }

    public boolean isScaled() {
        return this.mMenuItem.isScaled();
    }

    public boolean isUpdatePhysics() {
        return this.mMenuItem.isUpdatePhysics();
    }

    public boolean removeShapeModifier(IModifier<IShape> pShapeModifier) {
        return this.mMenuItem.removeShapeModifier(pShapeModifier);
    }

    public void setAcceleration(float pAcceleration) {
        this.mMenuItem.setAcceleration(pAcceleration);
    }

    public void setAcceleration(float pAccelerationX, float pAccelerationY) {
        this.mMenuItem.setAcceleration(pAccelerationX, pAccelerationY);
    }

    public void setAccelerationX(float pAccelerationX) {
        this.mMenuItem.setAccelerationX(pAccelerationX);
    }

    public void setAccelerationY(float pAccelerationY) {
        this.mMenuItem.setAccelerationY(pAccelerationY);
    }

    public void setAlpha(float pAlpha) {
        this.mMenuItem.setAlpha(pAlpha);
    }

    public void setAngularVelocity(float pAngularVelocity) {
        this.mMenuItem.setAngularVelocity(pAngularVelocity);
    }

    public void setBasePosition() {
        this.mMenuItem.setBasePosition();
    }

    public void setBlendFunction(int pSourceBlendFunction, int pDestinationBlendFunction) {
        this.mMenuItem.setBlendFunction(pSourceBlendFunction, pDestinationBlendFunction);
    }

    public void setColor(float pRed, float pGreen, float pBlue) {
        this.mMenuItem.setColor(pRed, pGreen, pBlue);
    }

    public void setColor(float pRed, float pGreen, float pBlue, float pAlpha) {
        this.mMenuItem.setColor(pRed, pGreen, pBlue, pAlpha);
    }

    public void setCullingEnabled(boolean pCullingEnabled) {
        this.mMenuItem.setCullingEnabled(pCullingEnabled);
    }

    public void setPosition(IShape pOtherShape) {
        this.mMenuItem.setPosition(pOtherShape);
    }

    public void setPosition(float pX, float pY) {
        this.mMenuItem.setPosition(pX, pY);
    }

    public void setRotation(float pRotation) {
        this.mMenuItem.setRotation(pRotation);
    }

    public void setRotationCenter(float pRotationCenterX, float pRotationCenterY) {
        this.mMenuItem.setRotationCenter(pRotationCenterX, pRotationCenterY);
    }

    public void setRotationCenterX(float pRotationCenterX) {
        this.mMenuItem.setRotationCenterX(pRotationCenterX);
    }

    public void setRotationCenterY(float pRotationCenterY) {
        this.mMenuItem.setRotationCenterY(pRotationCenterY);
    }

    public void setScale(float pScale) {
        this.mMenuItem.setScale(pScale);
    }

    public void setScale(float pScaleX, float pScaleY) {
        this.mMenuItem.setScale(pScaleX, pScaleY);
    }

    public void setScaleCenter(float pScaleCenterX, float pScaleCenterY) {
        this.mMenuItem.setScaleCenter(pScaleCenterX, pScaleCenterY);
    }

    public void setScaleCenterX(float pScaleCenterX) {
        this.mMenuItem.setScaleCenterX(pScaleCenterX);
    }

    public void setScaleCenterY(float pScaleCenterY) {
        this.mMenuItem.setScaleCenterY(pScaleCenterY);
    }

    public void setScaleX(float pScaleX) {
        this.mMenuItem.setScaleX(pScaleX);
    }

    public void setScaleY(float pScaleY) {
        this.mMenuItem.setScaleY(pScaleY);
    }

    public void setUpdatePhysics(boolean pUpdatePhysics) {
        this.mMenuItem.setUpdatePhysics(pUpdatePhysics);
    }

    public void setVelocity(float pVelocity) {
        this.mMenuItem.setVelocity(pVelocity);
    }

    public void setVelocity(float pVelocityX, float pVelocityY) {
        this.mMenuItem.setVelocity(pVelocityX, pVelocityY);
    }

    public void setVelocityX(float pVelocityX) {
        this.mMenuItem.setVelocityX(pVelocityX);
    }

    public void setVelocityY(float pVelocityY) {
        this.mMenuItem.setVelocityY(pVelocityY);
    }

    public int getZIndex() {
        return this.mMenuItem.getZIndex();
    }

    public void setZIndex(int pZIndex) {
        this.mMenuItem.setZIndex(pZIndex);
    }

    public void onDraw(GL10 pGL, Camera pCamera) {
        this.mMenuItem.onDraw(pGL, pCamera);
    }

    public void onUpdate(float pSecondsElapsed) {
        this.mMenuItem.onUpdate(pSecondsElapsed);
    }

    public void reset() {
        this.mMenuItem.reset();
        onMenuItemReset(this.mMenuItem);
    }

    public boolean contains(float pX, float pY) {
        return this.mMenuItem.contains(pX, pY);
    }

    public float[] convertLocalToSceneCoordinates(float pX, float pY) {
        return this.mMenuItem.convertLocalToSceneCoordinates(pX, pY);
    }

    public float[] convertSceneToLocalCoordinates(float pX, float pY) {
        return this.mMenuItem.convertSceneToLocalCoordinates(pX, pY);
    }

    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        return this.mMenuItem.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
    }
}
