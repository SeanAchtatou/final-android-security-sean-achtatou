package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class AlphaModifier extends SingleValueSpanShapeModifier {
    public AlphaModifier(float pDuration, float pFromAlpha, float pToAlpha) {
        this(pDuration, pFromAlpha, pToAlpha, null, IEaseFunction.DEFAULT);
    }

    public AlphaModifier(float pDuration, float pFromAlpha, float pToAlpha, IEaseFunction pEaseFunction) {
        this(pDuration, pFromAlpha, pToAlpha, null, pEaseFunction);
    }

    public AlphaModifier(float pDuration, float pFromAlpha, float pToAlpha, IShapeModifier.IShapeModifierListener pShapeModifierListener) {
        super(pDuration, pFromAlpha, pToAlpha, pShapeModifierListener, IEaseFunction.DEFAULT);
    }

    public AlphaModifier(float pDuration, float pFromAlpha, float pToAlpha, IShapeModifier.IShapeModifierListener pShapeModifierListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromAlpha, pToAlpha, pShapeModifierListener, pEaseFunction);
    }

    protected AlphaModifier(AlphaModifier pAlphaModifier) {
        super(pAlphaModifier);
    }

    public AlphaModifier clone() {
        return new AlphaModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(IShape pShape, float pAlpha) {
        pShape.setAlpha(pAlpha);
    }

    /* access modifiers changed from: protected */
    public void onSetValue(IShape pShape, float pPercentageDone, float pAlpha) {
        pShape.setAlpha(pAlpha);
    }
}
