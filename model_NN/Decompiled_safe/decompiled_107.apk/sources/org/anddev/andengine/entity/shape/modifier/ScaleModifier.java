package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class ScaleModifier extends DoubleValueSpanShapeModifier {
    public ScaleModifier(float pDuration, float pFromScale, float pToScale) {
        this(pDuration, pFromScale, pToScale, (IShapeModifier.IShapeModifierListener) null, IEaseFunction.DEFAULT);
    }

    public ScaleModifier(float pDuration, float pFromScale, float pToScale, IEaseFunction pEaseFunction) {
        this(pDuration, pFromScale, pToScale, (IShapeModifier.IShapeModifierListener) null, pEaseFunction);
    }

    public ScaleModifier(float pDuration, float pFromScale, float pToScale, IShapeModifier.IShapeModifierListener pShapeModifierListener) {
        this(pDuration, pFromScale, pToScale, pFromScale, pToScale, pShapeModifierListener, IEaseFunction.DEFAULT);
    }

    public ScaleModifier(float pDuration, float pFromScale, float pToScale, IShapeModifier.IShapeModifierListener pShapeModifierListener, IEaseFunction pEaseFunction) {
        this(pDuration, pFromScale, pToScale, pFromScale, pToScale, pShapeModifierListener, pEaseFunction);
    }

    public ScaleModifier(float pDuration, float pFromScaleX, float pToScaleX, float pFromScaleY, float pToScaleY) {
        this(pDuration, pFromScaleX, pToScaleX, pFromScaleY, pToScaleY, null, IEaseFunction.DEFAULT);
    }

    public ScaleModifier(float pDuration, float pFromScaleX, float pToScaleX, float pFromScaleY, float pToScaleY, IEaseFunction pEaseFunction) {
        this(pDuration, pFromScaleX, pToScaleX, pFromScaleY, pToScaleY, null, pEaseFunction);
    }

    public ScaleModifier(float pDuration, float pFromScaleX, float pToScaleX, float pFromScaleY, float pToScaleY, IShapeModifier.IShapeModifierListener pShapeModifierListener) {
        super(pDuration, pFromScaleX, pToScaleX, pFromScaleY, pToScaleY, pShapeModifierListener, IEaseFunction.DEFAULT);
    }

    public ScaleModifier(float pDuration, float pFromScaleX, float pToScaleX, float pFromScaleY, float pToScaleY, IShapeModifier.IShapeModifierListener pShapeModifierListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromScaleX, pToScaleX, pFromScaleY, pToScaleY, pShapeModifierListener, pEaseFunction);
    }

    protected ScaleModifier(ScaleModifier pScaleModifier) {
        super(pScaleModifier);
    }

    public ScaleModifier clone() {
        return new ScaleModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValues(IShape pShape, float pScaleA, float pScaleB) {
        pShape.setScale(pScaleA, pScaleB);
    }

    /* access modifiers changed from: protected */
    public void onSetValues(IShape pShape, float pPercentageDone, float pScaleA, float pScaleB) {
        pShape.setScale(pScaleA, pScaleB);
    }
}
