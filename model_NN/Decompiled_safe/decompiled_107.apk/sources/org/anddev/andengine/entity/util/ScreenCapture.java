package org.anddev.andengine.entity.util;

import android.graphics.Bitmap;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.StreamUtils;

public class ScreenCapture extends Entity {
    private String mFilePath;
    private int mHeight;
    private IScreenCaptureCallback mScreenCaptureCallback;
    private boolean mScreenCapturePending = false;
    private int mWidth;

    public interface IScreenCaptureCallback {
        void onScreenCaptured(String str);
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 pGL, Camera pCamera) {
        if (this.mScreenCapturePending) {
            saveCapture(this.mWidth, this.mHeight, this.mFilePath, pGL);
            this.mScreenCaptureCallback.onScreenCaptured(this.mFilePath);
            this.mScreenCapturePending = false;
            this.mFilePath = null;
            this.mScreenCaptureCallback = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float pSecondsElapsed) {
    }

    public void reset() {
    }

    public void capture(int pWidth, int pHeight, String pFilePath, IScreenCaptureCallback pScreenCaptureCallback) {
        this.mWidth = pWidth;
        this.mHeight = pHeight;
        this.mFilePath = pFilePath;
        this.mScreenCaptureCallback = pScreenCaptureCallback;
        this.mScreenCapturePending = true;
    }

    /* JADX INFO: Multiple debug info for r21v1 int: [D('k' int), D('pGL' javax.microedition.khronos.opengles.GL10)] */
    /* JADX INFO: Multiple debug info for r8v9 int: [D('pb' int), D('pix1' int)] */
    private static Bitmap capture(int pX, int pY, int pWidth, int pHeight, GL10 pGL) {
        int[] b = new int[((pY + pHeight) * pWidth)];
        int[] bt = new int[(pWidth * pHeight)];
        IntBuffer ib = IntBuffer.wrap(b);
        ib.position(0);
        pGL.glReadPixels(pX, 0, pWidth, pY + pHeight, 6408, 5121, ib);
        int i = 0;
        int j = 0;
        while (true) {
            int k = j;
            if (i >= pHeight) {
                return Bitmap.createBitmap(bt, pWidth, pHeight, Bitmap.Config.ARGB_8888);
            }
            for (int j2 = 0; j2 < pWidth; j2++) {
                int pix = b[(i * pWidth) + j2];
                bt[(((pHeight - k) - 1) * pWidth) + j2] = ((pix >> 16) & 255) | (pix & -16711936) | ((pix << 16) & 16711680);
            }
            i++;
            j = k + 1;
        }
    }

    private static void saveCapture(int pWidth, int pHeight, String pFilePath, GL10 pGL) {
        saveCapture(0, 0, pWidth, pHeight, pFilePath, pGL);
    }

    private static void saveCapture(int pX, int pY, int pWidth, int pHeight, String pFilePath, GL10 pGL) {
        Bitmap bmp = capture(pX, pY, pWidth, pHeight, pGL);
        try {
            FileOutputStream fos = new FileOutputStream(pFilePath);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
            StreamUtils.flushCloseStream(fos);
        } catch (FileNotFoundException e) {
            Debug.e("Error saving file to: " + pFilePath, e);
        }
    }
}
