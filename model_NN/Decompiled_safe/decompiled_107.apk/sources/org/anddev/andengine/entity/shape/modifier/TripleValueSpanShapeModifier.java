package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.util.modifier.BaseTripleValueSpanModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public abstract class TripleValueSpanShapeModifier extends BaseTripleValueSpanModifier<IShape> implements IShapeModifier {
    public TripleValueSpanShapeModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB, float pFromValueC, float pToValueC, IEaseFunction pEaseFunction) {
        super(pDuration, pFromValueA, pToValueA, pFromValueB, pToValueB, pFromValueC, pToValueC, pEaseFunction);
    }

    public TripleValueSpanShapeModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB, float pFromValueC, float pToValueC, IShapeModifier.IShapeModifierListener pShapeModifierListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromValueA, pToValueA, pFromValueB, pToValueB, pFromValueC, pToValueC, pShapeModifierListener, pEaseFunction);
    }

    protected TripleValueSpanShapeModifier(TripleValueSpanShapeModifier pTripleValueSpanModifier) {
        super(pTripleValueSpanModifier);
    }
}
