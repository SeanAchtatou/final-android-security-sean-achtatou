package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.util.modifier.LoopModifier;

public class LoopShapeModifier extends LoopModifier<IShape> implements IShapeModifier {

    public interface ILoopShapeModifierListener extends LoopModifier.ILoopModifierListener<IShape> {
    }

    public LoopShapeModifier(IShapeModifier pShapeModifier) {
        super(pShapeModifier);
    }

    public LoopShapeModifier(IShapeModifier.IShapeModifierListener pShapeModifierListener, int pLoopCount, ILoopShapeModifierListener pLoopModifierListener, IShapeModifier pShapeModifier) {
        super(pShapeModifierListener, pLoopCount, pLoopModifierListener, pShapeModifier);
    }

    public LoopShapeModifier(IShapeModifier.IShapeModifierListener pShapeModifierListener, int pLoopCount, IShapeModifier pShapeModifier) {
        super(pShapeModifierListener, pLoopCount, pShapeModifier);
    }

    public LoopShapeModifier(int pLoopCount, IShapeModifier pShapeModifier) {
        super(pLoopCount, pShapeModifier);
    }

    protected LoopShapeModifier(LoopShapeModifier pLoopShapeModifier) {
        super((LoopModifier) pLoopShapeModifier);
    }

    public LoopShapeModifier clone() {
        return new LoopShapeModifier(this);
    }
}
