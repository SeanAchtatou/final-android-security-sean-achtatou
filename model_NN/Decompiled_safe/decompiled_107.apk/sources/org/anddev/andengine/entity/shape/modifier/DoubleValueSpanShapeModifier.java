package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.util.modifier.BaseDoubleValueSpanModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public abstract class DoubleValueSpanShapeModifier extends BaseDoubleValueSpanModifier<IShape> implements IShapeModifier {
    public DoubleValueSpanShapeModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB) {
        super(pDuration, pFromValueA, pToValueA, pFromValueB, pToValueB);
    }

    public DoubleValueSpanShapeModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB, IEaseFunction pEaseFunction) {
        super(pDuration, pFromValueA, pToValueA, pFromValueB, pToValueB, pEaseFunction);
    }

    public DoubleValueSpanShapeModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB, IShapeModifier.IShapeModifierListener pShapeModifierListener) {
        super(pDuration, pFromValueA, pToValueA, pFromValueB, pToValueB, pShapeModifierListener);
    }

    public DoubleValueSpanShapeModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB, IShapeModifier.IShapeModifierListener pShapeModifierListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromValueA, pToValueA, pFromValueB, pToValueB, pShapeModifierListener, pEaseFunction);
    }

    protected DoubleValueSpanShapeModifier(DoubleValueSpanShapeModifier pDoubleValueSpanModifier) {
        super(pDoubleValueSpanModifier);
    }
}
