package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.util.Path;
import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.SequenceModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class PathModifier extends ShapeModifier {
    private final Path mPath;
    /* access modifiers changed from: private */
    public IPathModifierListener mPathModifierListener;
    private final SequenceModifier<IShape> mSequenceModifier;

    public interface IPathModifierListener {
        void onWaypointPassed(PathModifier pathModifier, IShape iShape, int i);
    }

    public PathModifier(float pDuration, Path pPath) {
        this(pDuration, pPath, (IShapeModifier.IShapeModifierListener) null, IEaseFunction.DEFAULT);
    }

    public PathModifier(float pDuration, Path pPath, IEaseFunction pEaseFunction) {
        this(pDuration, pPath, (IShapeModifier.IShapeModifierListener) null, pEaseFunction);
    }

    public PathModifier(float pDuration, Path pPath, IShapeModifier.IShapeModifierListener pShapeModifierListener) {
        this(pDuration, pPath, pShapeModifierListener, null, IEaseFunction.DEFAULT);
    }

    public PathModifier(float pDuration, Path pPath, IShapeModifier.IShapeModifierListener pShapeModifierListener, IEaseFunction pEaseFunction) {
        this(pDuration, pPath, pShapeModifierListener, null, pEaseFunction);
    }

    public PathModifier(float pDuration, Path pPath, IShapeModifier.IShapeModifierListener pShapeModifierListener, IPathModifierListener pPathModifierListener) throws IllegalArgumentException {
        this(pDuration, pPath, pShapeModifierListener, pPathModifierListener, IEaseFunction.DEFAULT);
    }

    public PathModifier(float pDuration, Path pPath, IShapeModifier.IShapeModifierListener pShapeModifierListener, IPathModifierListener pPathModifierListener, IEaseFunction pEaseFunction) throws IllegalArgumentException {
        int pathSize = pPath.getSize();
        if (pathSize < 2) {
            throw new IllegalArgumentException("Path needs at least 2 waypoints!");
        }
        this.mPath = pPath;
        this.mModifierListener = pShapeModifierListener;
        this.mPathModifierListener = pPathModifierListener;
        MoveModifier[] moveModifiers = new MoveModifier[(pathSize - 1)];
        float[] coordinatesX = pPath.getCoordinatesX();
        float[] coordinatesY = pPath.getCoordinatesY();
        float velocity = pPath.getLength() / pDuration;
        int modifierCount = moveModifiers.length;
        for (int i = 0; i < modifierCount; i++) {
            float duration = pPath.getSegmentLength(i) / velocity;
            if (i == 0) {
                moveModifiers[i] = new MoveModifier(duration, coordinatesX[i], coordinatesX[i + 1], coordinatesY[i], coordinatesY[i + 1], null, pEaseFunction) {
                    /* access modifiers changed from: protected */
                    public void onManagedInitialize(IShape pShape) {
                        super.onManagedInitialize((Object) pShape);
                        if (PathModifier.this.mPathModifierListener != null) {
                            PathModifier.this.mPathModifierListener.onWaypointPassed(PathModifier.this, pShape, 0);
                        }
                    }
                };
            } else {
                moveModifiers[i] = new MoveModifier(duration, coordinatesX[i], coordinatesX[i + 1], coordinatesY[i], coordinatesY[i + 1], null, pEaseFunction);
            }
        }
        final int i2 = modifierCount;
        this.mSequenceModifier = new SequenceModifier<>(new IShapeModifier.IShapeModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier iModifier, Object obj) {
                onModifierFinished((IModifier<IShape>) iModifier, (IShape) obj);
            }

            public void onModifierFinished(IModifier<IShape> iModifier, IShape pShape) {
                if (PathModifier.this.mPathModifierListener != null) {
                    PathModifier.this.mPathModifierListener.onWaypointPassed(PathModifier.this, pShape, i2);
                }
                if (PathModifier.this.mModifierListener != null) {
                    PathModifier.this.mModifierListener.onModifierFinished(PathModifier.this, pShape);
                }
            }
        }, new SequenceModifier.ISubSequenceModifierListener<IShape>() {
            public /* bridge */ /* synthetic */ void onSubSequenceFinished(IModifier iModifier, Object obj, int i) {
                onSubSequenceFinished((IModifier<IShape>) iModifier, (IShape) obj, i);
            }

            public void onSubSequenceFinished(IModifier<IShape> iModifier, IShape pShape, int pIndex) {
                if (PathModifier.this.mPathModifierListener != null) {
                    PathModifier.this.mPathModifierListener.onWaypointPassed(PathModifier.this, pShape, pIndex);
                }
            }
        }, moveModifiers);
    }

    protected PathModifier(PathModifier pPathModifier) {
        this.mPath = pPathModifier.mPath.clone();
        this.mSequenceModifier = pPathModifier.mSequenceModifier.clone();
    }

    public PathModifier clone() {
        return new PathModifier(this);
    }

    public Path getPath() {
        return this.mPath;
    }

    public boolean isFinished() {
        return this.mSequenceModifier.isFinished();
    }

    public float getDuration() {
        return this.mSequenceModifier.getDuration();
    }

    public IPathModifierListener getPathModifierListener() {
        return this.mPathModifierListener;
    }

    public void setPathModifierListener(IPathModifierListener pPathModifierListener) {
        this.mPathModifierListener = pPathModifierListener;
    }

    public void reset() {
        this.mSequenceModifier.reset();
    }

    public void onUpdate(float pSecondsElapsed, IShape pShape) {
        this.mSequenceModifier.onUpdate(pSecondsElapsed, pShape);
    }
}
