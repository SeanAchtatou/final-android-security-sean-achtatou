package org.anddev.andengine.entity.scene.transition;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.shape.modifier.AlphaModifier;
import org.anddev.andengine.entity.shape.modifier.SequenceShapeModifier;

public class FadeTransitionScene extends BaseTransitionScene {
    private final Rectangle mFadeRectangle = new Rectangle(0.0f, 0.0f, 100.0f, 100.0f);

    public FadeTransitionScene(float pDuration, Scene pOldScene, Scene pNewScene, float pRed, float pGreen, float pBlue) {
        super(1, pDuration, pOldScene, pNewScene);
        setBackgroundEnabled(false);
        float durationHalf = pDuration * 0.5f;
        this.mFadeRectangle.setColor(pRed, pGreen, pBlue);
        this.mFadeRectangle.addShapeModifier(new SequenceShapeModifier(new AlphaModifier(durationHalf, 0.0f, 1.0f), new AlphaModifier(durationHalf, 1.0f, 0.0f)));
        getBottomLayer().addEntity(this.mFadeRectangle);
        this.mNewScene.setVisible(false);
        registerUpdateHandler(new TimerHandler(durationHalf, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                FadeTransitionScene.this.mOldScene.setVisible(false);
                FadeTransitionScene.this.mNewScene.setVisible(true);
            }
        }));
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 pGL, Camera pCamera) {
        this.mOldScene.onDraw(pGL, pCamera);
        this.mNewScene.onDraw(pGL, pCamera);
        super.onManagedDraw(pGL, pCamera);
    }
}
