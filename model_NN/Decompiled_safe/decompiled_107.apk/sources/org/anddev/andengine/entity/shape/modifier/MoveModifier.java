package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class MoveModifier extends DoubleValueSpanShapeModifier {
    public MoveModifier(float pDuration, float pFromX, float pToX, float pFromY, float pToY) {
        this(pDuration, pFromX, pToX, pFromY, pToY, null, IEaseFunction.DEFAULT);
    }

    public MoveModifier(float pDuration, float pFromX, float pToX, float pFromY, float pToY, IEaseFunction pEaseFunction) {
        this(pDuration, pFromX, pToX, pFromY, pToY, null, pEaseFunction);
    }

    public MoveModifier(float pDuration, float pFromX, float pToX, float pFromY, float pToY, IShapeModifier.IShapeModifierListener pShapeModifierListener) {
        super(pDuration, pFromX, pToX, pFromY, pToY, pShapeModifierListener, IEaseFunction.DEFAULT);
    }

    public MoveModifier(float pDuration, float pFromX, float pToX, float pFromY, float pToY, IShapeModifier.IShapeModifierListener pShapeModifierListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromX, pToX, pFromY, pToY, pShapeModifierListener, pEaseFunction);
    }

    protected MoveModifier(MoveModifier pMoveModifier) {
        super(pMoveModifier);
    }

    public MoveModifier clone() {
        return new MoveModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValues(IShape pShape, float pX, float pY) {
        pShape.setPosition(pX, pY);
    }

    /* access modifiers changed from: protected */
    public void onSetValues(IShape pShape, float pPercentageDone, float pX, float pY) {
        pShape.setPosition(pX, pY);
    }
}
