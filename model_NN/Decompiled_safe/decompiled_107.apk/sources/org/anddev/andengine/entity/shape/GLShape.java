package org.anddev.andengine.entity.shape;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.opengl.vertex.VertexBuffer;

public abstract class GLShape extends Shape {
    /* access modifiers changed from: protected */
    public abstract VertexBuffer getVertexBuffer();

    /* access modifiers changed from: protected */
    public abstract void onUpdateVertexBuffer();

    public GLShape(float pX, float pY) {
        super(pX, pY);
    }

    /* access modifiers changed from: protected */
    public void onApplyVertices(GL10 pGL) {
        if (GLHelper.EXTENSIONS_VERTEXBUFFEROBJECTS) {
            GL11 gl11 = (GL11) pGL;
            getVertexBuffer().selectOnHardware(gl11);
            GLHelper.vertexZeroPointer(gl11);
            return;
        }
        GLHelper.vertexPointer(pGL, getVertexBuffer().getFloatBuffer());
    }

    /* access modifiers changed from: protected */
    public void updateVertexBuffer() {
        onUpdateVertexBuffer();
    }
}
