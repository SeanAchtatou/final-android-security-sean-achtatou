package org.anddev.andengine.opengl.texture.source.decorator;

import android.graphics.Paint;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import org.anddev.andengine.opengl.texture.source.decorator.BaseShapeTextureSourceDecorator;

public class FillTextureSourceDecorator extends BaseShapeTextureSourceDecorator {
    protected final int mFillColor;

    public FillTextureSourceDecorator(ITextureSource pTextureSource, int pFillColor) {
        this(pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape.RECTANGLE, pFillColor);
    }

    public FillTextureSourceDecorator(ITextureSource pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape pTextureSourceDecoratorShape, int pFillColor) {
        this(pTextureSource, pTextureSourceDecoratorShape, pFillColor, false);
    }

    public FillTextureSourceDecorator(ITextureSource pTextureSource, int pFillColor, boolean pAntiAliasing) {
        this(pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape.RECTANGLE, pFillColor, pAntiAliasing);
    }

    public FillTextureSourceDecorator(ITextureSource pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape pTextureSourceDecoratorShape, int pFillColor, boolean pAntiAliasing) {
        super(pTextureSource, pTextureSourceDecoratorShape, pAntiAliasing);
        this.mFillColor = pFillColor;
        this.mPaint.setStyle(Paint.Style.FILL);
        this.mPaint.setColor(pFillColor);
    }

    public FillTextureSourceDecorator clone() {
        return new FillTextureSourceDecorator(this.mTextureSource, this.mTextureSourceDecoratorShape, this.mFillColor, this.mAntiAliasing);
    }
}
