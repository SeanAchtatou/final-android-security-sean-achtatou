package org.anddev.andengine.opengl.texture.source.decorator;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import org.anddev.andengine.opengl.texture.source.ITextureSource;

public abstract class BaseTextureSourceDecorator implements ITextureSource {
    protected final boolean mAntiAliasing;
    protected final Paint mPaint;
    protected final ITextureSource mTextureSource;

    public abstract BaseTextureSourceDecorator clone();

    /* access modifiers changed from: protected */
    public abstract void onDecorateBitmap(Canvas canvas);

    public BaseTextureSourceDecorator(ITextureSource pTextureSource) {
        this(pTextureSource, false);
    }

    public BaseTextureSourceDecorator(ITextureSource pTextureSource, boolean pAntiAliasing) {
        this.mPaint = new Paint();
        this.mTextureSource = pTextureSource;
        this.mAntiAliasing = pAntiAliasing;
        this.mPaint.setAntiAlias(pAntiAliasing);
    }

    public int getWidth() {
        return this.mTextureSource.getWidth();
    }

    public int getHeight() {
        return this.mTextureSource.getHeight();
    }

    public Bitmap onLoadBitmap() {
        Bitmap bitmap = ensureLoadedBitmapIsMutable(this.mTextureSource.onLoadBitmap());
        onDecorateBitmap(new Canvas(bitmap));
        return bitmap;
    }

    private Bitmap ensureLoadedBitmapIsMutable(Bitmap pBitmap) {
        if (pBitmap.isMutable()) {
            return pBitmap;
        }
        Bitmap mutableBitmap = pBitmap.copy(pBitmap.getConfig(), true);
        pBitmap.recycle();
        return mutableBitmap;
    }
}
