package org.anddev.andengine.opengl.view;

import javax.microedition.khronos.opengles.GL;

public interface GLWrapper {
    GL wrap(GL gl);
}
