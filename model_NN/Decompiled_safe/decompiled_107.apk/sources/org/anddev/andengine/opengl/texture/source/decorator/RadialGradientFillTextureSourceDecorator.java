package org.anddev.andengine.opengl.texture.source.decorator;

import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import org.anddev.andengine.opengl.texture.source.decorator.BaseShapeTextureSourceDecorator;

public class RadialGradientFillTextureSourceDecorator extends BaseShapeTextureSourceDecorator {
    private static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$opengl$texture$source$decorator$RadialGradientFillTextureSourceDecorator$RadialGradientDirection;
    protected final int mFromColor;
    protected final RadialGradientDirection mRadialGradientDirection;
    protected final int mToColor;

    public enum RadialGradientDirection {
        INSIDE_OUT,
        OUTSIDE_IN
    }

    static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$opengl$texture$source$decorator$RadialGradientFillTextureSourceDecorator$RadialGradientDirection() {
        int[] iArr = $SWITCH_TABLE$org$anddev$andengine$opengl$texture$source$decorator$RadialGradientFillTextureSourceDecorator$RadialGradientDirection;
        if (iArr == null) {
            iArr = new int[RadialGradientDirection.values().length];
            try {
                iArr[RadialGradientDirection.INSIDE_OUT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[RadialGradientDirection.OUTSIDE_IN.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$org$anddev$andengine$opengl$texture$source$decorator$RadialGradientFillTextureSourceDecorator$RadialGradientDirection = iArr;
        }
        return iArr;
    }

    public RadialGradientFillTextureSourceDecorator(ITextureSource pTextureSource, int pFromColor, int pToColor, RadialGradientDirection pRadialGradientDirection) {
        this(pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape.RECTANGLE, pFromColor, pToColor, pRadialGradientDirection);
    }

    public RadialGradientFillTextureSourceDecorator(ITextureSource pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape pTextureSourceDecoratorShape, int pFromColor, int pToColor, RadialGradientDirection pRadialGradientDirection) {
        this(pTextureSource, pTextureSourceDecoratorShape, pFromColor, pToColor, pRadialGradientDirection, false);
    }

    public RadialGradientFillTextureSourceDecorator(ITextureSource pTextureSource, int pFromColor, int pToColor, RadialGradientDirection pRadialGradientDirection, boolean pAntiAliasing) {
        this(pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape.RECTANGLE, pFromColor, pToColor, pRadialGradientDirection, pAntiAliasing);
    }

    public RadialGradientFillTextureSourceDecorator(ITextureSource pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape pTextureSourceDecoratorShape, int pFromColor, int pToColor, RadialGradientDirection pRadialGradientDirection, boolean pAntiAliasing) {
        super(pTextureSource, pTextureSourceDecoratorShape, pAntiAliasing);
        this.mFromColor = pFromColor;
        this.mToColor = pToColor;
        this.mRadialGradientDirection = pRadialGradientDirection;
        this.mPaint.setStyle(Paint.Style.FILL);
        float centerX = 0.5f * ((float) pTextureSource.getWidth());
        float centerY = 0.5f * ((float) pTextureSource.getHeight());
        float radius = Math.max(centerX, centerY);
        switch ($SWITCH_TABLE$org$anddev$andengine$opengl$texture$source$decorator$RadialGradientFillTextureSourceDecorator$RadialGradientDirection()[pRadialGradientDirection.ordinal()]) {
            case 1:
                this.mPaint.setShader(new RadialGradient(centerX, centerY, radius, pFromColor, pToColor, Shader.TileMode.CLAMP));
                return;
            case 2:
                this.mPaint.setShader(new RadialGradient(centerX, centerY, radius, pToColor, pFromColor, Shader.TileMode.CLAMP));
                return;
            default:
                return;
        }
    }

    public RadialGradientFillTextureSourceDecorator clone() {
        return new RadialGradientFillTextureSourceDecorator(this.mTextureSource, this.mTextureSourceDecoratorShape, this.mFromColor, this.mToColor, this.mRadialGradientDirection, this.mAntiAliasing);
    }
}
