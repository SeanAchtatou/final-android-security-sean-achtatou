package org.anddev.andengine.opengl.font;

import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;

public class FontManager {
    private final ArrayList<Font> mFontsManaged = new ArrayList<>();

    public void clear() {
        this.mFontsManaged.clear();
    }

    public void loadFont(Font pFont) {
        this.mFontsManaged.add(pFont);
    }

    public void loadFonts(FontLibrary pFontLibrary) {
        pFontLibrary.loadFonts(this);
    }

    public void loadFonts(Font... pFonts) {
        for (int i = pFonts.length - 1; i >= 0; i--) {
            loadFont(pFonts[i]);
        }
    }

    public void updateFonts(GL10 pGL) {
        ArrayList<Font> fonts = this.mFontsManaged;
        int fontCount = fonts.size();
        if (fontCount > 0) {
            for (int i = fontCount - 1; i >= 0; i--) {
                fonts.get(i).update(pGL);
            }
        }
    }

    public void reloadFonts() {
        ArrayList<Font> managedFonts = this.mFontsManaged;
        for (int i = managedFonts.size() - 1; i >= 0; i--) {
            managedFonts.get(i).reload();
        }
    }
}
