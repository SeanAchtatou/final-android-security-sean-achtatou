package org.anddev.progressmonitor;

public interface IProgressListener {
    void onProgressChanged(int i);
}
