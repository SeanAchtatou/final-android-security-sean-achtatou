package com.benchbee.AST;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class resultBoxView extends View {
    resultItem resultItem = new resultItem();
    String signal_strength;
    float start_pos_x = check.r.getDimension(R.dimen.result_str_x);
    float start_pos_y = check.r.getDimension(R.dimen.result_str_y);
    float step_y = check.r.getDimension(R.dimen.result_str_step);
    Paint text_paint = new Paint(1);
    float text_result_str = check.r.getDimension(R.dimen.text_result_str);

    public resultBoxView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.text_paint.setTextSize(this.text_result_str);
        this.text_paint.setTextAlign(Paint.Align.RIGHT);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float pos_x = this.start_pos_x;
        float pos_y = this.start_pos_y;
        for (int idx = 2; idx < this.resultItem.results_cnt; idx++) {
            if (this.resultItem.results[idx] != null) {
                canvas.drawText(this.resultItem.results[idx], pos_x, pos_y, this.text_paint);
            }
            pos_y += this.step_y;
        }
    }
}
