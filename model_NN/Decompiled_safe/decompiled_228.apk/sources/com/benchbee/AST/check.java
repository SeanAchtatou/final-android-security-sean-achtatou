package com.benchbee.AST;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import java.util.Timer;
import java.util.TimerTask;

public class check extends Activity {
    static final int CHECK = 0;
    static final int DOWNLOAD = 1;
    static final int END = 10;
    static final int FAIL = 0;
    static final int PING = 3;
    static final int SUCCESS = 1;
    static int TotaleventCnt = 0;
    public static final String UPDATE_SET_ACTION = "com.benchbee.AST.SET";
    static final int UPLOAD = 2;
    public static final String USER_PREFERENCE = "USER_PREFERENCES";
    static SharedPreferences admin;
    static ToggleButton[] button_chart = new ToggleButton[totalButtonCnt];
    static Button button_start;
    static int chartDigitNum = 1;
    static int chartViewNum = UPLOAD;
    static boolean check_Mode = false;
    static controller control;
    public static RelativeLayout digitBack;
    static float digitHeigh;
    public static LinearLayout digitLayout;
    static ToggleButton digitMode_back;
    static ImageView digitMode_front;
    static ToggleButton digitUnit;
    static float digitWidth;
    static int eventCnts;
    static boolean for_ad = true;
    static int gaugeViewNum = 0;
    static gpsAdapter gps;
    static View[] graph = new View[totalGraphCnt];
    static boolean isMobile = false;
    static ToggleButton menu_about;
    static ToggleButton menu_result;
    static ToggleButton menu_setting;
    static ToggleButton menu_statistics;
    public static TextView myLocationText;
    static boolean network_changed = false;
    static int network_changed_reason = 0;
    static int[] order = new int[PING];
    static ProgressDialog pd;
    static boolean pref_digit;
    static boolean pref_unit;
    static SharedPreferences prefs;
    public static Resources r;
    static int[] resID = {R.drawable.chart_back_01, R.drawable.chart_back_02, R.drawable.chart_back_03, R.drawable.chart_back_10, R.drawable.chart_back_20, R.drawable.chart_back_30};
    static boolean restartLock = false;
    public static float setUnit = 1000000.0f;
    static int startCnt;
    static boolean start_flag = false;
    static int totalButtonCnt = 6;
    static int totalGraphCnt = PING;
    static String warning;
    static WebView webView;
    int Test_NetworkType = -1;
    String ad_host;
    private View.OnClickListener check_listener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.dnPointButton:
                case R.id.dnChartButton:
                    check.control.clickButtons(0, false);
                    return;
                case R.id.upPointButton:
                case R.id.upChartButton:
                    check.control.clickButtons(1, false);
                    return;
                case R.id.pingPointButton:
                case R.id.pingChartButton:
                    check.control.clickButtons(check.UPLOAD, false);
                    return;
                case R.id.myLocationText:
                case R.id.gaugeView:
                default:
                    return;
                case R.id.start:
                    if (!check.start_flag) {
                        check.start_flag = true;
                        check.this.checkingServer();
                        return;
                    } else if (!check.control.end_flag) {
                        new AlertDialog.Builder(check.this).setTitle("중지").setMessage("\n측정을 중지 하시겠습니까?").setPositiveButton("확인", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                check.control.stop();
                            }
                        }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
                        return;
                    } else if (check.control.end_flag && !check.restartLock) {
                        check.restartLock = true;
                        libFuncs.toastShow(check.this, "잠시 후 재 측정을 시작 합니다.");
                        check.this.msghandler.postDelayed(new Runnable() {
                            public void run() {
                                check.this.reStart();
                            }
                        }, 1500);
                        return;
                    } else {
                        return;
                    }
            }
        }
    };
    Button check_push_button;
    private View.OnClickListener check_push_listener = new View.OnClickListener() {
        public void onClick(View v) {
            if (check.startCnt == 0) {
                check.gps = new gpsAdapter(check.this, check.control);
                check.gps.startGPS();
                check.this.pushLayout.setVisibility(8);
            }
            check.button_start.performClick();
        }
    };
    check context;
    int cur_patch_code;
    Intent intent;
    Handler msghandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    check.pd.dismiss();
                    if (msg.arg1 != 1) {
                        check.start_flag = false;
                        libFuncs.toastShow(check.this, check.control.strError);
                        return;
                    } else if (check.isMobile) {
                        check.this.modeWaning();
                        return;
                    } else {
                        check.start();
                        return;
                    }
                case 1:
                case check.UPLOAD /*2*/:
                case check.PING /*3*/:
                    if (!check.control.sInterupted) {
                        if (msg.arg1 == 1) {
                            if (check.for_ad) {
                                check.control.buttons.get(msg.what - 1).setChecked(true);
                            }
                            check.control.showAnimation(true, msg.what, false);
                            ((gaugeView) check.graph[check.gaugeViewNum]).gaugeDown();
                            check.this.msghandler.postDelayed(new Runnable() {
                                public void run() {
                                    int i = check.eventCnts - 1;
                                    check.eventCnts = i;
                                    if (i > 0) {
                                        check.control.run(check.order[check.eventCnts - 1]);
                                    } else {
                                        check.this.msghandler.sendMessage(Message.obtain(check.this.msghandler, (int) check.END));
                                    }
                                }
                            }, (long) check.control.wating_time);
                        } else {
                            check.control.stop();
                        }
                    }
                    float numeric = ((float) msg.arg2) / 1000000.0f;
                    if (numeric < 10.0f) {
                        check.control.setButtonText(msg.what - 1, String.format("%.2f", Float.valueOf(numeric)));
                        return;
                    }
                    check.control.setButtonText(msg.what - 1, String.format("%.1f", Float.valueOf(numeric)));
                    return;
                case 4:
                    check.pd = ProgressDialog.show(check.this, "", "핑 테스트 준비 중입니다.\n단말기에 따라 다소 시간이 소요 됩니다.", true, true);
                    return;
                case 5:
                    if (check.pd != null) {
                        check.pd.dismiss();
                        return;
                    }
                    return;
                case 6:
                case 7:
                case controller.GPS_MAX /*8*/:
                case 9:
                default:
                    return;
                case check.END /*10*/:
                    if (!check.control.sInterupted) {
                        if (!check.this.CompareCurrentNetworkType()) {
                            check.network_changed = true;
                        }
                        check.control.endEvent();
                        check.control.runExtra();
                        return;
                    }
                    return;
            }
        }
    };
    private NetworkChangeReceiver myNetworkChangeReceiver = new NetworkChangeReceiver();
    int pre_patch_code;
    RelativeLayout pushLayout;
    private final BroadcastReceiver updateSetReceiver = new BroadcastReceiver() {
        /* Debug info: failed to restart local var, previous not found, register: 2 */
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("pref_Mbps", true)) {
                check.setUnit = 1000000.0f;
                check.pref_unit = true;
            } else {
                check.setUnit = 8000000.0f;
                check.pref_unit = false;
            }
            if (check.start_flag) {
                ((chartDigit) check.graph[check.chartDigitNum]).changeUnit();
            }
            ((gaugeView) check.graph[check.gaugeViewNum]).setGaugeMode(check.prefs);
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.check);
        r = getResources();
        control = new controller(this);
        getConfig();
        setDisplay();
        setConfig();
        setAd();
        registerReceiver(this.myNetworkChangeReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    public void setDisplay() {
        myLocationText = (TextView) findViewById(R.id.myLocationText);
        button_chart[0] = (ToggleButton) findViewById(R.id.dnChartButton);
        button_chart[1] = (ToggleButton) findViewById(R.id.upChartButton);
        button_chart[UPLOAD] = (ToggleButton) findViewById(R.id.pingChartButton);
        button_chart[PING] = (ToggleButton) findViewById(R.id.dnPointButton);
        button_chart[4] = (ToggleButton) findViewById(R.id.upPointButton);
        button_chart[5] = (ToggleButton) findViewById(R.id.pingPointButton);
        for (int i = 0; i < totalButtonCnt; i++) {
            button_chart[i].setOnClickListener(this.check_listener);
            control.addButton(button_chart[i]);
        }
        button_start = (Button) findViewById(R.id.start);
        button_start.setOnClickListener(this.check_listener);
        graph[gaugeViewNum] = (gaugeView) findViewById(R.id.gaugeView);
        graph[chartDigitNum] = (chartDigit) findViewById(R.id.ChartDigit);
        graph[chartViewNum] = (chartView) findViewById(R.id.ChartView);
        digitLayout = (LinearLayout) findViewById(R.id.ChartDigitLayout);
        digitBack = (RelativeLayout) findViewById(R.id.ChartBack);
        control.setTouchEvent();
        for (int i2 = 0; i2 < totalGraphCnt; i2++) {
            control.addEventListener((eventListener) graph[i2]);
        }
        menu_result = (ToggleButton) findViewById(R.id.menu_result);
        menu_setting = (ToggleButton) findViewById(R.id.menu_setting);
        menu_about = (ToggleButton) findViewById(R.id.menu_about);
        menu_statistics = (ToggleButton) findViewById(R.id.menu_statistics);
        digitMode_front = (ImageView) findViewById(R.id.digitMode_front);
        digitMode_back = (ToggleButton) findViewById(R.id.digitMode_back);
        digitUnit = (ToggleButton) findViewById(R.id.digitUnit);
        if (control.mac.length() < 1) {
            queryNetInfo();
        }
        check_Mode = checkMode();
        this.check_push_button = (Button) findViewById(R.id.check_push);
        ((ImageView) findViewById(R.id.push_ani)).setVisibility(8);
        if (!isMobile) {
            ((ImageView) findViewById(R.id.push_ani_3g)).setVisibility(8);
            ((ImageView) findViewById(R.id.push_ani_wifi)).startAnimation(AnimationUtils.loadAnimation(this, R.anim.ani_push));
        } else {
            ((ImageView) findViewById(R.id.push_ani_wifi)).setVisibility(8);
            ((ImageView) findViewById(R.id.push_ani_3g)).startAnimation(AnimationUtils.loadAnimation(this, R.anim.ani_push));
        }
        this.check_push_button.setOnClickListener(this.check_push_listener);
        sortingOrder();
    }

    public void getConfig() {
        prefs = getSharedPreferences("USER_PREFERENCES", 0);
        admin = PreferenceManager.getDefaultSharedPreferences(this);
        pref_digit = prefs.getBoolean("pref_digit", true);
        pref_unit = prefs.getBoolean("pref_Mbps", true);
        this.pre_patch_code = prefs.getInt("patch_info_code", 0);
        this.cur_patch_code = Integer.parseInt(r.getString(R.string.app_code));
        if (this.pre_patch_code < this.cur_patch_code) {
            Intent patch = new Intent(this, patchInfo.class);
            patch.addFlags(1073741824);
            startActivity(patch);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("patch_info_code", this.cur_patch_code);
            editor.commit();
        }
        control.mac = prefs.getString("mac", "");
        control.host = r.getString(R.string.host);
        control.timeout = Integer.parseInt(r.getString(R.string.timeout));
        control.longtimeout = Integer.parseInt(r.getString(R.string.longtimeout));
        control.wating_time = Integer.parseInt(r.getString(R.string.break_time));
        control.port_tcp = Integer.parseInt(r.getString(R.string.port_tcp));
        control.test_period = Integer.parseInt(admin.getString("list_period", "15000"));
        control.event_period = Integer.parseInt(r.getString(R.string.event_period_time));
        control.blink_period = Integer.parseInt(r.getString(R.string.blink_period_time));
        control.port_udp = Integer.parseInt(r.getString(R.string.port_udp));
        control.maxPingCnt = Integer.parseInt(admin.getString("list_pingcnt", "100"));
        control.limitRTT = Integer.parseInt(r.getString(R.string.limitRTT));
        control.minRTT = control.limitRTT;
        control.acceptRTT = Integer.parseInt(r.getString(R.string.acceptRTT));
        control.remoteDB_host = r.getString(R.string.remoteDB);
        control.move_senstive = Integer.parseInt(r.getString(R.string.touch_sens));
        control.eventCnt = control.test_period / control.event_period;
        control.app_ver = r.getString(R.string.app_ver);
        control.buttonTextOnColor = r.getColor(R.color.buttonTextOn);
        control.buttonTextOffColor = r.getColor(R.color.buttonTextOff);
        this.ad_host = r.getString(R.string.ad_host);
        this.pushLayout = (RelativeLayout) findViewById(R.id.pushLayout);
        warning = r.getString(R.string.warning);
    }

    private static void sortingOrder() {
        TotaleventCnt = 0;
        control.dn_num = UPLOAD;
        if (control.dn_num <= 0 || control.dn_num >= 4) {
            order[0] = -1;
        } else {
            order[0] = control.dn_num;
            TotaleventCnt++;
        }
        control.up_num = PING;
        if (control.up_num <= 0 || control.up_num >= 4) {
            order[1] = -1;
        } else {
            order[1] = control.up_num;
            TotaleventCnt++;
        }
        control.ping_num = 1;
        if (control.ping_num <= 0 || control.ping_num >= 4) {
            order[UPLOAD] = -1;
        } else {
            order[UPLOAD] = control.ping_num;
            TotaleventCnt++;
        }
        int[] temp = new int[PING];
        int cnt = TotaleventCnt;
        for (int i = 1; i < 4; i++) {
            for (int j = 0; j < PING; j++) {
                if (order[j] == i) {
                    temp[cnt - 1] = j + 1;
                    cnt--;
                }
            }
        }
        for (int i2 = 0; i2 < PING; i2++) {
            order[i2] = temp[i2];
        }
        eventCnts = TotaleventCnt;
        setBackground();
    }

    public static void setBackground() {
        if (TotaleventCnt != 0) {
            digitBack.setBackgroundResource(resID[order[TotaleventCnt - 1] - 1]);
        } else if (start_flag) {
            control.stop();
        }
    }

    public void setConfig() {
        if (!pref_unit) {
            setUnit = 8000000.0f;
        }
        if (!pref_digit) {
            graph[chartViewNum].setVisibility(0);
            digitLayout.setVisibility(8);
        }
        registerReceiver(this.updateSetReceiver, new IntentFilter(UPDATE_SET_ACTION));
        PreferenceManager.setDefaultValues(this, R.xml.admin, false);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        gps.endEvent();
        unregisterReceiver(this.updateSetReceiver);
        releaseBitmap();
    }

    class startAniTask extends TimerTask {
        startAniTask() {
        }

        public void run() {
            ((AnimationDrawable) check.digitMode_front.getBackground()).start();
        }
    }

    public void checkingServer() {
        pd = ProgressDialog.show(this, "", "NOW LOADING", true, true);
        if (checkMode()) {
            new Thread(new Runnable() {
                public void run() {
                    Log.d("[Benchbee.AST]", String.format("chkdate =%s", check.control.setTime()));
                    if (!check.control.getServerHost() || !check.control.connCheck()) {
                        check.this.msghandler.sendMessage(Message.obtain(check.this.msghandler, 0, 0, 0));
                    } else {
                        check.this.msghandler.sendMessage(Message.obtain(check.this.msghandler, 0, 1, 0));
                    }
                }
            }).start();
        } else {
            this.msghandler.sendMessage(Message.obtain(this.msghandler, 0, 0, 0));
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    public void clean() {
        this.Test_NetworkType = -1;
        network_changed = false;
        network_changed_reason = 0;
        control.end_flag = false;
        control.eventStart = false;
        control.sInterupted = false;
        control.onTouch = false;
        control.maxRTT = 0;
        control.minRTT = control.limitRTT;
        control.rttCnt = 0;
        control.upCnt = 0;
        control.downCnt = 0;
        control.setGpsCnt = 0;
        control.setNetworkGpsCnt = 0;
        control.sum_distance_result = 0.0f;
        for (int i = 0; i < 8; i++) {
            control.latitude[i] = -1.0d;
            control.longitude[i] = -1.0d;
            control.networklatitude[i] = -1.0d;
            control.networklongitude[i] = -1.0d;
        }
        control.sido = "";
        control.gugun = "";
        control.dong = "";
        control.m_down_speed_stdev = 0.0d;
        control.m_up_speed_stdev = 0.0d;
        control.m_ping_rtt_stdev = 0.0d;
        control.m_sum_avgPingRtt = 0.0d;
        control.m_sum_avgPingRtt_stdev = 0.0d;
        control.m_sum_dnspeed = 0.0d;
        control.m_sum_dnspeed_sq = 0.0d;
        control.m_sum_upspeed = 0.0d;
        control.m_sum_upspeed_sq = 0.0d;
        control.totalRTT = 0;
        control.first_event = true;
        control.ping_end_flag = false;
        control.cur_event = 0;
        control.strError = "측정이 중지 되었습니다.";
        control.timeOutCnt = 0;
        control.blinkRateDn = 0.0f;
        control.blinkRateUp = 0.0f;
        control.totalDownloadByte = 0;
        control.totalUploadByte = 0;
        control.ssid = "";
        ((chartView) graph[chartViewNum]).clean();
        ((chartDigit) graph[chartDigitNum]).clean();
        ((gaugeView) graph[gaugeViewNum]).clean();
        control.resultItem.clean();
        control.test_period = Integer.parseInt(admin.getString("list_period", "15000"));
        control.maxPingCnt = Integer.parseInt(admin.getString("list_pingcnt", "100"));
    }

    public static void start() {
        startCnt++;
        if (for_ad) {
            control.setButtonText(0, "0.0");
            button_chart[0].setChecked(false);
            control.setButtonText(1, "0.0");
            button_chart[1].setChecked(false);
            control.setButtonText(UPLOAD, "0.0");
            button_chart[UPLOAD].setChecked(false);
        }
        getAnimationDimention();
        control.buttonStop();
        control.changeStartButton();
        sortingOrder();
        if (eventCnts > 0) {
            control.run(order[eventCnts - 1]);
        }
    }

    public static void getAnimationDimention() {
        digitWidth = ((float) digitBack.getWidth()) / 2.0f;
        digitHeigh = ((float) digitBack.getHeight()) / 2.0f;
    }

    public void reStart() {
        resetDisplay();
        clean();
        if (control.mac.length() < 1) {
            queryNetInfo();
        }
        gps.startGPS();
        checkingServer();
    }

    public void resetDisplay() {
        prefs = getSharedPreferences("USER_PREFERENCES", 0);
        pref_digit = prefs.getBoolean("pref_digit", true);
        if (!pref_digit) {
            graph[chartViewNum].setVisibility(0);
            digitLayout.setVisibility(8);
            return;
        }
        graph[chartViewNum].setVisibility(8);
        digitLayout.setVisibility(0);
    }

    public void onBackPressed() {
        if (!start_flag || control.end_flag) {
            requestKillProcess(this);
        } else {
            button_start.performClick();
        }
    }

    public void releaseBitmap() {
    }

    public static void menuClose() {
        menu_result.setClickable(false);
        menu_setting.setClickable(false);
        menu_about.setClickable(false);
        menu_statistics.setClickable(false);
    }

    public static void menuOpen() {
        menu_result.setClickable(true);
        menu_setting.setClickable(true);
        menu_about.setClickable(true);
        menu_statistics.setClickable(true);
    }

    public void queryNetInfo() {
        WifiManager wfManager = (WifiManager) getSystemService("wifi");
        boolean wifi_turn_on = false;
        if (!wfManager.isWifiEnabled()) {
            Log.d("[Benchbee.AST]", "[Ani] WIFI Enable");
            wfManager.setWifiEnabled(true);
            wifi_turn_on = true;
        }
        WifiInfo wifiInfo = wfManager.getConnectionInfo();
        control.mac = wifiInfo.getMacAddress();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("mac", control.mac);
        editor.commit();
        if (wifi_turn_on) {
            Log.d("[Benchbee.AST]", "[Ani] WIFI OFF");
            wfManager.setWifiEnabled(false);
        }
    }

    public boolean checkMode() {
        String mode = "NOT";
        ConnectivityManager cm = (ConnectivityManager) getSystemService("connectivity");
        final TelephonyManager tm = (TelephonyManager) getSystemService("phone");
        String telecomtemp = tm.getNetworkOperator();
        if (telecomtemp == null) {
            control.setTelecom(tm.getSimOperator());
        } else if (!tm.getSimCountryIso().equals("kr")) {
            control.setTelecom(tm.getSimOperator());
        } else if (telecomtemp.equals("45002") || telecomtemp.equals("45003") || telecomtemp.equals("45004") || telecomtemp.equals("45005") || telecomtemp.equals("45006") || telecomtemp.equals("45008")) {
            control.setTelecom(tm.getNetworkOperator());
        } else {
            control.setTelecom("");
        }
        if (cm.getActiveNetworkInfo() != null) {
            this.Test_NetworkType = cm.getActiveNetworkInfo().getType();
            if (this.Test_NetworkType == 0) {
                mode = "3G";
                control.setNewtworkMode(mode);
                isMobile = true;
                digitMode_back.setChecked(true);
                setAnimation(true);
                tm.listen(new PhoneStateListener() {
                    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
                        if (check.control.signalStrength == 0) {
                            if (signalStrength.isGsm()) {
                                check.control.setSignalStrength(signalStrength.getGsmSignalStrength());
                            } else {
                                check.control.setSignalStrength(signalStrength.getEvdoDbm());
                            }
                        }
                        tm.listen(this, 0);
                    }
                }, 256);
            } else if (this.Test_NetworkType == 1) {
                mode = "WIFI";
                control.setNewtworkMode(mode);
                isMobile = false;
                digitMode_back.setChecked(false);
                setAnimation(false);
                WifiInfo info = ((WifiManager) getSystemService("wifi")).getConnectionInfo();
                control.setSignalStrength(info.getRssi());
                control.setWifiNetworkType(info.getLinkSpeed());
                control.ssid = info.getSSID();
            } else if (this.Test_NetworkType == 6) {
                mode = "Wibro";
                control.setNewtworkMode(mode);
                isMobile = false;
                digitMode_back.setChecked(false);
                setAnimation(false);
                control.setMobileNetworkType();
                tm.listen(new PhoneStateListener() {
                    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
                        if (check.control.signalStrength == 0) {
                            check.control.setSignalStrength(signalStrength.getEvdoDbm());
                        }
                        tm.listen(this, 0);
                    }
                }, 256);
                if (check_Mode) {
                    Toast.makeText(this, String.valueOf(mode) + " 모드로 측정을 시작 합니다", 0).show();
                }
            }
        }
        if (!mode.equals("NOT")) {
            return true;
        }
        control.strError = "올바른 네트워크를 찾을 수 없습니다.";
        return false;
    }

    public boolean CompareCurrentNetworkType() {
        boolean retVal;
        ConnectivityManager cm = (ConnectivityManager) getSystemService("connectivity");
        if (cm.getActiveNetworkInfo() != null) {
            NetworkInfo niWifi = cm.getNetworkInfo(1);
            NetworkInfo ni3g = cm.getNetworkInfo(0);
            if (!niWifi.isConnected() || !ni3g.isConnected()) {
                int networkType = cm.getActiveNetworkInfo().getType();
                if (this.Test_NetworkType != networkType) {
                    retVal = false;
                    if (this.Test_NetworkType == 0) {
                        if (networkType == 6) {
                            network_changed_reason = 4;
                        } else {
                            network_changed_reason = 1;
                        }
                    } else if (this.Test_NetworkType == 1) {
                        if (networkType == 6) {
                            network_changed_reason = 6;
                        } else {
                            network_changed_reason = UPLOAD;
                        }
                    } else if (this.Test_NetworkType == 6) {
                        if (networkType == 1) {
                            network_changed_reason = 7;
                        } else {
                            network_changed_reason = 5;
                        }
                    }
                } else if (this.Test_NetworkType != 1) {
                    retVal = true;
                } else {
                    WifiInfo info = ((WifiManager) getSystemService("wifi")).getConnectionInfo();
                    Object[] objArr = new Object[UPLOAD];
                    objArr[0] = control.ssid;
                    objArr[1] = info.getSSID();
                    Log.d("[Benchbee.AST]", String.format("[Ani] control_ssid = %s, info_ssid = %s", objArr));
                    if (control.ssid.equals(info.getSSID())) {
                        retVal = true;
                    } else {
                        retVal = false;
                        control.ssid = info.getSSID();
                        network_changed_reason = PING;
                    }
                }
            } else {
                Log.d("[Benchbee.AST]", String.format("[Ani] WIFI & 3g Connected", new Object[0]));
                if (this.Test_NetworkType == 0) {
                    network_changed_reason = 1;
                } else {
                    network_changed_reason = UPLOAD;
                }
                retVal = false;
            }
        } else {
            Log.d("[Benchbee.AST]", "[Ani] NetworkInfo NULL");
            retVal = true;
        }
        if (!retVal) {
            Log.d("[Benchbee.AST]", String.format("[Ani] Network_Changed reason = %d", Integer.valueOf(network_changed_reason)));
        }
        return retVal;
    }

    public void setAnimation(boolean isMobile2) {
        if (isMobile2) {
            digitMode_front.setBackgroundResource(R.anim.ani_3g);
        } else {
            digitMode_front.setBackgroundResource(R.anim.ani_wifi);
        }
        new Timer(false).schedule(new startAniTask(), 100);
    }

    public void modeWaning() {
        if (!prefs.getBoolean("3GAllow", true)) {
            new AlertDialog.Builder(this).setTitle("3G Connection").setMessage("3G 접속이 차단되었습니다.").setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (check.startCnt == 0) {
                        check.start_flag = false;
                    } else {
                        check.restartLock = false;
                    }
                    check.control.end_flag = true;
                }
            }).show();
        } else {
            new AlertDialog.Builder(this).setTitle("< 3G 접속 알림 >").setMessage(warning).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    check.start();
                }
            }).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (check.startCnt == 0) {
                        check.start_flag = false;
                    } else {
                        check.restartLock = false;
                    }
                    check.control.end_flag = true;
                }
            }).show();
        }
    }

    public void requestKillProcess(Context context2) {
        if (Integer.parseInt(Build.VERSION.SDK) < 8) {
            ((ActivityManager) context2.getSystemService("activity")).restartPackage(getPackageName());
        } else {
            Process.killProcess(Process.myPid());
        }
    }

    private void setAd() {
        webView = (WebView) findViewById(R.id.webview);
    }

    public static void startAd() {
    }

    public static void stopAd() {
    }

    public static class adDown implements Animation.AnimationListener {
        public void onAnimationEnd(Animation animation) {
            check.webView.setVisibility(8);
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    class NetworkChangeReceiver extends BroadcastReceiver {
        NetworkChangeReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            Log.d("[Benchbee.AST]", String.format("[Ani] NetworkChangeReceiver onReceive %s", intent.toString()));
            if (intent.getAction() != null && intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                Log.d("[Benchbee.AST]", String.format("[Ani] NetworkChangeReceiver ConnectivityManager.CONNECTIVITY_ACTION", new Object[0]));
                if (!check.this.CompareCurrentNetworkType() && check.control.eventStart && !check.control.end_flag && check.control.eventStart) {
                    check.network_changed = true;
                }
            }
        }
    }
}
