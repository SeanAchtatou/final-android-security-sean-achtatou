package com.benchbee.AST;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ToggleButton;
import java.util.ArrayList;

public class resultBox extends ListActivity {
    int db_id;
    private View.OnClickListener del_listener = new View.OnClickListener() {
        public void onClick(View v) {
            dbAdapter db = new dbAdapter(resultBox.this, dbAdapter.SQL_CREATE_BENCHBEE, "BENCHBEE");
            db.open();
            db.deleteTable("id", (long) resultBox.this.db_id);
            db.close();
            resultBox.this.sendBroadcast(new Intent(result.REFRESH));
            resultBox.this.finish();
        }
    };
    String dn_avg;
    Intent intent;
    listAdapter listAd;
    String ping_avg;
    Button resultBoxDel;
    Button resultBoxDetail;
    private View.OnClickListener resultBoxListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.resultBoxOk:
                    resultBox.this.finish();
                    return;
                default:
                    return;
            }
        }
    };
    Button resultBoxOk;
    resultDigit resultDigit;
    resultItem resultItem;
    ToggleButton resultMode;
    float setMbps = 1000000.0f;
    String up_avg;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResultBox();
    }

    public void setResultBox() {
        setContentView((int) R.layout.resultbox);
        this.resultBoxOk = (Button) findViewById(R.id.resultBoxOk);
        this.resultBoxOk.setOnClickListener(this.resultBoxListener);
        this.resultMode = (ToggleButton) findViewById(R.id.resultMode);
        ArrayList<resultItem> lists = new ArrayList<>();
        this.intent = getIntent();
        this.resultItem = new resultItem();
        String Mode = this.intent.getExtras().get("mode").toString();
        if (Mode.equals("3G")) {
            this.resultMode.setChecked(true);
        }
        this.dn_avg = this.intent.getExtras().get("dn_avg").toString();
        this.up_avg = this.intent.getExtras().get("up_avg").toString();
        this.ping_avg = this.intent.getExtras().get("ping_avg").toString();
        int idx = 0 + 1;
        this.resultItem.results[0] = this.intent.getExtras().get("date").toString();
        int idx2 = idx + 1;
        this.resultItem.results[idx] = Mode;
        int idx3 = idx2 + 1;
        this.resultItem.results[idx2] = this.intent.getExtras().get("dn_avg").toString();
        int idx4 = idx3 + 1;
        this.resultItem.results[idx3] = this.intent.getExtras().get("up_avg").toString();
        int idx5 = idx4 + 1;
        this.resultItem.results[idx4] = this.intent.getExtras().get("blink_rate").toString();
        int idx6 = idx5 + 1;
        this.resultItem.results[idx5] = this.intent.getExtras().get("ping_avg").toString();
        int idx7 = idx6 + 1;
        this.resultItem.results[idx6] = this.intent.getExtras().get("ping_max").toString();
        int idx8 = idx7 + 1;
        this.resultItem.results[idx7] = this.intent.getExtras().get("ping_min").toString();
        int idx9 = idx8 + 1;
        this.resultItem.results[idx8] = this.intent.getExtras().get("ping_loss").toString();
        int idx10 = idx9 + 1;
        this.resultItem.results[idx9] = this.intent.getExtras().get("consumed_data").toString();
        int idx11 = idx10 + 1;
        this.resultItem.results[idx10] = this.intent.getExtras().get("network_type").toString();
        this.resultItem.results[idx11] = this.intent.getExtras().get("signal_strength").toString();
        this.resultItem.results[idx11 + 1] = this.intent.getExtras().get("area").toString();
        this.db_id = this.intent.getExtras().getInt("db_id");
        this.resultItem.db_id = this.db_id;
        lists.add(this.resultItem);
        this.listAd = new listAdapter(this, R.layout.listresult, lists);
        setListAdapter(this.listAd);
        this.resultBoxDetail = (Button) findViewById(R.id.resultBoxDetailButton);
        if (this.db_id > 0) {
            this.resultBoxDetail.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent i = new Intent(resultBox.this, resultDetailActivity.class);
                    int idx = 0 + 1;
                    i.putExtra("date", resultBox.this.resultItem.results[0]);
                    int idx2 = idx + 1;
                    i.putExtra("mode", resultBox.this.resultItem.results[idx]);
                    int idx3 = idx2 + 1;
                    i.putExtra("dn_avg", resultBox.this.resultItem.results[idx2]);
                    int idx4 = idx3 + 1;
                    i.putExtra("up_avg", resultBox.this.resultItem.results[idx3]);
                    int idx5 = idx4 + 1;
                    i.putExtra("blink_rate", resultBox.this.resultItem.results[idx4]);
                    int idx6 = idx5 + 1;
                    i.putExtra("ping_avg", resultBox.this.resultItem.results[idx5]);
                    int idx7 = idx6 + 1;
                    i.putExtra("ping_max", resultBox.this.resultItem.results[idx6]);
                    int idx8 = idx7 + 1;
                    i.putExtra("ping_min", resultBox.this.resultItem.results[idx7]);
                    int idx9 = idx8 + 1;
                    i.putExtra("ping_loss", resultBox.this.resultItem.results[idx8]);
                    int idx10 = idx9 + 1;
                    i.putExtra("consumed_data", resultBox.this.resultItem.results[idx9]);
                    int idx11 = idx10 + 1;
                    i.putExtra("network_type", resultBox.this.resultItem.results[idx10]);
                    i.putExtra("signal_strength", resultBox.this.resultItem.results[idx11]);
                    i.putExtra("area", resultBox.this.resultItem.results[idx11 + 1]);
                    i.putExtra("db_id", resultBox.this.resultItem.db_id);
                    resultBox.this.startActivity(i);
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void onApplyThemeResource(Resources.Theme theme, int resid, boolean first) {
        super.onApplyThemeResource(theme, resid, first);
        theme.applyStyle(16973913, true);
    }

    public void onBackPressed() {
        finish();
    }

    private class listAdapter extends ArrayAdapter<resultItem> {
        private ArrayList<resultItem> items;

        public listAdapter(Context context, int textViewResourceId, ArrayList<resultItem> objects) {
            super(context, textViewResourceId, objects);
            this.items = objects;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) resultBox.this.getSystemService("layout_inflater")).inflate((int) R.layout.listresult, (ViewGroup) null);
            }
            if (this.items.get(position) != null) {
                resultBox.this.resultDigit = (resultDigit) v.findViewById(R.id.resultDigit);
                if (!check.pref_unit) {
                    resultBox.this.resultDigit.setBackgroundResource(R.drawable.result_box_02);
                }
                resultBox.this.resultDigit.setValue(libFuncs.sf("%.2f", libFuncs.psFloat(resultBox.this.dn_avg) / check.setUnit), libFuncs.sf("%.2f", libFuncs.psFloat(resultBox.this.up_avg) / check.setUnit), resultBox.this.ping_avg);
            }
            return v;
        }
    }
}
