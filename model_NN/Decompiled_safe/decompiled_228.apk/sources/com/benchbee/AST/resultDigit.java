package com.benchbee.AST;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

public class resultDigit extends View {
    float avg;
    int digit_dn_x = check.r.getDimensionPixelSize(R.dimen.digit_dn_x);
    int digit_dn_y = check.r.getDimensionPixelSize(R.dimen.digit_dn_y);
    int digit_ping_y = check.r.getDimensionPixelSize(R.dimen.digit_ping_y);
    int digit_up_x = check.r.getDimensionPixelSize(R.dimen.digit_up_x);
    int digit_up_y = check.r.getDimensionPixelSize(R.dimen.digit_up_y);
    String dn;
    Paint paintDigitDn = new Paint(1);
    Paint paintDigitUp;
    String ping;
    String up;

    public resultDigit(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface mTypeBold = Typeface.create(Typeface.SANS_SERIF, 1);
        this.paintDigitDn.setTextSize((float) check.r.getDimensionPixelSize(R.dimen.text_digit_dn_str));
        this.paintDigitDn.setTypeface(mTypeBold);
        this.paintDigitDn.setTextAlign(Paint.Align.RIGHT);
        this.paintDigitUp = new Paint(1);
        this.paintDigitUp.setTextSize((float) check.r.getDimensionPixelSize(R.dimen.text_digit_up_str));
        this.paintDigitUp.setTypeface(mTypeBold);
        this.paintDigitUp.setTextAlign(Paint.Align.RIGHT);
    }

    public void setValue(String dn2, String up2, String ping2) {
        this.dn = dn2;
        this.up = up2;
        this.ping = ping2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawText(this.dn, (float) this.digit_dn_x, (float) this.digit_dn_y, this.paintDigitDn);
        canvas.drawText(this.up, (float) this.digit_up_x, (float) this.digit_up_y, this.paintDigitUp);
        canvas.drawText(this.ping, (float) this.digit_up_x, (float) this.digit_ping_y, this.paintDigitUp);
    }
}
