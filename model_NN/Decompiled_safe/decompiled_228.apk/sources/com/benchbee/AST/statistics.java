package com.benchbee.AST;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

public class statistics extends Activity {
    ProgressDialog pd;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.statistics);
        this.pd = new ProgressDialog(this);
        this.pd.setProgressStyle(1);
        this.pd.setMessage("LOADING...");
        this.pd.setCancelable(true);
        this.pd.show();
        WebView web = (WebView) findViewById(R.id.statistics);
        web.setWebViewClient(new WebViewClient());
        web.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int percent) {
                statistics.this.pd.setProgress(percent);
                if (percent > 98) {
                    statistics.this.pd.dismiss();
                }
            }
        });
        web.loadUrl(getResources().getString(R.string.statistics_host));
        web.getSettings().setUseWideViewPort(true);
        web.setInitialScale(10);
    }

    public void grabURL(String url) {
        new GrabURL(this, null).execute(url);
    }

    private class GrabURL extends AsyncTask<String, Void, Void> {
        private final HttpClient Client;
        private String Content;
        private ProgressDialog Dialog;
        private String Error;

        private GrabURL() {
            this.Client = new DefaultHttpClient();
            this.Error = null;
            this.Dialog = new ProgressDialog(statistics.this);
        }

        /* synthetic */ GrabURL(statistics statistics, GrabURL grabURL) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.Dialog.setMessage("NOW LOADING ...");
            this.Dialog.show();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(String... params) {
            try {
                this.Content = (String) this.Client.execute(new HttpGet(params[0]), new BasicResponseHandler<>());
                return null;
            } catch (ClientProtocolException e) {
                this.Error = e.getMessage();
                return null;
            } catch (IOException e2) {
                this.Error = e2.getMessage();
                cancel(true);
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void unused) {
            this.Dialog.dismiss();
            if (this.Error != null) {
                Toast.makeText(statistics.this, this.Error, 0).show();
            } else {
                Toast.makeText(statistics.this, "Source: " + this.Content, 0).show();
            }
        }
    }
}
