package com.benchbee.AST;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ToggleButton;

public class preferences extends Activity implements View.OnClickListener {
    public static final String USER_PREFERENCE = "USER_PREFERENCES";
    boolean PREF_3GALLOW;
    boolean PREF_CHART_DIGIT;
    boolean PREF_GAUGE_AVG;
    boolean PREF_Mbps;
    int collectCnt;
    int[] pass = {1, 4, 5, 1, 6};
    ToggleButton pref_MBps;
    ToggleButton pref_Mbps;
    ToggleButton pref_avg;
    Button pref_cancel;
    ToggleButton pref_digit;
    ToggleButton pref_graph;
    ToggleButton pref_inst;
    Button pref_save;
    SharedPreferences prefs;
    ToggleButton tb3GAllow;
    ToggleButton tb3GDeny;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.preferences);
        this.pref_avg = (ToggleButton) findViewById(R.id.pref_avg);
        this.pref_avg.setOnClickListener(this);
        this.pref_inst = (ToggleButton) findViewById(R.id.pref_inst);
        this.pref_inst.setOnClickListener(this);
        this.pref_graph = (ToggleButton) findViewById(R.id.pref_graph);
        this.pref_graph.setOnClickListener(this);
        this.pref_digit = (ToggleButton) findViewById(R.id.pref_digit);
        this.pref_digit.setOnClickListener(this);
        this.pref_Mbps = (ToggleButton) findViewById(R.id.pref_Mbps);
        this.pref_Mbps.setOnClickListener(this);
        this.pref_MBps = (ToggleButton) findViewById(R.id.pref_MBps);
        this.pref_MBps.setOnClickListener(this);
        this.pref_save = (Button) findViewById(R.id.pref_save);
        this.pref_save.setOnClickListener(this);
        this.pref_cancel = (Button) findViewById(R.id.pref_cancel);
        this.pref_cancel.setOnClickListener(this);
        this.pref_save = (Button) findViewById(R.id.pref_save);
        this.pref_save.setOnClickListener(this);
        this.pref_cancel = (Button) findViewById(R.id.pref_cancel);
        this.pref_cancel.setOnClickListener(this);
        this.tb3GAllow = (ToggleButton) findViewById(R.id.tb3GAllow);
        this.tb3GAllow.setOnClickListener(this);
        this.tb3GDeny = (ToggleButton) findViewById(R.id.tb3GDeny);
        this.tb3GDeny.setOnClickListener(this);
        loadPreference();
        setButton();
    }

    public void loadPreference() {
        this.prefs = getSharedPreferences("USER_PREFERENCES", 0);
        this.PREF_GAUGE_AVG = this.prefs.getBoolean("pref_avg", true);
        this.PREF_CHART_DIGIT = this.prefs.getBoolean("pref_digit", true);
        this.PREF_Mbps = this.prefs.getBoolean("pref_Mbps", true);
        this.PREF_3GALLOW = this.prefs.getBoolean("3GAllow", true);
    }

    public void setButton() {
        if (this.PREF_GAUGE_AVG) {
            this.pref_avg.setChecked(true);
            this.pref_inst.setChecked(false);
        } else {
            this.pref_avg.setChecked(false);
            this.pref_inst.setChecked(true);
        }
        if (this.PREF_CHART_DIGIT) {
            this.pref_digit.setChecked(true);
            this.pref_graph.setChecked(false);
        } else {
            this.pref_digit.setChecked(false);
            this.pref_graph.setChecked(true);
        }
        if (this.PREF_Mbps) {
            this.pref_Mbps.setChecked(true);
            this.pref_MBps.setChecked(false);
        } else {
            this.pref_Mbps.setChecked(false);
            this.pref_MBps.setChecked(true);
        }
        if (this.PREF_3GALLOW) {
            this.tb3GAllow.setChecked(true);
            this.tb3GDeny.setChecked(false);
            return;
        }
        this.tb3GAllow.setChecked(false);
        this.tb3GDeny.setChecked(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void savePreference() {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putBoolean("pref_avg", this.pref_avg.isChecked());
        editor.remove("pref_inst");
        editor.putBoolean("pref_digit", this.pref_digit.isChecked());
        editor.putBoolean("pref_Mbps", this.pref_Mbps.isChecked());
        editor.putBoolean("3GAllow", this.tb3GAllow.isChecked());
        editor.commit();
        Intent setUnit = new Intent(check.UPDATE_SET_ACTION);
        if (this.pref_Mbps.isChecked()) {
            setUnit.putExtra("pref_Mbps", true);
        } else {
            setUnit.putExtra("pref_Mbps", false);
        }
        sendBroadcast(setUnit);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pref_inst:
                this.pref_avg.setChecked(false);
                this.pref_inst.setChecked(true);
                checkPass(1);
                return;
            case R.id.pref_avg:
                this.pref_avg.setChecked(true);
                this.pref_inst.setChecked(false);
                checkPass(2);
                return;
            case R.id.pref_graph:
                this.pref_graph.setChecked(true);
                this.pref_digit.setChecked(false);
                checkPass(3);
                return;
            case R.id.pref_digit:
                this.pref_graph.setChecked(false);
                this.pref_digit.setChecked(true);
                checkPass(4);
                return;
            case R.id.tb3GAllow:
                this.tb3GDeny.setChecked(false);
                this.tb3GAllow.setChecked(true);
                return;
            case R.id.tb3GDeny:
                this.tb3GAllow.setChecked(false);
                this.tb3GDeny.setChecked(true);
                return;
            case R.id.pref_Mbps:
                this.pref_Mbps.setChecked(true);
                this.pref_MBps.setChecked(false);
                checkPass(5);
                return;
            case R.id.pref_MBps:
            default:
                return;
            case R.id.pref_save:
                savePreference();
                finish();
                return;
            case R.id.pref_cancel:
                finish();
                return;
        }
    }

    public void checkPass(int num) {
        if (this.pass[this.collectCnt] == num) {
            this.collectCnt++;
        } else {
            this.collectCnt = 0;
        }
        if (this.collectCnt == this.pass.length) {
            startActivity(new Intent(this, admin.class));
            this.collectCnt = 0;
        }
    }

    public void onBackPressed() {
        finish();
    }
}
