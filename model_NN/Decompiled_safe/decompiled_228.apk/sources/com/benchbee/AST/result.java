package com.benchbee.AST;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class result extends ListActivity implements View.OnClickListener {
    public static final String REFRESH = "com.benchbee.AST.REFRESH";
    Context context;
    Handler handler = new Handler();
    int idx = 0;
    listAdapter listAllAdapter;
    String mode = null;
    String model = null;
    ProgressDialog pd;
    private final BroadcastReceiver refreshReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            result.this.setList();
        }
    };
    Button resetButton;
    float setMbps = 1000000.0f;
    boolean warning = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.result);
        this.context = this;
        this.resetButton = (Button) findViewById(R.id.list_reset_button);
        this.resetButton.setOnClickListener(this);
        setList();
        registerReceiver(this.refreshReceiver, new IntentFilter(REFRESH));
    }

    public void setList() {
        ArrayList<resultItem> listAllItems = new ArrayList<>();
        dbAdapter db = new dbAdapter(this, dbAdapter.SQL_CREATE_BENCHBEE, "BENCHBEE");
        db.open();
        Cursor cursor = db.selectTable(new String[]{"date", "mode", "down", "up", "blink", "ping", "max", "min", "loss", "consume", "type", "signal", "area", "id"}, null, null, null, null, "date DESC");
        startManagingCursor(cursor);
        if (cursor.moveToFirst()) {
            do {
                int i = 0;
                resultItem resultItem = new resultItem();
                while (i < resultItem.results_cnt) {
                    resultItem.results[i] = cursor.getString(i);
                    i++;
                }
                resultItem.db_id = cursor.getInt(i);
                listAllItems.add(resultItem);
            } while (cursor.moveToNext());
        }
        db.close();
        this.listAllAdapter = new listAdapter(this, R.layout.listview, listAllItems);
        setListAdapter(this.listAllAdapter);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.refreshReceiver);
    }

    public void onClick(View v) {
        this.pd = ProgressDialog.show(this.context, "", "LOADING", true, true);
        switch (v.getId()) {
            case R.id.list_reset_button:
                this.listAllAdapter = null;
                new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.reset_title)).setMessage(getResources().getString(R.string.reset_msg)).setPositiveButton(getResources().getString(R.string.check_ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        result.this.setListAdapter(null);
                        result.this.dbListdelete();
                        result.this.pd.dismiss();
                    }
                }).setNegativeButton(getResources().getString(R.string.check_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        result.this.pd.dismiss();
                    }
                }).show();
                return;
            default:
                return;
        }
    }

    public void dbListdelete() {
        dbAdapter db = new dbAdapter(this, dbAdapter.SQL_CREATE_BENCHBEE, "BENCHBEE");
        db.open();
        db.deleteAllTable();
        db.close();
    }

    private class listAdapter extends ArrayAdapter<resultItem> {
        private ArrayList<resultItem> items;

        public listAdapter(Context context, int textViewResourceId, ArrayList<resultItem> objects) {
            super(context, textViewResourceId, objects);
            this.items = objects;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) result.this.getSystemService("layout_inflater")).inflate((int) R.layout.listview, (ViewGroup) null);
            }
            final resultItem Item = this.items.get(position);
            if (Item != null) {
                ImageView mIcon = (ImageView) v.findViewById(R.id.list_mode);
                if (Item.results[1].equals("WIFI") || Item.results[1].equals("Wibro")) {
                    mIcon.setImageResource(R.drawable.icon_wifi);
                    v.setBackgroundColor(Color.argb(30, 21, 45, 61));
                } else {
                    mIcon.setImageResource(R.drawable.icon_3g);
                    v.setBackgroundColor(Color.argb(30, 22, 36, 60));
                }
                TextView[] texts = {(TextView) v.findViewById(R.id.list_date), (TextView) v.findViewById(R.id.list_down), (TextView) v.findViewById(R.id.list_up), (TextView) v.findViewById(R.id.list_ping)};
                SimpleDateFormat formater = new SimpleDateFormat("yyyy.MM.dd\n    HH:mm:ss");
                if (Item.results[Item.date_idx] != null) {
                    texts[0].setText(formater.format(new Date(Long.parseLong(Item.results[Item.date_idx]))));
                }
                if (Item.results[Item.down_avg_idx] != null) {
                    texts[1].setText(String.valueOf(libFuncs.sf("%5.2f", ((float) libFuncs.psInt(Item.results[Item.down_avg_idx])) / result.this.setMbps)) + "\nMbps");
                }
                if (Item.results[Item.up_avg_idx] != null) {
                    texts[2].setText(String.valueOf(libFuncs.sf("%5.2f", ((float) libFuncs.psInt(Item.results[Item.up_avg_idx])) / result.this.setMbps)) + "\nMbps");
                }
                if (Item.results[Item.ping_avg_idx] != null) {
                    texts[3].setText(String.valueOf(libFuncs.sf("%4.0f", libFuncs.psFloat(Item.results[Item.ping_avg_idx]))) + "\n  ms");
                }
                ((Button) v.findViewById(R.id.list_button)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        result.this.pd = ProgressDialog.show(result.this.context, "", "LOADING", true, true);
                        Handler handler = result.this.handler;
                        final resultItem resultitem = Item;
                        handler.post(new Runnable() {
                            public void run() {
                                result.this.pd.dismiss();
                                int idx = resultitem.date_idx;
                                Intent resultDetailBox = new Intent(result.this.context, resultDetailActivity.class);
                                int idx2 = idx + 1;
                                resultDetailBox.putExtra("date", resultitem.results[idx]);
                                int idx3 = idx2 + 1;
                                resultDetailBox.putExtra("mode", resultitem.results[idx2]);
                                int idx4 = idx3 + 1;
                                resultDetailBox.putExtra("dn_avg", resultitem.results[idx3]);
                                int idx5 = idx4 + 1;
                                resultDetailBox.putExtra("up_avg", resultitem.results[idx4]);
                                int idx6 = idx5 + 1;
                                resultDetailBox.putExtra("blink_rate", resultitem.results[idx5]);
                                int idx7 = idx6 + 1;
                                resultDetailBox.putExtra("ping_avg", resultitem.results[idx6]);
                                int idx8 = idx7 + 1;
                                resultDetailBox.putExtra("ping_max", resultitem.results[idx7]);
                                int idx9 = idx8 + 1;
                                resultDetailBox.putExtra("ping_min", resultitem.results[idx8]);
                                int idx10 = idx9 + 1;
                                resultDetailBox.putExtra("ping_loss", resultitem.results[idx9]);
                                int idx11 = idx10 + 1;
                                resultDetailBox.putExtra("consumed_data", resultitem.results[idx10]);
                                int idx12 = idx11 + 1;
                                resultDetailBox.putExtra("network_type", resultitem.results[idx11]);
                                resultDetailBox.putExtra("signal_strength", resultitem.results[idx12]);
                                resultDetailBox.putExtra("area", resultitem.results[idx12 + 1]);
                                resultDetailBox.putExtra("db_id", resultitem.db_id);
                                result.this.startActivityForResult(resultDetailBox, resultDetailActivity.class.hashCode());
                            }
                        });
                    }
                });
            }
            return v;
        }
    }

    public void onBackPressed() {
        finish();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
