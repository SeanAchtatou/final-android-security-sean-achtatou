package com.benchbee.AST;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

public class chartDigit extends View implements eventListener {
    static String draw_digit = "0.00";
    static String draw_pingdigit = "0.00";
    float avg;
    int chartNum;
    int cur_event;
    int digitColor;
    float[] float_digit;
    Typeface mType;
    Typeface mTypeBold;
    Paint paintDigit;
    float ping_digit;
    int position_y;
    boolean speedChart_flag = true;
    int textSize;

    public chartDigit(Context context, AttributeSet attrs) {
        super(context, attrs);
        Resources r = getResources();
        this.paintDigit = new Paint(1);
        this.mTypeBold = Typeface.create(Typeface.SANS_SERIF, 1);
        this.mType = Typeface.create(Typeface.SANS_SERIF, 0);
        this.position_y = r.getDimensionPixelSize(R.dimen.digit_y);
        this.textSize = r.getDimensionPixelSize(R.dimen.text_digit_str);
        this.paintDigit.setTextSize((float) this.textSize);
        this.paintDigit.setTypeface(this.mType);
        this.paintDigit.setTextAlign(Paint.Align.CENTER);
        this.float_digit = new float[2];
        this.digitColor = r.getColor(R.color.digitColor);
    }

    public void startEvent(int eventNum) {
        this.cur_event = eventNum;
    }

    public void nextEvent(int eventNum) {
        if (this.cur_event < 3) {
            this.float_digit[this.cur_event - 1] = this.avg;
        }
        this.cur_event = eventNum;
    }

    public void endEvent() {
        if (this.cur_event < 3) {
            this.float_digit[this.cur_event - 1] = this.avg;
        }
    }

    public void changeEvent(eventItem items) {
        if (items.num != 3) {
            this.avg = items.avg;
        } else {
            this.ping_digit = items.avg;
        }
        calculateUnit();
    }

    public void calculateUnit() {
        if (this.speedChart_flag) {
            float numeric = this.avg / check.setUnit;
            if (((double) numeric) >= 100.0d) {
                draw_digit = String.format("%4.0f", Float.valueOf(numeric));
            } else if (numeric < 10.0f || ((double) numeric) >= 100.0d) {
                draw_digit = String.format("%2.2f", Float.valueOf(numeric));
            } else {
                draw_digit = String.format("%2.1f", Float.valueOf(numeric));
            }
        } else if (((double) this.ping_digit) >= 100.0d) {
            draw_pingdigit = String.format("%4.0f", Float.valueOf(this.ping_digit));
        } else if (this.ping_digit < 10.0f || ((double) this.ping_digit) >= 100.0d) {
            draw_pingdigit = String.format("%2.2f", Float.valueOf(this.ping_digit));
        } else {
            draw_pingdigit = String.format("%2.1f", Float.valueOf(this.ping_digit));
        }
        rePaint();
    }

    public void changeUnit() {
        if (this.speedChart_flag) {
            float numeric = this.float_digit[this.chartNum] / check.setUnit;
            if (numeric < 10.0f) {
                draw_digit = String.format("%4.2f", Float.valueOf(numeric));
            } else {
                draw_digit = String.format("%4.1f", Float.valueOf(numeric));
            }
        }
        rePaint();
    }

    public void rePaint() {
        postInvalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.speedChart_flag) {
            canvas.drawText(draw_digit, (float) (getWidth() / 2), (float) this.position_y, this.paintDigit);
        } else {
            canvas.drawText(draw_pingdigit, (float) (getWidth() / 2), (float) this.position_y, this.paintDigit);
        }
    }

    public void setSpeedChart(int chartNum2) {
        this.speedChart_flag = true;
        this.chartNum = chartNum2;
        float numeric = this.float_digit[chartNum2] / check.setUnit;
        if (numeric < 10.0f) {
            draw_digit = String.format("%4.2f", Float.valueOf(numeric));
        } else {
            draw_digit = String.format("%4.1f", Float.valueOf(numeric));
        }
        rePaint();
    }

    public void clean() {
        this.float_digit[0] = 0.0f;
        this.float_digit[1] = 0.0f;
        draw_digit = "0.00";
        this.ping_digit = 0.0f;
        draw_pingdigit = "0.00";
    }

    public void setPingChart() {
        this.speedChart_flag = false;
    }

    public void setColor(boolean turn) {
        if (turn) {
            this.paintDigit.setColor(-1);
            this.paintDigit.setTypeface(this.mTypeBold);
        } else {
            this.paintDigit.setColor(-16777216);
            this.paintDigit.setTypeface(this.mType);
        }
        rePaint();
    }
}
