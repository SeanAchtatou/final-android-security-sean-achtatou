package com.benchbee.AST;

import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.graphics.drawable.AnimationDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.ToggleButton;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.ProtocolException;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Random;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class controller {
    public static final int GPS_MAX = 8;
    int acceptRTT;
    InetAddress addr_udp;
    int aniDepth = 200;
    int aniFrame = 300;
    String app_ver = "";
    float avgRTT;
    float avg_speed;
    int b_size;
    long begin_time;
    float blinkRateDn;
    float blinkRateUp;
    int blink_period;
    int buttonTextOffColor;
    int buttonTextOnColor;
    ArrayList<ToggleButton> buttons = new ArrayList<>();
    String chkdate = "";
    check context;
    int cur_chart;
    int cur_event;
    int currLen;
    int dn_num;
    String dong = "";
    int downCnt = 0;
    boolean end_flag = false;
    long end_time;
    int error_no;
    int eventCnt;
    boolean eventStart = false;
    int event_period;
    boolean first_event = true;
    String gugun = "";
    String host;
    DataInputStream in;
    float inst_speed;
    eventItem items = new eventItem();
    double[] latitude;
    int limitRTT;
    ArrayList<eventListener> listeners = new ArrayList<>();
    String local_ip = "0.0.0.0";
    double[] longitude;
    int longtimeout;
    long mElapsed_time;
    long mLastElapsed_time;
    int mPLastTotalLen;
    int mTotalLen;
    double m_down_speed_stdev = 0.0d;
    double m_ping_rtt_stdev = 0.0d;
    double m_sum_avgDnSpeed = 0.0d;
    double m_sum_avgDnSpeed_stdev = 0.0d;
    double m_sum_avgPingRtt = 0.0d;
    double m_sum_avgPingRtt_stdev = 0.0d;
    double m_sum_avgUpSpeed = 0.0d;
    double m_sum_avgUpSpeed_stdev = 0.0d;
    double m_sum_dnspeed = 0.0d;
    double m_sum_dnspeed_sq = 0.0d;
    double m_sum_upspeed = 0.0d;
    double m_sum_upspeed_sq = 0.0d;
    double m_up_speed_stdev = 0.0d;
    String mac = "";
    int maxPingCnt;
    int maxRTT;
    int minRTT;
    int mobile_b_size = 4096;
    String mode;
    int move_senstive;
    int networkType;
    double[] networklatitude;
    double[] networklongitude;
    boolean onTouch = false;
    DataOutputStream out;
    boolean ping_end_flag;
    int ping_num;
    int port_tcp;
    int port_udp;
    String provider;
    String remoteDB_host;
    boolean reported = false;
    resultItem resultItem = new resultItem();
    int retryCnt = 2;
    int rttCnt = 0;
    boolean sInterupted = false;
    int setGpsCnt = 0;
    float setMbps = 1000000.0f;
    int setNetworkGpsCnt = 0;
    String sido = "";
    int signalStrength;
    Socket so_tcp;
    DatagramSocket so_udp;
    String ssid = "";
    String strError = "측정이 중지되었습니다.";
    float sum_distance_result = 0.0f;
    String telecom;
    String test_host = "0.0.0.0";
    int test_period;
    int timeOutCnt;
    int timeout;
    setTimer timer = new setTimer();
    int totalDownloadByte;
    int totalRTT;
    int totalUploadByte;
    private View.OnTouchListener touchListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == 0) {
                controller.this.touch_down = event.getX();
                controller.this.onTouch = true;
                return true;
            } else if (event.getAction() == 2) {
                if (controller.this.touch_down - event.getX() < ((float) (-controller.this.move_senstive))) {
                    controller.this.touch_down = event.getX();
                    if (controller.this.end_flag) {
                        controller.this.showAnimation(true, -1, false);
                    }
                    controller.this.onTouch = false;
                } else if (controller.this.touch_down - event.getX() > ((float) controller.this.move_senstive)) {
                    controller.this.touch_down = event.getX();
                    if (controller.this.end_flag) {
                        controller.this.showAnimation(false, -1, false);
                    }
                    controller.this.onTouch = false;
                }
                return true;
            } else if (event.getAction() != 1) {
                return false;
            } else {
                if (controller.this.onTouch && controller.this.cur_chart < 3) {
                    if (check.pref_digit) {
                        controller.this.showAnimation(true, -1, true);
                    } else {
                        controller.this.showAnimation(false, -1, true);
                    }
                }
                return true;
            }
        }
    };
    float touch_down;
    boolean turned = false;
    int upCnt = 0;
    int up_num;
    int wait_cnt;
    int wating_time;
    int wifi_b_size = 8192;

    public controller(check context2) {
        this.context = context2;
        this.latitude = new double[8];
        this.longitude = new double[8];
        this.networklatitude = new double[8];
        this.networklongitude = new double[8];
        for (int i = 0; i < 8; i++) {
            this.latitude[i] = -1.0d;
            this.longitude[i] = -1.0d;
            this.networklatitude[i] = -1.0d;
            this.networklongitude[i] = -1.0d;
        }
    }

    public void run(int eventNum) {
        switch (eventNum) {
            case 1:
                setLoading(1, "다운로드 측정준비중");
                new Thread(new Runnable() {
                    public void run() {
                        controller.this.downloadTest();
                    }
                }).start();
                return;
            case 2:
                setLoading(2, "업로드 측정준비중");
                new Thread(new Runnable() {
                    public void run() {
                        controller.this.uploadTest();
                    }
                }).start();
                return;
            case 3:
                setLoading(3, "핑 측정준비중");
                new Thread(new Runnable() {
                    public void run() {
                        controller.this.pingTest();
                    }
                }).start();
                return;
            default:
                return;
        }
    }

    public void setLoading(int eventNum, String msg) {
        initTransfer(eventNum);
        if (this.turned) {
            showAnimation(false, eventNum, false);
        }
        clickButtons(eventNum - 1, true);
        if (eventNum != 3 && !makeTcpConnect(this.test_host, this.port_tcp)) {
            this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, eventNum, 0, 0));
        }
        if (this.first_event) {
            this.first_event = false;
            startEvent();
            return;
        }
        nextEvent();
    }

    public void showAnimation(boolean turn, int eventNum, boolean rotateX) {
        changeDumyView(true);
        check.digitBack.startAnimation(applyRotation(turn, eventNum, rotateX));
    }

    public void changeDumyView(boolean dumy) {
    }

    public void runExtra() {
        if (!this.sInterupted) {
            if (check.isMobile) {
                setMobileNetworkType();
            }
            setBlinkRate();
            setConsumedData();
            CalculateDistance();
            new Thread(new Runnable() {
                public void run() {
                    controller.this.saveRemoteDB();
                }
            }).start();
            if (!check.network_changed) {
                saveLocalDB();
                showResult();
                return;
            }
            libFuncs.toastShow(this.context.msghandler, this.context, "측정시작시 네트워크와 측정종료시 네트워크가 달라 결과를 저장하지 않습니다.");
        }
    }

    public void addEventListener(eventListener listener) {
        this.listeners.add(listener);
    }

    public void addButton(ToggleButton button) {
        this.buttons.add(button);
    }

    public void clickButtons(int idx, boolean start_flag) {
        if (this.end_flag || start_flag) {
            switch (idx) {
                case 0:
                    check.digitUnit.setChecked(true);
                    setSpeedChart(idx);
                    setButtons(true, false, false);
                    break;
                case 1:
                    check.digitUnit.setChecked(true);
                    setSpeedChart(idx);
                    setButtons(false, true, false);
                    break;
                case 2:
                    check.digitUnit.setChecked(false);
                    setPingChart();
                    setButtons(false, false, true);
                    break;
            }
            this.cur_chart = idx;
        }
    }

    public void setButtons(boolean down, boolean up, boolean ping) {
        if (!check.for_ad) {
            this.buttons.get(0).setChecked(down);
            this.buttons.get(1).setChecked(up);
            this.buttons.get(2).setChecked(ping);
        }
        this.buttons.get(4).setChecked(down);
        this.buttons.get(5).setChecked(up);
        this.buttons.get(3).setChecked(ping);
        if (down) {
            this.buttons.get(0).setTextColor(this.buttonTextOnColor);
        } else {
            this.buttons.get(0).setTextColor(this.buttonTextOffColor);
        }
        if (up) {
            this.buttons.get(1).setTextColor(this.buttonTextOnColor);
        } else {
            this.buttons.get(1).setTextColor(this.buttonTextOffColor);
        }
        if (ping) {
            this.buttons.get(2).setTextColor(this.buttonTextOnColor);
        } else {
            this.buttons.get(2).setTextColor(this.buttonTextOffColor);
        }
    }

    public void changeStartButton() {
        check.button_start.setBackgroundResource(R.drawable.selector_stop);
    }

    public boolean connCheck() {
        if (!makeTcpConnect(this.test_host, this.port_tcp) || disTcpConnect()) {
            return true;
        }
        this.strError = "측정서버에서 응답이 없습니다.";
        return false;
    }

    public boolean getServerHost() {
        boolean get_flag;
        int i = this.retryCnt;
        do {
            get_flag = checkServerHost();
            i--;
            if (i <= 0) {
                break;
            }
        } while (!get_flag);
        if (!get_flag) {
            this.strError = "측정서버목록을 가져올 수 없습니다.";
        }
        return get_flag;
    }

    public boolean checkServerHost() {
        try {
            try {
                HttpURLConnection httpURLCon = (HttpURLConnection) new URL(this.host).openConnection();
                httpURLCon.setReadTimeout(this.timeout);
                httpURLCon.setConnectTimeout(this.timeout);
                httpURLCon.setDefaultUseCaches(false);
                httpURLCon.setDoInput(true);
                httpURLCon.setDoOutput(true);
                try {
                    BufferedReader br = new BufferedReader(new InputStreamReader(httpURLCon.getInputStream(), "EUC-KR"));
                    try {
                        String line = br.readLine();
                        if (line != null) {
                            this.test_host = line.trim();
                        }
                        try {
                            br.close();
                            httpURLCon.disconnect();
                            if (this.test_host != null) {
                                return true;
                            }
                            return false;
                        } catch (IOException e) {
                            e.printStackTrace();
                            return false;
                        }
                    } catch (IOException e2) {
                        e2.printStackTrace();
                        return false;
                    }
                } catch (UnsupportedEncodingException e3) {
                    e3.printStackTrace();
                    return false;
                } catch (IOException e4) {
                    e4.printStackTrace();
                    return false;
                }
            } catch (IOException e5) {
                e5.printStackTrace();
                return false;
            }
        } catch (MalformedURLException e6) {
            e6.printStackTrace();
            return false;
        }
    }

    public boolean makeTcpConnect(String host2, int port) {
        try {
            InetSocketAddress addr = new InetSocketAddress(host2, port);
            this.so_tcp = new Socket();
            this.so_tcp.connect(addr, this.longtimeout);
            this.so_tcp.setTcpNoDelay(true);
            this.in = new DataInputStream(this.so_tcp.getInputStream());
            this.out = new DataOutputStream(this.so_tcp.getOutputStream());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            this.strError = "측정서버와의 연결이 실패 하였습니다.";
            return false;
        }
    }

    public boolean disTcpConnect() {
        try {
            if (this.in != null) {
                this.in.close();
            }
            if (this.out != null) {
                this.out.close();
            }
            if (this.so_tcp != null) {
                this.so_tcp.close();
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean disTcpSocket() {
        try {
            if (this.in != null) {
                this.in.close();
            }
            if (this.out != null) {
                this.out.close();
            }
            if (this.so_tcp == null) {
                return true;
            }
            this.so_tcp.shutdownOutput();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return true;
        }
    }

    public boolean makeUdpConnect(String host2) {
        try {
            this.addr_udp = InetAddress.getByName(host2);
            try {
                this.so_udp = new DatagramSocket();
                this.so_udp.setSoTimeout(this.longtimeout);
                return true;
            } catch (SocketException e) {
                this.strError = "측정서버와의 연결이 실패 하였습니다.";
                e.printStackTrace();
                return false;
            }
        } catch (UnknownHostException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public boolean disUdpConnect() {
        if (this.so_udp == null) {
            return true;
        }
        this.so_udp.close();
        return true;
    }

    public void startEvent() {
        this.context.getWindow().addFlags(128);
        int size = this.listeners.size();
        for (int i = 0; i < size; i++) {
            this.listeners.get(i).startEvent(this.items.num);
        }
        this.eventStart = true;
        this.cur_event = this.items.num;
    }

    public void broadcastEvent(eventItem items2) {
        int size = this.listeners.size();
        for (int i = 0; i < size; i++) {
            this.listeners.get(i).changeEvent(items2);
        }
    }

    public void endEvent() {
        if (!this.end_flag) {
            this.end_flag = true;
            this.context.getWindow().clearFlags(128);
            int size = this.listeners.size();
            if (this.eventStart) {
                for (int i = 0; i < size; i++) {
                    this.listeners.get(i).endEvent();
                }
                this.eventStart = false;
            }
            ((chartDigit) check.graph[check.chartDigitNum]).clearAnimation();
            if (check.isMobile) {
                check.digitMode_front.setBackgroundResource(R.drawable.ani_3g_frame_1);
            } else {
                check.digitMode_front.setBackgroundResource(R.drawable.ani_wifi_frame_1);
            }
            buttonRelease();
            check.restartLock = false;
            check.gps.endEvent();
        }
    }

    class stopAniTask extends TimerTask {
        stopAniTask() {
        }

        public void run() {
            ((AnimationDrawable) check.digitMode_front.getBackground()).stop();
        }
    }

    public void buttonStop() {
        int size = this.buttons.size();
        for (int i = 0; i < size; i++) {
            this.buttons.get(i).setEnabled(false);
        }
        check.menuClose();
        check.startAd();
    }

    public void buttonRelease() {
        int i;
        int size = this.buttons.size();
        if (check.for_ad) {
            i = 3;
        } else {
            i = 0;
        }
        while (i < size) {
            this.buttons.get(i).setEnabled(true);
            i++;
        }
        check.button_start.setBackgroundResource(R.drawable.selector_restart);
        check.stopAd();
        check.menuOpen();
    }

    public void setTouchEvent() {
        ((chartView) check.graph[check.chartViewNum]).setOnTouchListener(this.touchListener);
        check.digitLayout.setOnTouchListener(this.touchListener);
    }

    public void nextEvent() {
        int size = this.listeners.size();
        for (int i = 0; i < size; i++) {
            this.listeners.get(i).nextEvent(this.items.num);
        }
        this.cur_event = this.items.num;
    }

    public void setSpeedChart(int chartNum) {
        int size = this.listeners.size();
        for (int i = 0; i < size; i++) {
            this.listeners.get(i).setSpeedChart(chartNum);
        }
    }

    public void setPingChart() {
        int size = this.listeners.size();
        for (int i = 0; i < size; i++) {
            this.listeners.get(i).setPingChart();
        }
    }

    public void downloadTest() {
        this.timer.startTimer();
        String command = "send " + (this.test_period / 1000) + "\r\n";
        try {
            this.out.write(command.getBytes(), 0, command.length());
            this.out.flush();
            this.totalUploadByte += command.length();
            byte[] buffer = new byte[this.b_size];
            this.begin_time = System.currentTimeMillis();
            do {
                try {
                    this.currLen = this.in.read(buffer);
                    this.end_time = System.currentTimeMillis();
                    if (this.currLen >= 0) {
                        reportPeriodicDNBW();
                        this.mTotalLen += this.currLen;
                        if (this.currLen <= 0 || this.sInterupted) {
                            break;
                        }
                    } else {
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    this.strError = "네트워크 장애가 발생하였습니다.";
                    this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 1, 0, (int) this.avg_speed));
                    return;
                }
            } while (this.mElapsed_time <= ((long) this.test_period));
            this.totalDownloadByte += this.mTotalLen;
            this.resultItem.results[this.resultItem.down_avg_idx] = Integer.toString((int) this.avg_speed);
            if (this.downCnt > 0) {
                this.m_down_speed_stdev = Math.sqrt((this.m_sum_dnspeed_sq / ((double) this.downCnt)) - (((this.m_sum_dnspeed * this.m_sum_dnspeed) / ((double) this.downCnt)) / ((double) this.downCnt)));
                Log.d("[Benchbee.AST]", String.format("End Down Test downCnt [%d], m_Dn_speed_stdev=%f, m_Dn_speed_stdev2=%f", Integer.valueOf(this.downCnt), Double.valueOf(this.m_down_speed_stdev), Double.valueOf(Math.sqrt(this.m_sum_avgDnSpeed_stdev - (this.m_sum_avgDnSpeed * this.m_sum_avgDnSpeed)))));
            }
            this.timer.stopTimer();
            if (!disTcpConnect()) {
                this.strError = "네트워크 장애가 발생하였습니다.";
                this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 1, 0, (int) this.avg_speed));
                return;
            }
            ((chartView) check.graph[check.chartViewNum]).stop();
            this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 1, 1, (int) this.avg_speed));
        } catch (IOException e2) {
            e2.printStackTrace();
            this.strError = "네트워크 장애가 발생하였습니다.";
            this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 1, 0, 0));
        }
    }

    public void uploadTest() {
        String command = "recv " + (this.test_period / 1000) + "\r\n";
        try {
            this.out.write(command.getBytes(), 0, command.length());
            this.out.flush();
            this.totalUploadByte += command.length();
            byte[] buffer = new byte[this.b_size];
            new Random().nextBytes(buffer);
            this.currLen = this.b_size;
            this.timer.startTimer();
            this.begin_time = System.currentTimeMillis();
            do {
                try {
                    this.out.write(buffer, 0, this.currLen);
                    this.out.flush();
                    this.end_time = System.currentTimeMillis();
                    reportPeriodicUPBW();
                    this.mTotalLen += this.currLen;
                    if (this.sInterupted) {
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    this.strError = "네트워크 장애가 발생하였습니다.";
                    this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 2, 0, (int) this.avg_speed));
                    return;
                }
            } while (this.mElapsed_time <= ((long) this.test_period));
            this.totalUploadByte += this.mTotalLen;
            this.resultItem.results[this.resultItem.up_avg_idx] = Integer.toString((int) this.avg_speed);
            if (this.upCnt > 0) {
                this.m_up_speed_stdev = Math.sqrt((this.m_sum_upspeed_sq / ((double) this.upCnt)) - (((this.m_sum_upspeed * this.m_sum_upspeed) / ((double) this.upCnt)) / ((double) this.upCnt)));
                Log.d("[Benchbee.AST]", String.format("End Upload Test upCnt [%d], m_up_speed_stdev=%f, m_up_speed_stdev2=%f", Integer.valueOf(this.upCnt), Double.valueOf(this.m_up_speed_stdev), Double.valueOf(Math.sqrt(this.m_sum_avgUpSpeed_stdev - (this.m_sum_avgUpSpeed * this.m_sum_avgUpSpeed)))));
            }
            this.timer.stopTimer();
            if (!disTcpSocket()) {
                this.strError = "네트워크 장애가 발생하였습니다.";
                this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 2, 0, (int) this.avg_speed));
                return;
            }
            ((chartView) check.graph[check.chartViewNum]).stop();
            this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 2, 1, (int) this.avg_speed));
        } catch (IOException e2) {
            e2.printStackTrace();
            this.strError = "네트워크 장애가 발생하였습니다.";
            this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 2, 0, 0));
        }
    }

    public void pingTest() {
        if (!makeUdpConnect(this.test_host)) {
            this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 3, 0, 0));
        } else {
            DatagramPacket send_packet = new DatagramPacket("start".getBytes(), "start".getBytes().length, this.addr_udp, this.port_udp);
            try {
                this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 4));
                for (int i = this.retryCnt; i > 0; i--) {
                    this.so_udp.send(send_packet);
                }
            } catch (IOException e) {
                e.printStackTrace();
                this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 4));
                this.strError = "네트워크 장애가 발생하였습니다.\n잠시 후 재측정 해주시기 바랍니다.";
                this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 3, 0, 0));
            }
            try {
                this.so_udp.receive(send_packet);
                this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 5));
            } catch (IOException e2) {
                e2.printStackTrace();
                this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 5));
                this.strError = "네트워크 장애가 발생하였습니다.\n잠시 후 재측정 해주시기 바랍니다.";
                this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 3, 0, 0));
            }
        }
        new Thread(new Runnable() {
            byte[] buffer = new byte[32];
            int pre_idx = -1;
            Long receive_time;
            DatagramPacket receiver_packet = new DatagramPacket(this.buffer, this.buffer.length);

            public void run() {
                int i;
                while (!controller.this.ping_end_flag && !controller.this.sInterupted) {
                    try {
                        controller.this.so_udp.receive(this.receiver_packet);
                        this.receive_time = Long.valueOf(System.currentTimeMillis());
                        String line = new String(this.receiver_packet.getData()).trim();
                        controller.this.totalDownloadByte += line.length();
                        String[] split_str = line.split("[ \t\r\n]");
                        if (split_str.length >= 2) {
                            String inst = split_str[0];
                            inst.trim();
                            if (inst.equals("rtt") && split_str.length >= 2) {
                                int idx = Integer.parseInt(split_str[1]);
                                int rtt = (int) (this.receive_time.longValue() - Long.parseLong(split_str[2]));
                                if (rtt <= controller.this.acceptRTT && rtt >= 0) {
                                    if (idx < controller.this.maxPingCnt && idx > this.pre_idx) {
                                        controller.this.totalRTT += rtt;
                                        controller.this.maxRTT = controller.this.maxRTT < rtt ? rtt : controller.this.maxRTT;
                                        controller controller = controller.this;
                                        if (controller.this.minRTT > rtt) {
                                            i = rtt;
                                        } else {
                                            i = controller.this.minRTT;
                                        }
                                        controller.minRTT = i;
                                        controller.this.m_sum_avgPingRtt = ((controller.this.m_sum_avgPingRtt * ((double) controller.this.rttCnt)) / ((double) (controller.this.rttCnt + 1))) + ((double) (rtt / (controller.this.rttCnt + 1)));
                                        controller.this.m_sum_avgPingRtt_stdev = ((controller.this.m_sum_avgPingRtt_stdev * ((double) controller.this.rttCnt)) / ((double) (controller.this.rttCnt + 1))) + ((double) ((rtt * rtt) / (controller.this.rttCnt + 1)));
                                        controller.this.rttCnt++;
                                        controller.this.avgRTT = ((float) controller.this.totalRTT) / ((float) controller.this.rttCnt);
                                        controller.this.items.ping_idx = idx;
                                        controller.this.items.inst = (float) rtt;
                                        controller.this.items.avg = controller.this.avgRTT;
                                        controller.this.broadcastEvent(controller.this.items);
                                        this.pre_idx = idx;
                                    } else if (idx == controller.this.maxPingCnt) {
                                        break;
                                    }
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e2) {
                        e2.printStackTrace();
                    }
                }
                controller.this.items.ping_idx = controller.this.maxPingCnt;
                if (!controller.this.sInterupted) {
                    controller.this.broadcastEvent(controller.this.items);
                }
                if (controller.this.minRTT == controller.this.limitRTT) {
                    controller.this.minRTT = 0;
                }
                controller.this.resultItem.results[controller.this.resultItem.ping_avg_idx] = String.format("%.2f", Float.valueOf(controller.this.avgRTT));
                controller.this.resultItem.results[controller.this.resultItem.ping_max_idx] = Integer.toString(controller.this.maxRTT);
                controller.this.resultItem.results[controller.this.resultItem.ping_min_idx] = Integer.toString(controller.this.minRTT);
                controller.this.resultItem.results[controller.this.resultItem.ping_loss_idx] = String.format("%.2f", Float.valueOf((((float) (controller.this.maxPingCnt - controller.this.rttCnt)) * 100.0f) / ((float) controller.this.maxPingCnt)));
            }
        }).start();
        int i2 = 0;
        while (i2 <= this.maxPingCnt && !this.sInterupted) {
            try {
                String data = "rtt\t" + i2 + "\t" + System.currentTimeMillis();
                this.so_udp.send(new DatagramPacket(data.getBytes(), data.getBytes().length, this.addr_udp, this.port_udp));
                this.totalUploadByte += data.getBytes().length;
                libFuncs.sleep(this.event_period);
                i2++;
            } catch (IOException e3) {
                e3.printStackTrace();
                this.strError = "네트워크 장애가 발생하였습니다.";
                this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 3, 0, (int) (this.avgRTT * 1000000.0f)));
            } catch (NullPointerException e4) {
                e4.printStackTrace();
                this.strError = "네트워크 장애가 발생하였습니다.";
                this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 3, 0, (int) (this.avgRTT * 1000000.0f)));
            }
        }
        libFuncs.sleep(this.acceptRTT);
        this.ping_end_flag = true;
        if (!disUdpConnect()) {
            this.strError = "네트워크 장애가 발생하였습니다.";
            this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 3, 0, (int) (this.avgRTT * 1000000.0f)));
        }
        if (this.rttCnt > 0) {
            this.m_ping_rtt_stdev = Math.sqrt(this.m_sum_avgPingRtt_stdev - (this.m_sum_avgPingRtt * this.m_sum_avgPingRtt));
            Log.d("[Benchbee.AST]", String.format("End Ping Test rttCnt [%d], m_ping_rtt_stdev=%f", Integer.valueOf(this.rttCnt), Double.valueOf(this.m_ping_rtt_stdev)));
        }
        this.context.msghandler.sendMessage(Message.obtain(this.context.msghandler, 3, 1, (int) (this.avgRTT * 1000000.0f)));
    }

    public void setButtonText(int idx, String str) {
        String unit = " Mbps";
        if (idx + 1 == 3) {
            unit = " ms";
        }
        this.buttons.get(idx).setTextOn(String.valueOf(str) + unit);
        this.buttons.get(idx).setTextOff(String.valueOf(str) + unit);
        this.buttons.get(idx).setText(String.valueOf(str) + unit);
    }

    public void initTransfer(int eventNum) {
        this.items.num = eventNum;
        this.items.inst = 0.0f;
        this.items.avg = 0.0f;
        this.items.ping_idx = 0;
        this.mPLastTotalLen = 0;
        this.mTotalLen = 0;
        this.mElapsed_time = 0;
        this.mLastElapsed_time = 0;
        this.avg_speed = 0.0f;
        this.inst_speed = 0.0f;
        this.reported = false;
    }

    public void setConsumedData() {
        this.resultItem.results[this.resultItem.consumedData_idx] = "DN " + String.format("%5.2f", Float.valueOf(((float) this.totalDownloadByte) / this.setMbps)) + " MB/ UP " + String.format("%5.2f", Float.valueOf(((float) this.totalUploadByte) / this.setMbps)) + " MB";
    }

    public void showResult() {
        libFuncs.openResultBox(this.context, this.resultItem);
    }

    public void saveLocalDB() {
        this.resultItem.results[this.resultItem.mode_idx] = this.mode;
        dbAdapter db = new dbAdapter(this.context, dbAdapter.SQL_CREATE_BENCHBEE, "BENCHBEE");
        db.open();
        this.resultItem.results[0] = Long.toString(System.currentTimeMillis());
        ContentValues values = new ContentValues();
        int idx = 0 + 1;
        values.put("date", this.resultItem.results[0]);
        int idx2 = idx + 1;
        values.put("mode", this.resultItem.results[idx]);
        int idx3 = idx2 + 1;
        values.put("down", this.resultItem.results[idx2]);
        int idx4 = idx3 + 1;
        values.put("up", this.resultItem.results[idx3]);
        int idx5 = idx4 + 1;
        values.put("blink", this.resultItem.results[idx4]);
        int idx6 = idx5 + 1;
        values.put("ping", this.resultItem.results[idx5]);
        int idx7 = idx6 + 1;
        values.put("max", this.resultItem.results[idx6]);
        int idx8 = idx7 + 1;
        values.put("min", this.resultItem.results[idx7]);
        int idx9 = idx8 + 1;
        values.put("loss", this.resultItem.results[idx8]);
        int idx10 = idx9 + 1;
        values.put("consume", this.resultItem.results[idx9]);
        int idx11 = idx10 + 1;
        values.put("type", this.resultItem.results[idx10]);
        values.put("signal", this.resultItem.results[idx11]);
        values.put("area", this.resultItem.results[idx11 + 1]);
        int max_tuple_cnt = Integer.parseInt(check.r.getString(R.string.max_tuple));
        Cursor cursor = db.selectTable(new String[]{"id"}, null, null, null, null, "date ASC");
        this.context.startManagingCursor(cursor);
        int tuple_cnt = cursor.getCount();
        if (tuple_cnt >= max_tuple_cnt) {
            cursor.moveToFirst();
            for (int i = 0; i <= tuple_cnt - max_tuple_cnt; i++) {
                db.deleteTable("id", cursor.getString(0));
                cursor.moveToNext();
            }
        }
        db.close();
        this.resultItem.db_id = (int) db.insertTable(values);
    }

    public void saveRemoteDB() {
        String str;
        try {
            try {
                HttpURLConnection httpURLCon = (HttpURLConnection) new URL(this.remoteDB_host).openConnection();
                getLocalIp();
                httpURLCon.setReadTimeout(this.timeout);
                httpURLCon.setConnectTimeout(this.timeout);
                httpURLCon.setDefaultUseCaches(false);
                httpURLCon.setDoInput(true);
                httpURLCon.setDoOutput(true);
                try {
                    httpURLCon.setRequestMethod("POST");
                    httpURLCon.setRequestProperty("content-type", "application/x-www-form-urlencoded");
                    StringBuffer sb = new StringBuffer();
                    postAppend(sb, "platform", "1");
                    postAppend(sb, "os", Build.VERSION.RELEASE);
                    postAppend(sb, "model", Build.MODEL);
                    int idx = 1 + 1;
                    postAppend(sb, "mode", this.resultItem.results[1]);
                    int idx2 = idx + 1;
                    postAppend(sb, "avgDn", this.resultItem.results[idx]);
                    postAppend(sb, "avgUp", this.resultItem.results[idx2]);
                    postAppend(sb, "blinkratedn", String.format("%.2f", Float.valueOf(this.blinkRateDn)));
                    postAppend(sb, "blinkrateup", String.format("%.2f", Float.valueOf(this.blinkRateUp)));
                    int idx3 = idx2 + 1 + 1;
                    int idx4 = idx3 + 1;
                    postAppend(sb, "avgPing", this.resultItem.results[idx3]);
                    int idx5 = idx4 + 1;
                    postAppend(sb, "maxPing", this.resultItem.results[idx4]);
                    postAppend(sb, "minPing", this.resultItem.results[idx5]);
                    postAppend(sb, "lossPing", this.resultItem.results[idx5 + 1]);
                    if (this.setGpsCnt > 0) {
                        postAppend(sb, "latitude", Double.toString(this.latitude[0]));
                        postAppend(sb, "longitude", Double.toString(this.longitude[0]));
                        GPSConvertGeoCode(this.latitude[0], this.longitude[0]);
                        if (this.setGpsCnt > 1) {
                            postAppend(sb, "latitude2", Double.toString(this.latitude[this.setGpsCnt - 1]));
                            postAppend(sb, "longitude2", Double.toString(this.longitude[this.setGpsCnt - 1]));
                        }
                    } else if (this.setNetworkGpsCnt > 0) {
                        postAppend(sb, "latitude", Double.toString(this.networklatitude[0]));
                        postAppend(sb, "longitude", Double.toString(this.networklongitude[0]));
                        GPSConvertGeoCode(this.networklatitude[0], this.networklongitude[0]);
                        if (this.setNetworkGpsCnt > 1) {
                            postAppend(sb, "latitude2", Double.toString(this.networklatitude[this.setNetworkGpsCnt - 1]));
                            postAppend(sb, "longitude2", Double.toString(this.networklongitude[this.setNetworkGpsCnt - 1]));
                        }
                    } else {
                        postAppend(sb, "latitude", "0");
                        postAppend(sb, "longitude", "0");
                    }
                    if (check.network_changed) {
                        postAppend(sb, "change", Integer.toString(check.network_changed_reason));
                    } else {
                        postAppend(sb, "change", "0");
                    }
                    postAppend(sb, "provider", this.provider);
                    postAppend(sb, "networktype", this.resultItem.results[this.resultItem.mobiletype_idx]);
                    postAppend(sb, "signalStrength", Integer.toString(this.signalStrength));
                    postAppend(sb, "appver", this.app_ver);
                    postAppend(sb, "telecom", this.telecom);
                    postAppend(sb, "mac", this.mac == null ? "null" : this.mac);
                    postAppend(sb, "ssid", this.ssid == null ? "null" : this.ssid);
                    postAppend(sb, "chkdate", this.chkdate == null ? "null" : this.chkdate);
                    postAppend(sb, "avgDn_stdev", String.format("%.6f", Double.valueOf(this.m_down_speed_stdev / 1048576.0d)));
                    postAppend(sb, "avgUp_stdev", String.format("%.6f", Double.valueOf(this.m_up_speed_stdev / 1048576.0d)));
                    postAppend(sb, "avgPing_stdev", String.format("%.6f", Double.valueOf(this.m_ping_rtt_stdev)));
                    postAppend(sb, "sido", this.sido == null ? "null" : this.sido);
                    postAppend(sb, "gugun", this.gugun == null ? "null" : this.gugun);
                    if (this.dong == null) {
                        str = "null";
                    } else {
                        str = this.dong;
                    }
                    postAppend(sb, "dong", str);
                    postAppend(sb, "serverip", this.test_host);
                    postAppend(sb, "localip", this.local_ip);
                    if (this.sum_distance_result >= 2500.0f || this.sum_distance_result <= 150.0f) {
                        postAppend(sb, "move", "0");
                    } else {
                        postAppend(sb, "move", "1");
                    }
                    Log.d("Benchbee.AST", String.format("[Ani] SaveRemoteDB sendResult = %s", sb.toString()));
                    Log.d("Benchbee.AST", String.format("[Ani] sido = %s, gugun = %s, dong = %s", this.sido, this.gugun, this.dong));
                    try {
                        PrintWriter pw = new PrintWriter(new OutputStreamWriter(httpURLCon.getOutputStream(), "UTF-8"));
                        pw.write(sb.toString());
                        pw.flush();
                        try {
                            BufferedReader bf = new BufferedReader(new InputStreamReader(httpURLCon.getInputStream(), "EUC-KR"));
                            StringBuilder buff = new StringBuilder();
                            while (true) {
                                try {
                                    String line = bf.readLine();
                                    if (line == null) {
                                        Log.d("[Benchbee.AST]", String.format("Response =\n %s", buff.toString()));
                                        try {
                                            bf.close();
                                            pw.close();
                                            httpURLCon.disconnect();
                                            return;
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                            return;
                                        }
                                    } else {
                                        buff.append(line);
                                    }
                                } catch (IOException e2) {
                                    e2.printStackTrace();
                                    return;
                                }
                            }
                        } catch (UnsupportedEncodingException e3) {
                            e3.printStackTrace();
                        } catch (IOException e4) {
                            e4.printStackTrace();
                        }
                    } catch (UnsupportedEncodingException e5) {
                        e5.printStackTrace();
                    } catch (IOException e6) {
                        e6.printStackTrace();
                    }
                } catch (ProtocolException e7) {
                    e7.printStackTrace();
                }
            } catch (IOException e8) {
                e8.printStackTrace();
            }
        } catch (MalformedURLException e9) {
            e9.printStackTrace();
        }
    }

    private void GPSConvertGeoCode(double latitude2, double longitude2) {
        int MaxIndex;
        try {
            Iterator<Address> addresses = new Geocoder(this.context).getFromLocation(latitude2, longitude2, 1).iterator();
            if (addresses != null) {
                while (addresses.hasNext()) {
                    Address namedLoc = addresses.next();
                    if (namedLoc.getAdminArea() != null) {
                        int MaxIndex2 = namedLoc.getMaxAddressLineIndex();
                        if (MaxIndex2 >= 0) {
                            String[] SpiltString = namedLoc.getAddressLine(MaxIndex2).split(" ");
                            this.sido = SpiltString[1];
                            this.gugun = SpiltString[2];
                            if (SpiltString[3].endsWith("구") || SpiltString[3].endsWith("gu")) {
                                this.gugun = String.valueOf(this.gugun) + " " + SpiltString[3];
                                this.dong = SpiltString[4];
                            } else {
                                this.dong = SpiltString[3];
                            }
                        }
                    } else {
                        this.sido = namedLoc.getLocality();
                        this.gugun = namedLoc.getSubLocality();
                        this.dong = namedLoc.getThoroughfare();
                        if ((this.sido == null || this.gugun == null || this.dong == null) && (MaxIndex = namedLoc.getMaxAddressLineIndex()) >= 0) {
                            this.gugun = namedLoc.getAddressLine(MaxIndex).split(" ")[2];
                        }
                    }
                }
            }
        } catch (IOException e) {
        }
    }

    public void postAppend(StringBuffer sb, String Mode, String value) {
        if (sb.length() > 0) {
            sb.append("&");
        }
        sb.append(Mode);
        sb.append("=");
        sb.append(value);
    }

    public void setGps(double latitude2, double longitude2, String area, String provider2) {
        if (this.end_flag && this.sInterupted) {
            return;
        }
        if (latitude2 != 0.0d || longitude2 != 0.0d) {
            if (provider2.equals("gps")) {
                if (this.setGpsCnt <= 0) {
                    saveGpsValue(latitude2, longitude2, area, provider2);
                } else if (this.latitude[this.setGpsCnt - 1] != latitude2 || this.longitude[this.setGpsCnt - 1] != longitude2) {
                    saveGpsValue(latitude2, longitude2, area, provider2);
                }
            } else if (!provider2.equals("network")) {
            } else {
                if (this.setNetworkGpsCnt <= 0) {
                    saveNetworkGpsValue(latitude2, longitude2, area, provider2);
                } else if (this.networklatitude[this.setNetworkGpsCnt - 1] != latitude2 || this.networklongitude[this.setNetworkGpsCnt - 1] != longitude2) {
                    saveNetworkGpsValue(latitude2, longitude2, area, provider2);
                }
            }
        }
    }

    private void saveGpsValue(double latitude2, double longitude2, String area, String provider2) {
        this.latitude[this.setGpsCnt] = latitude2;
        this.longitude[this.setGpsCnt] = longitude2;
        this.provider = provider2;
        if (this.setGpsCnt < 7) {
            this.setGpsCnt++;
        }
        this.resultItem.results[this.resultItem.area_idx] = area;
    }

    private void saveNetworkGpsValue(double latitude2, double longitude2, String area, String provider2) {
        this.networklatitude[this.setGpsCnt] = latitude2;
        this.networklongitude[this.setGpsCnt] = longitude2;
        this.provider = provider2;
        if (this.setNetworkGpsCnt < 7) {
            this.setNetworkGpsCnt++;
        }
        this.resultItem.results[this.resultItem.area_idx] = area;
    }

    public void CalculateDistance() {
        float[] results = new float[3];
        if (this.setGpsCnt > 1) {
            for (int i = 0; i < this.setGpsCnt; i++) {
                if (!(this.latitude[i + 1] == -1.0d || this.longitude[i + 1] == -1.0d)) {
                    Location.distanceBetween(this.latitude[i], this.longitude[i], this.latitude[i + 1], this.longitude[i + 1], results);
                    this.sum_distance_result += results[0];
                }
            }
        } else if (this.setGpsCnt == 0 && this.setNetworkGpsCnt > 1) {
            for (int i2 = 0; i2 < this.setNetworkGpsCnt; i2++) {
                if (!(this.networklatitude[i2 + 1] == -1.0d || this.networklongitude[i2 + 1] == -1.0d)) {
                    Location.distanceBetween(this.networklatitude[i2], this.networklongitude[i2], this.networklatitude[i2 + 1], this.networklongitude[i2 + 1], results);
                    this.sum_distance_result += results[0];
                }
            }
        }
    }

    public void stop() {
        this.sInterupted = true;
        stopSocket();
        endEvent();
        libFuncs.toastShow(this.context.msghandler, this.context, this.strError);
    }

    public void stopSocket() {
        disTcpConnect();
        disUdpConnect();
    }

    public void setNewtworkMode(String mode2) {
        this.mode = mode2;
        if (check.isMobile) {
            this.b_size = this.mobile_b_size;
        } else {
            this.b_size = this.wifi_b_size;
        }
    }

    public void setSignalStrength(int signalStrength2) {
        this.signalStrength = signalStrength2;
        if (!check.isMobile || signalStrength2 < 0) {
            this.resultItem.results[this.resultItem.signal_strength_idx] = Integer.toString(signalStrength2);
        } else if (signalStrength2 == 0) {
            this.resultItem.results[this.resultItem.signal_strength_idx] = "<\t" + Integer.toString(gaugeView.start_angle);
        } else if (signalStrength2 > 0 && signalStrength2 < 31) {
            this.resultItem.results[this.resultItem.signal_strength_idx] = Integer.toString((signalStrength2 * 2) - gaugeView.center_angle);
        } else if (signalStrength2 == 31) {
            this.resultItem.results[this.resultItem.signal_strength_idx] = ">\t" + Integer.toString(-51);
        } else {
            this.resultItem.results[this.resultItem.signal_strength_idx] = " ";
        }
    }

    public void reportPeriodicDNBW() {
        this.mElapsed_time = this.end_time - this.begin_time;
        if (this.mElapsed_time > this.mLastElapsed_time + ((long) this.event_period) && this.mElapsed_time < ((long) this.test_period)) {
            this.avg_speed = (((float) this.mTotalLen) * 8000.0f) / ((float) this.mElapsed_time);
            this.inst_speed = (((float) (this.mTotalLen - this.mPLastTotalLen)) * 8000.0f) / ((float) (this.mElapsed_time - this.mLastElapsed_time));
            this.mPLastTotalLen = this.mTotalLen;
            this.mLastElapsed_time = this.mElapsed_time;
            this.m_sum_avgDnSpeed = ((this.m_sum_avgDnSpeed * ((double) this.downCnt)) / ((double) (this.downCnt + 1))) + ((double) (this.avg_speed / ((float) (this.downCnt + 1))));
            this.m_sum_avgDnSpeed_stdev = ((this.m_sum_avgDnSpeed_stdev * ((double) this.downCnt)) / ((double) (this.downCnt + 1))) + ((double) ((this.avg_speed * this.avg_speed) / ((float) (this.downCnt + 1))));
            this.downCnt++;
            this.m_sum_dnspeed += (double) this.inst_speed;
            this.m_sum_dnspeed_sq += (double) (this.inst_speed * this.inst_speed);
            this.items.inst = this.inst_speed;
            this.items.avg = this.avg_speed;
            broadcastEvent(this.items);
            this.reported = true;
        }
        this.timeOutCnt = 0;
    }

    public void reportPeriodicUPBW() {
        this.mElapsed_time = this.end_time - this.begin_time;
        if (this.mElapsed_time > this.mLastElapsed_time + ((long) this.event_period) && this.mElapsed_time < ((long) this.test_period)) {
            this.avg_speed = (((float) this.mTotalLen) * 8000.0f) / ((float) this.mElapsed_time);
            this.inst_speed = (((float) (this.mTotalLen - this.mPLastTotalLen)) * 8000.0f) / ((float) (this.mElapsed_time - this.mLastElapsed_time));
            this.mPLastTotalLen = this.mTotalLen;
            this.mLastElapsed_time = this.mElapsed_time;
            this.m_sum_avgUpSpeed = ((this.m_sum_avgUpSpeed * ((double) this.upCnt)) / ((double) (this.upCnt + 1))) + ((double) (this.avg_speed / ((float) (this.upCnt + 1))));
            this.m_sum_avgUpSpeed_stdev = ((this.m_sum_avgUpSpeed_stdev * ((double) this.upCnt)) / ((double) (this.upCnt + 1))) + ((double) ((this.avg_speed * this.avg_speed) / ((float) (this.upCnt + 1))));
            this.upCnt++;
            this.m_sum_upspeed += (double) this.inst_speed;
            this.m_sum_upspeed_sq += (double) (this.inst_speed * this.inst_speed);
            this.items.inst = this.inst_speed;
            this.items.avg = this.avg_speed;
            broadcastEvent(this.items);
            this.reported = true;
        }
        this.timeOutCnt = 0;
    }

    public void reportZeroEvent() {
        this.mElapsed_time = System.currentTimeMillis() - this.begin_time;
        if (this.mElapsed_time < ((long) this.test_period) && this.reported) {
            this.avg_speed = (((float) this.mTotalLen) * 8000.0f) / ((float) this.mElapsed_time);
            this.inst_speed = -1.0f;
            this.items.inst = this.inst_speed;
            this.items.avg = this.avg_speed;
            broadcastEvent(this.items);
        }
    }

    public class setTimer {
        ScheduledExecutorService executor;

        public setTimer() {
        }

        public void startTimer() {
            this.executor = Executors.newSingleThreadScheduledExecutor();
            this.executor.scheduleWithFixedDelay(new Runnable() {
                public void run() {
                    if (!controller.this.sInterupted) {
                        controller.this.timeOutCnt++;
                        if (controller.this.timeOutCnt > 2) {
                            controller.this.reportZeroEvent();
                        }
                        if (controller.this.timeOutCnt * controller.this.blink_period > controller.this.longtimeout) {
                            controller.this.strError = "네트워크 장애로 타임아웃이 발생하였습니다.";
                            controller.this.context.msghandler.post(new Runnable() {
                                public void run() {
                                    controller.this.stop();
                                }
                            });
                            setTimer.this.stopTimer();
                            return;
                        }
                        return;
                    }
                    setTimer.this.stopTimer();
                }
            }, 0, (long) controller.this.blink_period, TimeUnit.MILLISECONDS);
        }

        public void stopTimer() {
            if (!this.executor.isShutdown()) {
                this.executor.shutdown();
            }
        }
    }

    public void setWifiNetworkType(int link_speed) {
        this.resultItem.results[this.resultItem.mobiletype_idx] = "WIFI " + link_speed + " Mbps";
    }

    public void setMobileNetworkType() {
        this.networkType = ((TelephonyManager) this.context.getSystemService("phone")).getNetworkType();
        switch (this.networkType) {
            case 0:
                this.resultItem.results[this.resultItem.mobiletype_idx] = "UNKNOWN";
                return;
            case 1:
                this.resultItem.results[this.resultItem.mobiletype_idx] = "GPRS";
                return;
            case 2:
                this.resultItem.results[this.resultItem.mobiletype_idx] = "EDGE";
                return;
            case 3:
                this.resultItem.results[this.resultItem.mobiletype_idx] = "UMTS";
                return;
            case 4:
                this.resultItem.results[this.resultItem.mobiletype_idx] = "CDMA";
                return;
            case 5:
                this.resultItem.results[this.resultItem.mobiletype_idx] = "EVDO_0";
                return;
            case 6:
                this.resultItem.results[this.resultItem.mobiletype_idx] = "EVDO_A";
                return;
            case 7:
                this.resultItem.results[this.resultItem.mobiletype_idx] = "1xRTT";
                return;
            case GPS_MAX /*8*/:
                this.resultItem.results[this.resultItem.mobiletype_idx] = "HSDPA";
                return;
            case 9:
                this.resultItem.results[this.resultItem.mobiletype_idx] = "HSUPA";
                return;
            case 10:
                this.resultItem.results[this.resultItem.mobiletype_idx] = "HSPA";
                return;
            default:
                return;
        }
    }

    public void setBlinkRate() {
        this.blinkRateDn = ((chartView) check.graph[check.chartViewNum]).blinkrate[0];
        this.blinkRateUp = ((chartView) check.graph[check.chartViewNum]).blinkrate[1];
        this.resultItem.results[this.resultItem.blinkrate_idx] = "DN " + String.format("%.2f", Float.valueOf(this.blinkRateDn)) + " %/ UP " + String.format("%.2f", Float.valueOf(this.blinkRateUp)) + " %";
    }

    public void setTelecom(String simOperatorName) {
        this.telecom = simOperatorName;
    }

    private Rotate3dAnimation applyRotation(boolean turn, int eventNum, boolean rotateX) {
        int start;
        int end;
        if (turn) {
            start = 0;
            end = 90;
        } else {
            start = 360;
            end = 270;
        }
        Rotate3dAnimation rotation = new Rotate3dAnimation((float) start, (float) end, check.digitWidth, check.digitHeigh, (float) this.aniDepth, true, rotateX);
        if (rotateX) {
            rotation.setDuration((long) (this.aniFrame / 2));
        } else {
            rotation.setDuration((long) this.aniFrame);
        }
        rotation.setInterpolator(new AccelerateInterpolator());
        if (eventNum > 0) {
            this.turned = turn;
            rotation.setAnimationListener(new nextAnimation(eventNum));
        } else {
            rotation.setAnimationListener(new nextTouchAnimation(turn, rotateX));
        }
        return rotation;
    }

    public class Rotate3dAnimation extends Animation {
        private Camera mCamera;
        private final float mCenterX;
        private final float mCenterY;
        private final float mDepthZ;
        private final float mFromDegrees;
        private final boolean mReverse;
        private final boolean mRotateX;
        private final float mToDegrees;

        public Rotate3dAnimation(float fromDegrees, float toDegrees, float centerX, float centerY, float depthZ, boolean reverse, boolean rotateX) {
            this.mFromDegrees = fromDegrees;
            this.mToDegrees = toDegrees;
            this.mCenterX = centerX;
            this.mCenterY = centerY;
            this.mDepthZ = depthZ;
            this.mReverse = reverse;
            this.mRotateX = rotateX;
        }

        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
            this.mCamera = new Camera();
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float interpolatedTime, Transformation t) {
            float fromDegrees = this.mFromDegrees;
            float degrees = fromDegrees + ((this.mToDegrees - fromDegrees) * interpolatedTime);
            float centerX = this.mCenterX;
            float centerY = this.mCenterY;
            Camera camera = this.mCamera;
            Matrix matrix = t.getMatrix();
            camera.save();
            if (this.mReverse) {
                camera.translate(0.0f, 0.0f, this.mDepthZ * interpolatedTime);
            } else {
                camera.translate(0.0f, 0.0f, this.mDepthZ * (1.0f - interpolatedTime));
            }
            if (this.mRotateX) {
                camera.rotateX(degrees);
            } else {
                camera.rotateY(degrees);
            }
            camera.getMatrix(matrix);
            camera.restore();
            matrix.preTranslate(-centerX, -centerY);
            matrix.postTranslate(centerX, centerY);
        }
    }

    private class nextAnimation implements Animation.AnimationListener {
        int chartNum;
        int end;
        int start;

        public nextAnimation(int chartNum2) {
            this.chartNum = chartNum2;
            if (controller.this.turned) {
                this.start = 270;
                this.end = 360;
                return;
            }
            this.start = 90;
            this.end = 0;
        }

        public void onAnimationEnd(Animation animation) {
            check.digitBack.post(new Runnable() {
                public void run() {
                    controller.this.setColor();
                    if (controller.this.turned) {
                        check.digitMode_front.setVisibility(8);
                        check.digitMode_back.setVisibility(0);
                        check.digitBack.setBackgroundResource(check.resID[nextAnimation.this.chartNum + 2]);
                    } else {
                        check.digitMode_front.setVisibility(0);
                        check.digitMode_back.setVisibility(8);
                        check.digitBack.setBackgroundResource(check.resID[nextAnimation.this.chartNum - 1]);
                    }
                    Rotate3dAnimation rot = new Rotate3dAnimation((float) nextAnimation.this.start, (float) nextAnimation.this.end, check.digitWidth, check.digitHeigh, (float) controller.this.aniDepth, false, false);
                    rot.setDuration((long) controller.this.aniFrame);
                    rot.setAnimationListener(new dumyView());
                    rot.setInterpolator(new DecelerateInterpolator());
                    check.digitBack.startAnimation(rot);
                }
            });
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    public void setColor() {
        ((chartDigit) check.graph[check.chartDigitNum]).setColor(this.turned);
        ((chartView) check.graph[check.chartViewNum]).setColor(this.turned);
    }

    private class nextTouchAnimation implements Animation.AnimationListener {
        boolean direction;
        int end;
        boolean rotateX;
        int start;

        public nextTouchAnimation(boolean direction2, boolean rotateX2) {
            this.direction = direction2;
            this.rotateX = rotateX2;
            if (direction2) {
                this.start = 270;
                this.end = 360;
                return;
            }
            this.start = 90;
            this.end = 0;
        }

        public void onAnimationEnd(Animation animation) {
            check.digitBack.post(new Runnable() {
                public void run() {
                    controller.this.setColor();
                    if (!nextTouchAnimation.this.rotateX) {
                        if (check.pref_digit) {
                            ((chartView) check.graph[check.chartViewNum]).setVisibility(8);
                            check.digitLayout.setVisibility(0);
                        }
                        if (nextTouchAnimation.this.direction) {
                            if (controller.this.cur_chart < 2) {
                                controller access$0 = controller.this;
                                controller access$02 = controller.this;
                                int i = access$02.cur_chart + 1;
                                access$02.cur_chart = i;
                                access$0.clickButtons(i, false);
                            } else {
                                controller.this.cur_chart = 0;
                                controller.this.clickButtons(controller.this.cur_chart, false);
                            }
                        } else if (controller.this.cur_chart > 0) {
                            controller access$03 = controller.this;
                            controller access$04 = controller.this;
                            int i2 = access$04.cur_chart - 1;
                            access$04.cur_chart = i2;
                            access$03.clickButtons(i2, false);
                        } else {
                            controller.this.cur_chart = 2;
                            controller.this.clickButtons(controller.this.cur_chart, false);
                        }
                    } else if (check.pref_digit) {
                        check.pref_digit = false;
                        ((chartView) check.graph[check.chartViewNum]).setVisibility(0);
                        check.digitLayout.setVisibility(8);
                    } else {
                        check.pref_digit = true;
                        ((chartView) check.graph[check.chartViewNum]).setVisibility(8);
                        check.digitLayout.setVisibility(0);
                    }
                    if (controller.this.turned) {
                        check.digitBack.setBackgroundResource(check.resID[controller.this.cur_chart + 3]);
                    } else {
                        check.digitBack.setBackgroundResource(check.resID[controller.this.cur_chart]);
                    }
                    Rotate3dAnimation rot = new Rotate3dAnimation((float) nextTouchAnimation.this.start, (float) nextTouchAnimation.this.end, check.digitWidth, check.digitHeigh, (float) controller.this.aniDepth, false, nextTouchAnimation.this.rotateX);
                    if (nextTouchAnimation.this.rotateX) {
                        rot.setDuration((long) (controller.this.aniFrame / 2));
                    } else {
                        rot.setDuration((long) controller.this.aniFrame);
                    }
                    rot.setInterpolator(new DecelerateInterpolator());
                    check.digitBack.startAnimation(rot);
                }
            });
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    class dumyView implements Animation.AnimationListener {
        dumyView() {
        }

        /* Debug info: failed to restart local var, previous not found, register: 3 */
        public void onAnimationEnd(Animation animation) {
            if (controller.this.turned) {
                ((chartDigit) check.graph[check.chartDigitNum]).startAnimation(AnimationUtils.loadAnimation(controller.this.context, R.anim.digit_wave_scale));
                return;
            }
            ((chartDigit) check.graph[check.chartDigitNum]).clearAnimation();
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    public boolean getLocalIp() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (enumIpAddr.hasMoreElements()) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        this.local_ip = inetAddress.getHostAddress().toString();
                    }
                }
            }
            return true;
        } catch (SocketException e) {
            return false;
        }
    }

    public String setTime() {
        Calendar rightNow = Calendar.getInstance();
        this.chkdate = String.format("%04d-%02d-%02d_%02d:%02d:%02d", Integer.valueOf(rightNow.get(1)), Integer.valueOf(rightNow.get(2) + 1), Integer.valueOf(rightNow.get(5)), Integer.valueOf(rightNow.get(11)), Integer.valueOf(rightNow.get(12)), Integer.valueOf(rightNow.get(13)));
        return this.chkdate;
    }
}
