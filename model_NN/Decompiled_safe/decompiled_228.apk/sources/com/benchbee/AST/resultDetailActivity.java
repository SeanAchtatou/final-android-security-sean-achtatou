package com.benchbee.AST;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class resultDetailActivity extends Activity {
    int db_id = -1;
    String dn_avg;
    SimpleDateFormat formater = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
    int idx;
    Intent intent;
    ImageView ivDelete;
    ListView list;
    String ping_avg;
    resultItem resultItem;
    float setMbps = 1000000.0f;
    String up_avg;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.result_detail);
        setResult(0);
        this.intent = getIntent();
        this.resultItem = new resultItem();
        this.idx = this.resultItem.date_idx;
        String Date = this.intent.getExtras().get("date").toString();
        String[] strArr = this.resultItem.results;
        int i = this.idx;
        this.idx = i + 1;
        strArr[i] = this.formater.format(new Date(Long.parseLong(Date)));
        String Mode = this.intent.getExtras().get("mode").toString();
        String[] strArr2 = this.resultItem.results;
        int i2 = this.idx;
        this.idx = i2 + 1;
        strArr2[i2] = Mode;
        this.dn_avg = this.intent.getExtras().get("dn_avg").toString();
        String[] strArr3 = this.resultItem.results;
        int i3 = this.idx;
        this.idx = i3 + 1;
        strArr3[i3] = String.valueOf(libFuncs.sf("%.2f", ((float) libFuncs.psInt(this.dn_avg)) / this.setMbps)) + " Mbps (" + libFuncs.sf("%5.2f", libFuncs.psFloat(this.dn_avg) / (this.setMbps * 8.0f)) + " MB/s)";
        this.up_avg = this.intent.getExtras().get("up_avg").toString();
        String[] strArr4 = this.resultItem.results;
        int i4 = this.idx;
        this.idx = i4 + 1;
        strArr4[i4] = String.valueOf(libFuncs.sf("%.2f", ((float) libFuncs.psInt(this.up_avg)) / this.setMbps)) + " Mbps (" + libFuncs.sf("%5.2f", libFuncs.psFloat(this.up_avg) / (this.setMbps * 8.0f)) + " MB/s)";
        String[] strArr5 = this.resultItem.results;
        int i5 = this.idx;
        this.idx = i5 + 1;
        strArr5[i5] = this.intent.getExtras().get("blink_rate").toString();
        this.ping_avg = this.intent.getExtras().get("ping_avg").toString();
        String[] strArr6 = this.resultItem.results;
        int i6 = this.idx;
        this.idx = i6 + 1;
        strArr6[i6] = String.valueOf(this.ping_avg) + " ms";
        String[] strArr7 = this.resultItem.results;
        int i7 = this.idx;
        this.idx = i7 + 1;
        strArr7[i7] = String.valueOf(this.intent.getExtras().get("ping_max").toString()) + " ms";
        String[] strArr8 = this.resultItem.results;
        int i8 = this.idx;
        this.idx = i8 + 1;
        strArr8[i8] = String.valueOf(this.intent.getExtras().get("ping_min").toString()) + " ms";
        String[] strArr9 = this.resultItem.results;
        int i9 = this.idx;
        this.idx = i9 + 1;
        strArr9[i9] = String.valueOf(this.intent.getExtras().get("ping_loss").toString()) + " %";
        String[] strArr10 = this.resultItem.results;
        int i10 = this.idx;
        this.idx = i10 + 1;
        strArr10[i10] = this.intent.getExtras().get("consumed_data").toString();
        String[] strArr11 = this.resultItem.results;
        int i11 = this.idx;
        this.idx = i11 + 1;
        strArr11[i11] = this.intent.getExtras().get("network_type").toString();
        String[] strArr12 = this.resultItem.results;
        int i12 = this.idx;
        this.idx = i12 + 1;
        strArr12[i12] = String.valueOf(this.intent.getExtras().get("signal_strength").toString()) + " dBm";
        this.resultItem.results[this.idx] = this.intent.getExtras().get("area").toString();
        this.ivDelete = (ImageView) findViewById(R.id.ivDelete);
        this.db_id = this.intent.getExtras().getInt("db_id");
        if (this.db_id > 0) {
            this.ivDelete.setVisibility(0);
            this.ivDelete.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    new AlertDialog.Builder(resultDetailActivity.this).setTitle(resultDetailActivity.this.getResources().getString(R.string.delete_dialog_title)).setMessage(resultDetailActivity.this.getResources().getString(R.string.delete_dialog_msg)).setPositiveButton(resultDetailActivity.this.getResources().getString(R.string.check_ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dbAdapter db = new dbAdapter(resultDetailActivity.this, dbAdapter.SQL_CREATE_BENCHBEE, "BENCHBEE");
                            db.open();
                            db.deleteTable("id", (long) resultDetailActivity.this.db_id);
                            db.close();
                            resultDetailActivity.this.sendBroadcast(new Intent(result.REFRESH));
                            resultDetailActivity.this.finish();
                            resultDetailActivity.this.setResult(1);
                            resultDetailActivity.this.finish();
                        }
                    }).setNegativeButton(resultDetailActivity.this.getResources().getString(R.string.check_cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
                }
            });
        }
        this.list = (ListView) findViewById(R.id.listView1);
        this.list.setAdapter((ListAdapter) new ResultAdapter(this));
    }

    private class ResultAdapter extends BaseAdapter {
        private ArrayList<String> key = new ArrayList<>();
        private ArrayList<Integer> type = new ArrayList<>();
        private ArrayList<String> value = new ArrayList<>();

        public boolean areAllItemsEnabled() {
            return false;
        }

        public boolean isEnabled(int position) {
            return false;
        }

        public ResultAdapter(resultDetailActivity resultDetailActivity) {
            this.key.clear();
            this.value.clear();
            addItem(1, "Section", "측정정보");
            addItem(0, "측정시간", resultDetailActivity.this.resultItem.getDate());
            addItem(0, "측정모드", resultDetailActivity.this.resultItem.getMode());
            addItem(0, "네트워크타입", resultDetailActivity.this.resultItem.getMobileType());
            addItem(1, "Section", "인터넷속도 품질측정 정보");
            addItem(0, "다운로드", resultDetailActivity.this.resultItem.getDownAvg());
            addItem(0, "업로드", resultDetailActivity.this.resultItem.getUpAvg());
            addItem(0, "지연평균", resultDetailActivity.this.resultItem.getPing_avg());
            addItem(0, "지연최소", resultDetailActivity.this.resultItem.getPing_min());
            addItem(0, "지연최대", resultDetailActivity.this.resultItem.getPing_max());
            addItem(0, "손실률", resultDetailActivity.this.resultItem.getPing_loss());
        }

        private void addItem(int t, String k, String v) {
            this.type.add(Integer.valueOf(t));
            this.key.add(k);
            this.value.add(v);
        }

        private void addItem(int t, String k, double v) {
            this.type.add(Integer.valueOf(t));
            this.key.add(k);
            this.value.add(String.format("%.2f", Double.valueOf(v)));
        }

        public int getCount() {
            return this.key.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) resultDetailActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.result_detail_item, (ViewGroup) null);
            }
            makeAllGone(v);
            RelativeLayout rlBase = (RelativeLayout) v.findViewById(R.id.rlBase);
            int t = this.type.get(position).intValue();
            if (t == 1) {
                rlBase.setBackgroundResource(R.drawable.str_detail_04);
                TextView tv = (TextView) v.findViewById(R.id.tvSectionHeader);
                tv.setVisibility(0);
                tv.setText(this.value.get(position));
            }
            if (t == 0) {
                rlBase.setBackgroundResource(0);
                TextView tv2 = (TextView) v.findViewById(R.id.tvKey);
                tv2.setVisibility(0);
                tv2.setText(this.key.get(position));
                TextView tv3 = (TextView) v.findViewById(R.id.tvValue);
                tv3.setVisibility(0);
                tv3.setText(this.value.get(position));
            }
            return v;
        }

        public void makeAllGone(View v) {
            ((TextView) v.findViewById(R.id.tvSectionHeader)).setVisibility(8);
            ((TextView) v.findViewById(R.id.tvKey)).setVisibility(8);
            ((TextView) v.findViewById(R.id.tvDesc)).setVisibility(8);
            ((TextView) v.findViewById(R.id.tvDesc2)).setVisibility(8);
            ((TextView) v.findViewById(R.id.tvValue)).setVisibility(8);
            ((TextView) v.findViewById(R.id.tvValue2)).setVisibility(8);
            ((TextView) v.findViewById(R.id.tvUnit)).setVisibility(8);
            ((TextView) v.findViewById(R.id.tvUnit2)).setVisibility(8);
        }
    }
}
