package com.benchbee.AST;

public interface eventListener {
    void changeEvent(eventItem eventitem);

    void endEvent();

    void nextEvent(int i);

    void setPingChart();

    void setSpeedChart(int i);

    void startEvent(int i);
}
