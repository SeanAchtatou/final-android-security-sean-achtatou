package com.benchbee.AST;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.Enumeration;

public class settingBox extends View {
    String ext_ip = " ";
    String ext_ip_host;
    String local_ip = " ";
    Resources r = getResources();
    float start_pos_x = this.r.getDimension(R.dimen.setting_str_x);
    float start_pos_y = this.r.getDimension(R.dimen.setting_str_y);
    float step_y = this.r.getDimension(R.dimen.setting_str_step);
    Paint text_paint = new Paint(1);
    float text_result_str = this.r.getDimension(R.dimen.text_setting_str);
    int timeout;

    public settingBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.text_paint.setTextSize(this.text_result_str);
        this.text_paint.setARGB(255, 255, 255, 255);
        this.text_paint.setTextAlign(Paint.Align.LEFT);
        this.ext_ip_host = this.r.getString(R.string.get_extip_host);
        this.timeout = Integer.parseInt(this.r.getString(R.string.timeout));
        new Thread(new Runnable() {
            public void run() {
                settingBox.this.getLocalIp();
                settingBox.this.postInvalidate();
                settingBox.this.getExtenalIp();
                settingBox.this.postInvalidate();
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float pos_x = this.start_pos_x;
        float pos_y = this.start_pos_y;
        canvas.drawText(Build.MODEL, pos_x, pos_y, this.text_paint);
        float pos_y2 = pos_y + this.step_y;
        canvas.drawText(String.valueOf(Build.VERSION.CODENAME) + "  " + Build.VERSION.RELEASE, pos_x, pos_y2, this.text_paint);
        float pos_y3 = pos_y2 + this.step_y;
        canvas.drawText(Build.CPU_ABI, pos_x, pos_y3, this.text_paint);
        float pos_y4 = pos_y3 + this.step_y;
        canvas.drawText(this.ext_ip, pos_x, pos_y4, this.text_paint);
        canvas.drawText(this.local_ip, pos_x, pos_y4 + this.step_y, this.text_paint);
    }

    public boolean getLocalIp() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (enumIpAddr.hasMoreElements()) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        this.local_ip = inetAddress.getHostAddress().toString();
                    }
                }
            }
            return true;
        } catch (SocketException e) {
            return false;
        }
    }

    public boolean getExtenalIp() {
        try {
            try {
                HttpURLConnection httpURLCon = (HttpURLConnection) new URL(this.ext_ip_host).openConnection();
                httpURLCon.setReadTimeout(this.timeout);
                httpURLCon.setConnectTimeout(this.timeout);
                httpURLCon.setDefaultUseCaches(false);
                httpURLCon.setDoInput(true);
                httpURLCon.setDoOutput(true);
                try {
                    BufferedReader br = new BufferedReader(new InputStreamReader(httpURLCon.getInputStream(), "EUC-KR"));
                    try {
                        String line = br.readLine();
                        if (line != null) {
                            this.ext_ip = line.trim();
                        }
                        try {
                            br.close();
                            httpURLCon.disconnect();
                            return true;
                        } catch (IOException e) {
                            e.printStackTrace();
                            return false;
                        }
                    } catch (IOException e2) {
                        e2.printStackTrace();
                        return false;
                    }
                } catch (UnsupportedEncodingException e3) {
                    e3.printStackTrace();
                    return false;
                } catch (IOException e4) {
                    e4.printStackTrace();
                    return false;
                }
            } catch (IOException e5) {
                e5.printStackTrace();
                return false;
            }
        } catch (MalformedURLException e6) {
            e6.printStackTrace();
            return false;
        }
    }
}
