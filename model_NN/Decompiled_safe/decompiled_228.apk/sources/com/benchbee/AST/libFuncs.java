package com.benchbee.AST;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.Date;

public class libFuncs {
    public static void toastShow(Handler handler, final Context context, final String noti) {
        handler.post(new Runnable() {
            public void run() {
                Toast.makeText(context, noti, 0).show();
            }
        });
    }

    public static void toastShow(Context context, String noti) {
        Toast.makeText(context, noti, 0).show();
    }

    static void sleep(int time) {
        try {
            Thread.sleep((long) time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static String sf(String format, float value) {
        if (value == 0.0f) {
            return "    --";
        }
        try {
            return String.format(format, Float.valueOf(value));
        } catch (Exception e) {
            return "   --";
        }
    }

    static int psInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            return 0;
        }
    }

    static float psFloat(String value) {
        try {
            return Float.parseFloat(value);
        } catch (Exception e) {
            return 0.0f;
        }
    }

    static void openResultBox(Context context, resultItem resultItem) {
        int idx = resultItem.date_idx;
        Intent resultBox = new Intent(context, resultBox.class);
        int idx2 = idx + 1;
        resultBox.putExtra("date", resultItem.results[idx]);
        int idx3 = idx2 + 1;
        resultBox.putExtra("mode", resultItem.results[idx2]);
        int idx4 = idx3 + 1;
        resultBox.putExtra("dn_avg", resultItem.results[idx3]);
        int idx5 = idx4 + 1;
        resultBox.putExtra("up_avg", resultItem.results[idx4]);
        int idx6 = idx5 + 1;
        resultBox.putExtra("blink_rate", resultItem.results[idx5]);
        int idx7 = idx6 + 1;
        resultBox.putExtra("ping_avg", resultItem.results[idx6]);
        int idx8 = idx7 + 1;
        resultBox.putExtra("ping_max", resultItem.results[idx7]);
        int idx9 = idx8 + 1;
        resultBox.putExtra("ping_min", resultItem.results[idx8]);
        int idx10 = idx9 + 1;
        resultBox.putExtra("ping_loss", resultItem.results[idx9]);
        int idx11 = idx10 + 1;
        resultBox.putExtra("consumed_data", resultItem.results[idx10]);
        int idx12 = idx11 + 1;
        resultBox.putExtra("network_type", resultItem.results[idx11]);
        resultBox.putExtra("signal_strength", resultItem.results[idx12]);
        resultBox.putExtra("area", resultItem.results[idx12 + 1]);
        resultBox.putExtra("db_id", resultItem.db_id);
        String Date = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(new Date(Long.parseLong(resultItem.results[resultItem.date_idx])));
        resultBox.addFlags(67108864);
        context.startActivity(resultBox);
    }
}
