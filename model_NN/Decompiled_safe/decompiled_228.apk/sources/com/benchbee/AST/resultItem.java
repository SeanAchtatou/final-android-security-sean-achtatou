package com.benchbee.AST;

public class resultItem {
    int area_idx = 12;
    int blinkrate_idx = 4;
    int consumedData_idx = 9;
    int date_idx = 0;
    int db_id = -1;
    int down_avg_idx = 2;
    int mobiletype_idx = 10;
    int mode_idx = 1;
    int ping_avg_idx = 5;
    int ping_loss_idx = 8;
    int ping_max_idx = 6;
    int ping_min_idx = 7;
    String[] results = new String[this.results_cnt];
    int results_cnt = 13;
    int signal_strength_idx = 11;
    int up_avg_idx = 3;

    public resultItem() {
        clean();
    }

    public void clean() {
        this.results[this.down_avg_idx] = "--";
        this.results[this.up_avg_idx] = "--";
        this.results[this.blinkrate_idx] = "--";
        this.results[this.ping_avg_idx] = "--";
        this.results[this.ping_max_idx] = "--";
        this.results[this.ping_min_idx] = "--";
        this.results[this.mobiletype_idx] = "--";
        this.results[this.ping_loss_idx] = "--";
        this.results[this.area_idx] = "위치 정보 획득 실패";
    }

    public String getMode() {
        return this.results[this.mode_idx];
    }

    public String getDate() {
        return this.results[this.date_idx];
    }

    public String getDownAvg() {
        return this.results[this.down_avg_idx];
    }

    public String getUpAvg() {
        return this.results[this.up_avg_idx];
    }

    public String getBlinkrate() {
        return this.results[this.blinkrate_idx];
    }

    public String getPing_avg() {
        return this.results[this.ping_avg_idx];
    }

    public String getPing_max() {
        return this.results[this.ping_max_idx];
    }

    public String getPing_min() {
        return this.results[this.ping_min_idx];
    }

    public String getPing_loss() {
        return this.results[this.ping_loss_idx];
    }

    public String getConsumdate() {
        return this.results[this.consumedData_idx];
    }

    public String getMobileType() {
        return this.results[this.mobiletype_idx];
    }

    public String getSignalstrength() {
        return this.results[this.signal_strength_idx];
    }

    public String getArea() {
        return this.results[this.area_idx];
    }
}
