package com.benchbee.AST;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;

public class gpsAdapter {
    Context context;
    controller control;
    String first;
    gpsListener_multi gps_multi;
    gpsListener_single gps_single;
    boolean isFinish = false;
    boolean isMultiProvider;
    boolean isNoProvider = false;
    Location location = null;
    LocationManager mgr;
    long minTime;
    boolean nextProvider = false;
    String second;

    public gpsAdapter(Context context2, controller control2) {
        this.context = context2;
        this.control = control2;
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context2);
        this.minTime = 8000;
        this.mgr = (LocationManager) context2.getSystemService("location");
    }

    public void startGPS() {
        this.nextProvider = false;
        this.isNoProvider = false;
        this.isFinish = false;
        Criteria criteria_first = new Criteria();
        criteria_first.setAccuracy(2);
        criteria_first.setCostAllowed(false);
        criteria_first.setPowerRequirement(1);
        criteria_first.setAltitudeRequired(false);
        this.first = this.mgr.getBestProvider(criteria_first, true);
        Criteria criteria_second = new Criteria();
        criteria_second.setAccuracy(1);
        criteria_second.setCostAllowed(false);
        criteria_second.setPowerRequirement(3);
        criteria_second.setAltitudeRequired(false);
        this.second = this.mgr.getBestProvider(criteria_second, true);
        if (this.first != null) {
            this.isMultiProvider = !this.first.equals(this.second);
            if (this.isMultiProvider) {
                this.gps_single = new gpsListener_single();
                this.gps_multi = new gpsListener_multi();
                this.mgr.requestLocationUpdates(this.first, this.minTime, 20.0f, this.gps_multi);
                return;
            }
            this.gps_single = new gpsListener_single();
            this.mgr.requestLocationUpdates(this.second, this.minTime, 20.0f, this.gps_single);
            return;
        }
        this.isNoProvider = true;
    }

    public void endEvent() {
        if (!this.isNoProvider && !this.isFinish) {
            this.isFinish = true;
            if (this.isMultiProvider && !this.nextProvider) {
                this.mgr.removeUpdates(this.gps_multi);
            }
            this.mgr.removeUpdates(this.gps_single);
        }
    }

    public void getLocationInfo(String provider) {
        if (provider != null) {
            this.location = this.mgr.getLastKnownLocation(provider);
        }
        if (this.location != null) {
            this.control.setGps(this.location.getLatitude(), this.location.getLongitude(), "", provider);
        }
    }

    public class gpsListener_multi implements LocationListener {
        public gpsListener_multi() {
        }

        public void onLocationChanged(Location location) {
            gpsAdapter.this.getLocationInfo(gpsAdapter.this.first);
            gpsAdapter.this.mgr.removeUpdates(this);
            gpsAdapter.this.nextProvider = true;
            gpsAdapter.this.mgr.requestLocationUpdates(gpsAdapter.this.second, gpsAdapter.this.minTime, 20.0f, gpsAdapter.this.gps_single);
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    public class gpsListener_single implements LocationListener {
        public gpsListener_single() {
        }

        public void onLocationChanged(Location location) {
            gpsAdapter.this.getLocationInfo(gpsAdapter.this.second);
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }
}
