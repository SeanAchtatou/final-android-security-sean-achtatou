package com.benchbee.AST;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import java.lang.reflect.Array;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class chartView extends View implements eventListener {
    Paint avg_paint_fill;
    Paint avg_paint_stroke;
    int before_eventNum;
    int blinkCnt = 0;
    Paint blink_paint_fill;
    float[] blinkrate;
    Thread chartView;
    float chart_height;
    float chart_width;
    Context context;
    float[] cur_avg_line;
    float cur_avg_y;
    float[] cur_blink_line;
    int cur_event;
    float[] cur_inst_line;
    float cur_inst_y;
    float cur_step_x;
    String cur_string_Max = " ";
    String cur_string_Range_Max = " ";
    boolean drawBlinkArea = false;
    int drop_frame = 3;
    int drop_weight;
    boolean end_flag = false;
    ScheduledExecutorService executor;
    int frame;
    Paint inst_paint_fill;
    Paint inst_paint_stroke;
    int[] int_MaxMbps = new int[this.sting_MaxMbps_cnt];
    int int_MaxPing;
    int[] int_RangeMaxMbps = new int[this.sting_MaxMbps_cnt];
    int int_RangeMaxPing;
    int limitRTT;
    float limit_coord_x;
    float limit_coord_y;
    float[] loss_line;
    int maxInst;
    int maxPingCnt;
    float max_Mbps;
    int max_Mbps_default;
    float max_Ping;
    int max_Ping_default;
    Paint paintMax;
    int period_time;
    Path pingPath;
    float[] ping_line;
    Paint ping_paint_stroke;
    float ping_scale_up;
    float ping_step_x;
    float pos_ping_y;
    float pos_time_x;
    Resources r;
    int rttCnt;
    float setMbps = 1000000.0f;
    boolean speedChart_flag;
    float[][] speed_line;
    float speed_scale_up;
    float speed_step_x;
    float start_coord_x;
    float start_coord_y;
    int sting_MaxMbps_cnt = 2;
    boolean stop_flag = false;
    float string_Max_x;
    float string_Max_y;
    float string_Range_Max_x;
    int total_line_cnt = 6;

    public chartView(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        this.context = context2;
        this.r = getResources();
        this.avg_paint_stroke = new Paint(1);
        this.avg_paint_fill = new Paint(1);
        this.inst_paint_stroke = new Paint(1);
        this.inst_paint_fill = new Paint(1);
        this.blink_paint_fill = new Paint(1);
        this.paintMax = new Paint(1);
        this.ping_paint_stroke = new Paint(1);
        this.paintMax.setTextSize(this.r.getDimension(R.dimen.text_Max));
        this.paintMax.setTypeface(Typeface.createFromAsset(context2.getAssets(), "fonts/dsdigit.ttf"));
        this.paintMax.setTextAlign(Paint.Align.RIGHT);
        this.string_Max_x = (float) this.r.getDimensionPixelSize(R.dimen.stringMax_x);
        this.string_Range_Max_x = (float) this.r.getDimensionPixelSize(R.dimen.stringRangeMax_x);
        this.string_Max_y = (float) this.r.getDimensionPixelSize(R.dimen.stringMax_y);
        this.limit_coord_x = (float) this.r.getDimensionPixelSize(R.dimen.chart_end_x);
        this.limit_coord_y = (float) this.r.getDimensionPixelSize(R.dimen.chart_end_y);
        this.start_coord_x = (float) this.r.getDimensionPixelSize(R.dimen.chart_start_x);
        this.start_coord_y = (float) this.r.getDimensionPixelSize(R.dimen.chart_start_y);
        this.chart_width = this.limit_coord_x - this.start_coord_x;
        this.chart_height = this.start_coord_y - this.limit_coord_y;
        this.pos_time_x = this.start_coord_x;
        this.cur_inst_y = this.start_coord_y;
        this.cur_avg_y = this.start_coord_y;
        this.avg_paint_stroke.setStyle(Paint.Style.STROKE);
        this.avg_paint_stroke.setARGB(255, 0, 0, 255);
        this.avg_paint_fill.setStyle(Paint.Style.STROKE);
        this.avg_paint_fill.setARGB(40, 0, 0, 255);
        this.inst_paint_stroke.setStyle(Paint.Style.STROKE);
        this.inst_paint_stroke.setARGB(255, 255, 0, 0);
        this.inst_paint_fill.setStyle(Paint.Style.STROKE);
        this.inst_paint_fill.setARGB(40, 255, 0, 0);
        this.blink_paint_fill.setStyle(Paint.Style.STROKE);
        this.blink_paint_fill.setARGB(50, 0, 0, 0);
        this.ping_paint_stroke.setARGB(255, 255, 0, 0);
        this.ping_paint_stroke.setStyle(Paint.Style.STROKE);
        this.speed_line = (float[][]) Array.newInstance(Float.TYPE, this.total_line_cnt, (int) this.chart_width);
        for (int i = 0; i < this.total_line_cnt; i++) {
            for (int j = 0; ((float) j) < this.chart_width; j++) {
                this.speed_line[i][j] = this.start_coord_y;
            }
        }
        this.frame = Integer.parseInt(this.r.getString(R.string.frame));
        this.max_Mbps_default = Integer.parseInt(this.r.getString(R.string.maxMbpsDef));
        this.max_Mbps = (float) this.max_Mbps_default;
        for (int i2 = 0; i2 < this.sting_MaxMbps_cnt; i2++) {
            this.int_RangeMaxMbps[i2] = this.max_Mbps_default;
        }
        this.limitRTT = Integer.parseInt(this.r.getString(R.string.limitRTT));
        this.max_Ping_default = Integer.parseInt(this.r.getString(R.string.maxPingDef));
        this.max_Ping = (float) this.max_Ping_default;
        this.int_RangeMaxPing = this.max_Ping_default;
        this.speed_scale_up = Float.parseFloat(this.r.getString(R.string.speed_scale_up));
        this.ping_scale_up = Float.parseFloat(this.r.getString(R.string.ping_scale_up));
        this.pingPath = new Path();
        this.pingPath.moveTo(this.start_coord_x, this.start_coord_y);
        this.cur_step_x = this.pos_time_x;
        this.blinkrate = new float[2];
        getAdminSetting();
    }

    public void startEvent(int eventNum) {
        this.cur_event = eventNum;
        this.before_eventNum = eventNum;
        if (eventNum != 3) {
            this.executor = Executors.newSingleThreadScheduledExecutor();
            this.executor.scheduleWithFixedDelay(new drawGraph(), 0, (long) this.frame, TimeUnit.MILLISECONDS);
        }
    }

    class drawGraph implements Runnable {
        drawGraph() {
        }

        public void run() {
            if (chartView.this.pos_time_x >= chartView.this.limit_coord_x - 10.0f || chartView.this.stop_flag) {
                chartView.this.executor.shutdown();
                return;
            }
            chartView.this.drop_weight++;
            if (chartView.this.drop_weight > chartView.this.drop_frame) {
                chartView.this.cur_inst_y += (float) (chartView.this.drop_weight - chartView.this.drop_frame);
            }
            while (chartView.this.pos_time_x < chartView.this.cur_step_x && chartView.this.pos_time_x < chartView.this.limit_coord_x) {
                chartView.this.cur_inst_line[(int) (chartView.this.pos_time_x - chartView.this.start_coord_x)] = chartView.this.cur_inst_y;
                chartView.this.cur_avg_line[(int) (chartView.this.pos_time_x - chartView.this.start_coord_x)] = chartView.this.cur_avg_y;
                if (chartView.this.drawBlinkArea) {
                    chartView.this.cur_blink_line[(int) (chartView.this.pos_time_x - chartView.this.start_coord_x)] = chartView.this.limit_coord_y;
                    chartView.this.blinkCnt++;
                }
                chartView.this.pos_time_x += 1.0f;
            }
            chartView.this.cur_step_x += chartView.this.speed_step_x;
            chartView.this.rePaint();
        }
    }

    public void stop() {
        this.stop_flag = true;
    }

    public void nextEvent(int eventNum) {
        if (!this.end_flag) {
            this.cur_event = eventNum;
            this.pos_time_x = this.start_coord_x;
            this.cur_inst_y = this.start_coord_y;
            this.cur_avg_y = this.start_coord_y;
            this.cur_step_x = this.pos_time_x;
            this.drawBlinkArea = false;
            if (this.before_eventNum != 3) {
                if (!this.executor.isShutdown()) {
                    this.executor.shutdownNow();
                }
                this.stop_flag = false;
                setSpeedMaxValue(this.before_eventNum);
            } else {
                setPingMaxValue();
            }
        }
        if (eventNum != 3) {
            this.executor = Executors.newSingleThreadScheduledExecutor();
            this.executor.scheduleWithFixedDelay(new drawGraph(), 0, (long) this.frame, TimeUnit.MILLISECONDS);
        }
        this.before_eventNum = eventNum;
    }

    public void setPingMaxValue() {
        this.int_MaxPing = this.maxInst;
        this.maxInst = 0;
        this.int_RangeMaxPing = (int) this.max_Ping;
    }

    public void setSpeedMaxValue(int eventNum) {
        this.int_RangeMaxMbps[eventNum - 1] = (int) this.max_Mbps;
        this.max_Mbps = (float) this.max_Mbps_default;
        this.int_MaxMbps[eventNum - 1] = this.maxInst;
        this.maxInst = 0;
        calculateBlinkRate(eventNum - 1);
        this.blinkCnt = 0;
    }

    public void calculateBlinkRate(int idx) {
        this.blinkrate[idx] = (((float) this.blinkCnt) / this.chart_width) * 100.0f;
    }

    public void endEvent() {
        this.end_flag = true;
        if (this.cur_event < 3) {
            setSpeedMaxValue(this.cur_event);
        } else {
            setPingMaxValue();
        }
        this.pos_time_x = this.limit_coord_x;
    }

    public void changeEvent(eventItem items) {
        if (items.num == 3) {
            pingChart(items);
        } else {
            speedChart(items);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.speedChart_flag) {
            canvas.drawPath(this.pingPath, this.ping_paint_stroke);
            for (int i = 0; i < this.rttCnt; i++) {
                if (this.loss_line[i] != 0.0f) {
                    canvas.drawLine(this.loss_line[i], this.start_coord_y, this.loss_line[i], this.limit_coord_y, this.inst_paint_stroke);
                }
            }
        } else {
            for (int i2 = 0; ((float) i2) < (this.pos_time_x - this.start_coord_x) - 1.0f; i2++) {
                Canvas canvas2 = canvas;
                canvas2.drawLine(((float) i2) + this.start_coord_x, this.cur_inst_line[i2], this.start_coord_x + ((float) i2) + 1.0f, this.cur_inst_line[i2 + 1], this.inst_paint_stroke);
                Canvas canvas3 = canvas;
                canvas3.drawLine(((float) i2) + this.start_coord_x, this.cur_inst_line[i2], ((float) i2) + this.start_coord_x, this.start_coord_y, this.inst_paint_fill);
                Canvas canvas4 = canvas;
                canvas4.drawLine(((float) i2) + this.start_coord_x, this.cur_avg_line[i2], this.start_coord_x + ((float) i2) + 1.0f, this.cur_avg_line[i2 + 1], this.avg_paint_stroke);
                Canvas canvas5 = canvas;
                canvas5.drawLine(((float) i2) + this.start_coord_x, this.cur_avg_line[i2], ((float) i2) + this.start_coord_x, this.start_coord_y, this.avg_paint_fill);
                Canvas canvas6 = canvas;
                canvas6.drawLine(((float) i2) + this.start_coord_x, this.cur_blink_line[i2], ((float) i2) + this.start_coord_x, this.start_coord_y, this.blink_paint_fill);
            }
        }
        canvas.drawText(this.cur_string_Max, this.string_Max_x, this.string_Max_y, this.paintMax);
        canvas.drawText(this.cur_string_Range_Max, this.string_Range_Max_x, this.string_Max_y, this.paintMax);
    }

    public void setSpeedChart(int chartNum) {
        this.speedChart_flag = true;
        int lineNum = chartNum * 3;
        int lineNum2 = lineNum + 1;
        this.cur_inst_line = this.speed_line[lineNum];
        this.cur_avg_line = this.speed_line[lineNum2];
        this.cur_blink_line = this.speed_line[lineNum2 + 1];
        this.cur_string_Max = Integer.toString(this.int_MaxMbps[chartNum]);
        this.cur_string_Range_Max = Integer.toString(this.int_RangeMaxMbps[chartNum]);
        setBackgroundResource(R.drawable.chart_view_speed);
        rePaint();
    }

    public void setPingChart() {
        this.speedChart_flag = false;
        this.cur_string_Max = Integer.toString(this.int_MaxPing);
        this.cur_string_Range_Max = Integer.toString(this.int_RangeMaxPing);
        setBackgroundResource(R.drawable.chart_view_ping);
        rePaint();
    }

    public void speedChart(eventItem items) {
        if (items.inst < 0.0f) {
            this.drawBlinkArea = true;
            this.cur_inst_y = this.start_coord_y;
            this.cur_avg_y = this.start_coord_y - ((this.chart_height * (items.avg / this.setMbps)) / this.max_Mbps);
            return;
        }
        this.drawBlinkArea = false;
        this.drop_weight = 0;
        float inst_Mbps = items.inst / this.setMbps;
        float avg_Mbps = items.avg / this.setMbps;
        checkScaleSpeed(inst_Mbps > avg_Mbps ? inst_Mbps : avg_Mbps);
        this.cur_inst_y = this.start_coord_y - ((this.chart_height * inst_Mbps) / this.max_Mbps);
        this.cur_avg_y = this.start_coord_y - ((this.chart_height * avg_Mbps) / this.max_Mbps);
        if (((int) inst_Mbps) > this.maxInst) {
            this.maxInst = (int) inst_Mbps;
            this.cur_string_Max = Integer.toString(this.maxInst);
        }
    }

    public void pingChart(eventItem items) {
        if (!this.end_flag) {
            checkScalePing(items.inst);
            if (this.rttCnt < items.ping_idx) {
                int lossCnt = items.ping_idx - this.rttCnt;
                for (int i = 0; i < lossCnt; i++) {
                    float[] fArr = this.loss_line;
                    int i2 = this.rttCnt;
                    this.rttCnt = i2 + 1;
                    fArr[i2] = this.pos_time_x;
                    this.pingPath.lineTo(this.pos_time_x, this.start_coord_y);
                    this.pos_time_x += this.ping_step_x;
                }
            }
            if (this.rttCnt != this.maxPingCnt) {
                this.pos_ping_y = this.start_coord_y - ((items.inst * this.chart_height) / this.max_Ping);
                this.ping_line[this.rttCnt] = this.pos_ping_y;
                this.pingPath.lineTo(this.pos_time_x, this.pos_ping_y);
                this.pos_time_x += this.ping_step_x;
                this.rttCnt++;
                if (((int) items.inst) > this.maxInst) {
                    this.maxInst = (int) items.inst;
                    this.cur_string_Max = Integer.toString(this.maxInst);
                }
                rePaint();
            }
        }
    }

    public void checkScaleSpeed(float speed_Mbps) {
        if (speed_Mbps >= this.max_Mbps) {
            float setScale = speed_Mbps * this.speed_scale_up;
            if (this.max_Mbps == ((float) this.max_Mbps_default)) {
                setScaleSpeed((float) (this.max_Mbps_default * 5));
            } else {
                setScaleSpeed((float) (Math.ceil((double) (setScale / 10.0f)) * 10.0d));
            }
        }
    }

    public void checkScalePing(float inst_ping) {
        if (inst_ping > this.max_Ping) {
            setScalePing((float) (Math.ceil((double) ((inst_ping * this.ping_scale_up) / 10.0f)) * 10.0d));
        }
    }

    public void setScaleSpeed(float maxMbps) {
        if (!this.end_flag) {
            float rateMbps = this.max_Mbps / maxMbps;
            for (int i = 0; ((float) i) < this.pos_time_x - this.start_coord_x; i++) {
                this.cur_inst_line[i] = this.start_coord_y - ((this.start_coord_y - this.cur_inst_line[i]) * rateMbps);
                this.cur_avg_line[i] = this.start_coord_y - ((this.start_coord_y - this.cur_avg_line[i]) * rateMbps);
            }
            this.max_Mbps = maxMbps;
            this.cur_string_Range_Max = Integer.toString((int) this.max_Mbps);
        }
    }

    public void setScalePing(float maxPing) {
        pathClear();
        float ratePing = this.max_Ping / maxPing;
        float pos_ping_x = this.start_coord_x;
        for (int i = 0; i < this.rttCnt; i++) {
            this.ping_line[i] = this.start_coord_y - ((this.start_coord_y - this.ping_line[i]) * ratePing);
            this.pingPath.lineTo(pos_ping_x, this.ping_line[i]);
            pos_ping_x += this.ping_step_x;
        }
        this.max_Ping = maxPing;
        this.cur_string_Range_Max = Integer.toString((int) this.max_Ping);
    }

    public void pathClear() {
        this.pingPath.reset();
        this.pingPath.moveTo(this.start_coord_x, this.start_coord_y);
    }

    public void clean() {
        initFlag();
        for (int i = 0; i < this.total_line_cnt; i++) {
            for (int j = 0; ((float) j) < this.chart_width; j++) {
                this.speed_line[i][j] = this.start_coord_y;
            }
        }
        for (int i2 = 0; i2 < this.maxPingCnt; i2++) {
            this.ping_line[i2] = this.start_coord_y;
            this.loss_line[i2] = 0.0f;
        }
        for (int i3 = 0; i3 < this.sting_MaxMbps_cnt; i3++) {
            this.blinkrate[i3] = 0.0f;
            this.int_MaxMbps[i3] = 0;
            this.int_RangeMaxMbps[i3] = this.max_Mbps_default;
        }
        this.pos_time_x = this.start_coord_x;
        this.cur_inst_y = this.start_coord_y;
        this.cur_avg_y = this.start_coord_y;
        this.cur_step_x = this.pos_time_x;
        this.max_Ping = (float) this.max_Ping_default;
        this.int_MaxPing = 0;
        this.int_RangeMaxPing = this.max_Ping_default;
        this.rttCnt = 0;
        pathClear();
        getAdminSetting();
    }

    public void initFlag() {
        this.drawBlinkArea = false;
        this.stop_flag = false;
        this.end_flag = false;
    }

    public void setColor(boolean turn) {
        if (turn) {
            this.avg_paint_fill.setAlpha(80);
            this.inst_paint_fill.setAlpha(80);
            this.ping_paint_stroke.setStrokeWidth(1.5f);
        } else {
            this.avg_paint_fill.setAlpha(40);
            this.inst_paint_fill.setAlpha(40);
            this.ping_paint_stroke.setStrokeWidth(1.0f);
        }
        rePaint();
    }

    public void rePaint() {
        postInvalidate();
    }

    public void getAdminSetting() {
        SharedPreferences admin = PreferenceManager.getDefaultSharedPreferences(this.context);
        this.maxPingCnt = Integer.parseInt(admin.getString("list_pingcnt", "100"));
        this.ping_line = new float[this.maxPingCnt];
        for (int i = 0; i < this.maxPingCnt; i++) {
            this.ping_line[i] = this.start_coord_y;
        }
        this.loss_line = new float[this.maxPingCnt];
        this.cur_inst_line = this.speed_line[0];
        this.cur_avg_line = this.speed_line[1];
        this.cur_blink_line = this.speed_line[2];
        this.period_time = Integer.parseInt(admin.getString("list_period", "15000"));
        this.speed_step_x = (this.limit_coord_x - this.start_coord_x) / ((float) (this.period_time / this.frame));
        this.ping_step_x = (this.limit_coord_x - this.start_coord_x) / ((float) this.maxPingCnt);
    }
}
