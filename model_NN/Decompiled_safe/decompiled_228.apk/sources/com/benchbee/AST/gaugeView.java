package com.benchbee.AST;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class gaugeView extends SurfaceView implements SurfaceHolder.Callback, eventListener {
    public static final String USER_PREFERENCE = "USER_PREFERENCES";
    public static final int center_angle = 113;
    public static final int max_angle = 226;
    public static final int scale_angle = 56;
    public static final int start_angle = -113;
    boolean PREF_GAUGE_AVG;
    float angle = 0.0f;
    Bitmap background;
    Bitmap background_ping;
    float beforeAngle = 0.0f;
    int current_chartNum = 0;
    boolean end_flag = false;
    int frame;
    Bitmap gauge_Hand_left;
    Bitmap gauge_Hand_right;
    int gauge_hand_x;
    int gauge_x;
    int gauge_y;
    int idx = 0;
    int movingCnt = this.movingWeight;
    int movingDnCnt = this.movingWeight;
    int movingUpCnt = this.movingWeight;
    int movingWeight = 1;
    Paint paintGauge;
    Paint paintMbps;
    Paint paintPing;
    Paint paintPingBack;
    float ping_scale1 = 0.0f;
    float ping_scale2 = 100.0f;
    float ping_scale3 = 200.0f;
    float ping_scale4 = 300.0f;
    float ping_scale5 = 1000.0f;
    SharedPreferences prefs;
    int scale1 = 0;
    int scale2 = 1;
    int scale3 = 5;
    int scale4 = 25;
    int scale5 = 50;
    float setMbps = 1000000.0f;
    boolean speedChart_flag = false;
    String[] string_Mbps;
    int string_Mbps_cnt = 2;
    String string_Ping;
    SurfaceHolder surholder = getHolder();
    float textSizeMbps;
    float textSizePing;

    public gaugeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.surholder.setFormat(-3);
        this.surholder.addCallback(this);
        this.background = BitmapFactory.decodeResource(getResources(), R.drawable.gauge_view_mbps);
        this.background_ping = BitmapFactory.decodeResource(getResources(), R.drawable.gauge_view_ping);
        Resources r = getResources();
        this.paintMbps = new Paint(1);
        this.paintGauge = new Paint(1);
        this.paintPing = new Paint(1);
        this.paintPingBack = new Paint(1);
        this.gauge_x = r.getDimensionPixelSize(R.dimen.gauge_x);
        this.gauge_y = r.getDimensionPixelSize(R.dimen.gauge_y);
        this.gauge_hand_x = r.getDimensionPixelSize(R.dimen.gauge_hand_x);
        this.textSizeMbps = r.getDimension(R.dimen.text_Mbps);
        this.textSizePing = r.getDimension(R.dimen.text_Ping);
        this.frame = Integer.parseInt(r.getString(R.string.frame));
        Typeface dsdigit = Typeface.createFromAsset(context.getAssets(), "fonts/dsdigit.ttf");
        this.paintMbps.setTextSize(this.textSizeMbps);
        this.paintMbps.setTypeface(dsdigit);
        this.paintMbps.setColor(-16777216);
        this.paintPing.setTextSize(this.textSizePing);
        this.paintPing.setTypeface(dsdigit);
        this.paintPing.setColor(-16777216);
        this.string_Mbps = new String[this.string_Mbps_cnt];
        for (int i = 0; i < this.string_Mbps_cnt; i++) {
            this.string_Mbps[i] = "0.00";
        }
        this.string_Ping = "0.00";
        this.gauge_Hand_left = BitmapFactory.decodeResource(r, R.drawable.st_guage_niddle_ls);
        this.gauge_Hand_right = BitmapFactory.decodeResource(r, R.drawable.st_guage_niddle_rs);
        this.prefs = context.getSharedPreferences("USER_PREFERENCES", 0);
        this.PREF_GAUGE_AVG = this.prefs.getBoolean("pref_avg", true);
    }

    public void setGaugeMode(SharedPreferences prefs2) {
        this.PREF_GAUGE_AVG = prefs2.getBoolean("pref_avg", true);
    }

    public void startEvent(int eventNum) {
    }

    public void endEvent() {
        this.end_flag = true;
        gaugeDown();
    }

    public void gaugeDown() {
        if (((int) this.angle) > 0) {
            new Thread(new Runnable() {
                public void run() {
                    int stopCnt = 0;
                    float beforeAngle = gaugeView.this.angle;
                    while (((int) gaugeView.this.angle) > 0 && stopCnt < 3) {
                        if (beforeAngle <= gaugeView.this.angle) {
                            stopCnt++;
                        }
                        float remainder = gaugeView.this.angle / 10.0f;
                        beforeAngle = gaugeView.this.angle;
                        gaugeView.this.angle -= remainder;
                        gaugeView.this.rePaint();
                    }
                }
            }).start();
        }
    }

    public void changeEvent(eventItem items) {
        if (this.speedChart_flag) {
            float inst_Mbps = items.inst / this.setMbps;
            float avg_Mbps = items.avg / this.setMbps;
            if (((double) avg_Mbps) >= 10.0d && ((double) avg_Mbps) < 100.0d) {
                this.string_Mbps[this.current_chartNum] = String.format("%2.1f", Float.valueOf(avg_Mbps));
            } else if (((double) avg_Mbps) >= 100.0d) {
                this.string_Mbps[this.current_chartNum] = String.format("%4.0f", Float.valueOf(avg_Mbps));
            } else {
                this.string_Mbps[this.current_chartNum] = String.format("%2.2f", Float.valueOf(avg_Mbps));
            }
            if (this.PREF_GAUGE_AVG) {
                calculateAngle(avg_Mbps);
            } else {
                calculateAngle(inst_Mbps);
            }
            accelerationEngine();
        } else {
            if (((double) items.avg) >= 10.0d && ((double) items.avg) < 100.0d) {
                this.string_Ping = String.format("%2.1f", Float.valueOf(items.avg));
            } else if (((double) items.avg) >= 100.0d) {
                this.string_Ping = String.format("%4.0f", Float.valueOf(items.avg));
            } else {
                this.string_Ping = String.format("%2.2f", Float.valueOf(items.avg));
            }
            calculatePingAngle(items.avg);
            accelerationEngine();
        }
        rePaint();
    }

    public void accelerationEngine() {
        if (this.angle - this.beforeAngle > ((float) ((this.movingUpCnt + 1) * (this.movingUpCnt + 1)))) {
            this.angle = this.beforeAngle + ((float) (this.movingUpCnt * this.movingUpCnt));
            rePaint();
            this.movingUpCnt++;
            this.movingDnCnt = this.movingWeight;
            this.angle = this.beforeAngle + ((float) (this.movingUpCnt * this.movingUpCnt));
        } else if (this.beforeAngle - this.angle > ((float) ((this.movingDnCnt + 1) * (this.movingDnCnt + 1)))) {
            this.angle = this.beforeAngle - ((float) (this.movingDnCnt * this.movingDnCnt));
            if (this.angle < 0.0f) {
                this.angle = 0.0f;
            }
            rePaint();
            this.movingDnCnt++;
            this.movingUpCnt = this.movingWeight;
            this.angle = this.beforeAngle - ((float) (this.movingDnCnt * this.movingDnCnt));
            if (this.angle < 0.0f) {
                this.angle = 0.0f;
            }
        } else {
            this.movingUpCnt = this.movingWeight;
            this.movingDnCnt = this.movingWeight;
        }
        this.beforeAngle = this.angle;
    }

    public void rePaint() {
        Canvas canvas = this.surholder.lockCanvas();
        if (canvas != null) {
            try {
                onDraw(canvas);
            } catch (Throwable th) {
                if (canvas != null) {
                    this.surholder.unlockCanvasAndPost(canvas);
                }
                throw th;
            }
        }
        if (canvas != null) {
            this.surholder.unlockCanvasAndPost(canvas);
        }
    }

    public void nextEvent(int eventNum) {
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.speedChart_flag) {
            canvas.drawBitmap(this.background, 0.0f, 0.0f, (Paint) null);
        } else {
            canvas.drawBitmap(this.background_ping, 0.0f, 0.0f, (Paint) null);
        }
        if (this.speedChart_flag && this.string_Mbps[this.current_chartNum] != null && this.paintMbps != null) {
            canvas.drawText(this.string_Mbps[this.current_chartNum], 158.0f, 172.0f, this.paintMbps);
        } else if (!(this.speedChart_flag || this.string_Ping == null || this.paintPing == null)) {
            canvas.drawText(this.string_Ping, 158.0f, 172.0f, this.paintPing);
        }
        canvas.translate((float) this.gauge_x, (float) this.gauge_y);
        canvas.rotate(this.angle - 0.03857422f);
        if (!(this.gauge_Hand_left == null || this.gauge_Hand_right == null)) {
            if (this.angle > 113.0f) {
                canvas.drawBitmap(this.gauge_Hand_right, (float) ((-this.gauge_Hand_right.getWidth()) / 2), (float) (this.gauge_hand_x - this.gauge_Hand_right.getHeight()), this.paintGauge);
            } else {
                canvas.drawBitmap(this.gauge_Hand_left, (float) ((-this.gauge_Hand_left.getWidth()) / 2), (float) (this.gauge_hand_x - this.gauge_Hand_left.getHeight()), this.paintGauge);
            }
        }
        canvas.rotate(-(this.angle - 0.03857422f));
    }

    public void setSpeedChart(int chartNum) {
        this.speedChart_flag = true;
        this.current_chartNum = chartNum;
        if (this.end_flag) {
            rePaint();
        }
    }

    public void setPingChart() {
        this.speedChart_flag = false;
        if (this.end_flag) {
            rePaint();
        }
    }

    public void calculateAngle(float speed_Mbps) {
        if (speed_Mbps > ((float) this.scale5)) {
            this.angle = 226.0f;
        } else if (speed_Mbps > ((float) this.scale4) && speed_Mbps <= ((float) this.scale5)) {
            this.angle = 168.0f + (((speed_Mbps - ((float) this.scale4)) * 56.0f) / ((float) (this.scale5 - this.scale4)));
        } else if (speed_Mbps > ((float) this.scale3) && speed_Mbps <= ((float) this.scale4)) {
            this.angle = 112.0f + (((speed_Mbps - ((float) this.scale3)) * 56.0f) / ((float) (this.scale4 - this.scale3)));
        } else if (speed_Mbps > ((float) this.scale2) && speed_Mbps <= ((float) this.scale3)) {
            this.angle = (((speed_Mbps - ((float) this.scale2)) * 56.0f) / ((float) (this.scale3 - this.scale2))) + 56.0f;
        } else if (speed_Mbps <= ((float) this.scale1) || speed_Mbps > ((float) this.scale2)) {
            this.angle = 0.0f;
        } else {
            this.angle = (speed_Mbps * 56.0f) / ((float) this.scale2);
        }
    }

    public void calculatePingAngle(float avg_ping_rtt) {
        if (avg_ping_rtt > this.ping_scale5) {
            this.angle = 226.0f;
        } else if (avg_ping_rtt > this.ping_scale4 && avg_ping_rtt <= this.ping_scale5) {
            this.angle = 168.0f + (((avg_ping_rtt - this.ping_scale4) * 56.0f) / (this.ping_scale5 - this.ping_scale4));
        } else if (avg_ping_rtt > this.ping_scale3 && avg_ping_rtt <= this.ping_scale4) {
            this.angle = 112.0f + (((avg_ping_rtt - this.ping_scale3) * 56.0f) / (this.ping_scale4 - this.ping_scale3));
        } else if (avg_ping_rtt > this.ping_scale2 && avg_ping_rtt <= this.ping_scale3) {
            this.angle = (((avg_ping_rtt - this.ping_scale2) * 56.0f) / (this.ping_scale3 - this.ping_scale2)) + 56.0f;
        } else if (avg_ping_rtt <= this.ping_scale1 || avg_ping_rtt > this.ping_scale2) {
            this.angle = 0.0f;
        } else {
            this.angle = (avg_ping_rtt * 56.0f) / this.ping_scale2;
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.background = Bitmap.createScaledBitmap(this.background, getWidth(), getHeight(), true);
        Canvas canvas = this.surholder.lockCanvas();
        if (canvas != null) {
            try {
                onDraw(canvas);
            } catch (Throwable th) {
                if (canvas != null) {
                    this.surholder.unlockCanvasAndPost(canvas);
                }
                throw th;
            }
        }
        if (canvas != null) {
            this.surholder.unlockCanvasAndPost(canvas);
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    public void clean() {
        initFlag();
    }

    public void initFlag() {
        this.end_flag = false;
        this.speedChart_flag = true;
    }
}
