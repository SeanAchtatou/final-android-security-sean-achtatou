package com.benchbee.AST;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class admin extends PreferenceActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.admin);
    }
}
