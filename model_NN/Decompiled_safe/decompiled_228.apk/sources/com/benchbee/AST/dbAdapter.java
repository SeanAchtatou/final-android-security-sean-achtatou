package com.benchbee.AST;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class dbAdapter {
    private static final String DATABASE_NAME = "benchbeeDB";
    private static final int DATABASE_VERSION = 2;
    public static final String SQL_CREATE_BENCHBEE = "CREATE TABLE BENCHBEE (id INTEGER PRIMARY KEY AUTOINCREMENT , date TEXT, mode TEXT, down TEXT, up TEXT, blink TEXT, ping TEXT, max TEXT, min TEXT, loss TEXT, consume TEXT, type TEXT, signal TEXT, area TEXT)";
    /* access modifiers changed from: private */
    public static String SQL_TABLE_CREATE;
    /* access modifiers changed from: private */
    public static String TABLE_NAME;
    private final Context mCxt;
    private DatabaseHelper mHelper;
    private SQLiteDatabase mdb;

    private static class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context) {
            super(context, dbAdapter.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, (int) dbAdapter.DATABASE_VERSION);
        }

        public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        public void onOpen(SQLiteDatabase db) {
            super.onOpen(db);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(dbAdapter.SQL_TABLE_CREATE);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + dbAdapter.TABLE_NAME);
            onCreate(db);
        }
    }

    public dbAdapter(Context cxt, String sql, String tableName) {
        this.mCxt = cxt;
        SQL_TABLE_CREATE = sql;
        TABLE_NAME = tableName;
    }

    public dbAdapter open() throws SQLException {
        this.mHelper = new DatabaseHelper(this.mCxt);
        this.mdb = this.mHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.mHelper.close();
    }

    public long insertTable(ContentValues values) {
        return this.mdb.insert(TABLE_NAME, null, values);
    }

    public boolean deleteTable(String pkColumn, long pkData) {
        return this.mdb.delete(TABLE_NAME, new StringBuilder(String.valueOf(pkColumn)).append("=").append(pkData).toString(), null) > 0;
    }

    public boolean deleteTable(String pkColumn, String pkData) {
        return this.mdb.delete(TABLE_NAME, new StringBuilder(String.valueOf(pkColumn)).append("=").append(pkData).toString(), null) > 0;
    }

    public boolean deleteAllTable() {
        return this.mdb.delete(TABLE_NAME, null, null) > 0;
    }

    public Cursor selectTable(String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        return this.mdb.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy);
    }

    public boolean updateTable(ContentValues args, String pkColumn, long pkData) {
        return this.mdb.update(TABLE_NAME, args, new StringBuilder(String.valueOf(pkColumn)).append("=").append(pkColumn).toString(), null) > 0;
    }
}
