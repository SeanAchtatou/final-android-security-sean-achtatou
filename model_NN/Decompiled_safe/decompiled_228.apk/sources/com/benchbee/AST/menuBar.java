package com.benchbee.AST;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

public class menuBar extends LinearLayout implements View.OnClickListener {
    String className;
    Context context;
    ToggleButton menu_about;
    ToggleButton menu_check = ((ToggleButton) findViewById(R.id.menu_check));
    ToggleButton menu_result;
    ToggleButton menu_setting;
    ToggleButton menu_statistics;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.benchbee.AST.menuBar, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public menuBar(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        this.context = context2;
        LayoutInflater.from(context2).inflate((int) R.layout.menubar, (ViewGroup) this, true);
        this.className = ((Activity) context2).getLocalClassName();
        this.menu_check.setOnClickListener(this);
        if (this.className.equals("check")) {
            this.menu_check.setClickable(false);
            this.menu_check.setChecked(true);
        }
        this.menu_result = (ToggleButton) findViewById(R.id.menu_result);
        this.menu_result.setOnClickListener(this);
        if (this.className.equals("result")) {
            this.menu_result.setClickable(false);
            this.menu_result.setChecked(true);
        }
        this.menu_setting = (ToggleButton) findViewById(R.id.menu_setting);
        this.menu_setting.setOnClickListener(this);
        if (this.className.equals("preferences")) {
            this.menu_setting.setClickable(false);
            this.menu_setting.setChecked(true);
        }
        this.menu_about = (ToggleButton) findViewById(R.id.menu_about);
        this.menu_about.setOnClickListener(this);
        if (this.className.equals("about")) {
            this.menu_about.setClickable(false);
            this.menu_about.setChecked(true);
        }
        this.menu_statistics = (ToggleButton) findViewById(R.id.menu_statistics);
        this.menu_statistics.setOnClickListener(this);
        if (this.className.equals("statistics")) {
            this.menu_statistics.setClickable(false);
            this.menu_statistics.setChecked(true);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu_check:
                ((Activity) this.context).onBackPressed();
                return;
            case R.id.menu_result:
                this.menu_result.setChecked(false);
                Intent result = new Intent(this.context, result.class);
                result.addFlags(67108864);
                this.context.startActivity(result);
                if (!this.className.equals("check")) {
                    ((Activity) this.context).finish();
                    return;
                }
                return;
            case R.id.menu_statistics:
                this.menu_statistics.setChecked(false);
                Intent statistics = new Intent(this.context, statistics.class);
                statistics.addFlags(67108864);
                this.context.startActivity(statistics);
                if (!this.className.equals("check")) {
                    ((Activity) this.context).finish();
                    return;
                }
                return;
            case R.id.menu_setting:
                this.menu_setting.setChecked(false);
                Intent preferences = new Intent(this.context, preferences.class);
                preferences.addFlags(67108864);
                this.context.startActivity(preferences);
                if (!this.className.equals("check")) {
                    ((Activity) this.context).finish();
                    return;
                }
                return;
            case R.id.menu_about:
                this.menu_about.setChecked(false);
                Intent about = new Intent(this.context, about.class);
                about.addFlags(67108864);
                this.context.startActivity(about);
                if (!this.className.equals("check")) {
                    ((Activity) this.context).finish();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
