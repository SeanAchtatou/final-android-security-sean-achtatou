package com.benchbee.AST;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class patchInfo extends Activity implements View.OnClickListener {
    Button info_button;
    TextView info_text;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.patch);
        this.info_button = (Button) findViewById(R.id.infoBoxButton);
        this.info_button.setOnClickListener(this);
        this.info_text = (TextView) findViewById(R.id.infoBox);
        setPatchInfo_1();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.infoBoxButton:
                finish();
                return;
            default:
                return;
        }
    }

    public void onBackPressed() {
        finish();
    }

    public void setPatchInfo_1() {
        this.info_text.setText(getResources().getString(R.string.patchInfo));
    }
}
