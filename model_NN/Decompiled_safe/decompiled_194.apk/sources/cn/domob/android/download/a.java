package cn.domob.android.download;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import cn.domob.android.ads.Constants;
import java.io.File;

class a extends Handler {
    private /* synthetic */ AppExchangeDownloader a;

    a(AppExchangeDownloader appExchangeDownloader) {
        this.a = appExchangeDownloader;
    }

    public final void handleMessage(Message msg) {
        super.handleMessage(msg);
        if (msg.what - this.a.j >= 5 || msg.what == 100) {
            this.a.j = msg.what;
            this.a.e = msg.what;
            this.a.b.setLatestEventInfo(AppExchangeDownloader.a, String.valueOf(this.a.l) + "正在下载", "已下载" + msg.what + "%", this.a.n);
            this.a.c.notify(this.a.d, this.a.b);
            if (this.a.e == 100) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, String.valueOf(this.a.l) + " download success");
                }
                File file = new File(this.a.g);
                this.a.g = this.a.g.substring(0, this.a.g.length() - ".temp".length());
                file.renameTo(new File(this.a.g));
                AppExchangeDownloader.Download_Map.remove(this.a.m);
                if (this.a.f != null) {
                    this.a.f.onDownloadSuccess(this.a.g);
                }
                this.a.e = 0;
                this.a.b.icon = 17301634;
                this.a.b.tickerText = String.valueOf(this.a.l) + "下载完毕";
                this.a.b.flags = 16;
                this.a.n = PendingIntent.getActivity(AppExchangeDownloader.a, this.a.d, AppExchangeDownloader.a(this.a.g), 134217728);
                this.a.b.setLatestEventInfo(AppExchangeDownloader.a, String.valueOf(this.a.l) + "下载完毕", "", this.a.n);
                this.a.c.notify(this.a.d, this.a.b);
                Intent a2 = AppExchangeDownloader.a(this.a.g);
                a2.addFlags(268435456);
                AppExchangeDownloader.a.startActivity(a2);
            }
        }
    }
}
