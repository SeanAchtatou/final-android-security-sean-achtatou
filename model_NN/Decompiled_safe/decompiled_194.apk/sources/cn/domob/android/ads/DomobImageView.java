package cn.domob.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import cn.domob.android.ads.giftool.GifView;

public class DomobImageView {
    private ImageView a = null;
    private GifView b = null;
    private int c = 0;
    private Context d = null;

    public DomobImageView(Context context, int type) {
        this.d = context;
        this.c = type;
        switch (this.c) {
            case 0:
            case 2:
                this.a = new ImageView(this.d);
                return;
            case 1:
                Looper.prepare();
                this.b = new GifView(this.d);
                this.b.setGifImageType(GifView.GifImageType.WAIT_FINISH);
                return;
            default:
                return;
        }
    }

    public View getCurrentView() {
        switch (this.c) {
            case 0:
            case 2:
                return this.a;
            case 1:
                return this.b;
            default:
                return null;
        }
    }

    public void setScaleType(ImageView.ScaleType scaleType) {
        if (this.a != null) {
            this.a.setScaleType(scaleType);
        }
    }

    public void setImageBitmap(Bitmap bitmap, byte[] data) {
        switch (this.c) {
            case 0:
            case 2:
                this.a.setImageBitmap(bitmap);
                return;
            case 1:
                if (data != null) {
                    this.b.setGifImage(data);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
