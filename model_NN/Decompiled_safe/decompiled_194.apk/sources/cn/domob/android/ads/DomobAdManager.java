package cn.domob.android.ads;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import java.util.GregorianCalendar;

public class DomobAdManager {
    public static final String ACTION_AUDIO = "audio";
    public static final String ACTION_CALL = "call";
    public static final String ACTION_IN_APP = "inapp";
    public static final String ACTION_MAIL = "mail";
    public static final String ACTION_MAP = "map";
    public static final String ACTION_MARKET = "market";
    public static final String ACTION_SMS = "sms";
    public static final String ACTION_URL = "url";
    public static final String ACTION_VIDEO = "video";
    public static final String GENDER_FEMALE = "f";
    public static final String GENDER_MALE = "m";
    public static final String TEST_EMULATOR = "emulator";
    private static String a;
    private static int b;
    private static String c;
    private static String d;
    private static Boolean e = null;
    private static boolean f = true;
    private static String g;
    private static String h;
    private static String i;
    private static Location j;
    private static boolean k = false;
    private static boolean l = false;
    private static long m = 0;
    private static String n;
    private static String o;
    private static GregorianCalendar p;
    private static boolean q = false;

    protected static synchronized void a(Context context) {
        PackageInfo packageInfo;
        ApplicationInfo applicationInfo;
        synchronized (DomobAdManager.class) {
            if (!q) {
                q = true;
                try {
                    PackageManager packageManager = context.getPackageManager();
                    if (!(packageManager == null || (applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 128)) == null)) {
                        if (applicationInfo.metaData != null) {
                            try {
                                String string = applicationInfo.metaData.getString("DOMOB_PID");
                                if (c == null && string != null) {
                                    Log.i(Constants.LOG, "Publisher ID read from AndroidManifest.xml is " + string);
                                    setPublisherId(string);
                                }
                                if (e == null && applicationInfo.metaData.containsKey("DOMOB_TEST_MODE")) {
                                    Boolean valueOf = Boolean.valueOf(applicationInfo.metaData.getBoolean("DOMOB_TEST_MODE", true));
                                    Log.i(Constants.LOG, "Test Mode read from AndroidManifest.xml is " + valueOf);
                                    setIsTestMode(valueOf.booleanValue());
                                }
                                if (!l) {
                                    k = applicationInfo.metaData.getBoolean("DOMOB_ALLOW_LOCATION", false);
                                }
                            } catch (Exception e2) {
                                Log.e(Constants.LOG, "Failed to initialize DomobAdManager! ");
                                Log.e(Constants.LOG, "DOMOB_PID is missed in AndroidManifest.xml!");
                            }
                        }
                        a = applicationInfo.packageName;
                        Log.d(Constants.LOG, "Application's package name is " + a);
                    }
                    if (!(packageManager == null || (packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0)) == null)) {
                        b = packageInfo.versionCode;
                        Log.d(Constants.LOG, "Application's version number is " + b);
                    }
                } catch (Exception e3) {
                    Log.e(Constants.LOG, "failed to init DomobAdManager!");
                    e3.printStackTrace();
                }
            }
        }
        return;
    }

    public static void setAllowUseOfLocation(boolean flag) {
        l = true;
        k = flag;
    }

    public static void setPublisherId(String s) {
        if (s == null || (s != null && s.length() == 0)) {
            Log.e(Constants.LOG, "Incorrect Domob publisher ID.");
        } else {
            c = s;
        }
    }

    public static String getPublisherId(Context context) {
        if (c == null) {
            a(context);
        }
        return c;
    }

    private static boolean k(Context context) {
        boolean equals;
        if (i == null && i == null) {
            i = Settings.Secure.getString(context.getContentResolver(), "android_id");
        }
        String l2 = l(context);
        if (l2 == null) {
            equals = true;
        } else {
            equals = l2.replaceAll("0", "").equals("");
        }
        return i == null && equals && "sdk".equalsIgnoreCase(Build.MODEL);
    }

    protected static String b(Context context) {
        if (h == null) {
            if (k(context)) {
                h = "-1,-1,emulator";
            } else {
                h = m(context);
            }
        }
        return h;
    }

    protected static String c(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == -1) {
            Log.e(Constants.LOG, "Cannot access user's network type.  Permissions are not set.");
            return "unknown";
        }
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            int type = activeNetworkInfo.getType();
            if (type == 0) {
                String subtypeName = activeNetworkInfo.getSubtypeName();
                if (subtypeName == null) {
                    return "gprs";
                }
                return subtypeName;
            } else if (type == 1) {
                return "wifi";
            }
        }
        return "unknown";
    }

    private static String l(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String m(android.content.Context r7) {
        /*
            r4 = 0
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r0 = l(r7)     // Catch:{ SecurityException -> 0x0061, Exception -> 0x0072 }
            if (r0 == 0) goto L_0x005b
            r1.append(r0)     // Catch:{ SecurityException -> 0x0061, Exception -> 0x0072 }
        L_0x000f:
            java.lang.String r0 = ","
            r1.append(r0)     // Catch:{ SecurityException -> 0x0061, Exception -> 0x0072 }
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r7.getSystemService(r0)     // Catch:{ SecurityException -> 0x0061, Exception -> 0x0072 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ SecurityException -> 0x0061, Exception -> 0x0072 }
            java.lang.String r0 = r0.getSubscriberId()     // Catch:{ SecurityException -> 0x0061, Exception -> 0x0072 }
            if (r0 == 0) goto L_0x006c
            r1.append(r0)     // Catch:{ SecurityException -> 0x0061, Exception -> 0x0072 }
        L_0x0025:
            java.lang.String r0 = ","
            r1.append(r0)     // Catch:{ SecurityException -> 0x0061, Exception -> 0x0072 }
            cn.domob.android.ads.DBHelper r0 = cn.domob.android.ads.DBHelper.a(r7)     // Catch:{ SecurityException -> 0x0061, Exception -> 0x0072 }
            android.database.Cursor r0 = r0.b()     // Catch:{ SecurityException -> 0x0061, Exception -> 0x0072 }
            if (r0 == 0) goto L_0x003a
            int r2 = r0.getCount()     // Catch:{ SecurityException -> 0x00b9, Exception -> 0x00af }
            if (r2 != 0) goto L_0x007b
        L_0x003a:
            java.lang.String r2 = "DomobSDK"
            r3 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r3)     // Catch:{ SecurityException -> 0x00b9, Exception -> 0x00af }
            if (r2 == 0) goto L_0x00c6
            java.lang.String r2 = "DomobSDK"
            java.lang.String r3 = "conf db is empty!"
            android.util.Log.d(r2, r3)     // Catch:{ SecurityException -> 0x00b9, Exception -> 0x00af }
            r2 = r0
            r0 = r4
        L_0x004c:
            if (r2 == 0) goto L_0x0051
            r2.close()
        L_0x0051:
            if (r0 == 0) goto L_0x00a9
            r1.append(r0)
        L_0x0056:
            java.lang.String r0 = r1.toString()
            return r0
        L_0x005b:
            java.lang.String r0 = "-1"
            r1.append(r0)     // Catch:{ SecurityException -> 0x0061, Exception -> 0x0072 }
            goto L_0x000f
        L_0x0061:
            r0 = move-exception
            r0 = r4
            r2 = r4
        L_0x0064:
            java.lang.String r3 = "DomobSDK"
            java.lang.String r4 = "you must set READ_PHONE_STATE permisson in AndroidManifest.xml"
            android.util.Log.e(r3, r4)
            goto L_0x004c
        L_0x006c:
            java.lang.String r0 = "-1"
            r1.append(r0)     // Catch:{ SecurityException -> 0x0061, Exception -> 0x0072 }
            goto L_0x0025
        L_0x0072:
            r0 = move-exception
            r2 = r4
            r3 = r4
        L_0x0075:
            r0.printStackTrace()
            r0 = r2
            r2 = r3
            goto L_0x004c
        L_0x007b:
            r0.moveToFirst()     // Catch:{ SecurityException -> 0x00b9, Exception -> 0x00af }
            java.lang.String r2 = "_uuid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ SecurityException -> 0x00b9, Exception -> 0x00af }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SecurityException -> 0x00b9, Exception -> 0x00af }
            java.lang.String r3 = "DomobSDK"
            r4 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r4)     // Catch:{ SecurityException -> 0x00bd, Exception -> 0x00b4 }
            if (r3 == 0) goto L_0x00c2
            java.lang.String r3 = "DomobSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SecurityException -> 0x00bd, Exception -> 0x00b4 }
            java.lang.String r5 = "get UUID from conf db:"
            r4.<init>(r5)     // Catch:{ SecurityException -> 0x00bd, Exception -> 0x00b4 }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ SecurityException -> 0x00bd, Exception -> 0x00b4 }
            java.lang.String r4 = r4.toString()     // Catch:{ SecurityException -> 0x00bd, Exception -> 0x00b4 }
            android.util.Log.d(r3, r4)     // Catch:{ SecurityException -> 0x00bd, Exception -> 0x00b4 }
            r6 = r2
            r2 = r0
            r0 = r6
            goto L_0x004c
        L_0x00a9:
            java.lang.String r0 = "-1"
            r1.append(r0)
            goto L_0x0056
        L_0x00af:
            r2 = move-exception
            r3 = r0
            r0 = r2
            r2 = r4
            goto L_0x0075
        L_0x00b4:
            r3 = move-exception
            r6 = r3
            r3 = r0
            r0 = r6
            goto L_0x0075
        L_0x00b9:
            r2 = move-exception
            r2 = r0
            r0 = r4
            goto L_0x0064
        L_0x00bd:
            r3 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
            goto L_0x0064
        L_0x00c2:
            r6 = r2
            r2 = r0
            r0 = r6
            goto L_0x004c
        L_0x00c6:
            r2 = r0
            r0 = r4
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.DomobAdManager.m(android.content.Context):java.lang.String");
    }

    protected static String d(Context context) {
        if (((WindowManager) context.getSystemService("window")).getDefaultDisplay().getOrientation() == 1) {
            return "h";
        }
        return "v";
    }

    private static float n(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        defaultDisplay.getMetrics(displayMetrics);
        return displayMetrics.density;
    }

    protected static float a(View view) {
        return view.getResources().getDisplayMetrics().density;
    }

    protected static int a(Context context, View view) {
        int i2;
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (defaultDisplay != null) {
            i2 = defaultDisplay.getWidth();
        } else {
            i2 = 0;
        }
        return Math.round(((float) i2) * (n(context) / a(view)));
    }

    protected static int b(Context context, View view) {
        int i2;
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (defaultDisplay != null) {
            i2 = defaultDisplay.getHeight();
        } else {
            i2 = 0;
        }
        return Math.round(((float) i2) * (n(context) / a(view)));
    }

    protected static int e(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (defaultDisplay != null) {
            return defaultDisplay.getHeight();
        }
        return 0;
    }

    public static class DomobLocationListener implements LocationListener {
        private LocationManager a;

        DomobLocationListener(LocationManager mgr) {
            this.a = mgr;
        }

        public final void onLocationChanged(Location location) {
            DomobAdManager.a(location);
            DomobAdManager.a(System.currentTimeMillis());
            this.a.removeUpdates(this);
        }

        public final void onProviderDisabled(String str) {
        }

        public final void onProviderEnabled(String str) {
        }

        public final void onStatusChanged(String str, int i, Bundle bundle) {
        }
    }

    private static Location o(Context context) {
        LocationManager locationManager;
        boolean z;
        String str;
        String str2;
        boolean z2;
        if (k(context) || !k) {
            Log.i(Constants.LOG, "Location information is not being used for ad requests.");
            return null;
        }
        if (k && context != null && (j == null || System.currentTimeMillis() > m + 9000000)) {
            synchronized (context) {
                m = System.currentTimeMillis();
                if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "Trying to get locations from the network.");
                    }
                    locationManager = (LocationManager) context.getSystemService("location");
                    if (locationManager != null) {
                        Criteria criteria = new Criteria();
                        criteria.setAccuracy(2);
                        criteria.setCostAllowed(false);
                        str = locationManager.getBestProvider(criteria, true);
                        z = true;
                    } else {
                        z = true;
                        str = null;
                    }
                } else {
                    locationManager = null;
                    z = false;
                    str = null;
                }
                if (str == null && context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "Trying to get locations from GPS.");
                    }
                    locationManager = (LocationManager) context.getSystemService("location");
                    if (locationManager != null) {
                        Criteria criteria2 = new Criteria();
                        criteria2.setAccuracy(1);
                        criteria2.setCostAllowed(false);
                        str2 = locationManager.getBestProvider(criteria2, true);
                        z2 = true;
                    } else {
                        str2 = str;
                        z2 = true;
                    }
                } else {
                    boolean z3 = z;
                    str2 = str;
                    z2 = z3;
                }
                if (!z2) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "Cannot access user's location.  Permissions are not set.");
                    }
                } else if (str2 != null) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "Location provider setup successfully.");
                    }
                    locationManager.requestLocationUpdates(str2, 0, 0.0f, new DomobLocationListener(locationManager), context.getMainLooper());
                } else if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "No location providers are available.  Ads will not be geotargeted.");
                }
            }
        }
        return j;
    }

    protected static Location a(Location location) {
        j = location;
        return location;
    }

    protected static String f(Context context) {
        String str = null;
        Location o2 = o(context);
        if (o2 != null) {
            str = o2.getLatitude() + "," + o2.getLongitude();
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "User coordinates are " + str);
            }
        }
        return str;
    }

    protected static long a(long j2) {
        m = j2;
        return j2;
    }

    protected static String a() {
        return String.valueOf(m);
    }

    public static void setPostalCode(String pc) {
        o = pc;
    }

    public static String getPostalCode() {
        return o;
    }

    public static void setBirthday(GregorianCalendar birthday) {
        p = birthday;
    }

    public static void setBirthday(int year, int month, int day) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(year, month - 1, day);
        setBirthday(gregorianCalendar);
    }

    public static GregorianCalendar getBirthday() {
        return p;
    }

    protected static String b() {
        GregorianCalendar birthday = getBirthday();
        if (birthday == null) {
            return null;
        }
        return String.format("%04d%02d%02d", Integer.valueOf(birthday.get(1)), Integer.valueOf(birthday.get(2) + 1), Integer.valueOf(birthday.get(5)));
    }

    public static void setGender(String gender) {
        n = gender;
    }

    public static String getGender() {
        return n;
    }

    public static boolean isTestMode(Context context) {
        if (e == null) {
            a(context);
        }
        if (e != null) {
            return e.booleanValue();
        }
        return true;
    }

    public static void setIsTestMode(boolean isTestMode) {
        e = Boolean.valueOf(isTestMode);
    }

    public static void setTestAction(String action) {
        if (action != null) {
            d = action;
        }
    }

    public static String getTestAction() {
        return d;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean isTestAllowed(android.content.Context r6) {
        /*
            r0 = 0
            cn.domob.android.ads.DBHelper r1 = cn.domob.android.ads.DBHelper.a(r6)     // Catch:{ Exception -> 0x006d }
            android.database.Cursor r0 = r1.b()     // Catch:{ Exception -> 0x006d }
            if (r0 == 0) goto L_0x0011
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x0060 }
            if (r1 != 0) goto L_0x0029
        L_0x0011:
            java.lang.String r1 = "DomobSDK"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ Exception -> 0x0060 }
            if (r1 == 0) goto L_0x0021
            java.lang.String r1 = "DomobSDK"
            java.lang.String r2 = "conf db is empty!"
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x0060 }
        L_0x0021:
            if (r0 == 0) goto L_0x0026
            r0.close()
        L_0x0026:
            boolean r0 = cn.domob.android.ads.DomobAdManager.f
            return r0
        L_0x0029:
            r0.moveToFirst()     // Catch:{ Exception -> 0x0060 }
            java.lang.String r1 = "_test_flag"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x0060 }
            int r1 = r0.getInt(r1)     // Catch:{ Exception -> 0x0060 }
            java.lang.String r2 = "DomobSDK"
            r3 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r3)     // Catch:{ Exception -> 0x0060 }
            if (r2 == 0) goto L_0x0053
            java.lang.String r2 = "DomobSDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0060 }
            java.lang.String r4 = "get test flag from conf db:"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0060 }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ Exception -> 0x0060 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0060 }
            android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x0060 }
        L_0x0053:
            if (r1 != 0) goto L_0x0069
            java.lang.String r1 = "DomobSDK"
            java.lang.String r2 = "test mode is disabled by server!"
            android.util.Log.i(r1, r2)     // Catch:{ Exception -> 0x0060 }
            r1 = 0
            cn.domob.android.ads.DomobAdManager.f = r1     // Catch:{ Exception -> 0x0060 }
            goto L_0x0021
        L_0x0060:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0064:
            r0.printStackTrace()
            r0 = r1
            goto L_0x0021
        L_0x0069:
            r1 = 1
            cn.domob.android.ads.DomobAdManager.f = r1     // Catch:{ Exception -> 0x0060 }
            goto L_0x0021
        L_0x006d:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.DomobAdManager.isTestAllowed(android.content.Context):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static java.lang.String g(android.content.Context r5) {
        /*
            r0 = 0
            cn.domob.android.ads.DBHelper r1 = cn.domob.android.ads.DBHelper.a(r5)     // Catch:{ Exception -> 0x0063 }
            android.database.Cursor r0 = r1.b()     // Catch:{ Exception -> 0x0063 }
            if (r0 == 0) goto L_0x0011
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x005a }
            if (r1 != 0) goto L_0x0029
        L_0x0011:
            java.lang.String r1 = "DomobSDK"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ Exception -> 0x005a }
            if (r1 == 0) goto L_0x0021
            java.lang.String r1 = "DomobSDK"
            java.lang.String r2 = "conf db is empty!"
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x005a }
        L_0x0021:
            if (r0 == 0) goto L_0x0026
            r0.close()
        L_0x0026:
            java.lang.String r0 = cn.domob.android.ads.DomobAdManager.g
            return r0
        L_0x0029:
            r0.moveToFirst()     // Catch:{ Exception -> 0x005a }
            java.lang.String r1 = "_cid"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x005a }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x005a }
            cn.domob.android.ads.DomobAdManager.g = r1     // Catch:{ Exception -> 0x005a }
            if (r1 == 0) goto L_0x0021
            java.lang.String r1 = "DomobSDK"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ Exception -> 0x005a }
            if (r1 == 0) goto L_0x0021
            java.lang.String r1 = "DomobSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005a }
            java.lang.String r3 = "get cid from conf db:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x005a }
            java.lang.String r3 = cn.domob.android.ads.DomobAdManager.g     // Catch:{ Exception -> 0x005a }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x005a }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x005a }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x005a }
            goto L_0x0021
        L_0x005a:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x005e:
            r0.printStackTrace()
            r0 = r1
            goto L_0x0021
        L_0x0063:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.DomobAdManager.g(android.content.Context):java.lang.String");
    }

    protected static String h(Context context) {
        if (a == null) {
            a(context);
        }
        return a;
    }

    protected static int i(Context context) {
        if (a == null) {
            a(context);
        }
        return b;
    }

    protected static Cursor j(Context context) {
        String c2 = c(context);
        if (c2 == null || !c2.equals("wifi")) {
            return context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null);
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "network is wifi, don't read apn.");
        }
        return null;
    }

    public static void setEndpoint(String url) {
        j.a(url);
    }

    public static String getEndpoint() {
        return j.d();
    }
}
