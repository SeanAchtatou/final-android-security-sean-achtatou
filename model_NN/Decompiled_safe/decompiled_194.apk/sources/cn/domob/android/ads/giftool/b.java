package cn.domob.android.ads.giftool;

import android.graphics.Bitmap;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

final class b extends Thread {
    private boolean A = false;
    private byte[] B = new byte[256];
    private int C = 0;
    private int D = 0;
    private int E = 0;
    private boolean F = false;
    private int G = 0;
    private int H;

    /* renamed from: I  reason: collision with root package name */
    private short[] f0I;
    private byte[] J;
    private byte[] K;
    private byte[] L;
    private c M;
    private int N;
    private a O = null;
    private byte[] P = null;
    public int a;
    public int b;
    private InputStream c;
    private int d;
    private boolean e;
    private int f;
    private int[] g;
    private int[] h;
    private int[] i;
    private int j;
    private int k;
    private int l;
    private boolean m;
    private boolean n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private int w;
    private Bitmap x;
    private Bitmap y;
    private c z = null;

    public b(byte[] bArr, a aVar) {
        this.P = bArr;
        this.O = aVar;
    }

    public b(InputStream inputStream, a aVar) {
        this.c = inputStream;
        this.O = aVar;
    }

    public final void run() {
        if (this.c != null) {
            f();
        } else if (this.P != null) {
            this.c = new ByteArrayInputStream(this.P);
            this.P = null;
            f();
        }
    }

    public final void a() {
        c cVar = this.M;
        while (cVar != null) {
            cVar.a.recycle();
            cVar.a = null;
            this.M = this.M.c;
            cVar = this.M;
        }
        if (this.c != null) {
            try {
                this.c.close();
            } catch (Exception e2) {
            }
            this.c = null;
        }
        this.P = null;
    }

    public final int b() {
        return this.N;
    }

    public final Bitmap c() {
        return a(0);
    }

    private void e() {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = 0;
        int[] iArr = new int[(this.a * this.b)];
        if (this.E > 0) {
            if (this.E == 3) {
                int i8 = this.N - 2;
                if (i8 > 0) {
                    this.y = a(i8 - 1);
                } else {
                    this.y = null;
                }
            }
            if (this.y != null) {
                this.y.getPixels(iArr, 0, this.a, 0, 0, this.a, this.b);
                if (this.E == 2) {
                    if (!this.F) {
                        i6 = this.l;
                    } else {
                        i6 = 0;
                    }
                    for (int i9 = 0; i9 < this.w; i9++) {
                        int i10 = ((this.u + i9) * this.a) + this.t;
                        int i11 = this.v + i10;
                        while (i10 < i11) {
                            iArr[i10] = i6;
                            i10++;
                        }
                    }
                }
            }
        }
        int i12 = 8;
        int i13 = 1;
        int i14 = 0;
        while (i14 < this.s) {
            if (this.n) {
                if (i7 >= this.s) {
                    i13++;
                    switch (i13) {
                        case 2:
                            i7 = 4;
                            break;
                        case 3:
                            i7 = 2;
                            i12 = 4;
                            break;
                        case 4:
                            i7 = 1;
                            i12 = 2;
                            break;
                    }
                }
                i2 = i13;
                i3 = i12;
                i4 = i7 + i12;
            } else {
                i2 = i13;
                i3 = i12;
                i4 = i7;
                i7 = i14;
            }
            int i15 = i7 + this.q;
            if (i15 < this.b) {
                int i16 = i15 * this.a;
                int i17 = this.p + i16;
                int i18 = this.r + i17;
                if (this.a + i16 < i18) {
                    i5 = i16 + this.a;
                } else {
                    i5 = i18;
                }
                int i19 = this.r * i14;
                while (i17 < i5) {
                    int i20 = i19 + 1;
                    int i21 = this.i[this.L[i19] & 255];
                    if (i21 != 0) {
                        iArr[i17] = i21;
                    }
                    i17++;
                    i19 = i20;
                }
            }
            i14++;
            i7 = i4;
            i12 = i3;
            i13 = i2;
        }
        try {
            this.x = Bitmap.createBitmap(iArr, this.a, this.b, Bitmap.Config.ARGB_4444);
        } catch (Exception e2) {
            Log.e("gif", "out of memory");
        } catch (Error e3) {
            Log.e("gif", "out of memory");
        }
    }

    private Bitmap a(int i2) {
        c cVar;
        c cVar2 = this.M;
        int i3 = 0;
        while (true) {
            if (cVar2 == null) {
                cVar = null;
                break;
            } else if (i3 == i2) {
                cVar = cVar2;
                break;
            } else {
                cVar2 = cVar2.c;
                i3++;
            }
        }
        if (cVar == null) {
            return null;
        }
        return cVar.a;
    }

    public final c d() {
        if (!this.A) {
            this.A = true;
            return this.M;
        }
        if (this.d != 0) {
            this.z = this.z.c;
            if (this.z == null) {
                this.z = this.M;
            }
        } else if (this.z.c != null) {
            this.z = this.z.c;
        }
        return this.z;
    }

    private int f() {
        this.d = 0;
        this.N = 0;
        this.M = null;
        this.g = null;
        this.h = null;
        if (this.c != null) {
            String str = "";
            for (int i2 = 0; i2 < 6; i2++) {
                str = String.valueOf(str) + ((char) h());
            }
            if (!str.startsWith("GIF")) {
                this.d = 1;
            } else {
                this.a = l();
                this.b = l();
                int h2 = h();
                this.e = (h2 & 128) != 0;
                this.f = 2 << (h2 & 7);
                this.j = h();
                h();
                if (this.e && !g()) {
                    this.g = b(this.f);
                    this.k = this.g[this.j];
                }
            }
            if (!g()) {
                j();
                if (this.N < 0) {
                    this.d = 1;
                    this.O.parseOk(false, -1);
                } else {
                    this.d = -1;
                    this.O.parseOk(true, -1);
                }
            }
            try {
                this.c.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else {
            this.d = 2;
            this.O.parseOk(false, -1);
        }
        return this.d;
    }

    private boolean g() {
        return this.d != 0;
    }

    private int h() {
        try {
            return this.c.read();
        } catch (Exception e2) {
            this.d = 1;
            return 0;
        }
    }

    private int i() {
        this.C = h();
        int i2 = 0;
        if (this.C > 0) {
            while (i2 < this.C) {
                try {
                    int read = this.c.read(this.B, i2, this.C - i2);
                    if (read == -1) {
                        break;
                    }
                    i2 += read;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (i2 < this.C) {
                this.d = 1;
            }
        }
        return i2;
    }

    private int[] b(int i2) {
        int i3;
        int i4 = i2 * 3;
        byte[] bArr = new byte[i4];
        try {
            i3 = this.c.read(bArr);
        } catch (Exception e2) {
            e2.printStackTrace();
            i3 = 0;
        }
        if (i3 < i4) {
            this.d = 1;
            return null;
        }
        int[] iArr = new int[256];
        int i5 = 0;
        int i6 = 0;
        while (i5 < i2) {
            int i7 = i6 + 1;
            int i8 = i7 + 1;
            iArr[i5] = ((bArr[i6] & 255) << 16) | -16777216 | ((bArr[i7] & 255) << 8) | (bArr[i8] & 255);
            i5++;
            i6 = i8 + 1;
        }
        return iArr;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r8v10, types: [int] */
    /* JADX WARN: Type inference failed for: r17v7, types: [int] */
    /* JADX WARN: Type inference failed for: r18v6, types: [int] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x0002 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0391 A[LOOP:3: B:43:0x012e->B:97:0x0391, LOOP_END] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void j() {
        /*
            r25 = this;
            r2 = 0
            r9 = r2
        L_0x0002:
            if (r9 != 0) goto L_0x000a
            boolean r2 = r25.g()
            if (r2 == 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            int r2 = r25.h()
            switch(r2) {
                case 0: goto L_0x0002;
                case 33: goto L_0x03df;
                case 44: goto L_0x0019;
                case 59: goto L_0x046a;
                default: goto L_0x0012;
            }
        L_0x0012:
            r2 = 1
            r0 = r2
            r1 = r25
            r1.d = r0
            goto L_0x0002
        L_0x0019:
            int r2 = r25.l()
            r0 = r2
            r1 = r25
            r1.p = r0
            int r2 = r25.l()
            r0 = r2
            r1 = r25
            r1.q = r0
            int r2 = r25.l()
            r0 = r2
            r1 = r25
            r1.r = r0
            int r2 = r25.l()
            r0 = r2
            r1 = r25
            r1.s = r0
            int r2 = r25.h()
            r3 = r2 & 128(0x80, float:1.794E-43)
            if (r3 == 0) goto L_0x0237
            r3 = 1
        L_0x0046:
            r0 = r3
            r1 = r25
            r1.m = r0
            r3 = r2 & 64
            if (r3 == 0) goto L_0x023a
            r3 = 1
        L_0x0050:
            r0 = r3
            r1 = r25
            r1.n = r0
            r3 = 2
            r2 = r2 & 7
            int r2 = r3 << r2
            r0 = r2
            r1 = r25
            r1.o = r0
            r0 = r25
            boolean r0 = r0.m
            r2 = r0
            if (r2 == 0) goto L_0x023d
            r0 = r25
            int r0 = r0.o
            r2 = r0
            r0 = r25
            r1 = r2
            int[] r2 = r0.b(r1)
            r0 = r2
            r1 = r25
            r1.h = r0
            r0 = r25
            int[] r0 = r0.h
            r2 = r0
            r0 = r2
            r1 = r25
            r1.i = r0
        L_0x0081:
            r2 = 0
            r0 = r25
            boolean r0 = r0.F
            r3 = r0
            if (r3 == 0) goto L_0x00a2
            r0 = r25
            int[] r0 = r0.i
            r2 = r0
            r0 = r25
            int r0 = r0.H
            r3 = r0
            r2 = r2[r3]
            r0 = r25
            int[] r0 = r0.i
            r3 = r0
            r0 = r25
            int r0 = r0.H
            r4 = r0
            r5 = 0
            r3[r4] = r5
        L_0x00a2:
            r10 = r2
            r0 = r25
            int[] r0 = r0.i
            r2 = r0
            if (r2 != 0) goto L_0x00b0
            r2 = 1
            r0 = r2
            r1 = r25
            r1.d = r0
        L_0x00b0:
            boolean r2 = r25.g()
            if (r2 != 0) goto L_0x0002
            r0 = r25
            int r0 = r0.r
            r2 = r0
            r0 = r25
            int r0 = r0.s
            r3 = r0
            int r2 = r2 * r3
            r0 = r25
            byte[] r0 = r0.L
            r3 = r0
            if (r3 == 0) goto L_0x00d0
            r0 = r25
            byte[] r0 = r0.L
            r3 = r0
            int r3 = r3.length
            if (r3 >= r2) goto L_0x00d7
        L_0x00d0:
            byte[] r3 = new byte[r2]
            r0 = r3
            r1 = r25
            r1.L = r0
        L_0x00d7:
            r0 = r25
            short[] r0 = r0.f0I
            r3 = r0
            if (r3 != 0) goto L_0x00e7
            r3 = 4096(0x1000, float:5.74E-42)
            short[] r3 = new short[r3]
            r0 = r3
            r1 = r25
            r1.f0I = r0
        L_0x00e7:
            r0 = r25
            byte[] r0 = r0.J
            r3 = r0
            if (r3 != 0) goto L_0x00f7
            r3 = 4096(0x1000, float:5.74E-42)
            byte[] r3 = new byte[r3]
            r0 = r3
            r1 = r25
            r1.J = r0
        L_0x00f7:
            r0 = r25
            byte[] r0 = r0.K
            r3 = r0
            if (r3 != 0) goto L_0x0107
            r3 = 4097(0x1001, float:5.741E-42)
            byte[] r3 = new byte[r3]
            r0 = r3
            r1 = r25
            r1.K = r0
        L_0x0107:
            int r3 = r25.h()
            r4 = 1
            int r4 = r4 << r3
            int r5 = r4 + 1
            int r6 = r4 + 2
            r7 = -1
            int r8 = r3 + 1
            r11 = 1
            int r11 = r11 << r8
            r12 = 1
            int r11 = r11 - r12
            r12 = 0
        L_0x0119:
            if (r12 < r4) goto L_0x025b
            r12 = 0
            r13 = 0
            r14 = r12
            r15 = r12
            r16 = r7
            r17 = r8
            r18 = r11
            r19 = r6
            r8 = r12
            r11 = r12
            r6 = r12
            r7 = r12
        L_0x012b:
            if (r13 < r2) goto L_0x026f
        L_0x012d:
            r3 = r6
        L_0x012e:
            if (r3 < r2) goto L_0x0391
            r25.m()
            boolean r2 = r25.g()
            if (r2 != 0) goto L_0x0002
            r0 = r25
            int r0 = r0.N
            r2 = r0
            int r2 = r2 + 1
            r0 = r2
            r1 = r25
            r1.N = r0
            r0 = r25
            int r0 = r0.a     // Catch:{ Exception -> 0x039d, Error -> 0x03a7 }
            r2 = r0
            r0 = r25
            int r0 = r0.b     // Catch:{ Exception -> 0x039d, Error -> 0x03a7 }
            r3 = r0
            android.graphics.Bitmap$Config r4 = android.graphics.Bitmap.Config.ARGB_4444     // Catch:{ Exception -> 0x039d, Error -> 0x03a7 }
            android.graphics.Bitmap r2 = android.graphics.Bitmap.createBitmap(r2, r3, r4)     // Catch:{ Exception -> 0x039d, Error -> 0x03a7 }
            r0 = r2
            r1 = r25
            r1.x = r0     // Catch:{ Exception -> 0x039d, Error -> 0x03a7 }
            r25.e()
            r0 = r25
            cn.domob.android.ads.giftool.a r0 = r0.O
            r2 = r0
            android.view.View r2 = (android.view.View) r2
            android.content.res.Resources r2 = r2.getResources()
            android.util.DisplayMetrics r2 = r2.getDisplayMetrics()
            android.graphics.Matrix r7 = new android.graphics.Matrix
            r7.<init>()
            float r3 = r2.density
            float r2 = r2.density
            r7.postScale(r3, r2)
            r0 = r25
            android.graphics.Bitmap r0 = r0.x     // Catch:{ Exception -> 0x03b1, Error -> 0x03bb }
            r2 = r0
            r3 = 0
            r4 = 0
            r0 = r25
            android.graphics.Bitmap r0 = r0.x     // Catch:{ Exception -> 0x03b1, Error -> 0x03bb }
            r5 = r0
            int r5 = r5.getWidth()     // Catch:{ Exception -> 0x03b1, Error -> 0x03bb }
            r0 = r25
            android.graphics.Bitmap r0 = r0.x     // Catch:{ Exception -> 0x03b1, Error -> 0x03bb }
            r6 = r0
            int r6 = r6.getHeight()     // Catch:{ Exception -> 0x03b1, Error -> 0x03bb }
            r8 = 1
            android.graphics.Bitmap r2 = android.graphics.Bitmap.createBitmap(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x03b1, Error -> 0x03bb }
            r0 = r25
            cn.domob.android.ads.giftool.c r0 = r0.M
            r3 = r0
            if (r3 != 0) goto L_0x03c5
            cn.domob.android.ads.giftool.c r3 = new cn.domob.android.ads.giftool.c
            r0 = r25
            int r0 = r0.G
            r4 = r0
            r3.<init>(r2, r4)
            r0 = r3
            r1 = r25
            r1.M = r0
            r0 = r25
            cn.domob.android.ads.giftool.c r0 = r0.M
            r2 = r0
            r0 = r2
            r1 = r25
            r1.z = r0
        L_0x01b6:
            r0 = r25
            boolean r0 = r0.F
            r2 = r0
            if (r2 == 0) goto L_0x01c9
            r0 = r25
            int[] r0 = r0.i
            r2 = r0
            r0 = r25
            int r0 = r0.H
            r3 = r0
            r2[r3] = r10
        L_0x01c9:
            r0 = r25
            int r0 = r0.D
            r2 = r0
            r0 = r2
            r1 = r25
            r1.E = r0
            r0 = r25
            int r0 = r0.p
            r2 = r0
            r0 = r2
            r1 = r25
            r1.t = r0
            r0 = r25
            int r0 = r0.q
            r2 = r0
            r0 = r2
            r1 = r25
            r1.u = r0
            r0 = r25
            int r0 = r0.r
            r2 = r0
            r0 = r2
            r1 = r25
            r1.v = r0
            r0 = r25
            int r0 = r0.s
            r2 = r0
            r0 = r2
            r1 = r25
            r1.w = r0
            r0 = r25
            android.graphics.Bitmap r0 = r0.x
            r2 = r0
            r0 = r2
            r1 = r25
            r1.y = r0
            r0 = r25
            int r0 = r0.k
            r2 = r0
            r0 = r2
            r1 = r25
            r1.l = r0
            r2 = 0
            r0 = r2
            r1 = r25
            r1.D = r0
            r2 = 0
            r0 = r2
            r1 = r25
            r1.F = r0
            r2 = 0
            r0 = r2
            r1 = r25
            r1.G = r0
            r2 = 0
            r0 = r2
            r1 = r25
            r1.h = r0
            r0 = r25
            cn.domob.android.ads.giftool.a r0 = r0.O
            r2 = r0
            r3 = 1
            r0 = r25
            int r0 = r0.N
            r4 = r0
            r2.parseOk(r3, r4)
            goto L_0x0002
        L_0x0237:
            r3 = 0
            goto L_0x0046
        L_0x023a:
            r3 = 0
            goto L_0x0050
        L_0x023d:
            r0 = r25
            int[] r0 = r0.g
            r2 = r0
            r0 = r2
            r1 = r25
            r1.i = r0
            r0 = r25
            int r0 = r0.j
            r2 = r0
            r0 = r25
            int r0 = r0.H
            r3 = r0
            if (r2 != r3) goto L_0x0081
            r2 = 0
            r0 = r2
            r1 = r25
            r1.k = r0
            goto L_0x0081
        L_0x025b:
            r0 = r25
            short[] r0 = r0.f0I
            r13 = r0
            r14 = 0
            r13[r12] = r14
            r0 = r25
            byte[] r0 = r0.J
            r13 = r0
            byte r14 = (byte) r12
            r13[r12] = r14
            int r12 = r12 + 1
            goto L_0x0119
        L_0x026f:
            if (r8 != 0) goto L_0x0358
            r0 = r15
            r1 = r17
            if (r0 >= r1) goto L_0x029e
            if (r14 != 0) goto L_0x0284
            int r7 = r25.i()
            if (r7 <= 0) goto L_0x012d
            r14 = 0
            r24 = r14
            r14 = r7
            r7 = r24
        L_0x0284:
            r0 = r25
            byte[] r0 = r0.B
            r20 = r0
            byte r20 = r20[r7]
            r0 = r20
            r0 = r0 & 255(0xff, float:3.57E-43)
            r20 = r0
            int r20 = r20 << r15
            int r12 = r12 + r20
            int r15 = r15 + 8
            int r7 = r7 + 1
            int r14 = r14 + -1
            goto L_0x012b
        L_0x029e:
            r20 = r12 & r18
            int r12 = r12 >> r17
            int r15 = r15 - r17
            r0 = r20
            r1 = r19
            if (r0 > r1) goto L_0x012d
            r0 = r20
            r1 = r5
            if (r0 == r1) goto L_0x012d
            r0 = r20
            r1 = r4
            if (r0 != r1) goto L_0x02ce
            int r16 = r3 + 1
            r17 = 1
            int r17 = r17 << r16
            r18 = 1
            int r17 = r17 - r18
            int r18 = r4 + 2
            r19 = -1
            r24 = r19
            r19 = r18
            r18 = r17
            r17 = r16
            r16 = r24
            goto L_0x012b
        L_0x02ce:
            r21 = -1
            r0 = r16
            r1 = r21
            if (r0 != r1) goto L_0x02ef
            r0 = r25
            byte[] r0 = r0.K
            r11 = r0
            int r16 = r8 + 1
            r0 = r25
            byte[] r0 = r0.J
            r21 = r0
            byte r21 = r21[r20]
            r11[r8] = r21
            r8 = r16
            r11 = r20
            r16 = r20
            goto L_0x012b
        L_0x02ef:
            r0 = r20
            r1 = r19
            if (r0 != r1) goto L_0x0474
            r0 = r25
            byte[] r0 = r0.K
            r21 = r0
            int r22 = r8 + 1
            byte r11 = (byte) r11
            r21[r8] = r11
            r8 = r22
            r11 = r16
        L_0x0304:
            if (r11 > r4) goto L_0x0373
            r0 = r25
            byte[] r0 = r0.J
            r21 = r0
            byte r11 = r21[r11]
            r11 = r11 & 255(0xff, float:3.57E-43)
            r21 = 4096(0x1000, float:5.74E-42)
            r0 = r19
            r1 = r21
            if (r0 >= r1) goto L_0x012d
            r0 = r25
            byte[] r0 = r0.K
            r21 = r0
            int r22 = r8 + 1
            r0 = r11
            byte r0 = (byte) r0
            r23 = r0
            r21[r8] = r23
            r0 = r25
            short[] r0 = r0.f0I
            r8 = r0
            r0 = r16
            short r0 = (short) r0
            r16 = r0
            r8[r19] = r16
            r0 = r25
            byte[] r0 = r0.J
            r8 = r0
            r0 = r11
            byte r0 = (byte) r0
            r16 = r0
            r8[r19] = r16
            int r8 = r19 + 1
            r16 = r8 & r18
            if (r16 != 0) goto L_0x046e
            r16 = 4096(0x1000, float:5.74E-42)
            r0 = r8
            r1 = r16
            if (r0 >= r1) goto L_0x046e
            int r16 = r17 + 1
            int r17 = r18 + r8
        L_0x034e:
            r18 = r17
            r19 = r8
            r17 = r16
            r8 = r22
            r16 = r20
        L_0x0358:
            int r8 = r8 + -1
            r0 = r25
            byte[] r0 = r0.L
            r20 = r0
            int r21 = r6 + 1
            r0 = r25
            byte[] r0 = r0.K
            r22 = r0
            byte r22 = r22[r8]
            r20[r6] = r22
            int r6 = r13 + 1
            r13 = r6
            r6 = r21
            goto L_0x012b
        L_0x0373:
            r0 = r25
            byte[] r0 = r0.K
            r21 = r0
            int r22 = r8 + 1
            r0 = r25
            byte[] r0 = r0.J
            r23 = r0
            byte r23 = r23[r11]
            r21[r8] = r23
            r0 = r25
            short[] r0 = r0.f0I
            r8 = r0
            short r8 = r8[r11]
            r11 = r8
            r8 = r22
            goto L_0x0304
        L_0x0391:
            r0 = r25
            byte[] r0 = r0.L
            r4 = r0
            r5 = 0
            r4[r3] = r5
            int r3 = r3 + 1
            goto L_0x012e
        L_0x039d:
            r2 = move-exception
            java.lang.String r2 = "gif"
            java.lang.String r3 = "out of memory"
            android.util.Log.e(r2, r3)
            goto L_0x0002
        L_0x03a7:
            r2 = move-exception
            java.lang.String r2 = "gif"
            java.lang.String r3 = "out of memory"
            android.util.Log.e(r2, r3)
            goto L_0x0002
        L_0x03b1:
            r2 = move-exception
            java.lang.String r2 = "gif"
            java.lang.String r3 = "out of memory"
            android.util.Log.e(r2, r3)
            goto L_0x0002
        L_0x03bb:
            r2 = move-exception
            java.lang.String r2 = "gif"
            java.lang.String r3 = "out of memory"
            android.util.Log.e(r2, r3)
            goto L_0x0002
        L_0x03c5:
            r0 = r25
            cn.domob.android.ads.giftool.c r0 = r0.M
            r3 = r0
        L_0x03ca:
            cn.domob.android.ads.giftool.c r4 = r3.c
            if (r4 != 0) goto L_0x03dc
            cn.domob.android.ads.giftool.c r4 = new cn.domob.android.ads.giftool.c
            r0 = r25
            int r0 = r0.G
            r5 = r0
            r4.<init>(r2, r5)
            r3.c = r4
            goto L_0x01b6
        L_0x03dc:
            cn.domob.android.ads.giftool.c r3 = r3.c
            goto L_0x03ca
        L_0x03df:
            int r2 = r25.h()
            switch(r2) {
                case 249: goto L_0x03eb;
                case 255: goto L_0x042d;
                default: goto L_0x03e6;
            }
        L_0x03e6:
            r25.m()
            goto L_0x0002
        L_0x03eb:
            r25.h()
            int r2 = r25.h()
            r3 = r2 & 28
            int r3 = r3 >> 2
            r0 = r3
            r1 = r25
            r1.D = r0
            r0 = r25
            int r0 = r0.D
            r3 = r0
            if (r3 != 0) goto L_0x0408
            r3 = 1
            r0 = r3
            r1 = r25
            r1.D = r0
        L_0x0408:
            r2 = r2 & 1
            if (r2 == 0) goto L_0x042b
            r2 = 1
        L_0x040d:
            r0 = r2
            r1 = r25
            r1.F = r0
            int r2 = r25.l()
            int r2 = r2 * 10
            r0 = r2
            r1 = r25
            r1.G = r0
            int r2 = r25.h()
            r0 = r2
            r1 = r25
            r1.H = r0
            r25.h()
            goto L_0x0002
        L_0x042b:
            r2 = 0
            goto L_0x040d
        L_0x042d:
            r25.i()
            java.lang.String r2 = ""
            r3 = 0
            r24 = r3
            r3 = r2
            r2 = r24
        L_0x0438:
            r4 = 11
            if (r2 < r4) goto L_0x0449
            java.lang.String r2 = "NETSCAPE2.0"
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x0465
            r25.k()
            goto L_0x0002
        L_0x0449:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r4.<init>(r3)
            r0 = r25
            byte[] r0 = r0.B
            r3 = r0
            byte r3 = r3[r2]
            char r3 = (char) r3
            java.lang.StringBuilder r3 = r4.append(r3)
            java.lang.String r3 = r3.toString()
            int r2 = r2 + 1
            goto L_0x0438
        L_0x0465:
            r25.m()
            goto L_0x0002
        L_0x046a:
            r2 = 1
            r9 = r2
            goto L_0x0002
        L_0x046e:
            r16 = r17
            r17 = r18
            goto L_0x034e
        L_0x0474:
            r11 = r20
            goto L_0x0304
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.giftool.b.j():void");
    }

    private void k() {
        do {
            i();
            if (this.B[0] == 1) {
                byte[] bArr = this.B;
                byte[] bArr2 = this.B;
            }
            if (this.C <= 0) {
                return;
            }
        } while (!g());
    }

    private int l() {
        return h() | (h() << 8);
    }

    private void m() {
        do {
            i();
            if (this.C <= 0) {
                return;
            }
        } while (!g());
    }
}
