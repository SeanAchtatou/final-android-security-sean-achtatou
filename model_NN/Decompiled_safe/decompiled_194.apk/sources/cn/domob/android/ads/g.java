package cn.domob.android.ads;

import android.content.Context;
import android.util.Log;

final class g extends Thread {
    private DomobAdView a;

    public g(DomobAdView domobAdView) {
        this.a = domobAdView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.domob.android.ads.DomobAdView.b(cn.domob.android.ads.DomobAdView, boolean):void
     arg types: [cn.domob.android.ads.DomobAdView, int]
     candidates:
      cn.domob.android.ads.DomobAdView.b(cn.domob.android.ads.DomobAdView, cn.domob.android.ads.DomobAdBuilder):void
      cn.domob.android.ads.DomobAdView.b(cn.domob.android.ads.DomobAdView, cn.domob.android.ads.c):void
      cn.domob.android.ads.DomobAdView.b(cn.domob.android.ads.DomobAdView, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdView, boolean):void
     arg types: [cn.domob.android.ads.DomobAdView, int]
     candidates:
      cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdView, cn.domob.android.ads.DomobAdBuilder):cn.domob.android.ads.DomobAdBuilder
      cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdView, cn.domob.android.ads.c):void
      cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdEngine, cn.domob.android.ads.DomobAdBuilder):void
      cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdView, boolean):void */
    public final void run() {
        if (this.a == null) {
            Log.e(Constants.LOG, "GetAdThread exit because adview is null!");
            return;
        }
        Context context = this.a.getContext();
        try {
            DomobAdBuilder domobAdBuilder = new DomobAdBuilder(null, context, this.a);
            j jVar = new j();
            if (jVar.a(DomobAdView.f(this.a), domobAdBuilder, DomobAdManager.a(context, this.a), DomobAdManager.b(context, this.a)) == null) {
                DomobAdView.g(this.a);
                d a2 = jVar.a();
                if (a2 != null) {
                    int e = a2.e();
                    if (e < 200 || e >= 300) {
                        if (Log.isLoggable(Constants.LOG, 3)) {
                            Log.d(Constants.LOG, "connection return error:" + e + ", try detector next time.");
                        }
                        DomobAdView.a(this.a, true);
                    } else if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "connection is OK, continue ad request next time.");
                    }
                }
            }
            DomobAdView.b(this.a, false);
            DomobAdView.c(this.a, true);
        } catch (Exception e2) {
            if (DomobAdManager.getPublisherId(context) == null) {
                Log.e(Constants.LOG, "Please set your publisher ID first!");
            } else {
                Log.e(Constants.LOG, "Unkown exception happened!" + e2.getMessage());
            }
            e2.printStackTrace();
        }
    }
}
