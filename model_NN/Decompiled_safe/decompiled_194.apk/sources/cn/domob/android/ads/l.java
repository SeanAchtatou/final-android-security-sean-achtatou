package cn.domob.android.ads;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

final class l {
    private static String a = null;

    l() {
    }

    protected static String a(Context context) {
        if (a == null) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("android");
            stringBuffer.append(",");
            stringBuffer.append(",");
            if (Build.VERSION.RELEASE.length() > 0) {
                stringBuffer.append(Build.VERSION.RELEASE.replaceAll(",", "_"));
            } else {
                stringBuffer.append("1.5");
            }
            stringBuffer.append(",");
            stringBuffer.append(",");
            String str = Build.MODEL;
            if (str.length() > 0) {
                stringBuffer.append(str.replaceAll(",", "_"));
            }
            stringBuffer.append(",");
            String networkOperatorName = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
            if (networkOperatorName != null) {
                stringBuffer.append(networkOperatorName.replaceAll(",", "_"));
            }
            stringBuffer.append(",");
            stringBuffer.append(",");
            stringBuffer.append(",");
            a = stringBuffer.toString();
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "getUserAgent:" + a);
            }
        }
        return a;
    }

    public static String a(String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.reset();
            instance.update(str.getBytes("UTF-8"));
            byte[] digest = instance.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                String hexString = Integer.toHexString(b & 255);
                if (hexString.length() == 1) {
                    sb.append("0").append(hexString);
                } else {
                    sb.append(hexString);
                }
            }
            return sb.toString();
        } catch (Exception | NoSuchAlgorithmException e) {
            return "";
        }
    }

    public static boolean b(String str) {
        return Pattern.compile("[0-9]*").matcher(str).matches();
    }
}
