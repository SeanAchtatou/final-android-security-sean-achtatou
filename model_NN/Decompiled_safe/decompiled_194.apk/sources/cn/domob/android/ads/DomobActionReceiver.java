package cn.domob.android.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import cn.domob.android.ads.DomobReport;
import java.util.ArrayList;

public class DomobActionReceiver extends BroadcastReceiver {
    private static boolean a = false;

    public static boolean hasRegReceiver() {
        return a;
    }

    public static void setRegReceiver(boolean reged) {
        a = reged;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.i(Constants.LOG, intent.toString());
        }
        if ("android.intent.action.PACKAGE_ADDED".equals(action)) {
            PackageManager packageManager = context.getPackageManager();
            try {
                String str = packageManager.getPackageInfo(intent.getDataString().substring(8), 0).packageName;
                if (DomobAdEngine.f()) {
                    a(context, 8, str);
                    Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(str);
                    launchIntentForPackage.setFlags(268435456);
                    context.startActivity(launchIntentForPackage);
                } else {
                    a(context, 7, str);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        a = false;
        context.unregisterReceiver(this);
    }

    private static void a(Context context, int i, String str) {
        DomobReport domobReport = new DomobReport(context);
        domobReport.getClass();
        DomobReport.ReportInfo reportInfo = new DomobReport.ReportInfo(domobReport);
        reportInfo.a = i;
        reportInfo.b = 2;
        reportInfo.c = new ArrayList<>();
        reportInfo.c.add(str);
        reportInfo.d = DomobAdEngine.g();
        domobReport.a(reportInfo);
    }
}
