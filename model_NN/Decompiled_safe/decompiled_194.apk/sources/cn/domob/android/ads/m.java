package cn.domob.android.ads;

import android.content.Context;
import android.util.Log;
import android.widget.ViewFlipper;

final class m extends ViewFlipper {
    public m(Context context) {
        super(context);
    }

    public final void onWindowFocusChanged(boolean flag) {
        if (!flag || super.getVisibility() != 0) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "onWindowFocusChanged, stop flipping.");
            }
            stopFlipping();
        } else {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "onWindowFocusChanged, start flipping.");
            }
            startFlipping();
        }
        super.onWindowFocusChanged(flag);
    }

    /* access modifiers changed from: protected */
    public final void onWindowVisibilityChanged(int v) {
        if (v == 0 && super.getVisibility() == 0) {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "onWindowFocusChanged, start flipping.");
            }
            startFlipping();
        } else {
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.d(Constants.LOG, "onWindowFocusChanged, stop flipping.");
            }
            stopFlipping();
        }
        super.onWindowVisibilityChanged(v);
    }
}
