package cn.appmedia.ad;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import cn.appmedia.ad.a.d;
import cn.appmedia.ad.c.h;
import cn.appmedia.ad.c.j;
import cn.appmedia.ad.g.a;
import com.adchina.android.ads.Common;
import com.smaato.SOMA.SOMATextBanner;

public class BannerAdView extends RelativeLayout {
    private int getAdInterval;
    /* access modifiers changed from: private */
    public j mAdClient;
    /* access modifiers changed from: private */
    public AdViewListener mDefaultListener;
    /* access modifiers changed from: private */
    public AdViewListener mListener;

    public BannerAdView(Context context) {
        this(context, 60);
    }

    public BannerAdView(Context context, int i) {
        super(context);
        this.mDefaultListener = new c(this, null);
        this.getAdInterval = i;
        init(context);
    }

    public BannerAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mDefaultListener = new c(this, null);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", -1);
            int attributeUnsignedIntValue2 = attributeSet.getAttributeUnsignedIntValue(str, "textColor", SOMATextBanner.DEFAULT_BACKGROUND_COLOR);
            this.getAdInterval = attributeSet.getAttributeIntValue(str, "get_ad_interval", 60);
            init(context, attributeUnsignedIntValue, attributeUnsignedIntValue2);
            return;
        }
        this.getAdInterval = 60;
        init(context);
    }

    private void init(Context context) {
        init(context, 0, 0);
    }

    private void init(Context context, int i, int i2) {
        setGetAdInterval(this.getAdInterval);
        d.a("get ad interval:" + h.b());
        this.mAdClient = new j(Common.KCLK, context, new a(), new Handler(new a(this, this)));
        this.mAdClient.f();
        setOnClickListener(new b(this));
        setClickable(true);
        setLongClickable(false);
        setAdVisibility(8);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (!z) {
            this.mAdClient.i();
            this.mAdClient.m();
            d.a("lose windowFocus");
        } else if (getVisibility() == 0) {
            this.mAdClient.h();
        }
    }

    public void requestAd() {
        if (this.mAdClient != null) {
            this.mAdClient.f();
        }
    }

    public void setAdListener(AdViewListener adViewListener) {
        this.mListener = adViewListener;
    }

    public void setAdVisibility(int i) {
        if (getVisibility() != i) {
            synchronized (this) {
                if (i == 0) {
                    this.mAdClient.h();
                } else {
                    this.mAdClient.i();
                }
                for (int i2 = 0; i2 < getChildCount(); i2++) {
                    getChildAt(i2).setVisibility(i);
                }
                super.setVisibility(i);
                invalidate();
            }
        }
    }

    public void setBackgroundColor(int i) {
        j.c = i;
    }

    public void setGetAdInterval(int i) {
        h.a(i);
    }

    public void setTextColor(int i) {
        j.b = i;
    }
}
