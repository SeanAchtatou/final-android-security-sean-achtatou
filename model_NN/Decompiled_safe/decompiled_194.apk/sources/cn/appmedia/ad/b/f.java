package cn.appmedia.ad.b;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.appmedia.ad.c.j;
import com.google.ads.AdActivity;
import java.util.Map;

public final class f extends e {
    public int a;
    private String c;
    private int d;
    private boolean e;
    private boolean f;

    private TextView b(Context context) {
        TextView textView = new TextView(context);
        textView.setTypeface(textView.getTypeface(), (!this.e || !this.f) ? this.e ? 1 : this.f ? 2 : 0 : 3);
        textView.setTextColor(this.a);
        textView.setTextSize((float) this.d);
        textView.setText(this.c);
        textView.setPadding(10, 0, 10, 0);
        return textView;
    }

    private TextView c(Context context) {
        TextView textView = new TextView(context);
        textView.setTextColor(this.a != 0 ? this.a : 0);
        textView.setTextSize(10.0f);
        textView.setTypeface(textView.getTypeface(), 2);
        textView.setText("Ads by AppMedia");
        textView.setPadding(0, 0, 3, 3);
        return textView;
    }

    public View a(Context context) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        TextView b = b(context);
        layoutParams.addRule(13);
        b.setGravity(3);
        b.setLayoutParams(layoutParams);
        relativeLayout.addView(b);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        TextView c2 = c(context);
        layoutParams2.addRule(12);
        layoutParams2.addRule(11);
        c2.setLayoutParams(layoutParams2);
        relativeLayout.addView(c2);
        return relativeLayout;
    }

    public void a(Map map) {
        if (map.get("c") == null) {
            this.c = "";
        } else {
            this.c = ((String) map.get("c")).trim().length() <= 20 ? ((String) map.get("c")).trim() : ((String) map.get("c")).trim().substring(0, 20);
        }
        String str = (String) map.get("fs");
        if (str != null) {
            this.d = Integer.parseInt(str);
            if (this.d == 0) {
                this.d = 14;
            }
        } else {
            this.d = 14;
        }
        String str2 = (String) map.get("fc");
        if (str2 == null || str2.length() <= 0) {
            this.a = j.b;
        } else {
            this.a = cn.appmedia.ad.a.f.a(str2);
        }
        String str3 = (String) map.get("fa");
        if (str3 != null && str3.length() > 0) {
            this.e = str3.indexOf("b") > -1;
            this.f = str3.indexOf(AdActivity.INTENT_ACTION_PARAM) > -1;
        }
    }
}
