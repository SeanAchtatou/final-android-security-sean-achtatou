package cn.appmedia.ad.b;

import android.content.Context;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import cn.appmedia.ad.c.j;
import java.util.Map;

public class c extends e {
    WebView a;
    private String c = "";

    public View a(Context context) {
        this.a = new WebView(context);
        WebSettings settings = this.a.getSettings();
        settings.setSupportMultipleWindows(false);
        settings.setSupportZoom(true);
        settings.setBlockNetworkImage(false);
        settings.setCacheMode(-1);
        this.a.loadUrl(this.c);
        this.a.setInitialScale((int) Math.ceil((double) ((1.0f / j.b()) * 100.0f)));
        this.a.setVisibility(8);
        this.a.setWebViewClient(new a(this));
        return this.a;
    }

    public void a(Map map) {
        this.c = (String) map.get("s");
    }
}
