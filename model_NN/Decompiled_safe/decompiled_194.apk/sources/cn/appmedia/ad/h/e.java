package cn.appmedia.ad.h;

import cn.appmedia.ad.a.f;
import cn.appmedia.ad.d.a;
import cn.appmedia.ad.d.b;
import cn.appmedia.ad.d.c;
import cn.appmedia.ad.d.g;
import cn.appmedia.ad.d.h;
import cn.appmedia.ad.d.i;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public final class e {
    public static h a(String str) {
        h hVar = new h();
        JSONObject jSONObject = (JSONObject) new JSONTokener(str).nextValue();
        hVar.c(jSONObject.optInt("st"));
        JSONArray optJSONArray = jSONObject.optJSONArray("ads");
        int length = optJSONArray.length();
        i[] iVarArr = new i[length];
        for (int i = 0; i < length; i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            i iVar = new i();
            iVar.a(optJSONObject.optString("id"));
            iVar.a(optJSONObject.optInt("type"));
            iVar.b(optJSONObject.optInt("t"));
            JSONArray optJSONArray2 = optJSONObject.optJSONArray("fr");
            for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                iVar.f()[i2] = optJSONArray2.optInt(i2);
            }
            JSONArray optJSONArray3 = optJSONObject.optJSONArray("vs");
            int length2 = optJSONArray3.length();
            g[] gVarArr = new g[length2];
            for (int i3 = 0; i3 < length2; i3++) {
                JSONObject optJSONObject2 = optJSONArray3.optJSONObject(i3);
                g gVar = new g();
                JSONArray optJSONArray4 = optJSONObject2.optJSONArray("r");
                for (int i4 = 0; i4 < optJSONArray4.length(); i4++) {
                    gVar.c()[i4] = optJSONArray4.optInt(i4);
                }
                gVar.a(optJSONObject2.optInt("vt"));
                JSONObject optJSONObject3 = optJSONObject2.optJSONObject("args");
                Iterator<String> keys = optJSONObject3.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    gVar.b().put(next, optJSONObject3.optString(next));
                }
                gVarArr[i3] = gVar;
            }
            JSONArray optJSONArray5 = optJSONObject.optJSONArray("es");
            int length3 = optJSONArray5.length();
            cn.appmedia.ad.d.e[] eVarArr = new cn.appmedia.ad.d.e[length3];
            for (int i5 = 0; i5 < length3; i5++) {
                JSONObject optJSONObject4 = optJSONArray5.optJSONObject(i5);
                a[] aVarArr = null;
                String optString = optJSONObject4.optString("did");
                JSONArray optJSONArray6 = optJSONObject4.optJSONArray("acts");
                if (optJSONArray6 != null && !optJSONArray6.equals("null")) {
                    int length4 = optJSONArray6.length();
                    a[] aVarArr2 = new a[length4];
                    int i6 = 0;
                    while (true) {
                        int i7 = i6;
                        if (i7 >= length4) {
                            break;
                        }
                        a aVar = new a();
                        JSONObject optJSONObject5 = optJSONArray6.optJSONObject(i7);
                        aVar.a(optJSONObject5.optString("at"));
                        JSONObject optJSONObject6 = optJSONObject5.optJSONObject("args");
                        Iterator<String> keys2 = optJSONObject6.keys();
                        while (keys2.hasNext()) {
                            String next2 = keys2.next();
                            aVar.b().put(next2, optJSONObject6.optString(next2));
                        }
                        aVarArr2[i7] = aVar;
                        i6 = i7 + 1;
                    }
                    aVarArr = aVarArr2;
                }
                eVarArr[i5] = new cn.appmedia.ad.d.e(optJSONObject4.optString("et"), optString, aVarArr);
            }
            iVar.a(gVarArr);
            iVar.a(eVarArr);
            iVarArr[i] = iVar;
        }
        hVar.a(iVarArr);
        return hVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public static String a(c cVar) {
        JSONObject jSONObject = new JSONObject();
        if (cVar.a() != null) {
            jSONObject.put("uid", cVar.a());
        }
        jSONObject.put("aid", cVar.b());
        jSONObject.put("ver", cVar.c());
        jSONObject.put("type", cVar.d());
        jSONObject.put("test", false);
        JSONObject jSONObject2 = new JSONObject();
        for (String str : cVar.e().keySet()) {
            jSONObject2.put(str, cVar.e().get(str));
        }
        jSONObject.put("args", jSONObject2);
        return jSONObject.toString();
    }

    public static Object b(String str) {
        b bVar = new b();
        JSONObject jSONObject = (JSONObject) new JSONTokener(str).nextValue();
        bVar.c(jSONObject.optInt("st"));
        bVar.a(jSONObject.optString("uid"));
        bVar.a(f.a(jSONObject.optString("tc")));
        bVar.b(f.a(jSONObject.optString("bgc")));
        return bVar;
    }
}
