package cn.appmedia.ad.h;

import cn.appmedia.ad.a.d;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

class c implements HttpResponseInterceptor {
    final /* synthetic */ f a;

    c(f fVar) {
        this.a = fVar;
    }

    public void process(HttpResponse httpResponse, HttpContext httpContext) {
        Header contentEncoding;
        HttpEntity entity = httpResponse.getEntity();
        if (entity != null && (contentEncoding = entity.getContentEncoding()) != null) {
            HeaderElement[] elements = contentEncoding.getElements();
            for (HeaderElement name : elements) {
                if (name.getName().equalsIgnoreCase("gzip")) {
                    d.b(EntityUtils.toString(entity, this.a.a(entity)));
                    httpResponse.setEntity(new b(httpResponse.getEntity()));
                    return;
                }
            }
        }
    }
}
