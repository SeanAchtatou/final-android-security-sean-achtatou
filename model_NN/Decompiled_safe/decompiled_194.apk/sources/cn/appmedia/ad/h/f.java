package cn.appmedia.ad.h;

import cn.appmedia.ad.a.d;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

public class f {
    private static f a;
    private HttpClient b = b();

    private f() {
    }

    public static f a() {
        if (a == null) {
            a = new f();
        }
        return a;
    }

    /* access modifiers changed from: private */
    public String a(HttpEntity httpEntity) {
        NameValuePair parameterByName;
        String str = null;
        Header contentType = httpEntity.getContentType();
        if (contentType == null) {
            return null;
        }
        HeaderElement[] elements = contentType.getElements();
        if (elements.length > 0 && (parameterByName = elements[0].getParameterByName("charset")) != null) {
            str = parameterByName.getValue();
        }
        return str == null ? "UTF-8" : str;
    }

    private HttpClient b() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(basicHttpParams, "UTF-8");
        HttpProtocolParams.setUseExpectContinue(basicHttpParams, true);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 30000);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
        defaultHttpClient.addRequestInterceptor(new d(this));
        defaultHttpClient.addResponseInterceptor(new c(this));
        return defaultHttpClient;
    }

    public InputStream a(String str) {
        try {
            HttpResponse execute = this.b.execute(new HttpGet(str));
            if (execute.getStatusLine().getStatusCode() == 200) {
                return execute.getEntity().getContent();
            }
            return null;
        } catch (ConnectTimeoutException e) {
            d.c(e.getMessage());
            throw e;
        } catch (IOException e2) {
            d.c(e2.getMessage());
            throw e2;
        }
    }

    public String a(String str, String str2) {
        String str3 = null;
        d.b("post url:" + str);
        d.b("post request:" + str2);
        try {
            HttpPost httpPost = new HttpPost(str);
            if (str2 != null && !str2.equalsIgnoreCase("")) {
                httpPost.setEntity(new StringEntity(str2));
            }
            HttpResponse execute = this.b.execute(httpPost);
            d.b("post response status:" + execute.getStatusLine().getStatusCode());
            if (execute.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = execute.getEntity();
                if (entity != null) {
                    str3 = EntityUtils.toString(entity, a(entity));
                }
            } else {
                str3 = "fail";
            }
            d.b("post resp:" + str3);
            return str3;
        } catch (ConnectTimeoutException e) {
            d.c(e.getMessage());
            throw e;
        } catch (IOException e2) {
            d.c(e2.getMessage());
            throw e2;
        }
    }
}
