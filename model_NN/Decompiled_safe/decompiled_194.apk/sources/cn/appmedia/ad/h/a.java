package cn.appmedia.ad.h;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import cn.appmedia.ad.a.d;
import cn.appmedia.ad.a.f;
import cn.appmedia.ad.c.j;
import cn.appmedia.ad.d.b;
import cn.appmedia.ad.d.c;
import cn.appmedia.ad.d.h;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;

public class a {
    private static final String a = Build.VERSION.RELEASE;
    private static final String b = Build.MODEL;
    private static long c = Calendar.getInstance().getTimeInMillis();
    private static a d;
    private String e;
    private String f;
    private String g;
    private final String h = "1A2df@g9&S*#eRF4";

    private a() {
    }

    private c a(String str, String str2) {
        c b2 = b();
        b2.d("getad");
        b2.e().put("loc", str);
        b2.e().put("res", str2);
        b2.e().put("model", b);
        return b2;
    }

    private c a(String str, ArrayList arrayList, ArrayList arrayList2) {
        c b2 = b();
        b2.d("info");
        b2.e().put("it", str);
        b2.e().put("model", b);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("sys:");
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            stringBuffer.append((String) arrayList.get(i));
            if (i < size - 1) {
                stringBuffer.append("|");
            }
        }
        stringBuffer.append(",third:");
        int size2 = arrayList2.size();
        for (int i2 = 0; i2 < size2; i2++) {
            stringBuffer.append((String) arrayList2.get(i2));
            if (i2 < size2 - 1) {
                stringBuffer.append("|");
            }
        }
        b2.e().put("ic", stringBuffer.toString());
        return b2;
    }

    public static a a() {
        if (d == null) {
            d = new a();
        }
        return d;
    }

    private c b() {
        c cVar = new c();
        cVar.b(this.g);
        cVar.a(this.f);
        cVar.c("A-1.0.2");
        return cVar;
    }

    private String c(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String subscriberId = telephonyManager.getSubscriberId();
        if (subscriberId == null) {
            subscriberId = "0";
        }
        String str = "0" == 0 ? "0" : "0";
        String deviceId = telephonyManager.getDeviceId();
        if (deviceId == null) {
            deviceId = "0";
        }
        return String.valueOf(subscriberId) + "-" + "0" + "-" + str + "-" + deviceId;
    }

    private c d(Context context) {
        c b2 = b();
        b2.d("login");
        b2.e().put("os", a);
        b2.e().put("model", b);
        b2.e().put("t", c(context));
        return b2;
    }

    private c e(String str) {
        c b2 = b();
        b2.d("data");
        b2.e().put("did", str);
        b2.e().put("model", b);
        return b2;
    }

    public b a(Context context) {
        c d2 = d(context);
        d.b("application id:" + d2.b());
        c = Calendar.getInstance().getTimeInMillis();
        d.b("login time:" + c);
        try {
            String a2 = e.a(d2);
            float b2 = j.b();
            this.e = String.valueOf(String.valueOf((int) Math.ceil((double) (((float) j.c()) * b2)))) + "x" + String.valueOf((int) Math.ceil((double) (b2 * ((float) j.d()))));
            String a3 = f.a().a("http://www.appmedia.cn/add/adrequest/login.do", a2);
            if (a3.equalsIgnoreCase("fail")) {
                return null;
            }
            return (b) e.b(a3);
        } catch (ConnectTimeoutException e2) {
            d.c(e2.getMessage());
            return null;
        } catch (JSONException e3) {
            d.c(e3.getMessage());
            return null;
        } catch (IOException e4) {
            d.c(e4.getMessage());
            return null;
        }
    }

    public void a(String str) {
        this.g = str;
    }

    public boolean a(ArrayList arrayList, ArrayList arrayList2) {
        if (arrayList == null) {
            return false;
        }
        try {
            String a2 = e.a(a("app", arrayList, arrayList2));
            int length = a2.getBytes().length;
            d.b(a2);
            d.b("length=" + length);
            String a3 = f.a("1A2df@g9&S*#eRF4", a2);
            int length2 = a3.getBytes().length;
            d.b(a3);
            d.b("length=" + length2);
            if (!f.a().a("http://www.appmedia.cn/add/adrequest/info.do", a3).equalsIgnoreCase("fail")) {
                return true;
            }
            d.b("send info fail");
            return false;
        } catch (Exception e2) {
            d.c(e2.getMessage());
            return false;
        }
    }

    public String b(Context context) {
        d.b("logout time:" + Calendar.getInstance().getTimeInMillis());
        c = Calendar.getInstance().getTimeInMillis() - c;
        d.b("online time:" + (c / 1000));
        c b2 = b();
        b2.e().put("os", a);
        b2.e().put("model", b);
        b2.e().put("t", c(context));
        b2.e().put("olt", String.valueOf(c / 1000));
        b2.d("logout");
        try {
            return f.a().a("http://www.appmedia.cn/add/adrequest/logout.do", e.a(b2));
        } catch (JSONException e2) {
            d.c(e2.toString());
            return "";
        } catch (IOException e3) {
            d.c(e3.toString());
            return "";
        }
    }

    public void b(String str) {
        this.f = str;
    }

    public h c(String str) {
        try {
            String a2 = f.a().a("http://www.appmedia.cn/add/adrequest/request.do", e.a(a(str, this.e)));
            if (!a2.equalsIgnoreCase("fail")) {
                return e.a(a2);
            }
        } catch (ConnectTimeoutException e2) {
            d.c(e2.getMessage());
            return null;
        } catch (JSONException e3) {
            d.c(e3.getMessage());
            return null;
        } catch (IOException e4) {
            d.c(e4.getMessage());
        }
        return null;
    }

    public boolean d(String str) {
        try {
            if (f.a().a("http://www.appmedia.cn/add/adrequest/stat.do", e.a(e(str))).equalsIgnoreCase("fail")) {
                return false;
            }
            d.b("send did success");
            return true;
        } catch (JSONException e2) {
            d.c(e2.getMessage());
            return false;
        } catch (ConnectTimeoutException e3) {
            d.c(e3.getMessage());
            return false;
        } catch (IOException e4) {
            d.c(e4.getMessage());
            return false;
        }
    }
}
