package cn.appmedia.ad.f;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.util.Map;

public class d implements b {
    private Context a;

    public void a(Map map, Context context) {
        this.a = context;
        Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + ((String) map.get("msisdn"))));
        intent.putExtra("sms_body", (String) map.get("msg"));
        this.a.startActivity(intent);
    }
}
