package cn.appmedia.ad.f;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import cn.appmedia.ad.AdActivity;
import cn.appmedia.ad.a.d;
import cn.domob.android.ads.DomobAdManager;
import com.adchina.android.ads.Common;
import java.util.Map;

public class c implements b {
    public void a(Map map, Context context) {
        String str = (String) map.get(DomobAdManager.ACTION_URL);
        String str2 = (String) map.get("target");
        if (str2 == null || str2.length() == 0) {
            str2 = "browser";
        }
        String str3 = !str.toLowerCase().startsWith("http://") ? "http://" + str : str;
        if (str2.equals("browser")) {
            d.b("open browser action");
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str3)));
        } else if (str2.equals("current")) {
            Intent intent = new Intent();
            intent.setClass(context, AdActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(DomobAdManager.ACTION_URL, str3);
            bundle.putString("type", Common.KCLK);
            intent.putExtras(bundle);
            context.startActivity(intent);
        } else {
            ((String) map.get("target")).equals("none");
        }
    }
}
