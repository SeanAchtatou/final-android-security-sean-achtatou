package cn.appmedia.ad.a;

import android.content.Context;
import cn.appmedia.ad.c.h;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public final class b {
    private static b a;

    public static b a() {
        if (a == null) {
            a = new b();
        }
        return a;
    }

    public InputStream a(Context context, String str) {
        try {
            return new FileInputStream(String.valueOf(context.getCacheDir().getPath()) + "/" + str);
        } catch (FileNotFoundException e) {
            d.c(e.getMessage());
            return null;
        }
    }

    public void a(String str, InputStream inputStream) {
        d.b("cacheImage start");
        FileOutputStream fileOutputStream = null;
        try {
            FileOutputStream fileOutputStream2 = new FileOutputStream(new File(h.a().e().getCacheDir(), str));
            try {
                byte[] bArr = new byte[128];
                while (inputStream.read(bArr) != -1) {
                    fileOutputStream2.write(bArr);
                }
                try {
                    inputStream.close();
                    fileOutputStream2.close();
                } catch (IOException e) {
                    d.c(e.getMessage());
                }
            } catch (Exception e2) {
                Exception exc = e2;
                fileOutputStream = fileOutputStream2;
                e = exc;
            } catch (Throwable th) {
                Throwable th2 = th;
                fileOutputStream = fileOutputStream2;
                th = th2;
                try {
                    inputStream.close();
                    fileOutputStream.close();
                } catch (IOException e3) {
                    d.c(e3.getMessage());
                }
                throw th;
            }
        } catch (Exception e4) {
            e = e4;
            try {
                d.c(e.getMessage());
                try {
                    inputStream.close();
                    fileOutputStream.close();
                } catch (IOException e5) {
                    d.c(e5.getMessage());
                }
                d.b("cacheImage end");
            } catch (Throwable th3) {
                th = th3;
                inputStream.close();
                fileOutputStream.close();
                throw th;
            }
        }
        d.b("cacheImage end");
    }
}
