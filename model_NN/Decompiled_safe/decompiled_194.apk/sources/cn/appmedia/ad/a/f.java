package cn.appmedia.ad.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import java.io.IOException;
import java.io.InputStream;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class f {
    public static int a(String str) {
        int indexOf = str.toLowerCase().indexOf("0x");
        if (indexOf == -1) {
            return 0;
        }
        String substring = str.substring(indexOf + 2);
        String substring2 = substring.substring(0, 2);
        return Integer.parseInt(substring.substring(2), 16) | (Integer.parseInt(substring2, 16) << 24);
    }

    public static Bitmap a(Context context, String str) {
        InputStream inputStream;
        try {
            InputStream open = context.getAssets().open(str);
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(open);
                try {
                    open.close();
                } catch (Exception e) {
                    d.c(e.getMessage());
                }
                return decodeStream;
            } catch (IOException e2) {
                IOException iOException = e2;
                inputStream = open;
                e = iOException;
            } catch (Throwable th) {
                Throwable th2 = th;
                inputStream = open;
                th = th2;
                try {
                    inputStream.close();
                } catch (Exception e3) {
                    d.c(e3.getMessage());
                }
                throw th;
            }
        } catch (IOException e4) {
            e = e4;
            inputStream = null;
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            inputStream.close();
            throw th;
        }
        return null;
        try {
            d.c(e.getMessage());
            try {
                inputStream.close();
            } catch (Exception e5) {
                d.c(e5.getMessage());
            }
            return null;
        } catch (Throwable th4) {
            th = th4;
            inputStream.close();
            throw th;
        }
    }

    public static Drawable a(int[] iArr) {
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, iArr);
        gradientDrawable.setBounds(0, 0, 320, 50);
        gradientDrawable.setGradientType(0);
        return gradientDrawable;
    }

    public static String a(String str, String str2) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(str.getBytes(), "AES");
        Cipher instance = Cipher.getInstance("AES");
        instance.init(1, secretKeySpec);
        return a.a(instance.doFinal(str2.getBytes()), 0);
    }
}
