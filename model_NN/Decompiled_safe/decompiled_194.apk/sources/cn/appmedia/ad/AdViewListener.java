package cn.appmedia.ad;

public interface AdViewListener {
    void onReceiveAdFailure(BannerAdView bannerAdView);

    void onReceiveAdSuccess(BannerAdView bannerAdView);
}
