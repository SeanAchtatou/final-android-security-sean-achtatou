package cn.appmedia.ad.e;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import cn.appmedia.ad.b.d;
import cn.appmedia.ad.b.e;
import cn.appmedia.ad.c.j;
import cn.appmedia.ad.d.g;
import java.util.Vector;

public final class h extends i {
    protected Vector a = new Vector();
    private c h;
    private float i = j.b();

    public h(String str, int i2, int i3) {
        super(str, i2, i3);
    }

    public void a() {
        int size = this.f.size();
        for (int i2 = 0; i2 < size; i2++) {
            g gVar = (g) this.f.get(i2);
            e a2 = d.a(gVar.a());
            if (a2 != null) {
                try {
                    a2.a(gVar.b());
                } catch (Exception e) {
                    cn.appmedia.ad.a.d.c(e.toString());
                }
                a2.a(gVar.c());
                this.a.add(a2);
            }
        }
        this.f.clear();
    }

    public void a(Context context, ViewGroup viewGroup) {
        int i2;
        int i3;
        int i4;
        int i5;
        AbsoluteLayout absoluteLayout = new AbsoluteLayout(context);
        absoluteLayout.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        int size = this.a.size();
        for (int i6 = 0; i6 < size; i6++) {
            e eVar = (e) this.a.get(i6);
            View a2 = eVar.a(context);
            if (a2 != null) {
                int[] a3 = eVar.a();
                if (j.e()) {
                    int i7 = (int) (((float) a3[0]) / this.i);
                    i2 = (int) (((float) a3[1]) / this.i);
                    i3 = ((int) (((float) a3[3]) / this.i)) - i2;
                    i4 = i7;
                    i5 = ((int) (((float) a3[2]) / this.i)) - i7;
                } else {
                    int i8 = a3[0];
                    i2 = a3[1];
                    i3 = a3[3] - i2;
                    i4 = i8;
                    i5 = a3[2] - i8;
                }
                cn.appmedia.ad.a.d.b("rect:" + i4 + " " + i2 + " " + i5 + " " + i3);
                a2.setLayoutParams(new AbsoluteLayout.LayoutParams(i5, i3, i4, i2));
                absoluteLayout.addView(a2);
            }
        }
        this.h = new c();
        this.h.a(absoluteLayout, viewGroup);
    }
}
