package cn.appmedia.ad.e;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;

public class c {
    /* access modifiers changed from: private */
    public View a;
    private View b;
    /* access modifiers changed from: private */
    public ViewGroup c;

    public c() {
        this.a = null;
        this.b = null;
        this.a = null;
    }

    /* access modifiers changed from: private */
    public void a() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, -1.0f, 2, 0.0f);
        translateAnimation.setAnimationListener(new a(this));
        translateAnimation.setDuration(1000);
        this.c.addView(this.b);
        this.b.startAnimation(translateAnimation);
    }

    private void b() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 0.0f, 2, -1.0f);
        translateAnimation.setDuration(1000);
        translateAnimation.setAnimationListener(new b(this));
        this.a.startAnimation(translateAnimation);
    }

    public void a(View view, ViewGroup viewGroup) {
        this.c = viewGroup;
        this.b = view;
        if (this.c.getChildCount() > 0) {
            this.a = this.c.getChildAt(0);
            b();
            return;
        }
        ((RelativeLayout) viewGroup).setGravity(1);
        viewGroup.addView(view);
    }
}
