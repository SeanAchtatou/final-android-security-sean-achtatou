package cn.appmedia.ad.e;

import android.content.Context;
import android.view.ViewGroup;
import cn.appmedia.ad.d.e;
import cn.appmedia.ad.d.g;
import java.util.Hashtable;
import java.util.Vector;

public abstract class i {
    protected String b;
    protected int c;
    protected int d;
    protected int e;
    protected Vector f = new Vector();
    protected Hashtable g = new Hashtable();

    public i(String str, int i, int i2) {
        this.b = str;
        this.c = i;
        this.d = i2;
        this.e = 0;
    }

    public e a(String str) {
        if (this.g.containsKey(str)) {
            return (e) this.g.get(str);
        }
        return null;
    }

    public void a() {
    }

    public void a(int i) {
        this.e = i;
    }

    public void a(Context context, ViewGroup viewGroup) {
    }

    public void a(g gVar) {
        this.f.add(gVar);
    }

    public int d() {
        return this.e;
    }

    public Hashtable e() {
        return this.g;
    }
}
