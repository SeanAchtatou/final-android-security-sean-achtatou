package cn.appmedia.ad.e;

import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;

class g implements View.OnTouchListener {
    final /* synthetic */ d a;
    private final /* synthetic */ Drawable b;
    private final /* synthetic */ Drawable c;

    g(d dVar, Drawable drawable, Drawable drawable2) {
        this.a = dVar;
        this.b = drawable;
        this.c = drawable2;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            view.setBackgroundDrawable(this.b);
            return false;
        } else if (motionEvent.getAction() != 1) {
            return false;
        } else {
            view.setBackgroundDrawable(this.c);
            return false;
        }
    }
}
