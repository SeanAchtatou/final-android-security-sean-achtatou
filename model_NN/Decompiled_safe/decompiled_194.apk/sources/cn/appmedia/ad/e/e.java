package cn.appmedia.ad.e;

import android.os.CountDownTimer;
import android.widget.TextView;

class e extends CountDownTimer {
    final /* synthetic */ d a;
    private final /* synthetic */ TextView b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    e(d dVar, long j, long j2, TextView textView) {
        super(j, j2);
        this.a = dVar;
        this.b = textView;
    }

    public void onFinish() {
        this.a.j = this.a.e;
        this.a.h.finish();
    }

    public void onTick(long j) {
        this.b.setText("广告剩余时间" + this.a.j + "秒");
        d dVar = this.a;
        dVar.j = dVar.j - 1;
    }
}
