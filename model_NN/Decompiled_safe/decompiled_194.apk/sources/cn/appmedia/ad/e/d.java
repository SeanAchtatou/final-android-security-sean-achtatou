package cn.appmedia.ad.e;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.appmedia.ad.AdActivity;
import cn.appmedia.ad.a.f;
import cn.appmedia.ad.b.e;
import cn.appmedia.ad.d.g;
import com.smaato.SOMA.SOMATextBanner;
import java.util.Iterator;
import java.util.Vector;

public final class d extends i {
    private static View i;
    /* access modifiers changed from: private */
    public static CountDownTimer k;
    protected final Vector a = new Vector();
    /* access modifiers changed from: private */
    public Activity h;
    /* access modifiers changed from: private */
    public int j = 10;

    public d(String str, int i2, int i3) {
        super(str, i2, i3);
    }

    private View a(Context context) {
        BitmapDrawable bitmapDrawable = new BitmapDrawable(f.a(context, "320button1.png"));
        BitmapDrawable bitmapDrawable2 = new BitmapDrawable(f.a(context, "320button3.png"));
        Drawable a2 = f.a(new int[]{-1, -6710887});
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -2);
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setLayoutParams(layoutParams);
        relativeLayout.setBackgroundDrawable(a2);
        TextView textView = new TextView(context);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(10);
        textView.setLayoutParams(layoutParams2);
        textView.setTextColor((int) SOMATextBanner.DEFAULT_BACKGROUND_COLOR);
        ImageButton imageButton = new ImageButton(context);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(11);
        imageButton.setLayoutParams(layoutParams3);
        imageButton.setBackgroundDrawable(bitmapDrawable);
        imageButton.setOnClickListener(new f(this));
        imageButton.setOnTouchListener(new g(this, bitmapDrawable2, bitmapDrawable));
        k = new e(this, 6000, 1000, textView);
        relativeLayout.addView(textView);
        relativeLayout.addView(imageButton);
        return relativeLayout;
    }

    public static void b() {
        if (i != null) {
            i.setVisibility(0);
            k.start();
            i = null;
            k = null;
        }
    }

    public void a() {
        int size = this.f.size();
        for (int i2 = 0; i2 < size; i2++) {
            g gVar = (g) this.f.get(i2);
            e a2 = cn.appmedia.ad.b.d.a(gVar.a());
            if (a2 != null) {
                try {
                    a2.a(gVar.b());
                } catch (Exception e) {
                    cn.appmedia.ad.a.d.c(e.toString());
                }
                a2.a(gVar.c());
                this.a.add(a2);
            }
        }
    }

    public void a(Context context, ViewGroup viewGroup) {
        this.h = (AdActivity) context;
        View view = null;
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            view = ((e) it.next()).a(context);
        }
        this.j = this.e;
        i = a(context);
        i.setVisibility(4);
        viewGroup.addView(i);
        viewGroup.addView(view, new ViewGroup.LayoutParams(-1, -1));
    }
}
