package cn.appmedia.ad;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.view.ViewGroup;
import cn.appmedia.ad.a.d;
import cn.appmedia.ad.a.f;
import cn.appmedia.ad.c.j;
import cn.appmedia.ad.e.i;

class a implements Handler.Callback {
    final /* synthetic */ BannerAdView a;
    private ViewGroup b;

    public a(BannerAdView bannerAdView, ViewGroup viewGroup) {
        this.a = bannerAdView;
        this.b = viewGroup;
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 1:
                Drawable a2 = f.a(new int[]{j.c, j.c});
                d.a("handleMessage MSG_RENDER_AD");
                this.a.setBackgroundDrawable(a2);
                ((i) message.obj).a(this.a.getContext(), this.b);
                return true;
            case 2:
                d.a("handleMessage MSG_LOAD_AD");
                int i = message.arg1;
                if (i == 0) {
                    d.a("load ad success");
                    this.a.mDefaultListener.onReceiveAdSuccess((BannerAdView) this.b);
                    if (this.a.mListener != null) {
                        this.a.mListener.onReceiveAdSuccess((BannerAdView) this.b);
                    }
                } else if (i == 2) {
                    d.a("no ad load");
                    this.a.mAdClient.l();
                    this.a.setVisibility(8);
                } else {
                    d.c("load fail");
                    this.a.mDefaultListener.onReceiveAdFailure((BannerAdView) this.b);
                    if (this.a.mListener != null) {
                        this.a.mListener.onReceiveAdFailure((BannerAdView) this.b);
                    }
                }
                return true;
            case 3:
                d.a("handleMessage MSG_CLEAR_AD");
                return true;
            default:
                return false;
        }
    }
}
