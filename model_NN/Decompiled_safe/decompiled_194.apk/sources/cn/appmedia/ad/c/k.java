package cn.appmedia.ad.c;

import android.content.Context;
import android.text.TextUtils;
import cn.appmedia.ad.d.e;
import cn.appmedia.ad.e.i;
import cn.appmedia.ad.f.b;
import cn.appmedia.ad.f.f;
import cn.appmedia.ad.h.a;

public final class k {
    public void a(String str, i iVar, Context context) {
        e a = iVar.a(str);
        if (a != null) {
            if (!TextUtils.isEmpty(a.c())) {
                a.a().d(a.c());
            }
            cn.appmedia.ad.d.a[] b = a.b();
            if (b != null) {
                for (cn.appmedia.ad.d.a aVar : b) {
                    b a2 = f.a(aVar.a());
                    if (a2 != null) {
                        a2.a(aVar.b(), context);
                        if (!TextUtils.isEmpty(aVar.c())) {
                            a.a().d(aVar.c());
                        }
                    }
                }
            }
        }
    }
}
