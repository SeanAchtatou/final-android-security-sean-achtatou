package cn.appmedia.ad.c;

import android.content.Context;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import cn.appmedia.ad.a.d;
import cn.appmedia.ad.e.i;
import cn.appmedia.ad.g.b;
import cn.appmedia.ad.h.a;

public class j {
    public static boolean a = false;
    public static int b;
    public static int c;
    public static i f;
    private static c h;
    private static float k;
    private static int l;
    private static int m;
    private static boolean n = false;
    /* access modifiers changed from: private */
    public static Object p = new Object();
    public b d;
    public Context e;
    public h g;
    private k i;
    private Handler j;
    private String o = "";

    public j(String str, Context context, b bVar, Handler handler) {
        this.d = bVar;
        this.d.a(this);
        this.j = handler;
        this.i = new k();
        this.g = h.a();
        this.g.a(context.getApplicationContext());
        this.e = context;
        this.o = str;
        a(context);
    }

    private void a(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) this.e.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        k = displayMetrics.density;
        int i2 = displayMetrics.widthPixels;
        int i3 = displayMetrics.heightPixels;
        if (i2 <= 320) {
            l = (int) Math.ceil((double) (((float) i2) * k));
            m = (int) Math.ceil((double) (((float) i3) * k));
            n = true;
        } else {
            l = i2;
            m = i3;
            n = false;
        }
        d.a("screen: " + l + "*" + m);
    }

    public static float b() {
        return k;
    }

    public static int c() {
        return l;
    }

    public static int d() {
        return m;
    }

    public static boolean e() {
        return n;
    }

    public static i k() {
        return f;
    }

    private void o() {
        d.a("start show thread");
        if (h == null || !h.isAlive()) {
            h = new c(this, "ShowAdThread", this.d, this.i, this.o);
            h.start();
            return;
        }
        h.a(this.d, this.i, this.o);
    }

    public String a() {
        return this.o;
    }

    public void f() {
        this.g.b(this);
    }

    public Handler g() {
        return this.j;
    }

    public void h() {
        if (h.b() != 0) {
            this.g.a(this);
        }
        o();
    }

    public void i() {
        l();
        this.g.c();
    }

    public void j() {
        this.i.a("click", f, this.e);
    }

    public void l() {
        if (h != null) {
            h.a(false);
            h = null;
            d.a("stop show ad thread");
        }
    }

    public boolean m() {
        if (!a) {
            return false;
        }
        a.a().b(this.e);
        d.a("logout success");
        a = false;
        return true;
    }
}
