package cn.appmedia.ad.c;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Message;
import cn.appmedia.ad.a.d;
import cn.appmedia.ad.d.b;
import cn.appmedia.ad.d.i;
import cn.appmedia.ad.h.a;
import com.energysource.szj.embeded.AdManager;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class h {
    private static h a;
    private static int c = 60;
    private final long b = 86400000;
    private Context d;
    private Timer e;
    private i f;
    private g g = new g();

    private h() {
    }

    public static synchronized h a() {
        h hVar;
        synchronized (h.class) {
            if (a == null) {
                a = new h();
            }
            hVar = a;
        }
        return hVar;
    }

    public static void a(int i) {
        int i2 = 60;
        if (i >= 60) {
            i2 = i;
        }
        c = i2;
    }

    private void a(j jVar, int i) {
        if (jVar != null) {
            Message.obtain(jVar.g(), 2, i, 0).sendToTarget();
        }
    }

    private void a(Timer timer) {
        timer.cancel();
        timer.purge();
        d.b("stop timer");
    }

    private void a(TimerTask timerTask) {
        timerTask.cancel();
        d.b("stop timerTask");
    }

    public static int b() {
        return c;
    }

    private ArrayList b(int i) {
        switch (i) {
            case 0:
                return f();
            case 1:
                return i();
            case 2:
                return h();
            case 3:
                return g();
            default:
                return null;
        }
    }

    private boolean c(j jVar) {
        b a2 = a.a().a(jVar.e);
        if (a2 == null) {
            return false;
        }
        switch (a2.d()) {
            case 0:
                d.a("login success");
                a.a().b(a2.a());
                j.c = a2.c();
                j.b = a2.b();
                d();
                return true;
            default:
                d.c("login failure");
                return false;
        }
    }

    /* access modifiers changed from: private */
    public void d(j jVar) {
        if (!j.a) {
            j.a = c(jVar);
        }
        if (j.a) {
            d.a("start load ad");
            cn.appmedia.ad.d.h c2 = a.a().c(jVar.a());
            if (c2 == null || c2.d() != 0) {
                d.c("load ad failure");
                a(jVar, 1);
                return;
            }
            i[] a2 = c2.a();
            if (a2.length == 0) {
                this.g.a(jVar.a()).a();
                a.a().d("did_view_0");
                a(jVar, 2);
                a(jVar);
                return;
            }
            Vector vector = new Vector();
            for (i iVar : a2) {
                if (iVar != null) {
                    try {
                        vector.add(a.a(iVar));
                    } catch (Exception e2) {
                        d.c(e2.toString());
                    }
                }
            }
            if (vector.size() > 0) {
                this.g.a(jVar.a()).a(vector);
                a(jVar, 0);
                return;
            }
            return;
        }
        a(jVar, 1);
    }

    private ArrayList f() {
        ArrayList arrayList = new ArrayList();
        for (PackageInfo packageInfo : this.d.getPackageManager().getInstalledPackages(0)) {
            arrayList.add(packageInfo.applicationInfo.packageName);
        }
        return arrayList;
    }

    private ArrayList g() {
        ArrayList arrayList = new ArrayList();
        for (PackageInfo next : this.d.getPackageManager().getInstalledPackages(1)) {
            if ((next.applicationInfo.flags & 1) > 0) {
                arrayList.add(next.applicationInfo.packageName);
            }
        }
        return arrayList;
    }

    private ArrayList h() {
        ArrayList arrayList = new ArrayList();
        for (PackageInfo next : this.d.getPackageManager().getInstalledPackages(0)) {
            if ((next.applicationInfo.flags & 1) <= 0) {
                arrayList.add(next.applicationInfo.packageName);
            }
        }
        return arrayList;
    }

    private ArrayList i() {
        return null;
    }

    public cn.appmedia.ad.e.i a(String str) {
        return this.g.a(str).b();
    }

    public void a(Context context) {
        this.d = context;
    }

    public void a(j jVar) {
        if (this.f != null) {
            a(this.f);
        }
        this.f = new i(this, jVar);
        if (this.e != null) {
            a(this.e);
        }
        this.e = new Timer("AdContainerTimer");
        this.e.scheduleAtFixedRate(this.f, (long) (c * AdManager.AD_FILL_PARENT), (long) (c * AdManager.AD_FILL_PARENT));
    }

    public void b(j jVar) {
        new b(this, "GetAdThread", jVar).start();
    }

    public void c() {
        if (this.f != null) {
            a(this.f);
        }
        if (this.e != null) {
            a(this.e);
        }
        d.a("stop get ad thread");
    }

    public void d() {
        e a2 = e.a();
        long b2 = a2.b();
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - b2 > 86400000) {
            a2.a(currentTimeMillis);
            new l(this, b(3), b(2)).start();
        }
    }

    public Context e() {
        return this.d;
    }
}
