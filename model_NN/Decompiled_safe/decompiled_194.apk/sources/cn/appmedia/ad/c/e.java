package cn.appmedia.ad.c;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class e {
    private static e a;
    private SQLiteDatabase b = this.c.getWritableDatabase();
    private SQLiteOpenHelper c = new f(h.a().e(), "appmedia.db", null, 1);

    private e() {
    }

    public static e a() {
        if (a == null) {
            a = new e();
        }
        return a;
    }

    public void a(long j) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("time", Long.valueOf(j));
        int i = 0;
        Cursor rawQuery = this.b.rawQuery("SELECT COUNT(*) AS count FROM scan", null);
        int columnIndex = rawQuery.getColumnIndex("count");
        if (columnIndex > -1 && rawQuery.moveToNext()) {
            i = rawQuery.getInt(columnIndex);
        }
        rawQuery.close();
        if (i == 0) {
            this.b.insert("scan", "time", contentValues);
        } else {
            this.b.update("scan", contentValues, null, null);
        }
    }

    public long b() {
        long j = 0;
        Cursor rawQuery = this.b.rawQuery("SELECT time FROM scan", null);
        if (rawQuery.moveToNext()) {
            j = rawQuery.getLong(rawQuery.getColumnIndex("time"));
        }
        rawQuery.close();
        return j;
    }
}
