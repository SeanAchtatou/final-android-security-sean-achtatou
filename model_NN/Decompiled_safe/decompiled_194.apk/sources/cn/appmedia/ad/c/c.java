package cn.appmedia.ad.c;

import cn.appmedia.ad.a.d;
import cn.appmedia.ad.e.i;
import cn.appmedia.ad.g.b;
import com.energysource.szj.embeded.AdManager;

public class c extends Thread {
    final /* synthetic */ j a;
    private boolean b = true;
    private b c;
    private k d;
    private String e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(j jVar, String str, b bVar, k kVar, String str2) {
        super(str);
        this.a = jVar;
        synchronized (this) {
            this.c = bVar;
            this.d = kVar;
            this.e = str2;
        }
    }

    private int a() {
        i a2;
        synchronized (j.p) {
            a2 = this.a.g.a(this.e);
        }
        if (a2 == null) {
            return 0;
        }
        d.a("show ad");
        j.f = a2;
        synchronized (j.p) {
            this.c.a(j.f);
            this.d.a("view", j.f, this.a.e);
        }
        return j.f.d();
    }

    public void a(b bVar, k kVar, String str) {
        synchronized (j.p) {
            this.c = bVar;
            this.d = kVar;
            this.e = str;
        }
    }

    public void a(boolean z) {
        this.b = z;
    }

    public void run() {
        boolean z = false;
        long j = 0;
        long j2 = 0;
        while (this.b) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - j >= j2) {
                z = false;
            }
            if (!z) {
                j2 = (long) (a() * AdManager.AD_FILL_PARENT);
                z = true;
                j = currentTimeMillis;
            }
            if (j2 == 0 || (j.f instanceof cn.appmedia.ad.e.d)) {
                d.b("always_show_ad or popad");
                a(false);
                return;
            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e2) {
                d.c(e2.getMessage());
            }
        }
    }
}
