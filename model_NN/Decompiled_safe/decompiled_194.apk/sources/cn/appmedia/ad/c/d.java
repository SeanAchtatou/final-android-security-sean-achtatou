package cn.appmedia.ad.c;

import cn.appmedia.ad.e.i;
import java.util.Vector;

public final class d {
    private final Vector a = new Vector();
    private final Vector b = new Vector();
    private int c = 0;

    public synchronized void a() {
        if (!this.b.isEmpty()) {
            this.b.clear();
        }
        if (!this.a.isEmpty()) {
            this.a.clear();
        }
        cn.appmedia.ad.a.d.b("clear adPool");
    }

    public synchronized void a(Vector vector) {
        this.b.clear();
        this.b.addAll(vector);
    }

    public synchronized i b() {
        i iVar;
        int size = this.a.size();
        int size2 = this.b.size();
        if (size == 0 && size2 == 0) {
            iVar = null;
        } else {
            if (this.c >= size) {
                if (size2 > 0) {
                    this.a.clear();
                    this.a.addAll(this.b);
                }
                this.c = 0;
            }
            iVar = (i) this.a.elementAt(this.c);
            this.c++;
        }
        return iVar;
    }
}
