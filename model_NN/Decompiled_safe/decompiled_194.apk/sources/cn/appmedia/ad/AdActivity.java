package cn.appmedia.ad;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import cn.appmedia.ad.b.d;
import cn.appmedia.ad.b.e;
import cn.appmedia.ad.c.j;
import cn.appmedia.ad.e.i;
import cn.domob.android.ads.DomobAdManager;
import com.adchina.android.ads.Common;
import java.util.HashMap;

public class AdActivity extends Activity {
    private static Handler mHandler = null;
    private String type;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        Bundle extras = getIntent().getExtras();
        this.type = extras.getString("type");
        if (this.type.equalsIgnoreCase(Common.KCLK)) {
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(1);
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
            e a = d.a(3);
            HashMap hashMap = new HashMap();
            hashMap.put("s", extras.getString(DomobAdManager.ACTION_URL));
            a.a(hashMap);
            linearLayout.addView((WebView) a.a(getBaseContext()), layoutParams);
            setContentView(linearLayout);
        } else if (this.type.equalsIgnoreCase(Common.KIMP)) {
            String string = extras.getString("op");
            if (string == null || !string.equalsIgnoreCase("close")) {
                i k = j.k();
                LinearLayout linearLayout2 = new LinearLayout(this);
                linearLayout2.setOrientation(1);
                k.a(this, linearLayout2);
                setContentView(linearLayout2);
            } else {
                finish();
            }
        }
        super.onCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
