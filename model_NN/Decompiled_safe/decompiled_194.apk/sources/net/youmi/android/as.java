package net.youmi.android;

import android.app.Activity;
import android.view.animation.Animation;
import android.widget.RelativeLayout;

class as extends RelativeLayout implements ba {
    bz a;
    int b = -1;
    Activity c;
    int d;
    de e;
    final /* synthetic */ cw f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public as(cw cwVar, Activity activity, bz bzVar, int i, int i2) {
        super(activity);
        this.f = cwVar;
        this.a = bzVar;
        this.b = i;
        this.c = activity;
        this.d = i2;
        e();
    }

    private void e() {
        if (this.e == null) {
            this.e = new de(this.f, this.c, this.b, this.a, this.d);
            this.e.setVisibility(0);
        }
        this.e.setId(1003);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        this.a.a().c().b();
        layoutParams.addRule(9);
        layoutParams.addRule(15);
        addView(this.e, layoutParams);
    }

    public void a() {
        setVisibility(8);
    }

    public void a(Animation animation) {
        try {
            startAnimation(animation);
        } catch (Exception e2) {
        }
    }

    public boolean a(ct ctVar) {
        if (ctVar == null) {
            return false;
        }
        try {
            return this.e.a(ctVar);
        } catch (Exception e2) {
            return false;
        }
    }

    public void b() {
        this.e.b();
    }

    public void c() {
        setVisibility(0);
    }

    public void d() {
        this.e.d();
    }
}
