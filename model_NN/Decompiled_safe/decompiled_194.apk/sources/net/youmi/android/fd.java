package net.youmi.android;

import android.view.View;
import android.widget.RelativeLayout;
import java.util.ArrayList;

final class fd extends RelativeLayout implements ah, fc {
    AdActivity a;
    private cn b;
    private aq c;
    private bz d;

    public fd(AdActivity adActivity, bz bzVar) {
        super(adActivity);
        this.a = adActivity;
        this.d = bzVar;
        this.b = new cn(this.a, bzVar);
        this.c = new aq(this.a, this, bzVar);
        this.b.setId(1010);
        this.c.setId(1009);
        RelativeLayout.LayoutParams a2 = h.a(bzVar.b().a());
        a2.addRule(12);
        addView(this.c, a2);
        RelativeLayout.LayoutParams b2 = h.b();
        b2.addRule(2, this.c.getId());
        addView(this.b, b2);
    }

    private void c() {
        try {
            ArrayList a2 = q.a();
            if (a2 != null && a2.size() > 0) {
                q.b(this.a);
            }
        } catch (Exception e) {
        }
        this.a.a();
    }

    public void a() {
        c();
    }

    public View b() {
        return this;
    }

    public void b_() {
        c();
    }
}
