package net.youmi.android;

import com.smaato.SOMA.SOMATextBanner;
import java.io.ByteArrayOutputStream;

class ak {
    static final int[] a = {255, 65280, 16711680, SOMATextBanner.DEFAULT_BACKGROUND_COLOR};

    ak() {
    }

    static final int a(byte b, int i, int i2) {
        return i + i2 > 8 ? b : ((b << ((8 - i) - i2)) & 255) >>> (8 - i2);
    }

    static final int a(byte[] bArr, int i, int i2) {
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < i2; i5++) {
            i4 |= (bArr[i + i5] << i3) & a[i5];
            i3 += 8;
        }
        return i4;
    }

    static final void a(int i, int i2, ByteArrayOutputStream byteArrayOutputStream) {
        byte[] bArr = new byte[i2];
        int i3 = i;
        for (int i4 = i2 - 1; i4 >= 0; i4--) {
            bArr[i4] = (byte) (i3 & 255);
            i3 >>>= 8;
        }
        byteArrayOutputStream.write(bArr);
    }

    static final void a(long j, int i, ByteArrayOutputStream byteArrayOutputStream) {
        byte[] bArr = new byte[i];
        long j2 = j;
        for (int i2 = i - 1; i2 >= 0; i2--) {
            bArr[i2] = (byte) ((int) (255 & j2));
            j2 >>>= 8;
        }
        byteArrayOutputStream.write(bArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    static final void a(String str, ByteArrayOutputStream byteArrayOutputStream) {
        if (str != null) {
            try {
                String trim = str.indexOf(32) > -1 ? str.trim() : str;
                if (trim.indexOf(38) > -1) {
                    trim = trim.replace('&', '_');
                }
                byteArrayOutputStream.write(trim.getBytes("UTF-8"));
            } catch (Exception e) {
            }
        }
    }
}
