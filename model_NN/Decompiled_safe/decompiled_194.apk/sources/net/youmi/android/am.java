package net.youmi.android;

import java.io.File;
import java.util.LinkedList;

class am implements Runnable {
    final /* synthetic */ eb a;

    am(eb ebVar) {
        this.a = ebVar;
    }

    public void run() {
        File[] listFiles;
        try {
            File file = new File(this.a.b);
            if ((file.exists() || file.mkdirs()) && (listFiles = file.listFiles()) != null) {
                LinkedList linkedList = new LinkedList();
                long j = 0;
                for (File file2 : listFiles) {
                    if (this.a.a(file2)) {
                        try {
                            if (file2.delete()) {
                            }
                        } catch (Exception e) {
                        }
                    }
                    if (this.a.c != -1 && this.a.c > 0) {
                        j += file2.length();
                        int i = 0;
                        while (true) {
                            if (i >= linkedList.size()) {
                                break;
                            } else if (file2.lastModified() <= ((File) linkedList.get(i)).lastModified()) {
                                linkedList.add(i, file2);
                                break;
                            } else {
                                i++;
                            }
                        }
                    }
                }
                long j2 = j;
                while (j2 > this.a.c) {
                    if (linkedList.size() > 0) {
                        File file3 = (File) linkedList.get(0);
                        j2 -= file3.length();
                        linkedList.remove(0);
                        try {
                            file3.delete();
                        } catch (Exception e2) {
                        }
                    }
                }
            }
        } catch (Exception e3) {
        }
    }
}
