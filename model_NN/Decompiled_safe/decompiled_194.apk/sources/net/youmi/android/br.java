package net.youmi.android;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import java.io.File;

class br extends AsyncTask implements ec {
    private static Notification c;
    PendingIntent a;
    Context b;
    private String d;
    private String e;
    private long f = 0;
    private long g = 0;
    private dk h;

    br(Context context, dk dkVar) {
        this.b = context;
        this.h = dkVar;
        this.a = PendingIntent.getActivity(this.b, 99998, new Intent(), 268435456);
        c = new Notification();
        c.icon = 17301633;
        c.setLatestEventInfo(this.b.getApplicationContext(), "下载更新", "开始下载", this.a);
    }

    private void a() {
        try {
            ((NotificationManager) this.b.getSystemService("notification")).notify(99998, c);
        } catch (Exception e2) {
            f.a(e2);
        }
    }

    private void a(int i) {
        try {
            c.tickerText = "下载进度:" + i + "%";
            c.flags = 2;
            c.setLatestEventInfo(this.b.getApplicationContext(), "下载更新", "下载进度:" + i + "%", this.a);
            a();
        } catch (Exception e2) {
            f.a(e2);
        }
    }

    private void b(File file) {
        if (file != null) {
            try {
                if (file.exists()) {
                    try {
                        ((NotificationManager) this.b.getSystemService("notification")).cancel(99998);
                    } catch (Exception e2) {
                        f.a(e2);
                    }
                    try {
                        ax.a(this.b, "下载成功,正在安装...");
                    } catch (Exception e3) {
                        f.a(e3);
                    }
                    fa.a(this.b, file, this.h);
                }
            } catch (Exception e4) {
                f.a(e4);
            }
        }
    }

    private void b(String str) {
        try {
            c.tickerText = "下载更新失败";
            c.icon = 17301634;
            c.flags = 16;
            c.setLatestEventInfo(this.b.getApplicationContext(), "下载更新", str, this.a);
            a();
        } catch (Exception e2) {
            f.a(e2);
        }
        ax.b(this.b, "更新失败,请稍候重试!");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01bd, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01be, code lost:
        r1 = null;
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x01cd, code lost:
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0117, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        net.youmi.android.f.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        r1.getConnectionManager().shutdown();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x017a, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x017b, code lost:
        r2 = r1;
        r1 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
        r2.getConnectionManager().shutdown();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x01bd A[ExcHandler: all (th java.lang.Throwable), Splitter:B:23:0x0098] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0121 A[SYNTHETIC, Splitter:B:58:0x0121] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x012a A[SYNTHETIC, Splitter:B:61:0x012a] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x017a A[ExcHandler: all (th java.lang.Throwable), Splitter:B:39:0x00fa] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x017f A[SYNTHETIC, Splitter:B:81:0x017f] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0188 A[SYNTHETIC, Splitter:B:84:0x0188] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.File doInBackground(java.lang.String... r14) {
        /*
            r13 = this;
            r2 = -3
            r12 = 0
            r1 = 1
            r6 = 0
            android.content.Context r0 = r13.b
            if (r0 != 0) goto L_0x0015
            java.lang.Integer[] r0 = new java.lang.Integer[r1]
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)
            r0[r6] = r1
            r13.publishProgress(r0)
            r0 = r12
        L_0x0014:
            return r0
        L_0x0015:
            if (r14 != 0) goto L_0x0024
            java.lang.Integer[] r0 = new java.lang.Integer[r1]
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)
            r0[r6] = r1
            r13.publishProgress(r0)
            r0 = r12
            goto L_0x0014
        L_0x0024:
            net.youmi.android.dk r0 = r13.h
            if (r0 != 0) goto L_0x0035
            java.lang.Integer[] r0 = new java.lang.Integer[r1]
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)
            r0[r6] = r1
            r13.publishProgress(r0)
            r0 = r12
            goto L_0x0014
        L_0x0035:
            android.content.Context r0 = r13.b
            boolean r0 = net.youmi.android.Cdo.b(r0)
            if (r0 != 0) goto L_0x004a
            java.lang.Integer[] r0 = new java.lang.Integer[r1]
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)
            r0[r6] = r1
            r13.publishProgress(r0)
            r0 = r12
            goto L_0x0014
        L_0x004a:
            android.content.Context r0 = r13.b
            boolean r0 = net.youmi.android.ay.a(r0)
            if (r0 != 0) goto L_0x0060
            java.lang.Integer[] r0 = new java.lang.Integer[r1]
            r1 = -1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r0[r6] = r1
            r13.publishProgress(r0)
            r0 = r12
            goto L_0x0014
        L_0x0060:
            android.content.Context r0 = r13.b
            boolean r0 = net.youmi.android.Cdo.c(r0)
            if (r0 != 0) goto L_0x0075
            java.lang.Integer[] r0 = new java.lang.Integer[r1]
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)
            r0[r6] = r1
            r13.publishProgress(r0)
            r0 = r12
            goto L_0x0014
        L_0x0075:
            android.content.Context r0 = r13.b
            boolean r0 = net.youmi.android.r.b(r0)
            if (r0 != 0) goto L_0x008b
            java.lang.Integer[] r0 = new java.lang.Integer[r1]
            r1 = -2
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r0[r6] = r1
            r13.publishProgress(r0)
            r0 = r12
            goto L_0x0014
        L_0x008b:
            int r0 = r14.length
            if (r0 <= 0) goto L_0x012d
            r0 = r14[r6]
            r13.d = r0
            java.lang.String r0 = r13.d
            r13.e = r0
            r0 = 0
            r1 = 1
            java.lang.Integer[] r1 = new java.lang.Integer[r1]     // Catch:{ Exception -> 0x011c, all -> 0x01bd }
            r2 = 0
            r3 = -4
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x011c, all -> 0x01bd }
            r1[r2] = r3     // Catch:{ Exception -> 0x011c, all -> 0x01bd }
            r13.publishProgress(r1)     // Catch:{ Exception -> 0x011c, all -> 0x01bd }
            net.youmi.android.dk r1 = r13.h     // Catch:{ Exception -> 0x011c, all -> 0x01bd }
            java.lang.String r1 = r1.c     // Catch:{ Exception -> 0x011c, all -> 0x01bd }
            java.lang.String r1 = net.youmi.android.cp.b(r1)     // Catch:{ Exception -> 0x011c, all -> 0x01bd }
            net.youmi.android.eb r2 = net.youmi.android.bb.e()     // Catch:{ Exception -> 0x011c, all -> 0x01bd }
            java.lang.String r1 = r2.a(r1)     // Catch:{ Exception -> 0x011c, all -> 0x01bd }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x011c, all -> 0x01bd }
            r2.<init>(r1)     // Catch:{ Exception -> 0x011c, all -> 0x01bd }
            boolean r1 = r2.exists()     // Catch:{ Exception -> 0x0117, all -> 0x01bd }
            if (r1 == 0) goto L_0x00c6
            boolean r1 = r2.delete()     // Catch:{ Exception -> 0x0117, all -> 0x01bd }
            if (r1 == 0) goto L_0x00c6
        L_0x00c6:
            android.content.Context r1 = r13.b     // Catch:{ Exception -> 0x011c, all -> 0x01bd }
            org.apache.http.impl.client.DefaultHttpClient r1 = net.youmi.android.r.a(r1, r13)     // Catch:{ Exception -> 0x011c, all -> 0x01bd }
            org.apache.http.client.methods.HttpGet r3 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x01c8, all -> 0x01c1 }
            java.lang.String r4 = r13.d     // Catch:{ Exception -> 0x01c8, all -> 0x01c1 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x01c8, all -> 0x01c1 }
            org.apache.http.HttpResponse r3 = r1.execute(r3)     // Catch:{ Exception -> 0x01c8, all -> 0x01c1 }
            org.apache.http.StatusLine r4 = r3.getStatusLine()     // Catch:{ Exception -> 0x01c8, all -> 0x01c1 }
            int r4 = r4.getStatusCode()     // Catch:{ Exception -> 0x01c8, all -> 0x01c1 }
            r5 = 200(0xc8, float:2.8E-43)
            if (r4 != r5) goto L_0x019d
            org.apache.http.HttpEntity r3 = r3.getEntity()     // Catch:{ Exception -> 0x01c8, all -> 0x01c1 }
            if (r3 == 0) goto L_0x019d
            long r4 = r3.getContentLength()     // Catch:{ Exception -> 0x01c8, all -> 0x01c1 }
            r13.f = r4     // Catch:{ Exception -> 0x01c8, all -> 0x01c1 }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x01c8, all -> 0x01c1 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x01c8, all -> 0x01c1 }
            java.io.InputStream r3 = r3.getContent()     // Catch:{ Exception -> 0x01c8, all -> 0x01c1 }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x01cc, all -> 0x017a }
            r5 = r6
        L_0x00fd:
            int r6 = r3.read(r4)     // Catch:{ Exception -> 0x01cc, all -> 0x017a }
            if (r6 > 0) goto L_0x0130
            r0.close()     // Catch:{ Exception -> 0x01ae, all -> 0x017a }
        L_0x0106:
            if (r1 == 0) goto L_0x010f
            org.apache.http.conn.ClientConnectionManager r0 = r1.getConnectionManager()     // Catch:{ Exception -> 0x01d0 }
            r0.shutdown()     // Catch:{ Exception -> 0x01d0 }
        L_0x010f:
            if (r3 == 0) goto L_0x0114
            r3.close()     // Catch:{ Exception -> 0x01b1 }
        L_0x0114:
            r0 = r2
            goto L_0x0014
        L_0x0117:
            r1 = move-exception
            net.youmi.android.f.a(r1)     // Catch:{ Exception -> 0x011c, all -> 0x01bd }
            goto L_0x00c6
        L_0x011c:
            r0 = move-exception
            r0 = r12
            r1 = r12
        L_0x011f:
            if (r1 == 0) goto L_0x0128
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()     // Catch:{ Exception -> 0x01c5 }
            r1.shutdown()     // Catch:{ Exception -> 0x01c5 }
        L_0x0128:
            if (r0 == 0) goto L_0x012d
            r0.close()     // Catch:{ Exception -> 0x01b4 }
        L_0x012d:
            r0 = r12
            goto L_0x0014
        L_0x0130:
            r7 = 0
            r0.write(r4, r7, r6)     // Catch:{ Exception -> 0x01cc, all -> 0x017a }
            int r5 = r5 + 1
            long r7 = r13.g     // Catch:{ Exception -> 0x01cc, all -> 0x017a }
            long r9 = (long) r6     // Catch:{ Exception -> 0x01cc, all -> 0x017a }
            long r6 = r7 + r9
            r13.g = r6     // Catch:{ Exception -> 0x01cc, all -> 0x017a }
            int r6 = r5 % 30
            if (r6 != 0) goto L_0x00fd
            long r6 = r13.f     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            r8 = 0
            int r6 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r6 <= 0) goto L_0x018c
            long r6 = r13.g     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            long r8 = r13.f     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            int r6 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r6 >= 0) goto L_0x016a
            r6 = 1
            java.lang.Integer[] r6 = new java.lang.Integer[r6]     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            r7 = 0
            long r8 = r13.g     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            r10 = 100
            long r8 = r8 * r10
            long r10 = r13.f     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            long r8 = r8 / r10
            int r8 = (int) r8     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            r6[r7] = r8     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            r13.publishProgress(r6)     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            goto L_0x00fd
        L_0x0168:
            r6 = move-exception
            goto L_0x00fd
        L_0x016a:
            r6 = 1
            java.lang.Integer[] r6 = new java.lang.Integer[r6]     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            r7 = 0
            r8 = 99
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            r6[r7] = r8     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            r13.publishProgress(r6)     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            goto L_0x00fd
        L_0x017a:
            r0 = move-exception
            r2 = r1
            r1 = r3
        L_0x017d:
            if (r2 == 0) goto L_0x0186
            org.apache.http.conn.ClientConnectionManager r2 = r2.getConnectionManager()     // Catch:{ Exception -> 0x01bb }
            r2.shutdown()     // Catch:{ Exception -> 0x01bb }
        L_0x0186:
            if (r1 == 0) goto L_0x018b
            r1.close()     // Catch:{ Exception -> 0x01b7 }
        L_0x018b:
            throw r0
        L_0x018c:
            r6 = 1
            java.lang.Integer[] r6 = new java.lang.Integer[r6]     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            r7 = 0
            r8 = 80
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            r6[r7] = r8     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            r13.publishProgress(r6)     // Catch:{ Exception -> 0x0168, all -> 0x017a }
            goto L_0x00fd
        L_0x019d:
            if (r1 == 0) goto L_0x01a6
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()     // Catch:{ Exception -> 0x01b9 }
            r1.shutdown()     // Catch:{ Exception -> 0x01b9 }
        L_0x01a6:
            if (r12 == 0) goto L_0x012d
            r0.close()     // Catch:{ Exception -> 0x01ac }
            goto L_0x012d
        L_0x01ac:
            r0 = move-exception
            goto L_0x012d
        L_0x01ae:
            r0 = move-exception
            goto L_0x0106
        L_0x01b1:
            r0 = move-exception
            goto L_0x0114
        L_0x01b4:
            r0 = move-exception
            goto L_0x012d
        L_0x01b7:
            r1 = move-exception
            goto L_0x018b
        L_0x01b9:
            r1 = move-exception
            goto L_0x01a6
        L_0x01bb:
            r2 = move-exception
            goto L_0x0186
        L_0x01bd:
            r0 = move-exception
            r1 = r12
            r2 = r12
            goto L_0x017d
        L_0x01c1:
            r0 = move-exception
            r2 = r1
            r1 = r12
            goto L_0x017d
        L_0x01c5:
            r1 = move-exception
            goto L_0x0128
        L_0x01c8:
            r0 = move-exception
            r0 = r12
            goto L_0x011f
        L_0x01cc:
            r0 = move-exception
            r0 = r3
            goto L_0x011f
        L_0x01d0:
            r0 = move-exception
            goto L_0x010f
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.br.doInBackground(java.lang.String[]):java.io.File");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(File file) {
        super.onPostExecute(file);
        if (file == null || !file.exists()) {
            b("更新失败,请稍候重试");
        } else {
            b(file);
        }
    }

    public void a(String str) {
        this.e = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
        super.onProgressUpdate(numArr);
        if (numArr != null) {
            try {
                if (numArr.length > 0) {
                    int intValue = numArr[0].intValue();
                    if (intValue == -1) {
                        b("存储卡不可用,无法更新下载,请检查存储卡设置!");
                    } else if (intValue == -2) {
                        b("网络错误,无法更新下载");
                    } else if (intValue == -3) {
                        b("下载过程遇到错误,取消本次下载");
                    } else if (intValue == -4) {
                        a(0);
                    } else if (intValue >= 0 && intValue <= 100) {
                        a(intValue);
                    }
                }
            } catch (Exception e2) {
                f.a(e2);
            }
        }
    }
}
