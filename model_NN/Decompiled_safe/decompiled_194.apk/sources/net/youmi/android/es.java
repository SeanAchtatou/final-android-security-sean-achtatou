package net.youmi.android;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;

class es {
    es() {
    }

    static void a(Activity activity, AdView adView) {
        try {
            int taskId = activity.getTaskId();
            if (ef.a(taskId)) {
                an.b(activity);
                return;
            }
            ef.b(taskId);
            try {
                an.a(activity);
            } catch (Exception e) {
            }
            a(activity);
            try {
                fa.a(activity, adView);
            } catch (Exception e2) {
            }
            try {
                k.a(activity.getApplicationContext());
            } catch (Exception e3) {
            }
            try {
                eg.a(activity, adView);
            } catch (Exception e4) {
            }
        } catch (Exception e5) {
        }
    }

    static void a(Context context) {
        try {
            f.a("Current sdk version is youmi android sdk " + bp.f() + "." + bp.h());
            f.a("App ID is set to " + eo.c());
            f.a("App Sec is set to " + eo.d());
            try {
                f.a("App PackageName is set to " + context.getPackageName());
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                if (packageInfo != null) {
                    f.a("App Version code is set to " + packageInfo.versionCode);
                    f.a("App Version name is set to " + packageInfo.versionName);
                }
            } catch (Exception e) {
            }
            try {
                f.a(eo.a() ? String.valueOf("TestMode is set to ") + "TRUE" : ef.e(context) ? String.valueOf("TestMode is set to ") + "FALSE." : String.valueOf("TestMode is set to ") + "FALSE");
            } catch (Exception e2) {
            }
            f.a("Requesting interval is set to " + eo.e() + " seconds");
        } catch (Exception e3) {
        }
    }
}
