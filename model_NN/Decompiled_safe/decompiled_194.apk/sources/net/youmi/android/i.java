package net.youmi.android;

import android.content.Context;
import android.os.AsyncTask;
import java.io.File;
import java.util.ArrayList;
import org.apache.http.Header;
import org.apache.http.HttpResponse;

class i extends AsyncTask implements ec {
    protected Context a;
    protected String b;
    protected String c;
    protected dz d;
    protected long e;
    protected String f;
    protected File g;
    private String h;
    private String i;
    private long j = 0;
    private long k = 0;
    private int l = -1;

    i(Context context, dz dzVar) {
        this.a = context;
        this.d = dzVar;
    }

    i(Context context, dz dzVar, String str) {
        this.a = context;
        this.d = dzVar;
        this.i = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00b8, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        net.youmi.android.f.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00bd, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00be, code lost:
        r8 = r1;
        r1 = r2;
        r2 = r0;
        r0 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00e1, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00e2, code lost:
        net.youmi.android.f.a(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00e6, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00e7, code lost:
        net.youmi.android.f.a(r1);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00bd A[ExcHandler: all (r1v15 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:9:0x0028] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00c4 A[SYNTHETIC, Splitter:B:54:0x00c4] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00c9 A[SYNTHETIC, Splitter:B:57:0x00c9] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int a(org.apache.http.HttpEntity r10) {
        /*
            r9 = this;
            r7 = 0
            r6 = 1
            r5 = 0
            r0 = 1
            java.lang.Integer[] r0 = new java.lang.Integer[r0]     // Catch:{ Exception -> 0x010b, all -> 0x0100 }
            r1 = 0
            r2 = 60
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x010b, all -> 0x0100 }
            r0[r1] = r2     // Catch:{ Exception -> 0x010b, all -> 0x0100 }
            r9.publishProgress(r0)     // Catch:{ Exception -> 0x010b, all -> 0x0100 }
            java.io.InputStream r0 = r10.getContent()     // Catch:{ Exception -> 0x010b, all -> 0x0100 }
            long r1 = r9.e     // Catch:{ Exception -> 0x010f, all -> 0x0104 }
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x009c
            long r1 = r9.e     // Catch:{ Exception -> 0x010f, all -> 0x0104 }
            int r1 = (int) r1     // Catch:{ Exception -> 0x010f, all -> 0x0104 }
        L_0x0021:
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x010f, all -> 0x0104 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x010f, all -> 0x0104 }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        L_0x002a:
            int r3 = r0.read(r1)     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            if (r3 > 0) goto L_0x009f
            byte[] r1 = r2.toByteArray()     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            java.lang.String r4 = "utf-8"
            r3.<init>(r1, r4)     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            r9.h = r3     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            java.lang.String r3 = r9.h     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            if (r3 == 0) goto L_0x0060
            java.lang.String r3 = r9.h     // Catch:{ Exception -> 0x00b8, all -> 0x00bd }
            java.lang.String r3 = net.youmi.android.cu.c(r3)     // Catch:{ Exception -> 0x00b8, all -> 0x00bd }
            if (r3 == 0) goto L_0x0060
            java.lang.String r3 = r3.trim()     // Catch:{ Exception -> 0x00b8, all -> 0x00bd }
            java.lang.String r3 = r3.toLowerCase()     // Catch:{ Exception -> 0x00b8, all -> 0x00bd }
            java.lang.String r4 = "utf-8"
            boolean r4 = r3.equals(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00bd }
            if (r4 != 0) goto L_0x0060
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x00b8, all -> 0x00bd }
            r4.<init>(r1, r3)     // Catch:{ Exception -> 0x00b8, all -> 0x00bd }
            r9.h = r4     // Catch:{ Exception -> 0x00b8, all -> 0x00bd }
        L_0x0060:
            java.lang.String r1 = r9.h     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            if (r1 == 0) goto L_0x00eb
            r1 = 1
            java.lang.Integer[] r1 = new java.lang.Integer[r1]     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            r3 = 0
            r4 = 95
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            r1[r3] = r4     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            r9.publishProgress(r1)     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            java.lang.String r1 = r9.c     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            java.lang.String r3 = r9.h     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            java.lang.String r1 = net.youmi.android.az.a(r1, r3)     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            r9.h = r1     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            r1 = 1
            java.lang.Integer[] r1 = new java.lang.Integer[r1]     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            r3 = 0
            r4 = 100
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            r1[r3] = r4     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            r9.publishProgress(r1)     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            java.lang.String r1 = r9.h     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            if (r1 == 0) goto L_0x00eb
            if (r0 == 0) goto L_0x0095
            r0.close()     // Catch:{ Exception -> 0x00cd }
        L_0x0095:
            if (r2 == 0) goto L_0x009a
            r2.close()     // Catch:{ Exception -> 0x00d2 }
        L_0x009a:
            r0 = r6
        L_0x009b:
            return r0
        L_0x009c:
            r1 = 2048(0x800, float:2.87E-42)
            goto L_0x0021
        L_0x009f:
            r4 = 0
            r2.write(r1, r4, r3)     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            goto L_0x002a
        L_0x00a4:
            r1 = move-exception
            r8 = r1
            r1 = r2
            r2 = r0
            r0 = r8
        L_0x00a9:
            net.youmi.android.f.a(r0)     // Catch:{ all -> 0x0109 }
            if (r2 == 0) goto L_0x00b1
            r2.close()     // Catch:{ Exception -> 0x00d7 }
        L_0x00b1:
            if (r1 == 0) goto L_0x00b6
            r1.close()     // Catch:{ Exception -> 0x00dc }
        L_0x00b6:
            r0 = r5
            goto L_0x009b
        L_0x00b8:
            r1 = move-exception
            net.youmi.android.f.a(r1)     // Catch:{ Exception -> 0x00a4, all -> 0x00bd }
            goto L_0x0060
        L_0x00bd:
            r1 = move-exception
            r8 = r1
            r1 = r2
            r2 = r0
            r0 = r8
        L_0x00c2:
            if (r2 == 0) goto L_0x00c7
            r2.close()     // Catch:{ Exception -> 0x00e1 }
        L_0x00c7:
            if (r1 == 0) goto L_0x00cc
            r1.close()     // Catch:{ Exception -> 0x00e6 }
        L_0x00cc:
            throw r0
        L_0x00cd:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0095
        L_0x00d2:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x009a
        L_0x00d7:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00b1
        L_0x00dc:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00b6
        L_0x00e1:
            r2 = move-exception
            net.youmi.android.f.a(r2)
            goto L_0x00c7
        L_0x00e6:
            r1 = move-exception
            net.youmi.android.f.a(r1)
            goto L_0x00cc
        L_0x00eb:
            if (r0 == 0) goto L_0x00f0
            r0.close()     // Catch:{ Exception -> 0x00fb }
        L_0x00f0:
            if (r2 == 0) goto L_0x00b6
            r2.close()     // Catch:{ Exception -> 0x00f6 }
            goto L_0x00b6
        L_0x00f6:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00b6
        L_0x00fb:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00f0
        L_0x0100:
            r0 = move-exception
            r1 = r7
            r2 = r7
            goto L_0x00c2
        L_0x0104:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r7
            goto L_0x00c2
        L_0x0109:
            r0 = move-exception
            goto L_0x00c2
        L_0x010b:
            r0 = move-exception
            r1 = r7
            r2 = r7
            goto L_0x00a9
        L_0x010f:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r7
            goto L_0x00a9
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.i.a(org.apache.http.HttpEntity):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0131, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:?, code lost:
        net.youmi.android.f.a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0135, code lost:
        r0 = r2;
        r1 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x0149, code lost:
        r0.c.delete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x0153, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x0154, code lost:
        net.youmi.android.f.a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x0158, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x0159, code lost:
        r1 = r2;
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x018d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x018e, code lost:
        net.youmi.android.f.a(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x0192, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x0193, code lost:
        net.youmi.android.f.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x01a7, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x01a8, code lost:
        r9 = r2;
        r2 = r1;
        r1 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x01bb, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x01bc, code lost:
        r9 = r2;
        r2 = r3;
        r3 = r1;
        r1 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00d7, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x00d8, code lost:
        r9 = r2;
        r2 = r3;
        r3 = r1;
        r1 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x010e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x010f, code lost:
        r9 = r2;
        r2 = r1;
        r1 = r9;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:77:0x00e1, B:121:0x013d] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0117 A[SYNTHETIC, Splitter:B:103:0x0117] */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x011c A[SYNTHETIC, Splitter:B:106:0x011c] */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x013d A[SYNTHETIC, Splitter:B:121:0x013d] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x0158 A[ExcHandler: all (th java.lang.Throwable), PHI: r2 r3 
      PHI: (r2v11 java.io.FileOutputStream) = (r2v15 java.io.FileOutputStream), (r2v15 java.io.FileOutputStream), (r2v17 java.io.FileOutputStream), (r2v17 java.io.FileOutputStream), (r2v17 java.io.FileOutputStream) binds: [B:127:0x014e, B:121:0x013d, B:77:0x00e1, B:115:0x0132, B:116:?] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r3v4 java.io.InputStream) = (r3v6 java.io.InputStream), (r3v6 java.io.InputStream), (r3v8 java.io.InputStream), (r3v8 java.io.InputStream), (r3v8 java.io.InputStream) binds: [B:127:0x014e, B:121:0x013d, B:77:0x00e1, B:115:0x0132, B:116:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:77:0x00e1] */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x015d A[SYNTHETIC, Splitter:B:136:0x015d] */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x0162 A[SYNTHETIC, Splitter:B:139:0x0162] */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x016b A[SYNTHETIC, Splitter:B:144:0x016b] */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x01a7 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:39:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x01bb A[ExcHandler: all (r2v25 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:24:0x0053] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x00e1 A[SYNTHETIC, Splitter:B:77:0x00e1] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x00f6 A[SYNTHETIC, Splitter:B:85:0x00f6] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x00fb A[SYNTHETIC, Splitter:B:88:0x00fb] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int a(org.apache.http.HttpResponse r11, org.apache.http.HttpEntity r12) {
        /*
            r10 = this;
            r8 = 1
            r7 = 0
            r2 = 0
            android.content.Context r0 = r10.a
            boolean r0 = net.youmi.android.ay.a(r0)
            if (r0 != 0) goto L_0x000d
            r0 = 4
        L_0x000c:
            return r0
        L_0x000d:
            boolean r0 = r10.a(r11)
            if (r0 != 0) goto L_0x0015
            r0 = r2
            goto L_0x000c
        L_0x0015:
            boolean r0 = r10.f()
            if (r0 == 0) goto L_0x001d
            r0 = 3
            goto L_0x000c
        L_0x001d:
            java.util.ArrayList r1 = net.youmi.android.q.a()     // Catch:{ Exception -> 0x00c8 }
            if (r1 == 0) goto L_0x0029
        L_0x0023:
            int r0 = r1.size()     // Catch:{ Exception -> 0x00c8 }
            if (r2 < r0) goto L_0x0093
        L_0x0029:
            r0 = 1
            java.lang.Integer[] r0 = new java.lang.Integer[r0]     // Catch:{ Exception -> 0x01ae, all -> 0x01a3 }
            r1 = 0
            r2 = -999(0xfffffffffffffc19, float:NaN)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x01ae, all -> 0x01a3 }
            r0[r1] = r2     // Catch:{ Exception -> 0x01ae, all -> 0x01a3 }
            r10.publishProgress(r0)     // Catch:{ Exception -> 0x01ae, all -> 0x01a3 }
            java.lang.String r0 = r10.f     // Catch:{ Exception -> 0x01ae, all -> 0x01a3 }
            long r1 = r10.e     // Catch:{ Exception -> 0x01ae, all -> 0x01a3 }
            net.youmi.android.ch r0 = net.youmi.android.q.a(r0, r1)     // Catch:{ Exception -> 0x01ae, all -> 0x01a3 }
            if (r0 == 0) goto L_0x01e7
            java.io.InputStream r1 = r12.getContent()     // Catch:{ Exception -> 0x01cb, all -> 0x0138 }
            if (r1 == 0) goto L_0x01e4
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x01d0, all -> 0x01b6 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x01d0, all -> 0x01b6 }
            java.io.File r4 = r0.c     // Catch:{ Exception -> 0x01d0, all -> 0x01b6 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x01d0, all -> 0x01b6 }
        L_0x0053:
            int r4 = r1.read(r2)     // Catch:{ Exception -> 0x00d7, all -> 0x01bb }
            if (r4 > 0) goto L_0x00d1
            r3.close()     // Catch:{ Exception -> 0x0101, all -> 0x01bb }
            r2 = r7
        L_0x005d:
            net.youmi.android.m r3 = net.youmi.android.q.b(r0)     // Catch:{ Exception -> 0x01d6, all -> 0x01c2 }
            if (r3 == 0) goto L_0x0166
            java.io.File r4 = r3.c     // Catch:{ Exception -> 0x01d6, all -> 0x01c2 }
            if (r4 == 0) goto L_0x0166
            java.io.File r4 = r3.c     // Catch:{ Exception -> 0x01d6, all -> 0x01c2 }
            boolean r4 = r4.exists()     // Catch:{ Exception -> 0x01d6, all -> 0x01c2 }
            if (r4 == 0) goto L_0x0166
            java.io.File r3 = r3.c     // Catch:{ Exception -> 0x01d6, all -> 0x01c2 }
            r10.g = r3     // Catch:{ Exception -> 0x01d6, all -> 0x01c2 }
            if (r0 == 0) goto L_0x0086
            java.io.File r3 = r0.c     // Catch:{ Exception -> 0x0108, all -> 0x01a7 }
            if (r3 == 0) goto L_0x0086
            java.io.File r3 = r0.c     // Catch:{ Exception -> 0x0108, all -> 0x01a7 }
            boolean r3 = r3.exists()     // Catch:{ Exception -> 0x0108, all -> 0x01a7 }
            if (r3 == 0) goto L_0x0086
            java.io.File r0 = r0.c     // Catch:{ Exception -> 0x0108, all -> 0x01a7 }
            r0.delete()     // Catch:{ Exception -> 0x0108, all -> 0x01a7 }
        L_0x0086:
            if (r1 == 0) goto L_0x008b
            r1.close()     // Catch:{ Exception -> 0x0125 }
        L_0x008b:
            if (r2 == 0) goto L_0x0090
            r2.close()     // Catch:{ Exception -> 0x012b }
        L_0x0090:
            r0 = r8
            goto L_0x000c
        L_0x0093:
            java.lang.Object r0 = r1.get(r2)     // Catch:{ Exception -> 0x00c8 }
            net.youmi.android.ch r0 = (net.youmi.android.ch) r0     // Catch:{ Exception -> 0x00c8 }
            java.lang.String r3 = r0.a     // Catch:{ Exception -> 0x00c8 }
            java.lang.String r4 = r10.f     // Catch:{ Exception -> 0x00c8 }
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x00c8 }
            if (r3 == 0) goto L_0x00be
            java.io.File r3 = r0.c     // Catch:{ Exception -> 0x00c8 }
            if (r3 == 0) goto L_0x00be
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00c8 }
            java.io.File r5 = r0.c     // Catch:{ Exception -> 0x00c8 }
            long r5 = r5.lastModified()     // Catch:{ Exception -> 0x00c8 }
            long r3 = r3 - r5
            r5 = 300000(0x493e0, double:1.482197E-318)
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x00ce
            java.io.File r0 = r0.c     // Catch:{ Exception -> 0x00c3 }
            r0.deleteOnExit()     // Catch:{ Exception -> 0x00c3 }
        L_0x00be:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0023
        L_0x00c3:
            r0 = move-exception
            net.youmi.android.f.a(r0)     // Catch:{ Exception -> 0x00c8 }
            goto L_0x00be
        L_0x00c8:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0029
        L_0x00ce:
            r0 = 2
            goto L_0x000c
        L_0x00d1:
            r5 = 0
            r3.write(r2, r5, r4)     // Catch:{ Exception -> 0x00d7, all -> 0x01bb }
            goto L_0x0053
        L_0x00d7:
            r2 = move-exception
            r9 = r2
            r2 = r3
            r3 = r1
            r1 = r9
        L_0x00dc:
            net.youmi.android.f.a(r1)     // Catch:{ all -> 0x01c8 }
            if (r0 == 0) goto L_0x01e0
            java.io.File r1 = r0.c     // Catch:{ Exception -> 0x0131, all -> 0x0158 }
            if (r1 == 0) goto L_0x01e0
            java.io.File r1 = r0.c     // Catch:{ Exception -> 0x0131, all -> 0x0158 }
            boolean r1 = r1.exists()     // Catch:{ Exception -> 0x0131, all -> 0x0158 }
            if (r1 == 0) goto L_0x01e0
            java.io.File r0 = r0.c     // Catch:{ Exception -> 0x0131, all -> 0x0158 }
            r0.delete()     // Catch:{ Exception -> 0x0131, all -> 0x0158 }
            r0 = r2
            r1 = r3
        L_0x00f4:
            if (r1 == 0) goto L_0x00f9
            r1.close()     // Catch:{ Exception -> 0x0197 }
        L_0x00f9:
            if (r0 == 0) goto L_0x00fe
            r0.close()     // Catch:{ Exception -> 0x019d }
        L_0x00fe:
            r0 = 5
            goto L_0x000c
        L_0x0101:
            r2 = move-exception
            net.youmi.android.f.a(r2)     // Catch:{ Exception -> 0x00d7, all -> 0x01bb }
            r2 = r3
            goto L_0x005d
        L_0x0108:
            r0 = move-exception
            net.youmi.android.f.a(r0)     // Catch:{ Exception -> 0x010e, all -> 0x01a7 }
            goto L_0x0086
        L_0x010e:
            r0 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
        L_0x0112:
            net.youmi.android.f.a(r0)     // Catch:{ all -> 0x01ac }
            if (r2 == 0) goto L_0x011a
            r2.close()     // Catch:{ Exception -> 0x0188 }
        L_0x011a:
            if (r1 == 0) goto L_0x00fe
            r1.close()     // Catch:{ Exception -> 0x0120 }
            goto L_0x00fe
        L_0x0120:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00fe
        L_0x0125:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x008b
        L_0x012b:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0090
        L_0x0131:
            r0 = move-exception
            net.youmi.android.f.a(r0)     // Catch:{ Exception -> 0x014f, all -> 0x0158 }
            r0 = r2
            r1 = r3
            goto L_0x00f4
        L_0x0138:
            r1 = move-exception
            r2 = r7
            r3 = r7
        L_0x013b:
            if (r0 == 0) goto L_0x014e
            java.io.File r4 = r0.c     // Catch:{ Exception -> 0x0153, all -> 0x0158 }
            if (r4 == 0) goto L_0x014e
            java.io.File r4 = r0.c     // Catch:{ Exception -> 0x0153, all -> 0x0158 }
            boolean r4 = r4.exists()     // Catch:{ Exception -> 0x0153, all -> 0x0158 }
            if (r4 == 0) goto L_0x014e
            java.io.File r0 = r0.c     // Catch:{ Exception -> 0x0153, all -> 0x0158 }
            r0.delete()     // Catch:{ Exception -> 0x0153, all -> 0x0158 }
        L_0x014e:
            throw r1     // Catch:{ Exception -> 0x014f, all -> 0x0158 }
        L_0x014f:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0112
        L_0x0153:
            r0 = move-exception
            net.youmi.android.f.a(r0)     // Catch:{ Exception -> 0x014f, all -> 0x0158 }
            goto L_0x014e
        L_0x0158:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x015b:
            if (r2 == 0) goto L_0x0160
            r2.close()     // Catch:{ Exception -> 0x018d }
        L_0x0160:
            if (r1 == 0) goto L_0x0165
            r1.close()     // Catch:{ Exception -> 0x0192 }
        L_0x0165:
            throw r0
        L_0x0166:
            r9 = r2
            r2 = r1
            r1 = r9
        L_0x0169:
            if (r0 == 0) goto L_0x01dc
            java.io.File r3 = r0.c     // Catch:{ Exception -> 0x0180 }
            if (r3 == 0) goto L_0x01dc
            java.io.File r3 = r0.c     // Catch:{ Exception -> 0x0180 }
            boolean r3 = r3.exists()     // Catch:{ Exception -> 0x0180 }
            if (r3 == 0) goto L_0x01dc
            java.io.File r0 = r0.c     // Catch:{ Exception -> 0x0180 }
            r0.delete()     // Catch:{ Exception -> 0x0180 }
            r0 = r1
            r1 = r2
            goto L_0x00f4
        L_0x0180:
            r0 = move-exception
            net.youmi.android.f.a(r0)     // Catch:{ Exception -> 0x01b3 }
            r0 = r1
            r1 = r2
            goto L_0x00f4
        L_0x0188:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x011a
        L_0x018d:
            r2 = move-exception
            net.youmi.android.f.a(r2)
            goto L_0x0160
        L_0x0192:
            r1 = move-exception
            net.youmi.android.f.a(r1)
            goto L_0x0165
        L_0x0197:
            r1 = move-exception
            net.youmi.android.f.a(r1)
            goto L_0x00f9
        L_0x019d:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00fe
        L_0x01a3:
            r0 = move-exception
            r1 = r7
            r2 = r7
            goto L_0x015b
        L_0x01a7:
            r0 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x015b
        L_0x01ac:
            r0 = move-exception
            goto L_0x015b
        L_0x01ae:
            r0 = move-exception
            r1 = r7
            r2 = r7
            goto L_0x0112
        L_0x01b3:
            r0 = move-exception
            goto L_0x0112
        L_0x01b6:
            r2 = move-exception
            r3 = r1
            r1 = r2
            r2 = r7
            goto L_0x013b
        L_0x01bb:
            r2 = move-exception
            r9 = r2
            r2 = r3
            r3 = r1
            r1 = r9
            goto L_0x013b
        L_0x01c2:
            r3 = move-exception
            r9 = r3
            r3 = r1
            r1 = r9
            goto L_0x013b
        L_0x01c8:
            r1 = move-exception
            goto L_0x013b
        L_0x01cb:
            r1 = move-exception
            r2 = r7
            r3 = r7
            goto L_0x00dc
        L_0x01d0:
            r2 = move-exception
            r3 = r1
            r1 = r2
            r2 = r7
            goto L_0x00dc
        L_0x01d6:
            r3 = move-exception
            r9 = r3
            r3 = r1
            r1 = r9
            goto L_0x00dc
        L_0x01dc:
            r0 = r1
            r1 = r2
            goto L_0x00f4
        L_0x01e0:
            r0 = r2
            r1 = r3
            goto L_0x00f4
        L_0x01e4:
            r2 = r1
            r1 = r7
            goto L_0x0169
        L_0x01e7:
            r1 = r7
            r2 = r7
            goto L_0x0169
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.i.a(org.apache.http.HttpResponse, org.apache.http.HttpEntity):int");
    }

    private boolean a(HttpResponse httpResponse) {
        try {
            Header[] headers = httpResponse.getHeaders("Content-Disposition");
            if (headers != null && headers.length > 0) {
                for (Header header : headers) {
                    if (header != null) {
                        this.f = cu.a(header.getValue());
                        if (this.f != null) {
                            this.f = this.f.trim();
                            if (this.f.length() > 0) {
                                break;
                            }
                            this.f = null;
                        } else {
                            continue;
                        }
                    }
                }
            }
            if (this.f == null) {
                this.f = cu.b(this.c);
            }
            if (this.f != null) {
                return true;
            }
        } catch (Exception e2) {
            f.a(e2);
        }
        return false;
    }

    private boolean f() {
        try {
            ArrayList b2 = q.b();
            if (b2 != null) {
                for (int i2 = 0; i2 < b2.size(); i2++) {
                    try {
                        m mVar = (m) b2.get(i2);
                        if (!mVar.d.equals(this.f)) {
                            continue;
                        } else if (!q.a(mVar)) {
                            try {
                                mVar.c.delete();
                            } catch (Exception e2) {
                                f.a(e2);
                            }
                        } else if (ed.a(this.a, mVar.c.getPath()) != null) {
                            this.g = mVar.c;
                            return true;
                        } else {
                            try {
                                mVar.c.delete();
                            } catch (Exception e3) {
                                f.a(e3);
                            }
                        }
                    } catch (Exception e4) {
                        f.a(e4);
                    }
                }
            }
        } catch (Exception e5) {
            f.a(e5);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x01f6 A[SYNTHETIC, Splitter:B:130:0x01f6] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Integer doInBackground(java.lang.String... r10) {
        /*
            r9 = this;
            r7 = -1
            r6 = 0
            long r0 = java.lang.System.currentTimeMillis()
            r9.j = r0
            net.youmi.android.dz r0 = r9.d
            if (r0 != 0) goto L_0x0011
            java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
        L_0x0010:
            return r0
        L_0x0011:
            android.content.Context r0 = r9.a
            if (r0 != 0) goto L_0x001a
            java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
            goto L_0x0010
        L_0x001a:
            if (r10 != 0) goto L_0x0021
            java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
            goto L_0x0010
        L_0x0021:
            int r0 = r10.length
            if (r0 <= 0) goto L_0x01e5
            r0 = r10[r6]
            r9.b = r0
            java.lang.String r0 = r9.b
            r9.c = r0
            android.content.Context r0 = r9.a
            boolean r0 = net.youmi.android.Cdo.c(r0)
            if (r0 != 0) goto L_0x0039
            java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
            goto L_0x0010
        L_0x0039:
            android.content.Context r0 = r9.a
            boolean r0 = net.youmi.android.r.b(r0)
            if (r0 != 0) goto L_0x0046
            java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
            goto L_0x0010
        L_0x0046:
            r0 = 0
            android.content.Context r1 = r9.a     // Catch:{ Exception -> 0x01d5, all -> 0x01f0 }
            org.apache.http.impl.client.DefaultHttpClient r0 = net.youmi.android.r.a(r1, r9)     // Catch:{ Exception -> 0x01d5, all -> 0x01f0 }
            org.apache.http.client.methods.HttpGet r1 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            java.lang.String r2 = r9.b     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r2 = 1
            java.lang.Integer[] r2 = new java.lang.Integer[r2]     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r3 = 0
            r4 = 35
            int r4 = net.youmi.android.bm.a(r4)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r2[r3] = r4     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r9.publishProgress(r2)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            org.apache.http.HttpResponse r2 = r0.execute(r1)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r3 = 1
            java.lang.Integer[] r3 = new java.lang.Integer[r3]     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r4 = 0
            r5 = 35
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r3[r4] = r5     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r9.publishProgress(r3)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            org.apache.http.StatusLine r3 = r2.getStatusLine()     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            int r3 = r3.getStatusCode()     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r3 != r4) goto L_0x0203
            r3 = 1
            java.lang.Integer[] r3 = new java.lang.Integer[r3]     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r4 = 0
            r5 = 38
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r3[r4] = r5     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r9.publishProgress(r3)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            org.apache.http.HttpEntity r3 = r2.getEntity()     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r3 == 0) goto L_0x0203
            long r4 = r3.getContentLength()     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r9.e = r4     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            org.apache.http.Header r4 = r3.getContentType()     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r4 != 0) goto L_0x00bd
            r1 = 0
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r0 == 0) goto L_0x00b5
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x00b8 }
            r0.shutdown()     // Catch:{ Exception -> 0x00b8 }
        L_0x00b5:
            r0 = r1
            goto L_0x0010
        L_0x00b8:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00b5
        L_0x00bd:
            java.lang.String r4 = r4.getValue()     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r4 != 0) goto L_0x00d9
            r1 = 0
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r0 == 0) goto L_0x00d1
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x00d4 }
            r0.shutdown()     // Catch:{ Exception -> 0x00d4 }
        L_0x00d1:
            r0 = r1
            goto L_0x0010
        L_0x00d4:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00d1
        L_0x00d9:
            java.lang.String r4 = r4.trim()     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            java.lang.String r4 = r4.toLowerCase()     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            java.lang.String r5 = "application/vnd.android.package-archive"
            int r5 = r4.indexOf(r5)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r5 <= r7) goto L_0x0105
            r1 = 1
            r9.l = r1     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            int r1 = r9.a(r2, r3)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r0 == 0) goto L_0x00fd
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x0100 }
            r0.shutdown()     // Catch:{ Exception -> 0x0100 }
        L_0x00fd:
            r0 = r1
            goto L_0x0010
        L_0x0100:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00fd
        L_0x0105:
            java.lang.String r5 = "application/octet-stream"
            int r5 = r4.indexOf(r5)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r5 <= r7) goto L_0x0129
            r1 = 1
            r9.l = r1     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            int r1 = r9.a(r2, r3)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r0 == 0) goto L_0x0121
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x0124 }
            r0.shutdown()     // Catch:{ Exception -> 0x0124 }
        L_0x0121:
            r0 = r1
            goto L_0x0010
        L_0x0124:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0121
        L_0x0129:
            java.lang.String r2 = "text/html"
            int r2 = r4.indexOf(r2)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r2 <= r7) goto L_0x014d
            r1 = 0
            r9.l = r1     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            int r1 = r9.a(r3)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r0 == 0) goto L_0x0145
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x0148 }
            r0.shutdown()     // Catch:{ Exception -> 0x0148 }
        L_0x0145:
            r0 = r1
            goto L_0x0010
        L_0x0148:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0145
        L_0x014d:
            java.lang.String r2 = "xhtml"
            int r2 = r4.indexOf(r2)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r2 <= r7) goto L_0x0171
            r1 = 0
            r9.l = r1     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            int r1 = r9.a(r3)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r0 == 0) goto L_0x0169
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x016c }
            r0.shutdown()     // Catch:{ Exception -> 0x016c }
        L_0x0169:
            r0 = r1
            goto L_0x0010
        L_0x016c:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0169
        L_0x0171:
            java.lang.String r2 = "xml"
            int r2 = r4.indexOf(r2)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r2 <= r7) goto L_0x0195
            r1 = 0
            r9.l = r1     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            int r1 = r9.a(r3)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r0 == 0) goto L_0x018d
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x0190 }
            r0.shutdown()     // Catch:{ Exception -> 0x0190 }
        L_0x018d:
            r0 = r1
            goto L_0x0010
        L_0x0190:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x018d
        L_0x0195:
            java.lang.String r2 = "wml"
            int r2 = r4.indexOf(r2)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r2 <= r7) goto L_0x01b9
            r1 = 0
            r9.l = r1     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            int r1 = r9.a(r3)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r0 == 0) goto L_0x01b1
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x01b4 }
            r0.shutdown()     // Catch:{ Exception -> 0x01b4 }
        L_0x01b1:
            r0 = r1
            goto L_0x0010
        L_0x01b4:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x01b1
        L_0x01b9:
            r2 = -1
            r9.l = r2     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r1.abort()     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            r1 = 0
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0219, all -> 0x0212 }
            if (r0 == 0) goto L_0x01cd
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x01d0 }
            r0.shutdown()     // Catch:{ Exception -> 0x01d0 }
        L_0x01cd:
            r0 = r1
            goto L_0x0010
        L_0x01d0:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x01cd
        L_0x01d5:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x01d9:
            net.youmi.android.f.a(r0)     // Catch:{ all -> 0x0217 }
            if (r1 == 0) goto L_0x01e5
            org.apache.http.conn.ClientConnectionManager r0 = r1.getConnectionManager()     // Catch:{ Exception -> 0x01eb }
            r0.shutdown()     // Catch:{ Exception -> 0x01eb }
        L_0x01e5:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
            goto L_0x0010
        L_0x01eb:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x01e5
        L_0x01f0:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x01f4:
            if (r1 == 0) goto L_0x01fd
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()     // Catch:{ Exception -> 0x01fe }
            r1.shutdown()     // Catch:{ Exception -> 0x01fe }
        L_0x01fd:
            throw r0
        L_0x01fe:
            r1 = move-exception
            net.youmi.android.f.a(r1)
            goto L_0x01fd
        L_0x0203:
            if (r0 == 0) goto L_0x01e5
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x020d }
            r0.shutdown()     // Catch:{ Exception -> 0x020d }
            goto L_0x01e5
        L_0x020d:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x01e5
        L_0x0212:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x01f4
        L_0x0217:
            r0 = move-exception
            goto L_0x01f4
        L_0x0219:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x01d9
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.i.doInBackground(java.lang.String[]):java.lang.Integer");
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Integer num) {
        super.onPostExecute(num);
        this.k = System.currentTimeMillis();
        try {
            if (this.d != null) {
                switch (this.l) {
                    case 0:
                        if (this.h != null) {
                            try {
                                this.d.a(this, new ek(this.c, this.h));
                                return;
                            } catch (Exception e2) {
                                f.a(e2);
                                return;
                            }
                        } else {
                            try {
                                this.d.e();
                                return;
                            } catch (Exception e3) {
                                f.a(e3);
                                return;
                            }
                        }
                    case 1:
                        switch (num.intValue()) {
                            case 1:
                                if (this.g != null && this.g.exists()) {
                                    try {
                                        this.d.a(this, this.g, this.f);
                                        return;
                                    } catch (Exception e4) {
                                        f.a(e4);
                                        return;
                                    }
                                }
                                try {
                                    this.d.e();
                                    return;
                                } catch (Exception e5) {
                                    f.a(e5);
                                    return;
                                }
                            case 2:
                                try {
                                    this.d.f();
                                    return;
                                } catch (Exception e6) {
                                    f.a(e6);
                                    return;
                                }
                            case 3:
                                if (this.g != null && this.g.exists()) {
                                    try {
                                        this.d.b(this, this.g, this.f);
                                        return;
                                    } catch (Exception e7) {
                                        f.a(e7);
                                        return;
                                    }
                                }
                                this.d.e();
                                return;
                            case 4:
                                try {
                                    this.d.g();
                                    return;
                                } catch (Exception e8) {
                                    f.a(e8);
                                    return;
                                }
                            case 5:
                                try {
                                    this.d.h();
                                    return;
                                } catch (Exception e9) {
                                    f.a(e9);
                                    return;
                                }
                            default:
                                try {
                                    this.d.e();
                                    return;
                                } catch (Exception e10) {
                                    f.a(e10);
                                    return;
                                }
                        }
                    default:
                        try {
                            this.d.e();
                            return;
                        } catch (Exception e11) {
                            f.a(e11);
                            return;
                        }
                }
                f.a(e);
            }
        } catch (Exception e12) {
            f.a(e12);
        }
    }

    public void a(String str) {
        this.c = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
        super.onProgressUpdate(numArr);
        try {
            if (this.d != null && numArr.length > 0) {
                int intValue = numArr[0].intValue();
                if (intValue >= 0) {
                    this.d.a(intValue);
                } else if (this.l == 1) {
                    this.d.a(this);
                }
            }
        } catch (Exception e2) {
            f.a(e2);
        }
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public long d() {
        return this.k - this.j;
    }

    /* access modifiers changed from: package-private */
    public long e() {
        return this.j;
    }
}
