package net.youmi.android;

import android.content.Context;
import com.energysource.szj.embeded.AdManager;

class eo {
    private static boolean a = false;
    private static String b = "";
    private static String c = "";
    private static String d = "";
    private static int e = -1;
    private static long f = 30000;
    private static int g = 30;
    private static int h = 0;
    private static boolean i = true;
    private static boolean j = true;

    eo() {
    }

    static int a(Context context) {
        if (e < 0) {
            try {
                e = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode * 100;
            } catch (Exception e2) {
            }
        }
        return e;
    }

    static void a(int i2) {
        int abs = Math.abs(i2);
        g = abs;
        f = (long) (abs * AdManager.AD_FILL_PARENT);
    }

    static void a(String str) {
        if (str != null) {
            String trim = str.trim();
            if (trim.length() > 0) {
                c = trim;
                b = String.valueOf(trim.substring(0, 1)) + cp.c(trim.substring(1));
            }
        }
    }

    static void a(boolean z) {
        a = z;
    }

    static boolean a() {
        return a;
    }

    static String b() {
        return b;
    }

    static void b(String str) {
        if (str != null) {
            d = str.trim();
        }
    }

    static String c() {
        return c;
    }

    static String d() {
        return d;
    }

    static int e() {
        return g;
    }

    static long f() {
        return f;
    }

    static int g() {
        return h;
    }

    static boolean h() {
        return i;
    }

    static void i() {
        i = false;
    }

    static void j() {
        j = false;
    }

    static boolean k() {
        return j;
    }
}
