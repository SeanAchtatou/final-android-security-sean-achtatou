package net.youmi.android;

import cn.domob.android.ads.DomobAdManager;
import java.net.URI;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class az {
    static final Pattern[] a = {Pattern.compile("background:url\\((.+?)\\)", 2), Pattern.compile("<img.*?src.*?=['\"](.+?)['\"]", 2), Pattern.compile("<input.*?type=['\"]image['\"].*?src.*?=['\"](.+?)['\"]")};
    static final String[] b = {"image/gif", "image/png", "image/jpeg"};
    static final String[] c = {DomobAdManager.ACTION_URL, "filepath"};

    az() {
    }

    static String a(String str, String str2) {
        String str3;
        URI resolve;
        try {
            if (!ay.a()) {
                return str2;
            }
            ArrayList a2 = a(str2);
            if (a2 == null || a2.size() <= 0) {
                return str2;
            }
            URI uri = new URI(str);
            int i = 0;
            String str4 = str2;
            while (i < a2.size()) {
                try {
                    try {
                        String str5 = (String) a2.get(i);
                        if (!(str5 == null || (resolve = uri.resolve(str5)) == null)) {
                            String uri2 = resolve.toString();
                            if (bb.f().e(uri2)) {
                                str4 = str4.replace(str5, "file://" + bb.f().c(uri2));
                            }
                        }
                    } catch (Exception e) {
                        f.a(e);
                    }
                    i++;
                } catch (Exception e2) {
                    e = e2;
                    str3 = str4;
                }
            }
            return str4;
        } catch (Exception e3) {
            e = e3;
            str3 = str2;
            f.a(e);
            return str3;
        }
    }

    static String a(String str, String str2, String str3, String str4) {
        if (str2 == null) {
            return str;
        }
        try {
            StringBuilder sb = new StringBuilder(str.length() + 256);
            sb.append(str);
            sb.append("<script type=\"text/javascript\" src=\"");
            sb.append(str2);
            sb.append("\"></script>");
            sb.append("<script type=\"text/javascript\">");
            sb.append("_youmi_click_.init(\"");
            sb.append(str3);
            sb.append("\",3,\"");
            sb.append(eo.b());
            sb.append("\",\"");
            sb.append(str4);
            sb.append("\")");
            sb.append("</script>");
            return sb.toString();
        } catch (Exception e) {
            f.a(e);
            return str;
        }
    }

    static ArrayList a(String str) {
        int i = 0;
        ArrayList arrayList = null;
        for (int i2 = 0; i2 < a.length; i2++) {
            try {
                Matcher matcher = a[i2].matcher(str);
                if (matcher != null) {
                    while (matcher.find()) {
                        if (arrayList == null) {
                            arrayList = new ArrayList(50);
                        }
                        try {
                            arrayList.add(matcher.group(matcher.groupCount()));
                        } catch (Exception e) {
                            f.a(e);
                        }
                        i++;
                        if (i > 300) {
                            break;
                        }
                    }
                }
            } catch (Exception e2) {
                Exception exc = e2;
                f.a(exc);
                arrayList = arrayList;
                i = i;
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:125:0x0196, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:?, code lost:
        net.youmi.android.f.a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x019c, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x019d, code lost:
        r3 = r9;
        r11 = r0;
        r0 = r2;
        r2 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x01a3, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x01a4, code lost:
        net.youmi.android.f.a(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x01b4, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x01b5, code lost:
        net.youmi.android.f.a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x01ba, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x01bb, code lost:
        net.youmi.android.f.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x01c0, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x01c1, code lost:
        net.youmi.android.f.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00d4, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00d5, code lost:
        r1 = null;
        r2 = null;
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x00de, code lost:
        if (r3.exists() != false) goto L_0x00e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00e0, code lost:
        r3.delete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x00f4, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        net.youmi.android.f.a(r2);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x019c A[Catch:{ Exception -> 0x00f9, all -> 0x019c }, ExcHandler: all (r2v20 'th' java.lang.Throwable A[CUSTOM_DECLARE, Catch:{ Exception -> 0x00f9, all -> 0x019c }]), Splitter:B:21:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00d4 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:11:0x0042] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00da A[SYNTHETIC, Splitter:B:65:0x00da] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00e5 A[SYNTHETIC, Splitter:B:70:0x00e5] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00ea A[SYNTHETIC, Splitter:B:73:0x00ea] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void a(android.app.Activity r12, android.webkit.WebView r13) {
        /*
            r10 = 0
            boolean r0 = net.youmi.android.ay.a(r12)
            if (r0 != 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            java.lang.String r0 = "webviewCache.db"
            java.io.File r0 = r12.getDatabasePath(r0)     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            java.lang.String r2 = ".db"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            boolean r2 = r0.canRead()     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            if (r2 == 0) goto L_0x0045
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            r0 = 0
            java.io.FileOutputStream r0 = r12.openFileOutput(r1, r0)     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            r3 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
        L_0x0039:
            int r4 = r2.read(r3)     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            if (r4 > 0) goto L_0x00a5
            r2.close()     // Catch:{ Exception -> 0x00ce, all -> 0x00d4 }
        L_0x0042:
            r0.close()     // Catch:{ Exception -> 0x00ee, all -> 0x00d4 }
        L_0x0045:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            java.io.File r2 = r12.getCacheDir()     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            java.lang.String r2 = r2.getPath()     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            java.lang.String r2 = "/webviewCache/"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            java.lang.String r8 = r0.toString()     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            java.io.File r9 = r12.getFileStreamPath(r1)     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            java.lang.String r0 = r9.getPath()     // Catch:{ Exception -> 0x01e2, all -> 0x01d2 }
            r1 = 0
            r2 = 1
            android.database.sqlite.SQLiteDatabase r0 = android.database.sqlite.SQLiteDatabase.openDatabase(r0, r1, r2)     // Catch:{ Exception -> 0x01e2, all -> 0x01d2 }
            java.lang.String r1 = "cache"
            java.lang.String[] r2 = net.youmi.android.az.c     // Catch:{ Exception -> 0x01e8, all -> 0x01d8 }
            java.lang.String r3 = "mimetype in (?,?,?)"
            java.lang.String[] r4 = net.youmi.android.az.b     // Catch:{ Exception -> 0x01e8, all -> 0x01d8 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x01e8, all -> 0x01d8 }
            if (r1 == 0) goto L_0x0127
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x00f9, all -> 0x019c }
            if (r2 > 0) goto L_0x0109
            r1.close()     // Catch:{ Exception -> 0x00f4, all -> 0x019c }
        L_0x0088:
            if (r9 == 0) goto L_0x0093
            boolean r2 = r9.exists()     // Catch:{ Exception -> 0x00ff }
            if (r2 == 0) goto L_0x0093
            r9.delete()     // Catch:{ Exception -> 0x00ff }
        L_0x0093:
            if (r1 == 0) goto L_0x0098
            r1.close()     // Catch:{ Exception -> 0x0104 }
        L_0x0098:
            if (r0 == 0) goto L_0x0007
            r0.close()     // Catch:{ Exception -> 0x009f }
            goto L_0x0007
        L_0x009f:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0007
        L_0x00a5:
            r5 = 0
            r0.write(r3, r5, r4)     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            goto L_0x0039
        L_0x00aa:
            r0 = move-exception
            r1 = r10
            r2 = r10
            r3 = r10
        L_0x00ae:
            net.youmi.android.f.a(r0)     // Catch:{ all -> 0x01df }
            if (r3 == 0) goto L_0x00bc
            boolean r0 = r3.exists()     // Catch:{ Exception -> 0x01a8 }
            if (r0 == 0) goto L_0x00bc
            r3.delete()     // Catch:{ Exception -> 0x01a8 }
        L_0x00bc:
            if (r1 == 0) goto L_0x00c1
            r1.close()     // Catch:{ Exception -> 0x01ae }
        L_0x00c1:
            if (r2 == 0) goto L_0x0007
            r2.close()     // Catch:{ Exception -> 0x00c8 }
            goto L_0x0007
        L_0x00c8:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0007
        L_0x00ce:
            r2 = move-exception
            net.youmi.android.f.a(r2)     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            goto L_0x0042
        L_0x00d4:
            r0 = move-exception
            r1 = r10
            r2 = r10
            r3 = r10
        L_0x00d8:
            if (r3 == 0) goto L_0x00e3
            boolean r4 = r3.exists()     // Catch:{ Exception -> 0x01b4 }
            if (r4 == 0) goto L_0x00e3
            r3.delete()     // Catch:{ Exception -> 0x01b4 }
        L_0x00e3:
            if (r1 == 0) goto L_0x00e8
            r1.close()     // Catch:{ Exception -> 0x01ba }
        L_0x00e8:
            if (r2 == 0) goto L_0x00ed
            r2.close()     // Catch:{ Exception -> 0x01c0 }
        L_0x00ed:
            throw r0
        L_0x00ee:
            r0 = move-exception
            net.youmi.android.f.a(r0)     // Catch:{ Exception -> 0x00aa, all -> 0x00d4 }
            goto L_0x0045
        L_0x00f4:
            r2 = move-exception
            net.youmi.android.f.a(r2)     // Catch:{ Exception -> 0x00f9, all -> 0x019c }
            goto L_0x0088
        L_0x00f9:
            r2 = move-exception
            r3 = r9
            r11 = r0
            r0 = r2
            r2 = r11
            goto L_0x00ae
        L_0x00ff:
            r2 = move-exception
            net.youmi.android.f.a(r2)
            goto L_0x0093
        L_0x0104:
            r1 = move-exception
            net.youmi.android.f.a(r1)
            goto L_0x0098
        L_0x0109:
            java.lang.String r2 = "url"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x00f9, all -> 0x019c }
            java.lang.String r3 = "filepath"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x00f9, all -> 0x019c }
        L_0x0115:
            boolean r4 = r1.moveToNext()     // Catch:{ Exception -> 0x00f9, all -> 0x019c }
            if (r4 != 0) goto L_0x0144
            if (r13 == 0) goto L_0x0127
            boolean r2 = net.youmi.android.eo.k()     // Catch:{ Exception -> 0x01a3, all -> 0x019c }
            if (r2 == 0) goto L_0x0127
            r2 = 1
            r13.clearCache(r2)     // Catch:{ Exception -> 0x01a3, all -> 0x019c }
        L_0x0127:
            if (r9 == 0) goto L_0x0132
            boolean r2 = r9.exists()     // Catch:{ Exception -> 0x01c6 }
            if (r2 == 0) goto L_0x0132
            r9.delete()     // Catch:{ Exception -> 0x01c6 }
        L_0x0132:
            if (r1 == 0) goto L_0x0137
            r1.close()     // Catch:{ Exception -> 0x01cc }
        L_0x0137:
            if (r0 == 0) goto L_0x0007
            r0.close()     // Catch:{ Exception -> 0x013e }
            goto L_0x0007
        L_0x013e:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0007
        L_0x0144:
            java.lang.String r4 = r1.getString(r2)     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            if (r4 == 0) goto L_0x0115
            java.lang.String r4 = r4.trim()     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            java.lang.String r5 = "http://"
            int r5 = r4.indexOf(r5)     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            if (r5 == 0) goto L_0x015e
            java.lang.String r5 = "https://"
            int r5 = r4.indexOf(r5)     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            if (r5 != 0) goto L_0x0115
        L_0x015e:
            net.youmi.android.eb r5 = net.youmi.android.bb.f()     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            java.io.File r4 = r5.d(r4)     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            boolean r5 = r4.exists()     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            if (r5 != 0) goto L_0x0115
            java.lang.String r5 = r1.getString(r3)     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            if (r5 == 0) goto L_0x0115
            java.lang.String r5 = r5.trim()     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            java.lang.String r7 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            r6.<init>(r7)     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            r6.<init>(r5)     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            boolean r5 = r6.exists()     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            if (r5 == 0) goto L_0x0115
            net.youmi.android.eh.a(r6, r4)     // Catch:{ Exception -> 0x0196, all -> 0x019c }
            goto L_0x0115
        L_0x0196:
            r4 = move-exception
            net.youmi.android.f.a(r4)     // Catch:{ Exception -> 0x00f9, all -> 0x019c }
            goto L_0x0115
        L_0x019c:
            r2 = move-exception
            r3 = r9
            r11 = r0
            r0 = r2
            r2 = r11
            goto L_0x00d8
        L_0x01a3:
            r2 = move-exception
            net.youmi.android.f.a(r2)     // Catch:{ Exception -> 0x00f9, all -> 0x019c }
            goto L_0x0127
        L_0x01a8:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00bc
        L_0x01ae:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00c1
        L_0x01b4:
            r3 = move-exception
            net.youmi.android.f.a(r3)
            goto L_0x00e3
        L_0x01ba:
            r1 = move-exception
            net.youmi.android.f.a(r1)
            goto L_0x00e8
        L_0x01c0:
            r1 = move-exception
            net.youmi.android.f.a(r1)
            goto L_0x00ed
        L_0x01c6:
            r2 = move-exception
            net.youmi.android.f.a(r2)
            goto L_0x0132
        L_0x01cc:
            r1 = move-exception
            net.youmi.android.f.a(r1)
            goto L_0x0137
        L_0x01d2:
            r0 = move-exception
            r1 = r10
            r2 = r10
            r3 = r9
            goto L_0x00d8
        L_0x01d8:
            r1 = move-exception
            r2 = r0
            r3 = r9
            r0 = r1
            r1 = r10
            goto L_0x00d8
        L_0x01df:
            r0 = move-exception
            goto L_0x00d8
        L_0x01e2:
            r0 = move-exception
            r1 = r10
            r2 = r10
            r3 = r9
            goto L_0x00ae
        L_0x01e8:
            r1 = move-exception
            r2 = r0
            r3 = r9
            r0 = r1
            r1 = r10
            goto L_0x00ae
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.az.a(android.app.Activity, android.webkit.WebView):void");
    }
}
