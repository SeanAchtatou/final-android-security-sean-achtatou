package net.youmi.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import java.io.File;
import java.util.ArrayList;

class cr extends BaseAdapter {
    Context a;
    private ArrayList b;
    private int c;

    private cr(Context context) {
        this.a = context;
    }

    static cr a(Context context, String[] strArr, int i) {
        if (strArr == null) {
            return null;
        }
        try {
            if (strArr.length < 1) {
                return null;
            }
            cr crVar = new cr(context);
            int i2 = i <= 0 ? 100 : i;
            crVar.c = i2;
            for (String str : strArr) {
                if (new File(str).exists()) {
                    int i3 = 2;
                    try {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = true;
                        BitmapFactory.decodeFile(str, options);
                        int i4 = options.outWidth;
                        int i5 = options.outHeight;
                        if (i4 > 0 && i5 > 0) {
                            i3 = i4 >= i5 ? i4 / i2 : i5 / i2;
                        }
                        options.inJustDecodeBounds = false;
                        options.inSampleSize = i3;
                        Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
                        if (decodeFile != null) {
                            ar arVar = new ar();
                            arVar.a = str;
                            arVar.b = Uri.parse("file://" + str);
                            arVar.d = options.outMimeType;
                            arVar.c = decodeFile;
                            crVar.a(arVar);
                        }
                    } catch (Exception e) {
                    }
                }
            }
            if (crVar.getCount() > 0) {
                return crVar;
            }
            return null;
        } catch (Exception e2) {
        }
    }

    private void a(ar arVar) {
        if (this.b == null) {
            this.b = new ArrayList(10);
        }
        this.b.add(arVar);
    }

    /* access modifiers changed from: package-private */
    public ar a(int i) {
        try {
            if (this.b != null && i > -1 && i < this.b.size()) {
                return (ar) this.b.get(i);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public int getCount() {
        if (this.b != null) {
            return this.b.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        return a(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        try {
            ar a2 = a(i);
            if (a2 == null) {
                return null;
            }
            Bitmap bitmap = a2.c;
            if (view != null) {
                ImageView imageView = (ImageView) view;
                if (bitmap == null) {
                    return view;
                }
                imageView.setImageBitmap(bitmap);
                return view;
            }
            ImageView imageView2 = new ImageView(this.a);
            if (bitmap != null) {
                imageView2.setImageBitmap(bitmap);
            }
            imageView2.setLayoutParams(new Gallery.LayoutParams(this.c, this.c));
            imageView2.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView2.setPadding(10, 0, 10, 0);
            imageView2.setBackgroundResource(17301606);
            return imageView2;
        } catch (Exception e) {
            return view;
        }
    }
}
