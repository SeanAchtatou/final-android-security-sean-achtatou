package net.youmi.android;

import android.graphics.Bitmap;
import com.admogo.util.AdMogoUtil;
import com.smaato.SOMA.SOMATextBanner;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

class cq {
    /* access modifiers changed from: private */
    public byte[] a;
    /* access modifiers changed from: private */
    public int b;
    private int c;
    private int d;
    /* access modifiers changed from: private */
    public int e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;
    private final Vector p;
    private int q;
    private final int[] r;
    private int s;
    private int[] t;
    /* access modifiers changed from: private */
    public byte[] u;
    private final int[] v;
    /* access modifiers changed from: private */
    public final byte[] w;
    /* access modifiers changed from: private */
    public final short[] x;
    /* access modifiers changed from: private */
    public final byte[] y;
    /* access modifiers changed from: private */
    public final byte[] z;

    cq(InputStream inputStream) {
        this.p = new Vector();
        this.q = 0;
        this.r = new int[4];
        this.v = new int[256];
        this.w = new byte[256];
        this.x = new short[4096];
        this.y = new byte[4096];
        this.z = new byte[4097];
        a(inputStream);
    }

    cq(byte[] bArr) {
        this(bArr, 0, bArr.length);
    }

    cq(byte[] bArr, int i2, int i3) {
        this.p = new Vector();
        this.q = 0;
        this.r = new int[4];
        this.v = new int[256];
        this.w = new byte[256];
        this.x = new short[4096];
        this.y = new byte[4096];
        this.z = new byte[4097];
        this.a = bArr;
        this.b = i2;
        g();
    }

    private final void a(int i2, int i3) {
        int i4 = 0;
        int i5 = i2;
        while (i4 < i3) {
            int i6 = i5 + 1;
            int i7 = i6 + 1;
            this.v[i4] = (((((this.a[i5] & 255) << 8) + (this.a[i6] & 255)) << 8) + (this.a[i7] & 255)) | SOMATextBanner.DEFAULT_BACKGROUND_COLOR;
            i4++;
            i5 = i7 + 1;
        }
    }

    private final void a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(inputStream.available());
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                break;
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
        this.a = byteArrayOutputStream.toByteArray();
        this.b = 0;
        try {
            inputStream.close();
            byteArrayOutputStream.close();
        } catch (Exception e2) {
        }
        g();
    }

    private final void a(v vVar, int[] iArr) {
        int i2;
        boolean z2;
        int i3;
        vVar.a();
        if (vVar.e == 1) {
            a(vVar.k, vVar.j);
        } else {
            a(this.m, this.l);
        }
        int[] iArr2 = vVar.n;
        if (iArr2 == null || iArr2[3] != 1) {
            i2 = 0;
            z2 = false;
            i3 = 0;
        } else {
            int i4 = iArr2[5];
            int i5 = this.v[i4];
            this.v[i4] = 0;
            int i6 = i5;
            z2 = true;
            i3 = i4;
            i2 = i6;
        }
        boolean z3 = vVar.f == 1;
        int i7 = vVar.c;
        int i8 = vVar.d;
        int i9 = vVar.a;
        int i10 = vVar.b;
        int i11 = 0;
        for (int i12 = 0; i12 < i8; i12++) {
            if (!z3) {
                i11 = i10 + i12;
            }
            if (i11 < this.d) {
                int i13 = this.c * i11;
                int i14 = i13 + i9;
                int i15 = i14 + i7;
                int i16 = this.c + i13 < i15 ? i13 + this.c : i15;
                int i17 = i14;
                int i18 = i12 * i7;
                while (i17 < i16) {
                    int i19 = i18 + 1;
                    int i20 = this.v[this.u[i18] & 255];
                    if (i20 != 0) {
                        iArr[i17] = i20;
                    }
                    i17++;
                    i18 = i19;
                }
            }
        }
        if (z2) {
            this.v[i3] = i2;
        }
    }

    private final void b(int i2) {
        if (this.t == null) {
            this.t = new int[(this.c * this.d)];
        }
        v vVar = (v) this.p.elementAt(i2);
        int[] iArr = vVar.n;
        if (this.q > 0 && this.q == 2) {
            if (iArr == null || iArr[3] != 1) {
                for (int i3 = this.r[1]; i3 < this.r[3]; i3++) {
                    for (int i4 = this.r[0]; i4 < this.r[2]; i4++) {
                        this.t[(this.c * i3) + i4] = this.s;
                    }
                }
            } else {
                for (int i5 = this.r[1]; i5 < this.r[3]; i5++) {
                    for (int i6 = this.r[0]; i6 < this.r[2]; i6++) {
                        this.t[(this.c * i5) + i6] = 0;
                    }
                }
            }
        }
        a(vVar, this.t);
        this.s = this.j;
        this.q = 0;
        if (iArr != null) {
            this.q = iArr[1];
            if (vVar.e == 0 && this.i == iArr[5]) {
                this.s = 0;
            }
        }
        this.r[0] = vVar.a;
        this.r[1] = vVar.b;
        this.r[2] = vVar.a + vVar.c;
        this.r[3] = vVar.d + vVar.b;
    }

    private final void g() {
        if (!(String.valueOf(String.valueOf(String.valueOf("") + ((char) this.a[this.b])) + ((char) this.a[this.b + 1])) + ((char) this.a[this.b + 2])).equalsIgnoreCase("GIF")) {
            throw new IOException("this is not a gif image");
        }
        String str = String.valueOf(String.valueOf(String.valueOf("") + ((char) this.a[this.b + 3])) + ((char) this.a[this.b + 4])) + ((char) this.a[this.b + 5]);
        if (str.equalsIgnoreCase("87a") || str.equalsIgnoreCase("89a")) {
            this.b += 6;
            this.c = ak.a(this.a, this.b, 2);
            this.d = ak.a(this.a, this.b + 2, 2);
            this.e = ak.a(this.a[this.b + 4], 7, 1);
            this.f = ak.a(this.a[this.b + 4], 4, 3);
            this.g = ak.a(this.a[this.b + 4], 3, 1);
            this.h = ak.a(this.a[this.b + 4], 0, 3);
            this.i = this.a[this.b + 5] & 255;
            this.k = this.a[this.b + 6] & 255;
            this.b += 7;
            if (this.e == 1) {
                this.m = this.b;
                this.l = 1 << (this.h + 1);
                this.b += this.l * 3;
                a(this.m, this.l);
                this.j = this.v[this.i];
            }
            this.n = 0;
            int[] iArr = null;
            boolean z2 = false;
            while (!z2) {
                byte[] bArr = this.a;
                int i2 = this.b;
                this.b = i2 + 1;
                byte b2 = bArr[i2] & 255;
                if (b2 != -1) {
                    switch (b2) {
                        case AdMogoUtil.NETWORK_TYPE_ADWO:
                            byte[] bArr2 = this.a;
                            int i3 = this.b;
                            this.b = i3 + 1;
                            switch (bArr2[i3] & 255) {
                                case 249:
                                    iArr = i();
                                    continue;
                                default:
                                    h();
                                    continue;
                            }
                        case 44:
                            v vVar = new v(this);
                            vVar.b();
                            vVar.n = iArr;
                            this.p.addElement(vVar);
                            this.n++;
                            break;
                        case 59:
                            z2 = true;
                            break;
                    }
                } else {
                    return;
                }
            }
            return;
        }
        throw new IOException("this is not a gif image");
    }

    private final void h() {
        byte[] bArr = this.a;
        int i2 = this.b;
        this.b = i2 + 1;
        byte b2 = bArr[i2] & 255;
        while (b2 > 0) {
            this.b = b2 + this.b;
            byte[] bArr2 = this.a;
            int i3 = this.b;
            this.b = i3 + 1;
            b2 = bArr2[i3] & 255;
        }
    }

    private final int[] i() {
        byte[] bArr = this.a;
        int i2 = this.b;
        this.b = i2 + 1;
        byte b2 = bArr[i2] & 255;
        if (b2 != 4) {
            throw new IOException("parse graphics extend block error");
        }
        int[] iArr = {ak.a(this.a[this.b], 5, 3), ak.a(this.a[this.b], 2, 3), ak.a(this.a[this.b], 1, 1), ak.a(this.a[this.b], 0, 1), ak.a(this.a, this.b + 1, 2) * 10, ak.a(this.a, this.b + 3, 1)};
        this.b = b2 + this.b;
        return iArr;
    }

    /* access modifiers changed from: package-private */
    public final int a() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final int a(int i2) {
        if (i2 < 0 || i2 >= this.n) {
            throw new IllegalArgumentException("the frame[ " + i2 + " ]is invalid");
        }
        int[] iArr = ((v) this.p.elementAt(i2)).n;
        if (iArr != null) {
            return iArr[4];
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final int b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final int c() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public final int[] d() {
        int i2 = this.o;
        this.o = i2 + 1;
        b(i2);
        if (this.o >= this.n) {
            this.o = 0;
        }
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public final Bitmap e() {
        try {
            int[] d2 = d();
            if (d2 != null) {
                return Bitmap.createBitmap(d2, a(), b(), Bitmap.Config.ARGB_8888);
            }
            return null;
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final int f() {
        int i2 = this.o - 1;
        if (i2 < 0) {
            i2 = this.n - 1;
        }
        return a(i2);
    }
}
