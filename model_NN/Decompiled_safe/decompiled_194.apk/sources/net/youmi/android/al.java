package net.youmi.android;

import java.net.URI;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.RedirectHandler;
import org.apache.http.protocol.HttpContext;

class al implements RedirectHandler {
    private final /* synthetic */ ec a;

    al(ec ecVar) {
        this.a = ecVar;
    }

    public URI getLocationURI(HttpResponse httpResponse, HttpContext httpContext) {
        Header firstHeader = httpResponse.containsHeader("location") ? httpResponse.getFirstHeader("location") : httpResponse.containsHeader("Location") ? httpResponse.getFirstHeader("Location") : httpResponse.containsHeader("LOCATION") ? httpResponse.getFirstHeader("LOCATION") : null;
        if (firstHeader == null) {
            return null;
        }
        String value = firstHeader.getValue();
        if (value == null) {
            return null;
        }
        if (this.a != null) {
            this.a.a(value);
        }
        return URI.create(value);
    }

    public boolean isRedirectRequested(HttpResponse httpResponse, HttpContext httpContext) {
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        return statusCode == 301 || statusCode == 302 || statusCode == 303 || statusCode == 307;
    }
}
