package net.youmi.android;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

class q {
    private static Notification a;
    private static int b = 0;

    q() {
    }

    static Notification a(Context context) {
        try {
            if (a == null) {
                PendingIntent activity = PendingIntent.getActivity(context, 99999, AdActivity.a(context), 268435456);
                a = new Notification();
                a.icon = 17301633;
                a.tickerText = "正在下载";
                a.flags = 16;
                a.setLatestEventInfo(context.getApplicationContext(), "正在下载", "点击查看下载状态", activity);
            }
        } catch (Exception e) {
        }
        return a;
    }

    static String a(File file) {
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(5120);
            if (fileInputStream.available() > 0) {
                byteArrayOutputStream.write(fileInputStream.read());
                fileInputStream.skip(4096);
            }
            try {
                fileInputStream.close();
            } catch (Exception e) {
            }
            String a2 = byteArrayOutputStream.size() > 0 ? cp.a(byteArrayOutputStream.toByteArray()) : null;
            try {
                byteArrayOutputStream.close();
            } catch (Exception e2) {
            }
            return a2 != null ? a2.substring(0, 8) : a2;
        } catch (Exception e3) {
            return null;
        }
    }

    static ArrayList a() {
        try {
            String[] b2 = bb.h().b();
            if (b2 == null) {
                return null;
            }
            ArrayList arrayList = null;
            for (String a2 : b2) {
                ch a3 = a(a2);
                if (a3 != null && a(a3)) {
                    if (arrayList == null) {
                        arrayList = new ArrayList(10);
                    }
                    if (a3.c != null) {
                        if (System.currentTimeMillis() - a3.c.lastModified() > 300000) {
                            try {
                                a3.c.delete();
                            } catch (Exception e) {
                            }
                        } else {
                            arrayList.add(a3);
                        }
                    }
                }
            }
            return arrayList;
        } catch (Exception e2) {
            return null;
        }
    }

    static ch a(String str) {
        try {
            int lastIndexOf = str.lastIndexOf(95);
            if (lastIndexOf > -1) {
                long parseLong = Long.parseLong(str.substring(lastIndexOf + 1));
                String substring = str.substring(0, lastIndexOf);
                int lastIndexOf2 = substring.lastIndexOf(95);
                if (lastIndexOf2 > -1) {
                    String substring2 = substring.substring(lastIndexOf2 + 1);
                    String substring3 = substring.substring(0, lastIndexOf2);
                    ch chVar = new ch();
                    chVar.b = str;
                    chVar.d = parseLong;
                    chVar.e = substring2;
                    chVar.a = substring3;
                    chVar.c = new File(bb.h().a(str));
                    return chVar;
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    static ch a(String str, long j) {
        ch chVar = new ch();
        chVar.d = j;
        chVar.a = str;
        chVar.e = cp.b(String.valueOf(str) + "sdfkoi897hk5490g89dlsd99").substring(0, 8);
        StringBuilder sb = new StringBuilder(128);
        sb.append(str);
        sb.append('_');
        sb.append(chVar.e);
        sb.append('_');
        sb.append(j);
        chVar.b = sb.toString();
        chVar.c = new File(bb.h().a(chVar.b));
        return chVar;
    }

    static boolean a(ch chVar) {
        try {
            return cp.b(String.valueOf(chVar.a) + "sdfkoi897hk5490g89dlsd99").substring(0, 8).equals(chVar.e);
        } catch (Exception e) {
            return false;
        }
    }

    static boolean a(m mVar) {
        try {
            String a2 = a(mVar.c);
            return a2 != null && mVar.b.equals(a2);
        } catch (Exception e) {
        }
    }

    static ArrayList b() {
        try {
            String[] b2 = bb.g().b();
            if (b2 == null) {
                return null;
            }
            ArrayList arrayList = null;
            for (String b3 : b2) {
                m b4 = b(b3);
                if (b4 != null && b(b4)) {
                    if (arrayList == null) {
                        arrayList = new ArrayList(10);
                    }
                    arrayList.add(b4);
                }
            }
            return arrayList;
        } catch (Exception e) {
            f.a(e);
            return null;
        }
    }

    static m b(String str) {
        try {
            int lastIndexOf = str.lastIndexOf(95);
            if (lastIndexOf <= -1 || str.length() < lastIndexOf + 16) {
                return null;
            }
            m mVar = new m();
            String substring = str.substring(0, lastIndexOf);
            mVar.e = str;
            mVar.a = str.substring(lastIndexOf + 1, lastIndexOf + 9);
            mVar.b = str.substring(lastIndexOf + 9, lastIndexOf + 17);
            mVar.d = str.length() > lastIndexOf + 17 ? String.valueOf(substring) + str.substring(lastIndexOf + 17) : substring;
            mVar.c = new File(bb.g().a(str));
            return mVar.c.exists() ? mVar : mVar;
        } catch (Exception e) {
            return null;
        }
    }

    static m b(ch chVar) {
        String str;
        try {
            m mVar = new m();
            mVar.d = chVar.a;
            StringBuilder sb = new StringBuilder(128);
            sb.append(chVar.a);
            sb.append(chVar.c.length());
            sb.append("l53d2lj8sdf6jks8lq9efx");
            mVar.a = cp.b(sb.toString()).substring(0, 8);
            mVar.b = a(chVar.c);
            sb.delete(0, sb.length());
            int lastIndexOf = mVar.d.lastIndexOf(46);
            if (lastIndexOf > -1) {
                sb.append(mVar.d.substring(0, lastIndexOf));
                str = mVar.d.substring(lastIndexOf);
            } else {
                sb.append(mVar.d);
                str = "";
            }
            sb.append('_');
            sb.append(mVar.a);
            sb.append(mVar.b);
            sb.append(str);
            mVar.e = sb.toString();
            mVar.c = new File(bb.g().a(mVar.e));
            if (!chVar.c.renameTo(mVar.c) || !mVar.c.exists()) {
                return null;
            }
            return mVar;
        } catch (Exception e) {
        }
    }

    static synchronized void b(Context context) {
        synchronized (q.class) {
            try {
                NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
                if (notificationManager != null) {
                    Notification a2 = a(context);
                    if (a2 == null) {
                        notificationManager.cancel(99999);
                    } else if (b > 0) {
                        a2.number = b;
                        a2.flags = 16;
                        notificationManager.notify(99999, a2);
                    } else {
                        a2.flags = 16;
                        notificationManager.cancel(99999);
                    }
                }
            } catch (Exception e) {
                f.a(e);
            }
        }
        return;
    }

    static boolean b(m mVar) {
        try {
            StringBuilder sb = new StringBuilder(128);
            sb.append(mVar.d);
            sb.append(mVar.c.length());
            sb.append("l53d2lj8sdf6jks8lq9efx");
            return cp.b(sb.toString()).substring(0, 8).equals(mVar.a);
        } catch (Exception e) {
            return false;
        }
    }

    static synchronized void c(Context context) {
        synchronized (q.class) {
            b++;
            b(context);
        }
    }

    static synchronized void d(Context context) {
        synchronized (q.class) {
            b--;
            b(context);
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static synchronized void e(android.content.Context r2) {
        /*
            java.lang.Class<net.youmi.android.q> r0 = net.youmi.android.q.class
            monitor-enter(r0)
            r1 = 0
            net.youmi.android.q.b = r1     // Catch:{ Exception -> 0x000e, all -> 0x000b }
            b(r2)     // Catch:{ Exception -> 0x000e, all -> 0x000b }
        L_0x0009:
            monitor-exit(r0)
            return
        L_0x000b:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x000e:
            r1 = move-exception
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.q.e(android.content.Context):void");
    }
}
