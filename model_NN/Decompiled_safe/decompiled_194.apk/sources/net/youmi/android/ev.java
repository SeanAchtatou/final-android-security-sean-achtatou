package net.youmi.android;

import android.view.View;
import android.widget.RelativeLayout;

class ev extends RelativeLayout implements bo, fc, o {
    /* access modifiers changed from: private */
    public av a;
    private cc b;
    /* access modifiers changed from: private */
    public AdActivity c;
    private bz d;

    public ev(AdActivity adActivity, bz bzVar) {
        super(adActivity);
        this.c = adActivity;
        this.d = bzVar;
        f();
        g();
    }

    public void a() {
        this.a.b();
    }

    public void a(d dVar) {
        try {
            this.b.a(dVar);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ek ekVar) {
        try {
            this.a.a(ekVar);
        } catch (Exception e) {
        }
    }

    public void a_() {
        this.a.d();
    }

    public View b() {
        return this;
    }

    public void b_() {
        if (this.a.c()) {
            this.a.b();
        } else {
            d();
        }
    }

    public void c() {
        this.a.a();
    }

    public void d() {
        try {
            new Thread(new dx(this)).start();
        } catch (Exception e) {
        }
        this.c.a();
    }

    public void e() {
        bd.a(this.c);
    }

    /* access modifiers changed from: package-private */
    public void f() {
        this.a = new av(this.c, this.c.a, this);
        this.a.setId(1005);
        this.b = new cc(this.c, this.d, this);
        this.b.setId(1006);
    }

    /* access modifiers changed from: package-private */
    public void g() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(2, this.b.getId());
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, this.d.b().a());
        layoutParams2.addRule(12);
        addView(this.b, layoutParams2);
        addView(this.a, layoutParams);
    }
}
