package net.youmi.android;

import android.content.Context;
import com.adchina.android.ads.Common;
import java.io.ByteArrayOutputStream;
import org.apache.http.HttpResponse;

class ap extends bi {
    protected String a;
    protected ByteArrayOutputStream b;

    public ap(Context context, String str) {
        super(context, str);
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public boolean a(HttpResponse httpResponse) {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        this.b = new ByteArrayOutputStream(4096);
        this.k = this.b;
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        try {
            if (this.b != null) {
                if (this.f == null) {
                    byte[] byteArray = this.b.toByteArray();
                    this.a = new String(byteArray, Common.KEnc);
                    if (this.a != null) {
                        this.f = cu.c(this.a);
                    }
                    if (this.f != null) {
                        this.f = this.f.trim().toLowerCase();
                        if (!this.f.equals(Common.KEnc)) {
                            this.a = new String(byteArray, this.f);
                        }
                    }
                } else {
                    this.a = new String(this.b.toByteArray(), this.f);
                }
                return true;
            }
            this.m = 2;
            return false;
        } catch (Exception e) {
            this.m = 2;
        }
    }
}
