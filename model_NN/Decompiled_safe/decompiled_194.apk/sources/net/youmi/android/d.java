package net.youmi.android;

import java.util.Stack;

class d {
    final /* synthetic */ av a;
    private o b;
    private Stack c = new Stack();
    private ek d;
    private Stack e = new Stack();

    d(av avVar, o oVar) {
        this.a = avVar;
        this.b = oVar;
    }

    /* access modifiers changed from: package-private */
    public ek a() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public ek a(ek ekVar) {
        if (!(this.d == null || this.d == ekVar)) {
            this.c.push(this.d);
        }
        this.e.clear();
        this.d = ekVar;
        if (this.b != null) {
            this.b.a(this);
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public ek b() {
        ek ekVar;
        if (this.e.size() <= 0 || (ekVar = (ek) this.e.pop()) == null) {
            return null;
        }
        if (this.d != null) {
            this.c.push(this.d);
        }
        this.d = ekVar;
        if (this.b != null) {
            this.b.a(this);
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public ek c() {
        ek ekVar;
        if (this.c.size() <= 0 || (ekVar = (ek) this.c.pop()) == null) {
            return null;
        }
        if (this.d != null) {
            this.e.push(this.d);
        }
        this.d = ekVar;
        if (this.b != null) {
            this.b.a(this);
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.d != null;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.e.size() > 0;
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return this.c.size() > 0;
    }
}
