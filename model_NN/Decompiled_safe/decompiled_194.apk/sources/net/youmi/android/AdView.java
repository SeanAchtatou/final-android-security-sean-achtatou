package net.youmi.android;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public final class AdView extends RelativeLayout {
    private a a = new a();
    private fb b;
    private Activity c;
    private bz d;
    private int e = Color.rgb(64, 118, 170);
    private int f = 255;
    private int g = -1;
    /* access modifiers changed from: private */
    public AdViewListener h;
    private ct i;
    private ey j;
    private z k = new cd(this);
    private int l = 0;
    private int m = 0;

    public AdView(Activity activity) {
        super(activity);
        a(activity);
    }

    public AdView(Activity activity, int i2, int i3, int i4) {
        super(activity);
        a(activity, i2, i3, i4);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a((Activity) context, attributeSet);
    }

    public AdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a((Activity) context, attributeSet, i2);
    }

    /* access modifiers changed from: package-private */
    public a a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity) {
        Activity activity2 = activity;
        a(activity2, null, 0, this.e, this.g, this.f);
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity, int i2, int i3, int i4) {
        a(activity, null, 0, i2, i3, i4);
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity, AttributeSet attributeSet) {
        Activity activity2 = activity;
        AttributeSet attributeSet2 = attributeSet;
        a(activity2, attributeSet2, 0, this.e, this.g, this.f);
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity, AttributeSet attributeSet, int i2) {
        Activity activity2 = activity;
        AttributeSet attributeSet2 = attributeSet;
        int i3 = i2;
        a(activity2, attributeSet2, i3, this.e, this.g, this.f);
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity, AttributeSet attributeSet, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        this.c = activity;
        try {
            this.d = bz.a(this.c);
        } catch (Exception e2) {
        }
        if (attributeSet != null) {
            try {
                String str = "http://schemas.android.com/apk/res/" + activity.getPackageName();
                i9 = attributeSet.getAttributeUnsignedIntValue(str, "textColor", i4);
                try {
                    int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", i3);
                    try {
                        i6 = attributeSet.getAttributeUnsignedIntValue(str, "backgroundTransparent", i5);
                        int i11 = attributeUnsignedIntValue;
                        i8 = i9;
                        i7 = i11;
                    } catch (Exception e3) {
                        i10 = attributeUnsignedIntValue;
                        i8 = i9;
                        i7 = i10;
                        i6 = i5;
                        this.g = i8;
                        this.e = i7;
                        this.f = i6;
                    }
                } catch (Exception e4) {
                    i10 = i3;
                    i8 = i9;
                    i7 = i10;
                    i6 = i5;
                    this.g = i8;
                    this.e = i7;
                    this.f = i6;
                }
            } catch (Exception e5) {
                i10 = i3;
                i9 = i4;
                i8 = i9;
                i7 = i10;
                i6 = i5;
                this.g = i8;
                this.e = i7;
                this.f = i6;
            }
        } else {
            i6 = i5;
            i7 = i3;
            i8 = i4;
        }
        if (i6 > 255) {
            i6 = 255;
        }
        if (i6 < 0) {
            i6 = 0;
        }
        this.g = i8;
        this.e = i7;
        this.f = i6;
    }

    /* access modifiers changed from: package-private */
    public void a(ct ctVar) {
        if (ctVar != null) {
            if (this.i == null || !this.i.f().equals(ctVar.f())) {
                this.i = ctVar;
                g();
                if (this.b != null) {
                    this.b.b(ctVar);
                }
            }
        }
    }

    public void addView(View view) {
        if (this.b == view) {
            super.addView(view);
        }
    }

    public void addView(View view, int i2) {
        if (this.b == view) {
            super.addView(view, i2);
        }
    }

    public void addView(View view, int i2, int i3) {
        if (view == this.b) {
            super.addView(view, i2, i3);
        }
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        if (view == this.b) {
            super.addView(view, i2, layoutParams);
        }
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        if (view == this.b) {
            super.addView(view, layoutParams);
        }
    }

    /* access modifiers changed from: protected */
    public boolean addViewInLayout(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        if (view == this.b) {
            return super.addViewInLayout(view, i2, layoutParams);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean addViewInLayout(View view, int i2, ViewGroup.LayoutParams layoutParams, boolean z) {
        if (view == this.b) {
            return super.addViewInLayout(view, i2, layoutParams, z);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public bz b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public ct c() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (getVisibility() == 0 && this.a.a(this)) {
            switch (cy.a(this.c)) {
                case 0:
                    try {
                        aj.a(this.c, this);
                        return;
                    } catch (Exception e2) {
                        f.a(e2);
                    }
                case 1:
                    a(cy.b(this.c));
                    return;
                case 2:
                    a(cy.c(this.c));
                    return;
                case 3:
                    a(cy.g(this.c));
                    return;
                case 4:
                    a(cy.h(this.c));
                    return;
                case 5:
                    a(cy.i(this.c));
                    return;
                case 6:
                    a(cy.f(this.c));
                    return;
                case 7:
                    a(cy.d(this.c));
                    return;
                case 8:
                    a(cy.e(this.c));
                    return;
                default:
                    return;
            }
            f.a(e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        new Thread(new cj(this)).start();
    }

    /* access modifiers changed from: package-private */
    public void g() {
        try {
            getHandler().post(new cg(this));
        } catch (Exception e2) {
        }
    }

    public int getAdHeight() {
        return this.m;
    }

    public int getAdWidth() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        try {
            getHandler().post(new cl(this));
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        if (this.b == null) {
            try {
                this.m = this.d.a().a();
            } catch (Exception e2) {
                f.a(e2);
            }
            try {
                ViewGroup.LayoutParams layoutParams = getLayoutParams();
                if (layoutParams != null) {
                    layoutParams.height = this.m;
                    if (layoutParams.width == -1) {
                        this.l = this.d.d();
                    } else if (layoutParams.width == -2) {
                        this.l = this.d.f();
                    } else {
                        layoutParams.width = -2;
                        this.l = this.d.f();
                    }
                } else {
                    this.l = this.d.d();
                }
            } catch (Exception e3) {
                f.a(e3);
            }
            try {
                this.b = new fb(this.c, this, this.d, this.e, this.g, this.f);
                addView(this.b, new RelativeLayout.LayoutParams(this.l, this.m));
            } catch (Exception e4) {
                f.a(e4);
            }
        }
        try {
            this.j = new ey(this.k, -1);
        } catch (Exception e5) {
        }
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            this.j.c();
        } catch (Exception e2) {
            f.a(e2);
        }
        super.onDetachedFromWindow();
    }

    public void onWindowFocusChanged(boolean z) {
        try {
            this.a.a = !z;
            if (z) {
                this.j.b();
                refreshAd();
            } else {
                this.j.a();
            }
        } catch (Exception e2) {
        }
        super.onWindowFocusChanged(z);
    }

    public void refreshAd() {
        f();
    }

    public void setAdViewListener(AdViewListener adViewListener) {
        this.h = adViewListener;
    }

    public void setVisibility(int i2) {
        super.setVisibility(i2);
        if (i2 == 0) {
            this.a.b = false;
        } else {
            this.a.b = true;
        }
    }
}
