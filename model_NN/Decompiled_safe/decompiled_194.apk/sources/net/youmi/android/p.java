package net.youmi.android;

import android.os.Environment;
import android.view.View;
import java.io.File;

class p implements View.OnClickListener {
    final /* synthetic */ ce a;

    p(ce ceVar) {
        this.a = ceVar;
    }

    public void onClick(View view) {
        try {
            if (this.a.o != null) {
                if (!ay.a(this.a.a)) {
                    ax.a(this.a.a, "存储卡不可用,无法保存图片");
                    return;
                }
                File file = new File(this.a.o.a);
                if (file.exists()) {
                    String name = file.getName();
                    try {
                        if (this.a.o.d != null) {
                            String lowerCase = this.a.o.d.toLowerCase();
                            name = lowerCase.equals("image/jpeg") ? String.valueOf(name) + ".jpg" : lowerCase.equals("image/png") ? String.valueOf(name) + ".png" : lowerCase.equals("image/gif") ? String.valueOf(name) + ".gif" : String.valueOf(name) + ".jpg";
                        }
                    } catch (Exception e) {
                    }
                    String str = Environment.getExternalStorageDirectory() + "/Pictures/";
                    File file2 = new File(str);
                    if (!file2.exists()) {
                        file2.mkdirs();
                    }
                    String str2 = String.valueOf(str) + name;
                    if (eh.a(file, new File(str2))) {
                        ax.b(this.a.a, "图片已经保存到" + str2);
                    }
                }
            }
        } catch (Exception e2) {
        }
    }
}
