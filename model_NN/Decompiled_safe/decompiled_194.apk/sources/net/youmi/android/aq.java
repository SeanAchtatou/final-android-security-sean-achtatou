package net.youmi.android;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.widget.Button;
import android.widget.RelativeLayout;

class aq extends RelativeLayout {
    Button a;
    ah b;
    bz c;

    public aq(Activity activity, ah ahVar, bz bzVar) {
        super(activity);
        this.b = ahVar;
        this.c = bzVar;
        this.a = new Button(activity);
        Drawable a2 = bx.a(this.c, EMPTY_STATE_SET, PRESSED_ENABLED_STATE_SET);
        if (a2 != null) {
            this.a.setBackgroundDrawable(a2);
        } else {
            this.a.setText("返回");
        }
        this.a.setOnClickListener(new co(this));
        RelativeLayout.LayoutParams a3 = h.a(this.c.b().a(), this.c.b().a());
        a3.addRule(15);
        a3.addRule(11);
        addView(this.a, a3);
        setBackgroundDrawable(bx.a(this.c));
    }
}
