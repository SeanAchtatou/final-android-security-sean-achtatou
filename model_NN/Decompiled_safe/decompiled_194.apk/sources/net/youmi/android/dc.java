package net.youmi.android;

import java.io.File;
import java.io.FileInputStream;

class dc extends dg {
    private cq i;

    dc(eb ebVar) {
        super(ebVar);
    }

    /* access modifiers changed from: protected */
    public boolean a(File file) {
        try {
            this.i = new cq(new FileInputStream(file));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(byte[] bArr) {
        try {
            this.i = new cq(bArr);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public cq c() {
        return this.i;
    }
}
