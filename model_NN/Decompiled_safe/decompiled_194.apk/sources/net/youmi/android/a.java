package net.youmi.android;

class a {
    boolean a = false;
    boolean b = false;
    boolean c = false;
    private boolean d = false;
    private long e = 0;

    a() {
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public boolean a(AdView adView) {
        if (this.d && System.currentTimeMillis() - this.e > 5000) {
            c();
            try {
                adView.postInvalidate();
            } catch (Exception e2) {
            }
        }
        return !this.a && !this.b && !this.c && !this.d;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.e = System.currentTimeMillis();
        this.d = true;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.d = false;
    }
}
