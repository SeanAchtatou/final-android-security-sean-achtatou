package net.youmi.android;

import android.app.Activity;
import android.content.Context;

class aj {
    private static ct a;
    private static String b;

    aj() {
    }

    static ct a() {
        return a;
    }

    static void a(Activity activity, AdView adView) {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            if (ef.a(currentTimeMillis)) {
                try {
                    adView.a(a);
                } catch (Exception e) {
                }
                ef.n = currentTimeMillis;
                ef.p = true;
                try {
                    ct a2 = au.a(activity, adView);
                    if (a2 != null) {
                        a = a2;
                        ef.o = System.currentTimeMillis();
                    }
                } catch (Exception e2) {
                }
                ef.p = false;
                adView.a(a);
                au.a(activity, a);
                au.a(activity, adView, a);
                try {
                    es.a(activity, adView);
                } catch (Exception e3) {
                }
            } else {
                adView.a(a);
                es.a(activity, adView);
            }
        } catch (Exception e4) {
        }
    }

    static void a(Context context, ct ctVar) {
        if (ctVar != null && !ctVar.s()) {
            ctVar.v();
            try {
                new Thread(new aa(context, ctVar)).start();
            } catch (Exception e) {
            }
        }
    }

    static void a(String str) {
        b = str;
    }
}
