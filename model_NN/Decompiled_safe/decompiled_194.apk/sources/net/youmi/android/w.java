package net.youmi.android;

import com.madhouse.android.ads.AdView;

class w {
    final /* synthetic */ bz a;
    private bz b;
    private dt c;
    private ae d;
    private ad e;

    public w(bz bzVar, bz bzVar2) {
        this.a = bzVar;
        this.b = bzVar2;
        if (bzVar2.c()) {
            this.c = dt.b;
            return;
        }
        switch (bzVar2.e()) {
            case 120:
                this.c = dt.a;
                return;
            case 160:
                this.c = dt.b;
                return;
            case AdView.AD_MEASURE_240:
                this.c = dt.c;
                return;
            case 320:
                this.c = dt.d;
                return;
            default:
                this.c = dt.b;
                return;
        }
    }

    public int a() {
        return this.c.b();
    }

    public dt b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public ae c() {
        if (this.d == null) {
            this.d = new ae(this, this.b, this);
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public ad d() {
        if (this.e == null) {
            this.e = new ad(this, this.b, this);
        }
        return this.e;
    }
}
