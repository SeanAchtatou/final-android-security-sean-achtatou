package net.youmi.android;

import android.app.Activity;
import android.content.Context;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.adchina.android.ads.Common;
import java.io.File;

final class av extends WebView implements dz {
    d a;
    Activity b;
    ct c;

    public av(Activity activity, ct ctVar, o oVar) {
        super(activity);
        this.a = new d(this, oVar);
        this.b = activity;
        this.c = ctVar;
        i();
    }

    public av(Activity activity, o oVar) {
        super(activity);
        this.a = new d(this, oVar);
        this.b = activity;
        i();
    }

    private void b(ek ekVar) {
        if (ekVar != null) {
            try {
                loadDataWithBaseURL(ekVar.a(), ekVar.b(), "text/html", Common.KEnc, null);
            } catch (Exception e) {
                f.a(e);
            }
        }
    }

    private void i() {
        try {
            WebView.enablePlatformNotifications();
        } catch (Exception e) {
        }
        j();
        k();
        l();
        m();
    }

    private void j() {
        WebSettings settings = getSettings();
        settings.setBuiltInZoomControls(true);
        settings.setCacheMode(1);
        settings.setAllowFileAccess(true);
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        settings.setUseWideViewPort(true);
        settings.setLightTouchEnabled(true);
        settings.setSavePassword(true);
    }

    private void k() {
        setWebViewClient(new ck(this));
    }

    private void l() {
        setWebChromeClient(new cf(this));
    }

    private void m() {
        try {
            setDownloadListener(new ci(this));
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        b(this.a.a());
    }

    public void a(int i) {
        if (i >= 0) {
            try {
                if (this.b == null) {
                    return;
                }
                if (i >= 100) {
                    this.b.setProgressBarVisibility(false);
                    return;
                }
                this.b.setProgressBarIndeterminateVisibility(true);
                this.b.setProgress(i * 100);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ek ekVar) {
        b(this.a.a(ekVar));
    }

    public void a(i iVar) {
        q.c(this.b);
        ax.a(this.b, "开始下载");
        a(100);
        try {
            bv bvVar = new bv();
            bvVar.a = iVar.c();
            bvVar.c = iVar.a();
            bvVar.d = iVar.b();
            k.a(this.b, bvVar, 1);
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.youmi.android.bd.b(android.content.Context, java.lang.String):void
     arg types: [android.app.Activity, java.lang.String]
     candidates:
      net.youmi.android.bd.b(android.app.Activity, java.lang.String):void
      net.youmi.android.bd.b(android.content.Context, java.lang.String):void */
    public void a(i iVar, File file, String str) {
        q.d(this.b);
        if (file != null && file.exists()) {
            ax.a(this.b, "下载成功");
            try {
                bv a2 = k.a(this.b, file.getPath(), str, iVar.a(), iVar.b(), iVar.c());
                k.a(this.b, a2, 2);
                k.a(this.b, a2, 3);
            } catch (Exception e) {
                f.a(e);
            }
            bd.b((Context) this.b, file.getPath());
        }
    }

    public void a(i iVar, ek ekVar) {
        if (ekVar != null) {
            if (this.c != null) {
                ekVar.a(az.a(ekVar.b(), this.c.z(), this.c.o(), this.c.k()));
            }
            a(ekVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        b(this.a.c());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.youmi.android.bd.b(android.content.Context, java.lang.String):void
     arg types: [android.app.Activity, java.lang.String]
     candidates:
      net.youmi.android.bd.b(android.app.Activity, java.lang.String):void
      net.youmi.android.bd.b(android.content.Context, java.lang.String):void */
    public void b(i iVar, File file, String str) {
        a(100);
        if (file != null && file.exists()) {
            ax.a(this.b, "下载成功");
            try {
                k.a(this.b, k.a(this.b, file.getPath(), str, iVar.a(), iVar.b(), iVar.c()), 3);
            } catch (Exception e) {
                f.a(e);
            }
            bd.b((Context) this.b, file.getPath());
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.a.f();
    }

    /* access modifiers changed from: package-private */
    public void d() {
        b(this.a.b());
    }

    public void e() {
        a(100);
    }

    public void f() {
        ax.a(this.b, "正在下载");
        a(100);
    }

    public void g() {
        ax.a(this.b, "存储卡不可用,请启用存储卡", 1);
        a(100);
    }

    public void h() {
        q.d(this.b);
        ax.a(this.b, "下载失败");
    }
}
