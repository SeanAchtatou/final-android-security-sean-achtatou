package defpackage;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.InterstitialAd;
import com.google.ads.util.AdUtil;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/* renamed from: c  reason: default package */
public final class c extends AsyncTask<AdRequest, String, AdRequest.ErrorCode> {
    String a = null;
    public String b = null;
    public AdRequest.ErrorCode c = null;
    private String d;
    private b e;
    private d f;
    private WebView g;
    private boolean h = false;
    private boolean i = false;

    /* renamed from: c$a */
    private class a extends Exception {
        public a(String str) {
            super(str);
        }
    }

    /* renamed from: c$b */
    private class b extends Exception {
        public b(String str) {
            super(str);
        }
    }

    public c(d dVar) {
        this.f = dVar;
        Activity e2 = dVar.e();
        if (e2 != null) {
            this.g = new WebView(e2.getApplicationContext());
            this.g.getSettings().setJavaScriptEnabled(true);
            this.g.setWebViewClient(new h(dVar, a.a, false, false));
            AdUtil.a(this.g);
            this.g.setVisibility(8);
            this.g.setWillNotDraw(true);
            this.e = new b(this, dVar, e2.getApplicationContext());
            return;
        }
        this.g = null;
        this.e = null;
        com.google.ads.util.a.e("activity was null while trying to create an AdLoader.");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.ads.AdRequest.ErrorCode doInBackground(com.google.ads.AdRequest... r14) {
        /*
            r13 = this;
            r11 = 0
            r10 = 0
            monitor-enter(r13)
            android.webkit.WebView r0 = r13.g     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x000c
            b r0 = r13.e     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x0015
        L_0x000c:
            java.lang.String r0 = "adRequestWebView was null while trying to load an ad."
            com.google.ads.util.a.e(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
        L_0x0014:
            return r0
        L_0x0015:
            r0 = 0
            r0 = r14[r0]     // Catch:{ all -> 0x0029 }
            d r1 = r13.f     // Catch:{ all -> 0x0029 }
            android.app.Activity r1 = r1.e()     // Catch:{ all -> 0x0029 }
            if (r1 != 0) goto L_0x002c
            java.lang.String r0 = "activity was null while forming an ad request."
            com.google.ads.util.a.e(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0029:
            r0 = move-exception
            monitor-exit(r13)
            throw r0
        L_0x002c:
            java.lang.String r2 = r13.a(r0, r1)     // Catch:{ b -> 0x0054, a -> 0x005e }
            android.webkit.WebView r0 = r13.g     // Catch:{ all -> 0x0029 }
            r1 = 0
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "utf-8"
            r5 = 0
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0029 }
            d r0 = r13.f     // Catch:{ all -> 0x0029 }
            long r6 = r0.n()     // Catch:{ all -> 0x0029 }
            long r8 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0029 }
            int r0 = (r6 > r11 ? 1 : (r6 == r11 ? 0 : -1))
            if (r0 <= 0) goto L_0x004c
            r13.wait(r6)     // Catch:{ InterruptedException -> 0x0068 }
        L_0x004c:
            com.google.ads.AdRequest$ErrorCode r0 = r13.c     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x0083
            com.google.ads.AdRequest$ErrorCode r0 = r13.c     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0054:
            r0 = move-exception
            java.lang.String r1 = "Unable to connect to network."
            com.google.ads.util.a.b(r1, r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x005e:
            r0 = move-exception
            java.lang.String r1 = "Caught internal exception."
            com.google.ads.util.a.b(r1, r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0068:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r1.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r2 = "AdLoader InterruptedException while getting the URL: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.a.e(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0083:
            java.lang.String r0 = r13.b     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x00a8
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r0.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "AdLoader timed out after "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "ms while getting the URL."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.a.c(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x00a8:
            r0 = 1
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ all -> 0x0029 }
            r1 = 0
            java.lang.String r2 = r13.b     // Catch:{ all -> 0x0029 }
            r0[r1] = r2     // Catch:{ all -> 0x0029 }
            r13.publishProgress(r0)     // Catch:{ all -> 0x0029 }
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0029 }
            long r0 = r0 - r8
            long r0 = r6 - r0
            int r2 = (r0 > r11 ? 1 : (r0 == r11 ? 0 : -1))
            if (r2 <= 0) goto L_0x00c1
            r13.wait(r0)     // Catch:{ InterruptedException -> 0x00ca }
        L_0x00c1:
            com.google.ads.AdRequest$ErrorCode r0 = r13.c     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x00e6
            com.google.ads.AdRequest$ErrorCode r0 = r13.c     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x00ca:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r1.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r2 = "AdLoader InterruptedException while getting the HTML: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.a.e(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x00e6:
            java.lang.String r0 = r13.a     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x010b
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r0.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "AdLoader timed out after "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "ms while getting the HTML."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.a.c(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x010b:
            d r0 = r13.f     // Catch:{ all -> 0x0029 }
            g r0 = r0.i()     // Catch:{ all -> 0x0029 }
            d r1 = r13.f     // Catch:{ all -> 0x0029 }
            h r1 = r1.j()     // Catch:{ all -> 0x0029 }
            r1.a()     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = r13.d     // Catch:{ all -> 0x0029 }
            java.lang.String r2 = r13.a     // Catch:{ all -> 0x0029 }
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "utf-8"
            r5 = 0
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0029 }
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0029 }
            long r1 = r1 - r8
            long r1 = r6 - r1
            int r3 = (r1 > r11 ? 1 : (r1 == r11 ? 0 : -1))
            if (r3 <= 0) goto L_0x0134
            r13.wait(r1)     // Catch:{ InterruptedException -> 0x013c }
        L_0x0134:
            boolean r1 = r13.i     // Catch:{ all -> 0x0029 }
            if (r1 == 0) goto L_0x015b
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            r0 = r10
            goto L_0x0014
        L_0x013c:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r2.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r3 = "AdLoader InterruptedException while loading the HTML: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.a.e(r1)     // Catch:{ all -> 0x0029 }
            r0.stopLoading()     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x015b:
            r0.stopLoading()     // Catch:{ all -> 0x0029 }
            r0 = 1
            r13.h = r0     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r0.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "AdLoader timed out after "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "ms while loading the HTML."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.a.c(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c.doInBackground(com.google.ads.AdRequest[]):com.google.ads.AdRequest$ErrorCode");
    }

    private String a(AdRequest adRequest, Activity activity) throws a, b {
        Context applicationContext = activity.getApplicationContext();
        Map<String, Object> requestMap = adRequest.getRequestMap(applicationContext);
        f l = this.f.l();
        long h2 = l.h();
        if (h2 > 0) {
            requestMap.put("prl", Long.valueOf(h2));
        }
        String g2 = l.g();
        if (g2 != null) {
            requestMap.put("ppcl", g2);
        }
        String f2 = l.f();
        if (f2 != null) {
            requestMap.put("pcl", f2);
        }
        long e2 = l.e();
        if (e2 > 0) {
            requestMap.put("pcc", Long.valueOf(e2));
        }
        requestMap.put("preqs", Long.valueOf(f.i()));
        String j = l.j();
        if (j != null) {
            requestMap.put("pai", j);
        }
        if (l.k()) {
            requestMap.put("aoi_timeout", "true");
        }
        if (l.m()) {
            requestMap.put("aoi_nofill", "true");
        }
        String p = l.p();
        if (p != null) {
            requestMap.put("pit", p);
        }
        l.a();
        l.d();
        if (this.f.f() instanceof InterstitialAd) {
            requestMap.put("format", "interstitial_mb");
        } else {
            AdSize k = this.f.k();
            String adSize = k.toString();
            if (adSize != null) {
                requestMap.put("format", adSize);
            } else {
                HashMap hashMap = new HashMap();
                hashMap.put("w", Integer.valueOf(k.getWidth()));
                hashMap.put("h", Integer.valueOf(k.getHeight()));
                requestMap.put("ad_frame", hashMap);
            }
        }
        requestMap.put("slotname", this.f.h());
        requestMap.put("js", "afma-sdk-a-v4.1.0");
        try {
            int i2 = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0).versionCode;
            requestMap.put("msid", applicationContext.getPackageName());
            requestMap.put("app_name", i2 + ".android." + applicationContext.getPackageName());
            requestMap.put("isu", AdUtil.a(applicationContext));
            String d2 = AdUtil.d(applicationContext);
            if (d2 == null) {
                throw new b("NETWORK_ERROR");
            }
            requestMap.put("net", d2);
            String e3 = AdUtil.e(applicationContext);
            if (!(e3 == null || e3.length() == 0)) {
                requestMap.put("cap", e3);
            }
            requestMap.put("u_audio", Integer.valueOf(AdUtil.f(applicationContext).ordinal()));
            requestMap.put("u_so", AdUtil.g(applicationContext));
            DisplayMetrics a2 = AdUtil.a(activity);
            requestMap.put("u_sd", Float.valueOf(a2.density));
            requestMap.put("u_h", Integer.valueOf((int) (((float) a2.heightPixels) / a2.density)));
            requestMap.put("u_w", Integer.valueOf((int) (((float) a2.widthPixels) / a2.density)));
            requestMap.put("hl", Locale.getDefault().getLanguage());
            if (AdUtil.a()) {
                requestMap.put("simulator", 1);
            }
            String str = "<html><head><script src=\"http://www.gstatic.com/afma/sdk-core-v40.js\"></script><script>AFMA_buildAdURL(" + AdUtil.a(requestMap) + ");" + "</script></head><body></body></html>";
            com.google.ads.util.a.c("adRequestUrlHtml: " + str);
            return str;
        } catch (PackageManager.NameNotFoundException e4) {
            throw new a("NameNotFound!");
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a() {
        this.i = true;
        notify();
    }

    public final synchronized void a(AdRequest.ErrorCode errorCode) {
        this.c = errorCode;
        notify();
    }

    public final synchronized void a(String str) {
        this.b = str;
        notify();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str, String str2) {
        this.d = str2;
        this.a = str;
        notify();
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        com.google.ads.util.a.a("AdLoader cancelled.");
        this.g.stopLoading();
        this.g.destroy();
        this.e.cancel(false);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object x0) {
        AdRequest.ErrorCode errorCode = (AdRequest.ErrorCode) x0;
        synchronized (this) {
            if (errorCode == null) {
                this.f.q();
            } else {
                this.g.stopLoading();
                this.g.destroy();
                this.e.cancel(false);
                if (this.h) {
                    this.f.i().setVisibility(8);
                }
                this.f.a(errorCode);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onProgressUpdate(Object[] x0) {
        this.e.execute(((String[]) x0)[0]);
    }
}
