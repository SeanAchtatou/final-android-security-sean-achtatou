package com.tobyyaa.fanyiyou;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.tobyyaa.fanyiyou.Languages;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class History {
    private static final String HISTORY = "history";
    private static final Comparator<HistoryRecord> LANGUAGE_COMPARATOR = new Comparator<HistoryRecord>() {
        public int compare(HistoryRecord object1, HistoryRecord object2) {
            int result = object1.to.getLongName().compareTo(object2.to.getLongName());
            if (result == 0) {
                return object1.input.compareTo(object2.input);
            }
            return result;
        }
    };
    private static final Comparator<HistoryRecord> MOST_RECENT_COMPARATOR = new Comparator<HistoryRecord>() {
        public int compare(HistoryRecord object1, HistoryRecord object2) {
            return (int) (object2.when - object1.when);
        }
    };
    private List<HistoryRecord> mHistoryRecords = Lists.newArrayList();

    public History(SharedPreferences prefs) {
        this.mHistoryRecords = restoreHistory(prefs);
    }

    public static List<HistoryRecord> restoreHistory(SharedPreferences prefs) {
        List<HistoryRecord> result = Lists.newArrayList();
        Map<String, ?> allKeys = prefs.getAll();
        for (String key : allKeys.keySet()) {
            if (key.startsWith(HISTORY)) {
                result.add(HistoryRecord.decode((String) allKeys.get(key)));
            }
        }
        return result;
    }

    /* JADX INFO: Multiple debug info for r9v1 android.content.SharedPreferences: [D('to' com.tobyyaa.fanyiyou.Languages$Language), D('prefs' android.content.SharedPreferences)] */
    /* JADX INFO: Multiple debug info for r7v3 android.content.SharedPreferences$Editor: [D('edit' android.content.SharedPreferences$Editor), D('i' int)] */
    public static void addHistoryRecord(Context context, Languages.Language from, Languages.Language to, String input, String output) {
        new History(TranslateActivity.getPrefs(context));
        HistoryRecord hr = new HistoryRecord(from, to, input, output, System.currentTimeMillis());
        SharedPreferences prefs = TranslateActivity.getPrefs(context);
        int i = 0;
        while (true) {
            String key = "history-" + i;
            if (!prefs.contains(key)) {
                SharedPreferences.Editor edit = prefs.edit();
                edit.putString(key, hr.encode());
                log("Committing " + key + " " + hr.encode());
                edit.commit();
                return;
            }
            i++;
        }
    }

    private static void log(String s) {
        Log.d("Translate", "[History] " + s);
    }

    public List<HistoryRecord> getHistoryRecordsMostRecentFirst() {
        Collections.sort(this.mHistoryRecords, MOST_RECENT_COMPARATOR);
        return this.mHistoryRecords;
    }

    public List<HistoryRecord> getHistoryRecordsByLanguages() {
        Collections.sort(this.mHistoryRecords, LANGUAGE_COMPARATOR);
        return this.mHistoryRecords;
    }

    public List<HistoryRecord> getHistoryRecords(Comparator<HistoryRecord> comparator) {
        if (comparator != null) {
            Collections.sort(this.mHistoryRecords, comparator);
        }
        return this.mHistoryRecords;
    }

    public void clear(Context context) {
        int size = this.mHistoryRecords.size();
        this.mHistoryRecords = Lists.newArrayList();
        SharedPreferences.Editor edit = TranslateActivity.getPrefs(context).edit();
        for (int i = 0; i < size; i++) {
            String key = "history-" + i;
            log("Removing key " + key);
            edit.remove(key);
        }
        edit.commit();
    }
}
