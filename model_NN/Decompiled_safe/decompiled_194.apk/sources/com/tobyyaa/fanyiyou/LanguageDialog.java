package com.tobyyaa.fanyiyou;

import android.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import com.tobyyaa.fanyiyou.Languages;

public class LanguageDialog extends AlertDialog implements View.OnClickListener {
    private TranslateActivity mActivity;
    private boolean mFrom;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    protected LanguageDialog(TranslateActivity activity) {
        super(activity);
        this.mActivity = activity;
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService("layout_inflater");
        ScrollView scrollView = (ScrollView) inflater.inflate((int) R.layout.language_dialog, (ViewGroup) null);
        setView(scrollView);
        LinearLayout layout = (LinearLayout) scrollView.findViewById(R.id.languages);
        LinearLayout current = null;
        Languages.Language[] languages = Languages.Language.values();
        for (Languages.Language language : languages) {
            if (current != null) {
                layout.addView(current, new ViewGroup.LayoutParams(-1, -1));
            }
            current = new LinearLayout(activity);
            current.setOrientation(0);
            Button button = (Button) inflater.inflate((int) R.layout.language_entry, (ViewGroup) current, false);
            language.configureButton(this.mActivity, button);
            button.setOnClickListener(this);
            current.addView(button, button.getLayoutParams());
        }
        if (current != null) {
            layout.addView(current, new ViewGroup.LayoutParams(-1, -1));
        }
        setTitle(" ");
    }

    private void log(String s) {
        Log.d("Translate", s);
    }

    public void onClick(View v) {
        this.mActivity.setNewLanguage((Languages.Language) v.getTag(), this.mFrom, true);
        dismiss();
    }

    public void setFrom(boolean from) {
        log("From set to " + from);
        this.mFrom = from;
        setTitle(from ? this.mActivity.getString(R.string.translate_from) : this.mActivity.getString(R.string.translate_to));
    }
}
