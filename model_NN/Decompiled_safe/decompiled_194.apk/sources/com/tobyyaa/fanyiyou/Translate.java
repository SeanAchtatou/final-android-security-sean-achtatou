package com.tobyyaa.fanyiyou;

import android.util.Log;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import org.json.JSONObject;

public class Translate {
    private static final String ENCODING = "UTF-8";
    private static final String TEXT_VAR = "&q=";
    private static final String URL_STRING = "http://ajax.googleapis.com/ajax/services/language/translate?v=1.0&langpair=";

    public static String translate(String text, String from, String to) throws Exception {
        return retrieveTranslation(text, from, to);
    }

    private static String retrieveTranslation(String text, String from, String to) throws Exception {
        HttpURLConnection uc;
        try {
            StringBuilder url = new StringBuilder();
            url.append(URL_STRING).append(from).append("%7C").append(to);
            url.append(TEXT_VAR).append(URLEncoder.encode(text, ENCODING));
            Log.d(TranslateServiceForMe.TAG, "Connecting to " + url.toString());
            uc = (HttpURLConnection) new URL(url.toString()).openConnection();
            uc.setDoInput(true);
            uc.setDoOutput(true);
            Log.d(TranslateServiceForMe.TAG, "getInputStream()");
            String string = ((JSONObject) new JSONObject(toString(uc.getInputStream())).get("responseData")).getString("translatedText");
            uc.getInputStream().close();
            if (uc.getErrorStream() != null) {
                uc.getErrorStream().close();
            }
            return string;
        } catch (Exception e) {
            throw e;
        } catch (Throwable th) {
            uc.getInputStream().close();
            if (uc.getErrorStream() != null) {
                uc.getErrorStream().close();
            }
            throw th;
        }
    }

    private static String toString(InputStream inputStream) throws Exception {
        StringBuilder outputBuilder = new StringBuilder();
        if (inputStream != null) {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, ENCODING));
                while (true) {
                    String string = reader.readLine();
                    if (string == null) {
                        break;
                    }
                    outputBuilder.append(string).append(10);
                }
            } catch (Exception e) {
                Log.e(TranslateServiceForMe.TAG, "Error reading translation stream.", e);
            }
        }
        return outputBuilder.toString();
    }
}
