package com.tobyyaa.fanyiyou;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.tobyyaa.fanyiyou.ITranslateForMe;

public class TranslateServiceForMe extends Service {
    public static final String TAG = "TranslateService";
    private static final String[] TRANSLATE_ACTIONS = {"android.intent.action.GET_CONTENT", "android.intent.action.PICK", "android.intent.action.VIEW"};
    private final ITranslateForMe.Stub mBinder = new ITranslateForMe.Stub() {
        public String translate(String text, String from, String to) {
            try {
                return Translate.translate(text, from, to);
            } catch (Exception e) {
                Log.e(TranslateServiceForMe.TAG, "Failed to perform translation: " + e.getMessage());
                return null;
            }
        }

        public int getVersion() {
            return 1;
        }
    };

    public IBinder onBind(Intent intent) {
        for (String equals : TRANSLATE_ACTIONS) {
            if (equals.equals(intent.getAction())) {
                return this.mBinder;
            }
        }
        return null;
    }
}
