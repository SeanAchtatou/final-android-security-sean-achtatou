package com.tobyyaa.fanyiyou;

import com.tobyyaa.fanyiyou.Languages;

public class HistoryRecord {
    private static final String SEPARATOR = "@";
    public Languages.Language from;
    public String input;
    public String output;
    public Languages.Language to;
    public long when;

    public HistoryRecord(Languages.Language from2, Languages.Language to2, String input2, String output2, long when2) {
        this.from = from2;
        this.to = to2;
        this.input = input2;
        this.output = output2;
        this.when = when2;
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        try {
            HistoryRecord other = (HistoryRecord) o;
            return other.from.equals(this.from) && other.to.equals(this.to) && other.input.equals(this.input) && other.output.equals(this.output);
        } catch (Exception e) {
            return false;
        }
    }

    public int hashCode() {
        return ((this.from.hashCode() ^ this.to.hashCode()) ^ this.input.hashCode()) ^ this.output.hashCode();
    }

    public String encode() {
        return String.valueOf(this.from.name()) + SEPARATOR + this.to.name() + SEPARATOR + this.input + SEPARATOR + this.output + SEPARATOR + new Long(this.when);
    }

    public String toString() {
        return encode();
    }

    public static HistoryRecord decode(String s) {
        String[] o = s.split(SEPARATOR);
        int i = 0 + 1;
        int i2 = i + 1;
        int i3 = i2 + 1;
        int i4 = i3 + 1;
        int i5 = i4 + 1;
        return new HistoryRecord(Languages.Language.valueOf(o[0]), Languages.Language.valueOf(o[i]), o[i2], o[i3], Long.valueOf(o[i4]).longValue());
    }
}
