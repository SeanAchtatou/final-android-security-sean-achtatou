package com.tobyyaa.fanyiyou;

import android.app.AlertDialog;
import android.content.Context;
import android.webkit.WebView;
import com.adchina.android.ads.Common;

public class AboutDialog extends AlertDialog {
    protected AboutDialog(Context context) {
        super(context);
        setContentView((int) R.layout.about_dialog);
        setTitle((int) R.string.about_title);
        setCancelable(true);
        ((WebView) findViewById(R.id.webview)).loadData("Written by C&eacute;dric Beust (<a href=\"mailto:cedric@beust.com\">cedric@beust.com)</a>", "text/html", Common.KEnc);
    }
}
