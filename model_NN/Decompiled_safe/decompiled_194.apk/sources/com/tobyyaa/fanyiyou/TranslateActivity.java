package com.tobyyaa.fanyiyou;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.admogo.AdMogoLayout;
import com.mobclick.android.MobclickAgent;
import com.tobyyaa.fanyiyou.ITranslateForMe;
import com.tobyyaa.fanyiyou.Languages;
import java.util.ArrayList;

public class TranslateActivity extends Activity implements View.OnClickListener {
    private static final int ABOUT_DIALOG_ID = 2;
    private static final String DEFAULT_FROM = Languages.Language.ENGLISH.getShortName();
    private static final String DEFAULT_TO = Languages.Language.CHINESE.getShortName();
    private static final String FROM = "from";
    private static final String INPUT = "input";
    private static final int LANGUAGE_DIALOG_ID = 1;
    private static final String OUTPUT = "output";
    static final String TAG = "Translate";
    private static final String TO = "to";
    private static byte[] mWordBuffer;
    private static int mWordCount;
    private static ArrayList<Integer> mWordIndices;
    private View.OnClickListener mClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            TranslateActivity.this.mLatestButton = (Button) v;
            TranslateActivity.this.showDialog(1);
        }
    };
    private boolean mDoTranslate = true;
    /* access modifiers changed from: private */
    public Button mFromButton;
    /* access modifiers changed from: private */
    public EditText mFromEditText;
    private Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public Button mLatestButton;
    private Button mSwapButton;
    /* access modifiers changed from: private */
    public Button mToButton;
    private EditText mToEditText;
    /* access modifiers changed from: private */
    public Button mTranslateButton;
    private ServiceConnection mTranslateConn = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            TranslateActivity.this.mTranslateService = ITranslateForMe.Stub.asInterface(service);
            if (TranslateActivity.this.mTranslateService != null) {
                TranslateActivity.this.mTranslateButton.setEnabled(true);
                return;
            }
            TranslateActivity.this.mTranslateButton.setEnabled(false);
            TranslateActivity.log("Unable to acquire TranslateService");
        }

        public void onServiceDisconnected(ComponentName name) {
            TranslateActivity.this.mTranslateButton.setEnabled(false);
            TranslateActivity.this.mTranslateService = null;
        }
    };
    /* access modifiers changed from: private */
    public ITranslateForMe mTranslateService;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.translate_activity);
        MobclickAgent.setUpdateOnlyWifi(false);
        MobclickAgent.update(this);
        MobclickAgent.onError(this);
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.mFromEditText = (EditText) findViewById(R.id.input);
        this.mToEditText = (EditText) findViewById(R.id.translation);
        this.mFromButton = (Button) findViewById(R.id.from);
        this.mToButton = (Button) findViewById(R.id.to);
        this.mTranslateButton = (Button) findViewById(R.id.button_translate);
        this.mSwapButton = (Button) findViewById(R.id.button_swap);
        this.mFromButton.setOnClickListener(this.mClickListener);
        this.mToButton.setOnClickListener(this.mClickListener);
        this.mTranslateButton.setOnClickListener(this);
        this.mSwapButton.setOnClickListener(this);
        this.mFromEditText.selectAll();
        connectToTranslateService();
        this.mHandler.postDelayed(new Runnable() {
            public void run() {
                TranslateActivity.this.initads();
            }
        }, 500);
    }

    private void connectToTranslateService() {
        bindService(new Intent("android.intent.action.VIEW"), this.mTranslateConn, 1);
    }

    public void onResume() {
        super.onResume();
        SharedPreferences prefs = getPrefs(this);
        this.mDoTranslate = false;
        updateButton(this.mFromButton, Languages.Language.findLanguageByShortName(prefs.getString(FROM, DEFAULT_FROM)), false);
        updateButton(this.mToButton, Languages.Language.findLanguageByShortName(prefs.getString(TO, DEFAULT_TO)), true);
        this.mFromEditText.setText(prefs.getString(INPUT, ""));
        setOutputText(prefs.getString(OUTPUT, ""));
        this.mDoTranslate = true;
        MobclickAgent.onResume(this);
    }

    /* access modifiers changed from: private */
    public void setOutputText(String string) {
        log("Setting output to " + string);
        this.mToEditText.setText(new Entities().unescape(string));
    }

    private void updateButton(Button button, Languages.Language language, boolean translate) {
        language.configureButton(this, button);
        if (translate) {
            maybeTranslate();
        }
    }

    private void maybeTranslate() {
        if (this.mDoTranslate && !TextUtils.isEmpty(this.mFromEditText.getText().toString())) {
            doTranslate();
        }
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        savePreferences(getPrefs(this).edit(), ((Languages.Language) this.mFromButton.getTag()).getShortName(), ((Languages.Language) this.mToButton.getTag()).getShortName(), this.mFromEditText.getText().toString(), this.mToEditText.getText().toString());
    }

    static void savePreferences(SharedPreferences.Editor edit, String from, String to, String input, String output) {
        log("Saving preferences " + from + " " + to + " " + input + " " + output);
        edit.putString(FROM, from);
        edit.putString(TO, to);
        edit.putString(INPUT, input);
        edit.putString(OUTPUT, output);
        edit.commit();
    }

    static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(TAG, 0);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unbindService(this.mTranslateConn);
    }

    public void onClick(View v) {
        if (v == this.mTranslateButton) {
            maybeTranslate();
        } else if (v == this.mSwapButton) {
            showHistory();
        }
    }

    private void doTranslate() {
        this.mHandler.post(new Runnable() {
            public void run() {
                try {
                    Languages.Language from = (Languages.Language) TranslateActivity.this.mFromButton.getTag();
                    Languages.Language to = (Languages.Language) TranslateActivity.this.mToButton.getTag();
                    String fromShortName = from.getShortName();
                    String toShortName = to.getShortName();
                    String input = TranslateActivity.this.mFromEditText.getText().toString();
                    TranslateActivity.log("Translating from " + fromShortName + " to " + toShortName);
                    String result = TranslateActivity.this.mTranslateService.translate(input, fromShortName, toShortName);
                    if (result == null) {
                        throw new Exception(TranslateActivity.this.getString(R.string.translation_failed));
                    }
                    History.addHistoryRecord(TranslateActivity.this, from, to, input, result);
                    TranslateActivity.this.setOutputText(result);
                    TranslateActivity.this.mFromEditText.selectAll();
                } catch (Exception e) {
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog d) {
        if (id == 1) {
            ((LanguageDialog) d).setFrom(this.mLatestButton == this.mFromButton);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        if (id == 1) {
            return new LanguageDialog(this);
        }
        if (id != 2) {
            return null;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.about_title);
        builder.setMessage(getString(R.string.about_message));
        builder.setIcon((int) R.drawable.icon);
        builder.setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
        builder.setNeutralButton((int) R.string.send_email, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent("android.intent.action.SENDTO");
                intent.setData(Uri.parse("mailto:cedric@beust.com"));
                TranslateActivity.this.startActivity(intent);
            }
        });
        builder.setCancelable(true);
        return builder.create();
    }

    public void setNewLanguage(Languages.Language language, boolean from, boolean translate) {
        updateButton(from ? this.mFromButton : this.mToButton, language, translate);
    }

    private void showHistory() {
        startActivity(new Intent(this, HistoryActivity.class));
    }

    /* access modifiers changed from: private */
    public static void log(String s) {
        Log.d(TAG, "[TranslateActivity] " + s);
    }

    public void initads() {
        AdMogoLayout adWhirlLayout = new AdMogoLayout(this, "029fd518eb7243e6acf7f0fb358e9056");
        LinearLayout llLayout = (LinearLayout) findViewById(R.id.ad);
        adWhirlLayout.setMaxHeight((int) ((60.0f * getResources().getDisplayMetrics().density) + 0.5f));
        llLayout.addView(adWhirlLayout, new RelativeLayout.LayoutParams(-1, -2));
        llLayout.invalidate();
    }
}
