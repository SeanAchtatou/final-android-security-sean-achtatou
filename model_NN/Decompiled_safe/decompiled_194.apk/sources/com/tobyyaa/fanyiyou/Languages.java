package com.tobyyaa.fanyiyou;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.Button;
import java.util.Map;

public final class Languages {

    enum Language {
        ENGLISH("en", "English（英语）", R.drawable.us),
        FRENCH("fr", "French（法语）", R.drawable.fr),
        CHINESE("zh", "Chinese(中文)", R.drawable.f5cn),
        JAPANESE("ja", "Japanese（日语）", R.drawable.jp),
        RUSSIAN("ru", "Russian（俄语）", R.drawable.ru),
        GERMAN("de", "German（德语）", R.drawable.de),
        CHINESE_TRADITIONAL("zh-TW", "Chinese traditional(中文繁体)", R.drawable.tw),
        AFRIKAANS("af", "Afrikaans（南非荷兰语）", R.drawable.af),
        ALBANIAN("sq", "Albanian（阿尔巴尼亚人语）"),
        AMHARIC("am", "Amharic（阿姆哈拉语）", R.drawable.am),
        ARABIC("ar", "Arabic（阿拉伯语）", R.drawable.ar),
        ARMENIAN("hy", "Armenian（亚美尼亚语）"),
        AZERBAIJANI("az", "Azerbaijani（亚塞拜然语）", R.drawable.az),
        BASQUE("eu", "Basque（巴斯克语）"),
        BELARUSIAN("be", "Belarusian（白俄罗斯语）", R.drawable.be),
        BENGALI("bn", "Bengali（孟加拉语）", R.drawable.bn),
        BIHARI("bh", "Bihari（比哈里语）", R.drawable.bh),
        BULGARIAN("bg", "Bulgarian（保加利亚人语)", R.drawable.bg),
        BURMESE("my", "Burmese(缅甸语)", R.drawable.my),
        CATALAN("ca", "Catalan(加泰罗尼亚语)", R.drawable.ca1),
        CROATIAN("hr", "Croatian（克罗地亚人语）", R.drawable.hr),
        CZECH("cs", "Czech（捷克人语）", R.drawable.cs),
        DANISH("da", "Danish（丹麦语）", R.drawable.dk),
        DHIVEHI("dv", "Dhivehi（迪维希语）"),
        DUTCH("nl", "Dutch（荷兰语）", R.drawable.nl),
        ESPERANTO("eo", "Esperanto（世界语）"),
        ESTONIAN("et", "Estonian（爱沙尼亚语）", R.drawable.et),
        FILIPINO("tl", "Filipino（菲律宾语）", R.drawable.ph),
        FINNISH("fi", "Finnish（芬兰语）", R.drawable.fi),
        GALICIAN("gl", "Galician（加利西亚人语）", R.drawable.gl),
        GEORGIAN("ka", "Georgian（格鲁吉亚语）"),
        GREEK("el", "Greek（希腊语）", R.drawable.gr),
        GUARANI("gn", "Guarani（瓜拉尼语）", R.drawable.gn),
        GUJARATI("gu", "Gujarati（古吉拉特语）", R.drawable.gu),
        HEBREW("iw", "Hebrew（希伯来人语）", R.drawable.il),
        HINDI("hi", "Hindi（印地语）"),
        HUNGARIAN("hu", "Hungarian（匈牙利语 ）", R.drawable.hu),
        ICELANDIC("is", "Icelandic（冰岛语）", R.drawable.is),
        INDONESIAN("id", "Indonesian（印尼语）", R.drawable.id),
        INUKTITUT("iu", "Inuktitut（伊努伊特语）"),
        ITALIAN("it", "Italian（意大利语）", R.drawable.it),
        KANNADA("kn", "Kannada（埃纳德语）", R.drawable.kn),
        KAZAKH("kk", "Kazakh（哈萨克语）"),
        KHMER("km", "Khmer（高棉语）", R.drawable.km),
        KOREAN("ko", "Korean（朝鲜语）", R.drawable.kr),
        KURDISH("ky", "Kurdish（库尔德语）", R.drawable.ky),
        LAOTHIAN("lo", "Laothian（老挝语）"),
        LATVIAN("la", "Latvian（拉脱维亚语）", R.drawable.la),
        LITHUANIAN("lt", "Lithuanian（立陶宛语）", R.drawable.lt),
        MACEDONIAN("mk", "Macedonian（马其顿与语）", R.drawable.mk),
        MALAY("ms", "Malay（马来语）", R.drawable.ms),
        MALAYALAM("ml", "Malayalam（马拉雅拉姆语）", R.drawable.ml),
        MALTESE("mt", "Maltese（马耳他语）", R.drawable.mt),
        MARATHI("mr", "Marathi（马拉地语）", R.drawable.mr),
        MONGOLIAN("mn", "Mongolian（蒙古语）", R.drawable.mn),
        NEPALI("ne", "Nepali（尼泊尔语）", R.drawable.ne),
        NORWEGIAN("no", "Norwegian（挪威语）", R.drawable.no),
        ORIYA("or", "Oriya（奥里亚语）"),
        PASHTO("ps", "Pashto（普什图语）", R.drawable.ps),
        PERSIAN("fa", "Persian（波斯语）"),
        POLISH("pl", "Polish（波兰语）", R.drawable.pl),
        PORTUGUESE("pt", "Portuguese（葡萄牙语）", R.drawable.pt),
        PUNJABI("pa", "Punjabi（旁遮普语）", R.drawable.pa),
        ROMANIAN("ro", "Romanian（罗马尼亚语）", R.drawable.ro),
        SANSKRIT("sa", "Sanskrit（梵语）", R.drawable.sa),
        SERBIAN("sr", "Serbian（塞尔维亚人语）", R.drawable.sr),
        SINDHI("sd", "Sindhi（信德语）", R.drawable.sd),
        SINHALESE("si", "Sinhalese（僧伽罗人语）", R.drawable.si),
        SLOVAK("sk", "Slovak（斯洛伐克人语）", R.drawable.sk),
        SLOVENIAN("sl", "Slovenian（斯洛文尼亚人语）", R.drawable.sl),
        SPANISH("es", "Spanish（西班牙语）", R.drawable.es),
        SWAHILI("sw", "Swahili（斯瓦希里人语）"),
        SWEDISH("sv", "Swedish（瑞典语）", R.drawable.sv),
        TAJIK("tg", "Tajik（塔吉克文）", R.drawable.tg),
        TAMIL("ta", "Tamil（泰米尔语）"),
        TAGALOG("tl", "Tagalog（他加禄语）", R.drawable.ph),
        TELUGU("te", "Telugu（泰卢固语）"),
        THAI("th", "Thai（泰国语）", R.drawable.th),
        TIBETAN("bo", "Tibetan（藏语）", R.drawable.bo),
        TURKISH("tr", "Turkish（土耳其语）", R.drawable.tr),
        UKRAINIAN("uk", "Ukrainian（乌克兰人语）", R.drawable.ua),
        URDU("ur", "Urdu"),
        UZBEK("uz", "Uzbek（乌兹别克语）", R.drawable.uz),
        UIGHUR("ug", "Uighur（维吾尔族语）", R.drawable.ug);
        
        private static Map<String, String> mLongNameToShortName = Maps.newHashMap();
        private static Map<String, Language> mShortNameToLanguage = Maps.newHashMap();
        private int mFlag;
        private String mLongName;
        private String mShortName;

        static {
            for (Language language : values()) {
                mLongNameToShortName.put(language.getLongName(), language.getShortName());
                mShortNameToLanguage.put(language.getShortName(), language);
            }
        }

        private Language(String shortName, String longName, int flag) {
            init(shortName, longName, flag);
        }

        private Language(String shortName, String longName) {
            init(shortName, longName, -1);
        }

        private void init(String shortName, String longName, int flag) {
            this.mShortName = shortName;
            this.mLongName = longName;
            this.mFlag = flag;
        }

        public String getShortName() {
            return this.mShortName;
        }

        public String getLongName() {
            return this.mLongName;
        }

        public int getFlag() {
            return this.mFlag;
        }

        public String toString() {
            return this.mLongName;
        }

        public static Language findLanguageByShortName(String shortName) {
            return mShortNameToLanguage.get(shortName);
        }

        public void configureButton(Activity activity, Button button) {
            button.setTag(this);
            button.setText(getLongName());
            int f = getFlag();
            if (f != -1) {
                button.setCompoundDrawablesWithIntrinsicBounds(activity.getResources().getDrawable(f), (Drawable) null, (Drawable) null, (Drawable) null);
                button.setCompoundDrawablePadding(5);
            }
        }
    }

    public static String getShortName(String longName) {
        return (String) Language.access$2().get(longName);
    }

    private static void log(String s) {
        Log.d("Translate", "[Languages] " + s);
    }
}
