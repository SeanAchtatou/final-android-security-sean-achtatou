package com.tobyyaa.fanyiyou;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import java.util.List;
import java.util.Map;

public class HistoryActivity extends ListActivity implements AdapterView.OnItemClickListener {
    private static final String[] COLUMN_NAMES = {INPUT, OUTPUT, FROM, TO};
    private static final String FROM = "from";
    private static final String FROM_SHORT_NAME = "from-short-name";
    private static final String INPUT = "input";
    private static final String OUTPUT = "output";
    private static final String TO = "to";
    private static final String TO_SHORT_NAME = "to-short-name";
    private static final int[] VIEW_IDS = {R.id.input, R.id.output, R.id.from, R.id.to};
    private SimpleAdapter mAdapter;
    private History mHistory;
    private List<Map<String, String>> mListData;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.history_activity);
        this.mHistory = new History(TranslateActivity.getPrefs(this));
        initializeAdapter(this.mHistory.getHistoryRecordsMostRecentFirst());
        getListView().setEmptyView(findViewById(R.id.empty));
    }

    private void initializeAdapter(List<HistoryRecord> historyRecords) {
        this.mListData = Lists.newArrayList();
        for (HistoryRecord hr : historyRecords) {
            Map<String, String> data = Maps.newHashMap();
            data.put(INPUT, hr.input);
            data.put(OUTPUT, hr.output);
            data.put(FROM, hr.from.name().toLowerCase());
            data.put(TO, hr.to.name().toLowerCase());
            data.put(FROM_SHORT_NAME, hr.from.getShortName());
            data.put(TO_SHORT_NAME, hr.to.getShortName());
            this.mListData.add(data);
        }
        this.mAdapter = new SimpleAdapter(this, this.mListData, R.layout.history_record, COLUMN_NAMES, VIEW_IDS);
        getListView().setAdapter((ListAdapter) this.mAdapter);
        getListView().setOnItemClickListener(this);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.history_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clear_history /*2131165211*/:
                this.mHistory.clear(this);
                initializeAdapter(this.mHistory.getHistoryRecordsByLanguages());
                return true;
            default:
                return true;
        }
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Map<String, String> data = (Map) parent.getItemAtPosition(position);
        TranslateActivity.savePreferences(TranslateActivity.getPrefs(this).edit(), (String) data.get(FROM_SHORT_NAME), (String) data.get(TO_SHORT_NAME), (String) data.get(INPUT), (String) data.get(OUTPUT));
        finish();
    }
}
