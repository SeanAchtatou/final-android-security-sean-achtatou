package com.adchina.android.test;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;
import com.adchina.android.ads.AdEngine;
import com.adchina.android.ads.AdListener;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.Utils;
import com.adchina.android.ads.views.AdView;
import com.adchina.android.ads.views.FullScreenAdView;
import com.tobyyaa.fanyiyou.R;

public class AdChinaTest extends Activity implements AdListener {
    private AdView a;
    private AdView b;
    /* access modifiers changed from: private */
    public AbsoluteLayout c;
    /* access modifiers changed from: private */
    public FullScreenAdView d;
    /* access modifiers changed from: private */
    public ImageButton e;
    private Button f;

    private void a(String str) {
        Toast.makeText(this, str, 0).show();
    }

    public boolean OnRecvSms(AdView adView, String str) {
        return true;
    }

    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(bundle);
        getPackageName();
        String string = getResources().getString(2130968577);
        setContentView((int) R.layout.about_dialog);
        findViewById(R.string.button_swap);
        this.f = (Button) findViewById(R.string.error);
        this.a = (AdView) findViewById(R.string.retrieving_translation);
        this.a.setDefaultImage((int) R.drawable.al);
        this.a.setVisibility(8);
        this.a.getLayoutParams().height = Utils.dip2px(this, 64.0f);
        this.a.getLayoutParams().width = Utils.dip2px(this, 320.0f);
        this.a.setBackgroundColor(Color.argb(10, 255, 255, 255));
        this.b = (AdView) findViewById(R.string.found_translation);
        this.b.setDefaultImage((int) R.drawable.al);
        this.b.setVisibility(8);
        this.b.getLayoutParams().height = Utils.dip2px(this, 64.0f);
        this.b.getLayoutParams().width = Utils.dip2px(this, 320.0f);
        this.b.setBackgroundColor(Color.argb(10, 255, 255, 255));
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        AdManager.setResolution(String.valueOf(defaultDisplay.getWidth()) + "x" + defaultDisplay.getHeight());
        AdManager.setAppName(string);
        AdManager.setDebugMode(false);
        AdManager.setLogMode(true);
        AdManager.setAdspaceId("71997");
        AdManager.setVideoAdspaceId("70539");
        AdManager.setrCloseImg(R.drawable.ai);
        AdManager.setrLoadingImg(R.drawable.ag);
        AdEngine initAdEngine = AdEngine.initAdEngine(this);
        initAdEngine.setAdListener(this);
        initAdEngine.addBannerAdView(this.a);
        initAdEngine.addBannerAdView(this.b);
        initAdEngine.startVideoAd();
        initAdEngine.setVideoFinishEvent(new a(this));
        AdManager.setFullScreenAdspaceId(AdManager.DEFAULT_FULLSCREEN_ADSPACE_ID);
        this.c = (AbsoluteLayout) findViewById(R.string.app_name);
        this.d = (FullScreenAdView) findViewById(R.string.show_history);
        this.d.setImageResource(R.drawable.af);
        AdEngine.getAdEngine().setFullScreenAdView(this.d);
        this.e = (ImageButton) findViewById(R.string.about);
        Display defaultDisplay2 = getWindowManager().getDefaultDisplay();
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(30, 30);
        marginLayoutParams.topMargin = defaultDisplay2.getHeight() - 30;
        marginLayoutParams.leftMargin = defaultDisplay2.getWidth() - 30;
        this.e.setLayoutParams(new AbsoluteLayout.LayoutParams(30, 30, defaultDisplay2.getWidth() - 30, defaultDisplay2.getHeight() - 30));
        this.e.setOnTouchListener(new c(this));
        initAdEngine.startFullScreenAd();
        initAdEngine.startBannerAd();
        this.f.setOnClickListener(new b(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        AdEngine.getAdEngine().stopBannerAd();
        AdEngine.getAdEngine().stopFullScreenAd();
        AdEngine.getAdEngine().startVideoAd();
    }

    public void onFailedToPlayVideoAd() {
        a("onFailedToPlayVideoAd");
    }

    public void onFailedToReceiveAd(AdView adView) {
        a("onFailedToRecvAd");
    }

    public void onFailedToReceiveFullScreenAd(FullScreenAdView fullScreenAdView) {
        a("onFailedToReceiveFullScreenAd");
    }

    public void onFailedToReceiveVideoAd() {
        a("onFailedToReceiveVideoAd");
    }

    public void onFailedToRefreshAd(AdView adView) {
        a("onFailedToRefreshAd");
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        System.exit(0);
        return true;
    }

    public void onPlayVideoAd() {
        a("onPlayVideoAd");
    }

    public void onReceiveAd(AdView adView) {
        a("onRecvAd");
        this.a.setVisibility(0);
        this.b.setVisibility(0);
    }

    public void onReceiveFullScreenAd(FullScreenAdView fullScreenAdView) {
        a("onRecvFullScreenAd");
    }

    public void onRefreshAd(AdView adView) {
        a("onRefreshAd");
    }
}
