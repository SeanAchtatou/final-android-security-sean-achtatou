package com.adchina.android.test;

import android.view.MotionEvent;
import android.view.View;
import com.adchina.android.ads.AdEngine;

final class c implements View.OnTouchListener {
    private /* synthetic */ AdChinaTest a;

    c(AdChinaTest adChinaTest) {
        this.a = adChinaTest;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        this.a.c.removeView(this.a.d);
        this.a.c.removeView(this.a.e);
        AdEngine.getAdEngine().stopFullScreenAd();
        return true;
    }
}
