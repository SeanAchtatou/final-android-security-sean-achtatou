package com.adchina.android.test;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.adchina.android.ads.AdVideoFinishListener;

final class a implements AdVideoFinishListener {
    a(AdChinaTest adChinaTest) {
    }

    public final void finished(Context context, Object obj) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse(obj.toString()), "video/mp4");
        context.startActivity(intent);
    }
}
