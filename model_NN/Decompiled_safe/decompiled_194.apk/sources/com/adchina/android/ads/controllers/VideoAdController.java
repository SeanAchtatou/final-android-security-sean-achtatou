package com.adchina.android.ads.controllers;

import android.content.Context;
import android.os.Message;
import android.util.Log;
import com.adchina.android.ads.AdEngine;
import com.adchina.android.ads.AdListener;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.AdVideoFinishListener;
import com.adchina.android.ads.Common;
import com.adchina.android.ads.Utils;
import com.admogo.util.AdMogoUtil;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;

public class VideoAdController extends BaseController {
    private static VideoAdController g;
    private static /* synthetic */ int[] h;
    private static /* synthetic */ int[] i;
    private LinkedList a = new LinkedList();
    private LinkedList b = new LinkedList();
    private LinkedList c = new LinkedList();
    private HashMap d = new HashMap();
    private e e;
    private AdVideoFinishListener f;

    private VideoAdController(Context context) {
        super(context);
    }

    private static /* synthetic */ int[] a() {
        int[] iArr = h;
        if (iArr == null) {
            iArr = new int[AdEngine.ERecvAdStatus.values().length];
            try {
                iArr[AdEngine.ERecvAdStatus.EGetFullScreenImgMaterial.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.EGetImgMaterial.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.EIdle.ordinal()] = 13;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.EReceiveAd.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ERefreshAd.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendFullScreenImpTrack.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendFullScreenThdImpTrack.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendImpTrack.ordinal()] = 4;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendThdImpTrack.ordinal()] = 5;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendVideoEndedImpTrack.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendVideoEndedThdImpTrack.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendVideoStartedImpTrack.ordinal()] = 9;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendVideoStartedThdImpTrack.ordinal()] = 10;
            } catch (NoSuchFieldError e14) {
            }
            h = iArr;
        }
        return iArr;
    }

    private static /* synthetic */ int[] b() {
        int[] iArr = i;
        if (iArr == null) {
            iArr = new int[AdEngine.EClkTrack.values().length];
            try {
                iArr[AdEngine.EClkTrack.EIdle.ordinal()] = 11;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendBtnClkTrack.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendBtnThdClkTrack.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendClkTrack.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendFullScreenClkTrack.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendFullScreenThdClkTrack.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendThdClkTrack.ordinal()] = 2;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendVideoClkTrack.ordinal()] = 7;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendVideoCloseClkTrack.ordinal()] = 9;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendVideoCloseThdClkTrack.ordinal()] = 10;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendVideoThdClkTrack.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
            i = iArr;
        }
        return iArr;
    }

    public static VideoAdController getVideoAdController() {
        return g;
    }

    public static VideoAdController initVideoAdController(Context context) {
        if (g == null) {
            g = new VideoAdController(context);
        }
        return g;
    }

    public void deleteUnavailVideoFile() {
        File file = new File(Common.KVideosDir);
        if (file.exists()) {
            for (File file2 : file.listFiles()) {
                if (!this.d.containsValue(file2.getAbsolutePath())) {
                    file2.delete();
                }
            }
        }
    }

    public void fireVideoFinishEvent(Context context, Object obj) {
        if (this.f != null) {
            this.f.finished(context, obj);
        }
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 3:
                setRecvAdStatus(AdEngine.ERecvAdStatus.ERefreshAd);
                break;
            case 6:
                String[] strArr = (String[]) message.obj;
                if (!this.d.containsKey(strArr[0])) {
                    this.d.put(strArr[0], strArr[1]);
                    saveLocalVideoList();
                    break;
                }
                break;
            case 7:
                if (this.mAdListener != null) {
                    this.mAdListener.onPlayVideoAd();
                    break;
                }
                break;
            case 8:
                if (this.mAdListener != null) {
                    this.mAdListener.onFailedToPlayVideoAd();
                }
                this.mHasAd = false;
                this.e.a(true);
                setRecvAdStatus(AdEngine.ERecvAdStatus.ERefreshAd);
                break;
            case 9:
                setRecvAdStatus(AdEngine.ERecvAdStatus.ESendVideoEndedImpTrack);
                break;
            case 16:
                if (this.mAdListener != null) {
                    this.mAdListener.onFailedToReceiveVideoAd();
                }
                setRecvAdStatus(AdEngine.ERecvAdStatus.ERefreshAd);
                break;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0099 A[SYNTHETIC, Splitter:B:25:0x0099] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x009e A[Catch:{ IOException -> 0x00a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c4 A[SYNTHETIC, Splitter:B:35:0x00c4] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c9 A[Catch:{ IOException -> 0x00d0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadLocalVideoList() {
        /*
            r10 = this;
            r3 = 0
            r4 = 0
            com.adchina.android.ads.AdLog r0 = r10.logger
            java.lang.String r1 = "loadLocalVideoList"
            r0.writeLog(r1)
            android.content.Context r0 = r10.mContext     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            java.lang.String r1 = "adchinaVideos.fc"
            java.io.File r0 = r0.getFileStreamPath(r1)     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            long r0 = r0.length()     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            int r0 = (int) r0     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            char[] r0 = new char[r0]     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            android.content.Context r1 = r10.mContext     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            java.lang.String r2 = "adchinaVideos.fc"
            java.io.FileInputStream r1 = r1.openFileInput(r2)     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0110, all -> 0x0105 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0110, all -> 0x0105 }
            r2.read(r0)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.util.HashMap r0 = r10.d     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r0.clear()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r0 = "\n"
            java.lang.String[] r0 = r3.split(r0)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            int r3 = r0.length     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
        L_0x0039:
            if (r4 < r3) goto L_0x0047
            r2.close()     // Catch:{ IOException -> 0x00ea }
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ IOException -> 0x00ea }
        L_0x0043:
            r10.deleteUnavailVideoFile()     // Catch:{ IOException -> 0x00ea }
        L_0x0046:
            return
        L_0x0047:
            r5 = r0[r4]     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r6 = "|||"
            int r6 = r5.indexOf(r6)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r7 = 0
            java.lang.String r7 = r5.substring(r7, r6)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r8 = "|||"
            int r8 = r8.length()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            int r6 = r6 + r8
            java.lang.String r5 = r5.substring(r6)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r6.<init>(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            boolean r8 = r6.exists()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            if (r8 == 0) goto L_0x0079
            java.lang.String r6 = r6.getName()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            boolean r6 = com.adchina.android.ads.Utils.isCachedFileTimeout(r6)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            if (r6 != 0) goto L_0x0079
            java.util.HashMap r6 = r10.d     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r6.put(r7, r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
        L_0x0079:
            int r4 = r4 + 1
            goto L_0x0039
        L_0x007c:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x007f:
            com.adchina.android.ads.AdLog r3 = r10.logger     // Catch:{ all -> 0x010e }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x010e }
            java.lang.String r5 = "Exceptions in loadLocalVideoList 1, err = "
            r4.<init>(r5)     // Catch:{ all -> 0x010e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x010e }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x010e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x010e }
            r3.writeLog(r0)     // Catch:{ all -> 0x010e }
            if (r1 == 0) goto L_0x009c
            r1.close()     // Catch:{ IOException -> 0x00a5 }
        L_0x009c:
            if (r2 == 0) goto L_0x00a1
            r2.close()     // Catch:{ IOException -> 0x00a5 }
        L_0x00a1:
            r10.deleteUnavailVideoFile()     // Catch:{ IOException -> 0x00a5 }
            goto L_0x0046
        L_0x00a5:
            r0 = move-exception
            com.adchina.android.ads.AdLog r1 = r10.logger
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Exceptions in loadLocalVideoList 2, err = "
            r2.<init>(r3)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.writeLog(r0)
            goto L_0x0046
        L_0x00bf:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x00c2:
            if (r1 == 0) goto L_0x00c7
            r1.close()     // Catch:{ IOException -> 0x00d0 }
        L_0x00c7:
            if (r2 == 0) goto L_0x00cc
            r2.close()     // Catch:{ IOException -> 0x00d0 }
        L_0x00cc:
            r10.deleteUnavailVideoFile()     // Catch:{ IOException -> 0x00d0 }
        L_0x00cf:
            throw r0
        L_0x00d0:
            r1 = move-exception
            com.adchina.android.ads.AdLog r2 = r10.logger
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Exceptions in loadLocalVideoList 2, err = "
            r3.<init>(r4)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.writeLog(r1)
            goto L_0x00cf
        L_0x00ea:
            r0 = move-exception
            com.adchina.android.ads.AdLog r1 = r10.logger
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Exceptions in loadLocalVideoList 2, err = "
            r2.<init>(r3)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.writeLog(r0)
            goto L_0x0046
        L_0x0105:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x00c2
        L_0x0109:
            r0 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x00c2
        L_0x010e:
            r0 = move-exception
            goto L_0x00c2
        L_0x0110:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x007f
        L_0x0115:
            r0 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.controllers.VideoAdController.loadLocalVideoList():void");
    }

    public void onClickVideoAd() {
        this.logger.writeLog("++ onVideoClick");
        this.logger.writeLog("++ open " + this.mAdClickUrl + " with browser");
        AdEngine.getAdEngine().openBrowser(this.mAdClickUrl.toString());
        this.logger.writeLog("-- open browser");
        setClkTrackStatus(AdEngine.EClkTrack.ESendVideoClkTrack);
        this.logger.writeLog("-- onVideoClick");
    }

    public void onClosedVideo() {
        this.logger.writeLog("++ onClosedVideo");
        setClkTrackStatus(AdEngine.EClkTrack.ESendVideoCloseClkTrack);
        this.logger.writeLog("-- onClosedVideo");
    }

    public void onDisplayAd() {
        this.logger.writeLog("++ onDisplayAd, mAdModel = " + this.mAdModel);
        sendMessage(7, "PlayVideo");
        setRecvAdStatus(AdEngine.ERecvAdStatus.ESendVideoStartedImpTrack);
        this.logger.writeLog("-- onDisplayAd");
    }

    public void onPlayEnded() {
        sendMessage(9, "VideoPlayEnded");
    }

    public void onPlayError() {
        sendMessage(8, "FailedToPlayVideo");
    }

    public void onSendVideoEndedImpTrack() {
        String str;
        this.logger.writeLog("++ onSendVideoEndedImpTrack");
        try {
            String str2 = String.valueOf(this.mReportBaseUrl.toString()) + Common.KVEIMP + ",0,0,0" + setupMaParams() + "&tm=" + Utils.getNowTime("yyyyMMddHHmmss");
            this.mHttpEngine.requestSend(str2);
            this.logger.writeLog("send VideoEndedImpTrack to " + str2);
        } catch (Exception e2) {
            String str3 = "Exceptions in onSendVideoEndedImpTrack, err = " + e2.toString();
            this.logger.writeLog(str3);
            Log.e(Common.KLogTag, str3);
        } finally {
            setRecvAdStatus(AdEngine.ERecvAdStatus.ESendVideoEndedThdImpTrack);
            str = "-- onSendVideoEndedImpTrack";
            this.logger.writeLog(str);
        }
    }

    public void onSendVideoEndedThdImpTrack() {
        setRecvAdStatus(AdEngine.ERecvAdStatus.EIdle);
        int size = this.b.size();
        this.logger.writeLog("++ onSendVideoEndedThdImpTrack, Size of list is " + size);
        if (size > 0) {
            try {
                String str = (String) this.b.get(size - 1);
                this.b.removeLast();
                this.mHttpEngine.requestSend(str);
                this.logger.writeLog("send VideoEndedThdImpTrack to " + str);
            } catch (Exception e2) {
                String str2 = "Failed to onSendVideoEndedThdImpTrack, err = " + e2.toString();
                this.logger.writeLog(str2);
                Log.e(Common.KLogTag, str2);
            } finally {
                setRecvAdStatus(AdEngine.ERecvAdStatus.ESendVideoEndedThdImpTrack);
            }
        } else {
            sendMessage(3, "RefreshAd");
        }
        if (size == 0) {
            this.e.a(true);
        }
        this.logger.writeLog("-- onSendVideoEndedThdImpTrack");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0034, code lost:
        if (new java.io.File(r0).exists() != false) goto L_0x0036;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void playVideo(java.lang.String r5) {
        /*
            r4 = this;
            r3 = 1
            com.adchina.android.ads.controllers.e r0 = r4.e
            if (r0 == 0) goto L_0x0046
            com.adchina.android.ads.controllers.e r0 = r4.e
            java.lang.StringBuffer r0 = r0.a()
            int r0 = r0.length()
            if (r0 <= 0) goto L_0x0046
            com.adchina.android.ads.controllers.e r0 = r4.e
            java.lang.StringBuffer r0 = r0.a()
            java.lang.String r1 = r0.toString()
            java.util.HashMap r0 = r4.d
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x0044
            java.util.HashMap r0 = r4.d
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            java.io.File r2 = new java.io.File
            r2.<init>(r0)
            boolean r2 = r2.exists()
            if (r2 == 0) goto L_0x0044
        L_0x0036:
            if (r0 == 0) goto L_0x003e
            int r1 = r0.length()
            if (r1 != 0) goto L_0x0049
        L_0x003e:
            android.content.Context r0 = r4.mContext
            r4.fireVideoFinishEvent(r0, r5)
        L_0x0043:
            return
        L_0x0044:
            r0 = r1
            goto L_0x0036
        L_0x0046:
            java.lang.String r0 = ""
            goto L_0x0036
        L_0x0049:
            r4.mRefreshAd = r3
            r4.mHasAd = r3
            android.content.Intent r1 = new android.content.Intent
            r1.<init>()
            java.lang.String r2 = "ADUrl"
            r1.putExtra(r2, r0)
            java.lang.String r0 = "Url"
            r1.putExtra(r0, r5)
            android.content.Context r0 = r4.mContext
            java.lang.Class<com.adchina.android.ads.views.AdVideoPlayerActivity> r2 = com.adchina.android.ads.views.AdVideoPlayerActivity.class
            r1.setClass(r0, r2)
            android.content.Context r0 = r4.mContext
            r0.startActivity(r1)
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.controllers.VideoAdController.playVideo(java.lang.String):void");
    }

    /* access modifiers changed from: protected */
    public String readFcParams() {
        return readFcParams(Common.KFSFcFilename);
    }

    /* access modifiers changed from: protected */
    public void resetAdserverInfo() {
        super.resetAdserverInfo();
        this.a.clear();
        this.b.clear();
    }

    public void runClkTrackJob() {
        String str;
        String str2;
        String str3;
        String str4;
        while (!this.mStop) {
            switch (b()[this.mClickStatus.ordinal()]) {
                case 1:
                    this.logger.writeLog("++ onSendClkTrack");
                    setClkTrackStatus(AdEngine.EClkTrack.EIdle);
                    try {
                        String str5 = String.valueOf(this.mReportBaseUrl.toString()) + Common.KCLK + ",0,0,0" + setupMaParams();
                        this.mHttpEngine.requestSend(str5);
                        this.logger.writeLog("send ClkTrack to " + str5);
                        break;
                    } catch (Exception e2) {
                        String str6 = "Exceptions in onSendClkTrack, err = " + e2.toString();
                        this.logger.writeLog(str6);
                        Log.e(Common.KLogTag, str6);
                        break;
                    } finally {
                        setClkTrackStatus(AdEngine.EClkTrack.ESendThdClkTrack);
                        str4 = "-- onSendClkTrack";
                        this.logger.writeLog(str4);
                    }
                case 2:
                    setClkTrackStatus(AdEngine.EClkTrack.EIdle);
                    int size = this.mThdClkTrackList.size();
                    this.logger.writeLog("++ onSendThdClkTrack, Size of list is " + size);
                    if (size > 0) {
                        try {
                            String str7 = (String) this.mThdClkTrackList.get(size - 1);
                            this.mThdClkTrackList.removeLast();
                            this.mHttpEngine.requestSend(str7);
                            this.logger.writeLog("send ThdClkTrack to " + str7);
                            break;
                        } catch (Exception e3) {
                            String str8 = "Failed to onSendThdClkTrack, err = " + e3.toString();
                            this.logger.writeLog(str8);
                            Log.e(Common.KLogTag, str8);
                            break;
                        } finally {
                            setClkTrackStatus(AdEngine.EClkTrack.ESendThdClkTrack);
                            str3 = "-- onSendThdClkTrack";
                            this.logger.writeLog(str3);
                        }
                    }
                    break;
                case 9:
                    this.logger.writeLog("++ onSendVideoCloseClkTrack");
                    setClkTrackStatus(AdEngine.EClkTrack.EIdle);
                    try {
                        String str9 = String.valueOf(this.mReportBaseUrl.toString()) + Common.KVCIMP + ",0,0,0" + setupMaParams();
                        this.mHttpEngine.requestSend(str9);
                        this.logger.writeLog("send VideoCloseClkTrack to " + str9);
                        break;
                    } catch (Exception e4) {
                        String str10 = "Exceptions in onSendVideoCloseClkTrack, err = " + e4.toString();
                        this.logger.writeLog(str10);
                        Log.e(Common.KLogTag, str10);
                        break;
                    } finally {
                        setClkTrackStatus(AdEngine.EClkTrack.ESendVideoCloseThdClkTrack);
                        str2 = "-- onSendVideoCloseClkTrack";
                        this.logger.writeLog(str2);
                    }
                case AdMogoUtil.NETWORK_TYPE_ADMOGO /*10*/:
                    setClkTrackStatus(AdEngine.EClkTrack.EIdle);
                    int size2 = this.c.size();
                    this.logger.writeLog("++ onSendVideoCloseThdClkTrack, Size of list is " + size2);
                    if (size2 > 0) {
                        try {
                            String str11 = (String) this.c.get(size2 - 1);
                            this.c.removeLast();
                            this.mHttpEngine.requestSend(str11);
                            this.logger.writeLog("send VideoCloseThdClkTrack to " + str11);
                        } catch (Exception e5) {
                            String str12 = "Failed to SendVideoCloseThdClkTrack, err = " + e5.toString();
                            this.logger.writeLog(str12);
                            Log.e(Common.KLogTag, str12);
                        } finally {
                            setClkTrackStatus(AdEngine.EClkTrack.ESendVideoCloseThdClkTrack);
                            str = "-- onSendVideoCloseThdClkTrack";
                            this.logger.writeLog(str);
                        }
                    }
                    if (size2 == 0) {
                        this.e.a(true);
                        break;
                    }
                    break;
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e6) {
            }
        }
    }

    public void runRecvAdThreadJob() {
        String str;
        String str2;
        String str3;
        String str4;
        InputStream inputStream;
        Exception exc;
        Throwable th;
        while (!this.mStop) {
            switch (a()[this.mRunState.ordinal()]) {
                case 1:
                    if (this.e == null || this.e.b() || this.e.a().length() <= 0) {
                        this.logger.writeLog("++ onReceiveAd");
                        setRecvAdStatus(AdEngine.ERecvAdStatus.EIdle);
                        InputStream inputStream2 = null;
                        try {
                            if (AdManager.getDebugMode()) {
                                String str5 = setupAdserverUrlFromConfig();
                                if (str5 != null) {
                                    this.logger.writeLog("AdserverUrl:" + str5);
                                    inputStream2 = this.mHttpEngine.requestGet(str5, this.mAdspaceId.toString());
                                }
                            } else {
                                String str6 = setupAdserverUrl();
                                this.logger.writeLog("AdserverUrl:" + str6);
                                inputStream2 = this.mHttpEngine.requestGet(str6, this.mAdspaceId.toString());
                            }
                            try {
                                readAdserverInfo(inputStream2);
                                if (this.mVideoAddr.length() > 0) {
                                    this.e = new e(this);
                                    this.e.a(this.mVideoAddr);
                                    loadLocalVideoList();
                                    if (!this.d.containsKey(this.mVideoAddr.toString())) {
                                        String upperCase = Utils.getNetworkTypes(this.mContext).toUpperCase();
                                        if (upperCase.contains("WIFI") || upperCase.contains("UMTS")) {
                                            new d(this, this.e.a().toString()).start();
                                        }
                                    }
                                    Utils.splitTrackUrl(this.mThdClkTrack.toString(), this.mThdClkTrackList);
                                    Utils.splitTrackUrl(this.mVideoStartedTrack.toString(), this.a);
                                    Utils.splitTrackUrl(this.mVideoEndedTrack.toString(), this.b);
                                    Utils.splitTrackUrl(this.mVideoClosedTrack.toString(), this.c);
                                } else {
                                    setRecvAdStatus(AdEngine.ERecvAdStatus.ERefreshAd);
                                }
                                this.mHttpEngine.closeStream(inputStream2);
                                this.logger.writeLog("-- onReceiveAd");
                                break;
                            } catch (Exception e2) {
                                Exception exc2 = e2;
                                inputStream = inputStream2;
                                exc = exc2;
                                break;
                            } catch (Throwable th2) {
                                Throwable th3 = th2;
                                inputStream = inputStream2;
                                th = th3;
                                this.mHttpEngine.closeStream(inputStream);
                                this.logger.writeLog("-- onReceiveAd");
                                throw th;
                            }
                        } catch (Exception e3) {
                            Exception exc3 = e3;
                            inputStream = null;
                            exc = exc3;
                            break;
                        } catch (Throwable th4) {
                            Throwable th5 = th4;
                            inputStream = null;
                            th = th5;
                            this.mHttpEngine.closeStream(inputStream);
                            this.logger.writeLog("-- onReceiveAd");
                            throw th;
                        }
                    }
                case 6:
                    this.logger.writeLog("++ onRefreshAd");
                    try {
                        int intValue = Integer.valueOf(this.mRefTime.toString()).intValue();
                        try {
                            Thread.sleep((long) (intValue * com.energysource.szj.embeded.AdManager.AD_FILL_PARENT));
                            break;
                        } catch (Exception e4) {
                            break;
                        } finally {
                            setRecvAdStatus(AdEngine.ERecvAdStatus.EReceiveAd);
                            str3 = "-- onRefreshAd, delay = ";
                            this.logger.writeLog(str3 + intValue);
                        }
                    } catch (Exception e5) {
                        try {
                            Thread.sleep(15000);
                            break;
                        } catch (Exception e6) {
                            break;
                        } finally {
                            setRecvAdStatus(AdEngine.ERecvAdStatus.EReceiveAd);
                            str2 = "-- onRefreshAd, delay = ";
                            this.logger.writeLog(str2 + 15);
                        }
                    } catch (Throwable th6) {
                        try {
                            Thread.sleep((long) (15 * com.energysource.szj.embeded.AdManager.AD_FILL_PARENT));
                        } catch (Exception e7) {
                        } finally {
                            setRecvAdStatus(AdEngine.ERecvAdStatus.EReceiveAd);
                            str = "-- onRefreshAd, delay = ";
                            this.logger.writeLog(str + 15);
                        }
                        throw th6;
                    }
                case 9:
                    this.logger.writeLog("++ onSendVideoStartedImpTrack");
                    try {
                        String str7 = String.valueOf(this.mReportBaseUrl.toString()) + Common.KVSIMP + ",0,0,0" + setupMaParams() + "&tm=" + Utils.getNowTime("yyyyMMddHHmmss");
                        this.mHttpEngine.requestSend(str7);
                        this.logger.writeLog("send VideoStartedImpTrack to " + str7);
                        break;
                    } catch (Exception e8) {
                        String str8 = "Exceptions in onSendVideoStartedImpTrack, err = " + e8.toString();
                        this.logger.writeLog(str8);
                        Log.e(Common.KLogTag, str8);
                        break;
                    } finally {
                        setRecvAdStatus(AdEngine.ERecvAdStatus.ESendVideoStartedThdImpTrack);
                        str4 = "-- onSendVideoStartedImpTrack";
                        this.logger.writeLog(str4);
                    }
                case AdMogoUtil.NETWORK_TYPE_ADMOGO /*10*/:
                    setRecvAdStatus(AdEngine.ERecvAdStatus.EIdle);
                    int size = this.a.size();
                    this.logger.writeLog("++ onSendVideoStartedThdImpTrack, Size of list is " + size);
                    if (size > 0) {
                        try {
                            String str9 = (String) this.a.get(size - 1);
                            this.a.removeLast();
                            this.mHttpEngine.requestSend(str9);
                            this.logger.writeLog("send VideoStartedThdImpTrack to " + str9);
                        } catch (Exception e9) {
                            String str10 = "Failed to onSendVideoStartedThdImpTrack, err = " + e9.toString();
                            this.logger.writeLog(str10);
                            Log.e(Common.KLogTag, str10);
                        } finally {
                            setRecvAdStatus(AdEngine.ERecvAdStatus.ESendVideoStartedThdImpTrack);
                        }
                    } else {
                        sendMessage(3, "RefreshAd");
                    }
                    this.logger.writeLog("-- onSendVideoStartedThdImpTrack");
                    break;
                case AdMogoUtil.NETWORK_TYPE_MOBCLIX /*11*/:
                    onSendVideoEndedImpTrack();
                    break;
                case AdMogoUtil.NETWORK_TYPE_MDOTM /*12*/:
                    onSendVideoEndedThdImpTrack();
                    break;
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e10) {
            }
        }
        return;
        try {
            String str11 = "Exceptions in onReceiveAd, err = " + exc.toString();
            sendMessage(16, str11);
            this.logger.writeLog(str11);
            Log.e(Common.KLogTag, str11);
            this.mHttpEngine.closeStream(inputStream);
            this.logger.writeLog("-- onReceiveAd");
            Thread.sleep(50);
        } catch (Throwable th7) {
            th = th7;
            this.mHttpEngine.closeStream(inputStream);
            this.logger.writeLog("-- onReceiveAd");
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x007f A[SYNTHETIC, Splitter:B:29:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0084 A[Catch:{ IOException -> 0x008a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void saveLocalVideoList() {
        /*
            r7 = this;
            r3 = 0
            com.adchina.android.ads.AdLog r0 = r7.logger
            java.lang.String r1 = "saveLocalVideoList"
            r0.writeLog(r1)
            android.content.Context r0 = r7.mContext     // Catch:{ Exception -> 0x0097, all -> 0x007a }
            java.lang.String r1 = "adchinaVideos.fc"
            r2 = 0
            java.io.FileOutputStream r1 = r0.openFileOutput(r1, r2)     // Catch:{ Exception -> 0x0097, all -> 0x007a }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x009b, all -> 0x008c }
            r2.<init>(r1)     // Catch:{ Exception -> 0x009b, all -> 0x008c }
            java.util.HashMap r0 = r7.d     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.Set r0 = r0.keySet()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
        L_0x0020:
            boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            if (r0 != 0) goto L_0x0032
            r2.flush()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.close()     // Catch:{ IOException -> 0x0088 }
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x0088 }
        L_0x0031:
            return
        L_0x0032:
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r4 = "|||"
            r2.write(r4)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.HashMap r4 = r7.d     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = "\n"
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            goto L_0x0020
        L_0x0051:
            r0 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x0055:
            com.adchina.android.ads.AdLog r3 = r7.logger     // Catch:{ all -> 0x0095 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0095 }
            java.lang.String r5 = "Exceptions in saveLocalVideoList, err = "
            r4.<init>(r5)     // Catch:{ all -> 0x0095 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0095 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0095 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0095 }
            r3.writeLog(r0)     // Catch:{ all -> 0x0095 }
            if (r1 == 0) goto L_0x0072
            r1.close()     // Catch:{ IOException -> 0x0078 }
        L_0x0072:
            if (r2 == 0) goto L_0x0031
            r2.close()     // Catch:{ IOException -> 0x0078 }
            goto L_0x0031
        L_0x0078:
            r0 = move-exception
            goto L_0x0031
        L_0x007a:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x007d:
            if (r1 == 0) goto L_0x0082
            r1.close()     // Catch:{ IOException -> 0x008a }
        L_0x0082:
            if (r2 == 0) goto L_0x0087
            r2.close()     // Catch:{ IOException -> 0x008a }
        L_0x0087:
            throw r0
        L_0x0088:
            r0 = move-exception
            goto L_0x0031
        L_0x008a:
            r1 = move-exception
            goto L_0x0087
        L_0x008c:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x007d
        L_0x0090:
            r0 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x007d
        L_0x0095:
            r0 = move-exception
            goto L_0x007d
        L_0x0097:
            r0 = move-exception
            r1 = r3
            r2 = r3
            goto L_0x0055
        L_0x009b:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.controllers.VideoAdController.saveLocalVideoList():void");
    }

    public void setAdListener(AdListener adListener) {
        this.mAdListener = adListener;
    }

    public void setVideoFinishEvent(AdVideoFinishListener adVideoFinishListener) {
        this.f = adVideoFinishListener;
    }

    /* access modifiers changed from: protected */
    public void setupAdspaceId() {
        this.mAdspaceId.setLength(0);
        this.mAdspaceId.append(AdManager.getVideoAdspaceId());
    }

    /* access modifiers changed from: protected */
    public void writeFcParams(String str) {
        writeFcParams(str, Common.KFSFcFilename);
    }
}
