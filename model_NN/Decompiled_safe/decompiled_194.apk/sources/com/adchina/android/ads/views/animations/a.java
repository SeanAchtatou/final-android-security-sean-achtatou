package com.adchina.android.ads.views.animations;

import android.view.animation.Animation;
import com.adchina.android.ads.views.ContentView;

final class a implements Animation.AnimationListener {
    private final /* synthetic */ ContentView a;
    private final /* synthetic */ float b;
    private final /* synthetic */ float c;

    a(ContentView contentView, float f, float f2) {
        this.a = contentView;
        this.b = f;
        this.c = f2;
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.post(new b(this, this.b, this.c, this.a));
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
