package com.adchina.android.ads.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.adchina.android.ads.controllers.FullScreenAdController;

public class FullScreenAdView extends ImageView implements View.OnClickListener {
    private FullScreenAdController a;

    public FullScreenAdView(Context context) {
        super(context);
        initFullScreenAd();
    }

    public FullScreenAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initFullScreenAd();
    }

    public FullScreenAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initFullScreenAd();
    }

    public void initFullScreenAd() {
        setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        setScaleType(ImageView.ScaleType.FIT_XY);
        setOnClickListener(this);
    }

    public void onClick(View view) {
        if (this.a != null) {
            this.a.onClickFullScreenAd();
        }
    }

    public void setController(FullScreenAdController fullScreenAdController) {
        this.a = fullScreenAdController;
    }
}
