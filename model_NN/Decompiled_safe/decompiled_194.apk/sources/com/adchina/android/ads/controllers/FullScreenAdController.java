package com.adchina.android.ads.controllers;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Message;
import android.util.Log;
import com.adchina.android.ads.AdEngine;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.Common;
import com.adchina.android.ads.Utils;
import com.adchina.android.ads.views.FullScreenAdView;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Timer;

public class FullScreenAdController extends BaseController {
    private static /* synthetic */ int[] g;
    private static /* synthetic */ int[] h;
    private LinkedList a = new LinkedList();
    private LinkedList b = new LinkedList();
    private HashMap c = new HashMap();
    private FullScreenAdView d;
    /* access modifiers changed from: private */
    public AdViewController e;
    /* access modifiers changed from: private */
    public String f;

    public FullScreenAdController(Context context) {
        super(context);
    }

    private static /* synthetic */ int[] a() {
        int[] iArr = g;
        if (iArr == null) {
            iArr = new int[AdEngine.ERecvAdStatus.values().length];
            try {
                iArr[AdEngine.ERecvAdStatus.EGetFullScreenImgMaterial.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.EGetImgMaterial.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.EIdle.ordinal()] = 13;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.EReceiveAd.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ERefreshAd.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendFullScreenImpTrack.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendFullScreenThdImpTrack.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendImpTrack.ordinal()] = 4;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendThdImpTrack.ordinal()] = 5;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendVideoEndedImpTrack.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendVideoEndedThdImpTrack.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendVideoStartedImpTrack.ordinal()] = 9;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendVideoStartedThdImpTrack.ordinal()] = 10;
            } catch (NoSuchFieldError e14) {
            }
            g = iArr;
        }
        return iArr;
    }

    private static /* synthetic */ int[] b() {
        int[] iArr = h;
        if (iArr == null) {
            iArr = new int[AdEngine.EClkTrack.values().length];
            try {
                iArr[AdEngine.EClkTrack.EIdle.ordinal()] = 11;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendBtnClkTrack.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendBtnThdClkTrack.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendClkTrack.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendFullScreenClkTrack.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendFullScreenThdClkTrack.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendThdClkTrack.ordinal()] = 2;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendVideoClkTrack.ordinal()] = 7;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendVideoCloseClkTrack.ordinal()] = 9;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendVideoCloseThdClkTrack.ordinal()] = 10;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendVideoThdClkTrack.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
            h = iArr;
        }
        return iArr;
    }

    public void deleteUnavailImgsFile() {
        File file = new File(Common.KFSImgsDir);
        if (file.exists()) {
            for (File file2 : file.listFiles()) {
                if (!this.c.containsValue(file2.getAbsolutePath())) {
                    file2.delete();
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00d6, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00d7, code lost:
        r2 = r0;
        r0 = r1;
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00ec, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00ed, code lost:
        r2 = r0;
        r0 = r1;
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        return r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0097 A[Catch:{ Exception -> 0x00f1, all -> 0x00e0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00bf A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d6 A[ExcHandler: all (r1v11 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:6:0x002c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.InputStream downLoadAndSaveImg(java.lang.String r9) {
        /*
            r8 = this;
            r5 = 0
            r0 = 0
            com.adchina.android.ads.AdLog r1 = r8.logger     // Catch:{ Exception -> 0x009c, all -> 0x00c8 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009c, all -> 0x00c8 }
            java.lang.String r3 = "++ start to download Img file"
            r2.<init>(r3)     // Catch:{ Exception -> 0x009c, all -> 0x00c8 }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x009c, all -> 0x00c8 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x009c, all -> 0x00c8 }
            r1.writeLog(r2)     // Catch:{ Exception -> 0x009c, all -> 0x00c8 }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x009c, all -> 0x00c8 }
            java.lang.String r2 = "/sdcard/ad/fsImg"
            r1.<init>(r2)     // Catch:{ Exception -> 0x009c, all -> 0x00c8 }
            boolean r2 = r1.exists()     // Catch:{ Exception -> 0x009c, all -> 0x00c8 }
            if (r2 != 0) goto L_0x0026
            r1.mkdirs()     // Catch:{ Exception -> 0x009c, all -> 0x00c8 }
        L_0x0026:
            com.adchina.android.ads.HttpEngine r2 = r8.mHttpEngine     // Catch:{ Exception -> 0x009c, all -> 0x00c8 }
            java.io.InputStream r0 = r2.requestGet(r9)     // Catch:{ Exception -> 0x009c, all -> 0x00c8 }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x00ec, all -> 0x00d6 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ec, all -> 0x00d6 }
            java.lang.String r4 = "yyyyMMddHHmmss"
            java.lang.String r4 = com.adchina.android.ads.Utils.getNowTime(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x00d6 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x00d6 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x00d6 }
            java.lang.String r4 = "fsImg.tmp"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x00d6 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00ec, all -> 0x00d6 }
            r2.<init>(r1, r3)     // Catch:{ Exception -> 0x00ec, all -> 0x00d6 }
            boolean r1 = r2.exists()     // Catch:{ Exception -> 0x00ec, all -> 0x00d6 }
            if (r1 == 0) goto L_0x0053
            r2.delete()     // Catch:{ Exception -> 0x00ec, all -> 0x00d6 }
        L_0x0053:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x008c, all -> 0x00d6 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x008c, all -> 0x00d6 }
            r3 = 16384(0x4000, float:2.2959E-41)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x008c, all -> 0x00d6 }
            r4 = r5
        L_0x005d:
            int r5 = r0.read(r3)     // Catch:{ Exception -> 0x00fb, all -> 0x00db }
            if (r5 <= 0) goto L_0x0069
            r6 = 0
            r1.write(r3, r6, r5)     // Catch:{ Exception -> 0x00fb, all -> 0x00db }
            r4 = 1
            goto L_0x005d
        L_0x0069:
            com.adchina.android.ads.HttpEngine r3 = r8.mHttpEngine     // Catch:{ Exception -> 0x00fb, all -> 0x00db }
            r3.closeStream(r0)     // Catch:{ Exception -> 0x00fb, all -> 0x00db }
            r1.flush()     // Catch:{ Exception -> 0x00fb, all -> 0x00db }
            r1.close()     // Catch:{ Exception -> 0x00fb, all -> 0x00db }
            r1 = r4
        L_0x0075:
            java.util.HashMap r3 = r8.c     // Catch:{ Exception -> 0x00f6, all -> 0x00e5 }
            java.lang.String r4 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x00f6, all -> 0x00e5 }
            r3.put(r9, r4)     // Catch:{ Exception -> 0x00f6, all -> 0x00e5 }
            r8.saveLocalImgList()     // Catch:{ Exception -> 0x00f6, all -> 0x00e5 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00f6, all -> 0x00e5 }
            java.lang.String r2 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x00f6, all -> 0x00e5 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x00f6, all -> 0x00e5 }
            r0 = r3
        L_0x008b:
            return r0
        L_0x008c:
            r1 = move-exception
            r3 = r5
        L_0x008e:
            r1.printStackTrace()     // Catch:{ Exception -> 0x00f1, all -> 0x00e0 }
            boolean r1 = r2.exists()     // Catch:{ Exception -> 0x00f1, all -> 0x00e0 }
            if (r1 == 0) goto L_0x009a
            r2.delete()     // Catch:{ Exception -> 0x00f1, all -> 0x00e0 }
        L_0x009a:
            r1 = r3
            goto L_0x0075
        L_0x009c:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r5
        L_0x00a0:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ea }
            java.lang.String r4 = "Failed to download Img file, err = "
            r3.<init>(r4)     // Catch:{ all -> 0x00ea }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00ea }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00ea }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00ea }
            com.adchina.android.ads.AdLog r3 = r8.logger     // Catch:{ all -> 0x00ea }
            r3.writeLog(r0)     // Catch:{ all -> 0x00ea }
            java.lang.String r3 = "AdChinaError"
            android.util.Log.e(r3, r0)     // Catch:{ all -> 0x00ea }
            if (r1 != 0) goto L_0x00c1
            if (r2 != 0) goto L_0x00fe
        L_0x00c1:
            com.adchina.android.ads.HttpEngine r0 = r8.mHttpEngine
            java.io.InputStream r0 = r0.requestGet(r9)
            goto L_0x008b
        L_0x00c8:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r5
        L_0x00cc:
            if (r1 != 0) goto L_0x00d0
            if (r2 != 0) goto L_0x00d5
        L_0x00d0:
            com.adchina.android.ads.HttpEngine r1 = r8.mHttpEngine
            r1.requestGet(r9)
        L_0x00d5:
            throw r0
        L_0x00d6:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r5
            goto L_0x00cc
        L_0x00db:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r4
            goto L_0x00cc
        L_0x00e0:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x00cc
        L_0x00e5:
            r2 = move-exception
            r7 = r2
            r2 = r0
            r0 = r7
            goto L_0x00cc
        L_0x00ea:
            r0 = move-exception
            goto L_0x00cc
        L_0x00ec:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r5
            goto L_0x00a0
        L_0x00f1:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x00a0
        L_0x00f6:
            r2 = move-exception
            r7 = r2
            r2 = r0
            r0 = r7
            goto L_0x00a0
        L_0x00fb:
            r1 = move-exception
            r3 = r4
            goto L_0x008e
        L_0x00fe:
            r0 = r2
            goto L_0x008b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.controllers.FullScreenAdController.downLoadAndSaveImg(java.lang.String):java.io.InputStream");
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 3:
                setRecvAdStatus(AdEngine.ERecvAdStatus.ERefreshAd);
                return true;
            case 4:
                if (this.mAdListener != null) {
                    this.mAdListener.onReceiveFullScreenAd(this.d);
                }
                this.logger.writeLog("++ onDisplayFullScreenAd");
                this.d.setImageBitmap((Bitmap) message.obj);
                setRecvAdStatus(AdEngine.ERecvAdStatus.ESendFullScreenImpTrack);
                this.logger.writeLog("-- onDisplayFullScreenAd");
                return true;
            case 5:
                if (this.mAdListener != null) {
                    this.mAdListener.onFailedToReceiveFullScreenAd(this.d);
                }
                setRecvAdStatus(AdEngine.ERecvAdStatus.ERefreshAd);
                return true;
            default:
                return true;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0099 A[SYNTHETIC, Splitter:B:25:0x0099] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x009e A[Catch:{ IOException -> 0x00a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c4 A[SYNTHETIC, Splitter:B:35:0x00c4] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c9 A[Catch:{ IOException -> 0x00d0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadLocalImgList() {
        /*
            r10 = this;
            r3 = 0
            r4 = 0
            com.adchina.android.ads.AdLog r0 = r10.logger
            java.lang.String r1 = "loadLocalFSImgList"
            r0.writeLog(r1)
            android.content.Context r0 = r10.mContext     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            java.lang.String r1 = "adchinaFSImgs.fc"
            java.io.File r0 = r0.getFileStreamPath(r1)     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            long r0 = r0.length()     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            int r0 = (int) r0     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            char[] r0 = new char[r0]     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            android.content.Context r1 = r10.mContext     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            java.lang.String r2 = "adchinaFSImgs.fc"
            java.io.FileInputStream r1 = r1.openFileInput(r2)     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0110, all -> 0x0105 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0110, all -> 0x0105 }
            r2.read(r0)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.util.HashMap r0 = r10.c     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r0.clear()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r0 = "\n"
            java.lang.String[] r0 = r3.split(r0)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            int r3 = r0.length     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
        L_0x0039:
            if (r4 < r3) goto L_0x0047
            r2.close()     // Catch:{ IOException -> 0x00ea }
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ IOException -> 0x00ea }
        L_0x0043:
            r10.deleteUnavailImgsFile()     // Catch:{ IOException -> 0x00ea }
        L_0x0046:
            return
        L_0x0047:
            r5 = r0[r4]     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r6 = "|||"
            int r6 = r5.indexOf(r6)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r7 = 0
            java.lang.String r7 = r5.substring(r7, r6)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r8 = "|||"
            int r8 = r8.length()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            int r6 = r6 + r8
            java.lang.String r5 = r5.substring(r6)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r6.<init>(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            boolean r8 = r6.exists()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            if (r8 == 0) goto L_0x0079
            java.lang.String r6 = r6.getName()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            boolean r6 = com.adchina.android.ads.Utils.isCachedFileTimeout(r6)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            if (r6 != 0) goto L_0x0079
            java.util.HashMap r6 = r10.c     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r6.put(r7, r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
        L_0x0079:
            int r4 = r4 + 1
            goto L_0x0039
        L_0x007c:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x007f:
            com.adchina.android.ads.AdLog r3 = r10.logger     // Catch:{ all -> 0x010e }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x010e }
            java.lang.String r5 = "Exceptions in loadLocalFSImgList , err = "
            r4.<init>(r5)     // Catch:{ all -> 0x010e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x010e }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x010e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x010e }
            r3.writeLog(r0)     // Catch:{ all -> 0x010e }
            if (r1 == 0) goto L_0x009c
            r1.close()     // Catch:{ IOException -> 0x00a5 }
        L_0x009c:
            if (r2 == 0) goto L_0x00a1
            r2.close()     // Catch:{ IOException -> 0x00a5 }
        L_0x00a1:
            r10.deleteUnavailImgsFile()     // Catch:{ IOException -> 0x00a5 }
            goto L_0x0046
        L_0x00a5:
            r0 = move-exception
            com.adchina.android.ads.AdLog r1 = r10.logger
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Exceptions in loadLocalFSImgList , err = "
            r2.<init>(r3)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.writeLog(r0)
            goto L_0x0046
        L_0x00bf:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x00c2:
            if (r1 == 0) goto L_0x00c7
            r1.close()     // Catch:{ IOException -> 0x00d0 }
        L_0x00c7:
            if (r2 == 0) goto L_0x00cc
            r2.close()     // Catch:{ IOException -> 0x00d0 }
        L_0x00cc:
            r10.deleteUnavailImgsFile()     // Catch:{ IOException -> 0x00d0 }
        L_0x00cf:
            throw r0
        L_0x00d0:
            r1 = move-exception
            com.adchina.android.ads.AdLog r2 = r10.logger
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Exceptions in loadLocalFSImgList , err = "
            r3.<init>(r4)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.writeLog(r1)
            goto L_0x00cf
        L_0x00ea:
            r0 = move-exception
            com.adchina.android.ads.AdLog r1 = r10.logger
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Exceptions in loadLocalFSImgList , err = "
            r2.<init>(r3)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.writeLog(r0)
            goto L_0x0046
        L_0x0105:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x00c2
        L_0x0109:
            r0 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x00c2
        L_0x010e:
            r0 = move-exception
            goto L_0x00c2
        L_0x0110:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x007f
        L_0x0115:
            r0 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.controllers.FullScreenAdController.loadLocalImgList():void");
    }

    public void onClickFullScreenAd() {
        this.logger.writeLog("++ onFullScreenClick");
        this.logger.writeLog("++ open " + this.mFullScreenClkUrl + " with browser");
        AdEngine.getAdEngine().openBrowser(this.mFullScreenClkUrl.toString());
        this.logger.writeLog("-- open browser");
        setClkTrackStatus(AdEngine.EClkTrack.ESendFullScreenClkTrack);
        this.logger.writeLog("-- onFullScreenClick");
    }

    /* access modifiers changed from: protected */
    public String readFcParams() {
        return readFcParams(Common.KFSFcFilename);
    }

    /* access modifiers changed from: protected */
    public void resetAdserverInfo() {
        super.resetAdserverInfo();
        this.a.clear();
        this.b.clear();
    }

    public void runClkTrackJob() {
        String str;
        String str2;
        while (!this.mStop) {
            switch (b()[this.mClickStatus.ordinal()]) {
                case 5:
                    this.logger.writeLog("++ onSendFullScreenClkTrack");
                    setClkTrackStatus(AdEngine.EClkTrack.EIdle);
                    try {
                        String str3 = String.valueOf(this.mReportBaseUrl.toString()) + Common.KFSCLK + ",0,0,0" + setupMaParams();
                        this.mHttpEngine.requestSend(str3);
                        this.logger.writeLog("send FullScreenClkTrack to " + str3);
                        break;
                    } catch (Exception e2) {
                        String str4 = "Exceptions in onSendFullScreenClkTrack, err = " + e2.toString();
                        this.logger.writeLog(str4);
                        Log.e(Common.KLogTag, str4);
                        break;
                    } finally {
                        setClkTrackStatus(AdEngine.EClkTrack.ESendFullScreenThdClkTrack);
                        str2 = "-- onSendFullScreenClkTrack";
                        this.logger.writeLog(str2);
                    }
                case 6:
                    setClkTrackStatus(AdEngine.EClkTrack.EIdle);
                    int size = this.b.size();
                    this.logger.writeLog("++ onSendFullScreenThdClkTrack, Size of list is " + size);
                    if (size > 0) {
                        try {
                            String str5 = (String) this.b.get(size - 1);
                            this.b.removeLast();
                            this.mHttpEngine.requestSend(str5);
                            this.logger.writeLog("send FullScreenThdClkTrack to " + str5);
                            break;
                        } catch (Exception e3) {
                            String str6 = "Failed to onSendFullScreenThdClkTrack, err = " + e3.toString();
                            this.logger.writeLog(str6);
                            Log.e(Common.KLogTag, str6);
                            break;
                        } finally {
                            setClkTrackStatus(AdEngine.EClkTrack.ESendFullScreenThdClkTrack);
                            str = "-- onSendFullScreenThdClkTrack";
                            this.logger.writeLog(str);
                        }
                    }
                    break;
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e4) {
            }
        }
    }

    public void runRecvAdThreadJob() {
        String str;
        String str2;
        String str3;
        String str4;
        InputStream inputStream;
        InputStream inputStream2;
        Throwable th;
        Exception e2;
        InputStream requestGet;
        while (!this.mStop) {
            switch (a()[this.mRunState.ordinal()]) {
                case 1:
                    this.logger.writeLog("++ onReceiveAd");
                    setRecvAdStatus(AdEngine.ERecvAdStatus.EIdle);
                    try {
                        if (AdManager.getDebugMode()) {
                            String str5 = setupAdserverUrlFromConfig();
                            if (str5 != null) {
                                this.logger.writeLog("AdserverUrl:" + str5);
                                requestGet = this.mHttpEngine.requestGet(str5, this.mAdspaceId.toString());
                            } else {
                                requestGet = null;
                            }
                        } else {
                            String str6 = setupAdserverUrl();
                            this.logger.writeLog("AdserverUrl:" + str6);
                            requestGet = this.mHttpEngine.requestGet(str6, this.mAdspaceId.toString());
                        }
                        try {
                            readAdserverInfo(requestGet);
                            this.e.a(this.mXmlVer, this.mAdType, this.mAdClickUrl, this.mReportBaseUrl, this.mThdImpTrack, this.mThdClkTrack, this.mFC, this.mRefTime, this.mSmsAddr, this.mSmsContent, this.mSmsType, this.mImgAddr, this.mTxtLnkText, this.mBtnType, this.mBtnX, this.mBtnY, this.mBtnW, this.mBtnH, this.mBtnThdClkTrack, this.mBtnDownloadUrl, this.mBtnSmsNumber, this.mBtnSmsContent, this.mBtnCallNumber, this.mFullScreenImgAddr, this.mFullScreenClkUrl, this.mFullScreenThdImpTrack, this.mFullScreenThdClkTrack, this.mVideoAddr, this.mVideoStartedTrack, this.mVideoEndedTrack);
                            this.e.a();
                            this.e.setRecvAdStatus(AdEngine.ERecvAdStatus.EGetImgMaterial);
                            loadLocalImgList();
                            setRecvAdStatus(AdEngine.ERecvAdStatus.EGetFullScreenImgMaterial);
                            Utils.splitTrackUrl(this.mFullScreenThdImpTrack.toString(), this.a);
                            Utils.splitTrackUrl(this.mFullScreenThdClkTrack.toString(), this.b);
                            this.mHttpEngine.closeStream(requestGet);
                            this.logger.writeLog("-- onReceiveAd");
                        } catch (Exception e3) {
                            e2 = e3;
                            inputStream2 = requestGet;
                            try {
                                String str7 = "Exceptions in onReceiveAd, err = " + e2.toString();
                                sendMessage(5, str7);
                                this.logger.writeLog(str7);
                                Log.e(Common.KLogTag, str7);
                                this.mHttpEngine.closeStream(inputStream2);
                                this.logger.writeLog("-- onReceiveAd");
                                Thread.sleep(50);
                            } catch (Throwable th2) {
                                th = th2;
                                this.mHttpEngine.closeStream(inputStream2);
                                this.logger.writeLog("-- onReceiveAd");
                                throw th;
                            }
                        } catch (Throwable th3) {
                            th = th3;
                            inputStream2 = requestGet;
                            this.mHttpEngine.closeStream(inputStream2);
                            this.logger.writeLog("-- onReceiveAd");
                            throw th;
                        }
                    } catch (Exception e4) {
                        Exception exc = e4;
                        inputStream2 = null;
                        e2 = exc;
                        String str72 = "Exceptions in onReceiveAd, err = " + e2.toString();
                        sendMessage(5, str72);
                        this.logger.writeLog(str72);
                        Log.e(Common.KLogTag, str72);
                        this.mHttpEngine.closeStream(inputStream2);
                        this.logger.writeLog("-- onReceiveAd");
                        Thread.sleep(50);
                    } catch (Throwable th4) {
                        Throwable th5 = th4;
                        inputStream2 = null;
                        th = th5;
                        this.mHttpEngine.closeStream(inputStream2);
                        this.logger.writeLog("-- onReceiveAd");
                        throw th;
                    }
                case 2:
                    this.logger.writeLog("++ onGetFullScreenImgMaterial");
                    setRecvAdStatus(AdEngine.ERecvAdStatus.EIdle);
                    FileInputStream fileInputStream = null;
                    try {
                        String stringBuffer = this.mFullScreenImgAddr.toString();
                        if (!this.c.containsKey(stringBuffer)) {
                            inputStream = downLoadAndSaveImg(stringBuffer);
                            try {
                                this.logger.writeLog("++ load remote fullScreen img");
                            } catch (Exception e5) {
                                Exception exc2 = e5;
                                fileInputStream = inputStream;
                                e = exc2;
                                try {
                                    String str8 = "Failed to get full-screen ad material, err = " + e.toString();
                                    sendMessage(5, str8);
                                    this.logger.writeLog(str8);
                                    Log.e(Common.KLogTag, str8);
                                    this.mHttpEngine.closeStream(fileInputStream);
                                    this.logger.writeLog("-- onGetFullScreenImgMaterial");
                                    Thread.sleep(50);
                                } catch (Throwable th6) {
                                    th = th6;
                                    this.mHttpEngine.closeStream(fileInputStream);
                                    this.logger.writeLog("-- onGetFullScreenImgMaterial");
                                    throw th;
                                }
                            } catch (Throwable th7) {
                                Throwable th8 = th7;
                                fileInputStream = inputStream;
                                th = th8;
                                this.mHttpEngine.closeStream(fileInputStream);
                                this.logger.writeLog("-- onGetFullScreenImgMaterial");
                                throw th;
                            }
                        } else {
                            FileInputStream fileInputStream2 = new FileInputStream((String) this.c.get(stringBuffer));
                            try {
                                this.logger.writeLog("++ load local fullScreen img");
                                inputStream = fileInputStream2;
                            } catch (Exception e6) {
                                e = e6;
                                fileInputStream = fileInputStream2;
                                String str82 = "Failed to get full-screen ad material, err = " + e.toString();
                                sendMessage(5, str82);
                                this.logger.writeLog(str82);
                                Log.e(Common.KLogTag, str82);
                                this.mHttpEngine.closeStream(fileInputStream);
                                this.logger.writeLog("-- onGetFullScreenImgMaterial");
                                Thread.sleep(50);
                            } catch (Throwable th9) {
                                th = th9;
                                fileInputStream = fileInputStream2;
                                this.mHttpEngine.closeStream(fileInputStream);
                                this.logger.writeLog("-- onGetFullScreenImgMaterial");
                                throw th;
                            }
                        }
                        try {
                            Bitmap convertStreamToBitmap = Utils.convertStreamToBitmap(inputStream);
                            if (convertStreamToBitmap != null) {
                                sendMessage(4, convertStreamToBitmap);
                            } else {
                                sendMessage(5, "Full Screen AdMaterial is null");
                            }
                            this.mHttpEngine.closeStream(inputStream);
                            this.logger.writeLog("-- onGetFullScreenImgMaterial");
                        } catch (Exception e7) {
                            Exception exc3 = e7;
                            fileInputStream = inputStream;
                            e = exc3;
                        } catch (Throwable th10) {
                            Throwable th11 = th10;
                            fileInputStream = inputStream;
                            th = th11;
                            this.mHttpEngine.closeStream(fileInputStream);
                            this.logger.writeLog("-- onGetFullScreenImgMaterial");
                            throw th;
                        }
                    } catch (Exception e8) {
                        e = e8;
                        String str822 = "Failed to get full-screen ad material, err = " + e.toString();
                        sendMessage(5, str822);
                        this.logger.writeLog(str822);
                        Log.e(Common.KLogTag, str822);
                        this.mHttpEngine.closeStream(fileInputStream);
                        this.logger.writeLog("-- onGetFullScreenImgMaterial");
                        Thread.sleep(50);
                    }
                case 6:
                    this.logger.writeLog("++ onRefreshAd");
                    try {
                        int intValue = Integer.valueOf(this.mRefTime.toString()).intValue();
                        try {
                            Thread.sleep((long) (intValue * com.energysource.szj.embeded.AdManager.AD_FILL_PARENT));
                            break;
                        } catch (Exception e9) {
                            break;
                        } finally {
                            setRecvAdStatus(AdEngine.ERecvAdStatus.EReceiveAd);
                            str3 = "-- onRefreshAd, delay = ";
                            this.logger.writeLog(str3 + intValue);
                        }
                    } catch (Exception e10) {
                        try {
                            Thread.sleep(15000);
                            break;
                        } catch (Exception e11) {
                            break;
                        } finally {
                            setRecvAdStatus(AdEngine.ERecvAdStatus.EReceiveAd);
                            str2 = "-- onRefreshAd, delay = ";
                            this.logger.writeLog(str2 + 15);
                        }
                    } catch (Throwable th12) {
                        try {
                            Thread.sleep((long) (15 * com.energysource.szj.embeded.AdManager.AD_FILL_PARENT));
                        } catch (Exception e12) {
                        } finally {
                            setRecvAdStatus(AdEngine.ERecvAdStatus.EReceiveAd);
                            str = "-- onRefreshAd, delay = ";
                            this.logger.writeLog(str + 15);
                        }
                        throw th12;
                    }
                case 7:
                    this.logger.writeLog("++ onSendFullScreenImpTrack");
                    try {
                        String str9 = String.valueOf(this.mReportBaseUrl.toString()) + Common.KFSIMP + ",0,0,0" + setupMaParams();
                        this.mHttpEngine.requestSend(str9);
                        this.logger.writeLog("send FullScreenImpTrack to " + str9);
                        break;
                    } catch (Exception e13) {
                        String str10 = "Exceptions in onSendFullScreenImpTrack, err = " + e13.toString();
                        this.logger.writeLog(str10);
                        Log.e(Common.KLogTag, str10);
                        break;
                    } finally {
                        setRecvAdStatus(AdEngine.ERecvAdStatus.ESendFullScreenThdImpTrack);
                        str4 = "-- onSendFullScreenImpTrack";
                        this.logger.writeLog(str4);
                    }
                case 8:
                    setRecvAdStatus(AdEngine.ERecvAdStatus.EIdle);
                    int size = this.a.size();
                    this.logger.writeLog("++ onSendFullScreenThdImpTrack, Size of list is " + size);
                    if (size > 0) {
                        try {
                            String str11 = (String) this.a.get(size - 1);
                            this.a.removeLast();
                            this.mHttpEngine.requestSend(str11);
                            this.logger.writeLog("send FullScreenThdImpTrack to " + str11);
                        } catch (Exception e14) {
                            String str12 = "Failed to onSendFullScreenThdImpTrack, err = " + e14.toString();
                            this.logger.writeLog(str12);
                            Log.e(Common.KLogTag, str12);
                        } finally {
                            setRecvAdStatus(AdEngine.ERecvAdStatus.ESendFullScreenThdImpTrack);
                        }
                    } else {
                        sendMessage(3, "RefreshAd");
                    }
                    this.logger.writeLog("-- onSendFullScreenThdImpTrack");
                    break;
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e15) {
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x007f A[SYNTHETIC, Splitter:B:29:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0084 A[Catch:{ IOException -> 0x008a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void saveLocalImgList() {
        /*
            r7 = this;
            r3 = 0
            com.adchina.android.ads.AdLog r0 = r7.logger
            java.lang.String r1 = "saveLocalFSImgList"
            r0.writeLog(r1)
            android.content.Context r0 = r7.mContext     // Catch:{ Exception -> 0x0097, all -> 0x007a }
            java.lang.String r1 = "adchinaFSImgs.fc"
            r2 = 0
            java.io.FileOutputStream r1 = r0.openFileOutput(r1, r2)     // Catch:{ Exception -> 0x0097, all -> 0x007a }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x009b, all -> 0x008c }
            r2.<init>(r1)     // Catch:{ Exception -> 0x009b, all -> 0x008c }
            java.util.HashMap r0 = r7.c     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.Set r0 = r0.keySet()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
        L_0x0020:
            boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            if (r0 != 0) goto L_0x0032
            r2.flush()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.close()     // Catch:{ IOException -> 0x0088 }
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x0088 }
        L_0x0031:
            return
        L_0x0032:
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r4 = "|||"
            r2.write(r4)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.HashMap r4 = r7.c     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = "\n"
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            goto L_0x0020
        L_0x0051:
            r0 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x0055:
            com.adchina.android.ads.AdLog r3 = r7.logger     // Catch:{ all -> 0x0095 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0095 }
            java.lang.String r5 = "Exceptions in saveLocalFSImgList, err = "
            r4.<init>(r5)     // Catch:{ all -> 0x0095 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0095 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0095 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0095 }
            r3.writeLog(r0)     // Catch:{ all -> 0x0095 }
            if (r1 == 0) goto L_0x0072
            r1.close()     // Catch:{ IOException -> 0x0078 }
        L_0x0072:
            if (r2 == 0) goto L_0x0031
            r2.close()     // Catch:{ IOException -> 0x0078 }
            goto L_0x0031
        L_0x0078:
            r0 = move-exception
            goto L_0x0031
        L_0x007a:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x007d:
            if (r1 == 0) goto L_0x0082
            r1.close()     // Catch:{ IOException -> 0x008a }
        L_0x0082:
            if (r2 == 0) goto L_0x0087
            r2.close()     // Catch:{ IOException -> 0x008a }
        L_0x0087:
            throw r0
        L_0x0088:
            r0 = move-exception
            goto L_0x0031
        L_0x008a:
            r1 = move-exception
            goto L_0x0087
        L_0x008c:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x007d
        L_0x0090:
            r0 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x007d
        L_0x0095:
            r0 = move-exception
            goto L_0x007d
        L_0x0097:
            r0 = move-exception
            r1 = r3
            r2 = r3
            goto L_0x0055
        L_0x009b:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.controllers.FullScreenAdController.saveLocalImgList():void");
    }

    public void setAdViewController(AdViewController adViewController) {
        adViewController.stop();
        this.e = adViewController;
        adViewController.a(false);
        adViewController.setRecvAdStatus(AdEngine.ERecvAdStatus.EIdle);
        this.f = AdManager.getAdspaceId();
        adViewController.start();
    }

    public void setFullScreenAdView(FullScreenAdView fullScreenAdView) {
        this.d = fullScreenAdView;
        this.d.setController(this);
    }

    /* access modifiers changed from: protected */
    public void setupAdspaceId() {
        this.mAdspaceId.setLength(0);
        this.mAdspaceId.append(AdManager.getFullScreenAdspaceId());
    }

    public void stop() {
        this.mStop = true;
        if (this.e != null) {
            new Timer().schedule(new c(this), 15000);
        }
    }

    /* access modifiers changed from: protected */
    public void writeFcParams(String str) {
        writeFcParams(str, Common.KFSFcFilename);
    }
}
