package com.adchina.android.ads;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import com.adchina.android.ads.controllers.AdViewController;
import com.adchina.android.ads.controllers.FullScreenAdController;
import com.adchina.android.ads.controllers.VideoAdController;
import com.adchina.android.ads.views.AdBrowserView;
import com.adchina.android.ads.views.AdView;
import com.adchina.android.ads.views.FullScreenAdView;

public class AdEngine {
    private static AdEngine f;
    private boolean a = false;
    private Context b;
    private AdViewController c;
    private FullScreenAdController d;
    private VideoAdController e;

    public enum EClkTrack {
        ESendClkTrack,
        ESendThdClkTrack,
        ESendBtnClkTrack,
        ESendBtnThdClkTrack,
        ESendFullScreenClkTrack,
        ESendFullScreenThdClkTrack,
        ESendVideoClkTrack,
        ESendVideoThdClkTrack,
        ESendVideoCloseClkTrack,
        ESendVideoCloseThdClkTrack,
        EIdle
    }

    public enum ERecvAdStatus {
        EReceiveAd,
        EGetFullScreenImgMaterial,
        EGetImgMaterial,
        ESendImpTrack,
        ESendThdImpTrack,
        ERefreshAd,
        ESendFullScreenImpTrack,
        ESendFullScreenThdImpTrack,
        ESendVideoStartedImpTrack,
        ESendVideoStartedThdImpTrack,
        ESendVideoEndedImpTrack,
        ESendVideoEndedThdImpTrack,
        EIdle
    }

    private AdEngine(Context context) {
        this.b = context;
        this.c = new AdViewController(context);
        this.d = new FullScreenAdController(context);
        this.e = VideoAdController.initVideoAdController(this.b);
        AdManager.setPhoneUA(new WebView(context).getSettings().getUserAgentString());
        f = this;
    }

    public static AdEngine getAdEngine() {
        return f;
    }

    public static AdEngine initAdEngine(Context context) {
        if (f == null) {
            f = new AdEngine(context);
        }
        return f;
    }

    public void addBannerAdView(AdView adView) {
        if (this.c != null) {
            this.c.addAdView(adView);
        }
    }

    public void makeCall(String str) {
        this.b.startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + str)));
    }

    /* access modifiers changed from: protected */
    public void onClickFullScreenAd() {
        if (this.d != null) {
            this.d.onClickFullScreenAd();
        }
    }

    public void openBrowser(String str) {
        if (str.length() > 0 && this.b != null) {
            Uri parse = Uri.parse(str);
            Intent intent = new Intent();
            intent.putExtra("browserurl", parse.toString());
            intent.setClass(this.b, AdBrowserView.class);
            this.b.startActivity(intent);
        }
    }

    public void playVideo(String str) {
        if (this.e != null) {
            this.e.playVideo(str);
        }
    }

    public void removeBannerAdView(AdView adView) {
        if (this.c != null) {
            this.c.removeAdView(adView);
        }
    }

    public void sendSms(String str, String str2) {
        Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + str));
        intent.putExtra("sms_body", str2);
        this.b.startActivity(intent);
    }

    public void setAdListener(AdListener adListener) {
        if (this.c != null) {
            this.c.setAdListener(adListener);
        }
        if (this.d != null) {
            this.d.setAdListener(adListener);
        }
        if (this.e != null) {
            this.e.setAdListener(adListener);
        }
    }

    public void setDefaultUrl(String str) {
        if (this.c != null) {
            this.c.setDefaultUrl(str);
        }
    }

    public void setFullScreenAdView(FullScreenAdView fullScreenAdView) {
        if (this.d != null) {
            this.d.setFullScreenAdView(fullScreenAdView);
        }
    }

    public void setVideoFinishEvent(AdVideoFinishListener adVideoFinishListener) {
        if (this.e != null) {
            this.e.setVideoFinishEvent(adVideoFinishListener);
        }
    }

    public void startBannerAd() {
        if (!this.a) {
            if (this.c != null) {
                this.c.start();
            }
            this.a = true;
        }
    }

    public void startFullScreenAd() {
        if (this.d != null) {
            this.d.setAdViewController(this.c);
        }
        this.d.start();
    }

    public void startVideoAd() {
        if (this.e != null) {
            this.e.start();
        }
    }

    public void stopBannerAd() {
        if (this.c != null) {
            this.c.stop();
        }
    }

    public void stopFullScreenAd() {
        if (this.d != null) {
            this.d.stop();
        }
    }

    public void stopVideoAd() {
        if (this.e != null) {
            this.e.stop();
        }
    }
}
