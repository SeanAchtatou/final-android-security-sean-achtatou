package com.adchina.android.ads.views;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.GravitySensorExt;
import com.adchina.android.ads.Utils;

public class AdBrowserView extends Activity implements DownloadListener, GravitySensorExt.MovingListener {
    private WebView a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(bundle);
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        AbsoluteLayout absoluteLayout = new AbsoluteLayout(this);
        absoluteLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        setContentView(absoluteLayout);
        this.a = new WebView(this);
        absoluteLayout.addView(this.a, new ViewGroup.LayoutParams(-1, -1));
        this.a.getSettings().setJavaScriptEnabled(true);
        this.a.setScrollBarStyle(0);
        this.a.loadUrl(getIntent().getExtras().getString("browserurl"));
        this.a.setWebViewClient(new b(this));
        this.a.setDownloadListener(this);
        Button button = new Button(this);
        absoluteLayout.addView(button);
        int dip2px = Utils.dip2px(this, 30.0f);
        int dip2px2 = Utils.dip2px(this, 30.0f);
        button.setLayoutParams(new AbsoluteLayout.LayoutParams(dip2px, dip2px2, width - dip2px, height - dip2px2));
        button.setBackgroundResource(AdManager.getrCloseImg());
        button.setOnClickListener(new a(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.a = null;
    }

    public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        try {
            Uri parse = Uri.parse(str);
            Intent intent = new Intent("android.intent.action.VIEW", parse);
            if (str4.equalsIgnoreCase("video/mp4")) {
                intent.setDataAndType(parse, str4);
            }
            startActivity(intent);
        } catch (Exception e) {
            Log.e("e", e.toString());
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || this.a == null || !this.a.canGoBack()) {
            return super.onKeyDown(i, keyEvent);
        }
        this.a.goBack();
        return true;
    }

    public void onMoving(GravitySensorExt.ThreeDPoint threeDPoint, GravitySensorExt.ThreeDPoint threeDPoint2, float f, int i) {
        if (this.a != null) {
            this.a.loadUrl(String.format("javascript:ad.sensors.fireMovingSensor({x:%s,y:%s,z:%s},{x:%s,y:%s,z:%s},{speed:%s,orientation:%s});", Float.valueOf(threeDPoint.getX()), Float.valueOf(threeDPoint.getY()), Float.valueOf(threeDPoint.getZ()), Float.valueOf(threeDPoint2.getX()), Float.valueOf(threeDPoint2.getY()), Float.valueOf(threeDPoint2.getZ()), Float.valueOf(f), Integer.valueOf(i)));
        }
    }
}
