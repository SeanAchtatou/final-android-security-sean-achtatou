package com.adchina.android.ads.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.adchina.android.ads.AdBitmap;
import com.adchina.android.ads.GifEngine;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;

public class GifImageView extends ImageView implements Handler.Callback {
    private GifEngine a;
    private Timer b;
    /* access modifiers changed from: private */
    public Handler c;
    private float d = 12.0f;
    private boolean e = false;
    private Bitmap f;

    public GifImageView(Context context) {
        super(context);
        new Matrix();
        a();
    }

    public GifImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new Matrix();
        a();
    }

    public GifImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        new Matrix();
        a();
    }

    private void a() {
        this.c = new Handler(this);
    }

    private void b() {
        if (this.b != null) {
            this.b.cancel();
        }
        if (this.a != null) {
            this.a.dispose();
            this.a = null;
        }
    }

    private void c() {
        int i = this.d != 0.0f ? (int) (1000.0d / ((double) this.d)) : 100;
        this.b = new Timer();
        this.b.schedule(new f(this), 0, (long) i);
    }

    private void d() {
        try {
            Rect rect = new Rect();
            getHitRect(rect);
            int width = rect.width();
            int height = rect.height();
            if (this.a != null && this.a.getWidth() <= (width << 1) && this.a.getHeight() <= (height << 1)) {
                int width2 = this.a.getWidth();
                width = width2;
                height = this.a.getHeight();
            }
            if (width > 0 && height > 0) {
                if (this.f != null) {
                    this.f.recycle();
                }
                this.f = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                setImageBitmap(this.f);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public Bitmap getImageBitmap() {
        return this.f;
    }

    public boolean handleMessage(Message message) {
        if (this.a != null) {
            try {
                AdBitmap image = this.a.getImage();
                this.a.nextFrame();
                if (!(image == null || image.getBitmap() == null)) {
                    if (this.f == null) {
                        d();
                    }
                    if (this.f != null) {
                        Canvas canvas = new Canvas();
                        canvas.setBitmap(this.f);
                        int offsetX = image.getOffsetX();
                        int offsetY = image.getOffsetY();
                        Bitmap bitmap = image.getBitmap();
                        if (offsetX <= 0) {
                            offsetX = 0;
                        }
                        float f2 = (float) offsetX;
                        if (offsetY <= 0) {
                            offsetY = 0;
                        }
                        canvas.drawBitmap(bitmap, f2, (float) offsetY, (Paint) null);
                        canvas.save();
                        setImageBitmap(this.f);
                    }
                    if (!this.e) {
                        int delay = image.getDelay();
                        if (delay <= 100) {
                            delay = 100;
                        }
                        float f3 = 1000.0f / ((float) delay);
                        if (f3 < this.d) {
                            Timer timer = this.b;
                            this.d = f3;
                            c();
                            timer.cancel();
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return false;
    }

    public void setBmp(Bitmap bitmap) {
        b();
        if (bitmap != null) {
            this.f = bitmap;
            setImageBitmap(bitmap);
        }
    }

    public void setGif(int i) {
        b();
        d();
        InputStream openRawResource = getResources().openRawResource(i);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            try {
                int read = openRawResource.read();
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(read);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        this.a = GifEngine.CreateGifImage(byteArrayOutputStream.toByteArray());
        d();
        c();
    }

    public void setGif(GifEngine gifEngine) {
        b();
        d();
        this.a = gifEngine;
        d();
        c();
    }

    public void setGifSpeed(int i) {
        this.d = (float) i;
        this.e = true;
    }
}
