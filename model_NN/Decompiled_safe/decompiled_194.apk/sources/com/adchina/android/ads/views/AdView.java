package com.adchina.android.ads.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import com.adchina.android.ads.controllers.AdViewController;

public class AdView extends RelativeLayout implements View.OnClickListener {
    private ContentView a = null;
    private AdViewController b;

    public AdView(Context context) {
        super(context);
        a(context);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public AdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
    }

    private void a(Context context) {
        this.a = new ContentView(context);
        this.a.setAdView(this);
        this.a.setOnClickListener(this);
        addView(this.a, new RelativeLayout.LayoutParams(-1, -1));
    }

    public void onClick(View view) {
        if (this.b != null) {
            this.b.onClickBackground();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (this.b == null) {
                    return true;
                }
                this.b.onTouchEvent(this, motionEvent);
                return true;
            default:
                return true;
        }
    }

    public void setBackgroundColor(int i) {
        if (this.a != null) {
            this.a.setBackgroundColor(i);
        }
    }

    public void setContent(Bitmap bitmap) {
        if (this.a != null) {
            this.a.setContent(bitmap);
        }
    }

    public void setContent(String str) {
        if (this.a != null) {
            this.a.setContent(str);
        }
    }

    public void setContent(byte[] bArr) {
        if (this.a != null) {
            this.a.setContent(bArr);
        }
    }

    public void setController(AdViewController adViewController) {
        this.b = adViewController;
        this.a.setController(adViewController);
    }

    public void setDefaultImage(int i) {
        setDefaultImage(new BitmapDrawable(getContext().getResources().openRawResource(i)).getBitmap());
    }

    public void setDefaultImage(Bitmap bitmap) {
        if (this.a != null) {
            this.a.setDefaultImage(bitmap);
        }
    }

    public void setFont(Typeface typeface) {
        if (this.a != null) {
            this.a.setFont(typeface);
        }
    }

    public void setPenColor(int i) {
        if (this.a != null) {
            this.a.setPenColor(i);
        }
    }
}
