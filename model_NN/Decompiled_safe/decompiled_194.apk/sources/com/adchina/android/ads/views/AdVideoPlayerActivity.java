package com.adchina.android.ads.views;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.Common;
import com.adchina.android.ads.Utils;
import com.adchina.android.ads.controllers.VideoAdController;
import java.io.IOException;

public class AdVideoPlayerActivity extends Activity implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnVideoSizeChangedListener, SurfaceHolder.Callback {
    /* access modifiers changed from: private */
    public MediaPlayer a;
    private SurfaceHolder b;
    private SurfaceView c;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    private GifImageView f;
    private AbsoluteLayout g;

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.d = true;
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(bundle);
        this.g = new AbsoluteLayout(this);
        this.g.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        setContentView(this.g);
        this.c = new SurfaceView(this);
        this.g.addView(this.c);
        this.f = new GifImageView(this);
        this.g.addView(this.f);
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        int dip2px = Utils.dip2px(this, 64.0f);
        int dip2px2 = Utils.dip2px(this, 64.0f);
        this.f.setLayoutParams(new AbsoluteLayout.LayoutParams(dip2px, dip2px2, (width - dip2px) / 2, (height - dip2px2) / 2));
        this.f.setGif(AdManager.getrLoadingImg());
        Button button = new Button(this);
        this.g.addView(button);
        int dip2px3 = Utils.dip2px(this, 30.0f);
        int dip2px4 = Utils.dip2px(this, 30.0f);
        button.setLayoutParams(new AbsoluteLayout.LayoutParams(dip2px3, dip2px4, width - dip2px3, height - dip2px4));
        button.setBackgroundResource(AdManager.getrCloseImg());
        button.setOnClickListener(new c(this));
        this.b = this.c.getHolder();
        this.b.addCallback(this);
        this.b.setType(3);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.d) {
            try {
                VideoAdController.getVideoAdController().onPlayEnded();
                VideoAdController.getVideoAdController().fireVideoFinishEvent(this, getIntent().getExtras().getString("Url"));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else if (this.e) {
            VideoAdController.getVideoAdController().onClosedVideo();
            VideoAdController.getVideoAdController().fireVideoFinishEvent(this, getIntent().getExtras().getString("Url"));
        }
        super.onDestroy();
        if (this.a != null) {
            this.a.release();
            this.a = null;
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        VideoAdController.getVideoAdController().onPlayError();
        this.d = false;
        finish();
        return false;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.g.removeView(this.f);
        this.b.setFixedSize(mediaPlayer.getVideoWidth(), mediaPlayer.getVideoHeight());
        mediaPlayer.start();
        VideoAdController.getVideoAdController().onDisplayAd();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        VideoAdController.getVideoAdController().onClickVideoAd();
        return false;
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
        int videoWidth = mediaPlayer.getVideoWidth();
        int videoHeight = mediaPlayer.getVideoHeight();
        if (videoWidth != 0 && videoHeight != 0) {
            this.b.setFixedSize(videoWidth, videoHeight);
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        int width = this.c.getWidth();
        int height = this.c.getHeight();
        if (i2 > 0 && i3 > 0) {
            this.c.setLayoutParams(new AbsoluteLayout.LayoutParams(i2, i3, (width - i2) / 2, (height - i3) / 2));
            this.g.postInvalidate();
        }
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        String string = getIntent().getExtras().getString("ADUrl");
        try {
            if (this.a == null) {
                this.a = new MediaPlayer();
            }
            this.a.reset();
            this.a.setDisplay(this.b);
            this.a.setScreenOnWhilePlaying(true);
            this.a.setDataSource(string);
            this.a.setOnErrorListener(this);
            this.a.setOnPreparedListener(this);
            this.a.setOnVideoSizeChangedListener(this);
            this.a.setAudioStreamType(3);
            this.a.setOnCompletionListener(this);
            this.a.prepareAsync();
        } catch (IllegalArgumentException e2) {
            Log.v(Common.KLogTag, e2.getMessage());
        } catch (IllegalStateException e3) {
            Log.v(Common.KLogTag, e3.getMessage());
        } catch (IOException e4) {
            Log.v(Common.KLogTag, e4.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
    }
}
