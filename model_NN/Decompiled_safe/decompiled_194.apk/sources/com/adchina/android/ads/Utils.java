package com.adchina.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

public class Utils {
    public static int GetRandomNumber() {
        return (int) (Math.random() * 10000.0d);
    }

    public static Bitmap convertStreamToBitmap(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read();
            if (read == -1) {
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            }
            byteArrayOutputStream.write(read);
        }
    }

    protected static String convertStreamToString(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
            } catch (IOException e) {
                Log.e(Common.KLogTag, "convertStreamToString:" + e.toString());
            }
        }
        return sb.toString();
    }

    public static int dip2px(Context context, float f) {
        return (int) ((context.getResources().getDisplayMetrics().density * f) + 0.5f);
    }

    public static String getNetworkTypes(Context context) {
        NetworkInfo[] allNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getAllNetworkInfo();
        StringBuffer stringBuffer = new StringBuffer();
        if (allNetworkInfo != null) {
            for (NetworkInfo networkInfo : allNetworkInfo) {
                if (networkInfo.isConnected()) {
                    stringBuffer.append(",");
                    stringBuffer.append(networkInfo.getTypeName());
                    stringBuffer.append(",");
                    stringBuffer.append(networkInfo.getSubtypeName());
                }
            }
        }
        return stringBuffer.length() > 0 ? stringBuffer.substring(1) : "";
    }

    public static String getNowTime(String str) {
        return new SimpleDateFormat(str).format((Object) new Date());
    }

    public static boolean isCachedFileTimeout(String str) {
        try {
            return isCachedFileTimeout(str, 7);
        } catch (ParseException e) {
            Log.e(Common.KLogTag, "isCachedFileTimeout:" + e.toString());
            return true;
        }
    }

    public static boolean isCachedFileTimeout(String str, int i) {
        int length = "yyyyMMddHHmmss".length();
        if (str.length() < length) {
            return true;
        }
        Date parse = new SimpleDateFormat("yyyyMMddHHmmss").parse(str.substring(0, length));
        Calendar instance = Calendar.getInstance();
        instance.setTime(parse);
        instance.add(5, i);
        return instance.getTime().before(new Date());
    }

    public static int px2dip(Context context, float f) {
        return (int) ((f / context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static void splitTrackUrl(String str, LinkedList linkedList) {
        linkedList.clear();
        int i = 0;
        while (true) {
            int indexOf = str.indexOf(Common.KSplitTag, i);
            if (-1 == indexOf) {
                linkedList.add(str.substring(i));
                return;
            } else {
                linkedList.add(str.substring(i, indexOf));
                i = Common.KSplitTag.length() + indexOf;
            }
        }
    }
}
