package com.adchina.android.ads.views;

import android.view.View;

final class c implements View.OnClickListener {
    private /* synthetic */ AdVideoPlayerActivity a;

    c(AdVideoPlayerActivity adVideoPlayerActivity) {
        this.a = adVideoPlayerActivity;
    }

    public final void onClick(View view) {
        if (this.a.a != null) {
            this.a.a.stop();
        }
        this.a.e = true;
        this.a.d = false;
        this.a.finish();
    }
}
