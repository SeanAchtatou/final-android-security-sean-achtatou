package com.adchina.android.ads.views.animations;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.ScaleAnimation;

final class d implements Runnable {
    /* access modifiers changed from: private */
    public /* synthetic */ EnlargementAnimation a;

    d(EnlargementAnimation enlargementAnimation) {
        this.a = enlargementAnimation;
    }

    public final void run() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.4f, 0.8f, 1.4f, 0.8f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(500);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setInterpolator(new AccelerateInterpolator());
        scaleAnimation.setAnimationListener(new e(this));
        this.a.a.startAnimation(scaleAnimation);
    }
}
