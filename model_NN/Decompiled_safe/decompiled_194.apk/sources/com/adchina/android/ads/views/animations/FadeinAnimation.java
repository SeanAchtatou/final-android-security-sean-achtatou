package com.adchina.android.ads.views.animations;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import com.adchina.android.ads.views.ContentView;

public class FadeinAnimation {
    /* access modifiers changed from: private */
    public ContentView a;

    public FadeinAnimation(ContentView contentView) {
        this.a = contentView;
    }

    public void startAnimation() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 0.8f);
        alphaAnimation.setDuration(700);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        alphaAnimation.setAnimationListener(new g(this));
        this.a.startAnimation(alphaAnimation);
    }
}
