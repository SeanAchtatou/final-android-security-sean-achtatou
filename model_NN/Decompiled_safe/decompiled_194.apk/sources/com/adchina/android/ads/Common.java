package com.adchina.android.ads;

public final class Common {
    public static String KAdDebugSvrUrlFormat = "http://192.168.21.132:7008/a.htm?pv=1&sp=%s,0,0,0,0,1,1,10&ec=utf-8&cb=%d&fc=%s&ts=1262152690.23&stt=4&mpid=100&muid=%s&g=4&an=%s&ct=%s&ua=%s&bn=%s";
    public static final String KAdSvrUrlFormat = "http://amob.acs86.com/a.htm?pv=1&sp=%s,0,0,0,0,1,1,10&ec=utf-8&cb=%d&fc=%s&ts=1262152690.23&stt=4&mpid=100&muid=%s&g=4&an=%s&ct=%s&ua=%s&bn=%s";
    public static final String KAdUrlParasFormat = "/a.htm?pv=1&sp=%s,0,0,0,0,1,1,10&ec=utf-8&cb=%d&fc=%s&ts=1262152690.23&stt=4&mpid=100&muid=%s&g=4&an=%s&ct=%s&ua=%s&bn=%s";
    public static final String KBNImgsDir = "/sdcard/ad/bnImg";
    public static final String KBNImgsFilename = "adchinaBNImgs.fc";
    public static final String KBTNCLK = "200";
    public static final String KCLK = "1";
    public static final int KCachedFileTimeout = 7;
    protected static final String KCookieFilename = "adchinaCookie.ce";
    public static final String KDebugCFGFilename = "/sdcard/adchina_debug.cfg";
    public static final String KEnc = "utf-8";
    public static final String KFSCLK = "204";
    public static final String KFSFcFilename = "adchinaFSFC.fc";
    public static final String KFSIMP = "203";
    public static final String KFSImgsDir = "/sdcard/ad/fsImg";
    public static final String KFSImgsFilename = "adchinaFSImgs.fc";
    public static final String KFcFilename = "adchinaFC.fc";
    public static final int KGifInterval = 2000;
    public static final String KIMP = "2";
    public static final String KLogTag = "AdChinaError";
    public static final String KMaParamsFormat = "&ma=%s,%s,%s,%s,%s,%s,%s,%s";
    public static final int KMsgDisplayAd = 1;
    public static final int KMsgDisplayFullScreenAd = 4;
    public static final int KMsgDownLoadedVideo = 6;
    public static final int KMsgEndedPlayVideo = 9;
    public static final int KMsgFailedToPlayVideo = 8;
    public static final int KMsgFailedToReceiveAd = 2;
    public static final int KMsgFailedToReceiveFullScreenAd = 5;
    public static final int KMsgFailedToReceiveVideoAd = 16;
    public static final int KMsgPlayVideo = 7;
    public static final int KMsgRefreshAd = 3;
    public static final String KSplitTag = "|||";
    public static final int KThdIdle = 50;
    public static final int KTimerDelay = 3000;
    public static final int KTimerInterval = 15000;
    public static final String KVCIMP = "209";
    public static final String KVEIMP = "208";
    public static final String KVSIMP = "207";
    public static final String KVideosDir = "/sdcard/ad/video";
    public static final String KVideosFilename = "adchinaVideos.fc";

    public enum EAdModel {
        EAdTXT,
        EAdJPG,
        EAdPNG,
        EAdGIF,
        EAdNONE
    }
}
