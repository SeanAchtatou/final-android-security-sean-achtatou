package com.adchina.android.ads.views.animations;

import android.view.animation.Animation;

final class g implements Animation.AnimationListener {
    private /* synthetic */ FadeinAnimation a;

    g(FadeinAnimation fadeinAnimation) {
        this.a = fadeinAnimation;
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.a.post(new h(this.a));
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
