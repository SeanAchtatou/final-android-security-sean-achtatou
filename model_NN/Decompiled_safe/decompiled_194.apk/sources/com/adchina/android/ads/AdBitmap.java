package com.adchina.android.ads;

import android.graphics.Bitmap;

public class AdBitmap {
    private Bitmap a;
    private int b;
    private int c;
    private int d = 100;

    public AdBitmap(Bitmap bitmap, int i, int i2, int i3) {
        this.a = bitmap;
        this.b = i;
        this.c = i2;
        this.d = i3;
    }

    public Bitmap getBitmap() {
        return this.a;
    }

    public int getDelay() {
        return this.d;
    }

    public int getOffsetX() {
        return this.b;
    }

    public int getOffsetY() {
        return this.c;
    }
}
