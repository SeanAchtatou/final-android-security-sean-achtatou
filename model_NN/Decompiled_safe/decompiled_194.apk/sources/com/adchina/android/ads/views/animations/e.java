package com.adchina.android.ads.views.animations;

import android.view.animation.Animation;

final class e implements Animation.AnimationListener {
    private /* synthetic */ d a;

    e(d dVar) {
        this.a = dVar;
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.a.a.post(new f(this.a.a));
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
