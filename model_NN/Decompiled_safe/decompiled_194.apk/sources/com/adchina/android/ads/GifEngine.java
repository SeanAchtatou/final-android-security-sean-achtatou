package com.adchina.android.ads;

import java.util.Vector;

public class GifEngine {
    private Vector a = new Vector(1);
    private int b = 0;
    private int c;
    private int d;

    public static GifEngine CreateGifImage(byte[] bArr) {
        try {
            GifEngine gifEngine = new GifEngine();
            b bVar = new b(bArr);
            gifEngine.c = bVar.a;
            gifEngine.d = bVar.b;
            while (bVar.a()) {
                try {
                    AdBitmap c2 = bVar.c();
                    if (c2 != null) {
                        gifEngine.addImage(c2);
                    }
                    bVar.b();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            bVar.d();
            return gifEngine;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void addImage(AdBitmap adBitmap) {
        this.a.addElement(adBitmap);
    }

    public void dispose() {
        try {
            if (this.a != null && this.a.size() > 0) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= this.a.size()) {
                        break;
                    }
                    ((AdBitmap) this.a.get(i2)).getBitmap().recycle();
                    i = i2 + 1;
                }
            }
            this.a.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getHeight() {
        return this.d;
    }

    public AdBitmap getImage() {
        if (size() == 0) {
            return null;
        }
        return (AdBitmap) this.a.elementAt(this.b);
    }

    public int getWidth() {
        return this.c;
    }

    public void nextFrame() {
        if (this.b + 1 < size()) {
            this.b++;
        } else {
            this.b = 0;
        }
    }

    public int size() {
        return this.a.size();
    }
}
