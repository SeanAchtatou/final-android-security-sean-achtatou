package com.adchina.android.ads.views.animations;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;

final class h implements Runnable {
    /* access modifiers changed from: private */
    public /* synthetic */ FadeinAnimation a;

    h(FadeinAnimation fadeinAnimation) {
        this.a = fadeinAnimation;
    }

    public final void run() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.8f, 0.2f);
        alphaAnimation.setDuration(500);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        alphaAnimation.setAnimationListener(new i(this));
        this.a.a.startAnimation(alphaAnimation);
    }
}
