package com.adchina.android.ads;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class a extends SQLiteOpenHelper {
    a(Context context) {
        super(context, "adcookie.db", (SQLiteDatabase.CursorFactory) null, 1);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE adcookie (_id INTEGER PRIMARY KEY,name TEXT,value TEXT,domain TEXT)");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE adcookie");
        sQLiteDatabase.execSQL("CREATE TABLE adcookie (_id INTEGER PRIMARY KEY,name TEXT,value TEXT,domain TEXT)");
    }
}
