package com.adchina.android.ads;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.FloatMath;

public class GravitySensorExt implements SensorEventListener {
    public static final int CWJ_BACKWARD = 16;
    public static final int CWJ_DOWN = 1;
    public static final int CWJ_FORWARD = 8;
    public static final int CWJ_LEFT = 2;
    public static final int CWJ_RIGHT = 4;
    public static final int CWJ_UNKNOWN = -1;
    public static final int CWJ_UP = 0;
    private static boolean a = false;
    public static boolean isRunning = false;
    public static final Object syncObj = new Object();
    private float b;
    private float c;
    private float d;
    private float e;
    private float f;
    private float g;
    private long h;
    private SensorManager i;
    private MovingListener j;

    public interface MovingListener {
        void onMoving(ThreeDPoint threeDPoint, ThreeDPoint threeDPoint2, float f, int i);
    }

    public class ThreeDPoint {
        private float a;
        private float b;
        private float c;

        public ThreeDPoint(GravitySensorExt gravitySensorExt, float f, float f2, float f3) {
            setX(f);
            setY(f2);
            setZ(f3);
        }

        public float getX() {
            return this.a;
        }

        public float getY() {
            return this.b;
        }

        public float getZ() {
            return this.c;
        }

        public void setX(float f) {
            this.a = f;
        }

        public void setY(float f) {
            this.b = f;
        }

        public void setZ(float f) {
            this.c = f;
        }
    }

    public GravitySensorExt(Activity activity) {
        this.i = (SensorManager) activity.getSystemService("sensor");
    }

    public static boolean isReg() {
        return a;
    }

    public static boolean isRunning() {
        return isRunning;
    }

    public static void setReg(boolean z) {
        a = z;
    }

    public static void setRunning(boolean z) {
        isRunning = z;
    }

    public MovingListener getListener() {
        return this.j;
    }

    public void onAccuracyChanged(Sensor sensor, int i2) {
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (!isRunning()) {
            synchronized (this) {
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - this.h > 100) {
                    long j2 = currentTimeMillis - this.h;
                    this.h = currentTimeMillis;
                    this.b = sensorEvent.values[0];
                    this.c = sensorEvent.values[1];
                    this.d = sensorEvent.values[2];
                    float f2 = this.b - this.e;
                    float f3 = this.c - this.f;
                    float f4 = this.d - this.g;
                    float abs = Math.abs(this.b);
                    float abs2 = Math.abs(this.c);
                    float abs3 = Math.abs(this.d);
                    int i2 = (abs <= abs2 || abs <= abs3) ? (abs2 <= abs || abs2 <= abs3) ? (abs3 <= abs || abs3 <= abs2) ? -1 : this.d > 0.0f ? 0 : 1 : this.c > 0.0f ? 8 : 16 : this.b > 0.0f ? 4 : 2;
                    float sqrt = (FloatMath.sqrt(((f2 * f2) + (f3 * f3)) + (f4 * f4)) / ((float) j2)) * 10000.0f;
                    if (sqrt > 800.0f) {
                        startMovableAction(new ThreeDPoint(this, this.e, this.f, this.g), new ThreeDPoint(this, this.b, this.c, this.d), sqrt, i2);
                        this.h = currentTimeMillis;
                    }
                    this.e = this.b;
                    this.f = this.c;
                    this.g = this.d;
                }
            }
        }
    }

    public void regSensorEvent() {
        if (!isReg()) {
            a = true;
            this.i.registerListener(this, this.i.getDefaultSensor(1), 2);
        }
    }

    public void setListener(MovingListener movingListener) {
        this.j = movingListener;
    }

    public void startMovableAction(ThreeDPoint threeDPoint, ThreeDPoint threeDPoint2, float f2, int i2) {
        synchronized (syncObj) {
            if (!isRunning()) {
                isRunning = true;
                if (this.j != null) {
                    this.j.onMoving(threeDPoint, threeDPoint2, f2, i2);
                }
                isRunning = false;
            }
        }
    }

    public void unRegSensorEvent() {
        if (isReg()) {
            a = false;
            this.i.unregisterListener(this);
        }
    }
}
