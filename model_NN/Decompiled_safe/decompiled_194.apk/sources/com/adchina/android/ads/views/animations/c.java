package com.adchina.android.ads.views.animations;

import android.view.animation.Animation;

final class c implements Animation.AnimationListener {
    private /* synthetic */ EnlargementAnimation a;

    c(EnlargementAnimation enlargementAnimation) {
        this.a = enlargementAnimation;
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.a.post(new d(this.a));
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
