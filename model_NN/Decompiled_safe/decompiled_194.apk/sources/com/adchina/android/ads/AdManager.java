package com.adchina.android.ads;

public class AdManager {
    public static final String DEFAULT_ADSPACE_ID = "69327";
    public static final String DEFAULT_FULLSCREEN_ADSPACE_ID = "70213";
    public static final String DEFAULT_VIDEO_ADSPACE_ID = "70804";
    public static final String SDK_DATE = "20110303";
    public static final String SDK_NAME = "AdChina_Android_SDK";
    public static final String SDK_VERSION = "2.2.3";
    private static boolean a = false;
    private static boolean b = false;
    private static StringBuffer c = new StringBuffer();
    private static StringBuffer d = new StringBuffer();
    private static StringBuffer e = new StringBuffer();
    private static StringBuffer f = new StringBuffer();
    private static StringBuffer g = new StringBuffer();
    private static StringBuffer h = new StringBuffer("");
    private static StringBuffer i = new StringBuffer("");
    private static EGender j = EGender.EFemale;
    private static StringBuffer k = new StringBuffer();
    private static StringBuffer l = new StringBuffer();
    private static StringBuffer m = new StringBuffer();
    private static int n = 0;
    private static int o;
    private static int p;

    public enum EGender {
        EFemale,
        EMale
    }

    public static String getAdspaceId() {
        return c.toString();
    }

    public static int getAnimations() {
        return n;
    }

    public static StringBuffer getAppName() {
        return l;
    }

    public static String getBirthday() {
        return h.toString();
    }

    public static StringBuffer getContentTargeting() {
        return m;
    }

    public static boolean getDebugMode() {
        return a;
    }

    public static String getFullScreenAdspaceId() {
        return d.toString();
    }

    public static EGender getGender() {
        return j;
    }

    public static boolean getLogMode() {
        return b;
    }

    public static StringBuffer getPhoneUA() {
        return k;
    }

    public static String getPostalCode() {
        return i.toString();
    }

    public static String getResolution() {
        return g.toString();
    }

    public static String getTelephoneNumber() {
        return f.toString();
    }

    public static String getVideoAdspaceId() {
        return e.toString();
    }

    public static int getrCloseImg() {
        return o;
    }

    public static int getrLoadingImg() {
        return p;
    }

    public static void setAdspaceId(String str) {
        c.setLength(0);
        c.append(str);
    }

    public static void setAnimations(int i2) {
        n = i2;
    }

    public static void setAppName(String str) {
        l.setLength(0);
        l.append(str);
    }

    public static void setBirthday(String str) {
        h.setLength(0);
        h.append(str);
    }

    public static void setContentTargeting(String str) {
        m.setLength(0);
        m.append(str);
    }

    public static void setDebugMode(boolean z) {
        a = z;
    }

    public static void setFullScreenAdspaceId(String str) {
        d.setLength(0);
        d.append(str);
    }

    public static void setGender(EGender eGender) {
        j = eGender;
    }

    public static void setLogMode(boolean z) {
        b = z;
    }

    public static void setPhoneUA(String str) {
        if (k.length() == 0) {
            k.append(str);
        }
    }

    public static void setPostalCode(String str) {
        i.setLength(0);
        i.append(str);
    }

    public static void setResolution(String str) {
        g.setLength(0);
        g.append(str);
    }

    public static void setTelephoneNumber(String str) {
        f.setLength(0);
        f.append(str);
    }

    public static void setVideoAdspaceId(String str) {
        e.setLength(0);
        e.append(str);
    }

    public static void setrCloseImg(int i2) {
        o = i2;
    }

    public static void setrLoadingImg(int i2) {
        p = i2;
    }
}
