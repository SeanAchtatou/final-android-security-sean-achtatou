package com.adchina.android.ads.controllers;

import com.adchina.android.ads.AdEngine;
import com.adchina.android.ads.AdManager;
import java.util.TimerTask;

final class c extends TimerTask {
    private /* synthetic */ FullScreenAdController a;

    c(FullScreenAdController fullScreenAdController) {
        this.a = fullScreenAdController;
    }

    public final void run() {
        if (this.a.f == null || this.a.f.length() <= 0) {
            this.a.e.stop();
            return;
        }
        AdManager.setAdspaceId(this.a.f);
        this.a.e.setRecvAdStatus(AdEngine.ERecvAdStatus.ERefreshAd);
        this.a.e.a(true);
    }
}
