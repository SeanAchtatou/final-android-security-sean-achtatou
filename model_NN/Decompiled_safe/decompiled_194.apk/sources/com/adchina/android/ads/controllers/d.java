package com.adchina.android.ads.controllers;

final class d extends Thread {
    private String a;
    private /* synthetic */ VideoAdController b;

    public d(VideoAdController videoAdController, String str) {
        this.b = videoAdController;
        this.a = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00ad, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ae, code lost:
        r7 = r1;
        r1 = r0;
        r0 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r0 = "Failed to download video file, err = " + r0.toString();
        r8.b.logger.writeLog(r0);
        android.util.Log.e(com.adchina.android.ads.Common.KLogTag, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00dc, code lost:
        r8.b.mHttpEngine.closeStream(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00ea, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ec, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ed, code lost:
        r7 = r1;
        r1 = r0;
        r0 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ad A[ExcHandler: all (r1v12 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:7:0x0030] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r8 = this;
            java.lang.String r0 = r8.a
            if (r0 == 0) goto L_0x000c
            java.lang.String r0 = r8.a
            int r0 = r0.length()
            if (r0 != 0) goto L_0x000d
        L_0x000c:
            return
        L_0x000d:
            r0 = 0
            com.adchina.android.ads.controllers.VideoAdController r1 = r8.b     // Catch:{ Exception -> 0x00b9, all -> 0x00e5 }
            com.adchina.android.ads.AdLog r1 = r1.logger     // Catch:{ Exception -> 0x00b9, all -> 0x00e5 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b9, all -> 0x00e5 }
            java.lang.String r3 = "++ start to download video file"
            r2.<init>(r3)     // Catch:{ Exception -> 0x00b9, all -> 0x00e5 }
            java.lang.String r3 = r8.a     // Catch:{ Exception -> 0x00b9, all -> 0x00e5 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00b9, all -> 0x00e5 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00b9, all -> 0x00e5 }
            r1.writeLog(r2)     // Catch:{ Exception -> 0x00b9, all -> 0x00e5 }
            com.adchina.android.ads.controllers.VideoAdController r1 = r8.b     // Catch:{ Exception -> 0x00b9, all -> 0x00e5 }
            com.adchina.android.ads.HttpEngine r1 = r1.mHttpEngine     // Catch:{ Exception -> 0x00b9, all -> 0x00e5 }
            java.lang.String r2 = r8.a     // Catch:{ Exception -> 0x00b9, all -> 0x00e5 }
            java.io.InputStream r0 = r1.requestGet(r2)     // Catch:{ Exception -> 0x00b9, all -> 0x00e5 }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            java.lang.String r2 = "/sdcard/ad/video"
            r1.<init>(r2)     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            boolean r2 = r1.exists()     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            if (r2 != 0) goto L_0x0040
            r1.mkdirs()     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
        L_0x0040:
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            java.lang.String r4 = "yyyyMMddHHmmss"
            java.lang.String r4 = com.adchina.android.ads.Utils.getNowTime(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            r3.<init>(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            java.lang.String r4 = "video.tmp"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            r2.<init>(r1, r3)     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            boolean r1 = r2.exists()     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            if (r1 == 0) goto L_0x0067
            r2.delete()     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
        L_0x0067:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x007b, all -> 0x00ad }
            r1.<init>(r2)     // Catch:{ Exception -> 0x007b, all -> 0x00ad }
            r3 = 16384(0x4000, float:2.2959E-41)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x007b, all -> 0x00ad }
        L_0x0070:
            int r4 = r0.read(r3)     // Catch:{ Exception -> 0x007b, all -> 0x00ad }
            if (r4 <= 0) goto L_0x00a6
            r5 = 0
            r1.write(r3, r5, r4)     // Catch:{ Exception -> 0x007b, all -> 0x00ad }
            goto L_0x0070
        L_0x007b:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            boolean r1 = r2.exists()     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            if (r1 == 0) goto L_0x0088
            r2.delete()     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
        L_0x0088:
            com.adchina.android.ads.controllers.VideoAdController r1 = r8.b     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            r3 = 6
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            r5 = 0
            java.lang.String r6 = r8.a     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            r4[r5] = r6     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            r5 = 1
            java.lang.String r2 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            r4[r5] = r2     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            r1.sendMessage(r3, r4)     // Catch:{ Exception -> 0x00ec, all -> 0x00ad }
            com.adchina.android.ads.controllers.VideoAdController r1 = r8.b
            com.adchina.android.ads.HttpEngine r1 = r1.mHttpEngine
            r1.closeStream(r0)
            goto L_0x000c
        L_0x00a6:
            r1.flush()     // Catch:{ Exception -> 0x007b, all -> 0x00ad }
            r1.close()     // Catch:{ Exception -> 0x007b, all -> 0x00ad }
            goto L_0x0088
        L_0x00ad:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x00b1:
            com.adchina.android.ads.controllers.VideoAdController r2 = r8.b
            com.adchina.android.ads.HttpEngine r2 = r2.mHttpEngine
            r2.closeStream(r1)
            throw r0
        L_0x00b9:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x00bd:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ea }
            java.lang.String r3 = "Failed to download video file, err = "
            r2.<init>(r3)     // Catch:{ all -> 0x00ea }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00ea }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x00ea }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00ea }
            com.adchina.android.ads.controllers.VideoAdController r2 = r8.b     // Catch:{ all -> 0x00ea }
            com.adchina.android.ads.AdLog r2 = r2.logger     // Catch:{ all -> 0x00ea }
            r2.writeLog(r0)     // Catch:{ all -> 0x00ea }
            java.lang.String r2 = "AdChinaError"
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00ea }
            com.adchina.android.ads.controllers.VideoAdController r0 = r8.b
            com.adchina.android.ads.HttpEngine r0 = r0.mHttpEngine
            r0.closeStream(r1)
            goto L_0x000c
        L_0x00e5:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x00b1
        L_0x00ea:
            r0 = move-exception
            goto L_0x00b1
        L_0x00ec:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x00bd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.controllers.d.run():void");
    }
}
