package com.adchina.android.ads;

import com.adchina.android.ads.views.AdView;
import com.adchina.android.ads.views.FullScreenAdView;

public interface AdListener {
    boolean OnRecvSms(AdView adView, String str);

    void onFailedToPlayVideoAd();

    void onFailedToReceiveAd(AdView adView);

    void onFailedToReceiveFullScreenAd(FullScreenAdView fullScreenAdView);

    void onFailedToReceiveVideoAd();

    void onFailedToRefreshAd(AdView adView);

    void onPlayVideoAd();

    void onReceiveAd(AdView adView);

    void onReceiveFullScreenAd(FullScreenAdView fullScreenAdView);

    void onRefreshAd(AdView adView);
}
