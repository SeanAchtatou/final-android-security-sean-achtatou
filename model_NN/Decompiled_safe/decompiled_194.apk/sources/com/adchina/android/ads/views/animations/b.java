package com.adchina.android.ads.views.animations;

import android.view.animation.DecelerateInterpolator;
import com.adchina.android.ads.views.ContentView;

final class b implements Runnable {
    private final /* synthetic */ float a;
    private final /* synthetic */ float b;
    private final /* synthetic */ ContentView c;

    b(a aVar, float f, float f2, ContentView contentView) {
        this.a = f;
        this.b = f2;
        this.c = contentView;
    }

    public final void run() {
        Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(-90.0f, 0.0f, this.a, this.b, 310.0f, false);
        rotate3dAnimation.setDuration(750);
        rotate3dAnimation.setFillAfter(true);
        rotate3dAnimation.setInterpolator(new DecelerateInterpolator());
        this.c.startAnimation(rotate3dAnimation);
    }
}
