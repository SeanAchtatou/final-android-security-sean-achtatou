package com.adchina.android.ads.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.adchina.android.ads.GifEngine;
import com.adchina.android.ads.controllers.AdViewController;
import com.adchina.android.ads.views.animations.AnimationManager;
import com.smaato.SOMA.SOMATextBanner;

public final class ContentView extends LinearLayout {
    private StringBuffer a = new StringBuffer();
    private int b;
    private int c;
    private Bitmap d;
    private Paint e;
    private Typeface f;
    private GifEngine g;
    private GifImageView h;
    /* access modifiers changed from: private */
    public AdView i;
    /* access modifiers changed from: private */
    public AdViewController j;

    public ContentView(Context context) {
        super(context);
        new Matrix();
        e eVar = e.ENONE;
        this.b = -1;
        this.c = SOMATextBanner.DEFAULT_BACKGROUND_COLOR;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        a(context);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setClickable(true);
    }

    public ContentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new Matrix();
        e eVar = e.ENONE;
        this.b = -1;
        this.c = SOMATextBanner.DEFAULT_BACKGROUND_COLOR;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        a(context);
    }

    private void a(Context context) {
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.f = Typeface.create("宋体", 1);
        this.h = new GifImageView(context);
        this.h.setScaleType(ImageView.ScaleType.FIT_CENTER);
        addView(this.h, new LinearLayout.LayoutParams(-1, -1, 0.0f));
        this.h.setOnTouchListener(new d(this));
    }

    /* access modifiers changed from: protected */
    public final void applyAnimation(Bitmap bitmap) {
        if (bitmap != null) {
            AnimationManager.startAnimation(this);
        }
    }

    public final void setAdView(AdView adView) {
        this.i = adView;
    }

    public final void setBackgroundColor(int i2) {
        this.c = i2;
        this.h.setBackgroundColor(i2);
    }

    /* access modifiers changed from: protected */
    public final void setContent(Bitmap bitmap) {
        this.d = bitmap;
        e eVar = e.EIMG;
        this.h.setBmp(this.d);
        applyAnimation(this.d);
    }

    /* access modifiers changed from: protected */
    public final void setContent(String str) {
        this.a.setLength(0);
        this.a.append(str);
        e eVar = e.ETXT;
        if (this.e == null) {
            this.e = new Paint();
        }
        Rect rect = new Rect();
        getHitRect(rect);
        Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        canvas.setBitmap(createBitmap);
        this.e.setColor(this.c);
        this.e.setStyle(Paint.Style.FILL);
        canvas.drawRect(rect, this.e);
        this.e.setColor(this.b);
        this.e.setTypeface(this.f);
        canvas.drawText(this.a.toString(), (((float) rect.width()) - this.e.measureText(str)) / 2.0f, (float) (rect.height() / 2), this.e);
        setContent(createBitmap);
    }

    /* access modifiers changed from: protected */
    public final void setContent(byte[] bArr) {
        this.g = GifEngine.CreateGifImage(bArr);
        this.h.setGif(this.g);
        e eVar = e.EGIF;
        applyAnimation(this.h.getImageBitmap());
    }

    public final void setController(AdViewController adViewController) {
        this.j = adViewController;
    }

    /* access modifiers changed from: protected */
    public final void setDefaultImage(Bitmap bitmap) {
        this.d = bitmap;
        e eVar = e.EIMG;
        this.h.setImageBitmap(this.d);
    }

    public final void setFont(Typeface typeface) {
        this.f = typeface;
    }

    public final void setPenColor(int i2) {
        this.b = i2;
    }
}
