package com.adchina.android.ads.controllers;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.adchina.android.ads.AdEngine;
import com.adchina.android.ads.AdListener;
import com.adchina.android.ads.AdLog;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.Common;
import com.adchina.android.ads.HttpEngine;
import com.adchina.android.ads.Utils;
import com.adchina.android.ads.XmlEngine;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.LinkedList;

public abstract class BaseController implements Handler.Callback {
    protected AdLog logger;
    protected StringBuffer mAdClickUrl = new StringBuffer();
    protected AdListener mAdListener = null;
    protected Common.EAdModel mAdModel = Common.EAdModel.EAdNONE;
    protected StringBuffer mAdType = new StringBuffer();
    protected StringBuffer mAdspaceId = new StringBuffer();
    protected StringBuffer mBtnCallNumber = new StringBuffer();
    protected StringBuffer mBtnDownloadUrl = new StringBuffer();
    protected StringBuffer mBtnH = new StringBuffer();
    protected StringBuffer mBtnSmsContent = new StringBuffer();
    protected StringBuffer mBtnSmsNumber = new StringBuffer();
    protected StringBuffer mBtnThdClkTrack = new StringBuffer();
    protected LinkedList mBtnThdClkTrackList = new LinkedList();
    protected StringBuffer mBtnType = new StringBuffer();
    protected StringBuffer mBtnW = new StringBuffer();
    protected StringBuffer mBtnX = new StringBuffer();
    protected StringBuffer mBtnY = new StringBuffer();
    protected AdEngine.EClkTrack mClickStatus = AdEngine.EClkTrack.EIdle;
    protected a mClkTrackThread;
    protected Context mContext;
    protected StringBuffer mFC = new StringBuffer();
    protected StringBuffer mFullScreenClkUrl = new StringBuffer();
    protected StringBuffer mFullScreenImgAddr = new StringBuffer();
    protected StringBuffer mFullScreenThdClkTrack = new StringBuffer();
    protected StringBuffer mFullScreenThdImpTrack = new StringBuffer();
    protected boolean mHasAd = false;
    protected HttpEngine mHttpEngine = null;
    protected StringBuffer mIMEI = new StringBuffer();
    protected StringBuffer mImgAddr = new StringBuffer();
    protected Handler mMsgHandler;
    protected StringBuffer mPhoneModel = new StringBuffer();
    protected StringBuffer mPhoneNumber = new StringBuffer();
    protected b mRecvAdThread;
    protected StringBuffer mRefTime = new StringBuffer();
    protected boolean mRefreshAd = false;
    protected StringBuffer mReportBaseUrl = new StringBuffer();
    protected AdEngine.ERecvAdStatus mRunState = AdEngine.ERecvAdStatus.EReceiveAd;
    protected StringBuffer mSmsAddr = new StringBuffer();
    protected StringBuffer mSmsContent = new StringBuffer();
    protected StringBuffer mSmsType = new StringBuffer();
    protected boolean mStart = false;
    protected boolean mStop = false;
    protected StringBuffer mThdClkTrack = new StringBuffer();
    protected LinkedList mThdClkTrackList = new LinkedList();
    protected StringBuffer mThdImpTrack = new StringBuffer();
    protected LinkedList mThdImpTrackList = new LinkedList();
    protected StringBuffer mTxtLnkText = new StringBuffer();
    protected StringBuffer mVideoAddr = new StringBuffer();
    protected StringBuffer mVideoClosedTrack = new StringBuffer();
    protected StringBuffer mVideoEndedTrack = new StringBuffer();
    protected StringBuffer mVideoStartedTrack = new StringBuffer();
    protected StringBuffer mXmlVer = new StringBuffer();

    public BaseController(Context context) {
        this.mContext = context;
        initView(context);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        if (this.logger != null) {
            this.logger.uninitLog();
            this.logger.uninitDebugLog();
        }
    }

    public AdLog getLogger() {
        return this.logger;
    }

    public boolean hasAd() {
        return this.mHasAd;
    }

    /* access modifiers changed from: protected */
    public void initView(Context context) {
        this.mHttpEngine = new HttpEngine(context);
        this.mMsgHandler = new Handler(this);
    }

    public void loadDebugCfg() {
        File file = new File(Common.KDebugCFGFilename);
        if (file.exists()) {
            AdManager.setDebugMode(true);
            AdManager.setLogMode(true);
            try {
                InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(file));
                char[] cArr = new char[((int) file.length())];
                inputStreamReader.read(cArr);
                String[] split = new String(cArr).replace("\r", "").split("\n");
                for (String str : split) {
                    int indexOf = str.indexOf(58);
                    String substring = str.substring(0, indexOf);
                    String substring2 = str.substring(indexOf + 1);
                    if (substring.equals("testurl")) {
                        Common.KAdDebugSvrUrlFormat = String.valueOf(substring2.trim()) + Common.KAdUrlParasFormat;
                    } else if (substring.equals("bannerAdSpaceId")) {
                        AdManager.setAdspaceId(substring2.trim());
                    } else if (substring.equals("fullScreenAdSpaceId")) {
                        AdManager.setFullScreenAdspaceId(substring2.trim());
                    } else if (substring.equals("videoPreAdSpaceId")) {
                        AdManager.setVideoAdspaceId(substring2.trim());
                    }
                }
            } catch (FileNotFoundException e) {
            } catch (IOException e2) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void readAdserverInfo(InputStream inputStream) {
        this.logger.writeLog("++ readAdserverInfo");
        XmlEngine xmlEngine = new XmlEngine();
        xmlEngine.setLogger(this.logger);
        xmlEngine.parseXmlStream(inputStream);
        resetAdserverInfo();
        xmlEngine.readXmlInfo(this.mXmlVer, this.mAdType, this.mAdClickUrl, this.mReportBaseUrl, this.mThdImpTrack, this.mThdClkTrack, this.mFC, this.mRefTime, this.mSmsAddr, this.mSmsContent, this.mSmsType, this.mImgAddr, this.mTxtLnkText, this.mBtnType, this.mBtnX, this.mBtnY, this.mBtnW, this.mBtnH, this.mBtnThdClkTrack, this.mBtnDownloadUrl, this.mBtnSmsNumber, this.mBtnSmsContent, this.mBtnCallNumber, this.mFullScreenImgAddr, this.mFullScreenClkUrl, this.mFullScreenThdImpTrack, this.mFullScreenThdClkTrack, this.mVideoAddr, this.mVideoStartedTrack, this.mVideoEndedTrack, this.mVideoClosedTrack);
        if (this.mAdType.toString().compareToIgnoreCase("txt") == 0) {
            this.mAdModel = Common.EAdModel.EAdTXT;
        } else if (this.mAdType.toString().compareToIgnoreCase("jpg") == 0) {
            this.mAdModel = Common.EAdModel.EAdJPG;
        } else if (this.mAdType.toString().compareToIgnoreCase("png") == 0) {
            this.mAdModel = Common.EAdModel.EAdPNG;
        } else if (this.mAdType.toString().compareToIgnoreCase("gif") == 0) {
            this.mAdModel = Common.EAdModel.EAdGIF;
        } else {
            this.mAdModel = Common.EAdModel.EAdNONE;
        }
        writeFcParams(this.mFC.toString());
        this.logger.writeLog(((Object) this.mAdType) + " : ");
        this.logger.writeLog("ImpUrl:" + this.mThdImpTrack.toString());
        this.logger.writeLog("ClkUrl:" + this.mAdClickUrl.toString());
        this.logger.writeLog("FC:" + this.mFC.toString());
        this.logger.writeLog("FullScreenImgAddr:" + this.mFullScreenImgAddr.toString());
        this.logger.writeLog("VideoAddr:" + this.mVideoAddr.toString());
        if (!(Common.EAdModel.EAdTXT == this.mAdModel || Common.EAdModel.EAdNONE == this.mAdModel)) {
            this.logger.writeLog("ImgAddr:" + this.mImgAddr.toString());
        }
        this.logger.writeLog("-- readAdserverInfo");
    }

    /* access modifiers changed from: protected */
    public String readFcParams() {
        return "";
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004a A[SYNTHETIC, Splitter:B:17:0x004a] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004f A[Catch:{ IOException -> 0x0054 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0074 A[SYNTHETIC, Splitter:B:27:0x0074] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0079 A[Catch:{ IOException -> 0x007d }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00c9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String readFcParams(java.lang.String r9) {
        /*
            r8 = this;
            r6 = 0
            android.content.Context r0 = r8.mContext     // Catch:{ Exception -> 0x002d, all -> 0x006f }
            java.io.File r0 = r0.getFileStreamPath(r9)     // Catch:{ Exception -> 0x002d, all -> 0x006f }
            long r0 = r0.length()     // Catch:{ Exception -> 0x002d, all -> 0x006f }
            int r0 = (int) r0     // Catch:{ Exception -> 0x002d, all -> 0x006f }
            char[] r0 = new char[r0]     // Catch:{ Exception -> 0x002d, all -> 0x006f }
            android.content.Context r1 = r8.mContext     // Catch:{ Exception -> 0x002d, all -> 0x006f }
            java.lang.String r2 = "adchinaFC.fc"
            java.io.FileInputStream r1 = r1.openFileInput(r2)     // Catch:{ Exception -> 0x002d, all -> 0x006f }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00be, all -> 0x00b3 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x00be, all -> 0x00b3 }
            r2.read(r0)     // Catch:{ Exception -> 0x00c3, all -> 0x00b7 }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x00c3, all -> 0x00b7 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x00c3, all -> 0x00b7 }
            r2.close()     // Catch:{ IOException -> 0x0097 }
            if (r1 == 0) goto L_0x00b0
            r1.close()     // Catch:{ IOException -> 0x0097 }
            r0 = r3
        L_0x002c:
            return r0
        L_0x002d:
            r0 = move-exception
            r1 = r6
            r2 = r6
        L_0x0030:
            com.adchina.android.ads.AdLog r3 = r8.logger     // Catch:{ all -> 0x00bc }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bc }
            java.lang.String r5 = "Exceptions in getFcParams 1, err = "
            r4.<init>(r5)     // Catch:{ all -> 0x00bc }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00bc }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x00bc }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00bc }
            r3.writeLog(r0)     // Catch:{ all -> 0x00bc }
            if (r1 == 0) goto L_0x004d
            r1.close()     // Catch:{ IOException -> 0x0054 }
        L_0x004d:
            if (r2 == 0) goto L_0x00c9
            r2.close()     // Catch:{ IOException -> 0x0054 }
            r0 = r6
            goto L_0x002c
        L_0x0054:
            r0 = move-exception
            com.adchina.android.ads.AdLog r1 = r8.logger
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Exceptions in getFcParams 2, err = "
            r2.<init>(r3)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.writeLog(r0)
            r0 = r6
            goto L_0x002c
        L_0x006f:
            r0 = move-exception
            r1 = r6
            r2 = r6
        L_0x0072:
            if (r1 == 0) goto L_0x0077
            r1.close()     // Catch:{ IOException -> 0x007d }
        L_0x0077:
            if (r2 == 0) goto L_0x007c
            r2.close()     // Catch:{ IOException -> 0x007d }
        L_0x007c:
            throw r0
        L_0x007d:
            r1 = move-exception
            com.adchina.android.ads.AdLog r2 = r8.logger
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Exceptions in getFcParams 2, err = "
            r3.<init>(r4)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.writeLog(r1)
            goto L_0x007c
        L_0x0097:
            r0 = move-exception
            com.adchina.android.ads.AdLog r1 = r8.logger
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r4 = "Exceptions in getFcParams 2, err = "
            r2.<init>(r4)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.writeLog(r0)
        L_0x00b0:
            r0 = r3
            goto L_0x002c
        L_0x00b3:
            r0 = move-exception
            r2 = r1
            r1 = r6
            goto L_0x0072
        L_0x00b7:
            r0 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x0072
        L_0x00bc:
            r0 = move-exception
            goto L_0x0072
        L_0x00be:
            r0 = move-exception
            r2 = r1
            r1 = r6
            goto L_0x0030
        L_0x00c3:
            r0 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x0030
        L_0x00c9:
            r0 = r6
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.controllers.BaseController.readFcParams(java.lang.String):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public void resetAdserverInfo() {
        this.mXmlVer.setLength(0);
        this.mAdType.setLength(0);
        this.mImgAddr.setLength(0);
        this.mAdClickUrl.setLength(0);
        this.mReportBaseUrl.setLength(0);
        this.mThdImpTrack.setLength(0);
        this.mThdClkTrack.setLength(0);
        this.mFC.setLength(0);
        this.mRefTime.setLength(0);
        this.mTxtLnkText.setLength(0);
        this.mSmsType.setLength(0);
        this.mThdClkTrackList.clear();
        this.mThdImpTrackList.clear();
        this.mBtnThdClkTrackList.clear();
        this.mBtnType.setLength(0);
        this.mBtnX.setLength(0);
        this.mBtnY.setLength(0);
        this.mBtnW.setLength(0);
        this.mBtnH.setLength(0);
        this.mBtnThdClkTrack.setLength(0);
        this.mBtnDownloadUrl.setLength(0);
        this.mBtnSmsNumber.setLength(0);
        this.mBtnSmsContent.setLength(0);
        this.mBtnCallNumber.setLength(0);
        this.mFullScreenImgAddr.setLength(0);
        this.mFullScreenClkUrl.setLength(0);
        this.mFullScreenThdImpTrack.setLength(0);
        this.mFullScreenThdClkTrack.setLength(0);
        this.mVideoAddr.setLength(0);
        this.mVideoStartedTrack.setLength(0);
        this.mVideoEndedTrack.setLength(0);
        this.mVideoClosedTrack.setLength(0);
    }

    public void runClkTrackJob() {
    }

    public void runRecvAdThreadJob() {
    }

    /* access modifiers changed from: protected */
    public void sendMessage(int i, Object obj) {
        Message message = new Message();
        message.what = i;
        message.obj = obj;
        this.mMsgHandler.sendMessage(message);
    }

    public void setAdListener(AdListener adListener) {
        this.mAdListener = adListener;
    }

    /* access modifiers changed from: protected */
    public void setClkTrackStatus(AdEngine.EClkTrack eClkTrack) {
        this.mClickStatus = eClkTrack;
    }

    /* access modifiers changed from: protected */
    public void setRecvAdStatus(AdEngine.ERecvAdStatus eRecvAdStatus) {
        this.mRunState = eRecvAdStatus;
    }

    /* access modifiers changed from: protected */
    public String setupAdserverUrl() {
        String readFcParams = readFcParams();
        if (!(readFcParams == null || -1 == readFcParams.indexOf(" "))) {
            readFcParams = "";
        }
        this.logger.writeLog("FC:" + readFcParams);
        setupAdspaceId();
        return String.valueOf(String.format(Common.KAdSvrUrlFormat, this.mAdspaceId, Integer.valueOf(Utils.GetRandomNumber()), readFcParams, this.mIMEI.toString(), URLEncoder.encode(AdManager.getAppName().toString(), Common.KEnc), URLEncoder.encode(AdManager.getContentTargeting().toString(), Common.KEnc), URLEncoder.encode(AdManager.getPhoneUA().toString(), Common.KEnc), URLEncoder.encode(Build.BRAND, Common.KEnc))) + setupMaParams();
    }

    /* access modifiers changed from: protected */
    public String setupAdserverUrlFromConfig() {
        String readFcParams = readFcParams();
        if (!(readFcParams == null || -1 == readFcParams.indexOf(" "))) {
            readFcParams = "";
        }
        this.logger.writeLog("FC:" + readFcParams);
        setupAdspaceId();
        return String.valueOf(String.format(Common.KAdDebugSvrUrlFormat, this.mAdspaceId, Integer.valueOf(Utils.GetRandomNumber()), readFcParams, this.mIMEI.toString(), URLEncoder.encode(AdManager.getAppName().toString(), Common.KEnc), URLEncoder.encode(AdManager.getContentTargeting().toString(), Common.KEnc), URLEncoder.encode(AdManager.getPhoneUA().toString(), Common.KEnc), URLEncoder.encode(Build.BRAND, Common.KEnc))) + setupMaParams();
    }

    /* access modifiers changed from: protected */
    public void setupAdspaceId() {
        this.mAdspaceId.setLength(0);
        this.mAdspaceId.append(AdManager.getAdspaceId());
    }

    /* access modifiers changed from: protected */
    public String setupMaParams() {
        try {
            StringBuilder sb = new StringBuilder(String.valueOf(""));
            Object[] objArr = new Object[8];
            objArr[0] = AdManager.SDK_VERSION;
            objArr[1] = "Android%20" + Build.VERSION.RELEASE;
            objArr[2] = this.mPhoneNumber.toString();
            objArr[3] = this.mPhoneModel.toString();
            objArr[4] = AdManager.getResolution();
            objArr[5] = AdManager.getGender() == AdManager.EGender.EFemale ? Common.KIMP : Common.KCLK;
            objArr[6] = AdManager.getPostalCode();
            objArr[7] = AdManager.getBirthday();
            return sb.append(String.format(Common.KMaParamsFormat, objArr)).toString();
        } catch (Exception e) {
            String str = "Failed to getMaParams, err = " + e.toString();
            this.logger.writeLog(str);
            Log.e(Common.KLogTag, str);
            return "";
        }
    }

    public void start() {
        if (!this.mStart) {
            this.mStop = false;
            loadDebugCfg();
            TelephonyManager telephonyManager = (TelephonyManager) this.mContext.getSystemService("phone");
            String deviceId = telephonyManager.getDeviceId();
            String line1Number = telephonyManager.getLine1Number();
            if (deviceId != null) {
                this.mIMEI.setLength(0);
                this.mIMEI.append(deviceId);
            }
            if (line1Number != null) {
                this.mPhoneNumber.setLength(0);
                this.mPhoneNumber.append(line1Number);
            }
            setupAdspaceId();
            this.mPhoneModel.setLength(0);
            this.mPhoneModel.append(Build.MODEL.replaceAll(" ", "%20"));
            this.logger = new AdLog();
            this.logger.initLog("AdChina_" + ((Object) this.mAdspaceId) + ".txt");
            this.logger.initDebugLog("AdChinaOutput_" + ((Object) this.mAdspaceId) + ".txt");
            this.mHttpEngine.setLogger(this.logger);
            setRecvAdStatus(AdEngine.ERecvAdStatus.EReceiveAd);
            this.mRecvAdThread = new b(this);
            this.mRecvAdThread.start();
            this.mClkTrackThread = new a(this);
            this.mClkTrackThread.start();
            this.mStart = true;
        }
    }

    public void stop() {
        this.mStop = true;
        this.mStart = false;
    }

    /* access modifiers changed from: protected */
    public void writeFcParams(String str) {
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004d A[SYNTHETIC, Splitter:B:16:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0052 A[Catch:{ IOException -> 0x0056 }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005d A[SYNTHETIC, Splitter:B:24:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0062 A[Catch:{ IOException -> 0x0068 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void writeFcParams(java.lang.String r8, java.lang.String r9) {
        /*
            r7 = this;
            r3 = 0
            com.adchina.android.ads.AdLog r0 = r7.logger
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "saveFcParams : "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r1 = r1.toString()
            r0.writeLog(r1)
            android.content.Context r0 = r7.mContext     // Catch:{ Exception -> 0x0030, all -> 0x0058 }
            r1 = 0
            java.io.FileOutputStream r0 = r0.openFileOutput(r9, r1)     // Catch:{ Exception -> 0x0030, all -> 0x0058 }
            java.io.OutputStreamWriter r1 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x0076, all -> 0x006a }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0076, all -> 0x006a }
            r1.write(r8)     // Catch:{ Exception -> 0x007b, all -> 0x006f }
            r1.flush()     // Catch:{ Exception -> 0x007b, all -> 0x006f }
            r1.close()     // Catch:{ IOException -> 0x0066 }
            if (r0 == 0) goto L_0x002f
            r0.close()     // Catch:{ IOException -> 0x0066 }
        L_0x002f:
            return
        L_0x0030:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0033:
            com.adchina.android.ads.AdLog r3 = r7.logger     // Catch:{ all -> 0x0074 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0074 }
            java.lang.String r5 = "Exceptions in saveFcParams, err = "
            r4.<init>(r5)     // Catch:{ all -> 0x0074 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0074 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0074 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0074 }
            r3.writeLog(r0)     // Catch:{ all -> 0x0074 }
            if (r1 == 0) goto L_0x0050
            r1.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0050:
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x0056 }
            goto L_0x002f
        L_0x0056:
            r0 = move-exception
            goto L_0x002f
        L_0x0058:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x005b:
            if (r1 == 0) goto L_0x0060
            r1.close()     // Catch:{ IOException -> 0x0068 }
        L_0x0060:
            if (r2 == 0) goto L_0x0065
            r2.close()     // Catch:{ IOException -> 0x0068 }
        L_0x0065:
            throw r0
        L_0x0066:
            r0 = move-exception
            goto L_0x002f
        L_0x0068:
            r1 = move-exception
            goto L_0x0065
        L_0x006a:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x005b
        L_0x006f:
            r2 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
            goto L_0x005b
        L_0x0074:
            r0 = move-exception
            goto L_0x005b
        L_0x0076:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x0033
        L_0x007b:
            r2 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.controllers.BaseController.writeFcParams(java.lang.String, java.lang.String):void");
    }
}
