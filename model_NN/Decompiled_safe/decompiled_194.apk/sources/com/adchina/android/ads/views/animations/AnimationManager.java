package com.adchina.android.ads.views.animations;

import android.view.animation.AccelerateInterpolator;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.views.ContentView;
import com.adchina.android.ads.views.animations.MoveAndBounceAnimation;
import java.util.ArrayList;

public class AnimationManager {
    public static final int ANY_ANIMATION = 0;
    public static final int ENLARGEMENT_ANIMATION = 16;
    public static final int FADEIN_ANIMATION = 8;
    public static final int FALLDOWN_ANIMATION = 4;
    public static final int ROTATE_3D_ANIMATION = 1;
    public static final int RUNIN_ANIMATION = 2;
    private static ArrayList a;

    public static void startAnimation(ContentView contentView) {
        if (a == null) {
            a = new ArrayList();
            int animations = AdManager.getAnimations();
            if (animations <= 0) {
                animations = 31;
            }
            if ((animations & 1) > 0) {
                a.add(1);
            }
            if ((animations & 2) > 0) {
                a.add(2);
            }
            if ((animations & 4) > 0) {
                a.add(4);
            }
            if ((animations & 8) > 0) {
                a.add(8);
            }
            if ((animations & 16) > 0) {
                a.add(16);
            }
        }
        if (a.size() != 0) {
            int random = ((int) (Math.random() * 10000.0d)) % a.size();
            if (contentView.getWidth() > 0 && contentView.getHeight() > 0) {
                switch (((Integer) a.get(random)).intValue()) {
                    case 1:
                        float width = ((float) contentView.getWidth()) / 2.0f;
                        float height = ((float) contentView.getHeight()) / 2.0f;
                        Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(0.0f, 90.0f, width, height, 310.0f, true);
                        rotate3dAnimation.setDuration(750);
                        rotate3dAnimation.setFillAfter(true);
                        rotate3dAnimation.setInterpolator(new AccelerateInterpolator());
                        rotate3dAnimation.setAnimationListener(new a(contentView, width, height));
                        contentView.startAnimation(rotate3dAnimation);
                        return;
                    case 2:
                        try {
                            MoveAndBounceAnimation.Orientation orientation = MoveAndBounceAnimation.Orientation.EHorizontal;
                            int[] iArr = new int[6];
                            iArr[0] = contentView.getWidth();
                            iArr[1] = -60;
                            iArr[2] = 50;
                            iArr[3] = -40;
                            iArr[4] = 30;
                            MoveAndBounceAnimation moveAndBounceAnimation = new MoveAndBounceAnimation(orientation, iArr);
                            moveAndBounceAnimation.setDuration(1500);
                            moveAndBounceAnimation.setFillAfter(true);
                            moveAndBounceAnimation.setInterpolator(new AccelerateInterpolator());
                            contentView.startAnimation(moveAndBounceAnimation);
                            return;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return;
                        }
                    case 4:
                        try {
                            MoveAndBounceAnimation.Orientation orientation2 = MoveAndBounceAnimation.Orientation.EVertical;
                            int[] iArr2 = new int[6];
                            iArr2[0] = -contentView.getHeight();
                            iArr2[1] = 40;
                            iArr2[2] = -30;
                            iArr2[3] = 20;
                            iArr2[4] = -10;
                            MoveAndBounceAnimation moveAndBounceAnimation2 = new MoveAndBounceAnimation(orientation2, iArr2);
                            moveAndBounceAnimation2.setDuration(1500);
                            moveAndBounceAnimation2.setFillAfter(true);
                            moveAndBounceAnimation2.setInterpolator(new AccelerateInterpolator());
                            contentView.startAnimation(moveAndBounceAnimation2);
                            return;
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            return;
                        }
                    case 8:
                        new FadeinAnimation(contentView).startAnimation();
                        return;
                    case 16:
                        new EnlargementAnimation(contentView).startAnimation();
                        return;
                    default:
                        return;
                }
            }
        }
    }
}
