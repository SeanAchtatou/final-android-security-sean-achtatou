package com.adchina.android.ads.views.animations;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.ScaleAnimation;

final class f implements Runnable {
    private /* synthetic */ EnlargementAnimation a;

    f(EnlargementAnimation enlargementAnimation) {
        this.a = enlargementAnimation;
    }

    public final void run() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.8f, 1.0f, 0.8f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(300);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setInterpolator(new AccelerateInterpolator());
        this.a.a.startAnimation(scaleAnimation);
    }
}
