package com.adchina.android.ads.controllers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import cn.domob.android.ads.DomobAdManager;
import com.adchina.android.ads.AdEngine;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.Common;
import com.adchina.android.ads.Utils;
import com.adchina.android.ads.views.AdView;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import org.xmlpull.v1.XmlPullParserException;

public class AdViewController extends BaseController {
    private static /* synthetic */ int[] d;
    private static /* synthetic */ int[] e;
    private LinkedList a = new LinkedList();
    private HashMap b = new HashMap();
    private boolean c = true;

    public AdViewController(Context context) {
        super(context);
    }

    private void b() {
        InputStream inputStream;
        this.logger.writeLog("++ onGetImgMaterial");
        setRecvAdStatus(AdEngine.ERecvAdStatus.EIdle);
        FileInputStream fileInputStream = null;
        try {
            String stringBuffer = this.mImgAddr.toString();
            if (Common.EAdModel.EAdNONE == this.mAdModel || Common.EAdModel.EAdTXT == this.mAdModel) {
                inputStream = null;
            } else if (!this.b.containsKey(stringBuffer)) {
                inputStream = downLoadAndSaveImg(stringBuffer);
                try {
                    this.logger.writeLog("++ load remote banner img");
                } catch (Exception e2) {
                    Exception exc = e2;
                    fileInputStream = inputStream;
                    e = exc;
                    try {
                        String str = "Failed to get ad material, err = " + e.toString();
                        sendMessage(2, str);
                        this.logger.writeLog(str);
                        Log.e(Common.KLogTag, str);
                        this.mHttpEngine.closeStream(fileInputStream);
                        this.logger.writeLog("-- onGetImgMaterial, AdModel = " + this.mAdModel);
                    } catch (Throwable th) {
                        th = th;
                        this.mHttpEngine.closeStream(fileInputStream);
                        this.logger.writeLog("-- onGetImgMaterial, AdModel = " + this.mAdModel);
                        throw th;
                    }
                } catch (Throwable th2) {
                    Throwable th3 = th2;
                    fileInputStream = inputStream;
                    th = th3;
                    this.mHttpEngine.closeStream(fileInputStream);
                    this.logger.writeLog("-- onGetImgMaterial, AdModel = " + this.mAdModel);
                    throw th;
                }
            } else {
                FileInputStream fileInputStream2 = new FileInputStream((String) this.b.get(stringBuffer));
                try {
                    this.logger.writeLog("++ load local banner img");
                    inputStream = fileInputStream2;
                } catch (Exception e3) {
                    e = e3;
                    fileInputStream = fileInputStream2;
                    String str2 = "Failed to get ad material, err = " + e.toString();
                    sendMessage(2, str2);
                    this.logger.writeLog(str2);
                    Log.e(Common.KLogTag, str2);
                    this.mHttpEngine.closeStream(fileInputStream);
                    this.logger.writeLog("-- onGetImgMaterial, AdModel = " + this.mAdModel);
                } catch (Throwable th4) {
                    th = th4;
                    fileInputStream = fileInputStream2;
                    this.mHttpEngine.closeStream(fileInputStream);
                    this.logger.writeLog("-- onGetImgMaterial, AdModel = " + this.mAdModel);
                    throw th;
                }
            }
            try {
                if (Common.EAdModel.EAdJPG == this.mAdModel) {
                    Bitmap convertStreamToBitmap = Utils.convertStreamToBitmap(inputStream);
                    if (convertStreamToBitmap != null) {
                        sendMessage(1, convertStreamToBitmap);
                    } else {
                        sendMessage(2, "JPG AdMaterial is null");
                    }
                } else if (Common.EAdModel.EAdPNG == this.mAdModel) {
                    Bitmap convertStreamToBitmap2 = Utils.convertStreamToBitmap(inputStream);
                    if (convertStreamToBitmap2 != null) {
                        sendMessage(1, convertStreamToBitmap2);
                    } else {
                        sendMessage(2, "PNG AdMaterial is null");
                    }
                } else if (Common.EAdModel.EAdGIF == this.mAdModel) {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    while (true) {
                        int read = inputStream.read();
                        if (read == -1) {
                            break;
                        }
                        byteArrayOutputStream.write(read);
                    }
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    if (byteArray != null) {
                        sendMessage(1, byteArray);
                    }
                    byteArrayOutputStream.close();
                }
                this.mHttpEngine.closeStream(inputStream);
                this.logger.writeLog("-- onGetImgMaterial, AdModel = " + this.mAdModel);
            } catch (Exception e4) {
                Exception exc2 = e4;
                fileInputStream = inputStream;
                e = exc2;
            } catch (Throwable th5) {
                Throwable th6 = th5;
                fileInputStream = inputStream;
                th = th6;
                this.mHttpEngine.closeStream(fileInputStream);
                this.logger.writeLog("-- onGetImgMaterial, AdModel = " + this.mAdModel);
                throw th;
            }
        } catch (Exception e5) {
            e = e5;
            String str22 = "Failed to get ad material, err = " + e.toString();
            sendMessage(2, str22);
            this.logger.writeLog(str22);
            Log.e(Common.KLogTag, str22);
            this.mHttpEngine.closeStream(fileInputStream);
            this.logger.writeLog("-- onGetImgMaterial, AdModel = " + this.mAdModel);
        }
    }

    private static /* synthetic */ int[] c() {
        int[] iArr = d;
        if (iArr == null) {
            iArr = new int[AdEngine.ERecvAdStatus.values().length];
            try {
                iArr[AdEngine.ERecvAdStatus.EGetFullScreenImgMaterial.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.EGetImgMaterial.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.EIdle.ordinal()] = 13;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.EReceiveAd.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ERefreshAd.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendFullScreenImpTrack.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendFullScreenThdImpTrack.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendImpTrack.ordinal()] = 4;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendThdImpTrack.ordinal()] = 5;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendVideoEndedImpTrack.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendVideoEndedThdImpTrack.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendVideoStartedImpTrack.ordinal()] = 9;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[AdEngine.ERecvAdStatus.ESendVideoStartedThdImpTrack.ordinal()] = 10;
            } catch (NoSuchFieldError e14) {
            }
            d = iArr;
        }
        return iArr;
    }

    private static /* synthetic */ int[] d() {
        int[] iArr = e;
        if (iArr == null) {
            iArr = new int[AdEngine.EClkTrack.values().length];
            try {
                iArr[AdEngine.EClkTrack.EIdle.ordinal()] = 11;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendBtnClkTrack.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendBtnThdClkTrack.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendClkTrack.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendFullScreenClkTrack.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendFullScreenThdClkTrack.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendThdClkTrack.ordinal()] = 2;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendVideoClkTrack.ordinal()] = 7;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendVideoCloseClkTrack.ordinal()] = 9;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendVideoCloseThdClkTrack.ordinal()] = 10;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[AdEngine.EClkTrack.ESendVideoThdClkTrack.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
            e = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        Utils.splitTrackUrl(this.mThdClkTrack.toString(), this.mThdClkTrackList);
        Utils.splitTrackUrl(this.mThdImpTrack.toString(), this.mThdImpTrackList);
        Utils.splitTrackUrl(this.mBtnThdClkTrack.toString(), this.mBtnThdClkTrackList);
    }

    /* access modifiers changed from: package-private */
    public final void a(StringBuffer stringBuffer, StringBuffer stringBuffer2, StringBuffer stringBuffer3, StringBuffer stringBuffer4, StringBuffer stringBuffer5, StringBuffer stringBuffer6, StringBuffer stringBuffer7, StringBuffer stringBuffer8, StringBuffer stringBuffer9, StringBuffer stringBuffer10, StringBuffer stringBuffer11, StringBuffer stringBuffer12, StringBuffer stringBuffer13, StringBuffer stringBuffer14, StringBuffer stringBuffer15, StringBuffer stringBuffer16, StringBuffer stringBuffer17, StringBuffer stringBuffer18, StringBuffer stringBuffer19, StringBuffer stringBuffer20, StringBuffer stringBuffer21, StringBuffer stringBuffer22, StringBuffer stringBuffer23, StringBuffer stringBuffer24, StringBuffer stringBuffer25, StringBuffer stringBuffer26, StringBuffer stringBuffer27, StringBuffer stringBuffer28, StringBuffer stringBuffer29, StringBuffer stringBuffer30) {
        resetAdserverInfo();
        this.mXmlVer.append(stringBuffer);
        this.mAdType.append(stringBuffer2);
        this.mAdClickUrl.append(stringBuffer3);
        this.mReportBaseUrl.append(stringBuffer4);
        this.mThdImpTrack.append(stringBuffer5);
        this.mThdClkTrack.append(stringBuffer6);
        this.mFC.append(stringBuffer7);
        this.mRefTime.append(stringBuffer8);
        this.mSmsAddr.append(stringBuffer9);
        this.mSmsContent.append(stringBuffer10);
        this.mSmsType.append(stringBuffer11);
        this.mImgAddr.append(stringBuffer12);
        this.mTxtLnkText.append(stringBuffer13);
        this.mBtnType.append(stringBuffer14);
        this.mBtnX.append(stringBuffer15);
        this.mBtnY.append(stringBuffer16);
        this.mBtnW.append(stringBuffer17);
        this.mBtnH.append(stringBuffer18);
        this.mBtnThdClkTrack.append(stringBuffer19);
        this.mBtnDownloadUrl.append(stringBuffer20);
        this.mBtnSmsNumber.append(stringBuffer21);
        this.mBtnSmsContent.append(stringBuffer22);
        this.mBtnCallNumber.append(stringBuffer23);
        this.mFullScreenImgAddr.append(stringBuffer24);
        this.mFullScreenClkUrl.append(stringBuffer25);
        this.mFullScreenThdImpTrack.append(stringBuffer26);
        this.mFullScreenThdClkTrack.append(stringBuffer27);
        this.mVideoAddr.append(stringBuffer28);
        this.mVideoStartedTrack.append(stringBuffer29);
        this.mVideoEndedTrack.append(stringBuffer30);
        if (this.mAdType.toString().compareToIgnoreCase("txt") == 0) {
            this.mAdModel = Common.EAdModel.EAdTXT;
        } else if (this.mAdType.toString().compareToIgnoreCase("jpg") == 0) {
            this.mAdModel = Common.EAdModel.EAdJPG;
        } else if (this.mAdType.toString().compareToIgnoreCase("png") == 0) {
            this.mAdModel = Common.EAdModel.EAdPNG;
        } else if (this.mAdType.toString().compareToIgnoreCase("gif") == 0) {
            this.mAdModel = Common.EAdModel.EAdGIF;
        } else {
            this.mAdModel = Common.EAdModel.EAdNONE;
        }
        writeFcParams(this.mFC.toString());
        this.logger.writeLog(((Object) this.mAdType) + " : ");
        this.logger.writeLog("ImpUrl:" + this.mThdImpTrack.toString());
        this.logger.writeLog("ClkUrl:" + this.mAdClickUrl.toString());
        this.logger.writeLog("FC:" + this.mFC.toString());
        this.logger.writeLog("FullScreenImgAddr:" + this.mFullScreenImgAddr.toString());
        if (!(Common.EAdModel.EAdTXT == this.mAdModel || Common.EAdModel.EAdNONE == this.mAdModel)) {
            this.logger.writeLog("ImgAddr:" + this.mImgAddr.toString());
        }
        this.logger.writeLog("-- readAdserverInfo");
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        this.c = z;
    }

    public void addAdView(AdView adView) {
        if (adView != null) {
            this.a.add(adView);
            adView.setController(this);
        }
    }

    public void deleteUnavailImgsFile() {
        File file = new File(Common.KBNImgsDir);
        if (file.exists()) {
            for (File file2 : file.listFiles()) {
                if (!this.b.containsValue(file2.getAbsolutePath())) {
                    file2.delete();
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008b, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x008c, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0094, code lost:
        if (r2.exists() != false) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0096, code lost:
        r2.delete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0099, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x009a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x009b, code lost:
        r2 = r0;
        r0 = r1;
        r1 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00d4, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00d5, code lost:
        r2 = r0;
        r0 = r1;
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00d9, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00da, code lost:
        r2 = r0;
        r0 = r1;
        r1 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00de, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00df, code lost:
        r2 = r0;
        r0 = r1;
        r1 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00f4, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f5, code lost:
        r3 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        return r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0096 A[Catch:{ Exception -> 0x009a, all -> 0x00de }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00bd A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00d4 A[ExcHandler: all (r1v11 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:6:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d9 A[ExcHandler: all (r1v22 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:13:0x005d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.InputStream downLoadAndSaveImg(java.lang.String r8) {
        /*
            r7 = this;
            r5 = 0
            r0 = 0
            com.adchina.android.ads.AdLog r1 = r7.logger     // Catch:{ Exception -> 0x00e5, all -> 0x00c6 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e5, all -> 0x00c6 }
            java.lang.String r3 = "++ start to download Banner Img file"
            r2.<init>(r3)     // Catch:{ Exception -> 0x00e5, all -> 0x00c6 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ Exception -> 0x00e5, all -> 0x00c6 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00e5, all -> 0x00c6 }
            r1.writeLog(r2)     // Catch:{ Exception -> 0x00e5, all -> 0x00c6 }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x00e5, all -> 0x00c6 }
            java.lang.String r2 = "/sdcard/ad/bnImg"
            r1.<init>(r2)     // Catch:{ Exception -> 0x00e5, all -> 0x00c6 }
            boolean r2 = r1.exists()     // Catch:{ Exception -> 0x00e5, all -> 0x00c6 }
            if (r2 != 0) goto L_0x0026
            r1.mkdirs()     // Catch:{ Exception -> 0x00e5, all -> 0x00c6 }
        L_0x0026:
            com.adchina.android.ads.HttpEngine r2 = r7.mHttpEngine     // Catch:{ Exception -> 0x00e5, all -> 0x00c6 }
            java.io.InputStream r0 = r2.requestGet(r8)     // Catch:{ Exception -> 0x00e5, all -> 0x00c6 }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x00ea, all -> 0x00d4 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ea, all -> 0x00d4 }
            java.lang.String r4 = "yyyyMMddHHmmss"
            java.lang.String r4 = com.adchina.android.ads.Utils.getNowTime(r4)     // Catch:{ Exception -> 0x00ea, all -> 0x00d4 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x00ea, all -> 0x00d4 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x00ea, all -> 0x00d4 }
            java.lang.String r4 = "bnImg.tmp"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00ea, all -> 0x00d4 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00ea, all -> 0x00d4 }
            r2.<init>(r1, r3)     // Catch:{ Exception -> 0x00ea, all -> 0x00d4 }
            boolean r1 = r2.exists()     // Catch:{ Exception -> 0x00ea, all -> 0x00d4 }
            if (r1 == 0) goto L_0x0053
            r2.delete()     // Catch:{ Exception -> 0x00ea, all -> 0x00d4 }
        L_0x0053:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x008b, all -> 0x00d4 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x008b, all -> 0x00d4 }
            r3 = 16384(0x4000, float:2.2959E-41)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x008b, all -> 0x00d4 }
            r4 = r5
        L_0x005d:
            int r5 = r0.read(r3)     // Catch:{ Exception -> 0x00f4, all -> 0x00d9 }
            if (r5 <= 0) goto L_0x0069
            r6 = 0
            r1.write(r3, r6, r5)     // Catch:{ Exception -> 0x00f4, all -> 0x00d9 }
            r4 = 1
            goto L_0x005d
        L_0x0069:
            com.adchina.android.ads.HttpEngine r3 = r7.mHttpEngine     // Catch:{ Exception -> 0x00f4, all -> 0x00d9 }
            r3.closeStream(r0)     // Catch:{ Exception -> 0x00f4, all -> 0x00d9 }
            r1.flush()     // Catch:{ Exception -> 0x00f4, all -> 0x00d9 }
            r1.close()     // Catch:{ Exception -> 0x00f4, all -> 0x00d9 }
            java.util.HashMap r1 = r7.b     // Catch:{ Exception -> 0x00ef, all -> 0x00d9 }
            java.lang.String r3 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x00ef, all -> 0x00d9 }
            r1.put(r8, r3)     // Catch:{ Exception -> 0x00ef, all -> 0x00d9 }
            r7.saveLocalImgList()     // Catch:{ Exception -> 0x00ef, all -> 0x00d9 }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00ef, all -> 0x00d9 }
            java.lang.String r2 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x00ef, all -> 0x00d9 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x00ef, all -> 0x00d9 }
            r0 = r1
        L_0x008a:
            return r0
        L_0x008b:
            r1 = move-exception
            r3 = r5
        L_0x008d:
            r1.printStackTrace()     // Catch:{ Exception -> 0x009a, all -> 0x00de }
            boolean r4 = r2.exists()     // Catch:{ Exception -> 0x009a, all -> 0x00de }
            if (r4 == 0) goto L_0x0099
            r2.delete()     // Catch:{ Exception -> 0x009a, all -> 0x00de }
        L_0x0099:
            throw r1     // Catch:{ Exception -> 0x009a, all -> 0x00de }
        L_0x009a:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
        L_0x009e:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e3 }
            java.lang.String r4 = "Failed to download Banner Img file, err = "
            r3.<init>(r4)     // Catch:{ all -> 0x00e3 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00e3 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00e3 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00e3 }
            com.adchina.android.ads.AdLog r3 = r7.logger     // Catch:{ all -> 0x00e3 }
            r3.writeLog(r0)     // Catch:{ all -> 0x00e3 }
            java.lang.String r3 = "AdChinaError"
            android.util.Log.e(r3, r0)     // Catch:{ all -> 0x00e3 }
            if (r1 != 0) goto L_0x00bf
            if (r2 != 0) goto L_0x00f7
        L_0x00bf:
            com.adchina.android.ads.HttpEngine r0 = r7.mHttpEngine
            java.io.InputStream r0 = r0.requestGet(r8)
            goto L_0x008a
        L_0x00c6:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r5
        L_0x00ca:
            if (r1 != 0) goto L_0x00ce
            if (r2 != 0) goto L_0x00d3
        L_0x00ce:
            com.adchina.android.ads.HttpEngine r1 = r7.mHttpEngine
            r1.requestGet(r8)
        L_0x00d3:
            throw r0
        L_0x00d4:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r5
            goto L_0x00ca
        L_0x00d9:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r4
            goto L_0x00ca
        L_0x00de:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x00ca
        L_0x00e3:
            r0 = move-exception
            goto L_0x00ca
        L_0x00e5:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r5
            goto L_0x009e
        L_0x00ea:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r5
            goto L_0x009e
        L_0x00ef:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r4
            goto L_0x009e
        L_0x00f4:
            r1 = move-exception
            r3 = r4
            goto L_0x008d
        L_0x00f7:
            r0 = r2
            goto L_0x008a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.controllers.AdViewController.downLoadAndSaveImg(java.lang.String):java.io.InputStream");
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 1:
                if (this.mAdListener != null) {
                    Iterator it = this.a.iterator();
                    while (it.hasNext()) {
                        AdView adView = (AdView) it.next();
                        if (this.mRefreshAd) {
                            this.mAdListener.onRefreshAd(adView);
                        } else {
                            this.mAdListener.onReceiveAd(adView);
                        }
                    }
                }
                this.logger.writeLog("++ onDisplayAd, mAdModel = " + this.mAdModel);
                this.mRefreshAd = true;
                Iterator it2 = this.a.iterator();
                while (it2.hasNext()) {
                    AdView adView2 = (AdView) it2.next();
                    try {
                        if (Common.EAdModel.EAdTXT == this.mAdModel) {
                            adView2.setContent(this.mTxtLnkText.toString());
                        } else if (Common.EAdModel.EAdJPG == this.mAdModel || Common.EAdModel.EAdPNG == this.mAdModel) {
                            adView2.setContent((Bitmap) message.obj);
                        } else if (Common.EAdModel.EAdGIF == this.mAdModel) {
                            adView2.setContent((byte[]) message.obj);
                        }
                    } catch (Exception e2) {
                        Log.e(Common.KLogTag, "Failed to DisplayAd, err = " + e2.toString());
                    }
                }
                this.mHasAd = true;
                setRecvAdStatus(AdEngine.ERecvAdStatus.ESendImpTrack);
                this.logger.writeLog("-- onDisplayAd");
                break;
            case 2:
                if (this.mAdListener != null) {
                    Iterator it3 = this.a.iterator();
                    while (it3.hasNext()) {
                        AdView adView3 = (AdView) it3.next();
                        if (this.mRefreshAd) {
                            this.mAdListener.onFailedToRefreshAd(adView3);
                        } else {
                            this.mAdListener.onFailedToReceiveAd(adView3);
                        }
                    }
                }
                this.mHasAd = false;
            case 3:
                setRecvAdStatus(AdEngine.ERecvAdStatus.ERefreshAd);
                break;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0099 A[SYNTHETIC, Splitter:B:25:0x0099] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x009e A[Catch:{ IOException -> 0x00a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c4 A[SYNTHETIC, Splitter:B:35:0x00c4] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c9 A[Catch:{ IOException -> 0x00d0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadLocalImgList() {
        /*
            r10 = this;
            r3 = 0
            r4 = 0
            com.adchina.android.ads.AdLog r0 = r10.logger
            java.lang.String r1 = "loadLocalBannerImgList"
            r0.writeLog(r1)
            android.content.Context r0 = r10.mContext     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            java.lang.String r1 = "adchinaBNImgs.fc"
            java.io.File r0 = r0.getFileStreamPath(r1)     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            long r0 = r0.length()     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            int r0 = (int) r0     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            char[] r0 = new char[r0]     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            android.content.Context r1 = r10.mContext     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            java.lang.String r2 = "adchinaBNImgs.fc"
            java.io.FileInputStream r1 = r1.openFileInput(r2)     // Catch:{ Exception -> 0x007c, all -> 0x00bf }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0110, all -> 0x0105 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0110, all -> 0x0105 }
            r2.read(r0)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.util.HashMap r0 = r10.b     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r0.clear()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r0 = "\n"
            java.lang.String[] r0 = r3.split(r0)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            int r3 = r0.length     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
        L_0x0039:
            if (r4 < r3) goto L_0x0047
            r2.close()     // Catch:{ IOException -> 0x00ea }
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ IOException -> 0x00ea }
        L_0x0043:
            r10.deleteUnavailImgsFile()     // Catch:{ IOException -> 0x00ea }
        L_0x0046:
            return
        L_0x0047:
            r5 = r0[r4]     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r6 = "|||"
            int r6 = r5.indexOf(r6)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r7 = 0
            java.lang.String r7 = r5.substring(r7, r6)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r8 = "|||"
            int r8 = r8.length()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            int r6 = r6 + r8
            java.lang.String r5 = r5.substring(r6)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r6.<init>(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            boolean r8 = r6.exists()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            if (r8 == 0) goto L_0x0079
            java.lang.String r6 = r6.getName()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            boolean r6 = com.adchina.android.ads.Utils.isCachedFileTimeout(r6)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            if (r6 != 0) goto L_0x0079
            java.util.HashMap r6 = r10.b     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r6.put(r7, r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
        L_0x0079:
            int r4 = r4 + 1
            goto L_0x0039
        L_0x007c:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x007f:
            com.adchina.android.ads.AdLog r3 = r10.logger     // Catch:{ all -> 0x010e }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x010e }
            java.lang.String r5 = "Exceptions in loadLocalBannerImgList , err = "
            r4.<init>(r5)     // Catch:{ all -> 0x010e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x010e }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x010e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x010e }
            r3.writeLog(r0)     // Catch:{ all -> 0x010e }
            if (r1 == 0) goto L_0x009c
            r1.close()     // Catch:{ IOException -> 0x00a5 }
        L_0x009c:
            if (r2 == 0) goto L_0x00a1
            r2.close()     // Catch:{ IOException -> 0x00a5 }
        L_0x00a1:
            r10.deleteUnavailImgsFile()     // Catch:{ IOException -> 0x00a5 }
            goto L_0x0046
        L_0x00a5:
            r0 = move-exception
            com.adchina.android.ads.AdLog r1 = r10.logger
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Exceptions in loadLocalBannerImgList , err = "
            r2.<init>(r3)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.writeLog(r0)
            goto L_0x0046
        L_0x00bf:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x00c2:
            if (r1 == 0) goto L_0x00c7
            r1.close()     // Catch:{ IOException -> 0x00d0 }
        L_0x00c7:
            if (r2 == 0) goto L_0x00cc
            r2.close()     // Catch:{ IOException -> 0x00d0 }
        L_0x00cc:
            r10.deleteUnavailImgsFile()     // Catch:{ IOException -> 0x00d0 }
        L_0x00cf:
            throw r0
        L_0x00d0:
            r1 = move-exception
            com.adchina.android.ads.AdLog r2 = r10.logger
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Exceptions in loadLocalBannerImgList , err = "
            r3.<init>(r4)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.writeLog(r1)
            goto L_0x00cf
        L_0x00ea:
            r0 = move-exception
            com.adchina.android.ads.AdLog r1 = r10.logger
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Exceptions in loadLocalBannerImgList , err = "
            r2.<init>(r3)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.writeLog(r0)
            goto L_0x0046
        L_0x0105:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x00c2
        L_0x0109:
            r0 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x00c2
        L_0x010e:
            r0 = move-exception
            goto L_0x00c2
        L_0x0110:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x007f
        L_0x0115:
            r0 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.controllers.AdViewController.loadLocalImgList():void");
    }

    public void onClickBackground() {
        this.logger.writeLog("++ onClick");
        if (this.mSmsType.length() > 0) {
            this.logger.writeLog("++ open " + this.mAdClickUrl + " with browser");
            AdEngine.getAdEngine().openBrowser(this.mAdClickUrl.toString());
            this.logger.writeLog("-- open browser");
            setClkTrackStatus(AdEngine.EClkTrack.ESendThdClkTrack);
            this.logger.writeLog("-- onClick");
        }
    }

    public void onTouchEvent(AdView adView, MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        try {
            if (this.mBtnType.length() > 0) {
                int intValue = Integer.valueOf(this.mBtnX.toString()).intValue();
                int intValue2 = Integer.valueOf(this.mBtnY.toString()).intValue();
                if (new Rect(intValue, intValue2, Integer.valueOf(this.mBtnW.toString()).intValue() + intValue, Integer.valueOf(this.mBtnH.toString()).intValue() + intValue2).contains((int) x, (int) y)) {
                    setClkTrackStatus(AdEngine.EClkTrack.ESendBtnClkTrack);
                    if (this.mBtnType.toString().equalsIgnoreCase("down")) {
                        this.logger.writeLog("++ open " + this.mBtnDownloadUrl + " with browser");
                        AdEngine.getAdEngine().openBrowser(this.mBtnDownloadUrl.toString());
                        this.logger.writeLog("-- open browser");
                    } else if (this.mBtnType.toString().equalsIgnoreCase(DomobAdManager.ACTION_SMS)) {
                        if (this.mAdListener.OnRecvSms(adView, "SMS:" + ((Object) this.mBtnSmsContent) + " To:" + ((Object) this.mBtnSmsNumber))) {
                            this.logger.writeLog("++ send sms to number " + this.mBtnSmsNumber.toString());
                        }
                        AdEngine.getAdEngine().sendSms(this.mBtnSmsNumber.toString(), this.mBtnSmsContent.toString());
                        this.logger.writeLog("-- send sms");
                    } else if (this.mBtnType.toString().equalsIgnoreCase(DomobAdManager.ACTION_CALL)) {
                        this.logger.writeLog("++ make call to number " + this.mBtnCallNumber.toString());
                        AdEngine.getAdEngine().makeCall(this.mBtnCallNumber.toString());
                        this.logger.writeLog("-- make call");
                    }
                } else {
                    onClickBackground();
                }
            } else {
                onClickBackground();
            }
        } catch (Exception e2) {
            this.logger.writeLog("Failed to AdView.onTouchEvent, err = " + e2.toString());
        }
    }

    /* access modifiers changed from: protected */
    public String readFcParams() {
        return readFcParams(Common.KFcFilename);
    }

    public void removeAdView(AdView adView) {
        if (adView != null && this.a.contains(adView)) {
            this.a.remove(adView);
        }
    }

    public void runClkTrackJob() {
        String str;
        String str2;
        String str3;
        String str4;
        while (!this.mStop) {
            switch (d()[this.mClickStatus.ordinal()]) {
                case 1:
                    this.logger.writeLog("++ onSendClkTrack");
                    setClkTrackStatus(AdEngine.EClkTrack.EIdle);
                    try {
                        String str5 = String.valueOf(this.mReportBaseUrl.toString()) + Common.KCLK + ",0,0,0" + setupMaParams();
                        this.mHttpEngine.requestSend(str5);
                        this.logger.writeLog("send ClkTrack to " + str5);
                        break;
                    } catch (Exception e2) {
                        String str6 = "Exceptions in onSendClkTrack, err = " + e2.toString();
                        this.logger.writeLog(str6);
                        Log.e(Common.KLogTag, str6);
                        break;
                    } finally {
                        setClkTrackStatus(AdEngine.EClkTrack.ESendThdClkTrack);
                        str4 = "-- onSendClkTrack";
                        this.logger.writeLog(str4);
                    }
                case 2:
                    setClkTrackStatus(AdEngine.EClkTrack.EIdle);
                    int size = this.mThdClkTrackList.size();
                    this.logger.writeLog("++ onSendThdClkTrack, Size of list is " + size);
                    if (size > 0) {
                        try {
                            String str7 = (String) this.mThdClkTrackList.get(size - 1);
                            this.mThdClkTrackList.removeLast();
                            this.mHttpEngine.requestSend(str7);
                            this.logger.writeLog("send ThdClkTrack to " + str7);
                            break;
                        } catch (Exception e3) {
                            String str8 = "Failed to onSendThdClkTrack, err = " + e3.toString();
                            this.logger.writeLog(str8);
                            Log.e(Common.KLogTag, str8);
                            break;
                        } finally {
                            setClkTrackStatus(AdEngine.EClkTrack.ESendThdClkTrack);
                            str3 = "-- onSendThdClkTrack";
                            this.logger.writeLog(str3);
                        }
                    }
                    break;
                case 3:
                    this.logger.writeLog("++ onSendBtnClkTrack");
                    setClkTrackStatus(AdEngine.EClkTrack.EIdle);
                    try {
                        String str9 = String.valueOf(this.mReportBaseUrl.toString()) + Common.KBTNCLK + ",0,0,0" + setupMaParams();
                        this.mHttpEngine.requestSend(str9);
                        this.logger.writeLog("send BtnClkTrack to " + str9);
                        break;
                    } catch (Exception e4) {
                        String str10 = "Exceptions in onSendBtnClkTrack, err = " + e4.toString();
                        this.logger.writeLog(str10);
                        Log.e(Common.KLogTag, str10);
                        break;
                    } finally {
                        setClkTrackStatus(AdEngine.EClkTrack.ESendBtnThdClkTrack);
                        str2 = "-- onSendBtnClkTrack";
                        this.logger.writeLog(str2);
                    }
                case 4:
                    setClkTrackStatus(AdEngine.EClkTrack.EIdle);
                    int size2 = this.mBtnThdClkTrackList.size();
                    this.logger.writeLog("++ onSendBtnThdClkTrack, Size of list is " + size2);
                    if (size2 > 0) {
                        try {
                            String str11 = (String) this.mBtnThdClkTrackList.get(size2 - 1);
                            this.mBtnThdClkTrackList.removeLast();
                            this.mHttpEngine.requestSend(str11);
                            this.logger.writeLog("send btnthdclktrack to " + str11);
                            break;
                        } catch (Exception e5) {
                            String str12 = "Failed to onSendBtnThdClkTrack, err = " + e5.toString();
                            this.logger.writeLog(str12);
                            Log.e(Common.KLogTag, str12);
                            break;
                        } finally {
                            setClkTrackStatus(AdEngine.EClkTrack.ESendBtnThdClkTrack);
                            str = "-- onSendBtnThdClkTrack";
                            this.logger.writeLog(str);
                        }
                    }
                    break;
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e6) {
            }
        }
    }

    public void runRecvAdThreadJob() {
        String str;
        String str2;
        String str3;
        String str4;
        InputStream inputStream;
        Exception exc;
        Throwable th;
        while (!this.mStop) {
            switch (c()[this.mRunState.ordinal()]) {
                case 1:
                    if (this.c) {
                        this.logger.writeLog("++ onReceiveAd");
                        setRecvAdStatus(AdEngine.ERecvAdStatus.EIdle);
                        InputStream inputStream2 = null;
                        try {
                            if (AdManager.getDebugMode()) {
                                String str5 = setupAdserverUrlFromConfig();
                                if (str5 != null) {
                                    this.logger.writeLog("AdserverUrl:" + str5);
                                    inputStream2 = this.mHttpEngine.requestGet(str5, this.mAdspaceId.toString());
                                }
                            } else {
                                String str6 = setupAdserverUrl();
                                this.logger.writeLog("AdserverUrl:" + str6);
                                inputStream2 = this.mHttpEngine.requestGet(str6, this.mAdspaceId.toString());
                            }
                            try {
                                readAdserverInfo(inputStream2);
                                if (Common.EAdModel.EAdTXT == this.mAdModel) {
                                    sendMessage(1, this.mTxtLnkText.toString());
                                } else if (Common.EAdModel.EAdJPG == this.mAdModel || Common.EAdModel.EAdPNG == this.mAdModel || Common.EAdModel.EAdGIF == this.mAdModel) {
                                    loadLocalImgList();
                                    setRecvAdStatus(AdEngine.ERecvAdStatus.EGetImgMaterial);
                                } else {
                                    throw new XmlPullParserException("Invalidate mAdModel");
                                }
                                a();
                                this.mHttpEngine.closeStream(inputStream2);
                                this.logger.writeLog("-- onReceiveAd");
                            } catch (Exception e2) {
                                Exception exc2 = e2;
                                inputStream = inputStream2;
                                exc = exc2;
                                try {
                                    String str7 = "Exceptions in onReceiveAd, err = " + exc.toString();
                                    sendMessage(2, str7);
                                    this.logger.writeLog(str7);
                                    Log.e(Common.KLogTag, str7);
                                    this.mHttpEngine.closeStream(inputStream);
                                    this.logger.writeLog("-- onReceiveAd");
                                    Thread.sleep(50);
                                } catch (Throwable th2) {
                                    th = th2;
                                    this.mHttpEngine.closeStream(inputStream);
                                    this.logger.writeLog("-- onReceiveAd");
                                    throw th;
                                }
                            } catch (Throwable th3) {
                                Throwable th4 = th3;
                                inputStream = inputStream2;
                                th = th4;
                                this.mHttpEngine.closeStream(inputStream);
                                this.logger.writeLog("-- onReceiveAd");
                                throw th;
                            }
                        } catch (Exception e3) {
                            Exception exc3 = e3;
                            inputStream = null;
                            exc = exc3;
                        } catch (Throwable th5) {
                            Throwable th6 = th5;
                            inputStream = null;
                            th = th6;
                            this.mHttpEngine.closeStream(inputStream);
                            this.logger.writeLog("-- onReceiveAd");
                            throw th;
                        }
                    }
                    break;
                case 3:
                    b();
                    break;
                case 4:
                    this.logger.writeLog("++ onSendImpTrack");
                    try {
                        String str8 = String.valueOf(this.mReportBaseUrl.toString()) + Common.KIMP + ",0,0,0" + setupMaParams();
                        this.mHttpEngine.requestSend(str8);
                        this.logger.writeLog("send ImpTrack to " + str8);
                        break;
                    } catch (Exception e4) {
                        String str9 = "Exceptions in onSendImpTrack, err = " + e4.toString();
                        this.logger.writeLog(str9);
                        Log.e(Common.KLogTag, str9);
                        break;
                    } finally {
                        setRecvAdStatus(AdEngine.ERecvAdStatus.ESendThdImpTrack);
                        str4 = "-- onSendImpTrack";
                        this.logger.writeLog(str4);
                    }
                case 5:
                    setRecvAdStatus(AdEngine.ERecvAdStatus.EIdle);
                    int size = this.mThdImpTrackList.size();
                    this.logger.writeLog("++ onSendThdImpTrack, Size of list is " + size);
                    if (size > 0) {
                        try {
                            String str10 = (String) this.mThdImpTrackList.get(size - 1);
                            this.mThdImpTrackList.removeLast();
                            this.mHttpEngine.requestSend(str10);
                            this.logger.writeLog("send ThdImpTrack to " + str10);
                        } catch (Exception e5) {
                            String str11 = "Failed to onSendThdImpTrack, err = " + e5.toString();
                            this.logger.writeLog(str11);
                            Log.e(Common.KLogTag, str11);
                        } finally {
                            setRecvAdStatus(AdEngine.ERecvAdStatus.ESendThdImpTrack);
                        }
                    } else {
                        sendMessage(3, "RefreshAd");
                    }
                    this.logger.writeLog("-- onSendThdImpTrack");
                    break;
                case 6:
                    this.logger.writeLog("++ onRefreshAd");
                    try {
                        int intValue = Integer.valueOf(this.mRefTime.toString()).intValue();
                        try {
                            Thread.sleep((long) (intValue * com.energysource.szj.embeded.AdManager.AD_FILL_PARENT));
                            break;
                        } catch (Exception e6) {
                            break;
                        } finally {
                            setRecvAdStatus(AdEngine.ERecvAdStatus.EReceiveAd);
                            str3 = "-- onRefreshAd, delay = ";
                            this.logger.writeLog(str3 + intValue);
                        }
                    } catch (Exception e7) {
                        try {
                            Thread.sleep(15000);
                            break;
                        } catch (Exception e8) {
                            break;
                        } finally {
                            setRecvAdStatus(AdEngine.ERecvAdStatus.EReceiveAd);
                            str2 = "-- onRefreshAd, delay = ";
                            this.logger.writeLog(str2 + 15);
                        }
                    } catch (Throwable th7) {
                        try {
                            Thread.sleep((long) (15 * com.energysource.szj.embeded.AdManager.AD_FILL_PARENT));
                        } catch (Exception e9) {
                        } finally {
                            setRecvAdStatus(AdEngine.ERecvAdStatus.EReceiveAd);
                            str = "-- onRefreshAd, delay = ";
                            this.logger.writeLog(str + 15);
                        }
                        throw th7;
                    }
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e10) {
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x007f A[SYNTHETIC, Splitter:B:29:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0084 A[Catch:{ IOException -> 0x008a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void saveLocalImgList() {
        /*
            r7 = this;
            r3 = 0
            com.adchina.android.ads.AdLog r0 = r7.logger
            java.lang.String r1 = "saveLocalBannerImgList"
            r0.writeLog(r1)
            android.content.Context r0 = r7.mContext     // Catch:{ Exception -> 0x0097, all -> 0x007a }
            java.lang.String r1 = "adchinaBNImgs.fc"
            r2 = 0
            java.io.FileOutputStream r1 = r0.openFileOutput(r1, r2)     // Catch:{ Exception -> 0x0097, all -> 0x007a }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x009b, all -> 0x008c }
            r2.<init>(r1)     // Catch:{ Exception -> 0x009b, all -> 0x008c }
            java.util.HashMap r0 = r7.b     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.Set r0 = r0.keySet()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
        L_0x0020:
            boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            if (r0 != 0) goto L_0x0032
            r2.flush()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.close()     // Catch:{ IOException -> 0x0088 }
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x0088 }
        L_0x0031:
            return
        L_0x0032:
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r4 = "|||"
            r2.write(r4)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.HashMap r4 = r7.b     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = "\n"
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            goto L_0x0020
        L_0x0051:
            r0 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x0055:
            com.adchina.android.ads.AdLog r3 = r7.logger     // Catch:{ all -> 0x0095 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0095 }
            java.lang.String r5 = "Exceptions in saveLocalBannerImgList, err = "
            r4.<init>(r5)     // Catch:{ all -> 0x0095 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0095 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0095 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0095 }
            r3.writeLog(r0)     // Catch:{ all -> 0x0095 }
            if (r1 == 0) goto L_0x0072
            r1.close()     // Catch:{ IOException -> 0x0078 }
        L_0x0072:
            if (r2 == 0) goto L_0x0031
            r2.close()     // Catch:{ IOException -> 0x0078 }
            goto L_0x0031
        L_0x0078:
            r0 = move-exception
            goto L_0x0031
        L_0x007a:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x007d:
            if (r1 == 0) goto L_0x0082
            r1.close()     // Catch:{ IOException -> 0x008a }
        L_0x0082:
            if (r2 == 0) goto L_0x0087
            r2.close()     // Catch:{ IOException -> 0x008a }
        L_0x0087:
            throw r0
        L_0x0088:
            r0 = move-exception
            goto L_0x0031
        L_0x008a:
            r1 = move-exception
            goto L_0x0087
        L_0x008c:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x007d
        L_0x0090:
            r0 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x007d
        L_0x0095:
            r0 = move-exception
            goto L_0x007d
        L_0x0097:
            r0 = move-exception
            r1 = r3
            r2 = r3
            goto L_0x0055
        L_0x009b:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.controllers.AdViewController.saveLocalImgList():void");
    }

    public void setDefaultImage(Bitmap bitmap) {
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            ((AdView) it.next()).setDefaultImage(bitmap);
        }
    }

    public void setDefaultUrl(String str) {
        this.mAdClickUrl.setLength(0);
        this.mAdClickUrl.append(str);
        this.mSmsType.setLength(0);
        this.mSmsType.append(Common.KIMP);
    }

    public void setFont(Typeface typeface) {
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            ((AdView) it.next()).setFont(typeface);
        }
    }

    /* access modifiers changed from: protected */
    public void setupAdspaceId() {
        this.mAdspaceId.setLength(0);
        this.mAdspaceId.append(AdManager.getAdspaceId());
    }

    /* access modifiers changed from: protected */
    public void writeFcParams(String str) {
        writeFcParams(str, Common.KFcFilename);
    }
}
