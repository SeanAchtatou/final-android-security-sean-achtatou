package com.adchina.android.ads;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CookieDB {
    public static final String KEY_DOMAIN = "domain";
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_VALUE = "value";
    private Context a = null;
    private a b = null;
    private SQLiteDatabase c = null;
    private SQLiteDatabase d = null;

    public CookieDB(Context context) {
        this.a = context;
        this.b = new a(this.a);
    }

    public boolean IsLocked() {
        return this.c.isDbLockedByCurrentThread() || this.c.isDbLockedByOtherThreads();
    }

    public void closeRead() {
        this.d.close();
    }

    public void closeWrite() {
        this.c.close();
    }

    public void deleteAll() {
        this.c.execSQL("DELETE FROM adcookie");
    }

    public long insertData(int i, String str, String str2, String str3) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NAME, str);
        contentValues.put(KEY_VALUE, str2);
        contentValues.put(KEY_DOMAIN, str3);
        return this.c.insert("adcookie", KEY_ID, contentValues);
    }

    public void openRead() {
        this.d = this.b.getReadableDatabase();
    }

    public void openWrite() {
        this.c = this.b.getWritableDatabase();
    }

    public Cursor readAll() {
        return this.d.rawQuery("SELECT * FROM adcookie", null);
    }
}
