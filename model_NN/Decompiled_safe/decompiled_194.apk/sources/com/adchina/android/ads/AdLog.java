package com.adchina.android.ads;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Date;
import java.text.SimpleDateFormat;

public final class AdLog {
    private File a;
    private File b;
    private FileOutputStream c;
    private FileOutputStream d;
    private boolean e;
    private boolean f;
    private boolean g;
    private boolean h;

    public AdLog() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = false;
        this.f = false;
        this.g = false;
        this.h = false;
        this.g = AdManager.getLogMode();
        this.h = this.g && AdManager.getDebugMode();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public final void initDebugLog(String str) {
        if (this.h && !this.f) {
            this.f = true;
            this.b = new File("/sdcard/" + str);
            if (this.b.exists()) {
                this.b.delete();
            }
            try {
                this.b.createNewFile();
                this.d = new FileOutputStream(this.b, true);
            } catch (Exception e2) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public final void initLog(String str) {
        if (this.g && !this.e) {
            this.e = true;
            this.a = new File("/sdcard/" + str);
            if (this.a.exists()) {
                this.a.delete();
            }
            try {
                this.a.createNewFile();
                this.c = new FileOutputStream(this.a, true);
            } catch (Exception e2) {
            }
        }
    }

    public final void uninitDebugLog() {
        if (this.h) {
            try {
                this.d.close();
            } catch (Exception e2) {
            }
        }
    }

    public final void uninitLog() {
        if (this.g) {
            try {
                this.c.close();
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void writeDebugLog(String str) {
        if (this.h) {
            try {
                this.d.write((String.valueOf(str) + "\r\n").getBytes());
                this.d.flush();
            } catch (Exception e2) {
            }
        }
    }

    public final void writeLog(String str) {
        if (this.g) {
            try {
                this.c.write((String.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss --- ").format(new Date(System.currentTimeMillis()))) + str + "\r\n").getBytes());
                this.c.flush();
            } catch (Exception e2) {
            }
        }
    }
}
