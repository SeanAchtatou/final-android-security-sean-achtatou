package com.adchina.android.ads;

import cn.domob.android.ads.DomobAdManager;
import java.io.InputStream;
import java.net.URLDecoder;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public final class XmlEngine {
    private String A = "";
    private String B = "";
    private String C = "";
    private String D = "";
    private String E = "";
    private String F = "";
    private AdLog G;
    private String a = "";
    private String b = "";
    private String c = "";
    private String d = "";
    private String e = "";
    private String f = "";
    private String g = "";
    private String h = "";
    private String i = "";
    private String j = "";
    private String k = "";
    private String l = "";
    private String m = "";
    private String n = "";
    private String o = "";
    private String p = "";
    private String q = "";
    private String r = "";
    private String s = "";
    private String t = "";
    private String u = "";
    private String v = "";
    private String w = "";
    private String x = "";
    private String y = "";
    private String z = "";

    /* access modifiers changed from: protected */
    public final void parseTag(XmlPullParser xmlPullParser, String str) {
        if (str.equalsIgnoreCase("ac")) {
            this.a = String.valueOf(this.a) + xmlPullParser.getAttributeValue(0);
        } else if (str.equalsIgnoreCase("type")) {
            this.b = String.valueOf(this.b) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("cu")) {
            this.c = String.valueOf(this.c) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("rbu")) {
            this.d = String.valueOf(this.d) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("itu")) {
            this.e = String.valueOf(this.e) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("ctu")) {
            this.f = String.valueOf(this.f) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("fc")) {
            this.g = String.valueOf(this.g) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("rt")) {
            this.h = String.valueOf(this.h) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("sa")) {
            this.i = String.valueOf(this.i) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("sc")) {
            this.j = String.valueOf(this.j) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("sptype")) {
            this.k = String.valueOf(this.k) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("txt")) {
            this.n = String.valueOf(this.n) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("img")) {
            this.m = String.valueOf(this.m) + xmlPullParser.getAttributeValue(0);
            this.l = String.valueOf(this.l) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("btn")) {
            this.o = String.valueOf(this.o) + xmlPullParser.getAttributeValue(0);
            this.p = String.valueOf(this.p) + xmlPullParser.getAttributeValue(1);
            this.q = String.valueOf(this.q) + xmlPullParser.getAttributeValue(2);
            this.r = String.valueOf(this.r) + xmlPullParser.getAttributeValue(3);
            this.s = String.valueOf(this.s) + xmlPullParser.getAttributeValue(4);
        } else if (str.equalsIgnoreCase("curl")) {
            this.t = String.valueOf(this.t) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("down")) {
            this.u = String.valueOf(this.u) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase(DomobAdManager.ACTION_SMS)) {
            String nextText = xmlPullParser.nextText();
            int indexOf = nextText.indexOf(":");
            if (-1 != indexOf) {
                this.v = String.valueOf(this.v) + nextText.substring(0, indexOf);
                this.w = String.valueOf(this.w) + nextText.substring(indexOf + 1);
            }
        } else if (str.equalsIgnoreCase(DomobAdManager.ACTION_CALL)) {
            this.x = String.valueOf(this.x) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("fsurl")) {
            this.y = String.valueOf(this.y) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("fscurl")) {
            this.z = String.valueOf(this.z) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("fsimp")) {
            this.A = String.valueOf(this.A) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("fsclk")) {
            this.B = String.valueOf(this.B) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("prurl")) {
            this.C = String.valueOf(this.C) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("prsimp")) {
            this.D = String.valueOf(this.D) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("preimp")) {
            this.E = String.valueOf(this.E) + xmlPullParser.nextText();
        } else if (str.equalsIgnoreCase("prcimp")) {
            this.F = String.valueOf(this.F) + xmlPullParser.nextText();
        }
    }

    public final void parseXmlStream(InputStream inputStream) {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(inputStream, Common.KEnc);
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case 2:
                    parseTag(newPullParser, newPullParser.getName());
                    break;
            }
        }
    }

    public final void readXmlInfo(StringBuffer stringBuffer, StringBuffer stringBuffer2, StringBuffer stringBuffer3, StringBuffer stringBuffer4, StringBuffer stringBuffer5, StringBuffer stringBuffer6, StringBuffer stringBuffer7, StringBuffer stringBuffer8, StringBuffer stringBuffer9, StringBuffer stringBuffer10, StringBuffer stringBuffer11, StringBuffer stringBuffer12, StringBuffer stringBuffer13, StringBuffer stringBuffer14, StringBuffer stringBuffer15, StringBuffer stringBuffer16, StringBuffer stringBuffer17, StringBuffer stringBuffer18, StringBuffer stringBuffer19, StringBuffer stringBuffer20, StringBuffer stringBuffer21, StringBuffer stringBuffer22, StringBuffer stringBuffer23, StringBuffer stringBuffer24, StringBuffer stringBuffer25, StringBuffer stringBuffer26, StringBuffer stringBuffer27, StringBuffer stringBuffer28, StringBuffer stringBuffer29, StringBuffer stringBuffer30, StringBuffer stringBuffer31) {
        stringBuffer.append(this.a);
        stringBuffer2.append(this.b);
        stringBuffer3.append(URLDecoder.decode(this.c, Common.KEnc));
        stringBuffer4.append(URLDecoder.decode(this.d, Common.KEnc));
        stringBuffer5.append(URLDecoder.decode(this.e, Common.KEnc));
        stringBuffer6.append(URLDecoder.decode(this.f, Common.KEnc));
        stringBuffer7.append(this.g);
        stringBuffer8.append(this.h);
        stringBuffer9.append(URLDecoder.decode(this.i, Common.KEnc));
        stringBuffer10.append(URLDecoder.decode(this.j, Common.KEnc));
        stringBuffer11.append(URLDecoder.decode(this.k, Common.KEnc));
        stringBuffer13.append(URLDecoder.decode(this.n, Common.KEnc));
        stringBuffer12.append(URLDecoder.decode(this.l, Common.KEnc));
        if (stringBuffer2.toString().compareToIgnoreCase("img") == 0) {
            stringBuffer2.setLength(0);
            stringBuffer2.append(URLDecoder.decode(this.m, Common.KEnc));
        }
        stringBuffer14.append(this.o);
        stringBuffer15.append(this.p);
        stringBuffer16.append(this.q);
        stringBuffer17.append(this.r);
        stringBuffer18.append(this.s);
        stringBuffer19.append(URLDecoder.decode(this.t, Common.KEnc));
        stringBuffer20.append(URLDecoder.decode(this.u, Common.KEnc));
        stringBuffer21.append(this.v);
        stringBuffer22.append(this.w);
        stringBuffer23.append(this.x);
        stringBuffer24.append(URLDecoder.decode(this.y, Common.KEnc));
        stringBuffer25.append(URLDecoder.decode(this.z, Common.KEnc));
        stringBuffer26.append(URLDecoder.decode(this.A, Common.KEnc));
        stringBuffer27.append(URLDecoder.decode(this.B, Common.KEnc));
        stringBuffer28.append(URLDecoder.decode(this.C, Common.KEnc));
        stringBuffer29.append(URLDecoder.decode(this.D, Common.KEnc));
        stringBuffer30.append(URLDecoder.decode(this.E, Common.KEnc));
        stringBuffer31.append(URLDecoder.decode(this.F, Common.KEnc));
        this.G.writeDebugLog("Xml from Adserver:");
        this.G.writeDebugLog("<?xml version=\"1.0\"?>");
        this.G.writeDebugLog("<ac ver=\"" + this.a + "\">");
        this.G.writeDebugLog("    <type>" + stringBuffer2.toString() + "</type>");
        this.G.writeDebugLog("    <cu>" + stringBuffer3.toString() + "</cu>");
        this.G.writeDebugLog("    <rbu>" + stringBuffer4.toString() + "</rbu>");
        this.G.writeDebugLog("    <itu>" + stringBuffer5.toString() + "</itu>");
        this.G.writeDebugLog("    <ctu>" + stringBuffer6.toString() + "</ctu>");
        this.G.writeDebugLog("    <fc>" + stringBuffer7.toString() + "</fc>");
        this.G.writeDebugLog("    <rt>" + stringBuffer8.toString() + "</rt>");
        this.G.writeDebugLog("    <sa>" + stringBuffer9.toString() + "</sa>");
        this.G.writeDebugLog("    <sc>" + stringBuffer10.toString() + "</sc>");
        this.G.writeDebugLog("    <sptype>" + stringBuffer11.toString() + "</sptype>");
        this.G.writeDebugLog("    <txt>" + stringBuffer13.toString() + "</txt>");
        this.G.writeDebugLog("    <img type=\"" + this.m.toString() + "\">" + stringBuffer12.toString() + "</img>");
        this.G.writeDebugLog("    <video type=\"\"></video>");
        this.G.writeDebugLog("    <btn type=\"" + this.o + "\" x=\"" + this.p + "\" y=\"" + this.q + "\" w=\"" + this.r + "\" h=\"" + this.s + "\">");
        this.G.writeDebugLog("    <curl>" + ((Object) stringBuffer19) + "</curl>");
        this.G.writeDebugLog("    <down>" + ((Object) stringBuffer20) + "</down>");
        this.G.writeDebugLog("    <sms>" + ((Object) stringBuffer21) + ":" + ((Object) stringBuffer22) + "</sms>");
        this.G.writeDebugLog("    <call>" + ((Object) stringBuffer23) + "</call>");
        this.G.writeDebugLog("    </btn>");
        this.G.writeDebugLog("    <fsad>");
        this.G.writeDebugLog("    <fsurl>" + stringBuffer24.toString() + "</fsurl>");
        this.G.writeDebugLog("    <fscurl>" + stringBuffer25.toString() + "</fscurl>");
        this.G.writeDebugLog("    <fsimp>" + stringBuffer26.toString() + "</fsimp>");
        this.G.writeDebugLog("    <fsclk>" + stringBuffer27.toString() + "</fsclk>");
        this.G.writeDebugLog("    </fsad>");
        this.G.writeDebugLog("</ac>");
    }

    public final void setLogger(AdLog adLog) {
        this.G = adLog;
    }
}
