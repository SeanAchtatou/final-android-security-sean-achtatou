package com.adchina.android.ads.views.animations;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;

final class j implements Runnable {
    private /* synthetic */ FadeinAnimation a;

    j(FadeinAnimation fadeinAnimation) {
        this.a = fadeinAnimation;
    }

    public final void run() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.2f, 1.0f);
        alphaAnimation.setDuration(300);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        this.a.a.startAnimation(alphaAnimation);
    }
}
