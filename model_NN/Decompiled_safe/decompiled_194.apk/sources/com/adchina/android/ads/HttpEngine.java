package com.adchina.android.ads;

import android.content.Context;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;

public final class HttpEngine {
    private List a = new LinkedList();
    private List b = new LinkedList();
    private Context c = null;
    private boolean d = false;
    private AdLog e;

    public HttpEngine(Context context) {
        this.c = context;
    }

    private void a(DefaultHttpClient defaultHttpClient) {
        if (!this.a.isEmpty()) {
            CookieStore cookieStore = defaultHttpClient.getCookieStore();
            cookieStore.clear();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.a.size()) {
                    defaultHttpClient.setCookieStore(cookieStore);
                    return;
                }
                BasicClientCookie basicClientCookie = (BasicClientCookie) this.a.get(i2);
                BasicClientCookie basicClientCookie2 = new BasicClientCookie(basicClientCookie.getName(), basicClientCookie.getValue());
                basicClientCookie2.setDomain(basicClientCookie.getDomain());
                cookieStore.addCookie(basicClientCookie2);
                i = i2 + 1;
            }
        }
    }

    private void a(DefaultHttpClient defaultHttpClient, String str) {
        boolean z;
        this.e.writeDebugLog("Cookies:");
        List cookies = defaultHttpClient.getCookieStore().getCookies();
        if (!cookies.isEmpty()) {
            this.b.clear();
            for (int i = 0; i < cookies.size(); i++) {
                Cookie cookie = (Cookie) cookies.get(i);
                BasicClientCookie basicClientCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
                basicClientCookie.setDomain(cookie.getDomain());
                basicClientCookie.setExpiryDate(cookie.getExpiryDate());
                if (basicClientCookie.getValue() != null) {
                    this.e.writeDebugLog(String.valueOf(basicClientCookie.getName()) + ":" + basicClientCookie.getValue());
                }
                this.b.add(basicClientCookie);
            }
            for (int i2 = 0; i2 < this.a.size(); i2++) {
                int i3 = 0;
                while (true) {
                    if (i3 >= cookies.size()) {
                        z = false;
                        break;
                    } else if (((BasicClientCookie) this.a.get(i2)).getName().compareToIgnoreCase(((Cookie) cookies.get(i3)).getName()) == 0) {
                        z = true;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (!z && ((BasicClientCookie) this.a.get(i2)).getValue() != null) {
                    this.b.add((BasicClientCookie) this.a.get(i2));
                }
            }
            writeCookieToFile(str);
        }
    }

    public final void closeStream(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e2) {
                Log.e(Common.KLogTag, "closeStream:" + e2.toString());
            }
        }
    }

    /* access modifiers changed from: protected */
    public final String getCookieFilename(String str) {
        return "adchinaCookie.ce" + str;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00e9 A[SYNTHETIC, Splitter:B:19:0x00e9] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ee A[Catch:{ IOException -> 0x00f9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0118 A[SYNTHETIC, Splitter:B:30:0x0118] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x011d A[Catch:{ IOException -> 0x0121 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void readCookieFromFile(java.lang.String r13) {
        /*
            r12 = this;
            r3 = 0
            r10 = -1
            com.adchina.android.ads.AdLog r0 = r12.e
            java.lang.String r1 = "++ readCookieFromFile"
            r0.writeLog(r1)
            java.util.List r0 = r12.a
            r0.clear()
            java.lang.String r0 = r12.getCookieFilename(r13)     // Catch:{ Exception -> 0x00cc, all -> 0x0113 }
            android.content.Context r1 = r12.c     // Catch:{ Exception -> 0x00cc, all -> 0x0113 }
            java.io.File r1 = r1.getFileStreamPath(r0)     // Catch:{ Exception -> 0x00cc, all -> 0x0113 }
            long r1 = r1.length()     // Catch:{ Exception -> 0x00cc, all -> 0x0113 }
            int r1 = (int) r1     // Catch:{ Exception -> 0x00cc, all -> 0x0113 }
            char[] r1 = new char[r1]     // Catch:{ Exception -> 0x00cc, all -> 0x0113 }
            android.content.Context r2 = r12.c     // Catch:{ Exception -> 0x00cc, all -> 0x0113 }
            java.io.FileInputStream r0 = r2.openFileInput(r0)     // Catch:{ Exception -> 0x00cc, all -> 0x0113 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            r2.<init>(r0)     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            r2.read(r1)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            r1 = r3
        L_0x0033:
            java.lang.String r3 = "name:"
            int r3 = r1.indexOf(r3)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            if (r10 == r3) goto L_0x013b
            int r3 = r3 + 5
            java.lang.String r4 = "\r\n"
            int r4 = r1.indexOf(r4)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.String r3 = r1.substring(r3, r4)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            int r4 = r4 + 2
            java.lang.String r1 = r1.substring(r4)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.String r4 = "value:"
            int r4 = r1.indexOf(r4)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            if (r10 == r4) goto L_0x013b
            int r4 = r4 + 6
            java.lang.String r5 = "\r\n"
            int r5 = r1.indexOf(r5)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.String r4 = r1.substring(r4, r5)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            int r5 = r5 + 2
            java.lang.String r1 = r1.substring(r5)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.String r5 = "domain:"
            int r5 = r1.indexOf(r5)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            if (r10 == r5) goto L_0x013b
            int r5 = r5 + 7
            java.lang.String r6 = "\r\n"
            int r6 = r1.indexOf(r6)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.String r5 = r1.substring(r5, r6)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            org.apache.http.impl.cookie.BasicClientCookie r7 = new org.apache.http.impl.cookie.BasicClientCookie     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            r7.<init>(r3, r4)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            r7.setDomain(r5)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.util.List r8 = r12.a     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            r8.add(r7)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            com.adchina.android.ads.AdLog r7 = r12.e     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.String r9 = "name:"
            r8.<init>(r9)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.StringBuilder r3 = r8.append(r3)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            r7.writeLog(r3)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            com.adchina.android.ads.AdLog r3 = r12.e     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.String r8 = "value:"
            r7.<init>(r8)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.StringBuilder r4 = r7.append(r4)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            r3.writeLog(r4)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            com.adchina.android.ads.AdLog r3 = r12.e     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.String r7 = "domain:"
            r4.<init>(r7)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            r3.writeLog(r4)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            int r3 = r6 + 2
            java.lang.String r1 = r1.substring(r3)     // Catch:{ Exception -> 0x0171, all -> 0x0163 }
            goto L_0x0033
        L_0x00cc:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x00cf:
            com.adchina.android.ads.AdLog r3 = r12.e     // Catch:{ all -> 0x0169 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0169 }
            java.lang.String r5 = "Exceptions in readCookieFromFile , err = "
            r4.<init>(r5)     // Catch:{ all -> 0x0169 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0169 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0169 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0169 }
            r3.writeLog(r0)     // Catch:{ all -> 0x0169 }
            if (r1 == 0) goto L_0x00ec
            r1.close()     // Catch:{ IOException -> 0x00f9 }
        L_0x00ec:
            if (r2 == 0) goto L_0x00f1
            r2.close()     // Catch:{ IOException -> 0x00f9 }
        L_0x00f1:
            com.adchina.android.ads.AdLog r0 = r12.e
            java.lang.String r1 = "-- readCookieFromFile"
            r0.writeLog(r1)
            return
        L_0x00f9:
            r0 = move-exception
            com.adchina.android.ads.AdLog r1 = r12.e
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Exceptions in readCookieFromFile release resource, err = "
            r2.<init>(r3)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.writeLog(r0)
            goto L_0x00f1
        L_0x0113:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0116:
            if (r1 == 0) goto L_0x011b
            r1.close()     // Catch:{ IOException -> 0x0121 }
        L_0x011b:
            if (r2 == 0) goto L_0x0120
            r2.close()     // Catch:{ IOException -> 0x0121 }
        L_0x0120:
            throw r0
        L_0x0121:
            r1 = move-exception
            com.adchina.android.ads.AdLog r2 = r12.e
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Exceptions in readCookieFromFile release resource, err = "
            r3.<init>(r4)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.writeLog(r1)
            goto L_0x0120
        L_0x013b:
            r2.close()     // Catch:{ IOException -> 0x0144 }
            if (r0 == 0) goto L_0x00f1
            r0.close()     // Catch:{ IOException -> 0x0144 }
            goto L_0x00f1
        L_0x0144:
            r0 = move-exception
            com.adchina.android.ads.AdLog r1 = r12.e
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Exceptions in readCookieFromFile release resource, err = "
            r2.<init>(r3)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.writeLog(r0)
            goto L_0x00f1
        L_0x015e:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x0116
        L_0x0163:
            r1 = move-exception
            r11 = r1
            r1 = r2
            r2 = r0
            r0 = r11
            goto L_0x0116
        L_0x0169:
            r0 = move-exception
            goto L_0x0116
        L_0x016b:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x00cf
        L_0x0171:
            r1 = move-exception
            r11 = r1
            r1 = r2
            r2 = r0
            r0 = r11
            goto L_0x00cf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.HttpEngine.readCookieFromFile(java.lang.String):void");
    }

    public final InputStream requestGet(String str) {
        HttpEntity entity;
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(str);
        httpGet.setHeader("Connection", "close");
        HttpResponse execute = defaultHttpClient.execute(httpGet);
        if (execute.getStatusLine().getStatusCode() != 200 || (entity = execute.getEntity()) == null) {
            return null;
        }
        return entity.getContent();
    }

    public final InputStream requestGet(String str, String str2) {
        HttpEntity entity;
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(str);
        httpGet.setHeader("Connection", "close");
        if (!this.d) {
            readCookieFromFile(str2);
            this.d = true;
        }
        a(defaultHttpClient);
        HttpResponse execute = defaultHttpClient.execute(httpGet);
        if (execute.getStatusLine().getStatusCode() != 200 || (entity = execute.getEntity()) == null) {
            return null;
        }
        InputStream content = entity.getContent();
        a(defaultHttpClient, str2);
        return content;
    }

    public final boolean requestSend(String str) {
        if (str.length() <= 0) {
            return false;
        }
        return new DefaultHttpClient().execute(new HttpGet(str)).getStatusLine().getStatusCode() == 200;
    }

    public final void setLogger(AdLog adLog) {
        this.e = adLog;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0127 A[SYNTHETIC, Splitter:B:32:0x0127] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x012c A[Catch:{ IOException -> 0x0133 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void writeCookieToFile(java.lang.String r9) {
        /*
            r8 = this;
            r4 = 0
            r3 = 0
            com.adchina.android.ads.AdLog r0 = r8.e
            java.lang.String r1 = "++ writeCookieToFile"
            r0.writeLog(r1)
            java.util.List r0 = r8.a
            r0.clear()
            java.lang.String r0 = r8.getCookieFilename(r9)     // Catch:{ Exception -> 0x00f8, all -> 0x0122 }
            android.content.Context r1 = r8.c     // Catch:{ Exception -> 0x00f8, all -> 0x0122 }
            r1.deleteFile(r0)     // Catch:{ Exception -> 0x00f8, all -> 0x0122 }
            android.content.Context r1 = r8.c     // Catch:{ Exception -> 0x00f8, all -> 0x0122 }
            r2 = 0
            java.io.FileOutputStream r1 = r1.openFileOutput(r0, r2)     // Catch:{ Exception -> 0x00f8, all -> 0x0122 }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x0140, all -> 0x0135 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0140, all -> 0x0135 }
        L_0x0023:
            java.util.List r0 = r8.b     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            if (r3 < r0) goto L_0x003b
            r2.close()     // Catch:{ IOException -> 0x0130 }
            if (r1 == 0) goto L_0x0033
            r1.close()     // Catch:{ IOException -> 0x0130 }
        L_0x0033:
            com.adchina.android.ads.AdLog r0 = r8.e
            java.lang.String r1 = "-- writeCookieToFile"
            r0.writeLog(r1)
            return
        L_0x003b:
            java.util.List r0 = r8.b     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            org.apache.http.impl.cookie.BasicClientCookie r0 = (org.apache.http.impl.cookie.BasicClientCookie) r0     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r4 = r0.getValue()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            if (r4 == 0) goto L_0x00f3
            java.util.List r4 = r8.a     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r4.add(r0)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r5 = "name:"
            r4.<init>(r5)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r5 = r0.getName()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r5 = "\r\n"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r2.write(r4)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r2.flush()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r5 = "value:"
            r4.<init>(r5)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r5 = r0.getValue()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r5 = "\r\n"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r2.write(r4)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r2.flush()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r5 = "domain:"
            r4.<init>(r5)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r5 = r0.getDomain()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r5 = "\r\n"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r2.write(r4)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r2.flush()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            com.adchina.android.ads.AdLog r4 = r8.e     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r6 = "name:"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r6 = r0.getName()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r4.writeLog(r5)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            com.adchina.android.ads.AdLog r4 = r8.e     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r6 = "value:"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r6 = r0.getValue()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r4.writeLog(r5)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            com.adchina.android.ads.AdLog r4 = r8.e     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r6 = "domain:"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r0 = r0.getDomain()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r4.writeLog(r0)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
        L_0x00f3:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0023
        L_0x00f8:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x00fb:
            com.adchina.android.ads.AdLog r3 = r8.e     // Catch:{ all -> 0x013e }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x013e }
            java.lang.String r5 = "Exceptions in writeCookieToFile, err = "
            r4.<init>(r5)     // Catch:{ all -> 0x013e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x013e }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x013e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x013e }
            r3.writeLog(r0)     // Catch:{ all -> 0x013e }
            if (r1 == 0) goto L_0x0118
            r1.close()     // Catch:{ IOException -> 0x011f }
        L_0x0118:
            if (r2 == 0) goto L_0x0033
            r2.close()     // Catch:{ IOException -> 0x011f }
            goto L_0x0033
        L_0x011f:
            r0 = move-exception
            goto L_0x0033
        L_0x0122:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x0125:
            if (r1 == 0) goto L_0x012a
            r1.close()     // Catch:{ IOException -> 0x0133 }
        L_0x012a:
            if (r2 == 0) goto L_0x012f
            r2.close()     // Catch:{ IOException -> 0x0133 }
        L_0x012f:
            throw r0
        L_0x0130:
            r0 = move-exception
            goto L_0x0033
        L_0x0133:
            r1 = move-exception
            goto L_0x012f
        L_0x0135:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x0125
        L_0x0139:
            r0 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x0125
        L_0x013e:
            r0 = move-exception
            goto L_0x0125
        L_0x0140:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x00fb
        L_0x0144:
            r0 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x00fb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.HttpEngine.writeCookieToFile(java.lang.String):void");
    }
}
