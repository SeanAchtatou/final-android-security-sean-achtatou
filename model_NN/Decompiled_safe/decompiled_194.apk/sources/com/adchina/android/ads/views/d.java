package com.adchina.android.ads.views;

import android.view.MotionEvent;
import android.view.View;

final class d implements View.OnTouchListener {
    private /* synthetic */ ContentView a;

    d(ContentView contentView) {
        this.a = contentView;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.a.j == null) {
            return false;
        }
        this.a.j.onTouchEvent(this.a.i, motionEvent);
        return false;
    }
}
