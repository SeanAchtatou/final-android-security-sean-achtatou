package com.adwo.adsdk;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.adchina.android.ads.Common;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public final class G extends FrameLayout implements C0010h {
    static Context a;
    /* access modifiers changed from: private */
    public static byte c = 0;
    /* access modifiers changed from: private */
    public ImageView b;
    private ProgressBar d = null;
    /* access modifiers changed from: private */
    public ImageView e = null;
    private float f = 20.0f;
    private String g = "安沃传媒  移动广告专家";
    /* access modifiers changed from: private */
    public C0008f h;
    private int i = -1;
    private int j = 0;
    private Bitmap k;
    private Handler l = new Handler();
    private final ScaleAnimation m = new ScaleAnimation(1.1f, 0.9f, 0.1f, 0.9f, 1, 0.5f, 1, 0.5f);
    /* access modifiers changed from: private */
    public final Animation n = new AlphaAnimation(1.0f, 0.2f);
    /* access modifiers changed from: private */
    public T o;

    public final void a(T t) {
        synchronized (this) {
            this.o = t;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adwo.adsdk.i.a(int, int, boolean):byte
     arg types: [int, int, int]
     candidates:
      com.adwo.adsdk.i.a(android.content.Context, byte, byte):com.adwo.adsdk.f
      com.adwo.adsdk.i.a(int, int, boolean):byte */
    public G(Context context, String str) {
        super(context);
        setFocusable(true);
        a = context;
        C0011i.b(str);
        C0011i.b(a);
        try {
            if (this.k == null) {
                this.k = BitmapFactory.decodeStream(a.getAssets().open("adwo_x.png"));
            }
        } catch (IOException e2) {
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) a).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        c = C0011i.a((int) (((float) displayMetrics.widthPixels) * displayMetrics.density), (int) (displayMetrics.density * ((float) displayMetrics.heightPixels)), true);
        this.d = new ProgressBar(a);
        this.d.setIndeterminate(false);
        this.d.setVisibility(0);
        RelativeLayout relativeLayout = new RelativeLayout(a);
        FrameLayout frameLayout = new FrameLayout(a);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        frameLayout.addView(this.d, layoutParams);
        this.b = new ImageView(a);
        this.b.setVisibility(4);
        frameLayout.addView(this.b, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(15, -1);
        layoutParams2.addRule(14, -1);
        relativeLayout.addView(frameLayout, layoutParams2);
        LinearLayout linearLayout = new LinearLayout(a);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        linearLayout.setOrientation(0);
        linearLayout.setLayoutParams(layoutParams3);
        linearLayout.setGravity(5);
        TextView textView = new TextView(a);
        textView.setText(this.g);
        textView.setTextSize(this.f);
        textView.setGravity(5);
        textView.setTextColor(this.i);
        setBackgroundColor(0);
        this.e = new ImageView(a);
        this.e.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        this.e.setClickable(true);
        if (this.k != null) {
            this.e.setImageBitmap(this.k);
        }
        this.e.setOnClickListener(new H(this));
        this.b.setOnClickListener(new I(this));
        this.e.setVisibility(4);
        this.e.setVisibility(0);
        linearLayout.addView(textView, layoutParams3);
        linearLayout.addView(this.e, layoutParams3);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(12);
        layoutParams4.addRule(11);
        relativeLayout.addView(linearLayout, layoutParams4);
        addView(relativeLayout, new FrameLayout.LayoutParams(-2, -2, 17));
        this.l.post(new L(this, a));
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0097  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r5 = this;
            r4 = 0
            com.adwo.adsdk.f r0 = r5.h
            if (r0 == 0) goto L_0x001e
            com.adwo.adsdk.f r0 = r5.h
            java.lang.String r0 = r0.e
            if (r0 != 0) goto L_0x001f
            com.adwo.adsdk.O.a()
            com.adwo.adsdk.T r0 = r5.o
            if (r0 == 0) goto L_0x0017
            com.adwo.adsdk.T r0 = r5.o
            r0.b()
        L_0x0017:
            android.content.Context r5 = com.adwo.adsdk.G.a
            android.app.Activity r5 = (android.app.Activity) r5
            r5.finish()
        L_0x001e:
            return
        L_0x001f:
            java.lang.String r1 = "/"
            int r1 = r0.lastIndexOf(r1)
            int r1 = r1 + 1
            java.lang.String r2 = "."
            int r2 = r0.lastIndexOf(r2)
            java.lang.String r1 = r0.substring(r1, r2)
            java.lang.String r1 = r1.toLowerCase()
            android.content.Context r2 = com.adwo.adsdk.G.a
            com.adwo.adsdk.a r2 = com.adwo.adsdk.C0003a.a(r2)
            boolean r2 = r2.a(r1)
            if (r2 == 0) goto L_0x0092
            android.content.Context r2 = com.adwo.adsdk.G.a     // Catch:{ Exception -> 0x008e }
            com.adwo.adsdk.a r2 = com.adwo.adsdk.C0003a.a(r2)     // Catch:{ Exception -> 0x008e }
            byte[] r2 = r2.b(r1)     // Catch:{ Exception -> 0x008e }
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x008e }
            r3.<init>(r2)     // Catch:{ Exception -> 0x008e }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r3)     // Catch:{ Exception -> 0x008e }
        L_0x0054:
            if (r0 == 0) goto L_0x0097
            android.widget.ImageView r1 = r5.b
            r1.setImageBitmap(r0)
            android.widget.ProgressBar r0 = r5.d
            r1 = 4
            r0.setVisibility(r1)
            android.widget.ImageView r0 = r5.e
            r0.setVisibility(r4)
            android.widget.ImageView r0 = r5.b
            r0.setVisibility(r4)
            com.adwo.adsdk.f r0 = r5.h
            r0.i = r5
            com.adwo.adsdk.T r0 = r5.o
            if (r0 == 0) goto L_0x0075
            com.adwo.adsdk.T r0 = r5.o
        L_0x0075:
            android.view.animation.ScaleAnimation r0 = r5.m
            r1 = 1000(0x3e8, double:4.94E-321)
            r0.setDuration(r1)
            android.view.animation.ScaleAnimation r0 = r5.m
            com.adwo.adsdk.K r1 = new com.adwo.adsdk.K
            r1.<init>(r5)
            r0.setAnimationListener(r1)
            android.widget.ImageView r0 = r5.b
            android.view.animation.ScaleAnimation r1 = r5.m
            r0.startAnimation(r1)
            goto L_0x001e
        L_0x008e:
            r2 = move-exception
            r2.printStackTrace()
        L_0x0092:
            android.graphics.Bitmap r0 = r5.a(r0, r1)
            goto L_0x0054
        L_0x0097:
            com.adwo.adsdk.T r0 = r5.o
            if (r0 == 0) goto L_0x001e
            com.adwo.adsdk.T r0 = r5.o
            r0.b()
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.G.a():void");
    }

    private Bitmap a(String str, String str2) {
        Bitmap bitmap;
        InputStream inputStream;
        try {
            "httpCall to url : " + str;
            O.a();
            HttpGet httpGet = new HttpGet(str);
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, O.a);
            HttpConnectionParams.setSoTimeout(basicHttpParams, Common.KTimerDelay);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            httpGet.setHeader("User-Agent", C0011i.g);
            if (C0011i.a != null) {
                httpGet.setHeader("Cookie", C0011i.a);
            }
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            if (execute.getStatusLine().getStatusCode() == 200) {
                inputStream = execute.getEntity().getContent();
            } else {
                inputStream = null;
            }
            if (inputStream == null) {
                return null;
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                int read = inputStream.read();
                if (read == -1) {
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    Bitmap decodeByteArray = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                    try {
                        C0003a.a(a).a(str2, byteArray);
                        byteArrayOutputStream.close();
                        inputStream.close();
                        return decodeByteArray;
                    } catch (IOException e2) {
                        bitmap = decodeByteArray;
                        Log.e("Splash Ads", "Unable to fetch image for ad from adwo server.");
                        return bitmap;
                    }
                } else {
                    byteArrayOutputStream.write(read);
                }
            }
        } catch (IOException e3) {
            bitmap = null;
        }
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public final void f() {
    }

    public final void e() {
        O.a();
        ((Activity) a).finish();
    }
}
