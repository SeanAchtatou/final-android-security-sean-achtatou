package com.adwo.adsdk;

import android.view.animation.Animation;

final class C implements Animation.AnimationListener {
    private /* synthetic */ AdwoAdView a;
    private final /* synthetic */ C0004b b;
    private final /* synthetic */ M c;

    C(AdwoAdView adwoAdView, C0004b bVar, M m) {
        this.a = adwoAdView;
        this.b = bVar;
        this.c = m;
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.post(new E(this.a, this.b, this.c));
    }

    public final void onAnimationRepeat(Animation animation) {
    }
}
