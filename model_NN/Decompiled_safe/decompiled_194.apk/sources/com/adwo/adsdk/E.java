package com.adwo.adsdk;

import android.view.animation.DecelerateInterpolator;

final class E implements Runnable {
    /* access modifiers changed from: private */
    public C0004b a;
    /* access modifiers changed from: private */
    public C0004b b;
    private M c;
    /* access modifiers changed from: private */
    public /* synthetic */ AdwoAdView d;

    public E(AdwoAdView adwoAdView, C0004b bVar, M m) {
        this.d = adwoAdView;
        this.a = bVar;
        this.c = m;
    }

    public final void run() {
        this.b = this.d.c;
        if (this.b != null) {
            this.b.setVisibility(8);
            this.b.c();
        }
        this.a.setVisibility(0);
        S s = new S(90.0f, 0.0f, ((float) this.d.getWidth()) / 2.0f, ((float) this.d.getHeight()) / 2.0f, 0.0f, 0.0f, -0.4f * ((float) this.d.getWidth()), false, this.c);
        s.setDuration(500);
        s.setFillAfter(true);
        s.setInterpolator(new DecelerateInterpolator());
        s.setAnimationListener(new F(this));
        this.d.startAnimation(s);
    }
}
