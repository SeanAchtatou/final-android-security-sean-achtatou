package com.adwo.adsdk;

public interface AdListener {
    void onFailedToReceiveAd(AdwoAdView adwoAdView);

    void onFailedToReceiveRefreshedAd(AdwoAdView adwoAdView);

    void onReceiveAd(AdwoAdView adwoAdView);
}
