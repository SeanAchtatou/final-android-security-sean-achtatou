package com.adwo.adsdk;

import android.app.Activity;
import android.os.Bundle;
import cn.domob.android.ads.DomobAdManager;

public class AdwoAdBrowserActivity extends Activity {
    private static String[] b = {"explode", "toptobottom", "bottomtotop"};
    private C0012j a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        long j;
        super.onCreate(bundle);
        requestWindowFeature(2);
        setProgressBarVisibility(true);
        int i = 0;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            long j2 = extras.getLong("transitionTime", 600);
            extras.getString("overlayTransition");
            i = extras.getInt("shouldResizeOverlay", 0);
            boolean z5 = extras.getBoolean("shouldShowTitlebar", false);
            extras.getString("overlayTitle");
            boolean z6 = extras.getBoolean("shouldShowBottomBar", true);
            boolean z7 = extras.getBoolean("shouldEnableBottomBar", true);
            z = extras.getBoolean("shouldMakeOverlayTransparent", false);
            z2 = z7;
            z3 = z6;
            z4 = z5;
            j = j2;
        } else {
            z = false;
            z2 = true;
            z3 = true;
            z4 = true;
            j = 600;
        }
        this.a = new C0012j(this, i, j, b[(int) (Math.random() * 3.0d)], z4, z3, z2, z);
        setContentView(this.a);
        String stringExtra = getIntent().getStringExtra(DomobAdManager.ACTION_URL);
        if (stringExtra != null) {
            this.a.a(stringExtra);
        } else {
            this.a.a("http://www.adwo.com");
        }
        setRequestedOrientation(1);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
