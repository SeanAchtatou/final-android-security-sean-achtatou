package com.madhouse.android.ads;

import I.I;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;

final class _ {
    private _() {
    }

    protected static final m _(Context context) {
        m mVar = new m();
        int __ = __(context);
        if (__ == 0 || __ == 2) {
            mVar._ = __;
            return mVar;
        }
        try {
            Cursor query = context.getContentResolver().query(Uri.parse(I.I(1686)), null, null, null, null);
            query.moveToFirst();
            if (query.getColumnIndex(I.I(1725)) != -1 || query.getColumnIndex(I.I(1731)) != -1 || query.getColumnIndex(I.I(1737)) != -1 || query.getColumnIndex(I.I(1743)) != -1) {
                if (query != null) {
                    query.close();
                }
                mVar._ = __;
                return mVar;
            } else if (query.getInt(query.getColumnIndex(I.I(1750))) > 0) {
                mVar._ = __;
                mVar.__ = query.getString(query.getColumnIndex(I.I(1750)));
                mVar.___ = query.getString(query.getColumnIndex(I.I(1756)));
                if (query != null) {
                    query.close();
                }
                return mVar;
            } else if (query.getInt(query.getColumnIndex(I.I(1761))) > 0) {
                mVar._ = __;
                mVar.__ = query.getString(query.getColumnIndex(I.I(1761)));
                mVar.___ = query.getString(query.getColumnIndex(I.I(1770)));
                if (query != null) {
                    query.close();
                }
                return mVar;
            } else {
                if (query != null) {
                    query.close();
                }
                mVar._ = __;
                return mVar;
            }
        } catch (Exception e) {
            mVar._ = __;
            return mVar;
        }
    }

    private static int __(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(I.I(1778));
            if (connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected()) {
                if (connectivityManager.getActiveNetworkInfo().getType() == 0) {
                    return 1;
                }
                if (connectivityManager.getActiveNetworkInfo().getType() == 1) {
                    return 2;
                }
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }
}
