package com.smaato.SOMA;

import android.content.Context;
import android.util.AttributeSet;
import com.smaato.SOMA.AdDownloader;

public class SOMAMedRectBanner extends SOMABanner {
    public SOMAMedRectBanner(Context context) {
        super(context);
    }

    public SOMAMedRectBanner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SOMAMedRectBanner(Context context, AttributeSet attrs, int defaultStyle) {
        super(context, attrs, defaultStyle);
    }

    public AdDownloader.MediaType getMediaType() {
        return AdDownloader.MediaType.MEDRECT;
    }

    public void setMediaType(AdDownloader.MediaType mediaType) {
    }
}
