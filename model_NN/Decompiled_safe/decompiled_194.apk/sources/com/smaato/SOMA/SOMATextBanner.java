package com.smaato.SOMA;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

public class SOMATextBanner extends TextView {
    private static final Typeface AD_FONT = Typeface.create(Typeface.SANS_SERIF, 1);
    public static final int DEFAULT_BACKGROUND_COLOR = -16777216;
    private static final int GRADIENT_BACKGROUND_COLOR = -1;
    private static final double GRADIENT_STOP = 0.5d;
    private static final int GRADIENT_TOP_ALPHA = 127;
    private int backgroundColor = -7829368;
    private Drawable defaultBackground;
    private Drawable focusedBackground;
    private String mText;
    private int textColor = DEFAULT_BACKGROUND_COLOR;

    private static void drawBackground(Canvas c, Rect r, int backgroundColor2, int color) {
        Paint paint = new Paint();
        paint.setColor(backgroundColor2);
        paint.setAntiAlias(true);
        c.drawRect(r, paint);
        GradientDrawable shine = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb((int) GRADIENT_TOP_ALPHA, Color.red(color), Color.green(color), Color.blue(color)), color});
        int stop = ((int) (((double) r.height()) * GRADIENT_STOP)) + r.top;
        shine.setBounds(r.left, r.top, r.right, stop);
        shine.draw(c);
        Rect shadowRect = new Rect(r.left, stop, r.right, r.bottom);
        Paint shadowPaint = new Paint();
        shadowPaint.setColor(color);
        c.drawRect(shadowRect, shadowPaint);
    }

    private static void drawIcon(Canvas c, Rect r) {
        Paint bg = new Paint();
        bg.setColor(-16764058);
        c.drawRect(r, bg);
        Paint o = new Paint();
        o.setColor(-1);
        o.setAntiAlias(true);
        o.setStyle(Paint.Style.STROKE);
        o.setStrokeWidth(4.0f);
        c.drawCircle((float) (((double) r.left) + (0.42d * ((double) r.width()))), (float) (((double) r.top) + (0.62d * ((double) r.height()))), 10.0f, o);
        c.drawArc(new RectF((float) r.left, (float) (r.top + 7), (float) r.right, (float) (r.bottom + 12)), 280.0f, 50.0f, false, o);
        c.drawArc(new RectF((float) (r.left - 5), (float) (r.top + 12), (float) (r.right - 4), (float) (r.bottom + 16)), 290.0f, 33.0f, false, o);
    }

    public SOMATextBanner(Context context) {
        super(context);
        initBanner();
    }

    public SOMATextBanner(Context context, AttributeSet attrs) {
        super(context, attrs);
        initBanner();
    }

    public SOMATextBanner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initBanner();
    }

    public String getText() {
        return this.mText;
    }

    public int getTextColor() {
        return this.textColor;
    }

    public void setbgColor(int color) {
        this.backgroundColor = color;
        createBgDrawables();
    }

    public void setText(String text) {
        this.mText = text;
    }

    public void setTextColor(int color) {
        this.textColor = color;
    }

    private void createBgDrawables() {
        Rect r = new Rect(0, 0, 320, 50);
        this.defaultBackground = generateBackgroundDrawable(r, -1, this.backgroundColor, false);
        this.focusedBackground = generateBackgroundDrawable(r, -1, this.backgroundColor, true);
        setBackgroundDrawable(this.defaultBackground);
    }

    private Drawable generateBackgroundDrawable(Rect r, int backgroundColor2, int color, boolean addFocusRing) {
        Bitmap bitmap = Bitmap.createBitmap(r.width(), r.height(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawBackground(canvas, r, backgroundColor2, color);
        drawIcon(canvas, new Rect(r.right - 41, r.top, r.right, r.bottom));
        if (addFocusRing) {
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(-1147097);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(4.0f);
            paint.setPathEffect(new CornerPathEffect(4.0f));
            Path path = new Path();
            path.addRoundRect(new RectF(r), 4.0f, 4.0f, Path.Direction.CW);
            canvas.drawPath(path, paint);
        }
        return new BitmapDrawable(bitmap);
    }

    private void initBanner() {
        this.defaultBackground = null;
        this.focusedBackground = null;
        createBgDrawables();
        setFocusable(true);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas c) {
        super.onDraw(c);
        try {
            String t = this.mText;
            if (t != null && t.length() > 0) {
                int height = getMeasuredHeight();
                int width = getMeasuredWidth() - 41;
                Paint paint = new Paint();
                paint.setAntiAlias(true);
                paint.setColor(this.textColor);
                paint.setTypeface(AD_FONT);
                paint.setTextSize(22.0f);
                for (float sizeX = paint.measureText(t); sizeX > ((float) width); sizeX = paint.measureText(t)) {
                    paint.setTextSize(paint.getTextSize() - 0.5f);
                }
                paint.setTextAlign(Paint.Align.CENTER);
                Paint.FontMetrics fm = paint.getFontMetrics();
                c.drawText(t, (float) (width / 2), ((float) (height / 2)) - ((fm.ascent + fm.descent) / 2.0f), paint);
            }
        } catch (Exception e) {
            Log.w("SOMA", "Can't draw the new text banner.", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean gainFocus, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
        if (gainFocus) {
            setBackgroundDrawable(this.focusedBackground);
        } else {
            setBackgroundDrawable(this.defaultBackground);
        }
    }
}
