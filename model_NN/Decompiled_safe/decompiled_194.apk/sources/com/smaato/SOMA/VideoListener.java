package com.smaato.SOMA;

public interface VideoListener {
    void onVideoFinished(SOMAVideo sOMAVideo);

    void onVideoPrepared(SOMAVideo sOMAVideo);
}
