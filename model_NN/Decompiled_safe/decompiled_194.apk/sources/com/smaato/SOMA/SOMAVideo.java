package com.smaato.SOMA;

public interface SOMAVideo {

    public enum VideoState {
        EMPTY,
        STOPPED,
        FINISHED,
        PAUSED,
        RUNNING
    }

    VideoState getState();

    void pause();

    void start();

    void stop();
}
