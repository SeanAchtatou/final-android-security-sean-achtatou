package com.smaato.SOMA;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import cn.domob.android.ads.DomobAdManager;

public class SOMADialog extends Dialog {
    private static final String profileAgeKey = "SOMAAgeProfile";
    private static final String profileEnableKey = "SOMAProfileEnable";
    private static final String profileGenderKey = "SOMAGenderProfile";
    private static final String profileKey = "SOMAProfile";
    SOMAProfile ll;
    SOMAUserProfile sup;

    public class SOMAProfile extends LinearLayout {
        EditText ageEditText;
        TextView ageTextView;
        CheckBox enablePro;
        TextView footerView;
        RadioButton genderF;
        RadioGroup genderG;
        RadioButton genderM;
        TextView genderTextView;
        int initAge = 0;
        boolean initEnabled = true;
        String initGender = null;
        Button okButton;

        public SOMAProfile(Context context) {
            super(context);
        }

        public SOMAProfile(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public void setInit(boolean isEnabled, int userAge, String userGender) {
            this.initEnabled = isEnabled;
            this.initAge = userAge;
            this.initGender = userGender;
            initView(getContext());
        }

        private void initView(Context context) {
            setOrientation(1);
            this.ageTextView = new TextView(getContext());
            this.ageEditText = new EditText(getContext());
            this.genderTextView = new TextView(getContext());
            this.footerView = new TextView(getContext());
            this.okButton = new Button(getContext());
            this.enablePro = new CheckBox(getContext());
            this.genderG = new RadioGroup(getContext());
            this.genderF = new RadioButton(getContext());
            this.genderM = new RadioButton(getContext());
            LinearLayout llh = new LinearLayout(getContext());
            llh.setOrientation(0);
            this.genderF.setText("Female");
            this.genderF.setId(1002);
            this.genderM.setText("Male");
            this.genderM.setId(1003);
            if (this.initGender != null) {
                if (this.initGender.equals(DomobAdManager.GENDER_FEMALE)) {
                    this.genderF.setChecked(true);
                } else if (this.initGender.equals("m")) {
                    this.genderM.setChecked(true);
                }
            }
            this.genderG.setOrientation(0);
            this.genderG.addView(this.genderF, new LinearLayout.LayoutParams(-2, -2));
            this.genderG.addView(this.genderM, new LinearLayout.LayoutParams(-2, -2));
            this.footerView.setText("The anonymous data is used only to target advertising within the application according to the entered information.");
            this.ageTextView.setText("Age:");
            if (this.initAge > 0) {
                this.ageEditText.setText(String.valueOf(this.initAge));
            } else {
                this.ageEditText.setHint("35");
            }
            this.ageEditText.setSingleLine(true);
            this.genderTextView.setText("Gender:");
            this.okButton.setText("OK");
            this.enablePro.setText("Enable Profile Setting");
            if (this.initEnabled) {
                this.enablePro.setChecked(true);
            }
            llh.addView(this.ageEditText, new LinearLayout.LayoutParams(-2, -1));
            llh.addView(this.enablePro, new LinearLayout.LayoutParams(-2, -1));
            addView(this.genderTextView, new LinearLayout.LayoutParams(-2, -2));
            addView(this.genderG, new LinearLayout.LayoutParams(-2, -2));
            addView(this.ageTextView, new LinearLayout.LayoutParams(-2, -2));
            addView(llh, new LinearLayout.LayoutParams(-2, -2));
            addView(this.footerView, new LinearLayout.LayoutParams(-2, -2));
            addView(this.okButton, new LinearLayout.LayoutParams(-1, -2));
        }
    }

    private class OKListener implements View.OnClickListener {
        private OKListener() {
        }

        /* synthetic */ OKListener(SOMADialog sOMADialog, OKListener oKListener) {
            this();
        }

        public void onClick(View v) {
            SOMADialog.this.saveProfile();
            SOMADialog.this.dismiss();
        }
    }

    public SOMADialog(Context context) {
        super(context);
    }

    public SOMADialog(Context context, boolean cancelable, DialogInterface.OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public SOMADialog(Context context, int theme) {
        super(context, theme);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("SOMA Anoymous Profile");
        this.sup = new SOMAUserProfile(getContext());
        boolean supEnable = getContext().getSharedPreferences(profileKey, 0).getBoolean(profileEnableKey, true);
        int supAge = SOMAUserProfile.getSomaAge();
        String supGender = SOMAUserProfile.getSomaGender();
        this.ll = new SOMAProfile(getContext());
        this.ll.setInit(supEnable, supAge, supGender);
        setContentView(this.ll);
        this.ll.okButton.setOnClickListener(new OKListener(this, null));
    }

    /* access modifiers changed from: private */
    public void saveProfile() {
        CheckBox enable = this.ll.enablePro;
        RadioGroup gender = this.ll.genderG;
        EditText age = this.ll.ageEditText;
        int gi = gender.getCheckedRadioButtonId();
        SharedPreferences.Editor editor = getContext().getSharedPreferences(profileKey, 0).edit();
        if (enable.isChecked()) {
            editor.putBoolean(profileEnableKey, true);
            if (gi == 1002) {
                SOMAUserProfile.setSomaGender(DomobAdManager.GENDER_FEMALE);
                editor.putString(profileGenderKey, DomobAdManager.GENDER_FEMALE);
                editor.commit();
            } else if (gi == 1003) {
                SOMAUserProfile.setSomaGender("m");
                editor.putString(profileGenderKey, "m");
                editor.commit();
            }
            String ag = age.getText().toString();
            if (ag.length() > 0) {
                int a = Integer.parseInt(ag);
                SOMAUserProfile.setSomaAge(a);
                editor.putInt(profileAgeKey, a);
                editor.commit();
                return;
            }
            return;
        }
        editor.putBoolean(profileEnableKey, false);
        SOMAUserProfile.setSomaAge(0);
        SOMAUserProfile.setSomaGender(null);
        editor.remove(profileGenderKey);
        editor.remove(profileAgeKey);
        editor.commit();
    }
}
