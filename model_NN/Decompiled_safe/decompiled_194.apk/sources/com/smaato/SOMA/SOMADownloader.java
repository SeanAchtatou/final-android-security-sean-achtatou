package com.smaato.SOMA;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.WebView;
import com.smaato.SOMA.AdDownloader;
import com.winad.android.ads.JsonUtils;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SOMADownloader implements AdDownloader, LocationListener {
    private static final String ClientID = "sdkandroid_2-5-1";
    private static int REQUEST_TIMEOUT = 5000;
    private static final String TAG = "SOMA";
    private static boolean mPingSent = false;
    private String UDID;
    private int adSpaceId;
    private String carrier = null;
    private String carriercode = null;
    private String connection = null;
    private Context context;
    private final Runnable doBackgroundThreadProcessing = new Runnable() {
        public void run() {
            SOMADownloader.this.refresh();
        }
    };
    /* access modifiers changed from: private */
    public AdDownloader downloader = null;
    private Handler handler = new Handler();
    private String keywordList = null;
    /* access modifiers changed from: private */
    public ErrorCode lastErrorCode = ErrorCode.NO_ERROR;
    private String lastErrorMessage = "";
    /* access modifiers changed from: private */
    public SOMAReceivedBanner lastReceivedBanner = null;
    /* access modifiers changed from: private */
    public List<AdListener> listenerList = new ArrayList();
    private boolean loadingBanner = false;
    private String locationPar;
    private boolean locationUpdate = false;
    private AdDownloader.MediaType mediaType = AdDownloader.MediaType.ALL;
    private final Runnable notifyBanner = new Runnable() {
        public void run() {
            for (AdListener listener : SOMADownloader.this.listenerList) {
                listener.onReceiveAd(SOMADownloader.this.downloader, SOMADownloader.this.lastReceivedBanner);
            }
        }
    };
    private final Runnable notifyError = new Runnable() {
        public void run() {
            for (AdListener listener : SOMADownloader.this.listenerList) {
                listener.onFailedToReceiveAd(SOMADownloader.this.downloader, SOMADownloader.this.lastErrorCode);
            }
        }
    };
    private int publisherId;
    private String searchQuery = null;
    private SOMAUserProfile sup;
    private String userAgent;

    public boolean isLocationUpdateEnabled() {
        return this.locationUpdate;
    }

    public void setLocation(double latitude, double longitude) {
        try {
            if (!this.locationUpdate) {
                this.locationPar = String.format("%.6f,%.6f", Double.valueOf(latitude), Double.valueOf(longitude));
            }
        } catch (Exception e) {
            Log.w(TAG, "Error during the setLocation.", e);
        }
    }

    public void setLocationUpdateEnabled(boolean locationUpdate2) {
        if (this.locationUpdate != locationUpdate2) {
            this.locationUpdate = locationUpdate2;
            if (!locationUpdate2) {
                try {
                    ((LocationManager) this.context.getSystemService("location")).removeUpdates(this);
                } catch (Exception e) {
                }
            }
            try {
                LocationManager locationManager = (LocationManager) this.context.getSystemService("location");
                String best = locationManager.getBestProvider(new Criteria(), true);
                Log.i(TAG, "Best location provider: " + best);
                locationManager.requestLocationUpdates(best, 60000, 0.0f, this);
            } catch (Exception e2) {
            }
        }
    }

    public SOMADownloader(Context context2) {
        WebView webview = new WebView(context2);
        String ua = webview.getSettings().getUserAgentString();
        webview.destroy();
        initClass(context2, ua);
    }

    public SOMADownloader(Context context2, String userAgent2) {
        initClass(context2, userAgent2);
    }

    private void initClass(Context context2, String userAgent2) {
        this.userAgent = userAgent2;
        this.sup = new SOMAUserProfile(context2);
        this.connection = getConnection(context2);
        this.context = context2;
        this.downloader = this;
        if (!mPingSent) {
            asyncSendTrackingPing(context2);
        }
        this.UDID = getUDID(context2);
    }

    public String getKeywordList() {
        return this.keywordList;
    }

    public String getSearchQuery() {
        return this.searchQuery;
    }

    public void setKeywordList(String keywordList2) {
        if (this.keywordList != null && keywordList2.length() > 0) {
            this.keywordList = keywordList2.toLowerCase();
        }
    }

    public void setSearchQuery(String searchQuery2) {
        if (this.searchQuery != null && searchQuery2.length() > 0) {
            this.searchQuery = searchQuery2.toLowerCase();
        }
    }

    private String getUDID(Context context2) {
        String imei;
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context2.getSystemService("phone");
            if (!(telephonyManager == null || (imei = telephonyManager.getDeviceId()) == null)) {
                this.UDID = imei.toLowerCase();
            }
            if (this.UDID == null || this.UDID.length() == 0) {
                Log.v(TAG, "No IMEI/MEID found, using ANDROID_ID instead");
                this.UDID = Settings.Secure.getString(context2.getContentResolver(), "android_id");
            }
        } catch (Exception e) {
        }
        if (this.UDID == null) {
            this.UDID = "0000000000000000";
        }
        return this.UDID;
    }

    private String getConnection(Context context2) {
        if (this.connection != null) {
            return this.connection;
        }
        if (context2.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == -1) {
            Log.v(TAG, "You should add the permission ACCESS_NETWORK_STATE in the manifest file.");
            return null;
        } else if (context2.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1) {
            Log.v(TAG, "You should add the permission READ_PHONE_STATE in the manifest file.");
            return null;
        } else {
            String result = null;
            try {
                NetworkInfo info = ((ConnectivityManager) context2.getSystemService("connectivity")).getActiveNetworkInfo();
                TelephonyManager tm = (TelephonyManager) context2.getSystemService("phone");
                this.carrier = tm.getNetworkOperatorName();
                this.carriercode = tm.getNetworkOperator();
                int type = info.getType();
                if (type == 1) {
                    result = "wifi";
                } else if (type == 0) {
                    result = "carrier";
                }
            } catch (Exception e) {
            }
            return result;
        }
    }

    private String makeURL() {
        try {
            StringBuffer url = new StringBuffer();
            url.append("http://soma.smaato.net/oapi/reqAd.jsp?");
            if (this.publisherId == 0 || this.adSpaceId == 0) {
                Log.v(TAG, "PubID or AdId is equal to 0, test ad is served.");
            }
            url.append("pub=" + this.publisherId);
            url.append("&adspace=" + this.adSpaceId);
            getMediaType();
            AdDownloader.MediaType mediaType2 = AdDownloader.MediaType.RICHMEDIA;
            url.append("&modifyRM=true");
            url.append("&beacon=true&response=XML&user=900");
            url.append("&ownid=").append(this.UDID);
            url.append("&formatstrict=true");
            if (getMediaType() == AdDownloader.MediaType.MEDRECT) {
                url.append("&format=ALL&dimension=MEDRECT");
            } else {
                url.append("&format=").append(getMediaType());
            }
            url.append("&offline=false");
            if (SOMAUserProfile.getSomaAge() > 0) {
                url.append("&age=").append(SOMAUserProfile.getSomaAge());
            }
            if (SOMAUserProfile.getSomaGender() != null) {
                url.append("&gender=").append(SOMAUserProfile.getSomaGender());
            }
            url.append("&client=").append(URLEncoder.encode(ClientID, "UTF-8"));
            if (this.locationPar != null) {
                url.append("&gps=").append(this.locationPar);
            }
            if (this.connection != null && this.connection.length() > 0) {
                url.append("&connection=").append(this.connection);
            }
            if (this.carrier != null && this.carrier.length() > 0) {
                url.append("&carrier=").append(URLEncoder.encode(this.carrier, "UTF-8"));
            }
            if (this.carriercode != null && this.carrier.length() > 0) {
                url.append("&carriercode=").append(this.carriercode);
            }
            if (this.keywordList != null && this.keywordList.length() > 0) {
                url.append("&kws=").append(URLEncoder.encode(this.keywordList, "UTF-8"));
            }
            if (this.searchQuery != null && this.keywordList.length() > 0) {
                url.append("&qs=").append(URLEncoder.encode(this.searchQuery, "UTF-8"));
            }
            String therequest = url.toString();
            Log.v(TAG, "request URL: " + therequest);
            return therequest;
        } catch (Exception e) {
            Log.w(TAG, "Can't make the url.", e);
            return null;
        }
    }

    /* JADX WARN: Type inference failed for: r7v5, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void connection() {
        /*
            r9 = this;
            java.lang.String r6 = r9.makeURL()
            if (r6 == 0) goto L_0x0051
            r2 = 0
            r4 = 0
            r1 = 0
            java.net.URL r5 = new java.net.URL     // Catch:{ Exception -> 0x0052 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0052 }
            java.net.URLConnection r7 = r5.openConnection()     // Catch:{ Exception -> 0x0052 }
            r0 = r7
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0052 }
            r2 = r0
            java.lang.String r7 = "GET"
            r2.setRequestMethod(r7)     // Catch:{ Exception -> 0x0052 }
            int r7 = com.smaato.SOMA.SOMADownloader.REQUEST_TIMEOUT     // Catch:{ Exception -> 0x0052 }
            r2.setConnectTimeout(r7)     // Catch:{ Exception -> 0x0052 }
            int r7 = com.smaato.SOMA.SOMADownloader.REQUEST_TIMEOUT     // Catch:{ Exception -> 0x0052 }
            r2.setReadTimeout(r7)     // Catch:{ Exception -> 0x0052 }
            java.lang.String r7 = "User-Agent"
            java.lang.String r8 = r9.userAgent     // Catch:{ Exception -> 0x0052 }
            r2.setRequestProperty(r7, r8)     // Catch:{ Exception -> 0x0052 }
            r2.connect()     // Catch:{ Exception -> 0x0052 }
            java.io.InputStream r4 = r2.getInputStream()     // Catch:{ Exception -> 0x0052 }
            com.smaato.SOMA.SOMAReceivedBanner r1 = r9.doParsing(r4)     // Catch:{ Exception -> 0x0052 }
            if (r4 == 0) goto L_0x003d
            r4.close()     // Catch:{ IOException -> 0x0080 }
            r4 = 0
        L_0x003d:
            if (r2 == 0) goto L_0x0043
            r2.disconnect()
            r2 = 0
        L_0x0043:
            r9.releaseLock()
        L_0x0046:
            if (r1 == 0) goto L_0x0051
            r9.lastReceivedBanner = r1
            android.os.Handler r7 = r9.handler
            java.lang.Runnable r8 = r9.notifyBanner
            r7.post(r8)
        L_0x0051:
            return
        L_0x0052:
            r7 = move-exception
            r3 = r7
            com.smaato.SOMA.ErrorCode r7 = com.smaato.SOMA.ErrorCode.NO_CONNECTION_ERROR     // Catch:{ all -> 0x006b }
            java.lang.String r8 = "Can't connect to the Server for ad fetching."
            r9.setError(r7, r8)     // Catch:{ all -> 0x006b }
            if (r4 == 0) goto L_0x0061
            r4.close()     // Catch:{ IOException -> 0x007c }
            r4 = 0
        L_0x0061:
            if (r2 == 0) goto L_0x0067
            r2.disconnect()
            r2 = 0
        L_0x0067:
            r9.releaseLock()
            goto L_0x0046
        L_0x006b:
            r7 = move-exception
            if (r4 == 0) goto L_0x0072
            r4.close()     // Catch:{ IOException -> 0x007e }
            r4 = 0
        L_0x0072:
            if (r2 == 0) goto L_0x0078
            r2.disconnect()
            r2 = 0
        L_0x0078:
            r9.releaseLock()
            throw r7
        L_0x007c:
            r7 = move-exception
            goto L_0x0061
        L_0x007e:
            r8 = move-exception
            goto L_0x0072
        L_0x0080:
            r7 = move-exception
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.smaato.SOMA.SOMADownloader.connection():void");
    }

    private void setError(ErrorCode code, String message) {
        releaseLock();
        this.lastErrorCode = code;
        this.lastErrorMessage = message;
        this.handler.post(this.notifyError);
    }

    public void SOMADLTracking(Context context2) {
        String imei;
        if (context2 == null) {
            Log.e(TAG, "Received uninitialized context!");
            return;
        }
        String UDID2 = null;
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context2.getSystemService("phone");
            if (!(telephonyManager == null || (imei = telephonyManager.getDeviceId()) == null)) {
                UDID2 = imei.toLowerCase();
            }
            if (UDID2 == null || UDID2.length() == 0) {
                Log.v(TAG, "No IMEI/MEID found, using ANDROID_ID instead");
                UDID2 = Settings.Secure.getString(context2.getContentResolver(), "android_id");
            }
        } catch (Exception e) {
        }
        if (UDID2 == null) {
            UDID2 = "0000000000000000";
        }
        HttpURLConnection connection2 = null;
        InputStream inB = null;
        try {
            PackageInfo pi = context2.getPackageManager().getPackageInfo(context2.getPackageName(), 0);
            String an = pi.packageName;
            String av = pi.versionName;
            SharedPreferences settings = context2.getSharedPreferences(an, 0);
            boolean firststart = settings.getBoolean("firstping", true);
            StringBuffer stringBuffer = new StringBuffer("http://soma.smaato.net/oapi/dl.jsp");
            stringBuffer.append("?app=").append(URLEncoder.encode(an, "UTF-8"));
            stringBuffer.append("&ownid=").append(UDID2);
            stringBuffer.append("&version=").append(URLEncoder.encode(av, "UTF-8"));
            stringBuffer.append("&firststart=").append(firststart);
            String theping = stringBuffer.toString();
            Log.v(TAG, "Download tracking ping: " + theping);
            HttpURLConnection connection3 = (HttpURLConnection) new URL(theping).openConnection();
            connection3.setRequestMethod("GET");
            connection3.setConnectTimeout(10000);
            connection3.connect();
            InputStream inB2 = connection3.getInputStream();
            if (connection3.getResponseCode() == 200) {
                if (firststart) {
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putBoolean("firstping", false);
                    editor.commit();
                }
                Log.v(TAG, "Success");
            }
            if (inB2 != null) {
                try {
                    inB2.close();
                } catch (IOException e2) {
                }
            }
            if (connection3 != null) {
                connection3.disconnect();
            }
        } catch (Exception e3) {
            Log.e(TAG, e3.getMessage());
            if (inB != null) {
                try {
                    inB.close();
                } catch (IOException e4) {
                }
            }
            if (connection2 != null) {
                connection2.disconnect();
            }
        } catch (Throwable th) {
            if (inB != null) {
                try {
                    inB.close();
                } catch (IOException e5) {
                }
            }
            if (connection2 != null) {
                connection2.disconnect();
            }
            throw th;
        }
    }

    /* JADX WARN: Type inference failed for: r4v4, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void beaconConnect(java.lang.String r8) {
        /*
            r7 = this;
            r1 = 0
            r2 = 0
            java.net.URL r3 = new java.net.URL     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            r3.<init>(r8)     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            java.net.URLConnection r4 = r3.openConnection()     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            r0 = r4
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            r1 = r0
            java.lang.String r4 = "GET"
            r1.setRequestMethod(r4)     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            int r4 = com.smaato.SOMA.SOMADownloader.REQUEST_TIMEOUT     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            r1.setConnectTimeout(r4)     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            java.lang.String r4 = "User-Agent"
            java.lang.String r5 = r7.userAgent     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            r1.setRequestProperty(r4, r5)     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            r1.connect()     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            java.io.InputStream r2 = r1.getInputStream()     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            java.lang.String r4 = "SOMA"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            java.lang.String r6 = "connect to beacon: "
            r5.<init>(r6)     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            android.util.Log.v(r4, r5)     // Catch:{ Exception -> 0x0048, all -> 0x0056 }
            if (r2 == 0) goto L_0x0041
            r2.close()     // Catch:{ IOException -> 0x006c }
            r2 = 0
        L_0x0041:
            if (r1 == 0) goto L_0x0047
            r1.disconnect()     // Catch:{ Exception -> 0x006e }
            r1 = 0
        L_0x0047:
            return
        L_0x0048:
            r4 = move-exception
            if (r2 == 0) goto L_0x004f
            r2.close()     // Catch:{ IOException -> 0x0064 }
            r2 = 0
        L_0x004f:
            if (r1 == 0) goto L_0x0047
            r1.disconnect()     // Catch:{ Exception -> 0x0066 }
            r1 = 0
            goto L_0x0047
        L_0x0056:
            r4 = move-exception
            if (r2 == 0) goto L_0x005d
            r2.close()     // Catch:{ IOException -> 0x0068 }
            r2 = 0
        L_0x005d:
            if (r1 == 0) goto L_0x0063
            r1.disconnect()     // Catch:{ Exception -> 0x006a }
            r1 = 0
        L_0x0063:
            throw r4
        L_0x0064:
            r4 = move-exception
            goto L_0x004f
        L_0x0066:
            r4 = move-exception
            goto L_0x0047
        L_0x0068:
            r5 = move-exception
            goto L_0x005d
        L_0x006a:
            r5 = move-exception
            goto L_0x0063
        L_0x006c:
            r4 = move-exception
            goto L_0x0041
        L_0x006e:
            r4 = move-exception
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: com.smaato.SOMA.SOMADownloader.beaconConnect(java.lang.String):void");
    }

    private SOMAReceivedBanner doParsing(InputStream in) {
        Exception e;
        ErrorCode error;
        SOMAReceivedBanner banner = null;
        try {
            Element entry = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(in).getDocumentElement();
            if (entry == null || !entry.getNodeName().equals("response")) {
                setError(ErrorCode.PARSING_ERROR, "Error during the XML parsing.Can't find the response tag.");
                Log.w(TAG, "Error during the XML parsing.Can't find the response tag.");
                return null;
            } else if (((Element) entry.getElementsByTagName("status").item(0)).getFirstChild().getNodeValue().equals(JsonUtils.ERROR_ACK)) {
                String code = ((Element) entry.getElementsByTagName("code").item(0)).getFirstChild().getNodeValue();
                String desc = ((Element) entry.getElementsByTagName("desc").item(0)).getFirstChild().getNodeValue();
                ErrorCode errorCode = ErrorCode.NO_ERROR;
                switch (Integer.parseInt(code)) {
                    case 42:
                        error = ErrorCode.NO_AD_AVAILABLE;
                        break;
                    case 106:
                        error = ErrorCode.UNKNOWN_PUBLISHER_ADSPACE_ID_ERROR;
                        break;
                    default:
                        error = ErrorCode.GENERAL_ERROR;
                        break;
                }
                setError(error, desc);
                Log.v(TAG, "code: " + code + " desc: " + desc);
                return null;
            } else {
                SOMAReceivedBanner banner2 = new SOMAReceivedBanner();
                try {
                    Element mAdType = (Element) entry.getElementsByTagName("ad").item(0);
                    String adType = mAdType.getAttribute("type");
                    Log.v(TAG, "adType: " + adType);
                    if (adType.equals("RICHMEDIA")) {
                        banner2.adText = ((Element) mAdType.getElementsByTagName("mediadata").item(0)).getChildNodes().item(0).getNodeValue();
                        Log.d(TAG, "Found rich ad");
                        Log.d(TAG, banner2.adText);
                        banner2.adType = AdDownloader.MediaType.RICHMEDIA;
                    } else {
                        if (adType.equals("TXT")) {
                            banner2.adText = ((Element) entry.getElementsByTagName("adtext").item(0)).getFirstChild().getNodeValue();
                            Log.v(TAG, "adText: " + banner2.adText);
                            banner2.imageURL = null;
                            banner2.adType = AdDownloader.MediaType.TXT;
                        } else if (adType.equals("VIDEO")) {
                            banner2.adText = null;
                            banner2.adType = AdDownloader.MediaType.VIDEO;
                            banner2.mediaFile = ((Element) ((Element) entry.getElementsByTagName("mediafiles").item(0)).getElementsByTagName("mediafile").item(0)).getFirstChild().getNodeValue();
                            Log.v(TAG, "MediaFile URL: " + banner2.mediaFile);
                        } else {
                            banner2.adText = null;
                            banner2.adType = AdDownloader.MediaType.IMG;
                            banner2.link = ((Element) entry.getElementsByTagName("link").item(0)).getFirstChild().getNodeValue();
                            banner2.imageURL = new URL(banner2.link);
                            Log.v(TAG, "ImageURL: " + banner2.imageURL);
                        }
                        banner2.mTarget = ((Element) entry.getElementsByTagName("action").item(0)).getAttribute("target");
                        banner2.targetURL = new URL(banner2.mTarget);
                        Log.v(TAG, "TargetURL: " + banner2.targetURL);
                    }
                    NodeList mBeacons = entry.getElementsByTagName("beacon");
                    if (mBeacons == null || mBeacons.getLength() <= 0) {
                        Log.v(TAG, "beacons: no beacons available.");
                        return banner2;
                    }
                    for (int i = 0; i < mBeacons.getLength(); i++) {
                        beaconConnect(parseValue((Element) mBeacons.item(i)));
                    }
                    return banner2;
                } catch (MalformedURLException e2) {
                    throw new RuntimeException(e2);
                } catch (MalformedURLException e3) {
                    throw new RuntimeException(e3);
                } catch (Exception e4) {
                    e = e4;
                    banner = banner2;
                }
            }
        } catch (Exception e5) {
            e = e5;
        }
        setError(ErrorCode.PARSING_ERROR, "Error during the XML parsing.");
        Log.w(TAG, "Error during the XML parsing.", e);
        return banner;
    }

    private String parseValue(Node node) {
        StringBuffer text = new StringBuffer();
        NodeList nodeChildren = node.getChildNodes();
        for (int i = 0; i < nodeChildren.getLength(); i++) {
            String value = nodeChildren.item(i).getNodeValue();
            if (value != null) {
                text.append(value);
            }
        }
        return text.toString();
    }

    public void asyncLoadNewBanner() {
        synchronized (this) {
            if (tryGetLock()) {
                new Thread(null, this.doBackgroundThreadProcessing, "background").start();
            }
        }
    }

    private boolean tryGetLock() {
        boolean retValue = false;
        synchronized (this) {
            if (!this.loadingBanner) {
                this.loadingBanner = true;
                retValue = true;
            }
        }
        return retValue;
    }

    private void releaseLock() {
        synchronized (this) {
            if (this.loadingBanner) {
                this.loadingBanner = false;
            }
        }
    }

    /* access modifiers changed from: private */
    public void refresh() {
        System.gc();
        Log.v(TAG, "Start new thread for next ad.");
        this.lastErrorCode = ErrorCode.NO_ERROR;
        this.lastErrorMessage = "";
        connection();
        System.gc();
    }

    private void asyncSendTrackingPing(final Context context2) {
        new Thread() {
            public void run() {
                SOMADownloader.this.SOMADLTracking(context2);
            }
        }.start();
    }

    public int getAdSpaceId() {
        return this.adSpaceId;
    }

    public void setAdSpaceId(int adSpaceId2) {
        this.adSpaceId = adSpaceId2;
    }

    public int getPublisherId() {
        return this.publisherId;
    }

    public void setPublisherId(int publisherId2) {
        this.publisherId = publisherId2;
    }

    public AdDownloader.MediaType getMediaType() {
        return this.mediaType;
    }

    public void setMediaType(AdDownloader.MediaType mediaType2) {
        if ((this.context.getResources().getConfiguration().screenLayout & 15) != 4) {
            if (mediaType2 == AdDownloader.MediaType.LEADER) {
                mediaType2 = AdDownloader.MediaType.IMG;
                Log.w(TAG, "Leaderboard is only supported on tablets. Settings type to IMG.");
            } else if (mediaType2 == AdDownloader.MediaType.SKY) {
                mediaType2 = AdDownloader.MediaType.MEDRECT;
                Log.w(TAG, "Skyscraper is only supported on tablets. Settings type to MEDRECT.");
            }
        }
        this.mediaType = mediaType2;
    }

    public void removeAdListener(AdListener adListener) {
        this.listenerList.remove(adListener);
    }

    public void addAdListener(AdListener adListener) {
        this.listenerList.add(adListener);
    }

    public ErrorCode getErrorCode() {
        return this.lastErrorCode;
    }

    public String getErrorMessage() {
        return this.lastErrorMessage;
    }

    public void onLocationChanged(Location location) {
        if (location != null) {
            double lat = location.getLatitude();
            double lon = location.getLongitude();
            this.locationPar = String.format(Locale.US, "%.6f,%.6f", Double.valueOf(lat), Double.valueOf(lon));
        }
    }

    public void onProviderDisabled(String arg0) {
        this.locationPar = "";
    }

    public void onProviderEnabled(String arg0) {
    }

    public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
    }
}
