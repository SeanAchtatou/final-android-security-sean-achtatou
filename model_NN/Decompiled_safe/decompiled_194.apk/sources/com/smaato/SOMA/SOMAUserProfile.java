package com.smaato.SOMA;

import android.content.Context;
import android.content.SharedPreferences;
import cn.domob.android.ads.DomobAdManager;

public class SOMAUserProfile {
    private static final String profileAgeKey = "SOMAAgeProfile";
    private static final String profileEnableKey = "SOMAProfileEnable";
    private static final String profileGenderKey = "SOMAGenderProfile";
    private static final String profileKey = "SOMAProfile";
    private static int somaAge = 0;
    private static String somaGender = null;

    public static int getSomaAge() {
        return somaAge;
    }

    public static String getSomaGender() {
        return somaGender;
    }

    public static void setSomaAge(int somaAge2) {
        if (somaAge2 >= 0 && somaAge2 < 100) {
            somaAge = somaAge2;
        }
    }

    public static void setSomaGender(String somaGender2) {
        if (somaGender2 == null) {
            somaGender = somaGender2;
        } else if (somaGender2 != null && somaGender2.length() > 0) {
            String gender = Character.toString(somaGender2.toLowerCase().charAt(0));
            if (gender.equals(DomobAdManager.GENDER_FEMALE) || gender.equals("m")) {
                somaGender = gender;
            }
        }
    }

    public SOMAUserProfile(Context context) {
        initFetchData(context);
    }

    private void initFetchData(Context context) {
        SharedPreferences settings = context.getSharedPreferences(profileKey, 0);
        if (settings.getBoolean(profileEnableKey, true)) {
            setSomaGender(settings.getString(profileGenderKey, null));
            setSomaAge(settings.getInt(profileAgeKey, 0));
        }
    }
}
