package com.smaato.SOMA;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.VideoView;
import com.smaato.SOMA.SOMAVideo;

public class SOMAVideoPlayer extends FrameLayout implements SOMAVideo {
    Context mContext = null;
    SOMAReceivedBanner mLastReceivedBanner = null;
    ProgressBar mProgress = null;
    SOMAVideo mSOMAVideo = null;
    SOMAVideo.VideoState mState = SOMAVideo.VideoState.EMPTY;
    VideoView mVideo = null;
    VideoListener mVideoListener = new VideoListener() {
        public void onVideoPrepared(SOMAVideo video) {
            video.start();
        }

        public void onVideoFinished(SOMAVideo video) {
        }
    };

    public SOMAVideoPlayer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initVideoBanner(context);
    }

    public SOMAVideoPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
        initVideoBanner(context);
    }

    public SOMAVideoPlayer(Context context) {
        super(context);
        initVideoBanner(context);
    }

    public void setVideoListener(VideoListener listener) {
        this.mVideoListener = listener;
    }

    private void initVideoBanner(Context context) {
        this.mContext = context;
        this.mSOMAVideo = this;
        this.mVideo = new VideoView(context);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(-1, -1);
        params.gravity = 17;
        this.mVideo.setLayoutParams(params);
        this.mVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                SOMAVideoPlayer.this.mProgress.setEnabled(false);
                SOMAVideoPlayer.this.mProgress.setVisibility(8);
                SOMAVideoPlayer.this.mState = SOMAVideo.VideoState.STOPPED;
                SOMAVideoPlayer.this.mVideoListener.onVideoPrepared(SOMAVideoPlayer.this.mSOMAVideo);
                Log.v("SOMA", "Starting video");
            }
        });
        this.mVideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                SOMAVideoPlayer.this.mState = SOMAVideo.VideoState.FINISHED;
                SOMAVideoPlayer.this.mVideoListener.onVideoFinished(SOMAVideoPlayer.this.mSOMAVideo);
            }
        });
        this.mVideo.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                SOMAVideoPlayer.this.mProgress.setEnabled(false);
                SOMAVideoPlayer.this.mProgress.setVisibility(8);
                Log.v("SOMA", "Video error: " + what);
                return false;
            }
        });
        this.mVideo.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (SOMAVideoPlayer.this.mLastReceivedBanner != null) {
                    SOMAVideoPlayer.this.mLastReceivedBanner.openLandingPage(SOMAVideoPlayer.this.mContext);
                }
                Log.v("SOMA", "Video clicked");
                return true;
            }
        });
        addView(this.mVideo);
        this.mProgress = new ProgressBar(context);
        this.mProgress.setLayoutParams(new FrameLayout.LayoutParams(-2, -2, 17));
        this.mProgress.setIndeterminate(true);
        addView(this.mProgress);
    }

    public void downloadStarted() {
        Log.v("SOMA", "Video download started");
        this.mProgress.setEnabled(true);
    }

    public void downloadFinished(SOMAReceivedBanner banner) {
        this.mLastReceivedBanner = banner;
        Log.v("SOMA", "Video url download finished (loading video)");
        this.mVideo.setVideoPath(banner.getMediaFile());
    }

    public void start() {
        this.mVideo.start();
        this.mState = SOMAVideo.VideoState.RUNNING;
    }

    public void stop() {
        this.mVideo.stopPlayback();
        this.mState = SOMAVideo.VideoState.STOPPED;
    }

    public void pause() {
        this.mVideo.pause();
        this.mState = SOMAVideo.VideoState.PAUSED;
    }

    public SOMAVideo.VideoState getState() {
        return this.mState;
    }
}
