package com.smaato.SOMA;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

public class ORMMA_Bridge {
    Context context = null;
    private Handler handler = null;

    public ORMMA_Bridge(Handler handler2, Context context2) {
        this.handler = handler2;
        this.context = context2;
    }

    public void showAlert(String message) {
        Log.i("SOMA", "alert " + message);
    }

    public void activate(String event) {
    }

    public void deactivate(String event) {
    }

    public void close() {
        Log.i("SOMA", "close");
        this.handler.sendMessage(this.handler.obtainMessage(SOMABanner.MESSAGE_CLOSE));
    }

    public void expand(int x, int y, int width, int height, String URL, String expandProperties) {
        Log.i("SOMA", "expand " + width + "x" + height);
        this.handler.sendMessage(this.handler.obtainMessage(SOMABanner.MESSAGE_EXPAND, width, height));
    }

    public void hide() {
    }

    public void open(String URL, boolean back, boolean forward, boolean refresh) {
        this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(URL)));
    }

    public void openMap(String POI, boolean fullscreen) {
    }

    public void playAudio(String URL, boolean autoPlay, boolean controls, boolean loop, int position, String startStyle, String stopStyle) {
    }

    public void playVideo(String URL, boolean audioMuted, boolean autoPlay, boolean controls, boolean loop, int position, String startStyle, String stopStyle) {
    }

    public void resize(int width, int height) {
    }

    public void show() {
    }
}
