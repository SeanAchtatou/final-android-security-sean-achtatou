package com.smaato.SOMA;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SOMADebugView extends RelativeLayout {
    String dataText;
    TextView dataTextView;
    Button okButton;

    public SOMADebugView(Context context) {
        super(context);
        initDebugView(context);
    }

    public SOMADebugView(Context context, String debugData) {
        super(context);
        this.dataText = debugData;
        initDebugView(context);
    }

    private void initDebugView(Context context) {
        LinearLayout llh = new LinearLayout(context);
        llh.setOrientation(1);
        this.dataTextView = new TextView(getContext());
        this.okButton = new Button(getContext());
        this.dataTextView.setText(this.dataText);
        Log.v("SOMA", this.dataText);
        this.okButton.setText("OK");
        llh.addView(this.dataTextView, new LinearLayout.LayoutParams(-2, -2));
        llh.addView(this.okButton, new LinearLayout.LayoutParams(-2, -2));
        addView(llh);
        this.okButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
    }
}
