package com.smaato.SOMA;

public interface AdDownloader {

    public enum MediaType {
        ALL,
        IMG,
        TXT,
        SKY,
        LEADER,
        MEDRECT,
        VIDEO,
        RICHMEDIA
    }

    void addAdListener(AdListener adListener);

    void asyncLoadNewBanner();

    int getAdSpaceId();

    ErrorCode getErrorCode();

    String getErrorMessage();

    String getKeywordList();

    MediaType getMediaType();

    int getPublisherId();

    String getSearchQuery();

    boolean isLocationUpdateEnabled();

    void removeAdListener(AdListener adListener);

    void setAdSpaceId(int i);

    void setKeywordList(String str);

    void setLocation(double d, double d2);

    void setLocationUpdateEnabled(boolean z);

    void setMediaType(MediaType mediaType);

    void setPublisherId(int i);

    void setSearchQuery(String str);
}
