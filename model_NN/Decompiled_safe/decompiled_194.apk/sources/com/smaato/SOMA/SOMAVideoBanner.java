package com.smaato.SOMA;

import android.content.Context;
import android.util.AttributeSet;
import com.smaato.SOMA.AdDownloader;

public class SOMAVideoBanner extends SOMABanner {
    public SOMAVideoBanner(Context context) {
        super(context);
    }

    public SOMAVideoBanner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SOMAVideoBanner(Context context, AttributeSet attrs, int defaultStyle) {
        super(context, attrs, defaultStyle);
    }

    public AdDownloader.MediaType getMediaType() {
        return AdDownloader.MediaType.VIDEO;
    }

    public void setMediaType(AdDownloader.MediaType mediaType) {
    }

    public void setVideoListener(VideoListener listener) {
        this.mVideoListener = listener;
        if (this.videoview != null) {
            this.videoview.setVideoListener(listener);
        }
    }

    public void onVideoPrepared(SOMAVideo video) {
    }

    public void onVideoFinished(SOMAVideo video) {
    }
}
