package com.smaato.SOMA;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.smaato.SOMA.AdDownloader;
import java.net.URL;

public class SOMAReceivedBanner {
    String adText;
    AdDownloader.MediaType adType;
    boolean debugMode = false;
    private boolean hasBeenClicked = false;
    URL imageURL;
    String link;
    String mTarget;
    String mediaFile;
    URL targetURL;

    public String getAdText() {
        if (this.adText != null) {
            return this.adText;
        }
        return "NO";
    }

    public AdDownloader.MediaType getAdType() {
        if (this.adType != null) {
            return this.adType;
        }
        return AdDownloader.MediaType.ALL;
    }

    public void openLandingPage(Context context) {
        if (!this.hasBeenClicked) {
            this.hasBeenClicked = true;
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.mTarget)));
        }
    }

    public String getMediaFile() {
        return this.mediaFile;
    }

    public URL getImageUrl() {
        return this.imageURL;
    }
}
