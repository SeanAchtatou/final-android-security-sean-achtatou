package com.smaato.SOMA;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.adchina.android.ads.Common;
import com.energysource.szj.embeded.AdManager;
import com.smaato.SOMA.AdDownloader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;

public class SOMABanner extends RelativeLayout implements AdDownloader, AdListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$smaato$SOMA$SOMABanner$AnimationType = null;
    public static int MESSAGE_CLOSE = 102;
    public static int MESSAGE_EXPAND = 101;
    private static final String TAG = "SOMA";
    private boolean EnableDebugMode = false;
    private int HEIGHT = 50;
    private Animation.AnimationListener animListener = new Animation.AnimationListener() {
        public void onAnimationEnd(Animation animation) {
            Log.d(SOMABanner.TAG, "Stop animation");
            SOMABanner.this.animating = false;
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    };
    /* access modifiers changed from: private */
    public boolean animating = false;
    private AnimationType animationType = AnimationType.FADE_IN_ANIMATION;
    /* access modifiers changed from: private */
    public boolean autoRefresh = false;
    private int backgroundColor = 0;
    private RelativeLayout baseLayout;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public View currentView = null;
    private SOMADownloader downloader;
    private int fontColor = -65536;
    /* access modifiers changed from: private */
    public Handler handler = null;
    private boolean hideWhenError = true;
    /* access modifiers changed from: private */
    public boolean isWindowFocus = true;
    private SOMAReceivedBanner lastBanner;
    private ErrorCode lastErrorCode = ErrorCode.NO_ERROR;
    private String lastErrorMessage = "";
    private View lastView = null;
    private List<AdListener> listenerList = new ArrayList();
    /* access modifiers changed from: private */
    public float mDensity;
    protected VideoListener mVideoListener = null;
    private WindowManager mWindowManager;
    private ORMMA_Bridge ormmaBridge = null;
    private Timer refreshTimer;
    /* access modifiers changed from: private */
    public Dialog richMediaDialog = null;
    private SOMATextBanner somaTextBannerView;
    /* access modifiers changed from: private */
    public int timerFQ = 60;
    /* access modifiers changed from: private */
    public Runnable updateTime = new Runnable() {
        public void run() {
            SOMABanner.this.handler.removeCallbacks(SOMABanner.this.updateTime);
            SOMABanner.this.asyncLoadNewBanner();
            if (SOMABanner.this.autoRefresh) {
                SOMABanner.this.handler.postDelayed(SOMABanner.this.updateTime, (long) (SOMABanner.this.timerFQ * AdManager.AD_FILL_PARENT));
            }
        }
    };
    private String userAgent;
    protected SOMAVideoPlayer videoview;
    private WebView webview;

    public enum AnimationType {
        NO_ANIMATION,
        FADE_IN_ANIMATION,
        MOVE_ANIMATION,
        RANDOM_ANIMATION,
        ROTATE_ANIMATION
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$smaato$SOMA$SOMABanner$AnimationType() {
        int[] iArr = $SWITCH_TABLE$com$smaato$SOMA$SOMABanner$AnimationType;
        if (iArr == null) {
            iArr = new int[AnimationType.values().length];
            try {
                iArr[AnimationType.FADE_IN_ANIMATION.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[AnimationType.MOVE_ANIMATION.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AnimationType.NO_ANIMATION.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[AnimationType.RANDOM_ANIMATION.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[AnimationType.ROTATE_ANIMATION.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            $SWITCH_TABLE$com$smaato$SOMA$SOMABanner$AnimationType = iArr;
        }
        return iArr;
    }

    public SOMABanner(Context context2) {
        super(context2);
        initBanner(context2, null, 0);
    }

    public SOMABanner(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        initBanner(context2, attrs, 0);
    }

    public SOMABanner(Context context2, AttributeSet attrs, int defStyle) {
        super(context2, attrs, defStyle);
        initBanner(context2, attrs, defStyle);
    }

    public boolean dispatchTouchEvent(MotionEvent e) {
        if (e.getAction() == 1 && this.lastBanner.adType != AdDownloader.MediaType.RICHMEDIA) {
            setisPressed(true);
        }
        return super.dispatchTouchEvent(e);
    }

    public boolean dispatchTrackballEvent(MotionEvent event) {
        if (event.getAction() == 0) {
            setisPressed(true);
        }
        return super.onTrackballEvent(event);
    }

    public void asyncLoadNewBanner() {
        this.lastErrorCode = ErrorCode.NO_ERROR;
        this.lastErrorMessage = "";
        if (this.downloader == null || !this.isWindowFocus || this.animating || this.richMediaDialog != null) {
            Log.d(TAG, "Skip call");
            return;
        }
        if (this.lastView != null) {
            removeView(this.lastView);
            if (this.lastView instanceof WebView) {
                ((WebView) this.lastView).destroy();
            }
            this.lastView = null;
        }
        Log.d(TAG, "Load new banner");
        if (getMediaType() == AdDownloader.MediaType.VIDEO) {
            this.videoview = new SOMAVideoPlayer(getContext());
            RelativeLayout.LayoutParams videoViewParams = new RelativeLayout.LayoutParams(-1, -1);
            videoViewParams.addRule(13);
            this.videoview.setLayoutParams(videoViewParams);
            addView(this.videoview);
            if (this.mVideoListener != null) {
                this.videoview.setVideoListener(this.mVideoListener);
            }
            this.videoview.downloadStarted();
        }
        this.downloader.asyncLoadNewBanner();
    }

    @Deprecated
    public String getAdID() {
        return Integer.toString(this.downloader.getAdSpaceId());
    }

    public int getAge() {
        return SOMAUserProfile.getSomaAge();
    }

    public int getFontColor() {
        return this.fontColor;
    }

    public String getGender() {
        return SOMAUserProfile.getSomaGender();
    }

    public AdDownloader.MediaType getMediaType() {
        return this.downloader.getMediaType();
    }

    @Deprecated
    public String getPubID() {
        return Integer.toString(getPublisherId());
    }

    public int getSOMABackgroundColor() {
        return this.backgroundColor;
    }

    public int getTimerFQ() {
        return this.timerFQ;
    }

    public boolean isAutoRefresh() {
        return this.autoRefresh;
    }

    @Deprecated
    public boolean isDownloadError() {
        return this.lastErrorCode == ErrorCode.NO_ERROR;
    }

    public boolean isHideWhenError() {
        return this.hideWhenError;
    }

    @Deprecated
    public void nextAd(int timer) {
        synchronized (this) {
            if (this.autoRefresh) {
                setTimerFQ(timer);
                manageRequestIntervalTimer(true);
            } else {
                manageRequestIntervalTimer(false);
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 66 || keyCode == 23) {
            setisPressed(true);
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onTouchEvent(MotionEvent event) {
        setisPressed(true);
        return super.onTouchEvent(event);
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        this.isWindowFocus = hasWindowFocus;
        if (this.autoRefresh) {
            manageRequestIntervalTimer(hasWindowFocus);
        }
    }

    @Deprecated
    public void setAdID(String adID) {
        setAdSpaceId(Integer.parseInt(adID));
    }

    public void setAge(int mAge) {
        if (SOMAUserProfile.getSomaAge() <= 0) {
            SOMAUserProfile.setSomaAge(mAge);
        }
    }

    public void setAnimationType(AnimationType type) {
        this.animationType = type;
    }

    @Deprecated
    public void setAnimationType(int typeID) {
        switch (typeID) {
            case 1:
                this.animationType = AnimationType.MOVE_ANIMATION;
            case 2:
                this.animationType = AnimationType.FADE_IN_ANIMATION;
            case 3:
                this.animationType = AnimationType.ROTATE_ANIMATION;
                break;
        }
        this.animationType = AnimationType.RANDOM_ANIMATION;
    }

    public void setAutoRefresh(boolean autoRefresh2) {
        if (this.autoRefresh != autoRefresh2) {
            this.autoRefresh = autoRefresh2;
            manageRequestIntervalTimer(autoRefresh2);
        }
    }

    public void setFontColor(int fontColor2) {
        this.fontColor = fontColor2;
    }

    public void setGender(String mGender) {
        if (SOMAUserProfile.getSomaGender() == null) {
            SOMAUserProfile.setSomaGender(mGender);
        }
    }

    public void setHideWhenError(boolean hideWhenError2) {
        this.hideWhenError = hideWhenError2;
    }

    @Deprecated
    public void setListener(AdListener listener) {
        addAdListener(listener);
    }

    @Deprecated
    public void setMediaType(String mediaType) {
        if (this.downloader == null) {
            return;
        }
        if (mediaType.equalsIgnoreCase("TXT")) {
            this.downloader.setMediaType(AdDownloader.MediaType.TXT);
        } else if (mediaType.equalsIgnoreCase("ALL")) {
            this.downloader.setMediaType(AdDownloader.MediaType.ALL);
        } else if (mediaType.equalsIgnoreCase("IMG")) {
            this.downloader.setMediaType(AdDownloader.MediaType.IMG);
        } else if (mediaType.equalsIgnoreCase("MEDRECT")) {
            this.downloader.setMediaType(AdDownloader.MediaType.MEDRECT);
            this.HEIGHT = 250;
        }
    }

    @Deprecated
    public void setPubID(String pubID) {
        setPublisherId(Integer.parseInt(pubID));
    }

    public String getKeywordList() {
        return this.downloader.getKeywordList();
    }

    public String getSearchQuery() {
        return this.downloader.getSearchQuery();
    }

    public void setKeywordList(String keywordList) {
        this.downloader.setKeywordList(keywordList);
    }

    public void setSearchQuery(String searchQuery) {
        this.downloader.setSearchQuery(searchQuery);
    }

    public void setSOMABackgroundColor(int color) {
        this.backgroundColor = color;
        setBackgroundColor(color);
        if (this.somaTextBannerView != null) {
            this.somaTextBannerView.setbgColor(this.backgroundColor);
        }
    }

    public void setTimerFQ(int timer) {
        if (timer >= 60) {
            this.timerFQ = timer;
        } else {
            this.timerFQ = 60;
        }
    }

    private void applyFadeAnimation(boolean addListener) {
        AlphaAnimation fadeInAnim = new AlphaAnimation(0.0f, 1.0f);
        fadeInAnim.setDuration(300);
        fadeInAnim.setFillAfter(true);
        if (this.lastView != null) {
            fadeInAnim.setStartOffset(100);
            AlphaAnimation fadeOutAnim = new AlphaAnimation(1.0f, 0.0f);
            fadeOutAnim.setDuration(300);
            fadeOutAnim.startNow();
            fadeOutAnim.setFillAfter(true);
            if (addListener) {
                fadeOutAnim.setAnimationListener(this.animListener);
            }
            this.lastView.startAnimation(fadeOutAnim);
        } else if (addListener) {
            fadeInAnim.setAnimationListener(this.animListener);
        }
        this.currentView.startAnimation(fadeInAnim);
    }

    private void applyFadeIn(SOMABanner newAd) {
        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(300);
        animation.startNow();
        animation.setFillAfter(true);
        animation.setInterpolator(new AccelerateInterpolator());
        startAnimation(animation);
    }

    private void applyFadeOut(SOMABanner newAd) {
        AlphaAnimation animation = new AlphaAnimation(1.0f, 0.3f);
        animation.setDuration(1000);
        animation.startNow();
        animation.setFillAfter(true);
        animation.setInterpolator(new AccelerateInterpolator());
        startAnimation(animation);
    }

    private void applyMoveAnimation() {
        TranslateAnimation inAnimation = new TranslateAnimation((float) getWidth(), 0.0f, 0.0f, 0.0f);
        inAnimation.setDuration(300);
        inAnimation.setFillAfter(true);
        inAnimation.setAnimationListener(this.animListener);
        inAnimation.setInterpolator(new DecelerateInterpolator());
        if (this.lastView != null) {
            inAnimation.setStartOffset(100);
            TranslateAnimation outAnimation = new TranslateAnimation(0.0f, (float) (-getWidth()), 0.0f, 0.0f);
            outAnimation.setDuration(300);
            outAnimation.startNow();
            outAnimation.setFillAfter(true);
            outAnimation.setInterpolator(new AccelerateInterpolator());
            this.lastView.startAnimation(outAnimation);
        }
        this.currentView.startAnimation(inAnimation);
    }

    private void applyRandom() {
        switch (new Random().nextInt(3)) {
            case 0:
                applyRotateAnimation();
                return;
            case 1:
                applyMoveAnimation();
                return;
            case 2:
                applyFadeAnimation(true);
                return;
            default:
                return;
        }
    }

    private void applyRotateAnimation() {
        RotateAnimation rotation = new RotateAnimation(0.0f, 360.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f);
        rotation.setDuration(700);
        rotation.setFillAfter(true);
        rotation.setAnimationListener(this.animListener);
        startAnimation(rotation);
        applyFadeAnimation(false);
    }

    private void cleanUp() {
        if (this.webview != null) {
            this.webview.destroy();
            removeView(this.webview);
            this.webview = null;
        }
        if (this.somaTextBannerView != null) {
            removeView(this.somaTextBannerView);
            this.somaTextBannerView = null;
        }
    }

    private void display(SOMAReceivedBanner banner) {
        this.lastView = this.currentView;
        AdDownloader.MediaType mType = banner.getAdType();
        if (mType == AdDownloader.MediaType.TXT) {
            String mText = banner.getAdText();
            if (this.somaTextBannerView == null) {
                this.somaTextBannerView = new SOMATextBanner(getContext());
                this.somaTextBannerView.setbgColor(this.backgroundColor);
                RelativeLayout.LayoutParams adTextViewParams = new RelativeLayout.LayoutParams(-1, -1);
                adTextViewParams.addRule(9);
                adTextViewParams.addRule(10);
                this.somaTextBannerView.setLayoutParams(adTextViewParams);
                this.somaTextBannerView.setText(mText);
                this.somaTextBannerView.setTextColor(this.fontColor);
                addView(this.somaTextBannerView);
            } else {
                this.somaTextBannerView.setText(mText);
                this.somaTextBannerView.setTextColor(this.fontColor);
                this.somaTextBannerView.invalidate();
            }
            this.currentView = this.somaTextBannerView;
            this.somaTextBannerView = null;
            startAnimation();
        } else if (mType == AdDownloader.MediaType.IMG || mType == AdDownloader.MediaType.RICHMEDIA) {
            float scale = getContext().getResources().getDisplayMetrics().density;
            StringBuffer htmlstring = new StringBuffer("<html><head><style>body {margin:0;text-align:center;");
            htmlstring.append("width:").append((int) (((float) getWidth()) / scale)).append("px;");
            htmlstring.append("height:").append((int) (((float) getHeight()) / scale)).append("px;");
            htmlstring.append("background-color:#").append(htmlColorHexStr(this.backgroundColor)).append(";");
            htmlstring.append("color:#").append(htmlColorHexStr(this.fontColor)).append(";");
            htmlstring.append("align:center; } </style></head>\n");
            if (mType == AdDownloader.MediaType.IMG) {
                htmlstring.append("<body><img src='").append(banner.link).append("'/>");
            } else {
                htmlstring.append("<body onclick=\"ormma.expand();\">\n");
                htmlstring.append("<script src=\"http://soma.smaato.net/oapi/js/ormma_bridge.js\" type=\"text/javascript\"></script>\n");
                htmlstring.append("<script src=\"http://soma.smaato.net/oapi/js/ormma.js\" type=\"text/javascript\"></script>\n");
                htmlstring.append("<div id=\"ad-container\"></div>");
                htmlstring.append(banner.adText);
            }
            htmlstring.append("\n</body></html>");
            String htmlstr = htmlstring.toString();
            if (this.webview == null) {
                this.webview = new WebView(getContext());
                this.webview.clearCache(true);
                this.webview.setFocusable(true);
                this.webview.getSettings().setJavaScriptEnabled(true);
                this.webview.setBackgroundColor(this.backgroundColor);
                this.webview.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
                this.webview.setVerticalScrollBarEnabled(false);
                this.webview.setHorizontalScrollBarEnabled(false);
                this.webview.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case 0:
                            case 1:
                                if (v.hasFocus()) {
                                    return false;
                                }
                                v.requestFocus();
                                return false;
                            default:
                                return false;
                        }
                    }
                });
            }
            if (mType == AdDownloader.MediaType.RICHMEDIA) {
                this.webview.setWebViewClient(new WebViewClient() {
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        Log.d(SOMABanner.TAG, "Requested url: " + url);
                        SOMABanner.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                        return true;
                    }

                    public void onPageFinished(WebView view, String url) {
                        SOMABanner.this.startAnimation();
                        view.loadUrl("javascript:window.ormmaview.fireChangeEvent({ state: 'default', size: { width: " + ((int) (((float) SOMABanner.this.getWidth()) / SOMABanner.this.mDensity)) + ", " + "height: " + ((int) (((float) SOMABanner.this.getHeight()) / SOMABanner.this.mDensity)) + "}" + "," + " maxSize: " + SOMABanner.this.getScreenSize() + "," + " screenSize: " + SOMABanner.this.getScreenSize() + "," + " defaultPosition: { x:" + ((int) (((float) view.getLeft()) / SOMABanner.this.mDensity)) + ", y: " + ((int) (((float) view.getTop()) / SOMABanner.this.mDensity)) + ", width: " + ((int) (((float) view.getWidth()) / SOMABanner.this.mDensity)) + ", height: " + ((int) (((float) view.getHeight()) / SOMABanner.this.mDensity)) + " }" + "," + SOMABanner.this.getSupports() + " });");
                    }
                });
            } else {
                this.webview.setWebViewClient(new WebViewClient() {
                    public void onPageFinished(WebView view, String url) {
                        SOMABanner.this.startAnimation();
                    }
                });
            }
            this.webview.setWebChromeClient(new WebChromeClient() {
                public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                    Log.i(SOMABanner.TAG, "JSAlert " + message);
                    return super.onJsAlert(view, url, message, result);
                }
            });
            this.ormmaBridge = new ORMMA_Bridge(this.handler, this.context);
            this.webview.addJavascriptInterface(this.ormmaBridge, "smaato_bridge");
            this.animating = true;
            addView(this.webview);
            if (this.lastView != null) {
                this.lastView.bringToFront();
            }
            this.currentView = this.webview;
            this.webview.loadDataWithBaseURL(null, htmlstr, "text/html", Common.KEnc, null);
            this.webview = null;
        } else if (mType == AdDownloader.MediaType.VIDEO) {
            this.videoview.downloadFinished(banner);
        }
    }

    /* access modifiers changed from: private */
    public String getSupports() {
        return "supports: [ 'level-1', 'screen']";
    }

    public String getScreenSize() {
        DisplayMetrics metrics = new DisplayMetrics();
        this.mWindowManager.getDefaultDisplay().getMetrics(metrics);
        return "{ width: " + ((int) (((float) metrics.widthPixels) / metrics.density)) + ", " + "height: " + ((int) (((float) metrics.heightPixels) / metrics.density)) + "}";
    }

    private String htmlColorHexStr(int colorInt) {
        return Integer.toHexString(-16777216 | colorInt).substring(2);
    }

    @Deprecated
    public void fetchDrawableOnThread() {
        asyncLoadNewBanner();
    }

    private void manageRequestIntervalTimer(boolean start) {
        synchronized (this) {
            if (start) {
                this.handler.postDelayed(this.updateTime, (long) (this.timerFQ * AdManager.AD_FILL_PARENT));
            } else {
                this.handler.removeCallbacks(this.updateTime);
            }
        }
    }

    /* access modifiers changed from: private */
    public void expandDialog(int width, int height) {
        RelativeLayout.LayoutParams dialogParams;
        this.richMediaDialog = new Dialog(this.context, 16973826);
        this.richMediaDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialog) {
                SOMABanner.this.currentView = null;
                SOMABanner.this.richMediaDialog = null;
                SOMABanner.this.isWindowFocus = true;
                SOMABanner.this.asyncLoadNewBanner();
            }
        });
        this.richMediaDialog.setOwnerActivity((Activity) this.context);
        removeView(this.currentView);
        FrameLayout dialogLayout = new FrameLayout(this.context);
        RelativeLayout.LayoutParams dialogParams2 = new RelativeLayout.LayoutParams(-1, -1);
        dialogParams2.addRule(13);
        dialogLayout.setLayoutParams(dialogParams2);
        if (width <= 0 || height <= 0) {
            dialogParams = new RelativeLayout.LayoutParams(-1, -1);
        } else {
            dialogParams = new RelativeLayout.LayoutParams((int) (((float) width) * this.mDensity), (int) (((float) height) * this.mDensity));
        }
        dialogParams.addRule(13);
        this.currentView.setLayoutParams(dialogParams);
        dialogLayout.addView(this.currentView);
        this.richMediaDialog.setContentView(dialogLayout);
        this.richMediaDialog.show();
        ((WebView) this.currentView).loadUrl("javascript:window.ormmaview.fireChangeEvent({state: 'expanded', size:{ width:" + this.currentView.getWidth() + ", height:" + this.currentView.getHeight() + "}});");
    }

    private void setisPressed(boolean pressed) {
        if (pressed && this.lastBanner != null) {
            if (this.EnableDebugMode) {
                StringBuffer debugData = new StringBuffer("Debug Mode: ");
                if (this.lastBanner.imageURL != null && this.lastBanner.imageURL.toString().length() > 0) {
                    debugData.append("\nImage URL redirect = ").append(redirectImageURLTrace(this.lastBanner.imageURL));
                }
                String t = this.lastBanner.link;
                if (t != null && t.length() > 0) {
                    String sessionID = t.split("jsessionid=")[1].split("\\.")[0];
                    Log.v(TAG, sessionID);
                    debugData.append("\nSessionID = ").append(sessionID);
                    Toast.makeText(this.context, debugData.toString(), 1).show();
                }
            } else if (getMediaType() == AdDownloader.MediaType.RICHMEDIA) {
                this.richMediaDialog = new Dialog(this.context, 16973826);
                this.richMediaDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        SOMABanner.this.currentView = null;
                        SOMABanner.this.richMediaDialog = null;
                        SOMABanner.this.isWindowFocus = true;
                        SOMABanner.this.asyncLoadNewBanner();
                    }
                });
                this.richMediaDialog.setOwnerActivity((Activity) this.context);
                removeView(this.currentView);
                FrameLayout dialogLayout = new FrameLayout(this.context);
                RelativeLayout.LayoutParams dialogParams = new RelativeLayout.LayoutParams(-1, -1);
                dialogParams.addRule(13);
                dialogLayout.setLayoutParams(dialogParams);
                RelativeLayout.LayoutParams dialogParams2 = new RelativeLayout.LayoutParams(320, 250);
                dialogParams2.addRule(13);
                this.currentView.setLayoutParams(dialogParams2);
                dialogLayout.addView(this.currentView);
                this.richMediaDialog.setContentView(dialogLayout);
                this.richMediaDialog.show();
            } else {
                String temp = this.lastBanner.mTarget;
                if (temp != null && temp.length() > 0) {
                    try {
                        applyFadeOut(this);
                        this.lastBanner.openLandingPage(this.context);
                        asyncLoadNewBanner();
                        applyFadeIn(this);
                    } catch (Exception e) {
                        Log.e(TAG, "Could not open browser on ad click to " + this.lastBanner.link, e);
                    }
                }
            }
        }
    }

    private String redirectImageURLTrace(URL redURL) {
        try {
            HttpURLConnection.setFollowRedirects(true);
            HttpURLConnection redirectConnection = (HttpURLConnection) redURL.openConnection();
            redirectConnection.setConnectTimeout(5000);
            redirectConnection.setReadTimeout(5000);
            redirectConnection.setRequestProperty("User-Agent", this.userAgent);
            redirectConnection.connect();
            redirectConnection.getResponseCode();
            return redirectConnection.getURL().toString();
        } catch (Exception e) {
            Log.e(TAG, "Unexpted error inside the debug mode image url redirect", e);
            return null;
        }
    }

    private void updateBanner() {
        Log.v(TAG, "Finish the thread.");
        try {
            if (this.lastErrorCode != ErrorCode.NO_ERROR) {
                cleanUp();
                if (this.hideWhenError) {
                    setVisibility(8);
                } else {
                    setVisibility(0);
                }
            } else {
                setVisibility(0);
                display(this.lastBanner);
            }
        } catch (Exception e) {
            Log.w(TAG, "can't refresh banner", e);
        }
    }

    /* access modifiers changed from: private */
    public void startAnimation() {
        Log.d(TAG, "Start animation " + this.animationType.toString());
        this.animating = true;
        switch ($SWITCH_TABLE$com$smaato$SOMA$SOMABanner$AnimationType()[this.animationType.ordinal()]) {
            case 2:
                applyFadeAnimation(true);
                return;
            case 3:
                applyMoveAnimation();
                return;
            case 4:
                applyRandom();
                return;
            case 5:
                applyRotateAnimation();
                return;
            default:
                this.currentView.bringToFront();
                this.animListener.onAnimationEnd(null);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void initBanner(Context context2, AttributeSet attrs, int defStyle) {
        DisplayMetrics metrics = new DisplayMetrics();
        this.mWindowManager = (WindowManager) context2.getSystemService("window");
        this.mWindowManager.getDefaultDisplay().getMetrics(metrics);
        this.mDensity = metrics.density;
        this.baseLayout = this;
        setDescendantFocusability(262144);
        setFocusable(true);
        this.webview = null;
        this.somaTextBannerView = null;
        if (attrs != null) {
            String namespace = "http://schemas.android.com/apk/res/" + context2.getPackageName();
            int fc = attrs.getAttributeUnsignedIntValue(namespace, "fontColor", this.fontColor);
            int bc = attrs.getAttributeUnsignedIntValue(namespace, "backgroundColor", this.backgroundColor);
            setFontColor(fc);
            setSOMABackgroundColor(bc);
        }
        if (!isInEditMode()) {
            this.context = context2;
            WebView webview2 = new WebView(context2);
            this.userAgent = webview2.getSettings().getUserAgentString();
            webview2.destroy();
            this.downloader = new SOMADownloader(context2, this.userAgent);
            this.downloader.addAdListener(this);
            this.downloader.setMediaType(getMediaType());
        }
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == SOMABanner.MESSAGE_EXPAND) {
                    SOMABanner.this.expandDialog(msg.arg1, msg.arg2);
                } else if (msg.what == SOMABanner.MESSAGE_CLOSE) {
                    SOMABanner.this.richMediaDialog.cancel();
                }
                super.handleMessage(msg);
            }
        };
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (getMeasuredHeight() < this.HEIGHT) {
            setMeasuredDimension(widthMeasureSpec, this.HEIGHT);
        }
    }

    public int getAdSpaceId() {
        return this.downloader.getAdSpaceId();
    }

    public void setAdSpaceId(int adSpaceId) {
        this.downloader.setAdSpaceId(adSpaceId);
    }

    public int getPublisherId() {
        return this.downloader.getPublisherId();
    }

    public void setPublisherId(int publisherId) {
        this.downloader.setPublisherId(publisherId);
    }

    public void setMediaType(AdDownloader.MediaType mediaType) {
        this.downloader.setMediaType(mediaType);
    }

    public void removeAdListener(AdListener adListener) {
        this.listenerList.remove(adListener);
    }

    public void addAdListener(AdListener adListener) {
        this.listenerList.add(adListener);
    }

    public ErrorCode getErrorCode() {
        return this.lastErrorCode;
    }

    public String getErrorMessage() {
        return this.lastErrorMessage;
    }

    public void onFailedToReceiveAd(AdDownloader sender, ErrorCode errorCode) {
        this.lastErrorCode = errorCode;
        this.lastErrorMessage = sender.getErrorMessage();
        cleanUp();
        if (this.hideWhenError) {
            setVisibility(8);
            if (this.lastView != null) {
                if (this.lastView instanceof WebView) {
                    ((WebView) this.lastView).destroy();
                }
                removeView(this.lastView);
                this.lastView = null;
            }
            if (this.currentView != null) {
                if (this.currentView instanceof WebView) {
                    ((WebView) this.currentView).destroy();
                }
                removeView(this.currentView);
                this.currentView = null;
            }
        } else {
            setVisibility(0);
        }
        for (AdListener listener : this.listenerList) {
            listener.onFailedToReceiveAd(this, errorCode);
        }
    }

    public void onReceiveAd(AdDownloader sender, SOMAReceivedBanner newAd) {
        this.lastBanner = newAd;
        updateBanner();
        for (AdListener listener : this.listenerList) {
            listener.onReceiveAd(this, newAd);
        }
    }

    public void setLocation(double latitude, double longitude) {
        this.downloader.setLocation(latitude, longitude);
    }

    public void setLocationUpdateEnabled(boolean locationUpdates) {
        this.downloader.setLocationUpdateEnabled(locationUpdates);
    }

    public boolean isLocationUpdateEnabled() {
        return this.downloader.isLocationUpdateEnabled();
    }
}
