package com.smaato.SOMA;

public interface AdListener {
    void onFailedToReceiveAd(AdDownloader adDownloader, ErrorCode errorCode);

    void onReceiveAd(AdDownloader adDownloader, SOMAReceivedBanner sOMAReceivedBanner);
}
