package com.smaato.SOMA;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int fontColor = 2130771969;
    }

    public static final class string {
        public static final int Keyword = 2130837505;
        public static final int Search = 2130837506;
        public static final int app_name = 2130837507;
        public static final int getad = 2130837504;
    }

    public static final class styleable {
        public static final int[] com_smaato_SOMA_SOMABanner = {R.attr.backgroundColor, R.attr.fontColor};
        public static final int com_smaato_SOMA_SOMABanner_backgroundColor = 0;
        public static final int com_smaato_SOMA_SOMABanner_fontColor = 1;
    }
}
