package com.vpon.adon.android.utils;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.vpon.adon.android.entity.ScreenSize;

public class PhoneStateUtils {
    public static String getIMEI(Context context) {
        String imei;
        try {
            imei = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
            imei = "";
        }
        if (StringUtils.isBlank(imei)) {
            return getWifiAddress(context);
        }
        return imei;
    }

    public static String getDeviceName(Context context) {
        try {
            if (StringUtils.isBlank("")) {
                return Build.MODEL;
            }
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getAndroidId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    public static String getWifiAddress(Context context) {
        try {
            return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getSdkOsVersion() {
        try {
            return Build.VERSION.SDK;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static ScreenSize getScreenSize(Context context) {
        ScreenSize screenSize = new ScreenSize();
        try {
            DisplayMetrics dm = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(dm);
            screenSize.setScreenWidth(dm.widthPixels);
            screenSize.setScreenHeight(dm.heightPixels);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return screenSize;
    }
}
