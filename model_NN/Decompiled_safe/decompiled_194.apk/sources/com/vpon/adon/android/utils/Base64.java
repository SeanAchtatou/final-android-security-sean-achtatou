package com.vpon.adon.android.utils;

import java.util.Arrays;

public final class Base64 {
    private static final char[] CA = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();
    private static final int[] IA = new int[256];

    static {
        Arrays.fill(IA, -1);
        int iS = CA.length;
        for (int i = 0; i < iS; i++) {
            IA[CA[i]] = i;
        }
        IA[61] = 0;
    }

    /* JADX INFO: Multiple debug info for r12v1 int: [D('lineSep' boolean), D('left' int)] */
    /* JADX INFO: Multiple debug info for r0v21 int: [D('cc' int), D('d' int)] */
    public static final char[] encodeToChar(byte[] sArr, boolean lineSep) {
        int sLen = sArr != null ? sArr.length : 0;
        if (sLen == 0) {
            return new char[0];
        }
        int eLen = (sLen / 3) * 3;
        int cCnt = (((sLen - 1) / 3) + 1) << 2;
        int dLen = cCnt + (lineSep ? ((cCnt - 1) / 76) << 1 : 0);
        char[] dArr = new char[dLen];
        int cc = 0;
        int s = 0;
        int d = 0;
        while (s < eLen) {
            int s2 = s + 1;
            int i = (sArr[s] & 255) << 16;
            int s3 = s2 + 1;
            int i2 = ((sArr[s2] & 255) << 8) | i;
            int s4 = s3 + 1;
            int i3 = (sArr[s3] & 255) | i2;
            int d2 = d + 1;
            dArr[d] = CA[(i3 >>> 18) & 63];
            int d3 = d2 + 1;
            dArr[d2] = CA[(i3 >>> 12) & 63];
            int d4 = d3 + 1;
            dArr[d3] = CA[(i3 >>> 6) & 63];
            d = d4 + 1;
            dArr[d4] = CA[i3 & 63];
            if (!lineSep || (cc = cc + 1) != 19 || d >= dLen - 2) {
                s = s4;
            } else {
                int d5 = d + 1;
                dArr[d] = 13;
                int d6 = d5 + 1;
                dArr[d5] = 10;
                cc = 0;
                d = d6;
                s = s4;
            }
        }
        int left = sLen - eLen;
        if (left > 0) {
            int i4 = (left == 2 ? (sArr[sLen - 1] & 255) << 2 : 0) | ((sArr[eLen] & 255) << 10);
            dArr[dLen - 4] = CA[i4 >> 12];
            dArr[dLen - 3] = CA[(i4 >>> 6) & 63];
            dArr[dLen - 2] = left == 2 ? CA[i4 & 63] : '=';
            dArr[dLen - 1] = '=';
        }
        return dArr;
    }

    public static final byte[] decode(char[] sArr) {
        int sLen;
        int s;
        if (sArr != null) {
            sLen = sArr.length;
        } else {
            sLen = 0;
        }
        if (sLen == 0) {
            return new byte[0];
        }
        int sepCnt = 0;
        for (int i = 0; i < sLen; i++) {
            if (IA[sArr[i]] < 0) {
                sepCnt++;
            }
        }
        if ((sLen - sepCnt) % 4 != 0) {
            return null;
        }
        int pad = 0;
        int i2 = sLen;
        while (i2 > 1) {
            i2--;
            if (IA[sArr[i2]] > 0) {
                break;
            } else if (sArr[i2] == '=') {
                pad++;
            }
        }
        int len = (((sLen - sepCnt) * 6) >> 3) - pad;
        byte[] dArr = new byte[len];
        int s2 = 0;
        int d = 0;
        while (d < len) {
            int i3 = 0;
            int j = 0;
            while (true) {
                s = s2;
                if (j >= 4) {
                    break;
                }
                s2 = s + 1;
                int c = IA[sArr[s]];
                if (c >= 0) {
                    i3 |= c << (18 - (j * 6));
                } else {
                    j--;
                }
                j++;
            }
            int d2 = d + 1;
            dArr[d] = (byte) (i3 >> 16);
            if (d2 < len) {
                d = d2 + 1;
                dArr[d2] = (byte) (i3 >> 8);
                if (d < len) {
                    d2 = d + 1;
                    dArr[d] = (byte) i3;
                } else {
                    s2 = s;
                }
            }
            d = d2;
            s2 = s;
        }
        return dArr;
    }

    /* JADX INFO: Multiple debug info for r12v2 byte[]: [D('d' int), D('sArr' char[])] */
    /* JADX INFO: Multiple debug info for r5v2 int: [D('j' int), D('eLen' int)] */
    /* JADX INFO: Multiple debug info for r12v5 int: [D('sArr' char[]), D('d' int)] */
    public static final byte[] decodeFast(char[] sArr) {
        int eIx;
        int sepCnt;
        int i;
        int sLen = sArr.length;
        if (sLen == 0) {
            return new byte[0];
        }
        int eIx2 = sLen - 1;
        int sIx = 0;
        while (true) {
            if (sIx >= eIx2) {
                eIx = eIx2;
                break;
            } else if (IA[sArr[sIx]] >= 0) {
                eIx = eIx2;
                break;
            } else {
                sIx++;
            }
        }
        while (eIx > 0 && IA[sArr[eIx]] < 0) {
            eIx--;
        }
        int pad = sArr[eIx] == '=' ? sArr[eIx - 1] == '=' ? 2 : 1 : 0;
        int cCnt = (eIx - sIx) + 1;
        if (sLen > 76) {
            sepCnt = (sArr[76] == 13 ? cCnt / 78 : 0) << 1;
        } else {
            sepCnt = 0;
        }
        int len = (((cCnt - sepCnt) * 6) >> 3) - pad;
        byte[] dArr = new byte[len];
        int cc = 0;
        int eLen = (len / 3) * 3;
        int sIx2 = sIx;
        int sIx3 = 0;
        while (sIx3 < eLen) {
            int sIx4 = sIx2 + 1;
            int i2 = IA[sArr[sIx2]] << 18;
            int sIx5 = sIx4 + 1;
            int i3 = i2 | (IA[sArr[sIx4]] << 12);
            int sIx6 = sIx5 + 1;
            int i4 = (IA[sArr[sIx5]] << 6) | i3;
            int sIx7 = sIx6 + 1;
            int i5 = i4 | IA[sArr[sIx6]];
            int d = sIx3 + 1;
            dArr[sIx3] = (byte) (i5 >> 16);
            int d2 = d + 1;
            dArr[d] = (byte) (i5 >> 8);
            int d3 = d2 + 1;
            dArr[d2] = (byte) i5;
            if (sepCnt <= 0 || (cc = cc + 1) != 19) {
                sIx3 = d3;
                sIx2 = sIx7;
            } else {
                cc = 0;
                sIx2 = sIx7 + 2;
                sIx3 = d3;
            }
        }
        if (sIx3 < len) {
            int i6 = 0;
            int i7 = 0;
            while (true) {
                int eLen2 = i7;
                int sIx8 = sIx2;
                i = i6;
                if (sIx8 > eIx - pad) {
                    break;
                }
                sIx2 = sIx8 + 1;
                i6 = (IA[sArr[sIx8]] << (18 - (eLen2 * 6))) | i;
                i7 = eLen2 + 1;
            }
            int r = 16;
            for (int r2 = sIx3; r2 < len; r2++) {
                dArr[r2] = (byte) (i >> r);
                r -= 8;
            }
        }
        return dArr;
    }

    /* JADX INFO: Multiple debug info for r12v1 int: [D('lineSep' boolean), D('left' int)] */
    /* JADX INFO: Multiple debug info for r0v21 int: [D('cc' int), D('d' int)] */
    public static final byte[] encodeToByte(byte[] sArr, boolean lineSep) {
        int sLen = sArr != null ? sArr.length : 0;
        if (sLen == 0) {
            return new byte[0];
        }
        int eLen = (sLen / 3) * 3;
        int cCnt = (((sLen - 1) / 3) + 1) << 2;
        int dLen = cCnt + (lineSep ? ((cCnt - 1) / 76) << 1 : 0);
        byte[] dArr = new byte[dLen];
        int cc = 0;
        int s = 0;
        int d = 0;
        while (s < eLen) {
            int s2 = s + 1;
            int i = (sArr[s] & 255) << 16;
            int s3 = s2 + 1;
            int i2 = ((sArr[s2] & 255) << 8) | i;
            int s4 = s3 + 1;
            int i3 = (sArr[s3] & 255) | i2;
            int d2 = d + 1;
            dArr[d] = (byte) CA[(i3 >>> 18) & 63];
            int d3 = d2 + 1;
            dArr[d2] = (byte) CA[(i3 >>> 12) & 63];
            int d4 = d3 + 1;
            dArr[d3] = (byte) CA[(i3 >>> 6) & 63];
            d = d4 + 1;
            dArr[d4] = (byte) CA[i3 & 63];
            if (!lineSep || (cc = cc + 1) != 19 || d >= dLen - 2) {
                s = s4;
            } else {
                int d5 = d + 1;
                dArr[d] = 13;
                int d6 = d5 + 1;
                dArr[d5] = 10;
                cc = 0;
                d = d6;
                s = s4;
            }
        }
        int left = sLen - eLen;
        if (left > 0) {
            int i4 = (left == 2 ? (sArr[sLen - 1] & 255) << 2 : 0) | ((sArr[eLen] & 255) << 10);
            dArr[dLen - 4] = (byte) CA[i4 >> 12];
            dArr[dLen - 3] = (byte) CA[(i4 >>> 6) & 63];
            dArr[dLen - 2] = left == 2 ? (byte) CA[i4 & 63] : 61;
            dArr[dLen - 1] = 61;
        }
        return dArr;
    }

    public static final byte[] decode(byte[] sArr) {
        int s;
        int sepCnt = 0;
        for (byte b : sArr) {
            if (IA[b & 255] < 0) {
                sepCnt++;
            }
        }
        if ((sLen - sepCnt) % 4 != 0) {
            return null;
        }
        int pad = 0;
        int i = sLen;
        while (i > 1) {
            i--;
            if (IA[sArr[i] & 255] > 0) {
                break;
            } else if (sArr[i] == 61) {
                pad++;
            }
        }
        int len = (((sLen - sepCnt) * 6) >> 3) - pad;
        byte[] dArr = new byte[len];
        int s2 = 0;
        int d = 0;
        while (d < len) {
            int i2 = 0;
            int j = 0;
            while (true) {
                s = s2;
                if (j >= 4) {
                    break;
                }
                s2 = s + 1;
                int c = IA[sArr[s] & 255];
                if (c >= 0) {
                    i2 |= c << (18 - (j * 6));
                } else {
                    j--;
                }
                j++;
            }
            int d2 = d + 1;
            dArr[d] = (byte) (i2 >> 16);
            if (d2 < len) {
                d = d2 + 1;
                dArr[d2] = (byte) (i2 >> 8);
                if (d < len) {
                    d2 = d + 1;
                    dArr[d] = (byte) i2;
                } else {
                    s2 = s;
                }
            }
            d = d2;
            s2 = s;
        }
        return dArr;
    }

    /* JADX INFO: Multiple debug info for r12v2 byte[]: [D('sArr' byte[]), D('d' int)] */
    /* JADX INFO: Multiple debug info for r5v2 int: [D('j' int), D('eLen' int)] */
    /* JADX INFO: Multiple debug info for r12v5 int: [D('sArr' byte[]), D('d' int)] */
    public static final byte[] decodeFast(byte[] sArr) {
        int eIx;
        int sepCnt;
        int i;
        int sLen = sArr.length;
        if (sLen == 0) {
            return new byte[0];
        }
        int eIx2 = sLen - 1;
        int sIx = 0;
        while (true) {
            if (sIx >= eIx2) {
                eIx = eIx2;
                break;
            } else if (IA[sArr[sIx] & 255] >= 0) {
                eIx = eIx2;
                break;
            } else {
                sIx++;
            }
        }
        while (eIx > 0 && IA[sArr[eIx] & 255] < 0) {
            eIx--;
        }
        int pad = sArr[eIx] == 61 ? sArr[eIx - 1] == 61 ? 2 : 1 : 0;
        int cCnt = (eIx - sIx) + 1;
        if (sLen > 76) {
            sepCnt = (sArr[76] == 13 ? cCnt / 78 : 0) << 1;
        } else {
            sepCnt = 0;
        }
        int len = (((cCnt - sepCnt) * 6) >> 3) - pad;
        byte[] dArr = new byte[len];
        int cc = 0;
        int eLen = (len / 3) * 3;
        int sIx2 = sIx;
        int sIx3 = 0;
        while (sIx3 < eLen) {
            int sIx4 = sIx2 + 1;
            int i2 = IA[sArr[sIx2]] << 18;
            int sIx5 = sIx4 + 1;
            int i3 = i2 | (IA[sArr[sIx4]] << 12);
            int sIx6 = sIx5 + 1;
            int i4 = (IA[sArr[sIx5]] << 6) | i3;
            int sIx7 = sIx6 + 1;
            int i5 = i4 | IA[sArr[sIx6]];
            int d = sIx3 + 1;
            dArr[sIx3] = (byte) (i5 >> 16);
            int d2 = d + 1;
            dArr[d] = (byte) (i5 >> 8);
            int d3 = d2 + 1;
            dArr[d2] = (byte) i5;
            if (sepCnt <= 0 || (cc = cc + 1) != 19) {
                sIx3 = d3;
                sIx2 = sIx7;
            } else {
                cc = 0;
                sIx2 = sIx7 + 2;
                sIx3 = d3;
            }
        }
        if (sIx3 < len) {
            int i6 = 0;
            int i7 = 0;
            while (true) {
                int eLen2 = i7;
                int sIx8 = sIx2;
                i = i6;
                if (sIx8 > eIx - pad) {
                    break;
                }
                sIx2 = sIx8 + 1;
                i6 = (IA[sArr[sIx8]] << (18 - (eLen2 * 6))) | i;
                i7 = eLen2 + 1;
            }
            int r = 16;
            for (int r2 = sIx3; r2 < len; r2++) {
                dArr[r2] = (byte) (i >> r);
                r -= 8;
            }
        }
        return dArr;
    }

    public static final String encodeToString(byte[] sArr, boolean lineSep) {
        return new String(encodeToChar(sArr, lineSep));
    }

    public static final byte[] decode(String str) {
        int sLen;
        int s;
        if (str != null) {
            sLen = str.length();
        } else {
            sLen = 0;
        }
        if (sLen == 0) {
            return new byte[0];
        }
        int sepCnt = 0;
        for (int i = 0; i < sLen; i++) {
            if (IA[str.charAt(i)] < 0) {
                sepCnt++;
            }
        }
        if ((sLen - sepCnt) % 4 != 0) {
            return null;
        }
        int pad = 0;
        int i2 = sLen;
        while (i2 > 1) {
            i2--;
            if (IA[str.charAt(i2)] > 0) {
                break;
            } else if (str.charAt(i2) == '=') {
                pad++;
            }
        }
        int len = (((sLen - sepCnt) * 6) >> 3) - pad;
        byte[] dArr = new byte[len];
        int s2 = 0;
        int d = 0;
        while (d < len) {
            int i3 = 0;
            int j = 0;
            while (true) {
                s = s2;
                if (j >= 4) {
                    break;
                }
                s2 = s + 1;
                int c = IA[str.charAt(s)];
                if (c >= 0) {
                    i3 |= c << (18 - (j * 6));
                } else {
                    j--;
                }
                j++;
            }
            int d2 = d + 1;
            dArr[d] = (byte) (i3 >> 16);
            if (d2 < len) {
                d = d2 + 1;
                dArr[d2] = (byte) (i3 >> 8);
                if (d < len) {
                    d2 = d + 1;
                    dArr[d] = (byte) i3;
                } else {
                    s2 = s;
                }
            }
            d = d2;
            s2 = s;
        }
        return dArr;
    }

    /* JADX INFO: Multiple debug info for r12v2 byte[]: [D('d' int), D('s' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v2 int: [D('j' int), D('eLen' int)] */
    /* JADX INFO: Multiple debug info for r12v5 int: [D('d' int), D('s' java.lang.String)] */
    public static final byte[] decodeFast(String s) {
        int eIx;
        int sepCnt;
        int i;
        int sLen = s.length();
        if (sLen == 0) {
            return new byte[0];
        }
        int eIx2 = sLen - 1;
        int sIx = 0;
        while (true) {
            if (sIx >= eIx2) {
                eIx = eIx2;
                break;
            } else if (IA[s.charAt(sIx) & 255] >= 0) {
                eIx = eIx2;
                break;
            } else {
                sIx++;
            }
        }
        while (eIx > 0 && IA[s.charAt(eIx) & 255] < 0) {
            eIx--;
        }
        int pad = s.charAt(eIx) == '=' ? s.charAt(eIx - 1) == '=' ? 2 : 1 : 0;
        int cCnt = (eIx - sIx) + 1;
        if (sLen > 76) {
            sepCnt = (s.charAt(76) == 13 ? cCnt / 78 : 0) << 1;
        } else {
            sepCnt = 0;
        }
        int len = (((cCnt - sepCnt) * 6) >> 3) - pad;
        byte[] dArr = new byte[len];
        int cc = 0;
        int eLen = (len / 3) * 3;
        int sIx2 = sIx;
        int sIx3 = 0;
        while (sIx3 < eLen) {
            int sIx4 = sIx2 + 1;
            int i2 = IA[s.charAt(sIx2)] << 18;
            int sIx5 = sIx4 + 1;
            int i3 = i2 | (IA[s.charAt(sIx4)] << 12);
            int sIx6 = sIx5 + 1;
            int i4 = (IA[s.charAt(sIx5)] << 6) | i3;
            int sIx7 = sIx6 + 1;
            int i5 = i4 | IA[s.charAt(sIx6)];
            int d = sIx3 + 1;
            dArr[sIx3] = (byte) (i5 >> 16);
            int d2 = d + 1;
            dArr[d] = (byte) (i5 >> 8);
            int d3 = d2 + 1;
            dArr[d2] = (byte) i5;
            if (sepCnt <= 0 || (cc = cc + 1) != 19) {
                sIx3 = d3;
                sIx2 = sIx7;
            } else {
                cc = 0;
                sIx2 = sIx7 + 2;
                sIx3 = d3;
            }
        }
        if (sIx3 < len) {
            int i6 = 0;
            int i7 = 0;
            while (true) {
                int eLen2 = i7;
                int sIx8 = sIx2;
                i = i6;
                if (sIx8 > eIx - pad) {
                    break;
                }
                sIx2 = sIx8 + 1;
                i6 = (IA[s.charAt(sIx8)] << (18 - (eLen2 * 6))) | i;
                i7 = eLen2 + 1;
            }
            int r = 16;
            for (int r2 = sIx3; r2 < len; r2++) {
                dArr[r2] = (byte) (i >> r);
                r -= 8;
            }
        }
        return dArr;
    }
}
