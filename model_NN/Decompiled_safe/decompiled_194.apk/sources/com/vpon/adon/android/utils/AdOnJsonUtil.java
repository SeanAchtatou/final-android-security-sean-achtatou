package com.vpon.adon.android.utils;

import android.content.Context;
import android.util.Log;
import com.vpon.adon.android.AppConfig;
import com.vpon.adon.android.entity.RespClick;
import com.vpon.adon.android.entity.ScreenSize;
import com.winad.android.ads.JsonUtils;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdOnJsonUtil {
    public static JSONObject getWebviewAdReqJson(Context context, double lat, double lon, String licenseKey) throws JSONException {
        String imei = PhoneStateUtils.getIMEI(context);
        String deviceName = PhoneStateUtils.getDeviceName(context);
        String deviceOsVersion = PhoneStateUtils.getSdkOsVersion();
        ScreenSize screenSize = PhoneStateUtils.getScreenSize(context);
        String cellId = CellTowerUtil.instance(context).getCellId();
        String lac = CellTowerUtil.instance(context).getLac();
        String mnc = CellTowerUtil.instance(context).getMnc();
        String mcc = CellTowerUtil.instance(context).getMcc();
        JSONObject json = new JSONObject();
        json.put("method", "webviewAdReq");
        json.put("imei", imei);
        json.put("licensekey", licenseKey);
        json.put("screenHeight", String.valueOf(screenSize.getScreenHeight()));
        json.put("screenWidth", String.valueOf(screenSize.getScreenWidth()));
        json.put("sdkName", "Android");
        json.put("sdkVersion", AppConfig.SDKVERSION);
        json.put("deviceName", deviceName);
        json.put("deviceOsName", "Anodrid");
        json.put("deviceOsVersion", deviceOsVersion);
        Log.v("SDK", "Using real location gps address");
        json.put("lat", String.valueOf(lat));
        json.put("lon", String.valueOf(lon));
        Log.v("SDK", String.valueOf(lat));
        Log.v("SDK", String.valueOf(lon));
        json.put("cellId", cellId);
        json.put("lac", lac);
        json.put("mcc", mcc);
        json.put("mnc", mnc);
        json.put("ts", 0);
        return json;
    }

    public static JSONObject getErrorJson(Context context, double lat, double lon, String licenseKey, String errorMessage) throws JSONException {
        String currentTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(Calendar.getInstance().getTime());
        JSONObject json = getWebviewAdReqJson(context, lat, lon, licenseKey);
        json.put("method", "sdkError");
        json.put(JsonUtils.ERROR_ACK, errorMessage);
        json.put("errorAt", currentTime);
        return json;
    }

    public static JSONObject getClickJson(Context context, double lat, double lon, String licenseKey, List<RespClick> respClicks) throws JSONException {
        JSONObject json = getWebviewAdReqJson(context, lat, lon, licenseKey);
        json.put("method", "adClick");
        JSONArray jClicks = new JSONArray();
        for (int i = 0; i < respClicks.size(); i++) {
            JSONObject jClick = new JSONObject();
            jClick.put("adId", respClicks.get(i).getAdId());
            jClick.put("licensekey", respClicks.get(i).getLicenseKey());
            jClick.put("clickAt", respClicks.get(i).getTime());
            jClick.put("lat", String.valueOf(respClicks.get(i).getLat()));
            jClick.put("lon", String.valueOf(respClicks.get(i).getLon()));
            jClick.put("cellId", respClicks.get(i).getCellID());
            jClick.put("lac", respClicks.get(i).getLac());
            jClick.put("mcc", respClicks.get(i).getMcc());
            jClick.put("mnc", respClicks.get(i).getMnc());
            jClicks.put(jClick);
        }
        json.put("adClickItemList", jClicks);
        return json;
    }
}
