package com.casee.adsdk;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.casee.adsdk.i;
import java.util.ArrayList;

class u extends Handler {
    final /* synthetic */ i a;

    u(i iVar) {
        this.a = iVar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                if (this.a.C != null) {
                    if (this.a.C.isShowing()) {
                        this.a.C.dismiss();
                        if (this.a.C != null) {
                            this.a.C.cancel();
                        }
                    }
                    i.a unused = this.a.C = (i.a) null;
                }
                e.a = false;
                return;
            case 2:
                if (!e.a) {
                    Log.i("result", "---AdViewGroup.myWebViewCreated == false");
                    return;
                } else if (this.a.C != null && this.a.C.isShowing()) {
                    if (this.a.G == null || this.a.G.size() < 5) {
                        ArrayList unused2 = this.a.G = (ArrayList) null;
                        Log.i("result", "---else {list = null;");
                        this.a.d();
                        return;
                    }
                    this.a.e();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }
}
