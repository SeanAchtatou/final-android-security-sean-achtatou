package com.casee.adsdk;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.util.Log;
import cn.domob.android.ads.DomobAdManager;
import com.casee.adsdk.gifview.b;
import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONObject;

public class p {
    public String a = "架势推荐";
    public String b = "测试状态";
    private String c;
    private String d;
    private Drawable e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private boolean n = false;
    private int o;
    private Context p;
    private String q;
    private boolean r = false;
    private int s = -1;

    private p(Context context) {
        this.p = context;
    }

    public static p a(Context context, String str) {
        if (str == null || "".equals(str) || str.indexOf("{") == -1) {
            Log.e("CASEE-AD", "JSON = " + str);
            return null;
        }
        try {
            p pVar = new p(context);
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("ulogo");
            if (string != null && !"".equals(string)) {
                pVar.b(string);
            }
            String string2 = jSONObject.getString("adtext1");
            if (string2 != null && !"".equals(string2)) {
                pVar.a(string2);
            }
            String string3 = jSONObject.getString("adurl");
            if (string3 != null && !"".equals(string3)) {
                pVar.c(string3);
            }
            String string4 = jSONObject.getString(DomobAdManager.ACTION_URL);
            if (string4 != null && !"".equals(string4)) {
                pVar.d(string4);
            }
            String string5 = jSONObject.getString("adid");
            if (string5 != null && !"".equals(string5)) {
                pVar.e(string5);
            }
            String string6 = jSONObject.getString("asq");
            if (string6 != null && !"".equals(string6)) {
                pVar.h(string6);
            }
            String string7 = jSONObject.getString("ssq");
            if (string7 != null && !"".equals(string7)) {
                pVar.g(string7);
            }
            String optString = jSONObject.optString("type", "0");
            if (optString != null && !"".equals(optString)) {
                pVar.b(Integer.parseInt(optString));
            }
            String optString2 = jSONObject.optString("pName");
            if (optString2 != null && !"".equals(optString2)) {
                pVar.i(optString2);
            }
            String optString3 = jSONObject.optString("secondUrl");
            if (optString3 != null && !"".equals(optString3)) {
                pVar.j(optString3);
            }
            String optString4 = jSONObject.optString("it");
            if (optString4 != null && !"".equals(optString4)) {
                try {
                    pVar.a(Integer.parseInt(optString4));
                } catch (NumberFormatException e2) {
                    Log.e("CASEE-AD", e2.getMessage(), e2);
                }
            }
            pVar.f(CaseeAdView.c());
            return pVar;
        } catch (JSONException e3) {
            Log.e("CASEE-AD", e3.getMessage(), e3);
            return null;
        }
    }

    public int a() {
        return this.s;
    }

    public Bitmap a(float f2) {
        Bitmap bitmap;
        Exception exc;
        Bitmap bitmap2;
        Bitmap decodeStream;
        Log.i("CASEE-AD", "mScale=" + f2);
        if (f2 >= 1.5f) {
            try {
                decodeStream = BitmapFactory.decodeStream(p.class.getResourceAsStream("recommend240.png"));
            } catch (Exception e2) {
                Exception exc2 = e2;
                bitmap = null;
                exc = exc2;
                Log.e("CASEE-AD", exc.getMessage(), exc);
                return bitmap;
            }
            try {
                Log.i("CASEE-AD", "getCaseeCommendIcon recommend240.png");
                return decodeStream;
            } catch (Exception e3) {
                Exception exc3 = e3;
                bitmap = bitmap2;
                exc = exc3;
            }
        } else {
            Bitmap decodeStream2 = BitmapFactory.decodeStream(p.class.getResourceAsStream("recommend160.png"));
            Log.i("CASEE-AD", "getCaseeCommendIcon recommend160.png");
            return decodeStream2;
        }
    }

    public b a(boolean z) {
        if (this.d == null) {
            return null;
        }
        b bVar = new b(this.p);
        InputStream b2 = f.b(this.p, this.d);
        if (b2 == null) {
            return null;
        }
        bVar.a(b2);
        return bVar;
    }

    public void a(int i2) {
        this.s = i2;
    }

    public void a(String str) {
        this.c = str;
    }

    public int b() {
        return this.o;
    }

    public void b(int i2) {
        this.o = i2;
    }

    public void b(String str) {
        if (str.substring(str.trim().lastIndexOf(".") + 1).equalsIgnoreCase("gif")) {
            this.r = true;
        }
        this.d = str;
    }

    public String c() {
        return this.m;
    }

    public void c(String str) {
        this.f = str;
    }

    public String d() {
        return this.c == null ? "Ads by casee" : this.c;
    }

    public void d(String str) {
        this.h = str;
    }

    public Drawable e() {
        if (this.e != null) {
            return this.e;
        }
        if (this.d != null) {
            this.e = f.a(this.p, this.d);
        }
        if (this.e == null) {
            f();
        }
        return this.e;
    }

    public void e(String str) {
        this.i = str;
    }

    public Drawable f() {
        try {
            this.e = this.p.getResources().getDrawable(17301651);
        } catch (Exception e2) {
            Log.e("CASEE-AD", e2.getMessage(), e2);
        }
        return this.e;
    }

    public void f(String str) {
        this.j = str;
    }

    public String g() {
        return this.f == null ? "http://www.casee.cn" : this.f;
    }

    public void g(String str) {
        this.k = str;
    }

    public String h() {
        return this.h;
    }

    public void h(String str) {
        this.l = str;
    }

    public String i() {
        return this.i;
    }

    public void i(String str) {
        this.q = str;
    }

    public String j() {
        return this.j;
    }

    public void j(String str) {
        this.g = str;
    }

    public String k() {
        return this.k;
    }

    public String l() {
        return this.l;
    }

    public String m() {
        return this.q;
    }

    public String n() {
        return this.g;
    }

    public boolean o() {
        return this.r;
    }
}
