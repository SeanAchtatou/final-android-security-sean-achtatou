package com.casee.adsdk.gifview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import java.io.InputStream;

public class b extends View implements GifAction {
    /* access modifiers changed from: private */
    public d a;
    /* access modifiers changed from: private */
    public Bitmap b;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public boolean d;
    private int e;
    private int f;
    private Rect g;
    private C0000b h;
    private a i;
    /* access modifiers changed from: private */
    public Handler j;

    public enum a {
        WAIT_FINISH(0),
        SYNC_DECODER(1),
        COVER(2);
        
        final int d;

        private a(int i) {
            this.d = i;
        }

        public static a[] a() {
            return (a[]) e.clone();
        }
    }

    /* renamed from: com.casee.adsdk.gifview.b$b  reason: collision with other inner class name */
    private class C0000b extends Thread {
        private C0000b() {
        }

        /* synthetic */ C0000b(b bVar, a aVar) {
            this();
        }

        public void run() {
            if (b.this.a != null) {
                b.this.a.d();
                while (b.this.c) {
                    if (!b.this.d) {
                        c e = b.this.a.e();
                        Bitmap unused = b.this.b = e.a;
                        long j = (long) e.b;
                        if (b.this.j != null) {
                            b.this.j.sendMessage(b.this.j.obtainMessage());
                            SystemClock.sleep(j);
                        } else {
                            return;
                        }
                    } else {
                        SystemClock.sleep(10);
                    }
                }
            }
        }
    }

    public b(Context context) {
        super(context);
        this.a = null;
        this.b = null;
        this.c = true;
        this.d = false;
        this.e = -1;
        this.f = -1;
        this.g = null;
        this.h = null;
        this.i = a.SYNC_DECODER;
        this.j = new a(this);
    }

    public b(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public b(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.a = null;
        this.b = null;
        this.c = true;
        this.d = false;
        this.e = -1;
        this.f = -1;
        this.g = null;
        this.h = null;
        this.i = a.SYNC_DECODER;
        this.j = new a(this);
    }

    private void b() {
        if (this.j != null) {
            this.j.sendMessage(this.j.obtainMessage());
        }
    }

    private void b(InputStream inputStream) {
        if (this.a != null) {
            this.a.a();
            this.a = null;
        }
        this.a = new d(inputStream, this);
        this.a.start();
    }

    public void a() {
        this.c = false;
        if (this.a != null) {
            this.a.a();
        }
        destroyDrawingCache();
        this.b = null;
        this.a = null;
        this.h = null;
    }

    public void a(int i2, int i3) {
        if (i2 > 0 && i3 > 0) {
            this.e = i2;
            this.f = i3;
            this.g = new Rect();
            this.g.left = 0;
            this.g.top = 0;
            this.g.right = i2;
            this.g.bottom = i3;
        }
    }

    public void a(InputStream inputStream) {
        b(inputStream);
    }

    public void a(boolean z, int i2) {
        if (!z) {
            return;
        }
        if (this.a != null) {
            switch (this.i) {
                case WAIT_FINISH:
                    if (i2 != -1) {
                        return;
                    }
                    if (this.a.b() > 1) {
                        new C0000b(this, null).start();
                        return;
                    } else {
                        b();
                        return;
                    }
                case COVER:
                    if (i2 == 1) {
                        this.b = this.a.c();
                        b();
                        return;
                    } else if (i2 != -1) {
                        return;
                    } else {
                        if (this.a.b() <= 1) {
                            b();
                            return;
                        } else if (this.h == null) {
                            this.h = new C0000b(this, null);
                            this.h.start();
                            return;
                        } else {
                            return;
                        }
                    }
                case SYNC_DECODER:
                    if (i2 == 1) {
                        this.b = this.a.c();
                        b();
                        return;
                    } else if (i2 == -1) {
                        b();
                        return;
                    } else if (this.h == null) {
                        this.h = new C0000b(this, null);
                        this.h.start();
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        } else {
            Log.e("gif", "parse error");
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.a != null) {
            if (this.b == null) {
                this.b = this.a.c();
            }
            if (this.b != null) {
                int saveCount = canvas.getSaveCount();
                canvas.save();
                canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
                if (this.e == -1) {
                    canvas.drawBitmap(this.b, 0.0f, 0.0f, (Paint) null);
                } else {
                    canvas.drawBitmap(this.b, (Rect) null, this.g, (Paint) null);
                }
                canvas.restoreToCount(saveCount);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5 = 1;
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        if (this.a == null) {
            i4 = 1;
        } else {
            i5 = this.a.a;
            i4 = this.a.b;
        }
        int max = Math.max(paddingLeft + paddingRight + i5, getSuggestedMinimumWidth());
        int max2 = Math.max(paddingTop + paddingBottom + i4, getSuggestedMinimumHeight());
        int resolveSize = resolveSize(max, i2);
        int resolveSize2 = resolveSize(max2, i3);
        setMeasuredDimension(resolveSize, resolveSize2);
        a(resolveSize, resolveSize2);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        if (i2 != 8) {
            if (i2 == 4) {
                this.d = true;
            } else {
                this.d = false;
            }
        }
    }
}
