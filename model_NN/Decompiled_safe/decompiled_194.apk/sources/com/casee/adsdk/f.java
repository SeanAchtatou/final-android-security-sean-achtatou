package com.casee.adsdk;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class f {

    public static class a implements Comparator {
        public int compare(Object obj, Object obj2) {
            if (obj == null && obj2 == null) {
                return 0;
            }
            return (int) (((File) obj).lastModified() - ((File) obj2).lastModified());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x006b A[SYNTHETIC, Splitter:B:42:0x006b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.drawable.Drawable a(android.content.Context r8, java.lang.String r9) {
        /*
            r7 = 0
            if (r9 == 0) goto L_0x000b
            java.lang.String r0 = ""
            boolean r0 = r0.equals(r9)
            if (r0 == 0) goto L_0x000d
        L_0x000b:
            r0 = r7
        L_0x000c:
            return r0
        L_0x000d:
            java.io.File r0 = r8.getCacheDir()
            java.io.File[] r1 = r0.listFiles()
            java.lang.String r2 = a(r9)
            r3 = 0
        L_0x001a:
            if (r1 == 0) goto L_0x0074
            int r4 = r1.length
            if (r3 >= r4) goto L_0x0074
            r4 = r1[r3]
            java.lang.String r5 = r4.getName()
            boolean r5 = r5.equalsIgnoreCase(r2)
            if (r5 == 0) goto L_0x004f
            java.lang.String r5 = "IMAGECACHE"
            java.lang.String r6 = "read img from cache"
            android.util.Log.i(r5, r6)     // Catch:{ FileNotFoundException -> 0x0057, all -> 0x0067 }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0057, all -> 0x0067 }
            r5.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0057, all -> 0x0067 }
            r4 = 0
            android.graphics.drawable.Drawable r4 = android.graphics.drawable.Drawable.createFromStream(r5, r4)     // Catch:{ FileNotFoundException -> 0x0107 }
            if (r4 == 0) goto L_0x004a
            if (r5 == 0) goto L_0x0043
            r5.close()     // Catch:{ IOException -> 0x0045 }
        L_0x0043:
            r0 = r4
            goto L_0x000c
        L_0x0045:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0043
        L_0x004a:
            if (r5 == 0) goto L_0x004f
            r5.close()     // Catch:{ IOException -> 0x0052 }
        L_0x004f:
            int r3 = r3 + 1
            goto L_0x001a
        L_0x0052:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x004f
        L_0x0057:
            r4 = move-exception
            r5 = r7
        L_0x0059:
            r4.printStackTrace()     // Catch:{ all -> 0x0103 }
            if (r5 == 0) goto L_0x004f
            r5.close()     // Catch:{ IOException -> 0x0062 }
            goto L_0x004f
        L_0x0062:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x004f
        L_0x0067:
            r0 = move-exception
            r1 = r7
        L_0x0069:
            if (r1 == 0) goto L_0x006e
            r1.close()     // Catch:{ IOException -> 0x006f }
        L_0x006e:
            throw r0
        L_0x006f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006e
        L_0x0074:
            java.net.URL r3 = new java.net.URL     // Catch:{ Exception -> 0x00ef }
            r3.<init>(r9)     // Catch:{ Exception -> 0x00ef }
            java.net.URLConnection r3 = r3.openConnection()     // Catch:{ Exception -> 0x00ef }
            r4 = 10000(0x2710, float:1.4013E-41)
            r3.setConnectTimeout(r4)     // Catch:{ Exception -> 0x00ef }
            r4 = 10000(0x2710, float:1.4013E-41)
            r3.setReadTimeout(r4)     // Catch:{ Exception -> 0x00ef }
            r4 = 1
            r3.setUseCaches(r4)     // Catch:{ Exception -> 0x00ef }
            r3.connect()     // Catch:{ Exception -> 0x00ef }
            java.io.InputStream r3 = r3.getInputStream()     // Catch:{ all -> 0x00ed }
            if (r1 == 0) goto L_0x009c
            int r4 = r1.length     // Catch:{ all -> 0x00fc }
            r5 = 30
            if (r4 < r5) goto L_0x009c
            a(r1)     // Catch:{ all -> 0x00fc }
        L_0x009c:
            if (r3 == 0) goto L_0x010a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00fc }
            r1.<init>()     // Catch:{ all -> 0x00fc }
            java.lang.String r4 = r0.getPath()     // Catch:{ all -> 0x00fc }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x00fc }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x00fc }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00fc }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00fc }
            a(r3, r1)     // Catch:{ all -> 0x00fc }
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x00fc }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00fc }
            r4.<init>()     // Catch:{ all -> 0x00fc }
            java.lang.String r0 = r0.getPath()     // Catch:{ all -> 0x00fc }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x00fc }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x00fc }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x00fc }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00fc }
            r1.<init>(r0)     // Catch:{ all -> 0x00fc }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ all -> 0x00fc }
            r0.<init>(r1)     // Catch:{ all -> 0x00fc }
            r1 = 0
            android.graphics.drawable.Drawable r0 = android.graphics.drawable.Drawable.createFromStream(r0, r1)     // Catch:{ all -> 0x00fc }
        L_0x00e6:
            if (r3 == 0) goto L_0x000c
            r3.close()     // Catch:{ all -> 0x00ed }
            goto L_0x000c
        L_0x00ed:
            r0 = move-exception
            throw r0     // Catch:{ Exception -> 0x00ef }
        L_0x00ef:
            r0 = move-exception
            java.lang.String r1 = "IMAGECACHE"
            java.lang.String r2 = r0.getMessage()
            android.util.Log.e(r1, r2, r0)
            r0 = r7
            goto L_0x000c
        L_0x00fc:
            r0 = move-exception
            if (r3 == 0) goto L_0x0102
            r3.close()     // Catch:{ all -> 0x00ed }
        L_0x0102:
            throw r0     // Catch:{ all -> 0x00ed }
        L_0x0103:
            r0 = move-exception
            r1 = r5
            goto L_0x0069
        L_0x0107:
            r4 = move-exception
            goto L_0x0059
        L_0x010a:
            r0 = r7
            goto L_0x00e6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.casee.adsdk.f.a(android.content.Context, java.lang.String):android.graphics.drawable.Drawable");
    }

    private static String a(String str) {
        if (str == null || "".equals(str)) {
            return null;
        }
        return str.substring(str.lastIndexOf("/") + 1);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0024 A[Catch:{ all -> 0x006b }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0029 A[SYNTHETIC, Splitter:B:15:0x0029] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002e A[SYNTHETIC, Splitter:B:18:0x002e] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0058 A[SYNTHETIC, Splitter:B:39:0x0058] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x005d A[SYNTHETIC, Splitter:B:42:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:54:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(java.io.InputStream r7, java.lang.String r8) {
        /*
            r5 = 0
            r0 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0070, all -> 0x0054 }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0070, all -> 0x0054 }
            r2.<init>(r8)     // Catch:{ Exception -> 0x0070, all -> 0x0054 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0070, all -> 0x0054 }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x001c }
        L_0x0010:
            int r3 = r7.read(r2)     // Catch:{ Exception -> 0x001c }
            r4 = -1
            if (r3 == r4) goto L_0x0032
            r4 = 0
            r1.write(r2, r4, r3)     // Catch:{ Exception -> 0x001c }
            goto L_0x0010
        L_0x001c:
            r2 = move-exception
        L_0x001d:
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x006b }
            r2.<init>(r8)     // Catch:{ all -> 0x006b }
            if (r2 == 0) goto L_0x0027
            r2.delete()     // Catch:{ all -> 0x006b }
        L_0x0027:
            if (r1 == 0) goto L_0x002c
            r1.close()     // Catch:{ IOException -> 0x004a }
        L_0x002c:
            if (r5 == 0) goto L_0x0031
            r0.close()     // Catch:{ IOException -> 0x004f }
        L_0x0031:
            return
        L_0x0032:
            r1.flush()     // Catch:{ Exception -> 0x001c }
            if (r1 == 0) goto L_0x003a
            r1.close()     // Catch:{ IOException -> 0x0045 }
        L_0x003a:
            if (r5 == 0) goto L_0x0031
            r0.close()     // Catch:{ IOException -> 0x0040 }
            goto L_0x0031
        L_0x0040:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0031
        L_0x0045:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003a
        L_0x004a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002c
        L_0x004f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0031
        L_0x0054:
            r1 = move-exception
            r2 = r5
        L_0x0056:
            if (r2 == 0) goto L_0x005b
            r2.close()     // Catch:{ IOException -> 0x0061 }
        L_0x005b:
            if (r5 == 0) goto L_0x0060
            r0.close()     // Catch:{ IOException -> 0x0066 }
        L_0x0060:
            throw r1
        L_0x0061:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x005b
        L_0x0066:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0060
        L_0x006b:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x0056
        L_0x0070:
            r1 = move-exception
            r1 = r5
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.casee.adsdk.f.a(java.io.InputStream, java.lang.String):void");
    }

    public static void a(File[] fileArr) {
        ArrayList arrayList = new ArrayList();
        for (File add : fileArr) {
            arrayList.add(add);
        }
        Collections.sort(arrayList, new a());
        int length = (fileArr.length - 30) + 1;
        for (int i = 0; i < length; i++) {
            ((File) arrayList.get(i)).delete();
        }
    }

    public static InputStream b(Context context, String str) {
        if (str == null || "".equals(str)) {
            return null;
        }
        File cacheDir = context.getCacheDir();
        File[] listFiles = cacheDir.listFiles();
        String a2 = a(str);
        int i = 0;
        while (listFiles != null && i < listFiles.length) {
            File file = listFiles[i];
            if (file.getName().equalsIgnoreCase(a2)) {
                try {
                    Log.i("IMAGECACHE", "read gif " + a2 + " from cache");
                    return new FileInputStream(file);
                } catch (IOException e) {
                    Log.e("IMAGECACHE", e.getMessage(), e);
                }
            } else {
                i++;
            }
        }
        try {
            URLConnection openConnection = new URL(str).openConnection();
            openConnection.setConnectTimeout(10000);
            openConnection.setReadTimeout(10000);
            openConnection.setUseCaches(true);
            openConnection.connect();
            InputStream inputStream = openConnection.getInputStream();
            if (listFiles != null && listFiles.length >= 30) {
                a(listFiles);
            }
            if (inputStream != null) {
                Log.i("IMAGECACHE", "read gif from server");
                a(inputStream, cacheDir.getPath() + "/" + a2);
            }
            return inputStream;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
