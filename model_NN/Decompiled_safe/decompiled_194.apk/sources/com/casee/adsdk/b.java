package com.casee.adsdk;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Properties;

public class b {
    static boolean a = false;
    static boolean b = false;
    static long c = 0;
    private static String d;
    private static String e;
    private static String f;
    private static String g;
    private static String h;

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0073 A[SYNTHETIC, Splitter:B:24:0x0073] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0078 A[SYNTHETIC, Splitter:B:27:0x0078] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.casee.adsdk.p a(android.content.Context r6, com.casee.adsdk.CaseeAdView r7) {
        /*
            r5 = 0
            boolean r0 = com.casee.adsdk.b.a
            if (r0 == 0) goto L_0x000a
            com.casee.adsdk.p r0 = com.casee.adsdk.p.a(r6, r5)
        L_0x0009:
            return r0
        L_0x000a:
            a(r6)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007c }
            r0.<init>()     // Catch:{ Exception -> 0x007c }
            java.lang.String r1 = "http://wap.casee.cn/mo/SiteAd.ad?b=1&m="
            r0.append(r1)     // Catch:{ Exception -> 0x007c }
            boolean r1 = com.casee.adsdk.CaseeAdView.d()     // Catch:{ Exception -> 0x007c }
            if (r1 == 0) goto L_0x0022
            java.lang.String r1 = "test"
            r0.append(r1)     // Catch:{ Exception -> 0x007c }
        L_0x0022:
            java.lang.String r1 = "&s="
            r0.append(r1)     // Catch:{ Exception -> 0x007c }
            java.lang.String r1 = com.casee.adsdk.CaseeAdView.c()     // Catch:{ Exception -> 0x007c }
            r0.append(r1)     // Catch:{ Exception -> 0x007c }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x007c }
            java.net.HttpURLConnection r0 = a(r6, r0)     // Catch:{ Exception -> 0x007c }
            java.io.OutputStream r1 = r0.getOutputStream()     // Catch:{ all -> 0x00aa }
            java.io.BufferedWriter r2 = new java.io.BufferedWriter     // Catch:{ all -> 0x00aa }
            java.io.OutputStreamWriter r3 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x00aa }
            r3.<init>(r1)     // Catch:{ all -> 0x00aa }
            r2.<init>(r3)     // Catch:{ all -> 0x00aa }
            java.lang.String r1 = b(r6, r7)     // Catch:{ all -> 0x0070 }
            r2.write(r1)     // Catch:{ all -> 0x0070 }
            r2.flush()     // Catch:{ all -> 0x0070 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ all -> 0x0070 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x0070 }
            java.io.InputStream r4 = r0.getInputStream()     // Catch:{ all -> 0x0070 }
            r3.<init>(r4)     // Catch:{ all -> 0x0070 }
            r1.<init>(r3)     // Catch:{ all -> 0x0070 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x006b }
            r3.<init>()     // Catch:{ all -> 0x006b }
        L_0x0061:
            java.lang.String r4 = r1.readLine()     // Catch:{ all -> 0x006b }
            if (r4 == 0) goto L_0x0088
            r3.append(r4)     // Catch:{ all -> 0x006b }
            goto L_0x0061
        L_0x006b:
            r3 = move-exception
            r1.close()     // Catch:{ all -> 0x0070 }
            throw r3     // Catch:{ all -> 0x0070 }
        L_0x0070:
            r1 = move-exception
        L_0x0071:
            if (r2 == 0) goto L_0x0076
            r2.close()     // Catch:{ Exception -> 0x00a5 }
        L_0x0076:
            if (r0 == 0) goto L_0x007b
            r0.disconnect()     // Catch:{ Exception -> 0x007c }
        L_0x007b:
            throw r1     // Catch:{ Exception -> 0x007c }
        L_0x007c:
            r0 = move-exception
            java.lang.String r1 = "CASEE-AD"
            java.lang.String r2 = r0.getMessage()
            android.util.Log.e(r1, r2, r0)
            r0 = r5
            goto L_0x0009
        L_0x0088:
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x006b }
            com.casee.adsdk.p r3 = com.casee.adsdk.p.a(r6, r3)     // Catch:{ all -> 0x006b }
            r1.close()     // Catch:{ all -> 0x0070 }
            if (r2 == 0) goto L_0x0098
            r2.close()     // Catch:{ Exception -> 0x00a0 }
        L_0x0098:
            if (r0 == 0) goto L_0x009d
            r0.disconnect()     // Catch:{ Exception -> 0x007c }
        L_0x009d:
            r0 = r3
            goto L_0x0009
        L_0x00a0:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ Exception -> 0x007c }
            goto L_0x0098
        L_0x00a5:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Exception -> 0x007c }
            goto L_0x0076
        L_0x00aa:
            r1 = move-exception
            r2 = r5
            goto L_0x0071
        */
        throw new UnsupportedOperationException("Method not decompiled: com.casee.adsdk.b.a(android.content.Context, com.casee.adsdk.CaseeAdView):com.casee.adsdk.p");
    }

    public static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            StringBuffer stringBuffer = new StringBuffer(str);
            stringBuffer.append("CASEE2010");
            return a(instance.digest(stringBuffer.toString().getBytes()));
        } catch (Exception e2) {
            return "00000000000000000000000000000000";
        }
    }

    public static String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < bArr.length; i++) {
            byte b2 = (bArr[i] >>> 4) & 15;
            int i2 = 0;
            while (true) {
                if (b2 < 0 || b2 > 9) {
                    stringBuffer.append((char) ((b2 - 10) + 97));
                } else {
                    stringBuffer.append((char) (b2 + 48));
                }
                b2 = bArr[i] & 15;
                int i3 = i2 + 1;
                if (i2 >= 1) {
                    break;
                }
                i2 = i3;
            }
        }
        return stringBuffer.toString();
    }

    public static HttpURLConnection a(Context context, String str) {
        HttpURLConnection httpURLConnection;
        HttpURLConnection httpURLConnection2;
        if (str == null) {
            return null;
        }
        try {
            if (!b) {
                HttpURLConnection httpURLConnection3 = (HttpURLConnection) new URL(str).openConnection();
                try {
                    httpURLConnection3.setRequestMethod("POST");
                    httpURLConnection3.setDoOutput(true);
                    httpURLConnection3.setRequestProperty("User-Agent", "Android-CASEE-ADSDK");
                    httpURLConnection3.setRequestProperty("Cookie", g(context));
                    httpURLConnection3.setConnectTimeout(20000);
                    httpURLConnection3.setReadTimeout(20000);
                    return httpURLConnection3;
                } catch (Exception e2) {
                    Exception exc = e2;
                    httpURLConnection = httpURLConnection2;
                    e = exc;
                }
            } else {
                URL url = new URL(str);
                Properties properties = System.getProperties();
                properties.setProperty("http.proxyHost", "10.0.0.172");
                properties.setProperty("http.proxyPort", "80");
                HttpURLConnection httpURLConnection4 = (HttpURLConnection) url.openConnection();
                httpURLConnection4.setRequestMethod("POST");
                httpURLConnection4.setDoOutput(true);
                httpURLConnection4.setDoInput(true);
                httpURLConnection4.setRequestProperty("User-Agent", "Android-CASEE-ADSDK");
                httpURLConnection4.setRequestProperty("Cookie", g(context));
                httpURLConnection4.setConnectTimeout(20000);
                httpURLConnection4.setReadTimeout(20000);
                return httpURLConnection4;
            }
        } catch (Exception e3) {
            e = e3;
            httpURLConnection = null;
            Log.e("CASEE-AD", e.getMessage(), e);
            return httpURLConnection;
        }
    }

    public static void a(Context context) {
        if (System.currentTimeMillis() - c >= 120000) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://wap.casee.cn/wlogo.gif").openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setConnectTimeout(5000);
                httpURLConnection.setReadTimeout(5000);
                if (httpURLConnection == null || httpURLConnection.getResponseCode() != 200) {
                    b = true;
                }
            } catch (Exception e2) {
                b = true;
            } finally {
                c = System.currentTimeMillis();
            }
            if (b) {
                Log.e("CASEE-AD", "network cmwap");
            } else {
                Log.e("CASEE-AD", "network cmnet or wifi");
            }
        }
    }

    static String b(Context context) {
        ConnectivityManager connectivityManager;
        NetworkInfo activeNetworkInfo;
        return (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0 || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null) ? "" : activeNetworkInfo.getTypeName();
    }

    static String b(Context context, CaseeAdView caseeAdView) {
        StringBuilder sb = new StringBuilder();
        sb.append("imei=");
        sb.append(c(context));
        sb.append("&imsi=");
        sb.append(d(context));
        sb.append("&ll=");
        if (CaseeAdView.a != null) {
            sb.append(CaseeAdView.a.a());
        }
        sb.append("&v=");
        sb.append("2.6");
        sb.append("&osv=");
        sb.append(CaseeAdView.e());
        sb.append("&ml=");
        sb.append(CaseeAdView.f());
        sb.append("&bid=");
        sb.append(CaseeAdView.g());
        sb.append("&nt=");
        sb.append(b(context));
        sb.append("&l1n=");
        sb.append(e(context));
        sb.append("&so=");
        sb.append(f(context));
        sb.append("&pa=");
        sb.append(context.getPackageName());
        if (!(caseeAdView == null || caseeAdView.i() == null)) {
            sb.append("&aid=");
            sb.append(caseeAdView.i().i());
        }
        if (caseeAdView != null) {
            sb.append("&ver=").append(caseeAdView.j());
            sb.append("&h=").append(caseeAdView.l());
            sb.append("&w=").append(caseeAdView.k());
        }
        sb.append("&auth=");
        sb.append(a(c(context) + d(context)));
        return sb.toString();
    }

    public static String c(Context context) {
        if (d != null && !"".equals(d)) {
            return d;
        }
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            d = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        }
        if (d == null || "".equals(d)) {
            d = Settings.System.getString(context.getContentResolver(), "android_id");
        }
        if (d == null || "".equals(d)) {
            d = "android_id";
        }
        if (d == null) {
            d = "";
        }
        if (d == null || "".equals(d)) {
            Log.i("CASEE-AD", "cannot get device id.");
        }
        Log.i("CASEE-AD", "create device id=" + d);
        return d;
    }

    public static String d(Context context) {
        if (e != null && !"".equals(e)) {
            return e;
        }
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            e = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        }
        return e;
    }

    public static String e(Context context) {
        if (f != null && !"".equals(f)) {
            return f;
        }
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            f = ((TelephonyManager) context.getSystemService("phone")).getLine1Number();
        }
        return f;
    }

    public static String f(Context context) {
        if (g != null && !"".equals(g)) {
            return g;
        }
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            g = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperator();
        }
        return g;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0038 A[Catch:{ Exception -> 0x00b3, all -> 0x00c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0078 A[SYNTHETIC, Splitter:B:28:0x0078] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007d A[Catch:{ IOException -> 0x00ae }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x008d A[SYNTHETIC, Splitter:B:39:0x008d] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0092 A[Catch:{ IOException -> 0x0096 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00a0 A[SYNTHETIC, Splitter:B:48:0x00a0] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00a5 A[Catch:{ IOException -> 0x00a9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00bb A[SYNTHETIC, Splitter:B:62:0x00bb] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00c0 A[Catch:{ IOException -> 0x00c4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00ce A[SYNTHETIC, Splitter:B:71:0x00ce] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00d3 A[Catch:{ IOException -> 0x00d7 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String g(android.content.Context r9) {
        /*
            r7 = 0
            java.lang.String r0 = com.casee.adsdk.b.h
            if (r0 == 0) goto L_0x000f
            java.lang.String r0 = ""
            java.lang.String r1 = com.casee.adsdk.b.h
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0080
        L_0x000f:
            java.lang.String r0 = "casee_cookie.txt"
            r1 = 255(0xff, float:3.57E-43)
            char[] r1 = new char[r1]
            java.io.FileInputStream r2 = r9.openFileInput(r0)     // Catch:{ Exception -> 0x0088, all -> 0x009b }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00f8, all -> 0x00f2 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f2 }
            int r4 = r3.read(r1)     // Catch:{ Exception -> 0x00fb, all -> 0x00f5 }
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x00fb, all -> 0x00f5 }
            r6 = 0
            r5.<init>(r1, r6, r4)     // Catch:{ Exception -> 0x00fb, all -> 0x00f5 }
            com.casee.adsdk.b.h = r5     // Catch:{ Exception -> 0x00fb, all -> 0x00f5 }
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x0083 }
        L_0x002f:
            if (r3 == 0) goto L_0x0034
            r3.close()     // Catch:{ IOException -> 0x0083 }
        L_0x0034:
            java.lang.String r1 = com.casee.adsdk.b.h     // Catch:{ Exception -> 0x00b3, all -> 0x00c9 }
            if (r1 == 0) goto L_0x0042
            java.lang.String r1 = ""
            java.lang.String r2 = com.casee.adsdk.b.h     // Catch:{ Exception -> 0x00b3, all -> 0x00c9 }
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x00b3, all -> 0x00c9 }
            if (r1 == 0) goto L_0x00fe
        L_0x0042:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b3, all -> 0x00c9 }
            r1.<init>()     // Catch:{ Exception -> 0x00b3, all -> 0x00c9 }
            java.lang.String r2 = "caseeuid="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00b3, all -> 0x00c9 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00b3, all -> 0x00c9 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00b3, all -> 0x00c9 }
            java.lang.String r2 = "; Path=/"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00b3, all -> 0x00c9 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00b3, all -> 0x00c9 }
            com.casee.adsdk.b.h = r1     // Catch:{ Exception -> 0x00b3, all -> 0x00c9 }
            r1 = 0
            java.io.FileOutputStream r0 = r9.openFileOutput(r0, r1)     // Catch:{ Exception -> 0x00b3, all -> 0x00c9 }
            java.io.OutputStreamWriter r1 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x00e8, all -> 0x00dc }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00e8, all -> 0x00dc }
            java.lang.String r2 = com.casee.adsdk.b.h     // Catch:{ Exception -> 0x00ed, all -> 0x00e1 }
            r1.write(r2)     // Catch:{ Exception -> 0x00ed, all -> 0x00e1 }
            r1.flush()     // Catch:{ Exception -> 0x00ed, all -> 0x00e1 }
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x0076:
            if (r1 == 0) goto L_0x007b
            r1.close()     // Catch:{ IOException -> 0x00ae }
        L_0x007b:
            if (r0 == 0) goto L_0x0080
            r0.close()     // Catch:{ IOException -> 0x00ae }
        L_0x0080:
            java.lang.String r0 = com.casee.adsdk.b.h
            return r0
        L_0x0083:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0034
        L_0x0088:
            r1 = move-exception
            r1 = r7
            r2 = r7
        L_0x008b:
            if (r2 == 0) goto L_0x0090
            r2.close()     // Catch:{ IOException -> 0x0096 }
        L_0x0090:
            if (r1 == 0) goto L_0x0034
            r1.close()     // Catch:{ IOException -> 0x0096 }
            goto L_0x0034
        L_0x0096:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0034
        L_0x009b:
            r0 = move-exception
            r1 = r7
            r2 = r7
        L_0x009e:
            if (r2 == 0) goto L_0x00a3
            r2.close()     // Catch:{ IOException -> 0x00a9 }
        L_0x00a3:
            if (r1 == 0) goto L_0x00a8
            r1.close()     // Catch:{ IOException -> 0x00a9 }
        L_0x00a8:
            throw r0
        L_0x00a9:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00a8
        L_0x00ae:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0080
        L_0x00b3:
            r0 = move-exception
            r1 = r7
            r2 = r7
        L_0x00b6:
            r0.printStackTrace()     // Catch:{ all -> 0x00e6 }
            if (r2 == 0) goto L_0x00be
            r2.close()     // Catch:{ IOException -> 0x00c4 }
        L_0x00be:
            if (r1 == 0) goto L_0x0080
            r1.close()     // Catch:{ IOException -> 0x00c4 }
            goto L_0x0080
        L_0x00c4:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0080
        L_0x00c9:
            r0 = move-exception
            r1 = r7
            r2 = r7
        L_0x00cc:
            if (r2 == 0) goto L_0x00d1
            r2.close()     // Catch:{ IOException -> 0x00d7 }
        L_0x00d1:
            if (r1 == 0) goto L_0x00d6
            r1.close()     // Catch:{ IOException -> 0x00d7 }
        L_0x00d6:
            throw r0
        L_0x00d7:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00d6
        L_0x00dc:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r7
            goto L_0x00cc
        L_0x00e1:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
            goto L_0x00cc
        L_0x00e6:
            r0 = move-exception
            goto L_0x00cc
        L_0x00e8:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r7
            goto L_0x00b6
        L_0x00ed:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
            goto L_0x00b6
        L_0x00f2:
            r0 = move-exception
            r1 = r7
            goto L_0x009e
        L_0x00f5:
            r0 = move-exception
            r1 = r3
            goto L_0x009e
        L_0x00f8:
            r1 = move-exception
            r1 = r7
            goto L_0x008b
        L_0x00fb:
            r1 = move-exception
            r1 = r3
            goto L_0x008b
        L_0x00fe:
            r0 = r7
            r1 = r7
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.casee.adsdk.b.g(android.content.Context):java.lang.String");
    }
}
