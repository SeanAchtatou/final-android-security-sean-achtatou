package com.casee.adsdk;

import android.util.Log;
import android.view.animation.Animation;

class z implements Animation.AnimationListener {
    final /* synthetic */ i a;

    z(i iVar) {
        this.a = iVar;
    }

    public void onAnimationEnd(Animation animation) {
        if (this.a.C != null && this.a.C.isShowing()) {
            this.a.C.dismiss();
        }
        if (this.a.C != null && !this.a.C.isShowing()) {
            Log.w("result", "===webViewMoveOut " + this.a.C.isShowing());
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
