package com.casee.adsdk;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class l extends WebViewClient {
    final /* synthetic */ i a;

    l(i iVar) {
        this.a = iVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.casee.adsdk.i.a(com.casee.adsdk.i, boolean):boolean
     arg types: [com.casee.adsdk.i, int]
     candidates:
      com.casee.adsdk.i.a(com.casee.adsdk.i, android.widget.RelativeLayout):android.widget.RelativeLayout
      com.casee.adsdk.i.a(com.casee.adsdk.i, com.casee.adsdk.i$a):com.casee.adsdk.i$a
      com.casee.adsdk.i.a(com.casee.adsdk.i, java.lang.String):java.lang.String
      com.casee.adsdk.i.a(com.casee.adsdk.i, java.util.ArrayList):java.util.ArrayList
      com.casee.adsdk.i.a(android.content.Context, com.casee.adsdk.CaseeAdView):void
      com.casee.adsdk.i.a(com.casee.adsdk.i, android.app.Activity):void
      com.casee.adsdk.i.a(android.content.Context, java.lang.String):android.widget.RelativeLayout
      com.casee.adsdk.i.a(com.casee.adsdk.i, boolean):boolean */
    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.a.j != null && (this.a.j.getVisibility() == 0 || this.a.j.getVisibility() == 8)) {
            this.a.j.clearAnimation();
            this.a.q.clearAnimation();
            this.a.j.setVisibility(4);
        }
        if (this.a.F) {
            this.a.h.clearAnimation();
            boolean unused = this.a.F = false;
            e.a = true;
            Log.i("result", "\t\tisFirstLoad = false;");
            Log.i("result", "----AdViewGroup.myWebViewCreated" + e.a);
            this.a.d();
        }
        this.a.f();
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        Log.e("result", "===" + str);
        if (this.a.j != null) {
            this.a.j.setVisibility(0);
            this.a.g();
        }
        if (str.indexOf(".apk") != str.length() - 4) {
            String unused = this.a.D = str;
        }
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str.startsWith("wtai://wp/")) {
            if (str.startsWith("wtai://wp/mc;")) {
                this.a.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("tel:" + str.substring("wtai://wp/mc;".length()))));
                return true;
            }
            if (str.startsWith("wtai://wp/sd;")) {
            }
            if (str.startsWith("wtai://wp/ap;")) {
            }
        }
        return super.shouldOverrideUrlLoading(webView, str);
    }
}
