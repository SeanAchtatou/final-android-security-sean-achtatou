package com.casee.adsdk;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.casee.adsdk.gifview.b;

class e extends RelativeLayout {
    static boolean a = false;
    static boolean b = false;
    static final Typeface c = Typeface.create(Typeface.SANS_SERIF, 1);
    static final Typeface d = Typeface.create(Typeface.SANS_SERIF, 0);
    private int A = 0;
    private BitmapDrawable e;
    private Context f;
    private p g;
    private ImageView h;
    private TextView i;
    private View j;
    private TextView k;
    private ScrollView l;
    private int m;
    private float n = 13.0f;
    private int o = -1;
    private boolean p = false;
    private boolean q = true;
    private float r = getResources().getDisplayMetrics().density;
    private CaseeAdView s;
    private b t;
    private i u;
    private final int v = 0;
    private final int w = 1;
    private Handler x = new w(this);
    private boolean y = true;
    private int z = 0;

    public e(Context context, p pVar, CaseeAdView caseeAdView) {
        super(context, null, 0);
        this.g = pVar;
        this.f = context;
        this.p = caseeAdView.j();
        this.q = CaseeAdView.d();
        this.o = caseeAdView.h();
        this.m = caseeAdView.b();
        this.n = caseeAdView.a();
        this.s = caseeAdView;
        this.u = new i(context, pVar, caseeAdView);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.casee.adsdk.e.a(android.graphics.Rect, int, int, boolean):android.graphics.drawable.BitmapDrawable
     arg types: [android.graphics.Rect, int, int, int]
     candidates:
      com.casee.adsdk.e.a(android.graphics.Canvas, android.graphics.Rect, int, int):void
      com.casee.adsdk.e.a(android.graphics.Rect, int, int, boolean):android.graphics.drawable.BitmapDrawable */
    private BitmapDrawable a(Rect rect, int i2, int i3) {
        return a(rect, i2, i3, false);
    }

    private BitmapDrawable a(Rect rect, int i2, int i3, boolean z2) {
        try {
            Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            a(new Canvas(createBitmap), rect, i2, i3);
            return new BitmapDrawable(createBitmap);
        } catch (Throwable th) {
            return null;
        }
    }

    private void a() {
        if (this.g != null) {
            setClickable(true);
            setFocusable(true);
            setOnClickListener(this.u);
            if (this.g.b() == 0) {
                a(this.g.b(), this.g.e());
                c();
            } else if (this.g.o()) {
                d();
            } else {
                a(this.g.b(), this.g.e());
            }
            b(this.g.b());
            b();
        }
    }

    private void a(int i2, Drawable drawable) {
        RelativeLayout.LayoutParams layoutParams;
        this.h = new ImageView(this.f);
        this.h.setImageDrawable(drawable);
        if (i2 != 0) {
            this.h.setScaleType(ImageView.ScaleType.FIT_CENTER);
            layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        } else {
            layoutParams = new RelativeLayout.LayoutParams((int) (this.r * 38.0f), (int) (this.r * 38.0f));
            if (this.p) {
                layoutParams.setMargins((int) (this.r * 5.0f), 5, 0, (int) (this.r * 5.0f));
                layoutParams.addRule(14);
            } else {
                layoutParams.setMargins(1, 1, 0, 1);
                layoutParams.addRule(9);
                layoutParams.addRule(15);
            }
        }
        this.h.setId(1);
        this.h.setLayoutParams(layoutParams);
        addView(this.h);
    }

    private static void a(Canvas canvas, Rect rect, int i2, int i3) {
        Paint paint = new Paint();
        paint.setColor(i2);
        paint.setAntiAlias(true);
        canvas.drawRect(rect, paint);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(127, Color.red(i3), Color.green(i3), Color.blue(i3)), i3});
        int height = ((int) (((double) rect.height()) * 0.5d)) + rect.top;
        gradientDrawable.setBounds(rect.left, rect.top, rect.right, height);
        gradientDrawable.draw(canvas);
        Rect rect2 = new Rect(rect.left, height, rect.right, rect.bottom);
        Paint paint2 = new Paint();
        paint2.setColor(i3);
        canvas.drawRect(rect2, paint2);
    }

    private void a(String str) {
        this.l = new ScrollView(this.f);
        this.l.setId(2);
        this.l.setVerticalScrollBarEnabled(false);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        if (this.r < 1.0f) {
            layoutParams.setMargins(2, 0, 5, 5);
        } else {
            layoutParams.setMargins(((int) ((38.0f - this.n) / 2.0f)) + 5, 0, 5, 5);
        }
        layoutParams.addRule(3, 1);
        layoutParams.addRule(2, 3);
        this.l.setLayoutParams(layoutParams);
        LinearLayout linearLayout = new LinearLayout(this.f);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(1);
        for (int i2 = 0; i2 < str.length(); i2++) {
            TextView textView = new TextView(this.f);
            textView.setTextSize(this.n);
            textView.setTextColor(this.o);
            String valueOf = String.valueOf(str.charAt(i2));
            textView.setTypeface(c);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams2.addRule(13);
            layoutParams2.setMargins(0, 0, 0, 0);
            textView.setText(valueOf);
            textView.setClickable(true);
            textView.setFocusable(true);
            textView.setOnClickListener(this.u);
            textView.setLayoutParams(layoutParams2);
            linearLayout.addView(textView, i2);
        }
        linearLayout.setClickable(true);
        linearLayout.setFocusable(true);
        linearLayout.setOnClickListener(this.u);
        this.l.addView(linearLayout);
    }

    private void b() {
        if (this.q || "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA".equals(CaseeAdView.c())) {
            this.k = new TextView(this.f);
            this.k.setGravity(17);
            this.k.setText(this.g.b);
            this.k.setId(4);
            if (this.g.b() != 0) {
                this.k.setBackgroundColor(Color.argb(255, 0, 0, 0));
            }
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(14);
            if (!this.p) {
                layoutParams.addRule(12);
            } else {
                layoutParams.addRule(15);
            }
            this.k.setLayoutParams(layoutParams);
            this.k.setTextSize(10.0f);
            this.k.setTextColor(this.o);
            addView(this.k);
        }
    }

    private void b(int i2) {
        this.j = a(this.r);
        addView(this.j);
        if (i2 == 1) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(7000);
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            alphaAnimation.startNow();
            this.j.startAnimation(alphaAnimation);
        }
    }

    private void c() {
        if (this.p) {
            a(this.g.d() + " ");
            this.l.setSmoothScrollingEnabled(true);
            this.l.setClickable(true);
            this.l.setFocusable(true);
            this.l.addStatesFromChildren();
            this.l.setOnClickListener(this.u);
            addView(this.l);
            e();
            return;
        }
        this.i = new n(this.f);
        this.i.setText(this.g.d());
        this.i.setId(2);
        this.i.setTextSize(this.n);
        this.i.setTextColor(this.o);
        this.i.setTypeface(c);
        this.i.setSingleLine(true);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        if (this.n < 16.0f) {
            layoutParams.setMargins(5, 0, 5, 5);
        } else {
            layoutParams.setMargins(5, 4, 5, 5);
        }
        layoutParams.addRule(1, 1);
        this.i.setLayoutParams(layoutParams);
        this.i.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        this.i.setMarqueeRepeatLimit(-1);
        this.i.setClickable(true);
        this.i.setFocusable(true);
        this.i.setOnClickListener(this.u);
        addView(this.i);
    }

    private void d() {
        this.t = this.g.a(this.p);
        if (this.t == null) {
            a(0, this.g.f());
            c();
            return;
        }
        this.t.setId(1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        addView(this.t, layoutParams);
    }

    private void e() {
        this.x.sendEmptyMessageDelayed(0, 100);
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.l == null) {
            return;
        }
        if (this.A > 2) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            this.x.sendEmptyMessageDelayed(1, 2000);
        } else if (this.y) {
            this.x.sendEmptyMessageDelayed(0, 2000);
            this.l.scrollTo(this.l.getScrollX(), 0);
            this.y = false;
            this.A++;
        } else {
            this.l.smoothScrollBy(0, 2);
            if (this.z != this.l.getScrollY() || this.z == 0) {
                this.z = this.l.getScrollY();
                this.x.sendEmptyMessageDelayed(0, 100);
                return;
            }
            this.y = true;
            this.z = this.l.getScrollY();
            this.x.sendEmptyMessageDelayed(0, 2000);
        }
    }

    /* access modifiers changed from: protected */
    public View a(float f2) {
        ImageView imageView = new ImageView(this.f);
        Bitmap a2 = this.g.a(f2);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setImageBitmap(a2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(11);
        layoutParams.addRule(12);
        imageView.setLayoutParams(layoutParams);
        return imageView;
    }

    public void a(int i2) {
        this.o = i2;
        if (this.i != null) {
            this.i.setTextColor(i2);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.t != null) {
            this.t.a();
        }
    }

    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int i4 = this.r < 1.0f ? 24 : this.r == 1.0f ? 48 : 72;
        if (this.p) {
            setMeasuredDimension(i4, getMeasuredHeight());
        } else {
            setMeasuredDimension(getMeasuredWidth(), i4);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != 0 && i3 != 0) {
            this.e = a(new Rect(0, 0, i2, i3), -1, this.m);
            setBackgroundDrawable(this.e);
        }
    }

    public void setBackgroundColor(int i2) {
        this.m = i2;
    }
}
