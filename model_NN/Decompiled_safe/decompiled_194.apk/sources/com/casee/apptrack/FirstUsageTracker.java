package com.casee.apptrack;

import android.content.Context;
import android.util.Log;
import com.adchina.android.ads.Common;
import com.casee.adsdk.b;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class FirstUsageTracker {
    Context a;
    NotifyUtil b;
    /* access modifiers changed from: private */
    public TrackerListener c;

    public interface TrackerListener {
        void onFailed();

        void onSuccess();
    }

    public FirstUsageTracker(Context context) {
        this.a = context;
        this.c = null;
        this.b = new NotifyUtil(context);
    }

    public FirstUsageTracker(Context context, TrackerListener trackerListener) {
        this.a = context;
        this.c = trackerListener;
        this.b = new NotifyUtil(context);
    }

    private Map a(Properties properties) {
        String property = properties.getProperty("aid");
        String property2 = properties.getProperty("cid");
        String c2 = b.c(this.a);
        if (a(property) || a(property2) || a(c2)) {
            return null;
        }
        HashMap hashMap = new HashMap(3);
        hashMap.put("aid", property);
        hashMap.put("cid", property2);
        hashMap.put("did", c2);
        hashMap.put("key", property + "-" + property2 + "-" + c2);
        hashMap.put("time", new SimpleDateFormat("yyyyMMdd").format(new Date()));
        return hashMap;
    }

    private boolean a(String str) {
        return str == null || str.trim().equals("");
    }

    public void doTrack(TrackerListener trackerListener) {
        Properties properties;
        Properties properties2;
        try {
            properties = this.b.b("casee_pt.txt");
        } catch (Throwable th) {
            Log.e("CASEE-TA", "casee_pt.txt not found");
            properties = null;
        }
        if (properties != null) {
            try {
                properties2 = this.b.a("pt.log");
            } catch (Throwable th2) {
                Log.e("CASEE-TA", th2.getMessage(), th2);
                properties2 = null;
            }
            if (properties2 == null) {
                properties2 = new Properties();
            }
            if (!Common.KCLK.equals(properties2.getProperty("fu"))) {
                if (this.b.a("http://wap.casee.cn/mo/fee.jsp", a(properties))) {
                    properties2.setProperty("fu", Common.KCLK);
                    try {
                        this.b.a("pt.log", properties2);
                    } catch (Throwable th3) {
                        Log.e("CASEE-TA", th3.getMessage(), th3);
                    }
                    if (trackerListener != null) {
                        trackerListener.onSuccess();
                    }
                } else if (trackerListener != null) {
                    trackerListener.onFailed();
                }
            }
        } else if (trackerListener != null) {
            trackerListener.onFailed();
        }
    }

    public void doTrackASync() {
        new a(this).start();
    }
}
