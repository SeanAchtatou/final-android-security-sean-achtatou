package com.casee.apptrack;

import android.content.Context;

public class VersionTracker {
    Context a;
    NotifyUtil b;
    String c;

    public VersionTracker(Context context) {
        this.a = context;
        this.b = new NotifyUtil(context);
        this.c = context.getPackageName();
    }
}
