package com.mobclick.android;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.winad.android.ads.JsonUtils;
import java.lang.reflect.Field;
import org.json.JSONObject;

public class UmengFeedback extends Activity {
    /* access modifiers changed from: private */
    public static e e;
    private static Context f;
    /* access modifiers changed from: private */
    public Spinner a;
    /* access modifiers changed from: private */
    public Spinner b;
    /* access modifiers changed from: private */
    public EditText c;
    private String d = JsonUtils.ERROR_ACK;
    /* access modifiers changed from: private */
    public JSONObject g;

    private int a(Context context, String str, String str2) {
        try {
            Field field = Class.forName(String.valueOf(context.getPackageName()) + ".R$" + str).getField(str2);
            return Integer.parseInt(field.get(field.getName()).toString());
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    /* access modifiers changed from: private */
    public JSONObject a(int i, int i2, String str) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("age", i);
            jSONObject.put("gender", i2);
            jSONObject.put("content", str);
            return jSONObject;
        } catch (Exception e2) {
            Log.e(this.d, e2.getMessage());
            return null;
        }
    }

    public static void a(Context context) {
        f = context;
    }

    public static void a(e eVar) {
        e = eVar;
    }

    private void b() {
        ((TextView) findViewById(a(f, "id", "feedback_umeng_title"))).setText(n.a(this));
        ((TextView) findViewById(a(f, "id", "feedback_title"))).setText(n.b(this));
        ((Button) findViewById(a(f, "id", "feedback_submit"))).setText(n.d(this));
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(a(f, "layout", "umeng_feedback"));
        ((ImageView) findViewById(a(f, "id", "umengBannerTop"))).setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.rgb(135, 135, 135), Color.rgb(90, 90, 90), Color.rgb(52, 52, 52), Color.rgb(0, 0, 0)}));
        b();
        this.c = (EditText) findViewById(a(f, "id", "feedback_content"));
        this.c.setHint(n.c(this));
        this.a = (Spinner) findViewById(a(f, "id", "feedback_age_spinner"));
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, n.f(this));
        arrayAdapter.setDropDownViewResource(17367049);
        this.a.setAdapter((SpinnerAdapter) arrayAdapter);
        this.b = (Spinner) findViewById(a(f, "id", "feedback_gender_spinner"));
        ArrayAdapter arrayAdapter2 = new ArrayAdapter(this, 17367048, n.g(this));
        arrayAdapter2.setDropDownViewResource(17367049);
        this.b.setAdapter((SpinnerAdapter) arrayAdapter2);
        ((Button) findViewById(a(f, "id", "feedback_submit"))).setOnClickListener(new p(this));
    }
}
