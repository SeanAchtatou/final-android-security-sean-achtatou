package com.wiyun.ad;

import java.io.IOException;
import java.util.Random;

public class p {
    static Random a = new Random();
    static int[][] b;

    static {
        int[] iArr = new int[6];
        iArr[0] = 2;
        iArr[1] = 3;
        iArr[2] = 5;
        iArr[3] = 4;
        iArr[4] = 1;
        int[] iArr2 = new int[6];
        iArr2[0] = 3;
        iArr2[1] = 4;
        iArr2[2] = 5;
        iArr2[3] = 1;
        iArr2[5] = 2;
        int[] iArr3 = new int[6];
        iArr3[1] = 5;
        iArr3[2] = 2;
        iArr3[3] = 1;
        iArr3[4] = 3;
        iArr3[5] = 4;
        int[] iArr4 = new int[6];
        iArr4[0] = 5;
        iArr4[1] = 4;
        iArr4[2] = 3;
        iArr4[3] = 2;
        iArr4[5] = 1;
        int[] iArr5 = new int[6];
        iArr5[0] = 5;
        iArr5[1] = 3;
        iArr5[2] = 1;
        iArr5[3] = 4;
        iArr5[5] = 2;
        int[] iArr6 = new int[6];
        iArr6[0] = 2;
        iArr6[1] = 4;
        iArr6[3] = 3;
        iArr6[4] = 5;
        iArr6[5] = 1;
        int[] iArr7 = new int[6];
        iArr7[0] = 4;
        iArr7[1] = 2;
        iArr7[3] = 3;
        iArr7[4] = 1;
        iArr7[5] = 5;
        int[] iArr8 = new int[6];
        iArr8[0] = 4;
        iArr8[2] = 2;
        iArr8[3] = 5;
        iArr8[4] = 1;
        iArr8[5] = 3;
        b = new int[][]{iArr, iArr2, iArr3, iArr4, iArr5, iArr6, iArr7, iArr8};
    }

    static String a(String str) {
        if (str == null || str.length() < 10) {
            return null;
        }
        try {
            int parseInt = Integer.parseInt(str.substring(0, 2), 16);
            String substring = str.substring(0, 8);
            int parseInt2 = Integer.parseInt(str.substring(str.length() - 2), 16);
            int parseInt3 = Integer.parseInt(a(String.format("%06x", Integer.valueOf(Integer.parseInt(str.substring(str.length() - 8, str.length() - 2), 16) ^ parseInt2)), substring, o.c(substring)), 16);
            if ((parseInt3 * 997) % parseInt == parseInt2) {
                return String.valueOf(parseInt3);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    private static String a(String str, int i) {
        String str2 = new String();
        int i2 = i % 8;
        char[] charArray = str.toCharArray();
        for (int i3 = 0; i3 < 6; i3++) {
            charArray[b[i2][i3]] = str.charAt(i3);
        }
        String str3 = str2;
        for (int i4 = 0; i4 < 6; i4++) {
            str3 = String.valueOf(str3) + charArray[i4];
        }
        return str3;
    }

    private static String a(String str, int i, int i2) {
        int length = i2 % str.length();
        if (length <= 0) {
            return str;
        }
        switch (i) {
            case -1:
                int i3 = length + 1;
                return String.valueOf(str.substring(i3 - 1, str.length())) + str.substring(0, i3 - 1);
            case 0:
            default:
                return str;
            case 1:
                return String.valueOf(str.substring(str.length() - length, str.length())) + str.substring(0, str.length() - length);
        }
    }

    private static String a(String str, String str2) throws IOException {
        long abs = Math.abs(Long.parseLong(o.c(str).substring(0, 8), 16));
        long abs2 = Math.abs(Long.parseLong(o.c(str2).substring(0, 8), 16));
        long j = abs ^ abs2;
        String c = o.c(String.valueOf(String.format("%010d", Long.valueOf(abs))) + String.format("%010d", Long.valueOf(abs2)));
        for (int i = 0; i < 16; i++) {
            j = i % 2 == 0 ? Math.abs((((j ^ ((long) c.charAt(i))) << 2) ^ abs2) % 1000000000) : j ^ ((long) c.charAt(i));
        }
        String binaryString = Long.toBinaryString(j);
        if (binaryString.length() > 56) {
            binaryString.substring(0, 55);
        }
        return binaryString;
    }

    private static String a(String str, String str2, String str3) throws IOException {
        int length = str.length() / 2;
        int length2 = str.length();
        String a2 = a(a(str2, str3), 1, 15);
        String str4 = str;
        for (int i = 15; i >= 0; i--) {
            int parseLong = (int) Long.parseLong(str4.substring(length, length2), 16);
            int intValue = Integer.valueOf(a2.substring(0, length), 2).intValue();
            int intValue2 = Integer.valueOf(a2.substring(a2.length() - length, a2.length()), 2).intValue();
            String format = String.format("%03x", Integer.valueOf(((int) Long.parseLong(str4.substring(0, length), 16)) ^ intValue));
            a2 = a(a2, -1, 1);
            str4 = a(String.valueOf(format) + String.format("%03x", Integer.valueOf(parseLong ^ intValue2)), i + 1);
        }
        return str4;
    }
}
