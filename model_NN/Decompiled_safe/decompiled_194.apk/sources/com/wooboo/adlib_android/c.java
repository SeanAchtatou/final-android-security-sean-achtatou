package com.wooboo.adlib_android;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.adchina.android.ads.Common;
import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public final class c {
    protected static volatile int a = 1;
    protected static long b;
    private static TelephonyManager c;
    private static boolean d;
    private static String e;
    private static String f;
    private static int g;
    private static int h;
    private static String i = null;
    private static String j = null;
    private static String k = null;
    private static String l = null;
    private static int m = -2;

    protected static void a(int i2) {
        a = i2;
    }

    public static void b(int i2) {
        h = i2;
    }

    protected static int a(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                int i2 = applicationInfo.metaData.getInt("Market_ID");
                Log.d("Wooboo SDK 1.2", "The Market_ID is set to " + i2);
                return i2;
            }
        } catch (Exception e2) {
            Log.e("Wooboo SDK 1.2", "Could not read Market_ID meta-data from AndroidManifest.xml.");
        }
        Log.d("Wooboo SDK 1.2", "Use the default Market_ID is " + 1);
        return 1;
    }

    protected static String b(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                return applicationInfo.metaData.getString("Wooboo_PID");
            }
            return null;
        } catch (Exception e2) {
            Log.e("Wooboo SDK 1.2", "Could not read Wooboo_PID meta-data from AndroidManifest.xml.");
            return null;
        }
    }

    protected static void a(String str) {
        l = str;
    }

    protected static void b(String str) {
        k = str;
    }

    protected static void c(int i2) {
        g = i2;
    }

    protected static void c(Context context) {
        String str;
        c = (TelephonyManager) context.getSystemService("phone");
        if (j == null) {
            if (c != null) {
                str = c.getSimSerialNumber();
                j = str;
            } else {
                str = null;
            }
            j = str;
        }
        if (j != null && i == null) {
            i = f(j);
        }
    }

    protected static void c(String str) {
        Log.e("Wooboo SDK 1.2", str);
        throw new IllegalArgumentException(str);
    }

    protected static String d(Context context) {
        String language = context.getResources().getConfiguration().locale.getLanguage();
        if (language.contains("en")) {
            return Common.KIMP;
        }
        if (language.contains("zh")) {
            return "0";
        }
        if (language.contains("ko")) {
            return "5";
        }
        if (language.contains("fr")) {
            return "3";
        }
        if (language.contains("es")) {
            return "8";
        }
        if (language.contains("de")) {
            return "6";
        }
        if (language.contains("it")) {
            return "7";
        }
        if (language.contains("ja")) {
            return "4";
        }
        if (language.contains("ru")) {
            return "9";
        }
        return Common.KIMP;
    }

    protected static void d(String str) {
        if (str == null || str.length() != 32) {
            c("CONFIGURATION ERROR:  Incorrect Telead_PID.  Should 32 [a-z,0-9] characters:  " + e);
        }
        Log.d("Wooboo SDK 1.2", "Your Telead_PID is " + str);
        e = str;
    }

    protected static void a(boolean z) {
        d = z;
    }

    protected static void e(String str) {
        f = str;
    }

    protected static String e(Context context) {
        String string = Settings.System.getString(context.getContentResolver(), "android_id");
        if (string == null && c != null) {
            string = c.getDeviceId();
        }
        if (string == null) {
            return "00000000";
        }
        return string;
    }

    /* JADX WARNING: Removed duplicated region for block: B:69:0x019a A[Catch:{ Exception -> 0x0265 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static com.wooboo.adlib_android.b f(android.content.Context r10) {
        /*
            r4 = 1
            r6 = -1
            r8 = 0
            java.lang.String r0 = "android.permission.INTERNET"
            int r0 = r10.checkCallingOrSelfPermission(r0)
            if (r0 != r6) goto L_0x0010
            java.lang.String r0 = "Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />"
            c(r0)
        L_0x0010:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            int r1 = com.wooboo.adlib_android.c.h
            java.lang.String r2 = "pit"
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.String r3 = "="
            java.lang.StringBuilder r2 = r2.append(r3)
            r2.append(r1)
            java.lang.String r1 = "4"
            java.lang.String r2 = "ifm"
            a(r0, r2, r1)
            int r1 = com.wooboo.adlib_android.c.m
            r2 = -2
            if (r1 != r2) goto L_0x0032
        L_0x0032:
            java.lang.String r1 = "mt"
            java.lang.String r2 = android.os.Build.MODEL
            a(r0, r1, r2)
            int r1 = com.wooboo.adlib_android.c.m
            if (r1 == r6) goto L_0x0051
            java.lang.String r1 = "mi"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            int r3 = com.wooboo.adlib_android.c.m
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            a(r0, r1, r2)
        L_0x0051:
            java.lang.String r1 = "bs"
            java.lang.String r2 = "7"
            a(r0, r1, r2)
            java.lang.String r1 = "pid"
            java.lang.String r2 = com.wooboo.adlib_android.c.e
            a(r0, r1, r2)
            java.lang.String r1 = "csdk"
            java.lang.String r2 = android.os.Build.VERSION.RELEASE
            a(r0, r1, r2)
            java.lang.String r1 = "sdk"
            java.lang.String r2 = "1.2"
            a(r0, r1, r2)
            java.lang.String r1 = "uid"
            java.lang.String r2 = com.wooboo.adlib_android.c.f
            a(r0, r1, r2)
            java.lang.String r1 = com.wooboo.adlib_android.c.j
            if (r1 == 0) goto L_0x0084
            java.lang.String r1 = com.wooboo.adlib_android.c.i
            if (r1 != 0) goto L_0x0084
            java.lang.String r1 = com.wooboo.adlib_android.c.j
            java.lang.String r1 = f(r1)
            com.wooboo.adlib_android.c.i = r1
        L_0x0084:
            java.lang.String r1 = com.wooboo.adlib_android.c.i
            if (r1 == 0) goto L_0x0093
            java.lang.String r1 = "&"
            java.lang.StringBuilder r1 = r0.append(r1)
            java.lang.String r2 = com.wooboo.adlib_android.c.i
            r1.append(r2)
        L_0x0093:
            java.lang.String r1 = "ml"
            java.lang.String r2 = com.wooboo.adlib_android.c.k
            a(r0, r1, r2)
            java.lang.String r1 = "pn"
            android.telephony.TelephonyManager r2 = com.wooboo.adlib_android.c.c
            if (r2 == 0) goto L_0x0103
            android.telephony.TelephonyManager r2 = com.wooboo.adlib_android.c.c
            java.lang.String r2 = r2.getLine1Number()
        L_0x00a6:
            a(r0, r1, r2)
            java.lang.String r1 = "apn"
            java.lang.String r2 = com.wooboo.adlib_android.c.l
            a(r0, r1, r2)
            java.lang.String r1 = "sw"
            a(r0, r1, r8)
            int r1 = com.wooboo.adlib_android.c.g
            java.lang.String r1 = java.lang.String.valueOf(r1)
            java.lang.String r2 = "mid"
            a(r0, r2, r1)
            java.lang.String r1 = r0.toString()
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            com.wooboo.adlib_android.c.b = r2     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            java.lang.String r2 = "pit=1&pf=android"
            int r0 = com.wooboo.adlib_android.c.a     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            if (r0 != r4) goto L_0x014b
            boolean r0 = com.wooboo.adlib_android.c.d     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            if (r0 == 0) goto L_0x0105
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            byte[] r4 = com.wooboo.adlib_android.e.b     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            java.lang.String r5 = "UTF-8"
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0114, all -> 0x018e }
        L_0x00e2:
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            r3 = 6000(0x1770, float:8.408E-42)
            r0.setConnectTimeout(r3)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            r3 = 6000(0x1770, float:8.408E-42)
            r0.setReadTimeout(r3)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
        L_0x00f2:
            if (r0 != 0) goto L_0x01ab
            r1 = 0
            a(r1)     // Catch:{ Exception -> 0x019e }
            r1 = 0
            a(r1)     // Catch:{ Exception -> 0x019e }
            if (r0 == 0) goto L_0x0101
            r0.disconnect()     // Catch:{ Exception -> 0x019e }
        L_0x0101:
            r0 = r8
        L_0x0102:
            return r0
        L_0x0103:
            r2 = r8
            goto L_0x00a6
        L_0x0105:
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            byte[] r4 = com.wooboo.adlib_android.e.a     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            java.lang.String r5 = "UTF-8"
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            goto L_0x00e2
        L_0x0114:
            r0 = move-exception
            r1 = r8
            r2 = r8
        L_0x0117:
            java.lang.String r3 = "Wooboo SDK 1.2"
            android.util.Log.w(r3, r0)     // Catch:{ all -> 0x0287 }
            java.lang.String r0 = "Wooboo SDK 1.2"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0287 }
            java.lang.String r4 = "Could not get ad from Wooboo servers ("
            r3.<init>(r4)     // Catch:{ all -> 0x0287 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0287 }
            long r6 = com.wooboo.adlib_android.c.b     // Catch:{ all -> 0x0287 }
            long r4 = r4 - r6
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0287 }
            java.lang.String r4 = " ms);"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0287 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0287 }
            android.util.Log.w(r0, r3)     // Catch:{ all -> 0x0287 }
            a(r2)     // Catch:{ Exception -> 0x0257 }
            r0 = 0
            a(r0)     // Catch:{ Exception -> 0x0257 }
            if (r1 == 0) goto L_0x028a
            r1.disconnect()     // Catch:{ Exception -> 0x0257 }
            r0 = r8
            goto L_0x0102
        L_0x014b:
            r3 = 2
            if (r0 != r3) goto L_0x028d
            boolean r0 = com.wooboo.adlib_android.c.d     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            if (r0 == 0) goto L_0x017f
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            byte[] r4 = com.wooboo.adlib_android.e.c     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            java.lang.String r5 = "utf-8"
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0114, all -> 0x018e }
        L_0x0160:
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            java.lang.String r3 = "X-Online-Host"
            java.lang.String r4 = "ade.wooboo.com.cn"
            r0.setRequestProperty(r3, r4)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            r3 = 12000(0x2ee0, float:1.6816E-41)
            r0.setConnectTimeout(r3)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            r3 = 12000(0x2ee0, float:1.6816E-41)
            r0.setReadTimeout(r3)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            goto L_0x00f2
        L_0x0179:
            r1 = move-exception
            r2 = r8
            r9 = r0
            r0 = r1
            r1 = r9
            goto L_0x0117
        L_0x017f:
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            byte[] r4 = com.wooboo.adlib_android.e.d     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            java.lang.String r5 = "utf-8"
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0114, all -> 0x018e }
            goto L_0x0160
        L_0x018e:
            r0 = move-exception
            r1 = r8
            r2 = r8
        L_0x0191:
            a(r2)     // Catch:{ Exception -> 0x0265 }
            r2 = 0
            a(r2)     // Catch:{ Exception -> 0x0265 }
            if (r1 == 0) goto L_0x019d
            r1.disconnect()     // Catch:{ Exception -> 0x0265 }
        L_0x019d:
            throw r0
        L_0x019e:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r1 = "Wooboo SDK 1.2"
            java.lang.String r2 = "Could not close stream"
            android.util.Log.e(r1, r2, r0)
            goto L_0x0101
        L_0x01ab:
            java.lang.String r3 = "POST"
            r0.setRequestMethod(r3)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            r3 = 1
            r0.setDoOutput(r3)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            java.lang.String r3 = "Content-Type"
            java.lang.String r4 = "application/x-www-form-urlencoded"
            r0.setRequestProperty(r3, r4)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            boolean r3 = com.wooboo.adlib_android.c.d     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            if (r3 == 0) goto L_0x0210
            java.lang.String r3 = "Content-Length"
            int r4 = r2.length()     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            r0.setRequestProperty(r3, r4)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
        L_0x01cc:
            r0.connect()     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            java.io.OutputStream r3 = r0.getOutputStream()     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            java.io.BufferedWriter r4 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            java.io.OutputStreamWriter r5 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            r5.<init>(r3)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            boolean r3 = com.wooboo.adlib_android.c.d     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            if (r3 == 0) goto L_0x0225
            r4.write(r2)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
        L_0x01e4:
            r4.close()     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x022d, all -> 0x0280 }
            r2.<init>()     // Catch:{ Exception -> 0x022d, all -> 0x0280 }
        L_0x01f0:
            int r3 = r1.read()     // Catch:{ Exception -> 0x022d, all -> 0x0280 }
            if (r3 != r6) goto L_0x0229
            byte[] r2 = r2.toByteArray()     // Catch:{ Exception -> 0x022d, all -> 0x0280 }
            int r3 = r2.length     // Catch:{ Exception -> 0x022d, all -> 0x0280 }
            if (r3 <= 0) goto L_0x0234
            com.wooboo.adlib_android.b r2 = com.wooboo.adlib_android.b.a(r10, r2)     // Catch:{ Exception -> 0x022d, all -> 0x0280 }
        L_0x0201:
            a(r1)     // Catch:{ Exception -> 0x0272 }
            r1 = 0
            a(r1)     // Catch:{ Exception -> 0x0272 }
            if (r0 == 0) goto L_0x027d
            r0.disconnect()     // Catch:{ Exception -> 0x0272 }
            r0 = r2
            goto L_0x0102
        L_0x0210:
            java.lang.String r3 = "Content-Length"
            int r4 = r1.length()     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            r0.setRequestProperty(r3, r4)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            goto L_0x01cc
        L_0x021e:
            r1 = move-exception
            r2 = r8
            r9 = r0
            r0 = r1
            r1 = r9
            goto L_0x0191
        L_0x0225:
            r4.write(r1)     // Catch:{ Exception -> 0x0179, all -> 0x021e }
            goto L_0x01e4
        L_0x0229:
            r2.write(r3)     // Catch:{ Exception -> 0x022d, all -> 0x0280 }
            goto L_0x01f0
        L_0x022d:
            r2 = move-exception
            r9 = r2
            r2 = r1
            r1 = r0
            r0 = r9
            goto L_0x0117
        L_0x0234:
            java.lang.String r2 = "Wooboo SDK 1.2"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x022d, all -> 0x0280 }
            java.lang.String r4 = "Could not get ad from Wooboo servers ("
            r3.<init>(r4)     // Catch:{ Exception -> 0x022d, all -> 0x0280 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x022d, all -> 0x0280 }
            long r6 = com.wooboo.adlib_android.c.b     // Catch:{ Exception -> 0x022d, all -> 0x0280 }
            long r4 = r4 - r6
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x022d, all -> 0x0280 }
            java.lang.String r4 = " ms);"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x022d, all -> 0x0280 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x022d, all -> 0x0280 }
            android.util.Log.w(r2, r3)     // Catch:{ Exception -> 0x022d, all -> 0x0280 }
            r2 = r8
            goto L_0x0201
        L_0x0257:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r1 = "Wooboo SDK 1.2"
            java.lang.String r2 = "Could not close stream"
            android.util.Log.e(r1, r2, r0)
            r0 = r8
            goto L_0x0102
        L_0x0265:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r2 = "Wooboo SDK 1.2"
            java.lang.String r3 = "Could not close stream"
            android.util.Log.e(r2, r3, r1)
            goto L_0x019d
        L_0x0272:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r1 = "Wooboo SDK 1.2"
            java.lang.String r3 = "Could not close stream"
            android.util.Log.e(r1, r3, r0)
        L_0x027d:
            r0 = r2
            goto L_0x0102
        L_0x0280:
            r2 = move-exception
            r9 = r2
            r2 = r1
            r1 = r0
            r0 = r9
            goto L_0x0191
        L_0x0287:
            r0 = move-exception
            goto L_0x0191
        L_0x028a:
            r0 = r8
            goto L_0x0102
        L_0x028d:
            r0 = r8
            goto L_0x00f2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wooboo.adlib_android.c.f(android.content.Context):com.wooboo.adlib_android.b");
    }

    private static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e2) {
                Log.e("Wooboo SDK 1.2", "Could not close stream", e2);
            }
        }
    }

    protected static void d(int i2) {
        m = i2;
    }

    protected static int g(Context context) {
        String extraInfo;
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == -1) {
            return 1;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable()) {
                return 0;
            }
            String typeName = activeNetworkInfo.getTypeName();
            if (typeName != null) {
                if (typeName.equalsIgnoreCase("WIFI")) {
                    return 1;
                }
                if (typeName.equalsIgnoreCase("MOBILE") && (extraInfo = activeNetworkInfo.getExtraInfo()) != null) {
                    if (extraInfo.contains("cmnet")) {
                        return 1;
                    }
                    if (extraInfo.contains("cmwap")) {
                        return 2;
                    }
                    if (extraInfo.toUpperCase().contains("CDMA")) {
                        return 1;
                    }
                    if (extraInfo.contains("777")) {
                        return 1;
                    }
                    if (extraInfo.contains("3gnet")) {
                        return 1;
                    }
                    if (extraInfo.contains("3gwap")) {
                        return 3;
                    }
                    if (extraInfo.contains("uniwap")) {
                        return 4;
                    }
                    if (extraInfo.contains("uninet")) {
                        return 1;
                    }
                    if (extraInfo.contains("internet")) {
                        return 1;
                    }
                }
            }
        }
        return 1;
    }

    private static void a(StringBuilder sb, String str, String str2) {
        if (str2 != null && str2.length() > 0) {
            try {
                sb.append("&").append(URLEncoder.encode(str, "UTF-8")).append("=").append(URLEncoder.encode(str2, "UTF-8"));
            } catch (UnsupportedEncodingException e2) {
                Log.e("Wooboo SDK 1.2", "UTF-8 encoding is not supported on this device.  Ad requests are impossible.", e2);
            }
        }
    }

    private static String f(String str) {
        int length = str.length();
        StringBuilder sb = new StringBuilder();
        if (length >= 6) {
            String substring = str.substring(4, 6);
            if (substring.endsWith("0") || substring.endsWith(Common.KIMP) || substring.endsWith("7")) {
                sb.append("on").append("=").append(5);
                if (length >= 7) {
                    String substring2 = str.substring(6, 7);
                    if (substring2.equals("0")) {
                        a(sb, "so", "0");
                    } else if (Common.KCLK.equals(substring2)) {
                        a(sb, "so", Common.KCLK);
                    } else if (Common.KIMP.equals(substring2)) {
                        a(sb, "so", Common.KIMP);
                    } else if ("3".equals(substring2)) {
                        a(sb, "so", "3");
                    } else if ("4".equals(substring2)) {
                        a(sb, "so", "4");
                    } else if ("5".equals(substring2)) {
                        a(sb, "so", "5");
                    } else if ("6".equals(substring2)) {
                        a(sb, "so", "6");
                    } else if ("7".equals(substring2)) {
                        a(sb, "so", "7");
                    } else if ("8".equals(substring2)) {
                        a(sb, "so", "8");
                    } else if ("9".equals(substring2)) {
                        a(sb, "so", "9");
                    } else if ("A".equals(substring2)) {
                        a(sb, "so", "10");
                    } else if ("B".equals(substring2)) {
                        a(sb, "so", "11");
                    } else if ("C".equals(substring2)) {
                        a(sb, "so", "12");
                    } else if ("D".equals(substring2)) {
                        a(sb, "so", "13");
                    } else if ("E".equals(substring2)) {
                        a(sb, "so", "14");
                    }
                    if (length >= 10) {
                        String substring3 = str.substring(8, 10);
                        if ("01".equals(substring3)) {
                            a(sb, "ac", "01");
                        } else if ("02".equals(substring3)) {
                            a(sb, "ac", "03");
                        } else if ("03".equals(substring3)) {
                            a(sb, "ac", "09");
                        } else if ("04".equals(substring3)) {
                            a(sb, "ac", "12");
                        } else if ("05".equals(substring3)) {
                            a(sb, "ac", "08");
                        } else if ("06".equals(substring3)) {
                            a(sb, "ac", "07");
                        } else if ("07".equals(substring3)) {
                            a(sb, "ac", "06");
                        } else if ("08".equals(substring3)) {
                            a(sb, "ac", "05");
                        } else if ("09".equals(substring3)) {
                            a(sb, "ac", "02");
                        } else if ("10".equals(substring3)) {
                            a(sb, "ac", "14");
                        } else if ("11".equals(substring3)) {
                            a(sb, "ac", "18");
                        } else if ("12".equals(substring3)) {
                            a(sb, "ac", "13");
                        } else if ("13".equals(substring3)) {
                            a(sb, "ac", "19");
                        } else if ("14".equals(substring3)) {
                            a(sb, "ac", "15");
                        } else if ("15".equals(substring3)) {
                            a(sb, "ac", "11");
                        } else if ("16".equals(substring3)) {
                            a(sb, "ac", "10");
                        } else if ("17".equals(substring3)) {
                            a(sb, "ac", "17");
                        } else if ("18".equals(substring3)) {
                            a(sb, "ac", "16");
                        } else if ("19".equals(substring3)) {
                            a(sb, "ac", "20");
                        } else if ("20".equals(substring3)) {
                            a(sb, "ac", "29");
                        } else if ("21".equals(substring3)) {
                            a(sb, "ac", "27");
                        } else if ("22".equals(substring3)) {
                            a(sb, "ac", "24");
                        } else if ("23".equals(substring3)) {
                            a(sb, "ac", "25");
                        } else if ("24".equals(substring3)) {
                            a(sb, "ac", "26");
                        } else if ("25".equals(substring3)) {
                            a(sb, "ac", "30");
                        } else if ("26".equals(substring3)) {
                            a(sb, "ac", "21");
                        } else if ("27".equals(substring3)) {
                            a(sb, "ac", "22");
                        } else if ("28".equals(substring3)) {
                            a(sb, "ac", "23");
                        } else if ("29".equals(substring3)) {
                            a(sb, "ac", "28");
                        } else if ("30".equals(substring3)) {
                            a(sb, "ac", "31");
                        } else if ("31".equals(substring3)) {
                            a(sb, "ac", "04");
                        }
                    }
                }
            } else if (substring.endsWith(Common.KCLK)) {
                sb.append("on").append("=").append(11);
                if (length >= 9) {
                    String substring4 = str.substring(8, 9);
                    if ("0".equals(substring4)) {
                        a(sb, "so", "24");
                    } else if (Common.KCLK.equals(substring4)) {
                        a(sb, "so", "15");
                    } else if (Common.KIMP.equals(substring4)) {
                        a(sb, "so", "16");
                    } else if ("5".equals(substring4)) {
                        a(sb, "so", "19");
                    } else if ("6".equals(substring4)) {
                        a(sb, "so", "20");
                    }
                    if (length >= 13) {
                        String substring5 = str.substring(10, 13);
                        if (substring5.equals("010")) {
                            a(sb, "ac", "01");
                        } else if (substring5.equals("022")) {
                            a(sb, "ac", "03");
                        } else if (substring5.startsWith("31") || substring5.startsWith("33")) {
                            a(sb, "ac", "09");
                        } else if (substring5.startsWith("35") || substring5.startsWith("34")) {
                            a(sb, "ac", "12");
                        } else if (substring5.startsWith("47") || substring5.startsWith("48")) {
                            a(sb, "ac", "08");
                        } else if (substring5.equals("024") || substring5.startsWith("41") || substring5.startsWith("42")) {
                            a(sb, "ac", "07");
                        } else if (substring5.startsWith("43")) {
                            a(sb, "ac", "06");
                        } else if (substring5.startsWith("45") || substring5.startsWith("46")) {
                            a(sb, "ac", "05");
                        } else if (substring5.equals("021")) {
                            a(sb, "ac", "02");
                        } else if (substring5.equals("025") || substring5.startsWith("51") || substring5.startsWith("52")) {
                            a(sb, "ac", "14");
                        } else if (substring5.startsWith("57")) {
                            a(sb, "ac", "18");
                        } else if (substring5.startsWith("55") || substring5.startsWith("56")) {
                            a(sb, "ac", "13");
                        } else if (substring5.startsWith("59")) {
                            a(sb, "ac", "19");
                        } else if (substring5.startsWith("79") || substring5.startsWith("70")) {
                            a(sb, "ac", "15");
                        } else if (substring5.startsWith("53") || substring5.startsWith("54") || substring5.startsWith("63")) {
                            a(sb, "ac", "11");
                        } else if (substring5.startsWith("37") || substring5.startsWith("39")) {
                            a(sb, "ac", "10");
                        } else if (substring5.equals("027") || substring5.startsWith("71") || substring5.startsWith("72")) {
                            a(sb, "ac", "17");
                        } else if (substring5.startsWith("73") || substring5.startsWith("74")) {
                            a(sb, "ac", "16");
                        } else if (substring5.equals("020") || substring5.startsWith("75") || substring5.startsWith("76") || substring5.startsWith("66")) {
                            a(sb, "ac", "20");
                        } else if (substring5.startsWith("77")) {
                            a(sb, "ac", "29");
                        } else if (substring5.equals("898")) {
                            a(sb, "ac", "27");
                        } else if (substring5.equals("028") || substring5.startsWith("81") || substring5.startsWith("82") || substring5.startsWith("83")) {
                            a(sb, "ac", "24");
                        } else if (substring5.startsWith("85")) {
                            a(sb, "ac", "25");
                        } else if (substring5.startsWith("87") || substring5.startsWith("88") || substring5.startsWith("69")) {
                            a(sb, "ac", "26");
                        } else if (substring5.startsWith("89")) {
                            a(sb, "ac", "30");
                        } else if (substring5.equals("029") || substring5.startsWith("91")) {
                            a(sb, "ac", "21");
                        } else if (substring5.startsWith("93") || substring5.startsWith("94")) {
                            a(sb, "ac", "22");
                        } else if (substring5.startsWith("97")) {
                            a(sb, "ac", "23");
                        } else if (substring5.startsWith("95")) {
                            a(sb, "ac", "28");
                        } else if (substring5.startsWith("90") || substring5.startsWith("99")) {
                            a(sb, "ac", "31");
                        } else if (substring5.equals("023")) {
                            a(sb, "ac", "04");
                        }
                    }
                }
            } else if (substring.endsWith("3")) {
                sb.append("on").append("=").append(12);
                if (length >= 9) {
                    String substring6 = str.substring(8, 9);
                    if ("3".equals(substring6)) {
                        a(sb, "so", "17");
                    } else if ("4".equals(substring6)) {
                        a(sb, "so", "18");
                    } else if ("7".equals(substring6)) {
                        a(sb, "so", "21");
                    } else if ("8".equals(substring6)) {
                        a(sb, "so", "22");
                    } else if ("9".equals(substring6)) {
                        a(sb, "so", "23");
                    }
                    if (length >= 13) {
                        String substring7 = str.substring(10, 13);
                        if ("010".equals(substring7)) {
                            a(sb, "ac", "01");
                        } else if ("022".equals(substring7)) {
                            a(sb, "ac", "03");
                        } else if (substring7.startsWith("31") || substring7.startsWith("33")) {
                            a(sb, "ac", "09");
                        } else if (substring7.startsWith("35") || substring7.startsWith("34")) {
                            a(sb, "ac", "12");
                        } else if (substring7.startsWith("47") || substring7.startsWith("48")) {
                            a(sb, "ac", "08");
                        } else if ("024".equals(substring7) || substring7.startsWith("41") || substring7.startsWith("42")) {
                            a(sb, "ac", "07");
                        } else if (substring7.startsWith("43")) {
                            a(sb, "ac", "06");
                        } else if (substring7.startsWith("45") || substring7.startsWith("46")) {
                            a(sb, "ac", "05");
                        } else if ("021".equals(substring7)) {
                            a(sb, "ac", "02");
                        } else if (substring7.equals("025") || substring7.startsWith("51") || substring7.startsWith("52")) {
                            a(sb, "ac", "14");
                        } else if (substring7.startsWith("57") || substring7.startsWith("58")) {
                            a(sb, "ac", "18");
                        } else if (substring7.startsWith("55") || substring7.startsWith("56")) {
                            a(sb, "ac", "13");
                        } else if (substring7.startsWith("59")) {
                            a(sb, "ac", "19");
                        } else if (substring7.startsWith("79") || substring7.startsWith("70")) {
                            a(sb, "ac", "15");
                        } else if (substring7.startsWith("53") || substring7.startsWith("54") || substring7.startsWith("63")) {
                            a(sb, "ac", "11");
                        } else if (substring7.startsWith("37") || substring7.startsWith("39")) {
                            a(sb, "ac", "10");
                        } else if ("027".equals(substring7) || substring7.startsWith("71") || substring7.startsWith("72")) {
                            a(sb, "ac", "17");
                        } else if (substring7.startsWith("73") || substring7.startsWith("74")) {
                            a(sb, "ac", "16");
                        } else if ("020".equals(substring7) || substring7.startsWith("75") || substring7.startsWith("76") || substring7.startsWith("66")) {
                            a(sb, "ac", "20");
                        } else if (substring7.startsWith("77")) {
                            a(sb, "ac", "29");
                        } else if ("898".equals(substring7)) {
                            a(sb, "ac", "27");
                        } else if ("028".equals(substring7) || substring7.startsWith("81") || substring7.startsWith("82") || substring7.startsWith("83")) {
                            a(sb, "ac", "24");
                        } else if (substring7.startsWith("85")) {
                            a(sb, "ac", "25");
                        } else if (substring7.startsWith("87") || substring7.startsWith("88") || substring7.startsWith("69")) {
                            a(sb, "ac", "26");
                        } else if (substring7.startsWith("89")) {
                            a(sb, "ac", "30");
                        } else if ("029".equals(substring7) || substring7.startsWith("91")) {
                            a(sb, "ac", "21");
                        } else if (substring7.startsWith("93") || substring7.startsWith("94")) {
                            a(sb, "ac", "22");
                        } else if (substring7.startsWith("97")) {
                            a(sb, "ac", "23");
                        } else if (substring7.startsWith("95")) {
                            a(sb, "ac", "28");
                        } else if (substring7.startsWith("90") || substring7.startsWith("99")) {
                            a(sb, "ac", "31");
                        } else if ("023".equals(substring7)) {
                            a(sb, "ac", "04");
                        }
                    }
                }
            }
        }
        return sb.toString();
    }
}
