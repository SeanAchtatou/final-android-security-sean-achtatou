package com.wooboo.adlib_android;

import android.content.Context;
import android.util.Log;
import java.util.ArrayList;

final class b {
    /* access modifiers changed from: private */
    public static int a = 5000;
    /* access modifiers changed from: private */
    public Context b;
    private String c = null;
    /* access modifiers changed from: private */
    public String d = null;
    private String e = null;
    /* access modifiers changed from: private */
    public byte f;
    /* access modifiers changed from: private */
    public a g;

    interface a {
        void a();

        void b();
    }

    public static b a(Context context, byte[] bArr) {
        b bVar = new b();
        bVar.b = context;
        ArrayList<Object> a2 = h.a(bArr);
        if (a2 != null) {
            bVar.c = (String) a2.get(0);
            bVar.c = bVar.c;
            bVar.e = (String) a2.get(1);
            bVar.d = (String) a2.get(2);
            bVar.f = ((Byte) a2.get(3)).byteValue();
            if (bVar.c.length() == 0 && bVar.e.length() == 0) {
                Log.w("Wooboo SDK 1.2", "Could not get ad from Wooboo servers (" + (System.currentTimeMillis() - c.b) + " ms);");
                return null;
            }
            Log.d("Wooboo SDK 1.2", "Get an ad from Wooboo servers (" + (System.currentTimeMillis() - c.b) + " ms);");
            return bVar;
        }
        Log.w("Wooboo SDK 1.2", "Could not get ad from Wooboo servers (" + (System.currentTimeMillis() - c.b) + " ms);");
        return null;
    }

    private b() {
    }

    public final void a() {
        if (this.d != null) {
            new Thread() {
                /* JADX WARNING: Removed duplicated region for block: B:13:0x004e A[SYNTHETIC, Splitter:B:13:0x004e] */
                /* JADX WARNING: Removed duplicated region for block: B:155:? A[RETURN, SYNTHETIC] */
                /* JADX WARNING: Removed duplicated region for block: B:16:0x0054  */
                /* JADX WARNING: Removed duplicated region for block: B:24:0x0074  */
                /* JADX WARNING: Removed duplicated region for block: B:39:0x00f2 A[SYNTHETIC, Splitter:B:39:0x00f2] */
                /* JADX WARNING: Removed duplicated region for block: B:71:0x0159 A[SYNTHETIC, Splitter:B:71:0x0159] */
                /* JADX WARNING: Removed duplicated region for block: B:74:0x015e A[Catch:{ Exception -> 0x0162 }] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final void run() {
                    /*
                        r11 = this;
                        r3 = 2
                        r9 = 0
                        r8 = 1
                        r7 = 0
                        com.wooboo.adlib_android.b r0 = com.wooboo.adlib_android.b.this
                        byte r1 = r0.f
                        com.wooboo.adlib_android.b r0 = com.wooboo.adlib_android.b.this     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        com.wooboo.adlib_android.b$a r0 = r0.g     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        if (r0 == 0) goto L_0x001b
                        com.wooboo.adlib_android.b r0 = com.wooboo.adlib_android.b.this     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        com.wooboo.adlib_android.b$a r0 = r0.g     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        r0.a()     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                    L_0x001b:
                        java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        com.wooboo.adlib_android.b r2 = com.wooboo.adlib_android.b.this     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        java.lang.String r2 = r2.d     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        r0.<init>(r2)     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        int r2 = com.wooboo.adlib_android.c.a     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        if (r2 != r8) goto L_0x007e
                        java.net.URLConnection r0 = r0.openConnection()     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        r2 = 1
                        java.net.HttpURLConnection.setFollowRedirects(r2)     // Catch:{ MalformedURLException -> 0x00ab, IOException -> 0x031c }
                        int r2 = com.wooboo.adlib_android.b.a     // Catch:{ MalformedURLException -> 0x00ab, IOException -> 0x031c }
                        r0.setConnectTimeout(r2)     // Catch:{ MalformedURLException -> 0x00ab, IOException -> 0x031c }
                        int r2 = com.wooboo.adlib_android.b.a     // Catch:{ MalformedURLException -> 0x00ab, IOException -> 0x031c }
                        r0.setReadTimeout(r2)     // Catch:{ MalformedURLException -> 0x00ab, IOException -> 0x031c }
                    L_0x0042:
                        r0.connect()     // Catch:{ MalformedURLException -> 0x00ab, IOException -> 0x031c }
                        r0.getResponseCode()     // Catch:{ MalformedURLException -> 0x00ab, IOException -> 0x031c }
                        java.net.URL r2 = r0.getURL()     // Catch:{ MalformedURLException -> 0x00ab, IOException -> 0x031c }
                    L_0x004c:
                        if (r1 != r8) goto L_0x00f2
                        java.lang.String r0 = r2.toString()     // Catch:{ NullPointerException -> 0x00e7 }
                    L_0x0052:
                        if (r0 == 0) goto L_0x006c
                        android.content.Intent r2 = new android.content.Intent
                        r2.<init>()
                        switch(r1) {
                            case 1: goto L_0x0190;
                            case 2: goto L_0x0190;
                            case 3: goto L_0x01a8;
                            case 4: goto L_0x01a8;
                            case 5: goto L_0x01a8;
                            case 6: goto L_0x01a8;
                            case 7: goto L_0x01a8;
                            case 8: goto L_0x01c4;
                            case 9: goto L_0x01ed;
                            case 10: goto L_0x024b;
                            case 11: goto L_0x01a8;
                            case 12: goto L_0x0264;
                            case 13: goto L_0x02a8;
                            default: goto L_0x005c;
                        }
                    L_0x005c:
                        r1 = r0
                        r0 = r2
                    L_0x005e:
                        r2 = 268435456(0x10000000, float:2.5243549E-29)
                        r0.addFlags(r2)
                        com.wooboo.adlib_android.b r2 = com.wooboo.adlib_android.b.this     // Catch:{ Exception -> 0x02f0 }
                        android.content.Context r2 = r2.b     // Catch:{ Exception -> 0x02f0 }
                        r2.startActivity(r0)     // Catch:{ Exception -> 0x02f0 }
                    L_0x006c:
                        com.wooboo.adlib_android.b r0 = com.wooboo.adlib_android.b.this
                        com.wooboo.adlib_android.b$a r0 = r0.g
                        if (r0 == 0) goto L_0x007d
                        com.wooboo.adlib_android.b r0 = com.wooboo.adlib_android.b.this
                        com.wooboo.adlib_android.b$a r0 = r0.g
                        r0.b()
                    L_0x007d:
                        return
                    L_0x007e:
                        int r2 = com.wooboo.adlib_android.c.a     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        if (r2 != r3) goto L_0x0331
                        java.net.Proxy r2 = new java.net.Proxy     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        java.net.Proxy$Type r3 = java.net.Proxy.Type.HTTP     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        java.net.InetSocketAddress r4 = new java.net.InetSocketAddress     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        java.lang.String r5 = "10.0.0.172"
                        r6 = 80
                        r4.<init>(r5, r6)     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        r2.<init>(r3, r4)     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        java.net.URLConnection r0 = r0.openConnection(r2)     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x031f, IOException -> 0x00c8 }
                        r2 = 1
                        java.net.HttpURLConnection.setFollowRedirects(r2)     // Catch:{ MalformedURLException -> 0x00ab, IOException -> 0x031c }
                        int r2 = com.wooboo.adlib_android.b.a     // Catch:{ MalformedURLException -> 0x00ab, IOException -> 0x031c }
                        r0.setConnectTimeout(r2)     // Catch:{ MalformedURLException -> 0x00ab, IOException -> 0x031c }
                        int r2 = com.wooboo.adlib_android.b.a     // Catch:{ MalformedURLException -> 0x00ab, IOException -> 0x031c }
                        r0.setReadTimeout(r2)     // Catch:{ MalformedURLException -> 0x00ab, IOException -> 0x031c }
                        goto L_0x0042
                    L_0x00ab:
                        r2 = move-exception
                    L_0x00ac:
                        java.lang.String r2 = "Wooboo SDK 1.2"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder
                        java.lang.String r4 = "Malformed click URL.  Will try to follow anyway."
                        r3.<init>(r4)
                        com.wooboo.adlib_android.b r4 = com.wooboo.adlib_android.b.this
                        java.lang.String r4 = r4.d
                        java.lang.StringBuilder r3 = r3.append(r4)
                        java.lang.String r3 = r3.toString()
                        android.util.Log.w(r2, r3)
                        r2 = r7
                        goto L_0x004c
                    L_0x00c8:
                        r0 = move-exception
                        r0 = r7
                    L_0x00ca:
                        java.lang.String r2 = "Wooboo SDK 1.2"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder
                        java.lang.String r4 = "Could not determine final click destination URL.  Will try to follow anyway.  "
                        r3.<init>(r4)
                        com.wooboo.adlib_android.b r4 = com.wooboo.adlib_android.b.this
                        java.lang.String r4 = r4.d
                        java.lang.StringBuilder r3 = r3.append(r4)
                        java.lang.String r3 = r3.toString()
                        android.util.Log.w(r2, r3)
                        r2 = r7
                        goto L_0x004c
                    L_0x00e7:
                        r0 = move-exception
                        java.lang.String r0 = "Wooboo SDK 1.2"
                        java.lang.String r2 = "Could not get ad click url from Telead server."
                        android.util.Log.e(r0, r2)
                        r0 = r7
                        goto L_0x0052
                    L_0x00f2:
                        java.io.InputStream r2 = r0.getInputStream()     // Catch:{ IOException -> 0x0119, all -> 0x0155 }
                        if (r2 == 0) goto L_0x032e
                        int r3 = r2.available()     // Catch:{ IOException -> 0x0319 }
                        byte[] r4 = new byte[r3]     // Catch:{ IOException -> 0x0319 }
                        r5 = r9
                    L_0x00ff:
                        if (r5 < r3) goto L_0x0113
                        java.lang.String r3 = new java.lang.String     // Catch:{ IOException -> 0x0319 }
                        r3.<init>(r4)     // Catch:{ IOException -> 0x0319 }
                    L_0x0106:
                        if (r2 == 0) goto L_0x010b
                        r2.close()     // Catch:{ Exception -> 0x0178 }
                    L_0x010b:
                        if (r0 == 0) goto L_0x018d
                        r0.disconnect()     // Catch:{ Exception -> 0x0178 }
                        r0 = r3
                        goto L_0x0052
                    L_0x0113:
                        r2.read(r4)     // Catch:{ IOException -> 0x0319 }
                        int r5 = r5 + 1
                        goto L_0x00ff
                    L_0x0119:
                        r2 = move-exception
                        r2 = r7
                    L_0x011b:
                        java.lang.String r3 = "Wooboo SDK 1.2"
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0316 }
                        java.lang.String r5 = "Connection off "
                        r4.<init>(r5)     // Catch:{ all -> 0x0316 }
                        r5 = 0
                        java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0316 }
                        java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0316 }
                        android.util.Log.e(r3, r4)     // Catch:{ all -> 0x0316 }
                        if (r2 == 0) goto L_0x0135
                        r2.close()     // Catch:{ Exception -> 0x013d }
                    L_0x0135:
                        if (r0 == 0) goto L_0x032b
                        r0.disconnect()     // Catch:{ Exception -> 0x013d }
                        r0 = r7
                        goto L_0x0052
                    L_0x013d:
                        r0 = move-exception
                        java.lang.String r0 = "Wooboo SDK 1.2"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        java.lang.String r3 = "Could not close stream"
                        r2.<init>(r3)
                        java.lang.StringBuilder r2 = r2.append(r7)
                        java.lang.String r2 = r2.toString()
                        android.util.Log.e(r0, r2)
                        r0 = r7
                        goto L_0x0052
                    L_0x0155:
                        r1 = move-exception
                        r2 = r7
                    L_0x0157:
                        if (r2 == 0) goto L_0x015c
                        r2.close()     // Catch:{ Exception -> 0x0162 }
                    L_0x015c:
                        if (r0 == 0) goto L_0x0161
                        r0.disconnect()     // Catch:{ Exception -> 0x0162 }
                    L_0x0161:
                        throw r1
                    L_0x0162:
                        r0 = move-exception
                        java.lang.String r0 = "Wooboo SDK 1.2"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        java.lang.String r3 = "Could not close stream"
                        r2.<init>(r3)
                        java.lang.StringBuilder r2 = r2.append(r7)
                        java.lang.String r2 = r2.toString()
                        android.util.Log.e(r0, r2)
                        goto L_0x0161
                    L_0x0178:
                        r0 = move-exception
                        java.lang.String r0 = "Wooboo SDK 1.2"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        java.lang.String r4 = "Could not close stream"
                        r2.<init>(r4)
                        java.lang.StringBuilder r2 = r2.append(r3)
                        java.lang.String r2 = r2.toString()
                        android.util.Log.e(r0, r2)
                    L_0x018d:
                        r0 = r3
                        goto L_0x0052
                    L_0x0190:
                        java.lang.String r1 = "android.intent.action.VIEW"
                        r2.setAction(r1)     // Catch:{ NullPointerException -> 0x01a0 }
                        android.net.Uri r1 = android.net.Uri.parse(r0)     // Catch:{ NullPointerException -> 0x01a0 }
                        r2.setData(r1)     // Catch:{ NullPointerException -> 0x01a0 }
                        r1 = r0
                        r0 = r2
                        goto L_0x005e
                    L_0x01a0:
                        r1 = move-exception
                        r1.printStackTrace()
                        r1 = r0
                        r0 = r2
                        goto L_0x005e
                    L_0x01a8:
                        java.lang.String r1 = r0.trim()     // Catch:{ NullPointerException -> 0x01bc }
                        java.lang.String r3 = "android.intent.action.VIEW"
                        r2.setAction(r3)     // Catch:{ NullPointerException -> 0x01bc }
                        android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ NullPointerException -> 0x01bc }
                        r2.setData(r1)     // Catch:{ NullPointerException -> 0x01bc }
                        r1 = r0
                        r0 = r2
                        goto L_0x005e
                    L_0x01bc:
                        r1 = move-exception
                        r1.printStackTrace()
                        r1 = r0
                        r0 = r2
                        goto L_0x005e
                    L_0x01c4:
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01e3 }
                        java.lang.String r3 = "tel:"
                        r1.<init>(r3)     // Catch:{ Exception -> 0x01e3 }
                        java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ Exception -> 0x01e3 }
                        java.lang.String r0 = r1.toString()     // Catch:{ Exception -> 0x01e3 }
                        java.lang.String r1 = "android.intent.action.DIAL"
                        r2.setAction(r1)     // Catch:{ Exception -> 0x0310 }
                        android.net.Uri r1 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x0310 }
                        r2.setData(r1)     // Catch:{ Exception -> 0x0310 }
                        r1 = r0
                        r0 = r2
                        goto L_0x005e
                    L_0x01e3:
                        r1 = move-exception
                        r10 = r1
                        r1 = r0
                        r0 = r10
                    L_0x01e7:
                        r0.printStackTrace()
                        r0 = r2
                        goto L_0x005e
                    L_0x01ed:
                        java.lang.String r1 = "|"
                        boolean r1 = r0.contains(r1)
                        if (r1 == 0) goto L_0x023e
                        java.lang.String r1 = "|"
                        int r1 = r0.indexOf(r1)
                        java.lang.String r3 = r0.substring(r9, r1)
                        int r1 = r1 + 1
                        java.lang.String r1 = r0.substring(r1)
                        r10 = r3
                        r3 = r1
                        r1 = r10
                    L_0x0208:
                        java.lang.String r4 = "android.intent.action.VIEW"
                        r2.setAction(r4)     // Catch:{ Exception -> 0x0243 }
                        java.lang.String r4 = "com.google.android.apps.maps"
                        java.lang.String r5 = "com.google.android.maps.MapsActivity"
                        r2.setClassName(r4, r5)     // Catch:{ Exception -> 0x0243 }
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0243 }
                        java.lang.String r5 = "http://maps.google.com/maps?q="
                        r4.<init>(r5)     // Catch:{ Exception -> 0x0243 }
                        java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x0243 }
                        java.lang.String r4 = "("
                        java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0243 }
                        java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0243 }
                        java.lang.String r3 = ")&z=22"
                        java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0243 }
                        java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0243 }
                        android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x0243 }
                        r2.setData(r1)     // Catch:{ Exception -> 0x0243 }
                        r1 = r0
                        r0 = r2
                        goto L_0x005e
                    L_0x023e:
                        java.lang.String r1 = "I am here"
                        r3 = r1
                        r1 = r0
                        goto L_0x0208
                    L_0x0243:
                        r1 = move-exception
                        r1.printStackTrace()
                        r1 = r0
                        r0 = r2
                        goto L_0x005e
                    L_0x024b:
                        android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x025c }
                        java.lang.String r3 = "android.intent.action.WEB_SEARCH"
                        r1.<init>(r3)     // Catch:{ Exception -> 0x025c }
                        java.lang.String r2 = "query"
                        r1.putExtra(r2, r0)     // Catch:{ Exception -> 0x030a }
                        r10 = r1
                        r1 = r0
                        r0 = r10
                        goto L_0x005e
                    L_0x025c:
                        r1 = move-exception
                    L_0x025d:
                        r1.printStackTrace()
                        r1 = r0
                        r0 = r2
                        goto L_0x005e
                    L_0x0264:
                        java.lang.String r1 = "|"
                        boolean r1 = r0.contains(r1)
                        if (r1 == 0) goto L_0x0327
                        java.lang.String r1 = "|"
                        int r1 = r0.indexOf(r1)
                        java.lang.String r3 = r0.substring(r9, r1)
                        int r1 = r1 + 1
                        java.lang.String r1 = r0.substring(r1)
                    L_0x027c:
                        java.lang.String r4 = "android.intent.action.SENDTO"
                        r2.setAction(r4)     // Catch:{ Exception -> 0x02a0 }
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a0 }
                        java.lang.String r5 = "smsto:"
                        r4.<init>(r5)     // Catch:{ Exception -> 0x02a0 }
                        java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x02a0 }
                        java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x02a0 }
                        android.net.Uri r3 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x02a0 }
                        r2.setData(r3)     // Catch:{ Exception -> 0x02a0 }
                        java.lang.String r3 = "sms_body"
                        r2.putExtra(r3, r1)     // Catch:{ Exception -> 0x02a0 }
                        r1 = r0
                        r0 = r2
                        goto L_0x005e
                    L_0x02a0:
                        r1 = move-exception
                        r1.printStackTrace()
                        r1 = r0
                        r0 = r2
                        goto L_0x005e
                    L_0x02a8:
                        java.lang.String r1 = "android.intent.action.SEND"
                        r2.setAction(r1)     // Catch:{ Exception -> 0x02ea }
                        java.lang.String r1 = "|"
                        boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x02ea }
                        if (r1 == 0) goto L_0x0323
                        java.lang.String[] r1 = com.wooboo.adlib_android.b.a(r0)     // Catch:{ Exception -> 0x02ea }
                        r3 = 0
                        r3 = r1[r3]     // Catch:{ Exception -> 0x02ea }
                        r4 = 1
                        r4 = r1[r4]     // Catch:{ Exception -> 0x02ea }
                        r5 = 2
                        r1 = r1[r5]     // Catch:{ Exception -> 0x02ea }
                        r10 = r4
                        r4 = r1
                        r1 = r10
                    L_0x02c5:
                        android.net.Uri r5 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x02ea }
                        r2.setData(r5)     // Catch:{ Exception -> 0x02ea }
                        r5 = 1
                        java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Exception -> 0x02ea }
                        r6 = 0
                        r5[r6] = r3     // Catch:{ Exception -> 0x02ea }
                        java.lang.String r3 = "android.intent.extra.EMAIL"
                        r2.putExtra(r3, r5)     // Catch:{ Exception -> 0x02ea }
                        java.lang.String r3 = "android.intent.extra.TEXT"
                        r2.putExtra(r3, r4)     // Catch:{ Exception -> 0x02ea }
                        java.lang.String r3 = "android.intent.extra.SUBJECT"
                        r2.putExtra(r3, r1)     // Catch:{ Exception -> 0x02ea }
                        java.lang.String r1 = "message/rfc882"
                        r2.setType(r1)     // Catch:{ Exception -> 0x02ea }
                        r1 = r0
                        r0 = r2
                        goto L_0x005e
                    L_0x02ea:
                        r1 = move-exception
                        r1.printStackTrace()
                        goto L_0x005c
                    L_0x02f0:
                        r0 = move-exception
                        r0.printStackTrace()
                        java.lang.String r2 = "Wooboo SDK 1.2"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder
                        java.lang.String r4 = "Could not intent to "
                        r3.<init>(r4)
                        java.lang.StringBuilder r1 = r3.append(r1)
                        java.lang.String r1 = r1.toString()
                        android.util.Log.e(r2, r1, r0)
                        goto L_0x006c
                    L_0x030a:
                        r2 = move-exception
                        r10 = r2
                        r2 = r1
                        r1 = r10
                        goto L_0x025d
                    L_0x0310:
                        r1 = move-exception
                        r10 = r1
                        r1 = r0
                        r0 = r10
                        goto L_0x01e7
                    L_0x0316:
                        r1 = move-exception
                        goto L_0x0157
                    L_0x0319:
                        r3 = move-exception
                        goto L_0x011b
                    L_0x031c:
                        r2 = move-exception
                        goto L_0x00ca
                    L_0x031f:
                        r0 = move-exception
                        r0 = r7
                        goto L_0x00ac
                    L_0x0323:
                        r1 = r7
                        r3 = r7
                        r4 = r7
                        goto L_0x02c5
                    L_0x0327:
                        r1 = r7
                        r3 = r7
                        goto L_0x027c
                    L_0x032b:
                        r0 = r7
                        goto L_0x0052
                    L_0x032e:
                        r3 = r7
                        goto L_0x0106
                    L_0x0331:
                        r0 = r7
                        goto L_0x0042
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.wooboo.adlib_android.b.AnonymousClass1.run():void");
                }
            }.start();
            ImpressionAdView.close();
        }
    }

    static /* synthetic */ String[] a(String str) {
        int indexOf = str.indexOf("|");
        String substring = str.substring(indexOf + 1);
        int indexOf2 = substring.indexOf("|");
        return new String[]{str.substring(0, indexOf), substring.substring(0, indexOf2), substring.substring(indexOf2 + 1)};
    }

    public final void a(a aVar) {
        this.g = aVar;
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.e;
    }

    public final String toString() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof b) {
            return toString().equals(((b) obj).toString());
        }
        return false;
    }

    public final int hashCode() {
        return toString().hashCode();
    }
}
