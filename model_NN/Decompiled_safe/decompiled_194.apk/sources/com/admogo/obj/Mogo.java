package com.admogo.obj;

import android.graphics.drawable.Drawable;

public class Mogo {
    public String description;
    public Drawable image;
    public String imageLink;
    public String link;
    public int type;
}
