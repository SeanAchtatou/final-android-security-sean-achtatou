package com.admogo;

public interface AdMogoListener {
    void onClickAd();

    void onFailedReceiveAd();

    void onReceiveAd();
}
