package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.ViewGroup;
import cn.domob.android.ads.DomobAdListener;
import cn.domob.android.ads.DomobAdManager;
import cn.domob.android.ads.DomobAdView;
import com.admogo.AdMogoLayout;
import com.admogo.AdMogoTargeting;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.madhouse.android.ads.AdManager;
import java.util.GregorianCalendar;

public class DomobAdapter extends AdMogoAdapter implements DomobAdListener {
    private Activity activity;
    private DomobAdView adView;

    public DomobAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                this.adView = new DomobAdView(this.activity);
                Extra extra = adMogoLayout.extra;
                int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
                int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
                try {
                    DomobAdManager.setPublisherId(this.ration.key);
                    this.adView.setAdListener(this);
                    AdMogoTargeting.Gender gender = AdMogoTargeting.getGender();
                    if (gender == AdMogoTargeting.Gender.FEMALE) {
                        DomobAdManager.setGender(AdManager.USER_GENDER_FEMALE);
                    } else if (gender == AdMogoTargeting.Gender.MALE) {
                        DomobAdManager.setGender(AdManager.USER_GENDER_MALE);
                    }
                    GregorianCalendar birthDate = AdMogoTargeting.getBirthDate();
                    if (birthDate != null) {
                        DomobAdManager.setBirthday(birthDate);
                    }
                    String postalCode = AdMogoTargeting.getPostalCode();
                    if (!TextUtils.isEmpty(postalCode)) {
                        DomobAdManager.setPostalCode(postalCode);
                    }
                    DomobAdManager.setAllowUseOfLocation(extra.locationOn == 1);
                    this.adView.setBackgroundColor(bgColor);
                    this.adView.setPrimaryTextColor(fgColor);
                    this.adView.setKeywords(this.ration.key2);
                    DomobAdManager.setIsTestMode(this.ration.testmodel);
                    adMogoLayout.addView(this.adView, new ViewGroup.LayoutParams(-2, -2));
                    this.adView.requestFreshAd();
                } catch (IllegalArgumentException e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void onFailedToReceiveFreshAd(DomobAdView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "Domob Failed");
        adView2.setAdListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void onReceivedFreshAd(DomobAdView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "Domob Success");
        adView2.setAdListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView2, 29));
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Domob Finished");
    }
}
