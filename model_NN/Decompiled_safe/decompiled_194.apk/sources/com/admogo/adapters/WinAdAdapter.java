package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.ViewGroup;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.winad.android.ads.ADListener;
import com.winad.android.ads.AdView;

public class WinAdAdapter extends AdMogoAdapter implements ADListener {
    private Activity activity;
    private AdView adView;

    public WinAdAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                Extra extra = new Extra();
                int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
                int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
                try {
                    this.adView = new AdView(this.activity, 255, bgColor, fgColor, 600, true);
                    this.adView.setBackgroundColor(bgColor);
                    this.adView.setTextColor(fgColor);
                    this.adView.setPublishId(this.ration.key);
                    this.adView.setRequestInterval(600);
                    this.adView.setListener(this);
                    this.adView.requestFreshAd();
                    this.adView.setTestMode(this.ration.testmodel);
                    adMogoLayout.addView(this.adView, new ViewGroup.LayoutParams(-1, -2));
                } catch (Exception e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void onFailedToReceiveAd(AdView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "WinAd failed");
        adView2.setListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void onReceiveAd(AdView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "WinAd success");
        adView2.setListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView2, 37));
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "WinAd finished");
    }
}
