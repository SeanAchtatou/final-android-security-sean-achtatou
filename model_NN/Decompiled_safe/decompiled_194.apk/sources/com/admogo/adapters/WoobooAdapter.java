package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.wooboo.adlib_android.AdListener;
import com.wooboo.adlib_android.WoobooAdView;

public class WoobooAdapter extends AdMogoAdapter implements AdListener {
    private Activity activity;
    private WoobooAdView adView;

    public WoobooAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                Extra extra = adMogoLayout.extra;
                try {
                    this.adView = new WoobooAdView(this.activity, this.ration.key, Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue), Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue), this.ration.testmodel, 600);
                    this.adView.setAdListener(this);
                    adMogoLayout.addView(this.adView);
                } catch (IllegalArgumentException e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void onFailedToReceiveAd(Object arg0) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "WooBoo failure");
        this.adView.setAdListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void onReceiveAd(Object arg0) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "WooBoo success");
        this.adView.setAdListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, this.adView, 23));
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Wooboo Finished");
    }
}
