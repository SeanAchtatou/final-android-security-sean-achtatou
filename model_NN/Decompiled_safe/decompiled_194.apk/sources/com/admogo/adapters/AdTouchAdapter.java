package com.admogo.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.ViewGroup;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.energysource.szj.embeded.AdListener;
import com.energysource.szj.embeded.AdManager;
import com.energysource.szj.embeded.AdView;

public class AdTouchAdapter extends AdMogoAdapter implements AdListener {
    private Activity activity;
    private AdView adView;

    public AdTouchAdapter(AdMogoLayout adMogolLayout, Ration ration) {
        super(adMogolLayout, ration);
    }

    public void handle() {
        AdMogoLayout adMogolLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogolLayout != null) {
            this.activity = adMogolLayout.activityReference.get();
            if (this.activity != null) {
                try {
                    AdManager.initAd(this.activity, this.activity.getPackageName());
                    this.adView = new AdView(this.activity, 0);
                    AdManager.openPermissionJudge();
                    AdManager.addAd(102, AdManager.AD_FILL_PARENT, 81, 0, 0);
                    AdManager.setAdListener(this);
                    AdManager.openAllAdView();
                    adMogolLayout.addView(this.adView, new ViewGroup.LayoutParams(-1, -2));
                } catch (IllegalArgumentException e) {
                    adMogolLayout.rollover();
                }
            }
        }
    }

    public void failedReceiveAd(AdView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "AdTouch failure");
        AdManager.setAdListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void receiveAd(AdView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "AdTouch success");
        AdManager.setAdListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView2, 28));
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "AdTouch Finished");
    }
}
