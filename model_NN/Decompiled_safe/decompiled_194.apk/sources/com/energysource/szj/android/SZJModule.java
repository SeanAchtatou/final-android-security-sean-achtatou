package com.energysource.szj.android;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public interface SZJModule {
    void destory();

    HashMap getSdkconfig();

    HashMap getSizeNO();

    void initValue(ConcurrentHashMap concurrentHashMap);

    AdvObject requestAd(int i, int i2);

    void saveClickOP(String str, int i);

    void saveShowNum(long j, long j2, String str, int i);

    void start();
}
