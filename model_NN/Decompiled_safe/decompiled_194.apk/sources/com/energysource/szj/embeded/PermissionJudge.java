package com.energysource.szj.embeded;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.util.SparseArray;
import com.energysource.szj.android.Log;

public class PermissionJudge {
    public static final int ACCESS_NETWORK_STATE = 1;
    public static final int ACCESS_WIFI_STATE = 2;
    public static final int CHANGE_WIFI_STATE = 4;
    public static final int INTERNET = 3;
    public static final int READ_PHONE_STATE = 0;
    private static final String TAG = "==PermissionJudge==";
    static final Handler adHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.i("===检查权限配置=====", "============");
            PermissionJudge.checkPermission(PermissionJudge.cxt);
        }
    };
    /* access modifiers changed from: private */
    public static Context cxt = null;
    private static SparseArray permisArray = new SparseArray();

    static {
        permisArray.append(0, new String[]{"android.permission.READ_PHONE_STATE", "获取手机设备信息，用来标识唯一用户"});
        permisArray.append(1, new String[]{"android.permission.ACCESS_NETWORK_STATE", "获取网络状态"});
        permisArray.append(2, new String[]{"android.permission.ACCESS_WIFI_STATE", "获取WIFI状态"});
        permisArray.append(3, new String[]{"android.permission.INTERNET", "上网获取广告"});
    }

    public void openSwitchFlag() {
        new Thread(new Runnable() {
            public void run() {
                PermissionJudge.adHandler.sendMessage(PermissionJudge.adHandler.obtainMessage());
            }
        }).start();
    }

    public void setContext(Context context) {
        cxt = context;
    }

    /* access modifiers changed from: private */
    public static void checkPermission(Context cxt2) {
        StringBuffer echo = new StringBuffer();
        boolean isexit = true;
        for (int i = 0; i < permisArray.size(); i++) {
            String[] perStr = (String[]) permisArray.get(i);
            try {
                if (cxt2.checkCallingOrSelfPermission(perStr[0]) == -1) {
                    Log.d(TAG, "没有配置以下权限,应用将关闭:[" + perStr[1] + "]" + perStr[0]);
                    echo.append("[" + perStr[1] + "]:" + perStr[0] + ",\n\n");
                    if (isexit) {
                        isexit = false;
                    }
                } else {
                    Log.d(TAG, "=可以正常访问==:[" + perStr[1] + "]" + perStr[0]);
                }
            } catch (Exception e) {
            }
        }
        if (!isexit) {
            new AlertDialog.Builder(cxt2).setTitle("没有配置以下权限,应用将关闭").setCancelable(false).setMessage(echo.toString()).setPositiveButton("确认", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialoginterface, int i) {
                    Process.killProcess(Process.myPid());
                }
            }).show();
        }
    }
}
