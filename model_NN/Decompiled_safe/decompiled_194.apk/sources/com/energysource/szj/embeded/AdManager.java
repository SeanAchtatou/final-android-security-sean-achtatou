package com.energysource.szj.embeded;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.energysource.szj.android.AdvObject;
import com.energysource.szj.android.DebugListener;
import com.energysource.szj.android.Log;
import com.energysource.szj.android.SZJModule;
import com.energysource.szj.embeded.AdView;
import com.energysource.szj.embeded.AdvWebView;
import com.madhouse.android.ads.AdView;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AdManager implements Handler.Callback {
    private static final int AD_EXIT_FULL_SCREEN = 6;
    public static final int AD_FILL_PARENT = 1000;
    private static final int AD_INTO_FULL_SCREEN = 5;
    static final AdManager AD_MANAGER = new AdManager();
    public static final int AD_SIZE_1024_768 = 2302;
    public static final int AD_SIZE_240_320 = 1201;
    public static final int AD_SIZE_240_38 = 1101;
    public static final int AD_SIZE_320_240 = 1202;
    public static final int AD_SIZE_320_480 = 2103;
    public static final int AD_SIZE_320_50 = 2101;
    public static final int AD_SIZE_468_60 = 8102;
    public static final int AD_SIZE_480_320 = 2102;
    public static final int AD_SIZE_480_800 = 1301;
    public static final int AD_SIZE_768_1024 = 2303;
    public static final int AD_SIZE_800_480 = 1302;
    public static final int Bottom_Banner = 81;
    public static final int Bottom_Left = 83;
    public static final int Bottom_Right = 85;
    private static final int CLOSE_FULL_AD = 100;
    private static final int DESTORY_ATONCE = 201;
    private static final int DESTORY_DELAY = 200;
    public static final int Middle_Banner = 17;
    private static final int SHOWTYPEQR = 8;
    private static final String TAG = "ADMANAGER";
    private static final int TRYREQUEST = 1;
    public static final int Top_Banner = 49;
    public static final int Top_Left = 51;
    public static final int Top_Right = 53;
    public static HashMap<String, Integer> handlerMsgCountMap = new HashMap<>();
    private static final Integer[] intArray = {Integer.valueOf((int) AdView.AD_MEASURE_240), 320, Integer.valueOf((int) AdView.AD_MEASURE_480), 768, 1024};
    protected static int isloadcount = 0;
    private static PermissionJudge permisJudge = new PermissionJudge();
    private static HashMap<Integer, String> sdkConfigMap;
    private HashMap activityMap = new HashMap();
    private AdListener adListener;
    private boolean advIntoFlag;
    private HashMap adviewMap = new HashMap();
    private boolean isDebug = false;
    private HashMap layoutMap = new HashMap();
    protected Activity mActivity;
    final SparseArray<AdView> mAdViewList = new SparseArray<>();
    private FrameLayout mRootView;
    final SparseArray<AdView.AdViewTime> mTimeList = new SparseArray<>();
    HashMap webViewMap = new HashMap();

    private AdManager() {
    }

    public static void openPermissionJudge() {
        permisJudge.setContext(AD_MANAGER.mActivity);
        permisJudge.openSwitchFlag();
    }

    /* access modifiers changed from: protected */
    public Context getContextFromActivity() {
        return AD_MANAGER.mActivity.getApplicationContext();
    }

    /* access modifiers changed from: protected */
    public void setDebug(boolean isDebug2) {
        this.isDebug = isDebug2;
        SZJServiceInstance.getInstance().setDebug(isDebug2);
    }

    public static void openDebug() {
        AD_MANAGER.setDebug(true);
    }

    public static void openDebugListener(DebugListener dl) {
        SZJServiceInstance.getInstance().setDebugListener(dl);
    }

    public static void setAdListener(AdListener adListener2) {
        AD_MANAGER.adListener = adListener2;
    }

    private static String getAppsec() {
        try {
            ApplicationInfo applicationinfo = AD_MANAGER.mActivity.getPackageManager().getApplicationInfo(AD_MANAGER.mActivity.getPackageName(), 128);
            if (applicationinfo != null) {
                String appsec = applicationinfo.metaData.getString(SZJFrameworkConfig.APPSEC);
                Log.d(TAG, "appsec from meta-data:" + appsec);
                return appsec;
            }
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
        return "";
    }

    public static boolean initAd(Activity activity, String appsec) {
        boolean flag = false;
        Log.d(TAG, " =====come in webViewMap.size:" + AD_MANAGER.webViewMap.size());
        AD_MANAGER.webViewMap.clear();
        SZJServiceInstance instance = SZJServiceInstance.getInstance();
        AD_MANAGER.mActivity = activity;
        View rootView = activity.getWindow().findViewById(16908290);
        if (rootView instanceof FrameLayout) {
            AD_MANAGER.mRootView = (FrameLayout) rootView;
        }
        if (!instance.isStartLoadFlag()) {
            Log.d(TAG, "===加载类。。。。只执行一次。");
            instance.setStartLoadFlag(true);
            instance.setContext(activity.getApplicationContext());
            instance.setDestoryFlag(false);
            instance.setActivity(activity);
            if (appsec.equals("")) {
                AdManager adManager = AD_MANAGER;
                appsec = getAppsec();
            }
            if (appsec != null || !"".equals(appsec)) {
                instance.setAppsec(appsec);
            } else {
                instance.setAppsec(activity.getPackageName());
            }
            new SZJClassLoad().start();
            flag = true;
            handlerMsgCountMap.clear();
        }
        AD_MANAGER.getSizeNo();
        isloadcount++;
        return flag;
    }

    private void getSizeNo() {
        ConcurrentHashMap chm = SZJServiceInstance.getInstance().getModulesMap();
        if (chm != null && chm.size() > 0 && chm.get(SZJFrameworkConfig.BOOTABLEMODULE) != null) {
            sdkConfigMap = ((SZJModule) chm.get(SZJFrameworkConfig.BOOTABLEMODULE)).getSizeNO();
        }
    }

    private static int getSizeNo(int width) {
        if (sdkConfigMap == null) {
            Log.d(TAG, "===getSizeNo null");
            AD_MANAGER.getSizeNo();
            if (sdkConfigMap == null) {
                Log.d(TAG, "===getSizeNo null agagin");
                return 0;
            }
        }
        int sizeNo = 0;
        int t = width;
        for (Map.Entry en : sdkConfigMap.entrySet()) {
            String[] values = ((String) en.getValue()).split(",");
            int twidth = Integer.parseInt(values[0]);
            int theight = Integer.parseInt(values[1]);
            if (width >= twidth && 200 >= theight && t > width - twidth) {
                t = width - twidth;
                sizeNo = ((Integer) en.getKey()).intValue();
            }
        }
        return sizeNo;
    }

    public static int changeDipToPx(float s) {
        double t = (double) AD_MANAGER.mActivity.getResources().getDisplayMetrics().density;
        int size = (int) ((((double) s) * t) + 0.5d);
        int w = AD_MANAGER.mActivity.getResources().getDisplayMetrics().widthPixels;
        int h = AD_MANAGER.mActivity.getResources().getDisplayMetrics().heightPixels;
        if (t == 1.0d) {
            Log.d(TAG, "==width:" + w + ",height:" + h);
            if (w < h) {
                Log.d(TAG, "t:" + 0.75d + ",当前手机分辨率为：" + Integer.toString((int) ((((double) w) * 0.75d) + 0.5d)) + ":" + Integer.toString((int) ((((double) h) * 0.75d) + 0.5d)));
                int width = (int) ((((double) w) * 0.75d) + 0.5d);
                int height = (int) ((((double) h) * 0.75d) + 0.5d);
            } else if (w > h) {
                Log.d(TAG, "t:" + 1.5d + ",当前手机分辨率为：" + Integer.toString((int) ((((double) h) * 1.5d) + 0.5d)) + ":" + Integer.toString((int) ((((double) w) * 1.5d) + 0.5d)));
                int width2 = (int) ((((double) h) * 1.5d) + 0.5d);
                int height2 = (int) ((((double) w) * 1.5d) + 0.5d);
            } else {
                Log.d(TAG, "其它：,w:" + w + ",h:" + h);
            }
        }
        return size;
    }

    /* JADX INFO: Multiple debug info for r2v1 int: [D('layoutParams' android.widget.FrameLayout$LayoutParams), D('w' int)] */
    /* JADX INFO: Multiple debug info for r2v4 int: [D('layoutParams' android.widget.FrameLayout$LayoutParams), D('w' int)] */
    /* JADX INFO: Multiple debug info for r2v7 int: [D('layoutParams' android.widget.FrameLayout$LayoutParams), D('w' int)] */
    /* JADX INFO: Multiple debug info for r2v10 int: [D('layoutParams' android.widget.FrameLayout$LayoutParams), D('w' int)] */
    /* JADX INFO: Multiple debug info for r2v13 int: [D('layoutParams' android.widget.FrameLayout$LayoutParams), D('w' int)] */
    /* JADX INFO: Multiple debug info for r2v16 int: [D('layoutParams' android.widget.FrameLayout$LayoutParams), D('w' int)] */
    /* JADX INFO: Multiple debug info for r2v21 int: [D('layoutParams' android.widget.FrameLayout$LayoutParams), D('w' int)] */
    /* JADX INFO: Multiple debug info for r2v24 int: [D('layoutParams' android.widget.FrameLayout$LayoutParams), D('w' int)] */
    /* JADX INFO: Multiple debug info for r2v27 int: [D('layoutParams' android.widget.FrameLayout$LayoutParams), D('w' int)] */
    /* JADX INFO: Multiple debug info for r2v30 int: [D('layoutParams' android.widget.FrameLayout$LayoutParams), D('w' int)] */
    /* JADX INFO: Multiple debug info for r2v37 int: [D('layoutParams' android.widget.FrameLayout$LayoutParams), D('w' int)] */
    /* JADX INFO: Multiple debug info for r2v41 android.widget.FrameLayout$LayoutParams: [D('it' java.util.Iterator), D('layoutParams' android.widget.FrameLayout$LayoutParams)] */
    /* JADX INFO: Multiple debug info for r0v104 java.util.Map$Entry: [D('height' int), D('en' java.util.Map$Entry)] */
    /* JADX INFO: Multiple debug info for r0v106 java.lang.String: [D('en' java.util.Map$Entry), D('hm' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v107 java.lang.String[]: [D('values' java.lang.String[]), D('hm' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v109 'height'  int: [D('values' java.lang.String[]), D('height' int)] */
    /* JADX INFO: Multiple debug info for r2v48 android.widget.FrameLayout$LayoutParams: [D('it' java.util.Iterator), D('layoutParams' android.widget.FrameLayout$LayoutParams)] */
    /* JADX INFO: Multiple debug info for r0v119 java.util.Map$Entry: [D('height' int), D('en' java.util.Map$Entry)] */
    /* JADX INFO: Multiple debug info for r0v121 java.lang.String: [D('en' java.util.Map$Entry), D('hm' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v122 java.lang.String[]: [D('values' java.lang.String[]), D('hm' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v124 'height'  int: [D('values' java.lang.String[]), D('height' int)] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0218  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0245  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void addAd(int r9, int r10, int r11, int r12, int r13) {
        /*
            r2 = 0
            r0 = 0
            com.energysource.szj.embeded.AdManager r1 = com.energysource.szj.embeded.AdManager.AD_MANAGER
            android.app.Activity r1 = r1.mActivity
            android.view.WindowManager r1 = r1.getWindowManager()
            android.view.Display r1 = r1.getDefaultDisplay()
            int r1 = r1.getWidth()
            r3 = 320(0x140, float:4.48E-43)
            if (r1 <= r3) goto L_0x0017
            r0 = 1
        L_0x0017:
            r3 = 0
            r1 = 0
            switch(r10) {
                case 1000: goto L_0x0324;
                case 1101: goto L_0x00a2;
                case 1201: goto L_0x0157;
                case 1202: goto L_0x018b;
                case 1301: goto L_0x02bc;
                case 1302: goto L_0x02f0;
                case 2101: goto L_0x0123;
                case 2102: goto L_0x01bf;
                case 2103: goto L_0x01f3;
                case 2302: goto L_0x0254;
                case 2303: goto L_0x0216;
                case 8102: goto L_0x0288;
                default: goto L_0x001c;
            }
        L_0x001c:
            java.lang.String r0 = "ADMANAGER"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "===add ad size(width):"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r10)
            java.lang.String r4 = r4.toString()
            com.energysource.szj.android.Log.d(r0, r4)
            java.util.HashMap<java.lang.Integer, java.lang.String> r0 = com.energysource.szj.embeded.AdManager.sdkConfigMap
            if (r0 != 0) goto L_0x003d
            com.energysource.szj.embeded.AdManager r0 = com.energysource.szj.embeded.AdManager.AD_MANAGER
            r0.getSizeNo()
        L_0x003d:
            java.util.HashMap<java.lang.Integer, java.lang.String> r0 = com.energysource.szj.embeded.AdManager.sdkConfigMap
            if (r0 == 0) goto L_0x034d
            java.util.HashMap<java.lang.Integer, java.lang.String> r0 = com.energysource.szj.embeded.AdManager.sdkConfigMap
            java.lang.Integer r4 = java.lang.Integer.valueOf(r10)
            java.lang.Object r0 = r0.get(r4)
            if (r0 == 0) goto L_0x03dd
            r4 = 0
            r0 = 0
            java.util.HashMap<java.lang.Integer, java.lang.String> r2 = com.energysource.szj.embeded.AdManager.sdkConfigMap
            java.util.Set r2 = r2.entrySet()
            java.util.Iterator r2 = r2.iterator()
        L_0x0059:
            boolean r5 = r2.hasNext()
            if (r5 == 0) goto L_0x0343
            java.lang.Object r0 = r2.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r0 = r0.getValue()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r4 = ","
            java.lang.String[] r0 = r0.split(r4)
            r4 = 0
            r4 = r0[r4]
            int r4 = java.lang.Integer.parseInt(r4)
            r5 = 1
            r0 = r0[r5]
            int r0 = java.lang.Integer.parseInt(r0)
            java.lang.String r5 = "ADMANAGER"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "==get sizeno:width:"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r4)
            java.lang.String r7 = ",height:"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r0)
            java.lang.String r6 = r6.toString()
            com.energysource.szj.android.Log.d(r5, r6)
            goto L_0x0059
        L_0x00a2:
            if (r0 == 0) goto L_0x0115
            java.lang.String r0 = "240"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r2 = changeDipToPx(r0)
            java.lang.String r0 = "38"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r0 = changeDipToPx(r0)
            android.widget.FrameLayout$LayoutParams r1 = new android.widget.FrameLayout$LayoutParams
            r1.<init>(r2, r0)
        L_0x00c5:
            com.energysource.szj.embeded.AdView r0 = new com.energysource.szj.embeded.AdView
            com.energysource.szj.embeded.AdManager r2 = com.energysource.szj.embeded.AdManager.AD_MANAGER
            android.app.Activity r2 = r2.mActivity
            r0.<init>(r2, r9)
            if (r1 == 0) goto L_0x0114
            r2 = 5
            if (r10 == r2) goto L_0x0114
            r2 = 6
            if (r10 == r2) goto L_0x0114
            r1.gravity = r11
            r0.setSizeNo(r10)
            switch(r11) {
                case 17: goto L_0x00de;
                case 49: goto L_0x03c1;
                case 51: goto L_0x03c5;
                case 53: goto L_0x03d1;
                case 81: goto L_0x00de;
                case 83: goto L_0x03cb;
                case 85: goto L_0x03d7;
                default: goto L_0x00de;
            }
        L_0x00de:
            com.energysource.szj.embeded.AdManager r10 = com.energysource.szj.embeded.AdManager.AD_MANAGER
            android.app.Activity r10 = r10.mActivity
            android.view.Window r10 = r10.getWindow()
            android.view.View r10 = r10.getDecorView()
            android.widget.FrameLayout r10 = (android.widget.FrameLayout) r10
            com.energysource.szj.embeded.AdManager r11 = com.energysource.szj.embeded.AdManager.AD_MANAGER
            java.util.HashMap r11 = r11.adviewMap
            java.lang.Integer r12 = java.lang.Integer.valueOf(r9)
            r11.put(r12, r0)
            com.energysource.szj.embeded.AdManager r11 = com.energysource.szj.embeded.AdManager.AD_MANAGER
            java.util.HashMap r11 = r11.layoutMap
            java.lang.Integer r12 = java.lang.Integer.valueOf(r9)
            r11.put(r12, r1)
            com.energysource.szj.embeded.AdManager r11 = com.energysource.szj.embeded.AdManager.AD_MANAGER
            java.util.HashMap r11 = r11.activityMap
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            com.energysource.szj.embeded.AdManager r12 = com.energysource.szj.embeded.AdManager.AD_MANAGER
            android.app.Activity r12 = r12.mActivity
            r11.put(r9, r12)
            r10.addView(r0, r1)
        L_0x0114:
            return
        L_0x0115:
            android.widget.FrameLayout$LayoutParams r0 = new android.widget.FrameLayout$LayoutParams
            r2 = 240(0xf0, float:3.36E-43)
            r4 = 38
            r0.<init>(r2, r4)
            r2 = r3
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00c5
        L_0x0123:
            if (r0 == 0) goto L_0x0148
            java.lang.String r0 = "320"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r2 = changeDipToPx(r0)
            java.lang.String r0 = "50"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r0 = changeDipToPx(r0)
            android.widget.FrameLayout$LayoutParams r1 = new android.widget.FrameLayout$LayoutParams
            r1.<init>(r2, r0)
            goto L_0x00c5
        L_0x0148:
            android.widget.FrameLayout$LayoutParams r0 = new android.widget.FrameLayout$LayoutParams
            r2 = 320(0x140, float:4.48E-43)
            r4 = 50
            r0.<init>(r2, r4)
            r2 = r3
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00c5
        L_0x0157:
            if (r0 == 0) goto L_0x017c
            java.lang.String r0 = "240"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r2 = changeDipToPx(r0)
            java.lang.String r0 = "320"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r0 = changeDipToPx(r0)
            android.widget.FrameLayout$LayoutParams r1 = new android.widget.FrameLayout$LayoutParams
            r1.<init>(r2, r0)
            goto L_0x00c5
        L_0x017c:
            android.widget.FrameLayout$LayoutParams r0 = new android.widget.FrameLayout$LayoutParams
            r2 = 240(0xf0, float:3.36E-43)
            r4 = 320(0x140, float:4.48E-43)
            r0.<init>(r2, r4)
            r2 = r3
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00c5
        L_0x018b:
            if (r0 == 0) goto L_0x01b0
            java.lang.String r0 = "320"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r2 = changeDipToPx(r0)
            java.lang.String r0 = "240"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r0 = changeDipToPx(r0)
            android.widget.FrameLayout$LayoutParams r1 = new android.widget.FrameLayout$LayoutParams
            r1.<init>(r2, r0)
            goto L_0x00c5
        L_0x01b0:
            android.widget.FrameLayout$LayoutParams r0 = new android.widget.FrameLayout$LayoutParams
            r2 = 320(0x140, float:4.48E-43)
            r4 = 240(0xf0, float:3.36E-43)
            r0.<init>(r2, r4)
            r2 = r3
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00c5
        L_0x01bf:
            if (r0 == 0) goto L_0x01e4
            java.lang.String r0 = "480"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r2 = changeDipToPx(r0)
            java.lang.String r0 = "320"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r0 = changeDipToPx(r0)
            android.widget.FrameLayout$LayoutParams r1 = new android.widget.FrameLayout$LayoutParams
            r1.<init>(r2, r0)
            goto L_0x00c5
        L_0x01e4:
            android.widget.FrameLayout$LayoutParams r0 = new android.widget.FrameLayout$LayoutParams
            r2 = 480(0x1e0, float:6.73E-43)
            r4 = 320(0x140, float:4.48E-43)
            r0.<init>(r2, r4)
            r2 = r3
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00c5
        L_0x01f3:
            if (r0 == 0) goto L_0x023b
            java.lang.String r1 = "320"
            java.lang.Float r1 = java.lang.Float.valueOf(r1)
            float r1 = r1.floatValue()
            int r3 = changeDipToPx(r1)
            java.lang.String r1 = "480"
            java.lang.Float r1 = java.lang.Float.valueOf(r1)
            float r1 = r1.floatValue()
            int r1 = changeDipToPx(r1)
            android.widget.FrameLayout$LayoutParams r2 = new android.widget.FrameLayout$LayoutParams
            r2.<init>(r3, r1)
        L_0x0216:
            if (r0 == 0) goto L_0x0245
            java.lang.String r0 = "768"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r2 = changeDipToPx(r0)
            java.lang.String r0 = "1024"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r0 = changeDipToPx(r0)
            android.widget.FrameLayout$LayoutParams r1 = new android.widget.FrameLayout$LayoutParams
            r1.<init>(r2, r0)
            goto L_0x00c5
        L_0x023b:
            android.widget.FrameLayout$LayoutParams r2 = new android.widget.FrameLayout$LayoutParams
            r4 = 320(0x140, float:4.48E-43)
            r5 = 480(0x1e0, float:6.73E-43)
            r2.<init>(r4, r5)
            goto L_0x0216
        L_0x0245:
            android.widget.FrameLayout$LayoutParams r0 = new android.widget.FrameLayout$LayoutParams
            r2 = 768(0x300, float:1.076E-42)
            r4 = 1024(0x400, float:1.435E-42)
            r0.<init>(r2, r4)
            r2 = r3
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00c5
        L_0x0254:
            if (r0 == 0) goto L_0x0279
            java.lang.String r0 = "1024"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r2 = changeDipToPx(r0)
            java.lang.String r0 = "768"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r0 = changeDipToPx(r0)
            android.widget.FrameLayout$LayoutParams r1 = new android.widget.FrameLayout$LayoutParams
            r1.<init>(r2, r0)
            goto L_0x00c5
        L_0x0279:
            android.widget.FrameLayout$LayoutParams r0 = new android.widget.FrameLayout$LayoutParams
            r2 = 1024(0x400, float:1.435E-42)
            r4 = 768(0x300, float:1.076E-42)
            r0.<init>(r2, r4)
            r2 = r3
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00c5
        L_0x0288:
            if (r0 == 0) goto L_0x02ad
            java.lang.String r0 = "468"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r2 = changeDipToPx(r0)
            java.lang.String r0 = "60"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r0 = changeDipToPx(r0)
            android.widget.FrameLayout$LayoutParams r1 = new android.widget.FrameLayout$LayoutParams
            r1.<init>(r2, r0)
            goto L_0x00c5
        L_0x02ad:
            android.widget.FrameLayout$LayoutParams r0 = new android.widget.FrameLayout$LayoutParams
            r2 = 468(0x1d4, float:6.56E-43)
            r4 = 60
            r0.<init>(r2, r4)
            r2 = r3
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00c5
        L_0x02bc:
            if (r0 == 0) goto L_0x02e1
            java.lang.String r0 = "480"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r2 = changeDipToPx(r0)
            java.lang.String r0 = "800"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r0 = changeDipToPx(r0)
            android.widget.FrameLayout$LayoutParams r1 = new android.widget.FrameLayout$LayoutParams
            r1.<init>(r2, r0)
            goto L_0x00c5
        L_0x02e1:
            android.widget.FrameLayout$LayoutParams r0 = new android.widget.FrameLayout$LayoutParams
            r2 = 480(0x1e0, float:6.73E-43)
            r4 = 800(0x320, float:1.121E-42)
            r0.<init>(r2, r4)
            r2 = r3
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00c5
        L_0x02f0:
            if (r0 == 0) goto L_0x0315
            java.lang.String r0 = "800"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r2 = changeDipToPx(r0)
            java.lang.String r0 = "480"
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            float r0 = r0.floatValue()
            int r0 = changeDipToPx(r0)
            android.widget.FrameLayout$LayoutParams r1 = new android.widget.FrameLayout$LayoutParams
            r1.<init>(r2, r0)
            goto L_0x00c5
        L_0x0315:
            android.widget.FrameLayout$LayoutParams r0 = new android.widget.FrameLayout$LayoutParams
            r2 = 800(0x320, float:1.121E-42)
            r4 = 480(0x1e0, float:6.73E-43)
            r0.<init>(r2, r4)
            r2 = r3
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00c5
        L_0x0324:
            com.energysource.szj.embeded.AdManager r10 = com.energysource.szj.embeded.AdManager.AD_MANAGER
            android.app.Activity r10 = r10.mActivity
            android.view.WindowManager r10 = r10.getWindowManager()
            android.view.Display r10 = r10.getDefaultDisplay()
            int r2 = r10.getWidth()
            int r10 = getSizeNo(r2)
            android.widget.FrameLayout$LayoutParams r0 = new android.widget.FrameLayout$LayoutParams
            r3 = -2
            r0.<init>(r2, r3)
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00c5
        L_0x0343:
            android.widget.FrameLayout$LayoutParams r2 = new android.widget.FrameLayout$LayoutParams
            r2.<init>(r4, r0)
            r0 = r1
            r1 = r2
            r2 = r3
            goto L_0x00c5
        L_0x034d:
            com.energysource.szj.embeded.AdManager r0 = com.energysource.szj.embeded.AdManager.AD_MANAGER
            r0.getSizeNo()
            java.util.HashMap<java.lang.Integer, java.lang.String> r0 = com.energysource.szj.embeded.AdManager.sdkConfigMap
            if (r0 != 0) goto L_0x0362
            java.lang.String r0 = "ADMANAGER"
            java.lang.String r4 = " get sizeno is null ....233"
            com.energysource.szj.android.Log.d(r0, r4)
            r0 = r1
            r1 = r2
            r2 = r3
            goto L_0x00c5
        L_0x0362:
            r4 = 0
            r0 = 0
            java.util.HashMap<java.lang.Integer, java.lang.String> r2 = com.energysource.szj.embeded.AdManager.sdkConfigMap
            java.util.Set r2 = r2.entrySet()
            java.util.Iterator r2 = r2.iterator()
        L_0x036e:
            boolean r5 = r2.hasNext()
            if (r5 == 0) goto L_0x03b7
            java.lang.Object r0 = r2.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r0 = r0.getValue()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r4 = ","
            java.lang.String[] r0 = r0.split(r4)
            r4 = 0
            r4 = r0[r4]
            int r4 = java.lang.Integer.parseInt(r4)
            r5 = 1
            r0 = r0[r5]
            int r0 = java.lang.Integer.parseInt(r0)
            java.lang.String r5 = "ADMANAGER"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "==get sizeno:width:"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r4)
            java.lang.String r7 = ",height:"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r0)
            java.lang.String r6 = r6.toString()
            com.energysource.szj.android.Log.d(r5, r6)
            goto L_0x036e
        L_0x03b7:
            android.widget.FrameLayout$LayoutParams r2 = new android.widget.FrameLayout$LayoutParams
            r2.<init>(r4, r0)
            r0 = r1
            r1 = r2
            r2 = r3
            goto L_0x00c5
        L_0x03c1:
            r1.topMargin = r13
            goto L_0x00de
        L_0x03c5:
            r1.leftMargin = r12
            r1.topMargin = r13
            goto L_0x00de
        L_0x03cb:
            r1.leftMargin = r12
            r1.bottomMargin = r13
            goto L_0x00de
        L_0x03d1:
            r1.rightMargin = r12
            r1.topMargin = r13
            goto L_0x00de
        L_0x03d7:
            r1.rightMargin = r12
            r1.bottomMargin = r13
            goto L_0x00de
        L_0x03dd:
            r0 = r1
            r1 = r2
            r2 = r3
            goto L_0x00c5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.energysource.szj.embeded.AdManager.addAd(int, int, int, int, int):void");
    }

    public static void destoryAd() {
        long adtime_out = System.currentTimeMillis();
        for (int i = 0; i < AD_MANAGER.mAdViewList.size(); i++) {
            AdView view = AD_MANAGER.mAdViewList.get(i);
            if (view == null) {
                Log.d(TAG, "===VIEW is null");
            } else if (view.getVisibility() == 0) {
                try {
                    if (view.getAdtime_in() == 0 || view.getTid() == null) {
                        Log.d(TAG, "广告展示时间记录过滤：adtime_in:" + view.getAdtime_in());
                    } else {
                        saveAdViewShowTime(view.getAdtime_in(), adtime_out, view.getTid(), view.getShowtype());
                        Log.d(TAG, "【destoryAd===onDetachedFromWindow】：adviewid:" + view.getId() + "=onDetachedFromWindow=广告成功展示一次:【时间】:" + (adtime_out - view.getAdtime_in()) + ",tid:" + view.getTid() + ",showtype:" + view.getShowtype() + "==");
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onWindowVisibilityChanged:", e);
                }
            }
        }
        SZJServiceInstance.getInstance().setStartLoadFlag(false);
        new SZJClassLoad().stopModule();
        SZJServiceInstance instance = SZJServiceInstance.getInstance();
        instance.setModulesMap(null);
        instance.setDestoryFlag(true);
        Log.d(TAG, "==admanager 总算销毁了==");
    }

    public static void openAllAdView() {
        for (int i = 0; i < AD_MANAGER.mAdViewList.size(); i++) {
            AdManager adManager = AD_MANAGER;
            openAdView(AD_MANAGER.mAdViewList.keyAt(i));
        }
    }

    public static void openAdView(int id) {
        Activity act = (Activity) AD_MANAGER.activityMap.get(Integer.valueOf(id));
        if (act == null) {
            Log.d(TAG, "openView没有取到当前Activity,id:" + id);
            return;
        }
        try {
            if (AD_MANAGER.layoutMap.get(Integer.valueOf(id)) != null) {
                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) AD_MANAGER.layoutMap.get(Integer.valueOf(id));
                AdView adView = (AdView) AD_MANAGER.adviewMap.get(Integer.valueOf(id));
                if (!AD_MANAGER.adviewMap.containsKey(Integer.valueOf(id))) {
                    AD_MANAGER.adviewMap.put(Integer.valueOf(id), adView);
                }
                if (!AD_MANAGER.layoutMap.containsKey(Integer.valueOf(id))) {
                    AD_MANAGER.layoutMap.put(Integer.valueOf(id), layoutParams);
                }
                if (!AD_MANAGER.activityMap.containsKey(Integer.valueOf(id))) {
                    AD_MANAGER.activityMap.put(Integer.valueOf(id), AD_MANAGER.mActivity);
                }
                if (adView != null && layoutParams != null) {
                    FrameLayout frameLayout = (FrameLayout) act.getWindow().getDecorView();
                    try {
                        adView.setVisibility(0);
                    } catch (IllegalStateException e1) {
                        Log.e(TAG, "==error==", e1);
                    }
                    if (AD_MANAGER.mAdViewList.get(id) == null) {
                        AD_MANAGER.mAdViewList.put(id, adView);
                        Log.i("wljie", "openAdView(int id)==(AD_MANAGER.mAdViewList.get(id)==null==============adView.getHeight()" + adView.getHeight());
                        return;
                    }
                    AD_MANAGER.mAdViewList.remove(id);
                    AD_MANAGER.mAdViewList.put(id, adView);
                    Log.i("wljie", "openAdView(int id)==(AD_MANAGER.mAdViewList.get(id)==null not ============adView.getHeight()" + adView.getHeight());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "==error==", e);
        }
    }

    public static void closeAdView(int id) {
        Activity act = (Activity) AD_MANAGER.activityMap.get(Integer.valueOf(id));
        if (act == null) {
            Log.d(TAG, "closeAdView没有取到当前Activity,id:" + id);
            return;
        }
        try {
            FrameLayout rootView = (FrameLayout) act.getWindow().getDecorView();
            if (AD_MANAGER.adviewMap.get(Integer.valueOf(id)) != null && rootView != null) {
                ((AdView) AD_MANAGER.adviewMap.get(Integer.valueOf(id))).setVisibility(4);
            }
        } catch (Exception e) {
            Log.e(TAG, "closeAdView Exception:", e);
        }
    }

    public static void closeAllAdView() {
        int i = 0;
        while (i < AD_MANAGER.mAdViewList.size()) {
            try {
                AdManager adManager = AD_MANAGER;
                closeAdView(AD_MANAGER.mAdViewList.keyAt(i));
                i++;
            } catch (Exception e) {
                Log.e(TAG, "=error=", e);
                return;
            }
        }
    }

    protected static void saveAdViewShowTime(long start, long end, String tid, int showtype) {
        try {
            ConcurrentHashMap chm = SZJServiceInstance.getInstance().getModulesMap();
            if (chm == null) {
                Log.d(TAG, "已经销毁,chm==null");
            } else {
                ((SZJModule) chm.get(SZJFrameworkConfig.BOOTABLEMODULE)).saveShowNum(start, end, tid, showtype);
            }
        } catch (Exception e) {
            Log.e(TAG, "=saveAdViewShowTime=", e);
        }
    }

    /* access modifiers changed from: protected */
    public void requestAdvById(int id) {
        try {
            SZJServiceInstance instance = SZJServiceInstance.getInstance();
            ConcurrentHashMap chm = instance.getModulesMap();
            if (chm != null && chm.size() > 0) {
                if (sdkConfigMap == null) {
                    AD_MANAGER.getSizeNo();
                }
                SZJModule bootable = (SZJModule) chm.get(SZJFrameworkConfig.BOOTABLEMODULE);
                AdView adView = this.mAdViewList.get(id);
                int sizeNo = 0;
                if (adView != null) {
                    if (adView.getSizeNo() == 0) {
                        int width = adView.getResWidth();
                        int height = adView.getResHeight();
                        if (width == 0) {
                            width = AD_MANAGER.mActivity.getWindowManager().getDefaultDisplay().getWidth();
                        }
                        if (height == 0) {
                        }
                        if (sdkConfigMap != null) {
                            Log.d("newZip", "check sdkConfig=====");
                            int t = width;
                            for (Map.Entry en : sdkConfigMap.entrySet()) {
                                String[] values = ((String) en.getValue()).split(",");
                                int twidth = Integer.parseInt(values[0]);
                                int theight = Integer.parseInt(values[1]);
                                if (width >= twidth && 200 >= theight && t > width - twidth) {
                                    t = width - twidth;
                                    sizeNo = ((Integer) en.getKey()).intValue();
                                }
                            }
                        } else {
                            AD_MANAGER.getSizeNo();
                        }
                        Log.i("wljie", "sizeNo========" + sizeNo);
                        adView.setSizeNo(sizeNo);
                    } else {
                        sizeNo = adView.getSizeNo();
                    }
                    if (sizeNo != 0 && bootable != null && !instance.isDestoryFlag()) {
                        if (4 != 0 || 8 != 0) {
                            AdvObject advObject = bootable.requestAd(sizeNo, 8);
                            if (advObject != null) {
                                adViewLoadAdvObject(id, adView, advObject);
                                return;
                            }
                            if (AD_MANAGER.adListener != null) {
                                AD_MANAGER.adListener.failedReceiveAd(adView);
                            }
                            Log.i("==AdView==", "return AdWebView is null.try late!===1====" + id);
                            Handler handler = new Handler(Looper.getMainLooper(), this);
                            Message msg = Message.obtain(handler, id);
                            msg.arg1 = 1;
                            handler.sendMessageDelayed(msg, 5000);
                        }
                    }
                }
            } else if (!instance.isDestoryFlag()) {
                Log.i("==AdView==", "wait load class.5");
                Handler handler2 = new Handler(Looper.getMainLooper(), this);
                Message msg2 = Message.obtain(handler2, id);
                msg2.arg1 = 1;
                handler2.sendMessageDelayed(msg2, 2000);
            }
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }

    public void adViewLoadAdvObject(int id, AdView adView, AdvObject advObject) {
        if (AD_MANAGER.webViewMap == null) {
            AD_MANAGER.webViewMap = new HashMap();
        }
        AdvWebView awv = null;
        int key = 0;
        Log.d(TAG, "====request id:" + id + "AD_MANAGER.webViewMap size:" + AD_MANAGER.webViewMap.size());
        if (AD_MANAGER.webViewMap.get(Integer.valueOf(id)) != null) {
            HashMap tempMap = (HashMap) AD_MANAGER.webViewMap.get(Integer.valueOf(id));
            for (Map.Entry e : tempMap.entrySet()) {
                AdvWebView tempView = (AdvWebView) e.getValue();
                if (4 == tempView.getVisibility()) {
                    Log.i("==AdView==", "==tempView.getVisibility()==" + tempView.getVisibility());
                    adView.removeView(tempView);
                    awv = tempView;
                    key = ((Integer) e.getKey()).intValue();
                }
                if (8 == tempView.getVisibility()) {
                    Log.i("==AdView==", "==View.GONE===tempView.getVisibility()==" + tempView.getVisibility());
                    adView.removeView(tempView);
                    awv = tempView;
                    key = ((Integer) e.getKey()).intValue();
                }
            }
            if (awv == null) {
                awv = new AdvWebView(AD_MANAGER.mActivity);
                awv.setBackgroundColor(0);
            }
            if (key == 0) {
                tempMap.put(Integer.valueOf(awv.hashCode()), awv);
                AD_MANAGER.webViewMap.put(Integer.valueOf(id), tempMap);
            } else {
                AD_MANAGER.webViewMap.put(Integer.valueOf(id), tempMap);
            }
        } else {
            HashMap tempMap2 = new HashMap();
            awv = new AdvWebView(AD_MANAGER.mActivity);
            awv.setBackgroundColor(0);
            tempMap2.put(Integer.valueOf(awv.hashCode()), awv);
            AD_MANAGER.webViewMap.put(Integer.valueOf(id), tempMap2);
        }
        awv.setLayoutParams(new LinearLayout.LayoutParams(advObject.getWidth(), advObject.getHeight()));
        if (advObject.getTime() == 0) {
            awv.setTime(3000);
        } else {
            awv.setTime(advObject.getTime());
        }
        awv.setTid(advObject.getTid());
        awv.setShowType(advObject.getShowtype());
        awv.setResWidth(advObject.getWidth());
        awv.setResHeight(advObject.getHeight());
        awv.getSettings().setJavaScriptEnabled(true);
        awv.setVerticalScrollBarEnabled(false);
        awv.setHorizontalScrollBarEnabled(false);
        EsScreen es = new EsScreen();
        es.setHeight(advObject.getHeight());
        es.setWidth(advObject.getWidth());
        AdvWebView.MyWebViewClient webViewClient = new AdvWebView.MyWebViewClient();
        webViewClient.setTid(advObject.getTid());
        webViewClient.setResTitle(advObject.getResTitle());
        webViewClient.setResContent(advObject.getResContent());
        webViewClient.setShowtype(advObject.getShowtype());
        awv.setWebViewClient(webViewClient);
        awv.addJavascriptInterface(es, "es");
        awv.loadUrl("file://" + advObject.getUrl());
        adView.addView(awv, -1, (ViewGroup.LayoutParams) null);
        awv.setLast(true);
        Log.i("wljie", "====adView.getWidth():" + adView.getWidth());
        Log.i("wljie", "====adView.getHeight():" + adView.getHeight());
        Log.i("wljie", "====adView.getWidth():" + adView.getResWidth());
        Log.i("wljie", "====adView.getHeight():" + adView.getResHeight());
        this.mAdViewList.remove(id);
        this.mAdViewList.append(id, adView);
        if (AD_MANAGER.adListener != null && adView.getChildCount() >= 2) {
            AD_MANAGER.adListener.receiveAd(adView);
        }
    }

    public boolean handleMessage(Message msg) {
        switch (msg.arg1) {
            case 1:
                requestAdvById(msg.what);
                return true;
            default:
                return true;
        }
    }

    public static void clearMemory() {
    }
}
