package com.energysource.szj.embeded;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import com.energysource.szj.android.Log;

public class AdView extends RelativeLayout {
    private static final String TAG = "==AdView==";
    /* access modifiers changed from: private */
    public static final AdManager mAdManager = AdManager.AD_MANAGER;
    private final RelativeLayout.LayoutParams LAYOUT_PARAMS = new RelativeLayout.LayoutParams(-2, -2);
    private final int MSG_FIRST = 2;
    private final int MSG_FLIP = 1;
    private boolean adExitFlag = false;
    /* access modifiers changed from: private */
    public long adtime_in = 0;
    private long adtime_out = 0;
    private boolean checkSdkConfigFlag = true;
    private int mAdNumInTask = 0;
    private int mCurr = -1;
    private final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    if (AdView.this.running) {
                        AdView.this.showNext();
                        sendMessageDelayed(obtainMessage(1), AdView.this.mInterval);
                        long unused = AdView.this.adtime_in = System.currentTimeMillis();
                        return;
                    }
                    return;
                case 2:
                    AdView.mAdManager.requestAdvById(AdView.this.getId());
                    return;
                default:
                    return;
            }
        }
    };
    private TranslateAnimation mInAnim;
    /* access modifiers changed from: private */
    public long mInterval;
    private boolean mIsTaskShowOver;
    private boolean mIsTransparent;
    private TranslateAnimation mOutAnim;
    private boolean mStart = false;
    private boolean mVisible = false;
    private int resHeight = 0;
    private int resWidth = 0;
    /* access modifiers changed from: private */
    public boolean running = false;
    private int showtype = 0;
    private int sizeNo = 0;
    private String tid;

    public AdView(Context context, int id) {
        super(context);
        initAnim(context);
        this.LAYOUT_PARAMS.addRule(13);
        setId(id);
        mAdManager.mAdViewList.put(id, this);
        this.mHandler.sendMessage(this.mHandler.obtainMessage(2));
    }

    public AdView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAnim(context);
        this.LAYOUT_PARAMS.addRule(13);
        getViewParam(attrs);
        if (getId() == -1) {
            setId(mAdManager.mAdViewList.size() + 1);
        }
        mAdManager.mAdViewList.put(getId(), this);
        if (context instanceof Activity) {
            AdManager.initAd((Activity) context, "");
            this.mHandler.sendMessage(this.mHandler.obtainMessage(2));
        }
    }

    private void getViewParam(AttributeSet attrs) {
        this.mIsTransparent = attrs.getAttributeBooleanValue(null, "transparent", true);
        boolean isdebug = attrs.getAttributeBooleanValue(null, "debug", false);
        Log.d(TAG, "-===1=isdebug:" + isdebug);
        mAdManager.setDebug(isdebug);
        this.resWidth = covertUnit(attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "layout_width"));
        this.resHeight = covertUnit(attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "layout_height"));
        Log.d(TAG, "-===o-=========resWith【" + this.resWidth + "】,resHeight【" + this.resHeight + "】");
    }

    private void checkScreenRotate(Context context) {
        int screen = context.getResources().getConfiguration().orientation;
        if (mAdManager.mTimeList.get(getId()) != null) {
            AdViewTime at = mAdManager.mTimeList.get(getId());
            int orientation = at.getOrientation();
            long time = System.currentTimeMillis();
            if (orientation == screen) {
                Log.d(TAG, "屏幕没有切换:" + (time - at.getTime_1()));
            } else {
                if (screen == 1) {
                    Log.d(TAG, "==从竖屏切换到横屏：ORIENTATION_PORTRAIT=1");
                } else if (screen == 2) {
                    Log.d(TAG, "==从横屏切换到竖屏：ORIENTATION_LANDSCAPE=2");
                }
                Log.d(TAG, "屏幕切换时间：" + (time - at.getTime_1()));
                if (time - at.getTime_1() < 3000) {
                    Log.d(TAG, "==============屏幕切换小于3秒，退出不显示广告==============");
                    at.setOrientation(screen);
                    mAdManager.mTimeList.remove(getId());
                    mAdManager.mTimeList.put(getId(), at);
                    return;
                }
            }
            at.setTime_1(System.currentTimeMillis());
            at.setOrientation(screen);
            mAdManager.mTimeList.remove(getId());
            mAdManager.mTimeList.put(getId(), at);
            return;
        }
        AdViewTime at2 = new AdViewTime();
        at2.setTime_1(System.currentTimeMillis());
        at2.setOrientation(screen);
        mAdManager.mTimeList.put(getId(), at2);
    }

    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        try {
            if (!(child instanceof AdvWebView)) {
                Log.d(TAG, "检测如果view类型不是AdWebView," + child.getClass());
                return;
            }
            if (getChildCount() == 0) {
                startFlipping();
            }
            AdvWebView ad = (AdvWebView) child;
            this.tid = ad.getTid();
            this.showtype = ad.getShowType();
            ad.setTransparent(this.mIsTransparent);
            ad.setVisibility(8);
            this.resWidth = ((AdvWebView) child).getResWidth();
            this.resHeight = ((AdvWebView) child).getResHeight();
            super.addView(ad, index, this.LAYOUT_PARAMS);
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthSize = View.MeasureSpec.getSize(widthMeasureSpec);
        int size = View.MeasureSpec.getSize(heightMeasureSpec);
        double t = (double) AdManager.AD_MANAGER.mActivity.getResources().getDisplayMetrics().density;
        int i = AdManager.AD_MANAGER.mActivity.getResources().getDisplayMetrics().widthPixels;
        int i2 = AdManager.AD_MANAGER.mActivity.getResources().getDisplayMetrics().heightPixels;
        if (t == 1.0d) {
            setMeasuredDimension(widthSize, AdManager.changeDipToPx((float) getResHeight()));
        } else if (t == 1.5d) {
            setMeasuredDimension(widthSize, getResHeight());
            setMeasuredDimension(widthSize, AdManager.changeDipToPx((float) getResHeight()));
        } else {
            setMeasuredDimension(widthSize, getResHeight());
        }
    }

    private void initAnim(Context context) {
        this.mInAnim = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, -1.0f, 2, 0.0f);
        this.mInAnim.setDuration(1600);
        this.mOutAnim = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 0.0f, 2, 1.0f);
        this.mOutAnim.setDuration(1600);
    }

    private void startFlipping() {
        this.mStart = true;
        Log.d(TAG, "____startFlipping___");
        updateRunning();
    }

    private void stopFlipping() {
        this.mStart = false;
        Log.d(TAG, "____stopFlipping____");
        updateRunning();
    }

    private void updateRunning() {
        this.running = this.mVisible && this.mStart;
        if (this.running) {
            this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(1), this.mInterval);
            return;
        }
        this.mHandler.removeMessages(1);
        Log.d(TAG, "-adview's id:" + getId() + "-mHandler.removeMessages(MSG_FLIP),11111");
    }

    /* access modifiers changed from: private */
    public void showNext() {
        if (getChildCount() != 0) {
            AdvWebView ad = (AdvWebView) getChildAt(this.mCurr + 1);
            if (ad == null || ad.getVisibility() == 0) {
                this.mInterval = this.mOutAnim.getDuration();
            } else {
                this.mInterval = ad.getTime();
                ad.setVisibility(0);
                ad.startAnimation(this.mInAnim);
                if (ad.isLast()) {
                    Log.d(TAG, "====请求新广告==【频率】" + ad.getTime() + "==");
                    mAdManager.requestAdvById(getId());
                }
            }
            AdvWebView ad2 = (AdvWebView) getChildAt(this.mCurr);
            if (ad2 != null && ad2.getVisibility() == 0) {
                ad2.startAnimation(this.mOutAnim);
                ad2.setVisibility(4);
                this.mAdNumInTask++;
                this.mIsTaskShowOver = ad2.isLast();
            }
            this.mCurr++;
            removeTask();
        }
    }

    private void removeTask() {
        if (this.mIsTaskShowOver) {
            super.removeViews(0, this.mAdNumInTask);
            Log.d(TAG, "=======AdView  removeTask===============");
            Log.d(TAG, "AdView(" + getId() + "),getChildCount:" + super.getChildCount());
            this.mCurr -= this.mAdNumInTask;
            if (getChildCount() == 0) {
                stopFlipping();
                this.mCurr = -1;
            }
            this.mAdNumInTask = 0;
            this.mIsTaskShowOver = false;
            if (!this.adExitFlag) {
                getDisplayAdTime();
            }
        }
    }

    private void getDisplayAdTime() {
        try {
            if (this.tid == null || this.adtime_in == 0) {
                Log.d(TAG, "广告展示时间记录过滤：adtime_in:" + this.adtime_in);
                return;
            }
            this.adtime_out = System.currentTimeMillis();
            Log.d(TAG, "adtime_in:" + this.adtime_in);
            Log.d(TAG, "adtime_out:" + this.adtime_out);
            Log.d(TAG, "【showNext】： adviewid" + getId() + "=showNext=广告成功展示一次:【时间】:" + (this.adtime_out - this.adtime_in) + ",tid:" + this.tid + ",showtype:" + this.showtype + "==");
            AdManager.saveAdViewShowTime(this.adtime_in, this.adtime_out, this.tid, this.showtype);
            this.mHandler.removeMessages(1);
            Log.d(TAG, "-adview's id:" + getId() + "-mHandler.removeMessages(MSG_FLIP),22222");
            this.adtime_in = 0;
            this.adtime_out = 0;
        } catch (Exception e) {
            Log.e(TAG, "showNext:", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mAdManager.mAdViewList.remove(getId());
        Log.d(TAG, "!!!!!!onDetachedFromWindow");
        this.mIsTaskShowOver = true;
        this.adExitFlag = true;
        removeTask();
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        this.mVisible = visibility == 0;
        updateRunning();
        Log.d(TAG, "--adview's id:" + getId() + "-onWindowVisibilityChanged(visibility):" + visibility + " mVisible:" + this.mVisible);
    }

    private int covertUnit(String value) {
        String tmp = null;
        try {
            if (value.contains("px")) {
                tmp = value.substring(0, value.indexOf("px"));
            } else if (value.contains("sp")) {
                tmp = value.substring(0, value.indexOf("sp"));
            } else if (value.contains("dip")) {
                tmp = value.substring(0, value.indexOf("dip"));
            }
            if (tmp == null) {
                return 0;
            }
            if (tmp.contains(".")) {
                tmp = value.substring(0, value.indexOf("."));
            }
            return Integer.parseInt(tmp.trim());
        } catch (Exception e) {
            Log.e(TAG, "", e);
            return 0;
        }
    }

    public long getAdtime_in() {
        return this.adtime_in;
    }

    /* access modifiers changed from: protected */
    public void setAdtime_in(long adtime_in2) {
        this.adtime_in = adtime_in2;
    }

    /* access modifiers changed from: protected */
    public void setAdtime_out(long adtime_out2) {
        this.adtime_out = adtime_out2;
    }

    public String getTid() {
        return this.tid;
    }

    public void setTid(String tid2) {
        this.tid = tid2;
    }

    /* access modifiers changed from: protected */
    public int getShowtype() {
        return this.showtype;
    }

    /* access modifiers changed from: protected */
    public void setShowtype(int showtype2) {
        this.showtype = showtype2;
    }

    /* access modifiers changed from: protected */
    public boolean isCheckSdkConfigFlag() {
        return this.checkSdkConfigFlag;
    }

    /* access modifiers changed from: protected */
    public void setCheckSdkConfigFlag(boolean checkSdkConfigFlag2) {
        this.checkSdkConfigFlag = checkSdkConfigFlag2;
    }

    /* access modifiers changed from: protected */
    public int getResWidth() {
        return this.resWidth;
    }

    /* access modifiers changed from: protected */
    public void setResWidth(int resWidth2) {
        this.resWidth = resWidth2;
    }

    /* access modifiers changed from: protected */
    public int getResHeight() {
        return this.resHeight;
    }

    /* access modifiers changed from: protected */
    public void setResHeight(int resHeight2) {
        this.resHeight = resHeight2;
    }

    /* access modifiers changed from: protected */
    public int getSizeNo() {
        return this.sizeNo;
    }

    /* access modifiers changed from: protected */
    public void setSizeNo(int sizeNo2) {
        this.sizeNo = sizeNo2;
    }

    class AdViewTime {
        private int orientation;
        private long time_1;
        private long time_2;

        AdViewTime() {
        }

        public long getTime_1() {
            return this.time_1;
        }

        public void setTime_1(long time_12) {
            this.time_1 = time_12;
        }

        public long getTime_2() {
            return this.time_2;
        }

        public void setTime_2(long time_22) {
            this.time_2 = time_22;
        }

        public int getOrientation() {
            return this.orientation;
        }

        public void setOrientation(int orientation2) {
            this.orientation = orientation2;
        }
    }
}
