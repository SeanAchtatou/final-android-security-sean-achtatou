package com.energysource.szj.embeded;

import android.content.Context;
import android.content.res.Resources;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.energysource.szj.android.Log;
import com.energysource.szj.android.SZJModule;
import com.energysource.szj.embeded.utils.FileUtils;
import com.energysource.szj.embeded.utils.ZipUtil;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.util.concurrent.ConcurrentHashMap;

public class SZJClassLoad {
    private static final int START_CLASSLOAD = 1;
    private static final String TAG = "SZJClassLoad.java";
    HandlerThread handleThread;
    Looper looper;
    Message msg;
    MyHandler myHandler;
    String pkg = "";

    class MyHandler extends Handler {
        public MyHandler() {
        }

        public MyHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.arg1) {
                case 1:
                    SZJClassLoad.this.startClassLoad();
                    return;
                default:
                    return;
            }
        }
    }

    public void start() {
        this.handleThread = new HandlerThread("handler_thread");
        this.handleThread.start();
        this.looper = this.handleThread.getLooper();
        this.myHandler = new MyHandler(this.looper);
        this.msg = this.myHandler.obtainMessage();
        this.msg.arg1 = 1;
        this.msg.sendToTarget();
    }

    public void startClassLoad() {
        new UpgradeJar().startUpgradeJar();
        boolean flag = true;
        SZJServiceInstance instance = SZJServiceInstance.getInstance();
        this.pkg = instance.getContext().getPackageName();
        File xmlFile = new File("data/data/" + this.pkg + "/loadjar/" + SZJFrameworkConfig.XMLNAME);
        if (!FileUtils.existsFile(xmlFile)) {
            flag = false;
            if (!copyBakFile()) {
                backupDatabase();
                boolean unzipflag = false;
                try {
                    unzipflag = unzipJar();
                } catch (Exception e) {
                    Log.e(TAG, "unzipJar", e);
                }
                if (unzipflag) {
                    flag = true;
                }
            }
        }
        ModuleEntity me = FileUtils.readXml(xmlFile);
        instance.setMe(me);
        if (checkJar(me)) {
            flag = false;
            instance.setLoadClassFlag(true);
            if (!copyBakFile() && checkJar(me)) {
                backupDatabase();
                if (unzipJar()) {
                    flag = true;
                }
            }
        }
        if (flag) {
            ConcurrentHashMap moduleMap = instance.getModulesMap();
            if (moduleMap == null || moduleMap.size() > 0) {
            }
            boolean loadFlag = classLoad();
            if (!loadFlag) {
                if (!instance.isLoadClassFlag() && !copyBakFile() && checkJar(me)) {
                    instance.setLoadClassFlag(true);
                }
                if (instance.isLoadClassFlag()) {
                    backupDatabase();
                    unzipJar();
                }
                loadFlag = classLoad();
            }
            if (!loadFlag) {
                return;
            }
            if (startModule()) {
                Log.i("framework", "startModuleSuccess");
            } else if (!copyBakFile() && checkJar(me)) {
                instance.setLoadClassFlag(true);
                backupDatabase();
                unzipJar();
                if (classLoad()) {
                    startModule();
                }
            }
        }
    }

    public boolean copyBakFile() {
        File f = new File("data/data/" + this.pkg + "/bakjar/" + SZJFrameworkConfig.BOOTABLEMAPKEY + SZJFrameworkConfig.POSTFIX);
        Log.i("framework", "path===" + f.getAbsolutePath());
        boolean flag = FileUtils.existsFile(f);
        boolean flag2 = FileUtils.existsFile(new File(SZJFrameworkConfig.BAKJARPATH + SZJFrameworkConfig.XMLNAME));
        if (!flag2) {
            return flag2;
        }
        boolean flag3 = FileUtils.deleteDirectory(SZJFrameworkConfig.JARPATH);
        if (!flag3) {
            return flag3;
        }
        try {
            boolean createDire = FileUtils.createDire(SZJFrameworkConfig.JARPATH);
            File targetFile = new File("data/data/" + this.pkg + "/loadjar/");
            ZipUtil.copyFile(new File("data/data/" + this.pkg + "/bakjar/" + SZJFrameworkConfig.BOOTABLEMAPKEY + SZJFrameworkConfig.POSTFIX), targetFile);
            ZipUtil.copyFile(new File("data/data/" + this.pkg + "/bakjar/" + SZJFrameworkConfig.XMLNAME), targetFile);
            return flag3;
        } catch (Exception e) {
            Log.e(TAG, "copyBakFileException:", e);
            return false;
        }
    }

    public void backupDatabase() {
        File database = new File("data/data/" + this.pkg + "/databases/SZJService");
        File databaseBak = new File("data/data/" + this.pkg + "/databases/SZJServiceBak");
        if (FileUtils.existsFile(database)) {
            if (FileUtils.existsFile(databaseBak)) {
                databaseBak.delete();
            }
            database.renameTo(new File("data/data/" + this.pkg + "/databases/SZJServiceBak"));
        }
    }

    public boolean unzipJar() {
        boolean flag = FileUtils.deleteDirectory("data/data/" + this.pkg + "/loadjar/");
        boolean flag2 = FileUtils.createDire("data/data/" + this.pkg + "/loadjar/");
        if (!flag2) {
            return flag2;
        }
        try {
            ZipUtil.doExtract(getClass().getClassLoader().getResourceAsStream("modulejar.zip"), new File("data/data/" + this.pkg + "/loadjar/"));
            if (!FileUtils.createDire("/data/data/" + this.pkg + "/bakjar/")) {
                return flag2;
            }
            ZipUtil.doExtract(getClass().getClassLoader().getResourceAsStream("modulejar.zip"), new File("/data/data/" + this.pkg + "/bakjar/"));
            return flag2;
        } catch (Exception e) {
            Log.e(TAG, "unzipjarException:", e);
            return false;
        }
    }

    public boolean checkJar(ModuleEntity me) {
        if (!FileUtils.checkJarFile(SZJFrameworkConfig.BOOTABLEMAPKEY, me, this.pkg)) {
            return true;
        }
        return false;
    }

    public boolean stopModule() {
        ConcurrentHashMap chm = SZJServiceInstance.getInstance().getModulesMap();
        if (chm == null) {
            return true;
        }
        try {
            if (chm.get(SZJFrameworkConfig.BOOTABLEMODULE) == null) {
                return true;
            }
            ((SZJModule) chm.get(SZJFrameworkConfig.BOOTABLEMODULE)).destory();
            return true;
        } catch (Exception e) {
            Log.e(TAG, "stopModuleException:", e);
            return false;
        }
    }

    public boolean classLoad() {
        boolean flag = true;
        ConcurrentHashMap chm = new ConcurrentHashMap();
        SZJServiceInstance instance = SZJServiceInstance.getInstance();
        ModuleEntity me = instance.getMe();
        try {
            SZJModule module = loadJar(me.getName(), me.getLoadClassPath());
            if (module == null) {
                flag = false;
            } else {
                chm.put(SZJFrameworkConfig.BOOTABLEMODULE, module);
            }
        } catch (Exception e) {
            Log.e(TAG, "classLoadException", e);
            flag = false;
        }
        if (flag) {
            instance.setModulesMap(chm);
        }
        return flag;
    }

    public SZJModule loadJar(String fileName, String classname) {
        try {
            Log.i("framework", "classname===" + classname + "===fileName====" + fileName);
            SZJModule module = (SZJModule) new DexClassLoader("data/data/" + this.pkg + "/loadjar/" + fileName + SZJFrameworkConfig.POSTFIX, "data/data/" + this.pkg + "/loadjar/", null, new DexClassLoader("data/data/" + this.pkg + "/loadjar/moduleconfig" + SZJFrameworkConfig.POSTFIX, "data/data/" + this.pkg + "/loadjar/", null, getClass().getClassLoader())).loadClass(classname).newInstance();
            Log.i("framework", "name==" + fileName + "==classpath==" + classname);
            return module;
        } catch (Exception e) {
            Log.e(TAG, "loadJarException:", e);
            return null;
        }
    }

    public boolean startModule() {
        try {
            SZJServiceInstance instance = SZJServiceInstance.getInstance();
            ConcurrentHashMap initValues = new ConcurrentHashMap();
            initValues.put(SZJFrameworkConfig.CONTEXT, instance.getContext());
            SZJModule bootable = (SZJModule) instance.getModulesMap().get(SZJFrameworkConfig.BOOTABLEMODULE);
            Context context = instance.getContext();
            Resources resources = context.getResources();
            initValues.put(SZJFrameworkConfig.WATCHERTYPE, "");
            initValues.put(SZJFrameworkConfig.BOOTABLEMODULE, bootable);
            initValues.put(SZJFrameworkConfig.PROJECTTYPE, 1);
            initValues.put(SZJFrameworkConfig.WIFIMANAGER, (WifiManager) context.getSystemService("wifi"));
            initValues.put(SZJFrameworkConfig.ACTIVITY, instance.getActivity());
            initValues.put(SZJFrameworkConfig.DEBUGFLAG, Boolean.valueOf(instance.isDebug()));
            if (instance.getDebugListener() != null) {
                initValues.put(SZJFrameworkConfig.DEBUGLISTENER, instance.getDebugListener());
            }
            if (instance.getAppsec() == null || "".equals(instance.getAppsec())) {
                instance.setAppsec(this.pkg);
            }
            initValues.put(SZJFrameworkConfig.APPSEC, instance.getAppsec());
            bootable.initValue(initValues);
            bootable.start();
            return true;
        } catch (Exception e) {
            Log.e(TAG, "startModuleException:", e);
            return false;
        }
    }
}
