package com.energysource.szj.embeded;

import com.energysource.szj.android.Log;

/* compiled from: AdvWebView */
class EsScreen {
    int height;
    int width;

    EsScreen() {
    }

    public String getScreen() {
        Log.d("es", "========getScreen....");
        return this.width + "," + this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width2) {
        this.width = width2;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height2) {
        this.height = height2;
    }
}
