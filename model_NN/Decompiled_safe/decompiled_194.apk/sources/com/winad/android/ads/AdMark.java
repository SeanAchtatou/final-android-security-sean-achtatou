package com.winad.android.ads;

import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

class AdMark {
    private static String a = "winad_cache";
    public static AdMark adMark;
    private static String b = ".properties";
    private static String c = "submit";
    private Properties d = null;
    private Context e;

    private AdMark(Context context) {
        this.e = context;
    }

    public static AdMark getInstance(Context context) {
        if (adMark == null) {
            adMark = new AdMark(context);
        }
        return adMark;
    }

    public String getDate(String str) {
        try {
            Properties properties = new Properties();
            File file = new File(this.e.getDir(a, 0), "2011-04-12");
            if (!file.exists()) {
                file.mkdir();
            }
            File file2 = new File(file, c + b);
            if (file2.exists()) {
                properties.load(new FileInputStream(file2));
                this.d = properties;
            }
        } catch (IOException e2) {
            this.d = null;
        }
        if (this.d != null) {
            return this.d.getProperty(str);
        }
        return null;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setKeepData(java.lang.String r6, java.lang.String r7) {
        /*
            r5 = this;
            java.util.Properties r0 = new java.util.Properties
            r0.<init>()
            android.content.Context r1 = r5.e     // Catch:{ Exception -> 0x005f }
            java.lang.String r2 = com.winad.android.ads.AdMark.a     // Catch:{ Exception -> 0x005f }
            r3 = 0
            java.io.File r1 = r1.getDir(r2, r3)     // Catch:{ Exception -> 0x005f }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x005f }
            java.lang.String r3 = "2011-04-12"
            r2.<init>(r1, r3)     // Catch:{ Exception -> 0x005f }
            boolean r1 = r2.exists()     // Catch:{ Exception -> 0x005f }
            if (r1 != 0) goto L_0x001e
            r2.mkdir()     // Catch:{ Exception -> 0x005f }
        L_0x001e:
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x005f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005f }
            r3.<init>()     // Catch:{ Exception -> 0x005f }
            java.lang.String r4 = com.winad.android.ads.AdMark.c     // Catch:{ Exception -> 0x005f }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x005f }
            java.lang.String r4 = com.winad.android.ads.AdMark.b     // Catch:{ Exception -> 0x005f }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x005f }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x005f }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x005f }
            boolean r2 = r1.exists()     // Catch:{ Exception -> 0x005f }
            if (r2 == 0) goto L_0x0046
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x005f }
            r2.<init>(r1)     // Catch:{ Exception -> 0x005f }
            r0.load(r2)     // Catch:{ Exception -> 0x005f }
        L_0x0046:
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x005f }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x005f }
            r3.<init>(r1)     // Catch:{ Exception -> 0x005f }
            r2.<init>(r3)     // Catch:{ Exception -> 0x005f }
            r0.setProperty(r6, r7)     // Catch:{ all -> 0x005a }
            r0.store(r2, r6)     // Catch:{ all -> 0x005a }
            r2.close()     // Catch:{ Exception -> 0x005f }
        L_0x0059:
            return
        L_0x005a:
            r0 = move-exception
            r2.close()     // Catch:{ Exception -> 0x005f }
            throw r0     // Catch:{ Exception -> 0x005f }
        L_0x005f:
            r0 = move-exception
            java.lang.String r0 = "winAd"
            r1 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r1)
            if (r0 == 0) goto L_0x0059
            java.lang.String r0 = "winAd"
            java.lang.String r1 = "Could not get localized strings from the DD servers."
            com.winad.android.ads.BannerLogger.d(r0, r1)
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.winad.android.ads.AdMark.setKeepData(java.lang.String, java.lang.String):void");
    }
}
