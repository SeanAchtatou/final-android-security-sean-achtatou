package com.winad.android.ads;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import com.adchina.android.ads.Common;
import java.util.Locale;

public class Utilities {
    protected static String a() {
        String country;
        Locale locale = Locale.getDefault();
        return (locale.getLanguage() == null || (country = locale.getCountry()) == null) ? "" : country.equalsIgnoreCase("cn") ? Common.KCLK : country.indexOf("tw") != -1 ? Common.KIMP : country.indexOf("en") != -1 ? "3" : country.indexOf("jajp") != -1 ? "4" : country.indexOf("kokr") != -1 ? "5" : country.indexOf("dede") != -1 ? "6" : country;
    }

    protected static String a(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        String str = displayMetrics.widthPixels + "|" + displayMetrics.heightPixels;
        String str2 = "0";
        if ("320|240".equals(str) || "240|320".equals(str)) {
            str2 = Common.KCLK;
        }
        if ("220|176".equals(str) || "176|220".equals(str)) {
            str2 = Common.KIMP;
        }
        if ("960|640".equals(str) || "640|960".equals(str)) {
            str2 = "3";
        }
        if ("480|854".equals(str) || "854|480".equals(str)) {
            str2 = "4";
        }
        if ("320|480".equals(str) || "480|320".equals(str)) {
            str2 = "5";
        }
        if ("400|800".equals(str) || "800|400".equals(str)) {
            str2 = "6";
        }
        if ("1024|640".equals(str) || "640|1024".equals(str)) {
            str2 = "7";
        }
        if ("240|400".equals(str) || "400|240".equals(str)) {
            str2 = "8";
        }
        if ("720|480".equals(str) || "480|720".equals(str)) {
            str2 = "9";
        }
        if ("480|800".equals(str) || "800|480".equals(str)) {
            str2 = "10";
        }
        if ("208|176".equals(str) || "176|208".equals(str)) {
            str2 = "11";
        }
        if ("1024|480".equals(str) || "480|1024".equals(str)) {
            str2 = "12";
        }
        if ("640|360".equals(str) || "360|640".equals(str)) {
            str2 = "13";
        }
        if ("160|128".equals(str) || "128|160".equals(str)) {
            str2 = "14";
        }
        if ("416|352".equals(str) || "352|416".equals(str)) {
            str2 = "15";
        }
        if ("800|352".equals(str) || "352|800".equals(str)) {
            str2 = "16";
        }
        if ("432|240".equals(str) || "240|432".equals(str)) {
            str2 = "17";
        }
        if ("132|176".equals(str) || "176|132".equals(str)) {
            str2 = "18";
        }
        if ("208|208".equals(str)) {
            str2 = "19";
        }
        if ("128|128".equals(str)) {
            str2 = "20";
        }
        return ("1024|600".equals(str) || "600|1024".equals(str)) ? "21" : str2;
    }

    protected static String b(Context context) {
        String substring = Build.VERSION.RELEASE.substring(0, 3);
        return ("1.5".equals(substring) || "Cup".equals(substring)) ? Common.KCLK : ("1.6".equals(substring) || "Don".equals(substring)) ? Common.KIMP : "2.0".equals(substring) ? "3" : ("2.1".equals(substring) || "Ecl".equals(substring)) ? "4" : ("2.2".equals(substring) || "Fro".equals(substring)) ? "5" : ("2.3".equals(substring) || "Gin".equals(substring)) ? "6" : "0";
    }

    protected static String c(Context context) {
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        return deviceId == null ? "" : deviceId;
    }

    protected static String d(Context context) {
        String subscriberId = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        if (subscriberId.length() > 15) {
            subscriberId = subscriberId.substring(0, 15);
        }
        return subscriberId == null ? "" : subscriberId;
    }

    protected static String e(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) ((Activity) context).getSystemService("connectivity")).getActiveNetworkInfo();
            String lowerCase = activeNetworkInfo.getTypeName().toLowerCase();
            return "wifi".equals(lowerCase) ? lowerCase : activeNetworkInfo.getExtraInfo().toLowerCase();
        } catch (Exception e) {
            return "unknow";
        }
    }

    protected static boolean f(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return false;
        }
        return activeNetworkInfo.isRoaming() ? true : true;
    }
}
