package com.winad.android.ads;

import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class AdLocation {
    private static Location a;

    protected static Location a(Context context) {
        String str;
        final LocationManager locationManager;
        boolean z = false;
        BannerLogger.d("lxs", "Trying to get lgetCoordinates");
        if (context != null && a == null) {
            synchronized (context) {
                if (a == null) {
                    if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                        if (Log.isLoggable("lxs", 3)) {
                            Log.d("lxs", "Trying to get locations from the network.");
                        }
                        locationManager = (LocationManager) context.getSystemService("location");
                        if (locationManager != null) {
                            Criteria criteria = new Criteria();
                            criteria.setAccuracy(2);
                            criteria.setCostAllowed(false);
                            str = locationManager.getBestProvider(criteria, true);
                            z = true;
                        } else {
                            str = null;
                            z = true;
                        }
                    } else {
                        str = null;
                        locationManager = null;
                    }
                    if (str == null && context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
                        if (BannerLogger.isLoggable("lxs", 3)) {
                            BannerLogger.d("lxs", "Trying to get locations from GPS.");
                        }
                        locationManager = (LocationManager) context.getSystemService("location");
                        if (locationManager != null) {
                            Criteria criteria2 = new Criteria();
                            criteria2.setAccuracy(1);
                            criteria2.setCostAllowed(false);
                            str = locationManager.getBestProvider(criteria2, true);
                            z = true;
                        } else {
                            z = true;
                        }
                    }
                    if (!z) {
                        if (Log.isLoggable("lxs", 3)) {
                            BannerLogger.d("lxs", "Cannot access user's location.  Permissions are not set.");
                        }
                    } else if (str != null) {
                        BannerLogger.i("lxs", "Location provider setup successfully.");
                        locationManager.requestLocationUpdates(str, 0, 0.0f, new LocationListener() {
                            public void onLocationChanged(Location location) {
                                locationManager.removeUpdates(this);
                            }

                            public void onProviderDisabled(String str) {
                            }

                            public void onProviderEnabled(String str) {
                            }

                            public void onStatusChanged(String str, int i, Bundle bundle) {
                            }
                        }, context.getMainLooper());
                        a = locationManager.getLastKnownLocation(str);
                    } else if (BannerLogger.isLoggable("lxs", 3)) {
                        BannerLogger.d("lxs", "No location providers are available.  Ads will not be geotargeted.");
                    }
                }
            }
        }
        return a;
    }

    static String b(Context context) {
        String str = "";
        Location a2 = a(context);
        if (a2 != null) {
            String str2 = a2.getLatitude() + "," + a2.getLongitude();
            try {
                List<Address> fromLocation = new Geocoder(context, Locale.CHINA).getFromLocation(a2.getLatitude(), a2.getLongitude(), 1);
                if (fromLocation.size() > 0) {
                    Address address = fromLocation.get(0);
                    str = str2 + "#" + address.getAdminArea() + "," + address.getLocality() + "," + address.getThoroughfare();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            str = str2;
        }
        if (BannerLogger.isLoggable("lxs", 3)) {
            BannerLogger.d("lxs", "User coordinates are " + str);
        }
        return str;
    }
}
