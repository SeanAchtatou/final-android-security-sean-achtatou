package com.winad.android.ads;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.adchina.android.ads.Common;
import java.io.File;

class Ad {
    protected static Dialog a;
    /* access modifiers changed from: private */
    public static int i = 50000;
    private static Context k;
    protected String b;
    protected String c;
    protected String d;
    protected String e;
    protected String f;
    protected String g;
    /* access modifiers changed from: private */
    public byte[] h;
    /* access modifiers changed from: private */
    public Context j;
    private String l;
    private String m;
    /* access modifiers changed from: private */
    public String n;
    private String o;
    private String p;
    private String q;
    private byte[] r;
    private byte[] s;
    /* access modifiers changed from: private */
    public NetworkListener t;

    private Ad() {
    }

    private static File a(String str) {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "winAds");
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(file, str);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0119 A[SYNTHETIC, Splitter:B:50:0x0119] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x011e A[SYNTHETIC, Splitter:B:53:0x011e] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0123 A[SYNTHETIC, Splitter:B:56:0x0123] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x016d A[SYNTHETIC, Splitter:B:89:0x016d] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0172 A[SYNTHETIC, Splitter:B:92:0x0172] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0177 A[SYNTHETIC, Splitter:B:95:0x0177] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] b(java.lang.String r9, boolean r10) {
        /*
            r7 = 0
            r3 = 0
            r0 = 47
            int r0 = r9.lastIndexOf(r0)
            int r0 = r0 + 1
            int r1 = r9.length()
            java.lang.String r0 = r9.substring(r0, r1)
            java.lang.String r1 = "."
            int r1 = r0.indexOf(r1)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r0 = r0.substring(r3, r1)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r1 = ".winAd"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.io.File r1 = a(r0)
            java.lang.String r2 = android.os.Environment.getExternalStorageState()
            java.lang.String r3 = "mounted"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0094
            boolean r3 = r1.exists()
            if (r3 == 0) goto L_0x0094
            java.lang.String r0 = "winAd"
            java.lang.String r2 = "the ad picture is exists, so we can read it from  your sdcard"
            com.winad.android.ads.BannerLogger.d(r0, r2)
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream
            r0.<init>()
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x007a, IOException -> 0x008d }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x007a, IOException -> 0x008d }
            long r3 = r1.length()     // Catch:{ FileNotFoundException -> 0x007a, IOException -> 0x008d }
            r5 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r1 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r1 <= 0) goto L_0x0068
            java.lang.String r1 = "TAG"
            java.lang.String r3 = "File is too large....."
            com.winad.android.ads.BannerLogger.v(r1, r3)     // Catch:{ FileNotFoundException -> 0x007a, IOException -> 0x008d }
        L_0x0068:
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]     // Catch:{ FileNotFoundException -> 0x007a, IOException -> 0x008d }
        L_0x006c:
            int r3 = r2.read(r1)     // Catch:{ FileNotFoundException -> 0x007a, IOException -> 0x008d }
            if (r3 <= 0) goto L_0x0081
            r4 = 0
            r0.write(r1, r4, r3)     // Catch:{ FileNotFoundException -> 0x007a, IOException -> 0x008d }
            r0.flush()     // Catch:{ FileNotFoundException -> 0x007a, IOException -> 0x008d }
            goto L_0x006c
        L_0x007a:
            r0 = move-exception
            r1 = r7
        L_0x007c:
            r0.printStackTrace()
            r0 = r1
        L_0x0080:
            return r0
        L_0x0081:
            byte[] r1 = r0.toByteArray()     // Catch:{ FileNotFoundException -> 0x007a, IOException -> 0x008d }
            r0.close()     // Catch:{ FileNotFoundException -> 0x01b3, IOException -> 0x01b0 }
            r2.close()     // Catch:{ FileNotFoundException -> 0x01b3, IOException -> 0x01b0 }
            r0 = r1
            goto L_0x0080
        L_0x008d:
            r0 = move-exception
            r1 = r7
        L_0x008f:
            r0.printStackTrace()
            r0 = r1
            goto L_0x0080
        L_0x0094:
            if (r9 == 0) goto L_0x01b6
            android.content.Context r1 = com.winad.android.ads.Ad.k     // Catch:{ Throwable -> 0x0130, all -> 0x0167 }
            java.lang.String[] r1 = com.winad.android.ads.ConnectType.getWapUrl(r1, r9)     // Catch:{ Throwable -> 0x0130, all -> 0x0167 }
            if (r1 == 0) goto L_0x0129
            java.net.URL r3 = new java.net.URL     // Catch:{ Throwable -> 0x0130, all -> 0x0167 }
            r4 = 0
            r4 = r1[r4]     // Catch:{ Throwable -> 0x0130, all -> 0x0167 }
            r3.<init>(r4)     // Catch:{ Throwable -> 0x0130, all -> 0x0167 }
        L_0x00a6:
            java.net.URLConnection r3 = r3.openConnection()     // Catch:{ Throwable -> 0x0130, all -> 0x0167 }
            if (r1 == 0) goto L_0x00b4
            java.lang.String r4 = "X-Online-Host"
            r5 = 1
            r1 = r1[r5]     // Catch:{ Throwable -> 0x0130, all -> 0x0167 }
            r3.setRequestProperty(r4, r1)     // Catch:{ Throwable -> 0x0130, all -> 0x0167 }
        L_0x00b4:
            r1 = 50000(0xc350, float:7.0065E-41)
            r3.setConnectTimeout(r1)     // Catch:{ Throwable -> 0x0130, all -> 0x0167 }
            r1 = 50000(0xc350, float:7.0065E-41)
            r3.setReadTimeout(r1)     // Catch:{ Throwable -> 0x0130, all -> 0x0167 }
            r3.setUseCaches(r10)     // Catch:{ Throwable -> 0x0130, all -> 0x0167 }
            r3.connect()     // Catch:{ Throwable -> 0x0130, all -> 0x0167 }
            java.io.InputStream r1 = r3.getInputStream()     // Catch:{ Throwable -> 0x0130, all -> 0x0167 }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Throwable -> 0x01a2, all -> 0x0189 }
            r3.<init>()     // Catch:{ Throwable -> 0x01a2, all -> 0x0189 }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r4]     // Catch:{ Throwable -> 0x015a, all -> 0x018e }
            if (r2 == 0) goto L_0x014c
            java.lang.String r2 = "winAd"
            java.lang.String r5 = " the sdcard is exsit, so we sava it"
            com.winad.android.ads.BannerLogger.d(r2, r5)     // Catch:{ Throwable -> 0x015a, all -> 0x018e }
            java.io.File r0 = a(r0)     // Catch:{ Throwable -> 0x015a, all -> 0x018e }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Throwable -> 0x015a, all -> 0x018e }
            r2.<init>(r0)     // Catch:{ Throwable -> 0x015a, all -> 0x018e }
        L_0x00e5:
            int r0 = r1.read(r4)     // Catch:{ Throwable -> 0x00fa, all -> 0x0193 }
            if (r0 <= 0) goto L_0x0135
            r5 = 0
            r2.write(r4, r5, r0)     // Catch:{ Throwable -> 0x00fa, all -> 0x0193 }
            r2.flush()     // Catch:{ Throwable -> 0x00fa, all -> 0x0193 }
            r5 = 0
            r3.write(r4, r5, r0)     // Catch:{ Throwable -> 0x00fa, all -> 0x0193 }
            r3.flush()     // Catch:{ Throwable -> 0x00fa, all -> 0x0193 }
            goto L_0x00e5
        L_0x00fa:
            r0 = move-exception
            r8 = r2
            r2 = r3
            r3 = r1
            r1 = r8
        L_0x00ff:
            java.lang.String r4 = "winAd"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x01a0 }
            r5.<init>()     // Catch:{ all -> 0x01a0 }
            java.lang.String r6 = "Problem getting image:  "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x01a0 }
            java.lang.StringBuilder r5 = r5.append(r9)     // Catch:{ all -> 0x01a0 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x01a0 }
            android.util.Log.w(r4, r5, r0)     // Catch:{ all -> 0x01a0 }
            if (r3 == 0) goto L_0x011c
            r3.close()     // Catch:{ IOException -> 0x017f }
        L_0x011c:
            if (r2 == 0) goto L_0x0121
            r2.close()     // Catch:{ IOException -> 0x0181 }
        L_0x0121:
            if (r1 == 0) goto L_0x01b6
            r1.close()     // Catch:{ IOException -> 0x0163 }
            r0 = r7
            goto L_0x0080
        L_0x0129:
            java.net.URL r3 = new java.net.URL     // Catch:{ Throwable -> 0x0130, all -> 0x0167 }
            r3.<init>(r9)     // Catch:{ Throwable -> 0x0130, all -> 0x0167 }
            goto L_0x00a6
        L_0x0130:
            r0 = move-exception
            r1 = r7
            r2 = r7
            r3 = r7
            goto L_0x00ff
        L_0x0135:
            r0 = r2
        L_0x0136:
            byte[] r2 = r3.toByteArray()     // Catch:{ Throwable -> 0x01a8, all -> 0x0199 }
            if (r1 == 0) goto L_0x013f
            r1.close()     // Catch:{ IOException -> 0x017b }
        L_0x013f:
            if (r3 == 0) goto L_0x0144
            r3.close()     // Catch:{ IOException -> 0x017d }
        L_0x0144:
            if (r0 == 0) goto L_0x01b9
            r0.close()     // Catch:{ IOException -> 0x015f }
            r0 = r2
            goto L_0x0080
        L_0x014c:
            int r0 = r1.read(r4)     // Catch:{ Throwable -> 0x015a, all -> 0x018e }
            if (r0 <= 0) goto L_0x01bc
            r2 = 0
            r3.write(r4, r2, r0)     // Catch:{ Throwable -> 0x015a, all -> 0x018e }
            r3.flush()     // Catch:{ Throwable -> 0x015a, all -> 0x018e }
            goto L_0x014c
        L_0x015a:
            r0 = move-exception
            r2 = r3
            r3 = r1
            r1 = r7
            goto L_0x00ff
        L_0x015f:
            r0 = move-exception
            r0 = r2
            goto L_0x0080
        L_0x0163:
            r0 = move-exception
            r0 = r7
            goto L_0x0080
        L_0x0167:
            r0 = move-exception
            r1 = r7
            r2 = r7
            r3 = r7
        L_0x016b:
            if (r3 == 0) goto L_0x0170
            r3.close()     // Catch:{ IOException -> 0x0183 }
        L_0x0170:
            if (r2 == 0) goto L_0x0175
            r2.close()     // Catch:{ IOException -> 0x0185 }
        L_0x0175:
            if (r1 == 0) goto L_0x017a
            r1.close()     // Catch:{ IOException -> 0x0187 }
        L_0x017a:
            throw r0
        L_0x017b:
            r1 = move-exception
            goto L_0x013f
        L_0x017d:
            r1 = move-exception
            goto L_0x0144
        L_0x017f:
            r0 = move-exception
            goto L_0x011c
        L_0x0181:
            r0 = move-exception
            goto L_0x0121
        L_0x0183:
            r3 = move-exception
            goto L_0x0170
        L_0x0185:
            r2 = move-exception
            goto L_0x0175
        L_0x0187:
            r1 = move-exception
            goto L_0x017a
        L_0x0189:
            r0 = move-exception
            r2 = r7
            r3 = r1
            r1 = r7
            goto L_0x016b
        L_0x018e:
            r0 = move-exception
            r2 = r3
            r3 = r1
            r1 = r7
            goto L_0x016b
        L_0x0193:
            r0 = move-exception
            r8 = r2
            r2 = r3
            r3 = r1
            r1 = r8
            goto L_0x016b
        L_0x0199:
            r2 = move-exception
            r8 = r2
            r2 = r3
            r3 = r1
            r1 = r0
            r0 = r8
            goto L_0x016b
        L_0x01a0:
            r0 = move-exception
            goto L_0x016b
        L_0x01a2:
            r0 = move-exception
            r2 = r7
            r3 = r1
            r1 = r7
            goto L_0x00ff
        L_0x01a8:
            r2 = move-exception
            r8 = r2
            r2 = r3
            r3 = r1
            r1 = r0
            r0 = r8
            goto L_0x00ff
        L_0x01b0:
            r0 = move-exception
            goto L_0x008f
        L_0x01b3:
            r0 = move-exception
            goto L_0x007c
        L_0x01b6:
            r0 = r7
            goto L_0x0080
        L_0x01b9:
            r0 = r2
            goto L_0x0080
        L_0x01bc:
            r0 = r7
            goto L_0x0136
        */
        throw new UnsupportedOperationException("Method not decompiled: com.winad.android.ads.Ad.b(java.lang.String, boolean):byte[]");
    }

    public static Ad createAd(Context context, String str, String str2) {
        if (str == null || str.equals("")) {
            return null;
        }
        Ad ad = new Ad();
        ad.j = context;
        k = context;
        try {
            AdContentInfo c2 = JsonUtils.c(str);
            BannerLogger.d("winAd", "the ad result_message is  " + str);
            if (c2 == null) {
                BannerLogger.d("winAd", "you request result_message of ad is error");
                return null;
            }
            ad.d = c2.getStylePicUrl();
            ad.m = c2.getStyleText();
            ad.b = c2.getStyleTitle();
            ad.c = c2.getClickeffectTypePic();
            ad.n = c2.getClickeffectUrl();
            ad.p = c2.getClickeffectTypeId();
            ad.q = c2.getStyleType();
            ad.f = c2.getApplyId();
            ad.e = c2.getAd_id();
            ad.g = c2.getRandomCode();
            return ad;
        } catch (Exception e2) {
            Log.e("winAd", "Failed to parse ad response:  " + str, e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public byte[] a() {
        if (this.s == null) {
            this.s = b(this.d, true);
            if (this.s == null) {
                BannerLogger.w("winAd", "Could not get icon for ad from " + this.d);
            }
        }
        return this.s;
    }

    public void clicked(final Handler handler) {
        final Message message = new Message();
        final Bundle bundle = new Bundle();
        if (this.t != null) {
            this.t.onNetworkActivityStart();
        }
        if (Common.KIMP.equals(this.p)) {
            message.what = 2;
            if (this.h == null) {
                a = new CustomProgressDilog(this.j).a();
                a.show();
                new Thread() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.winad.android.ads.Ad.a(java.lang.String, boolean):byte[]
                     arg types: [java.lang.String, int]
                     candidates:
                      com.winad.android.ads.Ad.a(com.winad.android.ads.Ad, byte[]):byte[]
                      com.winad.android.ads.Ad.a(java.lang.String, boolean):byte[] */
                    public void run() {
                        byte[] unused = Ad.this.h = Ad.b(Ad.this.n, true);
                        if (handler != null) {
                            bundle.putByteArray("lookImage", Ad.this.h);
                            bundle.putString("sendAim", Ad.this.n);
                            message.setData(bundle);
                            handler.sendMessage(message);
                        }
                    }
                }.start();
            } else {
                bundle.putByteArray("lookImage", this.h);
                bundle.putString("sendAim", this.n);
                message.setData(bundle);
                handler.sendMessage(message);
            }
        } else if ("3".equals(this.p)) {
            bundle.putString("sendAim", this.n);
            message.setData(bundle);
            message.what = 3;
            handler.sendMessage(message);
        } else if ("5".equals(this.p)) {
            bundle.putString("explainText", this.n);
            bundle.putString("sendAim", this.n);
            message.setData(bundle);
            message.what = 5;
            handler.sendMessage(message);
        } else if ("4".equals(this.p)) {
            bundle.putString("explainText", getText());
            bundle.putString("sendAim", this.n);
            message.setData(bundle);
            message.what = 4;
            handler.sendMessage(message);
        } else if ("6".equals(this.p)) {
            bundle.putString("sendAim", this.n);
            message.setData(bundle);
            message.what = 6;
            handler.sendMessage(message);
        } else if ("7".equals(this.p)) {
            bundle.putString("sendAim", this.n);
            message.setData(bundle);
            message.what = 7;
            handler.sendMessage(message);
        } else if ("9".equals(this.p)) {
            bundle.putString("explainText", getText());
            bundle.putString("sendAim", this.n);
            message.setData(bundle);
            message.what = 9;
            handler.sendMessage(message);
        } else if ("8".equals(this.p)) {
            bundle.putString("explainText", getText());
            bundle.putString("sendAim", this.n);
            message.setData(bundle);
            message.what = 8;
            handler.sendMessage(message);
        } else if (this.n != null) {
            new Thread() {
                /* JADX WARNING: Removed duplicated region for block: B:12:0x0061  */
                /* JADX WARNING: Removed duplicated region for block: B:15:0x0086  */
                /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r7 = this;
                        r5 = 1
                        r0 = 0
                        java.net.URL r1 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0090, IOException -> 0x00b4 }
                        com.winad.android.ads.Ad r2 = com.winad.android.ads.Ad.this     // Catch:{ MalformedURLException -> 0x0090, IOException -> 0x00b4 }
                        java.lang.String r2 = r2.n     // Catch:{ MalformedURLException -> 0x0090, IOException -> 0x00b4 }
                        r1.<init>(r2)     // Catch:{ MalformedURLException -> 0x0090, IOException -> 0x00b4 }
                        r0 = 1
                        java.net.HttpURLConnection.setFollowRedirects(r0)     // Catch:{ MalformedURLException -> 0x00df, IOException -> 0x00d8 }
                        java.net.URLConnection r0 = r1.openConnection()     // Catch:{ MalformedURLException -> 0x00df, IOException -> 0x00d8 }
                        java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x00df, IOException -> 0x00d8 }
                        int r2 = com.winad.android.ads.Ad.i     // Catch:{ MalformedURLException -> 0x00df, IOException -> 0x00d8 }
                        r0.setConnectTimeout(r2)     // Catch:{ MalformedURLException -> 0x00df, IOException -> 0x00d8 }
                        int r2 = com.winad.android.ads.Ad.i     // Catch:{ MalformedURLException -> 0x00df, IOException -> 0x00d8 }
                        r0.setReadTimeout(r2)     // Catch:{ MalformedURLException -> 0x00df, IOException -> 0x00d8 }
                        java.lang.String r2 = "X-WINAD-ISU"
                        com.winad.android.ads.Ad r3 = com.winad.android.ads.Ad.this     // Catch:{ MalformedURLException -> 0x00df, IOException -> 0x00d8 }
                        android.content.Context r3 = r3.j     // Catch:{ MalformedURLException -> 0x00df, IOException -> 0x00d8 }
                        java.lang.String r3 = com.winad.android.ads.AdManager.getUserId(r3)     // Catch:{ MalformedURLException -> 0x00df, IOException -> 0x00d8 }
                        r0.setRequestProperty(r2, r3)     // Catch:{ MalformedURLException -> 0x00df, IOException -> 0x00d8 }
                        r0.connect()     // Catch:{ MalformedURLException -> 0x00df, IOException -> 0x00d8 }
                        r0.getResponseCode()     // Catch:{ MalformedURLException -> 0x00df, IOException -> 0x00d8 }
                        java.net.URL r0 = r0.getURL()     // Catch:{ MalformedURLException -> 0x00df, IOException -> 0x00d8 }
                        java.lang.String r1 = "winAd"
                        r2 = 3
                        boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ MalformedURLException -> 0x00e1, IOException -> 0x00da }
                        if (r1 == 0) goto L_0x005f
                        java.lang.String r1 = "winAd"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x00e1, IOException -> 0x00da }
                        r2.<init>()     // Catch:{ MalformedURLException -> 0x00e1, IOException -> 0x00da }
                        java.lang.String r3 = "Final click destination URL:  "
                        java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x00e1, IOException -> 0x00da }
                        java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ MalformedURLException -> 0x00e1, IOException -> 0x00da }
                        java.lang.String r2 = r2.toString()     // Catch:{ MalformedURLException -> 0x00e1, IOException -> 0x00da }
                        com.winad.android.ads.BannerLogger.d(r1, r2)     // Catch:{ MalformedURLException -> 0x00e1, IOException -> 0x00da }
                    L_0x005f:
                        if (r0 == 0) goto L_0x007e
                        android.os.Bundle r1 = r1
                        java.lang.String r2 = "sendAim"
                        java.lang.String r0 = r0.toString()
                        r1.putString(r2, r0)
                        android.os.Message r0 = r0
                        android.os.Bundle r1 = r1
                        r0.setData(r1)
                        android.os.Message r0 = r0
                        r0.what = r5
                        android.os.Handler r0 = r5
                        android.os.Message r1 = r0
                        r0.sendMessage(r1)
                    L_0x007e:
                        com.winad.android.ads.Ad r0 = com.winad.android.ads.Ad.this
                        com.winad.android.ads.NetworkListener r0 = r0.t
                        if (r0 == 0) goto L_0x008f
                        com.winad.android.ads.Ad r0 = com.winad.android.ads.Ad.this
                        com.winad.android.ads.NetworkListener r0 = r0.t
                        r0.onNetworkActivityEnd()
                    L_0x008f:
                        return
                    L_0x0090:
                        r1 = move-exception
                        r6 = r1
                        r1 = r0
                        r0 = r6
                    L_0x0094:
                        java.lang.String r2 = "winAd"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder
                        r3.<init>()
                        java.lang.String r4 = "Malformed click URL.  Will try to follow anyway.  "
                        java.lang.StringBuilder r3 = r3.append(r4)
                        com.winad.android.ads.Ad r4 = com.winad.android.ads.Ad.this
                        java.lang.String r4 = r4.n
                        java.lang.StringBuilder r3 = r3.append(r4)
                        java.lang.String r3 = r3.toString()
                        android.util.Log.w(r2, r3, r0)
                        r0 = r1
                        goto L_0x005f
                    L_0x00b4:
                        r1 = move-exception
                        r6 = r1
                        r1 = r0
                        r0 = r6
                    L_0x00b8:
                        java.lang.String r2 = "winAd"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder
                        r3.<init>()
                        java.lang.String r4 = "Could not determine final click destination URL.  Will try to follow anyway.  "
                        java.lang.StringBuilder r3 = r3.append(r4)
                        com.winad.android.ads.Ad r4 = com.winad.android.ads.Ad.this
                        java.lang.String r4 = r4.n
                        java.lang.StringBuilder r3 = r3.append(r4)
                        java.lang.String r3 = r3.toString()
                        android.util.Log.w(r2, r3, r0)
                        r0 = r1
                        goto L_0x005f
                    L_0x00d8:
                        r0 = move-exception
                        goto L_0x00b8
                    L_0x00da:
                        r1 = move-exception
                        r6 = r1
                        r1 = r0
                        r0 = r6
                        goto L_0x00b8
                    L_0x00df:
                        r0 = move-exception
                        goto L_0x0094
                    L_0x00e1:
                        r1 = move-exception
                        r6 = r1
                        r1 = r0
                        r0 = r6
                        goto L_0x0094
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.winad.android.ads.Ad.AnonymousClass2.run():void");
                }
            }.start();
        }
        if (this.t != null) {
            this.t.onNetworkActivityEnd();
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof Ad) {
            return toString().equals(((Ad) obj).toString());
        }
        return false;
    }

    public String getAdId() {
        return this.e;
    }

    public String getClickPicURL() {
        return this.o;
    }

    public String getClickURL() {
        return this.n;
    }

    public String getHTML() {
        return this.l;
    }

    public byte[] getImage() {
        if (this.r == null && this.c != null) {
            this.r = b(this.c, true);
        }
        return this.r;
    }

    public String getImageURL() {
        return this.c;
    }

    public NetworkListener getNetworkListener() {
        return this.t;
    }

    public String getStyleTitle() {
        return this.b;
    }

    public String getText() {
        return this.m;
    }

    public String getType() {
        return this.p;
    }

    public String getshowType() {
        return this.q;
    }

    public boolean hasImage() {
        return this.c != null;
    }

    public int hashCode() {
        return toString().hashCode();
    }

    public void setNetworkListener(NetworkListener networkListener) {
        this.t = networkListener;
    }

    public String toString() {
        String adId = getAdId();
        return adId == null ? "" : adId;
    }
}
