package com.winad.android.ads;

public class AdContentInfo {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;

    public String getAd_id() {
        return this.b;
    }

    public String getApplyId() {
        return this.a;
    }

    public String getClickeffectPicUrl() {
        return this.h;
    }

    public String getClickeffectTypeId() {
        return this.f;
    }

    public String getClickeffectTypePic() {
        return this.j;
    }

    public String getClickeffectUrl() {
        return this.g;
    }

    public String getRandomCode() {
        return this.i;
    }

    public String getStylePicUrl() {
        return this.e;
    }

    public String getStyleText() {
        return this.d;
    }

    public String getStyleTitle() {
        return this.k;
    }

    public String getStyleType() {
        return this.c;
    }

    public void setAd_id(String str) {
        this.b = str;
    }

    public void setApplyId(String str) {
        this.a = str;
    }

    public void setClickeffectPicUrl(String str) {
        this.h = str;
    }

    public void setClickeffectTypeId(String str) {
        this.f = str;
    }

    public void setClickeffectTypePic(String str) {
        this.j = str;
    }

    public void setClickeffectUrl(String str) {
        this.g = str;
    }

    public void setRandomCode(String str) {
        this.i = str;
    }

    public void setStylePicUrl(String str) {
        this.e = str;
    }

    public void setStyleText(String str) {
        this.d = str;
    }

    public void setStyleTitle(String str) {
        this.k = str;
    }

    public void setStyleType(String str) {
        this.c = str;
    }
}
