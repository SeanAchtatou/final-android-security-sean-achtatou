package com.winad.android.ads;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.GregorianCalendar;
import java.util.Random;

public class AdManager {
    protected static int a = 1;
    protected static int b = 1;
    protected static int c = 1;
    private static String d;
    private static String e;
    private static boolean f;
    private static String g;
    private static GregorianCalendar h;
    private static Gender i;

    public static int NextInt(int i2, int i3) {
        return (Math.abs(new Random().nextInt()) % ((i3 - i2) + 1)) + i2;
    }

    protected static String a(Context context) {
        if (e == null) {
            try {
                ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
                if (applicationInfo != null) {
                    String string = applicationInfo.metaData.getString("PUBLISHER_ID");
                    BannerLogger.d("winAd", "Publisher ID read from AndroidManifest.xml is " + string);
                    if (!string.equals("A92DE2001E8F59F19056121A148") || !context.getPackageName().equals("com.dd.android.ads")) {
                        setPublisherId(string);
                    } else {
                        BannerLogger.i("winAd", "This is a sample application so allowing sample publisher ID.");
                        e = string;
                    }
                }
            } catch (Exception e2) {
                Log.e("winAd", "Could not read DD_PUBLISHER_ID meta-data from AndroidManifest.xml.", e2);
            }
        }
        return e;
    }

    protected static String a(Ad ad, Context context, int i2) {
        String str;
        AdMark instance = AdMark.getInstance(context);
        if (instance.getDate("sumbitTime") == null) {
            instance.setKeepData("sumbitTime", AdMd5.a(1 + String.valueOf(NextInt(100000, 999999)) + String.valueOf(System.currentTimeMillis())));
        }
        String date = instance.getDate("sumbitTime");
        StringBuffer stringBuffer = new StringBuffer();
        int i3 = f ? 1 : 0;
        String b2 = AdLocation.b(context);
        String str2 = "";
        if (!"".equals(b2)) {
            String[] split = b2.split("#");
            if (split.length > 1) {
                String str3 = split[0];
                String str4 = split[1];
                str2 = str3;
                str = str4;
            } else {
                str2 = b2;
                str = "";
            }
            try {
                str = URLEncoder.encode(str, "UTF-8");
            } catch (UnsupportedEncodingException e2) {
            }
        } else {
            str = "";
        }
        stringBuffer.append("{");
        stringBuffer.append(ad.g);
        stringBuffer.append(";");
        stringBuffer.append(ad.f);
        stringBuffer.append(";");
        stringBuffer.append(ad.e);
        stringBuffer.append(";");
        stringBuffer.append(i2);
        String str5 = Build.MODEL;
        String c2 = Utilities.c(context);
        if ("000000000000000".equals(c2) || "sdk".equalsIgnoreCase(str5)) {
            c2 = "000000";
        }
        stringBuffer.append(";");
        stringBuffer.append(c2);
        stringBuffer.append(";");
        String d2 = Utilities.d(context);
        stringBuffer.append(d2);
        stringBuffer.append(";");
        if (str5.length() > 0) {
            stringBuffer.append(str5);
        } else {
            stringBuffer.append("");
        }
        String b3 = Utilities.b(context);
        stringBuffer.append(";");
        stringBuffer.append(b3);
        stringBuffer.append(";");
        stringBuffer.append(c);
        String a2 = Utilities.a();
        stringBuffer.append(";");
        stringBuffer.append(a2);
        stringBuffer.append(";");
        stringBuffer.append(str2);
        String a3 = Utilities.a(context);
        stringBuffer.append(";");
        stringBuffer.append(a3);
        String e3 = Utilities.e(context);
        stringBuffer.append(";");
        stringBuffer.append(e3);
        stringBuffer.append(";");
        stringBuffer.append("");
        stringBuffer.append(";");
        stringBuffer.append(i3);
        stringBuffer.append(";");
        stringBuffer.append(date);
        stringBuffer.append(";");
        stringBuffer.append(a);
        stringBuffer.append(";");
        stringBuffer.append(str);
        stringBuffer.append(";");
        stringBuffer.append(b);
        stringBuffer.append(";");
        stringBuffer.append(AdMd5.a(ad.f + ad.e + i2 + c2 + d2 + str5 + b3 + c + a2 + str2 + a3 + e3 + "" + i3 + date + a));
        stringBuffer.append("}");
        d = stringBuffer.toString();
        if (Log.isLoggable("winAd", 3)) {
            BannerLogger.d("winAd", "Phone's user-agent is:  " + d);
        }
        return d;
    }

    protected static void a(String str) {
        Log.e("winAd", str);
        throw new IllegalArgumentException(str);
    }

    public static GregorianCalendar getBirthday() {
        return h;
    }

    public static Gender getGender() {
        return i;
    }

    public static String getUserId(Context context) {
        if (g == null) {
            g = Settings.System.getString(context.getContentResolver(), "android_id");
            g = AdMd5.a(g);
            BannerLogger.i("winAd", "The user ID is " + g);
        }
        return g;
    }

    public static boolean isInTestMode() {
        return f;
    }

    public static void setBirthday(int i2, int i3, int i4) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(i2, i3 - 1, i4);
        setBirthday(gregorianCalendar);
    }

    public static void setBirthday(GregorianCalendar gregorianCalendar) {
        h = gregorianCalendar;
    }

    public static void setGender(Gender gender) {
        i = gender;
    }

    public static void setInTestMode(boolean z) {
        f = z;
    }

    public static void setPublisherId(String str) {
        if (str.equalsIgnoreCase("A92DE2001E8F59F19056121A148")) {
            a("SETUP ERROR:  Cannot use the sample publisher ID (A92DE2001E8F59F19056121A148).  Yours is available on www.winad.com.");
        }
        BannerLogger.i("winAd", "Publisher ID set to " + str);
        e = str;
    }
}
