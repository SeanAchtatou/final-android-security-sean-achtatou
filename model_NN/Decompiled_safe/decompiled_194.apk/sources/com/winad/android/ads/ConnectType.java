package com.winad.android.ads;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectType {
    public static String[] getWap(String str, String str2) {
        int indexOf = str.indexOf("//") + 2;
        int indexOf2 = str.indexOf(47, indexOf);
        if (indexOf2 == -1) {
            return null;
        }
        return new String[]{str.substring(0, indexOf) + str2 + str.substring(indexOf2), str.substring(indexOf, indexOf2)};
    }

    public static String[] getWapUrl(Context context, String str) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if ("wifi".equals(activeNetworkInfo.getTypeName().toLowerCase())) {
                return null;
            }
            String lowerCase = activeNetworkInfo.getExtraInfo().toLowerCase();
            if ("cmwap".equalsIgnoreCase(lowerCase) || "uniwap".equalsIgnoreCase(lowerCase)) {
                return getWap(str, "10.0.0.172:80");
            }
            if ("ctwap".equalsIgnoreCase(lowerCase)) {
                return getWap(str, "10.0.0.200:80");
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
