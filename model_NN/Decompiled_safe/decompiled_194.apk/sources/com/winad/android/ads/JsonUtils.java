package com.winad.android.ads;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtils {
    public static final String ERROR_ACK = "error";
    public static final String LOG_TAG = "JsonUtils";

    protected static String a(String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        try {
            return new JSONObject(str.toString()).getJSONObject("ad").getJSONObject("head").getString("appid");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    protected static JSONObject b(String str) {
        if (!(str == null || str.length() == 0)) {
            try {
                return new JSONObject(str.toString()).getJSONObject("ad").getJSONObject("body");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    protected static AdContentInfo c(String str) {
        if (!(str == null || str.length() == 0)) {
            AdContentInfo adContentInfo = new AdContentInfo();
            try {
                String a = a(str);
                if (ERROR_ACK.equalsIgnoreCase(a)) {
                    return null;
                }
                adContentInfo.setApplyId(a);
                JSONObject b = b(str);
                adContentInfo.setAd_id(b.getString("id"));
                adContentInfo.setStyleType(b.getString("styleTypeId"));
                adContentInfo.setStyleTitle(b.getString("styleTitle"));
                adContentInfo.setClickeffectTypePic(b.getString("clickeffectTypePic"));
                adContentInfo.setStyleText(b.getString("styleText"));
                adContentInfo.setStylePicUrl(b.getString("stylePicUrl"));
                adContentInfo.setClickeffectTypeId(b.getString("clickeffectTypeId"));
                adContentInfo.setClickeffectUrl(b.getString("clickeffectContent"));
                adContentInfo.setRandomCode(b.getString("randomCode"));
                return adContentInfo;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
