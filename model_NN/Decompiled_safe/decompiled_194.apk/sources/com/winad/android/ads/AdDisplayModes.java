package com.winad.android.ads;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

public class AdDisplayModes {
    protected static void a(Context context, String str) {
        try {
            Intent intent = new Intent("android.intent.action.SEND");
            String[] split = str.split("#");
            if (split.length > 2) {
                String str2 = "";
                for (int length = split.length - 3; length >= 0; length--) {
                    str2 = str2 + split[length] + ",";
                }
                intent.putExtra("android.intent.extra.EMAIL", new String[]{str2});
                intent.putExtra("android.intent.extra.SUBJECT", split[split.length - 2]);
                intent.putExtra("android.intent.extra.TEXT", split[split.length - 1]);
            }
            intent.setType("message/rfc822");
            context.startActivity(Intent.createChooser(intent, "选择一个Email客户端"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected static void a(Context context, String str, String str2) {
        try {
            Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + str2));
            intent.addFlags(268435456);
            intent.putExtra("sms_body", str);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected static void b(Context context, String str) {
        try {
            Intent intent = new Intent("android.intent.action.DIAL", Uri.parse("tel:" + str));
            intent.addFlags(268435456);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected static void c(Context context, String str) {
        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.WEB_SEARCH");
            intent.putExtra("query", str);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected static void d(Context context, String str) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://ditu.google.com/maps?q=" + str + "&z=15&cbp=1"));
            intent.addFlags(0);
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            context.startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(context, "未安装Google地图,不能为您定位", 1).show();
            e.printStackTrace();
        }
    }

    protected static void e(Context context, String str) {
        Intent intent = new Intent(context, VideoPlayerActivity.class);
        intent.putExtra("URL", str);
        context.startActivity(intent);
    }
}
