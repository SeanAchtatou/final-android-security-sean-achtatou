package com.winad.android.ads;

public interface ADListener {
    void onFailedToReceiveAd(AdView adView);

    void onReceiveAd(AdView adView);
}
