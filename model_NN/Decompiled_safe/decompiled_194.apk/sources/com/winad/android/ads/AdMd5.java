package com.winad.android.ads;

import android.util.Log;
import java.math.BigInteger;
import java.security.MessageDigest;

public class AdMd5 {
    protected static String a(String str) {
        String str2 = null;
        if (str == null || str.length() <= 0) {
            return str2;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes(), 0, str.length());
            return String.format("%032X", new BigInteger(1, instance.digest()));
        } catch (Exception e) {
            Log.d("lxs", "Could not generate hash of " + str, e);
            return str2.substring(0, 32);
        }
    }
}
