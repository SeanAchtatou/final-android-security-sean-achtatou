package com.winad.android.ads;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class CustomProgressDilog {
    final Handler a = new Handler() {
        public void handleMessage(Message message) {
            int i = message.getData().getInt("total");
            CustomProgressDilog.this.c.setProgress(i);
            if (i >= 100) {
                CustomProgressDilog.this.c.dismiss();
                CustomProgressDilog.this.b.a(0);
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressThread b;
    /* access modifiers changed from: private */
    public ProgressDialog c;
    private Context d;

    class ProgressThread extends Thread {
        Handler a;
        int b;
        int c;

        ProgressThread(Handler handler) {
            this.a = handler;
        }

        /* access modifiers changed from: private */
        public void a(int i) {
            this.b = i;
        }

        public void run() {
            this.b = 1;
            this.c = 0;
            while (this.b == 1) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    Log.e("ERROR", "Thread Interrupted");
                }
                Message obtainMessage = this.a.obtainMessage();
                Bundle bundle = new Bundle();
                bundle.putInt("total", this.c);
                obtainMessage.setData(bundle);
                this.a.sendMessage(obtainMessage);
                this.c++;
            }
        }
    }

    public CustomProgressDilog(Context context) {
        this.d = context;
    }

    /* access modifiers changed from: protected */
    public ProgressDialog a() {
        try {
            this.c = new ProgressDialog(this.d);
            this.c.setTitle("乐点广告");
            this.c.setMessage("正在加载");
            this.c.setIndeterminate(false);
            this.c.setCancelable(true);
            this.c.setProgressStyle(1);
            this.b = new ProgressThread(this.a);
            this.b.start();
            return this.c;
        } catch (Exception e) {
            e.printStackTrace();
            return this.c;
        }
    }
}
