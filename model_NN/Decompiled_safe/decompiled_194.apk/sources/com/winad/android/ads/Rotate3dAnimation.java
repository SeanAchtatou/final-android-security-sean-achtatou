package com.winad.android.ads;

import android.app.Activity;
import android.content.Context;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.util.DisplayMetrics;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class Rotate3dAnimation extends Animation {
    private float a;
    private float b;
    private float c;
    private float d;
    private float e;
    private Camera f;
    private Activity g;
    private DisplayMetrics h = null;
    private boolean i = false;

    public Rotate3dAnimation(float f2, float f3, float f4, float f5, float f6, Context context, boolean z) {
        this.i = z;
        this.a = f2;
        this.b = f3;
        this.c = f4;
        this.d = f5;
        this.e = f6;
        this.g = (Activity) context;
        BannerLogger.e("lxs", "mCenterX = " + this.c + " , mCenterY = " + this.d);
        this.h = new DisplayMetrics();
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f2, Transformation transformation) {
        this.g.getWindowManager().getDefaultDisplay().getMetrics(this.h);
        float f3 = (float) (this.h.widthPixels >> 1);
        float f4 = (float) (this.h.heightPixels >> 1);
        float f5 = this.a;
        float f6 = f5 + ((this.b - f5) * f2);
        Camera camera = this.f;
        Matrix matrix = transformation.getMatrix();
        camera.save();
        if (this.i) {
            camera.translate(0.0f, 0.0f, this.e * f2);
        } else {
            camera.translate(0.0f, 0.0f, this.e * (1.0f - f2));
        }
        camera.rotateY(f6);
        camera.getMatrix(matrix);
        camera.restore();
        matrix.preTranslate(-f3, -f4);
        matrix.postTranslate(f3, f4);
    }

    public void initialize(int i2, int i3, int i4, int i5) {
        super.initialize(i2, i3, i4, i5);
        this.f = new Camera();
    }
}
