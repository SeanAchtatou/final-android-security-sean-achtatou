package com.winad.android.ads;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class WinadUtil {
    /* access modifiers changed from: private */
    public static Handler a = new Handler() {
        public void handleMessage(Message message) {
            final Context context = (Context) message.obj;
            Bundle data = message.getData();
            String string = data.getString("versionName");
            String string2 = data.getString("oldVersion");
            final String string3 = data.getString("appUrl");
            final String obj = context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("更新提示").setMessage(string.equals("") ? "检测到有新版本发布，是否更新？" : "检测到有新版本" + string + "发布，当前版本" + string2 + ",是否更新？").setPositiveButton("更新", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (!Utilities.f(context)) {
                        Toast.makeText(context, "没有可用的网络，请连接后重试", 1).show();
                    } else if (Environment.getExternalStorageState().equals("mounted")) {
                        DownLoad downLoad = new DownLoad(context, string3, obj, "WinadupdateAppid");
                        downLoad.setOnPanelListener(new OnDownLoadListener() {
                            public void downLoadCompleted() {
                            }

                            public void downLoadFalse() {
                            }
                        });
                        downLoad.execute(string3);
                        Toast.makeText(context, "开始下载新版本", 1).show();
                    } else {
                        Toast.makeText(context, "请插入您的储存卡", 1).show();
                    }
                }
            }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).create();
            builder.show();
        }
    };

    public static void checkUpdate(final Context context) {
        new Thread() {
            public void run() {
                try {
                    PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                    String a2 = AdRequester.a(context, "http://www.winads.cn/updateApp.action", AdRequester.a(context, packageInfo));
                    BannerLogger.d("xhc", "UpdageApp is  " + a2);
                    if (a2 != null && !a2.equals(JsonUtils.ERROR_ACK)) {
                        Message obtain = Message.obtain();
                        String[] split = a2.split("\\|");
                        Bundle bundle = new Bundle();
                        Log.e("xhc", "str length " + split.length);
                        if (split.length > 1) {
                            bundle.putString("versionName", split[0]);
                            bundle.putString("appUrl", split[1]);
                            bundle.putString("oldVersion", packageInfo.versionName);
                            obtain.setData(bundle);
                            obtain.obj = context;
                            WinadUtil.a.sendMessage(obtain);
                        }
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
}
