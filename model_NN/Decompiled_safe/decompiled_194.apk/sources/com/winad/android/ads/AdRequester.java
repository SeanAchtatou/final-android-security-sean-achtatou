package com.winad.android.ads;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.util.Log;
import com.adchina.android.ads.Common;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

class AdRequester {
    static Ad a = null;
    static long b = System.currentTimeMillis();
    private static String c = null;
    private static int d = 30000;

    AdRequester() {
    }

    static String a(Context context, PackageInfo packageInfo) {
        String a2 = AdManager.a(context);
        if (a2 == null) {
            throw new IllegalStateException("Publisher ID is not set!  To serve ads you must set your publisher ID assigned from www.DD.com.  Either add it to AndroidManifest.xml under the <application> tag or call AdManager.setPublisherId().");
        }
        Log.e("xhc", packageInfo.packageName + "|" + packageInfo.versionCode + "|" + packageInfo.versionName + "|");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("?version=1&").append("appId=").append(a2).append("&").append("pname=").append("com.ledian").append("&").append("versionName=").append(packageInfo.versionName).append("&").append("versionCode=").append(packageInfo.versionCode).append("&").append("os_name=").append(Common.KCLK);
        return stringBuffer.toString();
    }

    protected static String a(Context context, Boolean bool, String str, String str2) {
        return a(context, str, b(context, bool, str, str2));
    }

    static String a(Context context, String str, String str2) {
        BufferedReader bufferedReader;
        String str3;
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") == -1) {
            AdManager.a("Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />");
        }
        StringBuilder sb = new StringBuilder();
        try {
            String[] wapUrl = ConnectType.getWapUrl(context, str + str2);
            BannerLogger.d("winAd", "wapUrl is " + wapUrl);
            URL url = wapUrl != null ? new URL(wapUrl[0]) : new URL(str + str2);
            BannerLogger.d("winAd", "The required address  of ad  is " + str + str2);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            if (wapUrl != null) {
                httpURLConnection.setRequestProperty("X-Online-Host", wapUrl[1]);
            }
            httpURLConnection.setConnectTimeout(d);
            httpURLConnection.setReadTimeout(d);
            bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    sb.append(readLine);
                } catch (Throwable th) {
                    th = th;
                    str3 = null;
                }
            }
            String sb2 = sb.toString();
            try {
                httpURLConnection.disconnect();
                if (c == null) {
                }
                if (bufferedReader == null) {
                    return sb2;
                }
                try {
                    bufferedReader.close();
                    return sb2;
                } catch (Exception e) {
                    return sb2;
                }
            } catch (Throwable th2) {
                Throwable th3 = th2;
                str3 = sb2;
                th = th3;
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (Exception e2) {
                    }
                }
                try {
                    throw th;
                } catch (Exception e3) {
                    Log.w("winAd", "Could not get ad from DD servers.", e3);
                    return str3;
                }
            }
        } catch (Throwable th4) {
            th = th4;
            bufferedReader = null;
            str3 = null;
        }
    }

    static String b(Context context, Boolean bool, String str, String str2) {
        AdMark instance = AdMark.getInstance(context);
        if (instance.getDate("sumbitTime") == null) {
            instance.setKeepData("sumbitTime", AdMd5.a(1 + String.valueOf(AdManager.NextInt(100000, 999999)) + String.valueOf(System.currentTimeMillis())));
        }
        String date = instance.getDate("sumbitTime");
        String b2 = AdLocation.b(context);
        String str3 = "";
        if (!"".equals(b2)) {
            String[] split = b2.split("#");
            if (split.length > 1) {
                b2 = split[0];
                String[] split2 = split[1].split(",");
                str3 = split2[0].substring(0, 2) + "," + split2[1].substring(0, 2);
            }
            try {
                str3 = URLEncoder.encode(str3, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        } else {
            b2 = "";
        }
        String a2 = AdManager.a(context);
        if (a2 == null) {
            throw new IllegalStateException("Publisher ID is not set!  To serve ads you must set your publisher ID assigned from www.DD.com.  Either add it to AndroidManifest.xml under the <application> tag or call AdManager.setPublisherId().");
        }
        return "?type=" + (bool.booleanValue() ? 1 : 0) + "&" + "version=1" + "&" + "moblieType=" + Build.MODEL.replaceAll(" ", "") + "&" + "imei=" + Utilities.c(context) + "&" + "appId=" + a2 + "&" + "ad_id=" + str2 + "&location=" + b2 + "&" + "p_id=" + date + "&" + "os_name=1" + "&" + "position=" + str3;
    }

    public static Ad requestAd(Context context, Boolean bool, String str, String str2) {
        String a2 = a(context, bool, str, str2);
        BannerLogger.d("xhc", "html is" + a2);
        if (a2 != null) {
            a = Ad.createAd(context, a2, c);
        } else {
            a = null;
        }
        if (BannerLogger.isLoggable("winAd", 4)) {
            long currentTimeMillis = System.currentTimeMillis() - b;
            if (a == null) {
                BannerLogger.i("winAd", "Server replied that no ads are available (" + currentTimeMillis + "ms)");
            } else {
                BannerLogger.i("winAd", "Ad returned in " + currentTimeMillis + "ms:  " + a);
            }
        }
        return a;
    }
}
