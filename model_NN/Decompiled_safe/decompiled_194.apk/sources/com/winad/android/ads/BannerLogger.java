package com.winad.android.ads;

import android.util.Log;

public class BannerLogger {
    public static boolean weatherPrint = false;

    public static void d(String str, String str2) {
        if (weatherPrint && isLoggable(str, 3)) {
            Log.d(str, str2);
        }
    }

    public static void e(String str, String str2) {
        if (weatherPrint && isLoggable(str, 6)) {
            Log.e(str, str2);
        }
    }

    public static void i(String str, String str2) {
        if (weatherPrint && isLoggable(str, 4)) {
            Log.i(str, str2);
        }
    }

    public static boolean isLoggable(String str, int i) {
        return true;
    }

    public static void v(String str, String str2) {
        if (weatherPrint && isLoggable(str, 2)) {
            Log.v(str, str2);
        }
    }

    public static void w(String str, String str2) {
        if (weatherPrint && isLoggable(str, 5)) {
            Log.w(str, str2);
        }
    }
}
