package com.winad.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.smaato.SOMA.SOMATextBanner;
import com.winad.android.gif.GifView;

public class CustomLayout {
    static EditText a;

    private static View a(Context context) {
        try {
            a = new EditText(context);
            a.setHint("Google一下");
            a.setGravity(0);
            a.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    CustomLayout.a.setText("乐点下载");
                }
            });
            return a;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    protected static View a(Context context, Handler handler, Bitmap bitmap) {
        ScrollView scrollView = new ScrollView(context);
        scrollView.setBackgroundColor(-1);
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(12, 2, 12, 2);
        linearLayout.setOrientation(1);
        scrollView.addView(linearLayout, layoutParams);
        View a2 = a(context, "立即注册,零投资,赢大奖");
        if (a2 != null) {
            linearLayout.addView(a2, layoutParams);
        }
        View b = b(context, "乐点下载是供网友在线下载手机软件安装文件、壁纸、音乐、铃声的开放平台,在这里,用户可以下载各种软件安装文件、壁纸、音乐、铃声");
        if (b != null) {
            linearLayout.addView(b, layoutParams);
        }
        View a3 = a(context);
        if (a3 != null) {
            linearLayout.addView(a3, layoutParams);
        }
        View b2 = b(context);
        if (b2 != null) {
            linearLayout.addView(b2, layoutParams);
        }
        return scrollView;
    }

    private static View a(Context context, String str) {
        try {
            TextView textView = new TextView(context);
            textView.setTextSize(18.0f);
            textView.setTextColor((int) SOMATextBanner.DEFAULT_BACKGROUND_COLOR);
            textView.setText("\n" + str + "\n");
            textView.setGravity(17);
            return textView;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    protected static ImageView a(Context context, Bitmap bitmap) {
        try {
            ImageView imageView = new ImageView(context);
            imageView.setImageBitmap(bitmap);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            return imageView;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    protected static GifView a(Context context, byte[] bArr) {
        try {
            GifView gifView = new GifView(context);
            gifView.setGifImage(bArr);
            return gifView;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static View b(final Context context) {
        try {
            Button button = new Button(context);
            button.setText("搜索");
            button.setGravity(17);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    AdDisplayModes.c(context, CustomLayout.a.getText().toString());
                }
            });
            return button;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static View b(Context context, String str) {
        try {
            TextView textView = new TextView(context);
            textView.setTextSize(15.0f);
            textView.setTextColor((int) SOMATextBanner.DEFAULT_BACKGROUND_COLOR);
            textView.setText(str.replace("\r", ""));
            return textView;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
