package com.winad.android.ads;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import com.winad.android.gif.GifView;

public class CustomDialog {
    static Handler a;
    static Context b;
    static AlertDialog c;
    static View d = null;
    static Bitmap e = null;

    public static Dialog showDialog(Context context, Handler handler, Bitmap bitmap) {
        b = context;
        if (a == null) {
            a = handler;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(17301628);
        builder.setTitle("乐点广告");
        View a2 = CustomLayout.a(context, handler, bitmap);
        if (a2 != null) {
            builder.setView(a2);
        }
        return builder.create();
    }

    public static Dialog showImageDialog(Context context, Handler handler, byte[] bArr, final String str) {
        b = context;
        if (a == null) {
            a = handler;
        }
        if (str.toLowerCase().trim().endsWith("gif")) {
            d = CustomLayout.a(context, bArr);
        } else {
            e = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
            d = CustomLayout.a(context, e);
        }
        d.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CustomDialog.c.dismiss();
            }
        });
        c = new AlertDialog.Builder(context).create();
        c.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                System.out.println("dismiss dialog");
                if (!str.toLowerCase().trim().endsWith("gif")) {
                    ((ImageView) CustomDialog.d).setImageBitmap(null);
                    if (CustomDialog.e != null) {
                        CustomDialog.e.recycle();
                    }
                } else if (CustomDialog.d != null) {
                    ((GifView) CustomDialog.d).free();
                }
            }
        });
        c.getWindow().setBackgroundDrawable(null);
        c.show();
        c.getWindow().setContentView(d);
        return c;
    }
}
