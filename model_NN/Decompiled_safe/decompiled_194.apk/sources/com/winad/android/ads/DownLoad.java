package com.winad.android.ads;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class DownLoad extends AsyncTask {
    static HashMap a = new HashMap();
    private static int i;
    private Context b;
    private String c;
    private String d;
    private File e;
    private NotificationManager f = null;
    private Notification g = null;
    private OnDownLoadListener h;
    private String j;

    public DownLoad(Context context, String str, String str2, String str3) {
        this.j = str3;
        this.b = context;
        this.c = str.substring(str.lastIndexOf(47) + 1, str.length());
        this.e = a(this.c);
        this.d = str2;
        if (!a.containsKey(str3)) {
            i++;
            a.put(str3, new Integer(i));
            return;
        }
        i = ((Integer) a.get(str3)).intValue();
    }

    private File a(String str) {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "winAds");
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(file, str);
    }

    public static boolean isDownLoading(String str) {
        return a.containsKey(str);
    }

    public void ShowNotification(int i2, String str, String str2, int i3) {
        this.f = (NotificationManager) this.b.getSystemService("notification");
        this.g = new Notification(i2, str, System.currentTimeMillis());
        Intent intent = new Intent();
        intent.setAction(str2);
        this.g.setLatestEventInfo(this.b, this.d, str, PendingIntent.getActivity(this.b, 0, intent, 134217728));
        this.f.notify(i3, this.g);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object doInBackground(String... strArr) {
        long contentLength;
        InputStream content;
        BannerLogger.d("lxs123", strArr[0]);
        int[] iArr = {0, i};
        try {
            String str = strArr[0];
            String[] wapUrl = ConnectType.getWapUrl(this.b, str);
            if (wapUrl != null) {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(wapUrl[0]).openConnection();
                httpURLConnection.setRequestProperty("X-Online-Host", wapUrl[1]);
                contentLength = (long) httpURLConnection.getContentLength();
                content = httpURLConnection.getInputStream();
            } else {
                HttpEntity entity = new DefaultHttpClient().execute(new HttpGet(str)).getEntity();
                contentLength = entity.getContentLength();
                content = entity.getContent();
            }
            if (content != null) {
                if (this.e.exists()) {
                    this.e.delete();
                }
                FileOutputStream fileOutputStream = new FileOutputStream(this.e);
                byte[] bArr = new byte[1024];
                int i2 = 0;
                int i3 = 0;
                while (true) {
                    int read = content.read(bArr);
                    if (read != -1) {
                        fileOutputStream.write(bArr, 0, read);
                        i3 += read;
                        i2++;
                        if (i2 > 30 && contentLength > 0) {
                            publishProgress(Integer.valueOf((int) ((((float) i3) / ((float) contentLength)) * 100.0f)));
                            i2 = 0;
                        }
                    } else {
                        fileOutputStream.close();
                        content.close();
                        return iArr;
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            iArr[0] = -1;
            BannerLogger.e("xhc", "doInBackground is false");
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
        ((Activity) this.b).setTitle("下载了  " + numArr[0] + "%");
    }

    public void installApk() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse("file://" + this.e.getAbsolutePath()), "application/vnd.android.package-archive");
        this.b.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object obj) {
        super.onPostExecute(obj);
        ((Activity) this.b).setTitle("");
        int[] iArr = (int[]) obj;
        if (iArr[0] != -1) {
            this.h.downLoadCompleted();
            ShowNotification(17301634, "下载成功", "completed", iArr[1]);
            this.f.cancel(iArr[1]);
            installApk();
        } else {
            this.h.downLoadFalse();
            this.f.cancel(iArr[1]);
            Toast.makeText(this.b, "DownLoad fail...", 1).show();
        }
        a.remove(this.j);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        ShowNotification(17301633, "下载中...", "", i);
    }

    public void setOnPanelListener(OnDownLoadListener onDownLoadListener) {
        this.h = onDownLoadListener;
        BannerLogger.d("lxs123", "onDownloadListener" + this.h);
    }
}
