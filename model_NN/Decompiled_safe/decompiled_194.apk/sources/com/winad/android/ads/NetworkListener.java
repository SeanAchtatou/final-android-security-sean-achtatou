package com.winad.android.ads;

public interface NetworkListener {
    void onNetworkActivityEnd();

    void onNetworkActivityStart();
}
