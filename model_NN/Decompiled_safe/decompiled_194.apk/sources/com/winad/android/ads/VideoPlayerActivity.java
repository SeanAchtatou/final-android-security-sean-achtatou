package com.winad.android.ads;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class VideoPlayerActivity extends Activity implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, View.OnTouchListener {
    int a;
    Runnable b = new Runnable() {
        public void run() {
            try {
                VideoPlayerActivity.this.d.prepare();
                VideoPlayerActivity.this.a = VideoPlayerActivity.this.d.getDuration();
            } catch (IllegalStateException e) {
                VideoPlayerActivity.this.h.sendEmptyMessage(4);
                e.printStackTrace();
            } catch (IOException e2) {
                VideoPlayerActivity.this.h.sendEmptyMessage(4);
                e2.printStackTrace();
            }
        }
    };
    private VideoView c;
    /* access modifiers changed from: private */
    public MediaPlayer d;
    private int e;
    /* access modifiers changed from: private */
    public File f;
    /* access modifiers changed from: private */
    public ProgressDialog g;
    /* access modifiers changed from: private */
    public Handler h = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 4:
                    if (VideoPlayerActivity.this.g != null) {
                        VideoPlayerActivity.this.g.dismiss();
                    }
                    Toast.makeText(VideoPlayerActivity.this, "play failed", 1).show();
                    VideoPlayerActivity.this.finish();
                    return;
                default:
                    return;
            }
        }
    };

    class VideoView extends SurfaceView implements SurfaceHolder.Callback {
        public VideoView(Context context) {
            super(context);
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
            BannerLogger.i("VideoPlayerActivity", "surfaceCreated");
            try {
                VideoPlayerActivity.this.a(VideoPlayerActivity.this.f, 0);
            } catch (Exception e) {
                Log.e("VideoPlayerActivity", e.toString());
            }
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        }
    }

    private void a() {
        this.d.stop();
    }

    /* access modifiers changed from: private */
    public void a(File file, int i) {
        this.c.setEnabled(false);
        this.d.reset();
        String stringExtra = getIntent().getStringExtra("URL");
        BannerLogger.i("VideoPlayerActivity", "url = " + stringExtra);
        String[] wapUrl = ConnectType.getWapUrl(this, stringExtra);
        BannerLogger.d("winAd", "wapUrl is " + wapUrl);
        if (wapUrl != null) {
            setProxy();
        }
        this.d.setDataSource(stringExtra);
        new Thread(this.b).start();
    }

    private void b() {
        if (this.d.isPlaying()) {
            this.d.pause();
        }
    }

    public static String getTime(long j) {
        long j2 = j < 0 ? 0 : j;
        long j3 = (j2 / 3600) % 60;
        String format = String.format("%02d:%02d", Long.valueOf((j2 / 60) % 60), Long.valueOf(j2 % 60));
        if (j3 <= 0) {
            return format;
        }
        return String.format("%02d:", Long.valueOf(j3)) + format;
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
        BannerLogger.i("VideoPlayerActivity", "percent = " + i + "%");
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        BannerLogger.i("VideoPlayerActivity", "*********************************");
        a();
        finish();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFormat(-1);
        getWindow().setFlags(1024, 1024);
        BannerLogger.i("VideoPlayerActivity", "onCreate()position = " + this.e);
        getWindow().addFlags(128);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int i = displayMetrics.heightPixels;
        int i2 = displayMetrics.widthPixels;
        this.g = new ProgressDialog(this);
        this.g.setMessage("正在加载,请稍后...");
        this.g.setIndeterminate(false);
        this.g.setCancelable(true);
        this.g.setProgressStyle(0);
        this.g.show();
        this.c = new VideoView(this);
        setContentView(this.c);
        this.c.setOnTouchListener(this);
        this.c.getHolder().setFixedSize(i2, i);
        this.c.getHolder().setType(3);
        this.c.getHolder().addCallback(this.c);
        this.c.setOnTouchListener(this);
        this.d = new MediaPlayer();
        this.d.setOnBufferingUpdateListener(this);
        this.d.setOnPreparedListener(this);
        this.d.setOnCompletionListener(this);
        this.d.setOnErrorListener(this);
        this.d.setAudioStreamType(3);
        this.d.setDisplay(this.c.getHolder());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        String str;
        String str2;
        super.onDestroy();
        BannerLogger.i("VideoPlayerActivity", "onDestroy()");
        if (this.d != null) {
            try {
                if (this.d.isPlaying()) {
                    BannerLogger.i("VideoPlayerActivity", "surfaceDestroyed");
                    this.e = this.d.getCurrentPosition();
                    BannerLogger.i("VideoPlayerActivity", "onDestroty----------> position" + this.e);
                    this.d.stop();
                }
                this.d.release();
            } catch (IllegalStateException e2) {
                e2.printStackTrace();
            } finally {
                str = "VideoPlayerActivity";
                str2 = "safe exit!";
                Log.i(str, str2);
                removeProxy();
            }
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        mediaPlayer.stop();
        mediaPlayer.release();
        if (this.g == null) {
            return false;
        }
        this.g.dismiss();
        return false;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        BannerLogger.i("VideoPlayerActivity", "onPrepared()");
        if (this.g != null) {
            this.g.dismiss();
        }
        this.c.setEnabled(true);
        this.d.start();
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view.equals(this.c)) {
        }
        return super.onTouchEvent(motionEvent);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        BannerLogger.i("VideoPlayerActivity", "onWindowFocusChanged() = " + z);
        if (this.d != null && this.d.isPlaying() && !z) {
            b();
        }
    }

    public void removeProxy() {
        Properties properties = System.getProperties();
        properties.remove("proxySet");
        properties.remove("proxyHost");
        properties.remove("proxyPort");
    }

    public void setProxy() {
        Properties properties = System.getProperties();
        if (!properties.containsKey("proxySet")) {
            properties.put("proxySet", "true");
            properties.put("proxyHost", "10.0.0.172:80");
            properties.put("proxyPort", 80);
        }
    }
}
