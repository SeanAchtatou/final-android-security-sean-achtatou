package com.winad.android.ads;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.adchina.android.ads.Common;
import com.winad.android.gif.GifView;

class AdContainer extends RelativeLayout implements Animation.AnimationListener, NetworkListener {
    public static final int DEFAULT_BACKGROUND_ALPHA = 200;
    public static final int DEFAULT_BACKGROUND_COLOR = 0;
    public static final int DEFAULT_TEXT_COLOR = -1;
    public static final int MAX_WIDTH = 320;
    private static final Typeface f = Typeface.create(Typeface.SANS_SERIF, 1);
    private static final Typeface g = Typeface.create(Typeface.SANS_SERIF, 0);
    protected TextView a;
    protected TextView b;
    protected TextView c;
    protected ImageView d;
    protected GifView e;
    private int h;
    private int i;
    private int j;
    private BitmapDrawable k;
    private BitmapDrawable l;
    private BitmapDrawable m;
    private Drawable n;
    /* access modifiers changed from: private */
    public Ad o;
    private Bitmap p;
    private int q;
    /* access modifiers changed from: private */
    public boolean r;
    /* access modifiers changed from: private */
    public Handler s;

    public AdContainer(Ad ad, Context context, Handler handler) {
        super(context);
        this.h = 200;
        this.i = 0;
        this.j = -1;
        a(handler);
        a(ad, context, (AttributeSet) null, 0);
    }

    public AdContainer(Ad ad, Context context, AttributeSet attributeSet) {
        this(ad, context, attributeSet, 0);
    }

    public AdContainer(Ad ad, Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.h = 200;
        this.i = 0;
        this.j = -1;
        a(ad, context, attributeSet, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.winad.android.ads.AdContainer.a(android.graphics.Rect, int, int, int, boolean):android.graphics.drawable.BitmapDrawable
     arg types: [android.graphics.Rect, int, int, int, int]
     candidates:
      com.winad.android.ads.AdContainer.a(android.graphics.Canvas, android.graphics.Rect, int, int, int):void
      com.winad.android.ads.AdContainer.a(android.graphics.Rect, int, int, int, boolean):android.graphics.drawable.BitmapDrawable */
    private BitmapDrawable a(Rect rect, int i2, int i3, int i4) {
        return a(rect, i2, i3, i4, false);
    }

    private BitmapDrawable a(Rect rect, int i2, int i3, int i4, boolean z) {
        try {
            Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            a(canvas, rect, i2, i3, i4);
            if (z) {
                a(canvas, rect);
            }
            return new BitmapDrawable(createBitmap);
        } catch (Throwable th) {
            return null;
        }
    }

    private static void a(Canvas canvas, Rect rect) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(-16724737);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3.0f);
        paint.setPathEffect(new CornerPathEffect(3.0f));
        Path path = new Path();
        path.addRoundRect(new RectF(rect), 3.0f, 3.0f, Path.Direction.CW);
        canvas.drawPath(path, paint);
    }

    private static void a(Canvas canvas, Rect rect, int i2, int i3, int i4) {
        BannerLogger.d("winAd", "backgroundAlpha== " + i3);
        Paint paint = new Paint();
        paint.setColor(i2);
        paint.setAlpha(i3);
        paint.setAntiAlias(true);
        canvas.drawRect(rect, paint);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(127, Color.red(i4), Color.green(i4), Color.blue(i4)), i4});
        int height = ((int) (((double) rect.height()) * 0.6d)) + rect.top;
        gradientDrawable.setBounds(rect.left, rect.top, rect.right, height);
        gradientDrawable.draw(canvas);
        Rect rect2 = new Rect(rect.left, height, rect.right, rect.bottom);
        Paint paint2 = new Paint();
        paint2.setColor(i4);
        canvas.drawRect(rect2, paint2);
    }

    private void a(BitmapDrawable bitmapDrawable) {
        Bitmap bitmap;
        if (bitmapDrawable != null && (bitmap = bitmapDrawable.getBitmap()) != null) {
            bitmap.recycle();
        }
    }

    private void a(Ad ad, Context context, AttributeSet attributeSet, int i2) {
        int i3;
        int i4;
        this.o = ad;
        ad.setNetworkListener(this);
        this.k = null;
        this.m = null;
        this.l = null;
        this.r = false;
        if (ad != null) {
            setFocusable(true);
            setClickable(true);
            byte[] bArr = null;
            if (ad.d != null && !"".equals(ad.d) && !"null".equals(ad.d)) {
                bArr = ad.a();
            }
            this.q = 8;
            int i5 = (AdView.HEIGHT - ((int) (27.0f * context.getResources().getDisplayMetrics().density))) / 5;
            ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());
            if (bArr != null) {
                BannerLogger.d("winAd", "AdContainer ad.iconURL == " + ad.d);
                if (Common.KIMP.equals(ad.getshowType())) {
                    int i6 = AdView.HEIGHT;
                    i3 = (int) (320.0f * context.getResources().getDisplayMetrics().density);
                    i4 = i6;
                } else {
                    int i7 = (int) (40.0f * context.getResources().getDisplayMetrics().density);
                    i3 = (int) (40.0f * context.getResources().getDisplayMetrics().density);
                    i4 = i7;
                }
                this.q = (AdView.HEIGHT - i4) / 2;
                this.e = new GifView(context);
                this.e.setGifImage(bArr);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(i3, i4);
                layoutParams.setMargins(this.q, this.q, this.q, this.q);
                this.e.setLayoutParams(layoutParams);
                this.e.setId(1);
                this.e.setShowDimension(i3, i4);
                addView(this.e);
            }
            if (!Common.KIMP.equals(ad.getshowType())) {
                this.b = new TextView(context);
                this.b.setText(ad.getStyleTitle());
                this.b.setTypeface(f);
                this.b.setTextColor(this.j);
                this.b.setTextSize(15.0f);
                this.b.setId(2);
                this.b.setGravity(51);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
                if (bArr != null) {
                    layoutParams2.addRule(1, 1);
                }
                layoutParams2.setMargins((int) (2.0f * context.getResources().getDisplayMetrics().density), i5, 0, 0);
                layoutParams2.addRule(11);
                layoutParams2.addRule(14);
                this.b.setLayoutParams(layoutParams2);
                addView(this.b);
                this.a = new TextView(context);
                this.a.setText(ad.getText());
                this.a.setTypeface(f);
                this.a.setTextColor(this.j);
                this.a.setTextSize(12.0f);
                this.a.setId(2);
                this.a.setGravity(83);
                this.a.setHorizontallyScrolling(true);
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
                if (bArr != null) {
                    layoutParams3.addRule(1, 1);
                }
                layoutParams3.setMargins((int) (2.0f * context.getResources().getDisplayMetrics().density), 0, (int) (65.0f * context.getResources().getDisplayMetrics().density), i5);
                layoutParams3.addRule(11);
                layoutParams3.addRule(14);
                this.a.setLayoutParams(layoutParams3);
                addView(this.a);
                this.d = null;
                byte[] image = ad.getImage();
                if (image != null) {
                    this.p = BitmapFactory.decodeByteArray(image, 0, image.length);
                    this.d = new ImageView(context);
                    this.d.setImageBitmap(this.p);
                    this.d.setScaleType(ImageView.ScaleType.FIT_END);
                    RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams((int) (65.0f * context.getResources().getDisplayMetrics().density), (int) (50.0f * context.getResources().getDisplayMetrics().density));
                    layoutParams4.addRule(11, 1);
                    this.d.setLayoutParams(layoutParams4);
                    addView(this.d);
                }
            }
        }
        setTextColor(-1);
        setBackgroundColor(0);
    }

    /* access modifiers changed from: protected */
    public Ad a() {
        return this.o;
    }

    /* access modifiers changed from: protected */
    public void a(Handler handler) {
        this.s = handler;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.o != null) {
        }
        if (this.e != null) {
            this.e.free();
        }
        if (this.p != null) {
            this.p.recycle();
            this.p = null;
        }
        if (this.k != null) {
            BitmapDrawable bitmapDrawable = this.k;
            this.k = null;
            a(bitmapDrawable);
        }
        if (this.m != null) {
            BitmapDrawable bitmapDrawable2 = this.m;
            this.m = null;
            a(bitmapDrawable2);
        }
        if (this.l != null) {
            BitmapDrawable bitmapDrawable3 = this.l;
            this.l = null;
            a(bitmapDrawable3);
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        float height;
        float width;
        BannerLogger.v("winAd", "click() :" + this.o);
        if (this.o != null && isPressed()) {
            setPressed(false);
            if (!this.r) {
                this.r = true;
                if (this.d == null && this.e == null) {
                    this.o.clicked(this.s);
                    return;
                }
                AnimationSet animationSet = new AnimationSet(true);
                if (this.d != null) {
                    height = ((float) this.d.getHeight()) / 2.0f;
                    width = ((float) this.d.getWidth()) / 2.0f;
                } else {
                    height = ((float) this.e.getHeight()) / 2.0f;
                    width = ((float) this.e.getWidth()) / 2.0f;
                }
                ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, width, height);
                scaleAnimation.setDuration(200);
                animationSet.addAnimation(scaleAnimation);
                ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.2f, 0.001f, 1.2f, 0.001f, width, height);
                scaleAnimation2.setDuration(299);
                scaleAnimation2.setStartOffset(200);
                scaleAnimation2.setAnimationListener(this);
                animationSet.addAnimation(scaleAnimation2);
                postDelayed(new Thread() {
                    public void run() {
                        AdContainer.this.o.clicked(AdContainer.this.s);
                    }
                }, 500);
                if (this.d != null) {
                    this.d.startAnimation(animationSet);
                } else if (this.e != null) {
                    this.e.startAnimation(animationSet);
                }
            }
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (Log.isLoggable("winAd", 2)) {
            Log.v("winAd", "dispatchTouchEvent: action=" + action + " x=" + motionEvent.getX() + " y=" + motionEvent.getY());
        }
        if (action == 0) {
            setPressed(true);
        } else if (action == 2) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int left = getLeft();
            int top = getTop();
            int right = getRight();
            int bottom = getBottom();
            if (x < ((float) left) || x > ((float) right) || y < ((float) top) || y > ((float) bottom)) {
                setPressed(false);
            } else {
                setPressed(true);
            }
        } else if (action == 1) {
            BannerLogger.v("winAd", "dispatchTouchEvent:" + isPressed());
            if (isPressed()) {
                c();
            }
            setPressed(false);
        } else if (action == 3) {
            setPressed(false);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (Log.isLoggable("winAd", 2)) {
            Log.v("winAd", "dispatchTrackballEvent: action=" + motionEvent.getAction());
        }
        if (motionEvent.getAction() == 0) {
            setPressed(true);
        } else if (motionEvent.getAction() == 1) {
            if (hasFocus()) {
                c();
            }
            setPressed(false);
        }
        return super.onTrackballEvent(motionEvent);
    }

    public int getBackgroundAlpha() {
        return this.h;
    }

    public int getBackgroundColor() {
        return this.i;
    }

    public int getTextColor() {
        return this.j;
    }

    public void onAnimationEnd(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i2, Rect rect) {
        super.onFocusChanged(z, i2, rect);
        if (z) {
            setBackgroundDrawable(this.l);
        } else {
            setBackgroundDrawable(this.k);
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (Log.isLoggable("winAd", 2)) {
            Log.v("winAd", "onKeyDown: keyCode=" + i2);
        }
        if (i2 == 66 || i2 == 23) {
            setPressed(true);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (Log.isLoggable("winAd", 2)) {
            Log.v("winAd", "onKeyUp: keyCode=" + i2);
        }
        if (i2 == 66 || i2 == 23) {
            c();
        }
        setPressed(false);
        return super.onKeyUp(i2, keyEvent);
    }

    public void onNetworkActivityEnd() {
        BannerLogger.d("lxs123", "onNetworkActivityEnd()");
        post(new Thread() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.winad.android.ads.AdContainer.a(com.winad.android.ads.AdContainer, boolean):boolean
             arg types: [com.winad.android.ads.AdContainer, int]
             candidates:
              com.winad.android.ads.AdContainer.a(android.graphics.Canvas, android.graphics.Rect):void
              com.winad.android.ads.AdContainer.a(com.winad.android.ads.AdContainer, boolean):boolean */
            public void run() {
                if (AdContainer.this.d != null) {
                    AdContainer.this.d.setImageMatrix(new Matrix());
                    AdContainer.this.d.setVisibility(0);
                }
                boolean unused = AdContainer.this.r = false;
            }
        });
    }

    public void onNetworkActivityStart() {
        BannerLogger.d("lxs123", "onNetworkActivityStart()");
        post(new Thread() {
            public void run() {
                if (AdContainer.this.d != null) {
                }
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.winad.android.ads.AdContainer.a(android.graphics.Rect, int, int, int, boolean):android.graphics.drawable.BitmapDrawable
     arg types: [android.graphics.Rect, int, int, int, int]
     candidates:
      com.winad.android.ads.AdContainer.a(android.graphics.Canvas, android.graphics.Rect, int, int, int):void
      com.winad.android.ads.AdContainer.a(android.graphics.Rect, int, int, int, boolean):android.graphics.drawable.BitmapDrawable */
    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (!(this.c == null || this.a == null)) {
            int visibility = this.c.getVisibility();
            if (i2 > 0) {
                if (i2 <= 128) {
                    this.a.setTextSize(15.0f);
                } else if (i2 <= 176) {
                    this.a.setTextSize(15.0f);
                    this.c.setTextSize(12.0f);
                } else {
                    this.a.setTextSize(15.0f);
                    this.c.setTextSize(12.0f);
                }
            }
            if (visibility == 0) {
                Typeface typeface = this.a.getTypeface();
                String text = this.o.getText();
                if (text != null) {
                    Paint paint = new Paint();
                    paint.setTypeface(typeface);
                    paint.setTextSize(this.a.getTextSize());
                    float measureText = paint.measureText(text);
                    float f2 = (float) (i2 - (this.q * 2));
                    if (this.d != null) {
                        f2 -= (float) (this.d.getWidth() + this.q);
                    }
                    if (measureText > f2) {
                    }
                }
            }
            this.c.setVisibility(visibility);
        }
        if (i2 != 0 && i3 != 0) {
            Rect rect = new Rect(0, 0, i2, i3);
            this.k = a(rect, this.i, this.h, 13434879);
            this.m = a(rect, -16724737, this.h, -16724737);
            this.l = a(rect, -16724737, this.h, 13434879, true);
            setBackgroundDrawable(this.k);
        }
    }

    public void setBackgroundAlpha(int i2) {
        BannerLogger.d("winAd", "backgroundAlpha AdContainer== " + i2);
        this.h = i2;
    }

    public void setBackgroundColor(int i2) {
        this.i = i2;
    }

    public void setPressed(boolean z) {
        Drawable drawable;
        BannerLogger.d("winAd", "setPressed==  " + z + "isPressed()==  " + isPressed());
        if ((!z || !this.r) && isPressed() != z) {
            BitmapDrawable bitmapDrawable = this.k;
            int i2 = this.j;
            if (z) {
                this.n = getBackground();
                drawable = this.m;
                i2 = -16777216;
            } else {
                drawable = this.n;
            }
            setBackgroundDrawable(drawable);
            if (this.a != null) {
                this.a.setTextColor(i2);
            }
            if (this.c != null) {
                this.c.setTextColor(i2);
            }
            super.setPressed(z);
            invalidate();
        }
    }

    public void setTextColor(int i2) {
        this.j = -16777216 | i2;
        if (this.a != null) {
            this.a.setTextColor(this.j);
        }
        if (this.b != null) {
            this.b.setTextColor(this.j);
        }
        postInvalidate();
    }
}
