package com.winad.android.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.adchina.android.ads.Common;
import com.admogo.util.AdMogoUtil;
import com.energysource.szj.embeded.AdManager;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class AdView extends RelativeLayout implements OnDownLoadListener {
    public static int HEIGHT = 50;
    private static Timer f;
    private static int l = 10000;
    /* access modifiers changed from: private */
    public static Handler p;
    private static Context q;
    /* access modifiers changed from: private */
    public Handler A;
    ADListener a;
    /* access modifiers changed from: private */
    public AdContainer b;
    /* access modifiers changed from: private */
    public Ad c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public int e;
    private int g;
    private int h;
    private int i;
    private String j;
    /* access modifiers changed from: private */
    public boolean k;
    private String m;
    private boolean n;
    private boolean o;
    /* access modifiers changed from: private */
    public AdContainer r;
    /* access modifiers changed from: private */
    public boolean s;
    /* access modifiers changed from: private */
    public boolean t;
    /* access modifiers changed from: private */
    public boolean u;
    /* access modifiers changed from: private */
    public Context v;
    /* access modifiers changed from: private */
    public String w;
    /* access modifiers changed from: private */
    public String x;
    /* access modifiers changed from: private */
    public String y;
    private boolean z;

    final class SwapViews implements Runnable {
        /* access modifiers changed from: private */
        public AdContainer b;
        /* access modifiers changed from: private */
        public AdContainer c;
        private int d;

        public SwapViews(AdContainer adContainer, int i) {
            this.b = adContainer;
            this.d = i;
        }

        public void run() {
            this.c = AdView.this.b;
            BannerLogger.i("winAd", "second rotation start ");
            if (this.c != null) {
                this.c.setVisibility(8);
            }
            this.b.setVisibility(0);
            Animation animation = null;
            switch (this.d) {
                case 0:
                    animation = new TranslateAnimation((float) AdView.this.getWidth(), 0.0f, 0.0f, 0.0f);
                    break;
                case 1:
                    animation = new TranslateAnimation(0.0f, 0.0f, (float) AdView.this.getHeight(), 0.0f);
                    break;
                case 2:
                    animation = new TranslateAnimation(0.0f, 0.0f, (float) (-AdView.this.getHeight()), 0.0f);
                    break;
                case 3:
                    animation = new Rotate3dAnimation(90.0f, 0.0f, ((float) AdView.this.getWidth()) / 2.0f, ((float) AdView.this.getHeight()) / 2.0f, 0.0f * ((float) AdView.this.getWidth()), AdView.this.getContext(), false);
                    animation.setDuration(500);
                    break;
            }
            animation.setDuration(500);
            animation.setFillAfter(true);
            animation.setInterpolator(new AccelerateInterpolator());
            animation.setAnimationListener(new Animation.AnimationListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.winad.android.ads.AdView.b(com.winad.android.ads.AdView, boolean):boolean
                 arg types: [com.winad.android.ads.AdView, int]
                 candidates:
                  com.winad.android.ads.AdView.b(com.winad.android.ads.AdView, java.lang.String):java.lang.String
                  com.winad.android.ads.AdView.b(com.winad.android.ads.AdView, com.winad.android.ads.AdContainer):void
                  com.winad.android.ads.AdView.b(com.winad.android.ads.AdView, boolean):boolean */
                public void onAnimationEnd(Animation animation) {
                    if (SwapViews.this.c != null) {
                        AdView.this.removeView(SwapViews.this.c);
                        SwapViews.this.c.b();
                        AdContainer unused = SwapViews.this.c = null;
                    }
                    AdView.this.invalidate();
                    AdContainer unused2 = AdView.this.b = SwapViews.this.b;
                    boolean unused3 = AdView.this.u = false;
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            });
            if (this.d == 3) {
                AdView.this.startAnimation(animation);
                return;
            }
            if (this.b.e != null) {
                this.b.e.startAnimation(animation);
            }
            if (this.b.b != null) {
                this.b.b.startAnimation(animation);
            }
            if (this.b.a != null) {
                this.b.a.startAnimation(animation);
            }
            if (this.b.d != null) {
                this.b.d.startAnimation(animation);
            }
        }
    }

    public AdView(Context context, int i2, int i3, int i4, int i5, boolean z2) {
        this(context, null, 0, i2, i3, i4, i5, z2);
        BannerLogger.d("lxs123", "AdView( Context context )");
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 0, 0, 0, true);
        BannerLogger.d("lxs123", "AdView( Context context , AttributeSet attrs)");
    }

    public AdView(Context context, AttributeSet attributeSet, int i2, int i3, int i4, int i5, int i6, boolean z2) {
        super(context, attributeSet, i2);
        this.s = false;
        this.t = true;
        this.u = false;
        this.v = null;
        this.w = "";
        this.x = "";
        this.y = "";
        this.z = true;
        this.A = new Handler() {
            /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.winad.android.ads.AdView.a(com.winad.android.ads.AdView, boolean):boolean
             arg types: [com.winad.android.ads.AdView, int]
             candidates:
              com.winad.android.ads.AdView.a(com.winad.android.ads.AdView, android.content.Context):android.content.Context
              com.winad.android.ads.AdView.a(com.winad.android.ads.AdView, com.winad.android.ads.Ad):com.winad.android.ads.Ad
              com.winad.android.ads.AdView.a(com.winad.android.ads.AdView, com.winad.android.ads.AdContainer):com.winad.android.ads.AdContainer
              com.winad.android.ads.AdView.a(com.winad.android.ads.AdView, java.lang.String):java.lang.String
              com.winad.android.ads.AdView.a(android.content.Context, int):void
              com.winad.android.ads.AdView.a(com.winad.android.ads.AdContainer, int):void
              com.winad.android.ads.AdView.a(com.winad.android.ads.AdView, boolean):boolean */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.winad.android.ads.AdView.b(com.winad.android.ads.AdView, boolean):boolean
             arg types: [com.winad.android.ads.AdView, int]
             candidates:
              com.winad.android.ads.AdView.b(com.winad.android.ads.AdView, java.lang.String):java.lang.String
              com.winad.android.ads.AdView.b(com.winad.android.ads.AdView, com.winad.android.ads.AdContainer):void
              com.winad.android.ads.AdView.b(com.winad.android.ads.AdView, boolean):boolean */
            public void handleMessage(Message message) {
                if (message.what != 15) {
                    Context unused = AdView.this.v = AdView.this.getContext();
                    String unused2 = AdView.this.w = message.getData().getString("explainText");
                    String unused3 = AdView.this.x = message.getData().getString("sendAim");
                    if (Ad.a != null) {
                        Ad.a.dismiss();
                    }
                    boolean unused4 = AdView.this.s = true;
                    if (!AdView.this.u && message.what != 8) {
                        boolean unused5 = AdView.this.u = true;
                        AdView.this.c();
                    }
                }
                switch (message.what) {
                    case 1:
                        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(AdView.this.x));
                        intent.addFlags(268435456);
                        try {
                            AdView.this.v.startActivity(intent);
                            break;
                        } catch (Exception e) {
                            Log.e("winAd", "Could not open browser on ad click to " + AdView.this.x, e);
                            break;
                        }
                    case 2:
                        byte[] byteArray = message.getData().getByteArray("lookImage");
                        if (byteArray == null || byteArray.length == 0) {
                            Toast.makeText(AdView.this.v, "加载失败", 1).show();
                            return;
                        } else {
                            CustomDialog.showImageDialog(AdView.this.v, AdView.this.A, byteArray, AdView.this.x);
                            return;
                        }
                    case 3:
                        AdDisplayModes.b(AdView.this.v, AdView.this.x);
                        return;
                    case 4:
                        String[] split = AdView.this.x.split("#");
                        if (split.length > 1) {
                            String unused6 = AdView.this.x = split[0];
                            String unused7 = AdView.this.y = split[1];
                        }
                        AdDisplayModes.a(AdView.this.v, AdView.this.y, AdView.this.x);
                        return;
                    case 5:
                        AdDisplayModes.a(AdView.this.v, AdView.this.w);
                        return;
                    case 6:
                        AdDisplayModes.d(AdView.this.v, AdView.this.x);
                        return;
                    case 7:
                        AdDisplayModes.e(AdView.this.v, AdView.this.x);
                        return;
                    case 8:
                        AdView.this.b();
                        return;
                    case 9:
                        Ad.a = CustomDialog.showDialog(AdView.this.v, AdView.this.A, null);
                        Ad.a.show();
                        return;
                    case AdMogoUtil.NETWORK_TYPE_ADMOGO:
                    case AdMogoUtil.NETWORK_TYPE_MOBCLIX:
                    case AdMogoUtil.NETWORK_TYPE_MDOTM:
                    case AdMogoUtil.NETWORK_TYPE_4THSCREEN:
                    case AdMogoUtil.NETWORK_TYPE_ADSENSE:
                    default:
                        return;
                    case AdMogoUtil.NETWORK_TYPE_DOUBLECLICK:
                        break;
                }
                if (AdView.this.r.a != null) {
                    AdView.this.r.a.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                    AdView.this.r.a.setFocusableInTouchMode(true);
                    AdView.this.r.a.setFocusable(true);
                    AdView.this.r.a.setMarqueeRepeatLimit(-1);
                    AdView.this.r.a.requestFocus();
                }
            }
        };
        q = context;
        this.g = 0;
        this.i = -1;
        this.h = 200;
        this.o = false;
        if (i4 != 0) {
            this.g = i4;
        }
        if (i5 != 0) {
            this.i = i5;
        }
        if (i3 != 0) {
            this.h = i3;
        }
        this.k = z2;
        this.e = i6;
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            this.k = attributeSet.getAttributeBooleanValue(str, "winAdTesting", true);
            this.i = attributeSet.getAttributeUnsignedIntValue(str, "winAdTextColor", -1);
            this.g = attributeSet.getAttributeUnsignedIntValue(str, "winAdBackgroundColor", 0);
            this.j = attributeSet.getAttributeValue(str, "winAdkeywords");
            this.e = attributeSet.getAttributeIntValue(str, "winAdRefreshInterval", 10);
            this.h = attributeSet.getAttributeUnsignedIntValue(str, "winAdBackgroundAlpha", 200);
            BannerLogger.d("winAd", "you  set freshAdsEverytime is  " + this.e);
            setGoneWithoutAd(attributeSet.getAttributeBooleanValue(str, "isGoneWithoutAd", isGoneWithoutAd()));
        }
        AdManager.setInTestMode(this.k);
        setTextColor(this.i);
        setBackgroundColor(this.g);
        BannerLogger.d("winAd", "backgroundAlpha adView== " + attributeSet + this.h);
        setBackgroundAlpha(this.h);
        if (context.getResources().getConfiguration().orientation == 2) {
            HEIGHT = (int) (context.getResources().getDisplayMetrics().density * 50.0f);
        }
        if (context.getResources().getConfiguration().orientation == 1) {
            HEIGHT = (int) (context.getResources().getDisplayMetrics().density * 50.0f);
        }
        BannerLogger.d("winAd", "height== " + HEIGHT);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.content.Context r5, int r6) {
        /*
            r4 = this;
            com.winad.android.ads.Ad r0 = r4.c     // Catch:{ Exception -> 0x002e, all -> 0x002c }
            java.lang.String r0 = com.winad.android.ads.AdManager.a(r0, r5, r6)     // Catch:{ Exception -> 0x002e, all -> 0x002c }
            a(r0)     // Catch:{ Exception -> 0x002e, all -> 0x002c }
            java.lang.String r1 = "winAd"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x002e, all -> 0x002c }
            r2.<init>()     // Catch:{ Exception -> 0x002e, all -> 0x002c }
            java.lang.String r3 = "submit server type is"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x002e, all -> 0x002c }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x002e, all -> 0x002c }
            java.lang.String r3 = "you will submit message is "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x002e, all -> 0x002c }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x002e, all -> 0x002c }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x002e, all -> 0x002c }
            com.winad.android.ads.BannerLogger.d(r1, r0)     // Catch:{ Exception -> 0x002e, all -> 0x002c }
        L_0x002b:
            return
        L_0x002c:
            r0 = move-exception
            throw r0
        L_0x002e:
            r0 = move-exception
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.winad.android.ads.AdView.a(android.content.Context, int):void");
    }

    /* access modifiers changed from: private */
    public void a(AdContainer adContainer) {
        BannerLogger.i("winAd", "isOnScreen==  " + this.o + "newAd== " + adContainer);
        this.b = adContainer;
        if (this.o) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(1500);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            startAnimation(alphaAnimation);
            this.u = false;
        }
    }

    /* access modifiers changed from: private */
    public void a(final AdContainer adContainer, final int i2) {
        Context context = getContext();
        adContainer.setVisibility(8);
        Animation animation = null;
        switch (i2) {
            case 0:
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) getWidth(), 0.0f, 0.0f);
                translateAnimation.setDuration(500);
                animation = translateAnimation;
                break;
            case 1:
                TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-getHeight()));
                translateAnimation2.setDuration(500);
                animation = translateAnimation2;
                break;
            case 2:
                animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(500);
                animation.startNow();
                break;
            case 3:
                animation = new Rotate3dAnimation(180.0f, 0.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, 0.0f * ((float) getWidth()), context, false);
                animation.setDuration(500);
                break;
        }
        animation.setFillAfter(true);
        animation.setInterpolator(new AccelerateInterpolator());
        animation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
                AdView.this.postDelayed(new SwapViews(adContainer, i2), 250);
            }
        });
        if (i2 == 3) {
            startAnimation(animation);
            return;
        }
        if (this.b.e != null) {
            this.b.e.startAnimation(animation);
        }
        if (this.b.b != null) {
            this.b.b.startAnimation(animation);
        }
        if (this.b.a != null) {
            this.b.a.startAnimation(animation);
        }
        if (this.b.d != null) {
            this.b.d.startAnimation(animation);
        }
    }

    private static void a(String str) {
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        defaultHttpClient.getParams().setIntParameter("http.socket.timeout", l);
        String[] wapUrl = ConnectType.getWapUrl(q, "http://www.winads.cn/up.action");
        BannerLogger.d("winAd", "wapUrl is " + wapUrl);
        HttpPost httpPost = wapUrl != null ? new HttpPost(wapUrl[0]) : new HttpPost("http://www.winads.cn/up.action");
        StringEntity stringEntity = new StringEntity(str);
        if (wapUrl != null) {
            httpPost.addHeader("X-Online-Host", wapUrl[1]);
        }
        httpPost.addHeader("Content-Type", "text/html");
        httpPost.setEntity(stringEntity);
        HttpResponse execute = defaultHttpClient.execute(httpPost);
        BannerLogger.d("lxs", "code == " + execute.getStatusLine().getStatusCode());
        execute.getEntity();
        defaultHttpClient.getConnectionManager().shutdown();
    }

    private void a(boolean z2) {
        synchronized (this) {
            BannerLogger.d("winAd", "start == " + z2 + " requestIntervalTimer== " + f + "requestInterval" + this.e);
            if (!z2 || this.e <= 0) {
                if ((!z2 || this.e == 0) && f != null) {
                    f.cancel();
                    f = null;
                }
            } else if (f == null) {
                f = new Timer();
                f.schedule(new TimerTask() {
                    public void run() {
                        if (Log.isLoggable("winAd", 3)) {
                            int p = AdView.this.e / AdManager.AD_FILL_PARENT;
                            if (Log.isLoggable("winAd", 3)) {
                                BannerLogger.d("winAd", "Requesting a fresh ad because a request interval passed (" + p + " seconds).");
                            }
                        }
                        AdView.this.requestFreshAd();
                    }
                }, (long) this.e, (long) this.e);
            }
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            String styleTitle = this.c.getStyleTitle();
            if (DownLoad.isDownLoading(this.c.getAdId())) {
                Toast.makeText(q, styleTitle + "正在下载中...", 1).show();
                return;
            }
            DownLoad downLoad = new DownLoad(this.v, this.x, styleTitle, this.c.e);
            BannerLogger.d("winAd", "action== " + ((Activity) this.v).getIntent().getAction());
            c();
            this.u = true;
            downLoad.setOnPanelListener(this);
            downLoad.execute(this.x);
            return;
        }
        Toast.makeText(this.v, "请插入您的储存卡", 1).show();
    }

    /* access modifiers changed from: private */
    public void c() {
        new Thread() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.winad.android.ads.AdView.a(com.winad.android.ads.AdView, boolean):boolean
             arg types: [com.winad.android.ads.AdView, int]
             candidates:
              com.winad.android.ads.AdView.a(com.winad.android.ads.AdView, android.content.Context):android.content.Context
              com.winad.android.ads.AdView.a(com.winad.android.ads.AdView, com.winad.android.ads.Ad):com.winad.android.ads.Ad
              com.winad.android.ads.AdView.a(com.winad.android.ads.AdView, com.winad.android.ads.AdContainer):com.winad.android.ads.AdContainer
              com.winad.android.ads.AdView.a(com.winad.android.ads.AdView, java.lang.String):java.lang.String
              com.winad.android.ads.AdView.a(android.content.Context, int):void
              com.winad.android.ads.AdView.a(com.winad.android.ads.AdContainer, int):void
              com.winad.android.ads.AdView.a(com.winad.android.ads.AdView, boolean):boolean */
            public void run() {
                BannerLogger.d("winAd", "ClickSubmit== " + AdView.this.s);
                if (AdView.this.s) {
                    AdView.this.a(AdView.this.getContext(), 1);
                    boolean unused = AdView.this.s = false;
                }
            }
        }.start();
    }

    public void downLoadCompleted() {
    }

    public void downLoadFalse() {
    }

    public int getBackgroundAlpha() {
        return this.h;
    }

    public int getBackgroundColor() {
        return this.g;
    }

    public String getKeywords() {
        return this.j;
    }

    public int getRequestInterval() {
        return this.e / AdManager.AD_FILL_PARENT;
    }

    public String getSearchQuery() {
        return this.m;
    }

    public int getTextColor() {
        return this.i;
    }

    public int getVisibility() {
        if (!this.n || hasAd()) {
            return super.getVisibility();
        }
        return 8;
    }

    public boolean hasAd() {
        return this.b != null;
    }

    public boolean isGoneWithoutAd() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        BannerLogger.d("hasWindowFous", "onAttachedToWindow");
        onDestroy();
        this.o = true;
        super.onAttachedToWindow();
    }

    public void onDestroy() {
        try {
            BannerLogger.d("winAd", "onDestroy is  ad == " + this.b);
            if (this.b != null) {
                removeView(this.b);
                this.b.b();
                this.b = null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.o = false;
        a(false);
        BannerLogger.d("hasWindowFous", "onDetechedFromWindow   ");
        if (Ad.a != null) {
            Ad.a.dismiss();
        }
        if (CustomDialog.c != null) {
            CustomDialog.c.dismiss();
        }
        onDestroy();
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        setMeasuredDimension(getMeasuredWidth(), HEIGHT);
    }

    public void onWindowFocusChanged(boolean z2) {
        BannerLogger.d("hasWindowFous", "hasWindowFous is " + z2);
        this.z = z2;
        a(z2);
        if (z2 && Ad.a != null) {
            Ad.a.dismiss();
        }
        if (z2 && CustomDialog.c != null) {
            CustomDialog.c.dismiss();
        }
    }

    public void requestFreshAd() {
        Context context = getContext();
        if (!Utilities.f(context) || !this.z) {
            HEIGHT = 0;
            return;
        }
        HEIGHT = (int) (context.getResources().getDisplayMetrics().density * 50.0f);
        BannerLogger.d("lxs", "start requestFreshAd......" + super.getVisibility());
        if (super.getVisibility() != 0) {
            Log.w("winAd", "Cannot requestFreshAd() when the AdView is not visible.  Call AdView.setVisibility(View.VISIBLE) first.");
        } else if (!this.d) {
            this.d = true;
            if (p == null) {
                p = new Handler();
            }
            new Thread() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.winad.android.ads.AdView.c(com.winad.android.ads.AdView, boolean):boolean
                 arg types: [com.winad.android.ads.AdView, int]
                 candidates:
                  com.winad.android.ads.AdView.c(com.winad.android.ads.AdView, com.winad.android.ads.AdContainer):com.winad.android.ads.AdContainer
                  com.winad.android.ads.AdView.c(com.winad.android.ads.AdView, java.lang.String):java.lang.String
                  com.winad.android.ads.AdView.c(com.winad.android.ads.AdView, boolean):boolean */
                public void run() {
                    try {
                        Context context = AdView.this.getContext();
                        Ad unused = AdView.this.c = AdRequester.requestAd(context, Boolean.valueOf(AdView.this.k), "http://www.winads.cn/getAdByClient.action", "");
                        if (AdView.this.c != null) {
                            BannerLogger.d("winAd", "you get alrealy newad ");
                            synchronized (this) {
                                if (AdView.this.b == null || !AdView.this.c.equals(AdView.this.b.a()) || AdManager.isInTestMode()) {
                                    final boolean z = AdView.this.b == null;
                                    final int o = AdView.super.getVisibility();
                                    AdContainer unused2 = AdView.this.r = new AdContainer(AdView.this.c, context, AdView.this.A);
                                    AdView.this.r.setBackgroundColor(AdView.this.getBackgroundColor());
                                    AdView.this.r.setTextColor(AdView.this.getTextColor());
                                    AdView.this.r.setBackgroundAlpha(AdView.this.getBackgroundAlpha());
                                    AdView.this.r.setVisibility(o);
                                    AdView.this.r.setLayoutParams(new RelativeLayout.LayoutParams(-1, AdView.HEIGHT));
                                    boolean unused3 = AdView.this.t = true;
                                    new Thread() {
                                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                         method: com.winad.android.ads.AdView.c(com.winad.android.ads.AdView, boolean):boolean
                                         arg types: [com.winad.android.ads.AdView, int]
                                         candidates:
                                          com.winad.android.ads.AdView.c(com.winad.android.ads.AdView, com.winad.android.ads.AdContainer):com.winad.android.ads.AdContainer
                                          com.winad.android.ads.AdView.c(com.winad.android.ads.AdView, java.lang.String):java.lang.String
                                          com.winad.android.ads.AdView.c(com.winad.android.ads.AdView, boolean):boolean */
                                        public void run() {
                                            if (AdView.this.t) {
                                                AdView.this.a(AdView.this.getContext(), 0);
                                                boolean unused = AdView.this.t = false;
                                            }
                                        }
                                    }.start();
                                    AdView.p.post(new Runnable() {
                                        public void run() {
                                            try {
                                                AdView.this.addView(AdView.this.r);
                                                if (o == 0) {
                                                    BannerLogger.d("winAd", "firstAd== " + z);
                                                    if (z) {
                                                        AdView.this.a(AdView.this.r);
                                                    } else {
                                                        AdView.this.a(AdView.this.r, new Random().nextInt(4));
                                                    }
                                                } else {
                                                    AdContainer unused = AdView.this.b = AdView.this.r;
                                                }
                                                if (!Common.KIMP.equalsIgnoreCase(AdView.this.c.getshowType())) {
                                                    AdView.this.A.sendEmptyMessage(15);
                                                }
                                                if (AdView.this.a != null) {
                                                    try {
                                                        AdView.this.a.onReceiveAd(AdView.this);
                                                    } catch (Exception e) {
                                                        Log.w("winAd", "Unhandled exception raised in your AdListener.onReceiveAd.", e);
                                                    }
                                                }
                                            } catch (Exception e2) {
                                                Log.e("winAd", "Unhandled exception placing AdContainer into AdView.", e2);
                                            } finally {
                                                boolean unused2 = AdView.this.d = false;
                                            }
                                        }
                                    });
                                } else {
                                    boolean unused4 = AdView.this.t = true;
                                    new Thread() {
                                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                         method: com.winad.android.ads.AdView.c(com.winad.android.ads.AdView, boolean):boolean
                                         arg types: [com.winad.android.ads.AdView, int]
                                         candidates:
                                          com.winad.android.ads.AdView.c(com.winad.android.ads.AdView, com.winad.android.ads.AdContainer):com.winad.android.ads.AdContainer
                                          com.winad.android.ads.AdView.c(com.winad.android.ads.AdView, java.lang.String):java.lang.String
                                          com.winad.android.ads.AdView.c(com.winad.android.ads.AdView, boolean):boolean */
                                        public void run() {
                                            if (AdView.this.t) {
                                                AdView.this.a(AdView.this.getContext(), 0);
                                                boolean unused = AdView.this.t = false;
                                            }
                                        }
                                    }.start();
                                    BannerLogger.d("winAd", "Received the same ad we already had.  Discarding it.");
                                    if (Log.isLoggable("winAd", 3)) {
                                        BannerLogger.d("winAd", "Received the same ad we already had.  Discarding it.");
                                    }
                                    boolean unused5 = AdView.this.d = false;
                                }
                            }
                            return;
                        }
                        AdView.HEIGHT = 0;
                        if (AdView.this.a != null) {
                            try {
                                AdView.this.a.onFailedToReceiveAd(AdView.this);
                            } catch (Exception e) {
                                Log.w("winAd", "Unhandled exception raised in your AdListener.onFailedToReceiveAd.", e);
                            }
                        }
                        boolean unused6 = AdView.this.d = false;
                    } catch (Exception e2) {
                        Log.e("winAd", "Unhandled exception requesting a fresh ad.", e2);
                        boolean unused7 = AdView.this.d = false;
                    }
                }
            }.start();
        } else if (Log.isLoggable("winAd", 3)) {
            BannerLogger.d("winAd", "Ignoring requestFreshAd() because we are already getting a fresh ad.");
        }
    }

    public void setBackgroundAlpha(int i2) {
        this.h = i2;
        if (this.b != null) {
            this.b.setBackgroundAlpha(i2);
        }
        invalidate();
    }

    public void setBackgroundColor(int i2) {
        this.g = i2;
        if (this.b != null) {
            this.b.setBackgroundColor(i2);
        }
        invalidate();
    }

    public void setGoneWithoutAd(boolean z2) {
        this.n = z2;
    }

    public void setKeywords(String str) {
        this.j = str;
    }

    public void setListener(ADListener aDListener) {
        synchronized (this) {
            this.a = aDListener;
        }
    }

    public void setPublishId(String str) {
        AdManager.setPublisherId(str);
    }

    public void setRequestInterval(int i2) {
        int i3 = 10;
        if (i2 <= 0) {
            i3 = 0;
        } else if (i2 >= 10) {
            i3 = i2 > 600 ? 600 : i2;
        }
        this.e = i3 * AdManager.AD_FILL_PARENT;
        if (i3 == 0) {
            a(false);
            return;
        }
        BannerLogger.i("winAd", "Requesting fresh ads every " + i3 + " seconds.");
        a(true);
    }

    public void setSearchQuery(String str) {
        this.m = str;
    }

    public void setTestMode(boolean z2) {
        this.k = z2;
        AdManager.setInTestMode(z2);
    }

    public void setTextColor(int i2) {
        this.i = -16777216 | i2;
        if (this.b != null) {
            this.b.setTextColor(i2);
        }
        invalidate();
    }

    public void setVisibility(int i2) {
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    getChildAt(i3).setVisibility(i2);
                }
                super.setVisibility(i2);
                if (i2 == 0) {
                    requestFreshAd();
                } else {
                    onDestroy();
                    invalidate();
                }
            }
        }
    }

    public void shutAdRequest() {
        a(false);
        setVisibility(8);
        onDestroy();
    }
}
