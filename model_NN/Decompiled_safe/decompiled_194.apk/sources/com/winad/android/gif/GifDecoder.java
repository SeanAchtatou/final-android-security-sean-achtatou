package com.winad.android.gif;

import android.graphics.Bitmap;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class GifDecoder extends Thread {
    public static final int STATUS_FINISH = -1;
    public static final int STATUS_FORMAT_ERROR = 1;
    public static final int STATUS_OPEN_ERROR = 2;
    public static final int STATUS_PARSING = 0;
    private boolean A = false;
    private byte[] B = new byte[256];
    private int C = 0;
    private int D = 0;
    private int E = 0;
    private boolean F = false;
    private int G = 0;
    private int H;

    /* renamed from: I  reason: collision with root package name */
    private short[] f6I;
    private byte[] J;
    private byte[] K;
    private byte[] L;
    private GifFrame M;
    private int N;
    private GifAction O = null;
    private byte[] P = null;
    private InputStream a;
    private int b;
    private boolean c;
    private int d;
    private int e = 1;
    private int[] f;
    private int[] g;
    private int[] h;
    public int height;
    private int i;
    private int j;
    private int k;
    private int l;
    private boolean m;
    private boolean n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private int w;
    public int width;
    private Bitmap x;
    private Bitmap y;
    private GifFrame z = null;

    public GifDecoder(InputStream inputStream, GifAction gifAction) {
        this.a = inputStream;
        this.O = gifAction;
    }

    public GifDecoder(byte[] bArr, GifAction gifAction) {
        this.P = bArr;
        this.O = gifAction;
    }

    private void a() {
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int[] iArr = new int[(this.width * this.height)];
        if (this.E > 0) {
            if (this.E == 3) {
                int i6 = this.N - 2;
                if (i6 > 0) {
                    this.y = getFrameImage(i6 - 1);
                } else {
                    this.y = null;
                }
            }
            if (this.y != null) {
                this.y.getPixels(iArr, 0, this.width, 0, 0, this.width, this.height);
                if (this.E == 2) {
                    int i7 = !this.F ? this.k : 0;
                    for (int i8 = 0; i8 < this.w; i8++) {
                        int i9 = ((this.u + i8) * this.width) + this.t;
                        int i10 = this.v + i9;
                        while (i9 < i10) {
                            iArr[i9] = i7;
                            i9++;
                        }
                    }
                }
            }
        }
        int i11 = 8;
        int i12 = 1;
        int i13 = 0;
        while (i13 < this.s) {
            if (this.n) {
                if (i5 >= this.s) {
                    i12++;
                    switch (i12) {
                        case 2:
                            i5 = 4;
                            break;
                        case 3:
                            i5 = 2;
                            i11 = 4;
                            break;
                        case 4:
                            i5 = 1;
                            i11 = 2;
                            break;
                    }
                }
                i2 = i12;
                i3 = i11;
                i4 = i5 + i11;
            } else {
                i2 = i12;
                i3 = i11;
                i4 = i5;
                i5 = i13;
            }
            int i14 = i5 + this.q;
            if (i14 < this.height) {
                int i15 = i14 * this.width;
                int i16 = this.p + i15;
                int i17 = this.r + i16;
                int i18 = this.width + i15 < i17 ? i15 + this.width : i17;
                int i19 = i16;
                int i20 = this.r * i13;
                while (i19 < i18) {
                    int i21 = i20 + 1;
                    int i22 = this.h[this.L[i20] & 255];
                    if (i22 != 0) {
                        iArr[i19] = i22;
                    }
                    i19++;
                    i20 = i21;
                }
            }
            i13++;
            i5 = i4;
            i11 = i3;
            i12 = i2;
        }
        if (this.x != null) {
            this.x.recycle();
            this.x = null;
        }
        this.x = Bitmap.createBitmap(iArr, this.width, this.height, Bitmap.Config.ARGB_4444);
    }

    private int[] a(int i2) {
        int i3;
        int i4 = i2 * 3;
        byte[] bArr = new byte[i4];
        try {
            i3 = this.a.read(bArr);
        } catch (Exception e2) {
            e2.printStackTrace();
            i3 = 0;
        }
        if (i3 < i4) {
            this.b = 1;
            return null;
        }
        int[] iArr = new int[256];
        int i5 = 0;
        for (int i6 = 0; i6 < i2; i6++) {
            int i7 = i5 + 1;
            int i8 = i7 + 1;
            iArr[i6] = ((bArr[i5] & 255) << 16) | -16777216 | ((bArr[i7] & 255) << 8) | (bArr[i8] & 255);
            i5 = i8 + 1;
        }
        return iArr;
    }

    private int b() {
        this.a = new ByteArrayInputStream(this.P);
        this.P = null;
        return c();
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 136 */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0056 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0059 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x009c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0044 A[LOOP:0: B:10:0x0047->B:9:0x0044, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(int r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            if (r7 != r4) goto L_0x009e
            java.io.InputStream r0 = r6.a
            if (r0 == 0) goto L_0x003f
            java.io.InputStream r0 = r6.a     // Catch:{ Exception -> 0x0040 }
            r0.reset()     // Catch:{ Exception -> 0x0040 }
            java.io.InputStream r0 = r6.a     // Catch:{ Exception -> 0x0040 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ Exception -> 0x0040 }
            r6.x = r0     // Catch:{ Exception -> 0x0040 }
            android.graphics.Bitmap r0 = r6.x     // Catch:{ Exception -> 0x0040 }
            int r0 = r0.getWidth()     // Catch:{ Exception -> 0x0040 }
            r6.width = r0     // Catch:{ Exception -> 0x0040 }
            android.graphics.Bitmap r0 = r6.x     // Catch:{ Exception -> 0x0040 }
            int r0 = r0.getHeight()     // Catch:{ Exception -> 0x0040 }
            r6.height = r0     // Catch:{ Exception -> 0x0040 }
            int r0 = r6.N     // Catch:{ Exception -> 0x0040 }
            int r0 = r0 + 1
            r6.N = r0     // Catch:{ Exception -> 0x0040 }
            com.winad.android.gif.GifFrame r0 = new com.winad.android.gif.GifFrame     // Catch:{ Exception -> 0x0040 }
            android.graphics.Bitmap r1 = r6.x     // Catch:{ Exception -> 0x0040 }
            r2 = 1000(0x3e8, float:1.401E-42)
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0040 }
            r6.M = r0     // Catch:{ Exception -> 0x0040 }
            com.winad.android.gif.GifFrame r0 = r6.M     // Catch:{ Exception -> 0x0040 }
            r1 = 0
            r0.nextFrame = r1     // Catch:{ Exception -> 0x0040 }
            com.winad.android.gif.GifFrame r0 = r6.M     // Catch:{ Exception -> 0x0040 }
            r6.z = r0     // Catch:{ Exception -> 0x0040 }
        L_0x003f:
            return
        L_0x0040:
            r0 = move-exception
            r6.b = r4
            goto L_0x003f
        L_0x0044:
            r6.k()
        L_0x0047:
            if (r0 != 0) goto L_0x003f
            boolean r1 = r6.e()
            if (r1 != 0) goto L_0x003f
            int r1 = r6.g()
            switch(r1) {
                case 0: goto L_0x0047;
                case 33: goto L_0x0059;
                case 44: goto L_0x0044;
                case 59: goto L_0x009c;
                default: goto L_0x0056;
            }
        L_0x0056:
            r6.b = r4
            goto L_0x0047
        L_0x0059:
            int r1 = r6.g()
            switch(r1) {
                case 249: goto L_0x0064;
                case 255: goto L_0x0068;
                default: goto L_0x0060;
            }
        L_0x0060:
            r6.p()
            goto L_0x0047
        L_0x0064:
            r6.i()
            goto L_0x0047
        L_0x0068:
            r6.h()
            java.lang.String r1 = ""
            r2 = r1
            r1 = r5
        L_0x006f:
            r3 = 11
            if (r1 >= r3) goto L_0x008c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            byte[] r3 = r6.B
            byte r3 = r3[r1]
            char r3 = (char) r3
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            int r1 = r1 + 1
            goto L_0x006f
        L_0x008c:
            java.lang.String r1 = "NETSCAPE2.0"
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x0098
            r6.m()
            goto L_0x0047
        L_0x0098:
            r6.p()
            goto L_0x0047
        L_0x009c:
            r0 = r4
            goto L_0x0047
        L_0x009e:
            r0 = r5
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: com.winad.android.gif.GifDecoder.b(int):void");
    }

    private int c() {
        f();
        if (this.a != null) {
            int j2 = j();
            if (!e()) {
                b(j2);
                if (this.N < 0) {
                    this.b = 1;
                    this.O.parseOk(false, -1);
                } else {
                    this.b = -1;
                    this.O.parseOk(true, 1);
                }
            }
            try {
                if (this.a != null) {
                    this.a.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else {
            this.b = 2;
            this.O.parseOk(false, -1);
        }
        return this.b;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r9v12, types: [int] */
    /* JADX WARN: Type inference failed for: r16v7, types: [int] */
    /* JADX WARN: Type inference failed for: r17v6, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d() {
        /*
            r24 = this;
            r2 = -1
            r0 = r24
            int r0 = r0.r
            r3 = r0
            r0 = r24
            int r0 = r0.s
            r4 = r0
            int r3 = r3 * r4
            r0 = r24
            byte[] r0 = r0.L
            r4 = r0
            if (r4 == 0) goto L_0x001b
            r0 = r24
            byte[] r0 = r0.L
            r4 = r0
            int r4 = r4.length
            if (r4 >= r3) goto L_0x0022
        L_0x001b:
            byte[] r4 = new byte[r3]
            r0 = r4
            r1 = r24
            r1.L = r0
        L_0x0022:
            r0 = r24
            short[] r0 = r0.f6I
            r4 = r0
            if (r4 != 0) goto L_0x0032
            r4 = 4096(0x1000, float:5.74E-42)
            short[] r4 = new short[r4]
            r0 = r4
            r1 = r24
            r1.f6I = r0
        L_0x0032:
            r0 = r24
            byte[] r0 = r0.J
            r4 = r0
            if (r4 != 0) goto L_0x0042
            r4 = 4096(0x1000, float:5.74E-42)
            byte[] r4 = new byte[r4]
            r0 = r4
            r1 = r24
            r1.J = r0
        L_0x0042:
            r0 = r24
            byte[] r0 = r0.K
            r4 = r0
            if (r4 != 0) goto L_0x0052
            r4 = 4097(0x1001, float:5.741E-42)
            byte[] r4 = new byte[r4]
            r0 = r4
            r1 = r24
            r1.K = r0
        L_0x0052:
            int r4 = r24.g()
            r5 = 1
            int r5 = r5 << r4
            int r6 = r5 + 1
            int r7 = r5 + 2
            int r8 = r4 + 1
            r9 = 1
            int r9 = r9 << r8
            r10 = 1
            int r9 = r9 - r10
            r10 = 0
        L_0x0063:
            if (r10 >= r5) goto L_0x0078
            r0 = r24
            short[] r0 = r0.f6I
            r11 = r0
            r12 = 0
            r11[r10] = r12
            r0 = r24
            byte[] r0 = r0.J
            r11 = r0
            byte r12 = (byte) r10
            r11[r10] = r12
            int r10 = r10 + 1
            goto L_0x0063
        L_0x0078:
            r10 = 0
            r11 = 0
            r12 = r11
            r13 = r10
            r14 = r10
            r15 = r2
            r16 = r8
            r17 = r9
            r18 = r7
            r11 = r10
            r7 = r10
            r8 = r10
            r9 = r10
        L_0x0088:
            if (r12 >= r3) goto L_0x0099
            if (r9 != 0) goto L_0x0184
            r0 = r14
            r1 = r16
            if (r0 >= r1) goto L_0x00c6
            if (r13 != 0) goto L_0x00ad
            int r8 = r24.h()
            if (r8 > 0) goto L_0x00a7
        L_0x0099:
            r2 = r7
        L_0x009a:
            if (r2 >= r3) goto L_0x019f
            r0 = r24
            byte[] r0 = r0.L
            r4 = r0
            r5 = 0
            r4[r2] = r5
            int r2 = r2 + 1
            goto L_0x009a
        L_0x00a7:
            r13 = 0
            r23 = r13
            r13 = r8
            r8 = r23
        L_0x00ad:
            r0 = r24
            byte[] r0 = r0.B
            r19 = r0
            byte r19 = r19[r8]
            r0 = r19
            r0 = r0 & 255(0xff, float:3.57E-43)
            r19 = r0
            int r19 = r19 << r14
            int r11 = r11 + r19
            int r14 = r14 + 8
            int r8 = r8 + 1
            int r13 = r13 + -1
            goto L_0x0088
        L_0x00c6:
            r19 = r11 & r17
            int r11 = r11 >> r16
            int r14 = r14 - r16
            r0 = r19
            r1 = r18
            if (r0 > r1) goto L_0x0099
            r0 = r19
            r1 = r6
            if (r0 == r1) goto L_0x0099
            r0 = r19
            r1 = r5
            if (r0 != r1) goto L_0x00f0
            int r15 = r4 + 1
            r16 = 1
            int r16 = r16 << r15
            r17 = 1
            int r16 = r16 - r17
            int r17 = r5 + 2
            r18 = r17
            r17 = r16
            r16 = r15
            r15 = r2
            goto L_0x0088
        L_0x00f0:
            if (r15 != r2) goto L_0x0109
            r0 = r24
            byte[] r0 = r0.K
            r10 = r0
            int r15 = r9 + 1
            r0 = r24
            byte[] r0 = r0.J
            r20 = r0
            byte r20 = r20[r19]
            r10[r9] = r20
            r9 = r15
            r10 = r19
            r15 = r19
            goto L_0x0088
        L_0x0109:
            r0 = r19
            r1 = r18
            if (r0 != r1) goto L_0x01a5
            r0 = r24
            byte[] r0 = r0.K
            r20 = r0
            int r21 = r9 + 1
            byte r10 = (byte) r10
            r20[r9] = r10
            r9 = r21
            r10 = r15
        L_0x011d:
            if (r10 <= r5) goto L_0x013c
            r0 = r24
            byte[] r0 = r0.K
            r20 = r0
            int r21 = r9 + 1
            r0 = r24
            byte[] r0 = r0.J
            r22 = r0
            byte r22 = r22[r10]
            r20[r9] = r22
            r0 = r24
            short[] r0 = r0.f6I
            r9 = r0
            short r9 = r9[r10]
            r10 = r9
            r9 = r21
            goto L_0x011d
        L_0x013c:
            r0 = r24
            byte[] r0 = r0.J
            r20 = r0
            byte r10 = r20[r10]
            r10 = r10 & 255(0xff, float:3.57E-43)
            r20 = 4096(0x1000, float:5.74E-42)
            r0 = r18
            r1 = r20
            if (r0 >= r1) goto L_0x0099
            r0 = r24
            byte[] r0 = r0.K
            r20 = r0
            int r21 = r9 + 1
            r0 = r10
            byte r0 = (byte) r0
            r22 = r0
            r20[r9] = r22
            r0 = r24
            short[] r0 = r0.f6I
            r9 = r0
            short r15 = (short) r15
            r9[r18] = r15
            r0 = r24
            byte[] r0 = r0.J
            r9 = r0
            byte r15 = (byte) r10
            r9[r18] = r15
            int r9 = r18 + 1
            r15 = r9 & r17
            if (r15 != 0) goto L_0x01a0
            r15 = 4096(0x1000, float:5.74E-42)
            if (r9 >= r15) goto L_0x01a0
            int r15 = r16 + 1
            int r16 = r17 + r9
        L_0x017a:
            r17 = r16
            r18 = r9
            r16 = r15
            r9 = r21
            r15 = r19
        L_0x0184:
            int r9 = r9 + -1
            r0 = r24
            byte[] r0 = r0.L
            r19 = r0
            int r20 = r7 + 1
            r0 = r24
            byte[] r0 = r0.K
            r21 = r0
            byte r21 = r21[r9]
            r19[r7] = r21
            int r7 = r12 + 1
            r12 = r7
            r7 = r20
            goto L_0x0088
        L_0x019f:
            return
        L_0x01a0:
            r15 = r16
            r16 = r17
            goto L_0x017a
        L_0x01a5:
            r10 = r19
            goto L_0x011d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.winad.android.gif.GifDecoder.d():void");
    }

    private boolean e() {
        return this.b != 0;
    }

    private void f() {
        this.b = 0;
        this.N = 0;
        this.M = null;
        this.f = null;
        this.g = null;
    }

    private int g() {
        try {
            return this.a.read();
        } catch (Exception e2) {
            this.b = 1;
            return 0;
        }
    }

    private int h() {
        this.C = g();
        int i2 = 0;
        if (this.C > 0) {
            while (i2 < this.C) {
                try {
                    int read = this.a.read(this.B, i2, this.C - i2);
                    if (read == -1) {
                        break;
                    }
                    i2 += read;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (i2 < this.C) {
                this.b = 1;
            }
        }
        return i2;
    }

    private void i() {
        g();
        int g2 = g();
        this.D = (g2 & 28) >> 2;
        if (this.D == 0) {
            this.D = 1;
        }
        this.F = (g2 & 1) != 0;
        this.G = n() * 10;
        this.H = g();
        g();
    }

    private int j() {
        String str = "";
        for (int i2 = 0; i2 < 6; i2++) {
            str = str + ((char) g());
        }
        if (!str.startsWith("GIF")) {
            return 1;
        }
        l();
        if (this.c && !e()) {
            this.f = a(this.d);
            this.j = this.f[this.i];
        }
        return 0;
    }

    private void k() {
        int i2;
        this.p = n();
        this.q = n();
        this.r = n();
        this.s = n();
        int g2 = g();
        this.m = (g2 & 128) != 0;
        this.n = (g2 & 64) != 0;
        this.o = 2 << (g2 & 7);
        if (this.m) {
            this.g = a(this.o);
            this.h = this.g;
        } else {
            this.h = this.f;
            if (this.i == this.H) {
                this.j = 0;
            }
        }
        if (this.F) {
            i2 = this.h[this.H];
            this.h[this.H] = 0;
        } else {
            i2 = 0;
        }
        if (this.h == null) {
            this.b = 1;
        }
        if (!e()) {
            d();
            p();
            if (!e()) {
                this.N++;
                this.x = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_4444);
                a();
                if (this.M == null) {
                    this.M = new GifFrame(this.x, this.G);
                    this.z = this.M;
                } else {
                    GifFrame gifFrame = this.M;
                    while (gifFrame.nextFrame != null) {
                        gifFrame = gifFrame.nextFrame;
                    }
                    gifFrame.nextFrame = new GifFrame(this.x, this.G);
                }
                if (this.F) {
                    this.h[this.H] = i2;
                }
                o();
                this.O.parseOk(true, this.N);
            }
        }
    }

    private void l() {
        this.width = n();
        this.height = n();
        int g2 = g();
        this.c = (g2 & 128) != 0;
        this.d = 2 << (g2 & 7);
        this.i = g();
        this.l = g();
    }

    private void m() {
        do {
            h();
            if (this.B[0] == 1) {
                this.e = (this.B[1] & 255) | ((this.B[2] & 255) << 8);
            }
            if (this.C <= 0) {
                return;
            }
        } while (!e());
    }

    private int n() {
        return g() | (g() << 8);
    }

    private void o() {
        this.E = this.D;
        this.t = this.p;
        this.u = this.q;
        this.v = this.r;
        this.w = this.s;
        this.y = this.x;
        this.k = this.j;
        this.D = 0;
        this.F = false;
        this.G = 0;
        this.g = null;
    }

    private void p() {
        do {
            h();
            if (this.C <= 0) {
                return;
            }
        } while (!e());
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        free();
    }

    public void free() {
        if (this.b == 0) {
            this.b = 1;
        }
        GifFrame gifFrame = this.M;
        while (gifFrame != null) {
            if (gifFrame.image != null && !gifFrame.image.isRecycled()) {
                gifFrame.image.recycle();
                gifFrame.image = null;
            }
            this.M = this.M.nextFrame;
            gifFrame = this.M;
        }
        if (this.x != null && !this.x.isRecycled()) {
            this.x.recycle();
            this.x = null;
        }
        if (this.y != null && !this.y.isRecycled()) {
            this.y.recycle();
            this.y = null;
        }
        if (this.a != null) {
            try {
                this.a.close();
            } catch (Exception e2) {
            }
            this.a = null;
        }
        this.P = null;
    }

    public GifFrame getCurrentFrame() {
        return this.z;
    }

    public int getDelay(int i2) {
        GifFrame frame;
        this.G = -1;
        if (i2 >= 0 && i2 < this.N && (frame = getFrame(i2)) != null) {
            this.G = frame.delay;
        }
        return this.G;
    }

    public int[] getDelays() {
        GifFrame gifFrame = this.M;
        int[] iArr = new int[this.N];
        GifFrame gifFrame2 = gifFrame;
        int i2 = 0;
        while (gifFrame2 != null && i2 < this.N) {
            iArr[i2] = gifFrame2.delay;
            gifFrame2 = gifFrame2.nextFrame;
            i2++;
        }
        return iArr;
    }

    public GifFrame getFrame(int i2) {
        GifFrame gifFrame = this.M;
        int i3 = 0;
        while (gifFrame != null) {
            if (i3 == i2) {
                return gifFrame;
            }
            gifFrame = gifFrame.nextFrame;
            i3++;
        }
        return null;
    }

    public int getFrameCount() {
        return this.N;
    }

    public Bitmap getFrameImage(int i2) {
        GifFrame frame = getFrame(i2);
        if (frame == null) {
            return null;
        }
        return frame.image;
    }

    public Bitmap getImage() {
        return getFrameImage(0);
    }

    public int getLoopCount() {
        return this.e;
    }

    public int getStatus() {
        return this.b;
    }

    public GifFrame next() {
        if (!this.A) {
            this.A = true;
            return this.M;
        }
        if (this.b != 0) {
            this.z = this.z.nextFrame;
            if (this.z == null) {
                this.z = this.M;
            }
        } else if (this.z.nextFrame != null) {
            this.z = this.z.nextFrame;
        }
        return this.z;
    }

    public boolean parseOk() {
        return this.b == -1;
    }

    public void reset() {
        this.z = this.M;
    }

    public void run() {
        if (this.a != null) {
            c();
        } else if (this.P != null) {
            b();
        }
    }
}
