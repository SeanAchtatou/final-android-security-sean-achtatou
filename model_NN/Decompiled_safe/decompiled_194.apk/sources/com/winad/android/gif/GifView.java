package com.winad.android.gif;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import com.winad.android.ads.BannerLogger;
import java.io.InputStream;

public class GifView extends View implements GifAction {
    /* access modifiers changed from: private */
    public GifDecoder a;
    /* access modifiers changed from: private */
    public Bitmap b;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public boolean d;
    private int e;
    private int f;
    private Rect g;
    private DrawThread h;
    private GifImageType i;
    /* access modifiers changed from: private */
    public ReDrawHandler j;

    class DrawThread extends Thread {
        private DrawThread() {
        }

        public void run() {
            if (GifView.this.a != null) {
                while (GifView.this.c) {
                    if (!GifView.this.d) {
                        GifFrame next = GifView.this.a.next();
                        Bitmap unused = GifView.this.b = next.image;
                        long j = (long) next.delay;
                        if (GifView.this.j != null) {
                            GifView.this.j.sendMessage(GifView.this.j.obtainMessage());
                            SystemClock.sleep(j);
                        } else {
                            SystemClock.sleep(j);
                        }
                    } else {
                        SystemClock.sleep(10);
                    }
                }
            }
        }
    }

    public enum GifImageType {
        WAIT_FINISH(0),
        SYNC_DECODER(1),
        COVER(2);
        
        final int a;

        private GifImageType(int i) {
            this.a = i;
        }
    }

    class ReDrawHandler extends Handler {
        private ReDrawHandler() {
        }

        public void handleMessage(Message message) {
            GifView.this.invalidate();
        }
    }

    public GifView(Context context) {
        super(context);
        this.a = null;
        this.b = null;
        this.c = true;
        this.d = false;
        this.e = -1;
        this.f = -1;
        this.g = null;
        this.h = null;
        this.i = GifImageType.SYNC_DECODER;
        this.j = null;
    }

    public GifView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public GifView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.a = null;
        this.b = null;
        this.c = true;
        this.d = false;
        this.e = -1;
        this.f = -1;
        this.g = null;
        this.h = null;
        this.i = GifImageType.SYNC_DECODER;
        this.j = null;
    }

    private void a() {
        if (this.j != null) {
            this.j.sendMessage(this.j.obtainMessage());
        }
    }

    private void a(InputStream inputStream) {
        if (this.a != null) {
            this.a.free();
            this.a = null;
        }
        this.a = new GifDecoder(inputStream, this);
        this.a.start();
    }

    private void a(byte[] bArr) {
        if (this.a != null) {
            this.a.free();
            this.a = null;
        }
        this.a = new GifDecoder(bArr, this);
        this.a.start();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        free();
    }

    public void free() {
        BannerLogger.i("lxs", "GifView is free " + this.b);
        this.c = false;
        post(new Runnable() {
            public void run() {
                if (GifView.this.a != null) {
                    GifView.this.a.free();
                    GifDecoder unused = GifView.this.a = (GifDecoder) null;
                }
                if (GifView.this.b != null) {
                    GifView.this.b.recycle();
                    Bitmap unused2 = GifView.this.b = (Bitmap) null;
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.a != null) {
            if (this.b == null) {
                this.b = this.a.getImage();
            }
            if (this.b != null) {
                int saveCount = canvas.getSaveCount();
                canvas.save();
                canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
                if (this.e == -1 || this.f == -1) {
                    canvas.drawBitmap(this.b, 0.0f, 0.0f, (Paint) null);
                } else {
                    canvas.drawBitmap(this.b, (Rect) null, this.g, (Paint) null);
                }
                canvas.restoreToCount(saveCount);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5 = 1;
        if (this.j == null) {
            this.j = new ReDrawHandler();
        }
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        if (this.a == null) {
            i4 = 1;
        } else {
            i5 = this.a.width;
            i4 = this.a.height;
        }
        setMeasuredDimension(resolveSize(Math.max(paddingLeft + paddingRight + i5, getSuggestedMinimumWidth()), i2), resolveSize(Math.max(paddingTop + paddingBottom + i4, getSuggestedMinimumHeight()), i3));
    }

    public void parseOk(boolean z, int i2) {
        if (!z) {
            return;
        }
        if (this.a != null) {
            switch (this.i) {
                case WAIT_FINISH:
                    if (i2 != -1) {
                        return;
                    }
                    if (this.a.getFrameCount() > 1) {
                        new DrawThread().start();
                        return;
                    } else {
                        a();
                        return;
                    }
                case COVER:
                    if (i2 == 1) {
                        this.b = this.a.getImage();
                        a();
                        return;
                    } else if (i2 != -1) {
                        return;
                    } else {
                        if (this.a.getFrameCount() <= 1) {
                            a();
                            return;
                        } else if (this.h == null) {
                            this.h = new DrawThread();
                            this.h.start();
                            return;
                        } else {
                            return;
                        }
                    }
                case SYNC_DECODER:
                    if (i2 == 1) {
                        this.b = this.a.getImage();
                        a();
                        return;
                    } else if (i2 == -1) {
                        a();
                        return;
                    } else if (this.h == null) {
                        this.h = new DrawThread();
                        this.h.start();
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        } else {
            BannerLogger.e("gif", "parse error");
        }
    }

    public void setGifImage(int i2) {
        a(getResources().openRawResource(i2));
    }

    public void setGifImage(InputStream inputStream) {
        a(inputStream);
    }

    public void setGifImage(byte[] bArr) {
        a(bArr);
    }

    public void setGifImageType(GifImageType gifImageType) {
        if (this.a == null) {
            this.i = gifImageType;
        }
    }

    public void setShowDimension(int i2, int i3) {
        if (i2 > 0 && i3 > 0) {
            this.e = i2;
            this.f = i3;
            this.g = new Rect();
            this.g.left = 0;
            this.g.top = 0;
            this.g.right = i2;
            this.g.bottom = i3;
        }
    }

    public void showAnimation() {
        if (this.d) {
            this.d = false;
        }
    }

    public void showCover() {
        if (this.a != null) {
            this.d = true;
            this.b = this.a.getImage();
            invalidate();
        }
    }
}
