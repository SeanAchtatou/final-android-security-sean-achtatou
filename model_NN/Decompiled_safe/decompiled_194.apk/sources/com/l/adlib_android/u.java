package com.l.adlib_android;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.widget.RelativeLayout;
import com.adchina.android.ads.Common;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.apache.commons.codec.binary.Base64;

public final class u {
    public static AdListenerEx a = null;

    public static GradientDrawable a(int[] iArr) {
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, iArr);
        gradientDrawable.setAlpha(k.g);
        return gradientDrawable;
    }

    public static RelativeLayout.LayoutParams a(int i, int i2) {
        return new RelativeLayout.LayoutParams(i, i2);
    }

    public static String a(int i, int i2, long j) {
        int nextInt = new Random().nextInt();
        int[] iArr = new int[7];
        iArr[0] = i;
        iArr[1] = i2;
        iArr[2] = (int) (j & -1);
        iArr[3] = (int) ((j >> 32) & -1);
        iArr[4] = (int) (System.currentTimeMillis() / 1000);
        iArr[5] = nextInt;
        iArr[6] = iArr[0];
        for (int i3 = 0; i3 < iArr.length - 1; i3++) {
            iArr[i3] = (iArr[i3] << 1) | ((iArr[i3 + 1] >> 31) & 1);
        }
        for (int i4 = 0; i4 < 5; i4++) {
            iArr[i4] = iArr[i4] ^ iArr[5];
        }
        for (int i5 = 0; i5 < 5; i5++) {
            iArr[i5] = iArr[i5] ^ 1791174611;
        }
        StringBuilder sb = new StringBuilder();
        for (int i6 = 0; i6 <= 5; i6++) {
            sb.append(a(Integer.toHexString(iArr[i6]), "0"));
        }
        return sb.toString();
    }

    public static String a(Context context, String str) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("ad", 0);
        return (str.equalsIgnoreCase("lastposition") || str.equalsIgnoreCase("lastpositionfull")) ? sharedPreferences.getString(str, "-1") : sharedPreferences.getString(str, "");
    }

    public static String a(InputStream inputStream) {
        if (inputStream == null) {
            return "";
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName(Common.KEnc)), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine.trim());
            } catch (Exception e) {
                e.printStackTrace();
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            } catch (Throwable th) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                }
                throw th;
            }
        }
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        }
        return sb.toString();
    }

    private static String a(String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer(8);
        int length = 8 - str.length();
        while (true) {
            length--;
            if (length < 0) {
                break;
            }
            stringBuffer.append(str2);
        }
        stringBuffer.append(str);
        int length2 = stringBuffer.length() - 8;
        return length2 > 0 ? stringBuffer.substring(length2) : stringBuffer.toString();
    }

    public static Map a(q qVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("key", a(qVar.n(), qVar.c(), qVar.d()));
        Log.i("adsdk", "LastPosition:" + String.valueOf(qVar.n()));
        Log.i("adsdk", "Mid:" + String.valueOf(qVar.c()));
        Log.i("adsdk", "Uid:" + String.valueOf(qVar.d()));
        hashMap.put("os", Byte.toString(qVar.e()));
        hashMap.put("model", qVar.f());
        hashMap.put("sdk", qVar.g());
        hashMap.put("opid", Byte.toString(qVar.h()));
        hashMap.put("cver", qVar.i());
        hashMap.put("contype", Byte.toString(qVar.j()));
        hashMap.put("loc", Byte.toString(qVar.k()));
        hashMap.put("lat", Double.toString(qVar.l()));
        hashMap.put("lng", Double.toString(qVar.m()));
        hashMap.put("adtype", Byte.toString(qVar.o()));
        return hashMap;
    }

    public static void a(int i) {
        if (a != null) {
            a.OnAcceptAd(i);
        }
    }

    public static void a(Context context, String str, String str2) {
        SharedPreferences.Editor edit = context.getSharedPreferences("ad", 0).edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public static void a(String str) {
        if (a != null) {
            a.OnConnectFailed(str.toString());
        }
    }

    public static Bitmap b(String str) {
        byte[] decodeBase64 = Base64.decodeBase64(str.getBytes());
        return BitmapFactory.decodeByteArray(decodeBase64, 0, decodeBase64.length);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0039 A[SYNTHETIC, Splitter:B:24:0x0039] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x004d A[SYNTHETIC, Splitter:B:34:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x005e A[SYNTHETIC, Splitter:B:42:0x005e] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:11:0x0020=Splitter:B:11:0x0020, B:21:0x0034=Splitter:B:21:0x0034, B:31:0x0048=Splitter:B:31:0x0048} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String b(android.content.Context r5, java.lang.String r6) {
        /*
            r0 = 0
            java.lang.String r1 = ""
            java.io.FileInputStream r0 = r5.openFileInput(r6)     // Catch:{ FileNotFoundException -> 0x001c, IOException -> 0x0030, Exception -> 0x0044, all -> 0x0058 }
            int r2 = r0.available()     // Catch:{ FileNotFoundException -> 0x007f, IOException -> 0x007a, Exception -> 0x0075, all -> 0x006d }
            byte[] r2 = new byte[r2]     // Catch:{ FileNotFoundException -> 0x007f, IOException -> 0x007a, Exception -> 0x0075, all -> 0x006d }
            r0.read(r2)     // Catch:{ FileNotFoundException -> 0x007f, IOException -> 0x007a, Exception -> 0x0075, all -> 0x006d }
            java.lang.String r3 = new java.lang.String     // Catch:{ FileNotFoundException -> 0x007f, IOException -> 0x007a, Exception -> 0x0075, all -> 0x006d }
            r3.<init>(r2)     // Catch:{ FileNotFoundException -> 0x007f, IOException -> 0x007a, Exception -> 0x0075, all -> 0x006d }
            if (r0 == 0) goto L_0x006b
            r0.close()     // Catch:{ Exception -> 0x0067 }
            r0 = r3
        L_0x001b:
            return r0
        L_0x001c:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
        L_0x0020:
            r0.printStackTrace()     // Catch:{ all -> 0x0072 }
            if (r2 == 0) goto L_0x0084
            r2.close()     // Catch:{ Exception -> 0x002a }
            r0 = r1
            goto L_0x001b
        L_0x002a:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x001b
        L_0x0030:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
        L_0x0034:
            r0.printStackTrace()     // Catch:{ all -> 0x0072 }
            if (r2 == 0) goto L_0x0084
            r2.close()     // Catch:{ Exception -> 0x003e }
            r0 = r1
            goto L_0x001b
        L_0x003e:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x001b
        L_0x0044:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
        L_0x0048:
            r0.printStackTrace()     // Catch:{ all -> 0x0072 }
            if (r2 == 0) goto L_0x0084
            r2.close()     // Catch:{ Exception -> 0x0052 }
            r0 = r1
            goto L_0x001b
        L_0x0052:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x001b
        L_0x0058:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x005c:
            if (r1 == 0) goto L_0x0061
            r1.close()     // Catch:{ Exception -> 0x0062 }
        L_0x0061:
            throw r0
        L_0x0062:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0061
        L_0x0067:
            r0 = move-exception
            r0.printStackTrace()
        L_0x006b:
            r0 = r3
            goto L_0x001b
        L_0x006d:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x005c
        L_0x0072:
            r0 = move-exception
            r1 = r2
            goto L_0x005c
        L_0x0075:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x0048
        L_0x007a:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x0034
        L_0x007f:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x0020
        L_0x0084:
            r0 = r1
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.l.adlib_android.u.b(android.content.Context, java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0021 A[SYNTHETIC, Splitter:B:16:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0026 A[Catch:{ Exception -> 0x002a }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0036 A[SYNTHETIC, Splitter:B:27:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x003b A[Catch:{ Exception -> 0x003f }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x004b A[SYNTHETIC, Splitter:B:38:0x004b] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0050 A[Catch:{ Exception -> 0x0054 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x005d A[SYNTHETIC, Splitter:B:47:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0062 A[Catch:{ Exception -> 0x0066 }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:76:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:78:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:24:0x0031=Splitter:B:24:0x0031, B:35:0x0046=Splitter:B:35:0x0046, B:13:0x001c=Splitter:B:13:0x001c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void b(android.content.Context r4, java.lang.String r5, java.lang.String r6) {
        /*
            r2 = 0
            r0 = 0
            java.io.FileOutputStream r0 = r4.openFileOutput(r5, r0)     // Catch:{ FileNotFoundException -> 0x001a, IOException -> 0x002f, Exception -> 0x0044, all -> 0x0059 }
            java.io.OutputStreamWriter r1 = new java.io.OutputStreamWriter     // Catch:{ FileNotFoundException -> 0x0093, IOException -> 0x0088, Exception -> 0x007d, all -> 0x0070 }
            r1.<init>(r0)     // Catch:{ FileNotFoundException -> 0x0093, IOException -> 0x0088, Exception -> 0x007d, all -> 0x0070 }
            r1.write(r6)     // Catch:{ FileNotFoundException -> 0x0099, IOException -> 0x008e, Exception -> 0x0083, all -> 0x0076 }
            r1.flush()     // Catch:{ FileNotFoundException -> 0x0099, IOException -> 0x008e, Exception -> 0x0083, all -> 0x0076 }
            if (r0 == 0) goto L_0x0016
            r0.close()     // Catch:{ Exception -> 0x006b }
        L_0x0016:
            r1.close()     // Catch:{ Exception -> 0x006b }
        L_0x0019:
            return
        L_0x001a:
            r0 = move-exception
            r1 = r2
        L_0x001c:
            r0.printStackTrace()     // Catch:{ all -> 0x007b }
            if (r2 == 0) goto L_0x0024
            r2.close()     // Catch:{ Exception -> 0x002a }
        L_0x0024:
            if (r1 == 0) goto L_0x0019
            r1.close()     // Catch:{ Exception -> 0x002a }
            goto L_0x0019
        L_0x002a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0019
        L_0x002f:
            r0 = move-exception
            r1 = r2
        L_0x0031:
            r0.printStackTrace()     // Catch:{ all -> 0x007b }
            if (r2 == 0) goto L_0x0039
            r2.close()     // Catch:{ Exception -> 0x003f }
        L_0x0039:
            if (r1 == 0) goto L_0x0019
            r1.close()     // Catch:{ Exception -> 0x003f }
            goto L_0x0019
        L_0x003f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0019
        L_0x0044:
            r0 = move-exception
            r1 = r2
        L_0x0046:
            r0.printStackTrace()     // Catch:{ all -> 0x007b }
            if (r2 == 0) goto L_0x004e
            r2.close()     // Catch:{ Exception -> 0x0054 }
        L_0x004e:
            if (r1 == 0) goto L_0x0019
            r1.close()     // Catch:{ Exception -> 0x0054 }
            goto L_0x0019
        L_0x0054:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0019
        L_0x0059:
            r0 = move-exception
            r1 = r2
        L_0x005b:
            if (r2 == 0) goto L_0x0060
            r2.close()     // Catch:{ Exception -> 0x0066 }
        L_0x0060:
            if (r1 == 0) goto L_0x0065
            r1.close()     // Catch:{ Exception -> 0x0066 }
        L_0x0065:
            throw r0
        L_0x0066:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0065
        L_0x006b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0019
        L_0x0070:
            r1 = move-exception
            r3 = r1
            r1 = r2
            r2 = r0
            r0 = r3
            goto L_0x005b
        L_0x0076:
            r2 = move-exception
            r3 = r2
            r2 = r0
            r0 = r3
            goto L_0x005b
        L_0x007b:
            r0 = move-exception
            goto L_0x005b
        L_0x007d:
            r1 = move-exception
            r3 = r1
            r1 = r2
            r2 = r0
            r0 = r3
            goto L_0x0046
        L_0x0083:
            r2 = move-exception
            r3 = r2
            r2 = r0
            r0 = r3
            goto L_0x0046
        L_0x0088:
            r1 = move-exception
            r3 = r1
            r1 = r2
            r2 = r0
            r0 = r3
            goto L_0x0031
        L_0x008e:
            r2 = move-exception
            r3 = r2
            r2 = r0
            r0 = r3
            goto L_0x0031
        L_0x0093:
            r1 = move-exception
            r3 = r1
            r1 = r2
            r2 = r0
            r0 = r3
            goto L_0x001c
        L_0x0099:
            r2 = move-exception
            r3 = r2
            r2 = r0
            r0 = r3
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.l.adlib_android.u.b(android.content.Context, java.lang.String, java.lang.String):void");
    }

    public static int c(Context context, String str) {
        return context.getApplicationContext().getPackageManager().checkPermission(str, context.getApplicationContext().getPackageName());
    }

    public static void c(String str) {
        if (k.l) {
            Log.d("LSENSE_SDK1", str);
        }
    }

    public static String d(String str) {
        return str.substring(str.lastIndexOf("/") + 1).toLowerCase().replace(".xml", "");
    }
}
