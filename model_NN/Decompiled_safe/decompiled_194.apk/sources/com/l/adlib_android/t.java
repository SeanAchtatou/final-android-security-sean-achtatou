package com.l.adlib_android;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.text.method.SingleLineTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public final class t extends RelativeLayout implements View.OnClickListener {
    private final int a = 8192;
    private final float b = 15.0f;
    private TextView c;
    private TextView d;
    private int e;
    private int f;
    private Context g;
    private int h;
    private AdListener i;

    public t(Context context, String str, String str2) {
        super(context);
        setOnClickListener(this);
        this.g = context;
        setPadding(0, 0, 0, 0);
        this.h = 8192;
        if (!str.trim().equals("")) {
            this.c = new TextView(this.g);
            int i2 = this.h + 1;
            this.h = i2;
            this.e = i2;
            this.c.setId(this.e);
            this.c.setTextColor(k.f);
            this.c.setTextSize(2, 15.0f);
            this.c.setTypeface(Typeface.create(Typeface.DEFAULT, 1));
            this.c.setTransformationMethod(SingleLineTransformationMethod.getInstance());
            this.c.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            this.c.setHorizontallyScrolling(true);
            this.c.setFocusable(true);
            this.c.setFocusableInTouchMode(true);
            this.c.setMarqueeRepeatLimit(-1);
            this.c.requestFocus();
            RelativeLayout.LayoutParams a2 = u.a(-2, -2);
            if (str2.trim().equals("")) {
                this.c.setSingleLine(false);
                a2.addRule(15);
            } else {
                this.c.setSingleLine(true);
            }
            this.c.setText(str);
            addView(this.c, a2);
        }
        if (!str2.trim().equals("")) {
            this.d = new TextView(this.g);
            int i3 = this.h + 1;
            this.h = i3;
            this.f = i3;
            this.d.setId(this.f);
            this.d.setTextSize(2, 11.0f);
            this.d.setTextColor(k.f);
            this.d.setEllipsize(TextUtils.TruncateAt.END);
            RelativeLayout.LayoutParams a3 = u.a(-1, -2);
            if (str.trim().equals("")) {
                a3.addRule(15);
            } else {
                a3.addRule(3, this.e);
            }
            this.d.setText(str2);
            addView(this.d, a3);
        }
    }

    public final void a(AdListener adListener) {
        this.i = adListener;
    }

    public final void onClick(View view) {
        if (this.i != null) {
            this.i.OnClickAd(view.getId());
        }
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            setBackgroundDrawable(u.a(new int[]{Color.rgb(155, 225, 35), Color.rgb(75, 120, 15)}));
        } else if (motionEvent.getAction() == 1) {
            setBackgroundColor(0);
        }
        return super.onTouchEvent(motionEvent);
    }
}
