package com.l.adlib_android;

import android.content.Context;
import android.content.pm.PackageManager;

public final class o {
    public static int a(Context context, String str) {
        return ((Integer) b(context, str)).intValue();
    }

    private static Object b(Context context, String str) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get(str);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }
}
