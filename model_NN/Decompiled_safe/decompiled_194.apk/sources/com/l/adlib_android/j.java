package com.l.adlib_android;

import java.util.ArrayList;
import java.util.List;

public final class j {
    public static int a = 0;
    private static List b = new ArrayList();

    public static int a() {
        return b.size();
    }

    public static synchronized void a(e eVar) {
        synchronized (j.class) {
            b.add(eVar);
        }
    }

    public static synchronized e b() {
        e eVar;
        boolean z;
        synchronized (j.class) {
            if (a() <= 0) {
                eVar = null;
            } else {
                if (a >= b.size()) {
                    a = 0;
                }
                eVar = (e) b.get(a);
                eVar.c().d(eVar.c().e() - 1);
                int i = a;
                if (eVar.c().e() <= 0) {
                    b.remove(a);
                    eVar.a(true);
                    z = true;
                } else {
                    a++;
                    eVar.a(false);
                    z = false;
                }
                u.c("position:" + String.valueOf(i) + " . size:" + String.valueOf(b.size()) + " . turns:" + String.valueOf(eVar.c().e()) + " . showtime:" + String.valueOf(eVar.c().d()) + " . adid:" + String.valueOf(eVar.c().c()) + " . lastposition:" + String.valueOf(eVar.c().b()) + " . del:" + Boolean.toString(z));
            }
        }
        return eVar;
    }

    public static synchronized void c() {
        synchronized (j.class) {
            b.remove(0);
            if (a > 0) {
                a--;
            } else {
                a = 0;
            }
        }
    }
}
