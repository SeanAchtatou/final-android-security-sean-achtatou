package com.l.adlib_android;

import android.util.Xml;
import java.util.List;
import org.xml.sax.SAXException;

public final class v {
    public static i a(String str) {
        x xVar = new x();
        try {
            Xml.parse(str, xVar);
            return xVar.a();
        } catch (SAXException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static List b(String str) {
        w wVar = new w();
        try {
            Xml.parse(str, wVar);
            return wVar.a();
        } catch (SAXException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
