package com.l.adlib_android;

import android.os.Handler;
import android.os.Message;
import com.energysource.szj.embeded.AdManager;

final class a extends Handler {
    final /* synthetic */ AdView a;

    a(AdView adView) {
        this.a = adView;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 200:
                AdView.i = j.b();
                if (AdView.i != null) {
                    AdView.i.a(System.currentTimeMillis());
                    this.a.b(AdView.i);
                    this.a.o.postDelayed(this.a, (long) (AdView.i.c().d() * AdManager.AD_FILL_PARENT));
                    u.c("getShowTime:" + String.valueOf(AdView.i.c().d()));
                    return;
                }
                u.c("getAdQueue: Null");
                this.a.o.postDelayed(this.a, 20000);
                return;
            case 300:
                this.a.o.postDelayed(this.a, 20000);
                return;
            default:
                return;
        }
    }
}
