package com.l.adlib_android;

import java.io.File;
import java.io.FileFilter;

final class h implements FileFilter {
    h() {
    }

    public final boolean accept(File file) {
        return file.getName().substring(file.getName().lastIndexOf(".") + 1, file.getName().length()).toLowerCase().equalsIgnoreCase("ad");
    }
}
