package com.l.adlib_android;

import java.text.SimpleDateFormat;

public final class i {
    static SimpleDateFormat a = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private String g;
    private String h;
    private String i;

    public final String a() {
        return this.i;
    }

    public final void a(int i2) {
        this.f = i2;
    }

    public final void a(String str) {
        this.i = str;
    }

    public final int b() {
        return this.f;
    }

    public final void b(int i2) {
        this.b = i2;
    }

    public final void b(String str) {
        this.g = str;
    }

    public final int c() {
        return this.b;
    }

    public final void c(int i2) {
        this.c = i2;
    }

    public final void c(String str) {
        this.h = str;
    }

    public final int d() {
        return this.c;
    }

    public final void d(int i2) {
        this.d = i2;
    }

    public final int e() {
        return this.d;
    }

    public final void e(int i2) {
        this.e = i2;
    }

    public final String f() {
        return this.g;
    }

    public final String g() {
        return this.h;
    }

    public final String toString() {
        return "AdId:" + String.valueOf(this.b) + "\n" + "ShowTime:" + String.valueOf(this.c) + "\n" + "Turns:" + String.valueOf(this.d) + "\n" + "AdType:" + String.valueOf(this.e) + "\n" + "LastPosition:" + String.valueOf(this.f) + "\n" + "Uri:" + this.g;
    }
}
