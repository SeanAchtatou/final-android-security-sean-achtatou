package com.l.adlib_android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;
import cn.domob.android.ads.DomobAdManager;
import com.energysource.szj.embeded.AdManager;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.List;

public class AdView extends ViewFlipper implements AdListener, Runnable {
    /* access modifiers changed from: private */
    public static e i = null;
    private static r j = null;
    private final String a;
    private final int b;
    private final int c;
    private List d;
    private View e;
    private Context f;
    private boolean g;
    private locationmanager h;
    private Animation k;
    private Animation l;
    private AlphaAnimation m;
    private AlphaAnimation n;
    /* access modifiers changed from: private */
    public Handler o;

    public AdView(Context context) {
        this(context, null);
    }

    public AdView(Context context, int i2, int i3, int i4, int i5, int i6, boolean z) {
        super(context);
        this.a = "http://schemas.android.com/apk/res/";
        this.b = 700;
        this.c = 20;
        this.d = new ArrayList();
        this.g = true;
        this.o = new a(this);
        this.f = context;
        k.b = i2;
        k.d = i3;
        k.e = i4;
        k.f = i5;
        k.g = i6;
        k.i = z;
        b();
    }

    public AdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = "http://schemas.android.com/apk/res/";
        this.b = 700;
        this.c = 20;
        this.d = new ArrayList();
        this.g = true;
        this.o = new a(this);
        this.f = context;
        k.b = o.a(context, "appkey");
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            k.g = attributeSet.getAttributeIntValue(str, "backgroundTransparent", k.g);
            k.i = attributeSet.getAttributeBooleanValue(str, "autoFit", k.i);
            String attributeValue = attributeSet.getAttributeValue(str, "textColor");
            if (attributeValue != null) {
                k.f = Color.parseColor(attributeValue);
            }
            String attributeValue2 = attributeSet.getAttributeValue(str, "startBackgroundColor");
            if (attributeValue2 != null) {
                k.d = Color.parseColor(attributeValue2);
            }
            String attributeValue3 = attributeSet.getAttributeValue(str, "endBackgroundColor");
            if (attributeValue3 != null) {
                k.e = Color.parseColor(attributeValue3);
            }
        }
        b();
    }

    private void b() {
        u.c("init");
        AnimationSet animationSet = new AnimationSet(true);
        this.k = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        this.k.setDuration(300);
        this.m = new AlphaAnimation(0.0f, 1.0f);
        this.m.setDuration(300);
        animationSet.addAnimation(this.k);
        animationSet.addAnimation(this.m);
        setInAnimation(animationSet);
        AnimationSet animationSet2 = new AnimationSet(true);
        this.l = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
        this.l.setDuration(300);
        this.n = new AlphaAnimation(1.0f, 0.0f);
        this.n.setDuration(300);
        animationSet2.addAnimation(this.l);
        animationSet2.addAnimation(this.n);
        setOutAnimation(animationSet2);
        setBackgroundColor(0);
        setLayoutParams(u.a(-1, -2));
        k.j = "/data/data/" + this.f.getApplicationContext().getPackageName() + "/files";
        l.a(this.f);
        f.a();
        this.e = this;
        u.c("initEx");
        this.o.removeCallbacks(this);
        this.g = true;
        this.h = new locationmanager(this.f);
        if (i != null) {
            b(i);
            u.c("get ad from buffer..." + String.valueOf(i.c().c()));
        } else {
            this.o.post(this);
        }
        r.b = this.f;
        r.d = this.o;
        r.c = this.h;
    }

    /* access modifiers changed from: private */
    public void b(e eVar) {
        u.c("showFreshAdID:" + String.valueOf(eVar.c().c()));
        ad adVar = new ad(this.f, eVar.c(), eVar.d(), k.i, eVar.a());
        adVar.a(this);
        adVar.setVisibility(0);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) adVar.getLayoutParams();
        layoutParams.addRule(14);
        int size = this.d.size();
        if (size >= 2) {
            removeView((View) this.d.get(0));
            this.d.remove(0);
        }
        this.d.add(adVar);
        addView(adVar, layoutParams);
        if (size > 0) {
            showNext();
        }
    }

    private void c() {
        this.o.removeCallbacks(this);
        r.a = false;
        this.g = false;
        this.h.a();
    }

    public void OnClickAd(int i2) {
        Intent intent;
        if (this.d.size() > 0) {
            int size = this.d.size() - 1;
            d dVar = (d) ((ad) this.d.get(size)).d().get(i2 - 256);
            String trim = dVar.g().toString().trim();
            String trim2 = dVar.h().toString().trim();
            String trim3 = dVar.f().toString().trim();
            if (!trim3.trim().equals("")) {
                String replace = trim3.replace("key=", "key=" + u.a(((ad) this.d.get(size)).c().c(), k.b, l.a));
                u.c("newFeeUri:" + replace);
                new m(replace).start();
            }
            if (trim.equalsIgnoreCase("tel")) {
                intent = u.c(this.f, "android.permission.CALL_PHONE") == 0 ? new Intent("android.intent.action.CALL", Uri.parse("tel:" + trim2)) : new Intent("android.intent.action.VIEW", Uri.parse("tel:" + trim2));
            } else if (trim.equalsIgnoreCase(DomobAdManager.ACTION_SMS)) {
                if (trim2.trim().equalsIgnoreCase("")) {
                    Intent intent2 = new Intent("android.intent.action.VIEW");
                    intent2.setType("vnd.android-dir/mms-sms");
                    intent = intent2;
                } else {
                    intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + trim2));
                }
            } else if (trim.equalsIgnoreCase(DomobAdManager.ACTION_VIDEO)) {
                Intent intent3 = new Intent("android.intent.action.VIEW");
                intent3.setDataAndType(Uri.parse("http://" + trim2), "video/*");
                intent = intent3;
            } else if (trim.equalsIgnoreCase(DomobAdManager.ACTION_AUDIO)) {
                Intent intent4 = new Intent("android.intent.action.VIEW");
                intent4.setDataAndType(Uri.parse("http://" + trim2), "audio/mp3");
                intent = intent4;
            } else {
                intent = !trim2.trim().equalsIgnoreCase("") ? new Intent("android.intent.action.VIEW", Uri.parse("http://" + trim2)) : null;
            }
            if (intent != null) {
                u.c("DeleteState:" + String.valueOf(((ad) this.d.get(size)).a()));
                if (!((ad) this.d.get(size)).a()) {
                    ((ad) this.d.get(size)).b();
                    i.a(true);
                    j.c();
                }
                this.f.startActivity(intent);
            }
        }
    }

    public String getVersion() {
        return k.a;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        String str;
        try {
            super.onDetachedFromWindow();
        } catch (IllegalArgumentException e2) {
            stopFlipping();
        } finally {
            str = "onDetachedFromWindow";
            u.c(str);
            c();
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        long j2;
        super.onWindowVisibilityChanged(i2);
        switch (i2) {
            case 0:
                this.o.removeCallbacks(this);
                this.g = true;
                this.h = new locationmanager(this.f);
                if (i != null) {
                    if (i != null) {
                        long currentTimeMillis = (System.currentTimeMillis() - i.b()) - ((long) (i.c().d() * AdManager.AD_FILL_PARENT));
                        j2 = currentTimeMillis < 0 ? Math.abs(currentTimeMillis) : 2000;
                    } else {
                        j2 = 0;
                    }
                    this.o.postDelayed(this, j2);
                    u.c("diff:" + String.valueOf(j2));
                } else {
                    this.o.post(this);
                }
                u.c("WindowVisibility(VISIBLE):" + String.valueOf(i2));
                return;
            case 4:
                u.c("WindowVisibility(INVISIBLE):" + String.valueOf(i2));
                return;
            case 8:
                this.h.a();
                u.c("WindowVisibility(GONE):" + String.valueOf(i2));
                this.o.removeCallbacks(this);
                r.a = false;
                this.g = false;
                return;
            default:
                return;
        }
    }

    public void run() {
        u.c("run...");
        if (!this.g) {
            return;
        }
        if (j == null) {
            r rVar = new r();
            j = rVar;
            rVar.start();
            u.c("run start requestfreshad one...");
            return;
        }
        u.c("thread state:" + j.getState().toString());
        if (j.getState() == Thread.State.TERMINATED) {
            r rVar2 = new r();
            j = rVar2;
            rVar2.start();
            u.c("run start requestfreshad...");
        }
    }

    public void setOnAdListenerEx(AdListenerEx adListenerEx) {
        u.a = adListenerEx;
    }
}
