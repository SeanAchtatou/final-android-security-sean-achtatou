package com.l.adlib_android;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

public final class l {
    public static long a;
    public static String b;
    public static String c;
    public static byte d;
    public static byte e;

    public static void a(Context context) {
        String replaceAll;
        b = Build.MODEL;
        c = Build.VERSION.RELEASE;
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (deviceId == null) {
            replaceAll = Settings.System.getString(context.getContentResolver(), "android_id");
            a = Long.parseLong(replaceAll.length() > 15 ? replaceAll.substring(0, 15) : replaceAll, 16);
        } else {
            replaceAll = deviceId.replaceAll("[^0-9]+", "");
            a = Long.parseLong(replaceAll);
        }
        Log.i("adsdk", "deviceId" + replaceAll);
        Log.i("adsdk", "uid" + Long.toString(a));
        d = -1;
        e = -1;
    }
}
