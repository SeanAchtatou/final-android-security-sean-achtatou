package com.l.adlib_android;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import cn.domob.android.ads.DomobAdManager;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.List;

public class AdViewFull extends RelativeLayout implements View.OnClickListener, AdapterView.OnItemClickListener {
    public static List a = new ArrayList();
    private static s k = null;
    private final String b;
    /* access modifiers changed from: private */
    public List c;
    private List d;
    private List e;
    /* access modifiers changed from: private */
    public Context f;
    private locationmanager g;
    /* access modifiers changed from: private */
    public Gallery h;
    /* access modifiers changed from: private */
    public ImageButton i;
    /* access modifiers changed from: private */
    public ProgressDialog j;
    private AdListener l;
    private Handler m;

    public AdViewFull(Context context) {
        this(context, null);
    }

    public AdViewFull(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = "IMG";
        this.c = new ArrayList();
        this.d = new ArrayList();
        this.e = new ArrayList();
        this.j = null;
        this.m = new b(this);
        this.f = context;
        this.h = new Gallery(context);
        this.h.setOnItemClickListener(this);
        addView(this.h, new Gallery.LayoutParams(-1, -1));
        this.i = new ImageButton(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(11);
        this.i.setBackgroundColor(0);
        this.i.setImageBitmap(u.b("iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAKQWlDQ1BJQ0MgUHJvZmlsZQAAeAGdlndUU9kWh8+9N73QEiIgJfQaegkg0jtIFQRRiUmAUAKGhCZ2RAVGFBEpVmRUwAFHhyJjRRQLg4Ji1wnyEFDGwVFEReXdjGsJ7601896a/cdZ39nnt9fZZ+9917oAUPyCBMJ0WAGANKFYFO7rwVwSE8vE9wIYEAEOWAHA4WZmBEf4RALU/L09mZmoSMaz9u4ugGS72yy/UCZz1v9/kSI3QyQGAApF1TY8fiYX5QKUU7PFGTL/BMr0lSkyhjEyFqEJoqwi48SvbPan5iu7yZiXJuShGlnOGbw0noy7UN6aJeGjjAShXJgl4GejfAdlvVRJmgDl9yjT0/icTAAwFJlfzOcmoWyJMkUUGe6J8gIACJTEObxyDov5OWieAHimZ+SKBIlJYqYR15hp5ejIZvrxs1P5YjErlMNN4Yh4TM/0tAyOMBeAr2+WRQElWW2ZaJHtrRzt7VnW5mj5v9nfHn5T/T3IevtV8Sbsz55BjJ5Z32zsrC+9FgD2JFqbHbO+lVUAtG0GQOXhrE/vIADyBQC03pzzHoZsXpLE4gwnC4vs7GxzAZ9rLivoN/ufgm/Kv4Y595nL7vtWO6YXP4EjSRUzZUXlpqemS0TMzAwOl89k/fcQ/+PAOWnNycMsnJ/AF/GF6FVR6JQJhIlou4U8gViQLmQKhH/V4X8YNicHGX6daxRodV8AfYU5ULhJB8hvPQBDIwMkbj96An3rWxAxCsi+vGitka9zjzJ6/uf6Hwtcim7hTEEiU+b2DI9kciWiLBmj34RswQISkAd0oAo0gS4wAixgDRyAM3AD3iAAhIBIEAOWAy5IAmlABLJBPtgACkEx2AF2g2pwANSBetAEToI2cAZcBFfADXALDIBHQAqGwUswAd6BaQiC8BAVokGqkBakD5lC1hAbWgh5Q0FQOBQDxUOJkBCSQPnQJqgYKoOqoUNQPfQjdBq6CF2D+qAH0CA0Bv0BfYQRmALTYQ3YALaA2bA7HAhHwsvgRHgVnAcXwNvhSrgWPg63whfhG/AALIVfwpMIQMgIA9FGWAgb8URCkFgkAREha5EipAKpRZqQDqQbuY1IkXHkAwaHoWGYGBbGGeOHWYzhYlZh1mJKMNWYY5hWTBfmNmYQM4H5gqVi1bGmWCesP3YJNhGbjS3EVmCPYFuwl7ED2GHsOxwOx8AZ4hxwfrgYXDJuNa4Etw/XjLuA68MN4SbxeLwq3hTvgg/Bc/BifCG+Cn8cfx7fjx/GvyeQCVoEa4IPIZYgJGwkVBAaCOcI/YQRwjRRgahPdCKGEHnEXGIpsY7YQbxJHCZOkxRJhiQXUiQpmbSBVElqIl0mPSa9IZPJOmRHchhZQF5PriSfIF8lD5I/UJQoJhRPShxFQtlOOUq5QHlAeUOlUg2obtRYqpi6nVpPvUR9Sn0vR5Mzl/OX48mtk6uRa5Xrl3slT5TXl3eXXy6fJ18hf0r+pvy4AlHBQMFTgaOwVqFG4bTCPYVJRZqilWKIYppiiWKD4jXFUSW8koGStxJPqUDpsNIlpSEaQtOledK4tE20Otpl2jAdRzek+9OT6cX0H+i99AllJWVb5SjlHOUa5bPKUgbCMGD4M1IZpYyTjLuMj/M05rnP48/bNq9pXv+8KZX5Km4qfJUilWaVAZWPqkxVb9UU1Z2qbapP1DBqJmphatlq+9Uuq43Pp893ns+dXzT/5PyH6rC6iXq4+mr1w+o96pMamhq+GhkaVRqXNMY1GZpumsma5ZrnNMe0aFoLtQRa5VrntV4wlZnuzFRmJbOLOaGtru2nLdE+pN2rPa1jqLNYZ6NOs84TXZIuWzdBt1y3U3dCT0svWC9fr1HvoT5Rn62fpL9Hv1t/ysDQINpgi0GbwaihiqG/YZ5ho+FjI6qRq9Eqo1qjO8Y4Y7ZxivE+41smsImdSZJJjclNU9jU3lRgus+0zwxr5mgmNKs1u8eisNxZWaxG1qA5wzzIfKN5m/krCz2LWIudFt0WXyztLFMt6ywfWSlZBVhttOqw+sPaxJprXWN9x4Zq42Ozzqbd5rWtqS3fdr/tfTuaXbDdFrtOu8/2DvYi+yb7MQc9h3iHvQ732HR2KLuEfdUR6+jhuM7xjOMHJ3snsdNJp9+dWc4pzg3OowsMF/AX1C0YctFx4bgccpEuZC6MX3hwodRV25XjWuv6zE3Xjed2xG3E3dg92f24+ysPSw+RR4vHlKeT5xrPC16Il69XkVevt5L3Yu9q76c+Oj6JPo0+E752vqt9L/hh/QL9dvrd89fw5/rX+08EOASsCegKpARGBFYHPgsyCRIFdQTDwQHBu4IfL9JfJFzUFgJC/EN2hTwJNQxdFfpzGC4sNKwm7Hm4VXh+eHcELWJFREPEu0iPyNLIR4uNFksWd0bJR8VF1UdNRXtFl0VLl1gsWbPkRoxajCCmPRYfGxV7JHZyqffS3UuH4+ziCuPuLjNclrPs2nK15anLz66QX8FZcSoeGx8d3xD/iRPCqeVMrvRfuXflBNeTu4f7kufGK+eN8V34ZfyRBJeEsoTRRJfEXYljSa5JFUnjAk9BteB1sl/ygeSplJCUoykzqdGpzWmEtPi000IlYYqwK10zPSe9L8M0ozBDuspp1e5VE6JA0ZFMKHNZZruYjv5M9UiMJJslg1kLs2qy3mdHZZ/KUcwR5vTkmuRuyx3J88n7fjVmNXd1Z752/ob8wTXuaw6thdauXNu5Tnddwbrh9b7rj20gbUjZ8MtGy41lG99uit7UUaBRsL5gaLPv5sZCuUJR4b0tzlsObMVsFWzt3WazrWrblyJe0fViy+KK4k8l3JLr31l9V/ndzPaE7b2l9qX7d+B2CHfc3em681iZYlle2dCu4F2t5czyovK3u1fsvlZhW3FgD2mPZI+0MqiyvUqvakfVp+qk6oEaj5rmvep7t+2d2sfb17/fbX/TAY0DxQc+HhQcvH/I91BrrUFtxWHc4azDz+ui6rq/Z39ff0TtSPGRz0eFR6XHwo911TvU1zeoN5Q2wo2SxrHjccdv/eD1Q3sTq+lQM6O5+AQ4ITnx4sf4H++eDDzZeYp9qukn/Z/2ttBailqh1tzWibakNml7THvf6YDTnR3OHS0/m/989Iz2mZqzymdLz5HOFZybOZ93fvJCxoXxi4kXhzpXdD66tOTSna6wrt7LgZevXvG5cqnbvfv8VZerZ645XTt9nX297Yb9jdYeu56WX+x+aem172296XCz/ZbjrY6+BX3n+l37L972un3ljv+dGwOLBvruLr57/17cPel93v3RB6kPXj/Mejj9aP1j7OOiJwpPKp6qP6391fjXZqm99Oyg12DPs4hnj4a4Qy//lfmvT8MFz6nPK0a0RupHrUfPjPmM3Xqx9MXwy4yX0+OFvyn+tveV0auffnf7vWdiycTwa9HrmT9K3qi+OfrW9m3nZOjk03dp76anit6rvj/2gf2h+2P0x5Hp7E/4T5WfjT93fAn88ngmbWbm3/eE8/ul8iYiAAAFG0lEQVRYCcVXWWxUVRj+7vR2o9NCp6CQiGkKahDZWtKqLCFGa4xGfJBY3IlGZZEGQzVojYGIBBNcnmp88dmo8VESX1wwVUMoWhO0gbYIYhemLbO00M7c6/ffw71zt5lp4wN/cvLPOedfv/P/59zRTNPEjaTIjXQuvvXZBDBSr91TGsFOaLhzNvKU+SsDdC06Z54oJq8VO4LRZdrGUuDH6L4PEalvKGbP2jfODyL1QTtmgE3Fgih4BDnnH0FfvRaRykpEYCJSVpafRyLQG5sQ3XUUErjYKBR1XgQc568cgb6uEZgYB65dK2Qrt1deDiyoRabnFFKfHCiIRCgCjvP2Y9DXrAUmJ9Vg5shmgWJcOiseh97cgujL7xVEIoCA4/yldy3YMT4GXL2ay24uvyoqgNoYMr+fRurTzlAkPAg4zne/D33lKiCV5EipjDOsa8l8LtwwgNER1sR6RF88FIqEg0C8QWsu0fBLdMc70FetAcYuA1NTwXx5tlY9BHescw/dY/EithCZ3t+Q+uwgsiZa6vrNX8WEg4CmYWe0rQP6bXcAyQSQ4ChjMc2wmWze1Ay0PQWsWOldl/2G5WqP3eLI23pZIjE8ZB1p1dY9EF92/M5FpAFNJQLZ0CUFs0hcmVBywtfTecvdar55C5BO87o5o+aLlwAPPMh0mM/GzapQTzJBt75ITk1CZ4eIL6XoQsBakPaRipeznp7O8bqFwL2udi4pAR56GLhrNbD8dvVb1mzasAmonJfTt+1JgIKoi5wjsNYmmKkIjLPn3bzvT6DzDRWUrSwO728FHnkUcDuXwN96Hbj4d9CO2JUidpE3AGmbGRrwcym8BIM7ejgYhNu56IpzHrIVlN+OzDOUcZE3gPG4chBnB0gmbl6zADjfD7zZ4Q3CNibyB7gnNuQi8uvb84JHIOcmWUjbhPFbblVnLmfpJ1mTDqiZn1/fsuutAacLLHvS+2Lg8ogy7+fyGj67Q1W7PwCdpp58BpBOOvlzuL7Yy3gD8KZiIUCBeVFVhG7eyDb0O5eCckMqQTz9HLBhS1Bf5Cx7hWogPsqz44s3OuTlks/zL3gzF4P79wId7cEgntgOzGfh+u3I3B0wzXoRqLqeebRaCdpcOuDwQQlDkWS+/1VeyXyoBvqAQ297DX98DBg8C9j6bp7vCFi4/5j/XqAhQjQikbq4VPaJb3OO9u0CzvIW5M2GChbuqW7gtd0qCAnm+NeqFf12ODcnU9IkdKDIQYCl05Xs74YRHwaqWYgSgJtX1QC9PUBriyrScnaKvb+E3dH3B7D9MeD7b4Cly4L6tGdwJM91wzTQZQfgvIayMNagPc4X8YvqpesQkb4PI4FQ58dWGBXYM3iMyQs94LvUFhswP7fVHQRkIdZvfsmnclvyYg+McWnJmGobNxdB91yc2vPYzUF57huGGepcTHkQkAUhB4nFKxAR6P8HGekEkkNn5BtgmyToN+VBwN50kKCikWAB1tRdz2xu3GCTFXIu/kIRsANxkIjV85O82l6eFTemkkiODebN3DYSioCmqLRuAN/1TmKPGDLTvAtqb+LHBu+AItwsrbCcn05jL238RHOVHHy/gxSKgChQtIGDNxPKvlqE1vuq0RlUz7/yQxpHtg7jOCVYpeCFgUv8F8ar1kv5AuCDDn4WQQLhZxJKeTOUXeFDy9+CmmQj3EZQ1nmVICucshpleZFYzoVPcUwzANn3UGgAHgnfRE6HSzJs57aEBGENOhI+K/oPwmHByP7VKUEAAAAASUVORK5CYII="));
        this.i.setOnClickListener(this);
        addView(this.i, layoutParams);
        if (this.c.size() > 0) {
            this.i.setVisibility(0);
        } else {
            this.i.setVisibility(4);
        }
        Init();
        this.h.setAdapter((SpinnerAdapter) new c(this, context));
    }

    static /* synthetic */ void a(AdViewFull adViewFull) {
        for (int i2 = 0; i2 < adViewFull.c.size(); i2++) {
            if (!((Bitmap) adViewFull.c.get(i2)).isRecycled()) {
                ((Bitmap) adViewFull.c.get(i2)).recycle();
            }
        }
        adViewFull.c.clear();
        adViewFull.d.clear();
        adViewFull.e.clear();
        for (int i3 = 0; i3 < a.size(); i3++) {
            d dVar = (d) a.get(i3);
            if (dVar.a().equalsIgnoreCase("IMG")) {
                adViewFull.c.add(u.b(dVar.b()));
                adViewFull.d.add(dVar.g());
                adViewFull.e.add(dVar.h());
            }
        }
    }

    public void GetFullAd() {
        boolean z = false;
        if (k == null) {
            z = true;
        } else if (k.getState() == Thread.State.TERMINATED) {
            z = true;
        }
        if (z) {
            this.j = ProgressDialog.show(this.f, "", "正在加载...");
            s sVar = new s();
            k = sVar;
            sVar.start();
        }
    }

    public void Init() {
        k.b = o.a(this.f, "appkey");
        l.a(this.f);
        this.g = new locationmanager(this.f);
        s.a = this.f;
        s.c = this.m;
        s.b = this.g;
    }

    public String getVersion() {
        return k.a;
    }

    public void onClick(View view) {
        if (this.l != null) {
            this.l.OnClickAd(view.getId());
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        Intent intent;
        String str = (String) this.d.get(i2);
        String str2 = (String) this.e.get(i2);
        if (str.equalsIgnoreCase("tel")) {
            intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + str2));
        } else if (str.equalsIgnoreCase(DomobAdManager.ACTION_SMS)) {
            if (str2.trim().equalsIgnoreCase("")) {
                intent = new Intent("android.intent.action.VIEW");
                intent.setType("vnd.android-dir/mms-sms");
            } else {
                intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + str2));
            }
        } else if (str.equalsIgnoreCase(DomobAdManager.ACTION_VIDEO)) {
            intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse("http://" + str2), "video/*");
        } else if (str.equalsIgnoreCase(DomobAdManager.ACTION_AUDIO)) {
            intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse("http://" + str2), "audio/mp3");
        } else {
            intent = !str2.trim().equalsIgnoreCase("") ? new Intent("android.intent.action.VIEW", Uri.parse("http://" + str2)) : null;
        }
        if (intent != null) {
            this.f.startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        switch (i2) {
            case 0:
                this.g = new locationmanager(this.f);
                return;
            case 4:
                u.c("WindowVisibility(INVISIBLE):" + String.valueOf(i2));
                return;
            case 8:
                this.g.a();
                return;
            default:
                return;
        }
    }

    public void setOnClickAdListener(AdListener adListener) {
        this.l = adListener;
    }
}
