package com.l.adlib_android;

import android.util.Log;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public final class p {
    public static InputStream a(String str) {
        u.c("requestBody..." + str);
        try {
            int lastIndexOf = str.lastIndexOf("/");
            URLConnection openConnection = new URL(String.valueOf(str.substring(0, lastIndexOf)) + "/" + URLEncoder.encode(str.substring(lastIndexOf + 1))).openConnection();
            openConnection.setConnectTimeout(30000);
            openConnection.setReadTimeout(60000);
            HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200) {
                return httpURLConnection.getInputStream();
            }
            u.a(String.valueOf(responseCode));
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            u.a(e.getMessage().toString());
            return null;
        }
    }

    public static InputStream a(String str, String str2) {
        InputStream inputStream;
        Exception exc;
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 30000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 60000);
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
            HttpPost httpPost = new HttpPost(str);
            if (str2 != null) {
                httpPost.setEntity(new StringEntity(str2, "UTF-8"));
                httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
            }
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() == 200) {
                InputStream content = execute.getEntity().getContent();
                try {
                    defaultHttpClient.getConnectionManager().shutdown();
                    Log.i("feeuri", "ok");
                    return content;
                } catch (Exception e) {
                    Exception exc2 = e;
                    inputStream = content;
                    exc = exc2;
                }
            } else {
                u.a(String.valueOf(execute.getStatusLine().getStatusCode()));
                return null;
            }
        } catch (Exception e2) {
            Exception exc3 = e2;
            inputStream = null;
            exc = exc3;
        }
        exc.printStackTrace();
        u.a(exc.getMessage().toString());
        return inputStream;
    }

    public static InputStream a(String str, Map map) {
        InputStream inputStream = null;
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 30000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 60000);
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
            HttpPost httpPost = new HttpPost(str);
            ArrayList arrayList = new ArrayList();
            if (map != null) {
                for (String str2 : map.keySet()) {
                    arrayList.add(new BasicNameValuePair(str2, (String) map.get(str2)));
                    Log.i("adsdk", String.valueOf(str2) + " | " + ((String) map.get(str2)));
                }
                Date date = new Date(System.currentTimeMillis());
                Log.i("adsdk", "rq:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(date));
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            }
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() == 200) {
                InputStream content = execute.getEntity().getContent();
                try {
                    defaultHttpClient.getConnectionManager().shutdown();
                    return content;
                } catch (Exception e) {
                    Exception exc = e;
                    inputStream = content;
                    e = exc;
                }
            } else {
                u.a(String.valueOf(execute.getStatusLine().getStatusCode()));
                return null;
            }
        } catch (Exception e2) {
            e = e2;
        }
        e.printStackTrace();
        u.a(e.getMessage().toString());
        return inputStream;
    }
}
