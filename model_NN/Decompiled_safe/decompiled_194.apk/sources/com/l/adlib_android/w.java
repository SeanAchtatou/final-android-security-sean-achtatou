package com.l.adlib_android;

import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

final class w extends DefaultHandler {
    private List a;
    private d b;
    private StringBuilder c;

    w() {
    }

    public final List a() {
        return this.a;
    }

    public final void characters(char[] cArr, int i, int i2) {
        super.characters(cArr, i, i2);
        this.c.append(cArr, i, i2);
    }

    public final void endElement(String str, String str2, String str3) {
        super.endElement(str, str2, str3);
        if (this.b != null) {
            if (str2.equalsIgnoreCase("type")) {
                this.b.a(this.c.toString());
            } else if (str2.equalsIgnoreCase("content")) {
                this.b.b(this.c.toString());
            } else if (str2.equalsIgnoreCase("title")) {
                this.b.d(this.c.toString());
            } else if (str2.equalsIgnoreCase("message")) {
                this.b.e(this.c.toString());
            } else if (str2.equalsIgnoreCase("footercaption")) {
                this.b.c(this.c.toString());
            } else if (str2.equalsIgnoreCase("feeuri")) {
                this.b.f(this.c.toString());
            } else if (str2.equalsIgnoreCase("action")) {
                this.b.g(this.c.toString());
            } else if (str2.equalsIgnoreCase("actdata")) {
                this.b.h(this.c.toString());
            } else if (str2.equalsIgnoreCase("width")) {
                this.b.a(Integer.parseInt(this.c.toString()));
            } else if (str2.equalsIgnoreCase("aditem")) {
                this.a.add(this.b);
            }
            this.c.setLength(0);
        }
    }

    public final void startDocument() {
        super.startDocument();
        this.a = new ArrayList();
        this.c = new StringBuilder();
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        super.startElement(str, str2, str3, attributes);
        if (str2.equalsIgnoreCase("aditem")) {
            this.b = new d();
        }
    }
}
