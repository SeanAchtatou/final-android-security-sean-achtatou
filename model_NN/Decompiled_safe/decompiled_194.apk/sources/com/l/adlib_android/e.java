package com.l.adlib_android;

import java.util.List;

public final class e {
    private i a;
    private List b;
    private boolean c;
    private long d;

    public e() {
    }

    public e(i iVar, List list) {
        this.a = iVar;
        this.b = list;
    }

    public final void a(long j) {
        this.d = j;
    }

    public final void a(boolean z) {
        this.c = z;
    }

    public final boolean a() {
        return this.c;
    }

    public final long b() {
        return this.d;
    }

    public final i c() {
        return this.a;
    }

    public final List d() {
        return this.b;
    }
}
