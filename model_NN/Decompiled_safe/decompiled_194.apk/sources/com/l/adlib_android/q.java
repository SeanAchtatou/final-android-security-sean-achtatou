package com.l.adlib_android;

public final class q {
    private int a;
    private long b;
    private byte c = 0;
    private String d;
    private String e;
    private byte f = -1;
    private String g = k.a;
    private byte h;
    private byte i;
    private double j;
    private double k;
    private int l = -1;
    private byte m = 1;

    public final void a() {
        this.c = 0;
    }

    public final void a(byte b2) {
        this.f = b2;
    }

    public final void a(double d2) {
        this.j = d2;
    }

    public final void a(int i2) {
        this.a = i2;
    }

    public final void a(long j2) {
        this.b = j2;
    }

    public final void a(String str) {
        this.d = str;
    }

    public final void b() {
        this.m = 2;
    }

    public final void b(byte b2) {
        this.h = b2;
    }

    public final void b(double d2) {
        this.k = d2;
    }

    public final void b(int i2) {
        this.l = i2;
    }

    public final void b(String str) {
        this.e = str;
    }

    public final int c() {
        return this.a;
    }

    public final void c(byte b2) {
        this.i = b2;
    }

    public final long d() {
        return this.b;
    }

    public final byte e() {
        return this.c;
    }

    public final String f() {
        return this.d;
    }

    public final String g() {
        return this.e;
    }

    public final byte h() {
        return this.f;
    }

    public final String i() {
        return this.g;
    }

    public final byte j() {
        return this.h;
    }

    public final byte k() {
        return this.i;
    }

    public final double l() {
        return this.j;
    }

    public final double m() {
        return this.k;
    }

    public final int n() {
        return this.l;
    }

    public final byte o() {
        return this.m;
    }
}
