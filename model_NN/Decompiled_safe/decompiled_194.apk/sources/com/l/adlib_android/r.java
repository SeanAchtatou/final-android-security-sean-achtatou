package com.l.adlib_android;

import android.content.Context;
import android.os.Handler;
import java.io.File;
import java.io.InputStream;
import java.util.List;

public final class r extends Thread {
    public static boolean a = false;
    public static Context b;
    public static locationmanager c;
    public static Handler d;
    private final int e = 100;

    private static void a(int i) {
        d.sendMessage(d.obtainMessage(i));
        u.c("sendMessage..." + String.valueOf(i));
    }

    public final void run() {
        i a2;
        String str;
        a = true;
        try {
            if (j.a() < 3) {
                String str2 = "";
                u.c("requestHead...");
                double[] b2 = c.b();
                q qVar = new q();
                qVar.a(k.b);
                qVar.a(l.a);
                qVar.a();
                qVar.a(l.b);
                qVar.b(l.c);
                qVar.a(l.d);
                qVar.b(l.e);
                qVar.c((byte) ((int) b2[0]));
                qVar.a(b2[1]);
                qVar.b(b2[2]);
                qVar.b(Integer.parseInt(u.a(b, "lastposition")));
                InputStream a3 = p.a("http://show.lsense.cn/AdShowService.asmx/GetAd", u.a(qVar));
                if (a3 == null) {
                    u.c("Enqueue requestHead...null");
                    i iVar = new i();
                    iVar.c("def");
                    iVar.a(u.a(b, "filename"));
                    iVar.b(u.a(b, "adid").trim() == "" ? 0 : Integer.parseInt(u.a(b, "adid").trim()));
                    iVar.d(u.a(b, "turns").trim() == "" ? 1 : Integer.parseInt(u.a(b, "turns").trim()));
                    iVar.a(u.a(b, "lastposition").trim() == "" ? -1 : Integer.parseInt(u.a(b, "lastposition").trim()));
                    iVar.c(u.a(b, "showtime").trim() == "" ? 20 : Integer.parseInt(u.a(b, "showtime").trim()));
                    String b3 = u.b(b, String.valueOf(u.a(b, "adid")) + ".default");
                    u.c("ad from default log file...");
                    String str3 = b3;
                    a2 = iVar;
                    str2 = str3;
                } else {
                    a2 = v.a(u.a(a3));
                    if (a2 != null && a2.c() < 0) {
                        u.a(a2.c());
                        a2 = null;
                    }
                    if (a2 != null) {
                        a2.a(u.d(a2.f()));
                        a2.c("");
                        File a4 = f.a(String.valueOf(a2.a()));
                        if (a4 != null) {
                            str2 = u.b(b, a4.getName());
                            a4.setLastModified(System.currentTimeMillis());
                            u.c("ad from log file..." + a2.c());
                        } else {
                            InputStream a5 = p.a(a2.f().trim());
                            if (a5 != null) {
                                str2 = u.a(a5);
                                if (a2.e() >= 100) {
                                    str = String.valueOf(a2.a()) + ".default";
                                    a2.d(a2.e() - 100);
                                    u.a(b, "adid", String.valueOf(a2.c()));
                                    u.a(b, "showtime", String.valueOf(a2.d()));
                                    u.a(b, "turns", String.valueOf(a2.e()));
                                    u.a(b, "filename", a2.a());
                                    u.c("aqueue.LENGTH:" + j.a());
                                    a2.c("def");
                                } else {
                                    str = String.valueOf(a2.a()) + ".ad";
                                    f.b(str);
                                }
                                u.b(b, str, str2);
                                u.c("ad from server..." + a2.c());
                            } else {
                                u.c("requestBody...null");
                            }
                        }
                    }
                }
                if (!str2.trim().equals("")) {
                    u.a(b, "lastposition", String.valueOf(a2.b()));
                    if (!a2.g().equalsIgnoreCase("def") || j.a() <= 0) {
                        List b4 = v.b(str2);
                        if (b4 != null) {
                            j.a(new e(a2, b4));
                            u.c("Enqueue..." + String.valueOf(a2.c()) + ".." + String.valueOf(j.a()));
                            a(200);
                            u.a(a2.c());
                        } else {
                            a(300);
                        }
                    } else {
                        a(200);
                        u.a(a2.c());
                    }
                } else {
                    a(300);
                }
            } else {
                a(200);
            }
        } catch (Exception e2) {
            a(300);
        } finally {
            a = false;
        }
    }
}
