package com.l.adlib_android;

import android.content.Context;
import android.os.Handler;
import java.io.InputStream;

public final class s extends Thread {
    public static Context a;
    public static locationmanager b;
    public static Handler c;

    private static void a(int i) {
        c.sendMessage(c.obtainMessage(i));
        u.c("sendMessage..." + String.valueOf(i));
    }

    public final void run() {
        i iVar = null;
        try {
            u.c("requestHead...");
            double[] b2 = b.b();
            q qVar = new q();
            qVar.a(k.b);
            qVar.a(l.a);
            qVar.a();
            qVar.a(l.b);
            qVar.b(l.c);
            qVar.a(l.d);
            qVar.b(l.e);
            qVar.c((byte) ((int) b2[0]));
            qVar.a(b2[1]);
            qVar.b(b2[2]);
            qVar.b(Integer.parseInt(u.a(a, "lastpositionfull")));
            qVar.b();
            InputStream a2 = p.a("http://show.lsense.cn/AdShowService.asmx/GetAd", u.a(qVar));
            if (a2 == null) {
                u.c("Enqueue requestHeadfull...null");
            } else {
                iVar = v.a(u.a(a2));
                u.c("Enqueue adHead:" + iVar.toString());
                InputStream a3 = p.a(iVar.f().trim());
                if (a3 != null) {
                    AdViewFull.a = v.b(u.a(a3));
                } else {
                    u.c("requestBody...null");
                }
            }
            if (AdViewFull.a.size() > 0) {
                u.a(iVar.c());
                u.a(a, "lastpositionfull", String.valueOf(iVar.b()));
                a(200);
                return;
            }
            a(300);
        } catch (Exception e) {
            a(300);
        }
    }
}
