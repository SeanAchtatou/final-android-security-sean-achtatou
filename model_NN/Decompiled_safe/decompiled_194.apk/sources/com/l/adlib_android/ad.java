package com.l.adlib_android;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.Display;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import java.util.List;

public class ad extends RelativeLayout implements AdListener {
    private final String a = "IMG";
    private final String b = "TEXT";
    private List c = new ArrayList();
    private Context d;
    private boolean e;
    private boolean f;
    private i g;
    private List h;
    private AdListener i;

    public ad(Context context) {
        super(context);
    }

    public ad(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ad(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }

    public ad(Context context, i iVar, List list, boolean z, boolean z2) {
        super(context);
        this.d = context;
        this.g = iVar;
        this.h = list;
        this.f = z2;
        this.e = z;
        setBackgroundDrawable(u.a(new int[]{k.d, k.e}));
        Display defaultDisplay = ((Activity) this.d).getWindowManager().getDefaultDisplay();
        int i2 = this.d.getResources().getConfiguration().orientation;
        k.k = i2;
        int height = i2 == 2 ? defaultDisplay.getHeight() : k.k == 1 ? defaultDisplay.getWidth() : 320;
        if (this.e) {
            k.m = (float) defaultDisplay.getWidth();
        } else {
            k.m = (float) height;
        }
        k.n = (0.15f * ((float) height)) + 2.0f;
        k.q = k.m / ((float) height);
        setLayoutParams(new RelativeLayout.LayoutParams((int) k.m, (int) k.n));
        k.o = k.m / 640.0f;
        k.p = k.n / 96.0f;
        a(list);
    }

    private void a(List list) {
        this.c.clear();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < list.size()) {
                d dVar = (d) list.get(i3);
                this.c.add(dVar.g().toString());
                int i4 = (int) (((float) (((double) dVar.i()) / 100.0d)) * k.m);
                int i5 = i3 + 256;
                n nVar = null;
                if (dVar.a().equalsIgnoreCase("IMG")) {
                    boolean equals = dVar.c().trim().equals("");
                    nVar = new n(this.d, u.b(dVar.b()), dVar.c(), equals ? 0 : 15, equals ? 0 : 5, equals ? 0 : 15, equals ? 0 : 5);
                    nVar.a(this);
                } else if (dVar.a().equalsIgnoreCase("TEXT")) {
                    t tVar = new t(this.d, dVar.d(), dVar.e());
                    tVar.a(this);
                    nVar = tVar;
                }
                if (nVar != null) {
                    nVar.setId(i5);
                    RelativeLayout.LayoutParams a2 = u.a(i4, -1);
                    if (i3 > 0) {
                        a2.addRule(1, i5 - 1);
                    }
                    addView(nVar, a2);
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public void OnClickAd(int i2) {
        if (this.i != null) {
            this.i.OnClickAd(i2);
        }
    }

    public final void a(AdListener adListener) {
        this.i = adListener;
    }

    public final boolean a() {
        return this.f;
    }

    public final void b() {
        this.f = true;
    }

    public final i c() {
        return this.g;
    }

    public final List d() {
        return this.h;
    }
}
