package jp.Adlantis.Android;

import android.app.Activity;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import java.io.Serializable;
import java.util.HashMap;

public class AdlantisAdActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private RelativeLayout f79a;
    private a b;
    private ImageView c;
    /* access modifiers changed from: private */
    public w d;
    /* access modifiers changed from: private */
    public ProgressBar e;
    private Button f;

    private static w a(Bundle bundle) {
        Serializable serializable = bundle.getSerializable("jp.Adlantis.Android.AdlantisAd");
        if (serializable == null) {
            return null;
        }
        if (serializable instanceof w) {
            return (w) serializable;
        }
        if (serializable instanceof HashMap) {
            return new w((HashMap) serializable);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.b.f80a != null) {
            new Handler().postDelayed(new q(this), 1);
        }
    }

    /* access modifiers changed from: private */
    public void a(Drawable drawable) {
        if (drawable != null) {
            a aVar = this.b;
            aVar.f80a = drawable;
            aVar.invalidate();
            a();
            this.e.setVisibility(4);
            this.f.setVisibility(4);
        }
    }

    static /* synthetic */ void d(AdlantisAdActivity adlantisAdActivity) {
        if (adlantisAdActivity.c != null) {
            Rect a2 = adlantisAdActivity.b.a();
            Log.d("AdlantisAdActivity", "imageBounds=" + a2);
            if (a2 != null && a2.width() > 0 && a2.height() > 0) {
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) adlantisAdActivity.c.getLayoutParams();
                Log.d("AdlantisAdActivity", "setting margins to left=" + a2.left + " top=" + a2.top);
                layoutParams.setMargins(a2.left, a2.top, 0, 0);
                adlantisAdActivity.c.setLayoutParams(layoutParams);
                adlantisAdActivity.c.requestLayout();
            }
            adlantisAdActivity.c.setVisibility(0);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0137  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x014d  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0165  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0170  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x017c  */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
            r9 = this;
            r8 = 1065353216(0x3f800000, float:1.0)
            r7 = 0
            r6 = 0
            r5 = -1
            r4 = -2
            super.onCreate(r10)
            android.widget.RelativeLayout r0 = new android.widget.RelativeLayout
            r0.<init>(r9)
            r9.f79a = r0
            android.widget.RelativeLayout r0 = r9.f79a
            r1 = -12626585(0xffffffffff3f5567, float:-2.5432598E38)
            r0.setBackgroundColor(r1)
            android.widget.RelativeLayout r0 = r9.f79a
            r9.setContentView(r0)
            float r0 = jp.Adlantis.Android.g.b(r9)
            android.widget.TextView r1 = new android.widget.TextView
            r1.<init>(r9)
            android.widget.RelativeLayout$LayoutParams r2 = new android.widget.RelativeLayout$LayoutParams
            r2.<init>(r4, r4)
            r3 = 11
            r2.addRule(r3, r5)
            r3 = 12
            r2.addRule(r3, r5)
            r3 = 1082130432(0x40800000, float:4.0)
            float r3 = r3 * r0
            int r3 = (int) r3
            float r0 = r0 * r8
            int r0 = (int) r0
            r2.setMargins(r6, r6, r3, r0)
            java.lang.String r0 = jp.Adlantis.Android.b.b()
            r1.setText(r0)
            r0 = 1094713344(0x41400000, float:12.0)
            r1.setTextSize(r0)
            r0 = 0
            r3 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r1.setShadowLayer(r8, r0, r8, r3)
            android.widget.RelativeLayout r0 = r9.f79a
            r0.addView(r1, r2)
            jp.Adlantis.Android.p r0 = new jp.Adlantis.Android.p
            r0.<init>(r9, r9)
            r9.b = r0
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r0.<init>(r5, r5)
            android.widget.RelativeLayout r1 = r9.f79a
            jp.Adlantis.Android.a r2 = r9.b
            r1.addView(r2, r0)
            jp.Adlantis.Android.a r0 = r9.b
            jp.Adlantis.Android.n r1 = new jp.Adlantis.Android.n
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            android.widget.ProgressBar r0 = new android.widget.ProgressBar
            r0.<init>(r9)
            r9.e = r0
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r0.<init>(r4, r4)
            r1 = 13
            r0.addRule(r1)
            android.widget.ProgressBar r1 = r9.e
            r1.setLayoutParams(r0)
            android.widget.ProgressBar r0 = r9.e
            r1 = 1
            r0.setIndeterminate(r1)
            android.widget.ProgressBar r0 = r9.e
            r1 = 101(0x65, float:1.42E-43)
            r0.setId(r1)
            android.widget.RelativeLayout r0 = r9.f79a
            android.widget.ProgressBar r1 = r9.e
            r0.addView(r1)
            android.widget.Button r0 = new android.widget.Button
            r0.<init>(r9)
            r9.f = r0
            android.widget.Button r0 = r9.f
            java.lang.String r1 = "Cancel"
            r0.setText(r1)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r0.<init>(r4, r4)
            r1 = 14
            r0.addRule(r1)
            r1 = 3
            r2 = 101(0x65, float:1.42E-43)
            r0.addRule(r1, r2)
            r1 = 10
            r0.setMargins(r6, r1, r6, r6)
            android.widget.Button r1 = r9.f
            r1.setLayoutParams(r0)
            android.widget.RelativeLayout r0 = r9.f79a
            android.widget.Button r1 = r9.f
            r0.addView(r1)
            android.widget.Button r0 = r9.f
            jp.Adlantis.Android.o r1 = new jp.Adlantis.Android.o
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            r9.d = r7
            if (r10 == 0) goto L_0x00df
            jp.Adlantis.Android.w r0 = a(r10)
            r9.d = r0
        L_0x00df:
            jp.Adlantis.Android.w r0 = r9.d
            if (r0 != 0) goto L_0x00f1
            android.content.Intent r0 = r9.getIntent()
            android.os.Bundle r0 = r0.getExtras()
            jp.Adlantis.Android.w r0 = a(r0)
            r9.d = r0
        L_0x00f1:
            jp.Adlantis.Android.w r0 = r9.d
            if (r0 == 0) goto L_0x01e5
            jp.Adlantis.Android.w r1 = r9.d
            android.widget.RelativeLayout r0 = r9.f79a
            android.content.res.Resources r2 = r0.getResources()
            android.content.res.Configuration r2 = r2.getConfiguration()
            int r2 = r2.orientation
            android.content.Context r0 = r0.getContext()
            boolean r3 = jp.Adlantis.Android.g.a(r0)
            java.lang.String r0 = "has_expand"
            java.lang.Object r0 = r1.get(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            if (r0 == 0) goto L_0x01e8
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x01e8
            r0 = 2
            if (r2 != r0) goto L_0x0176
            r0 = 1
        L_0x011f:
            if (r0 == 0) goto L_0x0178
            java.lang.String r0 = "landscape"
            r2 = r0
        L_0x0124:
            java.lang.String r0 = "expand_content"
            java.lang.Object r0 = r1.get(r0)
            java.util.Map r0 = (java.util.Map) r0
            if (r0 == 0) goto L_0x01e8
            java.lang.Object r0 = r0.get(r2)
            java.util.Map r0 = (java.util.Map) r0
            r1 = r0
        L_0x0135:
            if (r1 == 0) goto L_0x01e5
            if (r3 == 0) goto L_0x01e2
            java.lang.String r0 = "src_2x"
            java.lang.Object r0 = r1.get(r0)
            java.lang.String r0 = (java.lang.String) r0
        L_0x0141:
            if (r0 != 0) goto L_0x014b
            java.lang.String r0 = "src"
            java.lang.Object r0 = r1.get(r0)
            java.lang.String r0 = (java.lang.String) r0
        L_0x014b:
            if (r0 == 0) goto L_0x0161
            jp.Adlantis.Android.b r1 = jp.Adlantis.Android.b.a()
            jp.Adlantis.Android.v r1 = r1.c()
            jp.Adlantis.Android.s r2 = new jp.Adlantis.Android.s
            r2.<init>(r9)
            android.graphics.drawable.Drawable r0 = r1.a(r0, r2)
            r9.a(r0)
        L_0x0161:
            android.widget.ImageView r0 = r9.c
            if (r0 == 0) goto L_0x017c
            java.lang.String r0 = "AdlantisAdActivity"
            java.lang.String r1 = "initCloseBoxButton called more than once"
            android.util.Log.d(r0, r1)
        L_0x016c:
            jp.Adlantis.Android.w r0 = r9.d
            if (r0 == 0) goto L_0x0175
            jp.Adlantis.Android.w r0 = r9.d
            r0.a()
        L_0x0175:
            return
        L_0x0176:
            r0 = r6
            goto L_0x011f
        L_0x0178:
            java.lang.String r0 = "portrait"
            r2 = r0
            goto L_0x0124
        L_0x017c:
            java.lang.String r0 = "AdlantisAdActivity"
            java.lang.String r1 = "initCloseBoxButton"
            android.util.Log.d(r0, r1)
            android.widget.ImageView r0 = new android.widget.ImageView
            r0.<init>(r9)
            r9.c = r0
            android.widget.ImageView r0 = r9.c
            r1 = 4
            r0.setVisibility(r1)
            android.graphics.Rect r0 = jp.Adlantis.Android.y.c()
            float r1 = jp.Adlantis.Android.g.b(r9)
            android.widget.RelativeLayout$LayoutParams r2 = new android.widget.RelativeLayout$LayoutParams
            int r3 = r0.width()
            float r3 = (float) r3
            float r3 = r3 * r1
            int r3 = (int) r3
            int r0 = r0.height()
            float r0 = (float) r0
            float r0 = r0 * r1
            int r0 = (int) r0
            r2.<init>(r3, r0)
            r0 = 9
            r2.addRule(r0, r5)
            r0 = 10
            r2.addRule(r0, r5)
            android.widget.ImageView r0 = r9.c
            r0.setLayoutParams(r2)
            boolean r0 = jp.Adlantis.Android.g.a(r9)
            if (r0 == 0) goto L_0x01dd
            android.graphics.drawable.Drawable r0 = jp.Adlantis.Android.y.b()
        L_0x01c4:
            if (r0 == 0) goto L_0x01cb
            android.widget.ImageView r1 = r9.c
            r1.setImageDrawable(r0)
        L_0x01cb:
            android.widget.ImageView r0 = r9.c
            jp.Adlantis.Android.t r1 = new jp.Adlantis.Android.t
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            android.widget.RelativeLayout r0 = r9.f79a
            android.widget.ImageView r1 = r9.c
            r0.addView(r1, r2)
            goto L_0x016c
        L_0x01dd:
            android.graphics.drawable.Drawable r0 = jp.Adlantis.Android.y.a()
            goto L_0x01c4
        L_0x01e2:
            r0 = r7
            goto L_0x0141
        L_0x01e5:
            r0 = r7
            goto L_0x014b
        L_0x01e8:
            r1 = r7
            goto L_0x0135
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.Adlantis.Android.AdlantisAdActivity.onCreate(android.os.Bundle):void");
    }
}
