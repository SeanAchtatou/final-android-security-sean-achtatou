package jp.Adlantis.Android;

import android.content.Context;
import android.util.Log;

final class p extends a {
    private /* synthetic */ AdlantisAdActivity b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    p(AdlantisAdActivity adlantisAdActivity, Context context) {
        super(context);
        this.b = adlantisAdActivity;
    }

    /* access modifiers changed from: protected */
    public final void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        Log.d("AdlantisAdActivity", "ExpandedAdImageView onSizeChanged(" + i + "," + i2 + ")");
        this.b.a();
    }
}
