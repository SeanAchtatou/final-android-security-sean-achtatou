package jp.Adlantis.Android;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.util.HashMap;

public final class b {
    /* access modifiers changed from: private */
    public static String f = "AdManager";

    /* renamed from: a  reason: collision with root package name */
    private x f81a;
    private int b;
    private long c;
    private long d;
    private v e;
    private HashMap g;
    private String h;
    private boolean i;
    private boolean j;

    /* synthetic */ b() {
        this((byte) 0);
    }

    private b(byte b2) {
        this.b = 0;
        this.c = 300000;
        this.d = 10000;
        this.e = new v();
        this.h = null;
        this.i = false;
        this.j = false;
        this.f81a = new m();
    }

    public static b a() {
        return k.f89a;
    }

    public static String b() {
        return "Ads by AdLantis";
    }

    private static String e() {
        Context context = null;
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e2) {
            Log.e(f, e2.toString());
            return null;
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.HashMap f() {
        /*
            r3 = this;
            monitor-enter(r3)
            java.util.HashMap r0 = r3.g     // Catch:{ all -> 0x0096 }
            if (r0 == 0) goto L_0x0009
            java.util.HashMap r0 = r3.g     // Catch:{ all -> 0x0096 }
            monitor-exit(r3)     // Catch:{ all -> 0x0096 }
        L_0x0008:
            return r0
        L_0x0009:
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ all -> 0x0096 }
            r0.<init>()     // Catch:{ all -> 0x0096 }
            r3.g = r0     // Catch:{ all -> 0x0096 }
            java.util.HashMap r0 = r3.g     // Catch:{ all -> 0x0096 }
            java.lang.String r1 = "deviceClass"
            java.lang.String r2 = "android"
            r0.put(r1, r2)     // Catch:{ all -> 0x0096 }
            java.lang.String r0 = android.os.Build.VERSION.RELEASE     // Catch:{ all -> 0x0096 }
            if (r0 == 0) goto L_0x0037
            java.util.HashMap r1 = r3.g     // Catch:{ all -> 0x0096 }
            java.lang.String r2 = "deviceOsVersionFull"
            r1.put(r2, r0)     // Catch:{ all -> 0x0096 }
            java.text.NumberFormat r1 = java.text.NumberFormat.getNumberInstance()     // Catch:{ ParseException -> 0x0091 }
            java.lang.Number r0 = r1.parse(r0)     // Catch:{ ParseException -> 0x0091 }
            java.lang.String r0 = r0.toString()     // Catch:{ ParseException -> 0x0091 }
            java.util.HashMap r1 = r3.g     // Catch:{ ParseException -> 0x0091 }
            java.lang.String r2 = "deviceOsVersion"
            r1.put(r2, r0)     // Catch:{ ParseException -> 0x0091 }
        L_0x0037:
            java.lang.String r0 = android.os.Build.MODEL     // Catch:{ all -> 0x0096 }
            if (r0 == 0) goto L_0x004c
            java.lang.String r1 = "sdk"
            int r1 = r0.compareTo(r1)     // Catch:{ all -> 0x0096 }
            if (r1 != 0) goto L_0x0045
            java.lang.String r0 = "simulator"
        L_0x0045:
            java.util.HashMap r1 = r3.g     // Catch:{ all -> 0x0096 }
            java.lang.String r2 = "deviceFamily"
            r1.put(r2, r0)     // Catch:{ all -> 0x0096 }
        L_0x004c:
            java.lang.String r0 = android.os.Build.BRAND     // Catch:{ all -> 0x0096 }
            if (r0 == 0) goto L_0x0059
            java.util.HashMap r0 = r3.g     // Catch:{ all -> 0x0096 }
            java.lang.String r1 = "deviceBrand"
            java.lang.String r2 = android.os.Build.BRAND     // Catch:{ all -> 0x0096 }
            r0.put(r1, r2)     // Catch:{ all -> 0x0096 }
        L_0x0059:
            java.lang.String r0 = android.os.Build.BRAND     // Catch:{ all -> 0x0096 }
            if (r0 == 0) goto L_0x0066
            java.util.HashMap r0 = r3.g     // Catch:{ all -> 0x0096 }
            java.lang.String r1 = "deviceName"
            java.lang.String r2 = android.os.Build.DEVICE     // Catch:{ all -> 0x0096 }
            r0.put(r1, r2)     // Catch:{ all -> 0x0096 }
        L_0x0066:
            java.lang.String r0 = e()     // Catch:{ all -> 0x0096 }
            r1 = 0
            if (r0 == 0) goto L_0x0099
            java.lang.String r0 = jp.Adlantis.Android.g.a(r0)     // Catch:{ all -> 0x0096 }
        L_0x0071:
            if (r0 == 0) goto L_0x007a
            java.util.HashMap r1 = r3.g     // Catch:{ all -> 0x0096 }
            java.lang.String r2 = "udid"
            r1.put(r2, r0)     // Catch:{ all -> 0x0096 }
        L_0x007a:
            java.util.HashMap r0 = r3.g     // Catch:{ all -> 0x0096 }
            java.lang.String r1 = "sdkVersion"
            java.lang.String r2 = "1.3.2"
            r0.put(r1, r2)     // Catch:{ all -> 0x0096 }
            java.util.HashMap r0 = r3.g     // Catch:{ all -> 0x0096 }
            java.lang.String r1 = "sdkBuild"
            java.lang.String r2 = "261"
            r0.put(r1, r2)     // Catch:{ all -> 0x0096 }
            monitor-exit(r3)     // Catch:{ all -> 0x0096 }
            java.util.HashMap r0 = r3.g
            goto L_0x0008
        L_0x0091:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0096 }
            goto L_0x0037
        L_0x0096:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0096 }
            throw r0
        L_0x0099:
            r0 = r1
            goto L_0x0071
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.Adlantis.Android.b.f():java.util.HashMap");
    }

    public final Uri.Builder a(Uri uri) {
        Uri.Builder buildUpon = uri != null ? uri.buildUpon() : new Uri.Builder();
        g.a(buildUpon, f());
        return buildUpon;
    }

    public final void a(String str, l lVar) {
        Uri parse = Uri.parse(str);
        String scheme = parse.getScheme();
        if (scheme.compareTo("http") == 0 || scheme.compareTo("https") == 0) {
            new e(this, new d(this, new f(this, lVar)), str).start();
            return;
        }
        Handler handler = new Handler();
        Message obtainMessage = handler.obtainMessage(0, parse);
        handler.sendMessage(obtainMessage);
        if (lVar != null) {
            lVar.a((Uri) obtainMessage.obj);
        }
    }

    public final v c() {
        return this.e;
    }
}
