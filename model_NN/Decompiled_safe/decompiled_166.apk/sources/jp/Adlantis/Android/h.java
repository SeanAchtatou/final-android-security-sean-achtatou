package jp.Adlantis.Android;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;

final class h extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ u f86a;
    private /* synthetic */ String b;
    private /* synthetic */ v c;

    h(v vVar, u uVar, String str) {
        this.c = vVar;
        this.f86a = uVar;
        this.b = str;
    }

    public final void handleMessage(Message message) {
        if (this.f86a != null) {
            this.f86a.a((Drawable) message.obj);
        }
    }
}
