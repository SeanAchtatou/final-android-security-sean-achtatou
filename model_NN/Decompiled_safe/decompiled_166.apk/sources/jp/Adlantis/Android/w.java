package jp.Adlantis.Android;

import android.net.Uri;
import android.util.Log;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public final class w extends HashMap implements Serializable, Map {

    /* renamed from: a  reason: collision with root package name */
    private boolean f97a = false;
    /* access modifiers changed from: private */
    public boolean b;
    /* access modifiers changed from: private */
    public boolean c;

    public w(HashMap hashMap) {
        super(hashMap);
    }

    /* access modifiers changed from: private */
    public boolean a(String str, String str2) {
        Uri build;
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            String str3 = (String) get(str);
            if (str3 == null) {
                build = null;
            } else {
                build = b.a().a(Uri.parse(str3)).build();
            }
            String uri = build.toString();
            if (uri == null) {
                return false;
            }
            int statusCode = defaultHttpClient.execute(new HttpGet(uri)).getStatusLine().getStatusCode();
            if (statusCode >= 200 && statusCode < 400) {
                return true;
            }
            Log.e("AdlantisAd", str2 + " status=" + statusCode);
            return false;
        } catch (MalformedURLException e) {
            Log.e("AdlantisAd", str2 + " exception=" + e.toString());
            return false;
        } catch (IOException e2) {
            Log.e("AdlantisAd", str2 + " exception=" + e2.toString());
            return false;
        } catch (OutOfMemoryError e3) {
            Log.e("AdlantisAd", str2 + " OutOfMemoryError=" + e3.toString());
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (!this.b && !this.c) {
            new j(this).start();
        }
    }
}
