package jp.Adlantis.Android;

import android.net.Uri;
import android.util.Log;
import java.io.IOException;
import java.net.MalformedURLException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultRedirectHandler;

final class e extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DefaultRedirectHandler f84a;
    private /* synthetic */ String b;
    private /* synthetic */ b c;

    e(b bVar, DefaultRedirectHandler defaultRedirectHandler, String str) {
        this.c = bVar;
        this.f84a = defaultRedirectHandler;
        this.b = str;
    }

    public final void run() {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            defaultHttpClient.setRedirectHandler(this.f84a);
            String uri = this.c.a(Uri.parse(this.b)).build().toString();
            Log.d(b.f, "handleHttpClickRequest=" + uri);
            defaultHttpClient.execute(new HttpGet(uri));
        } catch (MalformedURLException e) {
            Log.e(b.f, e.toString());
        } catch (IOException e2) {
            Log.e(b.f, e2.toString());
        }
    }
}
