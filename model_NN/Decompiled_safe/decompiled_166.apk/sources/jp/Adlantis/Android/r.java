package jp.Adlantis.Android;

final class r {

    /* renamed from: a  reason: collision with root package name */
    private static final String f93a = System.getProperty("line.separator");
    private static char[] b = new char[64];
    private static byte[] c = new byte[128];

    static {
        char c2 = 'A';
        int i = 0;
        while (c2 <= 'Z') {
            b[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = 'a';
        while (c3 <= 'z') {
            b[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        char c4 = '0';
        while (c4 <= '9') {
            b[i] = c4;
            c4 = (char) (c4 + 1);
            i++;
        }
        b[i] = '+';
        b[i + 1] = '/';
        for (int i2 = 0; i2 < c.length; i2++) {
            c[i2] = -1;
        }
        for (int i3 = 0; i3 < 64; i3++) {
            c[b[i3]] = (byte) i3;
        }
    }

    private r() {
    }

    public static byte[] a(String str) {
        int i;
        char c2;
        int i2;
        char c3;
        int i3 = 0;
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        if (length % 4 != 0) {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }
        while (length > 0 && charArray[(length + 0) - 1] == '=') {
            length--;
        }
        int i4 = (length * 3) / 4;
        byte[] bArr = new byte[i4];
        int i5 = length + 0;
        int i6 = 0;
        while (i6 < i5) {
            int i7 = i6 + 1;
            char c4 = charArray[i6];
            int i8 = i7 + 1;
            char c5 = charArray[i7];
            if (i8 < i5) {
                i = i8 + 1;
                c2 = charArray[i8];
            } else {
                i = i8;
                c2 = 'A';
            }
            if (i < i5) {
                i2 = i + 1;
                c3 = charArray[i];
            } else {
                i2 = i;
                c3 = 'A';
            }
            if (c4 > 127 || c5 > 127 || c2 > 127 || c3 > 127) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            byte b2 = c[c4];
            byte b3 = c[c5];
            byte b4 = c[c2];
            byte b5 = c[c3];
            if (b2 < 0 || b3 < 0 || b4 < 0 || b5 < 0) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int i9 = (b2 << 2) | (b3 >>> 4);
            int i10 = ((b3 & 15) << 4) | (b4 >>> 2);
            byte b6 = ((b4 & 3) << 6) | b5;
            int i11 = i3 + 1;
            bArr[i3] = (byte) i9;
            if (i11 < i4) {
                i3 = i11 + 1;
                bArr[i11] = (byte) i10;
            } else {
                i3 = i11;
            }
            if (i3 < i4) {
                bArr[i3] = (byte) b6;
                i3++;
            }
            i6 = i2;
        }
        return bArr;
    }
}
