package jp.Adlantis.Android;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import java.lang.ref.SoftReference;

final class i extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ String f87a;
    private /* synthetic */ Handler b;
    private /* synthetic */ v c;

    i(v vVar, String str, Handler handler) {
        this.c = vVar;
        this.f87a = str;
        this.b = handler;
    }

    public final void run() {
        Drawable a2 = v.a(this.f87a);
        if (a2 != null) {
            this.c.f96a.put(this.f87a, new SoftReference(a2));
            Log.d("AsyncImageLoader", "imageCache.size()=" + this.c.f96a.size());
            this.b.sendMessage(this.b.obtainMessage(0, a2));
        }
    }
}
