package jp.Adlantis.Android;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;

class a extends View {

    /* renamed from: a  reason: collision with root package name */
    Drawable f80a;

    public a(Context context) {
        super(context);
    }

    public final Rect a() {
        Rect rect = new Rect();
        getDrawingRect(rect);
        if (this.f80a == null) {
            return rect;
        }
        int intrinsicWidth = this.f80a.getIntrinsicWidth();
        int intrinsicHeight = this.f80a.getIntrinsicHeight();
        if (intrinsicWidth == 0 || intrinsicHeight == 0 || rect.height() == 0 || rect.width() == 0) {
            return rect;
        }
        float height = ((float) intrinsicWidth) / ((float) intrinsicHeight) < ((float) rect.width()) / ((float) rect.height()) ? ((float) rect.height()) / ((float) intrinsicHeight) : ((float) rect.width()) / ((float) intrinsicWidth);
        int i = (int) (((float) intrinsicWidth) * height);
        int i2 = (int) (((float) intrinsicHeight) * height);
        int width = rect.left + ((rect.width() - i) / 2);
        int height2 = ((rect.height() - i2) / 2) + rect.top;
        return new Rect(width, height2, i + width, i2 + height2);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f80a != null) {
            this.f80a.setBounds(a());
            this.f80a.draw(canvas);
        }
    }
}
