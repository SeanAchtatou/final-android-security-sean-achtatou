package jp.a.a.a.a;

import a.a.b;
import a.a.f;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;

public final class d extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private long f102a;
    private int b;
    private long c;
    private Date d;
    private String e;
    private String f;
    private int g;
    private boolean h;
    private Context i;
    private ProgressDialog j;
    private List k;
    private boolean l;
    private b m;

    public d() {
        this.f102a = 0;
        this.b = -1;
        this.c = 0;
        this.d = null;
        this.e = null;
        this.f = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = false;
        this.m = null;
        this.g = 10;
        this.h = false;
    }

    public d(int i2) {
        this.f102a = 0;
        this.b = i2;
        this.c = 0;
        this.d = null;
        this.e = null;
        this.f = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = false;
        this.m = null;
        this.h = false;
    }

    private void a(String str) {
        if (this.k == null) {
            this.k = new ArrayList();
        } else {
            this.k.clear();
        }
        this.l = false;
        try {
            a.a.d dVar = new a.a.d(str);
            Log.d("ScoreRankSample", str);
            b b2 = dVar.b("HighScore");
            for (int i2 = 0; i2 < b2.a(); i2++) {
                a.a.d a2 = b2.a(i2);
                this.f102a = (long) i2;
                this.b = a2.a("kindGames");
                this.e = a2.d("name");
                this.c = a2.c("score");
                this.d = new Date(a2.d("date"));
                if (str.matches("version")) {
                    this.f = a2.d("version");
                } else {
                    this.f = "";
                }
                this.k.add(new c(this.f102a, this.b, this.c, this.d, this.e, this.f));
                this.l = true;
            }
        } catch (f e2) {
            e2.printStackTrace();
        }
    }

    private Boolean c() {
        boolean z = true;
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(defaultHttpClient.getParams(), 5000);
            HttpConnectionParams.setSoTimeout(defaultHttpClient.getParams(), 10000);
            StringBuffer stringBuffer = new StringBuffer("http://6.latest.satbox-scorerank.appspot.com/");
            stringBuffer.append("scorerank?kindGames=");
            stringBuffer.append(Integer.toString(this.b));
            stringBuffer.append("&format=json&rank=");
            stringBuffer.append(Integer.toString(this.g));
            if (this.h) {
                stringBuffer.append("&reverse");
            }
            Log.d("RegistScore", stringBuffer.toString());
            HttpResponse execute = defaultHttpClient.execute(new HttpGet(stringBuffer.toString()));
            if (execute.getStatusLine().getStatusCode() < 400) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(execute.getEntity().getContent(), "UTF-8"));
                StringBuilder sb = new StringBuilder();
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    sb.append(readLine);
                }
                a(sb.toString());
            } else {
                z = false;
            }
        } catch (Exception e2) {
            Log.d("RegistScore", e2.toString());
            z = false;
        }
        return Boolean.valueOf(z);
    }

    public final List a() {
        return this.k;
    }

    public final void a(Context context) {
        this.i = context;
    }

    public final void a(b bVar) {
        this.m = bVar;
    }

    public final void b() {
        this.g = 100;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        return c();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        if (this.j != null) {
            this.j.dismiss();
            this.j = null;
        }
        if (this.m != null) {
            this.m.a(this.l);
        }
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        if (this.i != null) {
            this.j = new ProgressDialog(this.i);
            this.j.setTitle("");
            this.j.setMessage("");
            this.j.setProgressStyle(0);
            this.j.show();
        }
    }
}
