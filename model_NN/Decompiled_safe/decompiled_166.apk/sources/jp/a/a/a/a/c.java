package jp.a.a.a.a;

import java.util.Date;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private long f101a;
    private int b;
    private long c;
    private Date d;
    private String e;
    private String f;

    public c(long j, int i, long j2, Date date, String str, String str2) {
        this.f101a = j;
        this.b = i;
        this.c = j2;
        this.d = date;
        this.e = str;
        this.f = str2;
    }

    public final long a() {
        return this.c;
    }

    public final String b() {
        return this.e;
    }
}
