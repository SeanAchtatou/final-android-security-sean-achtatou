package jp.a.a.a.a;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import java.util.ArrayList;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;

public final class a extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private long f100a;
    private String b;
    private int c;
    private Context d;
    private ProgressDialog e;
    private boolean f;
    private b g;

    public a() {
        this.c = -1;
        this.f100a = 0;
        this.b = null;
        this.d = null;
        this.e = null;
        this.f = false;
        this.g = null;
    }

    public a(int i, long j, String str) {
        this.c = i;
        this.f100a = j;
        this.b = str;
        this.d = null;
        this.e = null;
        this.f = false;
        this.g = null;
    }

    private Boolean a() {
        this.f = false;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("kindGames", Integer.toString(this.c)));
        arrayList.add(new BasicNameValuePair("score", Long.toString(this.f100a)));
        arrayList.add(new BasicNameValuePair("name", this.b));
        try {
            HttpPost httpPost = new HttpPost("http://6.latest.satbox-scorerank.appspot.com/");
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(defaultHttpClient.getParams(), 5000);
            HttpConnectionParams.setSoTimeout(defaultHttpClient.getParams(), 10000);
            return defaultHttpClient.execute(httpPost).getStatusLine().getStatusCode() < 400;
        } catch (Exception e2) {
            return false;
        }
    }

    private void b() {
        if (this.e != null) {
            this.e.dismiss();
            this.e = null;
        }
    }

    public final void a(Context context) {
        this.d = context;
    }

    public final void a(b bVar) {
        this.g = bVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        b();
        if (this.g != null) {
            this.g.a(false);
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        Boolean bool = (Boolean) obj;
        b();
        if (this.g != null) {
            this.g.a(bool.booleanValue());
        }
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        if (this.d != null) {
            this.e = new ProgressDialog(this.d);
            this.e.setTitle("");
            this.e.setMessage("");
            this.e.setProgressStyle(0);
            this.e.show();
        }
    }
}
