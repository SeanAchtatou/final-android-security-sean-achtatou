package com.GalaxyLaser;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private Context f4a;
    private String b;
    private int c;

    public a(Context context, String str, int i) {
        this.f4a = context;
        this.b = str;
        this.c = i;
    }

    public final void a() {
        SharedPreferences sharedPreferences = this.f4a.getSharedPreferences("prefkey", 0);
        ArrayList arrayList = new ArrayList();
        arrayList.clear();
        for (int i = 0; i < 99; i++) {
            String format = String.format("%02d", Integer.valueOf(i + 1));
            arrayList.add(new j(sharedPreferences.getInt("MyScore" + this.b + format, 0), sharedPreferences.getString("MyDate" + this.b + format, "9999/99/99 99:99:99"), "", ""));
        }
        Calendar instance = Calendar.getInstance();
        int i2 = instance.get(1);
        int i3 = instance.get(5);
        int i4 = instance.get(11);
        int i5 = instance.get(12);
        int i6 = instance.get(13);
        arrayList.add(new j(this.c, String.valueOf(String.format("%04d", Integer.valueOf(i2))) + "/" + String.format("%02d", Integer.valueOf(instance.get(2) + 1)) + "/" + String.format("%02d", Integer.valueOf(i3)) + " " + String.format("%02d", Integer.valueOf(i4)) + ":" + String.format("%02d", Integer.valueOf(i5)) + ":" + String.format("%02d", Integer.valueOf(i6)), "", ""));
        Collections.sort(arrayList, new ag(this));
        SharedPreferences.Editor edit = sharedPreferences.edit();
        int i7 = 0;
        while (true) {
            int i8 = i7;
            if (i8 >= 99) {
                edit.commit();
                return;
            }
            String format2 = String.format("%02d", Integer.valueOf(i8 + 1));
            j jVar = (j) arrayList.get(i8);
            edit.putInt("MyScore" + this.b + format2, jVar.a());
            edit.putString("MyDate" + this.b + format2, jVar.b());
            i7 = i8 + 1;
        }
    }
}
