package com.GalaxyLaser;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

final class i implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HighScoreActivity f28a;

    i(HighScoreActivity highScoreActivity) {
        this.f28a = highScoreActivity;
    }

    public final void onClick(View view) {
        try {
            this.f28a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=jp.co.arttec.satbox.galaxylaser_vsboss")));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }
}
