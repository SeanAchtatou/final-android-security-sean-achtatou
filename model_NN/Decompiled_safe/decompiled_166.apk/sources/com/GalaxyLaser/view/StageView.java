package com.GalaxyLaser.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.a.b;
import com.GalaxyLaser.a.d;
import com.GalaxyLaser.a.g;
import com.GalaxyLaser.a.h;
import com.GalaxyLaser.a.i;
import com.GalaxyLaser.a.k;
import com.GalaxyLaser.a.l;
import com.GalaxyLaser.a.m;
import com.GalaxyLaser.a.n;
import com.GalaxyLaser.a.o;
import com.GalaxyLaser.a.r;
import com.GalaxyLaser.b.a;
import com.GalaxyLaser.b.c;
import com.GalaxyLaser.b.f;
import com.GalaxyLaser.c.aa;
import com.GalaxyLaser.c.an;
import com.GalaxyLaser.c.aw;
import com.GalaxyLaser.c.x;
import com.GalaxyLaser.c.y;
import com.GalaxyLaser.c.z;
import com.GalaxyLaser.util.e;

public class StageView extends SurfaceView implements SurfaceHolder.Callback, c {
    private boolean A;
    private boolean B;
    private int C;
    private int D;
    private boolean E;
    private boolean F;

    /* renamed from: a  reason: collision with root package name */
    private Context f72a;
    private SurfaceHolder b;
    private Bitmap c;
    private Rect d;
    private Rect e;
    private Resources f = this.f72a.getResources();
    private com.GalaxyLaser.a.c g = com.GalaxyLaser.a.c.a(this.f72a);
    private n h = n.a(this.f72a);
    private h i = h.a(this.f72a);
    private g j = g.a(this.f72a);
    private l k = l.a(this.f72a);
    private o l = o.a(this.f72a);
    private b m = b.a(this.f72a);
    private d n = d.a(this.f72a);
    private r o = r.a(this.f72a);
    private i p = i.a(this.f72a);
    private e q = e.a();
    private a r;
    private com.GalaxyLaser.b.b s;
    private com.GalaxyLaser.b.h t;
    private com.GalaxyLaser.b.e u;
    private com.GalaxyLaser.b.d v;
    private f w;
    private com.GalaxyLaser.b.g x;
    private int y;
    private Thread z;

    public StageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f72a = context;
        getHolder().addCallback(this);
        setFocusable(true);
        requestFocus();
        com.GalaxyLaser.a.f.a(this.f72a);
        k.a(this.f72a);
        if (this.E) {
            this.c = BitmapFactory.decodeResource(this.f, C0000R.drawable.game_background_nightmare);
            this.e = new Rect(0, 0, this.c.getWidth(), this.c.getHeight());
        } else {
            this.c = BitmapFactory.decodeResource(this.f, C0000R.drawable.game_background);
            this.e = new Rect(0, 0, this.c.getWidth(), this.c.getHeight());
        }
        this.r = null;
        this.s = null;
        this.t = null;
        this.y = 0;
        this.u = null;
        this.v = null;
        this.w = null;
        this.x = null;
        this.z = null;
        this.C = 0;
        this.D = 1;
        this.q.setSeed((long) this.D);
        this.A = false;
        this.B = false;
        this.d = null;
        com.GalaxyLaser.a.a.a().a(false);
        k.a(this.f72a).a(com.GalaxyLaser.util.d.GameStart);
        this.F = false;
    }

    private void a(Canvas canvas) {
        if (this.g != null) {
            while (this.g.a()) {
                this.g.a(new aw(this.d));
            }
            this.g.b();
            this.g.a(this.d);
            this.g.a(canvas);
        }
    }

    private void b(Canvas canvas) {
        if (this.o != null) {
            while (this.o.a()) {
                if (!this.E) {
                    switch (this.q.nextInt(2)) {
                        case 0:
                            this.o.a(new z(this.d, this.f72a));
                            continue;
                        case 1:
                            this.o.a(new aa(this.d, this.f72a));
                            continue;
                    }
                } else {
                    switch (this.q.nextInt(2)) {
                        case 0:
                            this.o.a(new x(this.d, this.f72a));
                            continue;
                        case 1:
                            this.o.a(new y(this.d, this.f72a));
                            continue;
                    }
                }
            }
            this.o.b();
            this.o.a(this.d);
            this.o.a(canvas);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x010e A[Catch:{ InterruptedException -> 0x0424 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void g() {
        /*
            r13 = this;
            r11 = 2
            r10 = 1
            r9 = 0
            r3 = 0
            monitor-enter(r13)
            com.GalaxyLaser.util.b r0 = new com.GalaxyLaser.util.b     // Catch:{ all -> 0x02d4 }
            r0.<init>()     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.n r1 = r13.h     // Catch:{ all -> 0x02d4 }
            if (r1 == 0) goto L_0x042e
            com.GalaxyLaser.a.n r1 = r13.h     // Catch:{ all -> 0x02d4 }
            int r1 = r1.h()     // Catch:{ all -> 0x02d4 }
        L_0x0014:
            com.GalaxyLaser.a.n r2 = r13.h     // Catch:{ all -> 0x02d4 }
            if (r2 == 0) goto L_0x042a
            com.GalaxyLaser.a.n r2 = r13.h     // Catch:{ all -> 0x02d4 }
            int r2 = r2.a()     // Catch:{ all -> 0x02d4 }
            r12 = r2
            r2 = r1
            r1 = r12
        L_0x0021:
            java.lang.Thread r3 = r13.z     // Catch:{ all -> 0x02d4 }
            if (r3 != 0) goto L_0x0027
        L_0x0025:
            monitor-exit(r13)
            return
        L_0x0027:
            boolean r3 = r13.F     // Catch:{ all -> 0x02d4 }
            if (r3 != 0) goto L_0x02c9
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x02d4 }
            r0.a(r3)     // Catch:{ all -> 0x02d4 }
            android.view.SurfaceHolder r3 = r13.b     // Catch:{ all -> 0x02d4 }
            android.graphics.Canvas r3 = r3.lockCanvas()     // Catch:{ all -> 0x02d4 }
            if (r3 == 0) goto L_0x0025
            android.graphics.Rect r4 = r13.d     // Catch:{ all -> 0x02d4 }
            if (r4 != 0) goto L_0x004f
            android.graphics.Rect r4 = new android.graphics.Rect     // Catch:{ all -> 0x02d4 }
            r5 = 0
            r6 = 0
            int r7 = r3.getWidth()     // Catch:{ all -> 0x02d4 }
            int r8 = r3.getHeight()     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6, r7, r8)     // Catch:{ all -> 0x02d4 }
            r13.d = r4     // Catch:{ all -> 0x02d4 }
        L_0x004f:
            android.graphics.Rect r4 = r13.e     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x0061
            android.graphics.Rect r4 = r13.d     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x0061
            android.graphics.Bitmap r4 = r13.c     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.e     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r6 = r13.d     // Catch:{ all -> 0x02d4 }
            r7 = 0
            r3.drawBitmap(r4, r5, r6, r7)     // Catch:{ all -> 0x02d4 }
        L_0x0061:
            int r4 = r13.D     // Catch:{ all -> 0x02d4 }
            int r4 = r4 - r10
            int r4 = r4 % 6
            if (r11 <= r4) goto L_0x02d7
            com.GalaxyLaser.util.e r5 = r13.q     // Catch:{ all -> 0x02d4 }
            r6 = 68
            int r5 = r5.nextInt(r6)     // Catch:{ all -> 0x02d4 }
        L_0x0070:
            boolean r6 = r13.A     // Catch:{ all -> 0x02d4 }
            if (r6 != 0) goto L_0x00a5
            int r6 = r4 + 1
            if (r5 >= r6) goto L_0x00a5
            com.GalaxyLaser.a.h r5 = r13.i     // Catch:{ all -> 0x02d4 }
            if (r5 == 0) goto L_0x00a5
            com.GalaxyLaser.a.n r5 = r13.h     // Catch:{ all -> 0x02d4 }
            if (r5 == 0) goto L_0x00a5
            com.GalaxyLaser.a.n r5 = r13.h     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.c.a r5 = r5.g()     // Catch:{ all -> 0x02d4 }
            if (r5 == 0) goto L_0x00a5
            if (r10 < r4) goto L_0x02e1
            com.GalaxyLaser.util.e r4 = r13.q     // Catch:{ all -> 0x02d4 }
            r5 = 2
            int r4 = r4.nextInt(r5)     // Catch:{ all -> 0x02d4 }
        L_0x0091:
            switch(r4) {
                case 0: goto L_0x0301;
                case 1: goto L_0x030c;
                case 2: goto L_0x0317;
                case 3: goto L_0x0328;
                case 4: goto L_0x0333;
                case 5: goto L_0x033e;
                default: goto L_0x0094;
            }     // Catch:{ all -> 0x02d4 }
        L_0x0094:
            r4 = r9
        L_0x0095:
            if (r4 == 0) goto L_0x00a5
            int r5 = r4.l()     // Catch:{ all -> 0x02d4 }
            int r5 = r5 + 1
            r4.a(r5)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.h r5 = r13.i     // Catch:{ all -> 0x02d4 }
            r5.a(r4)     // Catch:{ all -> 0x02d4 }
        L_0x00a5:
            com.GalaxyLaser.a.a r4 = com.GalaxyLaser.a.a.a()     // Catch:{ all -> 0x02d4 }
            boolean r4 = r4.c()     // Catch:{ all -> 0x02d4 }
            if (r4 != 0) goto L_0x00df
            boolean r4 = r13.A     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x00df
            boolean r4 = r13.B     // Catch:{ all -> 0x02d4 }
            if (r4 != 0) goto L_0x00df
            r4 = 1
            r13.B = r4     // Catch:{ all -> 0x02d4 }
            int r4 = r13.D     // Catch:{ all -> 0x02d4 }
            int r4 = r4 - r10
            int r4 = r4 % 6
            switch(r4) {
                case 0: goto L_0x0349;
                case 1: goto L_0x0363;
                case 2: goto L_0x037d;
                case 3: goto L_0x03b1;
                case 4: goto L_0x0397;
                case 5: goto L_0x03cb;
                default: goto L_0x00c2;
            }     // Catch:{ all -> 0x02d4 }
        L_0x00c2:
            com.GalaxyLaser.c.bd r4 = new com.GalaxyLaser.c.bd     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x02d4 }
            r4.a(r13)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.b.e r5 = r13.u     // Catch:{ all -> 0x02d4 }
            if (r5 == 0) goto L_0x00da
            com.GalaxyLaser.b.e r5 = r13.u     // Catch:{ all -> 0x02d4 }
            r6 = 2130968578(0x7f040002, float:1.7545814E38)
            r5.b(r6)     // Catch:{ all -> 0x02d4 }
        L_0x00da:
            com.GalaxyLaser.a.h r5 = r13.i     // Catch:{ all -> 0x02d4 }
            r5.a(r4)     // Catch:{ all -> 0x02d4 }
        L_0x00df:
            r13.a(r3)     // Catch:{ all -> 0x02d4 }
            r13.b(r3)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.l r4 = r13.k     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x012b
            com.GalaxyLaser.util.e r4 = r13.q     // Catch:{ all -> 0x02d4 }
            r5 = 400(0x190, float:5.6E-43)
            int r4 = r4.nextInt(r5)     // Catch:{ all -> 0x02d4 }
            if (r4 >= r11) goto L_0x00fd
            com.GalaxyLaser.util.e r4 = r13.q     // Catch:{ all -> 0x02d4 }
            r5 = 5
            int r4 = r4.nextInt(r5)     // Catch:{ all -> 0x02d4 }
            switch(r4) {
                case 0: goto L_0x03e5;
                case 1: goto L_0x03e5;
                case 2: goto L_0x03e5;
                case 3: goto L_0x03f5;
                case 4: goto L_0x0409;
                default: goto L_0x00fd;
            }     // Catch:{ all -> 0x02d4 }
        L_0x00fd:
            r4 = r9
        L_0x00fe:
            if (r4 == 0) goto L_0x010c
            com.GalaxyLaser.a.l r5 = r13.k     // Catch:{ all -> 0x02d4 }
            boolean r5 = r5.a(r4)     // Catch:{ all -> 0x02d4 }
            if (r5 == 0) goto L_0x010c
            r4.j()     // Catch:{ all -> 0x02d4 }
            r4 = r9
        L_0x010c:
            if (r4 == 0) goto L_0x0113
            com.GalaxyLaser.a.l r5 = r13.k     // Catch:{ all -> 0x02d4 }
            r5.a(r4)     // Catch:{ all -> 0x02d4 }
        L_0x0113:
            com.GalaxyLaser.a.l r4 = r13.k     // Catch:{ all -> 0x02d4 }
            r4.b()     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.l r4 = r13.k     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            r4.a(r5)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.l r4 = r13.k     // Catch:{ all -> 0x02d4 }
            r4.a(r3)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.l r4 = r13.k     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.n r5 = r13.h     // Catch:{ all -> 0x02d4 }
            r4.a(r5)     // Catch:{ all -> 0x02d4 }
        L_0x012b:
            com.GalaxyLaser.a.h r4 = r13.i     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x0166
            com.GalaxyLaser.a.h r4 = r13.i     // Catch:{ all -> 0x02d4 }
            r4.a()     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.h r4 = r13.i     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            r4.a(r5)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.h r4 = r13.i     // Catch:{ all -> 0x02d4 }
            r4.a(r3)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.n r4 = r13.h     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x0166
            com.GalaxyLaser.a.h r4 = r13.i     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.n r5 = r13.h     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.c.a r5 = r5.g()     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.a(r5, r6)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.h r4 = r13.i     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.n r5 = r13.h     // Catch:{ all -> 0x02d4 }
            r4.a(r5)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.h r4 = r13.i     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.n r5 = r13.h     // Catch:{ all -> 0x02d4 }
            r4.b(r5)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.h r4 = r13.i     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.l r5 = r13.k     // Catch:{ all -> 0x02d4 }
            r4.a(r5)     // Catch:{ all -> 0x02d4 }
        L_0x0166:
            com.GalaxyLaser.a.n r4 = r13.h     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x0186
            com.GalaxyLaser.a.n r4 = r13.h     // Catch:{ all -> 0x02d4 }
            r4.a(r3)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.n r4 = r13.h     // Catch:{ all -> 0x02d4 }
            boolean r4 = r4.f()     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x0186
            com.GalaxyLaser.a.g r4 = r13.j     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x0186
            com.GalaxyLaser.a.g r4 = r13.j     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.n r5 = r13.h     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.c.a r5 = r5.g()     // Catch:{ all -> 0x02d4 }
            r4.a(r5)     // Catch:{ all -> 0x02d4 }
        L_0x0186:
            com.GalaxyLaser.a.g r4 = r13.j     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x01b7
            com.GalaxyLaser.a.g r4 = r13.j     // Catch:{ all -> 0x02d4 }
            r4.a()     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.g r4 = r13.j     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            r4.a(r5)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.g r4 = r13.j     // Catch:{ all -> 0x02d4 }
            r4.a(r3)     // Catch:{ all -> 0x02d4 }
            int r4 = r13.y     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.g r5 = r13.j     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.h r6 = r13.i     // Catch:{ all -> 0x02d4 }
            int r5 = r5.a(r6)     // Catch:{ all -> 0x02d4 }
            int r4 = r4 + r5
            r13.y = r4     // Catch:{ all -> 0x02d4 }
            int r4 = r13.y     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.g r5 = r13.j     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.l r6 = r13.k     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.o r7 = r13.l     // Catch:{ all -> 0x02d4 }
            int r5 = r5.a(r6, r7)     // Catch:{ all -> 0x02d4 }
            int r4 = r4 + r5
            r13.y = r4     // Catch:{ all -> 0x02d4 }
        L_0x01b7:
            com.GalaxyLaser.a.o r4 = r13.l     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x01d9
            com.GalaxyLaser.a.o r4 = r13.l     // Catch:{ all -> 0x02d4 }
            r4.b()     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.o r4 = r13.l     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            r4.a(r5)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.o r4 = r13.l     // Catch:{ all -> 0x02d4 }
            r4.a(r3)     // Catch:{ all -> 0x02d4 }
            int r4 = r13.y     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.o r5 = r13.l     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.n r6 = r13.h     // Catch:{ all -> 0x02d4 }
            int r5 = r5.a(r6)     // Catch:{ all -> 0x02d4 }
            int r4 = r4 + r5
            r13.y = r4     // Catch:{ all -> 0x02d4 }
        L_0x01d9:
            com.GalaxyLaser.b.b r4 = r13.s     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x0201
            com.GalaxyLaser.a.n r4 = r13.h     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x0201
            com.GalaxyLaser.a.n r4 = r13.h     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.c.a r4 = r4.g()     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x0201
            com.GalaxyLaser.a.n r4 = r13.h     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.c.a r4 = r4.g()     // Catch:{ all -> 0x02d4 }
            int r4 = r4.h()     // Catch:{ all -> 0x02d4 }
            if (r2 == r4) goto L_0x0201
            com.GalaxyLaser.b.b r2 = r13.s     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.n r5 = r13.h     // Catch:{ all -> 0x02d4 }
            int r5 = r5.a()     // Catch:{ all -> 0x02d4 }
            r2.a(r4, r5)     // Catch:{ all -> 0x02d4 }
            r2 = r4
        L_0x0201:
            com.GalaxyLaser.b.b r4 = r13.s     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x0221
            com.GalaxyLaser.a.n r4 = r13.h     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x0221
            com.GalaxyLaser.a.n r4 = r13.h     // Catch:{ all -> 0x02d4 }
            int r4 = r4.a()     // Catch:{ all -> 0x02d4 }
            if (r1 == r4) goto L_0x0221
            com.GalaxyLaser.b.b r1 = r13.s     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.n r5 = r13.h     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.c.a r5 = r5.g()     // Catch:{ all -> 0x02d4 }
            int r5 = r5.h()     // Catch:{ all -> 0x02d4 }
            r1.a(r5, r4)     // Catch:{ all -> 0x02d4 }
            r1 = r4
        L_0x0221:
            com.GalaxyLaser.b.h r4 = r13.t     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x022f
            com.GalaxyLaser.b.h r4 = r13.t     // Catch:{ all -> 0x02d4 }
            int r5 = r13.y     // Catch:{ all -> 0x02d4 }
            r4.a(r5)     // Catch:{ all -> 0x02d4 }
            r4 = 0
            r13.y = r4     // Catch:{ all -> 0x02d4 }
        L_0x022f:
            com.GalaxyLaser.a.b r4 = r13.m     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x0242
            com.GalaxyLaser.a.b r4 = r13.m     // Catch:{ all -> 0x02d4 }
            r4.a()     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.b r4 = r13.m     // Catch:{ all -> 0x02d4 }
            r4.b()     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.b r4 = r13.m     // Catch:{ all -> 0x02d4 }
            r4.a(r3)     // Catch:{ all -> 0x02d4 }
        L_0x0242:
            com.GalaxyLaser.a.b r4 = r13.m     // Catch:{ all -> 0x02d4 }
            if (r4 == 0) goto L_0x0255
            com.GalaxyLaser.a.d r4 = r13.n     // Catch:{ all -> 0x02d4 }
            r4.a()     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.d r4 = r13.n     // Catch:{ all -> 0x02d4 }
            r4.b()     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.d r4 = r13.n     // Catch:{ all -> 0x02d4 }
            r4.a(r3)     // Catch:{ all -> 0x02d4 }
        L_0x0255:
            android.view.SurfaceHolder r4 = r13.b     // Catch:{ all -> 0x02d4 }
            r4.unlockCanvasAndPost(r3)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.a r3 = com.GalaxyLaser.a.a.a()     // Catch:{ all -> 0x02d4 }
            boolean r3 = r3.c()     // Catch:{ all -> 0x02d4 }
            if (r3 == 0) goto L_0x027c
            r3 = 0
            r13.C = r3     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.b.e r3 = r13.u     // Catch:{ all -> 0x02d4 }
            if (r3 == 0) goto L_0x0270
            com.GalaxyLaser.b.e r3 = r13.u     // Catch:{ all -> 0x02d4 }
            r3.e()     // Catch:{ all -> 0x02d4 }
        L_0x0270:
            com.GalaxyLaser.b.d r3 = r13.v     // Catch:{ all -> 0x02d4 }
            if (r3 == 0) goto L_0x027c
            com.GalaxyLaser.b.d r3 = r13.v     // Catch:{ all -> 0x02d4 }
            r3.h()     // Catch:{ all -> 0x02d4 }
            r3 = 0
            r13.v = r3     // Catch:{ all -> 0x02d4 }
        L_0x027c:
            int r3 = r13.C     // Catch:{ all -> 0x02d4 }
            int r3 = r3 + 1
            r13.C = r3     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.a r3 = com.GalaxyLaser.a.a.a()     // Catch:{ all -> 0x02d4 }
            int r4 = r13.D     // Catch:{ all -> 0x02d4 }
            int r3 = r3.a(r4)     // Catch:{ all -> 0x02d4 }
            int r4 = r13.C     // Catch:{ all -> 0x02d4 }
            int r4 = r4 / 30
            if (r3 >= r4) goto L_0x02ad
            boolean r3 = r13.A     // Catch:{ all -> 0x02d4 }
            if (r3 != 0) goto L_0x02ad
            r3 = 1
            r13.A = r3     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.b.e r3 = r13.u     // Catch:{ all -> 0x02d4 }
            if (r3 == 0) goto L_0x02a2
            com.GalaxyLaser.b.e r3 = r13.u     // Catch:{ all -> 0x02d4 }
            r3.e()     // Catch:{ all -> 0x02d4 }
        L_0x02a2:
            android.content.Context r3 = r13.f72a     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.k r3 = com.GalaxyLaser.a.k.a(r3)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.util.d r4 = com.GalaxyLaser.util.d.Boss     // Catch:{ all -> 0x02d4 }
            r3.a(r4)     // Catch:{ all -> 0x02d4 }
        L_0x02ad:
            int r3 = r13.C     // Catch:{ all -> 0x02d4 }
            r4 = 60
            if (r3 <= r4) goto L_0x02bf
            com.GalaxyLaser.b.g r3 = r13.x     // Catch:{ all -> 0x02d4 }
            if (r3 == 0) goto L_0x02bf
            com.GalaxyLaser.b.g r3 = r13.x     // Catch:{ all -> 0x02d4 }
            r3.j()     // Catch:{ all -> 0x02d4 }
            r3 = 0
            r13.x = r3     // Catch:{ all -> 0x02d4 }
        L_0x02bf:
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x02d4 }
            boolean r3 = r0.b(r3)     // Catch:{ all -> 0x02d4 }
            if (r3 == 0) goto L_0x041d
        L_0x02c9:
            com.GalaxyLaser.b.a r3 = r13.r     // Catch:{ all -> 0x02d4 }
            if (r3 == 0) goto L_0x0021
            com.GalaxyLaser.b.a r3 = r13.r     // Catch:{ all -> 0x02d4 }
            r3.a()     // Catch:{ all -> 0x02d4 }
            goto L_0x0021
        L_0x02d4:
            r0 = move-exception
            monitor-exit(r13)
            throw r0
        L_0x02d7:
            com.GalaxyLaser.util.e r5 = r13.q     // Catch:{ all -> 0x02d4 }
            r6 = 80
            int r5 = r5.nextInt(r6)     // Catch:{ all -> 0x02d4 }
            goto L_0x0070
        L_0x02e1:
            if (r11 < r4) goto L_0x02ec
            com.GalaxyLaser.util.e r4 = r13.q     // Catch:{ all -> 0x02d4 }
            r5 = 3
            int r4 = r4.nextInt(r5)     // Catch:{ all -> 0x02d4 }
            goto L_0x0091
        L_0x02ec:
            r5 = 3
            if (r5 < r4) goto L_0x02f8
            com.GalaxyLaser.util.e r4 = r13.q     // Catch:{ all -> 0x02d4 }
            r5 = 4
            int r4 = r4.nextInt(r5)     // Catch:{ all -> 0x02d4 }
            goto L_0x0091
        L_0x02f8:
            com.GalaxyLaser.util.e r4 = r13.q     // Catch:{ all -> 0x02d4 }
            r5 = 6
            int r4 = r4.nextInt(r5)     // Catch:{ all -> 0x02d4 }
            goto L_0x0091
        L_0x0301:
            com.GalaxyLaser.c.ad r4 = new com.GalaxyLaser.c.ad     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x02d4 }
            goto L_0x0095
        L_0x030c:
            com.GalaxyLaser.c.af r4 = new com.GalaxyLaser.c.af     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x02d4 }
            goto L_0x0095
        L_0x0317:
            com.GalaxyLaser.c.ag r4 = new com.GalaxyLaser.c.ag     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.a.n r7 = r13.h     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.c.a r7 = r7.g()     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6, r7)     // Catch:{ all -> 0x02d4 }
            goto L_0x0095
        L_0x0328:
            com.GalaxyLaser.c.ah r4 = new com.GalaxyLaser.c.ah     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x02d4 }
            goto L_0x0095
        L_0x0333:
            com.GalaxyLaser.c.ai r4 = new com.GalaxyLaser.c.ai     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x02d4 }
            goto L_0x0095
        L_0x033e:
            com.GalaxyLaser.c.aj r4 = new com.GalaxyLaser.c.aj     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x02d4 }
            goto L_0x0095
        L_0x0349:
            com.GalaxyLaser.c.bc r4 = new com.GalaxyLaser.c.bc     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x02d4 }
            r4.a(r13)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.b.e r5 = r13.u     // Catch:{ all -> 0x02d4 }
            if (r5 == 0) goto L_0x00da
            com.GalaxyLaser.b.e r5 = r13.u     // Catch:{ all -> 0x02d4 }
            r6 = 2130968578(0x7f040002, float:1.7545814E38)
            r5.b(r6)     // Catch:{ all -> 0x02d4 }
            goto L_0x00da
        L_0x0363:
            com.GalaxyLaser.c.bd r4 = new com.GalaxyLaser.c.bd     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x02d4 }
            r4.a(r13)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.b.e r5 = r13.u     // Catch:{ all -> 0x02d4 }
            if (r5 == 0) goto L_0x00da
            com.GalaxyLaser.b.e r5 = r13.u     // Catch:{ all -> 0x02d4 }
            r6 = 2130968578(0x7f040002, float:1.7545814E38)
            r5.b(r6)     // Catch:{ all -> 0x02d4 }
            goto L_0x00da
        L_0x037d:
            com.GalaxyLaser.c.bb r4 = new com.GalaxyLaser.c.bb     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x02d4 }
            r4.a(r13)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.b.e r5 = r13.u     // Catch:{ all -> 0x02d4 }
            if (r5 == 0) goto L_0x00da
            com.GalaxyLaser.b.e r5 = r13.u     // Catch:{ all -> 0x02d4 }
            r6 = 2130968578(0x7f040002, float:1.7545814E38)
            r5.b(r6)     // Catch:{ all -> 0x02d4 }
            goto L_0x00da
        L_0x0397:
            com.GalaxyLaser.c.ay r4 = new com.GalaxyLaser.c.ay     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x02d4 }
            r4.a(r13)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.b.e r5 = r13.u     // Catch:{ all -> 0x02d4 }
            if (r5 == 0) goto L_0x00da
            com.GalaxyLaser.b.e r5 = r13.u     // Catch:{ all -> 0x02d4 }
            r6 = 2130968578(0x7f040002, float:1.7545814E38)
            r5.b(r6)     // Catch:{ all -> 0x02d4 }
            goto L_0x00da
        L_0x03b1:
            com.GalaxyLaser.c.az r4 = new com.GalaxyLaser.c.az     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x02d4 }
            r4.a(r13)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.b.e r5 = r13.u     // Catch:{ all -> 0x02d4 }
            if (r5 == 0) goto L_0x00da
            com.GalaxyLaser.b.e r5 = r13.u     // Catch:{ all -> 0x02d4 }
            r6 = 2130968578(0x7f040002, float:1.7545814E38)
            r5.b(r6)     // Catch:{ all -> 0x02d4 }
            goto L_0x00da
        L_0x03cb:
            com.GalaxyLaser.c.ax r4 = new com.GalaxyLaser.c.ax     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x02d4 }
            r4.a(r13)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.b.e r5 = r13.u     // Catch:{ all -> 0x02d4 }
            if (r5 == 0) goto L_0x00da
            com.GalaxyLaser.b.e r5 = r13.u     // Catch:{ all -> 0x02d4 }
            r6 = 2130968578(0x7f040002, float:1.7545814E38)
            r5.b(r6)     // Catch:{ all -> 0x02d4 }
            goto L_0x00da
        L_0x03e5:
            com.GalaxyLaser.c.al r4 = new com.GalaxyLaser.c.al     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.util.i r5 = com.GalaxyLaser.util.i.Rock1     // Catch:{ all -> 0x02d4 }
            r4.a(r5)     // Catch:{ all -> 0x02d4 }
            goto L_0x00fe
        L_0x03f5:
            boolean r4 = r13.A     // Catch:{ all -> 0x02d4 }
            if (r4 != 0) goto L_0x00fd
            com.GalaxyLaser.c.ak r4 = new com.GalaxyLaser.c.ak     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.util.i r5 = com.GalaxyLaser.util.i.Rock2     // Catch:{ all -> 0x02d4 }
            r4.a(r5)     // Catch:{ all -> 0x02d4 }
            goto L_0x00fe
        L_0x0409:
            boolean r4 = r13.A     // Catch:{ all -> 0x02d4 }
            if (r4 != 0) goto L_0x00fd
            com.GalaxyLaser.c.am r4 = new com.GalaxyLaser.c.am     // Catch:{ all -> 0x02d4 }
            android.graphics.Rect r5 = r13.d     // Catch:{ all -> 0x02d4 }
            android.content.Context r6 = r13.f72a     // Catch:{ all -> 0x02d4 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x02d4 }
            com.GalaxyLaser.util.i r5 = com.GalaxyLaser.util.i.Rock2     // Catch:{ all -> 0x02d4 }
            r4.a(r5)     // Catch:{ all -> 0x02d4 }
            goto L_0x00fe
        L_0x041d:
            r3 = 1
            java.lang.Thread.sleep(r3)     // Catch:{ InterruptedException -> 0x0424 }
            goto L_0x02bf
        L_0x0424:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x02d4 }
            goto L_0x02bf
        L_0x042a:
            r2 = r1
            r1 = r3
            goto L_0x0021
        L_0x042e:
            r1 = r3
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.GalaxyLaser.view.StageView.g():void");
    }

    private void h() {
        if (this.u == null) {
            return;
        }
        if (this.A) {
            this.u.b(C0000R.raw.boss);
        } else if (this.E) {
            this.u.b(C0000R.raw.stage4);
        } else {
            this.u.b(C0000R.raw.stage1);
        }
    }

    public final void a() {
        if (this.u != null) {
            this.u.e();
            m.a(this.f72a).d();
        }
        this.j.c();
        this.i.c();
        this.l.c();
        this.k.c();
        this.h.c();
        this.n.c();
        this.m.c();
        this.o.c();
        i.f();
        com.GalaxyLaser.a.f.a(this.f72a).a();
        k.a(this.f72a).a();
        if (this.w != null) {
            this.w.k();
        }
    }

    public final void a(int i2) {
        this.D = i2;
        this.q.setSeed((long) this.D);
    }

    public final void a(a aVar) {
        this.r = aVar;
    }

    public final void a(com.GalaxyLaser.b.b bVar) {
        this.s = bVar;
    }

    public final void a(com.GalaxyLaser.b.d dVar) {
        this.v = dVar;
    }

    public final void a(com.GalaxyLaser.b.e eVar) {
        this.u = eVar;
    }

    public final void a(f fVar) {
        this.w = fVar;
    }

    public final void a(com.GalaxyLaser.b.g gVar) {
        this.x = gVar;
    }

    public final void a(com.GalaxyLaser.b.h hVar) {
        this.t = hVar;
    }

    public final void a(boolean z2) {
        this.E = z2;
        com.GalaxyLaser.a.a.a().b(this.E);
        if (this.E) {
            this.D += 6;
            this.c = BitmapFactory.decodeResource(this.f, C0000R.drawable.game_background_nightmare);
            this.e = new Rect(0, 0, this.c.getWidth(), this.c.getHeight());
        }
    }

    public final void b() {
        this.A = false;
        this.B = false;
        this.C = 0;
        this.D++;
        this.q.setSeed((long) this.D);
        if (6 < this.D) {
            this.p.b();
        }
        h();
    }

    public final void b(boolean z2) {
        this.F = z2;
    }

    public final void c() {
        this.z = null;
        if (this.u != null) {
            this.u.g();
        }
    }

    public final void d() {
        if (this.u != null) {
            this.u.f();
        }
        this.z = new Thread(new a(this));
        this.z.start();
    }

    public final int e() {
        return this.E ? this.D - 6 : this.D;
    }

    public final boolean f() {
        return this.F;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        try {
            Thread.sleep(16);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (this.h == null) {
            return super.onTouchEvent(motionEvent);
        }
        if (com.GalaxyLaser.a.a.a().c()) {
            this.h.b(0);
            return super.onTouchEvent(motionEvent);
        }
        int h2 = this.h.h();
        com.GalaxyLaser.util.a d2 = this.h.d();
        com.GalaxyLaser.util.a b2 = this.h.b();
        switch (motionEvent.getAction()) {
            case 0:
                b2.f57a = (int) motionEvent.getX();
                b2.b = (int) motionEvent.getY();
                this.h.b(b2);
                this.h.a(true);
                return true;
            case 1:
                b2.f57a = 0;
                b2.b = 0;
                this.h.b(b2);
                int i2 = (this.E ? 16 : 0) | 1;
                this.h.b(h2 > 0 ? i2 | 8 : i2);
                this.h.a(false);
                return true;
            case 2:
                com.GalaxyLaser.util.a aVar = new com.GalaxyLaser.util.a(((int) motionEvent.getX()) - b2.f57a, ((int) motionEvent.getY()) - b2.b);
                int i3 = d2.f57a + aVar.f57a;
                int i4 = d2.b + aVar.b;
                int i5 = this.E ? 16 : 0;
                int i6 = aVar.f57a < -3 ? i5 | 2 : aVar.f57a > 3 ? i5 | 4 : i5 | 1;
                this.h.b(h2 > 0 ? i6 | 8 : i6);
                int i7 = i3 < 0 ? 0 : i3;
                if (this.h.e().f59a + i7 > getWidth()) {
                    i7 = getWidth() - this.h.e().f59a;
                }
                int i8 = i4 < 0 ? 0 : i4;
                if (this.h.e().b + i8 > getHeight()) {
                    i8 = getHeight() - this.h.e().b;
                }
                d2.f57a = i7;
                d2.b = i8;
                this.h.a(d2);
                b2.f57a = (int) motionEvent.getX();
                b2.b = (int) motionEvent.getY();
                this.h.b(b2);
                return true;
            default:
                return super.onTouchEvent(motionEvent);
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.b = surfaceHolder;
        com.GalaxyLaser.c.a aVar = new com.GalaxyLaser.c.a(this.f72a);
        int i2 = 9;
        if (this.D >= 6) {
            aVar.c(4);
            aVar.a(14);
            this.h.a(6);
        } else if (this.D >= 5) {
            aVar.c(3);
            aVar.a(7);
            this.h.a(7);
        } else if (this.D >= 4) {
            aVar.c(2);
            aVar.a(3);
            this.h.a(8);
        } else {
            aVar.a(1);
            aVar.c(1);
            i2 = 1;
        }
        if (this.E) {
            i2 |= 16;
            aVar.c(aVar.h() + 2);
        }
        aVar.b(i2);
        aVar.a(new com.GalaxyLaser.util.a((getWidth() - aVar.f().f59a) / 2, (getHeight() * 3) / 4));
        this.h.a(aVar);
        if (this.s != null) {
            this.s.a(this.h.h(), this.h.a());
        }
        if (this.t != null) {
            this.t.a(0);
        }
        while (this.g.a()) {
            this.g.a(new aw(new com.GalaxyLaser.util.a(this.q.nextInt(getWidth()), this.q.nextInt(getHeight()))));
        }
        while (this.o.a()) {
            com.GalaxyLaser.util.a aVar2 = new com.GalaxyLaser.util.a(this.q.nextInt(getWidth()), this.q.nextInt(getHeight()));
            this.o.a(this.E ? new x(aVar2, this.f72a) : new z(aVar2, this.f72a));
        }
        while (this.k.a()) {
            this.k.a((com.GalaxyLaser.c.g) new an(new com.GalaxyLaser.util.a(this.q.nextInt(getWidth()), this.q.nextInt(getHeight())), this.f72a));
        }
        h();
        if (this.x != null) {
            this.x.i();
        }
        d();
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        c();
    }
}
