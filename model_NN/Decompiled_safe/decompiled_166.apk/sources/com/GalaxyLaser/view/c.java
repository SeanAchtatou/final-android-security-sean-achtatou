package com.GalaxyLaser.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.GalaxyLaser.opening.d;
import com.GalaxyLaser.util.j;
import com.GalaxyLaser.util.l;

public final class c extends SurfaceView implements SurfaceHolder.Callback, Runnable {

    /* renamed from: a  reason: collision with root package name */
    private SurfaceHolder f74a;
    private Thread b;
    private Context c;
    private l d;
    private l e;
    private l f;
    private l g;
    private l h;
    private l i;
    private l j;
    private Paint k;
    private Rect l;
    private d m;
    private boolean n = false;
    private boolean o = false;
    private b p;

    public c(Context context, d dVar) {
        super(context);
        this.c = context;
        this.f74a = getHolder();
        this.f74a.addCallback(this);
        this.f74a.setFixedSize(getWidth(), getHeight());
        this.m = dVar;
        setFocusable(true);
    }

    public final void a() {
        if (this.b != null) {
            this.b = null;
        }
        System.gc();
    }

    public final void a(b bVar) {
        this.p = bVar;
    }

    public final synchronized boolean onTouchEvent(MotionEvent motionEvent) {
        return motionEvent.getAction() == 0;
    }

    public final void run() {
        Canvas canvas = null;
        while (this.b != null) {
            try {
                if (this.n) {
                    this.d = this.m.b;
                    this.e = this.m.c;
                    this.f = this.m.d;
                    this.g = this.m.e;
                    this.h = this.m.f;
                    this.i = this.m.g;
                    this.j = this.m.h;
                    this.k = this.m.i;
                    this.o = true;
                    this.n = false;
                }
                canvas = this.f74a.lockCanvas();
                if (this.o && this.m.f39a) {
                    this.d.a(canvas);
                    this.e.a(canvas);
                    this.f.a(canvas);
                    this.g.a(canvas);
                    this.h.a(canvas);
                    this.i.a(canvas);
                    this.j.a(canvas, this.k);
                }
                if (canvas != null) {
                    this.f74a.unlockCanvasAndPost(canvas);
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                Canvas canvas2 = canvas;
                Throwable th3 = th2;
                if (canvas2 != null) {
                    this.f74a.unlockCanvasAndPost(canvas2);
                }
                throw th3;
            }
        }
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (this.l == null) {
            this.l = new Rect(getLeft(), getTop(), getRight(), getBottom());
        }
        getContext();
        j.a().a(this.l);
        this.m.a(this.l);
        this.b = new Thread(this);
        this.b.start();
        this.n = true;
    }

    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.b = null;
    }
}
