package com.GalaxyLaser;

import android.content.Intent;
import android.widget.Toast;
import jp.a.a.a.a.b;

final class n implements b {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GameActivity f33a;

    n(GameActivity gameActivity) {
        this.f33a = gameActivity;
    }

    public final void a(boolean z) {
        (z ? Toast.makeText(this.f33a, "Score entry completion.", 1) : Toast.makeText(this.f33a, "Score entry failure.", 1)).show();
        Intent intent = new Intent();
        intent.putExtra("Stage", this.f33a.j);
        this.f33a.setResult(-1, intent);
        this.f33a.finish();
    }
}
