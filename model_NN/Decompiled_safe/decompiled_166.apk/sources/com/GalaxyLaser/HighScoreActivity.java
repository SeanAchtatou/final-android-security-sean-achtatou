package com.GalaxyLaser;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import com.GalaxyLaser.a.i;

public class HighScoreActivity extends HighScoreBaseActivity {
    private ImageButton b;
    private ImageButton c;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.hiscore);
        this.b = (ImageButton) findViewById(C0000R.id.link_vsboss);
        this.b.setOnClickListener(new i(this));
        this.c = (ImageButton) findViewById(C0000R.id.link_act2);
        this.c.setOnClickListener(new h(this));
        boolean a2 = i.a(getApplication().getApplicationContext()).a();
        if (a2) {
            ((TextView) findViewById(C0000R.id.title)).setText((int) C0000R.string.normal);
        }
        ((Button) findViewById(C0000R.id.ok)).setOnClickListener(new g(this, a2));
        a(30);
    }
}
