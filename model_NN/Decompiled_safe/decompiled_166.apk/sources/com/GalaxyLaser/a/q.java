package com.GalaxyLaser.a;

import android.content.Context;
import android.graphics.Rect;
import com.GalaxyLaser.c.g;
import com.GalaxyLaser.util.c;
import com.GalaxyLaser.util.f;

public abstract class q {

    /* renamed from: a  reason: collision with root package name */
    protected g[] f12a = new g[100];
    protected Context b;

    public q(Context context) {
        this.b = context;
        for (int i = 0; i < this.f12a.length; i++) {
            this.f12a[i] = null;
        }
    }

    public void a(Rect rect) {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                g gVar = this.f12a[i];
                if (gVar != null) {
                    c f = gVar.f();
                    if (gVar.e().f57a + f.f59a <= (-f.f59a) || rect.right <= gVar.e().f57a || gVar.e().b + f.b <= (-f.b) || rect.bottom <= gVar.e().b) {
                        this.f12a[i] = null;
                    }
                }
            }
        }
    }

    public final void a(g gVar) {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                if (this.f12a[i] == null) {
                    this.f12a[i] = gVar;
                    return;
                }
            }
        }
    }

    public final void b(f fVar) {
        f.a(this.b).a(fVar);
    }

    public void c() {
        for (int i = 0; i < this.f12a.length; i++) {
            this.f12a[i] = null;
        }
    }

    public final int i() {
        if (this.f12a == null) {
            return 0;
        }
        return this.f12a.length;
    }
}
