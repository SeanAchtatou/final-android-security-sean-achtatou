package com.GalaxyLaser.a;

import android.media.MediaPlayer;

final class e implements MediaPlayer.OnPreparedListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ m f6a;
    private final /* synthetic */ boolean b;

    e(m mVar, boolean z) {
        this.f6a = mVar;
        this.b = z;
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.setLooping(this.b);
        mediaPlayer.start();
    }
}
