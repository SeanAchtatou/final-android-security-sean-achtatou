package com.GalaxyLaser.a;

import android.content.Context;
import android.graphics.Canvas;
import com.GalaxyLaser.util.a;
import com.GalaxyLaser.util.c;

public final class n extends q {
    private static n d = null;
    private a c = new a();
    private int e = 0;
    private int f = 1;
    private int g = 9;
    private boolean h = false;

    private n(Context context) {
        super(context);
    }

    public static n a(Context context) {
        if (d == null) {
            d = new n(context);
        }
        return d;
    }

    public final int a() {
        return this.g;
    }

    public final void a(int i) {
        int i2 = 9;
        if (i <= 9) {
            i2 = i;
        }
        if (i2 < 4) {
            i2 = 4;
        }
        this.g = i2;
    }

    public final void a(Canvas canvas) {
        com.GalaxyLaser.c.a aVar;
        if (this.f12a != null && (aVar = (com.GalaxyLaser.c.a) this.f12a[0]) != null) {
            aVar.a(canvas);
            this.e++;
        }
    }

    public final void a(a aVar) {
        com.GalaxyLaser.c.a aVar2;
        if (this.f12a != null && (aVar2 = (com.GalaxyLaser.c.a) this.f12a[0]) != null) {
            aVar2.a(aVar);
        }
    }

    public final void a(boolean z) {
        this.h = z;
    }

    public final a b() {
        return this.c;
    }

    public final void b(int i) {
        if (this.f12a != null) {
            com.GalaxyLaser.c.a aVar = (com.GalaxyLaser.c.a) this.f12a[0];
            if (this.f12a[0] != null) {
                aVar.b(i);
            }
        }
    }

    public final void b(a aVar) {
        this.c = aVar;
    }

    public final void c() {
        super.c();
        if (d != null) {
            d = null;
        }
    }

    public final a d() {
        if (this.f12a == null) {
            return null;
        }
        com.GalaxyLaser.c.a aVar = (com.GalaxyLaser.c.a) this.f12a[0];
        if (this.f12a[0] == null) {
            return null;
        }
        return aVar.e();
    }

    public final c e() {
        if (this.f12a == null) {
            return new c(0, 0);
        }
        com.GalaxyLaser.c.a aVar = (com.GalaxyLaser.c.a) this.f12a[0];
        if (this.f12a[0] == null) {
            return null;
        }
        return aVar.f();
    }

    public final boolean f() {
        if (a.a().c()) {
            return false;
        }
        if (!this.h || this.e % this.g != 0) {
            return false;
        }
        this.e = 0;
        return true;
    }

    public final com.GalaxyLaser.c.a g() {
        if (this.f12a == null) {
            return null;
        }
        return (com.GalaxyLaser.c.a) this.f12a[0];
    }

    public final int h() {
        if (this.f12a == null) {
            return 0;
        }
        com.GalaxyLaser.c.a aVar = (com.GalaxyLaser.c.a) this.f12a[0];
        if (this.f12a[0] == null) {
            return 0;
        }
        return aVar.h();
    }
}
