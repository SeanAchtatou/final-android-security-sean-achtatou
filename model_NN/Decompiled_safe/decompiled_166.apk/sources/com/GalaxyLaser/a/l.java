package com.GalaxyLaser.a;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Vibrator;
import com.GalaxyLaser.c.a;
import com.GalaxyLaser.c.as;
import com.GalaxyLaser.c.d;
import com.GalaxyLaser.c.g;
import com.GalaxyLaser.c.o;
import com.GalaxyLaser.util.f;
import com.GalaxyLaser.util.h;

public final class l extends q {
    private static l c = null;

    private l(Context context) {
        super(context);
    }

    public static l a(Context context) {
        if (c == null) {
            c = new l(context);
        }
        return c;
    }

    public final void a(int i) {
        if (this.f12a != null) {
            try {
                this.f12a[i] = null;
            } catch (IndexOutOfBoundsException e) {
            }
        }
    }

    public final void a(Canvas canvas) {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                if (this.f12a[i] != null) {
                    this.f12a[i].a(canvas);
                }
            }
        }
    }

    public final void a(f fVar) {
        super.b(fVar);
    }

    public final boolean a() {
        if (this.f12a == null) {
            return false;
        }
        int i = 0;
        int i2 = 0;
        while (i < this.f12a.length) {
            int i3 = (this.f12a[i] == null || ((as) this.f12a[i]).a()) ? i2 : i2 + 1;
            i++;
            i2 = i3;
        }
        return 3 > i2;
    }

    public final boolean a(n nVar) {
        if (this.f12a == null || nVar == null) {
            return false;
        }
        if (a.a().c()) {
            return false;
        }
        a g = nVar.g();
        if (g == null) {
            return false;
        }
        for (int i = 0; i < this.f12a.length; i++) {
            as asVar = (as) this.f12a[i];
            if (asVar != null && asVar.a()) {
                com.GalaxyLaser.util.a e = asVar.e();
                if (h.a(e, asVar.f(), g.e(), g.f())) {
                    g.d(-1);
                    if (g.h() < 0) {
                        f.a(this.b).a(f.Ally_Destroy);
                        a.a().a(true);
                        this.b.getResources();
                        nVar.b(0);
                        b.a(this.b).a(new o(g.e(), this.b));
                        return true;
                    }
                    b.a(this.b).a(new d(e, this.b));
                    if (g.h() == 0) {
                        int i2 = a.a().d() ? 16 : 0;
                        this.b.getResources();
                        nVar.b(i2 | 1);
                    }
                    ((Vibrator) this.b.getSystemService("vibrator")).vibrate(100);
                    asVar.d(-1);
                    if (1 > asVar.h()) {
                        this.f12a[i] = null;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public final boolean a(as asVar) {
        if (this.f12a == null) {
            return false;
        }
        for (int i = 0; i < this.f12a.length; i++) {
            if (this.f12a[i] != null) {
                as asVar2 = (as) this.f12a[i];
                if (h.a(asVar.e(), asVar.f(), asVar2.e(), asVar2.f())) {
                    return true;
                }
            }
        }
        return false;
    }

    public final g b(int i) {
        if (this.f12a == null) {
            return null;
        }
        try {
            return this.f12a[i];
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public final void b() {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                if (this.f12a[i] != null) {
                    this.f12a[i].b();
                }
            }
        }
    }

    public final void c() {
        super.c();
        if (c != null) {
            c = null;
        }
    }
}
