package com.GalaxyLaser.a;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import com.GalaxyLaser.util.d;
import java.util.HashMap;
import java.util.Locale;

public final class k implements TextToSpeech.OnInitListener {

    /* renamed from: a  reason: collision with root package name */
    private static k f10a = null;
    private Context b;
    private TextToSpeech c;
    private boolean d;
    private HashMap e;

    private k(Context context) {
        this.b = context;
        this.d = false;
        this.e = new HashMap();
        this.c = null;
        this.c = new TextToSpeech(this.b, this);
        this.e.put(d.DoubleShot, "Multiple Shot");
        this.e.put(d.BackShot, "Back Shot");
        this.e.put(d.PowerShot, "Power Shot");
        this.e.put(d.SpeedUp, "Speed Up");
        this.e.put(d.Shield, "Shield");
        this.e.put(d.Warning, "Warning");
        this.e.put(d.Boss, "Warning. Warning. An unknown spaceship is approaching fast.");
        this.e.put(d.GameStart, "We are currently in a state of emergency.");
        this.e.put(d.Nightmare, "Nightmare stage is unlocked");
        this.e.put(d.ShieldBroken, "Shield is broken!");
        this.e.put(d.Black, "Oh, No!");
    }

    public static k a(Context context) {
        if (f10a == null) {
            f10a = new k(context);
        }
        return f10a;
    }

    public final void a() {
        if (this.c != null) {
            this.c.shutdown();
            this.c = null;
        }
        if (f10a != null) {
            f10a = null;
        }
    }

    public final void a(d dVar) {
        String str;
        if (this.d && (str = (String) this.e.get(dVar)) != null) {
            this.c.speak(str, 0, null);
        }
    }

    public final void onInit(int i) {
        if (this.c != null && i == 0) {
            Locale locale = Locale.US;
            if (this.c.isLanguageAvailable(locale) >= 0) {
                this.c.setLanguage(locale);
                this.d = true;
                this.c.speak("", 0, null);
            }
        }
    }
}
