package com.GalaxyLaser.a;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import com.GalaxyLaser.C0000R;
import java.util.HashMap;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static f f7a = null;
    private Context b;
    private SoundPool c;
    private HashMap d = new HashMap();
    private int[] e;
    private int[] f;
    private AudioManager g;
    private boolean h;

    private f(Context context) {
        this.b = context;
        this.d.put(com.GalaxyLaser.util.f.Shot, Integer.valueOf((int) C0000R.raw.shot));
        this.d.put(com.GalaxyLaser.util.f.Enemy_Destroy, Integer.valueOf((int) C0000R.raw.hit_se));
        this.d.put(com.GalaxyLaser.util.f.Meteor_Destroy, Integer.valueOf((int) C0000R.raw.baku));
        this.d.put(com.GalaxyLaser.util.f.Star_Item, Integer.valueOf((int) C0000R.raw.star));
        this.d.put(com.GalaxyLaser.util.f.Ally_Destroy, Integer.valueOf((int) C0000R.raw.ally_explode));
        this.d.put(com.GalaxyLaser.util.f.Boss_Destroy, Integer.valueOf((int) C0000R.raw.boss_explode));
        this.d.put(com.GalaxyLaser.util.f.Title_SE, Integer.valueOf((int) C0000R.raw.se_title));
        this.d.put(com.GalaxyLaser.util.f.Hit, Integer.valueOf((int) C0000R.raw.hit_se));
        this.d.put(com.GalaxyLaser.util.f.Powerup, Integer.valueOf((int) C0000R.raw.powerup));
        this.d.put(com.GalaxyLaser.util.f.Flash, Integer.valueOf((int) C0000R.raw.flash));
        this.e = new int[com.GalaxyLaser.util.f.Item_End.ordinal()];
        this.f = new int[com.GalaxyLaser.util.f.Item_End.ordinal()];
        this.c = new SoundPool(this.e.length, 3, 0);
        this.g = (AudioManager) context.getSystemService("audio");
        this.h = true;
        b();
    }

    public static f a(Context context) {
        if (f7a == null) {
            f7a = new f(context);
        }
        return f7a;
    }

    private void b() {
        for (Object next : this.d.keySet()) {
            this.e[((com.GalaxyLaser.util.f) next).ordinal()] = this.c.load(this.b, ((Integer) this.d.get(next)).intValue(), 1);
        }
    }

    public final void a() {
        for (Object obj : this.d.keySet()) {
            this.c.unload(((Integer) this.d.get(obj)).intValue());
        }
        this.c.release();
        this.c = null;
        f7a = null;
    }

    public final void a(com.GalaxyLaser.util.f fVar) {
        int ordinal = fVar.ordinal();
        try {
            if (this.h) {
                this.c.stop(this.f[ordinal]);
                float streamVolume = ((float) this.g.getStreamVolume(3)) / ((float) this.g.getStreamMaxVolume(3));
                this.f[ordinal] = this.c.play(this.e[ordinal], streamVolume, streamVolume, 1, 0, 1.0f);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
