package com.GalaxyLaser.a;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Vibrator;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.c.a;
import com.GalaxyLaser.c.ap;
import com.GalaxyLaser.c.aq;
import com.GalaxyLaser.c.d;
import com.GalaxyLaser.c.g;
import com.GalaxyLaser.c.n;
import com.GalaxyLaser.c.o;
import com.GalaxyLaser.util.e;
import com.GalaxyLaser.util.f;
import com.GalaxyLaser.util.i;

public final class h extends q {
    private static h c = null;
    private static /* synthetic */ int[] f;
    private p d;
    private Context e;

    private h(Context context) {
        super(context);
        this.d = p.a(context);
        this.e = context;
    }

    public static h a(Context context) {
        if (c == null) {
            c = new h(context);
        }
        return c;
    }

    private static /* synthetic */ int[] d() {
        int[] iArr = f;
        if (iArr == null) {
            iArr = new int[i.values().length];
            try {
                iArr[i.Boss1.ordinal()] = 7;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[i.Boss2.ordinal()] = 8;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[i.Boss3.ordinal()] = 9;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[i.Boss4.ordinal()] = 10;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[i.Boss5.ordinal()] = 11;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[i.Boss6.ordinal()] = 12;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[i.Rock1.ordinal()] = 13;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[i.Rock2.ordinal()] = 14;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[i.Type1.ordinal()] = 1;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[i.Type2.ordinal()] = 2;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[i.Type3.ordinal()] = 3;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[i.Type4.ordinal()] = 4;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[i.Type5.ordinal()] = 5;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[i.Type6.ordinal()] = 6;
            } catch (NoSuchFieldError e15) {
            }
            f = iArr;
        }
        return iArr;
    }

    public final g a(int i) {
        if (this.f12a == null) {
            return null;
        }
        try {
            return this.f12a[i];
        } catch (IndexOutOfBoundsException e2) {
            return null;
        }
    }

    public final void a() {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                if (this.f12a[i] != null) {
                    this.f12a[i].b();
                }
            }
        }
    }

    public final void a(Canvas canvas) {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                if (this.f12a[i] != null) {
                    this.f12a[i].a(canvas);
                }
            }
            if (this.d != null) {
                this.d.a();
                this.d.a(canvas);
            }
        }
    }

    public final void a(Rect rect) {
        super.a(rect);
        this.d.a(rect);
    }

    public final void a(l lVar) {
        this.d.a(lVar);
    }

    public final void a(a aVar, Context context) {
        aq aqVar;
        if (this.f12a != null && this.d != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.f12a.length) {
                    if (this.f12a[i2] != null) {
                        int b = 200 - (a.a().b() * 15);
                        int i3 = b <= 5 ? 5 : b;
                        n nVar = (n) this.f12a[i2];
                        if (nVar.l() > e.a().nextInt(i3)) {
                            aq aqVar2 = new aq(aVar, nVar, context);
                            aqVar2.e(i2);
                            switch (d()[i.values()[nVar.a().ordinal()].ordinal()]) {
                                case 1:
                                    aqVar2.a(context.getResources(), C0000R.drawable.enemy_bullet1);
                                    aqVar = aqVar2;
                                    break;
                                case 2:
                                    aqVar2.a(context.getResources(), C0000R.drawable.enemy_bullet2);
                                    aqVar = aqVar2;
                                    break;
                                case 3:
                                    aqVar2.a(context.getResources(), C0000R.drawable.enemy_bullet3);
                                    aqVar = aqVar2;
                                    break;
                                case 4:
                                    aqVar2.a(context.getResources(), C0000R.drawable.enemy_bullet4);
                                    aqVar = aqVar2;
                                    break;
                                case 5:
                                    aqVar2.a(context.getResources(), C0000R.drawable.enemy_bullet5);
                                    aqVar = aqVar2;
                                    break;
                                case 6:
                                    aqVar2.a(context.getResources(), C0000R.drawable.enemy_bullet6);
                                    aqVar = aqVar2;
                                    break;
                                case 7:
                                    aqVar2.a(context.getResources(), C0000R.drawable.boss1_bullet);
                                    aqVar2.b(Integer.MAX_VALUE);
                                    aqVar2.e(-1);
                                    if (a.a().d()) {
                                        ap apVar = new ap(nVar, context);
                                        apVar.a(context.getResources(), C0000R.drawable.enemy_bullet);
                                        apVar.a(new com.GalaxyLaser.util.a((nVar.e().f57a + (nVar.f().f59a / 2)) - (apVar.f().f59a / 2), nVar.f().b + nVar.e().b));
                                        apVar.b(1);
                                        apVar.e(-1);
                                        this.d.a(apVar);
                                        aqVar = aqVar2;
                                        break;
                                    }
                                    aqVar = aqVar2;
                                    break;
                                case 8:
                                    ap apVar2 = new ap(nVar, context);
                                    apVar2.a(context.getResources(), C0000R.drawable.enemy_big1_bullet);
                                    apVar2.a(new com.GalaxyLaser.util.a((nVar.e().f57a + (nVar.f().f59a / 2)) - (apVar2.f().f59a / 2), nVar.f().b + nVar.e().b));
                                    apVar2.b(1);
                                    apVar2.e(-1);
                                    this.d.a(apVar2);
                                    if (!a.a().d()) {
                                        aqVar = null;
                                        break;
                                    } else {
                                        aqVar2.a(context.getResources(), C0000R.drawable.enemy_bullet);
                                        aqVar2.a(20);
                                        aqVar = aqVar2;
                                        break;
                                    }
                                case 9:
                                    ap apVar3 = new ap(nVar, context);
                                    apVar3.a(context.getResources(), C0000R.drawable.enemy_big2_bullet);
                                    apVar3.b(1);
                                    apVar3.e(-1);
                                    apVar3.a(new com.GalaxyLaser.util.a(nVar.e().f57a, nVar.e().b + nVar.f().b));
                                    this.d.a(apVar3);
                                    ap apVar4 = new ap(nVar, context);
                                    apVar4.a(context.getResources(), C0000R.drawable.enemy_big2_bullet);
                                    apVar4.b(1);
                                    apVar4.e(-1);
                                    apVar4.a(new com.GalaxyLaser.util.a((nVar.e().f57a + nVar.f().f59a) - apVar4.f().f59a, nVar.f().b + nVar.e().b));
                                    this.d.a(apVar4);
                                    if (!a.a().d()) {
                                        aqVar = null;
                                        break;
                                    } else {
                                        aqVar2.a(context.getResources(), C0000R.drawable.enemy_bullet);
                                        aqVar2.a(20);
                                        aqVar = aqVar2;
                                        break;
                                    }
                                case 10:
                                    ap apVar5 = new ap(nVar, context);
                                    apVar5.a(context.getResources(), C0000R.drawable.enemy_big3_bullet);
                                    apVar5.b(1);
                                    apVar5.e(-1);
                                    apVar5.c(-5);
                                    apVar5.a(new com.GalaxyLaser.util.a((nVar.e().f57a + (nVar.f().f59a / 2)) - (apVar5.f().f59a / 2), nVar.e().b + nVar.f().b));
                                    this.d.a(apVar5);
                                    ap apVar6 = new ap(nVar, context);
                                    apVar6.a(context.getResources(), C0000R.drawable.enemy_big3_bullet);
                                    apVar6.b(1);
                                    apVar6.e(-1);
                                    apVar6.c(0);
                                    apVar6.a(new com.GalaxyLaser.util.a((nVar.e().f57a + (nVar.f().f59a / 2)) - (apVar6.f().f59a / 2), nVar.e().b + nVar.f().b));
                                    this.d.a(apVar6);
                                    ap apVar7 = new ap(nVar, context);
                                    apVar7.a(context.getResources(), C0000R.drawable.enemy_big3_bullet);
                                    apVar7.b(1);
                                    apVar7.e(-1);
                                    apVar7.c(5);
                                    apVar7.a(new com.GalaxyLaser.util.a((nVar.e().f57a + (nVar.f().f59a / 2)) - (apVar7.f().f59a / 2), nVar.f().b + nVar.e().b));
                                    this.d.a(apVar7);
                                    if (!a.a().d()) {
                                        aqVar = null;
                                        break;
                                    } else {
                                        aqVar2.a(context.getResources(), C0000R.drawable.enemy_bullet);
                                        aqVar2.a(20);
                                        aqVar = aqVar2;
                                        break;
                                    }
                                case 11:
                                    ap apVar8 = new ap(nVar, context);
                                    apVar8.a(context.getResources(), C0000R.drawable.boss2_bullet);
                                    apVar8.b(1);
                                    apVar8.e(-1);
                                    apVar8.a(new com.GalaxyLaser.util.a(nVar.e().f57a, nVar.e().b + nVar.f().b));
                                    apVar8.b(Integer.MAX_VALUE);
                                    this.d.a(apVar8);
                                    ap apVar9 = new ap(nVar, context);
                                    apVar9.a(context.getResources(), C0000R.drawable.boss2_bullet);
                                    apVar9.b(1);
                                    apVar9.e(-1);
                                    apVar9.a(new com.GalaxyLaser.util.a((nVar.e().f57a + nVar.f().f59a) - apVar9.f().f59a, nVar.f().b + nVar.e().b));
                                    apVar9.b(Integer.MAX_VALUE);
                                    this.d.a(apVar9);
                                    aqVar2.a(context.getResources(), C0000R.drawable.enemy_bullet2);
                                    aqVar2.a(20);
                                    aqVar = aqVar2;
                                    break;
                                case 12:
                                    ap apVar10 = new ap(nVar, context);
                                    apVar10.a(context.getResources(), C0000R.drawable.boss3_bullet);
                                    apVar10.b(1);
                                    apVar10.e(-1);
                                    apVar10.c(-5);
                                    apVar10.a(new com.GalaxyLaser.util.a((nVar.e().f57a + (nVar.f().f59a / 2)) - (apVar10.f().f59a / 2), nVar.e().b + nVar.f().b));
                                    apVar10.b(Integer.MAX_VALUE);
                                    this.d.a(apVar10);
                                    ap apVar11 = new ap(nVar, context);
                                    apVar11.a(context.getResources(), C0000R.drawable.boss3_bullet);
                                    apVar11.b(1);
                                    apVar11.e(-1);
                                    apVar11.c(0);
                                    apVar11.a(new com.GalaxyLaser.util.a((nVar.e().f57a + (nVar.f().f59a / 2)) - (apVar11.f().f59a / 2), nVar.e().b + nVar.f().b));
                                    apVar11.b(Integer.MAX_VALUE);
                                    this.d.a(apVar11);
                                    ap apVar12 = new ap(nVar, context);
                                    apVar12.a(context.getResources(), C0000R.drawable.boss3_bullet);
                                    apVar12.b(1);
                                    apVar12.e(-1);
                                    apVar12.c(5);
                                    apVar12.a(new com.GalaxyLaser.util.a((nVar.e().f57a + (nVar.f().f59a / 2)) - (apVar12.f().f59a / 2), nVar.f().b + nVar.e().b));
                                    apVar12.b(Integer.MAX_VALUE);
                                    this.d.a(apVar12);
                                    if (!a.a().d()) {
                                        aqVar = null;
                                        break;
                                    } else {
                                        aqVar2.a(context.getResources(), C0000R.drawable.enemy_bullet);
                                        aqVar2.a(20);
                                        aqVar = aqVar2;
                                        break;
                                    }
                                default:
                                    aqVar2.a(context.getResources(), C0000R.drawable.enemy_bullet);
                                    aqVar = aqVar2;
                                    break;
                            }
                            if (aqVar != null) {
                                this.d.a(aqVar);
                            }
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public final void a(f fVar) {
        super.b(fVar);
    }

    public final boolean a(n nVar) {
        if (this.f12a == null || nVar == null) {
            return false;
        }
        if (a.a().c()) {
            return false;
        }
        a g = nVar.g();
        if (g == null) {
            return false;
        }
        for (int i = 0; i < this.f12a.length; i++) {
            n nVar2 = (n) this.f12a[i];
            if (nVar2 != null) {
                com.GalaxyLaser.util.a e2 = nVar2.e();
                if (com.GalaxyLaser.util.h.a(e2, nVar2.f(), g.e(), g.f())) {
                    g.d(-1);
                    if (g.h() < 0) {
                        f.a(this.e).a(f.Ally_Destroy);
                        a.a().a(true);
                        this.e.getResources();
                        nVar.b(0);
                        b.a(this.e).a(new o(g.e(), this.e));
                        return true;
                    }
                    b.a(this.e).a(new d(e2, this.e));
                    if (g.h() == 0) {
                        int i2 = a.a().d() ? 16 : 0;
                        this.e.getResources();
                        nVar.b(i2 | 1);
                    }
                    ((Vibrator) this.e.getSystemService("vibrator")).vibrate(100);
                    nVar2.d(-1);
                    if (1 > nVar2.h()) {
                        this.f12a[i] = null;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public final p b() {
        return this.d;
    }

    public final void b(int i) {
        if (this.f12a != null) {
            try {
                this.f12a[i] = null;
            } catch (IndexOutOfBoundsException e2) {
            }
        }
    }

    public final boolean b(n nVar) {
        return this.d.a(nVar);
    }

    public final void c() {
        super.c();
        this.d.c();
        if (c != null) {
            c = null;
        }
    }
}
