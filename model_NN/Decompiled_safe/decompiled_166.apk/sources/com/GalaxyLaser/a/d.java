package com.GalaxyLaser.a;

import android.content.Context;
import android.graphics.Canvas;
import com.GalaxyLaser.c.w;

public final class d extends q {
    private static d c = null;
    private Context d;

    private d(Context context) {
        super(context);
        this.d = context;
    }

    public static d a(Context context) {
        if (c == null) {
            c = new d(context);
        }
        return c;
    }

    public final void a() {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                if (this.f12a[i] != null) {
                    this.f12a[i].b();
                }
            }
        }
    }

    public final void a(Canvas canvas) {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                if (this.f12a[i] != null) {
                    this.f12a[i].a(canvas);
                }
            }
        }
    }

    public final void b() {
        if (this.f12a != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.f12a.length) {
                    w wVar = (w) this.f12a[i2];
                    if (wVar != null && !wVar.a()) {
                        this.f12a[i2] = null;
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public final void c() {
        super.c();
        if (c != null) {
            c = null;
        }
    }
}
