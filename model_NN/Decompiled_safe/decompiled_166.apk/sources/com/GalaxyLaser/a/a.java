package com.GalaxyLaser.a;

import java.util.HashMap;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f5a = null;
    private HashMap b = new HashMap();
    private int c;
    private boolean d;
    private boolean e;

    private a() {
        this.b.put(1, 60);
        this.b.put(2, 75);
        this.b.put(3, 75);
        this.b.put(4, 90);
        this.b.put(5, 120);
        this.b.put(6, 120);
        this.c = 1;
        this.e = false;
        this.d = false;
    }

    public static a a() {
        if (f5a == null) {
            f5a = new a();
        }
        return f5a;
    }

    public final int a(int i) {
        this.c = i;
        Integer num = (Integer) this.b.get(Integer.valueOf(((i - 1) % 6) + 1));
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    public final void a(boolean z) {
        this.e = z;
    }

    public final int b() {
        return this.c;
    }

    public final void b(boolean z) {
        this.d = z;
    }

    public final boolean c() {
        return this.e;
    }

    public final boolean d() {
        return this.d;
    }
}
