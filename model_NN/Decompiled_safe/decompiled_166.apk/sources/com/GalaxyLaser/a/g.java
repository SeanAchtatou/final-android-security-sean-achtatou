package com.GalaxyLaser.a;

import android.content.Context;
import android.graphics.Canvas;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.c.ab;
import com.GalaxyLaser.c.ac;
import com.GalaxyLaser.c.ae;
import com.GalaxyLaser.c.ao;
import com.GalaxyLaser.c.aq;
import com.GalaxyLaser.c.ar;
import com.GalaxyLaser.c.as;
import com.GalaxyLaser.c.at;
import com.GalaxyLaser.c.av;
import com.GalaxyLaser.c.b;
import com.GalaxyLaser.c.ba;
import com.GalaxyLaser.c.d;
import com.GalaxyLaser.c.f;
import com.GalaxyLaser.c.j;
import com.GalaxyLaser.c.k;
import com.GalaxyLaser.c.l;
import com.GalaxyLaser.c.m;
import com.GalaxyLaser.c.n;
import com.GalaxyLaser.c.p;
import com.GalaxyLaser.c.q;
import com.GalaxyLaser.c.r;
import com.GalaxyLaser.c.t;
import com.GalaxyLaser.c.v;
import com.GalaxyLaser.util.a;
import com.GalaxyLaser.util.c;
import com.GalaxyLaser.util.e;
import com.GalaxyLaser.util.h;
import com.GalaxyLaser.util.i;

public final class g extends q {
    private static g c = null;
    private static /* synthetic */ int[] f;
    private b d;
    private f e;

    private g(Context context) {
        super(context);
        this.d = b.a(context);
        this.e = f.a(context);
    }

    public static g a(Context context) {
        if (c == null) {
            c = new g(context);
        }
        return c;
    }

    private static /* synthetic */ int[] b() {
        int[] iArr = f;
        if (iArr == null) {
            iArr = new int[i.values().length];
            try {
                iArr[i.Boss1.ordinal()] = 7;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[i.Boss2.ordinal()] = 8;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[i.Boss3.ordinal()] = 9;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[i.Boss4.ordinal()] = 10;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[i.Boss5.ordinal()] = 11;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[i.Boss6.ordinal()] = 12;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[i.Rock1.ordinal()] = 13;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[i.Rock2.ordinal()] = 14;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[i.Type1.ordinal()] = 1;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[i.Type2.ordinal()] = 2;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[i.Type3.ordinal()] = 3;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[i.Type4.ordinal()] = 4;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[i.Type5.ordinal()] = 5;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[i.Type6.ordinal()] = 6;
            } catch (NoSuchFieldError e15) {
            }
            f = iArr;
        }
        return iArr;
    }

    public final int a(h hVar) {
        int i;
        f fVar;
        int i2 = 0;
        if (this.f12a != null) {
            int i3 = 0;
            while (true) {
                int i4 = i3;
                int i5 = i2;
                if (i4 >= this.f12a.length) {
                    i2 = i5;
                } else {
                    r rVar = (r) this.f12a[i4];
                    if (rVar != null) {
                        a e2 = rVar.e();
                        c f2 = rVar.f();
                        int i6 = 0;
                        while (true) {
                            int i7 = i6;
                            int i8 = i5;
                            if (i7 >= hVar.i()) {
                                i2 = i8;
                            } else {
                                n nVar = (n) hVar.a(i7);
                                if (nVar != null && h.a(e2, f2, nVar.e(), nVar.f())) {
                                    nVar.d(-1);
                                    rVar.d(-1);
                                    if (1 > rVar.h()) {
                                        this.f12a[i4] = null;
                                    }
                                    if (nVar.h() <= 0) {
                                        int ordinal = nVar.a().ordinal();
                                        switch (b()[i.values()[ordinal].ordinal()]) {
                                            case 1:
                                                hVar.a(com.GalaxyLaser.util.f.Enemy_Destroy);
                                                f fVar2 = new f(nVar.e());
                                                fVar2.a(nVar.i());
                                                fVar2.a(this.b.getResources(), C0000R.drawable.enemy_star1);
                                                this.d.a(new j(nVar.e(), this.b));
                                                f fVar3 = fVar2;
                                                i = i8;
                                                fVar = fVar3;
                                                break;
                                            case 2:
                                                hVar.a(com.GalaxyLaser.util.f.Enemy_Destroy);
                                                f fVar4 = new f(nVar.e());
                                                fVar4.a(nVar.i());
                                                fVar4.a(this.b.getResources(), C0000R.drawable.enemy_star2);
                                                this.d.a(new j(nVar.e(), this.b));
                                                f fVar5 = fVar4;
                                                i = i8;
                                                fVar = fVar5;
                                                break;
                                            case 3:
                                                hVar.a(com.GalaxyLaser.util.f.Enemy_Destroy);
                                                f fVar6 = new f(nVar.e());
                                                fVar6.a(nVar.i());
                                                fVar6.a(this.b.getResources(), C0000R.drawable.enemy_star3);
                                                this.d.a(new j(nVar.e(), this.b));
                                                f fVar7 = fVar6;
                                                i = i8;
                                                fVar = fVar7;
                                                break;
                                            case 4:
                                                hVar.a(com.GalaxyLaser.util.f.Enemy_Destroy);
                                                f fVar8 = new f(nVar.e());
                                                fVar8.a(nVar.i());
                                                fVar8.a(this.b.getResources(), C0000R.drawable.enemy_star4);
                                                this.d.a(new j(nVar.e(), this.b));
                                                f fVar9 = fVar8;
                                                i = i8;
                                                fVar = fVar9;
                                                break;
                                            case 5:
                                                hVar.a(com.GalaxyLaser.util.f.Enemy_Destroy);
                                                f fVar10 = new f(nVar.e());
                                                fVar10.a(nVar.i());
                                                fVar10.a(this.b.getResources(), C0000R.drawable.enemy_star5);
                                                this.d.a(new j(nVar.e(), this.b));
                                                f fVar11 = fVar10;
                                                i = i8;
                                                fVar = fVar11;
                                                break;
                                            case 6:
                                                hVar.a(com.GalaxyLaser.util.f.Enemy_Destroy);
                                                f fVar12 = new f(nVar.e());
                                                fVar12.a(nVar.i());
                                                fVar12.a(this.b.getResources(), C0000R.drawable.enemy_star6);
                                                this.d.a(new j(nVar.e(), this.b));
                                                f fVar13 = fVar12;
                                                i = i8;
                                                fVar = fVar13;
                                                break;
                                            case 7:
                                                hVar.a(com.GalaxyLaser.util.f.Boss_Destroy);
                                                this.d.a(a.a().d() ? new q(nVar.e(), this.b) : new k(nVar.e(), this.b));
                                                int i9 = i8 + nVar.i();
                                                for (int i10 = 0; i10 < 20; i10++) {
                                                    f fVar14 = new f(rVar.e());
                                                    fVar14.a(this.b.getResources(), C0000R.drawable.enemy_star3);
                                                    fVar14.a(400);
                                                    fVar14.b((i10 - 10) / 2);
                                                    o.a(this.b).a(fVar14);
                                                }
                                                i = i9;
                                                fVar = null;
                                                break;
                                            case 8:
                                                hVar.a(com.GalaxyLaser.util.f.Boss_Destroy);
                                                this.d.a(a.a().d() ? new q(nVar.e(), this.b) : new k(nVar.e(), this.b));
                                                int i11 = i8 + nVar.i();
                                                for (int i12 = 0; i12 < 20; i12++) {
                                                    f fVar15 = new f(rVar.e());
                                                    fVar15.a(this.b.getResources(), C0000R.drawable.enemy_star3);
                                                    fVar15.a(200);
                                                    fVar15.b((i12 - 10) / 2);
                                                    o.a(this.b).a(fVar15);
                                                }
                                                i = i11;
                                                fVar = null;
                                                break;
                                            case 9:
                                                hVar.a(com.GalaxyLaser.util.f.Boss_Destroy);
                                                this.d.a(a.a().d() ? new q(nVar.e(), this.b) : new k(nVar.e(), this.b));
                                                int i13 = i8 + nVar.i();
                                                for (int i14 = 0; i14 < 20; i14++) {
                                                    f fVar16 = new f(rVar.e());
                                                    fVar16.a(this.b.getResources(), C0000R.drawable.enemy_star2);
                                                    fVar16.a(600);
                                                    fVar16.b((i14 - 10) / 2);
                                                    o.a(this.b).a(fVar16);
                                                }
                                                i = i13;
                                                fVar = null;
                                                break;
                                            case 10:
                                                hVar.a(com.GalaxyLaser.util.f.Boss_Destroy);
                                                this.d.a(a.a().d() ? new q(nVar.e(), this.b) : new k(nVar.e(), this.b));
                                                int i15 = i8 + nVar.i();
                                                for (int i16 = 0; i16 < 20; i16++) {
                                                    f fVar17 = new f(rVar.e());
                                                    fVar17.a(this.b.getResources(), C0000R.drawable.enemy_star4);
                                                    fVar17.a(1000);
                                                    fVar17.b((i16 - 10) / 2);
                                                    o.a(this.b).a(fVar17);
                                                }
                                                i = i15;
                                                fVar = null;
                                                break;
                                            case 11:
                                                hVar.a(com.GalaxyLaser.util.f.Boss_Destroy);
                                                this.d.a(a.a().d() ? new q(nVar.e(), this.b) : new k(nVar.e(), this.b));
                                                int i17 = i8 + nVar.i();
                                                for (int i18 = 0; i18 < 20; i18++) {
                                                    f fVar18 = new f(rVar.e());
                                                    fVar18.a(this.b.getResources(), C0000R.drawable.enemy_star2);
                                                    fVar18.a(800);
                                                    fVar18.b((i18 - 10) / 2);
                                                    o.a(this.b).a(fVar18);
                                                }
                                                i = i17;
                                                fVar = null;
                                                break;
                                            case 12:
                                                hVar.a(com.GalaxyLaser.util.f.Boss_Destroy);
                                                this.d.a(a.a().d() ? new q(nVar.e(), this.b) : new k(nVar.e(), this.b));
                                                int i19 = i8 + nVar.i();
                                                for (int i20 = 0; i20 < 20; i20++) {
                                                    f fVar19 = new f(rVar.e());
                                                    fVar19.a(this.b.getResources(), C0000R.drawable.enemy_star4);
                                                    fVar19.a(1200);
                                                    fVar19.b((i20 - 10) / 2);
                                                    o.a(this.b).a(fVar19);
                                                }
                                                i = i19;
                                                fVar = null;
                                                break;
                                            default:
                                                i = i8;
                                                fVar = null;
                                                break;
                                        }
                                        if (fVar != null) {
                                            o.a(this.b).a(fVar);
                                        }
                                        if (a.a().d()) {
                                            aq aqVar = new aq(n.a(this.b).g(), nVar, this.b);
                                            aqVar.e(-1);
                                            aqVar.a(30);
                                            aqVar.a(this.b.getResources(), C0000R.drawable.enemy_bullet);
                                            p.a(this.b).a(aqVar);
                                        }
                                        o a2 = o.a(this.b);
                                        p a3 = p.a(this.b);
                                        int i21 = 0;
                                        while (true) {
                                            int i22 = i21;
                                            if (i22 >= a3.i()) {
                                                hVar.b(i7);
                                                com.GalaxyLaser.b.c k = nVar.k();
                                                if (k != null) {
                                                    k.b();
                                                    i5 = i;
                                                } else {
                                                    i5 = i;
                                                }
                                                i6 = i7 + 1;
                                            } else {
                                                r rVar2 = (r) a3.f12a[i22];
                                                if (rVar2 != null && i7 == rVar2.d()) {
                                                    switch (b()[i.values()[ordinal].ordinal()]) {
                                                        case 1:
                                                            a2.a(new com.GalaxyLaser.c.c(rVar2.e(), this.b));
                                                            break;
                                                        case 2:
                                                            a2.a(new p(rVar2.e(), this.b));
                                                            break;
                                                        case 3:
                                                            a2.a(new ar(rVar2.e(), this.b));
                                                            break;
                                                        case 4:
                                                            a2.a(new com.GalaxyLaser.c.i(rVar2.e(), this.b));
                                                            break;
                                                        case 5:
                                                            a2.a(new m(rVar2.e(), this.b));
                                                            break;
                                                        case 6:
                                                            a2.a(new b(rVar2.e(), this.b));
                                                            break;
                                                    }
                                                    a3.f12a[i22] = null;
                                                }
                                                i21 = i22 + 1;
                                            }
                                        }
                                    } else {
                                        this.e.a(com.GalaxyLaser.util.f.Hit);
                                        this.d.a(new d(rVar.e(), this.b));
                                    }
                                }
                                i5 = i8;
                                i6 = i7 + 1;
                            }
                        }
                    } else {
                        i2 = i5;
                    }
                    i3 = i4 + 1;
                }
            }
        }
        p b = hVar.b();
        if (this.f12a != null) {
            for (int i23 = 0; i23 < this.f12a.length; i23++) {
                r rVar3 = (r) this.f12a[i23];
                if (rVar3 != null) {
                    a e3 = rVar3.e();
                    c f3 = rVar3.f();
                    for (int i24 = 0; i24 < b.i(); i24++) {
                        r rVar4 = (r) b.f12a[i24];
                        if (rVar4 != null && rVar4.c() && h.a(e3, f3, rVar4.e(), rVar4.f())) {
                            rVar4.d(-1);
                            this.f12a[i23] = null;
                            if (rVar4.h() <= 0) {
                                this.e.a(com.GalaxyLaser.util.f.Enemy_Destroy);
                                b.f12a[i24] = null;
                            }
                        }
                    }
                }
            }
        }
        return i2;
    }

    public final int a(l lVar, o oVar) {
        com.GalaxyLaser.c.g lVar2;
        int i = 0;
        if (this.f12a == null) {
            return 0;
        }
        int i2 = 0;
        while (true) {
            int i3 = i2;
            int i4 = i;
            if (i3 >= this.f12a.length) {
                return i4;
            }
            r rVar = (r) this.f12a[i3];
            if (rVar != null) {
                a e2 = rVar.e();
                c f2 = rVar.f();
                int i5 = 0;
                while (true) {
                    int i6 = i4;
                    if (i5 >= lVar.i()) {
                        i = i6;
                    } else {
                        as asVar = (as) lVar.b(i5);
                        if (asVar != null && asVar.a()) {
                            a e3 = asVar.e();
                            if (h.a(e2, f2, e3, asVar.f())) {
                                asVar.d(-1);
                                rVar.d(-1);
                                if (1 > rVar.h()) {
                                    this.f12a[i3] = null;
                                }
                                if (asVar.h() <= 0) {
                                    if (i.Rock1 == asVar.k()) {
                                        this.d.a(new j(asVar.e(), this.b));
                                    } else {
                                        this.d.a(new t(asVar.e(), this.b));
                                    }
                                    i4 = asVar.i() + i6;
                                    lVar.a(com.GalaxyLaser.util.f.Meteor_Destroy);
                                    lVar.a(i5);
                                    Context a2 = oVar.a();
                                    switch (e.a().nextInt(7)) {
                                        case 0:
                                            lVar2 = new ac(e3, a2);
                                            break;
                                        case 1:
                                            lVar2 = new com.GalaxyLaser.c.h(e3, a2);
                                            break;
                                        case 2:
                                            lVar2 = new ba(e3, a2);
                                            break;
                                        case 3:
                                            lVar2 = new ae(e3, a2);
                                            break;
                                        case 4:
                                            lVar2 = new av(e3, a2);
                                            break;
                                        case 5:
                                            lVar2 = new ao(e3, a2);
                                            break;
                                        case 6:
                                            lVar2 = new l(e3, a2);
                                            break;
                                        default:
                                            lVar2 = null;
                                            break;
                                    }
                                    av avVar = (6 == a.a().b() && e.a().nextInt(3) == 0) ? new av(e3, a2) : lVar2;
                                    if (avVar != null) {
                                        oVar.a(avVar);
                                    }
                                    i5++;
                                } else {
                                    this.e.a(com.GalaxyLaser.util.f.Hit);
                                    this.d.a(new d(rVar.e(), this.b));
                                }
                            }
                        }
                        i4 = i6;
                        i5++;
                    }
                }
            } else {
                i = i4;
            }
            i2 = i3 + 1;
        }
    }

    public final void a() {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                if (this.f12a[i] != null) {
                    this.f12a[i].b();
                }
            }
        }
    }

    public final void a(Canvas canvas) {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                if (this.f12a[i] != null) {
                    this.f12a[i].a(canvas);
                }
            }
        }
    }

    public final void a(com.GalaxyLaser.c.a aVar) {
        if (aVar != null) {
            int c2 = aVar.c();
            if (1 == (c2 & 1)) {
                a(new at(aVar, this.b));
            }
            if (2 == (c2 & 2)) {
                for (int i = 0; i < 2; i++) {
                    a(new v(aVar, this.b, i));
                }
            }
            if (4 == (c2 & 4)) {
                a(new ab(aVar, this.b));
            }
            if (8 == (c2 & 8)) {
                a(new com.GalaxyLaser.c.e(aVar, this.b));
            }
            this.e.a(com.GalaxyLaser.util.f.Shot);
        }
    }

    public final void c() {
        super.c();
        if (c != null) {
            c = null;
        }
    }
}
