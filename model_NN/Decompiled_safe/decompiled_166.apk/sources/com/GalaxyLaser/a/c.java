package com.GalaxyLaser.a;

import android.content.Context;
import android.graphics.Canvas;
import com.GalaxyLaser.c.g;

public final class c extends q {
    private static c c = null;

    private c(Context context) {
        super(context);
    }

    public static c a(Context context) {
        if (c == null) {
            c = new c(context);
        }
        return c;
    }

    public final void a(Canvas canvas) {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                if (this.f12a[i] != null) {
                    this.f12a[i].a(canvas);
                }
            }
        }
    }

    public final boolean a() {
        if (this.f12a == null) {
            return false;
        }
        int i = 0;
        for (g gVar : this.f12a) {
            if (gVar != null) {
                i++;
            }
        }
        return 15 > i;
    }

    public final void b() {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                if (this.f12a[i] != null) {
                    this.f12a[i].b();
                }
            }
        }
    }

    public final void c() {
        super.c();
        if (c != null) {
            c = null;
        }
    }
}
