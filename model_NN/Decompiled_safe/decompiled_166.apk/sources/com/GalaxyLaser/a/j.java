package com.GalaxyLaser.a;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.GalaxyLaser.C0000R;
import java.util.HashMap;

public final class j {
    private static j b = null;

    /* renamed from: a  reason: collision with root package name */
    private Context f9a;
    private HashMap c = new HashMap();

    private j(Context context) {
        this.f9a = context;
        Resources resources = this.f9a.getResources();
        try {
            this.c.put(Integer.valueOf((int) C0000R.drawable.rock2), BitmapFactory.decodeResource(resources, C0000R.drawable.rock2));
            this.c.put(Integer.valueOf((int) C0000R.drawable.rock2_background), BitmapFactory.decodeResource(resources, C0000R.drawable.rock2_background));
            this.c.put(Integer.valueOf((int) C0000R.drawable.rock3), BitmapFactory.decodeResource(resources, C0000R.drawable.rock3));
            this.c.put(Integer.valueOf((int) C0000R.drawable.rock4), BitmapFactory.decodeResource(resources, C0000R.drawable.rock4));
            this.c.put(Integer.valueOf((int) C0000R.drawable.planet1), BitmapFactory.decodeResource(resources, C0000R.drawable.planet1));
            this.c.put(Integer.valueOf((int) C0000R.drawable.planet2), BitmapFactory.decodeResource(resources, C0000R.drawable.planet2));
            this.c.put(Integer.valueOf((int) C0000R.drawable.planet3), BitmapFactory.decodeResource(resources, C0000R.drawable.planet3));
            this.c.put(Integer.valueOf((int) C0000R.drawable.planet4), BitmapFactory.decodeResource(resources, C0000R.drawable.planet4));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy1), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy1));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy2), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy2));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy3), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy3));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy4), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy4));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy4_left), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy4_left));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy4_right), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy4_right));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy5), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy5));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy6), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy6));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy1n), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy1n));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy2n), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy2n));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy3n), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy3n));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy4n), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy4n));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy4n_left), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy4n_left));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy4n_right), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy4n_right));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy5n), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy5n));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy6n), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy6n));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy_bullet), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy_bullet));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy_bullet1), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy_bullet1));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy_bullet2), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy_bullet2));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy_bullet3), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy_bullet3));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy_bullet4), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy_bullet4));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy_bullet5), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy_bullet5));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy_bullet6), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy_bullet6));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally), BitmapFactory.decodeResource(resources, C0000R.drawable.ally));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_left), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_left));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_right), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_right));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_shield), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_shield));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_s), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_s));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_left_s), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_left_s));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_right_s), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_right_s));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_nightmare), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_nightmare));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_nightmare_left), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_nightmare_left));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_nightmare_right), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_nightmare_right));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_nightmare_shield), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_nightmare_shield));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_nightmare_s), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_nightmare_s));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_nightmare_left_s), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_nightmare_left_s));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_nightmare_right_s), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_nightmare_right_s));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_bullet), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_bullet));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_bullet_back), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_bullet_back));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_bullet_double), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_bullet_double));
            this.c.put(Integer.valueOf((int) C0000R.drawable.ally_bullet_power), BitmapFactory.decodeResource(resources, C0000R.drawable.ally_bullet_power));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy_explode00), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy_explode00));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy_explode01), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy_explode01));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy_explode02), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy_explode02));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy_explode03), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy_explode03));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy_explode04), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy_explode04));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy_explode05), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy_explode05));
            this.c.put(Integer.valueOf((int) C0000R.drawable.enemy_explode06), BitmapFactory.decodeResource(resources, C0000R.drawable.enemy_explode06));
            this.c.put(Integer.valueOf((int) C0000R.drawable.explode_rock00), BitmapFactory.decodeResource(resources, C0000R.drawable.explode_rock00));
            this.c.put(Integer.valueOf((int) C0000R.drawable.explode_rock01), BitmapFactory.decodeResource(resources, C0000R.drawable.explode_rock01));
            this.c.put(Integer.valueOf((int) C0000R.drawable.explode_rock02), BitmapFactory.decodeResource(resources, C0000R.drawable.explode_rock02));
            this.c.put(Integer.valueOf((int) C0000R.drawable.explode_rock03), BitmapFactory.decodeResource(resources, C0000R.drawable.explode_rock03));
            this.c.put(Integer.valueOf((int) C0000R.drawable.explode_rock04), BitmapFactory.decodeResource(resources, C0000R.drawable.explode_rock04));
            this.c.put(Integer.valueOf((int) C0000R.drawable.explode_rock05), BitmapFactory.decodeResource(resources, C0000R.drawable.explode_rock05));
            this.c.put(Integer.valueOf((int) C0000R.drawable.explode_rock06), BitmapFactory.decodeResource(resources, C0000R.drawable.explode_rock06));
            this.c.put(Integer.valueOf((int) C0000R.drawable.explode_rock07), BitmapFactory.decodeResource(resources, C0000R.drawable.explode_rock07));
            this.c.put(Integer.valueOf((int) C0000R.drawable.explode_rock08), BitmapFactory.decodeResource(resources, C0000R.drawable.explode_rock08));
            this.c.put(Integer.valueOf((int) C0000R.drawable.explode_rock09), BitmapFactory.decodeResource(resources, C0000R.drawable.explode_rock09));
            this.c.put(Integer.valueOf((int) C0000R.drawable.explode_rock10), BitmapFactory.decodeResource(resources, C0000R.drawable.explode_rock10));
            this.c.put(Integer.valueOf((int) C0000R.drawable.explode_rock11), BitmapFactory.decodeResource(resources, C0000R.drawable.explode_rock11));
            this.c.put(Integer.valueOf((int) C0000R.drawable.explode_rock12), BitmapFactory.decodeResource(resources, C0000R.drawable.explode_rock12));
            this.c.put(Integer.valueOf((int) C0000R.drawable.explode_rock13), BitmapFactory.decodeResource(resources, C0000R.drawable.explode_rock13));
            this.c.put(Integer.valueOf((int) C0000R.drawable.explode_rock14), BitmapFactory.decodeResource(resources, C0000R.drawable.explode_rock14));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static j a() {
        return b;
    }

    public static j a(Context context) {
        if (b == null) {
            b = new j(context);
        }
        return b;
    }

    public final Bitmap a(int i) {
        return (Bitmap) this.c.get(Integer.valueOf(i));
    }

    public final boolean b(int i) {
        if (this.c == null) {
            return false;
        }
        return this.c.get(Integer.valueOf(i)) != null;
    }
}
