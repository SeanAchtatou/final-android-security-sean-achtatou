package com.GalaxyLaser.a;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Vibrator;
import com.GalaxyLaser.c.as;
import com.GalaxyLaser.c.d;
import com.GalaxyLaser.c.j;
import com.GalaxyLaser.c.o;
import com.GalaxyLaser.c.r;
import com.GalaxyLaser.c.t;
import com.GalaxyLaser.util.a;
import com.GalaxyLaser.util.c;
import com.GalaxyLaser.util.f;
import com.GalaxyLaser.util.h;
import com.GalaxyLaser.util.i;

public final class p extends q {
    private static p c = null;

    private p(Context context) {
        super(context);
    }

    public static p a(Context context) {
        if (c == null) {
            c = new p(context);
        }
        return c;
    }

    public final void a() {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                if (this.f12a[i] != null) {
                    this.f12a[i].b();
                }
            }
        }
    }

    public final void a(Canvas canvas) {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                if (this.f12a[i] != null) {
                    this.f12a[i].a(canvas);
                }
            }
        }
    }

    public final void a(l lVar) {
        if (this.f12a != null && lVar != null && !a.a().c()) {
            for (int i = 0; i < this.f12a.length; i++) {
                r rVar = (r) this.f12a[i];
                if (rVar != null) {
                    a e = rVar.e();
                    c f = rVar.f();
                    for (int i2 = 0; i2 < lVar.i(); i2++) {
                        as asVar = (as) lVar.b(i2);
                        if (asVar != null && h.a(e, f, asVar.e(), asVar.f())) {
                            asVar.d(-1);
                            if (1 > asVar.h()) {
                                f.a(this.b).a(f.Meteor_Destroy);
                                if (i.Rock1 == asVar.k()) {
                                    b.a(this.b).a(new j(e, this.b));
                                } else {
                                    b.a(this.b).a(new t(e, this.b));
                                }
                                lVar.a(i2);
                            } else {
                                b.a(this.b).a(new d(e, this.b));
                            }
                            this.f12a[i] = null;
                        }
                    }
                }
            }
        }
    }

    public final boolean a(n nVar) {
        if (this.f12a == null || nVar == null) {
            return false;
        }
        if (a.a().c()) {
            return false;
        }
        com.GalaxyLaser.c.a g = nVar.g();
        if (g == null) {
            return false;
        }
        c cVar = new c((byte) 0);
        cVar.b = (int) (((float) g.f().b) * 0.4f);
        cVar.f59a = (int) (((float) g.f().f59a) * 0.4f);
        a aVar = new a(g.e());
        aVar.f57a += (g.f().f59a / 2) - (cVar.f59a / 2);
        aVar.b += (g.f().b / 2) - (cVar.b / 2);
        for (int i = 0; i < this.f12a.length; i++) {
            r rVar = (r) this.f12a[i];
            if (rVar != null) {
                a e = rVar.e();
                if (h.a(e, rVar.f(), aVar, cVar)) {
                    g.d(-1);
                    if (g.h() < 0) {
                        f.a(this.b).a(f.Ally_Destroy);
                        a.a().a(true);
                        this.b.getResources();
                        nVar.b(0);
                        b.a(this.b).a(new o(g.e(), this.b));
                        return true;
                    }
                    b.a(this.b).a(new d(e, this.b));
                    if (g.h() == 0) {
                        int i2 = a.a().d() ? 16 : 0;
                        this.b.getResources();
                        nVar.b(i2 | 1);
                    }
                    ((Vibrator) this.b.getSystemService("vibrator")).vibrate(100);
                    rVar.d(-1);
                    if (1 > rVar.h()) {
                        this.f12a[i] = null;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public final void c() {
        super.c();
        if (c != null) {
            c = null;
        }
    }
}
