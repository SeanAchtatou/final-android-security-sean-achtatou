package com.GalaxyLaser.a;

import android.content.Context;
import android.content.SharedPreferences;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    private static i f8a = null;
    private SharedPreferences b;

    private i(Context context) {
        this.b = context.getSharedPreferences("GalaxyLaserII", 0);
    }

    public static i a(Context context) {
        if (f8a == null) {
            f8a = new i(context);
        }
        return f8a;
    }

    public static void f() {
        if (f8a != null) {
            f8a = null;
        }
    }

    public final void a(int i) {
        if (i > this.b.getInt("HighScore", 0)) {
            SharedPreferences.Editor edit = this.b.edit();
            edit.putInt("HighScore", i);
            edit.commit();
        }
    }

    public final void a(String str) {
        SharedPreferences.Editor edit = this.b.edit();
        edit.putString("Name", str);
        edit.commit();
    }

    public final boolean a() {
        return this.b.getBoolean("NightMare", false);
    }

    public final void b() {
        SharedPreferences.Editor edit = this.b.edit();
        edit.putBoolean("NightMare", true);
        edit.commit();
    }

    public final void b(int i) {
        SharedPreferences.Editor edit = this.b.edit();
        edit.putInt("StageNormal", i);
        edit.commit();
    }

    public final String c() {
        return this.b.getString("Name", "");
    }

    public final void c(int i) {
        SharedPreferences.Editor edit = this.b.edit();
        edit.putInt("StageNightmare", i);
        edit.commit();
    }

    public final int d() {
        return this.b.getInt("StageNormal", 1);
    }

    public final int e() {
        return this.b.getInt("StageNightmare", 1);
    }
}
