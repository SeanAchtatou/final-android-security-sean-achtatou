package com.GalaxyLaser.a;

import android.content.Context;
import android.media.MediaPlayer;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    private static m f11a = null;
    private MediaPlayer b = null;
    private Context c;

    private m(Context context) {
        this.c = context;
    }

    public static m a(Context context) {
        if (f11a == null) {
            f11a = new m(context);
        }
        return f11a;
    }

    public final void a() {
        if (this.b != null && this.b.isPlaying()) {
            this.b.stop();
        }
    }

    public final void a(int i, boolean z) {
        a();
        this.b = MediaPlayer.create(this.c, i);
        this.b.setOnPreparedListener(new e(this, z));
    }

    public final void b() {
        if (this.b != null && this.b.isPlaying()) {
            this.b.pause();
        }
    }

    public final void c() {
        if (this.b != null) {
            this.b.start();
        }
    }

    public final void d() {
        a();
        if (this.b != null) {
            this.b.release();
            this.b = null;
        }
        if (f11a != null) {
            f11a = null;
        }
    }
}
