package com.GalaxyLaser.a;

import android.content.Context;
import android.graphics.Canvas;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.c.a;
import com.GalaxyLaser.c.aq;
import com.GalaxyLaser.c.ar;
import com.GalaxyLaser.c.b;
import com.GalaxyLaser.c.c;
import com.GalaxyLaser.c.f;
import com.GalaxyLaser.c.j;
import com.GalaxyLaser.c.m;
import com.GalaxyLaser.c.n;
import com.GalaxyLaser.c.p;
import com.GalaxyLaser.c.r;
import com.GalaxyLaser.c.u;
import com.GalaxyLaser.c.w;
import com.GalaxyLaser.util.d;
import com.GalaxyLaser.util.g;
import com.GalaxyLaser.util.h;
import com.GalaxyLaser.util.i;

public final class o extends q {
    private static o c = null;
    private static /* synthetic */ int[] e;
    private static /* synthetic */ int[] f;
    private Context d;

    private o(Context context) {
        super(context);
        this.d = context;
    }

    public static o a(Context context) {
        if (c == null) {
            c = new o(context);
        }
        return c;
    }

    private static /* synthetic */ int[] d() {
        int[] iArr = e;
        if (iArr == null) {
            iArr = new int[g.values().length];
            try {
                iArr[g.BackShot.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[g.Black.ordinal()] = 12;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[g.DoubleShot.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[g.Item_Point.ordinal()] = 13;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[g.PowerShot.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[g.Rainbow.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[g.Shield.ordinal()] = 5;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[g.SpeedUp.ordinal()] = 1;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[g.Star_Green.ordinal()] = 11;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[g.Star_Orange.ordinal()] = 8;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[g.Star_Purple.ordinal()] = 9;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[g.Star_Red.ordinal()] = 7;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[g.Star_Yellow.ordinal()] = 10;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[g.Warning.ordinal()] = 14;
            } catch (NoSuchFieldError e15) {
            }
            e = iArr;
        }
        return iArr;
    }

    private static /* synthetic */ int[] e() {
        int[] iArr = f;
        if (iArr == null) {
            iArr = new int[i.values().length];
            try {
                iArr[i.Boss1.ordinal()] = 7;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[i.Boss2.ordinal()] = 8;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[i.Boss3.ordinal()] = 9;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[i.Boss4.ordinal()] = 10;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[i.Boss5.ordinal()] = 11;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[i.Boss6.ordinal()] = 12;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[i.Rock1.ordinal()] = 13;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[i.Rock2.ordinal()] = 14;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[i.Type1.ordinal()] = 1;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[i.Type2.ordinal()] = 2;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[i.Type3.ordinal()] = 3;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[i.Type4.ordinal()] = 4;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[i.Type5.ordinal()] = 5;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[i.Type6.ordinal()] = 6;
            } catch (NoSuchFieldError e15) {
            }
            f = iArr;
        }
        return iArr;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final int a(n nVar) {
        a g;
        f fVar;
        int i;
        int i2;
        int i3 = 0;
        if (this.f12a == null || nVar == null || a.a().c() || (g = nVar.g()) == null) {
            return 0;
        }
        int i4 = 0;
        while (true) {
            int i5 = i3;
            if (i4 >= this.f12a.length) {
                return i5;
            }
            u uVar = (u) this.f12a[i4];
            if (uVar != null) {
                com.GalaxyLaser.util.a e2 = uVar.e();
                if (h.a(e2, uVar.f(), g.e(), g.f())) {
                    switch (d()[g.values()[uVar.c().ordinal()].ordinal()]) {
                        case 1:
                            int a2 = nVar.a();
                            nVar.a(a2 - 1);
                            i3 = a2 == nVar.a() ? 1000 : i5;
                            k.a(this.d).a(d.SpeedUp);
                            f.a(this.d).a(com.GalaxyLaser.util.f.Powerup);
                            g.a();
                            break;
                        case 2:
                            if (2 != (g.c() & 2)) {
                                g.a(g.c() | 2);
                                i3 = i5;
                            } else {
                                i3 = 1000;
                            }
                            k.a(this.d).a(d.DoubleShot);
                            f.a(this.d).a(com.GalaxyLaser.util.f.Powerup);
                            g.a();
                            break;
                        case 3:
                            if (4 != (g.c() & 4)) {
                                g.a(g.c() | 4);
                                i2 = i5;
                            } else {
                                i2 = 1000;
                            }
                            k.a(this.d).a(d.BackShot);
                            f.a(this.d).a(com.GalaxyLaser.util.f.Powerup);
                            g.a();
                            break;
                        case 4:
                            if (8 != (g.c() & 8)) {
                                g.a(g.c() | 8);
                                if (1 == (g.c() & 1)) {
                                    g.a(g.c() - 1);
                                    i = i5;
                                } else {
                                    i = i5;
                                }
                            } else {
                                i = 1000;
                            }
                            k.a(this.d).a(d.PowerShot);
                            f.a(this.d).a(com.GalaxyLaser.util.f.Powerup);
                            g.a();
                            break;
                        case 5:
                            int h = g.h();
                            if (6 > h) {
                                g.d(1);
                            }
                            i3 = h == g.h() ? 1000 : i5;
                            k.a(this.d).a(d.Shield);
                            f.a(this.d).a(com.GalaxyLaser.util.f.Powerup);
                            g.a();
                            int i6 = 0;
                            if (a.a().d()) {
                                i6 = 16;
                            }
                            this.d.getResources();
                            nVar.b(i6 | 9);
                            break;
                        case 6:
                            f.a(this.d).a(com.GalaxyLaser.util.f.Flash);
                            h a3 = h.a(this.d);
                            if (a3 != null) {
                                int i7 = 0;
                                while (true) {
                                    int i8 = i7;
                                    if (i8 < a3.i()) {
                                        n nVar2 = (n) a3.f12a[i8];
                                        if (!(nVar2 == null || i.Boss1 == nVar2.a() || i.Boss2 == nVar2.a() || i.Boss3 == nVar2.a() || i.Boss4 == nVar2.a() || i.Boss5 == nVar2.a() || i.Boss6 == nVar2.a())) {
                                            int ordinal = nVar2.a().ordinal();
                                            switch (e()[i.values()[ordinal].ordinal()]) {
                                                case 1:
                                                    fVar = new f(nVar2.e());
                                                    fVar.a(nVar2.i());
                                                    fVar.a(this.d.getResources(), C0000R.drawable.enemy_star1);
                                                    b.a(this.d).a(new j(nVar2.e(), this.d));
                                                    break;
                                                case 2:
                                                    fVar = new f(nVar2.e());
                                                    fVar.a(nVar2.i());
                                                    fVar.a(this.d.getResources(), C0000R.drawable.enemy_star2);
                                                    b.a(this.d).a(new j(nVar2.e(), this.d));
                                                    break;
                                                case 3:
                                                    fVar = new f(nVar2.e());
                                                    fVar.a(nVar2.i());
                                                    fVar.a(this.d.getResources(), C0000R.drawable.enemy_star3);
                                                    b.a(this.d).a(new j(nVar2.e(), this.d));
                                                    break;
                                                case 4:
                                                    fVar = new f(nVar2.e());
                                                    fVar.a(nVar2.i());
                                                    fVar.a(this.d.getResources(), C0000R.drawable.enemy_star4);
                                                    b.a(this.d).a(new j(nVar2.e(), this.d));
                                                    break;
                                                case 5:
                                                    fVar = new f(nVar2.e());
                                                    fVar.a(nVar2.i());
                                                    fVar.a(this.d.getResources(), C0000R.drawable.enemy_star5);
                                                    b.a(this.d).a(new j(nVar2.e(), this.d));
                                                    break;
                                                case 6:
                                                    fVar = new f(nVar2.e());
                                                    fVar.a(nVar2.i());
                                                    fVar.a(this.d.getResources(), C0000R.drawable.enemy_star6);
                                                    b.a(this.d).a(new j(nVar2.e(), this.d));
                                                    break;
                                                default:
                                                    fVar = null;
                                                    break;
                                            }
                                            if (fVar != null) {
                                                a(this.d).a(fVar);
                                            }
                                            if (a.a().d()) {
                                                aq aqVar = new aq(n.a(this.d).g(), nVar2, this.d);
                                                aqVar.e(-1);
                                                aqVar.a(30);
                                                aqVar.a(this.d.getResources(), C0000R.drawable.enemy_bullet);
                                                p.a(this.d).a(aqVar);
                                            }
                                            o a4 = a(this.d);
                                            p a5 = p.a(this.d);
                                            int i9 = 0;
                                            while (true) {
                                                int i10 = i9;
                                                if (i10 >= a5.i()) {
                                                    a3.b(i8);
                                                } else {
                                                    r rVar = (r) a5.f12a[i10];
                                                    if (rVar != null && i8 == rVar.d()) {
                                                        switch (e()[i.values()[ordinal].ordinal()]) {
                                                            case 1:
                                                                a4.a(new c(rVar.e(), this.d));
                                                                break;
                                                            case 2:
                                                                a4.a(new p(rVar.e(), this.d));
                                                                break;
                                                            case 3:
                                                                a4.a(new ar(rVar.e(), this.d));
                                                                break;
                                                            case 4:
                                                                a4.a(new com.GalaxyLaser.c.i(rVar.e(), this.d));
                                                                break;
                                                            case 5:
                                                                a4.a(new m(rVar.e(), this.d));
                                                                break;
                                                            case 6:
                                                                a4.a(new b(rVar.e(), this.d));
                                                                break;
                                                        }
                                                        a5.f12a[i10] = null;
                                                    }
                                                    i9 = i10 + 1;
                                                }
                                            }
                                        }
                                        i7 = i8 + 1;
                                    }
                                }
                            }
                            i3 = i5;
                            break;
                        case 7:
                            i3 = 10;
                            f.a(this.d).a(com.GalaxyLaser.util.f.Star_Item);
                            break;
                        case 8:
                            i3 = 20;
                            f.a(this.d).a(com.GalaxyLaser.util.f.Star_Item);
                            break;
                        case 9:
                            i3 = 30;
                            f.a(this.d).a(com.GalaxyLaser.util.f.Star_Item);
                            break;
                        case 10:
                            i3 = 40;
                            f.a(this.d).a(com.GalaxyLaser.util.f.Star_Item);
                            break;
                        case 11:
                            i3 = 50;
                            f.a(this.d).a(com.GalaxyLaser.util.f.Star_Item);
                            break;
                        case 12:
                            k.a(this.d).a(d.Black);
                            int a6 = nVar.a();
                            nVar.a(a6 + 1);
                            if (a6 == nVar.a()) {
                                int c2 = g.c();
                                if ((c2 & 8) == 0) {
                                    if ((c2 & 2) == 0) {
                                        if ((c2 & 4) == 0) {
                                            int h2 = g.h();
                                            if (h2 <= 0) {
                                                i3 = -1000;
                                                break;
                                            } else {
                                                g.c(h2 - 1);
                                                i3 = i5;
                                                break;
                                            }
                                        } else {
                                            g.a(c2 - 4);
                                            i3 = i5;
                                            break;
                                        }
                                    } else {
                                        g.a(c2 - 2);
                                        i3 = i5;
                                        break;
                                    }
                                } else {
                                    g.a((c2 - 8) | 1);
                                    i3 = i5;
                                    break;
                                }
                            }
                            i3 = i5;
                            break;
                        case 13:
                            i3 = uVar.i();
                            f.a(this.d).a(com.GalaxyLaser.util.f.Star_Item);
                            break;
                        default:
                            i3 = i5;
                            break;
                    }
                    if (i3 != 0) {
                        d.a(this.d).a(new w(e2, String.valueOf(i3)));
                    }
                    this.f12a[i4] = null;
                    i4++;
                }
            }
            i3 = i5;
            i4++;
        }
    }

    public final Context a() {
        return this.d;
    }

    public final void a(Canvas canvas) {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                if (this.f12a[i] != null) {
                    this.f12a[i].a(canvas);
                }
            }
        }
    }

    public final void b() {
        if (this.f12a != null) {
            for (int i = 0; i < this.f12a.length; i++) {
                if (this.f12a[i] != null) {
                    this.f12a[i].b();
                }
            }
        }
    }

    public final void c() {
        super.c();
        if (c != null) {
            c = null;
        }
    }
}
