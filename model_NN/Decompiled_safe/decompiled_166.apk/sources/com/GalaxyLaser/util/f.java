package com.GalaxyLaser.util;

public enum f {
    Shot,
    Enemy_Destroy,
    Meteor_Destroy,
    Powerup_Item,
    Speedup_Item,
    Shield_Item,
    Star_Item,
    Ally_Destroy,
    Boss_Destroy,
    Hit,
    Title_SE,
    Powerup,
    Flash,
    Item_End
}
