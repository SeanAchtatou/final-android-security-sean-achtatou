package com.GalaxyLaser.util;

import android.graphics.Rect;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    private static j f65a;
    private Rect b = new Rect(0, 0, 0, 0);
    private k c = new k();

    private j() {
    }

    public static j a() {
        if (f65a == null) {
            f65a = new j();
        }
        return f65a;
    }

    public final void a(Rect rect) {
        this.b = rect;
        if (480 >= this.b.right) {
            this.c.f66a = 480.0f / ((float) this.b.right);
        } else {
            this.c.f66a = ((float) this.b.right) / 480.0f;
        }
        if (800 >= this.b.bottom) {
            this.c.b = 800.0f / ((float) this.b.bottom);
            return;
        }
        this.c.b = ((float) this.b.bottom) / 800.0f;
    }

    public final k b() {
        return this.c;
    }
}
