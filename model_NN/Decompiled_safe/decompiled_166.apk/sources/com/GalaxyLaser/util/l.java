package com.GalaxyLaser.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f67a;
    private Rect b = new Rect(0, 0, this.f67a.getWidth(), this.f67a.getHeight());
    private Rect c = new Rect((int) (((float) this.d) * this.f), (int) (((float) this.e) * this.g), (int) ((((float) this.d) * this.f) + ((float) this.h.f59a)), (int) ((((float) this.e) * this.g) + ((float) this.h.b)));
    private int d;
    private int e;
    private float f;
    private float g;
    private c h = new c((int) (((float) this.f67a.getWidth()) * this.f), (int) (((float) this.f67a.getHeight()) * this.g));
    private a i = new a(this.c.left, this.c.top);
    private boolean j = true;

    public l(Bitmap bitmap, int i2, int i3, float f2, float f3) {
        this.f67a = bitmap;
        this.f = f2;
        this.g = f3;
        this.d = i2;
        this.e = i3;
    }

    public final void a() {
        if (this.f67a != null) {
            this.f67a.recycle();
        }
    }

    public final void a(int i2, int i3) {
        this.d = i2;
        this.e = i3;
        this.c.set((int) (((float) this.d) * this.f), (int) (((float) this.e) * this.g), (int) ((((float) this.d) * this.f) + ((float) this.h.f59a)), (int) ((((float) this.e) * this.g) + ((float) this.h.b)));
        this.i.f57a = this.c.left;
        this.i.b = this.c.top;
    }

    public final void a(Canvas canvas) {
        if (canvas != null && this.j && this.f67a != null && this.b != null && this.c != null) {
            canvas.drawBitmap(this.f67a, this.b, this.c, (Paint) null);
        }
    }

    public final void a(Canvas canvas, Paint paint) {
        if (this.f67a != null) {
            canvas.drawBitmap(this.f67a, this.b, this.c, paint);
        }
    }
}
