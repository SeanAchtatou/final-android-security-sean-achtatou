package com.GalaxyLaser.util;

import java.util.Random;

public final class e extends Random {
    private static e b = null;

    /* renamed from: a  reason: collision with root package name */
    private n f61a;

    private e() {
        this(System.currentTimeMillis());
    }

    private e(long j) {
        super(j);
        this.f61a = null;
        setSeed(j);
    }

    public static e a() {
        if (b == null) {
            b = new e();
        }
        return b;
    }

    /* access modifiers changed from: protected */
    public final int next(int i) {
        return this.f61a.a() >>> (32 - i);
    }

    public final synchronized void setSeed(long j) {
        int[] iArr = {(int) (-1 & j), (int) (j >>> 32)};
        if (iArr[1] == 0) {
            int i = iArr[0];
            if (this.f61a == null) {
                this.f61a = new n(i);
            } else {
                this.f61a.a(i);
            }
        } else if (this.f61a == null) {
            this.f61a = new n(iArr);
        } else {
            this.f61a.a(iArr);
        }
    }
}
