package com.GalaxyLaser.util;

public final class h {
    public static boolean a(a aVar, c cVar, a aVar2, c cVar2) {
        a aVar3 = new a(aVar.f57a, aVar.b);
        a aVar4 = new a(aVar2.f57a, aVar2.b);
        c cVar3 = new c(cVar.f59a, cVar.b);
        c cVar4 = new c(cVar2.f59a, cVar2.b);
        cVar3.f59a = (int) (((double) cVar3.f59a) * 0.8d);
        cVar3.b = (int) (((double) cVar3.b) * 0.8d);
        cVar4.f59a = (int) (((double) cVar4.f59a) * 0.8d);
        cVar4.b = (int) (((double) cVar4.b) * 0.8d);
        aVar3.f57a += (cVar.f59a - cVar3.f59a) / 2;
        aVar3.b += (cVar.b - cVar3.b) / 2;
        aVar4.f57a += (cVar2.f59a - cVar4.f59a) / 2;
        aVar4.b += (cVar2.b - cVar4.b) / 2;
        if (aVar3.f57a + cVar3.f59a <= aVar4.f57a || aVar4.f57a + cVar4.f59a <= aVar3.f57a || aVar3.b + cVar3.b <= aVar4.b || aVar4.b + cVar4.b <= aVar3.b) {
            if (aVar4.f57a < aVar3.f57a + cVar3.f59a && aVar3.f57a < aVar4.f57a + cVar4.f59a) {
                if (aVar4.b >= cVar3.b + aVar3.b || aVar3.b >= aVar4.b + cVar4.b) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }
}
