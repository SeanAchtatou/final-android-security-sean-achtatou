package com.GalaxyLaser.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.ViewGroup;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.a.f;

public class BaseActivity extends Activity {
    /* access modifiers changed from: protected */

    /* renamed from: a  reason: collision with root package name */
    public boolean f56a;

    /* access modifiers changed from: protected */
    public final void b() {
        f.a(getApplication().getApplicationContext()).a(f.Title_SE);
        this.f56a = true;
        new AlertDialog.Builder(this).setTitle((int) C0000R.string.about_title).setIcon((int) C0000R.drawable.icon).setView(getLayoutInflater().inflate((int) C0000R.layout.about, (ViewGroup) findViewById(C0000R.id.layout_about_root))).setPositiveButton("OK", new m(this)).create().show();
    }

    /* access modifiers changed from: protected */
    public final void c() {
        f.a(getApplication().getApplicationContext()).a(f.Title_SE);
        try {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.arttec.co.jp/sat-box")));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        f.a(getApplication().getApplicationContext()).a(f.Title_SE);
        try {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:SAT-BOX")));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        setVolumeControlStream(3);
        this.f56a = false;
    }
}
