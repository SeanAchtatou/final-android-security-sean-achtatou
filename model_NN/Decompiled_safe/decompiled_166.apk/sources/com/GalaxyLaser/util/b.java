package com.GalaxyLaser.util;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private int f58a = 30;
    private int b = (1000 / this.f58a);
    private long c;
    private long d;
    private long e = 0;
    private int f = 0;

    public final void a(long j) {
        this.c = j;
        if (this.f == 0) {
            this.e = this.c;
        }
        this.d = this.e + ((long) ((this.f + 1) * this.b));
    }

    public final boolean b(long j) {
        if (j < this.d) {
            return false;
        }
        int i = this.f + 1;
        this.f = i;
        if (i != this.f58a) {
            return true;
        }
        this.f = 0;
        return true;
    }
}
