package com.GalaxyLaser.util;

public final class n {

    /* renamed from: a  reason: collision with root package name */
    private final int[] f69a = new int[624];
    private int b = 625;

    public n(int i) {
        a(i);
    }

    public n(int[] iArr) {
        a(iArr);
    }

    private synchronized void a(long[] jArr) {
        for (int i = 0; i < 624; i++) {
            int[] iArr = this.f69a;
            long j = jArr[i];
            if (j > 2147483647L) {
                j -= 4294967296L;
            }
            iArr[i] = (int) j;
        }
        this.b = 624;
    }

    private synchronized int b() {
        int i;
        int[] iArr = new int[2];
        iArr[1] = -1727483681;
        if (this.b >= 624) {
            if (this.b > 624) {
                a(5489);
            }
            for (int i2 = 0; i2 < 624; i2++) {
                int i3 = (this.f69a[i2] & Integer.MIN_VALUE) | (this.f69a[(i2 + 1) % 624] & Integer.MAX_VALUE);
                this.f69a[i2] = iArr[i3 & 1] ^ (this.f69a[(i2 + 397) % 624] ^ (i3 >>> 1));
            }
            this.b = 0;
        }
        i = this.f69a[this.b];
        this.b++;
        return i;
    }

    private static long[] b(int i) {
        long[] jArr = new long[624];
        jArr[0] = ((long) i) & 4294967295L;
        for (int i2 = 1; i2 < 624; i2++) {
            jArr[i2] = jArr[i2 - 1];
            jArr[i2] = jArr[i2] >>> 30;
            jArr[i2] = jArr[i2] ^ jArr[i2 - 1];
            jArr[i2] = jArr[i2] * 1812433253;
            jArr[i2] = jArr[i2] + ((long) i2);
            jArr[i2] = jArr[i2] & 4294967295L;
        }
        return jArr;
    }

    public final int a() {
        int b2 = b();
        int i = b2 ^ (b2 >>> 11);
        int i2 = i ^ ((i << 7) & -1658038656);
        int i3 = i2 ^ ((i2 << 15) & -272236544);
        return i3 ^ (i3 >>> 18);
    }

    public final void a(int i) {
        a(b(i));
    }

    public final void a(int[] iArr) {
        long[] b2 = b(19650218);
        int length = 624 > iArr.length ? 624 : iArr.length;
        int i = 0;
        int i2 = 1;
        for (int i3 = 0; i3 < length; i3++) {
            if (624 <= i2) {
                b2[0] = b2[623];
                i2 = 1;
            }
            if (iArr.length <= i) {
                i = 0;
            }
            b2[i2] = b2[i2] ^ ((b2[i2 - 1] ^ (b2[i2 - 1] >>> 30)) * 1664525);
            b2[i2] = b2[i2] + ((long) (iArr[i] + i));
            b2[i2] = b2[i2] & 4294967295L;
            i2++;
            i++;
        }
        int i4 = (length % 623) + 1;
        for (int i5 = 0; i5 < 623; i5++) {
            if (624 <= i4) {
                b2[0] = b2[623];
                i4 = 1;
            }
            b2[i4] = b2[i4] ^ ((b2[i4 - 1] ^ (b2[i4 - 1] >>> 30)) * 1566083941);
            b2[i4] = b2[i4] - ((long) i4);
            b2[i4] = b2[i4] & 4294967295L;
            i4++;
        }
        b2[0] = 2147483648L;
        a(b2);
    }
}
