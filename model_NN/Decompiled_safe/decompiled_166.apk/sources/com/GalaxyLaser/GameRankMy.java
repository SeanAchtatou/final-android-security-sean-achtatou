package com.GalaxyLaser;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.GalaxyLaser.opening.TitleActivity;
import com.GalaxyLaser.util.BaseActivity;
import java.util.ArrayList;

public class GameRankMy extends BaseActivity {
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public SoundPool c;
    /* access modifiers changed from: private */
    public float d = 0.5f;
    private String e = "LOCAL";
    private Button f;
    private ListView g;
    private ImageButton h;
    private ImageButton i;

    private synchronized void a(String str) {
        synchronized (this) {
            SharedPreferences sharedPreferences = getSharedPreferences("prefkey", 0);
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < 99; i2++) {
                int i3 = i2 + 1;
                String format = String.format("%02d", Integer.valueOf(i3));
                String valueOf = String.valueOf(sharedPreferences.getInt("MyScore" + str + format, 0));
                String string = sharedPreferences.getString("MyDate" + str + format, "9999/99/99 99:99:99");
                arrayList.add(new j(i3, valueOf, string.substring(0, 10), string.substring(11, 19)));
            }
            ((ListView) findViewById(C0000R.id.ranklistview)).setAdapter((ListAdapter) new ab(this, arrayList));
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) C0000R.layout.rank);
        this.c = new SoundPool(1, 3, 0);
        this.b = this.c.load(this, C0000R.raw.se_title, 1);
        this.g = (ListView) findViewById(C0000R.id.ranklistview);
        this.g.setOnItemClickListener(new ae(this));
        this.h = (ImageButton) findViewById(C0000R.id.link_vsboss);
        this.h.setOnClickListener(new ac(this));
        this.i = (ImageButton) findViewById(C0000R.id.link_act2);
        this.i.setOnClickListener(new ad(this));
        this.f = (Button) findViewById(C0000R.id.rank_tx_touch);
        this.f.setOnClickListener(new af(this));
        a(this.e);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.c != null) {
            this.c.stop(this.b);
            this.c.release();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        this.c.play(this.b, this.d, this.d, 1, 0, 1.0f);
        startActivity(new Intent(this, TitleActivity.class));
        finish();
        return true;
    }
}
