package com.GalaxyLaser;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

final class ad implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GameRankMy f16a;

    ad(GameRankMy gameRankMy) {
        this.f16a = gameRankMy;
    }

    public final void onClick(View view) {
        try {
            this.f16a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=jp.co.arttec.satbox.galaxylaser_act2")));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }
}
