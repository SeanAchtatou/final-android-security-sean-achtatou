package com.GalaxyLaser;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.GalaxyLaser.a.i;
import com.GalaxyLaser.a.m;
import com.GalaxyLaser.b.a;
import com.GalaxyLaser.b.b;
import com.GalaxyLaser.b.d;
import com.GalaxyLaser.b.e;
import com.GalaxyLaser.b.f;
import com.GalaxyLaser.b.g;
import com.GalaxyLaser.b.h;
import com.GalaxyLaser.util.BaseActivity;
import com.GalaxyLaser.view.StageView;

public class GameActivity extends BaseActivity implements a, b, d, e, f, g, h {
    /* access modifiers changed from: private */
    public ImageView b;
    /* access modifiers changed from: private */
    public TextView c;
    /* access modifiers changed from: private */
    public StageView d;
    private Handler e = new Handler();
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public m g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public boolean i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public TextView k;
    /* access modifiers changed from: private */
    public ImageView l;
    /* access modifiers changed from: private */
    public boolean m;
    private Handler n = new y(this);

    static /* synthetic */ void a(GameActivity gameActivity, String str) {
        jp.a.a.a.a.a aVar = new jp.a.a.a.a.a(gameActivity.h ? 31 : 30, (long) gameActivity.f, str);
        aVar.a(gameActivity);
        aVar.a(new n(gameActivity));
        aVar.execute(new Void[0]);
    }

    public final void a() {
        this.e.post(new w(this));
    }

    public final void a(int i2) {
        this.f += i2;
        if (this.f < 0) {
            this.f = 0;
        }
        this.e.post(new x(this, String.format("SCORE %07d", Integer.valueOf(this.f))));
    }

    public final void a(int i2, int i3) {
        this.e.post(new z(this, i2, i3));
    }

    public final void b(int i2) {
        this.e.post(new u(this, i2));
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            switch (keyEvent.getKeyCode()) {
                case 4:
                    this.m = false;
                    this.f56a = false;
                    this.d.b(false);
                    return true;
            }
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    public final void e() {
        this.e.post(new v(this));
    }

    public final void f() {
        this.e.post(new s(this));
    }

    public final void g() {
        this.e.post(new t(this));
    }

    public final void h() {
        this.e.post(new aa(this));
        this.n.sendEmptyMessageDelayed(0, 3000);
    }

    public final void i() {
        this.e.post(new o(this));
    }

    public final void j() {
        this.e.post(new p(this));
    }

    public final void k() {
        this.e.post(new q(this));
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.main);
        this.h = false;
        this.b = (ImageView) findViewById(C0000R.id.shieldView);
        this.b.setImageLevel(0);
        this.d = (StageView) findViewById(C0000R.id.stageView);
        this.d.a((b) this);
        this.f = 0;
        this.c = (TextView) findViewById(C0000R.id.score);
        this.d.a((h) this);
        this.g = m.a(this);
        this.d.a((e) this);
        this.d.a((d) this);
        this.d.a((f) this);
        this.d.a((g) this);
        this.l = (ImageView) findViewById(C0000R.id.pause_iv);
        this.k = (TextView) findViewById(C0000R.id.pause_tv);
        this.d.a((a) this);
        this.i = false;
        Intent intent = getIntent();
        this.h = intent.getBooleanExtra("NightMare", false);
        this.d.a(intent.getIntExtra("Stage", 1));
        this.d.a(this.h);
        this.m = false;
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        View inflate = LayoutInflater.from(this).inflate((int) C0000R.layout.regist, (ViewGroup) null);
        ((TextView) inflate.findViewById(C0000R.id.dialog_edittext)).setText(i.a(getApplication().getApplicationContext()).c());
        return new AlertDialog.Builder(this).setIcon((int) C0000R.drawable.icon).setTitle((int) C0000R.string.dialog_title).setView(inflate).setPositiveButton("OK", new r(this, inflate)).setNegativeButton("CANCEL", new m(this)).setCancelable(false).create();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(C0000R.menu.menu, menu);
        return true;
    }

    public boolean onMenuItemSelected(int i2, MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case C0000R.id.finish /*2131230754*/:
                this.i = true;
                com.GalaxyLaser.a.f.a(getApplication().getApplicationContext()).a(com.GalaxyLaser.util.f.Title_SE);
                this.d.c();
                this.d.a();
                break;
            case C0000R.id.about /*2131230755*/:
                b();
                break;
            case C0000R.id.applink /*2131230756*/:
                d();
                break;
            case C0000R.id.satbox /*2131230757*/:
                c();
                break;
        }
        return true;
    }

    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
        this.m = false;
        if (!this.f56a) {
            this.d.b(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.d.c();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        this.m = true;
        this.d.b(true);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        this.d.d();
    }
}
