package com.GalaxyLaser;

import android.content.Intent;
import android.view.View;
import com.GalaxyLaser.opening.TitleActivity;

final class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NightMareHighScoreActivity f25a;

    f(NightMareHighScoreActivity nightMareHighScoreActivity) {
        this.f25a = nightMareHighScoreActivity;
    }

    public final void onClick(View view) {
        com.GalaxyLaser.a.f.a(this.f25a.getApplication().getApplicationContext()).a(com.GalaxyLaser.util.f.Title_SE);
        this.f25a.startActivity(new Intent(this.f25a, TitleActivity.class));
        this.f25a.finish();
    }
}
