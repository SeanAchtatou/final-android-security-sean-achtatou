package com.GalaxyLaser;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

final class h implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HighScoreActivity f27a;

    h(HighScoreActivity highScoreActivity) {
        this.f27a = highScoreActivity;
    }

    public final void onClick(View view) {
        try {
            this.f27a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=jp.co.arttec.satbox.galaxylaser_act2")));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }
}
