package com.GalaxyLaser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

public final class ab extends ArrayAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List f14a;
    private LayoutInflater b;
    private int c = C0000R.layout.row_my;

    public ab(Context context, List list) {
        super(context, (int) C0000R.layout.row_my, list);
        this.f14a = list;
        this.b = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.b.inflate(this.c, (ViewGroup) null) : view;
        j jVar = (j) this.f14a.get(i);
        ((TextView) inflate.findViewById(C0000R.id.txtRankNo)).setText(String.valueOf(jVar.a()));
        ((TextView) inflate.findViewById(C0000R.id.txtItem1)).setText(jVar.b());
        ((TextView) inflate.findViewById(C0000R.id.txtItem21)).setText(jVar.c());
        ((TextView) inflate.findViewById(C0000R.id.txtItem22)).setText(jVar.d());
        return inflate;
    }
}
