package com.GalaxyLaser;

import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import com.GalaxyLaser.a.f;
import com.GalaxyLaser.opening.TitleActivity;
import com.GalaxyLaser.util.BaseActivity;
import java.util.ArrayList;
import jp.a.a.a.a.d;

public class HighScoreBaseActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public ListView b;
    /* access modifiers changed from: private */
    public l c;
    /* access modifiers changed from: private */
    public ArrayList d;

    /* access modifiers changed from: protected */
    public final void a(int i) {
        if (this.d != null) {
            this.d.clear();
        } else {
            this.d = new ArrayList();
        }
        d dVar = new d(i);
        dVar.a(this);
        dVar.b();
        dVar.a(new c(this, dVar));
        dVar.execute(new Void[0]);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            switch (keyEvent.getKeyCode()) {
                case 4:
                    return true;
            }
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(C0000R.menu.menu, menu);
        return true;
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case C0000R.id.finish /*2131230754*/:
                f.a(getApplication().getApplicationContext()).a(com.GalaxyLaser.util.f.Title_SE);
                startActivity(new Intent(this, TitleActivity.class));
                finish();
                return true;
            case C0000R.id.about /*2131230755*/:
                b();
                return true;
            case C0000R.id.applink /*2131230756*/:
                d();
                return true;
            case C0000R.id.satbox /*2131230757*/:
                c();
                return true;
            default:
                return true;
        }
    }
}
