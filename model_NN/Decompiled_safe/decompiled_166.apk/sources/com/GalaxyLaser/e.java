package com.GalaxyLaser;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

final class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NightMareHighScoreActivity f24a;

    e(NightMareHighScoreActivity nightMareHighScoreActivity) {
        this.f24a = nightMareHighScoreActivity;
    }

    public final void onClick(View view) {
        try {
            this.f24a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=jp.co.arttec.satbox.galaxylaser_act2")));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }
}
