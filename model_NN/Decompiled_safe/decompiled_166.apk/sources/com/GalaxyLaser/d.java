package com.GalaxyLaser;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

final class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NightMareHighScoreActivity f23a;

    d(NightMareHighScoreActivity nightMareHighScoreActivity) {
        this.f23a = nightMareHighScoreActivity;
    }

    public final void onClick(View view) {
        try {
            this.f23a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=jp.co.arttec.satbox.galaxylaser_vsboss")));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }
}
