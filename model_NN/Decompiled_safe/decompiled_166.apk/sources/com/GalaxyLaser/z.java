package com.GalaxyLaser;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

final class z implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GameActivity f78a;
    private final /* synthetic */ int b;
    private final /* synthetic */ int c;

    z(GameActivity gameActivity, int i, int i2) {
        this.f78a = gameActivity;
        this.b = i;
        this.c = i2;
    }

    public final void run() {
        Bitmap decodeResource;
        Bitmap decodeResource2;
        Bitmap decodeResource3 = this.f78a.h ? BitmapFactory.decodeResource(this.f78a.getResources(), C0000R.drawable.shield_ally_nightmare) : BitmapFactory.decodeResource(this.f78a.getResources(), C0000R.drawable.shield_ally);
        Bitmap createBitmap = Bitmap.createBitmap(decodeResource3.getWidth(), decodeResource3.getHeight(), Bitmap.Config.ARGB_8888);
        switch (this.b) {
            case 0:
                decodeResource = null;
                break;
            case 1:
                decodeResource = BitmapFactory.decodeResource(this.f78a.getResources(), C0000R.drawable.shield1);
                break;
            case 2:
                decodeResource = BitmapFactory.decodeResource(this.f78a.getResources(), C0000R.drawable.shield2);
                break;
            case 3:
                decodeResource = BitmapFactory.decodeResource(this.f78a.getResources(), C0000R.drawable.shield3);
                break;
            case 4:
                decodeResource = BitmapFactory.decodeResource(this.f78a.getResources(), C0000R.drawable.shield4);
                break;
            case 5:
                decodeResource = BitmapFactory.decodeResource(this.f78a.getResources(), C0000R.drawable.shield5);
                break;
            case 6:
                decodeResource = BitmapFactory.decodeResource(this.f78a.getResources(), C0000R.drawable.shield6);
                break;
            default:
                decodeResource = null;
                break;
        }
        switch (this.c) {
            case 4:
                decodeResource2 = BitmapFactory.decodeResource(this.f78a.getResources(), C0000R.drawable.speed5);
                break;
            case 5:
                decodeResource2 = BitmapFactory.decodeResource(this.f78a.getResources(), C0000R.drawable.speed4);
                break;
            case 6:
                decodeResource2 = BitmapFactory.decodeResource(this.f78a.getResources(), C0000R.drawable.speed3);
                break;
            case 7:
                decodeResource2 = BitmapFactory.decodeResource(this.f78a.getResources(), C0000R.drawable.speed2);
                break;
            case 8:
                decodeResource2 = BitmapFactory.decodeResource(this.f78a.getResources(), C0000R.drawable.speed1);
                break;
            default:
                decodeResource2 = null;
                break;
        }
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(decodeResource3, 0.0f, 0.0f, (Paint) null);
        if (decodeResource != null) {
            canvas.drawBitmap(decodeResource, 0.0f, 0.0f, (Paint) null);
        }
        if (decodeResource2 != null) {
            canvas.drawBitmap(decodeResource2, 0.0f, 0.0f, (Paint) null);
        }
        this.f78a.b.setImageBitmap(createBitmap);
    }
}
