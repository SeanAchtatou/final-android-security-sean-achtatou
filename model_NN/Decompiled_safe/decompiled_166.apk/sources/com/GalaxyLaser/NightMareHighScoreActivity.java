package com.GalaxyLaser;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class NightMareHighScoreActivity extends HighScoreBaseActivity {
    private ImageButton b;
    private ImageButton c;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.nightmare);
        ((TextView) findViewById(C0000R.id.title)).setText((int) C0000R.string.nightmare);
        a(31);
        this.b = (ImageButton) findViewById(C0000R.id.link_vsboss);
        this.b.setOnClickListener(new d(this));
        this.c = (ImageButton) findViewById(C0000R.id.link_act2);
        this.c.setOnClickListener(new e(this));
        ((Button) findViewById(C0000R.id.ok)).setOnClickListener(new f(this));
    }
}
