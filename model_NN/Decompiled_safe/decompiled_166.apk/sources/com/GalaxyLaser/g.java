package com.GalaxyLaser;

import android.content.Intent;
import android.view.View;
import com.GalaxyLaser.a.f;
import com.GalaxyLaser.opening.TitleActivity;

final class g implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HighScoreActivity f26a;
    private final /* synthetic */ boolean b;

    g(HighScoreActivity highScoreActivity, boolean z) {
        this.f26a = highScoreActivity;
        this.b = z;
    }

    public final void onClick(View view) {
        f.a(this.f26a.getApplication().getApplicationContext()).a(com.GalaxyLaser.util.f.Title_SE);
        Intent intent = new Intent();
        if (this.b) {
            intent.setClass(this.f26a, NightMareHighScoreActivity.class);
        } else {
            intent.setClass(this.f26a, TitleActivity.class);
        }
        this.f26a.startActivity(intent);
        this.f26a.finish();
    }
}
