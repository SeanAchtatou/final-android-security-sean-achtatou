package com.GalaxyLaser;

public final class j implements Comparable {

    /* renamed from: a  reason: collision with root package name */
    private int f29a;
    private String b;
    private String c;
    private String d;

    public j(int i, String str, String str2, String str3) {
        this.f29a = i;
        this.b = str;
        this.c = str2;
        this.d = str3;
    }

    public final int a() {
        return this.f29a;
    }

    public final String b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }

    public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return 0;
    }

    public final String d() {
        return this.d;
    }
}
