package com.GalaxyLaser;

import android.content.DialogInterface;
import android.content.Intent;
import com.GalaxyLaser.a.f;

final class m implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GameActivity f32a;

    m(GameActivity gameActivity) {
        this.f32a = gameActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        f.a(this.f32a.getApplication().getApplicationContext()).a(com.GalaxyLaser.util.f.Title_SE);
        Intent intent = new Intent();
        intent.putExtra("Stage", this.f32a.j);
        this.f32a.setResult(-1, intent);
        this.f32a.finish();
    }
}
