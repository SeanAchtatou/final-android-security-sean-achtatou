package com.GalaxyLaser;

import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import com.GalaxyLaser.a.f;
import com.GalaxyLaser.a.i;

final class r implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GameActivity f52a;
    private final /* synthetic */ View b;

    r(GameActivity gameActivity, View view) {
        this.f52a = gameActivity;
        this.b = view;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        f.a(this.f52a.getApplication().getApplicationContext()).a(com.GalaxyLaser.util.f.Title_SE);
        EditText editText = (EditText) this.b.findViewById(C0000R.id.dialog_edittext);
        i.a(this.f52a.getApplication().getApplicationContext()).a(editText.getText().toString());
        GameActivity.a(this.f52a, editText.getText().toString());
    }
}
