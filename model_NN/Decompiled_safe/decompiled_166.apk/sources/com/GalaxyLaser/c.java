package com.GalaxyLaser;

import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.util.List;
import jp.a.a.a.a.b;
import jp.a.a.a.a.d;

final class c implements b {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HighScoreBaseActivity f21a;
    private final /* synthetic */ d b;

    c(HighScoreBaseActivity highScoreBaseActivity, d dVar) {
        this.f21a = highScoreBaseActivity;
        this.b = dVar;
    }

    public final void a(boolean z) {
        Toast makeText;
        int i = 1;
        if (z) {
            Toast makeText2 = Toast.makeText(this.f21a, "High Score Rankings acquires succesful.", 1);
            List<jp.a.a.a.a.c> a2 = this.b.a();
            if (a2 != null) {
                for (jp.a.a.a.a.c cVar : a2) {
                    this.f21a.d.add(new b(i, cVar.a(), cVar.b()));
                    i++;
                }
                this.f21a.c = new l(this.f21a, this.f21a.d);
                this.f21a.b = (ListView) this.f21a.findViewById(C0000R.id.world_rank);
                this.f21a.b.setAdapter((ListAdapter) this.f21a.c);
                this.f21a.b.setScrollingCacheEnabled(false);
                makeText = makeText2;
            } else {
                makeText = makeText2;
            }
        } else {
            makeText = Toast.makeText(this.f21a, "High Score Rankings acquires failure.", 1);
        }
        makeText.show();
    }
}
