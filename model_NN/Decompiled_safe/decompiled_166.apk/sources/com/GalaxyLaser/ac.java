package com.GalaxyLaser;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

final class ac implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ GameRankMy f15a;

    ac(GameRankMy gameRankMy) {
        this.f15a = gameRankMy;
    }

    public final void onClick(View view) {
        try {
            this.f15a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=jp.co.arttec.satbox.galaxylaser_vsboss")));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }
}
