package com.GalaxyLaser.c;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.util.a;

public final class t extends s {
    private Context f;
    private int[] g = {C0000R.drawable.explode_rock00, C0000R.drawable.explode_rock01, C0000R.drawable.explode_rock02, C0000R.drawable.explode_rock03, C0000R.drawable.explode_rock04, C0000R.drawable.explode_rock05, C0000R.drawable.explode_rock06, C0000R.drawable.explode_rock07, C0000R.drawable.explode_rock08, C0000R.drawable.explode_rock09, C0000R.drawable.explode_rock10, C0000R.drawable.explode_rock11, C0000R.drawable.explode_rock12, C0000R.drawable.explode_rock13, C0000R.drawable.explode_rock14, C0000R.drawable.explode_dummy};

    public t(a aVar, Context context) {
        super(aVar, context);
        this.f = context;
        a(context.getResources(), this.g[0]);
        a(this.g.length);
    }

    public final void a(Canvas canvas) {
        super.a(canvas);
    }

    public final void b() {
        super.b();
        Resources resources = this.f.getResources();
        if (this.g.length > c()) {
            a(resources, this.g[c()]);
        } else {
            a(resources, this.g[this.g.length - 1]);
        }
    }
}
