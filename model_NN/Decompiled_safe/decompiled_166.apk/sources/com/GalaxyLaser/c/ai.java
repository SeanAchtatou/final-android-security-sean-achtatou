package com.GalaxyLaser.c;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.a.a;
import com.GalaxyLaser.a.d;
import com.GalaxyLaser.a.k;
import com.GalaxyLaser.util.e;
import com.GalaxyLaser.util.i;

public final class ai extends n {
    private int h;
    private Context i;

    public ai(Rect rect, Context context) {
        super(rect);
        this.f = rect;
        this.i = context;
        if (a.a().d()) {
            a(context.getResources(), C0000R.drawable.enemy5n);
        } else {
            a(context.getResources(), C0000R.drawable.enemy5);
        }
        c();
        this.f22a.f57a = e.a().nextInt(rect.right - f().f59a);
        this.f22a.b = rect.bottom - 1;
        this.d = 1;
        a(i.Type5);
        this.g = 4;
        this.e = 500;
        this.h = 0;
        w wVar = new w(new com.GalaxyLaser.util.a(this.f22a.f57a, this.f22a.b - 40), "WARNING");
        wVar.c();
        d.a(this.i).a(wVar);
        k.a(this.i).a(com.GalaxyLaser.util.d.Warning);
    }

    public final void a(Canvas canvas) {
        if (this.h > 30) {
            super.a(canvas);
        }
    }

    public final void b() {
        this.h++;
        if (this.h > 30) {
            super.b();
        }
        if (this.f22a.b < 0) {
            this.f22a.b = 0;
            this.b.b = -this.b.b;
        }
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.b.f70a = 0;
        this.b.b = -20;
    }
}
