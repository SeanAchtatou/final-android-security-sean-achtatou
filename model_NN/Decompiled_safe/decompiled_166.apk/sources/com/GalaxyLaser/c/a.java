package com.GalaxyLaser.c;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.a.j;
import com.GalaxyLaser.util.c;

public final class a extends g {
    private int f = 1;
    private int g;
    private Context h;
    private Bitmap i;
    private Bitmap j;
    private Bitmap k;
    private Bitmap l;
    private Bitmap m;
    private Bitmap n;
    private Bitmap o;
    private Bitmap p;
    private Bitmap q;
    private Bitmap r;
    private Bitmap s;
    private Bitmap t;
    private Bitmap u;
    private Bitmap v;
    private Bitmap w;
    private Bitmap x;
    private boolean y = false;
    private int z = 0;

    public a(Context context) {
        this.h = context;
        this.d = 3;
        Resources resources = this.h.getResources();
        j a2 = j.a();
        if (a2 != null) {
            this.i = a2.a((int) C0000R.drawable.ally);
            this.j = a2.a((int) C0000R.drawable.ally_shield);
            this.k = a2.a((int) C0000R.drawable.ally_left);
            this.l = a2.a((int) C0000R.drawable.ally_right);
            this.p = a2.a((int) C0000R.drawable.ally_nightmare);
            this.q = a2.a((int) C0000R.drawable.ally_nightmare_shield);
            this.r = a2.a((int) C0000R.drawable.ally_nightmare_left);
            this.s = a2.a((int) C0000R.drawable.ally_nightmare_right);
            this.m = a2.a((int) C0000R.drawable.ally_s);
            this.n = a2.a((int) C0000R.drawable.ally_left_s);
            this.o = a2.a((int) C0000R.drawable.ally_right_s);
            this.t = a2.a((int) C0000R.drawable.ally_nightmare_s);
            this.u = a2.a((int) C0000R.drawable.ally_nightmare_left_s);
            this.v = a2.a((int) C0000R.drawable.ally_nightmare_right_s);
        } else {
            this.i = BitmapFactory.decodeResource(resources, C0000R.drawable.ally);
            this.j = BitmapFactory.decodeResource(resources, C0000R.drawable.ally_shield);
            this.k = BitmapFactory.decodeResource(resources, C0000R.drawable.ally_left);
            this.l = BitmapFactory.decodeResource(resources, C0000R.drawable.ally_right);
            this.p = BitmapFactory.decodeResource(resources, C0000R.drawable.ally_nightmare);
            this.q = BitmapFactory.decodeResource(resources, C0000R.drawable.ally_nightmare_shield);
            this.r = BitmapFactory.decodeResource(resources, C0000R.drawable.ally_nightmare_left);
            this.s = BitmapFactory.decodeResource(resources, C0000R.drawable.ally_nightmare_right);
            this.m = BitmapFactory.decodeResource(resources, C0000R.drawable.ally_s);
            this.n = BitmapFactory.decodeResource(resources, C0000R.drawable.ally_left_s);
            this.o = BitmapFactory.decodeResource(resources, C0000R.drawable.ally_right_s);
            this.t = BitmapFactory.decodeResource(resources, C0000R.drawable.ally_nightmare_s);
            this.u = BitmapFactory.decodeResource(resources, C0000R.drawable.ally_nightmare_left_s);
            this.v = BitmapFactory.decodeResource(resources, C0000R.drawable.ally_nightmare_right_s);
        }
        this.x = BitmapFactory.decodeResource(resources, C0000R.drawable.ally_dummy);
    }

    public final void a() {
        this.y = true;
        this.z = 0;
    }

    public final void a(int i2) {
        this.f = i2;
    }

    public final void a(Canvas canvas) {
        canvas.drawBitmap(this.w, (float) this.f22a.f57a, (float) this.f22a.b, (Paint) null);
        if (this.y) {
            this.z++;
            if (30 < this.z) {
                this.y = false;
            }
        }
    }

    public final void b() {
    }

    public final void b(int i2) {
        this.g = i2;
        this.w = Bitmap.createBitmap(this.i.getWidth(), this.i.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(this.w);
        if (16 == (i2 & 16)) {
            if (1 == (i2 & 1)) {
                if (this.y) {
                    canvas.drawBitmap(this.t, 0.0f, 0.0f, (Paint) null);
                } else {
                    canvas.drawBitmap(this.p, 0.0f, 0.0f, (Paint) null);
                }
            } else if (2 == (i2 & 2)) {
                if (this.y) {
                    canvas.drawBitmap(this.u, 0.0f, 0.0f, (Paint) null);
                } else {
                    canvas.drawBitmap(this.r, 0.0f, 0.0f, (Paint) null);
                }
            } else if (4 != (i2 & 4)) {
                canvas.drawBitmap(this.x, 0.0f, 0.0f, (Paint) null);
            } else if (this.y) {
                canvas.drawBitmap(this.v, 0.0f, 0.0f, (Paint) null);
            } else {
                canvas.drawBitmap(this.s, 0.0f, 0.0f, (Paint) null);
            }
            if (this.d > 0) {
                canvas.drawBitmap(this.q, 0.0f, 0.0f, (Paint) null);
            }
        } else {
            if (1 == (i2 & 1)) {
                if (this.y) {
                    canvas.drawBitmap(this.m, 0.0f, 0.0f, (Paint) null);
                } else {
                    canvas.drawBitmap(this.i, 0.0f, 0.0f, (Paint) null);
                }
            } else if (2 == (i2 & 2)) {
                if (this.y) {
                    canvas.drawBitmap(this.n, 0.0f, 0.0f, (Paint) null);
                } else {
                    canvas.drawBitmap(this.k, 0.0f, 0.0f, (Paint) null);
                }
            } else if (4 != (i2 & 4)) {
                canvas.drawBitmap(this.x, 0.0f, 0.0f, (Paint) null);
            } else if (this.y) {
                canvas.drawBitmap(this.o, 0.0f, 0.0f, (Paint) null);
            } else {
                canvas.drawBitmap(this.l, 0.0f, 0.0f, (Paint) null);
            }
            if (this.d > 0) {
                canvas.drawBitmap(this.j, 0.0f, 0.0f, (Paint) null);
            }
        }
        if (this.c == null) {
            this.c = new c(this.w.getWidth(), this.w.getHeight());
            return;
        }
        this.c.f59a = this.w.getWidth();
        this.c.b = this.w.getHeight();
    }

    public final int c() {
        return this.f;
    }

    public final void c(int i2) {
        this.d = i2;
    }
}
