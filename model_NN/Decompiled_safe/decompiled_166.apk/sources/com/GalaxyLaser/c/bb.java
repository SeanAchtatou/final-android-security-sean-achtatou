package com.GalaxyLaser.c;

import android.content.Context;
import android.graphics.Rect;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.a.a;
import com.GalaxyLaser.util.e;
import com.GalaxyLaser.util.i;

public final class bb extends n {
    private Rect h;
    private int i;

    public bb(Rect rect, Context context) {
        super(rect);
        if (a.a().d()) {
            a(context.getResources(), C0000R.drawable.enemy_big2n);
        } else {
            a(context.getResources(), C0000R.drawable.enemy_big2);
        }
        this.f22a.f57a = (rect.right - f().f59a) / 2;
        this.f22a.b = -this.c.b;
        c();
        this.d = (a.a().b() * 30) + 400;
        a(i.Boss3);
        this.e = a.a().b() * 5000;
        this.h = rect;
        if (a.a().b() >= 7) {
            this.g = 6;
        } else {
            this.g = a.a().b() + 6;
        }
        this.i = 0;
    }

    public final void b() {
        c();
        super.b();
        if (this.i != 0) {
            if (this.f22a.f57a < 0) {
                this.f22a.f57a = 0;
                if (e.a().nextInt(2) == 0) {
                    this.i = 2;
                } else {
                    this.i = 4;
                }
            }
            if (this.f22a.f57a > this.h.right - this.c.f59a) {
                this.f22a.f57a = this.h.right - this.c.f59a;
                if (e.a().nextInt(2) == 0) {
                    this.i = 1;
                } else {
                    this.i = 3;
                }
            }
            if (this.f22a.b < 0) {
                this.f22a.b = 0;
                if (e.a().nextInt(2) == 0) {
                    this.i = 2;
                } else {
                    this.i = 1;
                }
            }
            if (this.f22a.b > this.h.top + (this.c.b / 2)) {
                this.f22a.b = this.h.top + (this.c.b / 2);
                if (e.a().nextInt(2) == 0) {
                    this.i = 4;
                } else {
                    this.i = 3;
                }
            }
        } else if (this.f22a.b + this.b.b >= this.h.top + (this.c.b / 2)) {
            this.i = 1;
        }
    }

    /* access modifiers changed from: protected */
    public final void c() {
        switch (this.i) {
            case 0:
                this.b.f70a = 0;
                this.b.b = 5;
                return;
            case 1:
                this.b.f70a = -12;
                this.b.b = 8;
                return;
            case 2:
                this.b.f70a = 12;
                this.b.b = 8;
                return;
            case 3:
                this.b.f70a = -8;
                this.b.b = -12;
                return;
            case 4:
                this.b.f70a = 8;
                this.b.b = -12;
                return;
            default:
                return;
        }
    }
}
