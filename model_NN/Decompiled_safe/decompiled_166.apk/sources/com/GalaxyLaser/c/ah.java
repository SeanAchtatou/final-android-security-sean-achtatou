package com.GalaxyLaser.c;

import android.content.Context;
import android.graphics.Rect;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.a.a;
import com.GalaxyLaser.util.e;
import com.GalaxyLaser.util.i;

public final class ah extends n {
    private int h;
    private Rect i;
    private int j;
    private Context k;

    public ah(Rect rect, Context context) {
        super(rect);
        this.i = rect;
        this.k = context;
        if (a.a().d()) {
            a(context.getResources(), C0000R.drawable.enemy4n);
        } else {
            a(context.getResources(), C0000R.drawable.enemy4);
        }
        c();
        this.f22a.f57a = e.a().nextInt(rect.right - f().f59a);
        this.f22a.b = -f().b;
        this.d = 2;
        a(i.Type4);
        this.g = 3;
        this.e = 300;
        this.h = e.a().nextInt(2);
        if (this.h == 0) {
            this.h = -1;
        }
        this.j = 5;
    }

    public final void b() {
        c();
        this.j++;
        super.b();
        if (this.f22a.f57a < 0) {
            this.f22a.f57a = 0;
            this.h = -this.h;
            this.j = 0;
        }
        if (this.f22a.f57a > this.i.right - f().f59a) {
            this.f22a.f57a = this.i.right - f().f59a;
            this.h = -this.h;
            this.j = 0;
        }
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (this.j > 5) {
            this.b.f70a = this.h * 25;
            this.b.b = 0;
            if (this.h < 0) {
                if (a.a().d()) {
                    a(this.k.getResources(), C0000R.drawable.enemy4n_left);
                } else {
                    a(this.k.getResources(), C0000R.drawable.enemy4_left);
                }
            } else if (a.a().d()) {
                a(this.k.getResources(), C0000R.drawable.enemy4n_right);
            } else {
                a(this.k.getResources(), C0000R.drawable.enemy4_right);
            }
        } else {
            this.b.f70a = 0;
            this.b.b = 20;
            if (a.a().d()) {
                a(this.k.getResources(), C0000R.drawable.enemy4n);
            } else {
                a(this.k.getResources(), C0000R.drawable.enemy4);
            }
        }
    }
}
