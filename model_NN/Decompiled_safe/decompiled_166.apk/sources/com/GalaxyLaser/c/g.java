package com.GalaxyLaser.c;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.GalaxyLaser.a.j;
import com.GalaxyLaser.util.a;
import com.GalaxyLaser.util.c;
import com.GalaxyLaser.util.o;

public abstract class g {

    /* renamed from: a  reason: collision with root package name */
    protected a f22a = new a();
    protected o b = new o();
    protected c c = null;
    protected int d;
    protected int e = 0;
    private Bitmap f = null;
    private String g;
    private int h;
    private int i = 0;

    private void a() {
        if (!this.f.isRecycled()) {
            this.f.recycle();
            this.f = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0014, code lost:
        if (r3.f == null) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.content.res.Resources r4, int r5) {
        /*
            r3 = this;
            r3.i = r5
            com.GalaxyLaser.a.j r0 = com.GalaxyLaser.a.j.a()
            if (r0 == 0) goto L_0x0016
            com.GalaxyLaser.a.j r0 = com.GalaxyLaser.a.j.a()
            android.graphics.Bitmap r0 = r0.a(r5)
            r3.f = r0
            android.graphics.Bitmap r0 = r3.f
            if (r0 != 0) goto L_0x001c
        L_0x0016:
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r4, r5)
            r3.f = r0
        L_0x001c:
            com.GalaxyLaser.util.c r0 = r3.c
            if (r0 != 0) goto L_0x0034
            com.GalaxyLaser.util.c r0 = new com.GalaxyLaser.util.c
            android.graphics.Bitmap r1 = r3.f
            int r1 = r1.getWidth()
            android.graphics.Bitmap r2 = r3.f
            int r2 = r2.getHeight()
            r0.<init>(r1, r2)
            r3.c = r0
        L_0x0033:
            return
        L_0x0034:
            com.GalaxyLaser.util.c r0 = r3.c
            android.graphics.Bitmap r1 = r3.f
            int r1 = r1.getWidth()
            r0.f59a = r1
            com.GalaxyLaser.util.c r0 = r3.c
            android.graphics.Bitmap r1 = r3.f
            int r1 = r1.getHeight()
            r0.b = r1
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.GalaxyLaser.c.g.a(android.content.res.Resources, int):void");
    }

    public void a(Canvas canvas) {
        canvas.drawBitmap(this.f, (float) this.f22a.f57a, (float) this.f22a.b, (Paint) null);
    }

    public final void a(a aVar) {
        this.f22a = aVar;
    }

    public final void a(String str) {
        this.g = str;
    }

    public void b() {
        this.f22a.f57a += this.b.f70a;
        this.f22a.b += this.b.b;
    }

    public final int d() {
        return this.h;
    }

    public final void d(int i2) {
        this.d += i2;
    }

    public final a e() {
        return this.f22a;
    }

    public final void e(int i2) {
        this.h = i2;
    }

    public final c f() {
        return this.c;
    }

    public final String g() {
        return this.g;
    }

    public final int h() {
        return this.d;
    }

    public int i() {
        return this.e;
    }

    public final void j() {
        if (this.f != null) {
            j a2 = j.a();
            if (a2 == null) {
                a();
            } else if (!a2.b(this.i)) {
                a();
            }
        }
    }
}
