package com.GalaxyLaser.c;

import android.content.Context;
import android.graphics.Rect;
import com.GalaxyLaser.util.a;
import com.GalaxyLaser.util.e;
import com.GalaxyLaser.util.i;

public class as extends g {
    private boolean f;
    private i g;

    public as(Rect rect, Context context) {
        a(context);
        this.f22a.f57a = e.a().nextInt(rect.right - f().f59a);
        this.f22a.b = -f().b;
    }

    public as(a aVar, Context context) {
        a(context);
        this.f22a.f57a = aVar.f57a;
        this.f22a.b = aVar.b;
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        this.b.f70a = 0;
        this.b.b = e.a().nextInt(3) + 2;
        this.f = true;
        this.g = i.Rock1;
    }

    public final void a(i iVar) {
        this.g = iVar;
    }

    public final boolean a() {
        return this.f;
    }

    public final void c() {
        this.f = false;
    }

    public final i k() {
        return this.g;
    }
}
