package com.GalaxyLaser.c;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.util.a;

public final class k extends s {
    private Context f;
    private int[] g = {C0000R.drawable.explosion_big_black00, C0000R.drawable.explosion_big_black01, C0000R.drawable.explosion_big_black02, C0000R.drawable.explosion_big_black03, C0000R.drawable.explosion_big_black04, C0000R.drawable.explosion_big_black05, C0000R.drawable.explosion_big_black06, C0000R.drawable.explosion_big_black07, C0000R.drawable.explosion_big_black08, C0000R.drawable.explosion_big_black09, C0000R.drawable.explosion_big_black10, C0000R.drawable.explosion_big_black11, C0000R.drawable.explosion_big_black12, C0000R.drawable.explosion_big_black13, C0000R.drawable.explosion_big_black14, C0000R.drawable.explosion_big_black15, C0000R.drawable.explosion_big_black16, C0000R.drawable.explosion_big_black17, C0000R.drawable.explosion_big_black18, C0000R.drawable.explosion_big_black19, C0000R.drawable.explosion_big_black20, C0000R.drawable.explosion_big_black21, C0000R.drawable.explosion_big_black22, C0000R.drawable.explosion_big_black23, C0000R.drawable.explode_dummy};

    public k(a aVar, Context context) {
        super(aVar, context);
        this.f = context;
        a(context.getResources(), this.g[0]);
        a(this.g.length);
    }

    public final void a(Canvas canvas) {
        super.a(canvas);
    }

    public final void b() {
        super.b();
        Resources resources = this.f.getResources();
        if (this.g.length > c()) {
            a(resources, this.g[c()]);
        } else {
            a(resources, this.g[this.g.length - 1]);
        }
    }
}
