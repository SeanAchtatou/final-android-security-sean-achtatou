package com.GalaxyLaser.c;

import android.content.Context;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.util.a;

public class s extends g {
    private int f = 0;
    private int g;

    public s(a aVar, Context context) {
        this.f22a.f57a = aVar.f57a;
        this.f22a.b = aVar.b;
        a(context.getResources(), C0000R.drawable.explode_dummy);
        this.g = 5;
    }

    public final void a(int i) {
        this.g = i;
    }

    public final boolean a() {
        return this.f < this.g;
    }

    public void b() {
        this.f++;
    }

    public final int c() {
        return this.f;
    }
}
