package com.GalaxyLaser.c;

import android.content.Context;
import android.graphics.Rect;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.util.a;

public final class z extends au {
    public z(Rect rect, Context context) {
        super(rect, context);
        a(context);
    }

    public z(a aVar, Context context) {
        super(aVar, context);
        a(context);
    }

    /* access modifiers changed from: protected */
    public final void a(Context context) {
        super.a(context);
        a(context.getResources(), C0000R.drawable.planet1);
    }
}
