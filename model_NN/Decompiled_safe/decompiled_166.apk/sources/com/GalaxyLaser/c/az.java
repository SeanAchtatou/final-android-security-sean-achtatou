package com.GalaxyLaser.c;

import android.content.Context;
import android.graphics.Rect;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.a.a;
import com.GalaxyLaser.util.i;

public final class az extends n {
    private int h;
    private Rect i;

    public az(Rect rect, Context context) {
        super(rect);
        if (a.a().d()) {
            a(context.getResources(), C0000R.drawable.enemy_boss2n);
        } else {
            a(context.getResources(), C0000R.drawable.enemy_boss2);
        }
        c();
        this.f22a.f57a = (rect.right - f().f59a) / 2;
        this.f22a.b = -this.c.b;
        this.d = (a.a().b() * 30) + 450;
        this.h = 0;
        a(i.Boss5);
        this.e = a.a().b() * 20000;
        this.i = rect;
        if (a.a().b() >= 7) {
            this.g = 16;
        } else {
            this.g = a.a().b() + 10;
        }
    }

    public final void b() {
        this.h++;
        c();
        super.b();
        if (this.f22a.f57a < 0) {
            this.f22a.f57a = 0;
        }
        if (this.f22a.f57a > this.i.right - f().f59a) {
            this.f22a.f57a = this.i.right - f().f59a;
        }
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (this.h < 50) {
            this.b.f70a = 0;
            this.b.b = 4;
        } else if (this.h < 120) {
            this.b.f70a = -10;
            this.b.b = 0;
        } else if (this.h < 200) {
            this.b.f70a = 10;
            this.b.b = 0;
        } else if (this.h < 300) {
            this.b.f70a = -10;
            this.b.b = 0;
            if (this.f22a.f57a < (this.i.right - f().f59a) / 2) {
                this.f22a.f57a = (this.i.right - f().f59a) / 2;
                this.h = 300;
            }
        } else if (this.h < 400) {
            this.b.f70a = 0;
            this.b.b = 10;
            if (this.f22a.b > (-f().b) / 5) {
                this.h = 400;
            }
        } else if (this.h < 500) {
            this.b.f70a = -10;
            this.b.b = 0;
        } else if (this.h < 600) {
            this.b.f70a = 10;
            this.b.b = 0;
        } else if (this.h < 700) {
            this.b.f70a = -10;
            this.b.b = 0;
            if (this.f22a.f57a < (this.i.right - f().f59a) / 2) {
                this.f22a.f57a = (this.i.right - f().f59a) / 2;
                this.h = 700;
            }
        } else if (this.h < 800) {
            this.b.f70a = 0;
            this.b.b = -6;
            if (this.f22a.b < (-f().b)) {
                this.f22a.b = -f().b;
                this.h = 0;
            }
        }
    }
}
