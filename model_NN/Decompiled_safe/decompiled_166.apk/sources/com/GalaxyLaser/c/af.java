package com.GalaxyLaser.c;

import android.content.Context;
import android.graphics.Rect;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.a.a;
import com.GalaxyLaser.util.e;
import com.GalaxyLaser.util.i;

public final class af extends n {
    private int h;

    public af(Rect rect, Context context) {
        super(rect);
        if (a.a().d()) {
            a(context.getResources(), C0000R.drawable.enemy2n);
        } else {
            a(context.getResources(), C0000R.drawable.enemy2);
        }
        c();
        this.f22a.f57a = e.a().nextInt(rect.right - f().f59a);
        this.f22a.b = -f().b;
        this.d = 1;
        this.h = 0;
        this.g = 4;
        a(i.Type2);
        this.e = 200;
    }

    public final void b() {
        this.h++;
        c();
        super.b();
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (this.h < 15) {
            this.b.f70a = 0;
            this.b.b = 8;
        } else if (this.h < 105) {
            this.b.f70a = 0;
            this.b.b = 0;
        } else {
            this.b.f70a = 0;
            this.b.b = -8;
        }
    }
}
