package com.GalaxyLaser.c;

import android.content.Context;
import android.graphics.Rect;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.a.a;
import com.GalaxyLaser.util.e;
import com.GalaxyLaser.util.i;

public final class ag extends n {
    private int h;
    private a i;
    private boolean j;

    public ag(Rect rect, Context context, a aVar) {
        super(rect);
        if (a.a().d()) {
            a(context.getResources(), C0000R.drawable.enemy3n);
        } else {
            a(context.getResources(), C0000R.drawable.enemy3);
        }
        c();
        this.f22a.f57a = e.a().nextInt(rect.right - f().f59a);
        this.f22a.b = -f().b;
        this.d = 1;
        this.h = 0;
        a(i.Type3);
        this.g = 3;
        this.e = 400;
        this.i = aVar;
        this.j = false;
    }

    public final void b() {
        this.h++;
        c();
        super.b();
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (this.h < 60) {
            this.b.f70a = 0;
            this.b.b = 2;
        } else if (this.i != null && !this.j) {
            this.j = true;
            com.GalaxyLaser.util.a e = this.i.e();
            int sqrt = (int) Math.sqrt((double) (((e.f57a - this.f22a.f57a) * (e.f57a - this.f22a.f57a)) + ((e.b - this.f22a.b) * (e.b - this.f22a.b))));
            if (sqrt == 0) {
                this.b.f70a = 0;
                this.b.b = 20;
                return;
            }
            this.b.f70a = ((e.f57a - this.f22a.f57a) * 20) / sqrt;
            this.b.b = ((e.b - this.f22a.b) * 20) / sqrt;
        }
    }
}
