package com.GalaxyLaser.c;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.util.a;

public final class o extends s {
    private Context f;
    private int[] g = {C0000R.drawable.ally_explode00, C0000R.drawable.ally_explode01, C0000R.drawable.ally_explode02, C0000R.drawable.ally_explode03, C0000R.drawable.ally_explode04, C0000R.drawable.ally_explode05, C0000R.drawable.ally_explode06, C0000R.drawable.ally_explode07, C0000R.drawable.ally_explode08, C0000R.drawable.ally_explode09, C0000R.drawable.ally_explode10, C0000R.drawable.ally_explode11, C0000R.drawable.ally_explode12, C0000R.drawable.ally_explode13, C0000R.drawable.ally_explode14, C0000R.drawable.ally_explode15, C0000R.drawable.ally_explode16, C0000R.drawable.ally_explode17, C0000R.drawable.ally_explode18, C0000R.drawable.ally_explode19, C0000R.drawable.ally_explode20, C0000R.drawable.ally_explode21, C0000R.drawable.ally_explode22, C0000R.drawable.ally_explode23, C0000R.drawable.ally_explode24, C0000R.drawable.ally_explode25, C0000R.drawable.ally_explode26, C0000R.drawable.ally_explode27, C0000R.drawable.ally_explode28, C0000R.drawable.ally_explode29, C0000R.drawable.explode_dummy};

    public o(a aVar, Context context) {
        super(aVar, context);
        this.f = context;
        a(context.getResources(), this.g[0]);
        a(this.g.length);
    }

    public final void a(Canvas canvas) {
        super.a(canvas);
    }

    public final void b() {
        super.b();
        Resources resources = this.f.getResources();
        if (this.g.length > c()) {
            a(resources, this.g[c()]);
        } else {
            a(resources, this.g[this.g.length - 1]);
        }
    }
}
