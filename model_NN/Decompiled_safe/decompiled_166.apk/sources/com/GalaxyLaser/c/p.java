package com.GalaxyLaser.c;

import android.content.Context;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.util.a;
import com.GalaxyLaser.util.g;

public final class p extends u {
    public p(a aVar, Context context) {
        super(aVar);
        a(context.getResources(), C0000R.drawable.star2);
        a(g.Star_Orange);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.b.f70a = 0;
        this.b.b = -10;
    }

    public final void b() {
        super.b();
        this.b.b++;
    }
}
