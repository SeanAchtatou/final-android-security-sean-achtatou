package com.GalaxyLaser.c;

import android.content.Context;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.util.a;

public final class aq extends r {
    public aq(a aVar, n nVar, Context context) {
        a(context.getResources(), C0000R.drawable.enemy_bullet);
        this.f22a.f57a = (nVar.e().f57a + (nVar.f().f59a / 2)) - (f().f59a / 2);
        this.f22a.b = nVar.e().b + nVar.f().f59a;
        a aVar2 = aVar == null ? new a() : aVar.e();
        int sqrt = (int) Math.sqrt((double) (((aVar2.f57a - this.f22a.f57a) * (aVar2.f57a - this.f22a.f57a)) + ((aVar2.b - this.f22a.b) * (aVar2.b - this.f22a.b))));
        int k = k();
        if (sqrt == 0) {
            this.b.f70a = 0;
            this.b.b = k;
        } else {
            this.b.f70a = ((aVar2.f57a - this.f22a.f57a) * k) / sqrt;
            this.b.b = ((aVar2.b - this.f22a.b) * k) / sqrt;
        }
        this.d = 1;
    }
}
