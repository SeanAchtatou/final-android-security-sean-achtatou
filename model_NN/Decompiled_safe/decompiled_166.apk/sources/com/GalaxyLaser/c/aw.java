package com.GalaxyLaser.c;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.GalaxyLaser.util.a;
import com.GalaxyLaser.util.c;
import com.GalaxyLaser.util.e;

public final class aw extends g {
    private int f;

    public aw(Rect rect) {
        a();
        this.f22a.f57a = e.a().nextInt(rect.right);
        this.f22a.b = 0;
    }

    public aw(a aVar) {
        a();
        this.f22a.f57a = aVar.f57a;
        this.f22a.b = aVar.b;
    }

    private void a() {
        if (this.c == null) {
            this.c = new c();
        } else {
            c cVar = this.c;
            this.c.b = 2;
            cVar.f59a = 2;
        }
        this.b.f70a = 0;
        switch (e.a().nextInt(4)) {
            case 0:
                this.b.b = 4;
                this.f = -8355712;
                return;
            case 1:
                this.b.b = 5;
                this.f = -6250336;
                return;
            case 2:
                this.b.b = 6;
                this.f = -4144960;
                return;
            case 3:
                this.b.b = 7;
                this.f = -1;
                return;
            default:
                return;
        }
    }

    public final void a(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(this.f);
        paint.setAntiAlias(false);
        canvas.drawCircle((float) this.f22a.f57a, (float) this.f22a.b, 2.0f, paint);
    }
}
