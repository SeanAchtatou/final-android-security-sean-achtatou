package com.GalaxyLaser.c;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.GalaxyLaser.util.a;

public final class w extends g {
    private int f = 0;
    private int g;
    private int h;

    public w(a aVar, String str) {
        this.f22a.f57a = aVar.f57a;
        this.f22a.b = aVar.b;
        a(str);
        this.g = 15;
        this.h = 18;
    }

    public final void a(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setTextSize((float) this.h);
        paint.setAntiAlias(false);
        canvas.drawText(g(), (float) this.f22a.f57a, (float) this.f22a.b, paint);
    }

    public final boolean a() {
        return this.f < this.g;
    }

    public final void b() {
        this.f++;
    }

    public final void c() {
        this.h = 24;
    }
}
