package com.GalaxyLaser.c;

import android.content.Context;
import android.graphics.Rect;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.a.a;
import com.GalaxyLaser.util.e;
import com.GalaxyLaser.util.i;

public final class aj extends n {
    private int h;
    private Rect i;
    private int j;

    public aj(Rect rect, Context context) {
        super(rect);
        this.i = rect;
        if (a.a().d()) {
            a(context.getResources(), C0000R.drawable.enemy6n);
        } else {
            a(context.getResources(), C0000R.drawable.enemy6);
        }
        c();
        if (1 < e.a().nextInt(2)) {
            this.f22a.f57a = rect.right - f().f59a;
            this.j = 2;
        } else {
            this.f22a.f57a = 0;
            this.j = 1;
        }
        this.f22a.b = -f().b;
        this.d = 3;
        a(i.Type6);
        this.g = 5;
        this.e = 400;
        this.h = 0;
    }

    public final void b() {
        c();
        super.b();
        if (1 == this.j && this.f22a.f57a > this.i.right - this.c.f59a && this.f22a.b > this.i.bottom - this.c.b) {
            this.f22a.f57a = this.i.right - this.c.f59a;
            this.f22a.b = this.i.bottom - this.c.b;
            this.j = 3;
        }
        if (3 == this.j && this.f22a.f57a < 0 && this.f22a.b == this.i.bottom - this.c.b) {
            this.f22a.f57a = 0;
            this.j = 6;
        }
        if (2 == this.j && this.f22a.f57a < 0 && this.f22a.b > this.i.bottom - this.c.b) {
            this.f22a.f57a = 0;
            this.f22a.b = this.i.bottom - this.c.b;
            this.j = 4;
        }
        if (4 == this.j && this.f22a.f57a > this.i.right - this.c.f59a && this.f22a.b == this.i.bottom - this.c.b) {
            this.f22a.f57a = this.i.right - this.c.f59a;
            this.j = 5;
        }
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.h++;
        int i2 = (this.i.right - this.c.f59a) / 60;
        int i3 = (this.i.bottom - this.c.b) / 60;
        switch (this.j) {
            case 1:
                this.b.f70a = i2;
                this.b.b = i3;
                return;
            case 2:
                this.b.f70a = -i2;
                this.b.b = i3;
                return;
            case 3:
                this.b.f70a = -i2;
                this.b.b = 0;
                return;
            case 4:
                this.b.f70a = i2;
                this.b.b = 0;
                return;
            case 5:
                this.b.f70a = -i2;
                this.b.b = -i3;
                return;
            case 6:
                this.b.f70a = i2;
                this.b.b = -i3;
                return;
            default:
                return;
        }
    }
}
