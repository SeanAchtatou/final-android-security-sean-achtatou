package com.GalaxyLaser.c;

import android.content.Context;
import android.graphics.Rect;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.a.a;
import com.GalaxyLaser.util.e;
import com.GalaxyLaser.util.i;

public final class ad extends n {
    public ad(Rect rect, Context context) {
        super(rect);
        if (a.a().d()) {
            a(context.getResources(), C0000R.drawable.enemy1n);
        } else {
            a(context.getResources(), C0000R.drawable.enemy1);
        }
        c();
        this.f22a.f57a = e.a().nextInt(rect.right - f().f59a);
        this.f22a.b = -f().b;
        this.g = 1;
        a(i.Type1);
        this.e = 100;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.b.f70a = 0;
        this.b.b = 5;
    }
}
