package com.GalaxyLaser.c;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.util.a;

public final class d extends s {
    private Context f;
    private int[] g = {C0000R.drawable.shield_explode00, C0000R.drawable.shield_explode01, C0000R.drawable.shield_explode02, C0000R.drawable.shield_explode03, C0000R.drawable.shield_explode04, C0000R.drawable.shield_explode05, C0000R.drawable.shield_explode06, C0000R.drawable.shield_explode07, C0000R.drawable.shield_explode08, C0000R.drawable.shield_explode09, C0000R.drawable.explode_dummy};

    public d(a aVar, Context context) {
        super(aVar, context);
        this.f = context;
        a(context.getResources(), this.g[0]);
        a(this.g.length);
    }

    public final void a(Canvas canvas) {
        super.a(canvas);
    }

    public final void b() {
        super.b();
        Resources resources = this.f.getResources();
        if (this.g.length > c()) {
            a(resources, this.g[c()]);
        } else {
            a(resources, this.g[this.g.length - 1]);
        }
    }
}
