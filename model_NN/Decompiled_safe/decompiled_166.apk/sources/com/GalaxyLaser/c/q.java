package com.GalaxyLaser.c;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.util.a;

public final class q extends s {
    private Context f;
    private int[] g = {C0000R.drawable.explosion_nightmare00, C0000R.drawable.explosion_nightmare01, C0000R.drawable.explosion_nightmare02, C0000R.drawable.explosion_nightmare03, C0000R.drawable.explosion_nightmare04, C0000R.drawable.explosion_nightmare05, C0000R.drawable.explosion_nightmare06, C0000R.drawable.explosion_nightmare07, C0000R.drawable.explosion_nightmare08, C0000R.drawable.explosion_nightmare09, C0000R.drawable.explosion_nightmare10, C0000R.drawable.explosion_nightmare11, C0000R.drawable.explosion_nightmare12, C0000R.drawable.explosion_nightmare13, C0000R.drawable.explosion_nightmare14, C0000R.drawable.explosion_nightmare15, C0000R.drawable.explosion_nightmare16, C0000R.drawable.explosion_nightmare17, C0000R.drawable.explosion_nightmare18, C0000R.drawable.explosion_nightmare19, C0000R.drawable.explosion_nightmare20, C0000R.drawable.explosion_nightmare21, C0000R.drawable.explosion_nightmare22, C0000R.drawable.explosion_nightmare23, C0000R.drawable.explode_dummy};

    public q(a aVar, Context context) {
        super(aVar, context);
        this.f = context;
        a(context.getResources(), this.g[0]);
        a(this.g.length);
    }

    public final void a(Canvas canvas) {
        super.a(canvas);
    }

    public final void b() {
        super.b();
        Resources resources = this.f.getResources();
        if (this.g.length > c()) {
            a(resources, this.g[c()]);
        } else {
            a(resources, this.g[this.g.length - 1]);
        }
    }
}
