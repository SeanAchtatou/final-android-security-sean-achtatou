package com.GalaxyLaser.c;

import android.graphics.Rect;
import com.GalaxyLaser.b.c;
import com.GalaxyLaser.util.i;

public class n extends g {
    protected Rect f;
    protected int g = 0;
    private i h;
    private c i = null;

    public n(Rect rect) {
        this.f = rect;
        this.e = 10;
    }

    public final i a() {
        return this.h;
    }

    public final void a(int i2) {
        this.g = i2;
    }

    public final void a(c cVar) {
        this.i = cVar;
    }

    public final void a(i iVar) {
        this.h = iVar;
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    public final int i() {
        return this.e;
    }

    public final c k() {
        return this.i;
    }

    public final int l() {
        return this.g;
    }
}
