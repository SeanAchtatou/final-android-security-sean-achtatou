package com.GalaxyLaser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;

public final class l extends ArrayAdapter {

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f31a;
    private View[] b;

    public l(Context context, ArrayList arrayList) {
        super(context, (int) C0000R.layout.list, arrayList);
        this.f31a = (LayoutInflater) context.getSystemService("layout_inflater");
        this.b = new View[arrayList.size()];
        for (int i = 0; i < this.b.length; i++) {
            this.b[i] = null;
        }
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (this.b[i] == null) {
            b bVar = (b) getItem(i);
            View[] viewArr = this.b;
            View inflate = this.f31a.inflate((int) C0000R.layout.list, (ViewGroup) null);
            k kVar = new k();
            kVar.f30a = (TextView) inflate.findViewById(C0000R.id.ranking);
            kVar.c = (TextView) inflate.findViewById(C0000R.id.name);
            kVar.b = (TextView) inflate.findViewById(C0000R.id.score);
            inflate.setTag(kVar);
            kVar.f30a.setText(String.valueOf(bVar.a()));
            kVar.c.setText(bVar.c());
            kVar.b.setText(String.valueOf(bVar.b()));
            viewArr[i] = inflate;
        }
        return this.b[i];
    }
}
