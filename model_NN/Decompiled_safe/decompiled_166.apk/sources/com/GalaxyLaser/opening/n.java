package com.GalaxyLaser.opening;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;

final class n extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ObjectInit f49a;

    n(ObjectInit objectInit) {
        this.f49a = objectInit;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                new h(this.f49a).execute(new Void[0]);
                return;
            case 1:
                this.f49a.startActivity(new Intent(this.f49a, TitleActivity.class));
                this.f49a.finish();
                return;
            default:
                return;
        }
    }
}
