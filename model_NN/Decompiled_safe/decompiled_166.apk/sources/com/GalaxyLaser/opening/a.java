package com.GalaxyLaser.opening;

import android.content.Intent;
import android.view.View;

final class a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ StageSelect f36a;

    a(StageSelect stageSelect) {
        this.f36a = stageSelect;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        intent.putExtra("Stage", this.f36a.c + 1);
        this.f36a.setResult(-1, intent);
        this.f36a.finish();
    }
}
