package com.GalaxyLaser.opening;

import android.app.AlertDialog;
import android.view.View;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.a.f;
import com.GalaxyLaser.a.i;

final class l implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TitleActivity f47a;

    l(TitleActivity titleActivity) {
        this.f47a = titleActivity;
    }

    public final void onClick(View view) {
        f.a(this.f47a.getApplication().getApplicationContext()).a(com.GalaxyLaser.util.f.Title_SE);
        if (i.a(this.f47a.getApplication().getApplicationContext()).a()) {
            TitleActivity titleActivity = this.f47a;
            new AlertDialog.Builder(titleActivity).setTitle((int) C0000R.string.select_stage).setIcon((int) C0000R.drawable.icon).setPositiveButton((int) C0000R.string.normal_select, new i(titleActivity)).setNegativeButton((int) C0000R.string.nightmare_select, new c(titleActivity)).create().show();
            return;
        }
        TitleActivity.a(this.f47a);
    }
}
