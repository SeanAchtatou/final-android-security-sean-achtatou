package com.GalaxyLaser.opening;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.GalaxyLaser.C0000R;

public class StageSelect extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private ArrayAdapter f35a;
    private int b;
    /* access modifiers changed from: private */
    public int c;

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            switch (keyEvent.getKeyCode()) {
                case 4:
                    return true;
            }
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.stage_select);
        this.b = getIntent().getIntExtra("Stage", 1);
        this.c = 0;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.f35a != null) {
            this.f35a.clear();
            this.f35a = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.f35a = new ArrayAdapter(this, 17367048);
        this.f35a.setDropDownViewResource(17367049);
        for (int i = 0; i < this.b; i++) {
            this.f35a.add("Stage " + (i + 1));
        }
        Spinner spinner = (Spinner) findViewById(C0000R.id.select_stage);
        spinner.setAdapter((SpinnerAdapter) this.f35a);
        spinner.setSelection(this.c);
        spinner.setOnItemSelectedListener(new b(this));
        ((Button) findViewById(C0000R.id.select_ok)).setOnClickListener(new a(this));
    }
}
