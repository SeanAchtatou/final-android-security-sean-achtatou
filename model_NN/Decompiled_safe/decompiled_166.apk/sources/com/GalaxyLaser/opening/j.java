package com.GalaxyLaser.opening;

import android.content.Intent;
import android.view.View;
import com.GalaxyLaser.GameRankMy;
import com.GalaxyLaser.a.f;

final class j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TitleActivity f45a;

    j(TitleActivity titleActivity) {
        this.f45a = titleActivity;
    }

    public final void onClick(View view) {
        f.a(this.f45a.getApplication().getApplicationContext()).a(com.GalaxyLaser.util.f.Title_SE);
        this.f45a.startActivity(new Intent(this.f45a, GameRankMy.class));
        this.f45a.finish();
    }
}
