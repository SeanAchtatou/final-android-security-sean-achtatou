package com.GalaxyLaser.opening;

import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.util.BaseActivity;

public class ObjectInit extends BaseActivity {
    /* access modifiers changed from: private */
    public Handler b = new n(this);

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            switch (keyEvent.getKeyCode()) {
                case 4:
                    return true;
            }
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.init);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.b.sendEmptyMessage(0);
    }
}
