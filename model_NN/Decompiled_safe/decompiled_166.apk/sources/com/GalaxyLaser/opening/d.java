package com.GalaxyLaser.opening;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Rect;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.util.a;
import com.GalaxyLaser.util.j;
import com.GalaxyLaser.util.l;

public final class d extends Thread {

    /* renamed from: a  reason: collision with root package name */
    public boolean f39a;
    public l b;
    public l c;
    public l d;
    public l e;
    public l f;
    public l g;
    public l h;
    public Paint i = new Paint();
    private Context j;
    private Rect k;
    private boolean l = false;
    private int m = 700;
    private a n;
    private a o;
    private a p;
    private a q;
    private int r = 0;
    private int s;
    private int t;

    public d(Context context) {
        this.j = context;
        this.i.setAlpha(this.r);
        this.p = new a(430, -180);
        this.q = new a(380, -20);
        this.n = new a(-300, 295);
        this.o = new a(500, 110);
        this.s = 0;
        this.t = 0;
    }

    public final void a(Rect rect) {
        this.k = rect;
        Resources resources = this.j.getResources();
        if (this.b == null) {
            this.b = new l(BitmapFactory.decodeResource(resources, C0000R.drawable.title_back), 0, 0, j.a().b().f66a, j.a().b().b);
        }
        if (this.c == null) {
            this.c = new l(BitmapFactory.decodeResource(resources, C0000R.drawable.title_earth), 0, this.m, j.a().b().f66a, j.a().b().b);
        }
        if (this.d == null) {
            this.d = new l(BitmapFactory.decodeResource(resources, C0000R.drawable.title_red_line), -300, 295, j.a().b().f66a, j.a().b().b);
        }
        if (this.e == null) {
            this.e = new l(BitmapFactory.decodeResource(resources, C0000R.drawable.title_blue_line), 500, 110, j.a().b().f66a, j.a().b().b);
        }
        if (this.f == null) {
            this.f = new l(BitmapFactory.decodeResource(resources, C0000R.drawable.title_jet), 530, -280, j.a().b().f66a, j.a().b().b);
        }
        if (this.g == null) {
            this.g = new l(BitmapFactory.decodeResource(resources, C0000R.drawable.title_ally), 480, -120, j.a().b().f66a, j.a().b().b);
        }
        if (this.h == null) {
            this.h = new l(BitmapFactory.decodeResource(resources, C0000R.drawable.title_logo), 15, 85, j.a().b().f66a, j.a().b().b);
        }
        this.l = true;
    }

    public final void run() {
        this.f39a = true;
        long currentTimeMillis = System.currentTimeMillis();
        while (true) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            if (!this.f39a) {
                currentTimeMillis = System.currentTimeMillis();
            } else {
                long currentTimeMillis2 = System.currentTimeMillis();
                long j2 = currentTimeMillis2 - currentTimeMillis;
                while (j2 >= 17) {
                    j2 -= 17;
                    if (this.l && this.f39a) {
                        if (this.s >= 0 && this.m > 600) {
                            this.m--;
                            this.c.a(0, this.m);
                        }
                        if (this.s >= 2) {
                            if (this.n.f57a < 110) {
                                this.n.f57a += 20;
                            }
                            if (this.n.b < 460) {
                                this.n.b += 8;
                            }
                            this.d.a(this.n.f57a, this.n.b);
                            if (this.o.f57a > 210) {
                                this.o.f57a -= 20;
                            }
                            if (this.o.b < 330) {
                                this.o.b += 15;
                            }
                            this.e.a(this.o.f57a, this.o.b);
                        }
                        if (this.s > 0) {
                            if (this.p.f57a > 170) {
                                this.p.f57a -= 15;
                            }
                            if (this.p.b < 80) {
                                this.p.b += 15;
                            }
                            this.f.a(this.p.f57a, this.p.b);
                            if (this.q.f57a > 120) {
                                this.q.f57a -= 15;
                            }
                            if (this.q.b < 240) {
                                this.q.b += 15;
                            }
                            this.g.a(this.q.f57a, this.q.b);
                        }
                        if (this.s >= 3 && this.r < 255) {
                            this.r += 5;
                            this.i.setAlpha(this.r);
                        }
                        this.t++;
                        if (this.t == 30) {
                            this.s++;
                        }
                        if (this.t == 40) {
                            this.s++;
                        }
                        if (this.t == 60) {
                            this.s++;
                        }
                    }
                }
                currentTimeMillis = currentTimeMillis2 - j2;
            }
        }
    }
}
