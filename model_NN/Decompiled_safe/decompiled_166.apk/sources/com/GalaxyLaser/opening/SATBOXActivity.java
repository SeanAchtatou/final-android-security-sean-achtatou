package com.GalaxyLaser.opening;

import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.util.BaseActivity;

public class SATBOXActivity extends BaseActivity {
    private ImageView b;
    /* access modifiers changed from: private */
    public Handler c = new g(this);

    static /* synthetic */ void a(SATBOXActivity sATBOXActivity) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(500);
        alphaAnimation.setRepeatCount(0);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setAnimationListener(new f(sATBOXActivity));
        sATBOXActivity.b.startAnimation(alphaAnimation);
    }

    static /* synthetic */ void b(SATBOXActivity sATBOXActivity) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(500);
        alphaAnimation.setRepeatCount(0);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setAnimationListener(new e(sATBOXActivity));
        sATBOXActivity.b.startAnimation(alphaAnimation);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.credit);
        this.b = (ImageView) findViewById(C0000R.id.credit);
        this.c.sendEmptyMessageDelayed(1, 0);
    }
}
