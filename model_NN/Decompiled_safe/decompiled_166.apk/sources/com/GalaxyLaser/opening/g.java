package com.GalaxyLaser.opening;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;

final class g extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SATBOXActivity f42a;

    g(SATBOXActivity sATBOXActivity) {
        this.f42a = sATBOXActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                SATBOXActivity.a(this.f42a);
                return;
            case 2:
                SATBOXActivity.b(this.f42a);
                return;
            case 3:
                SATBOXActivity sATBOXActivity = this.f42a;
                try {
                    Intent intent = new Intent(sATBOXActivity.getApplicationContext(), ObjectInit.class);
                    intent.addFlags(268435456);
                    sATBOXActivity.startActivity(intent);
                    sATBOXActivity.finish();
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            default:
                return;
        }
    }
}
