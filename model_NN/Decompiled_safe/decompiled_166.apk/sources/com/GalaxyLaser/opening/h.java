package com.GalaxyLaser.opening;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import com.GalaxyLaser.a.b;
import com.GalaxyLaser.a.c;
import com.GalaxyLaser.a.d;
import com.GalaxyLaser.a.f;
import com.GalaxyLaser.a.g;
import com.GalaxyLaser.a.i;
import com.GalaxyLaser.a.j;
import com.GalaxyLaser.a.k;
import com.GalaxyLaser.a.l;
import com.GalaxyLaser.a.n;
import com.GalaxyLaser.a.o;
import com.GalaxyLaser.a.r;
import com.GalaxyLaser.util.e;

final class h extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private ProgressDialog f43a = null;
    private Context b;
    private /* synthetic */ ObjectInit c;

    public h(ObjectInit objectInit) {
        this.c = objectInit;
        this.b = objectInit.getApplication().getApplicationContext();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        System.gc();
        c.a(this.b);
        n.a(this.b);
        com.GalaxyLaser.a.h.a(this.b);
        g.a(this.b);
        l.a(this.b);
        o.a(this.b);
        b.a(this.b);
        d.a(this.b);
        r.a(this.b);
        f.a(this.b);
        k.a(this.b);
        i.a(this.b);
        j.a(this.b);
        e.a();
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        this.f43a.dismiss();
        this.c.b.sendEmptyMessage(1);
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        this.f43a = new ProgressDialog(this.c);
        this.f43a.setTitle("");
        this.f43a.setMessage("Now Loading...");
        this.f43a.setProgressStyle(0);
        this.f43a.show();
    }
}
