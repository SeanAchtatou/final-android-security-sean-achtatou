package com.GalaxyLaser.opening;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import com.GalaxyLaser.C0000R;
import com.GalaxyLaser.GameActivity;
import com.GalaxyLaser.a.f;
import com.GalaxyLaser.a.i;
import com.GalaxyLaser.a.m;
import com.GalaxyLaser.util.BaseActivity;
import com.GalaxyLaser.view.b;
import com.GalaxyLaser.view.c;
import mediba.ad.sdk.android.MasAdView;

public class TitleActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public boolean b;
    private int c;
    private int d;
    private c e;
    private d f;
    private ImageButton g;
    private ImageButton h;
    private ImageButton i;
    private b j = new m(this);

    static /* synthetic */ void a(TitleActivity titleActivity) {
        Class<GameActivity> cls = GameActivity.class;
        m.a(titleActivity.getApplication().getApplicationContext()).a();
        if (!titleActivity.b) {
            if (1 < titleActivity.c) {
                Intent intent = new Intent(titleActivity, StageSelect.class);
                intent.putExtra("Stage", titleActivity.c);
                titleActivity.startActivityForResult(intent, 0);
                return;
            }
            Class<GameActivity> cls2 = GameActivity.class;
            Intent intent2 = new Intent(titleActivity, cls);
            intent2.putExtra("NightMare", titleActivity.b);
            titleActivity.startActivityForResult(intent2, 1);
        } else if (1 < titleActivity.d) {
            Intent intent3 = new Intent(titleActivity, StageSelect.class);
            intent3.putExtra("Stage", titleActivity.d);
            titleActivity.startActivityForResult(intent3, 0);
        } else {
            Class<GameActivity> cls3 = GameActivity.class;
            Intent intent4 = new Intent(titleActivity, cls);
            intent4.putExtra("NightMare", titleActivity.b);
            titleActivity.startActivityForResult(intent4, 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 0:
                int intExtra = intent.getIntExtra("Stage", 1);
                Intent intent2 = new Intent(this, GameActivity.class);
                intent2.putExtra("NightMare", this.b);
                intent2.putExtra("Stage", intExtra);
                startActivityForResult(intent2, 1);
                return;
            case 1:
                if (intent != null) {
                    int intExtra2 = intent.getIntExtra("Stage", 1);
                    if (this.b) {
                        if (intExtra2 > this.d) {
                            this.d = intExtra2;
                            i.a(this).c(this.d);
                            return;
                        }
                        return;
                    } else if (intExtra2 > this.c) {
                        this.c = intExtra2;
                        i.a(this).b(this.c);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f = new d(this);
        this.e = new c(this, this.f);
        this.e.a(this.j);
        setContentView(this.e);
        this.f.start();
        this.b = false;
        this.c = i.a(this).d();
        this.d = i.a(this).e();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(C0000R.menu.menu, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        m.a(getApplication().getApplicationContext()).d();
        if (this.e != null) {
            this.e.a();
            this.e = null;
        }
        if (this.f != null) {
            d dVar = this.f;
            if (dVar.b != null) {
                dVar.b.a();
                dVar.b = null;
            }
            if (dVar.c != null) {
                dVar.c.a();
                dVar.c = null;
            }
            if (dVar.d != null) {
                dVar.d.a();
                dVar.d = null;
            }
            if (dVar.e != null) {
                dVar.e.a();
                dVar.e = null;
            }
            if (dVar.f != null) {
                dVar.f.a();
                dVar.f = null;
            }
            if (dVar.g != null) {
                dVar.g.a();
                dVar.g = null;
            }
            if (dVar.h != null) {
                dVar.h.a();
                dVar.h = null;
            }
            dVar.f39a = false;
            this.f = null;
        }
    }

    public boolean onMenuItemSelected(int i2, MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case C0000R.id.finish /*2131230754*/:
                f.a(getApplication().getApplicationContext()).a(com.GalaxyLaser.util.f.Title_SE);
                finish();
                return true;
            case C0000R.id.about /*2131230755*/:
                b();
                return true;
            case C0000R.id.applink /*2131230756*/:
                d();
                return true;
            case C0000R.id.satbox /*2131230757*/:
                c();
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        m.a(getApplication().getApplicationContext()).a();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        m.a(getApplication().getApplicationContext()).a(C0000R.raw.title_bgm, true);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setGravity(85);
        this.g = new ImageButton(this);
        this.g.setBackgroundResource(C0000R.drawable.pc_satbox);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        layoutParams2.setMargins(0, 0, (int) (10.0f * displayMetrics.scaledDensity), 0);
        this.g.setLayoutParams(layoutParams2);
        linearLayout.addView(this.g);
        LinearLayout linearLayout2 = new LinearLayout(this);
        linearLayout2.setGravity(81);
        linearLayout2.setOrientation(1);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.width = (int) (displayMetrics.scaledDensity * 220.0f);
        layoutParams3.height = (int) (displayMetrics.scaledDensity * 45.0f);
        layoutParams4.width = (int) (displayMetrics.scaledDensity * 220.0f);
        layoutParams4.height = (int) (displayMetrics.scaledDensity * 45.0f);
        layoutParams3.setMargins(0, 0, 0, (int) (5.0f * displayMetrics.scaledDensity));
        layoutParams4.setMargins(0, 0, 0, (int) (displayMetrics.scaledDensity * 80.0f));
        this.h = new ImageButton(this);
        this.h.setBackgroundResource(C0000R.drawable.button1);
        this.i = new ImageButton(this);
        this.i.setBackgroundResource(C0000R.drawable.button2);
        linearLayout2.addView(this.h, layoutParams3);
        linearLayout2.addView(this.i, layoutParams4);
        addContentView(linearLayout, layoutParams);
        addContentView(linearLayout2, layoutParams);
        this.h.setOnClickListener(new l(this));
        this.g.setOnClickListener(new k(this));
        this.i.setOnClickListener(new j(this));
        addContentView(new MasAdView(this), new LinearLayout.LayoutParams(-2, -2));
    }
}
