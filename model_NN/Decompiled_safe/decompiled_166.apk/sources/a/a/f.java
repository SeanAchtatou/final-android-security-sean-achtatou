package a.a;

public final class f extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private Throwable f3a;

    public f(String str) {
        super(str);
    }

    public f(Throwable th) {
        super(th.getMessage());
        this.f3a = th;
    }

    public final Throwable getCause() {
        return this.f3a;
    }
}
