package a.a;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList f1a;

    public b() {
        this.f1a = new ArrayList();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005c A[SYNTHETIC] */
    public b(a.a.a r5) {
        /*
            r4 = this;
            r3 = 93
            r4.<init>()
            char r0 = r5.c()
            r1 = 91
            if (r0 != r1) goto L_0x0015
            r0 = r3
        L_0x000e:
            char r1 = r5.c()
            if (r1 != r3) goto L_0x0023
        L_0x0014:
            return
        L_0x0015:
            r1 = 40
            if (r0 != r1) goto L_0x001c
            r0 = 41
            goto L_0x000e
        L_0x001c:
            java.lang.String r0 = "A JSONArray text must start with '['"
            a.a.f r0 = r5.a(r0)
            throw r0
        L_0x0023:
            r5.a()
        L_0x0026:
            char r1 = r5.c()
            r2 = 44
            if (r1 != r2) goto L_0x0045
            r5.a()
            java.util.ArrayList r1 = r4.f1a
            r2 = 0
            r1.add(r2)
        L_0x0037:
            char r1 = r5.c()
            switch(r1) {
                case 41: goto L_0x005c;
                case 44: goto L_0x0052;
                case 59: goto L_0x0052;
                case 93: goto L_0x005c;
                default: goto L_0x003e;
            }
        L_0x003e:
            java.lang.String r0 = "Expected a ',' or ']'"
            a.a.f r0 = r5.a(r0)
            throw r0
        L_0x0045:
            r5.a()
            java.util.ArrayList r1 = r4.f1a
            java.lang.Object r2 = r5.d()
            r1.add(r2)
            goto L_0x0037
        L_0x0052:
            char r1 = r5.c()
            if (r1 == r3) goto L_0x0014
            r5.a()
            goto L_0x0026
        L_0x005c:
            if (r0 == r1) goto L_0x0014
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Expected a '"
            r1.<init>(r2)
            java.lang.Character r2 = new java.lang.Character
            r2.<init>(r0)
            java.lang.StringBuilder r0 = r1.append(r2)
            java.lang.String r1 = "'"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            a.a.f r0 = r5.a(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.b.<init>(a.a.a):void");
    }

    public b(Object obj) {
        this();
        if (obj.getClass().isArray()) {
            int length = Array.getLength(obj);
            for (int i = 0; i < length; i++) {
                this.f1a.add(d.b(Array.get(obj, i)));
            }
            return;
        }
        throw new f("JSONArray initial value should be a string or collection or array.");
    }

    public b(Collection collection) {
        this.f1a = new ArrayList();
        if (collection != null) {
            for (Object b : collection) {
                this.f1a.add(d.b(b));
            }
        }
    }

    private String a(String str) {
        int size = this.f1a.size();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                stringBuffer.append(str);
            }
            stringBuffer.append(d.a(this.f1a.get(i)));
        }
        return stringBuffer.toString();
    }

    public final int a() {
        return this.f1a.size();
    }

    public final d a(int i) {
        Object obj = (i < 0 || i >= this.f1a.size()) ? null : this.f1a.get(i);
        if (obj == null) {
            throw new f("JSONArray[" + i + "] not found.");
        } else if (obj instanceof d) {
            return (d) obj;
        } else {
            throw new f("JSONArray[" + i + "] is not a JSONObject.");
        }
    }

    public final String toString() {
        try {
            return String.valueOf('[') + a(",") + ']';
        } catch (Exception e) {
            return null;
        }
    }
}
