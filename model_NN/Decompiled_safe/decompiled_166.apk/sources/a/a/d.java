package a.a;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class d {
    private static Object b = new e();

    /* renamed from: a  reason: collision with root package name */
    private Map f2a;

    public d() {
        this.f2a = new HashMap();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0063 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00a5 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001c  */
    public d(a.a.a r5) {
        /*
            r4 = this;
            r4.<init>()
            char r0 = r5.c()
            r1 = 123(0x7b, float:1.72E-43)
            if (r0 == r1) goto L_0x0015
            java.lang.String r0 = "A JSONObject text must begin with '{'"
            a.a.f r0 = r5.a(r0)
            throw r0
        L_0x0012:
            r5.a()
        L_0x0015:
            char r0 = r5.c()
            switch(r0) {
                case 0: goto L_0x0063;
                case 125: goto L_0x00a5;
                default: goto L_0x001c;
            }
        L_0x001c:
            r5.a()
            java.lang.Object r0 = r5.d()
            java.lang.String r0 = r0.toString()
            char r1 = r5.c()
            r2 = 61
            if (r1 != r2) goto L_0x006a
            char r1 = r5.b()
            r2 = 62
            if (r1 == r2) goto L_0x003a
            r5.a()
        L_0x003a:
            java.lang.Object r1 = r5.d()
            if (r0 == 0) goto L_0x0089
            if (r1 == 0) goto L_0x0089
            java.lang.Object r2 = r4.g(r0)
            if (r2 == 0) goto L_0x0075
            a.a.f r1 = new a.a.f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Duplicate key \""
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = "\""
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x0063:
            java.lang.String r0 = "A JSONObject text must end with '}'"
            a.a.f r0 = r5.a(r0)
            throw r0
        L_0x006a:
            r2 = 58
            if (r1 == r2) goto L_0x003a
            java.lang.String r0 = "Expected a ':' after a key"
            a.a.f r0 = r5.a(r0)
            throw r0
        L_0x0075:
            if (r0 != 0) goto L_0x007f
            a.a.f r0 = new a.a.f
            java.lang.String r1 = "Null key."
            r0.<init>(r1)
            throw r0
        L_0x007f:
            if (r1 == 0) goto L_0x0097
            c(r1)
            java.util.Map r2 = r4.f2a
            r2.put(r0, r1)
        L_0x0089:
            char r0 = r5.c()
            switch(r0) {
                case 44: goto L_0x009d;
                case 59: goto L_0x009d;
                case 125: goto L_0x00a5;
                default: goto L_0x0090;
            }
        L_0x0090:
            java.lang.String r0 = "Expected a ',' or '}'"
            a.a.f r0 = r5.a(r0)
            throw r0
        L_0x0097:
            java.util.Map r1 = r4.f2a
            r1.remove(r0)
            goto L_0x0089
        L_0x009d:
            char r0 = r5.c()
            r1 = 125(0x7d, float:1.75E-43)
            if (r0 != r1) goto L_0x0012
        L_0x00a5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.d.<init>(a.a.a):void");
    }

    private d(Object obj) {
        this();
        Class<?> cls = obj.getClass();
        Method[] methods = cls.getClassLoader() != null ? cls.getMethods() : cls.getDeclaredMethods();
        for (int i = 0; i < methods.length; i++) {
            try {
                Method method = methods[i];
                if (Modifier.isPublic(method.getModifiers())) {
                    String name = method.getName();
                    String substring = name.startsWith("get") ? (name.equals("getClass") || name.equals("getDeclaringClass")) ? "" : name.substring(3) : name.startsWith("is") ? name.substring(2) : "";
                    if (substring.length() > 0 && Character.isUpperCase(substring.charAt(0)) && method.getParameterTypes().length == 0) {
                        if (substring.length() == 1) {
                            substring = substring.toLowerCase();
                        } else if (!Character.isUpperCase(substring.charAt(1))) {
                            substring = String.valueOf(substring.substring(0, 1).toLowerCase()) + substring.substring(1);
                        }
                        this.f2a.put(substring, b(method.invoke(obj, null)));
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    public d(String str) {
        this(new a(str));
    }

    private d(Map map) {
        this.f2a = new HashMap();
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                this.f2a.put(entry.getKey(), b(entry.getValue()));
            }
        }
    }

    private static String a(Number number) {
        if (number == null) {
            throw new f("Null pointer");
        }
        c(number);
        String obj = number.toString();
        if (obj.indexOf(46) <= 0 || obj.indexOf(101) >= 0 || obj.indexOf(69) >= 0) {
            return obj;
        }
        while (obj.endsWith("0")) {
            obj = obj.substring(0, obj.length() - 1);
        }
        return obj.endsWith(".") ? obj.substring(0, obj.length() - 1) : obj;
    }

    static String a(Object obj) {
        if (obj == null || obj.equals(null)) {
            return "null";
        }
        if (!(obj instanceof c)) {
            return obj instanceof Number ? a((Number) obj) : ((obj instanceof Boolean) || (obj instanceof d) || (obj instanceof b)) ? obj.toString() : obj instanceof Map ? new d((Map) obj).toString() : obj instanceof Collection ? new b((Collection) obj).toString() : obj.getClass().isArray() ? new b(obj).toString() : h(obj.toString());
        }
        try {
            String a2 = ((c) obj).a();
            if (a2 instanceof String) {
                return a2;
            }
            throw new f("Bad value from toJSONString: " + ((Object) a2));
        } catch (Exception e) {
            throw new f(e);
        }
    }

    static Object b(Object obj) {
        if (obj == null) {
            try {
                return b;
            } catch (Exception e) {
                return null;
            }
        } else if ((obj instanceof d) || (obj instanceof b) || b.equals(obj) || (obj instanceof c) || (obj instanceof Byte) || (obj instanceof Character) || (obj instanceof Short) || (obj instanceof Integer) || (obj instanceof Long) || (obj instanceof Boolean) || (obj instanceof Float) || (obj instanceof Double) || (obj instanceof String)) {
            return obj;
        } else {
            if (obj instanceof Collection) {
                return new b((Collection) obj);
            }
            if (obj.getClass().isArray()) {
                return new b(obj);
            }
            if (obj instanceof Map) {
                return new d((Map) obj);
            }
            Package packageR = obj.getClass().getPackage();
            String name = packageR != null ? packageR.getName() : "";
            return (name.startsWith("java.") || name.startsWith("javax.") || obj.getClass().getClassLoader() == null) ? obj.toString() : new d(obj);
        }
    }

    private static void c(Object obj) {
        if (obj == null) {
            return;
        }
        if (obj instanceof Double) {
            if (((Double) obj).isInfinite() || ((Double) obj).isNaN()) {
                throw new f("JSON does not allow non-finite numbers.");
            }
        } else if (!(obj instanceof Float)) {
        } else {
            if (((Float) obj).isInfinite() || ((Float) obj).isNaN()) {
                throw new f("JSON does not allow non-finite numbers.");
            }
        }
    }

    public static Object e(String str) {
        if (str.equals("")) {
            return str;
        }
        if (str.equalsIgnoreCase("true")) {
            return Boolean.TRUE;
        }
        if (str.equalsIgnoreCase("false")) {
            return Boolean.FALSE;
        }
        if (str.equalsIgnoreCase("null")) {
            return b;
        }
        char charAt = str.charAt(0);
        if ((charAt >= '0' && charAt <= '9') || charAt == '.' || charAt == '-' || charAt == '+') {
            if (charAt == '0' && str.length() > 2 && (str.charAt(1) == 'x' || str.charAt(1) == 'X')) {
                try {
                    return new Integer(Integer.parseInt(str.substring(2), 16));
                } catch (Exception e) {
                }
            }
            try {
                if (str.indexOf(46) >= 0 || str.indexOf(101) >= 0 || str.indexOf(69) >= 0) {
                    return Double.valueOf(str);
                }
                Long l = new Long(str);
                return l.longValue() == ((long) l.intValue()) ? new Integer(l.intValue()) : l;
            } catch (Exception e2) {
            }
        }
        return str;
    }

    private Object f(String str) {
        Object g = g(str);
        if (g != null) {
            return g;
        }
        throw new f("JSONObject[" + h(str) + "] not found.");
    }

    private Object g(String str) {
        if (str == null) {
            return null;
        }
        return this.f2a.get(str);
    }

    private static String h(String str) {
        char c = 0;
        if (str == null || str.length() == 0) {
            return "\"\"";
        }
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer(length + 4);
        stringBuffer.append('\"');
        int i = 0;
        while (i < length) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case 8:
                    stringBuffer.append("\\b");
                    break;
                case 9:
                    stringBuffer.append("\\t");
                    break;
                case 10:
                    stringBuffer.append("\\n");
                    break;
                case 12:
                    stringBuffer.append("\\f");
                    break;
                case 13:
                    stringBuffer.append("\\r");
                    break;
                case '\"':
                case '\\':
                    stringBuffer.append('\\');
                    stringBuffer.append(charAt);
                    break;
                case '/':
                    if (c == '<') {
                        stringBuffer.append('\\');
                    }
                    stringBuffer.append(charAt);
                    break;
                default:
                    if (charAt >= ' ' && ((charAt < 128 || charAt >= 160) && (charAt < 8192 || charAt >= 8448))) {
                        stringBuffer.append(charAt);
                        break;
                    } else {
                        String str2 = "000" + Integer.toHexString(charAt);
                        stringBuffer.append("\\u" + str2.substring(str2.length() - 4));
                        break;
                    }
                    break;
            }
            i++;
            c = charAt;
        }
        stringBuffer.append('\"');
        return stringBuffer.toString();
    }

    public final int a() {
        return this.f2a.size();
    }

    public final int a(String str) {
        Object f = f(str);
        try {
            return f instanceof Number ? ((Number) f).intValue() : Integer.parseInt((String) f);
        } catch (Exception e) {
            throw new f("JSONObject[" + h(str) + "] is not an int.");
        }
    }

    public final b b(String str) {
        Object f = f(str);
        if (f instanceof b) {
            return (b) f;
        }
        throw new f("JSONObject[" + h(str) + "] is not a JSONArray.");
    }

    public final long c(String str) {
        Object f = f(str);
        try {
            return f instanceof Number ? ((Number) f).longValue() : Long.parseLong((String) f);
        } catch (Exception e) {
            throw new f("JSONObject[" + h(str) + "] is not a long.");
        }
    }

    public final String d(String str) {
        return f(str).toString();
    }

    public final String toString() {
        try {
            StringBuffer stringBuffer = new StringBuffer("{");
            for (Object next : this.f2a.keySet()) {
                if (stringBuffer.length() > 1) {
                    stringBuffer.append(',');
                }
                stringBuffer.append(h(next.toString()));
                stringBuffer.append(':');
                stringBuffer.append(a(this.f2a.get(next)));
            }
            stringBuffer.append('}');
            return stringBuffer.toString();
        } catch (Exception e) {
            return null;
        }
    }
}
