package a.a;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private int f0a;
    private boolean b;
    private int c;
    private int d;
    private char e;
    private Reader f;
    private boolean g;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.Object.<init>():void in method: a.a.a.<init>(java.io.Reader):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.Object.<init>():void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    private a(java.io.Reader r1) {
        /*
            r3 = this;
            r2 = 1
            r1 = 0
            r3.<init>()
            boolean r0 = r4.markSupported()
            if (r0 == 0) goto L_0x001b
            r0 = r4
        L_0x000c:
            r3.f = r0
            r3.b = r1
            r3.g = r1
            r3.e = r1
            r3.c = r1
            r3.f0a = r2
            r3.d = r2
            return
        L_0x001b:
            java.io.BufferedReader r0 = new java.io.BufferedReader
            r0.<init>(r4)
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.<init>(java.io.Reader):void");
    }

    public a(String str) {
        this(new StringReader(str));
    }

    private String e() {
        char[] cArr = new char[4];
        for (int i = 0; i < 4; i++) {
            cArr[i] = b();
            if (this.b && !this.g) {
                throw a("Substring bounds error");
            }
        }
        return new String(cArr);
    }

    public final f a(String str) {
        return new f(String.valueOf(str) + toString());
    }

    public final void a() {
        if (this.g || this.c <= 0) {
            throw new f("Stepping back two steps is not supported");
        }
        this.c--;
        this.f0a--;
        this.g = true;
        this.b = false;
    }

    public final char b() {
        int read;
        if (this.g) {
            this.g = false;
            read = this.e;
        } else {
            try {
                read = this.f.read();
                if (read <= 0) {
                    this.b = true;
                    read = 0;
                }
            } catch (IOException e2) {
                throw new f(e2);
            }
        }
        this.c++;
        if (this.e == 13) {
            this.d++;
            this.f0a = read == 10 ? 0 : 1;
        } else if (read == 10) {
            this.d++;
            this.f0a = 0;
        } else {
            this.f0a++;
        }
        this.e = (char) read;
        return this.e;
    }

    public final char c() {
        char b2;
        do {
            b2 = b();
            if (b2 == 0) {
                break;
            }
        } while (b2 <= ' ');
        return b2;
    }

    public final Object d() {
        char c2 = c();
        switch (c2) {
            case '\"':
            case '\'':
                StringBuffer stringBuffer = new StringBuffer();
                while (true) {
                    char b2 = b();
                    switch (b2) {
                        case 0:
                        case 10:
                        case 13:
                            throw a("Unterminated string");
                        case '\\':
                            char b3 = b();
                            switch (b3) {
                                case '\"':
                                case '\'':
                                case '/':
                                case '\\':
                                    stringBuffer.append(b3);
                                    continue;
                                case 'b':
                                    stringBuffer.append(8);
                                    continue;
                                case 'f':
                                    stringBuffer.append(12);
                                    continue;
                                case 'n':
                                    stringBuffer.append(10);
                                    continue;
                                case 'r':
                                    stringBuffer.append(13);
                                    continue;
                                case 't':
                                    stringBuffer.append(9);
                                    continue;
                                case 'u':
                                    stringBuffer.append((char) Integer.parseInt(e(), 16));
                                    continue;
                                default:
                                    throw a("Illegal escape.");
                            }
                        default:
                            if (b2 != c2) {
                                stringBuffer.append(b2);
                                break;
                            } else {
                                return stringBuffer.toString();
                            }
                    }
                }
            case '(':
            case '[':
                a();
                return new b(this);
            case '{':
                a();
                return new d(this);
            default:
                StringBuffer stringBuffer2 = new StringBuffer();
                while (c2 >= ' ' && ",:]}/\\\"[{;=#".indexOf(c2) < 0) {
                    stringBuffer2.append(c2);
                    c2 = b();
                }
                a();
                String trim = stringBuffer2.toString().trim();
                if (!trim.equals("")) {
                    return d.e(trim);
                }
                throw a("Missing value");
        }
    }

    public final String toString() {
        return " at " + this.c + " [character " + this.f0a + " line " + this.d + "]";
    }
}
