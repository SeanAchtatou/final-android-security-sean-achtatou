package mediba.ad.sdk.android;

import android.util.Log;
import java.lang.ref.WeakReference;

final class x implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference f127a;

    public x(MasAdView masAdView) {
        this.f127a = new WeakReference(masAdView);
    }

    public final void run() {
        Log.w("MasAdView", "AdFailed");
        MasAdView masAdView = (MasAdView) this.f127a.get();
        if (masAdView == null) {
            return;
        }
        if (MasAdView.h(masAdView) == null || MasAdView.h(masAdView).getParent() == null) {
            masAdView.f103a = false;
        } else {
            masAdView.f103a = true;
        }
    }
}
