package mediba.ad.sdk.android;

import android.content.Intent;
import android.net.Uri;

final class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ j f114a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;

    i(j jVar, String str, String str2) {
        this.f114a = jVar;
        this.b = str;
        this.c = str2;
    }

    public final void run() {
        if (this.b.equals("nomal")) {
            ac.f107a = new Intent("android.intent.action.VIEW", Uri.parse(this.c));
            ac.q.startActivity(ac.f107a);
        } else if (this.b.equals("overlay")) {
            new ad(this.f114a.f115a.getContext(), this.c).a().show();
        }
    }
}
