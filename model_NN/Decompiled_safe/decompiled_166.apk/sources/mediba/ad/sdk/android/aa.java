package mediba.ad.sdk.android;

import android.util.Log;
import java.lang.ref.WeakReference;

final class aa implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference f105a;
    private WeakReference b;
    private int c;
    private boolean d = true;
    private /* synthetic */ MasAdView e;

    public aa(MasAdView masAdView, MasAdView masAdView2, ac acVar, int i) {
        this.e = masAdView;
        this.f105a = new WeakReference(masAdView2);
        this.b = new WeakReference(acVar);
        this.c = i;
    }

    public final void run() {
        try {
            MasAdView masAdView = (MasAdView) this.f105a.get();
            ac i = this.e.g;
            if (i != null) {
                masAdView.setClickable(false);
                if (MasAdView.o == 2) {
                    i.f();
                } else if (MasAdView.o == 3) {
                    i.h();
                } else if (MasAdView.o == 4) {
                    i.j();
                }
            }
            ac acVar = (ac) this.b.get();
            if (masAdView != null && acVar != null) {
                if (masAdView.g == null || masAdView.g.getParent() == null) {
                    masAdView.f103a = false;
                } else {
                    masAdView.f103a = true;
                }
                masAdView.removeAllViews();
                masAdView.addView(acVar);
                if (this.c == 0) {
                    if (this.d) {
                        MasAdView.a(masAdView, acVar);
                    } else {
                        MasAdView.a(acVar);
                    }
                }
                if (MasAdView.o == 2) {
                    acVar.e();
                } else if (MasAdView.o == 3) {
                    acVar.g();
                } else if (MasAdView.o == 4) {
                    acVar.i();
                }
                masAdView.setClickable(true);
                MasAdView.b(masAdView, acVar);
            }
        } catch (Exception e2) {
            Log.e("MasAdView", "Unhandled exception placing AdContainer into AdView.", e2);
            MasAdView masAdView2 = (MasAdView) this.f105a.get();
            if (masAdView2 != null) {
                MasAdView.a(masAdView2);
            }
        }
    }
}
