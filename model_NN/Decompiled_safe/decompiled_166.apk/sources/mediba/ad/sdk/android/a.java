package mediba.ad.sdk.android;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static String f104a = "MasAdManager";
    private static String b;
    private static String c;
    private static String d;
    private static String e;
    private static String f;
    private static int g;
    private static String h;
    private static String i;
    private static String j;
    private static String k;

    static {
        Log.i("MasAdManager", "mediba_ad_sdk_android version 1.0.0.0");
    }

    public static String a() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e2) {
            Log.e(f104a, "SocketException caught in MasAdManager getIpAddress, " + e2.getMessage());
        } catch (Exception e3) {
            Log.e(f104a, "exception caught in MasAdManager getIpAddress, " + e3.getMessage());
        }
        return null;
    }

    public static String a(Context context) {
        if (d == null) {
            f(context);
        }
        if (d == null && Log.isLoggable(f104a, 6)) {
            Log.e(f104a, "ADID not defined");
        }
        return d;
    }

    public static String a(Context context, boolean z) {
        if (c == null && z) {
            c = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        }
        return c;
    }

    private static void a(String str) {
        if (!str.startsWith("A")) {
            Log.e(f104a, "ADIDが不正にセットされています。");
        }
        d = str.substring(1, str.length());
    }

    private static void a(StringBuilder sb, String str, String str2) {
        if (str2 != null && str2.length() > 0) {
            try {
                sb.append("&").append(str).append("=").append(str2);
            } catch (Exception e2) {
                Log.e(f104a, "exception caught in AdRequestor extend_request_param, " + e2.getMessage());
            }
        }
    }

    public static String b(Context context) {
        if (e == null) {
            f(context);
        }
        if (e == null && Log.isLoggable(f104a, 6)) {
            Log.e(f104a, "APKEY not defined");
        }
        return e;
    }

    public static String b(Context context, boolean z) {
        boolean z2;
        boolean z3;
        boolean z4;
        String replace = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName().replace(" ", "");
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (z) {
            try {
                z4 = connectivityManager.getNetworkInfo(1).isConnectedOrConnecting();
            } catch (Exception e2) {
                Log.w(f104a, "no supported device.isWifi");
                z4 = false;
            }
            try {
                boolean z5 = z4;
                z3 = connectivityManager.getNetworkInfo(0).isConnectedOrConnecting();
                z2 = z5;
            } catch (Exception e3) {
                Log.w(f104a, "no supported device.isMobile");
                z2 = z4;
                z3 = false;
            }
        } else {
            z2 = false;
            z3 = false;
        }
        String str = Build.VERSION.RELEASE.split("-")[0];
        String g2 = g(context);
        String num = Integer.toString(context.getResources().getConfiguration().orientation);
        String str2 = z3 ? "1" : z2 ? "2" : null;
        String language = context.getResources().getConfiguration().locale.getLanguage();
        String country = context.getResources().getConfiguration().locale.getCountry();
        String str3 = i;
        String str4 = j;
        String str5 = k;
        StringBuilder sb = new StringBuilder();
        a(sb, "nv01", str);
        a(sb, "nv02", g2);
        a(sb, "nv03", num);
        a(sb, "nv04", replace);
        a(sb, "nv05", str2);
        a(sb, "nv06", language);
        a(sb, "nv07", country);
        a(sb, "K001", str3);
        a(sb, "K003", str4);
        a(sb, "K006", str5);
        String sb2 = sb.toString();
        h = sb2;
        return sb2;
    }

    public static String c(Context context) {
        if (b == null) {
            b = Settings.Secure.getString(context.getContentResolver(), "android_id");
        }
        return b;
    }

    public static int d(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (defaultDisplay != null) {
            return defaultDisplay.getWidth();
        }
        return 0;
    }

    public static int e(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (defaultDisplay != null) {
            return defaultDisplay.getHeight();
        }
        return 0;
    }

    private static void f(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                if (MasAdView.b()) {
                    e = "z8YiPhJxo0";
                    a("A30100520133");
                } else if (applicationInfo.metaData != null) {
                    String string = applicationInfo.metaData.getString("apkey");
                    Log.d(f104a, "Manifest APKEY:" + string);
                    if (e == null && string != null) {
                        e = string;
                    }
                    String string2 = applicationInfo.metaData.getString("aid");
                    Log.d(f104a, "Manifest AID:" + string2);
                    if (d == null && string2 != null) {
                        a(string2);
                    }
                }
                f = applicationInfo.packageName;
                if (Log.isLoggable(f104a, 2)) {
                    Log.i(f104a, "Application's package name is " + f);
                }
            }
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            if (packageInfo != null) {
                g = packageInfo.versionCode;
                if (Log.isLoggable(f104a, 2)) {
                    Log.i(f104a, "Application's version number is " + g);
                }
            }
        } catch (Exception e2) {
            Log.e(f104a, "exception caught in MasAdManager parseManifest, " + e2.getMessage());
        }
    }

    private static String g(Context context) {
        int i2;
        try {
            int i3 = context.getResources().getConfiguration().orientation;
            int d2 = i3 == 1 ? d(context) : e(context);
            int e2 = i3 == 1 ? e(context) : d(context);
            switch (d2) {
                case 240:
                    if (e2 != 320) {
                        if (e2 != 400) {
                            i2 = 0;
                            break;
                        } else {
                            i2 = 2;
                            break;
                        }
                    } else {
                        i2 = 1;
                        break;
                    }
                case 320:
                    if (e2 != 480) {
                        i2 = 0;
                        break;
                    } else {
                        i2 = 3;
                        break;
                    }
                case 360:
                    if (e2 != 640) {
                        i2 = 0;
                        break;
                    } else {
                        i2 = 4;
                        break;
                    }
                case 480:
                    if (e2 != 800) {
                        if (e2 != 854) {
                            if (e2 != 864) {
                                if (e2 != 960) {
                                    if (e2 != 1024) {
                                        i2 = 0;
                                        break;
                                    } else {
                                        i2 = 9;
                                        break;
                                    }
                                } else {
                                    i2 = 8;
                                    break;
                                }
                            } else {
                                i2 = 7;
                                break;
                            }
                        } else {
                            i2 = 6;
                            break;
                        }
                    } else {
                        i2 = 5;
                        break;
                    }
                default:
                    i2 = 0;
                    break;
            }
            return Integer.toString(i2);
        } catch (Exception e3) {
            Log.e(f104a, "exception caught in MasAdManager getDisplay, " + e3.getMessage());
            return null;
        }
    }
}
