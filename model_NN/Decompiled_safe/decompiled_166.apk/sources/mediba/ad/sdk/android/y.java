package mediba.ad.sdk.android;

import android.util.Log;
import java.lang.ref.WeakReference;

final class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private ac f128a;
    private WeakReference b;

    public y(ac acVar, c cVar) {
        this.f128a = acVar;
        this.b = new WeakReference(cVar);
    }

    public final void run() {
        try {
            if (this.f128a != null) {
                this.f128a.setPadding(0, 0, 0, 0);
                this.f128a.invalidate();
                this.f128a.requestLayout();
            } else {
                Log.e("AdProxy", "コンテナデータがありません");
            }
            c cVar = (c) this.b.get();
            if (cVar != null) {
                c.a(cVar);
            }
        } catch (Exception e) {
            Log.e("AdProxy", "exception caught in AdProxy Viewadd, " + e.getMessage());
        }
    }
}
