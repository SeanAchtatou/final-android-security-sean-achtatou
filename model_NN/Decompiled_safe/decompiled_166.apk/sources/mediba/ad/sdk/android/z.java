package mediba.ad.sdk.android;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.SystemClock;
import android.util.Log;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

final class z {

    /* renamed from: a  reason: collision with root package name */
    private static String f129a = "http://api.ad.mediba.jp/webapi";
    private static String b = null;
    private static boolean c;
    private static boolean d;

    z() {
    }

    private static String a(String str) {
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("String to encript cannot be null or zero length");
        }
        byte[] bArr = null;
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            bArr = instance.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return a(bArr);
    }

    private static String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < bArr.length; i++) {
            if ((bArr[i] & 255) < 16) {
                stringBuffer.append("0" + Integer.toHexString(bArr[i] & 255));
            } else {
                stringBuffer.append(Integer.toHexString(bArr[i] & 255));
            }
        }
        return stringBuffer.toString();
    }

    static c a(h hVar, Context context, ac acVar) {
        String str;
        String str2;
        c cVar;
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") == -1) {
            Log.e("AdRequestor", "INTERNET Permission missing in manifest");
            return null;
        }
        c = context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != -1;
        d = context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") != -1;
        SystemClock.uptimeMillis();
        Log.d("AdRequestor", "Ad Request");
        StringBuilder sb = new StringBuilder();
        String a2 = a.a(context);
        String a3 = MasAdView.a();
        String a4 = a.a() != null ? a.a() : "127.0.0.1";
        String b2 = a.b(context, c);
        String l = Long.toString(System.currentTimeMillis() / 1000);
        String a5 = a(a.b(context) + l);
        if (a.a(context, d) != null) {
            str = a.a(context, d);
            str2 = "android_did";
        } else {
            str = null;
            str2 = "android";
        }
        if (str == null && a.c(context) != null) {
            str = a.c(context);
            str2 = "android_id";
        }
        if (str == null) {
            str = a(a3 + a4);
            str2 = "android_oid";
        }
        sb.append("aid").append("=").append(a2);
        a(sb, "uid", str);
        a(sb, "type", str2);
        a(sb, "ua", a3);
        a(sb, "ip", a4);
        a(sb, "param", b2);
        a(sb, "time", l);
        a(sb, "key", a5);
        if (b == null) {
            StringBuilder sb2 = new StringBuilder();
            List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:jp.mediba")), 65563);
            if (queryIntentActivities == null || queryIntentActivities.size() == 0) {
                if (sb2.length() > 0) {
                    sb2.append(",");
                }
                sb2.append("a");
            }
            b = sb2.toString();
        }
        Log.d("AdRequestor", sb.toString());
        String sb3 = sb.toString();
        f fVar = new f(f129a, a.c(context), sb3);
        Log.d("AdRequestor", "POST params:  " + sb3);
        String str3 = null;
        boolean a6 = fVar.a();
        if (a6) {
            str3 = new String(fVar.b());
        }
        Log.d("AdRequestor", "connect_result: " + a6 + " / " + str3);
        if (!a6) {
            return null;
        }
        if (!str3.equals("")) {
            Log.d("AdRequestor", "Ad response: " + str3);
            str3.split(" ");
            cVar = c.a(hVar, str3, acVar);
        } else {
            cVar = null;
        }
        SystemClock.uptimeMillis();
        return cVar;
    }

    private static void a(StringBuilder sb, String str, String str2) {
        if (str2 != null && str2.length() > 0) {
            try {
                sb.append("&").append(URLEncoder.encode(str, "UTF-8")).append("=").append(URLEncoder.encode(str2, "UTF-8"));
                Log.d("AdRequestor", "    " + str + ": " + str2);
            } catch (Exception e) {
                Log.e("AdRequestor", "exception caught in AdRequestor extend_request_param, " + e.getMessage());
            }
        }
    }
}
