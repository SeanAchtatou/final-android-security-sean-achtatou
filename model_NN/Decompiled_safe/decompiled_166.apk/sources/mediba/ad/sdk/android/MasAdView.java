package mediba.ad.sdk.android;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import java.util.Vector;

public class MasAdView extends RelativeLayout {
    static final Handler b = new Handler();
    private static String n;
    /* access modifiers changed from: private */
    public static int o;
    private static boolean p;

    /* renamed from: a  reason: collision with root package name */
    public boolean f103a;
    private int c;
    private int d;
    private int e;
    private d f;
    /* access modifiers changed from: private */
    public ac g;
    private int h;
    private int i;
    private boolean j;
    private h k;
    private boolean l;
    private long m;
    private boolean q;
    private ab r;

    static {
        new Vector();
    }

    public MasAdView(Activity activity) {
        this(activity, null, 0);
    }

    public MasAdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MasAdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        boolean z;
        int i3;
        int i4;
        int i5;
        int i6 = 30;
        this.f103a = false;
        this.j = true;
        this.q = true;
        setFocusable(true);
        setFocusableInTouchMode(true);
        setDescendantFocusability(262144);
        setClickable(true);
        setSelected(true);
        setGravity(17);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", 1);
            int attributeUnsignedIntValue2 = attributeSet.getAttributeUnsignedIntValue(str, "requestInterval", 30);
            int attributeUnsignedIntValue3 = attributeSet.getAttributeUnsignedIntValue(str, "refreshAnimation", 1);
            int attributeUnsignedIntValue4 = attributeSet.getAttributeUnsignedIntValue(str, "visibility", 0);
            z = attributeSet.getAttributeBooleanValue(str, "testMode", false);
            int i7 = attributeUnsignedIntValue4;
            i5 = attributeUnsignedIntValue;
            i3 = i7;
            int i8 = attributeUnsignedIntValue2;
            i4 = attributeUnsignedIntValue3;
            i6 = i8;
        } else {
            z = false;
            i3 = 0;
            i4 = 1;
            i5 = 1;
        }
        setBackgroundColor(i5);
        a(i6);
        if (i4 == 0 || i4 == 1 || i4 == 2 || i4 == 3 || i4 == 4) {
            o = i4;
        } else {
            Log.w("MasAdView", "setRefreshAnimation(int) must be 0 or 1 or 2 or 3 or 4. Default is 1.");
            o = 1;
        }
        if (o == 0) {
            h();
            this.i = this.h;
            this.h = 0;
        } else if (o == 1 && this.i > 0) {
            this.h = this.i;
        }
        setVisibility(i3);
        p = z;
        n = new WebView(context).getSettings().getUserAgentString();
        Log.i("MasAdView", "create ads by mediba");
    }

    protected static String a() {
        return n;
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x003b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r7) {
        /*
            r6 = this;
            r3 = 300(0x12c, float:4.2E-43)
            r2 = 30
            java.lang.String r5 = "AdView.setRequestInterval("
            java.lang.String r4 = "MasAdView"
            int r0 = r7 * 1000
            int r1 = r6.h
            if (r1 == r0) goto L_0x005c
            if (r7 <= 0) goto L_0x0086
            if (r7 >= r2) goto L_0x005d
            java.lang.String r0 = "MasAdView"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "AdView.setRequestInterval("
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.String r1 = ") seconds must be >= "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            android.util.Log.w(r4, r0)
            r0 = 30000(0x7530, float:4.2039E-41)
            r1 = r2
        L_0x0037:
            r6.h = r0
            if (r1 > 0) goto L_0x003e
            r6.h()
        L_0x003e:
            java.lang.String r0 = "MasAdView"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "Requesting fresh ads every "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = " seconds."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.i(r4, r0)
        L_0x005c:
            return
        L_0x005d:
            if (r7 <= r3) goto L_0x0086
            java.lang.String r0 = "MasAdView"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "AdView.setRequestInterval("
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.String r1 = ") seconds must be <= "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            android.util.Log.w(r4, r0)
            r0 = 300000(0x493e0, float:4.2039E-40)
            r1 = r3
            goto L_0x0037
        L_0x0086:
            r1 = r7
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: mediba.ad.sdk.android.MasAdView.a(int):void");
    }

    static void a(MasAdView masAdView) {
        if (masAdView.f != null) {
            Handler handler = b;
            masAdView.getClass();
            handler.post(new x(masAdView));
        }
    }

    static void a(MasAdView masAdView, ac acVar) {
        masAdView.g = acVar;
    }

    static void a(ac acVar) {
        acVar.setVisibility(8);
    }

    private void a(boolean z) {
        synchronized (this) {
            if (z) {
                if (this.h > 0 && getVisibility() == 0) {
                    int i2 = this.h;
                    h();
                    if (this.q) {
                        getClass();
                        this.r = new ab(this);
                        b.postDelayed(this.r, (long) i2);
                        Log.d("MasAdView", "Ad refresh scheduled for " + i2 + " from now.");
                    }
                }
            }
            if (!z || this.h == 0 || getVisibility() != 0) {
                h();
            }
        }
    }

    static int b(MasAdView masAdView) {
        return masAdView.h;
    }

    static ac b(MasAdView masAdView, ac acVar) {
        masAdView.g = acVar;
        return acVar;
    }

    private void b(int i2) {
        this.c = -16777216 | i2;
        this.d = -16777216 | i2;
    }

    protected static boolean b() {
        return p;
    }

    static void c(MasAdView masAdView) {
        masAdView.g();
    }

    static h d(MasAdView masAdView) {
        if (masAdView.k == null) {
            masAdView.k = new h(masAdView);
        }
        return masAdView.k;
    }

    static boolean e(MasAdView masAdView) {
        masAdView.l = false;
        return false;
    }

    static void f(MasAdView masAdView) {
        masAdView.a(true);
    }

    static long g(MasAdView masAdView) {
        return masAdView.m;
    }

    private void g() {
        if ((this.j || super.getVisibility() == 0) && !this.l) {
            this.m = SystemClock.uptimeMillis();
            this.l = true;
            new e(this).start();
        }
    }

    static ac h(MasAdView masAdView) {
        return masAdView.g;
    }

    private void h() {
        if (this.r != null) {
            this.r.f106a = true;
            this.r = null;
            Log.v("MasAdView", "Cancelled an ad refresh scheduled for the future.");
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(c cVar, ac acVar) {
        int visibility = super.getVisibility();
        double b2 = cVar.b();
        if (b2 >= 0.0d) {
            a((int) b2);
            a(true);
        }
        if (this.j) {
            this.j = false;
        }
        acVar.a(cVar);
        acVar.setVisibility(visibility);
        acVar.setGravity(17);
        cVar.a(acVar);
        acVar.setLayoutParams(new RelativeLayout.LayoutParams(cVar.a(cVar.c()), cVar.a(cVar.d())));
        b.post(new aa(this, this, acVar, visibility));
    }

    public final int c() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public final int d() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public final int e() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        a(true);
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        a(false);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        c d2;
        super.onMeasure(i2, i3);
        int size = View.MeasureSpec.getSize(i2);
        int size2 = View.MeasureSpec.getSize(i3);
        int mode = View.MeasureSpec.getMode(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int d3 = (mode == Integer.MIN_VALUE || mode == 1073741824) ? size : a.d(getContext());
        if (mode2 == 1073741824) {
            i4 = size2;
        } else {
            int i5 = 0;
            if (!(this.g == null || (d2 = this.g.d()) == null)) {
                i5 = d2.a(d2.d());
                if (mode2 == Integer.MIN_VALUE && size2 < i5) {
                    i4 = size2;
                }
            }
            i4 = i5;
        }
        setMeasuredDimension(d3, i4);
        Log.v("MasAdView", "AdView.onMeasure:  widthSize " + size + " heightSize " + size2);
        Log.v("MasAdView", "AdView.onMeasure:  measuredWidth " + d3 + " measuredHeight " + i4);
        int measuredWidth = getMeasuredWidth();
        Log.d("MasAdView", "AdView size is " + measuredWidth + " by " + getMeasuredHeight());
        if (this.g != null && ((float) measuredWidth) / ac.a() <= 310.0f) {
            Log.w("MasAdView", "表示領域の横幅が狭すぎます。");
            h();
        } else if (this.j) {
            g();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        a(z);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        a(i2 == 0);
    }

    public void setBackgroundColor(int i2) {
        if (i2 == 1 || i2 == 2) {
            this.e = i2;
        } else {
            Log.w("MasAdView", "setBackgroundColor(int) must be 1(Black) or 2(White). Default is 1.");
            this.e = 1;
        }
        if (this.e == 1) {
            b(Color.rgb(255, 255, 255));
        } else if (this.e == 2) {
            b(Color.rgb(0, 0, 0));
        }
        invalidate();
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.q = z;
        if (z) {
            setVisibility(0);
        } else {
            setVisibility(8);
        }
    }

    public void setVisibility(int i2) {
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    b.post(new w(getChildAt(i3), i2));
                }
                super.setVisibility(i2);
                invalidate();
            }
        }
        a(i2 == 0);
    }
}
