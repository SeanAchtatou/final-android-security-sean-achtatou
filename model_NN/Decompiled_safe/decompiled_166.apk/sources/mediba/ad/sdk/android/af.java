package mediba.ad.sdk.android;

import android.os.Build;
import android.util.Log;
import java.net.URL;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executor;

public abstract class af implements Runnable {
    private static Executor j = null;
    private static String k;

    /* renamed from: a  reason: collision with root package name */
    protected int f109a;
    protected Exception b = null;
    protected Map c;
    protected int d;
    protected int e;
    protected URL f;
    protected byte[] g;
    protected boolean h;
    protected b i;
    private String l;
    private String m = null;
    private String n;
    private String o;

    protected af(String str, String str2) {
        this.n = str;
        this.i = null;
        this.f109a = 3000;
        this.c = null;
        this.h = true;
        this.d = 0;
        this.e = 3;
        if (str2 != null) {
            this.o = str2;
            this.l = "application/x-www-form-urlencoded";
            return;
        }
        this.o = null;
        this.l = null;
    }

    public static String c() {
        if (k == null) {
            StringBuffer stringBuffer = new StringBuffer();
            String str = Build.VERSION.RELEASE;
            if (str.length() > 0) {
                stringBuffer.append(str);
            } else {
                stringBuffer.append("1.0");
            }
            stringBuffer.append("; ");
            String language = Locale.getDefault().getLanguage();
            if (language != null) {
                stringBuffer.append(language.toLowerCase());
                if (Locale.getDefault().getCountry() != null) {
                    stringBuffer.append("-");
                    stringBuffer.append(Locale.getDefault().getCountry().toLowerCase());
                }
            } else {
                stringBuffer.append("en");
            }
            String str2 = Build.MODEL;
            if (str2.length() > 0) {
                stringBuffer.append("; ");
                stringBuffer.append(str2);
            }
            String str3 = Build.ID;
            if (str3.length() > 0) {
                stringBuffer.append(" Build/");
                stringBuffer.append(str3);
            }
            k = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/528.5+ (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1", stringBuffer);
            Log.d("AdProxyConnector", "user-agent :  " + k);
        }
        return k;
    }

    public abstract boolean a();

    public final byte[] b() {
        return this.g;
    }

    public final String d() {
        return this.m;
    }
}
