package mediba.ad.sdk.android;

import android.content.Intent;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;
import java.util.Hashtable;
import java.util.Vector;

public final class c implements View.OnClickListener, b {

    /* renamed from: a  reason: collision with root package name */
    public static Intent f110a;
    private static String b;
    private static String c;
    private static String d;
    private static String e;
    private static String f;
    private static String g;
    private static String h;
    private int i = -1;
    private int j = 48;
    private h k = null;
    private ac l;
    private Hashtable m = new Hashtable();
    private double n;
    private double o;

    protected c() {
        new Vector();
        this.n = -1.0d;
        this.o = -1.0d;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static mediba.ad.sdk.android.c a(mediba.ad.sdk.android.h r8, java.lang.String r9, mediba.ad.sdk.android.ac r10) {
        /*
            r4 = 0
            java.lang.String r7 = "不正なADを所得しました。"
            java.lang.String r6 = "clickurl"
            java.lang.String r5 = "AdProxy"
            mediba.ad.sdk.android.c r0 = new mediba.ad.sdk.android.c
            r0.<init>()
            r0.k = r8
            r0.l = r10
            a.a.d r1 = new a.a.d
            r1.<init>(r9)
            int r2 = r1.a()
            r3 = 6
            if (r2 == r3) goto L_0x002c
            int r2 = r1.a()
            r3 = 3
            if (r2 == r3) goto L_0x002c
            java.lang.String r0 = "AdProxy"
            java.lang.String r0 = "不正なADを所得しました。"
            android.util.Log.w(r5, r7)
            r0 = r4
        L_0x002b:
            return r0
        L_0x002c:
            java.lang.String r2 = "type"
            java.lang.String r2 = r1.d(r2)
            java.lang.String r2 = r2.toString()
            mediba.ad.sdk.android.c.b = r2
            float r2 = mediba.ad.sdk.android.ac.a()
            double r2 = (double) r2
            r0.o = r2
            mediba.ad.sdk.android.c.c = r4
            mediba.ad.sdk.android.c.d = r4
            mediba.ad.sdk.android.c.e = r4
            mediba.ad.sdk.android.c.f = r4
            mediba.ad.sdk.android.c.g = r4
            mediba.ad.sdk.android.c.h = r4
            java.lang.String r2 = mediba.ad.sdk.android.c.b
            java.lang.String r3 = "1"
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = mediba.ad.sdk.android.c.b
            java.lang.String r3 = "3"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x007f
        L_0x005f:
            java.lang.String r2 = "imageurl"
            java.lang.String r2 = r1.d(r2)
            java.lang.String r2 = r2.toString()
            mediba.ad.sdk.android.c.f = r2
            java.lang.String r2 = "clickurl"
            java.lang.String r1 = r1.d(r6)
            java.lang.String r1 = r1.toString()
            mediba.ad.sdk.android.c.c = r1
        L_0x0077:
            r0.f()
            r1 = 1
        L_0x007b:
            if (r1 != 0) goto L_0x002b
            r0 = r4
            goto L_0x002b
        L_0x007f:
            java.lang.String r2 = mediba.ad.sdk.android.c.b
            java.lang.String r3 = "2"
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x0093
            java.lang.String r2 = mediba.ad.sdk.android.c.b
            java.lang.String r3 = "4"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x00d0
        L_0x0093:
            java.lang.String r2 = "icon1url"
            java.lang.String r2 = r1.d(r2)
            java.lang.String r2 = r2.toString()
            mediba.ad.sdk.android.c.g = r2
            java.lang.String r2 = "icon2url"
            java.lang.String r2 = r1.d(r2)
            java.lang.String r2 = r2.toString()
            mediba.ad.sdk.android.c.h = r2
            java.lang.String r2 = "text1"
            java.lang.String r2 = r1.d(r2)
            java.lang.String r2 = r2.toString()
            mediba.ad.sdk.android.c.d = r2
            java.lang.String r2 = "text2"
            java.lang.String r2 = r1.d(r2)
            java.lang.String r2 = r2.toString()
            mediba.ad.sdk.android.c.e = r2
            java.lang.String r2 = "clickurl"
            java.lang.String r1 = r1.d(r6)
            java.lang.String r1 = r1.toString()
            mediba.ad.sdk.android.c.c = r1
            goto L_0x0077
        L_0x00d0:
            java.lang.String r1 = "AdProxy"
            java.lang.String r1 = "不正なADを所得しました。"
            android.util.Log.w(r5, r7)
            r1 = 0
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: mediba.ad.sdk.android.c.a(mediba.ad.sdk.android.h, java.lang.String, mediba.ad.sdk.android.ac):mediba.ad.sdk.android.c");
    }

    static void a(c cVar) {
        if (cVar.k != null) {
            cVar.k.a(cVar);
        }
    }

    private void f() {
        try {
            y yVar = new y(this.l, this);
            g();
            MasAdView.b.post(yVar);
            if (this.m != null) {
                this.m.clear();
                this.m = null;
            }
        } catch (Exception e2) {
            Log.e("AdProxy", "Handler Illeguler in AdProxy.constructView ");
        }
    }

    private boolean g() {
        new Rect(50, 50, 50, 50);
        try {
            this.l.getContext();
            if (b.equals("1")) {
                this.l.a(f);
                this.l.a(c, "nomal");
                return true;
            } else if (b.equals("2")) {
                this.l.a(d, e, g, h);
                this.l.b(c, "nomal");
                return true;
            } else if (b.equals("3")) {
                this.l.a(f);
                this.l.a(c, "overlay");
                return true;
            } else if (!b.equals("4")) {
                return true;
            } else {
                this.l.a(d, e, g, h);
                this.l.b(c, "overlay");
                return true;
            }
        } catch (Exception e2) {
            Log.e("AdProxy", "Handler Illeguler in AdProxy.createInnerView " + e2.getMessage());
            return true;
        }
    }

    private void h() {
        if (this.m != null) {
            this.m.clear();
            this.m = null;
        }
        if (this.k != null) {
            this.k.a();
        }
    }

    /* access modifiers changed from: package-private */
    public final int a(int i2) {
        double d2 = (double) i2;
        if (this.o > 0.0d) {
            d2 *= this.o;
        }
        return (int) d2;
    }

    public final void a() {
        h();
    }

    public final void a(ac acVar) {
        this.l = acVar;
    }

    public final void a(af afVar) {
        String d2 = afVar.d();
        byte[] b2 = afVar.b();
        if (b2 != null) {
            this.m.put(d2, b2);
            f();
            return;
        }
        Log.d("AdProxy", "Failed reading asset(" + d2 + ") for ad");
        h();
    }

    /* access modifiers changed from: package-private */
    public final double b() {
        return this.n;
    }

    public final int c() {
        return this.i;
    }

    public final int d() {
        return this.j;
    }

    public final ac e() {
        return this.l;
    }

    public final void onClick(View view) {
        Log.d("AdProxy", "View Clicked");
    }
}
