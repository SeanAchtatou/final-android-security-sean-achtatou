package mediba.ad.sdk.android;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public final class ad {
    private static float m = -1.0f;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f108a;
    /* access modifiers changed from: private */
    public LinearLayout b;
    private LinearLayout c;
    /* access modifiers changed from: private */
    public TextView d;
    private ImageView e;
    /* access modifiers changed from: private */
    public ImageView f;
    /* access modifiers changed from: private */
    public WebView g;
    private RelativeLayout h;
    /* access modifiers changed from: private */
    public ProgressDialog i;
    private int j;
    private int k;
    private String l;

    public ad(Context context, String str) {
        this.f108a = context;
        this.l = str;
        Log.d("AdOverlay", context.toString());
    }

    private static Bitmap a(URL url) {
        Bitmap bitmap;
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(((JarURLConnection) url.openConnection()).getInputStream(), 1048576);
            bitmap = BitmapFactory.decodeStream(bufferedInputStream, null, new BitmapFactory.Options());
            try {
                bufferedInputStream.close();
                return bitmap;
            } catch (MalformedURLException e2) {
                e = e2;
                e.printStackTrace();
                return bitmap;
            } catch (IOException e3) {
                e = e3;
                e.printStackTrace();
                return bitmap;
            }
        } catch (MalformedURLException e4) {
            e = e4;
            bitmap = null;
        } catch (IOException e5) {
            e = e5;
            bitmap = null;
            e.printStackTrace();
            return bitmap;
        }
    }

    public final ae a() {
        ae aeVar = new ae(this.f108a);
        aeVar.requestWindowFeature(1);
        m = this.f108a.getResources().getDisplayMetrics().density;
        this.j = a.d(this.f108a);
        this.k = a.e(this.f108a);
        int i2 = (int) (6.0f * m);
        this.i = new ProgressDialog(this.f108a);
        this.i.requestWindowFeature(1);
        this.i.setMessage("Loading");
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.j, this.k);
        this.b = new LinearLayout(this.f108a);
        this.b.setOrientation(1);
        this.b.setVisibility(4);
        this.h = new RelativeLayout(this.f108a);
        this.e = new ImageView(this.f108a);
        Bitmap a2 = a(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/header_bg.png"));
        Bitmap a3 = a(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/header_btn.png"));
        Bitmap a4 = a(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/header_btn_over.png"));
        this.e.setImageBitmap(a2);
        this.e.setScaleType(ImageView.ScaleType.FIT_XY);
        this.e.setLayoutParams(new Gallery.LayoutParams(-1, (int) (40.0f * m)));
        this.h.addView(this.e);
        this.c = new LinearLayout(this.f108a);
        this.c.setOrientation(0);
        this.d = new TextView(this.f108a);
        this.d.setTextColor(-1);
        this.d.setWidth(this.j - ((int) (128.0f * m)));
        this.d.setSingleLine();
        this.d.setFocusable(true);
        this.d.setSelected(true);
        this.d.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        this.d.setMarqueeRepeatLimit(10);
        this.d.setGravity(16);
        this.d.setPadding((int) (m * 10.0f), (int) (m * 10.0f), 0, 0);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams((int) (86.0f * m), (int) (28.0f * m));
        layoutParams2.setMargins(i2, i2, i2, i2);
        this.f = new ImageView(this.f108a);
        this.f.setImageBitmap(a3);
        this.f.setOnTouchListener(new v(this, a4, a3));
        this.f.setOnClickListener(new t(aeVar));
        this.c.addView(this.d);
        this.c.addView(this.f, layoutParams2);
        this.h.addView(this.c);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(this.j - ((int) (25.0f * m)), this.k - ((int) (90.0f * m)));
        this.g = new WebView(this.f108a);
        this.g.setHorizontalScrollBarEnabled(true);
        this.g.setVerticalScrollBarEnabled(true);
        this.g.getSettings().setJavaScriptEnabled(true);
        this.g.setWebViewClient(new u(this));
        this.g.loadUrl(this.l);
        this.b.addView(this.h);
        this.b.addView(this.g, layoutParams3);
        aeVar.addContentView(this.b, layoutParams);
        return aeVar;
    }
}
