package mediba.ad.sdk.android;

import a.a.f;
import android.content.Context;
import android.util.Log;
import java.lang.ref.WeakReference;

public final class e extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference f111a;

    public e(MasAdView masAdView) {
        this.f111a = new WeakReference(masAdView);
    }

    public final void run() {
        Log.d("AdRequestThread", "Starting AdRequestThread");
        MasAdView masAdView = (MasAdView) this.f111a.get();
        Context context = masAdView.getContext();
        ac acVar = new ac(context, masAdView);
        int measuredWidth = (int) (((float) masAdView.getMeasuredWidth()) / ac.a());
        if (masAdView.getMeasuredWidth() <= 0 || ((float) measuredWidth) > 310.0f) {
            try {
                if (z.a(MasAdView.d(masAdView), context, acVar) == null) {
                    MasAdView.a(masAdView);
                }
            } catch (f e) {
                Log.e("AdRequestThread", "JSONException caught in AdRequestThread.run " + e.getMessage());
            } catch (Exception e2) {
                Log.e("AdRequestThread", "Unhandled exception caught in AdRequestThread.run " + e2.getMessage());
            }
        } else {
            Log.w("MasAdView", "表示領域の横幅が狭すぎます。");
        }
        MasAdView.e(masAdView);
        MasAdView.f(masAdView);
    }
}
