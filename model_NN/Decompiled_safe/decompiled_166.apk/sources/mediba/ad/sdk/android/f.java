package mediba.ad.sdk.android;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public final class f extends af {
    private URL j;
    private HttpURLConnection k;

    public f(String str, String str2, String str3) {
        super(str2, str3);
        try {
            Log.d("AdProxyURLConnector", str);
            this.j = new URL(String.valueOf(str) + "?" + str3);
            this.f = this.j;
        } catch (Exception e) {
            this.j = null;
            this.b = e;
            Log.e("AdProxyURLConnector", "exception caught in AdProxyURLConnector " + e.getMessage());
        }
        this.k = null;
        this.d = 0;
    }

    private void e() {
        if (this.k != null) {
            this.k.disconnect();
            this.k = null;
        }
    }

    /* JADX INFO: finally extract failed */
    public final boolean a() {
        boolean z;
        boolean z2;
        String str;
        if (this.j == null) {
            if (this.i != null) {
                b bVar = this.i;
                new Exception("url was null");
                bVar.a();
            }
            z = false;
        } else {
            Log.d("AdProxyURLConnector", "URLConn");
            HttpURLConnection.setFollowRedirects(true);
            boolean z3 = false;
            while (this.d < this.e && !z3) {
                Log.v("AdProxyURLConnector", "attempt " + this.d + " to connect to url " + this.j);
                try {
                    e();
                    this.k = (HttpURLConnection) this.j.openConnection();
                    if (this.k != null) {
                        this.k.setRequestProperty("User-Agent", c());
                        this.k.setConnectTimeout(this.f109a);
                        this.k.setReadTimeout(this.f109a);
                        if (this.c != null) {
                            for (String str2 : this.c.keySet()) {
                                if (!(str2 == null || (str = (String) this.c.get(str2)) == null)) {
                                    this.k.addRequestProperty(str2, str);
                                }
                            }
                        }
                        this.k.connect();
                        int responseCode = this.k.getResponseCode();
                        if (responseCode >= 200 && responseCode < 300) {
                            this.f = this.k.getURL();
                            if (this.h) {
                                BufferedInputStream bufferedInputStream = new BufferedInputStream(this.k.getInputStream(), 4096);
                                byte[] bArr = new byte[4096];
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(4096);
                                while (true) {
                                    int read = bufferedInputStream.read(bArr);
                                    if (read == -1) {
                                        break;
                                    }
                                    byteArrayOutputStream.write(bArr, 0, read);
                                }
                                this.g = byteArrayOutputStream.toByteArray();
                            }
                            if (this.i != null) {
                                this.i.a(this);
                            }
                            z2 = true;
                            e();
                            e();
                            this.d++;
                            z3 = z2;
                        }
                    }
                    z2 = z3;
                    e();
                    e();
                } catch (Exception e) {
                    Log.w("AdProxyURLConnector", "could not open connection to url " + this.j, e);
                    this.b = e;
                    e();
                    z2 = false;
                } catch (Throwable th) {
                    e();
                    throw th;
                }
                this.d++;
                z3 = z2;
            }
            z = z3;
        }
        if (this.i != null && z) {
            this.i.a();
        }
        return z;
    }

    public final void run() {
        try {
            a();
        } catch (Exception e) {
            if (Log.isLoggable("AdProxyURLConnector", 6)) {
                Log.e("AdProxyURLConnector", "exception caught in AdProxyURLConnector.run(), " + e.getMessage());
            }
        }
    }
}
