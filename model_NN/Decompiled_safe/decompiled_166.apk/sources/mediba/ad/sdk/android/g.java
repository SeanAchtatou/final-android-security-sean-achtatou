package mediba.ad.sdk.android;

import android.view.MotionEvent;
import android.view.View;

final class g implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ac f112a;

    g(ac acVar) {
        this.f112a = acVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.f112a.setPressed(true);
                break;
            case 1:
                if (this.f112a.p.isPressed()) {
                    this.f112a.p.performClick();
                }
                this.f112a.setPressed(false);
                break;
        }
        return false;
    }
}
