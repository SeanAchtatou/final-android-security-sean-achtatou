package mediba.ad.sdk.android;

import android.view.MotionEvent;
import android.view.View;

final class k implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ac f116a;

    k(ac acVar) {
        this.f116a = acVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.f116a.setPressed(true);
                break;
            case 1:
                if (this.f116a.f.isPressed()) {
                    this.f116a.f.performClick();
                }
                this.f116a.setPressed(false);
                break;
        }
        return false;
    }
}
