package mediba.ad.sdk.android;

import android.graphics.Bitmap;
import android.view.MotionEvent;
import android.view.View;

final class v implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ad f125a;
    private final /* synthetic */ Bitmap b;
    private final /* synthetic */ Bitmap c;

    v(ad adVar, Bitmap bitmap, Bitmap bitmap2) {
        this.f125a = adVar;
        this.b = bitmap;
        this.c = bitmap2;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.f125a.f.setImageBitmap(this.b);
                return false;
            case 1:
                this.f125a.f.setImageBitmap(this.c);
                return false;
            default:
                return false;
        }
    }
}
