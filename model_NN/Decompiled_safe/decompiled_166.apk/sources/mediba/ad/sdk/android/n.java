package mediba.ad.sdk.android;

import android.os.Handler;
import android.view.View;
import android.view.animation.ScaleAnimation;

final class n implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ ac f118a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;

    n(ac acVar, String str, String str2) {
        this.f118a = acVar;
        this.b = str;
        this.c = str2;
    }

    public final void onClick(View view) {
        this.f118a.f.setClickable(false);
        if (ac.m.getVisibility() != 0) {
            Handler handler = new Handler();
            handler.post(new m());
            handler.postDelayed(new p(this, this.b, this.c), 500);
            handler.postDelayed(new o(this), 1000);
            return;
        }
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.2f, 0.0f, 1.2f, 0.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(500);
        ac.m.startAnimation(scaleAnimation);
        ac.m.setVisibility(4);
        Handler handler2 = new Handler();
        handler2.postDelayed(new r(), 600);
        handler2.postDelayed(new q(this, this.b, this.c), 1000);
        handler2.postDelayed(new s(this), 2000);
    }
}
