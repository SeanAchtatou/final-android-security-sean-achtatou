package mediba.ad.sdk.android;

import android.os.SystemClock;
import android.util.Log;
import java.lang.ref.WeakReference;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference f113a;

    public h(MasAdView masAdView) {
        this.f113a = new WeakReference(masAdView);
    }

    public final void a() {
        MasAdView masAdView = (MasAdView) this.f113a.get();
        if (masAdView != null) {
            MasAdView.a(masAdView);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(c cVar) {
        MasAdView masAdView = (MasAdView) this.f113a.get();
        if (masAdView != null) {
            synchronized (masAdView) {
                try {
                    if (MasAdView.h(masAdView) == null || !cVar.equals(MasAdView.h(masAdView).d())) {
                        Log.i("AdProxy", "Ad Response (" + (SystemClock.uptimeMillis() - MasAdView.g(masAdView)) + " ms)");
                        masAdView.getContext();
                        masAdView.a(cVar, cVar.e());
                    } else {
                        Log.d("AdProxy", "Same AD SKIP");
                    }
                } catch (Exception e) {
                    Log.e("AdProxy", "Handler Illeguler in AdProxy.a(AdProxy adproxy) " + e.getMessage());
                }
            }
            return;
        }
        return;
    }
}
