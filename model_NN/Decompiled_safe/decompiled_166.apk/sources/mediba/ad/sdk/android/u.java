package mediba.ad.sdk.android;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class u extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private boolean f124a;
    private /* synthetic */ ad b;

    /* synthetic */ u(ad adVar) {
        this(adVar, (byte) 0);
    }

    private u(ad adVar, byte b2) {
        this.b = adVar;
        this.f124a = false;
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        String title = this.b.g.getTitle();
        if (title != null && title.length() > 0) {
            this.b.d.setText(title);
        }
        this.b.i.dismiss();
        this.b.b.setVisibility(0);
        ac.c();
        ac.b();
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        this.b.i.show();
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        super.shouldOverrideUrlLoading(webView, str);
        if (!this.f124a) {
            webView.loadUrl(str);
            this.f124a = true;
        } else if (this.f124a) {
            this.b.f108a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        }
        return true;
    }
}
