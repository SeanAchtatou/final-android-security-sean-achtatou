package mediba.ad.sdk.android;

import android.os.Handler;
import android.view.View;

final class j implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ ac f115a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;

    j(ac acVar, String str, String str2) {
        this.f115a = acVar;
        this.b = str;
        this.c = str2;
    }

    public final void onClick(View view) {
        this.f115a.p.setClickable(false);
        Handler handler = new Handler();
        handler.post(new i(this, this.b, this.c));
        handler.postDelayed(new l(this), 1000);
    }
}
