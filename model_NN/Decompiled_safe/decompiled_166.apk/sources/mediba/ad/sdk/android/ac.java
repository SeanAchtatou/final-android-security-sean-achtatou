package mediba.ad.sdk.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public final class ac extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    public static Intent f107a;
    private static float d = -1.0f;
    /* access modifiers changed from: private */
    public static ImageView m;
    /* access modifiers changed from: private */
    public static ProgressBar o;
    /* access modifiers changed from: private */
    public static Context q;
    private final MasAdView b;
    private c c;
    private Long e = -1L;
    /* access modifiers changed from: private */
    public RelativeLayout f;
    private TextView g;
    private TextView h;
    private TextView i;
    private ImageView j;
    private ImageView k;
    private ImageView l;
    private LinearLayout n;
    /* access modifiers changed from: private */
    public ImageView p;

    static {
        Log.d("AdContainer", "Activate AdContainer");
    }

    public ac(Context context, MasAdView masAdView) {
        super(context);
        Bitmap bitmap;
        Bitmap a2;
        q = context;
        setFocusable(false);
        setFocusableInTouchMode(false);
        Log.d("AdContainer", "AdContainer Construct");
        this.e = -1L;
        this.b = masAdView;
        setId(1);
        d = getResources().getDisplayMetrics().density;
        setBackgroundDrawable(context.getResources().getDrawable(17301602));
        int i2 = (int) (48.0f * d);
        int i3 = (int) (5.0f * d);
        int i4 = (int) (8.0f * d);
        int d2 = a.d(context);
        a.e(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        this.p = new ImageView(context);
        this.p.setScaleType(ImageView.ScaleType.FIT_CENTER);
        this.f = new RelativeLayout(context);
        this.f.setBackgroundColor(this.b.c());
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(i2, i2);
        this.j = new ImageView(context);
        this.j.setScaleType(ImageView.ScaleType.FIT_XY);
        this.j.setPadding(i3, i3, i3, i3);
        this.j.setId(10);
        this.f.addView(this.j, layoutParams2);
        if (this.b.c() == 2) {
            Bitmap a3 = a(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/gradation_w_alpha_left.png"));
            bitmap = a3;
            a2 = a(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/gradation_w_alpha_right.png"));
        } else {
            Bitmap a4 = a(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/gradation_b_alpha_left.png"));
            bitmap = a4;
            a2 = a(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/gradation_b_alpha_right.png"));
        }
        this.n = new LinearLayout(context);
        this.n.setOrientation(0);
        this.k = new ImageView(context);
        this.k.setImageBitmap(bitmap);
        this.k.setScaleType(ImageView.ScaleType.FIT_XY);
        this.k.setLayoutParams(new Gallery.LayoutParams(i2, i2));
        this.l = new ImageView(context);
        this.l.setImageBitmap(a2);
        this.l.setScaleType(ImageView.ScaleType.FIT_XY);
        this.l.setLayoutParams(new Gallery.LayoutParams(d2, -1));
        this.n.addView(this.k);
        this.n.addView(this.l);
        this.f.addView(this.n);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(i2, i2);
        layoutParams3.addRule(11);
        ProgressBar progressBar = new ProgressBar(context);
        o = progressBar;
        progressBar.setPadding(i4, i4, i4, i4);
        o.setMax(2);
        o.incrementProgressBy(1);
        o.setVisibility(4);
        this.f.addView(o, layoutParams3);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(i2, i2);
        layoutParams4.addRule(11);
        ImageView imageView = new ImageView(context);
        m = imageView;
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        m.setPadding(i3, i3, i3, i3);
        m.setId(13);
        this.f.addView(m, layoutParams4);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, -2);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams5.setMargins(0, (int) (5.0f * d), 0, -((int) (3.0f * d)));
        layoutParams5.addRule(1, 10);
        layoutParams6.addRule(1, 10);
        layoutParams6.addRule(3, 11);
        layoutParams6.setMargins(0, 0, 0, -((int) (5.0f * d)));
        this.g = new TextView(context);
        this.g.setId(11);
        this.g.setTextColor(this.b.d());
        this.h = new TextView(context);
        this.h.setId(12);
        this.h.setTextColor(this.b.e());
        this.f.addView(this.g, layoutParams5);
        this.f.addView(this.h, layoutParams6);
        RelativeLayout.LayoutParams layoutParams7 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams7.addRule(3, 12);
        layoutParams7.addRule(11);
        this.i = new TextView(context);
        this.i.setTextSize(10.0f);
        this.i.setText("Ads by mediba");
        if (this.b.c() == 1) {
            this.i.setTextColor(Color.rgb(153, 153, 153));
        } else if (this.b.c() == 2) {
            this.i.setTextColor(Color.rgb(102, 102, 102));
        }
        this.f.addView(this.i, layoutParams7);
        addView(this.f);
        addView(this.p, layoutParams);
        a((c) null);
    }

    static float a() {
        return d;
    }

    private static Bitmap a(URL url) {
        Bitmap bitmap;
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(((JarURLConnection) url.openConnection()).getInputStream(), 1048576);
            bitmap = BitmapFactory.decodeStream(bufferedInputStream, null, new BitmapFactory.Options());
            try {
                bufferedInputStream.close();
                return bitmap;
            } catch (MalformedURLException e2) {
                e = e2;
                Log.e("AdContainer", "MalformedURLException caught in AdContainer.getImageBitmap(), " + e.getMessage());
                e.printStackTrace();
                return bitmap;
            } catch (IOException e3) {
                e = e3;
                Log.e("AdContainer", "IOException caught in AdContainer.getImageBitmap(), " + e.getMessage());
                e.printStackTrace();
                return bitmap;
            } catch (Exception e4) {
                e = e4;
                Log.e("AdContainer", "exception caught in AdContainer.getImageBitmap(), " + e.getMessage());
                e.printStackTrace();
                return bitmap;
            }
        } catch (MalformedURLException e5) {
            e = e5;
            bitmap = null;
        } catch (IOException e6) {
            e = e6;
            bitmap = null;
            Log.e("AdContainer", "IOException caught in AdContainer.getImageBitmap(), " + e.getMessage());
            e.printStackTrace();
            return bitmap;
        } catch (Exception e7) {
            e = e7;
            bitmap = null;
            Log.e("AdContainer", "exception caught in AdContainer.getImageBitmap(), " + e.getMessage());
            e.printStackTrace();
            return bitmap;
        }
    }

    private static Drawable b(String str) {
        try {
            Object content = new URL(str).getContent();
            Log.d("AdContainer", "fetching:" + str);
            return Drawable.createFromStream((InputStream) content, "src");
        } catch (MalformedURLException e2) {
            Log.e("AdContainer", "MalformedURLException caught in AdContainer.ImageOperations(), " + e2.getMessage());
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            Log.e("AdContainer", "IOException caught in AdContainer.ImageOperations(), " + e3.getMessage());
            e3.printStackTrace();
            return null;
        } catch (Exception e4) {
            e4.printStackTrace();
            Log.e("AdContainer", "exception caught in AdContainer.ImageOperations(), " + e4.getMessage());
            return null;
        }
    }

    protected static void b() {
        m.setVisibility(0);
    }

    protected static void c() {
        o.setVisibility(4);
    }

    public final void a(String str) {
        ImageView imageView = this.p;
        getContext();
        imageView.setImageDrawable(b(str));
        this.f.setVisibility(4);
        this.p.setVisibility(0);
        invalidate();
    }

    public final void a(String str, String str2) {
        this.p.setOnTouchListener(new g(this));
        this.p.setOnClickListener(new j(this, str2, str));
    }

    public final void a(String str, String str2, String str3, String str4) {
        this.g.setTextSize(12.0f);
        this.h.setTextSize(12.0f);
        this.g.setText(str);
        this.h.setText(str2);
        getContext();
        Drawable b2 = b(str3);
        getContext();
        Drawable b3 = b(str4);
        if (b2 != null && b3 != null) {
            this.j.setImageDrawable(b2);
            m.setImageDrawable(b3);
            this.i.setPadding(0, 0, (int) (48.0f * d), 0);
        } else if (b2 != null && b3 == null) {
            this.j.setImageDrawable(b2);
            m.setVisibility(4);
            int i2 = (int) (d * 14.0f);
            o.setPadding(i2, i2, i2, i2);
            this.i.setPadding(0, 0, (int) (d * 8.0f), 0);
        } else if (b2 == null && b3 != null) {
            this.j.setImageDrawable(b3);
            m.setVisibility(4);
            int i3 = (int) (d * 14.0f);
            o.setPadding(i3, i3, i3, i3);
            this.i.setPadding(0, 0, (int) (d * 8.0f), 0);
        }
        this.p.setVisibility(4);
        this.f.setVisibility(0);
        invalidate();
    }

    /* access modifiers changed from: package-private */
    public final void a(c cVar) {
        this.c = cVar;
        if (cVar == null) {
            setClickable(false);
            return;
        }
        cVar.a(this);
        setFocusable(true);
        setClickable(true);
    }

    public final void b(String str, String str2) {
        this.f.setOnTouchListener(new k(this));
        this.f.setOnClickListener(new n(this, str2, str));
    }

    /* access modifiers changed from: protected */
    public final c d() {
        return this.c;
    }

    public final boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                setPressed(true);
                break;
            case 1:
                setPressed(false);
                break;
        }
        return super.onTrackballEvent(motionEvent);
    }

    public final void e() {
        setVisibility(0);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(300);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        startAnimation(alphaAnimation);
    }

    public final void f() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(300);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        startAnimation(alphaAnimation);
    }

    public final void g() {
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
        translateAnimation.setDuration(300);
        translateAnimation.startNow();
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(translateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(300);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(alphaAnimation);
        startAnimation(animationSet);
    }

    public final void h() {
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
        translateAnimation.setDuration(300);
        translateAnimation.startNow();
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(translateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(300);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(alphaAnimation);
        startAnimation(animationSet);
    }

    public final void i() {
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        translateAnimation.setDuration(300);
        translateAnimation.startNow();
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(translateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(300);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(alphaAnimation);
        startAnimation(animationSet);
    }

    public final void j() {
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
        translateAnimation.setDuration(300);
        translateAnimation.startNow();
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(translateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(300);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(alphaAnimation);
        startAnimation(animationSet);
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        if (isPressed() || isFocused()) {
            canvas.clipRect(3, 3, getWidth() - 3, getHeight() - 3);
        }
        super.onDraw(canvas);
        if (this.e.longValue() != -1) {
            this.e = Long.valueOf(SystemClock.uptimeMillis());
        }
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        setPressed(false);
        return super.onKeyDown(i2, keyEvent);
    }

    public final boolean onKeyUp(int i2, KeyEvent keyEvent) {
        setPressed(true);
        return super.onKeyUp(i2, keyEvent);
    }

    public final void setPressed(boolean z) {
        if (isPressed() != z) {
            if (c.f110a != null) {
                getContext().startActivity(c.f110a);
            }
            super.setPressed(z);
            invalidate();
        }
    }
}
