package com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class GuitarPitchPipe extends Activity implements View.OnClickListener {
    private int instrument = 2;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.guitar_pitch_pipe);
        findViewById(R.id.e2).setOnClickListener(this);
        findViewById(R.id.a2).setOnClickListener(this);
        findViewById(R.id.d3).setOnClickListener(this);
        findViewById(R.id.g3).setOnClickListener(this);
        findViewById(R.id.b3).setOnClickListener(this);
        findViewById(R.id.e4).setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.e2 /*2131230758*/:
                Music.play(this, Pitch.getSongId(this.instrument, 1001));
                return;
            case R.id.a2 /*2131230759*/:
                Music.play(this, Pitch.getSongId(this.instrument, 1002));
                return;
            case R.id.d3 /*2131230760*/:
                Music.play(this, Pitch.getSongId(this.instrument, 1003));
                return;
            case R.id.g3 /*2131230761*/:
                Music.play(this, Pitch.getSongId(this.instrument, 1004));
                return;
            case R.id.b3 /*2131230762*/:
                Music.play(this, Pitch.getSongId(this.instrument, 1005));
                return;
            case R.id.e4 /*2131230763*/:
                Music.play(this, Pitch.getSongId(this.instrument, 1006));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Music.stop(this);
    }
}
