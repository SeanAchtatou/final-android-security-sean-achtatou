package com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class FlashcardTypeActivity extends Activity implements View.OnClickListener {
    ArrayAdapter<String> instrumentAdapter;
    Spinner instrumentSpinner;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.flashcards_chooser);
        findViewById(R.id.notes_first).setOnClickListener(this);
        findViewById(R.id.tones_first).setOnClickListener(this);
        this.instrumentSpinner = (Spinner) findViewById(R.id.instruments_spinner);
        this.instrumentAdapter = new ArrayAdapter<>(this, 17367048, new String[]{"Piano", "Guitar", "Pure Tone"});
        this.instrumentAdapter.setDropDownViewResource(17367049);
        this.instrumentSpinner.setAdapter((SpinnerAdapter) this.instrumentAdapter);
        ((AdView) findViewById(R.id.adView_ft)).loadAd(new AdRequest());
    }

    public void onClick(View v) {
        Intent fr = new Intent(this, FlashcardsActivity.class);
        switch (v.getId()) {
            case R.id.tones_first /*2131230755*/:
                fr.putExtra("whatGoesFirst", "tones");
                break;
            case R.id.notes_first /*2131230756*/:
                fr.putExtra("whatGoesFirst", "notes");
                break;
        }
        fr.putExtra("Instrument", this.instrumentSpinner.getSelectedItem().toString());
        startActivity(fr);
    }
}
