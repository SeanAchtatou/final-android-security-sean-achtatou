package com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE;

public class Score {
    private String name;
    private int score;

    public Score() {
        this.score = 0;
        this.name = "";
    }

    public Score(String inputName, int inputScore) {
        this.name = inputName;
        this.score = inputScore;
    }

    public String getName() {
        return this.name;
    }

    public int getScore() {
        return this.score;
    }

    public void setName(String inputName) {
        this.name = inputName;
    }

    public void setScore(int inputScore) {
        this.score = inputScore;
    }
}
