package com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;

public class Results extends Activity implements View.OnClickListener {
    public static final String HIGH_SCORES = "HighScoresFile";
    private Button closeButton;
    private int lowScore;
    private boolean newHighScore;
    private int numAnswered;
    private int numCorrect;
    private String ranking;
    private Button saveButton;
    private int score;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.results);
        this.lowScore = getSharedPreferences("HighScoresFile", 0).getInt("score10", 0);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.numAnswered = extras.getInt("numAnswered");
            this.numCorrect = extras.getInt("numCorrect");
        }
        double ratio = 100.0d * (((double) this.numCorrect) / ((double) this.numAnswered));
        this.score = (int) ((1000.0d * (((double) this.numCorrect) / ((double) this.numAnswered))) + 0.5d);
        if (ratio <= 25.0d) {
            this.ranking = "Poor Musician";
        } else if (ratio <= 50.0d) {
            this.ranking = "Decent Musician";
        } else if (ratio <= 75.0d) {
            this.ranking = "Good Musician";
        } else if (ratio < 100.0d) {
            this.ranking = "Great Musician";
        }
        if (this.numCorrect == 0) {
            this.ranking = "Future Percussionist";
        }
        if (this.numCorrect == this.numAnswered) {
            this.ranking = "Virtuoso";
        }
        ((TextView) findViewById(R.id.result)).setText(String.valueOf(this.numCorrect) + "/" + this.numAnswered);
        TextView tv = (TextView) findViewById(R.id.tv);
        if (this.numCorrect == 1) {
            tv.setText(String.valueOf(this.numCorrect) + " question was correct out of " + this.numAnswered + ".\n\n" + "Your score: " + this.score + ".\n" + "Your ranking: " + this.ranking + ".");
        } else {
            tv.setText(String.valueOf(this.numCorrect) + " questions were correct out of " + this.numAnswered + ".\n\n" + "Your score: " + this.score + ".\n" + "Your ranking: " + this.ranking + ".");
        }
        if (this.score > this.lowScore) {
            this.newHighScore = true;
        } else {
            this.newHighScore = false;
            ((TextView) findViewById(R.id.results_save_text)).setVisibility(8);
            ((TableLayout) findViewById(R.id.results_save_menu)).setVisibility(8);
        }
        this.saveButton = (Button) findViewById(R.id.results_save_button);
        this.saveButton.setOnClickListener(this);
        this.closeButton = (Button) findViewById(R.id.results_close_button);
        this.closeButton.setOnClickListener(this);
    }

    public boolean onTouchEvent(MotionEvent e) {
        if (e.getAction() != 0) {
            return super.onTouchEvent(e);
        }
        if (!this.newHighScore) {
            finish();
        }
        return true;
    }

    public void onClick(View v) {
        if (this.newHighScore) {
            switch (v.getId()) {
                case R.id.results_save_button:
                    Intent hs = new Intent(this, HighScores.class);
                    hs.putExtra("Score", this.score);
                    startActivity(hs);
                    finish();
                    return;
                case R.id.results_close_button:
                    finish();
                    return;
                default:
                    return;
            }
        }
    }
}
