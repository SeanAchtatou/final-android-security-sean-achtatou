package com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class ChallengeChooser extends Activity implements View.OnClickListener {
    ArrayAdapter<String> instrumentAdapter;
    Spinner instrumentSpinner;
    ArrayAdapter<String> questionAdapter;
    Spinner questionSpinner;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.challenge_chooser);
        findViewById(R.id.tones_given).setOnClickListener(this);
        findViewById(R.id.notes_given).setOnClickListener(this);
        this.questionSpinner = (Spinner) findViewById(R.id.questions_spinner);
        this.questionAdapter = new ArrayAdapter<>(this, 17367048, new String[]{"5", "10", "20", "50", "100", "Unlimited"});
        this.questionAdapter.setDropDownViewResource(17367049);
        this.questionSpinner.setAdapter((SpinnerAdapter) this.questionAdapter);
        this.instrumentSpinner = (Spinner) findViewById(R.id.instruments_spinner);
        this.instrumentAdapter = new ArrayAdapter<>(this, 17367048, new String[]{"Piano", "Guitar", "Pure Tone"});
        this.instrumentAdapter.setDropDownViewResource(17367049);
        this.instrumentSpinner.setAdapter((SpinnerAdapter) this.instrumentAdapter);
        ((AdView) findViewById(R.id.adView_cc)).loadAd(new AdRequest());
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tones_given /*2131230722*/:
                Intent cT = new Intent(this, ChallengeTonesGivenActivity.class);
                cT.putExtra("numQuestions", this.questionSpinner.getSelectedItem().toString());
                cT.putExtra("Instrument", this.instrumentSpinner.getSelectedItem().toString());
                startActivity(cT);
                return;
            case R.id.notes_given /*2131230723*/:
                Intent cN = new Intent(this, ChallengeNotesGivenActivity.class);
                cN.putExtra("numQuestions", this.questionSpinner.getSelectedItem().toString());
                cN.putExtra("Instrument", this.instrumentSpinner.getSelectedItem().toString());
                startActivity(cN);
                return;
            default:
                return;
        }
    }
}
