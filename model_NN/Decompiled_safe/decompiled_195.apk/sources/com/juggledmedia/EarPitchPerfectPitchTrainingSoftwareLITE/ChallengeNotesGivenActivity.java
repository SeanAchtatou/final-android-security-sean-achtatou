package com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class ChallengeNotesGivenActivity extends Activity implements View.OnClickListener {
    private int answerLocation;
    private int[] buttonValues = new int[4];
    private Button[] buttons = new Button[4];
    private int currentNote;
    private int instrument = 1;
    private int lastAnswerLocation;
    private int lastNote;
    private int numAnswered = 0;
    private int numCorrect = 0;
    private int numQuestionsInQuiz;
    private Button[] playButtons = new Button[4];
    private TextView question;
    private TextView questionsCorrect;
    private TextView resultOfQuestion;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.challenge_notes_given);
        this.buttons[0] = (Button) findViewById(R.id.choice1);
        this.buttons[0].setOnClickListener(this);
        this.buttons[1] = (Button) findViewById(R.id.choice2);
        this.buttons[1].setOnClickListener(this);
        this.buttons[2] = (Button) findViewById(R.id.choice3);
        this.buttons[2].setOnClickListener(this);
        this.buttons[3] = (Button) findViewById(R.id.choice4);
        this.buttons[3].setOnClickListener(this);
        this.playButtons[0] = (Button) findViewById(R.id.play_choice1);
        this.playButtons[0].setOnClickListener(this);
        this.playButtons[1] = (Button) findViewById(R.id.play_choice2);
        this.playButtons[1].setOnClickListener(this);
        this.playButtons[2] = (Button) findViewById(R.id.play_choice3);
        this.playButtons[2].setOnClickListener(this);
        this.playButtons[3] = (Button) findViewById(R.id.play_choice4);
        this.playButtons[3].setOnClickListener(this);
        this.resultOfQuestion = (TextView) findViewById(R.id.result);
        this.resultOfQuestion.setVisibility(4);
        this.questionsCorrect = (TextView) findViewById(R.id.question_counter);
        this.question = (TextView) findViewById(R.id.question);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String questions = extras.getString("numQuestions");
            Log.d("Notes Given", questions);
            if (questions.substring(0, 1).equals("U")) {
                this.numQuestionsInQuiz = 1000000;
            } else {
                this.numQuestionsInQuiz = Integer.parseInt(questions);
            }
            String instr = extras.getString("Instrument");
            if (instr.equals("Pure Tone")) {
                this.instrument = 0;
            } else if (instr.equals("Piano")) {
                this.instrument = 1;
            } else if (instr.equals("Guitar")) {
                this.instrument = 2;
            }
        }
        pickANewNote();
        changeToneLocations();
        updateQuestion();
        updateCorrectAnsweredLabel();
        ((AdView) findViewById(R.id.adView_cng)).loadAd(new AdRequest());
    }

    private void changeToneLocations() {
        int randomNote;
        this.answerLocation = (int) ((Math.random() * 4.0d) + 1.0d);
        this.buttonValues[this.answerLocation - 1] = this.currentNote;
        int[] notes = new int[12];
        notes[1] = 1;
        notes[2] = 2;
        notes[3] = 3;
        notes[4] = 4;
        notes[5] = 5;
        notes[6] = 6;
        notes[7] = 7;
        notes[8] = 8;
        notes[9] = 9;
        notes[10] = 10;
        notes[11] = 11;
        notes[this.currentNote] = -1;
        for (int i = 0; i <= 3; i++) {
            if (i != this.answerLocation - 1) {
                double random = Math.random();
                while (true) {
                    randomNote = (int) (random * 12.0d);
                    if (notes[randomNote] != -1) {
                        break;
                    }
                    random = Math.random();
                }
                this.buttonValues[i] = randomNote;
                notes[randomNote] = -1;
            }
        }
    }

    private void showResults() {
        Intent resultDisplay = new Intent(this, Results.class);
        resultDisplay.putExtra("numCorrect", this.numCorrect);
        resultDisplay.putExtra("numAnswered", this.numAnswered);
        startActivity(resultDisplay);
        reset();
    }

    public void reset() {
        this.numCorrect = 0;
        this.numAnswered = 0;
        this.resultOfQuestion.setVisibility(4);
        pickANewNote();
        changeToneLocations();
        updateCorrectAnsweredLabel();
    }

    private void pickANewNote() {
        double random = Math.random();
        while (true) {
            int newNote = (int) (random * 12.0d);
            if (this.currentNote != newNote) {
                this.currentNote = newNote;
                return;
            }
            random = Math.random();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.choice1 /*2131230728*/:
                if (this.answerLocation - 1 == 0) {
                    this.numCorrect++;
                    correct();
                } else {
                    incorrect();
                }
                selectionMade();
                return;
            case R.id.play_choice1 /*2131230729*/:
                Music.play(this, Pitch.getSongId(this.instrument, this.buttonValues[0]));
                return;
            case R.id.choice2 /*2131230730*/:
                if (this.answerLocation - 1 == 1) {
                    this.numCorrect++;
                    correct();
                } else {
                    incorrect();
                }
                selectionMade();
                return;
            case R.id.play_choice2 /*2131230731*/:
                Music.play(this, Pitch.getSongId(this.instrument, this.buttonValues[1]));
                return;
            case R.id.choice3 /*2131230732*/:
                if (this.answerLocation - 1 == 2) {
                    this.numCorrect++;
                    correct();
                } else {
                    incorrect();
                }
                selectionMade();
                return;
            case R.id.play_choice3 /*2131230733*/:
                Music.play(this, Pitch.getSongId(this.instrument, this.buttonValues[2]));
                return;
            case R.id.choice4 /*2131230734*/:
                if (this.answerLocation - 1 == 3) {
                    this.numCorrect++;
                    correct();
                } else {
                    incorrect();
                }
                selectionMade();
                return;
            case R.id.play_choice4 /*2131230735*/:
                Music.play(this, Pitch.getSongId(this.instrument, this.buttonValues[3]));
                return;
            default:
                return;
        }
    }

    private void correct() {
        this.resultOfQuestion.setVisibility(0);
        this.resultOfQuestion.setTextColor(-16711936);
        this.resultOfQuestion.setText("Correct! " + Pitch.getNoteName(this.currentNote) + " was Choice " + this.answerLocation + ".");
        this.lastNote = this.currentNote;
        this.lastAnswerLocation = this.answerLocation;
    }

    private void incorrect() {
        this.resultOfQuestion.setVisibility(0);
        this.resultOfQuestion.setTextColor(-65536);
        this.resultOfQuestion.setText("Incorrect, " + Pitch.getNoteName(this.currentNote) + " was Choice " + this.answerLocation + ".");
        this.lastNote = this.currentNote;
        this.lastAnswerLocation = this.answerLocation;
    }

    private void selectionMade() {
        Music.stop(this);
        this.numAnswered++;
        pickANewNote();
        changeToneLocations();
        updateCorrectAnsweredLabel();
        updateQuestion();
        if (this.numAnswered >= this.numQuestionsInQuiz) {
            showResults();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Music.stop(this);
    }

    private void updateCorrectAnsweredLabel() {
        this.questionsCorrect.setText(String.valueOf(this.numCorrect) + "/" + this.numAnswered);
    }

    private void updateQuestion() {
        this.question.setText("Which tone is " + Pitch.getNoteName(this.currentNote) + "?");
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("Last", this.lastNote);
        savedInstanceState.putInt("Current", this.currentNote);
        savedInstanceState.putInt("AnswerLoc", this.answerLocation);
        savedInstanceState.putInt("LastAnswerLoc", this.lastAnswerLocation);
        savedInstanceState.putInt("numQs", this.numQuestionsInQuiz);
        savedInstanceState.putInt("numCorrect", this.numCorrect);
        savedInstanceState.putInt("numAnswered", this.numAnswered);
        savedInstanceState.putIntArray("buttonValues", this.buttonValues);
        savedInstanceState.putString("question", (String) this.question.getText());
        savedInstanceState.putString("result", (String) this.resultOfQuestion.getText());
        super.onSaveInstanceState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        this.currentNote = savedInstanceState.getInt("Last");
        this.answerLocation = savedInstanceState.getInt("LastAnswerLoc");
        this.numQuestionsInQuiz = savedInstanceState.getInt("numQs");
        this.numCorrect = savedInstanceState.getInt("numCorrect");
        this.numAnswered = savedInstanceState.getInt("numAnswered");
        this.buttonValues = savedInstanceState.getIntArray("buttonValues");
        this.question.setText(savedInstanceState.getString("question"));
        String result = savedInstanceState.getString("result");
        if (this.numAnswered > 0) {
            if (result.substring(0, 1).equals("I")) {
                incorrect();
            } else if (result.substring(0, 1).equals("C")) {
                correct();
            }
        }
        updateCorrectAnsweredLabel();
        this.currentNote = savedInstanceState.getInt("Current");
        this.answerLocation = savedInstanceState.getInt("AnswerLoc");
        super.onRestoreInstanceState(savedInstanceState);
    }
}
