package com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class ChromaticPitchPipe extends Activity implements View.OnClickListener {
    private int instrument = 1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.chromatic_pitch_pipe);
        findViewById(R.id.c).setOnClickListener(this);
        findViewById(R.id.csharp).setOnClickListener(this);
        findViewById(R.id.d).setOnClickListener(this);
        findViewById(R.id.dsharp).setOnClickListener(this);
        findViewById(R.id.e).setOnClickListener(this);
        findViewById(R.id.f).setOnClickListener(this);
        findViewById(R.id.fsharp).setOnClickListener(this);
        findViewById(R.id.g).setOnClickListener(this);
        findViewById(R.id.gsharp).setOnClickListener(this);
        findViewById(R.id.a).setOnClickListener(this);
        findViewById(R.id.asharp).setOnClickListener(this);
        findViewById(R.id.b).setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.c /*2131230738*/:
                Music.play(this, Pitch.getSongId(this.instrument, 0));
                return;
            case R.id.csharp /*2131230739*/:
                Music.play(this, Pitch.getSongId(this.instrument, 1));
                return;
            case R.id.d /*2131230740*/:
                Music.play(this, Pitch.getSongId(this.instrument, 2));
                return;
            case R.id.dsharp /*2131230741*/:
                Music.play(this, Pitch.getSongId(this.instrument, 3));
                return;
            case R.id.e /*2131230742*/:
                Music.play(this, Pitch.getSongId(this.instrument, 4));
                return;
            case R.id.f /*2131230743*/:
                Music.play(this, Pitch.getSongId(this.instrument, 5));
                return;
            case R.id.fsharp /*2131230744*/:
                Music.play(this, Pitch.getSongId(this.instrument, 6));
                return;
            case R.id.g /*2131230745*/:
                Music.play(this, Pitch.getSongId(this.instrument, 7));
                return;
            case R.id.gsharp /*2131230746*/:
                Music.play(this, Pitch.getSongId(this.instrument, 8));
                return;
            case R.id.a /*2131230747*/:
                Music.play(this, Pitch.getSongId(this.instrument, 9));
                return;
            case R.id.asharp /*2131230748*/:
                Music.play(this, Pitch.getSongId(this.instrument, 10));
                return;
            case R.id.b /*2131230749*/:
                Music.play(this, Pitch.getSongId(this.instrument, 11));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Music.stop(this);
    }
}
