package com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class ChallengeTonesGivenActivity extends Activity implements View.OnClickListener {
    private int answerLocation;
    private Button[] buttons = new Button[4];
    private int currentNote;
    private int instrument = 1;
    private int lastNote;
    private int numAnswered = 0;
    private int numCorrect = 0;
    private int numQuestionsInQuiz;
    private TextView questionsCorrect;
    private TextView resultOfQuestion;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.challenge_tones_given);
        this.buttons[0] = (Button) findViewById(R.id.choice1);
        this.buttons[0].setOnClickListener(this);
        this.buttons[1] = (Button) findViewById(R.id.choice2);
        this.buttons[1].setOnClickListener(this);
        this.buttons[2] = (Button) findViewById(R.id.choice3);
        this.buttons[2].setOnClickListener(this);
        this.buttons[3] = (Button) findViewById(R.id.choice4);
        this.buttons[3].setOnClickListener(this);
        this.resultOfQuestion = (TextView) findViewById(R.id.result);
        this.resultOfQuestion.setVisibility(4);
        this.questionsCorrect = (TextView) findViewById(R.id.question_counter);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String questions = extras.getString("numQuestions");
            if (questions.substring(0, 1).equals("U")) {
                this.numQuestionsInQuiz = 1000000;
            } else {
                this.numQuestionsInQuiz = Integer.parseInt(questions);
            }
            String instr = extras.getString("Instrument");
            if (instr.equals("Pure Tone")) {
                this.instrument = 0;
            } else if (instr.equals("Piano")) {
                this.instrument = 1;
            } else if (instr.equals("Guitar")) {
                this.instrument = 2;
            }
        }
        pickANewNote();
        playCurrentNote();
        changeButtonLabels();
        updateCorrectAnsweredLabel();
        ((AdView) findViewById(R.id.adView_ctg)).loadAd(new AdRequest());
    }

    private void changeButtonLabels() {
        int randomNote;
        this.answerLocation = (int) ((Math.random() * 4.0d) + 1.0d);
        this.buttons[this.answerLocation - 1].setText(Pitch.getNoteName(this.currentNote));
        int[] notes = new int[12];
        notes[1] = 1;
        notes[2] = 2;
        notes[3] = 3;
        notes[4] = 4;
        notes[5] = 5;
        notes[6] = 6;
        notes[7] = 7;
        notes[8] = 8;
        notes[9] = 9;
        notes[10] = 10;
        notes[11] = 11;
        notes[this.currentNote] = -1;
        for (int i = 0; i <= 3; i++) {
            if (i != this.answerLocation - 1) {
                double random = Math.random();
                while (true) {
                    randomNote = (int) (random * 12.0d);
                    if (notes[randomNote] != -1) {
                        break;
                    }
                    random = Math.random();
                }
                this.buttons[i].setText(Pitch.getNoteName(randomNote));
                notes[randomNote] = -1;
            }
        }
    }

    private void showResults() {
        Intent resultDisplay = new Intent(this, Results.class);
        resultDisplay.putExtra("numCorrect", this.numCorrect);
        resultDisplay.putExtra("numAnswered", this.numAnswered);
        startActivity(resultDisplay);
        reset();
    }

    public void reset() {
        this.numCorrect = 0;
        this.numAnswered = 0;
        this.resultOfQuestion.setVisibility(4);
        pickANewNote();
        playCurrentNote();
        changeButtonLabels();
        updateCorrectAnsweredLabel();
    }

    private void pickANewNote() {
        double random = Math.random();
        while (true) {
            int newNote = (int) (random * 12.0d);
            if (this.currentNote != newNote) {
                this.currentNote = newNote;
                return;
            }
            random = Math.random();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.choice1 /*2131230728*/:
                if (this.answerLocation - 1 != 0) {
                    incorrect();
                    break;
                } else {
                    this.numCorrect++;
                    correct();
                    break;
                }
            case R.id.choice2 /*2131230730*/:
                if (this.answerLocation - 1 != 1) {
                    incorrect();
                    break;
                } else {
                    this.numCorrect++;
                    correct();
                    break;
                }
            case R.id.choice3 /*2131230732*/:
                if (this.answerLocation - 1 != 2) {
                    incorrect();
                    break;
                } else {
                    this.numCorrect++;
                    correct();
                    break;
                }
            case R.id.choice4 /*2131230734*/:
                if (this.answerLocation - 1 != 3) {
                    incorrect();
                    break;
                } else {
                    this.numCorrect++;
                    correct();
                    break;
                }
        }
        this.numAnswered++;
        pickANewNote();
        playCurrentNote();
        changeButtonLabels();
        updateCorrectAnsweredLabel();
        if (this.numAnswered >= this.numQuestionsInQuiz) {
            showResults();
        }
    }

    private void playCurrentNote() {
        Music.play(this, Pitch.getSongId(this.instrument, this.currentNote));
    }

    private void correct() {
        this.resultOfQuestion.setVisibility(0);
        this.resultOfQuestion.setTextColor(-16711936);
        this.resultOfQuestion.setText("Correct! The tone played was " + Pitch.getNoteName(this.currentNote) + ".");
        this.lastNote = this.currentNote;
    }

    private void incorrect() {
        this.resultOfQuestion.setVisibility(0);
        this.resultOfQuestion.setTextColor(-65536);
        this.resultOfQuestion.setText("Incorrect, the tone played was " + Pitch.getNoteName(this.currentNote) + ".");
        this.lastNote = this.currentNote;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        playCurrentNote();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Music.stop(this);
    }

    private void updateCorrectAnsweredLabel() {
        this.questionsCorrect.setText(String.valueOf(this.numCorrect) + "/" + this.numAnswered);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("Last", this.lastNote);
        savedInstanceState.putInt("Current", this.currentNote);
        savedInstanceState.putInt("AnswerLoc", this.answerLocation);
        savedInstanceState.putInt("numQs", this.numQuestionsInQuiz);
        savedInstanceState.putInt("numCorrect", this.numCorrect);
        savedInstanceState.putInt("numAnswered", this.numAnswered);
        savedInstanceState.putString("result", (String) this.resultOfQuestion.getText());
        savedInstanceState.putString("Button1 label", (String) this.buttons[0].getText());
        savedInstanceState.putString("Button2 label", (String) this.buttons[1].getText());
        savedInstanceState.putString("Button3 label", (String) this.buttons[2].getText());
        savedInstanceState.putString("Button4 label", (String) this.buttons[3].getText());
        super.onSaveInstanceState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        this.currentNote = savedInstanceState.getInt("Last");
        this.answerLocation = savedInstanceState.getInt("AnswerLoc");
        this.numQuestionsInQuiz = savedInstanceState.getInt("numQs");
        this.numCorrect = savedInstanceState.getInt("numCorrect");
        this.numAnswered = savedInstanceState.getInt("numAnswered");
        String result = savedInstanceState.getString("result");
        if (this.numAnswered > 0) {
            if (result.substring(0, 1).equals("I")) {
                incorrect();
            } else if (result.substring(0, 1).equals("C")) {
                correct();
            }
        }
        updateCorrectAnsweredLabel();
        this.currentNote = savedInstanceState.getInt("Current");
        playCurrentNote();
        this.buttons[0].setText(savedInstanceState.getString("Button1 label"));
        this.buttons[1].setText(savedInstanceState.getString("Button2 label"));
        this.buttons[2].setText(savedInstanceState.getString("Button3 label"));
        this.buttons[3].setText(savedInstanceState.getString("Button4 label"));
        super.onRestoreInstanceState(savedInstanceState);
    }
}
