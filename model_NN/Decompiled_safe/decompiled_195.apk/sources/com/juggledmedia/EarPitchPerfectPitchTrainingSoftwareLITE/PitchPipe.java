package com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class PitchPipe extends TabActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.pitch_pipe);
        TabHost tabHost = getTabHost();
        tabHost.addTab(tabHost.newTabSpec("chromatic").setIndicator("", getResources().getDrawable(R.drawable.piano_icon)).setContent(new Intent().setClass(this, ChromaticPitchPipe.class)));
        tabHost.addTab(tabHost.newTabSpec("guitar").setIndicator("", getResources().getDrawable(R.drawable.guitar_icon)).setContent(new Intent().setClass(this, GuitarPitchPipe.class)));
        tabHost.setCurrentTab(0);
        ((AdView) findViewById(R.id.adView_pp)).loadAd(new AdRequest());
    }
}
