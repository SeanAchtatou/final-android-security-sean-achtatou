package com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE;

public class Pitch {
    public static String getNoteName(int x) {
        switch (x) {
            case 0:
                return "C";
            case 1:
                return "C#/Db";
            case 2:
                return "D";
            case 3:
                return "D#/Eb";
            case 4:
                return "E";
            case 5:
                return "F";
            case 6:
                return "F#/Gb";
            case 7:
                return "G";
            case 8:
                return "G#/Ab";
            case 9:
                return "A";
            case 10:
                return "A#/Bb";
            case 11:
                return "B";
            default:
                return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0045 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0049 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004d A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0051 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0055 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0059 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005d A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0061 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0065 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0069 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x006d A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0071 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0075 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0079 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x007d A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0081 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0085 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x008a A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x008f A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0094 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0099 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x009e A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00a3 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00a8 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00ad A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00b2 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00b7 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0012 A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int getSongId(int r1, int r2) {
        /*
            r0 = 2130968604(0x7f04001c, float:1.7545866E38)
            switch(r1) {
                case 0: goto L_0x0008;
                case 1: goto L_0x000b;
                case 2: goto L_0x000e;
                default: goto L_0x0006;
            }
        L_0x0006:
            r0 = -1
        L_0x0007:
            return r0
        L_0x0008:
            switch(r2) {
                case 0: goto L_0x0016;
                case 1: goto L_0x001a;
                case 2: goto L_0x001e;
                case 3: goto L_0x0022;
                case 4: goto L_0x0026;
                case 5: goto L_0x002a;
                case 6: goto L_0x002e;
                case 7: goto L_0x0032;
                case 8: goto L_0x0036;
                case 9: goto L_0x003a;
                case 10: goto L_0x003d;
                case 11: goto L_0x0041;
                default: goto L_0x000b;
            }
        L_0x000b:
            switch(r2) {
                case 0: goto L_0x0045;
                case 1: goto L_0x0049;
                case 2: goto L_0x004d;
                case 3: goto L_0x0051;
                case 4: goto L_0x0055;
                case 5: goto L_0x0059;
                case 6: goto L_0x005d;
                case 7: goto L_0x0061;
                case 8: goto L_0x0065;
                case 9: goto L_0x0069;
                case 10: goto L_0x006d;
                case 11: goto L_0x0071;
                default: goto L_0x000e;
            }
        L_0x000e:
            switch(r2) {
                case 0: goto L_0x0012;
                case 1: goto L_0x0075;
                case 2: goto L_0x0079;
                case 3: goto L_0x007d;
                case 4: goto L_0x0007;
                case 5: goto L_0x0081;
                case 6: goto L_0x0085;
                case 7: goto L_0x008a;
                case 8: goto L_0x008f;
                case 9: goto L_0x0094;
                case 10: goto L_0x0099;
                case 11: goto L_0x009e;
                case 1001: goto L_0x00a3;
                case 1002: goto L_0x00a8;
                case 1003: goto L_0x00ad;
                case 1004: goto L_0x00b2;
                case 1005: goto L_0x00b7;
                case 1006: goto L_0x0007;
                default: goto L_0x0011;
            }
        L_0x0011:
            goto L_0x0006
        L_0x0012:
            r0 = 2130968598(0x7f040016, float:1.7545854E38)
            goto L_0x0007
        L_0x0016:
            r0 = 2130968581(0x7f040005, float:1.754582E38)
            goto L_0x0007
        L_0x001a:
            r0 = 2130968582(0x7f040006, float:1.7545822E38)
            goto L_0x0007
        L_0x001e:
            r0 = 2130968583(0x7f040007, float:1.7545824E38)
            goto L_0x0007
        L_0x0022:
            r0 = 2130968585(0x7f040009, float:1.7545828E38)
            goto L_0x0007
        L_0x0026:
            r0 = 2130968586(0x7f04000a, float:1.754583E38)
            goto L_0x0007
        L_0x002a:
            r0 = 2130968588(0x7f04000c, float:1.7545834E38)
            goto L_0x0007
        L_0x002e:
            r0 = 2130968589(0x7f04000d, float:1.7545836E38)
            goto L_0x0007
        L_0x0032:
            r0 = 2130968590(0x7f04000e, float:1.7545838E38)
            goto L_0x0007
        L_0x0036:
            r0 = 2130968592(0x7f040010, float:1.7545842E38)
            goto L_0x0007
        L_0x003a:
            r0 = 2130968576(0x7f040000, float:1.754581E38)
            goto L_0x0007
        L_0x003d:
            r0 = 2130968578(0x7f040002, float:1.7545814E38)
            goto L_0x0007
        L_0x0041:
            r0 = 2130968579(0x7f040003, float:1.7545816E38)
            goto L_0x0007
        L_0x0045:
            r0 = 2130968613(0x7f040025, float:1.7545885E38)
            goto L_0x0007
        L_0x0049:
            r0 = 2130968614(0x7f040026, float:1.7545887E38)
            goto L_0x0007
        L_0x004d:
            r0 = 2130968615(0x7f040027, float:1.7545889E38)
            goto L_0x0007
        L_0x0051:
            r0 = 2130968616(0x7f040028, float:1.754589E38)
            goto L_0x0007
        L_0x0055:
            r0 = 2130968617(0x7f040029, float:1.7545893E38)
            goto L_0x0007
        L_0x0059:
            r0 = 2130968618(0x7f04002a, float:1.7545895E38)
            goto L_0x0007
        L_0x005d:
            r0 = 2130968619(0x7f04002b, float:1.7545897E38)
            goto L_0x0007
        L_0x0061:
            r0 = 2130968620(0x7f04002c, float:1.7545899E38)
            goto L_0x0007
        L_0x0065:
            r0 = 2130968621(0x7f04002d, float:1.75459E38)
            goto L_0x0007
        L_0x0069:
            r0 = 2130968610(0x7f040022, float:1.7545879E38)
            goto L_0x0007
        L_0x006d:
            r0 = 2130968611(0x7f040023, float:1.754588E38)
            goto L_0x0007
        L_0x0071:
            r0 = 2130968612(0x7f040024, float:1.7545883E38)
            goto L_0x0007
        L_0x0075:
            r0 = 2130968599(0x7f040017, float:1.7545856E38)
            goto L_0x0007
        L_0x0079:
            r0 = 2130968601(0x7f040019, float:1.754586E38)
            goto L_0x0007
        L_0x007d:
            r0 = 2130968602(0x7f04001a, float:1.7545862E38)
            goto L_0x0007
        L_0x0081:
            r0 = 2130968605(0x7f04001d, float:1.7545868E38)
            goto L_0x0007
        L_0x0085:
            r0 = 2130968606(0x7f04001e, float:1.754587E38)
            goto L_0x0007
        L_0x008a:
            r0 = 2130968608(0x7f040020, float:1.7545874E38)
            goto L_0x0007
        L_0x008f:
            r0 = 2130968609(0x7f040021, float:1.7545876E38)
            goto L_0x0007
        L_0x0094:
            r0 = 2130968594(0x7f040012, float:1.7545846E38)
            goto L_0x0007
        L_0x0099:
            r0 = 2130968595(0x7f040013, float:1.7545848E38)
            goto L_0x0007
        L_0x009e:
            r0 = 2130968597(0x7f040015, float:1.7545852E38)
            goto L_0x0007
        L_0x00a3:
            r0 = 2130968603(0x7f04001b, float:1.7545864E38)
            goto L_0x0007
        L_0x00a8:
            r0 = 2130968593(0x7f040011, float:1.7545844E38)
            goto L_0x0007
        L_0x00ad:
            r0 = 2130968600(0x7f040018, float:1.7545858E38)
            goto L_0x0007
        L_0x00b2:
            r0 = 2130968607(0x7f04001f, float:1.7545872E38)
            goto L_0x0007
        L_0x00b7:
            r0 = 2130968596(0x7f040014, float:1.754585E38)
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE.Pitch.getSongId(int, int):int");
    }
}
