package com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int background = 2131034112;
        public static final int correct = 2131034114;
        public static final int incorrect = 2131034115;
        public static final int pitch_view = 2131034113;
        public static final int white = 2131034116;
    }

    public static final class dimen {
        public static final int challenge_cng_land_button_height = 2131099650;
        public static final int flashcards_land_button_height = 2131099651;
        public static final int flashcards_land_button_width = 2131099652;
        public static final int pitch_pipe_button_width = 2131099648;
        public static final int pitch_pipe_button_width_land = 2131099649;
    }

    public static final class drawable {
        public static final int ear_pitch_icon_fuzzier_lite = 2130837504;
        public static final int ear_pitch_lite = 2130837505;
        public static final int guitar_icon = 2130837506;
        public static final int icon = 2130837507;
        public static final int icon_plain = 2130837508;
        public static final int piano_icon = 2130837509;
    }

    public static final class id {
        public static final int a = 2131230747;
        public static final int a2 = 2131230759;
        public static final int about_button = 2131230783;
        public static final int adView = 2131230778;
        public static final int adView_cc = 2131230724;
        public static final int adView_cng = 2131230736;
        public static final int adView_ctg = 2131230737;
        public static final int adView_f = 2131230754;
        public static final int adView_ft = 2131230757;
        public static final int adView_hs = 2131230776;
        public static final int adView_pp = 2131230786;
        public static final int asharp = 2131230748;
        public static final int b = 2131230749;
        public static final int b3 = 2131230762;
        public static final int buy_button = 2131230784;
        public static final int c = 2131230738;
        public static final int challenge_button = 2131230779;
        public static final int choice1 = 2131230728;
        public static final int choice2 = 2131230730;
        public static final int choice3 = 2131230732;
        public static final int choice4 = 2131230734;
        public static final int csharp = 2131230739;
        public static final int d = 2131230740;
        public static final int d3 = 2131230760;
        public static final int dsharp = 2131230741;
        public static final int e = 2131230742;
        public static final int e2 = 2131230758;
        public static final int e4 = 2131230763;
        public static final int exit_button = 2131230785;
        public static final int f = 2131230743;
        public static final int flashcard_button = 2131230780;
        public static final int flashcard_image = 2131230750;
        public static final int flip_button = 2131230751;
        public static final int fsharp = 2131230744;
        public static final int g = 2131230745;
        public static final int g3 = 2131230761;
        public static final int gsharp = 2131230746;
        public static final int high_score_layout = 2131230764;
        public static final int high_scores_button = 2131230782;
        public static final int high_scores_row0 = 2131230766;
        public static final int high_scores_row1 = 2131230767;
        public static final int high_scores_row2 = 2131230768;
        public static final int high_scores_row3 = 2131230769;
        public static final int high_scores_row4 = 2131230770;
        public static final int high_scores_row5 = 2131230771;
        public static final int high_scores_row6 = 2131230772;
        public static final int high_scores_row7 = 2131230773;
        public static final int high_scores_row8 = 2131230774;
        public static final int high_scores_row9 = 2131230775;
        public static final int high_scores_table = 2131230765;
        public static final int instruments_spinner = 2131230721;
        public static final int main_layout = 2131230777;
        public static final int next_button = 2131230753;
        public static final int notes_first = 2131230756;
        public static final int notes_given = 2131230723;
        public static final int pitch_pipe_button = 2131230781;
        public static final int play_choice1 = 2131230729;
        public static final int play_choice2 = 2131230731;
        public static final int play_choice3 = 2131230733;
        public static final int play_choice4 = 2131230735;
        public static final int previous_button = 2131230752;
        public static final int question = 2131230727;
        public static final int question_counter = 2131230725;
        public static final int questions_spinner = 2131230720;
        public static final int result = 2131230726;
        public static final int results_close_button = 2131230791;
        public static final int results_save_button = 2131230790;
        public static final int results_save_menu = 2131230789;
        public static final int results_save_text = 2131230788;
        public static final int tones_first = 2131230755;
        public static final int tones_given = 2131230722;
        public static final int tv = 2131230787;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int challenge_chooser = 2130903041;
        public static final int challenge_notes_given = 2130903042;
        public static final int challenge_tones_given = 2130903043;
        public static final int chromatic_pitch_pipe = 2130903044;
        public static final int flashcards = 2130903045;
        public static final int flashcards_chooser = 2130903046;
        public static final int guitar_pitch_pipe = 2130903047;
        public static final int high_scores = 2130903048;
        public static final int main = 2130903049;
        public static final int pitch_pipe = 2130903050;
        public static final int results = 2130903051;
    }

    public static final class raw {
        public static final int a = 2130968576;
        public static final int a2 = 2130968577;
        public static final int asharp = 2130968578;
        public static final int b = 2130968579;
        public static final int b3 = 2130968580;
        public static final int c = 2130968581;
        public static final int csharp = 2130968582;
        public static final int d = 2130968583;
        public static final int d3 = 2130968584;
        public static final int dsharp = 2130968585;
        public static final int e = 2130968586;
        public static final int e2 = 2130968587;
        public static final int f = 2130968588;
        public static final int fsharp = 2130968589;
        public static final int g = 2130968590;
        public static final int g3 = 2130968591;
        public static final int gsharp = 2130968592;
        public static final int guitar_a2 = 2130968593;
        public static final int guitar_a4 = 2130968594;
        public static final int guitar_asharp4 = 2130968595;
        public static final int guitar_b3 = 2130968596;
        public static final int guitar_b4 = 2130968597;
        public static final int guitar_c4 = 2130968598;
        public static final int guitar_csharp4 = 2130968599;
        public static final int guitar_d3 = 2130968600;
        public static final int guitar_d4 = 2130968601;
        public static final int guitar_dsharp4 = 2130968602;
        public static final int guitar_e2 = 2130968603;
        public static final int guitar_e4 = 2130968604;
        public static final int guitar_f4 = 2130968605;
        public static final int guitar_fsharp4 = 2130968606;
        public static final int guitar_g3 = 2130968607;
        public static final int guitar_g4 = 2130968608;
        public static final int guitar_gsharp4 = 2130968609;
        public static final int piano_a4 = 2130968610;
        public static final int piano_asharp4 = 2130968611;
        public static final int piano_b4 = 2130968612;
        public static final int piano_c4 = 2130968613;
        public static final int piano_csharp4 = 2130968614;
        public static final int piano_d4 = 2130968615;
        public static final int piano_dsharp4 = 2130968616;
        public static final int piano_e4 = 2130968617;
        public static final int piano_f4 = 2130968618;
        public static final int piano_fsharp4 = 2130968619;
        public static final int piano_g4 = 2130968620;
        public static final int piano_gsharp4 = 2130968621;
    }

    public static final class string {
        public static final int about_button_label = 2131165189;
        public static final int about_text = 2131165192;
        public static final int about_title = 2131165190;
        public static final int app_display_name = 2131165184;
        public static final int app_name = 2131165185;
        public static final int buy_button_label = 2131165191;
        public static final int challenge_button_label = 2131165186;
        public static final int challenge_chooser_title = 2131165197;
        public static final int challenge_title = 2131165198;
        public static final int copyright_label = 2131165194;
        public static final int exit_button_label = 2131165193;
        public static final int flashcard_button_label = 2131165187;
        public static final int flashcard_chooser_title = 2131165196;
        public static final int flashcards_title = 2131165195;
        public static final int high_score_info = 2131165200;
        public static final int high_scores_button_label = 2131165201;
        public static final int pitch_pipe_button_label = 2131165188;
        public static final int pitch_pipe_title = 2131165199;
    }
}
