package com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;

public class About extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about);
    }

    public boolean onTouchEvent(MotionEvent e) {
        if (e.getAction() != 0) {
            return super.onTouchEvent(e);
        }
        finish();
        return true;
    }
}
