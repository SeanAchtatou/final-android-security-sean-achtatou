package com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class EarPitchPerfectPitchTrainer extends Activity implements View.OnClickListener {
    AdView adView;
    String url = "https://market.android.com/details?id=com.juggledmedia.EarPitchPerfectPitchTrainingSoftware";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        findViewById(R.id.challenge_button).setOnClickListener(this);
        findViewById(R.id.flashcard_button).setOnClickListener(this);
        findViewById(R.id.pitch_pipe_button).setOnClickListener(this);
        findViewById(R.id.high_scores_button).setOnClickListener(this);
        findViewById(R.id.about_button).setOnClickListener(this);
        findViewById(R.id.buy_button).setOnClickListener(this);
        findViewById(R.id.exit_button).setOnClickListener(this);
        ((AdView) findViewById(R.id.adView)).loadAd(new AdRequest());
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.challenge_button /*2131230779*/:
                startActivity(new Intent(this, ChallengeChooser.class));
                return;
            case R.id.flashcard_button /*2131230780*/:
                startActivity(new Intent(this, FlashcardTypeActivity.class));
                return;
            case R.id.pitch_pipe_button /*2131230781*/:
                startActivity(new Intent(this, PitchPipe.class));
                return;
            case R.id.high_scores_button /*2131230782*/:
                startActivity(new Intent(this, HighScores.class));
                return;
            case R.id.about_button /*2131230783*/:
                startActivity(new Intent(this, About.class));
                return;
            case R.id.buy_button /*2131230784*/:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.url)));
                return;
            case R.id.exit_button /*2131230785*/:
                finish();
                return;
            default:
                return;
        }
    }
}
