package com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class HighScores extends Activity {
    public static final String HIGH_SCORES = "HighScoresFile";
    /* access modifiers changed from: private */
    public int newScore;
    /* access modifiers changed from: private */
    public boolean newScoreHasBeenEntered = true;
    private TableRow[] row = new TableRow[10];
    private Score[] scores = new Score[10];

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences highScores = getSharedPreferences("HighScoresFile", 0);
        this.scores[0] = new Score(highScores.getString("score1name", "-----"), highScores.getInt("score1", 0));
        this.scores[1] = new Score(highScores.getString("score2name", "-----"), highScores.getInt("score2", 0));
        this.scores[2] = new Score(highScores.getString("score3name", "-----"), highScores.getInt("score3", 0));
        this.scores[3] = new Score(highScores.getString("score4name", "-----"), highScores.getInt("score4", 0));
        this.scores[4] = new Score(highScores.getString("score5name", "-----"), highScores.getInt("score5", 0));
        this.scores[5] = new Score(highScores.getString("score6name", "-----"), highScores.getInt("score6", 0));
        this.scores[6] = new Score(highScores.getString("score7name", "-----"), highScores.getInt("score7", 0));
        this.scores[7] = new Score(highScores.getString("score8name", "-----"), highScores.getInt("score8", 0));
        this.scores[8] = new Score(highScores.getString("score9name", "-----"), highScores.getInt("score9", 0));
        this.scores[9] = new Score(highScores.getString("score10name", "-----"), highScores.getInt("score10", 0));
        setContentView((int) R.layout.high_scores);
        updateScores();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.newScoreHasBeenEntered = false;
            this.newScore = extras.getInt("Score");
        }
        ((AdView) findViewById(R.id.adView_hs)).loadAd(new AdRequest());
    }

    public void onResume() {
        super.onResume();
        enterNewScore();
    }

    public void updateScores() {
        this.row[0] = (TableRow) findViewById(R.id.high_scores_row0);
        this.row[0].removeAllViews();
        this.row[1] = (TableRow) findViewById(R.id.high_scores_row1);
        this.row[1].removeAllViews();
        this.row[2] = (TableRow) findViewById(R.id.high_scores_row2);
        this.row[2].removeAllViews();
        this.row[3] = (TableRow) findViewById(R.id.high_scores_row3);
        this.row[3].removeAllViews();
        this.row[4] = (TableRow) findViewById(R.id.high_scores_row4);
        this.row[4].removeAllViews();
        this.row[5] = (TableRow) findViewById(R.id.high_scores_row5);
        this.row[5].removeAllViews();
        this.row[6] = (TableRow) findViewById(R.id.high_scores_row6);
        this.row[6].removeAllViews();
        this.row[7] = (TableRow) findViewById(R.id.high_scores_row7);
        this.row[7].removeAllViews();
        this.row[8] = (TableRow) findViewById(R.id.high_scores_row8);
        this.row[8].removeAllViews();
        this.row[9] = (TableRow) findViewById(R.id.high_scores_row9);
        this.row[9].removeAllViews();
        TableRow.LayoutParams para = new TableRow.LayoutParams(1);
        TextView[] numbering = new TextView[10];
        for (int i = 0; i < 10; i++) {
            numbering[i] = new TextView(this);
            numbering[i].setText(new StringBuilder().append(i + 1).toString());
            numbering[i].setTextColor(-16777216);
            numbering[i].setGravity(3);
            this.row[i].addView(numbering[i]);
        }
        TextView[] names = new TextView[10];
        for (int i2 = 0; i2 < 10; i2++) {
            names[i2] = new TextView(this);
            names[i2].setText(this.scores[i2].getName());
            names[i2].setTextColor(-16777216);
            names[i2].setGravity(1);
            names[i2].setLayoutParams(para);
            this.row[i2].addView(names[i2]);
        }
        TextView[] scoreStrings = new TextView[10];
        for (int i3 = 0; i3 < 10; i3++) {
            scoreStrings[i3] = new TextView(this);
            scoreStrings[i3].setText(new StringBuilder(String.valueOf(this.scores[i3].getScore())).toString());
            scoreStrings[i3].setTextColor(-16777216);
            scoreStrings[i3].setGravity(5);
            this.row[i3].addView(scoreStrings[i3]);
        }
    }

    public void enterNewScore() {
        if (!this.newScoreHasBeenEntered) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("New High Score");
            alert.setMessage("Enter Your Name");
            final EditText input = new EditText(this);
            alert.setView(input);
            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String value = input.getText().toString();
                    if (value.length() >= 30) {
                        value = value.substring(0, 30);
                    }
                    HighScores.this.addNewScoreToArray(HighScores.this.newScore, value);
                }
            });
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    HighScores.this.newScoreHasBeenEntered = true;
                }
            });
            alert.show();
        }
    }

    public void addNewScoreToArray(int score, String name) {
        int newIndex = 0;
        while (this.scores[newIndex].getScore() >= score) {
            newIndex++;
        }
        if (newIndex < this.scores.length) {
            for (int i = 9; i > newIndex; i--) {
                this.scores[i] = this.scores[i - 1];
            }
            this.scores[newIndex] = new Score(name, score);
        }
        updateScores();
        this.row[newIndex].setBackgroundColor(-16711681);
        saveScores();
        this.newScoreHasBeenEntered = true;
    }

    public void saveScores() {
        SharedPreferences.Editor editor = getSharedPreferences("HighScoresFile", 0).edit();
        editor.putString("score1name", this.scores[0].getName());
        editor.putInt("score1", this.scores[0].getScore());
        editor.putString("score2name", this.scores[1].getName());
        editor.putInt("score2", this.scores[1].getScore());
        editor.putString("score3name", this.scores[2].getName());
        editor.putInt("score3", this.scores[2].getScore());
        editor.putString("score4name", this.scores[3].getName());
        editor.putInt("score4", this.scores[3].getScore());
        editor.putString("score5name", this.scores[4].getName());
        editor.putInt("score5", this.scores[4].getScore());
        editor.putString("score6name", this.scores[5].getName());
        editor.putInt("score6", this.scores[5].getScore());
        editor.putString("score7name", this.scores[6].getName());
        editor.putInt("score7", this.scores[6].getScore());
        editor.putString("score8name", this.scores[7].getName());
        editor.putInt("score8", this.scores[7].getScore());
        editor.putString("score9name", this.scores[8].getName());
        editor.putInt("score9", this.scores[8].getScore());
        editor.putString("score10name", this.scores[9].getName());
        editor.putInt("score10", this.scores[9].getScore());
        editor.commit();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean("New Score Has Been Entered", this.newScoreHasBeenEntered);
        super.onSaveInstanceState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        this.newScoreHasBeenEntered = savedInstanceState.getBoolean("New Score Has Been Entered");
        super.onRestoreInstanceState(savedInstanceState);
    }
}
