package com.juggledmedia.EarPitchPerfectPitchTrainingSoftwareLITE;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class FlashcardsActivity extends Activity implements View.OnClickListener {
    private Button flipButton;
    private int index = 0;
    private int instrument = 1;
    private boolean notesFirst;
    private boolean toneIsPlaying;
    private int[] tones;

    public FlashcardsActivity() {
        int[] iArr = new int[12];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        iArr[4] = 4;
        iArr[5] = 5;
        iArr[6] = 6;
        iArr[7] = 7;
        iArr[8] = 8;
        iArr[9] = 9;
        iArr[10] = 10;
        iArr[11] = 11;
        this.tones = iArr;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.flashcards);
        findViewById(R.id.next_button).setOnClickListener(this);
        findViewById(R.id.previous_button).setOnClickListener(this);
        this.flipButton = (Button) findViewById(R.id.flip_button);
        this.flipButton.setOnClickListener(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String extra = extras.getString("whatGoesFirst");
            if (extra.equals("notes")) {
                this.notesFirst = true;
                this.toneIsPlaying = false;
            } else if (extra.equals("tones")) {
                this.notesFirst = false;
                this.toneIsPlaying = true;
            }
            String instr = extras.getString("Instrument");
            if (instr.equals("Pure Tone")) {
                this.instrument = 0;
            } else if (instr.equals("Piano")) {
                this.instrument = 1;
            } else if (instr.equals("Guitar")) {
                this.instrument = 2;
            }
        }
        scramble();
        musicOrNot();
        buttonLabel();
        ((AdView) findViewById(R.id.adView_f)).loadAd(new AdRequest());
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.flip_button /*2131230751*/:
                if (!this.toneIsPlaying) {
                    this.toneIsPlaying = true;
                    break;
                } else {
                    this.toneIsPlaying = false;
                    break;
                }
            case R.id.previous_button /*2131230752*/:
                this.index--;
                if (this.index < 0) {
                    this.index = 11;
                }
                if (!this.notesFirst) {
                    this.toneIsPlaying = true;
                    break;
                } else {
                    this.toneIsPlaying = false;
                    break;
                }
            case R.id.next_button /*2131230753*/:
                this.index++;
                if (this.index > 11) {
                    this.index = 0;
                }
                if (!this.notesFirst) {
                    this.toneIsPlaying = true;
                    break;
                } else {
                    this.toneIsPlaying = false;
                    break;
                }
        }
        buttonLabel();
        musicOrNot();
        ((AdView) findViewById(R.id.adView_f)).loadAd(new AdRequest());
    }

    private void scramble() {
        for (int i = 0; i < 12; i++) {
            swap((int) (Math.random() * 12.0d), (int) (Math.random() * 12.0d));
        }
    }

    private void swap(int a, int b) {
        int temp = this.tones[a];
        this.tones[a] = this.tones[b];
        this.tones[b] = temp;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        musicOrNot();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Music.stop(this);
    }

    private void buttonLabel() {
        if (this.toneIsPlaying) {
            this.flipButton.setText("Press for Note Name");
        } else {
            this.flipButton.setText(String.valueOf(Pitch.getNoteName(this.tones[this.index])) + "\nPress for Tone");
        }
    }

    private void musicOrNot() {
        if (this.toneIsPlaying) {
            Music.play(this, Pitch.getSongId(this.instrument, this.tones[this.index]));
        } else {
            Music.stop(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("Index", this.index);
        savedInstanceState.putBoolean("toneIsPlaying", this.toneIsPlaying);
        savedInstanceState.putBoolean("notesFirst", this.notesFirst);
        savedInstanceState.putIntArray("tonesArray", this.tones);
        super.onSaveInstanceState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        this.index = savedInstanceState.getInt("Index");
        this.toneIsPlaying = savedInstanceState.getBoolean("toneIsPlaying");
        this.notesFirst = savedInstanceState.getBoolean("notesFirst");
        this.tones = savedInstanceState.getIntArray("tonesArray");
        super.onRestoreInstanceState(savedInstanceState);
    }
}
