package com.tearn.karchgear;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int health = 2130837504;
        public static final int icon = 2130837505;
        public static final int out = 2130837506;
    }

    public static final class id {
        public static final int webview = 2131034112;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app = 2130968578;
        public static final int app_name = 2130968579;
        public static final int hello = 2130968576;
        public static final int pack = 2130968577;
    }
}
