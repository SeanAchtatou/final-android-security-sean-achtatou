package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;

public class Range {
    private int a;
    private int b;

    @PublishedFor__1_0_0
    public Range(int i, int i2) {
        if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("length & location must be >= 0");
        }
        this.b = i;
        this.a = i2;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    public void a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("location must be >= 0");
        }
        this.b = i;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.b + this.a;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Range)) {
            return super.equals(obj);
        }
        Range range = (Range) obj;
        return a() == range.a() && b() == range.b();
    }

    @PublishedFor__1_0_0
    public int getLength() {
        return this.a;
    }

    @PublishedFor__1_0_0
    public int getLocation() {
        return this.b;
    }

    public int hashCode() {
        return new Integer(a()).hashCode() ^ new Integer(b()).hashCode();
    }

    public String toString() {
        return " [" + a() + ", " + b() + "] ";
    }
}
