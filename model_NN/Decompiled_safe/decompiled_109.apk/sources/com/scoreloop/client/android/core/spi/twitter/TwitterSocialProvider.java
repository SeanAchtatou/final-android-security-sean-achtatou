package com.scoreloop.client.android.core.spi.twitter;

import com.scoreloop.client.android.core.model.SocialProvider;

public class TwitterSocialProvider extends SocialProvider {
    public Class<?> b() {
        return TwitterSocialProviderController.class;
    }

    public String getIdentifier() {
        return SocialProvider.TWITTER_IDENTIFIER;
    }
}
