package com.scoreloop.client.android.ui.component.challenge;

import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.scoreloop.client.android.ui.component.base.StringFormatter;
import com.skyd.bestpuzzle.n1649.R;

public class ChallengeOpenListItem extends StandardListItem<Challenge> {
    public ChallengeOpenListItem(ComponentActivity context, Challenge challenge) {
        super(context, challenge);
        setDrawable(context.getResources().getDrawable(R.drawable.sl_icon_user));
        setTitle(challenge.getContender().getDisplayName());
        setSubTitle(context.getModeString(challenge.getMode().intValue()));
        setSubTitle2(StringFormatter.formatMoney(challenge.getStake(), getComponentActivity().getConfiguration()));
    }

    /* access modifiers changed from: protected */
    public String getImageUrl() {
        return ((Challenge) getTarget()).getContender().getImageUrl();
    }

    public int getType() {
        return 6;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_challenge_open;
    }

    /* access modifiers changed from: protected */
    public int getSubTitle2Id() {
        return R.id.sl_subtitle2;
    }

    public boolean isEnabled() {
        return true;
    }
}
