package com.scoreloop.client.android.ui.component.user;

import android.graphics.drawable.Drawable;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.skyd.bestpuzzle.n1649.R;

public class UserAddBuddyListItem extends StandardListItem<Object> {
    public UserAddBuddyListItem(ComponentActivity context, Drawable drawable, String title, Object target) {
        super(context, drawable, title, null, target);
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_icon_title;
    }

    public int getType() {
        return 25;
    }
}
