package com.scoreloop.client.android.ui.component.agent;

import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.Entity;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.framework.ValueStore;

public class UserAgent extends BaseAgent {
    public static final String[] SUPPORTED_KEYS = {Constant.USER_NAME, Constant.USER_IMAGE_URL, Constant.USER_BALANCE, Constant.NUMBER_GAMES, Constant.NUMBER_BUDDIES, Constant.NUMBER_GLOBAL_ACHIEVEMENTS};
    private UserController _userController;

    public UserAgent() {
        super(SUPPORTED_KEYS);
    }

    /* access modifiers changed from: protected */
    public void onFinishRetrieve(RequestController aRequestController, ValueStore valueStore) {
        User user = this._userController.getUser();
        putValue(Constant.USER_NAME, user.getDisplayName());
        putValue(Constant.USER_IMAGE_URL, user.getImageUrl());
        putValue(Constant.USER_BALANCE, Session.getCurrentSession().getBalance());
        putValue(Constant.NUMBER_GAMES, user.getGamesCounter());
        putValue(Constant.NUMBER_BUDDIES, user.getBuddiesCounter());
        putValue(Constant.NUMBER_GLOBAL_ACHIEVEMENTS, user.getGlobalAchievementsCounter());
    }

    /* access modifiers changed from: protected */
    public void onStartRetrieve(ValueStore valueStore) {
        this._userController = new UserController(this);
        this._userController.setUser((Entity) valueStore.getValue(Constant.USER));
        this._userController.loadUser();
    }

    public void retrieve(ValueStore valueStore) {
        super.retrieve(valueStore);
        putValue(Constant.USER_NAME, this._userController.getUser().getDisplayName());
    }
}
