package com.scoreloop.client.android.core.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class HTTPUtils {
    public static InputStream a(String str) throws URISyntaxException, ClientProtocolException, IOException {
        return a(new URL(str));
    }

    public static InputStream a(URL url) throws URISyntaxException, ClientProtocolException, IOException {
        return b(new DefaultHttpClient().execute(new HttpGet(url.toURI())));
    }

    public static String a(HttpResponse httpResponse) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
        String str = "";
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                str = str + readLine;
            } else {
                bufferedReader.close();
                return str;
            }
        }
    }

    public static InputStream b(HttpResponse httpResponse) throws IOException {
        return httpResponse.getEntity().getContent();
    }
}
