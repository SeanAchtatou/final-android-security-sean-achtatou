package com.scoreloop.client.android.ui.framework;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public abstract class SegmentedView extends LinearLayout implements View.OnClickListener {
    public int oldSelectedSegment = -1;
    private View.OnClickListener onSegmentClickListener;
    protected int selectedSegment = -1;

    /* access modifiers changed from: protected */
    public abstract void setSegmentEnabled(int i, boolean z);

    public SegmentedView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public boolean allowsReselection() {
        return false;
    }

    public int getSelectedSegment() {
        return this.selectedSegment;
    }

    public View getSelectedSegmentView() {
        if (this.selectedSegment != -1) {
            return getChildAt(this.selectedSegment);
        }
        return null;
    }

    public void onClick(View view) {
        int count = getChildCount();
        int i = 0;
        while (i < count) {
            if (getChildAt(i) != view) {
                i++;
            } else if (i != this.selectedSegment || allowsReselection()) {
                switchToSegment(i);
                if (this.onSegmentClickListener != null) {
                    this.onSegmentClickListener.onClick(this);
                    return;
                }
                return;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void prepareSelection() {
    }

    public void prepareUsage() {
        requestLayout();
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            getChildAt(i).setOnClickListener(this);
        }
        prepareSelection();
    }

    public void setOnSegmentClickListener(View.OnClickListener listener) {
        this.onSegmentClickListener = listener;
    }

    /* access modifiers changed from: protected */
    public void setSegment(int segment) {
        if (this.selectedSegment != -1) {
            setSegmentEnabled(this.selectedSegment, false);
        }
        this.oldSelectedSegment = this.selectedSegment;
        this.selectedSegment = segment;
        if (this.selectedSegment != -1) {
            setSegmentEnabled(this.selectedSegment, true);
        }
    }

    public void switchToSegment(int segment) {
        setSegment(segment);
    }
}
