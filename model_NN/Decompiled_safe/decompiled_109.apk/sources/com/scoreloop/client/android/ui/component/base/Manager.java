package com.scoreloop.client.android.ui.component.base;

import com.scoreloop.client.android.core.model.Challenge;

public interface Manager {
    boolean canStartGamePlay();

    boolean isChallengeOngoing();

    void persistSessionUserName();

    void startGamePlay(Integer num, Challenge challenge);

    void submitAchievements(Runnable runnable);
}
