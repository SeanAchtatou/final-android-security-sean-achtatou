package com.scoreloop.client.android.ui.framework;

import com.scoreloop.client.android.ui.framework.NavigationIntent;
import com.scoreloop.client.android.ui.framework.ScreenManager;
import java.util.ArrayList;
import java.util.List;

public class StandardScreenManager implements ScreenManager {
    private static final boolean TRACE_FRAMEWORK = false;
    private ScreenManager.Delegate _delegate;
    private boolean _frameworkHooksEnabled = true;
    /* access modifiers changed from: private */
    public StackEntry _stack = null;
    private ScreenDescription _storedDescription = null;

    protected static class StackEntry {
        /* access modifiers changed from: private */
        public final StackEntry _next;
        /* access modifiers changed from: private */
        public ScreenActivityProtocol _screenActivity;
        private final ScreenDescription _screenDescription;

        public static int getDepth(StackEntry entry) {
            if (entry == null) {
                return 0;
            }
            return getDepth(entry._next) + 1;
        }

        private StackEntry(StackEntry next, ScreenDescription description, ScreenActivityProtocol activity) {
            this._next = next;
            this._screenDescription = description;
            this._screenActivity = activity;
        }

        /* synthetic */ StackEntry(StackEntry stackEntry, ScreenDescription screenDescription, ScreenActivityProtocol screenActivityProtocol, StackEntry stackEntry2) {
            this(stackEntry, screenDescription, screenActivityProtocol);
        }

        /* access modifiers changed from: package-private */
        public ActivityDescription getActivityDescription(String id) {
            ActivityDescription description = this._screenDescription.getActivityDescription(id);
            if (description != null || this._next == null) {
                return description;
            }
            return this._next.getActivityDescription(id);
        }

        /* access modifiers changed from: package-private */
        public ScreenActivityProtocol getScreenActivity() {
            return this._screenActivity;
        }

        /* access modifiers changed from: package-private */
        public ScreenDescription getScreenDescription() {
            return this._screenDescription;
        }
    }

    public void onShowedTab(ScreenDescription screenDescription) {
        this._delegate.screenManagerWillShowScreenDescription(screenDescription, ScreenManager.Delegate.Direction.NONE);
    }

    /* Debug info: failed to restart local var, previous not found, register: 12 */
    /* access modifiers changed from: protected */
    public void applyCurrentDescription(ScreenDescription referenceDescription, int anim) {
        ScreenDescription screenDescription = getCurrentDescription();
        ScreenActivityProtocol screenActivity = getCurrentActivity();
        this._delegate.screenManagerWillShowScreenDescription(screenDescription, getDirection(anim));
        ActivityDescription headerDescription = screenDescription.getHeaderDescription();
        if (headerDescription == null) {
            throw new IllegalStateException("we don't currently support screens without a header");
        }
        if (referenceDescription != null) {
            headerDescription.setWantsClearTop(wantsNewActivity(headerDescription, referenceDescription.getHeaderDescription()));
        }
        screenActivity.startHeader(headerDescription, anim);
        List<ActivityDescription> bodyDescriptions = screenDescription.getBodyDescriptions();
        int bodyCount = bodyDescriptions.size();
        if (referenceDescription != null) {
            List<ActivityDescription> referenceBodyDescriptions = referenceDescription.getBodyDescriptions();
            int count = Math.min(bodyCount, referenceBodyDescriptions.size());
            for (int i = 0; i < count; i++) {
                ActivityDescription bodyDescription = bodyDescriptions.get(i);
                bodyDescription.setWantsClearTop(wantsNewActivity(bodyDescription, referenceBodyDescriptions.get(i)));
            }
        }
        if (bodyCount == 0) {
            screenActivity.startEmptyBody();
        } else if (bodyCount == 1) {
            screenActivity.startBody(bodyDescriptions.get(0), anim);
        } else if (bodyCount > 1) {
            storeDescription(screenDescription);
            screenActivity.startTabBody(screenDescription, anim);
        }
        screenActivity.setShortcuts(screenDescription);
    }

    public void display(final ScreenDescription description) {
        if (description != null) {
            withNavigationAllowed(NavigationIntent.Type.FORWARD, new Runnable() {
                public void run() {
                    ScreenDescription previousDescription = StandardScreenManager.this.getCurrentDescription();
                    if (previousDescription == null || !StandardScreenManager.this.wantsNewScreen(description, previousDescription)) {
                        StandardScreenManager.this.pushDescriptionAndActivity(description, StandardScreenManager.this.getCurrentActivity());
                        StandardScreenManager.this.applyCurrentDescription(previousDescription, 1);
                        return;
                    }
                    StandardScreenManager.this.storeDescription(description);
                    StandardScreenManager.this.startNewScreen();
                }
            });
        }
    }

    public void displayInScreen(ScreenDescription description, ScreenActivityProtocol screenActivity, boolean wantsEmptyStack) {
        if (wantsEmptyStack) {
            doFinishDisplay();
        }
        pushDescriptionAndActivity(description, screenActivity);
        applyCurrentDescription(getPreviousDescription(), 1);
    }

    public void displayPreviousDescription() {
        withNavigationAllowed(NavigationIntent.Type.BACK, new Runnable() {
            public void run() {
                StackEntry previousEntry = StandardScreenManager.this.popEntry();
                StackEntry currentEntry = StandardScreenManager.this._stack;
                if (currentEntry == null || currentEntry.getScreenActivity() != previousEntry.getScreenActivity()) {
                    previousEntry.getScreenActivity().getActivity().finish();
                } else {
                    StandardScreenManager.this.applyCurrentDescription(previousEntry.getScreenDescription(), 2);
                }
            }
        });
    }

    public void displayReferencedStackEntryInScreen(int stackEntryReference, ScreenActivityProtocol newScreenActivity) {
        ScreenActivityProtocol oldScreenActivity = this._stack.getScreenActivity();
        for (StackEntry entry = this._stack; entry != null; entry = entry._next) {
            if (entry.getScreenActivity() == oldScreenActivity) {
                entry._screenActivity = newScreenActivity;
            }
        }
        applyCurrentDescription(null, 0);
    }

    public void displayStoredDescriptionInScreen(ScreenActivityProtocol screenActivity) {
        if (this._storedDescription != null) {
            ScreenDescription description = this._storedDescription;
            this._storedDescription = null;
            displayInScreen(description, screenActivity, false);
        }
    }

    public void displayStoredDescriptionInTabs(TabsActivityProtocol tabs) {
        if (this._storedDescription != null) {
            ScreenDescription description = this._storedDescription;
            this._storedDescription = null;
            tabs.startDescription(description);
        }
    }

    public void displayWithEmptyStack(ScreenDescription description) {
        ScreenDescription previousDescription = getCurrentDescription();
        List<ScreenActivityProtocol> screenActivities = getScreenActivities();
        int count = screenActivities.size() - 1;
        for (int i = 0; i < count; i++) {
            screenActivities.get(i).getActivity().finish();
        }
        setFrameworkHooksEnabled(false);
        setStack(null);
        ScreenActivityProtocol lastScreenActivity = screenActivities.get(count);
        lastScreenActivity.cleanOutSubactivities();
        pushDescriptionAndActivity(description, lastScreenActivity);
        applyCurrentDescription(previousDescription, 0);
        setFrameworkHooksEnabled(true);
    }

    /* access modifiers changed from: private */
    public void doFinishDisplay() {
        for (ScreenActivityProtocol screenActivity : getScreenActivities()) {
            screenActivity.getActivity().finish();
        }
        setStack(null);
        this._storedDescription = null;
    }

    public void finishDisplay() {
        withNavigationAllowed(NavigationIntent.Type.EXIT, new Runnable() {
            public void run() {
                StandardScreenManager.this.doFinishDisplay();
            }
        });
    }

    public ActivityDescription getActivityDescription(String id) {
        return this._stack.getActivityDescription(id);
    }

    /* access modifiers changed from: protected */
    public ScreenActivityProtocol getCurrentActivity() {
        if (this._stack != null) {
            return this._stack.getScreenActivity();
        }
        return null;
    }

    public ScreenDescription getCurrentDescription() {
        if (this._stack != null) {
            return this._stack.getScreenDescription();
        }
        return null;
    }

    public int getCurrentStackEntryReference() {
        return StackEntry.getDepth(this._stack);
    }

    private ScreenManager.Delegate.Direction getDirection(int anim) {
        switch (anim) {
            case 0:
                return ScreenManager.Delegate.Direction.NONE;
            case 1:
                return ScreenManager.Delegate.Direction.FORWARD;
            case 2:
                return ScreenManager.Delegate.Direction.BACKWARD;
            default:
                return ScreenManager.Delegate.Direction.NONE;
        }
    }

    /* access modifiers changed from: protected */
    public ScreenDescription getPreviousDescription() {
        if (this._stack == null) {
            return null;
        }
        StackEntry previousEntry = this._stack._next;
        if (previousEntry != null) {
            return previousEntry.getScreenDescription();
        }
        return null;
    }

    private List<ScreenActivityProtocol> getScreenActivities() {
        List<ScreenActivityProtocol> screenActivities = new ArrayList<>();
        ScreenActivityProtocol lastAddedActivity = null;
        for (StackEntry entry = this._stack; entry != null; entry = entry._next) {
            ScreenActivityProtocol activity = entry.getScreenActivity();
            if (lastAddedActivity == null || activity != lastAddedActivity) {
                screenActivities.add(activity);
                lastAddedActivity = activity;
            }
        }
        return screenActivities;
    }

    private void onFrameworkEntered() {
        if (this._delegate != null) {
            this._delegate.screenManagerWillEnterFramework(this);
        }
    }

    private void onFrameworkLeft() {
        if (this._delegate != null) {
            this._delegate.screenManagerDidLeaveFramework(this);
        }
    }

    public void onWillShowOptionsMenu() {
        this._delegate.screenManagerWillShowOptionsMenu();
    }

    /* access modifiers changed from: private */
    public StackEntry popEntry() {
        if (this._stack == null) {
            return null;
        }
        StackEntry previous = this._stack;
        setStack(this._stack._next);
        return previous;
    }

    /* access modifiers changed from: private */
    public void pushDescriptionAndActivity(ScreenDescription model, ScreenActivityProtocol screen) {
        setStack(new StackEntry(this._stack, model, screen, null));
    }

    public void setDelegate(ScreenManager.Delegate policy) {
        this._delegate = policy;
    }

    private void setFrameworkHooksEnabled(boolean enabled) {
        this._frameworkHooksEnabled = enabled;
    }

    private void setStack(StackEntry entry) {
        StackEntry oldStack = this._stack;
        this._stack = entry;
        if (this._frameworkHooksEnabled) {
            if (oldStack == null && entry != null) {
                onFrameworkEntered();
            } else if (oldStack != null && entry == null) {
                onFrameworkLeft();
            }
        }
    }

    /* access modifiers changed from: private */
    public void startNewScreen() {
        getCurrentActivity().startNewScreen();
    }

    /* access modifiers changed from: private */
    public void storeDescription(ScreenDescription model) {
        this._storedDescription = model;
    }

    private boolean wantsNewActivity(ActivityDescription description, ActivityDescription referenceDescription) {
        return true;
    }

    /* access modifiers changed from: private */
    public boolean wantsNewScreen(ScreenDescription description, ScreenDescription referenceDescription) {
        if (this._delegate != null) {
            return this._delegate.screenManagerWantsNewScreen(null, description, referenceDescription);
        }
        return false;
    }

    private void withNavigationAllowed(NavigationIntent.Type navigationType, Runnable runnable) {
        ScreenActivityProtocol activity = getCurrentActivity();
        if (activity == null) {
            runnable.run();
            return;
        }
        NavigationIntent navigationIntent = new NavigationIntent(navigationType, runnable);
        if (activity.isNavigationAllowed(navigationIntent)) {
            navigationIntent.execute();
        }
    }
}
