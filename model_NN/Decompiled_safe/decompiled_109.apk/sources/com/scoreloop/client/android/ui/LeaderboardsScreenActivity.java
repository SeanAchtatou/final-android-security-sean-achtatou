package com.scoreloop.client.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.ui.framework.ScreenActivity;

public class LeaderboardsScreenActivity extends ScreenActivity {
    public static final String LEADERBOARD = "leaderboard";
    public static final int LEADERBOARD_24h = 2;
    public static final int LEADERBOARD_FRIENDS = 1;
    public static final int LEADERBOARD_GLOBAL = 0;
    public static final String MODE = "mode";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StandardScoreloopManager manager = StandardScoreloopManager.getFactory(ScoreloopManagerSingleton.get());
        Intent intent = getIntent();
        Integer mode = null;
        if (intent.hasExtra("mode")) {
            mode = Integer.valueOf(intent.getIntExtra("mode", 0));
            Game game = manager.getSession().getGame();
            Integer minMode = game.getMinMode();
            Integer maxMode = game.getMaxMode();
            if (!(minMode == null || maxMode == null || (mode.intValue() >= minMode.intValue() && mode.intValue() < maxMode.intValue()))) {
                Log.e(ScreenActivity.TAG, "mode extra parameter on LeaderboardsScreenActivity is out of range [" + minMode + "," + maxMode + "[");
                finish();
                return;
            }
        }
        Integer leaderboard = null;
        if (intent.hasExtra(LEADERBOARD)) {
            leaderboard = Integer.valueOf(intent.getIntExtra(LEADERBOARD, 0));
            if (leaderboard.intValue() < 0 || leaderboard.intValue() > 2) {
                Log.e(ScreenActivity.TAG, "leaderboard extra parameter on LeaderboardsScreenActivity is invalid");
                finish();
                return;
            }
        }
        display(manager.createScoreScreenDescription(null, mode, leaderboard), savedInstanceState);
    }
}
