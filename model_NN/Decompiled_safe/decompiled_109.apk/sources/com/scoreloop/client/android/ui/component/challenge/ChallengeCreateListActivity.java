package com.scoreloop.client.android.ui.component.challenge;

import android.os.Bundle;
import com.scoreloop.client.android.core.controller.ChallengeController;
import com.scoreloop.client.android.core.controller.ChallengeControllerObserver;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.agent.UserDetailsAgent;
import com.scoreloop.client.android.ui.component.base.CaptionListItem;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.StringFormatter;
import com.scoreloop.client.android.ui.component.challenge.ChallengeControlsListItem;
import com.scoreloop.client.android.ui.framework.BaseDialog;
import com.scoreloop.client.android.ui.framework.ValueStore;
import com.skyd.bestpuzzle.n1649.R;

public class ChallengeCreateListActivity extends ChallengeActionListActivity implements ChallengeControllerObserver, BaseDialog.OnActionListener, ChallengeControlsListItem.OnControlObserver {
    private ChallengeControlsListItem _challengeControlsListItem;
    private ChallengeParticipantsListItem _challengeParticipantsListItem;
    private ChallengeSettingsEditListItem _challengeStakeAndModeEditListItem;
    private User _contestant;
    private ValueStore _opponentValueStore;

    public void challengeControllerDidFailOnInsufficientBalance(ChallengeController challengeController) {
        throw new IllegalStateException("this should not happen - an insufficient balance error occured while creating the challenge");
    }

    public void challengeControllerDidFailToAcceptChallenge(ChallengeController challengeController) {
        throw new IllegalStateException("this should not happen - an accept error occured while creating the challenge");
    }

    public void challengeControllerDidFailToRejectChallenge(ChallengeController challengeController) {
        throw new IllegalStateException("this should not happen - a reject error occured while creating the challenge");
    }

    /* access modifiers changed from: package-private */
    public CaptionListItem getCaptionListItem() {
        return new CaptionListItem(this, null, getString(R.string.sl_new_challenge));
    }

    /* access modifiers changed from: package-private */
    public ChallengeControlsListItem getChallengeControlsListItem() {
        this._challengeControlsListItem = new ChallengeControlsListItem(this, null, this);
        return this._challengeControlsListItem;
    }

    /* access modifiers changed from: package-private */
    public ChallengeParticipantsListItem getChallengeParticipantsListItem() {
        if (this._challengeParticipantsListItem == null) {
            this._challengeParticipantsListItem = new ChallengeParticipantsListItem(this, getUser(), this._contestant);
        }
        return this._challengeParticipantsListItem;
    }

    /* access modifiers changed from: package-private */
    public ChallengeSettingsListItem getChallengeStakeAndModeListItem() {
        this._challengeStakeAndModeEditListItem = new ChallengeSettingsEditListItem(this);
        return this._challengeStakeAndModeEditListItem;
    }

    public void onAction(BaseDialog dialog, int actionId) {
        dialog.dismiss();
        displayPrevious();
    }

    public void onControl1() {
        Money stake;
        if (challengeGamePlayAllowed() && (stake = this._challengeStakeAndModeEditListItem.getStake()) != null) {
            ChallengeController challengeController = new ChallengeController(this);
            challengeController.createChallenge(stake, this._contestant);
            Integer mode = this._challengeStakeAndModeEditListItem.getMode();
            if (mode != null) {
                challengeController.getChallenge().setMode(mode);
            }
            finishDisplay();
            getManager().startGamePlay(mode, challengeController.getChallenge());
        }
    }

    public void onControl2() {
        throw new IllegalStateException("this should not happen - a button has been clicked that isn't there");
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addObservedKeys(ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NUMBER_CHALLENGES_WON));
        this._contestant = (User) getActivityArguments().getValue(Constant.CONTESTANT, null);
        if (this._contestant != null) {
            this._opponentValueStore = new ValueStore();
            this._opponentValueStore.putValue(Constant.USER, this._contestant);
            this._opponentValueStore.addObserver(Constant.NUMBER_CHALLENGES_WON, this);
            this._opponentValueStore.addValueSources(new UserDetailsAgent());
            setNeedsRefresh();
        }
        initAdapter();
    }

    public void onRefresh(int flags) {
        if (this._contestant != null) {
            this._opponentValueStore.retrieveValue(Constant.NUMBER_CHALLENGES_WON, ValueStore.RetrievalMode.NOT_DIRTY, null);
        }
    }

    public void onValueChanged(ValueStore valueStore, String key, Object oldValue, Object newValue) {
        if (valueStore == this._opponentValueStore) {
            if (isValueChangedFor(key, Constant.NUMBER_CHALLENGES_WON, oldValue, newValue)) {
                getChallengeParticipantsListItem().setContestantStats(StringFormatter.getChallengesSubTitle(this, this._opponentValueStore));
                getBaseListAdapter().notifyDataSetChanged();
            }
        } else if (isValueChangedFor(key, Constant.NUMBER_CHALLENGES_WON, oldValue, newValue)) {
            getChallengeParticipantsListItem().setContenderStats(StringFormatter.getChallengesSubTitle(this, valueStore));
            getBaseListAdapter().notifyDataSetChanged();
        }
    }

    public void onValueSetDirty(ValueStore valueStore, String key) {
        if (valueStore == this._opponentValueStore) {
            this._opponentValueStore.retrieveValue(Constant.NUMBER_CHALLENGES_WON, ValueStore.RetrievalMode.NOT_DIRTY, null);
        } else if (Constant.NUMBER_CHALLENGES_WON.equals(key)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        }
    }
}
