package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;

public interface ChallengeControllerObserver extends RequestControllerObserver {
    @PublishedFor__1_0_0
    void challengeControllerDidFailOnInsufficientBalance(ChallengeController challengeController);

    @PublishedFor__1_0_0
    void challengeControllerDidFailToAcceptChallenge(ChallengeController challengeController);

    @PublishedFor__1_0_0
    void challengeControllerDidFailToRejectChallenge(ChallengeController challengeController);
}
