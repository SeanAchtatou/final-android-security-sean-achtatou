package com.skyd.core.android.game.crosswisewar;

import com.skyd.core.game.crosswisewar.IBuff;
import com.skyd.core.game.crosswisewar.IEntity;
import com.skyd.core.game.crosswisewar.IWarrior;

public abstract class Buff extends Warrior implements IBuff {
    private int _KeepTime = 0;

    public int getKeepTime() {
        return this._KeepTime;
    }

    public void setKeepTime(int value) {
        this._KeepTime = value;
    }

    public void setKeepTimeToDefault() {
        setKeepTime(0);
    }

    public void remove() {
        IEntity buff = getBuff();
        getBuffTarget().setBuff(buff);
        if (buff != null) {
            buff.setBuffTarget(getBuffTarget());
        }
    }

    public void setToTarget(IWarrior value) {
        setBuffTarget(value.getBuffed());
        value.getBuffed().setBuff(this);
    }

    public boolean getIsCanMoveAttac() {
        return ((IWarrior) getBuffTarget()).getIsCanMoveAttac();
    }

    public int getNeedPay() {
        return ((IWarrior) getBuffTarget()).getNeedPay();
    }
}
