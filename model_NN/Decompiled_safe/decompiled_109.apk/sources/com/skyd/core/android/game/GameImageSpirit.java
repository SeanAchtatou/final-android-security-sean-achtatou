package com.skyd.core.android.game;

public class GameImageSpirit extends GamePixelImageSpirit implements IGameImageHolder {
    private GameImage _Image;

    public GameImageSpirit() {
        this(new GameImage());
    }

    public GameImageSpirit(GameImage img) {
        this._Image = null;
        this._Image = img;
        try {
            this._Image.setParent(this);
        } catch (GameException e) {
            e.printStackTrace();
        }
    }

    public GameObject getDisplayContentChild() {
        return getImage();
    }

    public GameImage getImage() {
        return this._Image;
    }

    /* access modifiers changed from: protected */
    public void setImage(GameImage value) {
        this._Image = value;
    }

    /* access modifiers changed from: protected */
    public void setImageToDefault() {
        setImage(null);
    }

    public GameImage getDisplayImage() {
        return getImage();
    }
}
