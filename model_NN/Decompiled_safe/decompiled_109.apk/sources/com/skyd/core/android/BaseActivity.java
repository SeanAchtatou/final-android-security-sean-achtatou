package com.skyd.core.android;

import android.app.Activity;
import android.os.Debug;

public class BaseActivity extends Activity {
    private boolean _IsTraceEnable = false;

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this._IsTraceEnable) {
            Debug.stopMethodTracing();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this._IsTraceEnable) {
            Debug.startMethodTracing("debug");
        }
    }

    public boolean getIsTraceEnable() {
        return this._IsTraceEnable;
    }

    public void setIsTraceEnable(boolean value) {
        this._IsTraceEnable = value;
    }

    public void setIsTraceEnableToDefault() {
        setIsTraceEnable(false);
    }
}
