package com.skyd.core.android.game.crosswisewar;

import com.skyd.core.game.crosswisewar.IEntity;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class Entity extends Obj implements IEntity {
    private int _ATK = 0;
    private int _AttackDelay = 0;
    private int _AttackRange = 1;
    private IEntity _Buff = null;
    private IEntity _BuffTarget = null;
    private int _DEF = 0;
    private ArrayList<OnDestroyedListener> _DestroyedListenerList = null;
    private int _HP = 1;
    private int _MaxHP = 1;
    private int _OccupyHeight = 0;
    private int _OccupyWidth = 0;

    public interface OnDestroyedListener {
        void OnDestroyedEvent(Object obj);
    }

    public int getATK() {
        return this._ATK;
    }

    public void setATK(int value) {
        this._ATK = value;
    }

    public void setATKToDefault() {
        setATK(0);
    }

    public int getAttackDelay() {
        return this._AttackDelay;
    }

    public void setAttackDelay(int value) {
        this._AttackDelay = value;
    }

    public void setAttackDelayToDefault() {
        setAttackDelay(0);
    }

    public int getAttackRange() {
        return this._AttackRange;
    }

    public void setAttackRange(int value) {
        this._AttackRange = value;
    }

    public void setAttackRangeToDefault() {
        setAttackRange(1);
    }

    public int getDEF() {
        return this._DEF;
    }

    public void setDEF(int value) {
        this._DEF = value;
    }

    public void setDEFToDefault() {
        setDEF(0);
    }

    public int getHP() {
        return this._HP;
    }

    public void setHP(int value) {
        this._HP = value;
        if (value <= 0) {
            onDestroyed();
        }
    }

    public void setHPToDefault() {
        setHP(1);
    }

    public int getMaxHP() {
        return this._MaxHP;
    }

    public void setMaxHP(int value) {
        this._MaxHP = value;
    }

    public void setMaxHPToDefault() {
        setMaxHP(1);
    }

    public int getOccupyHeight() {
        return this._OccupyHeight;
    }

    public void setOccupyHeight(int value) {
        this._OccupyHeight = value;
    }

    public void setOccupyHeightToDefault() {
        setOccupyHeight(0);
    }

    public int getOccupyWidth() {
        return this._OccupyWidth;
    }

    public void setOccupyWidth(int value) {
        this._OccupyWidth = value;
    }

    public void setOccupyWidthToDefault() {
        setOccupyWidth(0);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: com.skyd.core.game.crosswisewar.IObj} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.skyd.core.game.crosswisewar.IEntity} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.skyd.core.game.crosswisewar.IEntity getEncounterEntity() {
        /*
            r9 = this;
            r3 = 0
            r2 = 2139095039(0x7f7fffff, float:3.4028235E38)
            com.skyd.core.game.crosswisewar.IScene r6 = r9.getParentScene()
            java.util.ArrayList r6 = r6.getChildrenList()
            java.util.Iterator r6 = r6.iterator()
        L_0x0010:
            boolean r7 = r6.hasNext()
            if (r7 != 0) goto L_0x0017
            return r3
        L_0x0017:
            java.lang.Object r1 = r6.next()
            com.skyd.core.game.crosswisewar.IObj r1 = (com.skyd.core.game.crosswisewar.IObj) r1
            boolean r7 = r1 instanceof com.skyd.core.game.crosswisewar.IEntity
            if (r7 == 0) goto L_0x0010
            r0 = r1
            com.skyd.core.game.crosswisewar.IEntity r0 = (com.skyd.core.game.crosswisewar.IEntity) r0
            r4 = r0
            float r7 = r1.getPositionInScene()
            float r8 = r9.getPositionInScene()
            float r7 = r7 - r8
            float r5 = java.lang.Math.abs(r7)
            com.skyd.core.game.crosswisewar.INation r7 = r4.getNation()
            com.skyd.core.game.crosswisewar.INation r8 = r9.getNation()
            boolean r7 = r7.equals(r8)
            if (r7 != 0) goto L_0x0010
            int r7 = r9.getAttackRange()
            float r7 = (float) r7
            int r7 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r7 > 0) goto L_0x0010
            int r7 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r7 >= 0) goto L_0x0010
            r2 = r5
            r3 = r4
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.skyd.core.android.game.crosswisewar.Entity.getEncounterEntity():com.skyd.core.game.crosswisewar.IEntity");
    }

    public IEntity getBuff() {
        return this._Buff;
    }

    public void setBuff(IEntity value) {
        this._Buff = value;
    }

    public void setBuffToDefault() {
        setBuff(null);
    }

    public IEntity getBuffed() {
        return getBuff() == null ? this : getBuff().getBuffed();
    }

    public IEntity getNative() {
        return getBuffTarget() == null ? this : getBuffTarget().getNative();
    }

    public int hurt(IEntity Source) {
        if (getBuff() != null) {
            return getBuff().hurt(Source);
        }
        int v = (int) (((double) Source.getATK()) * (1.0d - (((double) getDEF()) * 0.01d)));
        setHP(getHP() - v);
        return v;
    }

    public int hurtTo(IEntity target) {
        if (getBuff() == null) {
            return target.hurt(this);
        }
        return getBuff().hurtTo(target);
    }

    public void attack(IEntity target) {
        hurtTo(target);
    }

    public IEntity getBuffTarget() {
        return this._BuffTarget;
    }

    public void setBuffTarget(IEntity value) {
        this._BuffTarget = value;
    }

    public void setBuffTargetToDefault() {
        setBuffTarget(null);
    }

    public boolean addOnDestroyedListener(OnDestroyedListener listener) {
        if (this._DestroyedListenerList == null) {
            this._DestroyedListenerList = new ArrayList<>();
        } else if (this._DestroyedListenerList.contains(listener)) {
            return false;
        }
        this._DestroyedListenerList.add(listener);
        return true;
    }

    public boolean removeOnDestroyedListener(OnDestroyedListener listener) {
        if (this._DestroyedListenerList == null || !this._DestroyedListenerList.contains(listener)) {
            return false;
        }
        this._DestroyedListenerList.remove(listener);
        return true;
    }

    public void clearOnDestroyedListeners() {
        if (this._DestroyedListenerList != null) {
            this._DestroyedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroyed() {
        if (this._DestroyedListenerList != null) {
            Iterator<OnDestroyedListener> it = this._DestroyedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnDestroyedEvent(this);
            }
        }
    }
}
