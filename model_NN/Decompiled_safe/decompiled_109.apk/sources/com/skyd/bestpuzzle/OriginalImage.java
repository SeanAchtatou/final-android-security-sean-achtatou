package com.skyd.bestpuzzle;

import com.skyd.core.android.game.GameImageSpirit;
import com.skyd.core.vector.Vector2DF;

public class OriginalImage extends GameImageSpirit {
    private Vector2DF _OriginalSize = new Vector2DF();
    private int _TotalSeparateColumn = 0;
    private int _TotalSeparateRow = 0;

    public int getTotalSeparateColumn() {
        return this._TotalSeparateColumn;
    }

    public void setTotalSeparateColumn(int value) {
        this._TotalSeparateColumn = value;
    }

    public void setTotalSeparateColumnToDefault() {
        setTotalSeparateColumn(0);
    }

    public int getTotalSeparateRow() {
        return this._TotalSeparateRow;
    }

    public void setTotalSeparateRow(int value) {
        this._TotalSeparateRow = value;
    }

    public void setTotalSeparateRowToDefault() {
        setTotalSeparateRow(0);
    }

    public Vector2DF getOriginalSize() {
        return this._OriginalSize;
    }

    public void setOriginalSize(Vector2DF value) {
        this._OriginalSize = value;
    }

    public void setOriginalSizeToDefault() {
        setOriginalSize(new Vector2DF());
    }
}
