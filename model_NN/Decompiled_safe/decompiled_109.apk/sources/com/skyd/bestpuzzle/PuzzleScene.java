package com.skyd.bestpuzzle;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.widget.Toast;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.skyd.bestpuzzle.n1649.R;
import com.skyd.core.android.game.GameImageSpirit;
import com.skyd.core.android.game.GameMaster;
import com.skyd.core.android.game.GameObject;
import com.skyd.core.android.game.GameScene;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.draw.DrawHelper;
import com.skyd.core.vector.Vector2DF;
import com.skyd.core.vector.VectorRect2DF;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class PuzzleScene extends GameScene {
    public Controller Controller;
    public Desktop Desktop;
    boolean IsDrawingPuzzle;
    boolean IsDrawnSelectRect;
    public GameImageSpirit MoveButton;
    public UI MoveButtonSlot;
    public OriginalImage OriginalImage;
    public long PastTime;
    public long StartTime;
    public UI SubmitScoreButton;
    /* access modifiers changed from: private */
    public RectF SubmitScoreButtonRect;
    public GameSpirit Time;
    public UI ViewFullButton;
    /* access modifiers changed from: private */
    public RectF ViewFullButtonRect;
    public UI ZoomInButton;
    public UI ZoomOutButton;
    private boolean _IsFinished = false;
    private ArrayList<OnIsFinishedChangedListener> _IsFinishedChangedListenerList = null;
    long savetime = Long.MIN_VALUE;
    RectF selectRect;

    public interface OnIsFinishedChangedListener {
        void OnIsFinishedChangedEvent(Object obj, boolean z);
    }

    public PuzzleScene() {
        setMaxDrawCacheLayer(25.0f);
        this.StartTime = new Date().getTime();
        loadGameState();
    }

    public void loadGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        this.PastTime = c.getPastTime().longValue();
        this._IsFinished = c.getIsFinished().booleanValue();
    }

    public void saveGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        if (getIsFinished()) {
            c.setPastTime(Long.valueOf(this.PastTime));
        } else {
            c.setPastTime(Long.valueOf(this.PastTime + (new Date().getTime() - this.StartTime)));
        }
        c.setIsFinished(Boolean.valueOf(getIsFinished()));
    }

    public void setFinished() {
        if (!getIsFinished()) {
            this.SubmitScoreButton.show();
        }
        setIsFinished(true);
        this.PastTime += new Date().getTime() - this.StartTime;
    }

    public boolean getIsFinished() {
        return this._IsFinished;
    }

    private void setIsFinished(boolean value) {
        onIsFinishedChanged(value);
        this._IsFinished = value;
    }

    private void setIsFinishedToDefault() {
        setIsFinished(false);
    }

    public boolean addOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null) {
            this._IsFinishedChangedListenerList = new ArrayList<>();
        } else if (this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null || !this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnIsFinishedChangedListeners() {
        if (this._IsFinishedChangedListenerList != null) {
            this._IsFinishedChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onIsFinishedChanged(boolean newValue) {
        if (this._IsFinishedChangedListenerList != null) {
            Iterator<OnIsFinishedChangedListener> it = this._IsFinishedChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnIsFinishedChangedEvent(this, newValue);
            }
        }
    }

    public int getTargetCacheID() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
        this.IsDrawingPuzzle = false;
        this.IsDrawnSelectRect = false;
        super.drawChilds(c, drawArea);
    }

    /* access modifiers changed from: protected */
    public boolean onDrawingChild(GameObject child, Canvas c, Rect drawArea) {
        float lvl = ((GameSpirit) child).getLevel();
        if (!this.IsDrawingPuzzle && lvl <= 10.0f) {
            this.IsDrawingPuzzle = true;
            c.setMatrix(this.Desktop.getMatrix());
        } else if (lvl > 10.0f && this.IsDrawingPuzzle) {
            this.IsDrawingPuzzle = false;
            c.setMatrix(new Matrix());
        }
        if (lvl > 25.0f && this.selectRect != null && !this.IsDrawnSelectRect) {
            this.IsDrawnSelectRect = true;
            Paint p = new Paint();
            p.setARGB(100, 180, 225, 255);
            c.drawRect(this.selectRect, p);
        }
        return super.onDrawingChild(child, c, drawArea);
    }

    public void load(Context c) {
        loadOriginalImage(c);
        loadDesktop(c);
        loadInterface(c);
        loadPuzzle(c);
        loadPuzzleState();
        this.Desktop.updateCurrentObserveAreaRectF();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF
     arg types: [int, int, float, float, int]
     candidates:
      com.skyd.core.draw.DrawHelper.calculateScaleSize(double, double, double, double, boolean):com.skyd.core.vector.Vector2D
      com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF */
    public void loadOriginalImage(Context c) {
        this.OriginalImage = new OriginalImage() {
            /* access modifiers changed from: protected */
            public boolean onDrawing(Canvas c, Rect drawArea) {
                c.drawARGB(180, 0, 0, 0);
                return super.onDrawing(c, drawArea);
            }
        };
        this.OriginalImage.setLevel(50.0f);
        this.OriginalImage.setName("OriginalImage");
        this.OriginalImage.getSize().resetWith(DrawHelper.calculateScaleSize(320.0f, 240.0f, (float) (GameMaster.getScreenWidth() - 40), (float) (GameMaster.getScreenHeight() - 40), true));
        this.OriginalImage.setTotalSeparateColumn(8);
        this.OriginalImage.setTotalSeparateRow(6);
        this.OriginalImage.getOriginalSize().reset(800.0f, 600.0f);
        this.OriginalImage.hide();
        this.OriginalImage.setIsUseAbsolutePosition(true);
        this.OriginalImage.setIsUseAbsoluteSize(true);
        this.OriginalImage.getImage().setIsUseAbsolutePosition(true);
        this.OriginalImage.getImage().setIsUseAbsoluteSize(true);
        this.OriginalImage.getPosition().reset(((float) (GameMaster.getScreenWidth() / 2)) - (this.OriginalImage.getSize().getX() / 2.0f), ((float) (GameMaster.getScreenHeight() / 2)) - (this.OriginalImage.getSize().getY() / 2.0f));
        getSpiritList().add(this.OriginalImage);
    }

    public void loadDesktop(Context c) {
        this.Desktop = new Desktop();
        this.Desktop.setLevel(-9999.0f);
        this.Desktop.setName("Desktop");
        this.Desktop.getSize().reset(1600.0f, 1200.0f);
        this.Desktop.getCurrentObservePosition().reset(800.0f, 600.0f);
        this.Desktop.setOriginalImage(this.OriginalImage);
        this.Desktop.setIsUseAbsolutePosition(true);
        this.Desktop.setIsUseAbsoluteSize(true);
        this.Desktop.ColorAPaint = new Paint();
        this.Desktop.ColorAPaint.setColor(Color.argb(255, 34, 30, 47));
        this.Desktop.ColorBPaint = new Paint();
        this.Desktop.ColorBPaint.setColor(Color.argb(255, 28, 24, 41));
        getSpiritList().add(this.Desktop);
    }

    public void loadPuzzle(Context c) {
        this.Controller = new Controller();
        this.Controller.setLevel(10.0f);
        this.Controller.setName("Controller");
        this.Controller.setDesktop(this.Desktop);
        this.Controller.setIsUseAbsolutePosition(true);
        this.Controller.setIsUseAbsoluteSize(true);
        getSpiritList().add(this.Controller);
        Puzzle.getRealitySize().reset(100.0f, 100.0f);
        Puzzle.getMaxRadius().reset(76.66666f, 76.66666f);
        c1395440640(c);
        c1718568215(c);
        c1566004604(c);
        c1813046260(c);
        c1281467703(c);
        c1084465376(c);
        c374208536(c);
        c868798569(c);
        c191758053(c);
        c538176265(c);
        c650989099(c);
        c2072784504(c);
        c2107762398(c);
        c1861710961(c);
        c180976878(c);
        c1626761966(c);
        c1192911393(c);
        c771831799(c);
        c1144198189(c);
        c1523076608(c);
        c1785268698(c);
        c2074854323(c);
        c1807296321(c);
        c391079276(c);
        c1000323591(c);
        c1640195765(c);
        c1125358841(c);
        c1854851027(c);
        c1482253141(c);
        c1860648131(c);
        c446019339(c);
        c1146332050(c);
        c105157770(c);
        c718940536(c);
        c1245263572(c);
        c1654792066(c);
        c1596080406(c);
        c322042350(c);
        c183069723(c);
        c2008660439(c);
        c1412775374(c);
        c1367843837(c);
        c971061690(c);
        c1022336723(c);
        c1760694289(c);
        c420528363(c);
        c27814050(c);
        c2060586374(c);
    }

    public void loadInterface(Context c) {
        this.MoveButton = new GameImageSpirit();
        this.MoveButton.setLevel(22.0f);
        this.MoveButton.getImage().loadImageFromResource(c, R.drawable.movebutton);
        this.MoveButton.getSize().reset(155.0f, 155.0f);
        this.MoveButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButton);
        final Vector2DF mvds = this.MoveButton.getDisplaySize();
        mvds.scale(0.5f);
        this.MoveButton.getPosition().reset(5.0f + mvds.getX(), ((float) (GameMaster.getScreenHeight() - 5)) - mvds.getY());
        this.MoveButton.getPositionOffset().resetWith(mvds.negateNew());
        this.MoveButtonSlot = new UI() {
            Vector2DF movevector;
            boolean needmove;

            public void executive(Vector2DF point) {
                this.movevector = point.minusNew(getPosition()).restrainLength(mvds.getX() * 0.4f);
                this.needmove = true;
            }

            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.needmove) {
                    PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition().plusNew(this.movevector));
                    PuzzleScene.this.Desktop.getCurrentObservePosition().plus(this.movevector);
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < mvds.getX() + 5.0f;
            }

            public void reset() {
                this.needmove = false;
                PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition());
                PuzzleScene.this.refreshDrawCacheBitmap();
            }
        };
        this.MoveButtonSlot.setLevel(21.0f);
        this.MoveButtonSlot.getImage().loadImageFromResource(c, R.drawable.movebuttonslot);
        this.MoveButtonSlot.getSize().reset(155.0f, 155.0f);
        this.MoveButtonSlot.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButtonSlot);
        this.MoveButtonSlot.getPosition().resetWith(this.MoveButton.getPosition());
        this.MoveButtonSlot.getPositionOffset().resetWith(this.MoveButton.getPositionOffset());
        this.ZoomInButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.min(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.min(double, double):double}
              ClspMth{java.lang.Math.min(long, long):long}
              ClspMth{java.lang.Math.min(int, int):int}
              ClspMth{java.lang.Math.min(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.min(1.3f, PuzzleScene.this.Desktop.getZoom() * 1.05f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }
        };
        this.ZoomInButton.setLevel(21.0f);
        this.ZoomInButton.getImage().loadImageFromResource(c, R.drawable.zoombutton1);
        this.ZoomInButton.getSize().reset(77.0f, 77.0f);
        this.ZoomInButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomInButton);
        Vector2DF zibds = this.ZoomInButton.getDisplaySize().scale(0.5f);
        this.ZoomInButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 50)) - zibds.getX(), ((float) (GameMaster.getScreenHeight() - 10)) - zibds.getY());
        this.ZoomInButton.getPositionOffset().resetWith(zibds.negateNew());
        this.ZoomOutButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.max(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.max(double, double):double}
              ClspMth{java.lang.Math.max(int, int):int}
              ClspMth{java.lang.Math.max(long, long):long}
              ClspMth{java.lang.Math.max(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.max(0.3f, PuzzleScene.this.Desktop.getZoom() * 0.95f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                    GameMaster.log(this, Float.valueOf(PuzzleScene.this.Desktop.getCurrentObserveAreaRectF().width()));
                }
                super.updateSelf();
            }
        };
        this.ZoomOutButton.setLevel(21.0f);
        this.ZoomOutButton.getImage().loadImageFromResource(c, R.drawable.zoombutton2);
        this.ZoomOutButton.getSize().reset(77.0f, 77.0f);
        this.ZoomOutButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomOutButton);
        Vector2DF zobds = this.ZoomOutButton.getDisplaySize().scale(0.5f);
        this.ZoomOutButton.getPosition().reset((((float) (GameMaster.getScreenWidth() - 100)) - zobds.getX()) - (zibds.getX() * 2.0f), ((float) (GameMaster.getScreenHeight() - 10)) - zobds.getY());
        this.ZoomOutButton.getPositionOffset().resetWith(zobds.negateNew());
        this.ViewFullButton = new UI() {
            public void executive(Vector2DF point) {
                if (PuzzleScene.this.OriginalImage.getImage().getImage() == null) {
                    PuzzleScene.this.OriginalImage.getImage().loadImageFromResource(GameMaster.getContext(), R.drawable.oim);
                }
                PuzzleScene.this.OriginalImage.show();
            }

            public boolean isInArea(Vector2DF point) {
                return point.isIn(PuzzleScene.this.ViewFullButtonRect);
            }

            public void reset() {
                PuzzleScene.this.OriginalImage.hide();
            }
        };
        this.ViewFullButton.getImage().loadImageFromResource(c, R.drawable.viewfull);
        this.ViewFullButton.setLevel(21.0f);
        this.ViewFullButton.setIsUseAbsolutePosition(true);
        this.ViewFullButton.getSize().reset(83.0f, 38.0f);
        getSpiritList().add(this.ViewFullButton);
        this.ViewFullButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 15)) - this.ViewFullButton.getDisplaySize().getX(), 15.0f);
        this.ViewFullButtonRect = new VectorRect2DF(this.ViewFullButton.getPosition(), this.ViewFullButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton = new UI() {
            public void executive(Vector2DF point) {
                Score score = new Score(Double.valueOf(Math.ceil((double) (PuzzleScene.this.PastTime / 1000))), null);
                score.setMode(21);
                ScoreloopManagerSingleton.get().onGamePlayEnded(score);
                hide();
                Toast.makeText(GameMaster.getContext(), (int) R.string.Submitting, 1).show();
            }

            public boolean isInArea(Vector2DF point) {
                return getVisibleOriginalValue() && point.isIn(PuzzleScene.this.SubmitScoreButtonRect);
            }

            public void reset() {
            }
        };
        this.SubmitScoreButton.getImage().loadImageFromResource(c, R.drawable.submitscore);
        this.SubmitScoreButton.setLevel(21.0f);
        this.SubmitScoreButton.setIsUseAbsolutePosition(true);
        this.SubmitScoreButton.getSize().reset(114.0f, 40.0f);
        getSpiritList().add(this.SubmitScoreButton);
        this.SubmitScoreButton.getPosition().reset(15.0f, 60.0f);
        this.SubmitScoreButtonRect = new VectorRect2DF(this.SubmitScoreButton.getPosition(), this.SubmitScoreButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton.hide();
        this.Time = new GameSpirit() {
            DecimalFormat FMT = new DecimalFormat("00");
            Paint p1;
            Paint p2;

            public GameObject getDisplayContentChild() {
                return null;
            }

            /* access modifiers changed from: protected */
            public void drawSelf(Canvas c, Rect drawArea) {
                if (this.p1 == null) {
                    this.p1 = new Paint();
                    this.p1.setARGB(160, 255, 255, 255);
                    this.p1.setAntiAlias(true);
                    this.p1.setTextSize(28.0f);
                    this.p1.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                if (this.p2 == null) {
                    this.p2 = new Paint();
                    this.p2.setARGB(100, 0, 0, 0);
                    this.p2.setAntiAlias(true);
                    this.p2.setTextSize(28.0f);
                    this.p2.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                long t = PuzzleScene.this.getIsFinished() ? PuzzleScene.this.PastTime : PuzzleScene.this.PastTime + (new Date().getTime() - PuzzleScene.this.StartTime);
                long h = t / 3600000;
                long m = (t - (((1000 * h) * 60) * 60)) / 60000;
                String str = String.valueOf(this.FMT.format(h)) + ":" + this.FMT.format(m) + ":" + this.FMT.format(((t - (((1000 * h) * 60) * 60)) - ((1000 * m) * 60)) / 1000) + (PuzzleScene.this.getIsFinished() ? " Finished" : "");
                c.drawText(str, 16.0f, 44.0f, this.p2);
                c.drawText(str, 15.0f, 43.0f, this.p1);
                super.drawSelf(c, drawArea);
            }
        };
        this.Time.setLevel(40.0f);
        getSpiritList().add(this.Time);
    }

    public float getMaxPuzzleLevel() {
        float maxlvl = 0.0f;
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getLevel() <= 10.0f) {
                maxlvl = Math.max(f.getLevel(), maxlvl);
            }
        }
        return maxlvl;
    }

    public Puzzle getTouchPuzzle(Vector2DF v) {
        Vector2DF rv = this.Desktop.reMapPoint(v);
        Puzzle o = null;
        float olen = 99999.0f;
        for (int i = getSpiritList().size() - 1; i >= 0; i--) {
            if (getSpiritList().get(i) instanceof Puzzle) {
                Puzzle p = (Puzzle) getSpiritList().get(i);
                float len = p.getPositionInDesktop().minusNew(rv).getLength();
                if (len <= Puzzle.getRealitySize().getX() * 0.4f) {
                    return p;
                }
                if (len <= Puzzle.getRealitySize().getX() && olen > len) {
                    o = p;
                    olen = len;
                }
            }
        }
        return o;
    }

    public void startDrawSelectRect(Vector2DF startDragPoint, Vector2DF v) {
        this.selectRect = new VectorRect2DF(startDragPoint, v.minusNew(startDragPoint)).getFixedRectF();
    }

    public void stopDrawSelectRect() {
        this.selectRect = null;
    }

    public ArrayList<Puzzle> getSelectPuzzle(Vector2DF startDragPoint, Vector2DF v) {
        Vector2DF rs = this.Desktop.reMapPoint(startDragPoint);
        ArrayList<Puzzle> l = new ArrayList<>();
        RectF rect = new VectorRect2DF(rs, this.Desktop.reMapPoint(v).minusNew(rs)).getFixedRectF();
        float xo = Puzzle.getRealitySize().getX() / 3.0f;
        float yo = Puzzle.getRealitySize().getY() / 3.0f;
        RectF sr = new RectF(rect.left - xo, rect.top - yo, rect.right + xo, rect.bottom + yo);
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getPositionInDesktop().isIn(sr)) {
                l.add(f);
            }
        }
        return l;
    }

    public void loadPuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences s = c.getSharedPreferences();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().load(c, s);
        }
    }

    public void savePuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences.Editor e = c.getSharedPreferences().edit();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().save(c, e);
        }
        e.commit();
    }

    public void reset() {
        this.Controller.reset();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            f.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
            this.Desktop.RandomlyPlaced(f);
        }
        refreshDrawCacheBitmap();
        setIsFinishedToDefault();
        this.StartTime = new Date().getTime();
        this.PastTime = 0;
        this.SubmitScoreButton.hide();
        saveGameState();
        savePuzzleState();
    }

    /* access modifiers changed from: package-private */
    public void c1395440640(Context c) {
        Puzzle p1395440640 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1395440640(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1395440640(editor, this);
            }
        };
        p1395440640.setID(1395440640);
        p1395440640.setName("1395440640");
        p1395440640.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1395440640);
        this.Desktop.RandomlyPlaced(p1395440640);
        p1395440640.setTopEdgeType(EdgeType.Flat);
        p1395440640.setBottomEdgeType(EdgeType.Convex);
        p1395440640.setLeftEdgeType(EdgeType.Flat);
        p1395440640.setRightEdgeType(EdgeType.Convex);
        p1395440640.setTopExactPuzzleID(-1);
        p1395440640.setBottomExactPuzzleID(1718568215);
        p1395440640.setLeftExactPuzzleID(-1);
        p1395440640.setRightExactPuzzleID(374208536);
        p1395440640.getDisplayImage().loadImageFromResource(c, R.drawable.p1395440640h);
        p1395440640.setExactRow(0);
        p1395440640.setExactColumn(0);
        p1395440640.getSize().reset(126.6667f, 126.6667f);
        p1395440640.getPositionOffset().reset(-50.0f, -50.0f);
        p1395440640.setIsUseAbsolutePosition(true);
        p1395440640.setIsUseAbsoluteSize(true);
        p1395440640.getImage().setIsUseAbsolutePosition(true);
        p1395440640.getImage().setIsUseAbsoluteSize(true);
        p1395440640.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1395440640.resetPosition();
        getSpiritList().add(p1395440640);
    }

    /* access modifiers changed from: package-private */
    public void c1718568215(Context c) {
        Puzzle p1718568215 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1718568215(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1718568215(editor, this);
            }
        };
        p1718568215.setID(1718568215);
        p1718568215.setName("1718568215");
        p1718568215.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1718568215);
        this.Desktop.RandomlyPlaced(p1718568215);
        p1718568215.setTopEdgeType(EdgeType.Concave);
        p1718568215.setBottomEdgeType(EdgeType.Concave);
        p1718568215.setLeftEdgeType(EdgeType.Flat);
        p1718568215.setRightEdgeType(EdgeType.Convex);
        p1718568215.setTopExactPuzzleID(1395440640);
        p1718568215.setBottomExactPuzzleID(1566004604);
        p1718568215.setLeftExactPuzzleID(-1);
        p1718568215.setRightExactPuzzleID(868798569);
        p1718568215.getDisplayImage().loadImageFromResource(c, R.drawable.p1718568215h);
        p1718568215.setExactRow(1);
        p1718568215.setExactColumn(0);
        p1718568215.getSize().reset(126.6667f, 100.0f);
        p1718568215.getPositionOffset().reset(-50.0f, -50.0f);
        p1718568215.setIsUseAbsolutePosition(true);
        p1718568215.setIsUseAbsoluteSize(true);
        p1718568215.getImage().setIsUseAbsolutePosition(true);
        p1718568215.getImage().setIsUseAbsoluteSize(true);
        p1718568215.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1718568215.resetPosition();
        getSpiritList().add(p1718568215);
    }

    /* access modifiers changed from: package-private */
    public void c1566004604(Context c) {
        Puzzle p1566004604 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1566004604(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1566004604(editor, this);
            }
        };
        p1566004604.setID(1566004604);
        p1566004604.setName("1566004604");
        p1566004604.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1566004604);
        this.Desktop.RandomlyPlaced(p1566004604);
        p1566004604.setTopEdgeType(EdgeType.Convex);
        p1566004604.setBottomEdgeType(EdgeType.Convex);
        p1566004604.setLeftEdgeType(EdgeType.Flat);
        p1566004604.setRightEdgeType(EdgeType.Concave);
        p1566004604.setTopExactPuzzleID(1718568215);
        p1566004604.setBottomExactPuzzleID(1813046260);
        p1566004604.setLeftExactPuzzleID(-1);
        p1566004604.setRightExactPuzzleID(191758053);
        p1566004604.getDisplayImage().loadImageFromResource(c, R.drawable.p1566004604h);
        p1566004604.setExactRow(2);
        p1566004604.setExactColumn(0);
        p1566004604.getSize().reset(100.0f, 153.3333f);
        p1566004604.getPositionOffset().reset(-50.0f, -76.66666f);
        p1566004604.setIsUseAbsolutePosition(true);
        p1566004604.setIsUseAbsoluteSize(true);
        p1566004604.getImage().setIsUseAbsolutePosition(true);
        p1566004604.getImage().setIsUseAbsoluteSize(true);
        p1566004604.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1566004604.resetPosition();
        getSpiritList().add(p1566004604);
    }

    /* access modifiers changed from: package-private */
    public void c1813046260(Context c) {
        Puzzle p1813046260 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1813046260(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1813046260(editor, this);
            }
        };
        p1813046260.setID(1813046260);
        p1813046260.setName("1813046260");
        p1813046260.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1813046260);
        this.Desktop.RandomlyPlaced(p1813046260);
        p1813046260.setTopEdgeType(EdgeType.Concave);
        p1813046260.setBottomEdgeType(EdgeType.Convex);
        p1813046260.setLeftEdgeType(EdgeType.Flat);
        p1813046260.setRightEdgeType(EdgeType.Convex);
        p1813046260.setTopExactPuzzleID(1566004604);
        p1813046260.setBottomExactPuzzleID(1281467703);
        p1813046260.setLeftExactPuzzleID(-1);
        p1813046260.setRightExactPuzzleID(538176265);
        p1813046260.getDisplayImage().loadImageFromResource(c, R.drawable.p1813046260h);
        p1813046260.setExactRow(3);
        p1813046260.setExactColumn(0);
        p1813046260.getSize().reset(126.6667f, 126.6667f);
        p1813046260.getPositionOffset().reset(-50.0f, -50.0f);
        p1813046260.setIsUseAbsolutePosition(true);
        p1813046260.setIsUseAbsoluteSize(true);
        p1813046260.getImage().setIsUseAbsolutePosition(true);
        p1813046260.getImage().setIsUseAbsoluteSize(true);
        p1813046260.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1813046260.resetPosition();
        getSpiritList().add(p1813046260);
    }

    /* access modifiers changed from: package-private */
    public void c1281467703(Context c) {
        Puzzle p1281467703 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1281467703(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1281467703(editor, this);
            }
        };
        p1281467703.setID(1281467703);
        p1281467703.setName("1281467703");
        p1281467703.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1281467703);
        this.Desktop.RandomlyPlaced(p1281467703);
        p1281467703.setTopEdgeType(EdgeType.Concave);
        p1281467703.setBottomEdgeType(EdgeType.Convex);
        p1281467703.setLeftEdgeType(EdgeType.Flat);
        p1281467703.setRightEdgeType(EdgeType.Convex);
        p1281467703.setTopExactPuzzleID(1813046260);
        p1281467703.setBottomExactPuzzleID(1084465376);
        p1281467703.setLeftExactPuzzleID(-1);
        p1281467703.setRightExactPuzzleID(650989099);
        p1281467703.getDisplayImage().loadImageFromResource(c, R.drawable.p1281467703h);
        p1281467703.setExactRow(4);
        p1281467703.setExactColumn(0);
        p1281467703.getSize().reset(126.6667f, 126.6667f);
        p1281467703.getPositionOffset().reset(-50.0f, -50.0f);
        p1281467703.setIsUseAbsolutePosition(true);
        p1281467703.setIsUseAbsoluteSize(true);
        p1281467703.getImage().setIsUseAbsolutePosition(true);
        p1281467703.getImage().setIsUseAbsoluteSize(true);
        p1281467703.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1281467703.resetPosition();
        getSpiritList().add(p1281467703);
    }

    /* access modifiers changed from: package-private */
    public void c1084465376(Context c) {
        Puzzle p1084465376 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1084465376(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1084465376(editor, this);
            }
        };
        p1084465376.setID(1084465376);
        p1084465376.setName("1084465376");
        p1084465376.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1084465376);
        this.Desktop.RandomlyPlaced(p1084465376);
        p1084465376.setTopEdgeType(EdgeType.Concave);
        p1084465376.setBottomEdgeType(EdgeType.Flat);
        p1084465376.setLeftEdgeType(EdgeType.Flat);
        p1084465376.setRightEdgeType(EdgeType.Convex);
        p1084465376.setTopExactPuzzleID(1281467703);
        p1084465376.setBottomExactPuzzleID(-1);
        p1084465376.setLeftExactPuzzleID(-1);
        p1084465376.setRightExactPuzzleID(2072784504);
        p1084465376.getDisplayImage().loadImageFromResource(c, R.drawable.p1084465376h);
        p1084465376.setExactRow(5);
        p1084465376.setExactColumn(0);
        p1084465376.getSize().reset(126.6667f, 100.0f);
        p1084465376.getPositionOffset().reset(-50.0f, -50.0f);
        p1084465376.setIsUseAbsolutePosition(true);
        p1084465376.setIsUseAbsoluteSize(true);
        p1084465376.getImage().setIsUseAbsolutePosition(true);
        p1084465376.getImage().setIsUseAbsoluteSize(true);
        p1084465376.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1084465376.resetPosition();
        getSpiritList().add(p1084465376);
    }

    /* access modifiers changed from: package-private */
    public void c374208536(Context c) {
        Puzzle p374208536 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load374208536(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save374208536(editor, this);
            }
        };
        p374208536.setID(374208536);
        p374208536.setName("374208536");
        p374208536.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p374208536);
        this.Desktop.RandomlyPlaced(p374208536);
        p374208536.setTopEdgeType(EdgeType.Flat);
        p374208536.setBottomEdgeType(EdgeType.Concave);
        p374208536.setLeftEdgeType(EdgeType.Concave);
        p374208536.setRightEdgeType(EdgeType.Concave);
        p374208536.setTopExactPuzzleID(-1);
        p374208536.setBottomExactPuzzleID(868798569);
        p374208536.setLeftExactPuzzleID(1395440640);
        p374208536.setRightExactPuzzleID(2107762398);
        p374208536.getDisplayImage().loadImageFromResource(c, R.drawable.p374208536h);
        p374208536.setExactRow(0);
        p374208536.setExactColumn(1);
        p374208536.getSize().reset(100.0f, 100.0f);
        p374208536.getPositionOffset().reset(-50.0f, -50.0f);
        p374208536.setIsUseAbsolutePosition(true);
        p374208536.setIsUseAbsoluteSize(true);
        p374208536.getImage().setIsUseAbsolutePosition(true);
        p374208536.getImage().setIsUseAbsoluteSize(true);
        p374208536.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p374208536.resetPosition();
        getSpiritList().add(p374208536);
    }

    /* access modifiers changed from: package-private */
    public void c868798569(Context c) {
        Puzzle p868798569 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load868798569(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save868798569(editor, this);
            }
        };
        p868798569.setID(868798569);
        p868798569.setName("868798569");
        p868798569.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p868798569);
        this.Desktop.RandomlyPlaced(p868798569);
        p868798569.setTopEdgeType(EdgeType.Convex);
        p868798569.setBottomEdgeType(EdgeType.Concave);
        p868798569.setLeftEdgeType(EdgeType.Concave);
        p868798569.setRightEdgeType(EdgeType.Concave);
        p868798569.setTopExactPuzzleID(374208536);
        p868798569.setBottomExactPuzzleID(191758053);
        p868798569.setLeftExactPuzzleID(1718568215);
        p868798569.setRightExactPuzzleID(1861710961);
        p868798569.getDisplayImage().loadImageFromResource(c, R.drawable.p868798569h);
        p868798569.setExactRow(1);
        p868798569.setExactColumn(1);
        p868798569.getSize().reset(100.0f, 126.6667f);
        p868798569.getPositionOffset().reset(-50.0f, -76.66666f);
        p868798569.setIsUseAbsolutePosition(true);
        p868798569.setIsUseAbsoluteSize(true);
        p868798569.getImage().setIsUseAbsolutePosition(true);
        p868798569.getImage().setIsUseAbsoluteSize(true);
        p868798569.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p868798569.resetPosition();
        getSpiritList().add(p868798569);
    }

    /* access modifiers changed from: package-private */
    public void c191758053(Context c) {
        Puzzle p191758053 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load191758053(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save191758053(editor, this);
            }
        };
        p191758053.setID(191758053);
        p191758053.setName("191758053");
        p191758053.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p191758053);
        this.Desktop.RandomlyPlaced(p191758053);
        p191758053.setTopEdgeType(EdgeType.Convex);
        p191758053.setBottomEdgeType(EdgeType.Concave);
        p191758053.setLeftEdgeType(EdgeType.Convex);
        p191758053.setRightEdgeType(EdgeType.Concave);
        p191758053.setTopExactPuzzleID(868798569);
        p191758053.setBottomExactPuzzleID(538176265);
        p191758053.setLeftExactPuzzleID(1566004604);
        p191758053.setRightExactPuzzleID(180976878);
        p191758053.getDisplayImage().loadImageFromResource(c, R.drawable.p191758053h);
        p191758053.setExactRow(2);
        p191758053.setExactColumn(1);
        p191758053.getSize().reset(126.6667f, 126.6667f);
        p191758053.getPositionOffset().reset(-76.66666f, -76.66666f);
        p191758053.setIsUseAbsolutePosition(true);
        p191758053.setIsUseAbsoluteSize(true);
        p191758053.getImage().setIsUseAbsolutePosition(true);
        p191758053.getImage().setIsUseAbsoluteSize(true);
        p191758053.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p191758053.resetPosition();
        getSpiritList().add(p191758053);
    }

    /* access modifiers changed from: package-private */
    public void c538176265(Context c) {
        Puzzle p538176265 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load538176265(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save538176265(editor, this);
            }
        };
        p538176265.setID(538176265);
        p538176265.setName("538176265");
        p538176265.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p538176265);
        this.Desktop.RandomlyPlaced(p538176265);
        p538176265.setTopEdgeType(EdgeType.Convex);
        p538176265.setBottomEdgeType(EdgeType.Convex);
        p538176265.setLeftEdgeType(EdgeType.Concave);
        p538176265.setRightEdgeType(EdgeType.Concave);
        p538176265.setTopExactPuzzleID(191758053);
        p538176265.setBottomExactPuzzleID(650989099);
        p538176265.setLeftExactPuzzleID(1813046260);
        p538176265.setRightExactPuzzleID(1626761966);
        p538176265.getDisplayImage().loadImageFromResource(c, R.drawable.p538176265h);
        p538176265.setExactRow(3);
        p538176265.setExactColumn(1);
        p538176265.getSize().reset(100.0f, 153.3333f);
        p538176265.getPositionOffset().reset(-50.0f, -76.66666f);
        p538176265.setIsUseAbsolutePosition(true);
        p538176265.setIsUseAbsoluteSize(true);
        p538176265.getImage().setIsUseAbsolutePosition(true);
        p538176265.getImage().setIsUseAbsoluteSize(true);
        p538176265.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p538176265.resetPosition();
        getSpiritList().add(p538176265);
    }

    /* access modifiers changed from: package-private */
    public void c650989099(Context c) {
        Puzzle p650989099 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load650989099(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save650989099(editor, this);
            }
        };
        p650989099.setID(650989099);
        p650989099.setName("650989099");
        p650989099.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p650989099);
        this.Desktop.RandomlyPlaced(p650989099);
        p650989099.setTopEdgeType(EdgeType.Concave);
        p650989099.setBottomEdgeType(EdgeType.Concave);
        p650989099.setLeftEdgeType(EdgeType.Concave);
        p650989099.setRightEdgeType(EdgeType.Convex);
        p650989099.setTopExactPuzzleID(538176265);
        p650989099.setBottomExactPuzzleID(2072784504);
        p650989099.setLeftExactPuzzleID(1281467703);
        p650989099.setRightExactPuzzleID(1192911393);
        p650989099.getDisplayImage().loadImageFromResource(c, R.drawable.p650989099h);
        p650989099.setExactRow(4);
        p650989099.setExactColumn(1);
        p650989099.getSize().reset(126.6667f, 100.0f);
        p650989099.getPositionOffset().reset(-50.0f, -50.0f);
        p650989099.setIsUseAbsolutePosition(true);
        p650989099.setIsUseAbsoluteSize(true);
        p650989099.getImage().setIsUseAbsolutePosition(true);
        p650989099.getImage().setIsUseAbsoluteSize(true);
        p650989099.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p650989099.resetPosition();
        getSpiritList().add(p650989099);
    }

    /* access modifiers changed from: package-private */
    public void c2072784504(Context c) {
        Puzzle p2072784504 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2072784504(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2072784504(editor, this);
            }
        };
        p2072784504.setID(2072784504);
        p2072784504.setName("2072784504");
        p2072784504.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2072784504);
        this.Desktop.RandomlyPlaced(p2072784504);
        p2072784504.setTopEdgeType(EdgeType.Convex);
        p2072784504.setBottomEdgeType(EdgeType.Flat);
        p2072784504.setLeftEdgeType(EdgeType.Concave);
        p2072784504.setRightEdgeType(EdgeType.Concave);
        p2072784504.setTopExactPuzzleID(650989099);
        p2072784504.setBottomExactPuzzleID(-1);
        p2072784504.setLeftExactPuzzleID(1084465376);
        p2072784504.setRightExactPuzzleID(771831799);
        p2072784504.getDisplayImage().loadImageFromResource(c, R.drawable.p2072784504h);
        p2072784504.setExactRow(5);
        p2072784504.setExactColumn(1);
        p2072784504.getSize().reset(100.0f, 126.6667f);
        p2072784504.getPositionOffset().reset(-50.0f, -76.66666f);
        p2072784504.setIsUseAbsolutePosition(true);
        p2072784504.setIsUseAbsoluteSize(true);
        p2072784504.getImage().setIsUseAbsolutePosition(true);
        p2072784504.getImage().setIsUseAbsoluteSize(true);
        p2072784504.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2072784504.resetPosition();
        getSpiritList().add(p2072784504);
    }

    /* access modifiers changed from: package-private */
    public void c2107762398(Context c) {
        Puzzle p2107762398 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2107762398(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2107762398(editor, this);
            }
        };
        p2107762398.setID(2107762398);
        p2107762398.setName("2107762398");
        p2107762398.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2107762398);
        this.Desktop.RandomlyPlaced(p2107762398);
        p2107762398.setTopEdgeType(EdgeType.Flat);
        p2107762398.setBottomEdgeType(EdgeType.Convex);
        p2107762398.setLeftEdgeType(EdgeType.Convex);
        p2107762398.setRightEdgeType(EdgeType.Convex);
        p2107762398.setTopExactPuzzleID(-1);
        p2107762398.setBottomExactPuzzleID(1861710961);
        p2107762398.setLeftExactPuzzleID(374208536);
        p2107762398.setRightExactPuzzleID(1144198189);
        p2107762398.getDisplayImage().loadImageFromResource(c, R.drawable.p2107762398h);
        p2107762398.setExactRow(0);
        p2107762398.setExactColumn(2);
        p2107762398.getSize().reset(153.3333f, 126.6667f);
        p2107762398.getPositionOffset().reset(-76.66666f, -50.0f);
        p2107762398.setIsUseAbsolutePosition(true);
        p2107762398.setIsUseAbsoluteSize(true);
        p2107762398.getImage().setIsUseAbsolutePosition(true);
        p2107762398.getImage().setIsUseAbsoluteSize(true);
        p2107762398.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2107762398.resetPosition();
        getSpiritList().add(p2107762398);
    }

    /* access modifiers changed from: package-private */
    public void c1861710961(Context c) {
        Puzzle p1861710961 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1861710961(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1861710961(editor, this);
            }
        };
        p1861710961.setID(1861710961);
        p1861710961.setName("1861710961");
        p1861710961.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1861710961);
        this.Desktop.RandomlyPlaced(p1861710961);
        p1861710961.setTopEdgeType(EdgeType.Concave);
        p1861710961.setBottomEdgeType(EdgeType.Concave);
        p1861710961.setLeftEdgeType(EdgeType.Convex);
        p1861710961.setRightEdgeType(EdgeType.Concave);
        p1861710961.setTopExactPuzzleID(2107762398);
        p1861710961.setBottomExactPuzzleID(180976878);
        p1861710961.setLeftExactPuzzleID(868798569);
        p1861710961.setRightExactPuzzleID(1523076608);
        p1861710961.getDisplayImage().loadImageFromResource(c, R.drawable.p1861710961h);
        p1861710961.setExactRow(1);
        p1861710961.setExactColumn(2);
        p1861710961.getSize().reset(126.6667f, 100.0f);
        p1861710961.getPositionOffset().reset(-76.66666f, -50.0f);
        p1861710961.setIsUseAbsolutePosition(true);
        p1861710961.setIsUseAbsoluteSize(true);
        p1861710961.getImage().setIsUseAbsolutePosition(true);
        p1861710961.getImage().setIsUseAbsoluteSize(true);
        p1861710961.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1861710961.resetPosition();
        getSpiritList().add(p1861710961);
    }

    /* access modifiers changed from: package-private */
    public void c180976878(Context c) {
        Puzzle p180976878 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load180976878(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save180976878(editor, this);
            }
        };
        p180976878.setID(180976878);
        p180976878.setName("180976878");
        p180976878.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p180976878);
        this.Desktop.RandomlyPlaced(p180976878);
        p180976878.setTopEdgeType(EdgeType.Convex);
        p180976878.setBottomEdgeType(EdgeType.Convex);
        p180976878.setLeftEdgeType(EdgeType.Convex);
        p180976878.setRightEdgeType(EdgeType.Concave);
        p180976878.setTopExactPuzzleID(1861710961);
        p180976878.setBottomExactPuzzleID(1626761966);
        p180976878.setLeftExactPuzzleID(191758053);
        p180976878.setRightExactPuzzleID(1785268698);
        p180976878.getDisplayImage().loadImageFromResource(c, R.drawable.p180976878h);
        p180976878.setExactRow(2);
        p180976878.setExactColumn(2);
        p180976878.getSize().reset(126.6667f, 153.3333f);
        p180976878.getPositionOffset().reset(-76.66666f, -76.66666f);
        p180976878.setIsUseAbsolutePosition(true);
        p180976878.setIsUseAbsoluteSize(true);
        p180976878.getImage().setIsUseAbsolutePosition(true);
        p180976878.getImage().setIsUseAbsoluteSize(true);
        p180976878.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p180976878.resetPosition();
        getSpiritList().add(p180976878);
    }

    /* access modifiers changed from: package-private */
    public void c1626761966(Context c) {
        Puzzle p1626761966 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1626761966(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1626761966(editor, this);
            }
        };
        p1626761966.setID(1626761966);
        p1626761966.setName("1626761966");
        p1626761966.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1626761966);
        this.Desktop.RandomlyPlaced(p1626761966);
        p1626761966.setTopEdgeType(EdgeType.Concave);
        p1626761966.setBottomEdgeType(EdgeType.Convex);
        p1626761966.setLeftEdgeType(EdgeType.Convex);
        p1626761966.setRightEdgeType(EdgeType.Concave);
        p1626761966.setTopExactPuzzleID(180976878);
        p1626761966.setBottomExactPuzzleID(1192911393);
        p1626761966.setLeftExactPuzzleID(538176265);
        p1626761966.setRightExactPuzzleID(2074854323);
        p1626761966.getDisplayImage().loadImageFromResource(c, R.drawable.p1626761966h);
        p1626761966.setExactRow(3);
        p1626761966.setExactColumn(2);
        p1626761966.getSize().reset(126.6667f, 126.6667f);
        p1626761966.getPositionOffset().reset(-76.66666f, -50.0f);
        p1626761966.setIsUseAbsolutePosition(true);
        p1626761966.setIsUseAbsoluteSize(true);
        p1626761966.getImage().setIsUseAbsolutePosition(true);
        p1626761966.getImage().setIsUseAbsoluteSize(true);
        p1626761966.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1626761966.resetPosition();
        getSpiritList().add(p1626761966);
    }

    /* access modifiers changed from: package-private */
    public void c1192911393(Context c) {
        Puzzle p1192911393 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1192911393(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1192911393(editor, this);
            }
        };
        p1192911393.setID(1192911393);
        p1192911393.setName("1192911393");
        p1192911393.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1192911393);
        this.Desktop.RandomlyPlaced(p1192911393);
        p1192911393.setTopEdgeType(EdgeType.Concave);
        p1192911393.setBottomEdgeType(EdgeType.Concave);
        p1192911393.setLeftEdgeType(EdgeType.Concave);
        p1192911393.setRightEdgeType(EdgeType.Concave);
        p1192911393.setTopExactPuzzleID(1626761966);
        p1192911393.setBottomExactPuzzleID(771831799);
        p1192911393.setLeftExactPuzzleID(650989099);
        p1192911393.setRightExactPuzzleID(1807296321);
        p1192911393.getDisplayImage().loadImageFromResource(c, R.drawable.p1192911393h);
        p1192911393.setExactRow(4);
        p1192911393.setExactColumn(2);
        p1192911393.getSize().reset(100.0f, 100.0f);
        p1192911393.getPositionOffset().reset(-50.0f, -50.0f);
        p1192911393.setIsUseAbsolutePosition(true);
        p1192911393.setIsUseAbsoluteSize(true);
        p1192911393.getImage().setIsUseAbsolutePosition(true);
        p1192911393.getImage().setIsUseAbsoluteSize(true);
        p1192911393.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1192911393.resetPosition();
        getSpiritList().add(p1192911393);
    }

    /* access modifiers changed from: package-private */
    public void c771831799(Context c) {
        Puzzle p771831799 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load771831799(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save771831799(editor, this);
            }
        };
        p771831799.setID(771831799);
        p771831799.setName("771831799");
        p771831799.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p771831799);
        this.Desktop.RandomlyPlaced(p771831799);
        p771831799.setTopEdgeType(EdgeType.Convex);
        p771831799.setBottomEdgeType(EdgeType.Flat);
        p771831799.setLeftEdgeType(EdgeType.Convex);
        p771831799.setRightEdgeType(EdgeType.Convex);
        p771831799.setTopExactPuzzleID(1192911393);
        p771831799.setBottomExactPuzzleID(-1);
        p771831799.setLeftExactPuzzleID(2072784504);
        p771831799.setRightExactPuzzleID(391079276);
        p771831799.getDisplayImage().loadImageFromResource(c, R.drawable.p771831799h);
        p771831799.setExactRow(5);
        p771831799.setExactColumn(2);
        p771831799.getSize().reset(153.3333f, 126.6667f);
        p771831799.getPositionOffset().reset(-76.66666f, -76.66666f);
        p771831799.setIsUseAbsolutePosition(true);
        p771831799.setIsUseAbsoluteSize(true);
        p771831799.getImage().setIsUseAbsolutePosition(true);
        p771831799.getImage().setIsUseAbsoluteSize(true);
        p771831799.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p771831799.resetPosition();
        getSpiritList().add(p771831799);
    }

    /* access modifiers changed from: package-private */
    public void c1144198189(Context c) {
        Puzzle p1144198189 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1144198189(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1144198189(editor, this);
            }
        };
        p1144198189.setID(1144198189);
        p1144198189.setName("1144198189");
        p1144198189.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1144198189);
        this.Desktop.RandomlyPlaced(p1144198189);
        p1144198189.setTopEdgeType(EdgeType.Flat);
        p1144198189.setBottomEdgeType(EdgeType.Concave);
        p1144198189.setLeftEdgeType(EdgeType.Concave);
        p1144198189.setRightEdgeType(EdgeType.Convex);
        p1144198189.setTopExactPuzzleID(-1);
        p1144198189.setBottomExactPuzzleID(1523076608);
        p1144198189.setLeftExactPuzzleID(2107762398);
        p1144198189.setRightExactPuzzleID(1000323591);
        p1144198189.getDisplayImage().loadImageFromResource(c, R.drawable.p1144198189h);
        p1144198189.setExactRow(0);
        p1144198189.setExactColumn(3);
        p1144198189.getSize().reset(126.6667f, 100.0f);
        p1144198189.getPositionOffset().reset(-50.0f, -50.0f);
        p1144198189.setIsUseAbsolutePosition(true);
        p1144198189.setIsUseAbsoluteSize(true);
        p1144198189.getImage().setIsUseAbsolutePosition(true);
        p1144198189.getImage().setIsUseAbsoluteSize(true);
        p1144198189.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1144198189.resetPosition();
        getSpiritList().add(p1144198189);
    }

    /* access modifiers changed from: package-private */
    public void c1523076608(Context c) {
        Puzzle p1523076608 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1523076608(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1523076608(editor, this);
            }
        };
        p1523076608.setID(1523076608);
        p1523076608.setName("1523076608");
        p1523076608.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1523076608);
        this.Desktop.RandomlyPlaced(p1523076608);
        p1523076608.setTopEdgeType(EdgeType.Convex);
        p1523076608.setBottomEdgeType(EdgeType.Concave);
        p1523076608.setLeftEdgeType(EdgeType.Convex);
        p1523076608.setRightEdgeType(EdgeType.Convex);
        p1523076608.setTopExactPuzzleID(1144198189);
        p1523076608.setBottomExactPuzzleID(1785268698);
        p1523076608.setLeftExactPuzzleID(1861710961);
        p1523076608.setRightExactPuzzleID(1640195765);
        p1523076608.getDisplayImage().loadImageFromResource(c, R.drawable.p1523076608h);
        p1523076608.setExactRow(1);
        p1523076608.setExactColumn(3);
        p1523076608.getSize().reset(153.3333f, 126.6667f);
        p1523076608.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1523076608.setIsUseAbsolutePosition(true);
        p1523076608.setIsUseAbsoluteSize(true);
        p1523076608.getImage().setIsUseAbsolutePosition(true);
        p1523076608.getImage().setIsUseAbsoluteSize(true);
        p1523076608.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1523076608.resetPosition();
        getSpiritList().add(p1523076608);
    }

    /* access modifiers changed from: package-private */
    public void c1785268698(Context c) {
        Puzzle p1785268698 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1785268698(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1785268698(editor, this);
            }
        };
        p1785268698.setID(1785268698);
        p1785268698.setName("1785268698");
        p1785268698.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1785268698);
        this.Desktop.RandomlyPlaced(p1785268698);
        p1785268698.setTopEdgeType(EdgeType.Convex);
        p1785268698.setBottomEdgeType(EdgeType.Concave);
        p1785268698.setLeftEdgeType(EdgeType.Convex);
        p1785268698.setRightEdgeType(EdgeType.Convex);
        p1785268698.setTopExactPuzzleID(1523076608);
        p1785268698.setBottomExactPuzzleID(2074854323);
        p1785268698.setLeftExactPuzzleID(180976878);
        p1785268698.setRightExactPuzzleID(1125358841);
        p1785268698.getDisplayImage().loadImageFromResource(c, R.drawable.p1785268698h);
        p1785268698.setExactRow(2);
        p1785268698.setExactColumn(3);
        p1785268698.getSize().reset(153.3333f, 126.6667f);
        p1785268698.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1785268698.setIsUseAbsolutePosition(true);
        p1785268698.setIsUseAbsoluteSize(true);
        p1785268698.getImage().setIsUseAbsolutePosition(true);
        p1785268698.getImage().setIsUseAbsoluteSize(true);
        p1785268698.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1785268698.resetPosition();
        getSpiritList().add(p1785268698);
    }

    /* access modifiers changed from: package-private */
    public void c2074854323(Context c) {
        Puzzle p2074854323 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2074854323(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2074854323(editor, this);
            }
        };
        p2074854323.setID(2074854323);
        p2074854323.setName("2074854323");
        p2074854323.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2074854323);
        this.Desktop.RandomlyPlaced(p2074854323);
        p2074854323.setTopEdgeType(EdgeType.Convex);
        p2074854323.setBottomEdgeType(EdgeType.Convex);
        p2074854323.setLeftEdgeType(EdgeType.Convex);
        p2074854323.setRightEdgeType(EdgeType.Convex);
        p2074854323.setTopExactPuzzleID(1785268698);
        p2074854323.setBottomExactPuzzleID(1807296321);
        p2074854323.setLeftExactPuzzleID(1626761966);
        p2074854323.setRightExactPuzzleID(1854851027);
        p2074854323.getDisplayImage().loadImageFromResource(c, R.drawable.p2074854323h);
        p2074854323.setExactRow(3);
        p2074854323.setExactColumn(3);
        p2074854323.getSize().reset(153.3333f, 153.3333f);
        p2074854323.getPositionOffset().reset(-76.66666f, -76.66666f);
        p2074854323.setIsUseAbsolutePosition(true);
        p2074854323.setIsUseAbsoluteSize(true);
        p2074854323.getImage().setIsUseAbsolutePosition(true);
        p2074854323.getImage().setIsUseAbsoluteSize(true);
        p2074854323.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2074854323.resetPosition();
        getSpiritList().add(p2074854323);
    }

    /* access modifiers changed from: package-private */
    public void c1807296321(Context c) {
        Puzzle p1807296321 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1807296321(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1807296321(editor, this);
            }
        };
        p1807296321.setID(1807296321);
        p1807296321.setName("1807296321");
        p1807296321.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1807296321);
        this.Desktop.RandomlyPlaced(p1807296321);
        p1807296321.setTopEdgeType(EdgeType.Concave);
        p1807296321.setBottomEdgeType(EdgeType.Convex);
        p1807296321.setLeftEdgeType(EdgeType.Convex);
        p1807296321.setRightEdgeType(EdgeType.Concave);
        p1807296321.setTopExactPuzzleID(2074854323);
        p1807296321.setBottomExactPuzzleID(391079276);
        p1807296321.setLeftExactPuzzleID(1192911393);
        p1807296321.setRightExactPuzzleID(1482253141);
        p1807296321.getDisplayImage().loadImageFromResource(c, R.drawable.p1807296321h);
        p1807296321.setExactRow(4);
        p1807296321.setExactColumn(3);
        p1807296321.getSize().reset(126.6667f, 126.6667f);
        p1807296321.getPositionOffset().reset(-76.66666f, -50.0f);
        p1807296321.setIsUseAbsolutePosition(true);
        p1807296321.setIsUseAbsoluteSize(true);
        p1807296321.getImage().setIsUseAbsolutePosition(true);
        p1807296321.getImage().setIsUseAbsoluteSize(true);
        p1807296321.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1807296321.resetPosition();
        getSpiritList().add(p1807296321);
    }

    /* access modifiers changed from: package-private */
    public void c391079276(Context c) {
        Puzzle p391079276 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load391079276(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save391079276(editor, this);
            }
        };
        p391079276.setID(391079276);
        p391079276.setName("391079276");
        p391079276.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p391079276);
        this.Desktop.RandomlyPlaced(p391079276);
        p391079276.setTopEdgeType(EdgeType.Concave);
        p391079276.setBottomEdgeType(EdgeType.Flat);
        p391079276.setLeftEdgeType(EdgeType.Concave);
        p391079276.setRightEdgeType(EdgeType.Convex);
        p391079276.setTopExactPuzzleID(1807296321);
        p391079276.setBottomExactPuzzleID(-1);
        p391079276.setLeftExactPuzzleID(771831799);
        p391079276.setRightExactPuzzleID(1860648131);
        p391079276.getDisplayImage().loadImageFromResource(c, R.drawable.p391079276h);
        p391079276.setExactRow(5);
        p391079276.setExactColumn(3);
        p391079276.getSize().reset(126.6667f, 100.0f);
        p391079276.getPositionOffset().reset(-50.0f, -50.0f);
        p391079276.setIsUseAbsolutePosition(true);
        p391079276.setIsUseAbsoluteSize(true);
        p391079276.getImage().setIsUseAbsolutePosition(true);
        p391079276.getImage().setIsUseAbsoluteSize(true);
        p391079276.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p391079276.resetPosition();
        getSpiritList().add(p391079276);
    }

    /* access modifiers changed from: package-private */
    public void c1000323591(Context c) {
        Puzzle p1000323591 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1000323591(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1000323591(editor, this);
            }
        };
        p1000323591.setID(1000323591);
        p1000323591.setName("1000323591");
        p1000323591.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1000323591);
        this.Desktop.RandomlyPlaced(p1000323591);
        p1000323591.setTopEdgeType(EdgeType.Flat);
        p1000323591.setBottomEdgeType(EdgeType.Convex);
        p1000323591.setLeftEdgeType(EdgeType.Concave);
        p1000323591.setRightEdgeType(EdgeType.Convex);
        p1000323591.setTopExactPuzzleID(-1);
        p1000323591.setBottomExactPuzzleID(1640195765);
        p1000323591.setLeftExactPuzzleID(1144198189);
        p1000323591.setRightExactPuzzleID(446019339);
        p1000323591.getDisplayImage().loadImageFromResource(c, R.drawable.p1000323591h);
        p1000323591.setExactRow(0);
        p1000323591.setExactColumn(4);
        p1000323591.getSize().reset(126.6667f, 126.6667f);
        p1000323591.getPositionOffset().reset(-50.0f, -50.0f);
        p1000323591.setIsUseAbsolutePosition(true);
        p1000323591.setIsUseAbsoluteSize(true);
        p1000323591.getImage().setIsUseAbsolutePosition(true);
        p1000323591.getImage().setIsUseAbsoluteSize(true);
        p1000323591.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1000323591.resetPosition();
        getSpiritList().add(p1000323591);
    }

    /* access modifiers changed from: package-private */
    public void c1640195765(Context c) {
        Puzzle p1640195765 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1640195765(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1640195765(editor, this);
            }
        };
        p1640195765.setID(1640195765);
        p1640195765.setName("1640195765");
        p1640195765.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1640195765);
        this.Desktop.RandomlyPlaced(p1640195765);
        p1640195765.setTopEdgeType(EdgeType.Concave);
        p1640195765.setBottomEdgeType(EdgeType.Concave);
        p1640195765.setLeftEdgeType(EdgeType.Concave);
        p1640195765.setRightEdgeType(EdgeType.Concave);
        p1640195765.setTopExactPuzzleID(1000323591);
        p1640195765.setBottomExactPuzzleID(1125358841);
        p1640195765.setLeftExactPuzzleID(1523076608);
        p1640195765.setRightExactPuzzleID(1146332050);
        p1640195765.getDisplayImage().loadImageFromResource(c, R.drawable.p1640195765h);
        p1640195765.setExactRow(1);
        p1640195765.setExactColumn(4);
        p1640195765.getSize().reset(100.0f, 100.0f);
        p1640195765.getPositionOffset().reset(-50.0f, -50.0f);
        p1640195765.setIsUseAbsolutePosition(true);
        p1640195765.setIsUseAbsoluteSize(true);
        p1640195765.getImage().setIsUseAbsolutePosition(true);
        p1640195765.getImage().setIsUseAbsoluteSize(true);
        p1640195765.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1640195765.resetPosition();
        getSpiritList().add(p1640195765);
    }

    /* access modifiers changed from: package-private */
    public void c1125358841(Context c) {
        Puzzle p1125358841 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1125358841(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1125358841(editor, this);
            }
        };
        p1125358841.setID(1125358841);
        p1125358841.setName("1125358841");
        p1125358841.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1125358841);
        this.Desktop.RandomlyPlaced(p1125358841);
        p1125358841.setTopEdgeType(EdgeType.Convex);
        p1125358841.setBottomEdgeType(EdgeType.Convex);
        p1125358841.setLeftEdgeType(EdgeType.Concave);
        p1125358841.setRightEdgeType(EdgeType.Convex);
        p1125358841.setTopExactPuzzleID(1640195765);
        p1125358841.setBottomExactPuzzleID(1854851027);
        p1125358841.setLeftExactPuzzleID(1785268698);
        p1125358841.setRightExactPuzzleID(105157770);
        p1125358841.getDisplayImage().loadImageFromResource(c, R.drawable.p1125358841h);
        p1125358841.setExactRow(2);
        p1125358841.setExactColumn(4);
        p1125358841.getSize().reset(126.6667f, 153.3333f);
        p1125358841.getPositionOffset().reset(-50.0f, -76.66666f);
        p1125358841.setIsUseAbsolutePosition(true);
        p1125358841.setIsUseAbsoluteSize(true);
        p1125358841.getImage().setIsUseAbsolutePosition(true);
        p1125358841.getImage().setIsUseAbsoluteSize(true);
        p1125358841.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1125358841.resetPosition();
        getSpiritList().add(p1125358841);
    }

    /* access modifiers changed from: package-private */
    public void c1854851027(Context c) {
        Puzzle p1854851027 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1854851027(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1854851027(editor, this);
            }
        };
        p1854851027.setID(1854851027);
        p1854851027.setName("1854851027");
        p1854851027.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1854851027);
        this.Desktop.RandomlyPlaced(p1854851027);
        p1854851027.setTopEdgeType(EdgeType.Concave);
        p1854851027.setBottomEdgeType(EdgeType.Convex);
        p1854851027.setLeftEdgeType(EdgeType.Concave);
        p1854851027.setRightEdgeType(EdgeType.Concave);
        p1854851027.setTopExactPuzzleID(1125358841);
        p1854851027.setBottomExactPuzzleID(1482253141);
        p1854851027.setLeftExactPuzzleID(2074854323);
        p1854851027.setRightExactPuzzleID(718940536);
        p1854851027.getDisplayImage().loadImageFromResource(c, R.drawable.p1854851027h);
        p1854851027.setExactRow(3);
        p1854851027.setExactColumn(4);
        p1854851027.getSize().reset(100.0f, 126.6667f);
        p1854851027.getPositionOffset().reset(-50.0f, -50.0f);
        p1854851027.setIsUseAbsolutePosition(true);
        p1854851027.setIsUseAbsoluteSize(true);
        p1854851027.getImage().setIsUseAbsolutePosition(true);
        p1854851027.getImage().setIsUseAbsoluteSize(true);
        p1854851027.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1854851027.resetPosition();
        getSpiritList().add(p1854851027);
    }

    /* access modifiers changed from: package-private */
    public void c1482253141(Context c) {
        Puzzle p1482253141 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1482253141(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1482253141(editor, this);
            }
        };
        p1482253141.setID(1482253141);
        p1482253141.setName("1482253141");
        p1482253141.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1482253141);
        this.Desktop.RandomlyPlaced(p1482253141);
        p1482253141.setTopEdgeType(EdgeType.Concave);
        p1482253141.setBottomEdgeType(EdgeType.Concave);
        p1482253141.setLeftEdgeType(EdgeType.Convex);
        p1482253141.setRightEdgeType(EdgeType.Convex);
        p1482253141.setTopExactPuzzleID(1854851027);
        p1482253141.setBottomExactPuzzleID(1860648131);
        p1482253141.setLeftExactPuzzleID(1807296321);
        p1482253141.setRightExactPuzzleID(1245263572);
        p1482253141.getDisplayImage().loadImageFromResource(c, R.drawable.p1482253141h);
        p1482253141.setExactRow(4);
        p1482253141.setExactColumn(4);
        p1482253141.getSize().reset(153.3333f, 100.0f);
        p1482253141.getPositionOffset().reset(-76.66666f, -50.0f);
        p1482253141.setIsUseAbsolutePosition(true);
        p1482253141.setIsUseAbsoluteSize(true);
        p1482253141.getImage().setIsUseAbsolutePosition(true);
        p1482253141.getImage().setIsUseAbsoluteSize(true);
        p1482253141.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1482253141.resetPosition();
        getSpiritList().add(p1482253141);
    }

    /* access modifiers changed from: package-private */
    public void c1860648131(Context c) {
        Puzzle p1860648131 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1860648131(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1860648131(editor, this);
            }
        };
        p1860648131.setID(1860648131);
        p1860648131.setName("1860648131");
        p1860648131.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1860648131);
        this.Desktop.RandomlyPlaced(p1860648131);
        p1860648131.setTopEdgeType(EdgeType.Convex);
        p1860648131.setBottomEdgeType(EdgeType.Flat);
        p1860648131.setLeftEdgeType(EdgeType.Concave);
        p1860648131.setRightEdgeType(EdgeType.Convex);
        p1860648131.setTopExactPuzzleID(1482253141);
        p1860648131.setBottomExactPuzzleID(-1);
        p1860648131.setLeftExactPuzzleID(391079276);
        p1860648131.setRightExactPuzzleID(1654792066);
        p1860648131.getDisplayImage().loadImageFromResource(c, R.drawable.p1860648131h);
        p1860648131.setExactRow(5);
        p1860648131.setExactColumn(4);
        p1860648131.getSize().reset(126.6667f, 126.6667f);
        p1860648131.getPositionOffset().reset(-50.0f, -76.66666f);
        p1860648131.setIsUseAbsolutePosition(true);
        p1860648131.setIsUseAbsoluteSize(true);
        p1860648131.getImage().setIsUseAbsolutePosition(true);
        p1860648131.getImage().setIsUseAbsoluteSize(true);
        p1860648131.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1860648131.resetPosition();
        getSpiritList().add(p1860648131);
    }

    /* access modifiers changed from: package-private */
    public void c446019339(Context c) {
        Puzzle p446019339 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load446019339(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save446019339(editor, this);
            }
        };
        p446019339.setID(446019339);
        p446019339.setName("446019339");
        p446019339.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p446019339);
        this.Desktop.RandomlyPlaced(p446019339);
        p446019339.setTopEdgeType(EdgeType.Flat);
        p446019339.setBottomEdgeType(EdgeType.Convex);
        p446019339.setLeftEdgeType(EdgeType.Concave);
        p446019339.setRightEdgeType(EdgeType.Convex);
        p446019339.setTopExactPuzzleID(-1);
        p446019339.setBottomExactPuzzleID(1146332050);
        p446019339.setLeftExactPuzzleID(1000323591);
        p446019339.setRightExactPuzzleID(1596080406);
        p446019339.getDisplayImage().loadImageFromResource(c, R.drawable.p446019339h);
        p446019339.setExactRow(0);
        p446019339.setExactColumn(5);
        p446019339.getSize().reset(126.6667f, 126.6667f);
        p446019339.getPositionOffset().reset(-50.0f, -50.0f);
        p446019339.setIsUseAbsolutePosition(true);
        p446019339.setIsUseAbsoluteSize(true);
        p446019339.getImage().setIsUseAbsolutePosition(true);
        p446019339.getImage().setIsUseAbsoluteSize(true);
        p446019339.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p446019339.resetPosition();
        getSpiritList().add(p446019339);
    }

    /* access modifiers changed from: package-private */
    public void c1146332050(Context c) {
        Puzzle p1146332050 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1146332050(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1146332050(editor, this);
            }
        };
        p1146332050.setID(1146332050);
        p1146332050.setName("1146332050");
        p1146332050.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1146332050);
        this.Desktop.RandomlyPlaced(p1146332050);
        p1146332050.setTopEdgeType(EdgeType.Concave);
        p1146332050.setBottomEdgeType(EdgeType.Convex);
        p1146332050.setLeftEdgeType(EdgeType.Convex);
        p1146332050.setRightEdgeType(EdgeType.Concave);
        p1146332050.setTopExactPuzzleID(446019339);
        p1146332050.setBottomExactPuzzleID(105157770);
        p1146332050.setLeftExactPuzzleID(1640195765);
        p1146332050.setRightExactPuzzleID(322042350);
        p1146332050.getDisplayImage().loadImageFromResource(c, R.drawable.p1146332050h);
        p1146332050.setExactRow(1);
        p1146332050.setExactColumn(5);
        p1146332050.getSize().reset(126.6667f, 126.6667f);
        p1146332050.getPositionOffset().reset(-76.66666f, -50.0f);
        p1146332050.setIsUseAbsolutePosition(true);
        p1146332050.setIsUseAbsoluteSize(true);
        p1146332050.getImage().setIsUseAbsolutePosition(true);
        p1146332050.getImage().setIsUseAbsoluteSize(true);
        p1146332050.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1146332050.resetPosition();
        getSpiritList().add(p1146332050);
    }

    /* access modifiers changed from: package-private */
    public void c105157770(Context c) {
        Puzzle p105157770 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load105157770(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save105157770(editor, this);
            }
        };
        p105157770.setID(105157770);
        p105157770.setName("105157770");
        p105157770.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p105157770);
        this.Desktop.RandomlyPlaced(p105157770);
        p105157770.setTopEdgeType(EdgeType.Concave);
        p105157770.setBottomEdgeType(EdgeType.Concave);
        p105157770.setLeftEdgeType(EdgeType.Concave);
        p105157770.setRightEdgeType(EdgeType.Concave);
        p105157770.setTopExactPuzzleID(1146332050);
        p105157770.setBottomExactPuzzleID(718940536);
        p105157770.setLeftExactPuzzleID(1125358841);
        p105157770.setRightExactPuzzleID(183069723);
        p105157770.getDisplayImage().loadImageFromResource(c, R.drawable.p105157770h);
        p105157770.setExactRow(2);
        p105157770.setExactColumn(5);
        p105157770.getSize().reset(100.0f, 100.0f);
        p105157770.getPositionOffset().reset(-50.0f, -50.0f);
        p105157770.setIsUseAbsolutePosition(true);
        p105157770.setIsUseAbsoluteSize(true);
        p105157770.getImage().setIsUseAbsolutePosition(true);
        p105157770.getImage().setIsUseAbsoluteSize(true);
        p105157770.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p105157770.resetPosition();
        getSpiritList().add(p105157770);
    }

    /* access modifiers changed from: package-private */
    public void c718940536(Context c) {
        Puzzle p718940536 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load718940536(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save718940536(editor, this);
            }
        };
        p718940536.setID(718940536);
        p718940536.setName("718940536");
        p718940536.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p718940536);
        this.Desktop.RandomlyPlaced(p718940536);
        p718940536.setTopEdgeType(EdgeType.Convex);
        p718940536.setBottomEdgeType(EdgeType.Convex);
        p718940536.setLeftEdgeType(EdgeType.Convex);
        p718940536.setRightEdgeType(EdgeType.Concave);
        p718940536.setTopExactPuzzleID(105157770);
        p718940536.setBottomExactPuzzleID(1245263572);
        p718940536.setLeftExactPuzzleID(1854851027);
        p718940536.setRightExactPuzzleID(2008660439);
        p718940536.getDisplayImage().loadImageFromResource(c, R.drawable.p718940536h);
        p718940536.setExactRow(3);
        p718940536.setExactColumn(5);
        p718940536.getSize().reset(126.6667f, 153.3333f);
        p718940536.getPositionOffset().reset(-76.66666f, -76.66666f);
        p718940536.setIsUseAbsolutePosition(true);
        p718940536.setIsUseAbsoluteSize(true);
        p718940536.getImage().setIsUseAbsolutePosition(true);
        p718940536.getImage().setIsUseAbsoluteSize(true);
        p718940536.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p718940536.resetPosition();
        getSpiritList().add(p718940536);
    }

    /* access modifiers changed from: package-private */
    public void c1245263572(Context c) {
        Puzzle p1245263572 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1245263572(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1245263572(editor, this);
            }
        };
        p1245263572.setID(1245263572);
        p1245263572.setName("1245263572");
        p1245263572.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1245263572);
        this.Desktop.RandomlyPlaced(p1245263572);
        p1245263572.setTopEdgeType(EdgeType.Concave);
        p1245263572.setBottomEdgeType(EdgeType.Convex);
        p1245263572.setLeftEdgeType(EdgeType.Concave);
        p1245263572.setRightEdgeType(EdgeType.Concave);
        p1245263572.setTopExactPuzzleID(718940536);
        p1245263572.setBottomExactPuzzleID(1654792066);
        p1245263572.setLeftExactPuzzleID(1482253141);
        p1245263572.setRightExactPuzzleID(1412775374);
        p1245263572.getDisplayImage().loadImageFromResource(c, R.drawable.p1245263572h);
        p1245263572.setExactRow(4);
        p1245263572.setExactColumn(5);
        p1245263572.getSize().reset(100.0f, 126.6667f);
        p1245263572.getPositionOffset().reset(-50.0f, -50.0f);
        p1245263572.setIsUseAbsolutePosition(true);
        p1245263572.setIsUseAbsoluteSize(true);
        p1245263572.getImage().setIsUseAbsolutePosition(true);
        p1245263572.getImage().setIsUseAbsoluteSize(true);
        p1245263572.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1245263572.resetPosition();
        getSpiritList().add(p1245263572);
    }

    /* access modifiers changed from: package-private */
    public void c1654792066(Context c) {
        Puzzle p1654792066 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1654792066(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1654792066(editor, this);
            }
        };
        p1654792066.setID(1654792066);
        p1654792066.setName("1654792066");
        p1654792066.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1654792066);
        this.Desktop.RandomlyPlaced(p1654792066);
        p1654792066.setTopEdgeType(EdgeType.Concave);
        p1654792066.setBottomEdgeType(EdgeType.Flat);
        p1654792066.setLeftEdgeType(EdgeType.Concave);
        p1654792066.setRightEdgeType(EdgeType.Concave);
        p1654792066.setTopExactPuzzleID(1245263572);
        p1654792066.setBottomExactPuzzleID(-1);
        p1654792066.setLeftExactPuzzleID(1860648131);
        p1654792066.setRightExactPuzzleID(1367843837);
        p1654792066.getDisplayImage().loadImageFromResource(c, R.drawable.p1654792066h);
        p1654792066.setExactRow(5);
        p1654792066.setExactColumn(5);
        p1654792066.getSize().reset(100.0f, 100.0f);
        p1654792066.getPositionOffset().reset(-50.0f, -50.0f);
        p1654792066.setIsUseAbsolutePosition(true);
        p1654792066.setIsUseAbsoluteSize(true);
        p1654792066.getImage().setIsUseAbsolutePosition(true);
        p1654792066.getImage().setIsUseAbsoluteSize(true);
        p1654792066.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1654792066.resetPosition();
        getSpiritList().add(p1654792066);
    }

    /* access modifiers changed from: package-private */
    public void c1596080406(Context c) {
        Puzzle p1596080406 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1596080406(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1596080406(editor, this);
            }
        };
        p1596080406.setID(1596080406);
        p1596080406.setName("1596080406");
        p1596080406.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1596080406);
        this.Desktop.RandomlyPlaced(p1596080406);
        p1596080406.setTopEdgeType(EdgeType.Flat);
        p1596080406.setBottomEdgeType(EdgeType.Convex);
        p1596080406.setLeftEdgeType(EdgeType.Concave);
        p1596080406.setRightEdgeType(EdgeType.Concave);
        p1596080406.setTopExactPuzzleID(-1);
        p1596080406.setBottomExactPuzzleID(322042350);
        p1596080406.setLeftExactPuzzleID(446019339);
        p1596080406.setRightExactPuzzleID(971061690);
        p1596080406.getDisplayImage().loadImageFromResource(c, R.drawable.p1596080406h);
        p1596080406.setExactRow(0);
        p1596080406.setExactColumn(6);
        p1596080406.getSize().reset(100.0f, 126.6667f);
        p1596080406.getPositionOffset().reset(-50.0f, -50.0f);
        p1596080406.setIsUseAbsolutePosition(true);
        p1596080406.setIsUseAbsoluteSize(true);
        p1596080406.getImage().setIsUseAbsolutePosition(true);
        p1596080406.getImage().setIsUseAbsoluteSize(true);
        p1596080406.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1596080406.resetPosition();
        getSpiritList().add(p1596080406);
    }

    /* access modifiers changed from: package-private */
    public void c322042350(Context c) {
        Puzzle p322042350 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load322042350(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save322042350(editor, this);
            }
        };
        p322042350.setID(322042350);
        p322042350.setName("322042350");
        p322042350.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p322042350);
        this.Desktop.RandomlyPlaced(p322042350);
        p322042350.setTopEdgeType(EdgeType.Concave);
        p322042350.setBottomEdgeType(EdgeType.Concave);
        p322042350.setLeftEdgeType(EdgeType.Convex);
        p322042350.setRightEdgeType(EdgeType.Convex);
        p322042350.setTopExactPuzzleID(1596080406);
        p322042350.setBottomExactPuzzleID(183069723);
        p322042350.setLeftExactPuzzleID(1146332050);
        p322042350.setRightExactPuzzleID(1022336723);
        p322042350.getDisplayImage().loadImageFromResource(c, R.drawable.p322042350h);
        p322042350.setExactRow(1);
        p322042350.setExactColumn(6);
        p322042350.getSize().reset(153.3333f, 100.0f);
        p322042350.getPositionOffset().reset(-76.66666f, -50.0f);
        p322042350.setIsUseAbsolutePosition(true);
        p322042350.setIsUseAbsoluteSize(true);
        p322042350.getImage().setIsUseAbsolutePosition(true);
        p322042350.getImage().setIsUseAbsoluteSize(true);
        p322042350.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p322042350.resetPosition();
        getSpiritList().add(p322042350);
    }

    /* access modifiers changed from: package-private */
    public void c183069723(Context c) {
        Puzzle p183069723 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load183069723(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save183069723(editor, this);
            }
        };
        p183069723.setID(183069723);
        p183069723.setName("183069723");
        p183069723.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p183069723);
        this.Desktop.RandomlyPlaced(p183069723);
        p183069723.setTopEdgeType(EdgeType.Convex);
        p183069723.setBottomEdgeType(EdgeType.Convex);
        p183069723.setLeftEdgeType(EdgeType.Convex);
        p183069723.setRightEdgeType(EdgeType.Convex);
        p183069723.setTopExactPuzzleID(322042350);
        p183069723.setBottomExactPuzzleID(2008660439);
        p183069723.setLeftExactPuzzleID(105157770);
        p183069723.setRightExactPuzzleID(1760694289);
        p183069723.getDisplayImage().loadImageFromResource(c, R.drawable.p183069723h);
        p183069723.setExactRow(2);
        p183069723.setExactColumn(6);
        p183069723.getSize().reset(153.3333f, 153.3333f);
        p183069723.getPositionOffset().reset(-76.66666f, -76.66666f);
        p183069723.setIsUseAbsolutePosition(true);
        p183069723.setIsUseAbsoluteSize(true);
        p183069723.getImage().setIsUseAbsolutePosition(true);
        p183069723.getImage().setIsUseAbsoluteSize(true);
        p183069723.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p183069723.resetPosition();
        getSpiritList().add(p183069723);
    }

    /* access modifiers changed from: package-private */
    public void c2008660439(Context c) {
        Puzzle p2008660439 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2008660439(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2008660439(editor, this);
            }
        };
        p2008660439.setID(2008660439);
        p2008660439.setName("2008660439");
        p2008660439.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2008660439);
        this.Desktop.RandomlyPlaced(p2008660439);
        p2008660439.setTopEdgeType(EdgeType.Concave);
        p2008660439.setBottomEdgeType(EdgeType.Concave);
        p2008660439.setLeftEdgeType(EdgeType.Convex);
        p2008660439.setRightEdgeType(EdgeType.Convex);
        p2008660439.setTopExactPuzzleID(183069723);
        p2008660439.setBottomExactPuzzleID(1412775374);
        p2008660439.setLeftExactPuzzleID(718940536);
        p2008660439.setRightExactPuzzleID(420528363);
        p2008660439.getDisplayImage().loadImageFromResource(c, R.drawable.p2008660439h);
        p2008660439.setExactRow(3);
        p2008660439.setExactColumn(6);
        p2008660439.getSize().reset(153.3333f, 100.0f);
        p2008660439.getPositionOffset().reset(-76.66666f, -50.0f);
        p2008660439.setIsUseAbsolutePosition(true);
        p2008660439.setIsUseAbsoluteSize(true);
        p2008660439.getImage().setIsUseAbsolutePosition(true);
        p2008660439.getImage().setIsUseAbsoluteSize(true);
        p2008660439.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2008660439.resetPosition();
        getSpiritList().add(p2008660439);
    }

    /* access modifiers changed from: package-private */
    public void c1412775374(Context c) {
        Puzzle p1412775374 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1412775374(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1412775374(editor, this);
            }
        };
        p1412775374.setID(1412775374);
        p1412775374.setName("1412775374");
        p1412775374.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1412775374);
        this.Desktop.RandomlyPlaced(p1412775374);
        p1412775374.setTopEdgeType(EdgeType.Convex);
        p1412775374.setBottomEdgeType(EdgeType.Convex);
        p1412775374.setLeftEdgeType(EdgeType.Convex);
        p1412775374.setRightEdgeType(EdgeType.Concave);
        p1412775374.setTopExactPuzzleID(2008660439);
        p1412775374.setBottomExactPuzzleID(1367843837);
        p1412775374.setLeftExactPuzzleID(1245263572);
        p1412775374.setRightExactPuzzleID(27814050);
        p1412775374.getDisplayImage().loadImageFromResource(c, R.drawable.p1412775374h);
        p1412775374.setExactRow(4);
        p1412775374.setExactColumn(6);
        p1412775374.getSize().reset(126.6667f, 153.3333f);
        p1412775374.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1412775374.setIsUseAbsolutePosition(true);
        p1412775374.setIsUseAbsoluteSize(true);
        p1412775374.getImage().setIsUseAbsolutePosition(true);
        p1412775374.getImage().setIsUseAbsoluteSize(true);
        p1412775374.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1412775374.resetPosition();
        getSpiritList().add(p1412775374);
    }

    /* access modifiers changed from: package-private */
    public void c1367843837(Context c) {
        Puzzle p1367843837 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1367843837(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1367843837(editor, this);
            }
        };
        p1367843837.setID(1367843837);
        p1367843837.setName("1367843837");
        p1367843837.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1367843837);
        this.Desktop.RandomlyPlaced(p1367843837);
        p1367843837.setTopEdgeType(EdgeType.Concave);
        p1367843837.setBottomEdgeType(EdgeType.Flat);
        p1367843837.setLeftEdgeType(EdgeType.Convex);
        p1367843837.setRightEdgeType(EdgeType.Concave);
        p1367843837.setTopExactPuzzleID(1412775374);
        p1367843837.setBottomExactPuzzleID(-1);
        p1367843837.setLeftExactPuzzleID(1654792066);
        p1367843837.setRightExactPuzzleID(2060586374);
        p1367843837.getDisplayImage().loadImageFromResource(c, R.drawable.p1367843837h);
        p1367843837.setExactRow(5);
        p1367843837.setExactColumn(6);
        p1367843837.getSize().reset(126.6667f, 100.0f);
        p1367843837.getPositionOffset().reset(-76.66666f, -50.0f);
        p1367843837.setIsUseAbsolutePosition(true);
        p1367843837.setIsUseAbsoluteSize(true);
        p1367843837.getImage().setIsUseAbsolutePosition(true);
        p1367843837.getImage().setIsUseAbsoluteSize(true);
        p1367843837.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1367843837.resetPosition();
        getSpiritList().add(p1367843837);
    }

    /* access modifiers changed from: package-private */
    public void c971061690(Context c) {
        Puzzle p971061690 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load971061690(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save971061690(editor, this);
            }
        };
        p971061690.setID(971061690);
        p971061690.setName("971061690");
        p971061690.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p971061690);
        this.Desktop.RandomlyPlaced(p971061690);
        p971061690.setTopEdgeType(EdgeType.Flat);
        p971061690.setBottomEdgeType(EdgeType.Convex);
        p971061690.setLeftEdgeType(EdgeType.Convex);
        p971061690.setRightEdgeType(EdgeType.Flat);
        p971061690.setTopExactPuzzleID(-1);
        p971061690.setBottomExactPuzzleID(1022336723);
        p971061690.setLeftExactPuzzleID(1596080406);
        p971061690.setRightExactPuzzleID(-1);
        p971061690.getDisplayImage().loadImageFromResource(c, R.drawable.p971061690h);
        p971061690.setExactRow(0);
        p971061690.setExactColumn(7);
        p971061690.getSize().reset(126.6667f, 126.6667f);
        p971061690.getPositionOffset().reset(-76.66666f, -50.0f);
        p971061690.setIsUseAbsolutePosition(true);
        p971061690.setIsUseAbsoluteSize(true);
        p971061690.getImage().setIsUseAbsolutePosition(true);
        p971061690.getImage().setIsUseAbsoluteSize(true);
        p971061690.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p971061690.resetPosition();
        getSpiritList().add(p971061690);
    }

    /* access modifiers changed from: package-private */
    public void c1022336723(Context c) {
        Puzzle p1022336723 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1022336723(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1022336723(editor, this);
            }
        };
        p1022336723.setID(1022336723);
        p1022336723.setName("1022336723");
        p1022336723.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1022336723);
        this.Desktop.RandomlyPlaced(p1022336723);
        p1022336723.setTopEdgeType(EdgeType.Concave);
        p1022336723.setBottomEdgeType(EdgeType.Concave);
        p1022336723.setLeftEdgeType(EdgeType.Concave);
        p1022336723.setRightEdgeType(EdgeType.Flat);
        p1022336723.setTopExactPuzzleID(971061690);
        p1022336723.setBottomExactPuzzleID(1760694289);
        p1022336723.setLeftExactPuzzleID(322042350);
        p1022336723.setRightExactPuzzleID(-1);
        p1022336723.getDisplayImage().loadImageFromResource(c, R.drawable.p1022336723h);
        p1022336723.setExactRow(1);
        p1022336723.setExactColumn(7);
        p1022336723.getSize().reset(100.0f, 100.0f);
        p1022336723.getPositionOffset().reset(-50.0f, -50.0f);
        p1022336723.setIsUseAbsolutePosition(true);
        p1022336723.setIsUseAbsoluteSize(true);
        p1022336723.getImage().setIsUseAbsolutePosition(true);
        p1022336723.getImage().setIsUseAbsoluteSize(true);
        p1022336723.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1022336723.resetPosition();
        getSpiritList().add(p1022336723);
    }

    /* access modifiers changed from: package-private */
    public void c1760694289(Context c) {
        Puzzle p1760694289 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1760694289(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1760694289(editor, this);
            }
        };
        p1760694289.setID(1760694289);
        p1760694289.setName("1760694289");
        p1760694289.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1760694289);
        this.Desktop.RandomlyPlaced(p1760694289);
        p1760694289.setTopEdgeType(EdgeType.Convex);
        p1760694289.setBottomEdgeType(EdgeType.Concave);
        p1760694289.setLeftEdgeType(EdgeType.Concave);
        p1760694289.setRightEdgeType(EdgeType.Flat);
        p1760694289.setTopExactPuzzleID(1022336723);
        p1760694289.setBottomExactPuzzleID(420528363);
        p1760694289.setLeftExactPuzzleID(183069723);
        p1760694289.setRightExactPuzzleID(-1);
        p1760694289.getDisplayImage().loadImageFromResource(c, R.drawable.p1760694289h);
        p1760694289.setExactRow(2);
        p1760694289.setExactColumn(7);
        p1760694289.getSize().reset(100.0f, 126.6667f);
        p1760694289.getPositionOffset().reset(-50.0f, -76.66666f);
        p1760694289.setIsUseAbsolutePosition(true);
        p1760694289.setIsUseAbsoluteSize(true);
        p1760694289.getImage().setIsUseAbsolutePosition(true);
        p1760694289.getImage().setIsUseAbsoluteSize(true);
        p1760694289.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1760694289.resetPosition();
        getSpiritList().add(p1760694289);
    }

    /* access modifiers changed from: package-private */
    public void c420528363(Context c) {
        Puzzle p420528363 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load420528363(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save420528363(editor, this);
            }
        };
        p420528363.setID(420528363);
        p420528363.setName("420528363");
        p420528363.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p420528363);
        this.Desktop.RandomlyPlaced(p420528363);
        p420528363.setTopEdgeType(EdgeType.Convex);
        p420528363.setBottomEdgeType(EdgeType.Concave);
        p420528363.setLeftEdgeType(EdgeType.Concave);
        p420528363.setRightEdgeType(EdgeType.Flat);
        p420528363.setTopExactPuzzleID(1760694289);
        p420528363.setBottomExactPuzzleID(27814050);
        p420528363.setLeftExactPuzzleID(2008660439);
        p420528363.setRightExactPuzzleID(-1);
        p420528363.getDisplayImage().loadImageFromResource(c, R.drawable.p420528363h);
        p420528363.setExactRow(3);
        p420528363.setExactColumn(7);
        p420528363.getSize().reset(100.0f, 126.6667f);
        p420528363.getPositionOffset().reset(-50.0f, -76.66666f);
        p420528363.setIsUseAbsolutePosition(true);
        p420528363.setIsUseAbsoluteSize(true);
        p420528363.getImage().setIsUseAbsolutePosition(true);
        p420528363.getImage().setIsUseAbsoluteSize(true);
        p420528363.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p420528363.resetPosition();
        getSpiritList().add(p420528363);
    }

    /* access modifiers changed from: package-private */
    public void c27814050(Context c) {
        Puzzle p27814050 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load27814050(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save27814050(editor, this);
            }
        };
        p27814050.setID(27814050);
        p27814050.setName("27814050");
        p27814050.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p27814050);
        this.Desktop.RandomlyPlaced(p27814050);
        p27814050.setTopEdgeType(EdgeType.Convex);
        p27814050.setBottomEdgeType(EdgeType.Concave);
        p27814050.setLeftEdgeType(EdgeType.Convex);
        p27814050.setRightEdgeType(EdgeType.Flat);
        p27814050.setTopExactPuzzleID(420528363);
        p27814050.setBottomExactPuzzleID(2060586374);
        p27814050.setLeftExactPuzzleID(1412775374);
        p27814050.setRightExactPuzzleID(-1);
        p27814050.getDisplayImage().loadImageFromResource(c, R.drawable.p27814050h);
        p27814050.setExactRow(4);
        p27814050.setExactColumn(7);
        p27814050.getSize().reset(126.6667f, 126.6667f);
        p27814050.getPositionOffset().reset(-76.66666f, -76.66666f);
        p27814050.setIsUseAbsolutePosition(true);
        p27814050.setIsUseAbsoluteSize(true);
        p27814050.getImage().setIsUseAbsolutePosition(true);
        p27814050.getImage().setIsUseAbsoluteSize(true);
        p27814050.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p27814050.resetPosition();
        getSpiritList().add(p27814050);
    }

    /* access modifiers changed from: package-private */
    public void c2060586374(Context c) {
        Puzzle p2060586374 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2060586374(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2060586374(editor, this);
            }
        };
        p2060586374.setID(2060586374);
        p2060586374.setName("2060586374");
        p2060586374.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2060586374);
        this.Desktop.RandomlyPlaced(p2060586374);
        p2060586374.setTopEdgeType(EdgeType.Convex);
        p2060586374.setBottomEdgeType(EdgeType.Flat);
        p2060586374.setLeftEdgeType(EdgeType.Convex);
        p2060586374.setRightEdgeType(EdgeType.Flat);
        p2060586374.setTopExactPuzzleID(27814050);
        p2060586374.setBottomExactPuzzleID(-1);
        p2060586374.setLeftExactPuzzleID(1367843837);
        p2060586374.setRightExactPuzzleID(-1);
        p2060586374.getDisplayImage().loadImageFromResource(c, R.drawable.p2060586374h);
        p2060586374.setExactRow(5);
        p2060586374.setExactColumn(7);
        p2060586374.getSize().reset(126.6667f, 126.6667f);
        p2060586374.getPositionOffset().reset(-76.66666f, -76.66666f);
        p2060586374.setIsUseAbsolutePosition(true);
        p2060586374.setIsUseAbsoluteSize(true);
        p2060586374.getImage().setIsUseAbsolutePosition(true);
        p2060586374.getImage().setIsUseAbsoluteSize(true);
        p2060586374.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2060586374.resetPosition();
        getSpiritList().add(p2060586374);
    }
}
