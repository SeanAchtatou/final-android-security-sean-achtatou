package com.skyd.bestpuzzle;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.skyd.core.android.game.GameMaster;
import com.skyd.core.android.game.GameObject;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.vector.Vector2DF;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Desktop extends GameSpirit {
    public Paint ColorAPaint;
    public Paint ColorBPaint;
    private RectF _CurrentObserveAreaRectF = new RectF();
    private Vector2DF _CurrentObservePosition = new Vector2DF();
    private Matrix _Matrix = null;
    private OriginalImage _OriginalImage = null;
    private ArrayList<Puzzle> _PuzzleList = new ArrayList<>();
    private float _Zoom = 1.0f;

    public Desktop() {
        this._CurrentObservePosition.addOnXChangingListener(new Vector2DF.OnXChangingListener() {
            public boolean OnXChangingEvent(Object sender, float currentValue, float newValue) {
                if (newValue < 0.0f || newValue > Desktop.this.getSize().getX()) {
                    return false;
                }
                return true;
            }
        });
        this._CurrentObservePosition.addOnYChangingListener(new Vector2DF.OnYChangingListener() {
            public boolean OnYChangingEvent(Object sender, float currentValue, float newValue) {
                if (newValue < 0.0f || newValue > Desktop.this.getSize().getY()) {
                    return false;
                }
                return true;
            }
        });
        this._CurrentObservePosition.addOnValueChangedListener(new Vector2DF.OnValueChangedListener() {
            public void OnValueChangedEvent(Object sender, float newX, float newY) {
                Desktop.this.updateCurrentObserveAreaRectF();
                Desktop.this.updateMatrix();
            }
        });
    }

    public GameObject getDisplayContentChild() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawSelf(Canvas c, Rect drawArea) {
        boolean z;
        super.drawSelf(c, drawArea);
        int row = (int) Math.floor((double) (getCurrentObserveAreaRectF().top / 40.0f));
        int col = (int) Math.floor((double) (getCurrentObserveAreaRectF().left / 40.0f));
        for (float i = getCurrentObserveAreaRectF().left - (getCurrentObserveAreaRectF().left % 40.0f); i <= getCurrentObserveAreaRectF().right; i += 40.0f) {
            for (float j = getCurrentObserveAreaRectF().top - (getCurrentObserveAreaRectF().top % 40.0f); j <= getCurrentObserveAreaRectF().bottom; j += 40.0f) {
                float f = i + 40.0f;
                float f2 = j + 40.0f;
                if (col % 2 == 0) {
                    z = true;
                } else {
                    z = false;
                }
                c.drawRect(i, j, f, f2, z ^ (row % 2 == 0) ? this.ColorAPaint : this.ColorBPaint);
                row++;
            }
            col++;
            row = (int) Math.floor((double) (getCurrentObserveAreaRectF().top / 40.0f));
        }
    }

    /* access modifiers changed from: protected */
    public void updateSelf() {
        super.updateSelf();
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    public boolean PiecePuzzle(List<Puzzle> pl, Controller c) {
        boolean b = false;
        Puzzle ok = null;
        Vector2DF tp = null;
        float tr = 0.0f;
        for (Puzzle p : pl) {
            tp = p.getPositionInDesktop().getClone();
            tr = p.getRotation();
            Iterator<Puzzle> it = getPuzzleList().iterator();
            while (true) {
                if (it.hasNext()) {
                    Puzzle f = it.next();
                    if (!pl.contains(f) && p.Piece(f)) {
                        b = true;
                        ok = p;
                        tp = p.getPositionInDesktop().minusNew(tp);
                        tr = p.getRotation() - tr;
                        continue;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (b) {
                break;
            }
        }
        if (b) {
            c.getPuzzleList().remove(ok);
            c.getPositionInDesktop().resetWith(ok.getPositionInDesktop().minusNew(tp));
            c.saveToMapList();
            c.rotatePuzzlesTo(tr, c.getPositionInDesktop().getX(), c.getPositionInDesktop().getY());
            c.saveToMapList();
            c.movePuzzlesTo(tp);
        }
        if (!((PuzzleScene) getRoot()).getIsFinished() && checkFinish()) {
            ((PuzzleScene) getRoot()).savePuzzleState();
            ((PuzzleScene) getRoot()).saveGameState();
            ((PuzzleScene) getRoot()).setFinished();
        }
        return b;
    }

    public boolean checkFinish() {
        Iterator<Puzzle> it = getPuzzleList().iterator();
        while (it.hasNext()) {
            if (!it.next().IsExact()) {
                return false;
            }
        }
        return true;
    }

    public void updateCurrentObserveAreaRectF() {
        float w = (((float) GameMaster.getScreenWidth()) / 2.0f) * (1.0f / getZoom());
        float h = (((float) GameMaster.getScreenHeight()) / 2.0f) * (1.0f / getZoom());
        getCurrentObserveAreaRectF().set((getCurrentObservePosition().getX() - w) - Puzzle.getMaxRadius().getX(), (getCurrentObservePosition().getY() - h) - Puzzle.getMaxRadius().getY(), getCurrentObservePosition().getX() + w + Puzzle.getMaxRadius().getX(), getCurrentObservePosition().getY() + h + Puzzle.getMaxRadius().getY());
    }

    /* access modifiers changed from: protected */
    public void updateChilds() {
        super.updateChilds();
    }

    public float getZoom() {
        return this._Zoom;
    }

    public void setZoom(float value) {
        this._Zoom = value;
        updateMatrix();
        updateCurrentObserveAreaRectF();
    }

    public void setZoomToDefault() {
        setZoom(1.0f);
    }

    public Vector2DF getCurrentObservePosition() {
        return this._CurrentObservePosition;
    }

    public RectF getCurrentObserveAreaRectF() {
        return this._CurrentObserveAreaRectF;
    }

    /* access modifiers changed from: protected */
    public void setCurrentObserveAreaRectF(RectF value) {
        this._CurrentObserveAreaRectF = value;
    }

    /* access modifiers changed from: protected */
    public void setCurrentObserveAreaRectFToDefault() {
        setCurrentObserveAreaRectF(new RectF());
    }

    public ArrayList<Puzzle> getPuzzleList() {
        return this._PuzzleList;
    }

    public void setPuzzleList(ArrayList<Puzzle> value) {
        this._PuzzleList = value;
    }

    public void setPuzzleListToDefault() {
        setPuzzleList(new ArrayList());
    }

    public OriginalImage getOriginalImage() {
        return this._OriginalImage;
    }

    public void setOriginalImage(OriginalImage value) {
        this._OriginalImage = value;
    }

    public void setOriginalImageToDefault() {
        setOriginalImage(null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void RandomlyPlaced(Puzzle p) {
        p.getPositionInDesktop().reset((GameMaster.getRandom().nextFloat() * 1120.0f) + Math.max(0.0f, Math.min((1600.0f - ((float) GameMaster.getScreenWidth())) / 2.0f, 240.0f)), (GameMaster.getRandom().nextFloat() * 840.0f) + Math.max(0.0f, Math.min((1200.0f - ((float) GameMaster.getScreenHeight())) / 2.0f, 180.0f)));
    }

    public Matrix getMatrix() {
        return this._Matrix;
    }

    public void updateMatrix() {
        if (this._Matrix == null) {
            this._Matrix = new Matrix();
        }
        this._Matrix.reset();
        this._Matrix.preTranslate((-getCurrentObservePosition().getX()) + ((float) (GameMaster.getScreenWidth() / 2)), (-getCurrentObservePosition().getY()) + ((float) (GameMaster.getScreenHeight() / 2)));
        this._Matrix.preScale(getZoom(), getZoom(), getCurrentObservePosition().getX(), getCurrentObservePosition().getY());
    }

    public Matrix creatReMatrix() {
        Matrix m = new Matrix();
        m.postTranslate(getCurrentObservePosition().getX() - ((float) (GameMaster.getScreenWidth() / 2)), getCurrentObservePosition().getY() - ((float) (GameMaster.getScreenHeight() / 2)));
        m.postScale(1.0f / getZoom(), 1.0f / getZoom(), getCurrentObservePosition().getX(), getCurrentObservePosition().getY());
        return m;
    }

    public Vector2DF mapPoint(Vector2DF v) {
        return v.mapNew(this._Matrix);
    }

    public Vector2DF reMapPoint(Vector2DF v) {
        return v.mapNew(creatReMatrix());
    }

    public Puzzle getNearPuzzle(Vector2DF PuzzlePosition, float Tolerance) {
        Iterator<Puzzle> it = getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getPositionInDesktop().minusNew(PuzzlePosition).getLength() < Tolerance) {
                return f;
            }
        }
        return null;
    }
}
