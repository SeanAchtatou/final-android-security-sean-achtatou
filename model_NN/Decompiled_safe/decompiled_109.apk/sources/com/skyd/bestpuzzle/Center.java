package com.skyd.bestpuzzle;

import android.content.SharedPreferences;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.skyd.core.android.Global;

public class Center extends Global {
    public void onCreate() {
        super.onCreate();
        ScoreloopManagerSingleton.init(this);
    }

    public void onTerminate() {
        super.onTerminate();
        ScoreloopManagerSingleton.destroy();
    }

    public String getAppKey() {
        return "com.skyd.bestpuzzle.n1649";
    }

    public Boolean getIsFinished() {
        return getIsFinished(getSharedPreferences());
    }

    public Boolean getIsFinished(SharedPreferences sharedPreferences) {
        return Boolean.valueOf(sharedPreferences.getBoolean("Boolean_IsFinished", false));
    }

    public void setIsFinished(Boolean value) {
        setIsFinished(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor setIsFinished(Boolean value, SharedPreferences.Editor editor) {
        return editor.putBoolean("Boolean_IsFinished", value.booleanValue());
    }

    public void setIsFinishedToDefault() {
        setIsFinished(false);
    }

    public SharedPreferences.Editor setIsFinishedToDefault(SharedPreferences.Editor editor) {
        return setIsFinished(false, editor);
    }

    public Long getPastTime() {
        return getPastTime(getSharedPreferences());
    }

    public Long getPastTime(SharedPreferences sharedPreferences) {
        return Long.valueOf(sharedPreferences.getLong("Long_PastTime", 0));
    }

    public void setPastTime(Long value) {
        setPastTime(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor setPastTime(Long value, SharedPreferences.Editor editor) {
        return editor.putLong("Long_PastTime", value.longValue());
    }

    public void setPastTimeToDefault() {
        setPastTime(Long.MIN_VALUE);
    }

    public SharedPreferences.Editor setPastTimeToDefault(SharedPreferences.Editor editor) {
        return setPastTime(Long.MIN_VALUE, editor);
    }

    public void load1395440640(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1395440640_X(sharedPreferences);
        float y = get1395440640_Y(sharedPreferences);
        float r = get1395440640_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1395440640(SharedPreferences.Editor editor, Puzzle p) {
        set1395440640_X(p.getPositionInDesktop().getX(), editor);
        set1395440640_Y(p.getPositionInDesktop().getY(), editor);
        set1395440640_R(p.getRotation(), editor);
    }

    public float get1395440640_X() {
        return get1395440640_X(getSharedPreferences());
    }

    public float get1395440640_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1395440640_X", Float.MIN_VALUE);
    }

    public void set1395440640_X(float value) {
        set1395440640_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1395440640_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1395440640_X", value);
    }

    public void set1395440640_XToDefault() {
        set1395440640_X(0.0f);
    }

    public SharedPreferences.Editor set1395440640_XToDefault(SharedPreferences.Editor editor) {
        return set1395440640_X(0.0f, editor);
    }

    public float get1395440640_Y() {
        return get1395440640_Y(getSharedPreferences());
    }

    public float get1395440640_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1395440640_Y", Float.MIN_VALUE);
    }

    public void set1395440640_Y(float value) {
        set1395440640_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1395440640_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1395440640_Y", value);
    }

    public void set1395440640_YToDefault() {
        set1395440640_Y(0.0f);
    }

    public SharedPreferences.Editor set1395440640_YToDefault(SharedPreferences.Editor editor) {
        return set1395440640_Y(0.0f, editor);
    }

    public float get1395440640_R() {
        return get1395440640_R(getSharedPreferences());
    }

    public float get1395440640_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1395440640_R", Float.MIN_VALUE);
    }

    public void set1395440640_R(float value) {
        set1395440640_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1395440640_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1395440640_R", value);
    }

    public void set1395440640_RToDefault() {
        set1395440640_R(0.0f);
    }

    public SharedPreferences.Editor set1395440640_RToDefault(SharedPreferences.Editor editor) {
        return set1395440640_R(0.0f, editor);
    }

    public void load1718568215(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1718568215_X(sharedPreferences);
        float y = get1718568215_Y(sharedPreferences);
        float r = get1718568215_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1718568215(SharedPreferences.Editor editor, Puzzle p) {
        set1718568215_X(p.getPositionInDesktop().getX(), editor);
        set1718568215_Y(p.getPositionInDesktop().getY(), editor);
        set1718568215_R(p.getRotation(), editor);
    }

    public float get1718568215_X() {
        return get1718568215_X(getSharedPreferences());
    }

    public float get1718568215_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1718568215_X", Float.MIN_VALUE);
    }

    public void set1718568215_X(float value) {
        set1718568215_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1718568215_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1718568215_X", value);
    }

    public void set1718568215_XToDefault() {
        set1718568215_X(0.0f);
    }

    public SharedPreferences.Editor set1718568215_XToDefault(SharedPreferences.Editor editor) {
        return set1718568215_X(0.0f, editor);
    }

    public float get1718568215_Y() {
        return get1718568215_Y(getSharedPreferences());
    }

    public float get1718568215_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1718568215_Y", Float.MIN_VALUE);
    }

    public void set1718568215_Y(float value) {
        set1718568215_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1718568215_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1718568215_Y", value);
    }

    public void set1718568215_YToDefault() {
        set1718568215_Y(0.0f);
    }

    public SharedPreferences.Editor set1718568215_YToDefault(SharedPreferences.Editor editor) {
        return set1718568215_Y(0.0f, editor);
    }

    public float get1718568215_R() {
        return get1718568215_R(getSharedPreferences());
    }

    public float get1718568215_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1718568215_R", Float.MIN_VALUE);
    }

    public void set1718568215_R(float value) {
        set1718568215_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1718568215_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1718568215_R", value);
    }

    public void set1718568215_RToDefault() {
        set1718568215_R(0.0f);
    }

    public SharedPreferences.Editor set1718568215_RToDefault(SharedPreferences.Editor editor) {
        return set1718568215_R(0.0f, editor);
    }

    public void load1566004604(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1566004604_X(sharedPreferences);
        float y = get1566004604_Y(sharedPreferences);
        float r = get1566004604_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1566004604(SharedPreferences.Editor editor, Puzzle p) {
        set1566004604_X(p.getPositionInDesktop().getX(), editor);
        set1566004604_Y(p.getPositionInDesktop().getY(), editor);
        set1566004604_R(p.getRotation(), editor);
    }

    public float get1566004604_X() {
        return get1566004604_X(getSharedPreferences());
    }

    public float get1566004604_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1566004604_X", Float.MIN_VALUE);
    }

    public void set1566004604_X(float value) {
        set1566004604_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1566004604_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1566004604_X", value);
    }

    public void set1566004604_XToDefault() {
        set1566004604_X(0.0f);
    }

    public SharedPreferences.Editor set1566004604_XToDefault(SharedPreferences.Editor editor) {
        return set1566004604_X(0.0f, editor);
    }

    public float get1566004604_Y() {
        return get1566004604_Y(getSharedPreferences());
    }

    public float get1566004604_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1566004604_Y", Float.MIN_VALUE);
    }

    public void set1566004604_Y(float value) {
        set1566004604_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1566004604_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1566004604_Y", value);
    }

    public void set1566004604_YToDefault() {
        set1566004604_Y(0.0f);
    }

    public SharedPreferences.Editor set1566004604_YToDefault(SharedPreferences.Editor editor) {
        return set1566004604_Y(0.0f, editor);
    }

    public float get1566004604_R() {
        return get1566004604_R(getSharedPreferences());
    }

    public float get1566004604_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1566004604_R", Float.MIN_VALUE);
    }

    public void set1566004604_R(float value) {
        set1566004604_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1566004604_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1566004604_R", value);
    }

    public void set1566004604_RToDefault() {
        set1566004604_R(0.0f);
    }

    public SharedPreferences.Editor set1566004604_RToDefault(SharedPreferences.Editor editor) {
        return set1566004604_R(0.0f, editor);
    }

    public void load1813046260(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1813046260_X(sharedPreferences);
        float y = get1813046260_Y(sharedPreferences);
        float r = get1813046260_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1813046260(SharedPreferences.Editor editor, Puzzle p) {
        set1813046260_X(p.getPositionInDesktop().getX(), editor);
        set1813046260_Y(p.getPositionInDesktop().getY(), editor);
        set1813046260_R(p.getRotation(), editor);
    }

    public float get1813046260_X() {
        return get1813046260_X(getSharedPreferences());
    }

    public float get1813046260_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1813046260_X", Float.MIN_VALUE);
    }

    public void set1813046260_X(float value) {
        set1813046260_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1813046260_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1813046260_X", value);
    }

    public void set1813046260_XToDefault() {
        set1813046260_X(0.0f);
    }

    public SharedPreferences.Editor set1813046260_XToDefault(SharedPreferences.Editor editor) {
        return set1813046260_X(0.0f, editor);
    }

    public float get1813046260_Y() {
        return get1813046260_Y(getSharedPreferences());
    }

    public float get1813046260_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1813046260_Y", Float.MIN_VALUE);
    }

    public void set1813046260_Y(float value) {
        set1813046260_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1813046260_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1813046260_Y", value);
    }

    public void set1813046260_YToDefault() {
        set1813046260_Y(0.0f);
    }

    public SharedPreferences.Editor set1813046260_YToDefault(SharedPreferences.Editor editor) {
        return set1813046260_Y(0.0f, editor);
    }

    public float get1813046260_R() {
        return get1813046260_R(getSharedPreferences());
    }

    public float get1813046260_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1813046260_R", Float.MIN_VALUE);
    }

    public void set1813046260_R(float value) {
        set1813046260_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1813046260_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1813046260_R", value);
    }

    public void set1813046260_RToDefault() {
        set1813046260_R(0.0f);
    }

    public SharedPreferences.Editor set1813046260_RToDefault(SharedPreferences.Editor editor) {
        return set1813046260_R(0.0f, editor);
    }

    public void load1281467703(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1281467703_X(sharedPreferences);
        float y = get1281467703_Y(sharedPreferences);
        float r = get1281467703_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1281467703(SharedPreferences.Editor editor, Puzzle p) {
        set1281467703_X(p.getPositionInDesktop().getX(), editor);
        set1281467703_Y(p.getPositionInDesktop().getY(), editor);
        set1281467703_R(p.getRotation(), editor);
    }

    public float get1281467703_X() {
        return get1281467703_X(getSharedPreferences());
    }

    public float get1281467703_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1281467703_X", Float.MIN_VALUE);
    }

    public void set1281467703_X(float value) {
        set1281467703_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1281467703_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1281467703_X", value);
    }

    public void set1281467703_XToDefault() {
        set1281467703_X(0.0f);
    }

    public SharedPreferences.Editor set1281467703_XToDefault(SharedPreferences.Editor editor) {
        return set1281467703_X(0.0f, editor);
    }

    public float get1281467703_Y() {
        return get1281467703_Y(getSharedPreferences());
    }

    public float get1281467703_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1281467703_Y", Float.MIN_VALUE);
    }

    public void set1281467703_Y(float value) {
        set1281467703_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1281467703_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1281467703_Y", value);
    }

    public void set1281467703_YToDefault() {
        set1281467703_Y(0.0f);
    }

    public SharedPreferences.Editor set1281467703_YToDefault(SharedPreferences.Editor editor) {
        return set1281467703_Y(0.0f, editor);
    }

    public float get1281467703_R() {
        return get1281467703_R(getSharedPreferences());
    }

    public float get1281467703_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1281467703_R", Float.MIN_VALUE);
    }

    public void set1281467703_R(float value) {
        set1281467703_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1281467703_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1281467703_R", value);
    }

    public void set1281467703_RToDefault() {
        set1281467703_R(0.0f);
    }

    public SharedPreferences.Editor set1281467703_RToDefault(SharedPreferences.Editor editor) {
        return set1281467703_R(0.0f, editor);
    }

    public void load1084465376(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1084465376_X(sharedPreferences);
        float y = get1084465376_Y(sharedPreferences);
        float r = get1084465376_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1084465376(SharedPreferences.Editor editor, Puzzle p) {
        set1084465376_X(p.getPositionInDesktop().getX(), editor);
        set1084465376_Y(p.getPositionInDesktop().getY(), editor);
        set1084465376_R(p.getRotation(), editor);
    }

    public float get1084465376_X() {
        return get1084465376_X(getSharedPreferences());
    }

    public float get1084465376_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1084465376_X", Float.MIN_VALUE);
    }

    public void set1084465376_X(float value) {
        set1084465376_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1084465376_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1084465376_X", value);
    }

    public void set1084465376_XToDefault() {
        set1084465376_X(0.0f);
    }

    public SharedPreferences.Editor set1084465376_XToDefault(SharedPreferences.Editor editor) {
        return set1084465376_X(0.0f, editor);
    }

    public float get1084465376_Y() {
        return get1084465376_Y(getSharedPreferences());
    }

    public float get1084465376_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1084465376_Y", Float.MIN_VALUE);
    }

    public void set1084465376_Y(float value) {
        set1084465376_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1084465376_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1084465376_Y", value);
    }

    public void set1084465376_YToDefault() {
        set1084465376_Y(0.0f);
    }

    public SharedPreferences.Editor set1084465376_YToDefault(SharedPreferences.Editor editor) {
        return set1084465376_Y(0.0f, editor);
    }

    public float get1084465376_R() {
        return get1084465376_R(getSharedPreferences());
    }

    public float get1084465376_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1084465376_R", Float.MIN_VALUE);
    }

    public void set1084465376_R(float value) {
        set1084465376_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1084465376_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1084465376_R", value);
    }

    public void set1084465376_RToDefault() {
        set1084465376_R(0.0f);
    }

    public SharedPreferences.Editor set1084465376_RToDefault(SharedPreferences.Editor editor) {
        return set1084465376_R(0.0f, editor);
    }

    public void load374208536(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get374208536_X(sharedPreferences);
        float y = get374208536_Y(sharedPreferences);
        float r = get374208536_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save374208536(SharedPreferences.Editor editor, Puzzle p) {
        set374208536_X(p.getPositionInDesktop().getX(), editor);
        set374208536_Y(p.getPositionInDesktop().getY(), editor);
        set374208536_R(p.getRotation(), editor);
    }

    public float get374208536_X() {
        return get374208536_X(getSharedPreferences());
    }

    public float get374208536_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_374208536_X", Float.MIN_VALUE);
    }

    public void set374208536_X(float value) {
        set374208536_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set374208536_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_374208536_X", value);
    }

    public void set374208536_XToDefault() {
        set374208536_X(0.0f);
    }

    public SharedPreferences.Editor set374208536_XToDefault(SharedPreferences.Editor editor) {
        return set374208536_X(0.0f, editor);
    }

    public float get374208536_Y() {
        return get374208536_Y(getSharedPreferences());
    }

    public float get374208536_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_374208536_Y", Float.MIN_VALUE);
    }

    public void set374208536_Y(float value) {
        set374208536_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set374208536_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_374208536_Y", value);
    }

    public void set374208536_YToDefault() {
        set374208536_Y(0.0f);
    }

    public SharedPreferences.Editor set374208536_YToDefault(SharedPreferences.Editor editor) {
        return set374208536_Y(0.0f, editor);
    }

    public float get374208536_R() {
        return get374208536_R(getSharedPreferences());
    }

    public float get374208536_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_374208536_R", Float.MIN_VALUE);
    }

    public void set374208536_R(float value) {
        set374208536_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set374208536_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_374208536_R", value);
    }

    public void set374208536_RToDefault() {
        set374208536_R(0.0f);
    }

    public SharedPreferences.Editor set374208536_RToDefault(SharedPreferences.Editor editor) {
        return set374208536_R(0.0f, editor);
    }

    public void load868798569(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get868798569_X(sharedPreferences);
        float y = get868798569_Y(sharedPreferences);
        float r = get868798569_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save868798569(SharedPreferences.Editor editor, Puzzle p) {
        set868798569_X(p.getPositionInDesktop().getX(), editor);
        set868798569_Y(p.getPositionInDesktop().getY(), editor);
        set868798569_R(p.getRotation(), editor);
    }

    public float get868798569_X() {
        return get868798569_X(getSharedPreferences());
    }

    public float get868798569_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_868798569_X", Float.MIN_VALUE);
    }

    public void set868798569_X(float value) {
        set868798569_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set868798569_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_868798569_X", value);
    }

    public void set868798569_XToDefault() {
        set868798569_X(0.0f);
    }

    public SharedPreferences.Editor set868798569_XToDefault(SharedPreferences.Editor editor) {
        return set868798569_X(0.0f, editor);
    }

    public float get868798569_Y() {
        return get868798569_Y(getSharedPreferences());
    }

    public float get868798569_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_868798569_Y", Float.MIN_VALUE);
    }

    public void set868798569_Y(float value) {
        set868798569_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set868798569_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_868798569_Y", value);
    }

    public void set868798569_YToDefault() {
        set868798569_Y(0.0f);
    }

    public SharedPreferences.Editor set868798569_YToDefault(SharedPreferences.Editor editor) {
        return set868798569_Y(0.0f, editor);
    }

    public float get868798569_R() {
        return get868798569_R(getSharedPreferences());
    }

    public float get868798569_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_868798569_R", Float.MIN_VALUE);
    }

    public void set868798569_R(float value) {
        set868798569_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set868798569_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_868798569_R", value);
    }

    public void set868798569_RToDefault() {
        set868798569_R(0.0f);
    }

    public SharedPreferences.Editor set868798569_RToDefault(SharedPreferences.Editor editor) {
        return set868798569_R(0.0f, editor);
    }

    public void load191758053(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get191758053_X(sharedPreferences);
        float y = get191758053_Y(sharedPreferences);
        float r = get191758053_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save191758053(SharedPreferences.Editor editor, Puzzle p) {
        set191758053_X(p.getPositionInDesktop().getX(), editor);
        set191758053_Y(p.getPositionInDesktop().getY(), editor);
        set191758053_R(p.getRotation(), editor);
    }

    public float get191758053_X() {
        return get191758053_X(getSharedPreferences());
    }

    public float get191758053_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_191758053_X", Float.MIN_VALUE);
    }

    public void set191758053_X(float value) {
        set191758053_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set191758053_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_191758053_X", value);
    }

    public void set191758053_XToDefault() {
        set191758053_X(0.0f);
    }

    public SharedPreferences.Editor set191758053_XToDefault(SharedPreferences.Editor editor) {
        return set191758053_X(0.0f, editor);
    }

    public float get191758053_Y() {
        return get191758053_Y(getSharedPreferences());
    }

    public float get191758053_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_191758053_Y", Float.MIN_VALUE);
    }

    public void set191758053_Y(float value) {
        set191758053_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set191758053_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_191758053_Y", value);
    }

    public void set191758053_YToDefault() {
        set191758053_Y(0.0f);
    }

    public SharedPreferences.Editor set191758053_YToDefault(SharedPreferences.Editor editor) {
        return set191758053_Y(0.0f, editor);
    }

    public float get191758053_R() {
        return get191758053_R(getSharedPreferences());
    }

    public float get191758053_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_191758053_R", Float.MIN_VALUE);
    }

    public void set191758053_R(float value) {
        set191758053_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set191758053_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_191758053_R", value);
    }

    public void set191758053_RToDefault() {
        set191758053_R(0.0f);
    }

    public SharedPreferences.Editor set191758053_RToDefault(SharedPreferences.Editor editor) {
        return set191758053_R(0.0f, editor);
    }

    public void load538176265(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get538176265_X(sharedPreferences);
        float y = get538176265_Y(sharedPreferences);
        float r = get538176265_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save538176265(SharedPreferences.Editor editor, Puzzle p) {
        set538176265_X(p.getPositionInDesktop().getX(), editor);
        set538176265_Y(p.getPositionInDesktop().getY(), editor);
        set538176265_R(p.getRotation(), editor);
    }

    public float get538176265_X() {
        return get538176265_X(getSharedPreferences());
    }

    public float get538176265_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_538176265_X", Float.MIN_VALUE);
    }

    public void set538176265_X(float value) {
        set538176265_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set538176265_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_538176265_X", value);
    }

    public void set538176265_XToDefault() {
        set538176265_X(0.0f);
    }

    public SharedPreferences.Editor set538176265_XToDefault(SharedPreferences.Editor editor) {
        return set538176265_X(0.0f, editor);
    }

    public float get538176265_Y() {
        return get538176265_Y(getSharedPreferences());
    }

    public float get538176265_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_538176265_Y", Float.MIN_VALUE);
    }

    public void set538176265_Y(float value) {
        set538176265_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set538176265_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_538176265_Y", value);
    }

    public void set538176265_YToDefault() {
        set538176265_Y(0.0f);
    }

    public SharedPreferences.Editor set538176265_YToDefault(SharedPreferences.Editor editor) {
        return set538176265_Y(0.0f, editor);
    }

    public float get538176265_R() {
        return get538176265_R(getSharedPreferences());
    }

    public float get538176265_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_538176265_R", Float.MIN_VALUE);
    }

    public void set538176265_R(float value) {
        set538176265_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set538176265_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_538176265_R", value);
    }

    public void set538176265_RToDefault() {
        set538176265_R(0.0f);
    }

    public SharedPreferences.Editor set538176265_RToDefault(SharedPreferences.Editor editor) {
        return set538176265_R(0.0f, editor);
    }

    public void load650989099(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get650989099_X(sharedPreferences);
        float y = get650989099_Y(sharedPreferences);
        float r = get650989099_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save650989099(SharedPreferences.Editor editor, Puzzle p) {
        set650989099_X(p.getPositionInDesktop().getX(), editor);
        set650989099_Y(p.getPositionInDesktop().getY(), editor);
        set650989099_R(p.getRotation(), editor);
    }

    public float get650989099_X() {
        return get650989099_X(getSharedPreferences());
    }

    public float get650989099_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_650989099_X", Float.MIN_VALUE);
    }

    public void set650989099_X(float value) {
        set650989099_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set650989099_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_650989099_X", value);
    }

    public void set650989099_XToDefault() {
        set650989099_X(0.0f);
    }

    public SharedPreferences.Editor set650989099_XToDefault(SharedPreferences.Editor editor) {
        return set650989099_X(0.0f, editor);
    }

    public float get650989099_Y() {
        return get650989099_Y(getSharedPreferences());
    }

    public float get650989099_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_650989099_Y", Float.MIN_VALUE);
    }

    public void set650989099_Y(float value) {
        set650989099_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set650989099_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_650989099_Y", value);
    }

    public void set650989099_YToDefault() {
        set650989099_Y(0.0f);
    }

    public SharedPreferences.Editor set650989099_YToDefault(SharedPreferences.Editor editor) {
        return set650989099_Y(0.0f, editor);
    }

    public float get650989099_R() {
        return get650989099_R(getSharedPreferences());
    }

    public float get650989099_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_650989099_R", Float.MIN_VALUE);
    }

    public void set650989099_R(float value) {
        set650989099_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set650989099_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_650989099_R", value);
    }

    public void set650989099_RToDefault() {
        set650989099_R(0.0f);
    }

    public SharedPreferences.Editor set650989099_RToDefault(SharedPreferences.Editor editor) {
        return set650989099_R(0.0f, editor);
    }

    public void load2072784504(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get2072784504_X(sharedPreferences);
        float y = get2072784504_Y(sharedPreferences);
        float r = get2072784504_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save2072784504(SharedPreferences.Editor editor, Puzzle p) {
        set2072784504_X(p.getPositionInDesktop().getX(), editor);
        set2072784504_Y(p.getPositionInDesktop().getY(), editor);
        set2072784504_R(p.getRotation(), editor);
    }

    public float get2072784504_X() {
        return get2072784504_X(getSharedPreferences());
    }

    public float get2072784504_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_2072784504_X", Float.MIN_VALUE);
    }

    public void set2072784504_X(float value) {
        set2072784504_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set2072784504_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_2072784504_X", value);
    }

    public void set2072784504_XToDefault() {
        set2072784504_X(0.0f);
    }

    public SharedPreferences.Editor set2072784504_XToDefault(SharedPreferences.Editor editor) {
        return set2072784504_X(0.0f, editor);
    }

    public float get2072784504_Y() {
        return get2072784504_Y(getSharedPreferences());
    }

    public float get2072784504_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_2072784504_Y", Float.MIN_VALUE);
    }

    public void set2072784504_Y(float value) {
        set2072784504_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set2072784504_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_2072784504_Y", value);
    }

    public void set2072784504_YToDefault() {
        set2072784504_Y(0.0f);
    }

    public SharedPreferences.Editor set2072784504_YToDefault(SharedPreferences.Editor editor) {
        return set2072784504_Y(0.0f, editor);
    }

    public float get2072784504_R() {
        return get2072784504_R(getSharedPreferences());
    }

    public float get2072784504_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_2072784504_R", Float.MIN_VALUE);
    }

    public void set2072784504_R(float value) {
        set2072784504_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set2072784504_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_2072784504_R", value);
    }

    public void set2072784504_RToDefault() {
        set2072784504_R(0.0f);
    }

    public SharedPreferences.Editor set2072784504_RToDefault(SharedPreferences.Editor editor) {
        return set2072784504_R(0.0f, editor);
    }

    public void load2107762398(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get2107762398_X(sharedPreferences);
        float y = get2107762398_Y(sharedPreferences);
        float r = get2107762398_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save2107762398(SharedPreferences.Editor editor, Puzzle p) {
        set2107762398_X(p.getPositionInDesktop().getX(), editor);
        set2107762398_Y(p.getPositionInDesktop().getY(), editor);
        set2107762398_R(p.getRotation(), editor);
    }

    public float get2107762398_X() {
        return get2107762398_X(getSharedPreferences());
    }

    public float get2107762398_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_2107762398_X", Float.MIN_VALUE);
    }

    public void set2107762398_X(float value) {
        set2107762398_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set2107762398_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_2107762398_X", value);
    }

    public void set2107762398_XToDefault() {
        set2107762398_X(0.0f);
    }

    public SharedPreferences.Editor set2107762398_XToDefault(SharedPreferences.Editor editor) {
        return set2107762398_X(0.0f, editor);
    }

    public float get2107762398_Y() {
        return get2107762398_Y(getSharedPreferences());
    }

    public float get2107762398_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_2107762398_Y", Float.MIN_VALUE);
    }

    public void set2107762398_Y(float value) {
        set2107762398_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set2107762398_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_2107762398_Y", value);
    }

    public void set2107762398_YToDefault() {
        set2107762398_Y(0.0f);
    }

    public SharedPreferences.Editor set2107762398_YToDefault(SharedPreferences.Editor editor) {
        return set2107762398_Y(0.0f, editor);
    }

    public float get2107762398_R() {
        return get2107762398_R(getSharedPreferences());
    }

    public float get2107762398_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_2107762398_R", Float.MIN_VALUE);
    }

    public void set2107762398_R(float value) {
        set2107762398_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set2107762398_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_2107762398_R", value);
    }

    public void set2107762398_RToDefault() {
        set2107762398_R(0.0f);
    }

    public SharedPreferences.Editor set2107762398_RToDefault(SharedPreferences.Editor editor) {
        return set2107762398_R(0.0f, editor);
    }

    public void load1861710961(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1861710961_X(sharedPreferences);
        float y = get1861710961_Y(sharedPreferences);
        float r = get1861710961_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1861710961(SharedPreferences.Editor editor, Puzzle p) {
        set1861710961_X(p.getPositionInDesktop().getX(), editor);
        set1861710961_Y(p.getPositionInDesktop().getY(), editor);
        set1861710961_R(p.getRotation(), editor);
    }

    public float get1861710961_X() {
        return get1861710961_X(getSharedPreferences());
    }

    public float get1861710961_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1861710961_X", Float.MIN_VALUE);
    }

    public void set1861710961_X(float value) {
        set1861710961_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1861710961_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1861710961_X", value);
    }

    public void set1861710961_XToDefault() {
        set1861710961_X(0.0f);
    }

    public SharedPreferences.Editor set1861710961_XToDefault(SharedPreferences.Editor editor) {
        return set1861710961_X(0.0f, editor);
    }

    public float get1861710961_Y() {
        return get1861710961_Y(getSharedPreferences());
    }

    public float get1861710961_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1861710961_Y", Float.MIN_VALUE);
    }

    public void set1861710961_Y(float value) {
        set1861710961_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1861710961_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1861710961_Y", value);
    }

    public void set1861710961_YToDefault() {
        set1861710961_Y(0.0f);
    }

    public SharedPreferences.Editor set1861710961_YToDefault(SharedPreferences.Editor editor) {
        return set1861710961_Y(0.0f, editor);
    }

    public float get1861710961_R() {
        return get1861710961_R(getSharedPreferences());
    }

    public float get1861710961_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1861710961_R", Float.MIN_VALUE);
    }

    public void set1861710961_R(float value) {
        set1861710961_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1861710961_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1861710961_R", value);
    }

    public void set1861710961_RToDefault() {
        set1861710961_R(0.0f);
    }

    public SharedPreferences.Editor set1861710961_RToDefault(SharedPreferences.Editor editor) {
        return set1861710961_R(0.0f, editor);
    }

    public void load180976878(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get180976878_X(sharedPreferences);
        float y = get180976878_Y(sharedPreferences);
        float r = get180976878_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save180976878(SharedPreferences.Editor editor, Puzzle p) {
        set180976878_X(p.getPositionInDesktop().getX(), editor);
        set180976878_Y(p.getPositionInDesktop().getY(), editor);
        set180976878_R(p.getRotation(), editor);
    }

    public float get180976878_X() {
        return get180976878_X(getSharedPreferences());
    }

    public float get180976878_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_180976878_X", Float.MIN_VALUE);
    }

    public void set180976878_X(float value) {
        set180976878_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set180976878_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_180976878_X", value);
    }

    public void set180976878_XToDefault() {
        set180976878_X(0.0f);
    }

    public SharedPreferences.Editor set180976878_XToDefault(SharedPreferences.Editor editor) {
        return set180976878_X(0.0f, editor);
    }

    public float get180976878_Y() {
        return get180976878_Y(getSharedPreferences());
    }

    public float get180976878_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_180976878_Y", Float.MIN_VALUE);
    }

    public void set180976878_Y(float value) {
        set180976878_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set180976878_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_180976878_Y", value);
    }

    public void set180976878_YToDefault() {
        set180976878_Y(0.0f);
    }

    public SharedPreferences.Editor set180976878_YToDefault(SharedPreferences.Editor editor) {
        return set180976878_Y(0.0f, editor);
    }

    public float get180976878_R() {
        return get180976878_R(getSharedPreferences());
    }

    public float get180976878_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_180976878_R", Float.MIN_VALUE);
    }

    public void set180976878_R(float value) {
        set180976878_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set180976878_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_180976878_R", value);
    }

    public void set180976878_RToDefault() {
        set180976878_R(0.0f);
    }

    public SharedPreferences.Editor set180976878_RToDefault(SharedPreferences.Editor editor) {
        return set180976878_R(0.0f, editor);
    }

    public void load1626761966(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1626761966_X(sharedPreferences);
        float y = get1626761966_Y(sharedPreferences);
        float r = get1626761966_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1626761966(SharedPreferences.Editor editor, Puzzle p) {
        set1626761966_X(p.getPositionInDesktop().getX(), editor);
        set1626761966_Y(p.getPositionInDesktop().getY(), editor);
        set1626761966_R(p.getRotation(), editor);
    }

    public float get1626761966_X() {
        return get1626761966_X(getSharedPreferences());
    }

    public float get1626761966_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1626761966_X", Float.MIN_VALUE);
    }

    public void set1626761966_X(float value) {
        set1626761966_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1626761966_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1626761966_X", value);
    }

    public void set1626761966_XToDefault() {
        set1626761966_X(0.0f);
    }

    public SharedPreferences.Editor set1626761966_XToDefault(SharedPreferences.Editor editor) {
        return set1626761966_X(0.0f, editor);
    }

    public float get1626761966_Y() {
        return get1626761966_Y(getSharedPreferences());
    }

    public float get1626761966_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1626761966_Y", Float.MIN_VALUE);
    }

    public void set1626761966_Y(float value) {
        set1626761966_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1626761966_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1626761966_Y", value);
    }

    public void set1626761966_YToDefault() {
        set1626761966_Y(0.0f);
    }

    public SharedPreferences.Editor set1626761966_YToDefault(SharedPreferences.Editor editor) {
        return set1626761966_Y(0.0f, editor);
    }

    public float get1626761966_R() {
        return get1626761966_R(getSharedPreferences());
    }

    public float get1626761966_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1626761966_R", Float.MIN_VALUE);
    }

    public void set1626761966_R(float value) {
        set1626761966_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1626761966_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1626761966_R", value);
    }

    public void set1626761966_RToDefault() {
        set1626761966_R(0.0f);
    }

    public SharedPreferences.Editor set1626761966_RToDefault(SharedPreferences.Editor editor) {
        return set1626761966_R(0.0f, editor);
    }

    public void load1192911393(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1192911393_X(sharedPreferences);
        float y = get1192911393_Y(sharedPreferences);
        float r = get1192911393_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1192911393(SharedPreferences.Editor editor, Puzzle p) {
        set1192911393_X(p.getPositionInDesktop().getX(), editor);
        set1192911393_Y(p.getPositionInDesktop().getY(), editor);
        set1192911393_R(p.getRotation(), editor);
    }

    public float get1192911393_X() {
        return get1192911393_X(getSharedPreferences());
    }

    public float get1192911393_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1192911393_X", Float.MIN_VALUE);
    }

    public void set1192911393_X(float value) {
        set1192911393_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1192911393_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1192911393_X", value);
    }

    public void set1192911393_XToDefault() {
        set1192911393_X(0.0f);
    }

    public SharedPreferences.Editor set1192911393_XToDefault(SharedPreferences.Editor editor) {
        return set1192911393_X(0.0f, editor);
    }

    public float get1192911393_Y() {
        return get1192911393_Y(getSharedPreferences());
    }

    public float get1192911393_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1192911393_Y", Float.MIN_VALUE);
    }

    public void set1192911393_Y(float value) {
        set1192911393_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1192911393_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1192911393_Y", value);
    }

    public void set1192911393_YToDefault() {
        set1192911393_Y(0.0f);
    }

    public SharedPreferences.Editor set1192911393_YToDefault(SharedPreferences.Editor editor) {
        return set1192911393_Y(0.0f, editor);
    }

    public float get1192911393_R() {
        return get1192911393_R(getSharedPreferences());
    }

    public float get1192911393_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1192911393_R", Float.MIN_VALUE);
    }

    public void set1192911393_R(float value) {
        set1192911393_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1192911393_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1192911393_R", value);
    }

    public void set1192911393_RToDefault() {
        set1192911393_R(0.0f);
    }

    public SharedPreferences.Editor set1192911393_RToDefault(SharedPreferences.Editor editor) {
        return set1192911393_R(0.0f, editor);
    }

    public void load771831799(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get771831799_X(sharedPreferences);
        float y = get771831799_Y(sharedPreferences);
        float r = get771831799_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save771831799(SharedPreferences.Editor editor, Puzzle p) {
        set771831799_X(p.getPositionInDesktop().getX(), editor);
        set771831799_Y(p.getPositionInDesktop().getY(), editor);
        set771831799_R(p.getRotation(), editor);
    }

    public float get771831799_X() {
        return get771831799_X(getSharedPreferences());
    }

    public float get771831799_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_771831799_X", Float.MIN_VALUE);
    }

    public void set771831799_X(float value) {
        set771831799_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set771831799_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_771831799_X", value);
    }

    public void set771831799_XToDefault() {
        set771831799_X(0.0f);
    }

    public SharedPreferences.Editor set771831799_XToDefault(SharedPreferences.Editor editor) {
        return set771831799_X(0.0f, editor);
    }

    public float get771831799_Y() {
        return get771831799_Y(getSharedPreferences());
    }

    public float get771831799_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_771831799_Y", Float.MIN_VALUE);
    }

    public void set771831799_Y(float value) {
        set771831799_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set771831799_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_771831799_Y", value);
    }

    public void set771831799_YToDefault() {
        set771831799_Y(0.0f);
    }

    public SharedPreferences.Editor set771831799_YToDefault(SharedPreferences.Editor editor) {
        return set771831799_Y(0.0f, editor);
    }

    public float get771831799_R() {
        return get771831799_R(getSharedPreferences());
    }

    public float get771831799_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_771831799_R", Float.MIN_VALUE);
    }

    public void set771831799_R(float value) {
        set771831799_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set771831799_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_771831799_R", value);
    }

    public void set771831799_RToDefault() {
        set771831799_R(0.0f);
    }

    public SharedPreferences.Editor set771831799_RToDefault(SharedPreferences.Editor editor) {
        return set771831799_R(0.0f, editor);
    }

    public void load1144198189(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1144198189_X(sharedPreferences);
        float y = get1144198189_Y(sharedPreferences);
        float r = get1144198189_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1144198189(SharedPreferences.Editor editor, Puzzle p) {
        set1144198189_X(p.getPositionInDesktop().getX(), editor);
        set1144198189_Y(p.getPositionInDesktop().getY(), editor);
        set1144198189_R(p.getRotation(), editor);
    }

    public float get1144198189_X() {
        return get1144198189_X(getSharedPreferences());
    }

    public float get1144198189_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1144198189_X", Float.MIN_VALUE);
    }

    public void set1144198189_X(float value) {
        set1144198189_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1144198189_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1144198189_X", value);
    }

    public void set1144198189_XToDefault() {
        set1144198189_X(0.0f);
    }

    public SharedPreferences.Editor set1144198189_XToDefault(SharedPreferences.Editor editor) {
        return set1144198189_X(0.0f, editor);
    }

    public float get1144198189_Y() {
        return get1144198189_Y(getSharedPreferences());
    }

    public float get1144198189_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1144198189_Y", Float.MIN_VALUE);
    }

    public void set1144198189_Y(float value) {
        set1144198189_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1144198189_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1144198189_Y", value);
    }

    public void set1144198189_YToDefault() {
        set1144198189_Y(0.0f);
    }

    public SharedPreferences.Editor set1144198189_YToDefault(SharedPreferences.Editor editor) {
        return set1144198189_Y(0.0f, editor);
    }

    public float get1144198189_R() {
        return get1144198189_R(getSharedPreferences());
    }

    public float get1144198189_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1144198189_R", Float.MIN_VALUE);
    }

    public void set1144198189_R(float value) {
        set1144198189_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1144198189_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1144198189_R", value);
    }

    public void set1144198189_RToDefault() {
        set1144198189_R(0.0f);
    }

    public SharedPreferences.Editor set1144198189_RToDefault(SharedPreferences.Editor editor) {
        return set1144198189_R(0.0f, editor);
    }

    public void load1523076608(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1523076608_X(sharedPreferences);
        float y = get1523076608_Y(sharedPreferences);
        float r = get1523076608_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1523076608(SharedPreferences.Editor editor, Puzzle p) {
        set1523076608_X(p.getPositionInDesktop().getX(), editor);
        set1523076608_Y(p.getPositionInDesktop().getY(), editor);
        set1523076608_R(p.getRotation(), editor);
    }

    public float get1523076608_X() {
        return get1523076608_X(getSharedPreferences());
    }

    public float get1523076608_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1523076608_X", Float.MIN_VALUE);
    }

    public void set1523076608_X(float value) {
        set1523076608_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1523076608_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1523076608_X", value);
    }

    public void set1523076608_XToDefault() {
        set1523076608_X(0.0f);
    }

    public SharedPreferences.Editor set1523076608_XToDefault(SharedPreferences.Editor editor) {
        return set1523076608_X(0.0f, editor);
    }

    public float get1523076608_Y() {
        return get1523076608_Y(getSharedPreferences());
    }

    public float get1523076608_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1523076608_Y", Float.MIN_VALUE);
    }

    public void set1523076608_Y(float value) {
        set1523076608_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1523076608_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1523076608_Y", value);
    }

    public void set1523076608_YToDefault() {
        set1523076608_Y(0.0f);
    }

    public SharedPreferences.Editor set1523076608_YToDefault(SharedPreferences.Editor editor) {
        return set1523076608_Y(0.0f, editor);
    }

    public float get1523076608_R() {
        return get1523076608_R(getSharedPreferences());
    }

    public float get1523076608_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1523076608_R", Float.MIN_VALUE);
    }

    public void set1523076608_R(float value) {
        set1523076608_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1523076608_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1523076608_R", value);
    }

    public void set1523076608_RToDefault() {
        set1523076608_R(0.0f);
    }

    public SharedPreferences.Editor set1523076608_RToDefault(SharedPreferences.Editor editor) {
        return set1523076608_R(0.0f, editor);
    }

    public void load1785268698(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1785268698_X(sharedPreferences);
        float y = get1785268698_Y(sharedPreferences);
        float r = get1785268698_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1785268698(SharedPreferences.Editor editor, Puzzle p) {
        set1785268698_X(p.getPositionInDesktop().getX(), editor);
        set1785268698_Y(p.getPositionInDesktop().getY(), editor);
        set1785268698_R(p.getRotation(), editor);
    }

    public float get1785268698_X() {
        return get1785268698_X(getSharedPreferences());
    }

    public float get1785268698_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1785268698_X", Float.MIN_VALUE);
    }

    public void set1785268698_X(float value) {
        set1785268698_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1785268698_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1785268698_X", value);
    }

    public void set1785268698_XToDefault() {
        set1785268698_X(0.0f);
    }

    public SharedPreferences.Editor set1785268698_XToDefault(SharedPreferences.Editor editor) {
        return set1785268698_X(0.0f, editor);
    }

    public float get1785268698_Y() {
        return get1785268698_Y(getSharedPreferences());
    }

    public float get1785268698_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1785268698_Y", Float.MIN_VALUE);
    }

    public void set1785268698_Y(float value) {
        set1785268698_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1785268698_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1785268698_Y", value);
    }

    public void set1785268698_YToDefault() {
        set1785268698_Y(0.0f);
    }

    public SharedPreferences.Editor set1785268698_YToDefault(SharedPreferences.Editor editor) {
        return set1785268698_Y(0.0f, editor);
    }

    public float get1785268698_R() {
        return get1785268698_R(getSharedPreferences());
    }

    public float get1785268698_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1785268698_R", Float.MIN_VALUE);
    }

    public void set1785268698_R(float value) {
        set1785268698_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1785268698_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1785268698_R", value);
    }

    public void set1785268698_RToDefault() {
        set1785268698_R(0.0f);
    }

    public SharedPreferences.Editor set1785268698_RToDefault(SharedPreferences.Editor editor) {
        return set1785268698_R(0.0f, editor);
    }

    public void load2074854323(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get2074854323_X(sharedPreferences);
        float y = get2074854323_Y(sharedPreferences);
        float r = get2074854323_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save2074854323(SharedPreferences.Editor editor, Puzzle p) {
        set2074854323_X(p.getPositionInDesktop().getX(), editor);
        set2074854323_Y(p.getPositionInDesktop().getY(), editor);
        set2074854323_R(p.getRotation(), editor);
    }

    public float get2074854323_X() {
        return get2074854323_X(getSharedPreferences());
    }

    public float get2074854323_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_2074854323_X", Float.MIN_VALUE);
    }

    public void set2074854323_X(float value) {
        set2074854323_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set2074854323_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_2074854323_X", value);
    }

    public void set2074854323_XToDefault() {
        set2074854323_X(0.0f);
    }

    public SharedPreferences.Editor set2074854323_XToDefault(SharedPreferences.Editor editor) {
        return set2074854323_X(0.0f, editor);
    }

    public float get2074854323_Y() {
        return get2074854323_Y(getSharedPreferences());
    }

    public float get2074854323_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_2074854323_Y", Float.MIN_VALUE);
    }

    public void set2074854323_Y(float value) {
        set2074854323_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set2074854323_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_2074854323_Y", value);
    }

    public void set2074854323_YToDefault() {
        set2074854323_Y(0.0f);
    }

    public SharedPreferences.Editor set2074854323_YToDefault(SharedPreferences.Editor editor) {
        return set2074854323_Y(0.0f, editor);
    }

    public float get2074854323_R() {
        return get2074854323_R(getSharedPreferences());
    }

    public float get2074854323_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_2074854323_R", Float.MIN_VALUE);
    }

    public void set2074854323_R(float value) {
        set2074854323_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set2074854323_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_2074854323_R", value);
    }

    public void set2074854323_RToDefault() {
        set2074854323_R(0.0f);
    }

    public SharedPreferences.Editor set2074854323_RToDefault(SharedPreferences.Editor editor) {
        return set2074854323_R(0.0f, editor);
    }

    public void load1807296321(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1807296321_X(sharedPreferences);
        float y = get1807296321_Y(sharedPreferences);
        float r = get1807296321_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1807296321(SharedPreferences.Editor editor, Puzzle p) {
        set1807296321_X(p.getPositionInDesktop().getX(), editor);
        set1807296321_Y(p.getPositionInDesktop().getY(), editor);
        set1807296321_R(p.getRotation(), editor);
    }

    public float get1807296321_X() {
        return get1807296321_X(getSharedPreferences());
    }

    public float get1807296321_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1807296321_X", Float.MIN_VALUE);
    }

    public void set1807296321_X(float value) {
        set1807296321_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1807296321_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1807296321_X", value);
    }

    public void set1807296321_XToDefault() {
        set1807296321_X(0.0f);
    }

    public SharedPreferences.Editor set1807296321_XToDefault(SharedPreferences.Editor editor) {
        return set1807296321_X(0.0f, editor);
    }

    public float get1807296321_Y() {
        return get1807296321_Y(getSharedPreferences());
    }

    public float get1807296321_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1807296321_Y", Float.MIN_VALUE);
    }

    public void set1807296321_Y(float value) {
        set1807296321_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1807296321_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1807296321_Y", value);
    }

    public void set1807296321_YToDefault() {
        set1807296321_Y(0.0f);
    }

    public SharedPreferences.Editor set1807296321_YToDefault(SharedPreferences.Editor editor) {
        return set1807296321_Y(0.0f, editor);
    }

    public float get1807296321_R() {
        return get1807296321_R(getSharedPreferences());
    }

    public float get1807296321_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1807296321_R", Float.MIN_VALUE);
    }

    public void set1807296321_R(float value) {
        set1807296321_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1807296321_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1807296321_R", value);
    }

    public void set1807296321_RToDefault() {
        set1807296321_R(0.0f);
    }

    public SharedPreferences.Editor set1807296321_RToDefault(SharedPreferences.Editor editor) {
        return set1807296321_R(0.0f, editor);
    }

    public void load391079276(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get391079276_X(sharedPreferences);
        float y = get391079276_Y(sharedPreferences);
        float r = get391079276_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save391079276(SharedPreferences.Editor editor, Puzzle p) {
        set391079276_X(p.getPositionInDesktop().getX(), editor);
        set391079276_Y(p.getPositionInDesktop().getY(), editor);
        set391079276_R(p.getRotation(), editor);
    }

    public float get391079276_X() {
        return get391079276_X(getSharedPreferences());
    }

    public float get391079276_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_391079276_X", Float.MIN_VALUE);
    }

    public void set391079276_X(float value) {
        set391079276_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set391079276_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_391079276_X", value);
    }

    public void set391079276_XToDefault() {
        set391079276_X(0.0f);
    }

    public SharedPreferences.Editor set391079276_XToDefault(SharedPreferences.Editor editor) {
        return set391079276_X(0.0f, editor);
    }

    public float get391079276_Y() {
        return get391079276_Y(getSharedPreferences());
    }

    public float get391079276_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_391079276_Y", Float.MIN_VALUE);
    }

    public void set391079276_Y(float value) {
        set391079276_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set391079276_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_391079276_Y", value);
    }

    public void set391079276_YToDefault() {
        set391079276_Y(0.0f);
    }

    public SharedPreferences.Editor set391079276_YToDefault(SharedPreferences.Editor editor) {
        return set391079276_Y(0.0f, editor);
    }

    public float get391079276_R() {
        return get391079276_R(getSharedPreferences());
    }

    public float get391079276_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_391079276_R", Float.MIN_VALUE);
    }

    public void set391079276_R(float value) {
        set391079276_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set391079276_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_391079276_R", value);
    }

    public void set391079276_RToDefault() {
        set391079276_R(0.0f);
    }

    public SharedPreferences.Editor set391079276_RToDefault(SharedPreferences.Editor editor) {
        return set391079276_R(0.0f, editor);
    }

    public void load1000323591(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1000323591_X(sharedPreferences);
        float y = get1000323591_Y(sharedPreferences);
        float r = get1000323591_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1000323591(SharedPreferences.Editor editor, Puzzle p) {
        set1000323591_X(p.getPositionInDesktop().getX(), editor);
        set1000323591_Y(p.getPositionInDesktop().getY(), editor);
        set1000323591_R(p.getRotation(), editor);
    }

    public float get1000323591_X() {
        return get1000323591_X(getSharedPreferences());
    }

    public float get1000323591_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1000323591_X", Float.MIN_VALUE);
    }

    public void set1000323591_X(float value) {
        set1000323591_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1000323591_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1000323591_X", value);
    }

    public void set1000323591_XToDefault() {
        set1000323591_X(0.0f);
    }

    public SharedPreferences.Editor set1000323591_XToDefault(SharedPreferences.Editor editor) {
        return set1000323591_X(0.0f, editor);
    }

    public float get1000323591_Y() {
        return get1000323591_Y(getSharedPreferences());
    }

    public float get1000323591_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1000323591_Y", Float.MIN_VALUE);
    }

    public void set1000323591_Y(float value) {
        set1000323591_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1000323591_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1000323591_Y", value);
    }

    public void set1000323591_YToDefault() {
        set1000323591_Y(0.0f);
    }

    public SharedPreferences.Editor set1000323591_YToDefault(SharedPreferences.Editor editor) {
        return set1000323591_Y(0.0f, editor);
    }

    public float get1000323591_R() {
        return get1000323591_R(getSharedPreferences());
    }

    public float get1000323591_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1000323591_R", Float.MIN_VALUE);
    }

    public void set1000323591_R(float value) {
        set1000323591_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1000323591_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1000323591_R", value);
    }

    public void set1000323591_RToDefault() {
        set1000323591_R(0.0f);
    }

    public SharedPreferences.Editor set1000323591_RToDefault(SharedPreferences.Editor editor) {
        return set1000323591_R(0.0f, editor);
    }

    public void load1640195765(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1640195765_X(sharedPreferences);
        float y = get1640195765_Y(sharedPreferences);
        float r = get1640195765_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1640195765(SharedPreferences.Editor editor, Puzzle p) {
        set1640195765_X(p.getPositionInDesktop().getX(), editor);
        set1640195765_Y(p.getPositionInDesktop().getY(), editor);
        set1640195765_R(p.getRotation(), editor);
    }

    public float get1640195765_X() {
        return get1640195765_X(getSharedPreferences());
    }

    public float get1640195765_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1640195765_X", Float.MIN_VALUE);
    }

    public void set1640195765_X(float value) {
        set1640195765_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1640195765_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1640195765_X", value);
    }

    public void set1640195765_XToDefault() {
        set1640195765_X(0.0f);
    }

    public SharedPreferences.Editor set1640195765_XToDefault(SharedPreferences.Editor editor) {
        return set1640195765_X(0.0f, editor);
    }

    public float get1640195765_Y() {
        return get1640195765_Y(getSharedPreferences());
    }

    public float get1640195765_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1640195765_Y", Float.MIN_VALUE);
    }

    public void set1640195765_Y(float value) {
        set1640195765_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1640195765_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1640195765_Y", value);
    }

    public void set1640195765_YToDefault() {
        set1640195765_Y(0.0f);
    }

    public SharedPreferences.Editor set1640195765_YToDefault(SharedPreferences.Editor editor) {
        return set1640195765_Y(0.0f, editor);
    }

    public float get1640195765_R() {
        return get1640195765_R(getSharedPreferences());
    }

    public float get1640195765_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1640195765_R", Float.MIN_VALUE);
    }

    public void set1640195765_R(float value) {
        set1640195765_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1640195765_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1640195765_R", value);
    }

    public void set1640195765_RToDefault() {
        set1640195765_R(0.0f);
    }

    public SharedPreferences.Editor set1640195765_RToDefault(SharedPreferences.Editor editor) {
        return set1640195765_R(0.0f, editor);
    }

    public void load1125358841(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1125358841_X(sharedPreferences);
        float y = get1125358841_Y(sharedPreferences);
        float r = get1125358841_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1125358841(SharedPreferences.Editor editor, Puzzle p) {
        set1125358841_X(p.getPositionInDesktop().getX(), editor);
        set1125358841_Y(p.getPositionInDesktop().getY(), editor);
        set1125358841_R(p.getRotation(), editor);
    }

    public float get1125358841_X() {
        return get1125358841_X(getSharedPreferences());
    }

    public float get1125358841_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1125358841_X", Float.MIN_VALUE);
    }

    public void set1125358841_X(float value) {
        set1125358841_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1125358841_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1125358841_X", value);
    }

    public void set1125358841_XToDefault() {
        set1125358841_X(0.0f);
    }

    public SharedPreferences.Editor set1125358841_XToDefault(SharedPreferences.Editor editor) {
        return set1125358841_X(0.0f, editor);
    }

    public float get1125358841_Y() {
        return get1125358841_Y(getSharedPreferences());
    }

    public float get1125358841_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1125358841_Y", Float.MIN_VALUE);
    }

    public void set1125358841_Y(float value) {
        set1125358841_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1125358841_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1125358841_Y", value);
    }

    public void set1125358841_YToDefault() {
        set1125358841_Y(0.0f);
    }

    public SharedPreferences.Editor set1125358841_YToDefault(SharedPreferences.Editor editor) {
        return set1125358841_Y(0.0f, editor);
    }

    public float get1125358841_R() {
        return get1125358841_R(getSharedPreferences());
    }

    public float get1125358841_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1125358841_R", Float.MIN_VALUE);
    }

    public void set1125358841_R(float value) {
        set1125358841_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1125358841_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1125358841_R", value);
    }

    public void set1125358841_RToDefault() {
        set1125358841_R(0.0f);
    }

    public SharedPreferences.Editor set1125358841_RToDefault(SharedPreferences.Editor editor) {
        return set1125358841_R(0.0f, editor);
    }

    public void load1854851027(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1854851027_X(sharedPreferences);
        float y = get1854851027_Y(sharedPreferences);
        float r = get1854851027_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1854851027(SharedPreferences.Editor editor, Puzzle p) {
        set1854851027_X(p.getPositionInDesktop().getX(), editor);
        set1854851027_Y(p.getPositionInDesktop().getY(), editor);
        set1854851027_R(p.getRotation(), editor);
    }

    public float get1854851027_X() {
        return get1854851027_X(getSharedPreferences());
    }

    public float get1854851027_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1854851027_X", Float.MIN_VALUE);
    }

    public void set1854851027_X(float value) {
        set1854851027_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1854851027_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1854851027_X", value);
    }

    public void set1854851027_XToDefault() {
        set1854851027_X(0.0f);
    }

    public SharedPreferences.Editor set1854851027_XToDefault(SharedPreferences.Editor editor) {
        return set1854851027_X(0.0f, editor);
    }

    public float get1854851027_Y() {
        return get1854851027_Y(getSharedPreferences());
    }

    public float get1854851027_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1854851027_Y", Float.MIN_VALUE);
    }

    public void set1854851027_Y(float value) {
        set1854851027_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1854851027_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1854851027_Y", value);
    }

    public void set1854851027_YToDefault() {
        set1854851027_Y(0.0f);
    }

    public SharedPreferences.Editor set1854851027_YToDefault(SharedPreferences.Editor editor) {
        return set1854851027_Y(0.0f, editor);
    }

    public float get1854851027_R() {
        return get1854851027_R(getSharedPreferences());
    }

    public float get1854851027_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1854851027_R", Float.MIN_VALUE);
    }

    public void set1854851027_R(float value) {
        set1854851027_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1854851027_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1854851027_R", value);
    }

    public void set1854851027_RToDefault() {
        set1854851027_R(0.0f);
    }

    public SharedPreferences.Editor set1854851027_RToDefault(SharedPreferences.Editor editor) {
        return set1854851027_R(0.0f, editor);
    }

    public void load1482253141(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1482253141_X(sharedPreferences);
        float y = get1482253141_Y(sharedPreferences);
        float r = get1482253141_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1482253141(SharedPreferences.Editor editor, Puzzle p) {
        set1482253141_X(p.getPositionInDesktop().getX(), editor);
        set1482253141_Y(p.getPositionInDesktop().getY(), editor);
        set1482253141_R(p.getRotation(), editor);
    }

    public float get1482253141_X() {
        return get1482253141_X(getSharedPreferences());
    }

    public float get1482253141_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1482253141_X", Float.MIN_VALUE);
    }

    public void set1482253141_X(float value) {
        set1482253141_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1482253141_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1482253141_X", value);
    }

    public void set1482253141_XToDefault() {
        set1482253141_X(0.0f);
    }

    public SharedPreferences.Editor set1482253141_XToDefault(SharedPreferences.Editor editor) {
        return set1482253141_X(0.0f, editor);
    }

    public float get1482253141_Y() {
        return get1482253141_Y(getSharedPreferences());
    }

    public float get1482253141_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1482253141_Y", Float.MIN_VALUE);
    }

    public void set1482253141_Y(float value) {
        set1482253141_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1482253141_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1482253141_Y", value);
    }

    public void set1482253141_YToDefault() {
        set1482253141_Y(0.0f);
    }

    public SharedPreferences.Editor set1482253141_YToDefault(SharedPreferences.Editor editor) {
        return set1482253141_Y(0.0f, editor);
    }

    public float get1482253141_R() {
        return get1482253141_R(getSharedPreferences());
    }

    public float get1482253141_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1482253141_R", Float.MIN_VALUE);
    }

    public void set1482253141_R(float value) {
        set1482253141_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1482253141_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1482253141_R", value);
    }

    public void set1482253141_RToDefault() {
        set1482253141_R(0.0f);
    }

    public SharedPreferences.Editor set1482253141_RToDefault(SharedPreferences.Editor editor) {
        return set1482253141_R(0.0f, editor);
    }

    public void load1860648131(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1860648131_X(sharedPreferences);
        float y = get1860648131_Y(sharedPreferences);
        float r = get1860648131_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1860648131(SharedPreferences.Editor editor, Puzzle p) {
        set1860648131_X(p.getPositionInDesktop().getX(), editor);
        set1860648131_Y(p.getPositionInDesktop().getY(), editor);
        set1860648131_R(p.getRotation(), editor);
    }

    public float get1860648131_X() {
        return get1860648131_X(getSharedPreferences());
    }

    public float get1860648131_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1860648131_X", Float.MIN_VALUE);
    }

    public void set1860648131_X(float value) {
        set1860648131_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1860648131_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1860648131_X", value);
    }

    public void set1860648131_XToDefault() {
        set1860648131_X(0.0f);
    }

    public SharedPreferences.Editor set1860648131_XToDefault(SharedPreferences.Editor editor) {
        return set1860648131_X(0.0f, editor);
    }

    public float get1860648131_Y() {
        return get1860648131_Y(getSharedPreferences());
    }

    public float get1860648131_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1860648131_Y", Float.MIN_VALUE);
    }

    public void set1860648131_Y(float value) {
        set1860648131_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1860648131_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1860648131_Y", value);
    }

    public void set1860648131_YToDefault() {
        set1860648131_Y(0.0f);
    }

    public SharedPreferences.Editor set1860648131_YToDefault(SharedPreferences.Editor editor) {
        return set1860648131_Y(0.0f, editor);
    }

    public float get1860648131_R() {
        return get1860648131_R(getSharedPreferences());
    }

    public float get1860648131_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1860648131_R", Float.MIN_VALUE);
    }

    public void set1860648131_R(float value) {
        set1860648131_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1860648131_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1860648131_R", value);
    }

    public void set1860648131_RToDefault() {
        set1860648131_R(0.0f);
    }

    public SharedPreferences.Editor set1860648131_RToDefault(SharedPreferences.Editor editor) {
        return set1860648131_R(0.0f, editor);
    }

    public void load446019339(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get446019339_X(sharedPreferences);
        float y = get446019339_Y(sharedPreferences);
        float r = get446019339_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save446019339(SharedPreferences.Editor editor, Puzzle p) {
        set446019339_X(p.getPositionInDesktop().getX(), editor);
        set446019339_Y(p.getPositionInDesktop().getY(), editor);
        set446019339_R(p.getRotation(), editor);
    }

    public float get446019339_X() {
        return get446019339_X(getSharedPreferences());
    }

    public float get446019339_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_446019339_X", Float.MIN_VALUE);
    }

    public void set446019339_X(float value) {
        set446019339_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set446019339_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_446019339_X", value);
    }

    public void set446019339_XToDefault() {
        set446019339_X(0.0f);
    }

    public SharedPreferences.Editor set446019339_XToDefault(SharedPreferences.Editor editor) {
        return set446019339_X(0.0f, editor);
    }

    public float get446019339_Y() {
        return get446019339_Y(getSharedPreferences());
    }

    public float get446019339_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_446019339_Y", Float.MIN_VALUE);
    }

    public void set446019339_Y(float value) {
        set446019339_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set446019339_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_446019339_Y", value);
    }

    public void set446019339_YToDefault() {
        set446019339_Y(0.0f);
    }

    public SharedPreferences.Editor set446019339_YToDefault(SharedPreferences.Editor editor) {
        return set446019339_Y(0.0f, editor);
    }

    public float get446019339_R() {
        return get446019339_R(getSharedPreferences());
    }

    public float get446019339_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_446019339_R", Float.MIN_VALUE);
    }

    public void set446019339_R(float value) {
        set446019339_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set446019339_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_446019339_R", value);
    }

    public void set446019339_RToDefault() {
        set446019339_R(0.0f);
    }

    public SharedPreferences.Editor set446019339_RToDefault(SharedPreferences.Editor editor) {
        return set446019339_R(0.0f, editor);
    }

    public void load1146332050(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1146332050_X(sharedPreferences);
        float y = get1146332050_Y(sharedPreferences);
        float r = get1146332050_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1146332050(SharedPreferences.Editor editor, Puzzle p) {
        set1146332050_X(p.getPositionInDesktop().getX(), editor);
        set1146332050_Y(p.getPositionInDesktop().getY(), editor);
        set1146332050_R(p.getRotation(), editor);
    }

    public float get1146332050_X() {
        return get1146332050_X(getSharedPreferences());
    }

    public float get1146332050_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1146332050_X", Float.MIN_VALUE);
    }

    public void set1146332050_X(float value) {
        set1146332050_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1146332050_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1146332050_X", value);
    }

    public void set1146332050_XToDefault() {
        set1146332050_X(0.0f);
    }

    public SharedPreferences.Editor set1146332050_XToDefault(SharedPreferences.Editor editor) {
        return set1146332050_X(0.0f, editor);
    }

    public float get1146332050_Y() {
        return get1146332050_Y(getSharedPreferences());
    }

    public float get1146332050_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1146332050_Y", Float.MIN_VALUE);
    }

    public void set1146332050_Y(float value) {
        set1146332050_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1146332050_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1146332050_Y", value);
    }

    public void set1146332050_YToDefault() {
        set1146332050_Y(0.0f);
    }

    public SharedPreferences.Editor set1146332050_YToDefault(SharedPreferences.Editor editor) {
        return set1146332050_Y(0.0f, editor);
    }

    public float get1146332050_R() {
        return get1146332050_R(getSharedPreferences());
    }

    public float get1146332050_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1146332050_R", Float.MIN_VALUE);
    }

    public void set1146332050_R(float value) {
        set1146332050_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1146332050_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1146332050_R", value);
    }

    public void set1146332050_RToDefault() {
        set1146332050_R(0.0f);
    }

    public SharedPreferences.Editor set1146332050_RToDefault(SharedPreferences.Editor editor) {
        return set1146332050_R(0.0f, editor);
    }

    public void load105157770(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get105157770_X(sharedPreferences);
        float y = get105157770_Y(sharedPreferences);
        float r = get105157770_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save105157770(SharedPreferences.Editor editor, Puzzle p) {
        set105157770_X(p.getPositionInDesktop().getX(), editor);
        set105157770_Y(p.getPositionInDesktop().getY(), editor);
        set105157770_R(p.getRotation(), editor);
    }

    public float get105157770_X() {
        return get105157770_X(getSharedPreferences());
    }

    public float get105157770_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_105157770_X", Float.MIN_VALUE);
    }

    public void set105157770_X(float value) {
        set105157770_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set105157770_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_105157770_X", value);
    }

    public void set105157770_XToDefault() {
        set105157770_X(0.0f);
    }

    public SharedPreferences.Editor set105157770_XToDefault(SharedPreferences.Editor editor) {
        return set105157770_X(0.0f, editor);
    }

    public float get105157770_Y() {
        return get105157770_Y(getSharedPreferences());
    }

    public float get105157770_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_105157770_Y", Float.MIN_VALUE);
    }

    public void set105157770_Y(float value) {
        set105157770_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set105157770_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_105157770_Y", value);
    }

    public void set105157770_YToDefault() {
        set105157770_Y(0.0f);
    }

    public SharedPreferences.Editor set105157770_YToDefault(SharedPreferences.Editor editor) {
        return set105157770_Y(0.0f, editor);
    }

    public float get105157770_R() {
        return get105157770_R(getSharedPreferences());
    }

    public float get105157770_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_105157770_R", Float.MIN_VALUE);
    }

    public void set105157770_R(float value) {
        set105157770_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set105157770_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_105157770_R", value);
    }

    public void set105157770_RToDefault() {
        set105157770_R(0.0f);
    }

    public SharedPreferences.Editor set105157770_RToDefault(SharedPreferences.Editor editor) {
        return set105157770_R(0.0f, editor);
    }

    public void load718940536(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get718940536_X(sharedPreferences);
        float y = get718940536_Y(sharedPreferences);
        float r = get718940536_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save718940536(SharedPreferences.Editor editor, Puzzle p) {
        set718940536_X(p.getPositionInDesktop().getX(), editor);
        set718940536_Y(p.getPositionInDesktop().getY(), editor);
        set718940536_R(p.getRotation(), editor);
    }

    public float get718940536_X() {
        return get718940536_X(getSharedPreferences());
    }

    public float get718940536_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_718940536_X", Float.MIN_VALUE);
    }

    public void set718940536_X(float value) {
        set718940536_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set718940536_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_718940536_X", value);
    }

    public void set718940536_XToDefault() {
        set718940536_X(0.0f);
    }

    public SharedPreferences.Editor set718940536_XToDefault(SharedPreferences.Editor editor) {
        return set718940536_X(0.0f, editor);
    }

    public float get718940536_Y() {
        return get718940536_Y(getSharedPreferences());
    }

    public float get718940536_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_718940536_Y", Float.MIN_VALUE);
    }

    public void set718940536_Y(float value) {
        set718940536_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set718940536_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_718940536_Y", value);
    }

    public void set718940536_YToDefault() {
        set718940536_Y(0.0f);
    }

    public SharedPreferences.Editor set718940536_YToDefault(SharedPreferences.Editor editor) {
        return set718940536_Y(0.0f, editor);
    }

    public float get718940536_R() {
        return get718940536_R(getSharedPreferences());
    }

    public float get718940536_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_718940536_R", Float.MIN_VALUE);
    }

    public void set718940536_R(float value) {
        set718940536_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set718940536_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_718940536_R", value);
    }

    public void set718940536_RToDefault() {
        set718940536_R(0.0f);
    }

    public SharedPreferences.Editor set718940536_RToDefault(SharedPreferences.Editor editor) {
        return set718940536_R(0.0f, editor);
    }

    public void load1245263572(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1245263572_X(sharedPreferences);
        float y = get1245263572_Y(sharedPreferences);
        float r = get1245263572_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1245263572(SharedPreferences.Editor editor, Puzzle p) {
        set1245263572_X(p.getPositionInDesktop().getX(), editor);
        set1245263572_Y(p.getPositionInDesktop().getY(), editor);
        set1245263572_R(p.getRotation(), editor);
    }

    public float get1245263572_X() {
        return get1245263572_X(getSharedPreferences());
    }

    public float get1245263572_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1245263572_X", Float.MIN_VALUE);
    }

    public void set1245263572_X(float value) {
        set1245263572_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1245263572_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1245263572_X", value);
    }

    public void set1245263572_XToDefault() {
        set1245263572_X(0.0f);
    }

    public SharedPreferences.Editor set1245263572_XToDefault(SharedPreferences.Editor editor) {
        return set1245263572_X(0.0f, editor);
    }

    public float get1245263572_Y() {
        return get1245263572_Y(getSharedPreferences());
    }

    public float get1245263572_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1245263572_Y", Float.MIN_VALUE);
    }

    public void set1245263572_Y(float value) {
        set1245263572_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1245263572_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1245263572_Y", value);
    }

    public void set1245263572_YToDefault() {
        set1245263572_Y(0.0f);
    }

    public SharedPreferences.Editor set1245263572_YToDefault(SharedPreferences.Editor editor) {
        return set1245263572_Y(0.0f, editor);
    }

    public float get1245263572_R() {
        return get1245263572_R(getSharedPreferences());
    }

    public float get1245263572_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1245263572_R", Float.MIN_VALUE);
    }

    public void set1245263572_R(float value) {
        set1245263572_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1245263572_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1245263572_R", value);
    }

    public void set1245263572_RToDefault() {
        set1245263572_R(0.0f);
    }

    public SharedPreferences.Editor set1245263572_RToDefault(SharedPreferences.Editor editor) {
        return set1245263572_R(0.0f, editor);
    }

    public void load1654792066(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1654792066_X(sharedPreferences);
        float y = get1654792066_Y(sharedPreferences);
        float r = get1654792066_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1654792066(SharedPreferences.Editor editor, Puzzle p) {
        set1654792066_X(p.getPositionInDesktop().getX(), editor);
        set1654792066_Y(p.getPositionInDesktop().getY(), editor);
        set1654792066_R(p.getRotation(), editor);
    }

    public float get1654792066_X() {
        return get1654792066_X(getSharedPreferences());
    }

    public float get1654792066_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1654792066_X", Float.MIN_VALUE);
    }

    public void set1654792066_X(float value) {
        set1654792066_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1654792066_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1654792066_X", value);
    }

    public void set1654792066_XToDefault() {
        set1654792066_X(0.0f);
    }

    public SharedPreferences.Editor set1654792066_XToDefault(SharedPreferences.Editor editor) {
        return set1654792066_X(0.0f, editor);
    }

    public float get1654792066_Y() {
        return get1654792066_Y(getSharedPreferences());
    }

    public float get1654792066_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1654792066_Y", Float.MIN_VALUE);
    }

    public void set1654792066_Y(float value) {
        set1654792066_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1654792066_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1654792066_Y", value);
    }

    public void set1654792066_YToDefault() {
        set1654792066_Y(0.0f);
    }

    public SharedPreferences.Editor set1654792066_YToDefault(SharedPreferences.Editor editor) {
        return set1654792066_Y(0.0f, editor);
    }

    public float get1654792066_R() {
        return get1654792066_R(getSharedPreferences());
    }

    public float get1654792066_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1654792066_R", Float.MIN_VALUE);
    }

    public void set1654792066_R(float value) {
        set1654792066_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1654792066_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1654792066_R", value);
    }

    public void set1654792066_RToDefault() {
        set1654792066_R(0.0f);
    }

    public SharedPreferences.Editor set1654792066_RToDefault(SharedPreferences.Editor editor) {
        return set1654792066_R(0.0f, editor);
    }

    public void load1596080406(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1596080406_X(sharedPreferences);
        float y = get1596080406_Y(sharedPreferences);
        float r = get1596080406_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1596080406(SharedPreferences.Editor editor, Puzzle p) {
        set1596080406_X(p.getPositionInDesktop().getX(), editor);
        set1596080406_Y(p.getPositionInDesktop().getY(), editor);
        set1596080406_R(p.getRotation(), editor);
    }

    public float get1596080406_X() {
        return get1596080406_X(getSharedPreferences());
    }

    public float get1596080406_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1596080406_X", Float.MIN_VALUE);
    }

    public void set1596080406_X(float value) {
        set1596080406_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1596080406_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1596080406_X", value);
    }

    public void set1596080406_XToDefault() {
        set1596080406_X(0.0f);
    }

    public SharedPreferences.Editor set1596080406_XToDefault(SharedPreferences.Editor editor) {
        return set1596080406_X(0.0f, editor);
    }

    public float get1596080406_Y() {
        return get1596080406_Y(getSharedPreferences());
    }

    public float get1596080406_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1596080406_Y", Float.MIN_VALUE);
    }

    public void set1596080406_Y(float value) {
        set1596080406_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1596080406_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1596080406_Y", value);
    }

    public void set1596080406_YToDefault() {
        set1596080406_Y(0.0f);
    }

    public SharedPreferences.Editor set1596080406_YToDefault(SharedPreferences.Editor editor) {
        return set1596080406_Y(0.0f, editor);
    }

    public float get1596080406_R() {
        return get1596080406_R(getSharedPreferences());
    }

    public float get1596080406_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1596080406_R", Float.MIN_VALUE);
    }

    public void set1596080406_R(float value) {
        set1596080406_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1596080406_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1596080406_R", value);
    }

    public void set1596080406_RToDefault() {
        set1596080406_R(0.0f);
    }

    public SharedPreferences.Editor set1596080406_RToDefault(SharedPreferences.Editor editor) {
        return set1596080406_R(0.0f, editor);
    }

    public void load322042350(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get322042350_X(sharedPreferences);
        float y = get322042350_Y(sharedPreferences);
        float r = get322042350_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save322042350(SharedPreferences.Editor editor, Puzzle p) {
        set322042350_X(p.getPositionInDesktop().getX(), editor);
        set322042350_Y(p.getPositionInDesktop().getY(), editor);
        set322042350_R(p.getRotation(), editor);
    }

    public float get322042350_X() {
        return get322042350_X(getSharedPreferences());
    }

    public float get322042350_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_322042350_X", Float.MIN_VALUE);
    }

    public void set322042350_X(float value) {
        set322042350_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set322042350_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_322042350_X", value);
    }

    public void set322042350_XToDefault() {
        set322042350_X(0.0f);
    }

    public SharedPreferences.Editor set322042350_XToDefault(SharedPreferences.Editor editor) {
        return set322042350_X(0.0f, editor);
    }

    public float get322042350_Y() {
        return get322042350_Y(getSharedPreferences());
    }

    public float get322042350_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_322042350_Y", Float.MIN_VALUE);
    }

    public void set322042350_Y(float value) {
        set322042350_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set322042350_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_322042350_Y", value);
    }

    public void set322042350_YToDefault() {
        set322042350_Y(0.0f);
    }

    public SharedPreferences.Editor set322042350_YToDefault(SharedPreferences.Editor editor) {
        return set322042350_Y(0.0f, editor);
    }

    public float get322042350_R() {
        return get322042350_R(getSharedPreferences());
    }

    public float get322042350_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_322042350_R", Float.MIN_VALUE);
    }

    public void set322042350_R(float value) {
        set322042350_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set322042350_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_322042350_R", value);
    }

    public void set322042350_RToDefault() {
        set322042350_R(0.0f);
    }

    public SharedPreferences.Editor set322042350_RToDefault(SharedPreferences.Editor editor) {
        return set322042350_R(0.0f, editor);
    }

    public void load183069723(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get183069723_X(sharedPreferences);
        float y = get183069723_Y(sharedPreferences);
        float r = get183069723_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save183069723(SharedPreferences.Editor editor, Puzzle p) {
        set183069723_X(p.getPositionInDesktop().getX(), editor);
        set183069723_Y(p.getPositionInDesktop().getY(), editor);
        set183069723_R(p.getRotation(), editor);
    }

    public float get183069723_X() {
        return get183069723_X(getSharedPreferences());
    }

    public float get183069723_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_183069723_X", Float.MIN_VALUE);
    }

    public void set183069723_X(float value) {
        set183069723_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set183069723_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_183069723_X", value);
    }

    public void set183069723_XToDefault() {
        set183069723_X(0.0f);
    }

    public SharedPreferences.Editor set183069723_XToDefault(SharedPreferences.Editor editor) {
        return set183069723_X(0.0f, editor);
    }

    public float get183069723_Y() {
        return get183069723_Y(getSharedPreferences());
    }

    public float get183069723_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_183069723_Y", Float.MIN_VALUE);
    }

    public void set183069723_Y(float value) {
        set183069723_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set183069723_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_183069723_Y", value);
    }

    public void set183069723_YToDefault() {
        set183069723_Y(0.0f);
    }

    public SharedPreferences.Editor set183069723_YToDefault(SharedPreferences.Editor editor) {
        return set183069723_Y(0.0f, editor);
    }

    public float get183069723_R() {
        return get183069723_R(getSharedPreferences());
    }

    public float get183069723_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_183069723_R", Float.MIN_VALUE);
    }

    public void set183069723_R(float value) {
        set183069723_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set183069723_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_183069723_R", value);
    }

    public void set183069723_RToDefault() {
        set183069723_R(0.0f);
    }

    public SharedPreferences.Editor set183069723_RToDefault(SharedPreferences.Editor editor) {
        return set183069723_R(0.0f, editor);
    }

    public void load2008660439(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get2008660439_X(sharedPreferences);
        float y = get2008660439_Y(sharedPreferences);
        float r = get2008660439_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save2008660439(SharedPreferences.Editor editor, Puzzle p) {
        set2008660439_X(p.getPositionInDesktop().getX(), editor);
        set2008660439_Y(p.getPositionInDesktop().getY(), editor);
        set2008660439_R(p.getRotation(), editor);
    }

    public float get2008660439_X() {
        return get2008660439_X(getSharedPreferences());
    }

    public float get2008660439_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_2008660439_X", Float.MIN_VALUE);
    }

    public void set2008660439_X(float value) {
        set2008660439_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set2008660439_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_2008660439_X", value);
    }

    public void set2008660439_XToDefault() {
        set2008660439_X(0.0f);
    }

    public SharedPreferences.Editor set2008660439_XToDefault(SharedPreferences.Editor editor) {
        return set2008660439_X(0.0f, editor);
    }

    public float get2008660439_Y() {
        return get2008660439_Y(getSharedPreferences());
    }

    public float get2008660439_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_2008660439_Y", Float.MIN_VALUE);
    }

    public void set2008660439_Y(float value) {
        set2008660439_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set2008660439_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_2008660439_Y", value);
    }

    public void set2008660439_YToDefault() {
        set2008660439_Y(0.0f);
    }

    public SharedPreferences.Editor set2008660439_YToDefault(SharedPreferences.Editor editor) {
        return set2008660439_Y(0.0f, editor);
    }

    public float get2008660439_R() {
        return get2008660439_R(getSharedPreferences());
    }

    public float get2008660439_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_2008660439_R", Float.MIN_VALUE);
    }

    public void set2008660439_R(float value) {
        set2008660439_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set2008660439_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_2008660439_R", value);
    }

    public void set2008660439_RToDefault() {
        set2008660439_R(0.0f);
    }

    public SharedPreferences.Editor set2008660439_RToDefault(SharedPreferences.Editor editor) {
        return set2008660439_R(0.0f, editor);
    }

    public void load1412775374(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1412775374_X(sharedPreferences);
        float y = get1412775374_Y(sharedPreferences);
        float r = get1412775374_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1412775374(SharedPreferences.Editor editor, Puzzle p) {
        set1412775374_X(p.getPositionInDesktop().getX(), editor);
        set1412775374_Y(p.getPositionInDesktop().getY(), editor);
        set1412775374_R(p.getRotation(), editor);
    }

    public float get1412775374_X() {
        return get1412775374_X(getSharedPreferences());
    }

    public float get1412775374_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1412775374_X", Float.MIN_VALUE);
    }

    public void set1412775374_X(float value) {
        set1412775374_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1412775374_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1412775374_X", value);
    }

    public void set1412775374_XToDefault() {
        set1412775374_X(0.0f);
    }

    public SharedPreferences.Editor set1412775374_XToDefault(SharedPreferences.Editor editor) {
        return set1412775374_X(0.0f, editor);
    }

    public float get1412775374_Y() {
        return get1412775374_Y(getSharedPreferences());
    }

    public float get1412775374_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1412775374_Y", Float.MIN_VALUE);
    }

    public void set1412775374_Y(float value) {
        set1412775374_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1412775374_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1412775374_Y", value);
    }

    public void set1412775374_YToDefault() {
        set1412775374_Y(0.0f);
    }

    public SharedPreferences.Editor set1412775374_YToDefault(SharedPreferences.Editor editor) {
        return set1412775374_Y(0.0f, editor);
    }

    public float get1412775374_R() {
        return get1412775374_R(getSharedPreferences());
    }

    public float get1412775374_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1412775374_R", Float.MIN_VALUE);
    }

    public void set1412775374_R(float value) {
        set1412775374_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1412775374_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1412775374_R", value);
    }

    public void set1412775374_RToDefault() {
        set1412775374_R(0.0f);
    }

    public SharedPreferences.Editor set1412775374_RToDefault(SharedPreferences.Editor editor) {
        return set1412775374_R(0.0f, editor);
    }

    public void load1367843837(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1367843837_X(sharedPreferences);
        float y = get1367843837_Y(sharedPreferences);
        float r = get1367843837_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1367843837(SharedPreferences.Editor editor, Puzzle p) {
        set1367843837_X(p.getPositionInDesktop().getX(), editor);
        set1367843837_Y(p.getPositionInDesktop().getY(), editor);
        set1367843837_R(p.getRotation(), editor);
    }

    public float get1367843837_X() {
        return get1367843837_X(getSharedPreferences());
    }

    public float get1367843837_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1367843837_X", Float.MIN_VALUE);
    }

    public void set1367843837_X(float value) {
        set1367843837_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1367843837_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1367843837_X", value);
    }

    public void set1367843837_XToDefault() {
        set1367843837_X(0.0f);
    }

    public SharedPreferences.Editor set1367843837_XToDefault(SharedPreferences.Editor editor) {
        return set1367843837_X(0.0f, editor);
    }

    public float get1367843837_Y() {
        return get1367843837_Y(getSharedPreferences());
    }

    public float get1367843837_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1367843837_Y", Float.MIN_VALUE);
    }

    public void set1367843837_Y(float value) {
        set1367843837_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1367843837_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1367843837_Y", value);
    }

    public void set1367843837_YToDefault() {
        set1367843837_Y(0.0f);
    }

    public SharedPreferences.Editor set1367843837_YToDefault(SharedPreferences.Editor editor) {
        return set1367843837_Y(0.0f, editor);
    }

    public float get1367843837_R() {
        return get1367843837_R(getSharedPreferences());
    }

    public float get1367843837_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1367843837_R", Float.MIN_VALUE);
    }

    public void set1367843837_R(float value) {
        set1367843837_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1367843837_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1367843837_R", value);
    }

    public void set1367843837_RToDefault() {
        set1367843837_R(0.0f);
    }

    public SharedPreferences.Editor set1367843837_RToDefault(SharedPreferences.Editor editor) {
        return set1367843837_R(0.0f, editor);
    }

    public void load971061690(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get971061690_X(sharedPreferences);
        float y = get971061690_Y(sharedPreferences);
        float r = get971061690_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save971061690(SharedPreferences.Editor editor, Puzzle p) {
        set971061690_X(p.getPositionInDesktop().getX(), editor);
        set971061690_Y(p.getPositionInDesktop().getY(), editor);
        set971061690_R(p.getRotation(), editor);
    }

    public float get971061690_X() {
        return get971061690_X(getSharedPreferences());
    }

    public float get971061690_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_971061690_X", Float.MIN_VALUE);
    }

    public void set971061690_X(float value) {
        set971061690_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set971061690_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_971061690_X", value);
    }

    public void set971061690_XToDefault() {
        set971061690_X(0.0f);
    }

    public SharedPreferences.Editor set971061690_XToDefault(SharedPreferences.Editor editor) {
        return set971061690_X(0.0f, editor);
    }

    public float get971061690_Y() {
        return get971061690_Y(getSharedPreferences());
    }

    public float get971061690_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_971061690_Y", Float.MIN_VALUE);
    }

    public void set971061690_Y(float value) {
        set971061690_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set971061690_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_971061690_Y", value);
    }

    public void set971061690_YToDefault() {
        set971061690_Y(0.0f);
    }

    public SharedPreferences.Editor set971061690_YToDefault(SharedPreferences.Editor editor) {
        return set971061690_Y(0.0f, editor);
    }

    public float get971061690_R() {
        return get971061690_R(getSharedPreferences());
    }

    public float get971061690_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_971061690_R", Float.MIN_VALUE);
    }

    public void set971061690_R(float value) {
        set971061690_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set971061690_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_971061690_R", value);
    }

    public void set971061690_RToDefault() {
        set971061690_R(0.0f);
    }

    public SharedPreferences.Editor set971061690_RToDefault(SharedPreferences.Editor editor) {
        return set971061690_R(0.0f, editor);
    }

    public void load1022336723(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1022336723_X(sharedPreferences);
        float y = get1022336723_Y(sharedPreferences);
        float r = get1022336723_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1022336723(SharedPreferences.Editor editor, Puzzle p) {
        set1022336723_X(p.getPositionInDesktop().getX(), editor);
        set1022336723_Y(p.getPositionInDesktop().getY(), editor);
        set1022336723_R(p.getRotation(), editor);
    }

    public float get1022336723_X() {
        return get1022336723_X(getSharedPreferences());
    }

    public float get1022336723_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1022336723_X", Float.MIN_VALUE);
    }

    public void set1022336723_X(float value) {
        set1022336723_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1022336723_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1022336723_X", value);
    }

    public void set1022336723_XToDefault() {
        set1022336723_X(0.0f);
    }

    public SharedPreferences.Editor set1022336723_XToDefault(SharedPreferences.Editor editor) {
        return set1022336723_X(0.0f, editor);
    }

    public float get1022336723_Y() {
        return get1022336723_Y(getSharedPreferences());
    }

    public float get1022336723_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1022336723_Y", Float.MIN_VALUE);
    }

    public void set1022336723_Y(float value) {
        set1022336723_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1022336723_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1022336723_Y", value);
    }

    public void set1022336723_YToDefault() {
        set1022336723_Y(0.0f);
    }

    public SharedPreferences.Editor set1022336723_YToDefault(SharedPreferences.Editor editor) {
        return set1022336723_Y(0.0f, editor);
    }

    public float get1022336723_R() {
        return get1022336723_R(getSharedPreferences());
    }

    public float get1022336723_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1022336723_R", Float.MIN_VALUE);
    }

    public void set1022336723_R(float value) {
        set1022336723_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1022336723_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1022336723_R", value);
    }

    public void set1022336723_RToDefault() {
        set1022336723_R(0.0f);
    }

    public SharedPreferences.Editor set1022336723_RToDefault(SharedPreferences.Editor editor) {
        return set1022336723_R(0.0f, editor);
    }

    public void load1760694289(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get1760694289_X(sharedPreferences);
        float y = get1760694289_Y(sharedPreferences);
        float r = get1760694289_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save1760694289(SharedPreferences.Editor editor, Puzzle p) {
        set1760694289_X(p.getPositionInDesktop().getX(), editor);
        set1760694289_Y(p.getPositionInDesktop().getY(), editor);
        set1760694289_R(p.getRotation(), editor);
    }

    public float get1760694289_X() {
        return get1760694289_X(getSharedPreferences());
    }

    public float get1760694289_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1760694289_X", Float.MIN_VALUE);
    }

    public void set1760694289_X(float value) {
        set1760694289_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1760694289_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1760694289_X", value);
    }

    public void set1760694289_XToDefault() {
        set1760694289_X(0.0f);
    }

    public SharedPreferences.Editor set1760694289_XToDefault(SharedPreferences.Editor editor) {
        return set1760694289_X(0.0f, editor);
    }

    public float get1760694289_Y() {
        return get1760694289_Y(getSharedPreferences());
    }

    public float get1760694289_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1760694289_Y", Float.MIN_VALUE);
    }

    public void set1760694289_Y(float value) {
        set1760694289_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1760694289_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1760694289_Y", value);
    }

    public void set1760694289_YToDefault() {
        set1760694289_Y(0.0f);
    }

    public SharedPreferences.Editor set1760694289_YToDefault(SharedPreferences.Editor editor) {
        return set1760694289_Y(0.0f, editor);
    }

    public float get1760694289_R() {
        return get1760694289_R(getSharedPreferences());
    }

    public float get1760694289_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_1760694289_R", Float.MIN_VALUE);
    }

    public void set1760694289_R(float value) {
        set1760694289_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set1760694289_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_1760694289_R", value);
    }

    public void set1760694289_RToDefault() {
        set1760694289_R(0.0f);
    }

    public SharedPreferences.Editor set1760694289_RToDefault(SharedPreferences.Editor editor) {
        return set1760694289_R(0.0f, editor);
    }

    public void load420528363(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get420528363_X(sharedPreferences);
        float y = get420528363_Y(sharedPreferences);
        float r = get420528363_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save420528363(SharedPreferences.Editor editor, Puzzle p) {
        set420528363_X(p.getPositionInDesktop().getX(), editor);
        set420528363_Y(p.getPositionInDesktop().getY(), editor);
        set420528363_R(p.getRotation(), editor);
    }

    public float get420528363_X() {
        return get420528363_X(getSharedPreferences());
    }

    public float get420528363_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_420528363_X", Float.MIN_VALUE);
    }

    public void set420528363_X(float value) {
        set420528363_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set420528363_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_420528363_X", value);
    }

    public void set420528363_XToDefault() {
        set420528363_X(0.0f);
    }

    public SharedPreferences.Editor set420528363_XToDefault(SharedPreferences.Editor editor) {
        return set420528363_X(0.0f, editor);
    }

    public float get420528363_Y() {
        return get420528363_Y(getSharedPreferences());
    }

    public float get420528363_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_420528363_Y", Float.MIN_VALUE);
    }

    public void set420528363_Y(float value) {
        set420528363_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set420528363_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_420528363_Y", value);
    }

    public void set420528363_YToDefault() {
        set420528363_Y(0.0f);
    }

    public SharedPreferences.Editor set420528363_YToDefault(SharedPreferences.Editor editor) {
        return set420528363_Y(0.0f, editor);
    }

    public float get420528363_R() {
        return get420528363_R(getSharedPreferences());
    }

    public float get420528363_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_420528363_R", Float.MIN_VALUE);
    }

    public void set420528363_R(float value) {
        set420528363_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set420528363_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_420528363_R", value);
    }

    public void set420528363_RToDefault() {
        set420528363_R(0.0f);
    }

    public SharedPreferences.Editor set420528363_RToDefault(SharedPreferences.Editor editor) {
        return set420528363_R(0.0f, editor);
    }

    public void load27814050(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get27814050_X(sharedPreferences);
        float y = get27814050_Y(sharedPreferences);
        float r = get27814050_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save27814050(SharedPreferences.Editor editor, Puzzle p) {
        set27814050_X(p.getPositionInDesktop().getX(), editor);
        set27814050_Y(p.getPositionInDesktop().getY(), editor);
        set27814050_R(p.getRotation(), editor);
    }

    public float get27814050_X() {
        return get27814050_X(getSharedPreferences());
    }

    public float get27814050_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_27814050_X", Float.MIN_VALUE);
    }

    public void set27814050_X(float value) {
        set27814050_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set27814050_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_27814050_X", value);
    }

    public void set27814050_XToDefault() {
        set27814050_X(0.0f);
    }

    public SharedPreferences.Editor set27814050_XToDefault(SharedPreferences.Editor editor) {
        return set27814050_X(0.0f, editor);
    }

    public float get27814050_Y() {
        return get27814050_Y(getSharedPreferences());
    }

    public float get27814050_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_27814050_Y", Float.MIN_VALUE);
    }

    public void set27814050_Y(float value) {
        set27814050_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set27814050_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_27814050_Y", value);
    }

    public void set27814050_YToDefault() {
        set27814050_Y(0.0f);
    }

    public SharedPreferences.Editor set27814050_YToDefault(SharedPreferences.Editor editor) {
        return set27814050_Y(0.0f, editor);
    }

    public float get27814050_R() {
        return get27814050_R(getSharedPreferences());
    }

    public float get27814050_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_27814050_R", Float.MIN_VALUE);
    }

    public void set27814050_R(float value) {
        set27814050_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set27814050_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_27814050_R", value);
    }

    public void set27814050_RToDefault() {
        set27814050_R(0.0f);
    }

    public SharedPreferences.Editor set27814050_RToDefault(SharedPreferences.Editor editor) {
        return set27814050_R(0.0f, editor);
    }

    public void load2060586374(SharedPreferences sharedPreferences, Puzzle p) {
        float x = get2060586374_X(sharedPreferences);
        float y = get2060586374_Y(sharedPreferences);
        float r = get2060586374_R(sharedPreferences);
        if (x != Float.MIN_VALUE) {
            p.getPositionInDesktop().setX(x);
        }
        if (y != Float.MIN_VALUE) {
            p.getPositionInDesktop().setY(y);
        }
        if (r != Float.MIN_VALUE) {
            p.setRotation(r);
        }
    }

    public void save2060586374(SharedPreferences.Editor editor, Puzzle p) {
        set2060586374_X(p.getPositionInDesktop().getX(), editor);
        set2060586374_Y(p.getPositionInDesktop().getY(), editor);
        set2060586374_R(p.getRotation(), editor);
    }

    public float get2060586374_X() {
        return get2060586374_X(getSharedPreferences());
    }

    public float get2060586374_X(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_2060586374_X", Float.MIN_VALUE);
    }

    public void set2060586374_X(float value) {
        set2060586374_X(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set2060586374_X(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_2060586374_X", value);
    }

    public void set2060586374_XToDefault() {
        set2060586374_X(0.0f);
    }

    public SharedPreferences.Editor set2060586374_XToDefault(SharedPreferences.Editor editor) {
        return set2060586374_X(0.0f, editor);
    }

    public float get2060586374_Y() {
        return get2060586374_Y(getSharedPreferences());
    }

    public float get2060586374_Y(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_2060586374_Y", Float.MIN_VALUE);
    }

    public void set2060586374_Y(float value) {
        set2060586374_Y(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set2060586374_Y(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_2060586374_Y", value);
    }

    public void set2060586374_YToDefault() {
        set2060586374_Y(0.0f);
    }

    public SharedPreferences.Editor set2060586374_YToDefault(SharedPreferences.Editor editor) {
        return set2060586374_Y(0.0f, editor);
    }

    public float get2060586374_R() {
        return get2060586374_R(getSharedPreferences());
    }

    public float get2060586374_R(SharedPreferences sharedPreferences) {
        return sharedPreferences.getFloat("Float_2060586374_R", Float.MIN_VALUE);
    }

    public void set2060586374_R(float value) {
        set2060586374_R(value, getSharedPreferences().edit()).commit();
    }

    public SharedPreferences.Editor set2060586374_R(float value, SharedPreferences.Editor editor) {
        return editor.putFloat("Float_2060586374_R", value);
    }

    public void set2060586374_RToDefault() {
        set2060586374_R(0.0f);
    }

    public SharedPreferences.Editor set2060586374_RToDefault(SharedPreferences.Editor editor) {
        return set2060586374_R(0.0f, editor);
    }
}
