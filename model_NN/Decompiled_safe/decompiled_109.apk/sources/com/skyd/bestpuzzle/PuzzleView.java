package com.skyd.bestpuzzle;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.skyd.core.android.game.GameDisplayView;
import com.skyd.core.android.game.GameMaster;
import com.skyd.core.vector.Vector2DF;
import java.util.ArrayList;
import java.util.Iterator;

public class PuzzleView extends GameDisplayView {
    Puzzle DragPuzzle;
    public PuzzleScene MainScene;
    private ArrayList<OnMainSceneLoadedListener> _MainSceneLoadedListenerList = null;
    boolean dragmany;
    boolean moveObservePosition;
    boolean rotate;
    boolean select;
    Vector2DF startDragPoint;

    public interface OnMainSceneLoadedListener {
        void OnMainSceneLoadedEvent(Object obj);
    }

    public PuzzleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void init(Boolean isFirstShow) {
        GameMaster.init(getContext());
        setFPS(30);
        if (isFirstShow.booleanValue()) {
            this.MainScene = new PuzzleScene();
            this.MainScene.setName("main");
            this.MainScene.getStandardDisplayAreaSize().reset(800.0f, 480.0f);
            this.MainScene.setIsFillParentDisplayArea(false);
            this.MainScene.setIsMatchHeight(true);
            this.MainScene.setIsMatchWidth(false);
            try {
                addScene(this.MainScene);
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.MainScene.load(getContext());
            onMainSceneLoaded();
            this.MainScene.show();
        }
    }

    public boolean addOnMainSceneLoadedListener(OnMainSceneLoadedListener listener) {
        if (this._MainSceneLoadedListenerList == null) {
            this._MainSceneLoadedListenerList = new ArrayList<>();
        } else if (this._MainSceneLoadedListenerList.contains(listener)) {
            return false;
        }
        this._MainSceneLoadedListenerList.add(listener);
        return true;
    }

    public boolean removeOnMainSceneLoadedListener(OnMainSceneLoadedListener listener) {
        if (this._MainSceneLoadedListenerList == null || !this._MainSceneLoadedListenerList.contains(listener)) {
            return false;
        }
        this._MainSceneLoadedListenerList.remove(listener);
        return true;
    }

    public void clearOnMainSceneLoadedListeners() {
        if (this._MainSceneLoadedListenerList != null) {
            this._MainSceneLoadedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onMainSceneLoaded() {
        if (this._MainSceneLoadedListenerList != null) {
            Iterator<OnMainSceneLoadedListener> it = this._MainSceneLoadedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnMainSceneLoadedEvent(this);
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        Vector2DF v = new Vector2DF(event.getX(), event.getY());
        switch (event.getAction()) {
            case 0:
                if (!this.MainScene.OriginalImage.getVisibleOriginalValue()) {
                    if (!this.MainScene.MoveButtonSlot.isInArea(v)) {
                        if (!this.MainScene.ViewFullButton.isInArea(v)) {
                            if (!this.MainScene.SubmitScoreButton.isInArea(v)) {
                                if (!this.MainScene.ZoomInButton.isInArea(v)) {
                                    if (!this.MainScene.ZoomOutButton.isInArea(v)) {
                                        if (!this.MainScene.Controller.isInRotateArea(v)) {
                                            if (!this.MainScene.Controller.isInArea(v)) {
                                                this.DragPuzzle = this.MainScene.getTouchPuzzle(v);
                                                this.startDragPoint = v;
                                                if (this.DragPuzzle == null) {
                                                    this.select = true;
                                                    break;
                                                } else {
                                                    this.MainScene.Controller.reset();
                                                    this.MainScene.Controller.setLevelAbove(this.MainScene.getMaxPuzzleLevel());
                                                    this.DragPuzzle.setLevelAbove(this.MainScene.Controller);
                                                    this.MainScene.Controller.getPuzzleList().add(this.DragPuzzle);
                                                    this.MainScene.Controller.updateState();
                                                    this.MainScene.Controller.initDrag();
                                                    this.dragmany = true;
                                                    this.MainScene.refreshDrawCacheBitmap();
                                                    break;
                                                }
                                            } else {
                                                this.startDragPoint = v;
                                                this.dragmany = true;
                                                break;
                                            }
                                        } else {
                                            this.rotate = true;
                                            break;
                                        }
                                    } else {
                                        this.MainScene.ZoomOutButton.executive(v);
                                        break;
                                    }
                                } else {
                                    this.MainScene.ZoomInButton.executive(v);
                                    break;
                                }
                            } else {
                                this.MainScene.SubmitScoreButton.executive(v);
                                break;
                            }
                        } else {
                            this.MainScene.ViewFullButton.executive(v);
                            break;
                        }
                    } else {
                        this.moveObservePosition = true;
                        this.MainScene.MoveButtonSlot.executive(v);
                        break;
                    }
                } else {
                    this.MainScene.ViewFullButton.reset();
                    break;
                }
            case 1:
                this.moveObservePosition = false;
                this.dragmany = false;
                this.rotate = false;
                this.MainScene.MoveButtonSlot.reset();
                this.MainScene.ZoomInButton.reset();
                this.MainScene.ZoomOutButton.reset();
                if (this.DragPuzzle != null) {
                    this.DragPuzzle = null;
                }
                if (this.select) {
                    this.select = false;
                    this.MainScene.stopDrawSelectRect();
                    ArrayList<Puzzle> al = this.MainScene.getSelectPuzzle(this.startDragPoint, v);
                    if (al.size() > 0) {
                        this.MainScene.Controller.getPuzzleList().clear();
                        this.MainScene.Controller.getPuzzleList().addAll(al);
                        this.MainScene.Controller.updateState();
                        this.MainScene.Controller.setLevelAbove(this.MainScene.getMaxPuzzleLevel());
                        Iterator<Puzzle> it = al.iterator();
                        while (it.hasNext()) {
                            it.next().setLevelAbove(this.MainScene.Controller);
                        }
                    } else {
                        this.MainScene.Controller.reset();
                    }
                    this.MainScene.refreshDrawCacheBitmap();
                    break;
                }
                break;
            case 2:
                if (this.DragPuzzle == null) {
                    if (!this.moveObservePosition) {
                        if (!this.rotate) {
                            if (!this.select) {
                                if (this.dragmany) {
                                    this.MainScene.Controller.movePuzzlesTo(this.startDragPoint, v);
                                    break;
                                }
                            } else {
                                this.MainScene.startDrawSelectRect(this.startDragPoint, v);
                                break;
                            }
                        } else {
                            this.MainScene.Controller.executive(v);
                            break;
                        }
                    } else {
                        this.MainScene.MoveButtonSlot.executive(v);
                        break;
                    }
                } else {
                    this.MainScene.Controller.movePuzzlesTo(this.startDragPoint, v);
                    break;
                }
                break;
        }
        return true;
    }
}
