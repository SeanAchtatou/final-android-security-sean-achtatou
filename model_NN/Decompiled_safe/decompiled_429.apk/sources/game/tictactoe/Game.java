package game.tictactoe;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class Game extends Activity {
    public static final String GAMESTATE = "gameSate";
    public static final String TURN = "turn";
    public static final String TURNCOUNT = "turnCount";
    private AIPlayer ai = new AIPlayer(this);
    private int[][] gameState = {new int[3], new int[3], new int[3]};
    private GameView gameView;
    private int turn = 1;
    private int turnCount = 0;
    private boolean twoPlayer;
    private int[] winLocation = new int[2];
    private int winner = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean newGame = getIntent().getBooleanExtra(ticTacToe.NEWGAME, false);
        this.twoPlayer = getIntent().getBooleanExtra(ticTacToe.TWOPLAYER, true);
        this.gameView = new GameView(this);
        setContentView(this.gameView);
        this.gameView.requestFocus();
        if (newGame) {
            newGame();
            getIntent().replaceExtras(savedInstanceState).removeExtra(ticTacToe.NEWGAME);
        }
    }

    public void move(int x, int y) {
        Log.d("", "x: " + x + "y: " + y);
        if (x < this.gameState.length && y < this.gameState[x].length && this.gameState[x][y] == 0 && this.winner == 0) {
            if (this.turn == 1) {
                this.gameState[x][y] = 1;
                this.turn = 2;
            } else {
                this.gameState[x][y] = 5;
                this.turn = 1;
                Log.d("TURN", new StringBuilder().append(this.turn).toString());
            }
            this.winner = checkForWinner();
            if (this.winner != 0) {
                this.turn = this.winner;
                String winString = "";
                if (this.winner == 1) {
                    winString = "Player one wins!";
                } else if (this.winner == 2) {
                    winString = "Player two wins!";
                }
                if (this.winner == 3) {
                    winString = "Cats Game";
                }
                setTitle(winString);
                AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
                alertbox.setMessage(winString);
                alertbox.setNeutralButton("New Game", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                        Game.this.newGame();
                    }
                });
                alertbox.show();
            }
            this.turnCount++;
        }
        if (!this.twoPlayer && this.turn == 2 && this.winner == 0) {
            Log.d("AI", "MOVED");
            int[] aiMove = this.ai.move();
            move(aiMove[0], aiMove[1]);
        }
    }

    public int checkForWinner() {
        int n = 0;
        while (n < 3) {
            this.winLocation[0] = n;
            int rowSum = sumRow(n);
            int colSum = sumCol(n);
            int diagSum = sumDiag(n);
            if (rowSum == 3) {
                this.winLocation[1] = 1;
                return 1;
            } else if (colSum == 3) {
                this.winLocation[1] = 2;
                return 1;
            } else if (diagSum == 3) {
                this.winLocation[1] = 3;
                return 1;
            } else if (rowSum == 15) {
                this.winLocation[1] = 1;
                return 2;
            } else if (colSum == 15) {
                this.winLocation[1] = 2;
                return 2;
            } else if (diagSum == 15) {
                this.winLocation[1] = 3;
                return 2;
            } else {
                n++;
            }
        }
        if (this.turnCount == 8) {
            return 3;
        }
        return 0;
    }

    public int sumRow(int row) {
        int val = 0;
        for (int i = 0; i < 3; i++) {
            val += this.gameState[i][row];
        }
        return val;
    }

    public int sumCol(int col) {
        int val = 0;
        for (int i = 0; i < 3; i++) {
            val += this.gameState[col][i];
        }
        return val;
    }

    public int sumDiag(int n) {
        int val = 0;
        if (n == 1) {
            for (int i = 0; i < 3; i++) {
                val += this.gameState[i][i];
            }
            return val;
        }
        if (n == 2) {
            val = this.gameState[2][0] + this.gameState[1][1] + this.gameState[0][2];
        }
        return val;
    }

    public void newGame() {
        for (int n = 0; n < 3; n++) {
            for (int i = 0; i < 3; i++) {
                this.gameState[n][i] = 0;
            }
        }
        this.turn = 1;
        this.winner = 0;
        this.turnCount = 0;
        getPreferences(0).edit().putString(GAMESTATE, toGameString()).commit();
        getPreferences(0).edit().putInt(TURN, this.turn).commit();
        getPreferences(0).edit().putInt(TURNCOUNT, this.turnCount).commit();
        this.gameView.invalidate();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, Prefs.class));
                return true;
            default:
                return false;
        }
    }

    public void gameFromString(String state) {
        if (state.length() > 1) {
            for (int n = 0; n < 9; n++) {
                this.gameState[n / 3][n % 3] = Integer.parseInt(new StringBuilder().append(state.charAt(n)).toString());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Log.d("TIC", toGameString());
        getPreferences(0).edit().putString(GAMESTATE, toGameString()).commit();
        getPreferences(0).edit().putInt(TURN, this.turn).commit();
        getPreferences(0).edit().putInt(TURNCOUNT, this.turnCount).commit();
        Log.d("TIC", "State: " + getPreferences(0).getString(GAMESTATE, "000000000"));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        String state = getPreferences(0).getString(GAMESTATE, "000000000");
        Log.d("TIC", "State: " + state);
        gameFromString(state);
        this.turn = getPreferences(0).getInt(TURN, 0);
        this.turnCount = getPreferences(0).getInt(TURNCOUNT, 0);
        this.winner = checkForWinner();
        Log.d("TIC", toGameString());
    }

    public String toGameString() {
        StringBuilder state = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            for (int n = 0; n < 3; n++) {
                state.append(this.gameState[i][n]);
            }
        }
        return state.toString();
    }

    public int getTurn() {
        return this.turn;
    }

    public int getTurnCount() {
        return this.turnCount;
    }

    public int getWinner() {
        return this.winner;
    }

    public int[] getWinLocation() {
        return this.winLocation;
    }

    public int[][] getGameState() {
        return this.gameState;
    }
}
