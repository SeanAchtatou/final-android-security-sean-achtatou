package game.tictactoe;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int olive = 2131034112;
        public static final int sgi_teal = 2131034113;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int about = 2131230720;
        public static final int about_button = 2131230727;
        public static final int about_content = 2131230721;
        public static final int adView = 2131230729;
        public static final int continue_button = 2131230724;
        public static final int exit_button = 2131230728;
        public static final int main = 2131230723;
        public static final int newGameButton = 2131230722;
        public static final int new_button = 2131230725;
        public static final int settings = 2131230730;
        public static final int two_player = 2131230726;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int gameview = 2130903041;
        public static final int main = 2130903042;
    }

    public static final class menu {
        public static final int menu = 2131165184;
    }

    public static final class string {
        public static final int about_label = 2131099653;
        public static final int about_text = 2131099657;
        public static final int about_title = 2131099658;
        public static final int app_name = 2131099649;
        public static final int continue_label = 2131099650;
        public static final int exit_label = 2131099654;
        public static final int fill_screen_summary = 2131099656;
        public static final int fill_screen_title = 2131099655;
        public static final int hello = 2131099648;
        public static final int new_game_label = 2131099651;
        public static final int two_player_label = 2131099652;
        public static final int two_player_summary = 2131099660;
        public static final int two_player_title = 2131099659;
    }

    public static final class xml {
        public static final int settings = 2130968576;
    }
}
