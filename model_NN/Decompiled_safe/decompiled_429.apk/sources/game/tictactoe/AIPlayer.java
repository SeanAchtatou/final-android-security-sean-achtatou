package game.tictactoe;

public class AIPlayer {
    public static final String DELIM = ",";
    int ai = 5;

    /* renamed from: game  reason: collision with root package name */
    Game f0game;
    private int[][] gameState;
    int loses;
    int[] move = {1, 1};
    String[] moves = new String[9];
    int numberOfMoves;
    int opponent = 1;
    int possible;
    String[] possibleMoves = new String[9];
    int potential;
    int wins;

    public AIPlayer(Game game2) {
        this.f0game = game2;
        this.gameState = game2.getGameState();
    }

    public int[] move() {
        this.numberOfMoves = 0;
        this.possibleMoves = new String[9];
        this.possible = checkPossible();
        this.wins = checkForWins();
        this.loses = checkForLoses();
        this.potential = checkPotential();
        if (this.wins > 0) {
            String[] win = this.moves[0].split(DELIM);
            this.move[0] = Integer.parseInt(win[0]);
            this.move[1] = Integer.parseInt(win[1]);
        } else if (this.loses > 0) {
            String[] lose = this.moves[0].split(DELIM);
            this.move[0] = Integer.parseInt(lose[0]);
            this.move[1] = Integer.parseInt(lose[1]);
        } else if (this.potential > 0) {
            String[] pot = this.moves[0].split(DELIM);
            this.move[0] = Integer.parseInt(pot[0]);
            this.move[1] = Integer.parseInt(pot[1]);
        } else {
            String[] lose2 = this.possibleMoves[(int) (Math.random() * ((double) this.possible))].split(DELIM);
            this.move[0] = Integer.parseInt(lose2[0]);
            this.move[1] = Integer.parseInt(lose2[1]);
        }
        return this.move;
    }

    private int checkForWins() {
        int count = 0;
        for (int i = 0; i < this.possible; i++) {
            String[] tmp = this.possibleMoves[i].split(DELIM);
            int x = Integer.parseInt(tmp[0]);
            int y = Integer.parseInt(tmp[1]);
            if (checkWin(x, y, 15)) {
                this.moves[this.numberOfMoves] = String.valueOf(x) + DELIM + y;
                count++;
                this.numberOfMoves++;
            }
        }
        return count;
    }

    private int checkPotential() {
        int count = 0;
        for (int i = 0; i < this.possible; i++) {
            String[] tmp = this.possibleMoves[i].split(DELIM);
            int x = Integer.parseInt(tmp[0]);
            int y = Integer.parseInt(tmp[1]);
            if (checkWin(x, y, 10)) {
                this.moves[this.numberOfMoves] = String.valueOf(x) + DELIM + y;
                count++;
                this.numberOfMoves++;
            }
        }
        return count;
    }

    private int checkForLoses() {
        int count = 0;
        for (int i = 0; i < this.possible; i++) {
            String[] tmp = this.possibleMoves[i].split(DELIM);
            int x = Integer.parseInt(tmp[0]);
            int y = Integer.parseInt(tmp[1]);
            if (checkLose(x, y)) {
                this.moves[this.numberOfMoves] = String.valueOf(x) + DELIM + y;
                count++;
                this.numberOfMoves++;
            }
        }
        return count;
    }

    private boolean checkWin(int x, int y, int total) {
        int colSum = sumCol(x) + this.ai;
        int rowSum = sumRow(y) + this.ai;
        int diag1Sum = 0;
        int diag2Sum = 0;
        if (x == y) {
            diag1Sum = sumDiag(1) + this.ai;
        }
        if (x + y == 2) {
            diag2Sum = sumDiag(2) + this.ai;
        }
        return colSum == total || rowSum == total || diag1Sum == total || diag2Sum == total;
    }

    private boolean checkLose(int x, int y) {
        int colSum = sumCol(x) + this.opponent;
        int rowSum = sumRow(y) + this.opponent;
        int diag1Sum = 0;
        int diag2Sum = 0;
        if (x == y) {
            diag1Sum = sumDiag(1) + this.opponent;
        }
        if (x + y == 2) {
            diag2Sum = sumDiag(2) + this.opponent;
        }
        return colSum == 3 || rowSum == 3 || diag1Sum == 3 || diag2Sum == 3;
    }

    private int sumRow(int y) {
        int sum = 0;
        for (int i = 0; i < 3; i++) {
            sum += this.gameState[i][y];
        }
        return sum;
    }

    private int sumCol(int x) {
        int sum = 0;
        for (int i = 0; i < 3; i++) {
            sum += this.gameState[x][i];
        }
        return sum;
    }

    public int sumDiag(int n) {
        int val = 0;
        if (n == 1) {
            for (int i = 0; i < 3; i++) {
                val += this.gameState[i][i];
            }
            return val;
        }
        if (n == 2) {
            val = this.gameState[2][0] + this.gameState[1][1] + this.gameState[0][2];
        }
        return val;
    }

    private int checkPossible() {
        int count = 0;
        for (int i = 0; i < 3; i++) {
            for (int n = 0; n < 3; n++) {
                if (this.gameState[i][n] == 0) {
                    this.possibleMoves[count] = String.valueOf(i) + DELIM + n;
                    count++;
                }
            }
        }
        return count;
    }
}
