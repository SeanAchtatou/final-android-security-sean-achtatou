package game.tictactoe;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class About extends Activity {
    public static final String MY_AD_UNIT_ID = "a14dff9429d3886";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about);
        AdView adView = new AdView(this, AdSize.BANNER, MY_AD_UNIT_ID);
        ((LinearLayout) findViewById(R.id.about)).addView(adView);
        adView.loadAd(new AdRequest());
    }
}
