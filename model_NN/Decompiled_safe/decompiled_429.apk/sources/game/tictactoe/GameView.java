package game.tictactoe;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class GameView extends View {

    /* renamed from: game  reason: collision with root package name */
    private Game f1game;
    private float height;
    private final Rect selRect = new Rect();
    private int selX;
    private int selY;
    private float width;
    private float xOffset = 0.0f;
    private float yOffset = 0.0f;

    public GameView(Context context) {
        super(context);
        this.f1game = (Game) context;
        setFocusable(true);
        setFocusableInTouchMode(true);
        new Button(context).setText("New Game");
        setBackgroundResource(R.color.sgi_teal);
    }

    private void getRect(int x, int y, Rect rect) {
        rect.set((int) ((((float) x) * this.width) + this.xOffset), (int) ((((float) y) * this.height) + this.yOffset), (int) ((((float) x) * this.width) + this.width + this.xOffset), (int) ((((float) y) * this.height) + this.height + this.yOffset));
    }

    /* access modifiers changed from: protected */
    public void setGrid() {
        this.width = ((float) getWidth()) / 3.0f;
        this.height = ((float) getHeight()) / 3.0f;
        if (!Prefs.getFillScreen(getContext())) {
            if (this.width > this.height) {
                this.width = this.height;
            } else {
                this.height = this.width;
            }
        }
        if (this.height != this.width) {
            this.xOffset = 0.0f;
            this.yOffset = 0.0f;
        } else if (getWidth() > getHeight()) {
            this.xOffset = (((float) getWidth()) - (this.width * 3.0f)) / 2.0f;
        } else if (getHeight() > getWidth()) {
            this.yOffset = (((float) getHeight()) - (this.height * 3.0f)) / 2.0f;
        }
        getRect(this.selX, this.selY, this.selRect);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Log.d("TIC", "XOF: " + this.xOffset + "YOF: " + this.yOffset);
        setGrid();
        Paint paint = new Paint();
        if (this.f1game.getTurn() == 1) {
            paint.setColor(-65536);
        } else if (this.f1game.getTurn() == 2) {
            paint.setColor(-16776961);
        } else if (this.f1game.getTurn() == 3) {
            paint.setColor(-16711936);
        }
        paint.setStrokeWidth(5.0f);
        for (int i = 1; i < 3; i++) {
            Canvas canvas2 = canvas;
            canvas2.drawLine(this.xOffset + (((float) i) * this.width), this.yOffset + 0.0f, this.xOffset + (((float) i) * this.width), this.yOffset + (this.height * 3.0f), paint);
            canvas.drawLine(this.xOffset + 0.0f, this.yOffset + (((float) i) * this.height), this.xOffset + (this.width * 3.0f), this.yOffset + (((float) i) * this.height), paint);
        }
        if (this.f1game.getWinner() == 0 && Prefs.getSelectSquare(getContext())) {
            Paint sPaint = new Paint();
            sPaint.setARGB(255, 175, 238, 238);
            canvas.drawRect(this.selRect, sPaint);
        }
        Paint p1Paint = new Paint();
        p1Paint.setColor(-65536);
        p1Paint.setStrokeWidth(5.0f);
        p1Paint.setStyle(Paint.Style.STROKE);
        Paint p2Paint = new Paint();
        p2Paint.setColor(-16776961);
        p2Paint.setStrokeWidth(5.0f);
        p2Paint.setStyle(Paint.Style.STROKE);
        if (this.f1game.getTurn() == 3) {
            p2Paint.setColor(-16711936);
            p1Paint.setColor(-16711936);
        }
        int[][] gameState = this.f1game.getGameState();
        for (int i2 = 0; i2 < 3; i2++) {
            for (int n = 0; n < 3; n++) {
                float centerX = (((float) i2) * this.width) + (this.width / 2.0f) + this.xOffset;
                float centerY = (((float) n) * this.height) + (this.height / 2.0f) + this.yOffset;
                float radius = (this.width > this.height ? this.height : this.width) / 3.0f;
                if (gameState[i2][n] == 1) {
                    canvas.drawLine(centerX - radius, centerY - radius, centerX + radius, centerY + radius, p1Paint);
                    canvas.drawLine(centerX + radius, centerY - radius, centerX - radius, centerY + radius, p1Paint);
                } else if (gameState[i2][n] == 5) {
                    canvas.drawCircle(centerX, centerY, radius, p2Paint);
                }
            }
        }
        if (this.f1game.getWinner() != 0) {
            Paint wpaint = new Paint();
            wpaint.setStrokeWidth(5.0f);
            wpaint.setColor(-16711936);
            float startX = 0.0f;
            float startY = 0.0f;
            float stopX = 0.0f;
            float stopY = 0.0f;
            int[] winLocation = this.f1game.getWinLocation();
            if (winLocation[1] == 1) {
                startX = this.width / 7.0f;
                stopX = (this.width * 3.0f) - (this.width / 7.0f);
                startY = (((float) winLocation[0]) * this.height) + (this.height / 2.0f);
                stopY = startY;
            } else if (winLocation[1] == 2) {
                startY = this.height / 7.0f;
                stopY = (this.height * 3.0f) - (this.height / 7.0f);
                startX = (((float) winLocation[0]) * this.width) + (this.width / 2.0f);
                stopX = startX;
            } else if (winLocation[1] == 3) {
                if (winLocation[0] == 1) {
                    startX = this.width / 7.0f;
                    startY = this.width / 7.0f;
                    stopX = (this.width * 3.0f) - (this.width / 7.0f);
                    stopY = (this.height * 3.0f) - (this.height / 7.0f);
                } else if (winLocation[0] == 2) {
                    startX = (this.width * 3.0f) - (this.width / 7.0f);
                    startY = this.height / 7.0f;
                    stopX = this.width / 7.0f;
                    stopY = (this.height * 3.0f) - (this.height / 7.0f);
                }
            }
            canvas.drawLine(startX + this.xOffset, startY + this.yOffset, stopX + this.xOffset, stopY + this.yOffset, wpaint);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != 0) {
            return super.onTouchEvent(event);
        }
        int x = 100;
        int y = 100;
        float xPress = event.getX();
        float yPress = event.getY();
        if (xPress < this.width + this.xOffset) {
            x = 0;
        } else if (xPress < (this.width * 2.0f) + this.xOffset) {
            x = 1;
        } else if (xPress < (this.width * 3.0f) + this.xOffset) {
            x = 2;
        }
        if (yPress < this.height + this.yOffset) {
            y = 0;
        } else if (yPress < (this.height * 2.0f) + this.yOffset) {
            y = 1;
        } else if (yPress < (this.height * 3.0f) + this.yOffset) {
            y = 2;
        }
        select(x, y);
        this.f1game.move(x, y);
        invalidate();
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (event.getKeyCode()) {
            case 19:
                select(this.selX, this.selY - 1);
                break;
            case 20:
                select(this.selX, this.selY + 1);
                break;
            case 21:
                select(this.selX - 1, this.selY);
                break;
            case 22:
                select(this.selX + 1, this.selY);
                break;
            case 23:
            case 66:
                this.f1game.move(this.selX, this.selY);
                invalidate();
                break;
            default:
                return super.onKeyDown(keyCode, event);
        }
        return true;
    }

    private void select(int x, int y) {
        invalidate(this.selRect);
        this.selX = Math.min(Math.max(x, 0), 2);
        this.selY = Math.min(Math.max(y, 0), 2);
        getRect(this.selX, this.selY, this.selRect);
        invalidate(this.selRect);
    }
}
