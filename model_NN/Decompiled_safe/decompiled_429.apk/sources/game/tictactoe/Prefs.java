package game.tictactoe;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;

public class Prefs extends PreferenceActivity {
    public static final String OPT_AIPOSITION = "ai_player_position";
    public static final boolean OPT_AIPOSITION_DEF = false;
    public static final String OPT_FILLSCREEN = "fillScreen";
    public static final boolean OPT_FILLSCREEN_DEF = false;
    public static final String OPT_SHOWSQUARE = "selectSquare";
    public static final boolean OPT_SHOWSQUARE_DEF = true;
    public static final String OPT_TWOPLAYERMODE = "two_player";
    public static final boolean OPT_TWOPLAYERMODE_DEF = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("TIC", "PREFS");
        addPreferencesFromResource(R.xml.settings);
    }

    public static boolean getSelectSquare(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(OPT_SHOWSQUARE, true);
    }

    public static boolean getFillScreen(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(OPT_FILLSCREEN, false);
    }

    public static boolean getIsTwoPlayerMode(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(OPT_TWOPLAYERMODE, false);
    }
}
