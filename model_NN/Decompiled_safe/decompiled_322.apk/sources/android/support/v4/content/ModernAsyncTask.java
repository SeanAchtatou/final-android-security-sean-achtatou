package android.support.v4.content;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: ProGuard */
abstract class ModernAsyncTask<Params, Progress, Result> {

    /* renamed from: a  reason: collision with root package name */
    public static final Executor f69a = new ThreadPoolExecutor(5, 128, 1, TimeUnit.SECONDS, c, b);
    private static final ThreadFactory b = new c();
    private static final BlockingQueue<Runnable> c = new LinkedBlockingQueue(10);
    private static final g d = new g(null);
    private static volatile Executor e = f69a;
    private final h<Params, Result> f = new d(this);
    private final FutureTask<Result> g = new e(this, this.f);
    private volatile Status h = Status.PENDING;
    /* access modifiers changed from: private */
    public final AtomicBoolean i = new AtomicBoolean();

    /* compiled from: ProGuard */
    public enum Status {
        PENDING,
        RUNNING,
        FINISHED
    }

    /* access modifiers changed from: protected */
    public abstract Result a(Params... paramsArr);

    /* access modifiers changed from: private */
    public void c(Result result) {
        if (!this.i.get()) {
            d(result);
        }
    }

    /* access modifiers changed from: private */
    public Result d(Result result) {
        d.obtainMessage(1, new f(this, result)).sendToTarget();
        return result;
    }

    /* access modifiers changed from: protected */
    public void a(Result result) {
    }

    /* access modifiers changed from: protected */
    public void b(Progress... progressArr) {
    }

    /* access modifiers changed from: protected */
    public void b(Result result) {
        a();
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    public final boolean b() {
        return this.g.isCancelled();
    }

    /* access modifiers changed from: private */
    public void e(Result result) {
        if (b()) {
            b(result);
        } else {
            a((Object) result);
        }
        this.h = Status.FINISHED;
    }
}
