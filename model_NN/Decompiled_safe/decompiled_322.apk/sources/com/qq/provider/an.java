package com.qq.provider;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import com.tencent.tmsecurelite.commom.ServiceManager;
import com.tencent.tmsecurelite.virusscan.IVirusScan;

/* compiled from: ProGuard */
final class an implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ak f331a;
    private int b;
    private volatile boolean c;

    private an(ak akVar) {
        this.f331a = akVar;
        this.b = 0;
        this.c = false;
    }

    /* synthetic */ an(ak akVar, al alVar) {
        this(akVar);
    }

    public void a(int i) {
        if (i > 0) {
            this.b = i;
        } else {
            this.b = 0;
        }
    }

    public void a() {
        synchronized (this.f331a) {
            this.f331a.notifyAll();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.provider.ak.a(com.qq.provider.ak, boolean, com.tencent.tmsecurelite.virusscan.IVirusScan):void
     arg types: [com.qq.provider.ak, int, ?[OBJECT, ARRAY]]
     candidates:
      com.qq.provider.ak.a(android.content.Context, com.qq.g.c, int):void
      com.qq.provider.ak.a(com.qq.provider.ak, int, int):void
      com.qq.provider.ak.a(com.qq.provider.ak, int, com.qq.d.b):void
      com.qq.provider.ak.a(com.qq.provider.ak, int, java.util.ArrayList):void
      com.qq.provider.ak.a(android.content.Context, java.io.BufferedInputStream, java.io.BufferedOutputStream):void
      com.qq.provider.ak.a(com.qq.provider.ak, boolean, com.tencent.tmsecurelite.virusscan.IVirusScan):void */
    public int b(int i) {
        if (i == 0) {
            Log.d("com.qq.connect", "no wait");
            return 0;
        }
        this.f331a.a(true, (IVirusScan) null);
        if (i > 0 && this.f331a.b == null) {
            this.c = true;
            synchronized (this.f331a) {
                try {
                    this.f331a.wait((long) this.b);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            this.c = false;
        }
        if (this.f331a.b == null) {
            try {
                Thread.sleep(850);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        if (this.f331a.b == null) {
            return -5;
        }
        return 1;
    }

    public synchronized void onServiceDisconnected(ComponentName componentName) {
        Log.d("com.qq.connect", "onServiceDisconnected");
        IVirusScan unused = this.f331a.b = (IVirusScan) null;
        a();
        synchronized (ak.class) {
            ak unused2 = ak.f328a = (ak) null;
        }
        if (this.f331a.i == null) {
            h.a(1);
        } else {
            this.f331a.a(this.f331a.b(1));
        }
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        Log.d("com.qq.connect", "onServiceConnected");
        try {
            IVirusScan unused = this.f331a.b = (IVirusScan) ServiceManager.getInterface(1, iBinder);
        } catch (Exception e) {
            e.printStackTrace();
        }
        a();
        if (this.f331a.i == null) {
            h.a(0);
        } else {
            this.f331a.a(this.f331a.b(0));
        }
    }
}
