package com.qq.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import com.qq.AppService.IPCService;
import com.qq.AppService.r;
import com.qq.d.d.a;
import com.qq.g.c;
import com.qq.ndk.Native;
import com.qq.util.b;
import com.qq.util.j;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class o {

    /* renamed from: a  reason: collision with root package name */
    private static o f346a = null;
    private long b = 0;
    private long c = 0;

    private o(Context context) {
        new ah(context).start();
        this.b = System.currentTimeMillis();
    }

    public static synchronized o a(Context context) {
        o oVar;
        synchronized (o.class) {
            if (f346a == null) {
                f346a = new o(context);
            }
            oVar = f346a;
        }
        return oVar;
    }

    public static void a() {
        if (f346a != null) {
            f346a = null;
        }
    }

    public static void a(a aVar, ArrayList<byte[]> arrayList) {
        if (arrayList != null) {
            arrayList.add(r.a(aVar.f278a));
            arrayList.add(r.a(aVar.b));
            arrayList.add(r.a(aVar.c));
            arrayList.add(r.a(aVar.d));
            arrayList.add(r.a(aVar.e));
            arrayList.add(r.a(aVar.f));
            arrayList.add(r.a(aVar.g));
            arrayList.add(r.a(aVar.h));
            arrayList.add(r.a(aVar.i));
            arrayList.add(r.a(aVar.j));
            arrayList.add(r.a(aVar.k));
            arrayList.add(r.a(aVar.n));
        }
    }

    public void a(Context context, c cVar) {
        File file;
        File file2;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h <= 0) {
            cVar.a(1);
        } else if (cVar.e() < h + 1) {
            cVar.a(1);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("image_id");
            sb.append(" in (");
            StringBuilder sb2 = new StringBuilder();
            sb2.append("_id");
            sb2.append(" in (");
            for (int i = 0; i < h; i++) {
                int h2 = cVar.h();
                if (i != 0) {
                    sb.append(", ");
                }
                sb.append(h2);
                if (i != 0) {
                    sb2.append(", ");
                }
                sb2.append(h2);
            }
            sb.append(" )");
            sb2.append(" )");
            String sb3 = sb.toString();
            String sb4 = sb2.toString();
            Cursor query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "_data"}, sb4, null, "_id");
            if (query == null) {
                cVar.a(8);
                return;
            }
            for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
                String string = query.getString(query.getColumnIndex("_data"));
                if (!(string == null || (file2 = new File(string)) == null || !file2.exists())) {
                    file2.delete();
                }
            }
            query.close();
            context.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, sb4, null);
            Cursor query2 = context.getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, null, sb3, null, null);
            if (query2 != null) {
                for (boolean moveToFirst2 = query2.moveToFirst(); moveToFirst2; moveToFirst2 = query2.moveToNext()) {
                    String string2 = query2.getString(query2.getColumnIndex("_data"));
                    if (!(string2 == null || (file = new File(string2)) == null || !file.exists())) {
                        file.delete();
                    }
                }
                query2.close();
                context.getContentResolver().delete(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, sb3, null);
                cVar.a(0);
            }
        }
    }

    public void b(Context context, c cVar) {
        long j;
        String str;
        String str2;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        File b2 = n.b();
        if (b2 == null) {
            cVar.a(9);
            return;
        }
        int h = cVar.h();
        Log.d("com.qq.connect", "replaceImage2 " + h);
        Uri withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        String str3 = null;
        if (query.moveToFirst()) {
            str3 = query.getString(query.getColumnIndex("_data"));
            j = query.getLong(query.getColumnIndex("datetaken"));
        } else {
            j = 0;
        }
        query.close();
        if (str3 == null) {
            context.getContentResolver().delete(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "image_id = " + h, null);
            context.getContentResolver().delete(withAppendedPath, null, null);
            cVar.a(7);
            return;
        }
        File file = new File(str3);
        if (!file.exists()) {
            context.getContentResolver().delete(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "image_id = " + h, null);
            context.getContentResolver().delete(withAppendedPath, null, null);
            cVar.a(6);
            return;
        }
        Cursor query2 = context.getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, null, "image_id = " + h, null, null);
        if (query2 != null) {
            for (boolean moveToFirst = query2.moveToFirst(); moveToFirst; moveToFirst = query2.moveToNext()) {
                String string = query2.getString(query2.getColumnIndex("_data"));
                if (string != null) {
                    new File(string).delete();
                }
            }
            query2.close();
            context.getContentResolver().delete(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "image_id = " + h, null);
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("_size", Long.valueOf(file.length()));
        contentValues.put("date_modified", Long.valueOf(System.currentTimeMillis() / 1000));
        if (j > 0) {
            contentValues.put("datetaken", Long.valueOf(j));
        }
        context.getContentResolver().update(withAppendedPath, contentValues, null, null);
        if (ah.f325a != null) {
            str = ah.f325a.getAbsolutePath();
        } else {
            str = b2.getAbsolutePath() + "/DCIM/.thumbnails";
            if (!new File(str).mkdirs()) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(r.hr);
                cVar.a(arrayList);
                cVar.a(0);
                return;
            }
        }
        try {
            str2 = ah.a(context, h, file.getAbsolutePath(), str);
        } catch (Exception e) {
            e.printStackTrace();
            str2 = null;
        }
        if (str2 == null) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(r.hr);
            cVar.a(arrayList2);
            cVar.a(0);
            return;
        }
        ArrayList arrayList3 = new ArrayList();
        arrayList3.add(r.a(str2));
        cVar.a(0);
        cVar.a(arrayList3);
    }

    public void c(Context context, c cVar) {
        long j;
        String str;
        String str2;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        File b2 = n.b();
        if (b2 == null) {
            cVar.a(9);
            return;
        }
        int h = cVar.h();
        Uri withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        String str3 = null;
        if (query.moveToFirst()) {
            str3 = query.getString(query.getColumnIndex("_data"));
            j = query.getLong(query.getColumnIndex("datetaken"));
        } else {
            j = 0;
        }
        query.close();
        Cursor query2 = context.getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, null, "image_id = " + h, null, null);
        if (query2 != null) {
            for (boolean moveToFirst = query2.moveToFirst(); moveToFirst; moveToFirst = query2.moveToNext()) {
                String string = query2.getString(query2.getColumnIndex("_data"));
                if (string != null) {
                    new File(string).delete();
                }
            }
            query2.close();
            context.getContentResolver().delete(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "image_id = " + h, null);
        }
        File file = new File(str3);
        if (!file.exists()) {
            context.getContentResolver().delete(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "image_id = " + h, null);
            context.getContentResolver().delete(withAppendedPath, null, null);
            cVar.a(7);
        } else if (!file.canRead() || !file.canWrite()) {
            cVar.a(4);
        } else {
            byte[] k = cVar.k();
            if (k == null || k.length < 8) {
                cVar.a(1);
                return;
            }
            File file2 = new File(str3 + "tmp");
            try {
                file2.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            n.b(file2, k);
            cVar.b = null;
            System.gc();
            ContentValues contentValues = new ContentValues();
            contentValues.put("_size", Long.valueOf(file2.length()));
            contentValues.put("date_modified", Long.valueOf(System.currentTimeMillis()));
            if (j > 0) {
                contentValues.put("datetaken", Long.valueOf(j));
            }
            context.getContentResolver().update(withAppendedPath, contentValues, null, null);
            if (ah.f325a != null) {
                str = ah.f325a.getAbsolutePath();
            } else {
                str = b2.getAbsolutePath() + "/DCIM/.thumbnails";
                File file3 = new File(str);
                if (!file3.exists() && !file3.mkdirs()) {
                    cVar.a(4);
                    return;
                }
            }
            new Native().sync();
            String a2 = ah.a(context, h, file2.getAbsolutePath(), str, true);
            if (a2 == null) {
                str2 = ah.a(context, h, file2.getAbsolutePath(), str, true);
            } else {
                str2 = a2;
            }
            if (str2 == null) {
                file2.delete();
                cVar.a(8);
                return;
            }
            file.delete();
            file2.renameTo(file);
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(str2));
            cVar.a(0);
            cVar.a(arrayList);
        }
    }

    public void d(Context context, c cVar) {
        Uri withAppendedPath;
        FileInputStream fileInputStream;
        FileDescriptor fileDescriptor;
        if (cVar.e() < 2) {
            cVar.a(0);
            return;
        }
        int h = cVar.h();
        if (cVar.h() > 0) {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        } else {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        }
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            cVar.a(1);
            query.close();
        } else {
            String string = query.getString(query.getColumnIndex("_data"));
            query.close();
            if (string == null) {
                cVar.a(7);
                return;
            }
            try {
                if (!new File(string).exists()) {
                    cVar.a(6);
                    return;
                }
                try {
                    fileInputStream = new FileInputStream(string);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    fileInputStream = null;
                }
                if (fileInputStream == null) {
                    cVar.a(6);
                }
                try {
                    fileDescriptor = fileInputStream.getFD();
                } catch (IOException e2) {
                    e2.printStackTrace();
                    fileDescriptor = null;
                }
                if (fileDescriptor == null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                    cVar.a(8);
                    return;
                }
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 1;
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
                try {
                    fileInputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                ArrayList arrayList = new ArrayList();
                arrayList.add(r.a(options.outHeight));
                arrayList.add(r.a(options.outWidth));
                cVar.a(0);
                cVar.a(arrayList);
            } catch (Exception e5) {
                cVar.a(4);
            }
        }
    }

    public void e(Context context, c cVar) {
        FileInputStream fileInputStream;
        FileDescriptor fileDescriptor;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        try {
            if (!new File(j).exists()) {
                cVar.a(6);
                return;
            }
            try {
                fileInputStream = new FileInputStream(j);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                fileInputStream = null;
            }
            if (fileInputStream == null) {
                cVar.a(6);
            }
            try {
                fileDescriptor = fileInputStream.getFD();
            } catch (IOException e2) {
                e2.printStackTrace();
                fileDescriptor = null;
            }
            if (fileDescriptor == null) {
                try {
                    fileInputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
                cVar.a(8);
                return;
            }
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
            try {
                fileInputStream.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(options.outHeight));
            arrayList.add(r.a(options.outWidth));
            cVar.a(0);
            cVar.a(arrayList);
        } catch (Exception e5) {
            e5.printStackTrace();
            cVar.a(4);
        }
    }

    public void f(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
        } else if (!new File(j).exists()) {
            cVar.a(1);
        } else {
            Cursor query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, "_data = '" + j + "'", null, null);
            if (query == null) {
                cVar.a(8);
                return;
            }
            ArrayList arrayList = new ArrayList();
            if (query.moveToFirst()) {
                arrayList.add(r.a(query.getInt(query.getColumnIndex("_id"))));
                cVar.a(0);
            } else {
                cVar.a(7);
            }
            query.close();
            cVar.a(arrayList);
        }
    }

    public void g(Context context, c cVar) {
        Cursor query;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h <= 0 || Environment.getExternalStorageState().equals("mounted")) {
            if (h == 1) {
                query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_size"}, null, null, null);
            } else {
                query = context.getContentResolver().query(MediaStore.Images.Media.INTERNAL_CONTENT_URI, new String[]{"_size"}, null, null, null);
            }
            if (query == null) {
                cVar.a(8);
                return;
            }
            long j = 0;
            for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
                j += query.getLong(query.getColumnIndex("_size"));
            }
            query.close();
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(j));
            cVar.a(arrayList);
            cVar.a(0);
            return;
        }
        cVar.a(9);
    }

    public void h(Context context, c cVar) {
        Cursor query;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h <= 0 || Environment.getExternalStorageState().equals("mounted")) {
            if (h == 1) {
                query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id"}, null, null, null);
            } else {
                query = context.getContentResolver().query(MediaStore.Images.Media.INTERNAL_CONTENT_URI, new String[]{"_id"}, null, null, null);
            }
            if (query == null) {
                cVar.a(8);
                return;
            }
            int count = query.getCount();
            query.close();
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(count));
            cVar.a(arrayList);
            cVar.a(0);
            return;
        }
        cVar.a(9);
    }

    public void i(Context context, c cVar) {
        Cursor query;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        if (cVar.h() == 1) {
            query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id"}, null, null, null);
        } else {
            query = context.getContentResolver().query(MediaStore.Images.Media.INTERNAL_CONTENT_URI, new String[]{"_id"}, null, null, null);
        }
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(count));
        for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
            arrayList.add(r.a(query.getInt(query.getColumnIndex("_id"))));
        }
        query.close();
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void j(Context context, c cVar) {
        File file;
        int i;
        Cursor query;
        int i2;
        Cursor query2;
        int i3;
        Cursor query3;
        int i4;
        if (cVar.e() < 4) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        String j = cVar.j();
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis > this.c + 10000) {
            com.qq.f.a.a().b();
        }
        this.c = currentTimeMillis;
        if (!r.b(j)) {
            File file2 = new File(j);
            file = !file2.exists() ? null : file2;
        } else {
            file = null;
        }
        if (h2 <= 0) {
            cVar.a(1);
            return;
        }
        if (h <= 0) {
            if (h3 == 1) {
                query3 = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, j.b, null, null, "_id");
            } else {
                query3 = context.getContentResolver().query(MediaStore.Images.Media.INTERNAL_CONTENT_URI, j.b, null, null, "_id");
            }
            if (query3 != null) {
                if (query3.moveToLast()) {
                    i4 = query3.getInt(0) + 1;
                } else {
                    i4 = h;
                }
                query3.close();
                i = i4;
            } else {
                cVar.a(8);
                return;
            }
        } else {
            i = h;
        }
        if (h3 == 1) {
            query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, "_id<" + i, null, "_id");
        } else {
            query = context.getContentResolver().query(MediaStore.Images.Media.INTERNAL_CONTENT_URI, null, "_id<" + i, null, "_id");
        }
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        if (h2 <= count) {
            count = h2;
        }
        ArrayList arrayList = new ArrayList();
        boolean moveToLast = query.moveToLast();
        int i5 = 0;
        arrayList.add(r.a(count));
        int[] iArr = new int[count];
        int i6 = 0;
        while (i5 < count && moveToLast) {
            String string = query.getString(query.getColumnIndex("_data"));
            int i7 = query.getInt(query.getColumnIndex("_id"));
            if (string == null) {
                if (h3 == 1) {
                    context.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "_id = " + i7, null);
                }
                moveToLast = query.moveToPrevious();
                i3 = i5;
            } else if (file == null || string.toLowerCase().startsWith(file.getAbsolutePath().toLowerCase())) {
                byte[] blob = query.getBlob(query.getColumnIndex("_display_name"));
                long j2 = query.getLong(query.getColumnIndex("_size"));
                byte[] blob2 = query.getBlob(query.getColumnIndex("title"));
                byte[] blob3 = query.getBlob(query.getColumnIndex("mime_type"));
                byte[] blob4 = query.getBlob(query.getColumnIndex(SocialConstants.PARAM_COMMENT));
                long j3 = 1000 * query.getLong(query.getColumnIndex("date_added"));
                long j4 = 1000 * query.getLong(query.getColumnIndex("date_modified"));
                long j5 = query.getLong(query.getColumnIndex("datetaken"));
                int i8 = query.getInt(query.getColumnIndex("isprivate"));
                if (blob == null) {
                    blob = r.hr;
                }
                if (blob2 == null) {
                    blob2 = r.hr;
                }
                if (blob3 == null) {
                    blob3 = r.hr;
                }
                if (blob4 == null) {
                    blob4 = r.hr;
                }
                if (j4 == 0 && j5 != 0) {
                    j4 = j5;
                } else if (j4 == 0 && j3 != 0) {
                    j4 = j3;
                } else if (j4 == 0 || j4 > System.currentTimeMillis()) {
                    j4 = new File(string).lastModified();
                }
                if (j3 < 0) {
                    j3 = 0;
                }
                if (j3 > 3153600000000L) {
                    j3 /= 1000;
                }
                if (j5 < 0) {
                    j5 = 0;
                }
                if (j5 > 3153600000000L) {
                    j5 /= 1000;
                }
                iArr[i5] = i7;
                arrayList.add(r.a(i7));
                arrayList.add(r.a(string));
                arrayList.add(blob);
                arrayList.add(r.a(j2));
                arrayList.add(blob2);
                arrayList.add(blob3);
                arrayList.add(blob4);
                arrayList.add(r.a(j3));
                arrayList.add(r.a(j4));
                arrayList.add(r.a(j5));
                arrayList.add(r.a(i8));
                moveToLast = query.moveToPrevious();
                i3 = i5 + 1;
            } else {
                moveToLast = query.moveToPrevious();
                i3 = i5;
            }
            i6++;
            i5 = i3;
        }
        query.close();
        arrayList.set(0, r.a(i5));
        int size = arrayList.size();
        arrayList.add(r.a(0));
        if (i5 >= 1) {
            i2 = iArr[i5 - 1];
        } else {
            i2 = 0;
        }
        if (h3 == 1) {
            query2 = context.getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, null, "image_id < " + i + " and " + "image_id" + " >= " + i2, null, "image_id");
        } else {
            query2 = context.getContentResolver().query(MediaStore.Images.Thumbnails.INTERNAL_CONTENT_URI, null, "image_id < " + i + " and " + "image_id" + " >= " + i2, null, "image_id");
        }
        if (query2 == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst = query2.moveToFirst();
        int i9 = 0;
        while (moveToFirst) {
            int i10 = query2.getInt(query2.getColumnIndex("image_id"));
            int i11 = query2.getInt(query2.getColumnIndex("_id"));
            if (i10 <= 0) {
                moveToFirst = query2.moveToNext();
                if (h3 == 1) {
                    context.getContentResolver().delete(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "image_id = " + i11, null);
                } else {
                    context.getContentResolver().delete(MediaStore.Images.Thumbnails.INTERNAL_CONTENT_URI, "image_id = " + i11, null);
                }
            } else {
                int i12 = 0;
                while (true) {
                    if (i12 >= i5) {
                        i12 = -1;
                        break;
                    } else if (i10 == iArr[i12]) {
                        break;
                    } else {
                        i12++;
                    }
                }
                if (i12 == -1) {
                    moveToFirst = query2.moveToNext();
                } else {
                    int i13 = query2.getInt(query2.getColumnIndex("kind"));
                    if (i13 == 3 || i13 == 1 || i13 == 2) {
                        moveToFirst = query2.moveToNext();
                    } else {
                        String string2 = query2.getString(query2.getColumnIndex("_data"));
                        if (string2 == null || !new File(string2).exists()) {
                            moveToFirst = query2.moveToNext();
                            if (h3 == 1) {
                                context.getContentResolver().delete(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "image_id = " + i11, null);
                            } else {
                                context.getContentResolver().delete(MediaStore.Images.Thumbnails.INTERNAL_CONTENT_URI, "image_id = " + i11, null);
                            }
                        } else {
                            arrayList.add(r.a(i12));
                            arrayList.add(r.a(string2));
                            i9++;
                            moveToFirst = query2.moveToNext();
                        }
                    }
                }
            }
        }
        query2.close();
        arrayList.set(size, r.a(i9));
        cVar.a(arrayList);
        cVar.a(0);
        com.qq.k.a.b = System.currentTimeMillis();
        com.qq.k.a.a(context);
    }

    public void k(Context context, c cVar) {
        File file;
        Cursor query;
        int i;
        Cursor query2;
        if (cVar.e() < 4) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        String j = cVar.j();
        if (!r.b(j)) {
            File file2 = new File(j);
            file = !file2.exists() ? null : file2;
        } else {
            file = null;
        }
        if (h2 <= 0) {
            cVar.a(1);
            return;
        }
        if (h3 > 0) {
            query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, "_id>" + h, null, "_id");
        } else {
            query = context.getContentResolver().query(MediaStore.Images.Media.INTERNAL_CONTENT_URI, null, "_id>" + h, null, "_id");
        }
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        if (h2 <= count) {
            count = h2;
        }
        boolean moveToFirst = query.moveToFirst();
        String str = null;
        if (file != null) {
            str = file.getAbsolutePath().toLowerCase();
        }
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        int i3 = 0;
        while (i2 < count && moveToFirst) {
            a aVar = new a(query, str);
            if (aVar.f278a > 0) {
                arrayList.add(aVar);
                i2++;
            }
            moveToFirst = query.moveToNext();
            i3++;
        }
        query.close();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(r.a(i2));
        a[] aVarArr = new a[arrayList.size()];
        int i4 = 0;
        while (true) {
            int i5 = i4;
            if (i5 >= arrayList.size()) {
                break;
            }
            aVarArr[i5] = (a) arrayList.get(i5);
            i4 = i5 + 1;
        }
        if (aVarArr.length >= 1) {
            i = aVarArr[aVarArr.length - 1].f278a;
        } else {
            i = 0;
        }
        if (h3 == 1) {
            query2 = context.getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, null, "image_id > " + h + " and " + "image_id" + " <= " + i, null, "image_id");
        } else {
            query2 = context.getContentResolver().query(MediaStore.Images.Thumbnails.INTERNAL_CONTENT_URI, null, "image_id > " + h + " and " + "image_id" + " <= " + i, null, "image_id");
        }
        if (query2 == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst2 = query2.moveToFirst();
        while (moveToFirst2) {
            int i6 = query2.getInt(query2.getColumnIndex("image_id"));
            int i7 = 0;
            while (true) {
                if (i7 >= aVarArr.length) {
                    i7 = -1;
                    break;
                } else if (i6 == aVarArr[i7].f278a) {
                    break;
                } else {
                    i7++;
                }
            }
            if (i7 != -1) {
                int i8 = query2.getInt(query2.getColumnIndex("kind"));
                String string = query2.getString(query2.getColumnIndex("_data"));
                int i9 = query2.getInt(query2.getColumnIndex("_id"));
                if (string == null || !new File(string).exists()) {
                    moveToFirst2 = query2.moveToNext();
                    if (h3 == 1) {
                        context.getContentResolver().delete(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "image_id = " + i9, null);
                    } else {
                        context.getContentResolver().delete(MediaStore.Images.Thumbnails.INTERNAL_CONTENT_URI, "image_id = " + i9, null);
                    }
                } else {
                    if (i8 == 135956) {
                        aVarArr[i7].n = string;
                    }
                    if (i8 == 1 && aVarArr[i7].n == null) {
                        aVarArr[i7].n = string;
                    } else if (aVarArr[i7].n == null && i8 == 2) {
                        aVarArr[i7].n = string;
                    }
                }
            }
            moveToFirst2 = query2.moveToNext();
        }
        query2.close();
        for (a a2 : aVarArr) {
            a(a2, arrayList2);
        }
        cVar.a(arrayList2);
        cVar.a(0);
        if (h <= 0) {
            com.qq.k.a.b = System.currentTimeMillis();
            com.qq.k.a.a(context);
        }
    }

    public void l(Context context, c cVar) {
        File file;
        int i;
        Cursor query;
        int i2;
        int i3;
        Cursor query2;
        int i4;
        int i5;
        Cursor query3;
        int i6;
        if (cVar.e() < 4) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        String j = cVar.j();
        if (h <= 0) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis > this.c + 10000) {
                Log.d("com.qq.connect", "fresh  scanCameraAsync");
                com.qq.f.a.a().b();
            }
            this.c = currentTimeMillis;
            if (currentTimeMillis > this.b + 480000) {
                new ah(context).start();
                this.b = currentTimeMillis;
            }
        }
        if (!r.b(j)) {
            File file2 = new File(j);
            file = !file2.exists() ? null : file2;
        } else {
            file = null;
        }
        if (h2 <= 0) {
            cVar.a(1);
            return;
        }
        if (h <= 0) {
            if (h3 == 1) {
                query3 = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, j.b, null, null, "_id");
            } else {
                query3 = context.getContentResolver().query(MediaStore.Images.Media.INTERNAL_CONTENT_URI, j.b, null, null, "_id");
            }
            if (query3 != null) {
                if (query3.moveToLast()) {
                    i6 = query3.getInt(0) + 1;
                } else {
                    i6 = h;
                }
                query3.close();
                i = i6;
            } else {
                cVar.a(8);
                return;
            }
        } else {
            i = h;
        }
        if (h3 == 1) {
            query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, "_id<" + i, null, "_id");
        } else {
            query = context.getContentResolver().query(MediaStore.Images.Media.INTERNAL_CONTENT_URI, null, "_id<" + i, null, "_id");
        }
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        if (h2 > count) {
            i2 = count;
        } else {
            i2 = h2;
        }
        ArrayList arrayList = new ArrayList();
        boolean moveToLast = query.moveToLast();
        int i7 = 0;
        arrayList.add(r.a(i2));
        int[] iArr = new int[i2];
        int i8 = 0;
        while (i7 < i2 && moveToLast) {
            String string = query.getString(query.getColumnIndex("_data"));
            int i9 = query.getInt(query.getColumnIndex("_id"));
            if (string == null) {
                if (h3 == 1) {
                    context.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "_id = " + i9, null);
                }
                moveToLast = query.moveToPrevious();
                i5 = i7;
            } else if (file == null || string.toLowerCase().startsWith(file.getAbsolutePath().toLowerCase())) {
                byte[] blob = query.getBlob(query.getColumnIndex("_display_name"));
                long j2 = query.getLong(query.getColumnIndex("_size"));
                byte[] blob2 = query.getBlob(query.getColumnIndex("title"));
                byte[] blob3 = query.getBlob(query.getColumnIndex("mime_type"));
                byte[] blob4 = query.getBlob(query.getColumnIndex(SocialConstants.PARAM_COMMENT));
                long j3 = 1000 * query.getLong(query.getColumnIndex("date_added"));
                long j4 = 1000 * query.getLong(query.getColumnIndex("date_modified"));
                long j5 = query.getLong(query.getColumnIndex("datetaken"));
                int i10 = query.getInt(query.getColumnIndex("isprivate"));
                int b2 = b.b().b(string);
                int i11 = query.getInt(query.getColumnIndex("orientation"));
                if (i11 == 90) {
                    i4 = 6;
                } else if (i11 == 180) {
                    i4 = 3;
                } else if (i11 == 270) {
                    i4 = 8;
                } else {
                    i4 = 1;
                }
                if (blob == null) {
                    blob = r.hr;
                }
                if (blob2 == null) {
                    blob2 = r.hr;
                }
                if (blob3 == null) {
                    blob3 = r.hr;
                }
                if (blob4 == null) {
                    blob4 = r.hr;
                }
                if (j4 == 0 && j5 != 0) {
                    j4 = j5;
                } else if (j4 == 0 && j3 != 0) {
                    j4 = j3;
                } else if (j4 == 0 || j4 > System.currentTimeMillis()) {
                    j4 = new File(string).lastModified();
                }
                if (j3 < 0) {
                    j3 = 0;
                }
                if (j3 > 3153600000000L) {
                    j3 /= 1000;
                }
                if (j5 < 0) {
                    j5 = 0;
                }
                if (j5 > 3153600000000L) {
                    j5 /= 1000;
                }
                iArr[i7] = i9;
                arrayList.add(r.a(i9));
                arrayList.add(r.a(string));
                arrayList.add(blob);
                arrayList.add(r.a(j2));
                arrayList.add(blob2);
                arrayList.add(blob3);
                arrayList.add(blob4);
                arrayList.add(r.a(j3));
                arrayList.add(r.a(j4));
                arrayList.add(r.a(j5));
                arrayList.add(r.a(i10));
                arrayList.add(r.a(b2));
                arrayList.add(r.a(i4));
                moveToLast = query.moveToPrevious();
                i5 = i7 + 1;
            } else {
                moveToLast = query.moveToPrevious();
                i5 = i7;
            }
            i8++;
            i7 = i5;
        }
        query.close();
        arrayList.set(0, r.a(i7));
        int size = arrayList.size();
        arrayList.add(r.a(0));
        if (i7 >= 1) {
            i3 = iArr[i7 - 1];
        } else {
            i3 = 0;
        }
        if (h3 == 1) {
            query2 = context.getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, null, "image_id < " + i + " and " + "image_id" + " >= " + i3, null, "image_id");
        } else {
            query2 = context.getContentResolver().query(MediaStore.Images.Thumbnails.INTERNAL_CONTENT_URI, null, "image_id < " + i + " and " + "image_id" + " >= " + i3, null, "image_id");
        }
        if (query2 == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst = query2.moveToFirst();
        int i12 = 0;
        while (moveToFirst) {
            int i13 = query2.getInt(query2.getColumnIndex("image_id"));
            int i14 = query2.getInt(query2.getColumnIndex("_id"));
            if (i13 <= 0) {
                moveToFirst = query2.moveToNext();
                if (h3 == 1) {
                    context.getContentResolver().delete(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "image_id = " + i14, null);
                } else {
                    context.getContentResolver().delete(MediaStore.Images.Thumbnails.INTERNAL_CONTENT_URI, "image_id = " + i14, null);
                }
            } else {
                int i15 = 0;
                while (true) {
                    if (i15 >= i7) {
                        i15 = -1;
                        break;
                    } else if (i13 == iArr[i15]) {
                        break;
                    } else {
                        i15++;
                    }
                }
                if (i15 == -1) {
                    moveToFirst = query2.moveToNext();
                } else {
                    String string2 = query2.getString(query2.getColumnIndex("_data"));
                    if (string2 == null || !new File(string2).exists()) {
                        moveToFirst = query2.moveToNext();
                        if (h3 == 1) {
                            context.getContentResolver().delete(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "image_id = " + i14, null);
                        } else {
                            context.getContentResolver().delete(MediaStore.Images.Thumbnails.INTERNAL_CONTENT_URI, "image_id = " + i14, null);
                        }
                    } else {
                        arrayList.add(r.a(i15));
                        arrayList.add(r.a(string2));
                        i12++;
                        moveToFirst = query2.moveToNext();
                    }
                }
            }
        }
        query2.close();
        arrayList.set(size, r.a(i12));
        cVar.a(arrayList);
        cVar.a(0);
        if (i7 < i2) {
            com.qq.k.a.b = System.currentTimeMillis();
            com.qq.k.a.a(context);
        }
    }

    public void m(Context context, c cVar) {
        String[] strArr;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (cVar.e() < h + 1) {
            cVar.a(1);
            return;
        }
        if (h > 0) {
            strArr = new String[(h + 1)];
            strArr[0] = "/DCIM/";
            for (int i = 1; i <= h; i++) {
                strArr[i] = cVar.j();
            }
        } else {
            strArr = new String[]{"/DCIM/"};
        }
        StringBuilder sb = new StringBuilder(1024);
        for (int i2 = 0; i2 < strArr.length; i2++) {
            if (i2 > 0) {
                sb.append(" or ");
            }
            sb.append("_data like '%" + strArr[i2] + "%' ");
        }
        Cursor query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, j.b, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        query.close();
        Cursor query2 = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, j.b, sb.toString(), null, null);
        if (query2 == null) {
            cVar.a(8);
            return;
        }
        int count2 = query2.getCount();
        query2.close();
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(count));
        arrayList.add(r.a(count2));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void n(Context context, c cVar) {
        Uri withAppendedPath;
        Uri uri;
        if (cVar.e() < 2) {
            cVar.a(0);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (h2 == 1) {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        } else {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        }
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst = query.moveToFirst();
        ArrayList arrayList = new ArrayList();
        if (moveToFirst) {
            a aVar = new a(query, null);
            query.close();
            if (h2 == 1) {
                uri = MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI;
            } else {
                uri = MediaStore.Images.Media.INTERNAL_CONTENT_URI;
            }
            Cursor query2 = context.getContentResolver().query(uri, null, "image_id = " + aVar.f278a + " and " + "kind" + " = " + 1, null, null);
            if (query2 == null) {
                cVar.a(8);
                return;
            }
            if (query2.moveToFirst()) {
                aVar.n = query2.getString(query2.getColumnIndex("_data"));
            }
            query2.close();
            a(aVar, arrayList);
            if (aVar.o == 90) {
                aVar.o = 6;
            } else if (aVar.o == 180) {
                aVar.o = 3;
            } else if (aVar.o == 270) {
                aVar.o = 8;
            } else {
                aVar.o = 1;
            }
            arrayList.add(r.a(aVar.o));
            arrayList.add(r.a(b.b().b(aVar.b)));
            cVar.a(arrayList);
            cVar.a(0);
            return;
        }
        cVar.a(1);
        query.close();
    }

    public void o(Context context, c cVar) {
        Uri withAppendedPath;
        Uri uri;
        if (cVar.e() < 2) {
            cVar.a(0);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (h2 == 1) {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        } else {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        }
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst = query.moveToFirst();
        ArrayList arrayList = new ArrayList();
        if (moveToFirst) {
            a aVar = new a(query, null);
            query.close();
            if (h2 == 1) {
                uri = MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI;
            } else {
                uri = MediaStore.Images.Media.INTERNAL_CONTENT_URI;
            }
            Cursor query2 = context.getContentResolver().query(uri, null, "image_id = " + aVar.f278a + " and " + "kind" + " = " + 1, null, null);
            if (query2 == null) {
                cVar.a(8);
                return;
            }
            if (query2.moveToFirst()) {
                aVar.n = query2.getString(query2.getColumnIndex("_data"));
            }
            query2.close();
            a(aVar, arrayList);
            cVar.a(arrayList);
            cVar.a(0);
            return;
        }
        cVar.a(1);
        query.close();
    }

    public void p(Context context, c cVar) {
        Uri uri;
        Cursor query;
        String str = null;
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        if (h3 > 0) {
            uri = MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI;
        } else {
            uri = MediaStore.Images.Thumbnails.INTERNAL_CONTENT_URI;
        }
        Cursor query2 = context.getContentResolver().query(uri, null, "image_id=" + h, null, null);
        if (query2 == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst = query2.moveToFirst();
        if (!moveToFirst) {
            cVar.a(1);
            query2.close();
            return;
        }
        ArrayList arrayList = new ArrayList();
        while (true) {
            if (!moveToFirst) {
                break;
            } else if (query2.getInt(query2.getColumnIndex("kind")) == h2) {
                String string = query2.getString(query2.getColumnIndex("_data"));
                int i = query2.getInt(query2.getColumnIndex("_id"));
                if (string != null && new File(string).exists()) {
                    arrayList.add(r.a(string));
                    break;
                }
                moveToFirst = query2.moveToNext();
                if (h3 == 1) {
                    context.getContentResolver().delete(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "image_id = " + i, null);
                } else {
                    context.getContentResolver().delete(MediaStore.Images.Thumbnails.INTERNAL_CONTENT_URI, "image_id = " + i, null);
                }
            } else {
                moveToFirst = query2.moveToNext();
            }
        }
        query2.close();
        if (!moveToFirst) {
            if (h3 > 0) {
                query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, "_id = " + h, null, null);
            } else {
                query = context.getContentResolver().query(MediaStore.Images.Media.INTERNAL_CONTENT_URI, null, "_id = " + h, null, null);
            }
            if (query != null) {
                if (query.moveToFirst()) {
                    str = query.getString(query.getColumnIndex("_data"));
                }
                query.close();
            }
            if (ah.f325a != null) {
                try {
                    String a2 = ah.a(context, h, str, ah.f325a.getAbsolutePath());
                    if (a2 != null) {
                        arrayList.add(r.a(a2));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (arrayList.size() > 0) {
            cVar.a(0);
        } else {
            cVar.a(7);
        }
        cVar.a(arrayList);
    }

    public void q(Context context, c cVar) {
        Uri uri;
        if (cVar.e() < 4) {
            cVar.a(0);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        int h4 = cVar.h();
        if (h2 <= 0) {
            cVar.a(0);
            return;
        }
        if (h4 == 1) {
            uri = MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI;
        } else {
            uri = MediaStore.Images.Thumbnails.INTERNAL_CONTENT_URI;
        }
        Cursor query = context.getContentResolver().query(uri, null, "image_id > " + h + " and " + "kind" + " = " + h3, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        if (count > h2) {
            count = h2;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(count));
        boolean moveToFirst = query.moveToFirst();
        if (!moveToFirst) {
            cVar.a(1);
            query.close();
            return;
        }
        boolean z = moveToFirst;
        for (int i = 0; i < count && z; i++) {
            String string = query.getString(query.getColumnIndex("_data"));
            arrayList.add(r.a(query.getInt(query.getColumnIndex("_id"))));
            arrayList.add(r.a(string));
            z = query.moveToFirst();
        }
        query.close();
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void r(Context context, c cVar) {
        Uri withAppendedPath;
        if (cVar.e() < 2) {
            cVar.a(0);
            return;
        }
        int h = cVar.h();
        if (cVar.h() == 1) {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        } else {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        }
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst = query.moveToFirst();
        ArrayList arrayList = new ArrayList();
        if (moveToFirst) {
            arrayList.add(r.a(query.getString(query.getColumnIndex("_data"))));
            query.close();
            cVar.a(arrayList);
            cVar.a(0);
            return;
        }
        cVar.a(1);
        query.close();
    }

    public void s(Context context, c cVar) {
        if (cVar.e() < a.p) {
            cVar.a(0);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        cVar.g();
        String j3 = cVar.j();
        ContentValues contentValues = new ContentValues();
        if (!r.b(j3)) {
            contentValues.put("title", j3);
        } else {
            contentValues.putNull("title");
        }
        if (!r.b(j)) {
            contentValues.put("_data", j);
        }
        if (!r.b(j2)) {
            contentValues.put("_display_name", j2);
        } else {
            contentValues.putNull("_display_name");
        }
        if (context.getContentResolver().update(Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h), contentValues, null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void t(Context context, c cVar) {
        FileInputStream fileInputStream;
        FileDescriptor fileDescriptor;
        String str;
        int i;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        if (!file.exists()) {
            cVar.a(6);
        } else if (file.length() < 32) {
            cVar.a(6);
        } else {
            try {
                fileInputStream = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                fileInputStream = null;
            }
            if (fileInputStream == null) {
                cVar.a(6);
                return;
            }
            try {
                fileDescriptor = fileInputStream.getFD();
            } catch (IOException e2) {
                e2.printStackTrace();
                fileDescriptor = null;
            }
            if (fileDescriptor == null) {
                try {
                    fileInputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
                cVar.a(6);
                return;
            }
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
            try {
                fileInputStream.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            if (options.outHeight <= 0 || options.outWidth <= 0 || options.outMimeType == null) {
                Log.d("com.qq.connect", "image is illegal");
                cVar.a(7);
                return;
            }
            if (options.outHeight * options.outWidth > 4194304) {
                Log.d("com.qq.connect", " is the image too large?");
            }
            Log.d("com.qq.connect", "insertImage!!!!!!!!!! wid:" + options.outWidth + " height:" + options.outHeight + "::::" + options.outMimeType);
            String str2 = options.outMimeType;
            if (str2 == null || !str2.contains("x-ms-bmp")) {
                str = str2;
            } else {
                str = "bmp";
            }
            Cursor query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id"}, null, null, "_id");
            if (query == null) {
                cVar.a(8);
                return;
            }
            ArrayList arrayList = new ArrayList();
            if (query != null) {
                if (query.moveToLast()) {
                    i = query.getInt(query.getColumnIndex("_id"));
                } else {
                    i = 0;
                }
                query.close();
                arrayList.add(r.a(i + 1));
            }
            try {
                new com.qq.a.a.b(context).a(j, str);
            } catch (Throwable th) {
                th.printStackTrace();
            }
            cVar.a(0);
            cVar.a(arrayList);
        }
    }

    public void u(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        Uri withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            cVar.a(7);
            query.close();
        } else {
            String string = query.getString(query.getColumnIndex("_data"));
            query.close();
            if (context.getContentResolver().delete(withAppendedPath, null, null) <= 0) {
                cVar.a(8);
            }
            try {
                new File(string).delete();
            } catch (Exception e) {
            }
            cVar.a(0);
            Cursor query2 = context.getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, null, "image_id = " + h, null, null);
            if (query2 != null) {
                boolean moveToFirst = query2.moveToFirst();
                if (!moveToFirst) {
                    query2.close();
                    return;
                }
                while (moveToFirst) {
                    new File(query2.getString(query2.getColumnIndex("_data"))).delete();
                    moveToFirst = query2.moveToNext();
                }
                query2.close();
                context.getContentResolver().delete(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "image_id = " + h, null);
            }
        }
    }

    public void v(Context context, c cVar) {
        boolean z = true;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
        } else if (!new File(j).exists()) {
            cVar.a(1);
        } else {
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeFile(j);
            } catch (Throwable th) {
                th.printStackTrace();
            }
            if (bitmap == null) {
                cVar.a(7);
                return;
            }
            try {
                context.setWallpaper(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
                z = false;
            }
            if (z) {
                cVar.a(0);
            } else {
                cVar.a(8);
            }
            cVar.a(0);
        }
    }

    public void w(Context context, c cVar) {
        Uri withAppendedPath;
        String str;
        boolean z;
        Bitmap bitmap = null;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (cVar.h() > 0) {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        } else {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        }
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(1);
            return;
        }
        if (query.moveToFirst()) {
            str = query.getString(query.getColumnIndex("_data"));
        } else {
            str = null;
        }
        query.close();
        if (str == null || !new File(str).exists()) {
            cVar.a(7);
            return;
        }
        try {
            bitmap = BitmapFactory.decodeFile(str);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        if (bitmap == null) {
            cVar.a(7);
            return;
        }
        try {
            context.setWallpaper(bitmap);
            z = true;
        } catch (IOException e) {
            e.printStackTrace();
            z = false;
        }
        if (z) {
            cVar.a(0);
        } else {
            cVar.a(8);
        }
    }

    public void x(Context context, c cVar) {
        Bitmap bitmap = ((BitmapDrawable) context.getWallpaper()).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        try {
            byteArrayOutputStream.close();
            ArrayList arrayList = new ArrayList();
            arrayList.add(byteArray);
            cVar.a(arrayList);
            cVar.a(0);
        } catch (IOException e) {
            cVar.a(8);
            e.printStackTrace();
        }
    }

    public void y(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        if (!file.exists()) {
            cVar.a(6);
            return;
        }
        Cursor query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, null, null, null);
        if (query == null) {
            cVar.a(1);
            return;
        }
        for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
            String string = query.getString(query.getColumnIndex("_data"));
            if (string == null) {
                Uri withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + query.getInt(query.getColumnIndex("_id")));
                context.getContentResolver().delete(withAppendedPath, null, null);
                context.getContentResolver().notifyChange(withAppendedPath, null);
            } else if (string.startsWith(file.getAbsolutePath())) {
                File file2 = new File(string);
                if (file2.exists()) {
                    file2.delete();
                }
                Uri withAppendedPath2 = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + query.getInt(query.getColumnIndex("_id")));
                context.getContentResolver().delete(withAppendedPath2, null, null);
                context.getContentResolver().notifyChange(withAppendedPath2, null);
            }
        }
        query.close();
        cVar.a(0);
    }

    public void z(Context context, c cVar) {
        Uri withAppendedPath;
        FileInputStream fileInputStream;
        FileDescriptor fileDescriptor;
        int i;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (cVar.h() > 0) {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        } else {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        }
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            cVar.a(1);
            query.close();
        } else {
            String string = query.getString(query.getColumnIndex("_data"));
            query.close();
            if (string == null) {
                cVar.a(7);
            } else if (!new File(string).exists()) {
                cVar.a(6);
            } else {
                try {
                    fileInputStream = new FileInputStream(string);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    fileInputStream = null;
                }
                if (fileInputStream == null) {
                    cVar.a(7);
                    return;
                }
                try {
                    fileDescriptor = fileInputStream.getFD();
                } catch (IOException e2) {
                    e2.printStackTrace();
                    fileDescriptor = null;
                }
                if (fileDescriptor == null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                    cVar.a(8);
                    return;
                }
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 1;
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
                Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
                defaultDisplay.getMetrics(new DisplayMetrics());
                int i2 = options.outHeight;
                int height = defaultDisplay.getHeight();
                if (i2 > height) {
                    i = i2 / height;
                    if (i2 - (height * i) >= height / 3) {
                        i++;
                    }
                } else {
                    i = 1;
                }
                options.inJustDecodeBounds = false;
                options.inSampleSize = i;
                options.inDither = false;
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap decodeFileDescriptor = BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
                try {
                    fileInputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                try {
                    context.setWallpaper(decodeFileDescriptor);
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
                cVar.a(1);
            }
        }
    }

    public void A(Context context, c cVar) {
        FileInputStream fileInputStream;
        FileDescriptor fileDescriptor;
        int i;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
        } else if (!new File(j).exists()) {
            cVar.a(6);
        } else {
            try {
                fileInputStream = new FileInputStream(j);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                fileInputStream = null;
            }
            if (fileInputStream == null) {
                cVar.a(7);
                return;
            }
            try {
                fileDescriptor = fileInputStream.getFD();
            } catch (IOException e2) {
                e2.printStackTrace();
                fileDescriptor = null;
            }
            if (fileDescriptor == null) {
                try {
                    fileInputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
                cVar.a(8);
                return;
            }
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            defaultDisplay.getMetrics(new DisplayMetrics());
            int i2 = options.outHeight;
            int height = defaultDisplay.getHeight();
            if (i2 > height) {
                i = i2 / height;
                if (i2 - (height * i) >= height / 3) {
                    i++;
                }
            } else {
                i = 1;
            }
            options.inJustDecodeBounds = false;
            options.inSampleSize = i;
            options.inDither = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap decodeFileDescriptor = BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
            try {
                fileInputStream.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            try {
                context.setWallpaper(decodeFileDescriptor);
            } catch (IOException e5) {
                e5.printStackTrace();
            }
            cVar.a(1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public void B(Context context, c cVar) {
        FileInputStream fileInputStream;
        FileDescriptor fileDescriptor;
        Bitmap bitmap;
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        int h2 = cVar.h();
        if (r.b(j) || h <= 0) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        if (!file.exists()) {
            cVar.a(6);
            return;
        }
        try {
            fileInputStream = new FileInputStream(j);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            fileInputStream = null;
        }
        if (fileInputStream == null) {
            cVar.a(7);
            return;
        }
        try {
            fileDescriptor = fileInputStream.getFD();
        } catch (IOException e2) {
            e2.printStackTrace();
            fileDescriptor = null;
        }
        if (fileDescriptor == null) {
            try {
                fileInputStream.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            cVar.a(8);
            return;
        }
        Log.d("com.qq.connect", "begin rotate");
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
        if (options.outHeight <= 0 || options.outWidth <= 0) {
            try {
                fileInputStream.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            cVar.a(7);
        } else if (options.outHeight * options.outWidth >= 6291456) {
            try {
                fileInputStream.close();
            } catch (IOException e5) {
                e5.printStackTrace();
            }
            cVar.a(7);
        } else {
            Log.d("com.qq.connect", " decode bitmap ");
            options.inJustDecodeBounds = false;
            options.inDither = false;
            Bitmap bitmap2 = null;
            try {
                bitmap2 = BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
            } catch (Throwable th) {
                th.printStackTrace();
            }
            if (bitmap2 == null) {
                try {
                    fileInputStream.close();
                } catch (IOException e6) {
                    e6.printStackTrace();
                }
                cVar.a(8);
                return;
            }
            Log.d("com.qq.connect", " load bitmap");
            try {
                fileInputStream.close();
            } catch (IOException e7) {
                e7.printStackTrace();
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            Matrix matrix = new Matrix();
            matrix.postRotate((float) h2);
            try {
                bitmap = Bitmap.createBitmap(bitmap2, 0, 0, bitmap2.getWidth(), bitmap2.getHeight(), matrix, true);
            } catch (Throwable th2) {
                th2.printStackTrace();
                bitmap = null;
            }
            Log.d("com.qq.connect", " rotate bitmap");
            bitmap2.recycle();
            if (bitmap == null) {
                cVar.a(8);
                return;
            }
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            Log.d("com.qq.connect", " compress bitmap");
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            try {
                byteArrayOutputStream.close();
            } catch (IOException e8) {
                e8.printStackTrace();
            }
            if (bitmap != null) {
                bitmap.recycle();
            }
            n.a(file, byteArray);
            com.qq.d.d.b.a(context, h);
            ContentValues contentValues = new ContentValues();
            contentValues.put("_size", Long.valueOf(file.length()));
            contentValues.put("date_modified", Long.valueOf(file.lastModified() / 1000));
            context.getContentResolver().update(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues, "_id = " + h, null);
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            if (externalStorageDirectory == null) {
                cVar.a(9);
                return;
            }
            String a2 = ah.a(context, h, j, externalStorageDirectory.getAbsolutePath() + File.separator + "DCIM" + File.separator + ".thumbnails", true);
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(a2));
            cVar.a(arrayList);
        }
    }

    public void C(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(com.qq.k.b.a(context)));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void D(Context context, c cVar) {
        com.qq.k.b.a(context, System.currentTimeMillis());
        cVar.a(0);
    }

    public void E(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        long i = cVar.i();
        cVar.a(0);
        if (IPCService.f203a != null && IPCService.f203a.b != null) {
            try {
                IPCService.f203a.b.a(i, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void F(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        long i = cVar.i();
        cVar.a(0);
        if (IPCService.f203a != null && IPCService.f203a.b != null) {
            try {
                IPCService.f203a.b.a(i, -1);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void G(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        long i = cVar.i();
        cVar.a(0);
        if (IPCService.f203a != null && IPCService.f203a.b != null) {
            try {
                IPCService.f203a.b.a(i, 1);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void H(Context context, c cVar) {
        int i = 90;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (h <= 0 || h2 < 0) {
            cVar.a(1);
            return;
        }
        if (h2 != 6) {
            if (h2 == 3) {
                i = 180;
            } else if (h2 == 8) {
                i = 270;
            } else if (h2 == 90 || h2 == 180) {
                i = h2;
            } else {
                i = h2 == 270 ? h2 : 0;
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("orientation", Integer.valueOf(i));
        if (context.getContentResolver().update(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues, "_id = " + h, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }
}
