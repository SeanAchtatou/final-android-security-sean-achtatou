package com.qq.provider;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import com.qq.AppService.r;
import com.qq.c.d;
import com.qq.c.h;
import com.qq.d.e.a;
import com.qq.g.c;
import com.qq.ndk.Native;
import com.qq.util.j;
import com.qq.util.s;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class v {

    /* renamed from: a  reason: collision with root package name */
    public static int f353a = -1;
    private static Object b = null;

    public void a(Context context, c cVar) {
        Native nativeR = new Native();
        nativeR.uncryptFile("/data/local/tmp/zergrush");
        nativeR.uncryptFile("/data/local/tmp/psneuter");
        nativeR.uncryptFile("/data/local/tmp/zerg");
        nativeR.uncryptFile("/data/local/tmp/su");
        nativeR.uncryptFile("/data/local/tmp/superuser.apk");
        nativeR.uncryptFile("/data/local/tmp/root");
        nativeR.uncryptFile("/data/local/tmp/libsuc.so");
        nativeR.uncryptFile("/data/local/tmp/libboy.so");
        nativeR.uncryptFile("/data/local/tmp/librootmgr-jni.so");
        nativeR.uncryptFile("/data/local/tmp/libxy.so");
    }

    public void b(Context context, c cVar) {
        if (b == null) {
            cVar.a(4);
        }
    }

    public void c(Context context, c cVar) {
        if (b == null) {
            cVar.a(4);
        }
    }

    public void d(Context context, c cVar) {
        String[] strArr;
        byte[] bArr;
        String str;
        int i;
        String str2;
        PackageInfo packageInfo;
        Drawable drawable;
        PackageManager packageManager = context.getPackageManager();
        Cursor query = context.getContentResolver().query(a.f279a, null, "type <> 2", null, null);
        if (query == null && b != null) {
            c(context, cVar);
        } else if (query == null) {
            cVar.a(7);
        } else {
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(query.getCount()));
            for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
                int i2 = query.getInt(query.getColumnIndex("_id"));
                arrayList.add(r.a(i2));
                arrayList.add(r.a(query.getInt(query.getColumnIndex(SocialConstants.PARAM_TYPE))));
                arrayList.add(r.a(query.getString(query.getColumnIndex("command"))));
                arrayList.add(r.a(query.getInt(query.getColumnIndex("times"))));
                arrayList.add(r.a(query.getInt(query.getColumnIndex("r_uid"))));
                arrayList.add(r.a(r.b(query.getLong(query.getColumnIndex("a_date")))));
                arrayList.add(r.a(r.b(query.getLong(query.getColumnIndex("c_date")))));
                try {
                    strArr = packageManager.getPackagesForUid(i2);
                } catch (Exception e) {
                    strArr = null;
                }
                if (strArr == null || strArr.length <= 0) {
                    str2 = s.a(i2);
                    i = 0;
                    str = null;
                    bArr = null;
                } else {
                    try {
                        packageInfo = packageManager.getPackageInfo(strArr[0], 0);
                    } catch (PackageManager.NameNotFoundException e2) {
                        packageInfo = null;
                    }
                    String str3 = strArr[0];
                    if (packageInfo == null) {
                        i = 0;
                        str2 = str3;
                        bArr = null;
                        str = null;
                    } else {
                        String str4 = packageInfo.versionName;
                        File file = new File(packageInfo.applicationInfo.sourceDir);
                        if (file.exists()) {
                            i = (int) file.length();
                        } else {
                            i = 0;
                        }
                        try {
                            drawable = packageManager.getApplicationIcon(strArr[0]);
                        } catch (PackageManager.NameNotFoundException e3) {
                            e3.printStackTrace();
                            drawable = null;
                        }
                        if (drawable != null) {
                            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();
                            try {
                                byteArrayOutputStream.close();
                            } catch (IOException e4) {
                                e4.printStackTrace();
                            }
                            bitmap.recycle();
                            String str5 = str3;
                            str = str4;
                            bArr = byteArray;
                            str2 = str5;
                        } else {
                            str2 = str3;
                            str = str4;
                            bArr = null;
                        }
                    }
                }
                arrayList.add(r.a(i));
                arrayList.add(r.a(str));
                arrayList.add(r.a(str2));
                arrayList.add(bArr);
            }
            query.close();
            cVar.a(0);
            cVar.a(arrayList);
        }
    }

    public void e(Context context, c cVar) {
        Cursor query = context.getContentResolver().query(a.f279a, null, "type <> 2", null, null);
        if (query == null && b != null) {
            b(context, cVar);
        } else if (query == null) {
            cVar.a(7);
        } else {
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(query.getCount()));
            for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
                arrayList.add(r.a(query.getInt(query.getColumnIndex("_id"))));
                arrayList.add(r.a(query.getInt(query.getColumnIndex(SocialConstants.PARAM_TYPE))));
                arrayList.add(r.a(query.getString(query.getColumnIndex("pkg"))));
                arrayList.add(r.a(query.getString(query.getColumnIndex("command"))));
                arrayList.add(r.a(query.getInt(query.getColumnIndex("times"))));
                arrayList.add(r.a(query.getInt(query.getColumnIndex("r_uid"))));
                arrayList.add(r.a(r.b(query.getLong(query.getColumnIndex("a_date")))));
                arrayList.add(r.a(r.b(query.getLong(query.getColumnIndex("c_date")))));
            }
            query.close();
            cVar.a(0);
            cVar.a(arrayList);
        }
    }

    public void f(Context context, c cVar) {
        int i;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        Uri withAppendedPath = Uri.withAppendedPath(a.f279a, Constants.STR_EMPTY + h);
        ContentValues contentValues = new ContentValues();
        contentValues.put(SocialConstants.PARAM_TYPE, Integer.valueOf(h2));
        try {
            i = context.getContentResolver().update(withAppendedPath, contentValues, null, null);
        } catch (Exception e) {
            i = 0;
        }
        if (i > 0) {
            cVar.a(0);
        } else if (b == null) {
            cVar.a(1);
        } else {
            context.getPackageManager().getNameForUid(h);
        }
    }

    public void g(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (b != null) {
            context.getPackageManager().getNameForUid(h);
        }
        try {
            context.getContentResolver().delete(Uri.withAppendedPath(a.f279a, Constants.STR_EMPTY + h), null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void h(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        if (j.c >= 12) {
            arrayList.add(r.a(2));
            cVar.a(0);
            cVar.a(arrayList);
            return;
        }
        arrayList.add(r.a(1));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void i(Context context, c cVar) {
        int i = 0;
        d.b(context);
        if (d.f266a != null) {
            if (new File(d.f266a).exists()) {
                if (!h.a()) {
                    i = 8;
                }
                d.f266a = null;
            } else {
                i = 4;
            }
        }
        cVar.a(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00df, code lost:
        if (r4.versionCode == 6) goto L_0x0064;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00fd, code lost:
        if (r3.versionCode == 10) goto L_0x0064;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void j(android.content.Context r12, com.qq.g.c r13) {
        /*
            r11 = this;
            r4 = 0
            r10 = 10
            r1 = 1
            r2 = 0
            int r0 = com.qq.util.j.c
            if (r0 > r10) goto L_0x00a6
            java.lang.String r0 = android.os.Build.VERSION.RELEASE
            java.lang.String r3 = "2.3.6"
            int r0 = r0.compareTo(r3)
            if (r0 >= 0) goto L_0x00a6
            r0 = r1
        L_0x0014:
            java.lang.String r5 = com.qq.c.d.a(r12)
            if (r5 != 0) goto L_0x00af
            com.qq.b.a.a(r12)
            boolean r3 = com.qq.b.a.c
            if (r3 == 0) goto L_0x0029
            boolean r3 = com.tencent.assistant.utils.bc.a()
            if (r3 == 0) goto L_0x0029
            int r0 = r0 + 32
        L_0x0029:
            if (r5 == 0) goto L_0x0075
            com.qq.ndk.Native r3 = new com.qq.ndk.Native
            r3.<init>()
            long r6 = r3.getFileSize(r5)
            java.lang.String r3 = "com.qq.connect"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "su length:"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.StringBuilder r8 = r8.append(r6)
            java.lang.String r8 = r8.toString()
            android.util.Log.d(r3, r8)
            r8 = 9780(0x2634, double:4.832E-320)
            int r3 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r3 == 0) goto L_0x0064
            r8 = 9244(0x241c, double:4.567E-320)
            int r3 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r3 == 0) goto L_0x0064
            r8 = 9122(0x23a2, double:4.507E-320)
            int r3 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r3 == 0) goto L_0x0064
            r8 = 9784(0x2638, double:4.834E-320)
            int r3 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r3 != 0) goto L_0x00b3
        L_0x0064:
            if (r1 == 0) goto L_0x0075
            java.io.File r1 = new java.io.File
            java.lang.String r3 = "/system/app/superuser.apk"
            r1.<init>(r3)
            boolean r1 = r1.exists()
            if (r1 == 0) goto L_0x0107
        L_0x0073:
            int r0 = r0 + 4
        L_0x0075:
            java.lang.String r1 = "com.qq.connect"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "rootstate :"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            android.util.Log.d(r1, r3)
            com.qq.provider.v.f353a = r0
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            byte[] r0 = com.qq.AppService.r.a(r0)
            r1.add(r0)
            r13.a(r1)
            r13.a(r2)
            return
        L_0x00a6:
            int r0 = com.qq.util.j.c
            r3 = 11
            if (r0 != r3) goto L_0x0118
            r0 = r1
            goto L_0x0014
        L_0x00af:
            int r0 = r0 + 2
            goto L_0x0029
        L_0x00b3:
            java.lang.String[] r3 = com.qq.c.d.b
            r3 = r3[r2]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x00c9
            r8 = 6952(0x1b28, double:3.4347E-320)
            int r3 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r3 == 0) goto L_0x0064
            r8 = 6908(0x1afc, double:3.413E-320)
            int r3 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r3 == 0) goto L_0x0064
        L_0x00c9:
            r8 = 14688(0x3960, double:7.257E-320)
            int r3 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r3 != 0) goto L_0x00e8
            android.content.pm.PackageManager r3 = r12.getPackageManager()     // Catch:{ NameNotFoundException -> 0x00e3 }
            java.lang.String r6 = "com.kingroot.kinguser"
            r7 = 0
            android.content.pm.PackageInfo r4 = r3.getPackageInfo(r6, r7)     // Catch:{ NameNotFoundException -> 0x00e3 }
        L_0x00da:
            if (r4 == 0) goto L_0x00e1
            int r3 = r4.versionCode
            r4 = 6
            if (r3 == r4) goto L_0x0064
        L_0x00e1:
            r1 = r2
            goto L_0x0064
        L_0x00e3:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x00da
        L_0x00e8:
            r8 = 16480(0x4060, double:8.142E-320)
            int r3 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r3 != 0) goto L_0x00e1
            android.content.pm.PackageManager r3 = r12.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0101 }
            java.lang.String r6 = "com.kingroot.kinguser"
            r7 = 0
            android.content.pm.PackageInfo r3 = r3.getPackageInfo(r6, r7)     // Catch:{ NameNotFoundException -> 0x0101 }
        L_0x00f9:
            if (r3 == 0) goto L_0x00e1
            int r3 = r3.versionCode
            if (r3 != r10) goto L_0x00e1
            goto L_0x0064
        L_0x0101:
            r3 = move-exception
            r3.printStackTrace()
            r3 = r4
            goto L_0x00f9
        L_0x0107:
            java.io.File r1 = new java.io.File
            java.lang.String r3 = "/system/app/Superuser.apk"
            r1.<init>(r3)
            boolean r1 = r1.exists()
            if (r1 != 0) goto L_0x0073
            int r0 = r0 + 8
            goto L_0x0073
        L_0x0118:
            r0 = r2
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.v.j(android.content.Context, com.qq.g.c):void");
    }

    public void k(Context context, c cVar) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo("com.qq.superuser", 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            packageInfo = null;
        }
        if (packageInfo == null) {
            cVar.a(7);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(packageInfo.versionCode));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void l(Context context, c cVar) {
        int i;
        String str;
        boolean z = true;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        d.b(context);
        Log.d("com.qq.connect", "removSystemApp :" + j);
        if (d.f266a == null) {
            i = 0;
        } else if (new File(d.f266a).exists()) {
            if (j.startsWith("/data/cust/app")) {
                if (j.startsWith("/data/cust/app")) {
                    com.qq.b.a aVar = new com.qq.b.a();
                    aVar.d("mount -o rw,remount /dev/block/mtdblock8 /cust", 150000);
                    aVar.a();
                }
                File file = new File(j);
                com.qq.b.a aVar2 = new com.qq.b.a();
                aVar2.a(105, file.getParent(), 777, 3333);
                aVar2.a();
                if (file.exists()) {
                    z = file.delete();
                }
            } else {
                int lastIndexOf = j.lastIndexOf(".apk");
                if (lastIndexOf > 0) {
                    str = j.substring(0, lastIndexOf);
                } else {
                    str = j;
                }
                z = h.a(str);
            }
            i = z ? 0 : 8;
        } else {
            i = 4;
        }
        cVar.a(i);
    }
}
