package com.qq.AppService;

import android.app.Notification;
import android.app.NotificationManager;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import com.qq.g.c;
import com.qq.provider.h;
import com.qq.provider.x;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class b extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppService f233a;

    b(AppService appService) {
        this.f233a = appService;
    }

    public void handleMessage(Message message) {
        AstApp i;
        super.handleMessage(message);
        if (message != null && AppService.o != null) {
            if (message.what == 0) {
                XLog.d("com.qq.connect", "switch to none");
                AppService.a((Notification) null);
                this.f233a.p();
                try {
                    ac.a(AppService.o, ac.a(AppService.o));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (IPCService.f203a != null && IPCService.f203a.b != null) {
                    try {
                        IPCService.f203a.b.b();
                    } catch (RemoteException e2) {
                        e2.printStackTrace();
                    }
                }
            } else if (message.what == 1) {
                AppService.o.f = true;
                XLog.d("com.qq.connect", "switch to usb");
                try {
                    ac.a(AppService.o, ac.e(AppService.o));
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                if (AppService.z == null) {
                    synchronized (AppService.o) {
                        try {
                            AppService.o.wait(1000);
                        } catch (InterruptedException e4) {
                            e4.printStackTrace();
                        }
                    }
                }
                AppService.a(AppService.z);
                this.f233a.p();
            } else if (message.what == 2) {
                AppService.o.f = true;
                if (message.arg1 > 0) {
                }
                XLog.d("com.qq.connect", "switch to wifi");
                try {
                    ac.a(AppService.o, ac.e(AppService.o));
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
                if (AppService.z == null) {
                    synchronized (AppService.o) {
                        try {
                            AppService.o.wait(1000);
                        } catch (InterruptedException e6) {
                            e6.printStackTrace();
                        }
                    }
                }
                Notification W = AppService.z;
                if (AppService.u()) {
                    AppService.F();
                    AppService.H();
                }
                AppService.a(W);
                this.f233a.p();
            } else if (message.what == 3) {
                AppService.o.f = true;
                if (message.arg1 > 0) {
                }
                XLog.d("com.qq.connect", "switch to qq");
                try {
                    ac.a(AppService.o, ac.e(AppService.o));
                } catch (Exception e7) {
                    e7.printStackTrace();
                }
                if (AppService.z == null) {
                    synchronized (AppService.o) {
                        try {
                            AppService.o.wait(1000);
                        } catch (InterruptedException e8) {
                            e8.printStackTrace();
                        }
                    }
                }
                Notification W2 = AppService.z;
                if (AppService.u()) {
                    AppService.F();
                    AppService.H();
                }
                AppService.a(W2);
                this.f233a.p();
            } else if (message.what == 4) {
                if (!AppService.q() && !AppService.r() && !AppService.s() && !AppService.t() && !AppService.u() && AppService.o != null) {
                    if (AppService.A == null) {
                        try {
                            ac.a(AppService.o, ac.f(AppService.o));
                        } catch (Exception e9) {
                            e9.printStackTrace();
                        }
                        if (AppService.A == null) {
                            synchronized (AppService.o) {
                                try {
                                    AppService.o.wait(1000);
                                } catch (InterruptedException e10) {
                                    e10.printStackTrace();
                                }
                            }
                        }
                    }
                    Notification X = AppService.A;
                    if (X != null) {
                        try {
                            ((NotificationManager) AstApp.i().getSystemService("notification")).notify(12345, X);
                            s.a();
                        } catch (Exception e11) {
                            e11.printStackTrace();
                        }
                    }
                }
            } else if (message.what == 5 && !h.b() && (i = AstApp.i()) != null) {
                x.a(i).d(i, new c());
            }
        }
    }
}
