package com.qq.AppService;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
public class ParcelObject implements Parcelable {
    public static final Parcelable.Creator<ParcelObject> CREATOR = new am();

    /* renamed from: a  reason: collision with root package name */
    private int f206a;
    private String b;
    private Bundle c;

    /* synthetic */ ParcelObject(Parcel parcel, am amVar) {
        this(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public void a(Parcel parcel) {
        this.f206a = parcel.readInt();
        this.b = parcel.readString();
        this.c = parcel.readBundle();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f206a);
        parcel.writeString(this.b);
        parcel.writeBundle(this.c);
    }

    private ParcelObject(Parcel parcel) {
        this.f206a = -1;
        this.b = null;
        this.c = null;
        this.f206a = parcel.readInt();
        this.b = parcel.readString();
        this.c = parcel.readBundle();
    }
}
