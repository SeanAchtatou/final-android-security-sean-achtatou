package com.qq.AppService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import com.qq.provider.h;
import com.qq.provider.x;
import com.qq.util.j;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
public class SmsManagerActivity extends Activity {
    public static volatile boolean b = false;
    public static volatile boolean c = false;

    /* renamed from: a  reason: collision with root package name */
    public boolean f209a = false;
    private volatile boolean d = false;
    private boolean e = false;
    private String f = null;

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.d = false;
        if (this.e) {
            b = false;
            String b2 = x.b(this);
            if (b2 == null || !b2.equals(getPackageName())) {
                Log.d("com.qq.connect", "request sms admin  fail!");
                h.b(1);
            } else {
                Log.d("com.qq.connect", "request sms admin  success!");
                h.b(0);
            }
            x a2 = x.a(this);
            if (a2 != null) {
                synchronized (a2) {
                    a2.notifyAll();
                }
            }
        } else if (this.f209a && this.f != null) {
            c = false;
            String b3 = x.b(this);
            if (b3 == null || !b3.equals(this.f)) {
                Log.d("com.qq.connect", "release sms admin  fail!" + b3 + ":" + this.f);
                h.b(4);
            } else {
                Log.d("com.qq.connect", "release sms admin  success!" + b3 + ":" + this.f);
                h.b(2);
            }
            x a3 = x.a(this);
            if (a3 != null) {
                synchronized (a3) {
                    a3.notifyAll();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        Bundle extras = intent.getExtras();
        if (extras == null) {
            finish();
            return;
        }
        String string = extras.getString(SocialConstants.PARAM_ACT);
        this.f = extras.getString("pkg");
        if (string == null) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.d = true;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (j.c < 19) {
            finish();
            return;
        }
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        Bundle extras = intent.getExtras();
        if (extras == null) {
            finish();
            return;
        }
        String string = extras.getString(SocialConstants.PARAM_ACT);
        this.f = extras.getString("pkg");
        if (string == null) {
            finish();
        } else if (string.equals(SocialConstants.TYPE_REQUEST)) {
            this.e = true;
            b = true;
            Intent intent2 = new Intent("android.provider.Telephony.ACTION_CHANGE_DEFAULT");
            intent2.putExtra("package", getPackageName());
            intent2.addFlags(1073741824);
            intent2.addFlags(8388608);
            try {
                startActivity(intent2);
            } catch (Throwable th) {
                this.e = false;
                b = false;
            }
        } else if (!string.equals("release") || this.f == null) {
            finish();
        } else {
            try {
                getPackageManager().getPackageInfo(this.f, 0);
                this.f209a = true;
                this.e = false;
                Intent intent3 = new Intent("android.provider.Telephony.ACTION_CHANGE_DEFAULT");
                intent3.addFlags(1073741824);
                intent3.addFlags(8388608);
                intent3.putExtra("package", this.f);
                c = true;
                try {
                    startActivity(intent3);
                } catch (Throwable th2) {
                    this.f209a = false;
                    c = false;
                }
            } catch (Throwable th3) {
                finish();
                h.b(3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (b && this.e) {
            b = false;
        }
        if (c && this.f209a) {
            c = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        this.d = false;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.d) {
            finish();
        }
    }
}
