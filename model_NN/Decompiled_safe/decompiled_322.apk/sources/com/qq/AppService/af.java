package com.qq.AppService;

import android.content.Context;
import android.util.Log;
import com.qq.g.c;
import com.qq.provider.a;
import com.qq.provider.aa;
import com.qq.provider.ab;
import com.qq.provider.ag;
import com.qq.provider.ai;
import com.qq.provider.aj;
import com.qq.provider.ak;
import com.qq.provider.b;
import com.qq.provider.d;
import com.qq.provider.e;
import com.qq.provider.f;
import com.qq.provider.g;
import com.qq.provider.h;
import com.qq.provider.j;
import com.qq.provider.l;
import com.qq.provider.n;
import com.qq.provider.o;
import com.qq.provider.p;
import com.qq.provider.q;
import com.qq.provider.v;
import com.qq.provider.w;
import com.qq.provider.x;
import com.qq.provider.y;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;

/* compiled from: ProGuard */
public final class af extends Thread {

    /* renamed from: a  reason: collision with root package name */
    public volatile boolean f215a = false;
    public volatile boolean b = false;
    private c c = null;
    private Context d = null;
    private Socket e = null;
    private volatile boolean f = false;
    private byte[] g = new byte[4];
    private InputStream h = null;
    private OutputStream i = null;
    private boolean j = false;
    private boolean k = false;

    public af(Context context, Socket socket) {
        this.d = context;
        this.e = socket;
        this.f = true;
    }

    public void a() {
        this.j = true;
    }

    public void a(Socket socket) {
        if (this.f215a) {
            this.b = false;
            this.f215a = false;
            this.e = socket;
            synchronized (this) {
                notify();
            }
        }
    }

    public void b() {
        this.f = false;
        this.f215a = false;
        if (this.e != null) {
            try {
                this.e.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            this.e = null;
        }
        synchronized (this) {
            notify();
        }
    }

    public void run() {
        while (this.f) {
            if (this.f215a) {
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
            if (!this.f215a) {
                if (this.e == null) {
                    this.f215a = true;
                    this.e = null;
                } else {
                    c();
                    this.f215a = true;
                    if (this.j) {
                        return;
                    }
                }
            }
        }
    }

    public void c() {
        byte[] a2;
        System.currentTimeMillis();
        if (this.e != null) {
            try {
                this.h = this.e.getInputStream();
                if (this.h == null) {
                    this.e.close();
                    this.e = null;
                    if (!this.b) {
                        this.b = true;
                    }
                    if (this.e != null) {
                        try {
                            this.e.close();
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        this.e = null;
                    }
                    if (this.h != null) {
                        try {
                            this.h.close();
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                        this.h = null;
                    }
                    if (this.i != null) {
                        try {
                            this.i.close();
                        } catch (Exception e4) {
                            e4.printStackTrace();
                        }
                        this.i = null;
                    }
                    if (this.c != null) {
                        this.c.b = null;
                    } else {
                        return;
                    }
                } else {
                    this.i = this.e.getOutputStream();
                    if (this.i == null) {
                        if (this.e != null) {
                            this.e.close();
                            this.e = null;
                        }
                        if (this.h != null) {
                            try {
                                this.h.close();
                            } catch (Exception e5) {
                                e5.printStackTrace();
                            }
                            this.h = null;
                        }
                        if (!this.b) {
                            this.b = true;
                        }
                        if (this.e != null) {
                            try {
                                this.e.close();
                            } catch (Exception e6) {
                                e6.printStackTrace();
                            }
                            this.e = null;
                        }
                        if (this.h != null) {
                            try {
                                this.h.close();
                            } catch (Exception e7) {
                                e7.printStackTrace();
                            }
                            this.h = null;
                        }
                        if (this.i != null) {
                            try {
                                this.i.close();
                            } catch (Exception e8) {
                                e8.printStackTrace();
                            }
                            this.i = null;
                        }
                        if (this.c != null) {
                            this.c.b = null;
                        } else {
                            return;
                        }
                    } else {
                        int read = this.h.read(this.g);
                        if (read < this.g.length) {
                            if (!this.b) {
                                this.b = true;
                            }
                            if (this.e != null) {
                                try {
                                    this.e.close();
                                } catch (Exception e9) {
                                    e9.printStackTrace();
                                }
                                this.e = null;
                            }
                            if (this.h != null) {
                                try {
                                    this.h.close();
                                } catch (Exception e10) {
                                    e10.printStackTrace();
                                }
                                this.h = null;
                            }
                            if (this.i != null) {
                                try {
                                    this.i.close();
                                } catch (Exception e11) {
                                    e11.printStackTrace();
                                }
                                this.i = null;
                            }
                            if (this.c != null) {
                                this.c.b = null;
                            } else {
                                return;
                            }
                        } else {
                            int a3 = r.a(this.g);
                            if (a3 <= 0 || a3 >= 4194304) {
                                if (!this.b) {
                                    this.b = true;
                                }
                                if (this.e != null) {
                                    try {
                                        this.e.close();
                                    } catch (Exception e12) {
                                        e12.printStackTrace();
                                    }
                                    this.e = null;
                                }
                                if (this.h != null) {
                                    try {
                                        this.h.close();
                                    } catch (Exception e13) {
                                        e13.printStackTrace();
                                    }
                                    this.h = null;
                                }
                                if (this.i != null) {
                                    try {
                                        this.i.close();
                                    } catch (Exception e14) {
                                        e14.printStackTrace();
                                    }
                                    this.i = null;
                                }
                                if (this.c != null) {
                                    this.c.b = null;
                                } else {
                                    return;
                                }
                            } else {
                                this.c = new c();
                                this.c.b = new byte[a3];
                                int i2 = read;
                                while (i2 >= 0 && read < a3) {
                                    i2 = this.h.read(this.c.b, read, a3 - read);
                                    if (i2 > 0) {
                                        read += i2;
                                    }
                                }
                                this.i.write(r.hs);
                                this.i.flush();
                                this.b = true;
                                if (read < a3 && this.c != null) {
                                    this.c.b = null;
                                }
                                if (this.c == null || this.c.b == null) {
                                    Log.d("com.qq.connect", "no data");
                                }
                                if (!(this.c == null || this.c.b == null)) {
                                    this.k = false;
                                    try {
                                        a2 = a(this.d, this.e);
                                    } catch (Throwable th) {
                                        th.printStackTrace();
                                        if (this.c == null || this.c.d() == null) {
                                            Log.d("com.qq.connect", th.toString() + " cmd is null");
                                        } else {
                                            Log.d("com.qq.connect", th.toString() + " cmd:" + new String(this.c.d()));
                                        }
                                        a2 = r.a(8, (ArrayList<byte[]>) null);
                                    }
                                    if (this.k) {
                                        h.a();
                                    }
                                    if (a2 != null) {
                                        int length = a2.length;
                                        int i3 = 0;
                                        while (length > i3) {
                                            if (length - i3 >= 8192) {
                                                this.i.write(a2, i3, 8192);
                                                this.i.flush();
                                                i3 += 8192;
                                            } else {
                                                this.i.write(a2, i3, length - i3);
                                                this.i.flush();
                                                i3 = length;
                                            }
                                        }
                                        this.i.write(r.hs);
                                        this.i.flush();
                                    }
                                }
                                if (this.e != null) {
                                    try {
                                        this.e.close();
                                    } catch (Exception e15) {
                                        e15.printStackTrace();
                                    }
                                    this.e = null;
                                }
                                if (this.h != null) {
                                    try {
                                        this.h.close();
                                    } catch (Exception e16) {
                                        e16.printStackTrace();
                                    }
                                    this.h = null;
                                }
                                if (this.i != null) {
                                    try {
                                        this.i.close();
                                    } catch (Exception e17) {
                                        e17.printStackTrace();
                                    }
                                    this.i = null;
                                }
                                System.currentTimeMillis();
                                if (!this.b) {
                                    this.b = true;
                                }
                                if (this.e != null) {
                                    try {
                                        this.e.close();
                                    } catch (Exception e18) {
                                        e18.printStackTrace();
                                    }
                                    this.e = null;
                                }
                                if (this.h != null) {
                                    try {
                                        this.h.close();
                                    } catch (Exception e19) {
                                        e19.printStackTrace();
                                    }
                                    this.h = null;
                                }
                                if (this.i != null) {
                                    try {
                                        this.i.close();
                                    } catch (Exception e20) {
                                        e20.printStackTrace();
                                    }
                                    this.i = null;
                                }
                                if (this.c != null) {
                                    this.c.b = null;
                                } else {
                                    return;
                                }
                            }
                        }
                    }
                }
            } catch (Exception e21) {
                e21.printStackTrace();
            } catch (Throwable th2) {
                try {
                    th2.printStackTrace();
                    if (!this.b) {
                        this.b = true;
                    }
                    if (this.e != null) {
                        try {
                            this.e.close();
                        } catch (Exception e22) {
                            e22.printStackTrace();
                        }
                        this.e = null;
                    }
                    if (this.h != null) {
                        try {
                            this.h.close();
                        } catch (Exception e23) {
                            e23.printStackTrace();
                        }
                        this.h = null;
                    }
                    if (this.i != null) {
                        try {
                            this.i.close();
                        } catch (Exception e24) {
                            e24.printStackTrace();
                        }
                        this.i = null;
                    }
                    if (this.c != null) {
                        this.c.b = null;
                    } else {
                        return;
                    }
                } catch (Throwable th3) {
                    if (!this.b) {
                        this.b = true;
                    }
                    if (this.e != null) {
                        try {
                            this.e.close();
                        } catch (Exception e25) {
                            e25.printStackTrace();
                        }
                        this.e = null;
                    }
                    if (this.h != null) {
                        try {
                            this.h.close();
                        } catch (Exception e26) {
                            e26.printStackTrace();
                        }
                        this.h = null;
                    }
                    if (this.i != null) {
                        try {
                            this.i.close();
                        } catch (Exception e27) {
                            e27.printStackTrace();
                        }
                        this.i = null;
                    }
                    if (this.c != null) {
                        this.c.b = null;
                        this.c = null;
                    }
                    throw th3;
                }
            }
            this.c = null;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r6v0, types: [com.qq.AppService.af] */
    private byte[] a(Context context, Socket socket) {
        byte[] bArr;
        this.c.c();
        byte[] d2 = this.c.d();
        if (AppService.d) {
            String inetAddress = socket.getInetAddress().toString();
            if (!inetAddress.contains("127.0.0.1") && (AppService.g == null || !AppService.g.contains(inetAddress))) {
                if (!r.b(d2, r.bf)) {
                    return r.a(4, (ArrayList<byte[]>) null);
                }
                ab.a().h(context, this.c);
                return r.a(this.c.a(), this.c.b());
            }
        }
        if (this.c.f292a) {
            if (!AppService.e) {
                if (r.b(d2, r.ba) || r.b(d2, r.bG) || r.b(d2, r.gV)) {
                    this.k = false;
                } else if (r.b(d2, r.bx) || r.b(d2, r.bk)) {
                    this.k = false;
                } else if (r.b(d2, r.da)) {
                    this.k = false;
                } else if (r.b(d2, r.aY) || r.b(d2, r.bB)) {
                    this.k = false;
                } else if (r.b(d2, r.bA) || r.b(d2, r.bd)) {
                    this.k = false;
                } else if (r.b(d2, r.dA)) {
                    this.k = false;
                } else if (r.b(d2, r.aZ) || r.b(d2, r.bb)) {
                    this.k = false;
                } else if (r.b(d2, r.bg) || r.b(d2, r.bD)) {
                    this.k = false;
                } else if (r.b(d2, r.bE)) {
                    this.k = false;
                } else if (r.b(d2, r.gc) || r.b(d2, r.bt)) {
                    this.k = false;
                } else if (r.b(d2, r.aH)) {
                    this.k = false;
                } else {
                    this.k = true;
                    Context b2 = AppService.b();
                    if (b2 == null) {
                        b2 = context;
                    }
                    h.a(b2, (String) null);
                }
            }
            if (r.b(d2, r.f250a)) {
                j.a().F(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.b)) {
                j.a().f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.d)) {
                j.a().E(context, this.c);
                bArr = r.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (r.b(d2, r.k)) {
                j.a().s(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.e)) {
                j.a().x(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.h)) {
                j.a().A(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.g)) {
                j.a().C(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.i)) {
                j.a().m(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.f)) {
                j.a().y(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.c)) {
                j.a().D(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.j)) {
                j.a().n(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.A)) {
                j.a().i(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.B)) {
                j.a().j(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.C)) {
                j.a().k(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.D)) {
                j.a().l(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.E)) {
                j.a().o(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.F)) {
                j.a().q(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.G)) {
                j.a().p(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.H)) {
                j.a().r(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.I)) {
                j.a().u(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.J)) {
                j.a().v(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.K)) {
                j.a().w(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.z)) {
                j.a().h(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.l)) {
                j.a().S(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.m)) {
                j.a().R(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.n)) {
                j.a().T(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.w)) {
                j.a().L(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.v)) {
                j.a().N(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.u)) {
                j.a().M(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.s)) {
                j.a().I(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.r)) {
                j.a().H(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.x)) {
                j.a().Q(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.q)) {
                j.a().y(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.t)) {
                j.a().K(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.y)) {
                j.a().G(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.o)) {
                j.a().O(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.p)) {
                j.a().P(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.L)) {
                w.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.M)) {
                w.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.N)) {
                w.a().d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.O)) {
                w.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.P)) {
                w.a().e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.U)) {
                j.a().t(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.V)) {
                j.a().d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.W)) {
                j.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.X)) {
                j.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.Y)) {
                j.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.Z)) {
                j.a().e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aa)) {
                j.a().J(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ab)) {
                j.a().ac(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ac)) {
                j.a().ab(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ad)) {
                j.a().U(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ae)) {
                j.a().W(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.af)) {
                j.a().g(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ag)) {
                j.a().z(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aj)) {
                f.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ah)) {
                f.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.an)) {
                f.a().e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.al)) {
                f.a().f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ak)) {
                f.a().g(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ai)) {
                f.a().h(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.am)) {
                f.a().i(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ao)) {
                f.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ap)) {
                f.a().d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aq)) {
                g.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.Q)) {
                j.a().Z(context, this.c);
                bArr = null;
            } else if (r.b(d2, r.S)) {
                j.a().X(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.R)) {
                j.a().V(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.T)) {
                j.a().Y(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bb)) {
                ab.a().f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aU)) {
                ab.a().i(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aY)) {
                ab.a().k(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bc)) {
                ab.a().m(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aX)) {
                ab.a().g(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aZ)) {
                ab.a().p(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ba)) {
                ab.a().C(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bd)) {
                ab.a().d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aW)) {
                ab.a().q(context, this.c);
                bArr = r.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (r.b(d2, r.aV)) {
                ab.a().r(context, this.c);
                bArr = r.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (r.b(d2, r.bf)) {
                ab.a().h(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bg)) {
                ab.a().a(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bh)) {
                ab.a().j(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bj)) {
                ab.s(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bk)) {
                ab.a().t(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bl)) {
                ab.a().u(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bm)) {
                ab.a().v(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bn)) {
                ab.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bo)) {
                ab.a().a(context, socket);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.be)) {
                if (!ab.a().A(context, this.c)) {
                    bArr = r.a(this.c.a(), this.c.b());
                } else {
                    bArr = this.c.b;
                }
            } else if (r.b(d2, r.bv)) {
                if (!ab.a().z(context, this.c)) {
                    bArr = r.a(this.c.a(), this.c.b());
                } else {
                    bArr = this.c.b;
                }
            } else if (r.b(d2, r.bt)) {
                ab.a().y(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.br)) {
                ab.a().w(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bs)) {
                ab.a().x(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bx)) {
                ab.a().a(context, this.c, socket);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.by)) {
                ab.a().e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bz)) {
                new v().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bA)) {
                ab.a().n(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bB)) {
                ab.a().o(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bC)) {
                ab.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bD)) {
                ab.a().B(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bE)) {
                ab.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bF)) {
                ab.a().H(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bG)) {
                ab.a().l(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bH)) {
                ab.a().F(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bI)) {
                ab.a().J(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bJ)) {
                ab.a().I(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bp)) {
                ag.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bq)) {
                ag.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cp)) {
                ab.a().G(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cq)) {
                ab.a().D(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cr)) {
                ab.a().E(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bi)) {
                aa.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cs)) {
                p.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ct)) {
                p.a().l(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cu)) {
                p.a().n(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cv)) {
                p.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cw)) {
                p.a().f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cx)) {
                p.a().e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cy)) {
                p.a().k(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cz)) {
                p.a().j(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cA)) {
                p.a().i(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cB)) {
                p.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cC)) {
                p.a().g(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cG)) {
                p.a().d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cH)) {
                p.a().h(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cD)) {
                a.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cE)) {
                a.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cF)) {
                a.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cI)) {
                p.a().o(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cJ)) {
                p.a().p(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cK)) {
                p.a().w(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cL)) {
                p.a().x(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cM)) {
                p.a().y(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cN)) {
                p.a().r(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cO)) {
                p.a().q(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cP)) {
                p.a().A(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cQ)) {
                p.a().z(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cR)) {
                p.a().v(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cS)) {
                p.a().u(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cT)) {
                p.a().s(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cU)) {
                p.a().t(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cV)) {
                p.a().B(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cW)) {
                p.a().C(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cX)) {
                p.a().m(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cY)) {
                p.a().D(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cZ)) {
                p.a().E(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aD)) {
                y.a(context).a(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.at)) {
                y.a(context).b(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ay)) {
                y.a(context).c(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.az)) {
                y.a(context).d(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.av)) {
                y.a(context).g(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aw)) {
                y.a(context).h(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ax)) {
                x.a(context).a(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aC)) {
                x.a(context).b(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ar)) {
                y.a(context).i(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.as)) {
                y.a(context).j(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.au)) {
                y.a(context).k(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aB)) {
                y.a(context).l(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aA)) {
                y.a(context).m(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aE)) {
                y.a(context).e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aF)) {
                y.a(context).d(context, this.c);
                bArr = r.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (r.b(d2, r.aG)) {
                y.a(context).e(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aH)) {
                y.a(context).a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aI)) {
                y.a(context).f(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aJ)) {
                y.a(context).f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aK)) {
                y.a(context).b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aL)) {
                y.a(context).c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aM)) {
                bArr = null;
            } else if (r.b(d2, r.aN)) {
                x.a(context).c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aO)) {
                x.a(context).a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aP)) {
                x.a(context).b(context, this.c);
                bArr = r.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (r.b(d2, r.aT)) {
                x.a(context).g(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aQ)) {
                x.a(context).e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aR)) {
                x.a(context).f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.aS)) {
                x.a(context).d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.da)) {
                h.c().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.de)) {
                h.c().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dh)) {
                n.a().a(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dA)) {
                n.a().b(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.di)) {
                n.a().c(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dj)) {
                n.a().d(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dk)) {
                n.a().f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dl)) {
                n.a().g(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dm)) {
                n.a().e(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dn)) {
                n.a().f(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.f0do)) {
                n.a().o(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dp)) {
                n.a().o(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dq)) {
                n.a().g(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dr)) {
                n.a().g(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ds)) {
                n.a().h(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dt)) {
                n.a().h(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.du)) {
                n.a().i(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dv)) {
                n.a().i(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dx)) {
                n.a().j(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dw)) {
                n.a().l(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dB)) {
                n.a().m(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dC)) {
                n.a().n(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dD)) {
                n.a().k(this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dE)) {
                n.a().e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dL)) {
                n.a().h(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dM)) {
                n.a().i(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dN)) {
                n.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dO)) {
                n.a().d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dP)) {
                n.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dQ)) {
                n.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dR)) {
                n.a().l(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dS)) {
                n.a().k(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dT)) {
                n.a().j(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dU)) {
                n.a().m(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dV)) {
                n.a().o(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dW)) {
                n.a().s(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dX)) {
                n.a().p(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dY)) {
                n.a().q(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.dZ)) {
                n.a().r(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ea)) {
                n.a().n(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ec)) {
                o.a(context).t(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ed)) {
                o.a(context).u(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ee)) {
                o.a(context).i(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ef)) {
                o.a(context).k(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eg)) {
                o.a(context).o(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eh)) {
                o.a(context).p(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ei)) {
                o.a(context).r(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ej)) {
                o.a(context).h(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ek)) {
                o.a(context).s(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.el)) {
                o.a(context).q(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ep)) {
                o.a(context).w(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eq)) {
                o.a(context).x(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.em)) {
                o.a(context).f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eo)) {
                o.a(context).v(context, this.c);
                bArr = r.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (r.b(d2, r.er)) {
                o.a(context).y(context, this.c);
                bArr = r.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (r.b(d2, r.es)) {
                o.a(context).z(context, this.c);
                bArr = r.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (r.b(d2, r.et)) {
                o.a(context).A(context, this.c);
                bArr = r.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (r.b(d2, r.eu)) {
                o.a(context).d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ev)) {
                o.a(context).e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.en)) {
                o.a(context).j(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ew)) {
                o.a(context).g(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ex)) {
                o.a(context).a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ey)) {
                o.a(context).c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ez)) {
                o.a(context).b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eA)) {
                o.a(context).B(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eJ)) {
                o.a(context).H(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eB)) {
                o.a(context).m(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eC)) {
                o.a(context).C(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eD)) {
                o.a(context).D(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eE)) {
                o.a(context).E(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eF)) {
                o.a(context).F(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eG)) {
                o.a(context).G(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eH)) {
                o.a(context).l(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eI)) {
                o.a(context).n(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eK)) {
                com.qq.provider.c.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eL)) {
                com.qq.provider.c.a().g(context, this.c);
                bArr = r.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (r.b(d2, r.eM)) {
                com.qq.provider.c.a().h(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eN)) {
                com.qq.provider.c.a().j(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eO)) {
                com.qq.provider.c.a().k(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eP)) {
                com.qq.provider.c.a().n(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fm)) {
                com.qq.provider.c.a().o(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eQ)) {
                com.qq.provider.c.a().p(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eR)) {
                com.qq.provider.c.a().s(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eW)) {
                com.qq.provider.c.a().t(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eX)) {
                com.qq.provider.c.a().v(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eS)) {
                com.qq.provider.c.a().f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eY)) {
                com.qq.provider.c.a().r(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eT)) {
                com.qq.provider.c.a().l(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eU)) {
                com.qq.provider.c.a().m(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eV)) {
                com.qq.provider.c.a().u(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.eZ)) {
                com.qq.provider.c.a().d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fa)) {
                com.qq.provider.c.a().e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fl)) {
                com.qq.provider.c.a().q(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fn)) {
                com.qq.provider.c.a().i(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fo)) {
                com.qq.provider.c.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fr)) {
                com.qq.provider.c.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fb)) {
                com.qq.provider.c.a().w(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fc)) {
                com.qq.provider.c.a().x(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fd)) {
                com.qq.provider.c.a().y(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fe)) {
                com.qq.provider.c.a().z(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ff)) {
                com.qq.provider.c.a().A(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fg)) {
                com.qq.provider.c.a().B(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fh)) {
                com.qq.provider.c.a().C(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fi)) {
                com.qq.provider.c.a().D(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fj)) {
                com.qq.provider.c.a().E(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fk)) {
                com.qq.provider.c.a().F(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fp)) {
                com.qq.provider.c.a().G(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fq)) {
                com.qq.provider.c.a().H(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fs)) {
                l.a().d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ft)) {
                l.a().e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fu)) {
                l.a().f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fv)) {
                l.a().g(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fw)) {
                l.a().j(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fx)) {
                l.a().h(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fy)) {
                l.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fz)) {
                l.a().i(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fA)) {
                l.a().l(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fB)) {
                l.a().k(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fC)) {
                l.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fD)) {
                l.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fE)) {
                ai.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fF)) {
                ai.a().h(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fG)) {
                ai.a().d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fH)) {
                ai.a().e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fI)) {
                ai.a().f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fJ)) {
                aj.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fK)) {
                ai.a().j(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fL)) {
                aj.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fM)) {
                ai.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fN)) {
                ai.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fO)) {
                aj.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fP)) {
                ai.a().k(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fQ)) {
                ai.a().l(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fR)) {
                ai.a().g(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fS)) {
                ai.a().i(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fT)) {
                ai.a().m(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bK)) {
                d.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bL)) {
                d.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bM)) {
                d.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bN)) {
                d.a().d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bO)) {
                d.a().e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bP)) {
                d.a().f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bQ)) {
                a.a(context).b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bR)) {
                a.a(context).c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bS)) {
                a.a(context).d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bT)) {
                a.a(context).e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bU)) {
                a.a(context).f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bV)) {
                a.a(context).g(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bW)) {
                a.a(context).h(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bX)) {
                a.a(context).i(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bY)) {
                a.a(context).a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.bZ)) {
                e.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ca)) {
                e.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cb)) {
                e.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cc)) {
                e.a().d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cd)) {
                e.a().e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ce)) {
                e.a().f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cf)) {
                e.a().g(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cg)) {
                e.a().h(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ch)) {
                e.a().i(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ci)) {
                e.a().j(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cj)) {
                e.a().k(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ck)) {
                e.a().l(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cl)) {
                e.a().n(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cm)) {
                e.a().m(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.cn)) {
                com.qq.provider.af.a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.co)) {
                com.qq.provider.af.b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fU)) {
                b.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fW)) {
                b.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fV)) {
                b.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fX)) {
                b.a().d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fY)) {
                b.a().e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gc)) {
                new v().j(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gd)) {
                new v().i(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.fZ)) {
                new v().e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gi)) {
                new v().d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ga)) {
                new v().f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gb)) {
                new v().g(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gg)) {
                new v().l(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gh)) {
                new v().k(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gj)) {
                new v().h(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gN)) {
                q.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gO)) {
                q.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gP)) {
                q.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gQ)) {
                q.a().e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gX)) {
                q.a().d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gR)) {
                q.a().f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gS)) {
                q.a().i(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gT)) {
                q.a().j(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gU)) {
                q.a().i(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gV)) {
                q.a().l(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gW)) {
                q.a().m(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gY)) {
                q.a().h(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.gZ)) {
                q.a().k(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ha)) {
                q.a().g(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.hb)) {
                ak.a().a(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.hc)) {
                ak.a().b(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.hd)) {
                ak.a().c(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.he)) {
                ak.a().d(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.hf)) {
                ak.a().e(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.hg)) {
                ak.a().f(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.hh)) {
                ak.a().g(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.hi)) {
                ak.a().h(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.hj)) {
                ak.a().j(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.hk)) {
                ak.a().i(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.hm)) {
                q.a().n(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.hn)) {
                q.a().o(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.ho)) {
                q.a().p(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else if (r.b(d2, r.hp)) {
                q.a().q(context, this.c);
                bArr = r.a(this.c.a(), this.c.b());
            } else {
                this.c.a(3);
                byte[] a2 = r.a(this.c.a(), (ArrayList<byte[]>) null);
                Log.d("com.qq.connect", "cmd:" + new String(d2) + " is not support");
                bArr = a2;
            }
            if (bArr != null) {
            }
            return bArr;
        } else if (r.b(d2, r.bf)) {
            ab.a().h(context, this.c);
            return r.a(this.c.a(), this.c.b());
        } else if (r.b(d2, r.dk)) {
            n.a().f(context, this.c);
            return r.a(this.c.a(), this.c.b());
        } else if (r.b(d2, r.dl)) {
            n.a().g(context, this.c);
            return r.a(this.c.a(), this.c.b());
        } else if (r.b(d2, r.dA)) {
            n.a().b(this.c);
            return r.a(this.c.a(), this.c.b());
        } else {
            Log.d("com.qq.connect", "request to exec:" + new String(d2) + " permission denied.");
            return r.a(4, (ArrayList<byte[]>) null);
        }
    }
}
