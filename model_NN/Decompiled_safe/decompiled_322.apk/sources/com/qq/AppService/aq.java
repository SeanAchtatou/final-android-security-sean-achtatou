package com.qq.AppService;

import android.content.Context;
import android.os.PowerManager;
import com.tencent.wcs.c.b;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Vector;

/* compiled from: ProGuard */
public final class aq extends Thread {

    /* renamed from: a  reason: collision with root package name */
    public volatile boolean f224a = false;
    public volatile boolean b = false;
    private ServerSocket c = null;
    /* access modifiers changed from: private */
    public boolean d = true;
    /* access modifiers changed from: private */
    public Vector<byte[]> e = new Vector<>(30);
    /* access modifiers changed from: private */
    public volatile boolean f = false;
    private as g = null;
    /* access modifiers changed from: private */
    public ar h = null;

    public void a(ar arVar) {
        this.h = arVar;
    }

    public void a(int i) {
        this.f224a = true;
        try {
            if (this.c == null) {
                this.c = new ServerSocket();
                this.c.setReuseAddress(true);
                this.c.setPerformancePreferences(1, 1, 0);
                this.c.bind(new InetSocketAddress(i), 3);
            }
        } catch (Throwable th) {
            try {
                th.printStackTrace();
                this.c = null;
                this.f224a = false;
                if (!this.f224a && this.c != null) {
                    this.c.close();
                    this.c = null;
                } else {
                    return;
                }
            } catch (Throwable th2) {
                th2.printStackTrace();
            }
        }
        if (!this.f224a) {
            if (this.c != null) {
                this.c.close();
            }
            this.c = null;
        }
        if (!this.f224a && this.c != null) {
            try {
                this.c.close();
            } catch (Throwable th3) {
                th3.printStackTrace();
            }
            this.c = null;
            this.f224a = false;
        }
    }

    public void a() {
        if (this.g != null) {
            this.g.a(false);
        }
        this.e.clear();
    }

    public void b() {
        this.f224a = false;
        if (this.c != null) {
            try {
                this.c.close();
                this.c = null;
            } catch (Throwable th) {
                th.printStackTrace();
                this.c = null;
            }
        }
        if (this.g != null) {
            this.g.a(false);
        }
        this.e.clear();
    }

    public void a(boolean z) {
        this.f = z;
    }

    public void a(byte[] bArr) {
        if (this.e.size() > 100) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        } else if (this.e.size() >= 200) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e3) {
                e3.printStackTrace();
            }
        }
        this.e.add(bArr);
        synchronized (this) {
            notifyAll();
        }
    }

    public boolean c() {
        return this.b;
    }

    public boolean a(DataOutputStream dataOutputStream) {
        if (dataOutputStream == null) {
            return false;
        }
        int i = 0;
        while (this.e.size() > 0 && i < 20) {
            byte[] bArr = this.e.get(0);
            if (bArr != null) {
                dataOutputStream.write(bArr);
                dataOutputStream.flush();
            }
            this.e.remove(0);
            i++;
        }
        return true;
    }

    public int a(InputStream inputStream) {
        if (inputStream == null) {
            return 0;
        }
        try {
            return inputStream.read(new byte[4]);
        } catch (EOFException e2) {
            return -1;
        } catch (IOException e3) {
            return 0;
        }
    }

    public void run() {
        Socket socket;
        while (this.f224a) {
            try {
                if (this.c != null) {
                    try {
                        socket = this.c.accept();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        socket = null;
                    }
                    if (socket != null) {
                        String inetAddress = socket.getInetAddress().toString();
                        b.a("SingleConnectionServer accept request addr " + inetAddress);
                        if (AppService.g == null) {
                            if (!inetAddress.contains("127.0.0.1")) {
                                socket.close();
                            }
                        } else if (!AppService.g.contains(inetAddress) && !inetAddress.contains("127.0.0.1")) {
                            socket.close();
                        }
                        if (socket == null) {
                            continue;
                        } else {
                            if (this.g != null && this.g.d) {
                                try {
                                    Thread.sleep(50);
                                } catch (InterruptedException e3) {
                                    e3.printStackTrace();
                                }
                                this.g.a(true);
                            }
                            try {
                                socket.setSoTimeout(150);
                            } catch (SocketException e4) {
                                e4.printStackTrace();
                            }
                            this.g = new as(this, socket);
                            this.g.start();
                            if (socket != null && !AppService.e) {
                                AppService a2 = AppService.a();
                                if (a2 != null) {
                                    synchronized (a2) {
                                        ax.a(AppService.b());
                                    }
                                }
                                Context b2 = AppService.b();
                                if (b2 != null) {
                                    ((PowerManager) b2.getSystemService("power")).newWakeLock(1, "scs-lock" + hashCode()).acquire(30000);
                                }
                            }
                        }
                    } else {
                        continue;
                    }
                } else {
                    return;
                }
            } catch (Exception e5) {
                e5.printStackTrace();
            }
        }
    }
}
