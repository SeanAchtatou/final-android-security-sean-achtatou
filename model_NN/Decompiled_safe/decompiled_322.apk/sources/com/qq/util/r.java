package com.qq.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;

/* compiled from: ProGuard */
public class r {
    private r() {
    }

    private static boolean a(SQLiteException sQLiteException) {
        return sQLiteException.getMessage().equals("unable to open database file");
    }

    public static void a(Context context, SQLiteException sQLiteException) {
        if (!a(sQLiteException)) {
            throw sQLiteException;
        }
    }

    public static Cursor a(Context context, ContentResolver contentResolver, Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        try {
            return contentResolver.query(uri, strArr, str, strArr2, str2);
        } catch (SQLiteException e) {
            e.printStackTrace();
            a(context, e);
            return null;
        }
    }
}
