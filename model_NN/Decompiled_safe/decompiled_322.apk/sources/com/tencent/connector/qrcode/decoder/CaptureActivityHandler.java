package com.tencent.connector.qrcode.decoder;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import com.google.zxing.a;
import com.google.zxing.h;
import com.tencent.android.qqdownloader.R;
import com.tencent.connector.CaptureActivity;
import com.tencent.connector.qrcode.a.d;
import com.tencent.connector.qrcode.a.g;
import java.util.Vector;

/* compiled from: ProGuard */
public final class CaptureActivityHandler extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2431a = CaptureActivityHandler.class.getSimpleName();
    private final CaptureActivity b;
    private final c c;
    private State d = State.SUCCESS;

    /* compiled from: ProGuard */
    enum State {
        PREVIEW,
        SUCCESS,
        DONE
    }

    public CaptureActivityHandler(CaptureActivity captureActivity, Vector<a> vector, String str) {
        this.b = captureActivity;
        this.c = new c(captureActivity, vector, str, new g(captureActivity.a()));
        this.c.start();
        d.a().c();
        b();
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case R.id.auto_focus /*2131165241*/:
                if (this.d == State.PREVIEW) {
                    d.a().b(this, R.id.auto_focus);
                    return;
                }
                return;
            case R.id.decode /*2131165242*/:
            case R.id.quit /*2131165245*/:
            default:
                return;
            case R.id.decode_failed /*2131165243*/:
                this.d = State.PREVIEW;
                d.a().a(this.c.a(), R.id.decode);
                return;
            case R.id.decode_succeeded /*2131165244*/:
                this.d = State.SUCCESS;
                this.b.a((h) message.obj, (Bitmap) null);
                return;
            case R.id.restart_preview /*2131165246*/:
                b();
                return;
        }
    }

    public void a() {
        this.d = State.DONE;
        d.a().d();
        Message.obtain(this.c.a(), (int) R.id.quit).sendToTarget();
        try {
            this.c.join();
        } catch (InterruptedException e) {
        }
        removeMessages(R.id.decode_succeeded);
        removeMessages(R.id.decode_failed);
    }

    private void b() {
        if (this.d == State.SUCCESS) {
            this.d = State.PREVIEW;
            d.a().a(this.c.a(), R.id.decode);
            d.a().b(this, R.id.auto_focus);
            this.b.c();
        }
    }
}
