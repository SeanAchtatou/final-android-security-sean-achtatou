package com.tencent.connector.qrcode.decoder;

import android.app.Activity;
import android.content.DialogInterface;

/* compiled from: ProGuard */
public final class d implements DialogInterface.OnCancelListener, DialogInterface.OnClickListener, Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final Activity f2436a;

    public d(Activity activity) {
        this.f2436a = activity;
    }

    public void onCancel(DialogInterface dialogInterface) {
        run();
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        run();
    }

    public void run() {
        this.f2436a.finish();
    }
}
