package com.tencent.connector.qrcode.decoder;

import android.app.Activity;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
public final class e {

    /* renamed from: a  reason: collision with root package name */
    private final ScheduledExecutorService f2437a = Executors.newSingleThreadScheduledExecutor(new f());
    private final Activity b;
    private ScheduledFuture<?> c = null;

    public e(Activity activity) {
        this.b = activity;
        a();
    }

    public void a() {
        c();
        this.c = this.f2437a.schedule(new d(this.b), 300, TimeUnit.SECONDS);
    }

    private void c() {
        if (this.c != null) {
            this.c.cancel(true);
            this.c = null;
        }
    }

    public void b() {
        c();
        this.f2437a.shutdown();
    }
}
