package com.tencent.connector.qrcode.decoder;

import android.os.Handler;
import android.os.Looper;
import com.google.zxing.a;
import com.google.zxing.d;
import com.google.zxing.k;
import com.tencent.connector.CaptureActivity;
import java.util.Hashtable;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;

/* compiled from: ProGuard */
final class c extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private final CaptureActivity f2435a;
    private final Hashtable<d, Object> b = new Hashtable<>(3);
    private Handler c;
    private final CountDownLatch d = new CountDownLatch(1);

    c(CaptureActivity captureActivity, Vector<a> vector, String str, k kVar) {
        this.f2435a = captureActivity;
        if (vector == null || vector.isEmpty()) {
            vector = new Vector<>();
            vector.addAll(a.f2433a);
        }
        this.b.put(d.c, vector);
        if (str != null) {
            this.b.put(d.e, str);
        }
        this.b.put(d.h, kVar);
    }

    /* access modifiers changed from: package-private */
    public Handler a() {
        try {
            this.d.await();
        } catch (InterruptedException e) {
        }
        return this.c;
    }

    public void run() {
        Looper.prepare();
        this.c = new b(this.f2435a, this.b);
        this.d.countDown();
        Looper.loop();
    }
}
