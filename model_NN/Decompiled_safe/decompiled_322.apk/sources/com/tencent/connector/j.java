package com.tencent.connector;

import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
public class j {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f2421a;
    /* access modifiers changed from: private */
    public int b;
    private Runnable c;

    /* synthetic */ j(k kVar) {
        this();
    }

    public static j a() {
        return l.f2423a;
    }

    private j() {
        this.f2421a = false;
        this.c = new k(this);
    }

    public void a(int i) {
        if (!this.f2421a) {
            this.b = i;
            TemporaryThreadManager.get().start(this.c);
        }
    }
}
