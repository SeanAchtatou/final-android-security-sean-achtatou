package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.adapter.a;
import com.tencent.assistant.adapter.a.d;
import com.tencent.assistant.adapter.a.e;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.as;
import com.tencent.assistant.module.callback.l;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.component.fps.FPSNormalItemNoReasonView;
import com.tencent.assistantv2.component.fps.FPSRankNormalItem;
import com.tencent.pangu.model.b;

/* compiled from: ProGuard */
public class s extends a {
    static l c;
    d d = new t(this);
    private IViewInvalidater e;

    public s(Context context, aa aaVar, IViewInvalidater iViewInvalidater) {
        super(context, aaVar);
        this.e = iViewInvalidater;
    }

    public Pair<View, Object> a() {
        FPSNormalItemNoReasonView fPSNormalItemNoReasonView = new FPSNormalItemNoReasonView(this.f1900a, this.e);
        return Pair.create(fPSNormalItemNoReasonView, fPSNormalItemNoReasonView.f1990a);
    }

    public void a(View view, Object obj, int i, b bVar) {
        SimpleAppModel simpleAppModel;
        v vVar = (v) obj;
        if (bVar != null) {
            simpleAppModel = bVar.c();
        } else {
            simpleAppModel = null;
        }
        view.setOnClickListener(new w(this.f1900a, i, simpleAppModel, this.b));
        a(vVar, simpleAppModel, i, this.b.a());
        e.a(this.f1900a, this.b.j(), this.d, obj, simpleAppModel, i);
    }

    private void a(v vVar, SimpleAppModel simpleAppModel, int i, com.tencent.assistant.model.d dVar) {
        AppConst.AppState appState;
        if (simpleAppModel != null && vVar != null) {
            if (!TextUtils.isEmpty(simpleAppModel.ae)) {
                vVar.f1920a.setVisibility(0);
                vVar.f1920a.setText(simpleAppModel.ae);
            } else {
                vVar.f1920a.setVisibility(8);
            }
            if (dVar != null) {
                appState = dVar.c;
            } else {
                appState = null;
            }
            vVar.e.a(simpleAppModel, dVar);
            vVar.c.setText(simpleAppModel.d);
            if (this.b.g()) {
                if (1 == (((int) (simpleAppModel.B >> 2)) & 3)) {
                    Drawable drawable = this.f1900a.getResources().getDrawable(R.drawable.appdownload_icon_original);
                    drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                    vVar.c.setCompoundDrawablePadding(by.b(6.0f));
                    vVar.c.setCompoundDrawables(null, null, drawable, null);
                } else {
                    vVar.c.setCompoundDrawables(null, null, null, null);
                }
            }
            vVar.b.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            vVar.d.a(simpleAppModel, dVar);
            if (vVar.f != null) {
                if (!this.b.k()) {
                    vVar.f.setVisibility(8);
                } else if (TextUtils.isEmpty(simpleAppModel.X)) {
                    vVar.f.setVisibility(8);
                } else {
                    vVar.f.setText(simpleAppModel.X);
                    vVar.f.setVisibility(0);
                }
            }
            if (vVar.g != null) {
                vVar.g.a(simpleAppModel, this.e);
            }
            if (com.tencent.pangu.component.appdetail.process.s.a(simpleAppModel, appState)) {
                vVar.d.setClickable(false);
            } else {
                vVar.d.setClickable(true);
                vVar.d.a(this.b.e(), new u(this, appState, vVar), dVar, vVar.d, vVar.e);
            }
            if (simpleAppModel.ao == null || simpleAppModel.ao.size() <= 0) {
                vVar.h.setVisibility(8);
                vVar.i.setVisibility(0);
            } else {
                vVar.h.setVisibility(0);
                vVar.h.setImageViewUrls(simpleAppModel.ao);
                vVar.h.requestLayout();
                vVar.i.setVisibility(8);
            }
            if (this.b.d()) {
                this.b.a(false);
                a(simpleAppModel, vVar.d);
            }
            if (vVar.m == null || simpleAppModel.aw == null) {
                vVar.m.setVisibility(8);
            } else {
                vVar.i.setVisibility(0);
                vVar.m.setVisibility(0);
                vVar.m.a(simpleAppModel);
            }
            if (this.b.f() == SmartListAdapter.SmartListType.SearchPage) {
                a.b(this.f1900a, simpleAppModel, vVar.c, true);
            } else {
                a.a(this.f1900a, simpleAppModel, vVar.c, false);
            }
            if (this.b.l() && vVar.l != null) {
                vVar.l.setVisibility(0);
                FPSRankNormalItem.a(vVar.l, this.b.m());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(SimpleAppModel simpleAppModel, View view) {
        boolean j = simpleAppModel.j();
        if (c == null) {
            c = new com.tencent.assistant.adapter.a.a(this.f1900a, this.b.j(), this.d);
            as.a().register(c);
        } else if (c instanceof com.tencent.assistant.adapter.a.a) {
            ((com.tencent.assistant.adapter.a.a) c).a(this.f1900a, this.b.j(), this.d);
        }
        if (e.a(this.f1900a, simpleAppModel, this.b.j(), this.d, (View) view.getParent(), !j)) {
            Object tag = ((View) view.getParent()).getTag();
            if (tag instanceof v) {
                simpleAppModel.ao = null;
                ((v) tag).h.setVisibility(8);
            }
        }
    }
}
