package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.a.c;
import com.tencent.assistant.component.HorizonMultiImageView;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.ListItemRelateNewsView;
import com.tencent.assistantv2.component.ListRecommendReasonView;

/* compiled from: ProGuard */
public class x extends s {
    public IViewInvalidater e;

    public x(Context context, aa aaVar, IViewInvalidater iViewInvalidater) {
        super(context, aaVar, iViewInvalidater);
        this.e = iViewInvalidater;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public Pair<View, Object> a() {
        View inflate = LayoutInflater.from(this.f1900a).inflate((int) R.layout.app_universal_item_v5, (ViewGroup) null, false);
        y yVar = new y();
        yVar.f1920a = (TextView) inflate.findViewById(R.id.download_rate_desc);
        yVar.b = (TXAppIconView) inflate.findViewById(R.id.app_icon_img);
        yVar.b.setInvalidater(this.e);
        yVar.c = (TextView) inflate.findViewById(R.id.title);
        yVar.d = (DownloadButton) inflate.findViewById(R.id.state_app_btn);
        yVar.e = (ListItemInfoView) inflate.findViewById(R.id.download_info);
        if (this.b.b() != null) {
            yVar.e.a(this.b.b());
        }
        yVar.h = (HorizonMultiImageView) inflate.findViewById(R.id.snap_shot_pics);
        yVar.i = inflate.findViewById(R.id.empty_padding_bottom);
        yVar.f = (TextView) inflate.findViewById(R.id.desc);
        yVar.g = (ListRecommendReasonView) inflate.findViewById(R.id.reasonView);
        yVar.m = (ListItemRelateNewsView) inflate.findViewById(R.id.relate_news);
        yVar.j = new c();
        yVar.j.f579a = (ListView) inflate.findViewById(R.id.one_more_list);
        yVar.j.b = (RelativeLayout) inflate.findViewById(R.id.one_more_list_parent);
        yVar.j.f579a.setDivider(null);
        yVar.j.e = (ImageView) inflate.findViewById(R.id.one_more_app_line_top_long);
        yVar.j.j = (RelativeLayout) inflate.findViewById(R.id.app_one_more_loading_parent);
        yVar.j.i = (TextView) inflate.findViewById(R.id.app_one_more_loading);
        yVar.j.f = (ProgressBar) inflate.findViewById(R.id.app_one_more_loading_gif);
        yVar.j.g = (ImageView) inflate.findViewById(R.id.one_more_app_error_img);
        yVar.j.d = (ImageView) inflate.findViewById(R.id.one_more_app_arrow);
        yVar.j.h = (TextView) inflate.findViewById(R.id.app_one_more_desc);
        yVar.l = (TextView) inflate.findViewById(R.id.sort_text);
        return Pair.create(inflate, yVar);
    }
}
