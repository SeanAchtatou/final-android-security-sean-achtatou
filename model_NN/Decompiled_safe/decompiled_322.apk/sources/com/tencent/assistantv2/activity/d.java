package com.tencent.assistantv2.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.SplashActivity;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.socialcontact.login.l;
import com.tencent.pangu.link.b;
import com.tencent.pangu.model.ShareBaseModel;
import com.tencent.pangu.module.timer.RecoverAppListReceiver;

/* compiled from: ProGuard */
class d extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GuideActivity f1870a;

    d(GuideActivity guideActivity) {
        this.f1870a = guideActivity;
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1870a, 200);
        buildSTInfo.scene = STConst.ST_PAGE_GUIDE_RESULT_HUANJI_NO;
        String str = STConst.ST_DEFAULT_SLOT;
        if (view.getTag(R.id.tma_st_slot_tag) instanceof String) {
            String str2 = (String) view.getTag(R.id.tma_st_slot_tag);
            if (this.f1870a.U != 0) {
                str = "03_" + str2;
            } else if (this.f1870a.T == null || !(this.f1870a.T.b == 1 || this.f1870a.T.b == 3)) {
                str = "04_003";
            } else {
                str = "04_" + str2;
            }
        }
        buildSTInfo.slotId = str;
        return buildSTInfo;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.GuideActivity.a(com.tencent.assistantv2.activity.GuideActivity, boolean):boolean
     arg types: [com.tencent.assistantv2.activity.GuideActivity, int]
     candidates:
      com.tencent.assistantv2.activity.GuideActivity.a(com.tencent.assistantv2.activity.GuideActivity, int):int
      com.tencent.assistantv2.activity.GuideActivity.a(com.tencent.assistantv2.activity.GuideActivity, com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse):com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.GuideActivity.a(com.tencent.assistantv2.activity.GuideActivity, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel, boolean):void
     arg types: [com.tencent.assistantv2.activity.GuideActivity, com.tencent.pangu.model.ShareBaseModel, int]
     candidates:
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void
     arg types: [com.tencent.assistantv2.activity.GuideActivity, com.tencent.pangu.model.ShareBaseModel, int]
     candidates:
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void */
    public void onTMAClick(View view) {
        String str;
        switch (view.getId()) {
            case R.id.btn_skip /*2131166150*/:
                if (SplashActivity.f383a) {
                    r.a(this.f1870a, System.currentTimeMillis());
                    m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal()));
                }
                this.f1870a.C();
                return;
            case R.id.btn_login /*2131166152*/:
                boolean unused = this.f1870a.P = true;
                if (this.f1870a.W && SplashActivity.f383a) {
                    r.a(this.f1870a, System.currentTimeMillis());
                    m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal()));
                }
                if (this.f1870a.W) {
                    b.a(this.f1870a, (Bundle) null, new ActionUrl("tmast://webview?url=http://qzs.qq.com/open/mobile/share_cj2015/index.html&mode=0", 1));
                    return;
                } else {
                    this.f1870a.C();
                    return;
                }
            case R.id.layout_btn /*2131166156*/:
                this.f1870a.E().a(false);
                String str2 = l.f().b;
                ShareBaseModel shareBaseModel = new ShareBaseModel();
                shareBaseModel.c = this.f1870a.B();
                shareBaseModel.b = Constants.STR_EMPTY;
                shareBaseModel.f = Constants.STR_EMPTY;
                shareBaseModel.e = Constants.STR_EMPTY;
                shareBaseModel.f3880a = this.f1870a.getString(R.string.guide_share_title, new Object[]{"我"});
                XLog.i(this.f1870a.v, "targetUrl=" + shareBaseModel.d);
                if (this.f1870a.C.isChecked()) {
                    boolean unused2 = this.f1870a.O = !this.f1870a.D.isChecked();
                    this.f1870a.E().a((Activity) this.f1870a, shareBaseModel, false);
                }
                if (this.f1870a.D.isChecked()) {
                    GuideActivity guideActivity = this.f1870a;
                    Object[] objArr = new Object[1];
                    if (TextUtils.isEmpty(str2)) {
                        str = "我";
                    } else {
                        str = str2;
                    }
                    objArr[0] = str;
                    shareBaseModel.f3880a = guideActivity.getString(R.string.guide_share_title, objArr);
                    this.f1870a.E().a((Context) this.f1870a, shareBaseModel, true);
                }
                if (!this.f1870a.C.isChecked() && !this.f1870a.D.isChecked()) {
                    this.f1870a.C();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
