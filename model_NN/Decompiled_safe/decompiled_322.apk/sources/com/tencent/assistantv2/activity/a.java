package com.tencent.assistantv2.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import com.qq.AppService.AstApp;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public abstract class a extends Fragment {
    protected Context P;
    protected LayoutInflater Q;
    private View R;
    private boolean S;

    public abstract void C();

    public abstract void D();

    public abstract int E();

    public abstract int F();

    public abstract void d(boolean z);

    public void d(Bundle bundle) {
        super.d(bundle);
        this.S = true;
    }

    public boolean A() {
        return this.S;
    }

    public a(Activity activity) {
        this.R = null;
        this.S = false;
        this.P = activity;
        this.Q = LayoutInflater.from(this.P);
    }

    public a() {
        this.R = null;
        this.S = false;
        this.P = MainActivity.t();
        if (this.P == null) {
            this.P = AstApp.i();
        }
        this.Q = LayoutInflater.from(this.P);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void b(int i) {
        this.R = this.Q.inflate(i, (ViewGroup) null, false);
    }

    public View h() {
        View h = super.h();
        if (h != null) {
            return h;
        }
        XLog.d("BaseGragment", "get View return null.");
        return this.R;
    }

    public View c(int i) {
        if (this.R != null) {
            return this.R.findViewById(i);
        }
        return null;
    }

    public void a(View view) {
        this.R = view;
    }

    public void B() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.P, 100);
        if (buildSTInfo != null) {
            buildSTInfo.scene = G();
            if (this.P instanceof BaseActivity) {
                buildSTInfo.updateWithExternalPara(((BaseActivity) this.P).q);
            }
            l.a(buildSTInfo);
        }
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ViewParent parent = this.R.getParent();
        if (parent != null && (parent instanceof ViewGroup)) {
            ((ViewGroup) parent).removeView(this.R);
        }
        return this.R;
    }

    public final void j() {
        super.j();
    }

    public int G() {
        return 2000;
    }

    public void c(boolean z) {
        super.c(z);
        if (z) {
            d(true);
        }
    }
}
