package com.tencent.assistantv2.component;

import android.content.Intent;
import android.view.View;
import com.tencent.nucleus.manager.about.HelperFeedbackActivity;

/* compiled from: ProGuard */
class bc implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleViewV5 f1978a;

    bc(SecondNavigationTitleViewV5 secondNavigationTitleViewV5) {
        this.f1978a = secondNavigationTitleViewV5;
    }

    public void onClick(View view) {
        this.f1978a.b.startActivity(new Intent(this.f1978a.b, HelperFeedbackActivity.class));
    }
}
