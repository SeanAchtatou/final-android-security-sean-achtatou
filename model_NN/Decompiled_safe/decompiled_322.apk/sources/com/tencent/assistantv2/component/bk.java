package com.tencent.assistantv2.component;

import android.view.View;

/* compiled from: ProGuard */
class bk implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TopTabWidget f1982a;
    private final int b;
    private final int c;

    /* synthetic */ bk(TopTabWidget topTabWidget, int i, int i2, bi biVar) {
        this(topTabWidget, i, i2);
    }

    private bk(TopTabWidget topTabWidget, int i, int i2) {
        this.f1982a = topTabWidget;
        this.b = i;
        this.c = i2;
    }

    public void onClick(View view) {
        this.f1982a.f1945a.a(this.b, true);
    }
}
