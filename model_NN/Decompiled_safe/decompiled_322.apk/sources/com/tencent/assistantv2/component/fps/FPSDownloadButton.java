package com.tencent.assistantv2.component.fps;

import android.content.Context;
import android.util.AttributeSet;
import com.tencent.assistantv2.component.DownloadButton;

/* compiled from: ProGuard */
public class FPSDownloadButton extends DownloadButton {
    private boolean b = true;

    public FPSDownloadButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public FPSDownloadButton(Context context) {
        super(context);
    }

    public void requestLayout() {
        if (this.b) {
            super.requestLayout();
        }
    }

    public void setBackgroundResource(int i) {
        this.b = false;
        super.setBackgroundResource(i);
        this.b = true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        this.b = false;
        super.a(str);
        this.b = true;
    }
}
