package com.tencent.assistantv2.component;

import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
/* synthetic */ class d {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f1987a = new int[AppConst.AppState.values().length];

    static {
        try {
            f1987a[AppConst.AppState.DOWNLOAD.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f1987a[AppConst.AppState.UPDATE.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f1987a[AppConst.AppState.DOWNLOADING.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f1987a[AppConst.AppState.QUEUING.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f1987a[AppConst.AppState.PAUSED.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f1987a[AppConst.AppState.FAIL.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f1987a[AppConst.AppState.DOWNLOADED.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f1987a[AppConst.AppState.INSTALLED.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f1987a[AppConst.AppState.SDKUNSUPPORT.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f1987a[AppConst.AppState.ILLEGAL.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f1987a[AppConst.AppState.INSTALLING.ordinal()] = 11;
        } catch (NoSuchFieldError e11) {
        }
        try {
            f1987a[AppConst.AppState.UNINSTALLING.ordinal()] = 12;
        } catch (NoSuchFieldError e12) {
        }
    }
}
