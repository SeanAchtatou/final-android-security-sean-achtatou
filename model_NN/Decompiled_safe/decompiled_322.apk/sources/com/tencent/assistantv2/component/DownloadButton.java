package com.tencent.assistantv2.component;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Pair;
import android.widget.Button;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.manager.a;
import com.tencent.assistant.manager.b;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bh;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.cloud.a.f;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.activity.AppDetailActivityV5;
import com.tencent.pangu.component.appdetail.process.s;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.manager.ak;
import com.tencent.pangu.mediadownload.d;

/* compiled from: ProGuard */
public class DownloadButton extends Button implements b {

    /* renamed from: a  reason: collision with root package name */
    public boolean f1926a;
    private Context b;
    private d c;
    private DownloadInfo d;
    private boolean e;
    private boolean f;
    private l g;
    private boolean h;
    private AppConst.AppState i;

    public DownloadButton(Context context) {
        this(context, null);
    }

    public DownloadButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f1926a = false;
        this.h = false;
        this.b = context;
        c();
    }

    private void c() {
        setHeight(this.b.getResources().getDimensionPixelSize(R.dimen.download_button_height));
        setMinWidth(this.b.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        setGravity(17);
        setTextSize(0, (float) this.b.getResources().getDimensionPixelSize(R.dimen.download_button_font_size));
        setSingleLine(true);
    }

    public void a(d dVar) {
        if (dVar != null) {
            this.c = dVar;
            com.tencent.assistant.model.d b2 = b(dVar);
            a(b2.c);
            a.a().a(b2.f942a, this);
        }
    }

    private com.tencent.assistant.model.d b(d dVar) {
        if (dVar == null || !(dVar instanceof SimpleAppModel)) {
            return null;
        }
        return k.e((SimpleAppModel) dVar);
    }

    public void a(d dVar, com.tencent.assistant.model.d dVar2) {
        if (dVar != null) {
            if (dVar2 == null) {
                dVar2 = b(dVar);
            }
            this.c = dVar;
            a(dVar2.c);
            a.a().a(dVar2.f942a, this);
        }
    }

    public void a(STCommonInfo sTCommonInfo) {
        b(sTCommonInfo, null, null, this);
    }

    public void a(STCommonInfo sTCommonInfo, j jVar) {
        b(sTCommonInfo, jVar, null, this);
    }

    public void a(STCommonInfo sTCommonInfo, j jVar, com.tencent.assistant.model.d dVar) {
        b(sTCommonInfo, jVar, dVar, this);
    }

    public void a(STCommonInfo sTCommonInfo, j jVar, com.tencent.assistant.model.d dVar, b... bVarArr) {
        b(sTCommonInfo, jVar, dVar, bVarArr);
    }

    private void b(STCommonInfo sTCommonInfo, j jVar, com.tencent.assistant.model.d dVar, b... bVarArr) {
        if (this.c != null) {
            SimpleAppModel simpleAppModel = (SimpleAppModel) this.c;
            a.a().a(dVar == null ? this.c.q() : dVar.f942a, this);
            setOnClickListener(new f(this, simpleAppModel, sTCommonInfo, jVar, bVarArr));
        }
    }

    public void onAppStateChange(String str, AppConst.AppState appState) {
        String d2;
        if (!TextUtils.isEmpty(str) && (d2 = d()) != null && d2.equals(str)) {
            if (appState == AppConst.AppState.ILLEGAL) {
                com.tencent.assistant.model.d c2 = k.c(DownloadProxy.a().d(str));
                if (c2 == null) {
                    c2 = b(this.c);
                }
                if (c2 != null) {
                    appState = c2.c;
                }
            }
            ah.a().post(new g(this, str, appState));
        }
    }

    private void a(AppConst.AppState appState) {
        b(appState);
    }

    /* access modifiers changed from: private */
    public void b(AppConst.AppState appState) {
        boolean z = true;
        this.i = appState;
        boolean z2 = false;
        l c2 = c(appState);
        String a2 = c2.a(this.b);
        if (!TextUtils.isEmpty(a2) && (this.g == null || !a2.equals(this.g.f2002a))) {
            a(a2);
            z2 = true;
        }
        if (!this.f1926a) {
            setTextColor(this.b.getResources().getColor(c2.d));
            if (this.g == null || c2.e != this.g.e) {
                try {
                    setBackgroundResource(c2.e);
                } catch (Throwable th) {
                }
            }
        } else {
            z = z2;
        }
        if (z) {
            this.g = c2;
        }
    }

    /* access modifiers changed from: private */
    public String d() {
        if (this.c != null) {
            return this.c.q();
        }
        if (this.d != null) {
            return this.d.downloadTicket;
        }
        return null;
    }

    private l c(AppConst.AppState appState) {
        l lVar = new l(null);
        lVar.e = R.drawable.state_bg_common_selector;
        lVar.d = R.color.state_normal;
        lVar.b = R.string.appbutton_unknown;
        if (!this.h || !(this.c instanceof SimpleAppModel) || !ApkResourceManager.getInstance().hasLocalPack(((SimpleAppModel) this.c).c)) {
            if (this.c instanceof SimpleAppModel) {
                SimpleAppModel simpleAppModel = (SimpleAppModel) this.c;
                if (s.b(simpleAppModel, appState)) {
                    lVar.b = R.string.jionbeta;
                    lVar.d = R.color.state_install;
                    lVar.e = R.drawable.state_bg_install_selector;
                    return lVar;
                } else if (s.c(simpleAppModel, appState)) {
                    lVar.b = R.string.jionfirstrelease_with_login;
                    lVar.d = R.color.state_install;
                    lVar.e = R.drawable.state_bg_install_selector;
                    return lVar;
                }
            }
            switch (i.f2001a[appState.ordinal()]) {
                case 1:
                    if (this.c != null && (this.c instanceof SimpleAppModel)) {
                        SimpleAppModel simpleAppModel2 = (SimpleAppModel) this.c;
                        if (!simpleAppModel2.h()) {
                            lVar.b = R.string.appbutton_download;
                            lVar.d = R.color.state_normal;
                            lVar.e = R.drawable.state_bg_common_selector;
                            if (bh.a().a(simpleAppModel2.f938a)) {
                                lVar.a(bh.a().b(simpleAppModel2.f938a));
                                lVar.d = bh.a().c(simpleAppModel2.f938a);
                                lVar.e = bh.a().a(lVar.d);
                            }
                            a(lVar, appState);
                            break;
                        } else {
                            lVar.b = R.string.jionbeta;
                            lVar.d = R.color.state_install;
                            lVar.e = R.drawable.state_bg_install_selector;
                            break;
                        }
                    }
                case 2:
                    lVar.b = R.string.appbutton_update;
                    lVar.d = R.color.state_update;
                    lVar.e = R.drawable.state_bg_update_selector;
                    a(lVar, appState);
                    break;
                case 3:
                    lVar.b = R.string.downloading_display_pause;
                    break;
                case 4:
                    lVar.b = R.string.queuing;
                    break;
                case 5:
                case 6:
                    lVar.b = R.string.appbutton_continuing;
                    lVar.d = R.color.state_install;
                    lVar.e = R.drawable.state_bg_install_selector;
                    break;
                case 7:
                    lVar.b = R.string.appbutton_install;
                    lVar.d = R.color.state_install;
                    lVar.e = R.drawable.state_bg_install_selector;
                    break;
                case 8:
                    String str = Constants.STR_EMPTY;
                    if (this.c != null && (this.c instanceof SimpleAppModel)) {
                        str = ((SimpleAppModel) this.c).c;
                    } else if (this.d != null) {
                        str = this.d.packageName;
                    }
                    if (ak.a().c(str)) {
                        ak.a();
                        if (!TextUtils.isEmpty(ak.h())) {
                            ak.a();
                            if (ak.h().equals(str)) {
                                lVar.b = R.string.qube_apk_using;
                            }
                        }
                        lVar.b = R.string.qube_apk_use;
                    } else {
                        lVar.b = R.string.appbutton_open;
                    }
                    lVar.e = R.drawable.state_bg_open_selector;
                    lVar.d = R.color.state_open;
                    a(lVar, appState);
                    break;
                case 9:
                    lVar.b = R.string.not_support;
                    break;
                case 10:
                    break;
                case 11:
                    lVar.b = R.string.installing;
                    lVar.e = R.drawable.state_bg_disable_bd_pressed;
                    lVar.d = R.color.state_disable;
                    break;
                case 12:
                    lVar.b = R.string.uninstalling;
                    break;
                default:
                    lVar.b = R.string.appbutton_unknown;
                    break;
            }
            return lVar;
        }
        lVar.b = R.string.play_now;
        lVar.e = R.drawable.state_bg_install_selector;
        lVar.d = R.color.state_install;
        return lVar;
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        if (str != null) {
            if (str.length() >= 4) {
                setMinWidth(this.b.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_4));
            } else {
                setMinWidth(this.b.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
            }
            setText(str);
        }
    }

    public void a(boolean z) {
        this.f = z;
    }

    public void b(boolean z) {
        this.e = z;
    }

    public void a(DownloadInfo downloadInfo) {
        this.d = downloadInfo;
        com.tencent.assistant.model.d dVar = new com.tencent.assistant.model.d();
        dVar.f942a = downloadInfo.downloadTicket;
        dVar.b = downloadInfo;
        dVar.c = k.a(this.d, this.f, this.e);
        a(dVar.c);
        a.a().a(dVar.f942a, this);
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel, STCommonInfo sTCommonInfo, j jVar, b... bVarArr) {
        DownloadInfo downloadInfo;
        AppConst.TwoBtnDialogInfo twoBtnDialogInfo;
        if (simpleAppModel != null) {
            AppConst.AppState d2 = k.d(simpleAppModel);
            if (s.a(simpleAppModel, d2)) {
                Intent intent = new Intent(getContext(), AppDetailActivityV5.class);
                intent.putExtra("simpleModeInfo", simpleAppModel);
                getContext().startActivity(intent);
                return;
            }
            DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
            StatInfo a3 = com.tencent.assistantv2.st.page.a.a(sTCommonInfo);
            if (a2 != null && a2.needReCreateInfo(simpleAppModel)) {
                DownloadProxy.a().b(a2.downloadTicket);
                a2 = null;
            }
            if (a2 == null) {
                a2 = DownloadInfo.createDownloadInfo(simpleAppModel, a3, bVarArr);
                if (!(jVar == null || jVar.a() == -1)) {
                    a2.isUpdate = jVar.a();
                    downloadInfo = a2;
                }
                downloadInfo = a2;
            } else {
                a2.updateDownloadInfoStatInfo(a3);
                downloadInfo = a2;
            }
            downloadInfo.applinkInfo = simpleAppModel.aC;
            if (jVar != null) {
                jVar.a(this);
            }
            switch (i.f2001a[d2.ordinal()]) {
                case 1:
                case 2:
                    h hVar = new h(this, jVar, downloadInfo, simpleAppModel);
                    if (downloadInfo.applinkInfo != null) {
                        twoBtnDialogInfo = downloadInfo.applinkInfo.a((SimpleAppModel) this.c, downloadInfo, hVar);
                    } else {
                        twoBtnDialogInfo = null;
                    }
                    if (twoBtnDialogInfo != null) {
                        DialogUtils.show2BtnDialog(twoBtnDialogInfo);
                        break;
                    } else {
                        hVar.a();
                        break;
                    }
                case 3:
                case 4:
                    downloadInfo.applinkInfo = null;
                    com.tencent.pangu.download.a.a().b(downloadInfo.downloadTicket);
                    break;
                case 5:
                    com.tencent.pangu.download.a.a().b(downloadInfo);
                    if (jVar != null) {
                        jVar.a(simpleAppModel, this);
                        break;
                    }
                    break;
                case 6:
                case 10:
                    com.tencent.pangu.download.a.a().a(downloadInfo);
                    if (jVar != null) {
                        jVar.a(simpleAppModel, this);
                        break;
                    }
                    break;
                case 7:
                    com.tencent.pangu.download.a.a().d(downloadInfo);
                    break;
                case 8:
                    com.tencent.pangu.download.a.a().c(downloadInfo);
                    break;
                case 9:
                    Toast.makeText(this.b, (int) R.string.unsupported, 0).show();
                    break;
                case 11:
                    Toast.makeText(this.b, (int) R.string.tips_slicent_install, 0).show();
                    break;
                case 12:
                    Toast.makeText(this.b, (int) R.string.tips_slicent_uninstall, 0).show();
                    break;
            }
            if (jVar != null) {
                jVar.a(this, d2);
            }
        }
    }

    public void a() {
        setTextSize(11.0f);
        this.f1926a = true;
        setBackgroundResource(R.drawable.guanjia_style_btn_backround_selector);
        setTextColor(-1);
    }

    public void b() {
        this.f1926a = false;
        this.g = null;
        a(this.c);
    }

    private void a(l lVar, AppConst.AppState appState) {
        f fVar;
        if (this.c != null && (this.c instanceof SimpleAppModel) && (fVar = ((SimpleAppModel) this.c).aC) != null) {
            Pair<Integer, Integer> a2 = fVar.a(appState);
            String b2 = fVar.b(appState);
            if (a2 != null) {
                lVar.e = ((Integer) a2.first).intValue();
                lVar.d = ((Integer) a2.second).intValue();
            }
            lVar.a(b2);
        }
    }

    public void c(boolean z) {
        this.h = z;
    }
}
