package com.tencent.assistantv2.component;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class z extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ListItemRelateNewsView f2013a;
    private Context b;
    private SimpleAppModel c;

    public z(ListItemRelateNewsView listItemRelateNewsView, Context context, SimpleAppModel simpleAppModel) {
        this.f2013a = listItemRelateNewsView;
        this.b = context;
        this.c = simpleAppModel;
    }

    public void onTMAClick(View view) {
        if (this.c != null && this.c.aw != null) {
            b.a(this.f2013a.getContext(), this.c.aw.b);
        }
    }
}
