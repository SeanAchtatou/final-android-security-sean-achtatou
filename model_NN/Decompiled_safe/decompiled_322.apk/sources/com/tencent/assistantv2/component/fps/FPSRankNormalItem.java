package com.tencent.assistantv2.component.fps;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.a;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.adapter.RankNormalListAdapter;
import com.tencent.pangu.component.appdetail.process.s;

/* compiled from: ProGuard */
public class FPSRankNormalItem extends RelativeLayout {
    private static int m = AstApp.i().getResources().getColor(R.color.rank_sort_txt_top1);
    private static int n = AstApp.i().getResources().getColor(R.color.rank_sort_txt_top2);
    private static int o = AstApp.i().getResources().getColor(R.color.rank_sort_txt_top3);
    private static int p = AstApp.i().getResources().getColor(R.color.rank_sort_txt_normal);

    /* renamed from: a  reason: collision with root package name */
    protected RankNormalListAdapter.ListType f1994a = RankNormalListAdapter.ListType.LISTTYPENORMAL;
    private FPSTextView b;
    private TXAppIconView c;
    private FPSTextView d;
    private FPSDownloadButton e;
    private ListItemInfoView f;
    private ImageView g;
    private View h;
    /* access modifiers changed from: private */
    public Context i;
    /* access modifiers changed from: private */
    public View j;
    private int k = 0;
    private LayoutInflater l;
    private int q = 0;
    private boolean r = false;
    private IViewInvalidater s = null;

    public FPSRankNormalItem(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.l = LayoutInflater.from(context);
        a();
        this.i = context;
    }

    public FPSRankNormalItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.l = LayoutInflater.from(context);
        a();
        this.i = context;
    }

    public FPSRankNormalItem(Context context) {
        super(context);
        this.l = LayoutInflater.from(context);
        a();
        this.i = context;
    }

    private void a() {
        try {
            this.l.inflate((int) R.layout.rank_item_normal, this);
            this.h = this;
            this.b = (FPSTextView) findViewById(R.id.sort_text);
            this.c = (TXAppIconView) findViewById(R.id.app_icon_img);
            this.d = (FPSTextView) findViewById(R.id.app_name_txt);
            this.e = (FPSDownloadButton) findViewById(R.id.state_app_btn);
            this.f = (ListItemInfoView) findViewById(R.id.download_info);
            this.g = (ImageView) findViewById(R.id.last_line);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void a(boolean z, boolean z2, int i2, IViewInvalidater iViewInvalidater, RankNormalListAdapter.ListType listType, int i3) {
        this.r = z;
        this.q = i2;
        this.f1994a = listType;
        this.s = iViewInvalidater;
        this.k = i3;
    }

    public void a(SimpleAppModel simpleAppModel, int i2, STInfoV2 sTInfoV2, d dVar, View view) {
        this.j = view;
        if (this.c != null) {
            this.c.setInvalidater(this.s);
        }
        this.h.setOnClickListener(new b(this, simpleAppModel, sTInfoV2));
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.c.getLayoutParams();
        if (RankNormalListAdapter.ListType.LISTTYPEGAMESORT == this.f1994a) {
            layoutParams.leftMargin = 0;
            try {
                this.b.setVisibility(0);
                a(this.b, simpleAppModel.al);
            } catch (Throwable th) {
                t.a().b();
            }
        } else {
            this.b.setVisibility(8);
        }
        this.c.setLayoutParams(layoutParams);
        if (i2 == 0) {
            if (com.tencent.assistant.utils.t.b > 320) {
                this.h.setBackgroundResource(R.drawable.common_cardbg_top_selector);
            } else {
                this.h.setBackgroundResource(R.drawable.common_cardbg_middle_selector);
            }
            this.g.setVisibility(0);
        } else if (i2 == this.q - 1) {
            this.h.setBackgroundResource(R.drawable.common_cardbg_bottom_selector);
            this.g.setVisibility(8);
        } else {
            this.h.setBackgroundResource(R.drawable.common_cardbg_middle_selector);
            this.g.setVisibility(0);
        }
        a(simpleAppModel, i2, sTInfoV2, dVar);
    }

    private void a(SimpleAppModel simpleAppModel, int i2, STInfoV2 sTInfoV2, d dVar) {
        if (simpleAppModel != null) {
            this.d.setText(simpleAppModel.d);
            if (this.r) {
                if (1 == (((int) (simpleAppModel.B >> 2)) & 3)) {
                    Drawable drawable = this.i.getResources().getDrawable(R.drawable.appdownload_icon_original);
                    drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                    this.d.setCompoundDrawablePadding(by.b(6.0f));
                    this.d.setCompoundDrawables(null, null, drawable, null);
                } else {
                    this.d.setCompoundDrawables(null, null, null, null);
                }
            }
            a.a(this.i, simpleAppModel, this.d, false);
            this.c.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.e.a(simpleAppModel, dVar);
            this.f.a(ListItemInfoView.InfoType.values()[this.k]);
            this.f.a(simpleAppModel, dVar);
            if (s.a(simpleAppModel, dVar.c)) {
                this.e.setClickable(false);
                return;
            }
            this.e.setClickable(true);
            this.e.a(sTInfoV2, new c(this), dVar);
        }
    }

    public static void a(TextView textView, int i2) {
        textView.setText(String.valueOf(i2));
        TextPaint paint = textView.getPaint();
        if (i2 <= 3) {
            paint.setFakeBoldText(true);
            textView.setTextSize(16.0f);
            if (i2 == 1) {
                textView.setTextColor(m);
            } else if (i2 == 2) {
                textView.setTextColor(n);
            } else if (i2 == 3) {
                textView.setTextColor(o);
            }
        } else {
            paint.setFakeBoldText(false);
            textView.setTextSize(14.0f);
            textView.setTextColor(p);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
    }
}
