package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.t;

/* compiled from: ProGuard */
public class TopTabContainer extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    TopTabWidget f1944a;
    private Context b;
    private ImageView c;
    private LinearLayout.LayoutParams d;

    public TopTabContainer(Context context) {
        super(context);
        a(context);
    }

    public TopTabContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        this.b = context;
        LayoutInflater from = LayoutInflater.from(this.b);
        try {
            from.inflate((int) R.layout.top_tab_view, this);
        } catch (OutOfMemoryError e) {
            t.a().b();
            from.inflate((int) R.layout.top_tab_view, this);
        } catch (Throwable th) {
            from.inflate((int) R.layout.top_tab_view, this);
        }
        this.f1944a = (TopTabWidget) findViewById(R.id.my_tabs);
        this.c = (ImageView) findViewById(R.id.cursor);
    }

    public void a(int i, float f) {
        this.c = (ImageView) findViewById(R.id.cursor);
        this.d = (LinearLayout.LayoutParams) this.c.getLayoutParams();
        this.d.width = getResources().getDimensionPixelSize(R.dimen.navigation_curcor_lenth);
        this.d.height = getResources().getDimensionPixelSize(R.dimen.curcor_height);
        this.d.topMargin = -this.d.height;
        this.d.leftMargin = (int) (((float) getResources().getDimensionPixelSize(R.dimen.navigation_curcor_padding_left)) + (((float) i) * f));
        this.c.setImageResource(R.color.second_tab_index);
        this.c.layout(this.d.leftMargin, this.c.getTop(), this.c.getWidth() + this.d.leftMargin, this.c.getBottom());
    }

    public TopTabWidget a() {
        return this.f1944a;
    }

    public void a(int i) {
        this.d = (LinearLayout.LayoutParams) this.c.getLayoutParams();
        this.d.leftMargin = i;
        this.c.layout(this.d.leftMargin, this.c.getTop(), this.c.getWidth() + this.d.leftMargin, this.c.getBottom());
    }
}
