package com.tencent.nucleus.socialcontact.tagpage;

import android.os.Build;
import android.view.Surface;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class ae implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TPVideoDownInfo f3180a;
    final /* synthetic */ TagPageCardAdapter b;

    ae(TagPageCardAdapter tagPageCardAdapter, TPVideoDownInfo tPVideoDownInfo) {
        this.b = tagPageCardAdapter;
        this.f3180a = tPVideoDownInfo;
    }

    public void run() {
        try {
            if (this.f3180a != null && this.b.v != null && TagPageCardAdapter.f3173a == this.f3180a.j) {
                this.b.l.reset();
                this.b.l.setDataSource(this.f3180a.c);
                this.b.l.setOnPreparedListener(this.b);
                this.b.l.setOnCompletionListener(this.b);
                this.b.l.setOnBufferingUpdateListener(this.b);
                this.b.l.setOnErrorListener(this.b);
                this.b.l.setVolume(0.0f, 0.0f);
                if (Build.VERSION.SDK_INT >= 14) {
                    if (this.b.v != null) {
                        this.b.l.setSurface(new Surface(this.b.v));
                    }
                } else if (this.b.w != null) {
                    this.b.l.setDisplay(this.b.w);
                }
                this.b.l.prepare();
            }
        } catch (Exception e) {
            XLog.e("TagPageCradAdapter", "[onVideoDownloadSucceed] ---> e.printStackTrace() = " + e.toString());
            e.printStackTrace();
        }
    }
}
