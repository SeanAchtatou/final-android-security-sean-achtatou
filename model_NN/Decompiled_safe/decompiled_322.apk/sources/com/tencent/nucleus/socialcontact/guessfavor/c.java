package com.tencent.nucleus.socialcontact.guessfavor;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.tencent.assistant.component.listener.OnTMAItemExClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class c extends OnTMAItemExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GuessFavorActivity f3145a;

    c(GuessFavorActivity guessFavorActivity) {
        this.f3145a = guessFavorActivity;
    }

    public void onTMAItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        SimpleAppModel a2 = this.f3145a.w.getItem(i);
        if (a2 != null) {
            Intent intent = new Intent(this.f3145a, AppDetailActivityV5.class);
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f3145a.f());
            intent.putExtra("simpleModeInfo", a2);
            intent.putExtra("statInfo", new StatInfo(a2.b, STConst.ST_PAGE_DOWNLOAD, 0, null, 0));
            this.f3145a.startActivity(intent);
        }
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3145a.n, 200);
        buildSTInfo.slotId = "03_" + bm.a(getPosition() + 1);
        buildSTInfo.status = "_01";
        SimpleAppModel a2 = this.f3145a.w.getItem(getPosition());
        if (a2 != null) {
            buildSTInfo.recommendId = a2.y;
            buildSTInfo.appId = a2.f938a;
        }
        return buildSTInfo;
    }
}
