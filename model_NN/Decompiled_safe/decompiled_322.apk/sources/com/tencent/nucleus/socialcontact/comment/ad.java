package com.tencent.nucleus.socialcontact.comment;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.Random;

/* compiled from: ProGuard */
class ad extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Drawable f3086a;
    final /* synthetic */ Drawable b;
    final /* synthetic */ TextView c;
    final /* synthetic */ CommentReplyListActivity d;

    ad(CommentReplyListActivity commentReplyListActivity, Drawable drawable, Drawable drawable2, TextView textView) {
        this.d = commentReplyListActivity;
        this.f3086a = drawable;
        this.b = drawable2;
        this.c = textView;
    }

    public STInfoV2 getStInfo(View view) {
        return null;
    }

    public void onTMAClick(View view) {
        long j;
        long j2;
        TextView textView = (TextView) view;
        if (this.d.P == 0) {
            ah.a().removeCallbacks(this.d.ag);
            ah.a().postDelayed(this.d.ag, 2000);
        } else if (this.d.P >= 3) {
            Toast.makeText(this.d, this.d.getString(R.string.comment_praise_tofast), 0).show();
            ah.a().removeCallbacks(this.d.ag);
            ah.a().postDelayed(this.d.ag, 2000);
        }
        CommentReplyListActivity.r(this.d);
        if (((Boolean) textView.getTag()).booleanValue()) {
            textView.setTag(false);
            textView.setCompoundDrawables(this.f3086a, null, null, null);
            long parseInt = ((long) Integer.parseInt(textView.getTag(R.id.comment_praise_count) + Constants.STR_EMPTY)) - 1;
            if (parseInt < 0) {
                j2 = 0;
            } else {
                j2 = parseInt;
            }
            textView.setText(bm.a(j2) + Constants.STR_EMPTY);
            textView.setTextColor(Color.parseColor("#6e6e6e"));
            textView.setTag(R.id.comment_praise_count, Long.valueOf(j2));
            this.d.I.a(this.d.K.h, this.d.L, (byte) 2, this.d.M, 0, Constants.STR_EMPTY, this.d.K.d, Constants.STR_EMPTY, Constants.STR_EMPTY, Constants.STR_EMPTY);
            this.d.K.s = 0;
            this.d.K.r = j2;
            l.a(new STInfoV2(STConst.ST_PAGE_COMMENT_REPLY, this.d.Y + "001", 0, STConst.ST_DEFAULT_SLOT, 200));
            return;
        }
        textView.setTag(true);
        textView.setCompoundDrawables(this.b, null, null, null);
        this.c.setText(this.d.Q[new Random().nextInt(this.d.Q.length)]);
        this.c.setVisibility(0);
        Animation loadAnimation = AnimationUtils.loadAnimation(this.d, R.anim.comment_praise_animation);
        loadAnimation.setAnimationListener(new ak(this.d, this.c));
        this.c.startAnimation(loadAnimation);
        long parseInt2 = ((long) Integer.parseInt(textView.getTag(R.id.comment_praise_count) + Constants.STR_EMPTY)) + 1;
        if (parseInt2 < 0) {
            j = 0;
        } else {
            j = parseInt2;
        }
        textView.setText(j + Constants.STR_EMPTY);
        textView.setTextColor(Color.parseColor("#b68a46"));
        textView.setTag(R.id.comment_praise_count, Long.valueOf(j));
        this.d.I.a(this.d.K.h, this.d.L, (byte) 1, this.d.M, 0, Constants.STR_EMPTY, this.d.K.d, Constants.STR_EMPTY, Constants.STR_EMPTY, Constants.STR_EMPTY);
        this.d.K.s = 1;
        this.d.K.r = j;
        l.a(new STInfoV2(STConst.ST_PAGE_COMMENT_REPLY, this.d.X + "001", 0, STConst.ST_DEFAULT_SLOT, 200));
    }
}
