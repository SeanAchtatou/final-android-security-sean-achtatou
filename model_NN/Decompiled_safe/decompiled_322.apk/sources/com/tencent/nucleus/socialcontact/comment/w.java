package com.tencent.nucleus.socialcontact.comment;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.protocol.jce.AnswerAppCommentRequest;
import com.tencent.assistant.protocol.jce.AnswerAppCommentResponse;

/* compiled from: ProGuard */
class w implements CallbackHelper.Caller<y> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3139a;
    final /* synthetic */ AnswerAppCommentResponse b;
    final /* synthetic */ AnswerAppCommentRequest c;
    final /* synthetic */ v d;

    w(v vVar, int i, AnswerAppCommentResponse answerAppCommentResponse, AnswerAppCommentRequest answerAppCommentRequest) {
        this.d = vVar;
        this.f3139a = i;
        this.b = answerAppCommentResponse;
        this.c = answerAppCommentRequest;
    }

    /* renamed from: a */
    public void call(y yVar) {
        yVar.a(this.f3139a, 0, this.b.c, this.c.f, this.c.d, this.c.e);
    }
}
