package com.tencent.nucleus.socialcontact.comment;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class CommentReplyActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public TextView n;
    /* access modifiers changed from: private */
    public EditText u;
    /* access modifiers changed from: private */
    public a v = a.a();
    /* access modifiers changed from: private */
    public CommentDetail w;
    /* access modifiers changed from: private */
    public SimpleAppModel x;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.comment_reply_box);
        u();
        v();
    }

    public int f() {
        return STConst.ST_PAGE_APP_DETAIL_COMMENT;
    }

    private void u() {
        Intent intent = getIntent();
        this.w = (CommentDetail) intent.getSerializableExtra("comment");
        this.x = (SimpleAppModel) intent.getParcelableExtra("simpleAppModel");
    }

    private void v() {
        this.n = (TextView) findViewById(R.id.reply_btn);
        this.u = (EditText) findViewById(R.id.input_box);
        if (!(this.w == null || this.w.k == null || this.w.k.size() <= 0)) {
            this.u.setHint("回复 " + this.w.k.get(this.w.k.size() - 1).d);
            this.u.setHintTextColor(getResources().getColor(R.color.comment_nick_name_user));
        }
        this.n.setOnClickListener(new t(this));
        this.u.addTextChangedListener(new u(this));
    }

    public STInfoV2 t() {
        STInfoV2 sTInfoV2 = null;
        if (this.x != null) {
            sTInfoV2 = STInfoBuilder.buildSTInfo(this, this.x, STConst.ST_DEFAULT_SLOT, 100, null);
            if (this.x.y != null) {
                sTInfoV2.recommendId = this.x.y;
            }
        }
        return sTInfoV2;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
