package com.tencent.nucleus.socialcontact.tagpage;

import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.text.TextUtils;
import android.view.TextureView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.aq;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class aj implements TextureView.SurfaceTextureListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TagPageCardAdapter f3184a;
    private int b = 0;

    public aj(TagPageCardAdapter tagPageCardAdapter, int i) {
        this.f3184a = tagPageCardAdapter;
        this.b = i;
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        SimpleAppModel simpleAppModel;
        TPVideoDownInfo tPVideoDownInfo;
        XLog.i("TagPageCradAdapter", "*** onSurfaceTextureAvailable ***");
        SurfaceTexture unused = this.f3184a.v = surfaceTexture;
        if (this.f3184a.l == null) {
            MediaPlayer unused2 = this.f3184a.l = new MediaPlayer();
        }
        if (this.b >= 0 && (simpleAppModel = (SimpleAppModel) this.f3184a.getItem(this.b)) != null) {
            String unused3 = this.f3184a.y = Constants.STR_EMPTY;
            if (!TextUtils.isEmpty(simpleAppModel.aD)) {
                String unused4 = this.f3184a.y = aq.b(simpleAppModel.aD);
                if (!TextUtils.isEmpty(this.f3184a.y)) {
                    TPVideoDownInfo b2 = l.c().b(this.f3184a.y);
                    if (b2 == null) {
                        TPVideoDownInfo tPVideoDownInfo2 = new TPVideoDownInfo();
                        tPVideoDownInfo2.b = simpleAppModel.aD;
                        tPVideoDownInfo2.f3171a = this.f3184a.y;
                        tPVideoDownInfo = tPVideoDownInfo2;
                    } else {
                        tPVideoDownInfo = b2;
                    }
                    tPVideoDownInfo.j = this.b;
                    XLog.i("TagPageCradAdapter", "[onSurfaceTextureAvailable] ---> startDownload video : " + tPVideoDownInfo);
                    l.c().a(tPVideoDownInfo);
                }
            }
        }
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        XLog.i("TagPageCradAdapter", "*** onSurfaceTextureSizeChanged ***");
        if (this.f3184a.m != null) {
            this.f3184a.m.s.b();
            ah.a().postDelayed(new ak(this), (long) this.f3184a.k());
        }
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        XLog.i("TagPageCradAdapter", "*** onSurfaceTextureDestroyed ***");
        return true;
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }
}
