package com.tencent.nucleus.socialcontact.comment;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.protocol.jce.AnswerAppCommentRequest;

/* compiled from: ProGuard */
class b implements CallbackHelper.Caller<j> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3107a;
    final /* synthetic */ AnswerAppCommentRequest b;
    final /* synthetic */ a c;

    b(a aVar, int i, AnswerAppCommentRequest answerAppCommentRequest) {
        this.c = aVar;
        this.f3107a = i;
        this.b = answerAppCommentRequest;
    }

    /* renamed from: a */
    public void call(j jVar) {
        jVar.a(this.f3107a, 0, this.b.f1129a, this.b.e, this.b.d, System.currentTimeMillis());
    }
}
