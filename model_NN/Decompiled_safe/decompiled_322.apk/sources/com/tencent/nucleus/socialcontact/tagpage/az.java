package com.tencent.nucleus.socialcontact.tagpage;

import android.view.SurfaceHolder;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class az implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SurfaceHolder f3197a;
    final /* synthetic */ ay b;

    az(ay ayVar, SurfaceHolder surfaceHolder) {
        this.b = ayVar;
        this.f3197a = surfaceHolder;
    }

    public void run() {
        try {
            this.b.f3196a.j.reset();
            this.b.f3196a.j.setDataSource(this.b.f3196a.l);
            this.b.f3196a.j.setOnPreparedListener(this.b.f3196a);
            this.b.f3196a.j.setOnCompletionListener(this.b.f3196a);
            this.b.f3196a.j.setOnBufferingUpdateListener(this.b.f3196a);
            this.b.f3196a.j.setOnErrorListener(this.b.f3196a);
            this.b.f3196a.j.setOnSeekCompleteListener(this.b.f3196a);
            this.b.f3196a.j.setVolume(0.0f, 0.0f);
            this.b.f3196a.j.setDisplay(this.f3197a);
            this.b.f3196a.j.prepare();
        } catch (Exception e) {
            XLog.e("VideoFullScreenManager", "[onVideoDownloadSucceed] ---> e.printStackTrace() = " + e.toString());
            e.printStackTrace();
        }
    }
}
