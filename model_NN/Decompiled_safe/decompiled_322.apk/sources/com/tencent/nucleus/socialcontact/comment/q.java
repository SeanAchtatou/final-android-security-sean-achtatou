package com.tencent.nucleus.socialcontact.comment;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class q implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentHeaderTagView f3133a;

    q(CommentHeaderTagView commentHeaderTagView) {
        this.f3133a = commentHeaderTagView;
    }

    public void onClick(View view) {
        int childCount = this.f3133a.e.getChildCount();
        for (int i = 0; i < childCount; i++) {
            RelativeLayout relativeLayout = (RelativeLayout) this.f3133a.e.getChildAt(i);
            View findViewById = relativeLayout.findViewById(R.id.commenttag_selectimg_id);
            if (findViewById != null) {
                relativeLayout.removeView(findViewById);
            }
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(7, view.getId());
        layoutParams.addRule(8, view.getId());
        ImageView imageView = new ImageView(this.f3133a.f3076a);
        imageView.setImageResource(R.drawable.pinglun_icon_on);
        imageView.setId(R.id.commenttag_selectimg_id);
        ((RelativeLayout) view.getParent()).addView(imageView, layoutParams);
        if (this.f3133a.i != null) {
            this.f3133a.i.a(view, null);
        }
    }
}
