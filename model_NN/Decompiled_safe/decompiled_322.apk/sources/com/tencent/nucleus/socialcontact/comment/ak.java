package com.tencent.nucleus.socialcontact.comment;

import android.view.animation.Animation;
import android.widget.TextView;

/* compiled from: ProGuard */
class ak implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentReplyListActivity f3093a;
    private TextView b;

    public ak(CommentReplyListActivity commentReplyListActivity, TextView textView) {
        this.f3093a = commentReplyListActivity;
        this.b = textView;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.b.setVisibility(8);
    }

    public void onAnimationRepeat(Animation animation) {
    }
}
