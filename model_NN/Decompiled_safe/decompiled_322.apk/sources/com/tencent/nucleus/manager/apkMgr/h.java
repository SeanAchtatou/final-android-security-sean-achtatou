package com.tencent.nucleus.manager.apkMgr;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class h extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f2771a;
    final /* synthetic */ k b;
    final /* synthetic */ a c;

    h(a aVar, int i, k kVar) {
        this.c = aVar;
        this.f2771a = i;
        this.b = kVar;
    }

    public void onTMAClick(View view) {
        this.c.a(this.f2771a, !this.b.b.isSelected());
        this.c.notifyDataSetChanged();
        if (this.c.f2764a != null) {
            this.c.f2764a.sendMessage(this.c.f2764a.obtainMessage(110005, new LocalApkInfo()));
        }
    }

    public STInfoV2 getStInfo() {
        return STInfoBuilder.buildSTInfo(this.c.i, 200);
    }
}
