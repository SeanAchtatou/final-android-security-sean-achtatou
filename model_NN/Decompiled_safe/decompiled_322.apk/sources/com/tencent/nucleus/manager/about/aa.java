package com.tencent.nucleus.manager.about;

import com.tencent.assistant.protocol.jce.Feedback;
import java.util.ArrayList;

/* compiled from: ProGuard */
class aa extends m {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HelperFeedbackActivity f2734a;

    aa(HelperFeedbackActivity helperFeedbackActivity) {
        this.f2734a = helperFeedbackActivity;
    }

    public void a(int i, boolean z, boolean z2, ArrayList<Feedback> arrayList) {
        if (i != 0) {
            this.f2734a.x.setVisibility(8);
            this.f2734a.w.setVisibility(8);
            this.f2734a.z.setVisibility(8);
            this.f2734a.w.onRefreshComplete(z2, false);
        } else if (arrayList.size() > 0) {
            if (z) {
                this.f2734a.y.a(arrayList);
            } else {
                this.f2734a.y.b(arrayList);
            }
            this.f2734a.x.setVisibility(0);
            this.f2734a.w.setVisibility(0);
            this.f2734a.z.setVisibility(8);
            this.f2734a.w.onRefreshComplete(z2, true);
        } else {
            this.f2734a.x.setVisibility(8);
            this.f2734a.w.setVisibility(8);
            this.f2734a.z.setVisibility(8);
            this.f2734a.w.onRefreshComplete(z2, false);
        }
    }
}
