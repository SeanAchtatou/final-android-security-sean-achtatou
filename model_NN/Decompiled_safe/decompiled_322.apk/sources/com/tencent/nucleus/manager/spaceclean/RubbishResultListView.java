package com.tencent.nucleus.manager.spaceclean;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: ProGuard */
public class RubbishResultListView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f3008a;
    private LayoutInflater b;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public w f;
    /* access modifiers changed from: private */
    public View g;
    /* access modifiers changed from: private */
    public TXExpandableListView h;
    /* access modifiers changed from: private */
    public RelativeLayout i;
    /* access modifiers changed from: private */
    public TextView j;
    private ListViewScrollListener k;

    public RubbishResultListView(Context context) {
        this(context, null);
    }

    public RubbishResultListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = -1;
        this.e = 0;
        this.i = null;
        this.j = null;
        this.k = new aa(this);
        this.f3008a = context;
        this.b = (LayoutInflater) this.f3008a.getSystemService("layout_inflater");
        a();
    }

    private void a() {
        this.g = this.b.inflate((int) R.layout.rubbish_result_component_view, this);
        this.h = (TXExpandableListView) this.g.findViewById(R.id.list_view);
        this.h.setGroupIndicator(null);
        this.h.setDivider(null);
        this.h.setChildDivider(null);
        this.h.setSelector(R.drawable.transparent_selector);
        this.h.setOnScrollListener(this.k);
        this.h.setOnGroupClickListener(new z(this));
        this.f = new w(this.f3008a);
        this.h.setAdapter(this.f);
        this.e = this.f.getGroupCount();
        this.j = (TextView) this.g.findViewById(R.id.group_title);
    }

    /* access modifiers changed from: private */
    public int b() {
        int i2 = this.d;
        int pointToPosition = this.h.pointToPosition(0, this.d);
        if (pointToPosition == -1 || ExpandableListView.getPackedPositionGroup(this.h.getExpandableListPosition(pointToPosition)) == this.c) {
            return i2;
        }
        View expandChildAt = this.h.getExpandChildAt(pointToPosition - this.h.getFirstVisiblePosition());
        if (expandChildAt != null) {
            return expandChildAt.getTop();
        }
        return 100;
    }

    public void a(Map<Integer, ArrayList<n>> map) {
        this.f.a(map);
        c();
        this.e = this.f.getGroupCount();
    }

    public void a(Handler handler) {
        this.f.a(handler);
    }

    private void c() {
        for (int i2 = 0; i2 < this.f.getGroupCount(); i2++) {
            this.h.expandGroup(i2);
        }
    }
}
