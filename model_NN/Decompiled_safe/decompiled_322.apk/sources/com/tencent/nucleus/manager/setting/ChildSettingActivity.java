package com.tencent.nucleus.manager.setting;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.adapter.ChildSettingAdapter;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.model.ItemElement;
import java.util.List;

/* compiled from: ProGuard */
public class ChildSettingActivity extends BaseActivity {
    private static List<ItemElement> y;
    private Context n;
    private SecondNavigationTitleViewV5 u;
    private ListView v;
    private ChildSettingAdapter w;
    private String x;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_child_setting);
        this.n = this;
        this.x = getIntent().getStringExtra("child_setting_title");
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            y = (List) extras.getSerializable("child_setting_page_key");
        }
        t();
        u();
    }

    private void t() {
        this.w = new ChildSettingAdapter(this.n);
        if (y != null) {
            this.w.a(y);
        }
    }

    private void u() {
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.u.a(this);
        this.u.b(this.x);
        this.u.d();
        this.u.c(new a(this));
        this.v = (ListView) findViewById(R.id.list_view);
        this.v.setAdapter((ListAdapter) this.w);
        this.v.setDivider(null);
    }

    public int f() {
        return STConst.ST_PAGE_SETTING_MESSAGE;
    }

    public boolean onMenuOpened(int i, Menu menu) {
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}
