package com.tencent.nucleus.manager.component;

import com.tencent.assistant.component.SideBar;

/* compiled from: ProGuard */
class am implements SideBar.OnTouchingLetterChangedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserAppListView f2858a;

    am(UserAppListView userAppListView) {
        this.f2858a = userAppListView;
    }

    public void onTouchingLetterChanged(String str) {
        int a2 = this.f2858a.i.a(str.charAt(0));
        if (a2 != -1) {
            this.f2858a.b.post(new an(this, a2));
        }
    }
}
