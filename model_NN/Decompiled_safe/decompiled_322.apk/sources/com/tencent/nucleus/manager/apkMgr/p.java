package com.tencent.nucleus.manager.apkMgr;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: ProGuard */
class p implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Map f2778a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ApkResultListView c;

    p(ApkResultListView apkResultListView, Map map, boolean z) {
        this.c = apkResultListView;
        this.f2778a = map;
        this.b = z;
    }

    public void run() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        ArrayList arrayList = (ArrayList) this.f2778a.get(4);
        ArrayList arrayList2 = (ArrayList) this.f2778a.get(3);
        if (arrayList2 == null || arrayList2.isEmpty()) {
            this.f2778a.remove(3);
        } else {
            ArrayList arrayList3 = new ArrayList();
            arrayList3.addAll(arrayList2);
            linkedHashMap.put(3, arrayList3);
        }
        if (arrayList == null || arrayList.isEmpty()) {
            this.f2778a.remove(4);
        } else {
            ArrayList arrayList4 = new ArrayList();
            arrayList4.addAll(arrayList);
            linkedHashMap.put(4, arrayList4);
        }
        this.c.f2763a.a(linkedHashMap, this.b);
        this.c.f();
        int unused = this.c.i = this.c.f2763a.getGroupCount();
    }
}
