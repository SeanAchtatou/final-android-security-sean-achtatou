package com.tencent.nucleus.manager.about;

import android.content.Context;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;

/* compiled from: ProGuard */
public class AboutDeclareActivity extends BaseActivity {
    private SecondNavigationTitleViewV5 n;
    private TextView u;
    /* access modifiers changed from: private */
    public Context v = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.about_declare_activity_layout);
        this.v = this;
        t();
    }

    private void t() {
        this.n = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.n.a(this);
        this.n.b(getString(R.string.about_declare));
        this.n.d();
        this.u = (TextView) findViewById(R.id.declare_content);
        this.u.setText(u());
        this.u.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private SpannableString u() {
        i iVar = new i(this);
        String str = "        " + getString(R.string.about_declare_content);
        SpannableString spannableString = new SpannableString(str);
        spannableString.setSpan(new j(this, iVar), str.indexOf("《") + 1, str.indexOf("》"), 33);
        return spannableString;
    }
}
