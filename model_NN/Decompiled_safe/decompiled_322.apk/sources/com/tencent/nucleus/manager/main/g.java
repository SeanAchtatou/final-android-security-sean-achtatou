package com.tencent.nucleus.manager.main;

import android.content.Intent;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.nucleus.manager.about.AboutActivity;
import com.tencent.nucleus.manager.setting.SettingActivity;
import com.tencent.pangu.activity.ShareBaseActivity;
import com.tencent.pangu.c.p;

/* compiled from: ProGuard */
class g extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AssistantTabActivity f2949a;

    g(AssistantTabActivity assistantTabActivity) {
        this.f2949a = assistantTabActivity;
    }

    public void onTMAClick(View view) {
        switch (view.getId()) {
            case R.id.layout_score /*2131165356*/:
            case R.id.switcher_tips /*2131165357*/:
                this.f2949a.t();
                return;
            case R.id.layout_toast /*2131165362*/:
                if (this.f2949a.ag != null) {
                    this.f2949a.v.startActivity(new Intent(this.f2949a.v, this.f2949a.ag));
                    return;
                }
                return;
            case R.id.layout_title /*2131165367*/:
                this.f2949a.finish();
                return;
            case R.id.layout_update /*2131165371*/:
                this.f2949a.K();
                return;
            case R.id.layout_setting /*2131165377*/:
                Intent intent = new Intent(this.f2949a.v, SettingActivity.class);
                intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f2949a.f());
                this.f2949a.v.startActivity(intent);
                return;
            case R.id.layout_share /*2131165380*/:
                p.a().a((ShareBaseActivity) this.f2949a.v);
                return;
            case R.id.layout_about /*2131165381*/:
                this.f2949a.v.startActivity(new Intent(this.f2949a.v, AboutActivity.class));
                return;
            default:
                return;
        }
    }

    public STInfoV2 getStInfo(View view) {
        String str = STConst.ST_DEFAULT_SLOT;
        if (view.getTag(R.id.tma_st_slot_tag) instanceof String) {
            str = (String) view.getTag(R.id.tma_st_slot_tag);
        }
        return this.f2949a.a(str, STConst.ST_STATUS_DEFAULT, 200);
    }
}
