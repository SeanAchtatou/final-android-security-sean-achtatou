package com.tencent.nucleus.manager.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.TextureView;

/* compiled from: ProGuard */
public class TxTextureView extends TextureView {
    public TxTextureView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public TxTextureView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public TxTextureView(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            super.onDetachedFromWindow();
        } catch (Exception e) {
        }
    }
}
