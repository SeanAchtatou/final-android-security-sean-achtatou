package com.tencent.nucleus.manager.backgroundscan;

import com.tencent.assistant.db.table.f;
import com.tencent.connect.common.Constants;
import java.util.List;

/* compiled from: ProGuard */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackgroundScanManager f2829a;

    h(BackgroundScanManager backgroundScanManager) {
        this.f2829a = backgroundScanManager;
    }

    public void run() {
        List<BackgroundScan> b = f.a().b();
        if (this.f2829a.f()) {
            this.f2829a.h();
        }
        String str = Constants.STR_EMPTY;
        for (BackgroundScan backgroundScan : b) {
            str = str + backgroundScan.toString() + " | ";
        }
        o.a().a("b_new_scan_push_scan_ret", (byte) 0, null, null, str, 0);
        o.a().a("b_new_scan_push_scan_ret_satisfy", (byte) 0, null, null, this.f2829a.a(b).toString(), 0);
    }
}
