package com.tencent.nucleus.manager.spaceclean;

import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.commom.b;
import com.tencent.tmsecurelite.commom.d;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* compiled from: ProGuard */
class al extends d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceScanManager f3023a;
    private WeakReference<b> b;

    private al(SpaceScanManager spaceScanManager) {
        this.f3023a = spaceScanManager;
        this.b = null;
    }

    /* synthetic */ al(SpaceScanManager spaceScanManager, af afVar) {
        this(spaceScanManager);
    }

    public void a(b bVar) {
        if (bVar == null) {
            this.b = null;
        } else {
            this.b = new WeakReference<>(bVar);
        }
    }

    public void a(int i, ArrayList<DataEntity> arrayList) {
        if (this.b != null && this.b.get() != null) {
            this.b.get().a(i, arrayList);
        }
    }

    public void a(int i, DataEntity dataEntity) {
        if (this.b != null && this.b.get() != null) {
            this.b.get().a(i, dataEntity);
        }
    }
}
