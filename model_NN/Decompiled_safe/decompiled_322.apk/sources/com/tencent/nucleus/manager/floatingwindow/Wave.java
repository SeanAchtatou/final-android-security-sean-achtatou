package com.tencent.nucleus.manager.floatingwindow;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class Wave extends View {
    private ac A;
    private int B;
    private int C;
    private int D;
    private double E;

    /* renamed from: a  reason: collision with root package name */
    public final int f2892a;
    public final int b;
    private final int c;
    private final int d;
    private final int e;
    private final float f;
    private final float g;
    private final float h;
    private final float i;
    private final float j;
    private final float k;
    private final float l;
    private final double m;
    private Path n;
    private Path o;
    private Paint p;
    private Paint q;
    private int r;
    private int s;
    private float t;
    private float u;
    private int v;
    private float w;
    private float x;
    private float y;
    private float z;

    public Wave(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.waveViewStyle);
    }

    public Wave(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.c = 16;
        this.d = 8;
        this.e = 5;
        this.f = 1.5f;
        this.g = 1.0f;
        this.h = 0.5f;
        this.i = 0.13f;
        this.j = 0.09f;
        this.k = 0.05f;
        this.f2892a = 50;
        this.b = 255;
        this.l = 20.0f;
        this.m = 6.283185307179586d;
        this.n = new Path();
        this.o = new Path();
        this.p = new Paint();
        this.q = new Paint();
        this.y = 0.0f;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(this.o, this.q);
        canvas.drawPath(this.n, this.p);
    }

    public void a(int i2) {
        this.r = i2;
    }

    public void b(int i2) {
        this.s = i2;
    }

    public Paint a() {
        return this.p;
    }

    public Paint b() {
        return this.q;
    }

    public void a(int i2, int i3, int i4) {
        this.t = c(i2);
        this.v = d(i3);
        this.x = e(i4);
        this.z = ((float) this.v) * 0.4f;
        setLayoutParams(new ViewGroup.LayoutParams(-1, this.v * 2));
    }

    public void c() {
        this.p.setColor(this.r);
        this.p.setAlpha(50);
        this.p.setStyle(Paint.Style.FILL);
        this.p.setAntiAlias(true);
        this.q.setColor(this.s);
        this.q.setAlpha(255);
        this.q.setStyle(Paint.Style.FILL);
        this.q.setAntiAlias(true);
    }

    private float c(int i2) {
        switch (i2) {
            case 1:
                return 1.5f;
            case 2:
                return 1.0f;
            case 3:
                return 0.5f;
            default:
                return 0.0f;
        }
    }

    private int d(int i2) {
        switch (i2) {
            case 1:
                return 16;
            case 2:
                return 8;
            case 3:
                return 5;
            default:
                return 0;
        }
    }

    private float e(int i2) {
        switch (i2) {
            case 1:
                return 0.13f;
            case 2:
                return 0.09f;
            case 3:
                return 0.05f;
            default:
                return 0.0f;
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        this.n.reset();
        this.o.reset();
        f();
        this.n.moveTo((float) this.B, (float) this.D);
        for (float f2 = 0.0f; f2 <= this.w; f2 += 20.0f) {
            this.n.lineTo(f2, (float) ((((double) this.v) * Math.sin((this.E * ((double) f2)) + ((double) this.y))) + ((double) this.v)));
        }
        this.n.lineTo((float) this.C, (float) this.D);
        this.o.moveTo((float) this.B, (float) this.D);
        for (float f3 = 0.0f; f3 <= this.w; f3 += 20.0f) {
            this.o.lineTo(f3, (float) ((((double) this.v) * Math.sin((this.E * ((double) f3)) + ((double) this.z))) + ((double) this.v)));
        }
        this.o.lineTo((float) this.C, (float) this.D);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        if (8 == i2) {
            removeCallbacks(this.A);
            return;
        }
        removeCallbacks(this.A);
        this.A = new ac(this);
        post(this.A);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (z2 && this.u == 0.0f) {
            e();
        }
    }

    private void e() {
        if (getWidth() != 0) {
            this.u = ((float) getWidth()) * this.t;
            this.B = getLeft();
            this.C = getRight();
            this.D = getBottom();
            this.w = ((float) this.C) + 20.0f;
            this.E = 6.283185307179586d / ((double) this.u);
        }
    }

    private void f() {
        if (this.z > Float.MAX_VALUE) {
            this.z = 0.0f;
        } else {
            this.z += this.x;
        }
        if (this.y > Float.MAX_VALUE) {
            this.y = 0.0f;
        } else {
            this.y += this.x;
        }
    }
}
