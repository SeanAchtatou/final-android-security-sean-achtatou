package com.tencent.nucleus.manager.spaceclean;

import java.util.Comparator;

/* compiled from: ProGuard */
class x implements Comparator<n> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ w f3047a;

    x(w wVar) {
        this.f3047a = wVar;
    }

    /* renamed from: a */
    public int compare(n nVar, n nVar2) {
        if (nVar == null || nVar2 == null) {
            return 0;
        }
        long j = nVar2.d - nVar.d;
        if (j == 0) {
            long j2 = nVar2.c - nVar.c;
            if (j2 > 0) {
                return 1;
            }
            return j2 == 0 ? 0 : -1;
        } else if (j <= 0) {
            return -1;
        } else {
            return 1;
        }
    }
}
