package com.tencent.nucleus.manager.backgroundscan;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseArray;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.db.table.f;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.assistant.protocol.jce.ContextItem;
import com.tencent.assistant.protocol.jce.PushIconInfo;
import com.tencent.assistant.protocol.jce.PushInfo;
import com.tencent.assistant.protocol.jce.PushMsgItem;
import com.tencent.assistant.protocol.jce.PushStyle;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.connect.common.Constants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class d {
    private static d c = null;
    /* access modifiers changed from: private */
    public static boolean d = false;
    /* access modifiers changed from: private */
    public static long i = 60000;

    /* renamed from: a  reason: collision with root package name */
    private Context f2825a;
    /* access modifiers changed from: private */
    public volatile boolean b;
    private int e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g;
    private boolean h;

    static {
        if (Global.isDev()) {
        }
    }

    private d() {
        this.f2825a = null;
        this.b = false;
        this.e = 0;
        this.f = null;
        this.g = null;
        this.h = false;
        this.f2825a = AstApp.i();
    }

    /* access modifiers changed from: package-private */
    public void a(byte b2, int i2) {
        long j;
        XLog.d("BackgroundScan", "<push> adjustWeight");
        PushMsgItem pushMsgItem = BackgroundScanManager.a().c().get(b2);
        BackgroundScan a2 = BackgroundScanManager.a().a(b2);
        XLog.d("BackgroundScan", "<push> Before adjustWeight: type = " + ((int) a2.f2818a) + ", timeGap = " + a2.b + ", weight = " + a2.c);
        switch (i2) {
            case 1:
                XLog.d("BackgroundScan", "<push> adjust click weight");
                a2.c = a2.c <= 0.0d ? pushMsgItem.b * pushMsgItem.h : a2.c * pushMsgItem.h;
                a2.b = a2.b <= 0 ? (long) (((double) a(pushMsgItem.e)) * (2.0d - pushMsgItem.h)) : (long) (((double) a2.b) * (2.0d - pushMsgItem.h));
                break;
            case 2:
                XLog.d("BackgroundScan", "<push> adjust cancel weight");
                a2.c = a2.c <= 0.0d ? pushMsgItem.b * pushMsgItem.i : a2.c * pushMsgItem.i;
                a2.b = a2.b <= 0 ? (long) (((double) a(pushMsgItem.e)) * (2.0d - pushMsgItem.i)) : (long) (((double) a2.b) * (2.0d - pushMsgItem.i));
                break;
            case 3:
                XLog.d("BackgroundScan", "<push> adjust refresh weight");
                a2.c = a2.c <= 0.0d ? pushMsgItem.b * pushMsgItem.j : a2.c * pushMsgItem.j;
                if (a2.b <= 0) {
                    j = (long) (((double) a(pushMsgItem.e)) * (2.0d - pushMsgItem.j));
                } else {
                    j = (long) (((double) a2.b) * (2.0d - pushMsgItem.j));
                }
                a2.b = j;
                break;
        }
        if (a2.c < pushMsgItem.c) {
            a2.c = pushMsgItem.c;
        }
        if (a2.c > pushMsgItem.d) {
            a2.c = pushMsgItem.d;
        }
        if (a2.b < a(pushMsgItem.f)) {
            a2.b = a(pushMsgItem.f);
        }
        if (a2.b > a(pushMsgItem.g)) {
            a2.b = a(pushMsgItem.g);
        }
        XLog.d("BackgroundScan", "<push> After adjustWeight: type = " + ((int) a2.f2818a) + ", timeGap = " + a2.b + ", weight = " + a2.c);
        f.a().b(a2);
    }

    public static long a(double d2) {
        return (long) (d2 * 60.0d * 60.0d * 1000.0d);
    }

    /* access modifiers changed from: private */
    public void b(byte b2) {
        byte a2;
        if (this.h && (a2 = m.a().a("background_scan_current_send_push_type", (byte) 0)) > 0 && a2 != b2) {
            a(a2, 3);
            o.a().a("b_new_scan_push_refresh", b2, this.f, this.g);
        }
        if (b2 > 0) {
            m.a().b("background_scan_current_send_push_type", Byte.valueOf(b2));
            this.h = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: package-private */
    public void a() {
        m.a().b("background_scan_current_send_push_type", (Object) 0);
        this.b = false;
    }

    public static synchronized d b() {
        d dVar;
        synchronized (d.class) {
            if (c == null) {
                c = new d();
            }
            dVar = c;
        }
        return dVar;
    }

    public synchronized void c() {
        XLog.d("BackgroundScan", "<push> push");
        if (this.b) {
            XLog.d("BackgroundScan", "<push> Push is running now, will appear right now !");
        } else {
            this.b = true;
            List<BackgroundScan> b2 = f.a().b();
            String str = Constants.STR_EMPTY;
            for (BackgroundScan backgroundScan : b2) {
                str = str + backgroundScan.toString() + " | ";
            }
            o.a().a("b_new_scan_push_begin_push", (byte) 0, null, null, str, 0);
            if (!i()) {
                XLog.d("BackgroundScan", "<push> Push condation not satified !!");
                this.b = false;
            } else {
                TemporaryThreadManager.get().start(new e(this, str));
            }
        }
    }

    private boolean i() {
        boolean z;
        int i2;
        if (d) {
            return true;
        }
        int d2 = BackgroundScanManager.a().d();
        boolean s = m.a().s();
        if (e() <= d2 && d2 > 0) {
            if (!BackgroundScanManager.a().f()) {
                if (s) {
                    i2 = 3;
                    Iterator<BackgroundScan> it = f.a().b().iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (it.next().e >= 0) {
                                z = true;
                                break;
                            }
                        } else {
                            z = false;
                            break;
                        }
                    }
                } else {
                    XLog.d("BackgroundScan", "<push> Push switch has been closed !");
                    i2 = 2;
                    z = false;
                }
            } else {
                XLog.d("BackgroundScan", "<push> Background scan is running, will push later!");
                i2 = 1;
                z = false;
            }
        } else {
            XLog.d("BackgroundScan", "<push> has reached the day push limit today, limit = " + d2);
            i2 = 0;
            z = false;
        }
        o.a().a("b_new_scan_push_scan_can_push", (byte) i2, Constants.STR_EMPTY, Constants.STR_EMPTY, String.valueOf(z), -1);
        return z;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.b;
    }

    private SparseArray<Integer> j() {
        boolean z;
        boolean z2 = true;
        XLog.i("BackgroundScan", "<push> getWeights");
        List<BackgroundScan> b2 = f.a().b();
        SparseArray<PushMsgItem> c2 = BackgroundScanManager.a().c();
        SparseArray<Integer> sparseArray = new SparseArray<>();
        if (b2.size() > 0 && c2.size() > 0) {
            for (BackgroundScan next : b2) {
                if (next.e > 0 && Math.abs(System.currentTimeMillis() - next.d) > next.b) {
                    switch (next.f2818a) {
                        case 1:
                            if (next.e > 60) {
                                break;
                            }
                            sparseArray.put(next.f2818a, Integer.valueOf((int) (next.c * 100.0d)));
                            XLog.d("BackgroundScan", "<push> Maybe push, type = " + ((int) next.f2818a) + ", weight = " + next.c);
                            break;
                        case 2:
                            if (next.e == 1) {
                                break;
                            }
                            sparseArray.put(next.f2818a, Integer.valueOf((int) (next.c * 100.0d)));
                            XLog.d("BackgroundScan", "<push> Maybe push, type = " + ((int) next.f2818a) + ", weight = " + next.c);
                            break;
                        case 3:
                            if (next.e < 3) {
                                break;
                            }
                            sparseArray.put(next.f2818a, Integer.valueOf((int) (next.c * 100.0d)));
                            XLog.d("BackgroundScan", "<push> Maybe push, type = " + ((int) next.f2818a) + ", weight = " + next.c);
                            break;
                        case 4:
                            if (next.e < 83886080) {
                                break;
                            }
                            sparseArray.put(next.f2818a, Integer.valueOf((int) (next.c * 100.0d)));
                            XLog.d("BackgroundScan", "<push> Maybe push, type = " + ((int) next.f2818a) + ", weight = " + next.c);
                            break;
                        case 5:
                            if (next.e < 536870912) {
                                break;
                            }
                            sparseArray.put(next.f2818a, Integer.valueOf((int) (next.c * 100.0d)));
                            XLog.d("BackgroundScan", "<push> Maybe push, type = " + ((int) next.f2818a) + ", weight = " + next.c);
                            break;
                        case 6:
                            if (next.e <= 0) {
                                break;
                            }
                            sparseArray.put(next.f2818a, Integer.valueOf((int) (next.c * 100.0d)));
                            XLog.d("BackgroundScan", "<push> Maybe push, type = " + ((int) next.f2818a) + ", weight = " + next.c);
                            break;
                    }
                }
            }
        } else {
            o a2 = o.a();
            String str = this.f;
            String str2 = this.g;
            StringBuilder sb = new StringBuilder();
            if (b2.size() > 0) {
                z = true;
            } else {
                z = false;
            }
            StringBuilder append = sb.append(z).append(",");
            if (c2.size() <= 0) {
                z2 = false;
            }
            a2.a("b_new_scan_push_weight", (byte) 0, str, str2, append.append(z2).toString(), 0);
        }
        return sparseArray;
    }

    /* access modifiers changed from: private */
    public byte k() {
        XLog.i("BackgroundScan", "<push> randomSelect");
        if (d) {
            this.e++;
            return (byte) ((this.e % 6) + 1);
        }
        SparseArray<Integer> j = j();
        if (j == null || j.size() <= 0) {
            o.a().a("b_new_scan_push_select", (byte) 0, this.f, this.g);
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < j.size(); i3++) {
            i2 += j.valueAt(i3).intValue();
        }
        if (i2 <= 0) {
            return 0;
        }
        SparseArray sparseArray = new SparseArray();
        int i4 = 0;
        while (i4 < j.size()) {
            int intValue = (j.valueAt(i4).intValue() * 10000) / i2;
            sparseArray.put(j.keyAt(i4), Integer.valueOf(i4 == 0 ? intValue : ((Integer) sparseArray.valueAt(i4 - 1)).intValue() + intValue));
            i4++;
        }
        int b2 = (int) BackgroundScanTimerJob.b(10000);
        for (int i5 = 0; i5 < sparseArray.size(); i5++) {
            if (b2 <= ((Integer) sparseArray.valueAt(i5)).intValue()) {
                return (byte) sparseArray.keyAt(i5);
            }
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: package-private */
    public int e() {
        int a2 = m.a().a("key_background_scan_push_day", 0);
        int a3 = a(System.currentTimeMillis());
        if (a2 != 0 && a2 == a3) {
            return m.a().a("key_background_scan_day_push_times", 0);
        }
        m.a().b("key_background_scan_push_day", Integer.valueOf(a3));
        m.a().b("key_background_scan_day_push_times", (Object) 0);
        return 0;
    }

    public void f() {
        m.a().b("key_background_scan_day_push_times", Integer.valueOf(m.a().a("key_background_scan_day_push_times", 0) + 1));
    }

    static int a(long j) {
        try {
            return Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(new Date(j)));
        } catch (NumberFormatException e2) {
            return 0;
        }
    }

    private String a(byte b2, ContextItem contextItem) {
        String string = this.f2825a.getResources().getString(R.string.app_name);
        BackgroundScan a2 = BackgroundScanManager.a().a(b2);
        if (TextUtils.isEmpty(contextItem.b)) {
            return string;
        }
        if (contextItem.b != null && contextItem.b.contains("$para")) {
            return contextItem.b.replace("$para", a(b2, a2));
        }
        if (contextItem.b != null) {
            return contextItem.b;
        }
        return string;
    }

    private String b(byte b2, ContextItem contextItem) {
        BackgroundScan a2 = BackgroundScanManager.a().a(b2);
        String str = contextItem.c;
        if (contextItem.c != null && contextItem.c.contains("$para")) {
            return contextItem.c.replace("$para", a(b2, a2));
        }
        if (contextItem.c != null) {
            return contextItem.c;
        }
        return str;
    }

    private String a(byte b2, BackgroundScan backgroundScan) {
        switch (b2) {
            case 1:
                return String.valueOf(backgroundScan.e);
            case 2:
                return backgroundScan.e == 2 ? "75%" : Constants.STR_EMPTY;
            case 3:
                return String.valueOf(backgroundScan.e);
            case 4:
                return at.a(backgroundScan.e);
            case 5:
                return at.c(backgroundScan.e);
            case 6:
                return String.valueOf(backgroundScan.e);
            default:
                return Constants.STR_EMPTY;
        }
    }

    private ContextItem c(byte b2) {
        ArrayList<ContextItem> arrayList;
        int intValue;
        XLog.i("BackgroundScan", "<push> getContentText");
        PushMsgItem pushMsgItem = BackgroundScanManager.a().c().get(b2);
        if (!(pushMsgItem == null || (arrayList = pushMsgItem.m) == null || arrayList.size() <= 0)) {
            Iterator<ContextItem> it = arrayList.iterator();
            double d2 = 0.0d;
            while (it.hasNext()) {
                d2 = it.next().d + d2;
            }
            if (d2 > 0.0d) {
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= arrayList.size()) {
                        break;
                    }
                    ContextItem contextItem = arrayList.get(i3);
                    int i4 = (int) ((contextItem.d / d2) * 10000.0d);
                    if (i3 == 0) {
                        intValue = i4;
                    } else {
                        intValue = ((Integer) linkedHashMap.get(arrayList.get(i3 - 1))).intValue() + i4;
                    }
                    linkedHashMap.put(contextItem, Integer.valueOf(intValue));
                    i2 = i3 + 1;
                }
                int b3 = (int) BackgroundScanTimerJob.b(10000);
                for (Map.Entry entry : linkedHashMap.entrySet()) {
                    if (b3 <= ((Integer) entry.getValue()).intValue()) {
                        return (ContextItem) entry.getKey();
                    }
                }
            }
        }
        return null;
    }

    private int d(byte b2) {
        switch (b2) {
            case 1:
                return R.drawable.icon_push_health;
            case 2:
            default:
                return R.drawable.icon_push_accelerate;
            case 3:
                return R.drawable.icon_push_package;
            case 4:
                return R.drawable.icon_push_clean;
            case 5:
                return R.drawable.icon_push_clean;
            case 6:
                return R.drawable.icon_push_safe;
        }
    }

    private String e(byte b2) {
        switch (b2) {
            case 1:
                return "立即优化";
            case 2:
                return "立即加速";
            case 3:
                return "立即清理";
            case 4:
                return "立即清理";
            case 5:
                return "立即清理";
            case 6:
                return "立即处理";
            default:
                return "立即优化";
        }
    }

    public PushInfo a(byte b2) {
        ActionUrl actionUrl = new ActionUrl();
        actionUrl.f1125a = "tmast://";
        PushStyle pushStyle = new PushStyle();
        pushStyle.f1450a = e(b2);
        PushIconInfo pushIconInfo = new PushIconInfo();
        pushIconInfo.f1445a = 7;
        pushIconInfo.b = String.valueOf(d(b2));
        ContextItem c2 = c(b2);
        PushInfo pushInfo = new PushInfo();
        pushInfo.g = 6;
        pushInfo.d = actionUrl;
        pushInfo.i = actionUrl;
        pushInfo.b = a(b2, c2);
        pushInfo.c = b(b2, c2);
        pushInfo.f = pushIconInfo;
        pushInfo.m = pushStyle;
        pushInfo.j = b2;
        XLog.d("BackgroundScan", "The random selected content text is : <" + pushInfo.c + ">");
        this.f = pushInfo.b;
        this.g = pushInfo.c;
        return pushInfo;
    }
}
