package com.tencent.nucleus.manager.apkMgr;

import android.graphics.drawable.Drawable;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Drawable f2770a;
    final /* synthetic */ f b;

    g(f fVar, Drawable drawable) {
        this.b = fVar;
        this.f2770a = drawable;
    }

    public void run() {
        if (this.b.b.c.getTag() != null && this.b.b.c.getTag().equals(this.b.f2769a.mLocalFilePath)) {
            this.b.b.c.setImageDrawable(this.f2770a);
        }
    }
}
