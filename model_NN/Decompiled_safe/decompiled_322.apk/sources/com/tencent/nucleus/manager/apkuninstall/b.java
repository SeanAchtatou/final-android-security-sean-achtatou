package com.tencent.nucleus.manager.apkuninstall;

import com.tencent.assistant.component.SideBar;

/* compiled from: ProGuard */
class b implements SideBar.OnTouchingLetterChangedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PreInstallAppListView f2784a;

    b(PreInstallAppListView preInstallAppListView) {
        this.f2784a = preInstallAppListView;
    }

    public void onTouchingLetterChanged(String str) {
        int a2 = this.f2784a.l.a(str.charAt(0));
        if (a2 != -1) {
            this.f2784a.c.post(new c(this, a2));
        }
    }
}
