package com.tencent.nucleus.manager.apkMgr;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class e extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f2768a;
    final /* synthetic */ LocalApkInfo b;
    final /* synthetic */ STInfoV2 c;
    final /* synthetic */ a d;

    e(a aVar, i iVar, LocalApkInfo localApkInfo, STInfoV2 sTInfoV2) {
        this.d = aVar;
        this.f2768a = iVar;
        this.b = localApkInfo;
        this.c = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.f2768a.b.setSelected(!this.f2768a.b.isSelected());
        this.d.notifyDataSetChanged();
        this.b.mIsSelect = this.f2768a.b.isSelected();
        if (this.d.f2764a != null) {
            this.d.f2764a.sendMessage(this.d.f2764a.obtainMessage(110005, this.b));
        }
    }

    public STInfoV2 getStInfo() {
        this.c.actionId = 200;
        this.c.status = "01";
        return this.c;
    }
}
