package com.tencent.nucleus.manager.spaceclean;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.tencent.assistant.utils.XLog;
import com.tencent.tmsecurelite.commom.ServiceManager;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;

/* compiled from: ProGuard */
class ag implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceScanManager f3018a;

    ag(SpaceScanManager spaceScanManager) {
        this.f3018a = spaceScanManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.spaceclean.SpaceScanManager.a(com.tencent.nucleus.manager.spaceclean.SpaceScanManager, boolean):boolean
     arg types: [com.tencent.nucleus.manager.spaceclean.SpaceScanManager, int]
     candidates:
      com.tencent.nucleus.manager.spaceclean.SpaceScanManager.a(com.tencent.nucleus.manager.spaceclean.SpaceScanManager, com.tencent.tmsecurelite.optimize.ISystemOptimize):com.tencent.tmsecurelite.optimize.ISystemOptimize
      com.tencent.nucleus.manager.spaceclean.SpaceScanManager.a(long, com.tencent.nucleus.manager.spaceclean.SubRubbishInfo):void
      com.tencent.nucleus.manager.spaceclean.SpaceScanManager.a(com.tencent.nucleus.manager.spaceclean.SpaceScanManager, java.util.List):void
      com.tencent.nucleus.manager.spaceclean.SpaceScanManager.a(com.tencent.nucleus.manager.spaceclean.ad, java.util.ArrayList<java.lang.String>):void
      com.tencent.nucleus.manager.spaceclean.SpaceScanManager.a(com.tencent.tmsecurelite.commom.b, java.util.ArrayList<java.lang.String>):void
      com.tencent.nucleus.manager.spaceclean.SpaceScanManager.a(com.tencent.nucleus.manager.spaceclean.SpaceScanManager, boolean):boolean */
    public void onServiceDisconnected(ComponentName componentName) {
        ISystemOptimize unused = this.f3018a.z = (ISystemOptimize) null;
        boolean unused2 = this.f3018a.l = false;
        this.f3018a.u();
        XLog.d("miles", "onServiceDisconnected.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.spaceclean.SpaceScanManager.a(com.tencent.nucleus.manager.spaceclean.SpaceScanManager, boolean):boolean
     arg types: [com.tencent.nucleus.manager.spaceclean.SpaceScanManager, int]
     candidates:
      com.tencent.nucleus.manager.spaceclean.SpaceScanManager.a(com.tencent.nucleus.manager.spaceclean.SpaceScanManager, com.tencent.tmsecurelite.optimize.ISystemOptimize):com.tencent.tmsecurelite.optimize.ISystemOptimize
      com.tencent.nucleus.manager.spaceclean.SpaceScanManager.a(long, com.tencent.nucleus.manager.spaceclean.SubRubbishInfo):void
      com.tencent.nucleus.manager.spaceclean.SpaceScanManager.a(com.tencent.nucleus.manager.spaceclean.SpaceScanManager, java.util.List):void
      com.tencent.nucleus.manager.spaceclean.SpaceScanManager.a(com.tencent.nucleus.manager.spaceclean.ad, java.util.ArrayList<java.lang.String>):void
      com.tencent.nucleus.manager.spaceclean.SpaceScanManager.a(com.tencent.tmsecurelite.commom.b, java.util.ArrayList<java.lang.String>):void
      com.tencent.nucleus.manager.spaceclean.SpaceScanManager.a(com.tencent.nucleus.manager.spaceclean.SpaceScanManager, boolean):boolean */
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            ISystemOptimize unused = this.f3018a.z = (ISystemOptimize) ServiceManager.getInterface(0, iBinder);
            if (!this.f3018a.z.checkVersion(2)) {
                XLog.d("miles", "SpaceScanManager >> onServiceConnected. But mobile manager version is low.");
                this.f3018a.d();
                this.f3018a.t();
                return;
            }
            boolean unused2 = this.f3018a.l = true;
            this.f3018a.s();
        } catch (Exception e) {
            e.printStackTrace();
            this.f3018a.d();
            this.f3018a.t();
        }
    }
}
