package com.tencent.nucleus.manager.appbackup;

import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
/* synthetic */ class b {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f2801a = new int[AppConst.AppState.values().length];

    static {
        try {
            f2801a[AppConst.AppState.DOWNLOADING.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f2801a[AppConst.AppState.QUEUING.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f2801a[AppConst.AppState.FAIL.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f2801a[AppConst.AppState.PAUSED.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f2801a[AppConst.AppState.INSTALLING.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
    }
}
