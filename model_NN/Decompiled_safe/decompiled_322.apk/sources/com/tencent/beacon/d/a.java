package com.tencent.beacon.d;

import android.util.Log;
import java.util.Locale;

/* compiled from: ProGuard */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2126a = false;

    public static void a(String str, Object... objArr) {
        if (f2126a) {
            if (str == null) {
                str = "null";
            } else if (!(objArr == null || objArr.length == 0)) {
                str = String.format(Locale.US, str, objArr);
            }
            Log.i("beacon", str);
        }
    }

    public static void b(String str, Object... objArr) {
        if (f2126a) {
            if (str == null) {
                str = "null";
            } else if (!(objArr == null || objArr.length == 0)) {
                str = String.format(Locale.US, str, objArr);
            }
            Log.d("beacon", str);
        }
    }

    public static void c(String str, Object... objArr) {
        if (str == null) {
            str = "null";
        } else if (!(objArr == null || objArr.length == 0)) {
            str = String.format(Locale.US, str, objArr);
        }
        Log.w("beacon", str);
    }

    public static void d(String str, Object... objArr) {
        if (str == null) {
            str = "null";
        } else if (!(objArr == null || objArr.length == 0)) {
            str = String.format(Locale.US, str, objArr);
        }
        Log.e("beacon", str);
    }

    private static void a(String str, String str2, Object... objArr) {
        if (f2126a) {
            if (str2 == null) {
                str2 = "null";
            } else if (!(objArr == null || objArr.length == 0)) {
                str2 = String.format(Locale.US, str2, objArr);
            }
            Log.d(str, str2);
        }
    }

    public static void e(String str, Object... objArr) {
        a("beacon_step_api", str, objArr);
    }

    public static void f(String str, Object... objArr) {
        a("beacon_step_buffer", str, objArr);
    }

    public static void g(String str, Object... objArr) {
        a("beacon_step_db", str, objArr);
    }

    public static void h(String str, Object... objArr) {
        a("beacon_step_upload", str, objArr);
    }
}
