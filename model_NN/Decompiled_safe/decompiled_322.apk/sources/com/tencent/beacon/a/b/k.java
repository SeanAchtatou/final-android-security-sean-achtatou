package com.tencent.beacon.a.b;

/* compiled from: ProGuard */
public final class k {

    /* renamed from: a  reason: collision with root package name */
    private long f2096a = -1;
    private int b = -1;
    private byte[] c = null;

    public final long a() {
        return this.f2096a;
    }

    public final void a(long j) {
        this.f2096a = j;
    }

    public final int b() {
        return this.b;
    }

    public final void a(int i) {
        this.b = i;
    }

    public final byte[] c() {
        return this.c;
    }

    public final void a(byte[] bArr) {
        this.c = bArr;
    }
}
