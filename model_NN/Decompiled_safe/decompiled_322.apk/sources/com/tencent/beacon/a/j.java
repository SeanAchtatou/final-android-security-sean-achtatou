package com.tencent.beacon.a;

import android.content.Context;
import com.tencent.beacon.d.a;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public final class j {
    private static j c = null;

    /* renamed from: a  reason: collision with root package name */
    private Context f2103a = null;
    private Map<String, FileChannel> b = null;

    private j(Context context) {
        this.f2103a = context;
        this.b = new HashMap(5);
    }

    public static synchronized j a(Context context) {
        j jVar;
        synchronized (j.class) {
            if (c == null) {
                c = new j(context);
            }
            jVar = c;
        }
        return jVar;
    }

    public final synchronized boolean a(String str) {
        boolean z;
        if (str != null) {
            if (str.trim().length() > 0) {
                File b2 = b(str);
                if (b2 == null) {
                    z = true;
                } else {
                    try {
                        FileChannel fileChannel = this.b.get(str);
                        if (fileChannel == null || !fileChannel.isOpen()) {
                            a.b(" create channel %s", str);
                            fileChannel = new FileOutputStream(b2).getChannel();
                            this.b.put(str, fileChannel);
                        }
                        FileLock tryLock = fileChannel.tryLock();
                        if (tryLock != null && tryLock.isValid()) {
                            z = true;
                        }
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                    z = false;
                }
            }
        }
        z = false;
        return z;
    }

    private synchronized File b(String str) {
        IOException e;
        File file;
        try {
            file = new File(this.f2103a.getFilesDir(), "Beacon_" + str + ".lock");
            try {
                if (!file.exists()) {
                    a.b(" create lock file: %s", file.getAbsolutePath());
                    file.createNewFile();
                }
            } catch (IOException e2) {
                e = e2;
                e.printStackTrace();
                return file;
            }
        } catch (IOException e3) {
            IOException iOException = e3;
            file = null;
            e = iOException;
            e.printStackTrace();
            return file;
        }
        return file;
    }
}
