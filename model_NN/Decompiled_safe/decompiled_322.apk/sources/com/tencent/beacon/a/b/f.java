package com.tencent.beacon.a.b;

import android.content.Context;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.beacon.a.h;
import com.tencent.beacon.d.a;
import com.tencent.beacon.f.c;
import com.tencent.beacon.f.d;
import com.tencent.beacon.f.i;
import com.tencent.beacon.f.j;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public final class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private static long f2093a = 0;
    private Context b = null;

    public f(Context context) {
        this.b = context;
    }

    private static synchronized long a() {
        long j;
        synchronized (f.class) {
            j = f2093a;
        }
        return j;
    }

    public static synchronized void a(long j) {
        synchronized (f.class) {
            f2093a = j;
        }
    }

    public final void run() {
        b[] e;
        b[] e2;
        b[] e3;
        k a2 = h.a(this.b, (int) TXTabBarLayout.TABITEM_TIPS_TEXT_ID);
        if (!(a2 == null || a2.b() != 101 || a2.c() == null)) {
            try {
                i c = c.a(this.b).c();
                if (c != null) {
                    c.a(TXTabBarLayout.TABITEM_TIPS_TEXT_ID, a2.c(), false);
                    a.e("common strategy setted by local db", new Object[0]);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        c a3 = c.a(this.b);
        if (a3.f() == 0) {
            a.e("stepCheckApp", new Object[0]);
            a3.a(1);
            boolean d = com.tencent.beacon.a.a.d(this.b);
            a.e("isAppFirstRun : %b", Boolean.valueOf(d));
            if (d) {
                a.e("clear all ao: %d", Integer.valueOf(com.tencent.beacon.a.a.a.a(this.b, null, -1, Long.MAX_VALUE)));
                synchronized (a3) {
                    a.e("appfirstRun", new Object[0]);
                    e3 = a3.e();
                    a3.a(true);
                }
                if (e3 != null) {
                    a.e("notify listener first run", new Object[0]);
                    for (b c2 : e3) {
                        c2.c();
                    }
                }
            } else {
                long a4 = a();
                if (a4 > 0) {
                    a.e("sleep: %d", Long.valueOf(a4));
                    try {
                        Thread.sleep(a4);
                    } catch (InterruptedException e4) {
                        e4.printStackTrace();
                    }
                }
            }
        }
        synchronized (a3) {
            a.e("stepStartQuery", new Object[0]);
            e = a3.e();
            a3.a(2);
        }
        if (e != null) {
            a.e("notify listener query start", new Object[0]);
            for (b a5 : e) {
                a5.a();
            }
        }
        if (!g.a().g()) {
            j a6 = c.a();
            int i = 0;
            while (a6 == null) {
                int i2 = i + 1;
                if (i2 >= 5) {
                    break;
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e5) {
                    e5.printStackTrace();
                }
                a6 = c.a();
                i = i2;
            }
            if (a6 != null) {
                a6.a(new c(this.b, 0, 100));
                try {
                    boolean z = g.a().f();
                    if (z && com.tencent.beacon.b.a.g().equals(com.tencent.beacon.a.a.b(this.b, "GEN_QIMEI", Constants.STR_EMPTY))) {
                        z = false;
                    }
                    if (Constants.STR_EMPTY.equals(com.tencent.beacon.a.a.b(this.b, "QIMEI_DENGTA", Constants.STR_EMPTY))) {
                        z = true;
                    }
                    if (z && !g.a().h()) {
                        a6.a(new d(this.b));
                        com.tencent.beacon.a.a.a(this.b, "GEN_QIMEI", com.tencent.beacon.b.a.g());
                    }
                } catch (Exception e6) {
                    a.c("save GEN_QIMEI flag failed! ", new Object[0]);
                }
            } else {
                a.h("no uphandler ,no upload!", new Object[0]);
            }
        }
        a.h("common query end!", new Object[0]);
        synchronized (a3) {
            e2 = a3.e();
            a3.a(3);
        }
        if (e2 != null) {
            a.e("notify listener query end", new Object[0]);
            for (b b2 : e2) {
                b2.b();
            }
        }
        g b3 = c.a(this.b).b();
        if (b3 == null) {
            a.d("magic should never null ? comStrategy", new Object[0]);
            return;
        }
        long c3 = (long) (b3.c() * 60000);
        if (c3 > 0) {
            com.tencent.beacon.a.d.a().a(this, c3);
            a.h("next time: %d", Long.valueOf(c3));
            c.a(this.b).a(4);
            return;
        }
        a.h("stop loop query", new Object[0]);
    }
}
