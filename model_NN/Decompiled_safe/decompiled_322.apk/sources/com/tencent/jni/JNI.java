package com.tencent.jni;

/* compiled from: ProGuard */
public class JNI {
    public static native String GetUcs2Pinyin(char c);

    static {
        try {
            System.loadLibrary("PinYin");
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
