package com.tencent.smtt.sdk.stat;

import MTT.ThirdAppInfoNew;
import android.os.Build;
import com.tencent.connect.common.Constants;
import com.tencent.smtt.sdk.SecuritySwitch;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class HttpUtils {
    public static final String DEFAULT_ENCODE_NAME = "utf-8";
    public static final String DEFAULT_POST_ADDR = "http://p.mb.qq.com/thirdapp";
    private static final int DEFAULT_TIME_OUT = 20000;
    public static byte[] POST_DATA_KEY = null;
    private static final String TAG = "HttpUtils";
    public static final String WUP_PROXY_DOMAIN = "http://wup.imtt.qq.com:8080";

    static {
        POST_DATA_KEY = null;
        try {
            POST_DATA_KEY = "65dRa93L".getBytes("utf-8");
        } catch (UnsupportedEncodingException e) {
        }
    }

    public static void post(final ThirdAppInfoNew appInfo) {
        new Thread(TAG) {
            public void run() {
                if (HttpUtils.POST_DATA_KEY == null) {
                    try {
                        HttpUtils.POST_DATA_KEY = "65dRa93L".getBytes("utf-8");
                    } catch (UnsupportedEncodingException e) {
                    }
                }
                if (HttpUtils.POST_DATA_KEY != null) {
                    try {
                        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(HttpUtils.DEFAULT_POST_ADDR).openConnection();
                        httpURLConnection.setRequestMethod(Constants.HTTP_POST);
                        httpURLConnection.setDoOutput(true);
                        httpURLConnection.setDoInput(true);
                        httpURLConnection.setUseCaches(false);
                        httpURLConnection.setConnectTimeout(HttpUtils.DEFAULT_TIME_OUT);
                        if (Build.VERSION.SDK_INT > 13) {
                            httpURLConnection.setRequestProperty("Connection", "close");
                        }
                        byte[] postData = null;
                        try {
                            postData = HttpUtils.getPostData(appInfo).toString().getBytes("utf-8");
                        } catch (Exception e2) {
                        }
                        if (postData != null) {
                            try {
                                byte[] postData2 = DesUtils.DesEncrypt(HttpUtils.POST_DATA_KEY, postData, 1);
                                httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                                httpURLConnection.setRequestProperty("Content-Length", String.valueOf(postData2.length));
                                try {
                                    OutputStream outputStream = httpURLConnection.getOutputStream();
                                    outputStream.write(postData2);
                                    outputStream.flush();
                                    if (httpURLConnection.getResponseCode() == 200) {
                                    }
                                } catch (Throwable th) {
                                }
                            } catch (Throwable th2) {
                            }
                        }
                    } catch (IOException e3) {
                    }
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public static StringBuffer getPostData(ThirdAppInfoNew appInfo) {
        StringBuffer buffer = new StringBuffer();
        try {
            buffer.append("sAppName").append("=").append(URLEncoder.encode(appInfo.sAppName, "utf-8")).append("|");
            buffer.append("sTime").append("=").append(URLEncoder.encode(appInfo.sTime, "utf-8")).append("|");
            buffer.append("sQua").append("=").append(URLEncoder.encode(appInfo.sQua, "utf-8")).append("|");
            buffer.append("sLc").append("=").append(URLEncoder.encode(appInfo.sLc, "utf-8")).append("|");
            buffer.append("sGuid").append("=").append(URLEncoder.encode(appInfo.sGuid, "utf-8")).append("|");
            buffer.append("sImei").append("=").append(URLEncoder.encode(appInfo.sImei, "utf-8")).append("|");
            buffer.append("sImsi").append("=").append(URLEncoder.encode(appInfo.sImsi, "utf-8")).append("|");
            buffer.append("sMac").append("=").append(URLEncoder.encode(appInfo.sMac, "utf-8")).append("|");
            buffer.append("iPv").append("=").append(URLEncoder.encode(String.valueOf(appInfo.iPv), "utf-8")).append("|");
            buffer.append("iCoreType").append("=").append(URLEncoder.encode(String.valueOf(appInfo.iCoreType), "utf-8")).append("|");
            buffer.append("SECURITY:");
            boolean securityStatus = SecuritySwitch.isSecurityApplyed();
            int securitySdkVersion = SecuritySwitch.getSecuritySdkVersion();
            int securityJsVersion = SecuritySwitch.getSecurityJsVersion();
            buffer.append("status").append("=").append(URLEncoder.encode(Constants.STR_EMPTY + securityStatus, "utf-8")).append(",");
            buffer.append("verSdk").append("=").append(URLEncoder.encode(Constants.STR_EMPTY + securitySdkVersion, "utf-8")).append(",");
            buffer.append("verJs").append("=").append(URLEncoder.encode(Constants.STR_EMPTY + securityJsVersion, "utf-8"));
            buffer.append(";");
            return buffer;
        } catch (Exception e) {
            return null;
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void doReport(android.content.Context r11, java.lang.String r12, java.lang.String r13, java.lang.String r14, int r15, boolean r16) {
        /*
            com.tencent.smtt.sdk.SecuritySwitch.setContext(r11)     // Catch:{ Throwable -> 0x0071 }
            MTT.ThirdAppInfoNew r7 = new MTT.ThirdAppInfoNew     // Catch:{ Throwable -> 0x0071 }
            r7.<init>()     // Catch:{ Throwable -> 0x0071 }
            java.lang.String r9 = r11.getPackageName()     // Catch:{ Throwable -> 0x0071 }
            r7.sAppName = r9     // Catch:{ Throwable -> 0x0071 }
            java.text.SimpleDateFormat r5 = new java.text.SimpleDateFormat     // Catch:{ Throwable -> 0x0071 }
            java.lang.String r9 = "yyyy-MM-dd hh:mm:ss"
            r5.<init>(r9)     // Catch:{ Throwable -> 0x0071 }
            java.lang.String r9 = "GMT+08"
            java.util.TimeZone r9 = java.util.TimeZone.getTimeZone(r9)     // Catch:{ Throwable -> 0x0071 }
            r5.setTimeZone(r9)     // Catch:{ Throwable -> 0x0071 }
            java.util.Calendar r0 = java.util.Calendar.getInstance()     // Catch:{ Throwable -> 0x0071 }
            java.util.Date r1 = r0.getTime()     // Catch:{ Throwable -> 0x0071 }
            java.lang.String r9 = r5.format(r1)     // Catch:{ Throwable -> 0x0071 }
            r7.sTime = r9     // Catch:{ Throwable -> 0x0071 }
            r7.sGuid = r12     // Catch:{ Throwable -> 0x0071 }
            r7.sQua = r13     // Catch:{ Throwable -> 0x0071 }
            r7.sLc = r14     // Catch:{ Throwable -> 0x0071 }
            java.lang.String r9 = "phone"
            java.lang.Object r8 = r11.getSystemService(r9)     // Catch:{ Throwable -> 0x0071 }
            android.telephony.TelephonyManager r8 = (android.telephony.TelephonyManager) r8     // Catch:{ Throwable -> 0x0071 }
            if (r8 == 0) goto L_0x005c
            java.lang.String r3 = r8.getDeviceId()     // Catch:{ Exception -> 0x006c }
            if (r3 == 0) goto L_0x004c
            java.lang.String r9 = ""
            boolean r9 = r9.equals(r3)     // Catch:{ Exception -> 0x006c }
            if (r9 != 0) goto L_0x004c
            r7.sImei = r3     // Catch:{ Exception -> 0x006c }
        L_0x004c:
            java.lang.String r4 = r8.getSubscriberId()     // Catch:{ Exception -> 0x006c }
            if (r4 == 0) goto L_0x005c
            java.lang.String r9 = ""
            boolean r9 = r9.equals(r4)     // Catch:{ Exception -> 0x006c }
            if (r9 != 0) goto L_0x005c
            r7.sImsi = r4     // Catch:{ Exception -> 0x006c }
        L_0x005c:
            java.lang.String r9 = ""
            r7.sMac = r9     // Catch:{ Throwable -> 0x0071 }
            long r9 = (long) r15     // Catch:{ Throwable -> 0x0071 }
            r7.iPv = r9     // Catch:{ Throwable -> 0x0071 }
            if (r16 == 0) goto L_0x0076
            r9 = 1
        L_0x0066:
            r7.iCoreType = r9     // Catch:{ Throwable -> 0x0071 }
            post(r7)     // Catch:{ Throwable -> 0x0071 }
        L_0x006b:
            return
        L_0x006c:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Throwable -> 0x0071 }
            goto L_0x005c
        L_0x0071:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x006b
        L_0x0076:
            r9 = 0
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.smtt.sdk.stat.HttpUtils.doReport(android.content.Context, java.lang.String, java.lang.String, java.lang.String, int, boolean):void");
    }
}
