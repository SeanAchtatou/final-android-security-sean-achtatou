package com.tencent.smtt.sdk;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import com.tencent.connect.common.Constants;
import com.tencent.smtt.sdk.stat.MttLoader;
import com.tencent.smtt.utils.ReflectionUtils;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.util.HashMap;
import java.util.List;

public class QbSdk {
    public static final String LOGIN_TYPE_KEY_PARTNER_CALL_POS = "PosID";
    public static final String LOGIN_TYPE_KEY_PARTNER_ID = "ChannelID";
    public static final String QB_PACKAGE_NAME = ((System.getProperty("os.arch").toLowerCase().contains("x86") || System.getProperty("os.arch").toLowerCase().contains("i686")) ? "com.tencent.mtt.x86" : "com.tencent.mtt");
    public static final double SDKVERSION = 1.3d;
    private static final String SHELL_DEX_FILE = "sdk_shell.jar";
    private static final String SHELL_IMPL_CLASS = "com.tencent.mtt.sdk.shell.SdkShell";
    public static final int SVNVERSION = 246018;
    public static final int VERSION = 1;
    private static boolean isInstalled = false;
    private static boolean mIsSysWebViewForced = false;
    private static boolean mIsSysWebViewForcedByOuter = false;
    public static boolean sIsVersionPrinted = false;
    private static String sQbVersionName = "0";
    private static Class<?> sShellClass;
    private static Object sShellObj;

    private static void deleteFiles(File dir) {
        if (dir != null && dir.exists() && dir.isDirectory()) {
            for (File file : dir.listFiles()) {
                if (file.isFile()) {
                    file.delete();
                }
            }
        }
    }

    public static boolean startQBToLoadurl(Context context, String url, int posID) {
        HashMap<String, String> args = new HashMap<>();
        args.put(LOGIN_TYPE_KEY_PARTNER_ID, context.getApplicationInfo().processName);
        args.put(LOGIN_TYPE_KEY_PARTNER_CALL_POS, Integer.toString(posID));
        return MttLoader.loadUrl(context, url, args) == 0;
    }

    public static boolean startQBForVideo(Context context, String url, int posID) {
        HashMap<String, String> args = new HashMap<>();
        args.put(LOGIN_TYPE_KEY_PARTNER_ID, context.getApplicationInfo().processName);
        args.put(LOGIN_TYPE_KEY_PARTNER_CALL_POS, Integer.toString(posID));
        return MttLoader.openVideoWithQb(context, url, args);
    }

    public static boolean startQBForDoc(Context context, String url, int posID, int type, String format) {
        HashMap<String, String> args = new HashMap<>();
        args.put(LOGIN_TYPE_KEY_PARTNER_ID, context.getApplicationInfo().processName);
        args.put(LOGIN_TYPE_KEY_PARTNER_CALL_POS, Integer.toString(posID));
        return MttLoader.openDocWithQb(context, url, type, format, args);
    }

    private static boolean init(Context context) {
        if (!sIsVersionPrinted) {
            QbSdkLog.e("QbSdk", "svn version is 246018");
            QbSdkLog.e("QbSdk", "sdk version is 1.3");
            sIsVersionPrinted = true;
        }
        if (mIsSysWebViewForced || mIsSysWebViewForcedByOuter) {
            return false;
        }
        try {
            File optDir = context.getDir(SDKEngine.X5_CORE_FOLDER_NAME, 0);
            PackageManager pm = context.getPackageManager();
            PackageInfo info = pm.getPackageInfo(QB_PACKAGE_NAME, 0);
            if (sQbVersionName.equals("0") || sQbVersionName.equals(info.versionName)) {
                sQbVersionName = info.versionName;
                if (sShellClass != null) {
                    return true;
                }
                Context qbCtx = context.createPackageContext(SDKEngine.X5QQBROWSER_PKG_NAME, 2);
                File dexFile = new File(qbCtx.getDir(SDKEngine.X5_SHARE_FOLDER_NAME, 0), SHELL_DEX_FILE);
                if (!dexFile.exists()) {
                    QbSdkLog.d("X5CoreNeedReboot", "QbSdk--init()--ACTION_INSTALL_X5");
                    Intent intent = new Intent("com.tencent.mtt.ACTION_INSTALL_X5");
                    intent.setPackage(QB_PACKAGE_NAME);
                    List<ResolveInfo> services = pm.queryIntentServices(intent, 0);
                    if (services != null && services.size() > 0) {
                        context.startService(intent);
                    }
                    isInstalled = true;
                    checkX5CoreInstalled();
                    QbSdkLog.e("QbSdk", "sys WebView: no dex");
                    return false;
                }
                sShellClass = new DexClassLoader(dexFile.getAbsolutePath(), optDir.getAbsolutePath(), null, QbSdk.class.getClassLoader()).loadClass(SHELL_IMPL_CLASS);
                sShellObj = sShellClass.getConstructor(Context.class).newInstance(qbCtx);
                ReflectionUtils.invokeInstance(sShellObj, "setClientVersion", new Class[]{Integer.TYPE}, 1);
                return true;
            }
            sShellClass = null;
            sShellObj = null;
            deleteFiles(optDir);
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            QbSdkLog.e("QbSdk", "sys WebView: nameNotFound");
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
            QbSdkLog.e("QbSdk", "sys WebView: " + e2.getMessage());
            return false;
        } catch (Throwable t) {
            QbSdkLog.e("QbSdk", "sys WebView: " + t.getMessage());
            return false;
        }
    }

    public static void setQbSdkLogClient(QbSdkLogClient qbsdklogclient) {
        QbSdkLog.setQbSdkLogClient(qbsdklogclient);
    }

    public static boolean canLoadX5(Context context) {
        if (!init(context)) {
            return false;
        }
        ReflectionUtils.invokeInstance(sShellObj, "setAppContext", new Class[]{Context.class}, context);
        Object ret = ReflectionUtils.invokeInstance(sShellObj, "canLoadX5");
        if (ret != null) {
            return ((Boolean) ret).booleanValue();
        }
        return false;
    }

    public static boolean canOpenMimeFileType(Context context, String mimeType) {
        Object ret;
        if (init(context) && (ret = ReflectionUtils.invokeStatic(sShellClass, "canOpenMimeFileType", new Class[]{String.class}, mimeType)) != null) {
            return ((Boolean) ret).booleanValue();
        }
        return false;
    }

    public static String getX5CoreTimestamp() {
        Object ret = ReflectionUtils.invokeStatic(sShellClass, "getX5CoreTimestamp", null, new Object[0]);
        return ret == null ? Constants.STR_EMPTY : (String) ret;
    }

    static void forceSysWebViewInner() {
        mIsSysWebViewForced = true;
        QbSdkLog.e("QbSdk", "sys WebView: forceSysWebViewInner called");
    }

    public static void forceSysWebView() {
        mIsSysWebViewForcedByOuter = true;
        QbSdkLog.e("QbSdk", "sys WebView: forceSysWebView called");
    }

    public static void unForceSysWebView() {
        mIsSysWebViewForcedByOuter = false;
        QbSdkLog.e("QbSdk", "sys WebView: unForceSysWebView called");
    }

    public static boolean isSdkVideoServiceFg(Context ctx) {
        try {
            List<ActivityManager.RunningAppProcessInfo> processList = ((ActivityManager) ctx.getApplicationContext().getSystemService("activity")).getRunningAppProcesses();
            if (processList == null || processList.size() == 0) {
                return false;
            }
            for (ActivityManager.RunningAppProcessInfo process : processList) {
                if (process.importance == 100 && process.processName != null && process.processName.contains("com.tencent.mtt:VideoService")) {
                    return true;
                }
            }
            return false;
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    static void installX5(Context context) {
        if (!isInstalled) {
            isInstalled = true;
            try {
                PackageManager pm = context.getPackageManager();
                if (!new File(context.createPackageContext(SDKEngine.X5QQBROWSER_PKG_NAME, 2).getDir(SDKEngine.X5_SHARE_FOLDER_NAME, 0), SHELL_DEX_FILE).exists()) {
                    QbSdkLog.d("X5CoreNeedReboot", "QbSdk--installX5()--ACTION_INSTALL_X5");
                    Intent intent = new Intent("com.tencent.mtt.ACTION_INSTALL_X5");
                    intent.setPackage(QB_PACKAGE_NAME);
                    List<ResolveInfo> services = pm.queryIntentServices(intent, 0);
                    if (services != null && services.size() > 0) {
                        context.startService(intent);
                    }
                    checkX5CoreInstalled();
                    QbSdkLog.e("QbSdk", "sys WebView: no dex");
                }
            } catch (PackageManager.NameNotFoundException e) {
            } catch (Throwable thr) {
                thr.printStackTrace();
            }
        }
    }

    static boolean checkX5CoreInstalled(Context context) {
        if (context == null) {
            return false;
        }
        try {
            if (new File(context.createPackageContext(SDKEngine.X5QQBROWSER_PKG_NAME, 2).getDir(SDKEngine.X5_SHARE_FOLDER_NAME, 0), SHELL_DEX_FILE).exists()) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static void checkX5CoreInstalled() {
        SDKEngine sdkEngine = SDKEngine.getInstance(false);
        if (sdkEngine != null) {
            sdkEngine.checkX5CoreInstalled();
        }
    }
}
