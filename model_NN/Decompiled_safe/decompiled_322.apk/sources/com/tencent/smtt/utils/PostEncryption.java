package com.tencent.smtt.utils;

import android.annotation.TargetApi;
import android.util.Base64;
import com.tencent.smtt.sdk.stat.DesUtils;
import java.security.KeyFactory;
import java.security.spec.X509EncodedKeySpec;
import java.util.Random;
import javax.crypto.Cipher;

public class PostEncryption {
    private static final char[] HEXARRAY = "0123456789abcdef".toCharArray();
    private static final String PUBLICKEY = "MCwwDQYJKoZIhvcNAQEBBQADGwAwGAIRAMRB/Q0hTCD+XtnQhpQJefUCAwEAAQ==";
    private static final String RSA_NO_PADDING = "RSA/ECB/NoPadding";
    private static PostEncryption mInstance;
    private String mDesKeyStr;
    private String mRsaKeyStr;
    private String mRsaKeyStrTemp;

    private PostEncryption() {
        int desKey = new Random().nextInt(89999999) + 10000000;
        int padding = new Random().nextInt(89999999) + 10000000;
        this.mDesKeyStr = String.valueOf(desKey);
        this.mRsaKeyStrTemp = this.mDesKeyStr + String.valueOf(padding);
    }

    public static synchronized PostEncryption getInstance() {
        PostEncryption postEncryption;
        synchronized (PostEncryption.class) {
            if (mInstance == null) {
                mInstance = new PostEncryption();
            }
            postEncryption = mInstance;
        }
        return postEncryption;
    }

    @TargetApi(8)
    public String initRSAKey() throws Exception {
        if (this.mRsaKeyStr == null) {
            byte[] rsaBytes = this.mRsaKeyStrTemp.getBytes();
            Cipher mRSAEncryptCipher = Cipher.getInstance(RSA_NO_PADDING);
            mRSAEncryptCipher.init(1, KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decode(PUBLICKEY.getBytes(), 0))));
            this.mRsaKeyStr = bytesToHex(mRSAEncryptCipher.doFinal(rsaBytes));
        }
        return this.mRsaKeyStr;
    }

    public byte[] DESEncrypt(byte[] byteArray) throws Exception {
        return DesUtils.DesEncrypt(this.mDesKeyStr.getBytes(), byteArray, 1);
    }

    public byte[] DESDecrypt(byte[] byteArray) throws Exception {
        return DesUtils.DesEncrypt(this.mDesKeyStr.getBytes(), byteArray, 0);
    }

    private String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[(bytes.length * 2)];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 255;
            hexChars[j * 2] = HEXARRAY[v >>> 4];
            hexChars[(j * 2) + 1] = HEXARRAY[v & 15];
        }
        return new String(hexChars);
    }
}
