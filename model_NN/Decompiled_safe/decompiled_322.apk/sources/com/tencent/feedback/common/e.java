package com.tencent.feedback.common;

import android.content.Context;
import com.tencent.assistant.st.STConst;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.proguard.ac;
import com.tencent.feedback.proguard.al;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public final class e {
    private static e G = null;
    private String A = null;
    private Map<String, PlugInInfo> B = null;
    private boolean C = true;
    private String D = null;
    private List<al> E;
    private Boolean F = null;
    private final long H;

    /* renamed from: a  reason: collision with root package name */
    private final Context f2543a;
    private final String b;
    private final byte c;
    private String d;
    private String e;
    private final String f;
    private final String g;
    private final String h;
    private final String i;
    private final String j;
    private final String k;
    private String l = "10000";
    private String m = STConst.ST_INSTALL_FAIL_STR_UNKNOWN;
    private long n = 0;
    private String o = Constants.STR_EMPTY;
    private String p = null;
    private String q = null;
    private String r = null;
    private String s = null;
    private String t = null;
    private String u = null;
    private String v = null;
    private long w = -1;
    private long x = -1;
    private long y = -1;
    private String z = null;

    private e(Context context) {
        Context applicationContext;
        g.b("rqdp{  init cominfo}", new Object[0]);
        if (context == null) {
            applicationContext = context;
        } else {
            applicationContext = context.getApplicationContext();
            if (applicationContext == null) {
                applicationContext = context;
            }
        }
        this.f2543a = applicationContext;
        f.a(this.f2543a);
        this.c = 1;
        this.d = b.b(context);
        this.e = b.c(context);
        this.f = "com.tencent.feedback";
        this.g = "1.9.6";
        this.h = f.l();
        this.i = f.a();
        this.j = "Android " + f.b() + ",level " + f.c();
        this.b = this.i + ";" + this.j;
        this.H = new Date().getTime();
        this.k = b.i(this.f2543a);
    }

    public static synchronized e a(Context context) {
        e eVar;
        synchronized (e.class) {
            if (G == null) {
                G = new e(context);
            }
            eVar = G;
        }
        return eVar;
    }

    public final String a() {
        return this.b;
    }

    public final byte b() {
        return 1;
    }

    public final synchronized String c() {
        return this.d;
    }

    public final String d() {
        return this.f;
    }

    public final String e() {
        return this.g;
    }

    public final String f() {
        return this.h;
    }

    public final String g() {
        return this.j;
    }

    public final synchronized String h() {
        return this.l;
    }

    public final synchronized void a(String str) {
        if (str == null) {
            str = "10000";
        }
        this.l = str;
    }

    public final synchronized String i() {
        return this.o;
    }

    public final synchronized void b(String str) {
        this.o = str;
        if (str != null) {
            a(false);
        }
    }

    public final synchronized String j() {
        return this.m;
    }

    public final synchronized void c(String str) {
        this.m = str;
    }

    public final synchronized long k() {
        return this.n;
    }

    public final synchronized void a(long j2) {
        this.n = j2;
    }

    public final synchronized String l() {
        if (this.p == null) {
            this.p = b.d(this.f2543a);
        }
        return this.p;
    }

    public final synchronized String m() {
        if (this.r == null) {
            this.r = b.h(this.f2543a);
        }
        return this.r;
    }

    public final synchronized boolean n() {
        return this.r != null;
    }

    public final synchronized void d(String str) {
        this.r = str;
    }

    public final synchronized String o() {
        if (this.s == null) {
            this.s = b.a(this.f2543a);
            if (this.s == null || this.s.equals(Constants.STR_EMPTY)) {
                this.s = this.d;
            }
        }
        return this.s;
    }

    public final synchronized String p() {
        String str;
        if (!F()) {
            str = Constants.STR_EMPTY;
        } else {
            if (this.q == null) {
                f.a(this.f2543a);
                this.q = f.b(this.f2543a);
            }
            str = this.q;
        }
        return str;
    }

    public final synchronized String q() {
        String str;
        if (!F()) {
            str = Constants.STR_EMPTY;
        } else {
            if (this.t == null) {
                f.a(this.f2543a);
                this.t = f.e(this.f2543a);
            }
            str = this.t;
        }
        return str;
    }

    public final synchronized String r() {
        String str;
        if (!F()) {
            str = Constants.STR_EMPTY;
        } else {
            if (this.u == null) {
                f.a(this.f2543a);
                this.u = f.c(this.f2543a);
            }
            str = this.u;
        }
        return str;
    }

    public final synchronized String s() {
        String str;
        if (!F()) {
            str = Constants.STR_EMPTY;
        } else {
            if (this.v == null) {
                f.a(this.f2543a);
                this.v = f.d(this.f2543a);
            }
            str = this.v;
        }
        return str;
    }

    public final synchronized long t() {
        if (this.w <= 0) {
            f.a(this.f2543a);
            this.w = f.e();
        }
        return this.w;
    }

    public final synchronized long u() {
        if (this.x <= 0) {
            f.a(this.f2543a);
            this.x = f.g();
        }
        return this.x;
    }

    public final synchronized long v() {
        if (this.y <= 0) {
            this.y = f.a(this.f2543a).i();
        }
        return this.y;
    }

    public final synchronized String w() {
        if (this.z == null) {
            f.a(this.f2543a);
            this.z = f.d();
        }
        return this.z;
    }

    public final synchronized String x() {
        if (this.A == null) {
            f.a(this.f2543a);
            this.A = ac.b("ro.board.platform");
        }
        return this.A;
    }

    private synchronized boolean F() {
        return this.C;
    }

    private synchronized void a(boolean z2) {
        this.C = false;
    }

    public final synchronized Map<String, PlugInInfo> y() {
        HashMap hashMap;
        if (this.B == null || this.B.size() <= 0) {
            hashMap = null;
        } else {
            hashMap = new HashMap(this.B.size());
            hashMap.putAll(this.B);
        }
        return hashMap;
    }

    public final synchronized boolean a(String str, String str2, String str3) {
        boolean z2 = true;
        synchronized (this) {
            if (str == null || str2 == null || str3 == null) {
                z2 = false;
            } else {
                if (this.B == null) {
                    this.B = new HashMap();
                }
                this.B.put(str, new PlugInInfo(str, str2, str3));
                g.a("add %s %s %s", str, str2, str3);
            }
        }
        return z2;
    }

    public final synchronized void e(String str) {
        if (str != null) {
            if (this.B != null) {
                this.B.remove(str);
            }
        }
    }

    public final String z() {
        if (this.D == null) {
            f.a(this.f2543a);
            this.D = f.k();
        }
        return this.D;
    }

    public final synchronized List<al> A() {
        return this.E;
    }

    public final synchronized void f(String str) {
        this.e = str;
    }

    public final synchronized String B() {
        return this.e;
    }

    public final synchronized boolean C() {
        if (this.F == null) {
            this.F = Boolean.valueOf(k.a().b());
        }
        return this.F.booleanValue();
    }

    public final long D() {
        return this.H;
    }

    public final String E() {
        return this.k;
    }
}
