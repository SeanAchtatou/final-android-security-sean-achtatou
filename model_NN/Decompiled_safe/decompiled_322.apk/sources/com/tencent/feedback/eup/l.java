package com.tencent.feedback.eup;

import android.content.Context;
import com.tencent.feedback.b.b;
import com.tencent.feedback.b.i;
import com.tencent.feedback.b.j;
import com.tencent.feedback.common.g;
import com.tencent.feedback.proguard.ac;
import com.tencent.feedback.proguard.aj;
import com.tencent.feedback.proguard.ao;

/* compiled from: ProGuard */
public final class l extends com.tencent.feedback.common.l {
    private static l b;
    private d c;
    private d d;
    private j e;
    private b f;
    private final boolean g;

    public static synchronized l a(Context context, String str, boolean z, i iVar, b bVar, b bVar2, d dVar) {
        l lVar;
        synchronized (l.class) {
            if (b == null) {
                g.a("rqdp{  eup create instance}", new Object[0]);
                l lVar2 = new l(context, str, false, iVar, bVar, bVar2, dVar);
                b = lVar2;
                lVar2.a(true);
            }
            lVar = b;
        }
        return lVar;
    }

    public static synchronized l l() {
        l lVar;
        synchronized (l.class) {
            lVar = b;
        }
        return lVar;
    }

    public static synchronized i a(Context context, boolean z) {
        j a2;
        synchronized (l.class) {
            a2 = j.a(context, z);
        }
        return a2;
    }

    public static boolean m() {
        if (!n()) {
            return false;
        }
        g.a("rqdp{  doUploadExceptionDatas}", new Object[0]);
        l l = l();
        if (l != null) {
            return l.h();
        }
        g.c("rqdp{  instance == null}", new Object[0]);
        return false;
    }

    public static boolean n() {
        l l = l();
        if (l == null) {
            g.d("rqdp{  not init eup}", new Object[0]);
            return false;
        }
        boolean a2 = l.a();
        if (!a2 || !l.s()) {
            return a2;
        }
        return l.b();
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private l(android.content.Context r11, java.lang.String r12, boolean r13, com.tencent.feedback.b.i r14, com.tencent.feedback.b.b r15, com.tencent.feedback.eup.b r16, com.tencent.feedback.eup.d r17) {
        /*
            r10 = this;
            r4 = 3
            r5 = 202(0xca, float:2.83E-43)
            r6 = 302(0x12e, float:4.23E-43)
            com.tencent.feedback.eup.k r8 = new com.tencent.feedback.eup.k
            if (r11 != 0) goto L_0x0041
            r1 = r11
        L_0x000a:
            r8.<init>(r1)
            r1 = r10
            r2 = r11
            r3 = r12
            r7 = r14
            r9 = r15
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            r1 = 0
            r10.c = r1
            r1 = 0
            r10.d = r1
            r1 = 0
            r10.e = r1
            r1 = 0
            r10.f = r1
            if (r17 == 0) goto L_0x0049
            java.lang.String r1 = "rqdp{  cus eupstrategy} %s"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            r2[r3] = r17
            com.tencent.feedback.common.g.b(r1, r2)
            r0 = r17
            r10.c = r0
        L_0x0032:
            android.content.Context r1 = r10.f2549a
            com.tencent.feedback.eup.j r1 = com.tencent.feedback.eup.j.a(r1)
            r10.e = r1
            r0 = r16
            r10.f = r0
            r10.g = r13
            return
        L_0x0041:
            android.content.Context r1 = r11.getApplicationContext()
            if (r1 != 0) goto L_0x000a
            r1 = r11
            goto L_0x000a
        L_0x0049:
            java.lang.String r1 = "rqdp{  default eupstrategy}"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.b(r1, r2)
            com.tencent.feedback.eup.d r1 = new com.tencent.feedback.eup.d
            r1.<init>()
            r10.c = r1
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.eup.l.<init>(android.content.Context, java.lang.String, boolean, com.tencent.feedback.b.i, com.tencent.feedback.b.b, com.tencent.feedback.eup.b, com.tencent.feedback.eup.d):void");
    }

    private synchronized boolean s() {
        return this.g;
    }

    public final synchronized d o() {
        return this.c;
    }

    public final synchronized d p() {
        return this.d;
    }

    public final synchronized void a(d dVar) {
        this.d = dVar;
    }

    public final synchronized b q() {
        return this.f;
    }

    public final void f() {
        int i = -1;
        super.f();
        Context context = this.f2549a;
        g.b("rqdp{  EUPDAO.deleteEup() start}", new Object[0]);
        if (context == null) {
            g.c("rqdp{  deleteEup() context is null arg}", new Object[0]);
        } else {
            i = aj.a(context, new int[]{1, 2}, -1, Long.MAX_VALUE, -1, -1);
        }
        g.b("rqdp{  eup clear} %d ", Integer.valueOf(i));
        g.b("rqdp{  eup strategy clear} %d ", Integer.valueOf(ac.b(this.f2549a, 302)));
    }

    public final boolean i() {
        return p() != null;
    }

    public final boolean h() {
        if (super.h()) {
            m a2 = m.a(this.f2549a);
            i c2 = c();
            if (a2 == null || c2 == null) {
                g.c("rqdp{  upDatas or uphandler null!}", new Object[0]);
                return false;
            }
            try {
                c2.a(a2);
                return true;
            } catch (Throwable th) {
                if (!g.a(th)) {
                    th.printStackTrace();
                }
                g.d("rqdp{  upload eupdata error} %s", th.toString());
            }
        }
        return false;
    }

    public final int g() {
        d r = r();
        if (r == null || super.g() < 0) {
            return -1;
        }
        if (!r.e()) {
            g.b("rqdp{  in no merge}", new Object[0]);
            return g.b(this.f2549a);
        }
        g.b("rqdp{  in merge}", new Object[0]);
        if (g.a(this.f2549a)) {
            return 1;
        }
        return 0;
    }

    public final d r() {
        d dVar;
        try {
            if (ao.a(this.f2549a).b().h()) {
                dVar = p();
            } else {
                dVar = null;
            }
            if (dVar == null) {
                return o();
            }
            return dVar;
        } catch (Throwable th) {
            if (!g.a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    public final synchronized void b(boolean z) {
        super.b(z);
        if (a()) {
            this.e.a();
        } else {
            this.e.b();
        }
    }

    public final void e() {
        int i = -1;
        super.e();
        Context context = this.f2549a;
        g.b("rqdp{  EUPDAO.deleteEup() start}", new Object[0]);
        if (context == null) {
            g.c("rqdp{  deleteEup() context is null arg}", new Object[0]);
        } else {
            i = aj.a(context, new int[]{1, 2}, -1, Long.MAX_VALUE, 3, -1);
        }
        g.b("remove fail updata num :%d", Integer.valueOf(i));
        if (k() == 1) {
            BuglyBroadcastRecevier.a(this.f2549a);
        }
    }
}
