package com.tencent.feedback.proguard;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tencent.feedback.common.f;
import com.tencent.feedback.common.g;
import java.io.File;

/* compiled from: ProGuard */
public final class n extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private Context f2603a;

    public n(Context context) {
        super(context, "eup_db", (SQLiteDatabase.CursorFactory) null, 14);
        this.f2603a = context;
    }

    public final synchronized void onCreate(SQLiteDatabase sQLiteDatabase) {
        synchronized (this) {
            if (sQLiteDatabase != null) {
                if (ak.f2588a != null) {
                    for (String[] strArr : ak.f2588a) {
                        g.b("rqdp{  table:}%s %s", strArr[0], strArr[1]);
                        sQLiteDatabase.execSQL(strArr[1]);
                    }
                }
            }
        }
    }

    public final synchronized void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        g.b("rqdp{  upgrade a db }[%s]rqdp{   from v}%d rqdp{  to v}%d rqdp{  , deleted all tables!}", "eup_db", Integer.valueOf(i), Integer.valueOf(i2));
        if (a(sQLiteDatabase)) {
            g.b("rqdp{  drop all success recreate!}", new Object[0]);
            onCreate(sQLiteDatabase);
        } else {
            g.d("rqdp{  drop all fail try deleted file,may next time will success!}", new Object[0]);
            File databasePath = this.f2603a.getDatabasePath("eup_db");
            if (databasePath != null && databasePath.canWrite()) {
                databasePath.delete();
            }
        }
    }

    @TargetApi(11)
    public final synchronized void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        f.a(this.f2603a);
        if (Integer.parseInt(f.c()) >= 11) {
            g.b("rqdp{  downgrade a db} [%s] rqdp{  from v}%d rqdp{  to} v%d rqdp{  , deleted all tables!}", "eup_db", Integer.valueOf(i), Integer.valueOf(i2));
            if (a(sQLiteDatabase)) {
                g.b("rqdp{  drop all success recreate!}", new Object[0]);
                onCreate(sQLiteDatabase);
            } else {
                g.d("rqdp{  drop all fail try deleted file,may next time will success!}", new Object[0]);
                File databasePath = this.f2603a.getDatabasePath("eup_db");
                if (databasePath != null && databasePath.canWrite()) {
                    databasePath.delete();
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x007d A[SYNTHETIC, Splitter:B:32:0x007d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean a(android.database.sqlite.SQLiteDatabase r13) {
        /*
            r12 = this;
            r8 = 1
            r10 = 0
            r9 = 0
            monitor-enter(r12)
            java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ Throwable -> 0x009a, all -> 0x0097 }
            r11.<init>()     // Catch:{ Throwable -> 0x009a, all -> 0x0097 }
            java.lang.String r1 = "sqlite_master"
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ Throwable -> 0x009a, all -> 0x0097 }
            r0 = 0
            java.lang.String r3 = "name"
            r2[r0] = r3     // Catch:{ Throwable -> 0x009a, all -> 0x0097 }
            java.lang.String r3 = "type = 'table'"
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r13
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x009a, all -> 0x0097 }
            if (r1 == 0) goto L_0x0047
        L_0x0020:
            boolean r0 = r1.moveToNext()     // Catch:{ Throwable -> 0x002f }
            if (r0 == 0) goto L_0x0047
            r0 = 0
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Throwable -> 0x002f }
            r11.add(r0)     // Catch:{ Throwable -> 0x002f }
            goto L_0x0020
        L_0x002f:
            r0 = move-exception
        L_0x0030:
            boolean r2 = com.tencent.feedback.common.g.a(r0)     // Catch:{ all -> 0x007a }
            if (r2 != 0) goto L_0x0039
            r0.printStackTrace()     // Catch:{ all -> 0x007a }
        L_0x0039:
            if (r1 == 0) goto L_0x0044
            boolean r0 = r1.isClosed()     // Catch:{ all -> 0x0087 }
            if (r0 != 0) goto L_0x0044
            r1.close()     // Catch:{ all -> 0x0087 }
        L_0x0044:
            r0 = r9
        L_0x0045:
            monitor-exit(r12)
            return r0
        L_0x0047:
            java.lang.String r2 = "drop table if exists %s"
            int r0 = r11.size()     // Catch:{ Throwable -> 0x002f }
            if (r0 <= 0) goto L_0x008a
            java.util.Iterator r3 = r11.iterator()     // Catch:{ Throwable -> 0x002f }
        L_0x0053:
            boolean r0 = r3.hasNext()     // Catch:{ Throwable -> 0x002f }
            if (r0 == 0) goto L_0x008a
            java.lang.Object r0 = r3.next()     // Catch:{ Throwable -> 0x002f }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Throwable -> 0x002f }
            java.util.Locale r4 = java.util.Locale.US     // Catch:{ Throwable -> 0x002f }
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x002f }
            r6 = 0
            r5[r6] = r0     // Catch:{ Throwable -> 0x002f }
            java.lang.String r4 = java.lang.String.format(r4, r2, r5)     // Catch:{ Throwable -> 0x002f }
            r13.execSQL(r4)     // Catch:{ Throwable -> 0x002f }
            java.lang.String r4 = "rqdp{  drop }%s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x002f }
            r6 = 0
            r5[r6] = r0     // Catch:{ Throwable -> 0x002f }
            com.tencent.feedback.common.g.b(r4, r5)     // Catch:{ Throwable -> 0x002f }
            goto L_0x0053
        L_0x007a:
            r0 = move-exception
        L_0x007b:
            if (r1 == 0) goto L_0x0086
            boolean r2 = r1.isClosed()     // Catch:{ all -> 0x0087 }
            if (r2 != 0) goto L_0x0086
            r1.close()     // Catch:{ all -> 0x0087 }
        L_0x0086:
            throw r0     // Catch:{ all -> 0x0087 }
        L_0x0087:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        L_0x008a:
            if (r1 == 0) goto L_0x0095
            boolean r0 = r1.isClosed()     // Catch:{ all -> 0x0087 }
            if (r0 != 0) goto L_0x0095
            r1.close()     // Catch:{ all -> 0x0087 }
        L_0x0095:
            r0 = r8
            goto L_0x0045
        L_0x0097:
            r0 = move-exception
            r1 = r10
            goto L_0x007b
        L_0x009a:
            r0 = move-exception
            r1 = r10
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.n.a(android.database.sqlite.SQLiteDatabase):boolean");
    }

    public final synchronized SQLiteDatabase getReadableDatabase() {
        SQLiteDatabase sQLiteDatabase;
        int i = 0;
        synchronized (this) {
            sQLiteDatabase = null;
            while (sQLiteDatabase == null && i < 5) {
                i++;
                try {
                    sQLiteDatabase = super.getReadableDatabase();
                } catch (Throwable th) {
                    g.c("rqdp{  getReadableDatabase error count} %d", Integer.valueOf(i));
                    if (i == 5) {
                        g.d("rqdp{  error get DB failed}", new Object[0]);
                    }
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        if (!g.a(th)) {
                            th.printStackTrace();
                        }
                    }
                }
            }
        }
        return sQLiteDatabase;
    }

    public final synchronized SQLiteDatabase getWritableDatabase() {
        SQLiteDatabase sQLiteDatabase;
        int i = 0;
        synchronized (this) {
            sQLiteDatabase = null;
            while (sQLiteDatabase == null && i < 5) {
                i++;
                try {
                    sQLiteDatabase = super.getWritableDatabase();
                } catch (Exception e) {
                    g.c("rqdp{  getWritableDatabase error count} %d", Integer.valueOf(i));
                    if (i == 5) {
                        g.d("rqdp{  error get DB failed}", new Object[0]);
                    }
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e2) {
                        if (!g.a(e)) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return sQLiteDatabase;
    }
}
