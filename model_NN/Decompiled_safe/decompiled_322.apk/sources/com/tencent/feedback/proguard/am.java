package com.tencent.feedback.proguard;

import android.content.Context;
import android.util.SparseArray;
import com.tencent.feedback.b.h;
import com.tencent.feedback.common.g;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public final class am implements h {

    /* renamed from: a  reason: collision with root package name */
    private Context f2590a = null;

    public am(Context context) {
        this.f2590a = context;
    }

    public final void a(int i, byte[] bArr, boolean z) {
        boolean z2;
        boolean z3;
        boolean z4;
        if (i != 300) {
            g.c("rqdp{  com strategy unmatch key:}%d", Integer.valueOf(i));
        } else if (bArr != null) {
            at b = ao.a(this.f2590a).b();
            if (b == null) {
                g.c("rqdp{  imposible! common strategy null!}", new Object[0]);
                return;
            }
            try {
                T t = new T();
                t.a(new ag(bArr));
                if (t == null || b == null) {
                    z2 = false;
                } else {
                    boolean z5 = false;
                    if (t.g != b.h()) {
                        g.b("rqdp{  useSStrategy changed to} %b", Boolean.valueOf(t.g));
                        z5 = true;
                        b.a(t.g);
                    }
                    if (!t.e.equals(b.a())) {
                        g.b("rqdp{  url changed to} %s", t.e);
                        z5 = true;
                        b.a(t.e);
                    } else {
                        g.b("rqdp{  url same to} %s", t.e);
                    }
                    if (t.c != b.b()) {
                        g.b("rqdp{  upStrategy changed to} %d", Integer.valueOf(t.c));
                        z5 = true;
                        b.a(t.c);
                    }
                    if (t.d != b.c()) {
                        g.b("rqdp{  QueryPeriod changed to} %d", Integer.valueOf(t.d));
                        z5 = true;
                        b.b(t.d);
                    }
                    if (t.f != b.j()) {
                        g.b("rqdp{  enforceQuery changed to} %b", Boolean.valueOf(t.f));
                        z5 = true;
                        b.b(t.f);
                    }
                    W w = t.b;
                    if (w == null || b == null) {
                        z3 = false;
                    } else {
                        z3 = false;
                        if (w.c != b.f()) {
                            g.b("rqdp{  ea changed:}%d", Integer.valueOf(w.c));
                            z3 = true;
                            b.c(w.c);
                        }
                        if (w.d != b.g()) {
                            g.b("rqdp{  za changed:}%d", Integer.valueOf(w.d));
                            z3 = true;
                            b.d(w.d);
                        }
                        if (!w.f2579a.equals(b.d())) {
                            g.b("rqdp{  enk changed}", new Object[0]);
                            z3 = true;
                            b.b(w.f2579a);
                        }
                        if (!w.b.equals(b.e())) {
                            g.b("rqdp{  enpk changed}", new Object[0]);
                            z3 = true;
                            b.c(w.b);
                        }
                    }
                    if (z3) {
                        z5 = true;
                    }
                    ArrayList<V> arrayList = t.f2576a;
                    if (b == null) {
                        z4 = false;
                    } else {
                        z4 = false;
                        if (arrayList != null) {
                            SparseArray<au> i2 = b.i();
                            if (i2 != null) {
                                int i3 = 0;
                                boolean z6 = false;
                                while (i3 < i2.size()) {
                                    au valueAt = i2.valueAt(i3);
                                    Iterator<V> it = arrayList.iterator();
                                    boolean z7 = z6;
                                    boolean z8 = false;
                                    while (it.hasNext()) {
                                        V next = it.next();
                                        if (next.f2578a == valueAt.f2596a) {
                                            z8 = true;
                                            if (valueAt.d() != next.c) {
                                                g.b("rqdp{  mid:}%d rqdp{  , need detail changed:}%b ", Integer.valueOf(next.f2578a), Boolean.valueOf(next.c));
                                                z7 = true;
                                                valueAt.c(next.c);
                                            }
                                            if (!valueAt.a().equals(next.b)) {
                                                g.b("rqdp{  mid:}%d rqdp{  , url changed:}%s", Integer.valueOf(next.f2578a), next.b);
                                                z7 = true;
                                                valueAt.a(next.b);
                                            } else {
                                                g.b("rqdp{  mid:}%d rqdp{  , url same:}%s", Integer.valueOf(next.f2578a), next.b);
                                            }
                                        }
                                        z7 = z7;
                                        z8 = z8;
                                    }
                                    if (z8 != valueAt.c()) {
                                        g.b("rqdp{  mid:}%d rqdp{  , enable} %b", Integer.valueOf(valueAt.f2596a), Boolean.valueOf(z8));
                                        z7 = true;
                                        valueAt.b(z8);
                                    }
                                    i3++;
                                    z6 = z7;
                                }
                                z4 = z6;
                            }
                            Iterator<V> it2 = arrayList.iterator();
                            while (it2.hasNext()) {
                                V next2 = it2.next();
                                if (b.e(next2.f2578a) == null) {
                                    g.c("rqdp{  imposiable! module base strategy not ready! mId:}%d", Integer.valueOf(next2.f2578a));
                                }
                            }
                        } else {
                            SparseArray<au> i4 = b.i();
                            if (i4 != null) {
                                int i5 = 0;
                                while (i5 < i4.size()) {
                                    au valueAt2 = i4.valueAt(i5);
                                    if (valueAt2.c()) {
                                        g.b("rqdp{  mid:}%d rqdp{  , server closed}", Integer.valueOf(valueAt2.e()));
                                        z4 = true;
                                        valueAt2.b(false);
                                    }
                                    i5++;
                                    z4 = z4;
                                }
                            }
                        }
                    }
                    z2 = z4 ? true : z5;
                }
                if (z2) {
                    if (z) {
                        g.b("rqdp{  update common strategy should save}", new Object[0]);
                        ac.a(this.f2590a, i, bArr);
                    }
                    g.b("rqdp{  com strategy changed notify!}", new Object[0]);
                    ao.a(this.f2590a).a(b);
                }
            } catch (Throwable th) {
                if (!g.a(th)) {
                    th.printStackTrace();
                }
                g.d("rqdp{  error to common strategy!}", new Object[0]);
            }
        }
    }
}
