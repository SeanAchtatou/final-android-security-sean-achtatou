package com.tencent.feedback.proguard;

import android.util.SparseArray;
import com.tencent.feedback.common.o;

/* compiled from: ProGuard */
public final class at {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2595a = false;
    private SparseArray<au> b;
    private String c;
    private int d;
    private int e;
    private String f;
    private String g;
    private int h;
    private int i;
    private boolean j;
    private boolean k;

    public at() {
        this.b = null;
        this.c = "http://monitor.uu.qq.com/analytics/upload";
        this.d = -1;
        this.e = 6;
        this.f = "*^@K#K@!";
        this.g = "S(@L@L@)";
        this.h = 1;
        this.i = 1;
        this.j = true;
        this.k = false;
        this.b = new SparseArray<>(5);
        this.b.append(3, new au(3));
        this.b = this.b;
    }

    public final synchronized String a() {
        return this.c;
    }

    public final synchronized void a(String str) {
        this.c = str;
    }

    public final synchronized int b() {
        return this.d;
    }

    public final synchronized void a(int i2) {
        this.d = i2;
    }

    public final synchronized int c() {
        return this.e;
    }

    public final synchronized void b(int i2) {
        if (i2 > 0) {
            this.e = i2;
        }
    }

    public final synchronized String d() {
        return this.f;
    }

    public final synchronized void b(String str) {
        this.f = str;
    }

    public final synchronized String e() {
        return this.g;
    }

    public final synchronized void c(String str) {
        this.g = str;
    }

    public final synchronized int f() {
        return this.h;
    }

    public final synchronized void c(int i2) {
        this.h = i2;
    }

    public final synchronized int g() {
        return this.i;
    }

    public final synchronized void d(int i2) {
        this.i = i2;
    }

    public final synchronized boolean h() {
        return this.j;
    }

    public final synchronized void a(boolean z) {
        this.j = z;
    }

    public final synchronized SparseArray<au> i() {
        SparseArray<au> sparseArray;
        if (this.b != null) {
            new o();
            sparseArray = o.a(this.b);
        } else {
            sparseArray = null;
        }
        return sparseArray;
    }

    public final synchronized au e(int i2) {
        au auVar;
        if (this.b != null) {
            auVar = this.b.get(i2);
        } else {
            auVar = null;
        }
        return auVar;
    }

    public final synchronized boolean j() {
        return this.k;
    }

    public final synchronized void b(boolean z) {
        this.k = z;
    }
}
