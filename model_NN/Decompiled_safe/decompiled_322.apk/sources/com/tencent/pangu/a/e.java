package com.tencent.pangu.a;

import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BaseActivity f3292a;
    final /* synthetic */ d b;

    e(d dVar, BaseActivity baseActivity) {
        this.b = dVar;
        this.f3292a = baseActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.a.d.a(com.tencent.pangu.a.d, boolean):void
     arg types: [com.tencent.pangu.a.d, int]
     candidates:
      com.tencent.pangu.a.d.a(com.tencent.pangu.a.d, android.graphics.drawable.AnimationDrawable):void
      com.tencent.pangu.a.d.a(com.tencent.pangu.a.d, boolean):void */
    public void onClick(View view) {
        if (this.b.c != null) {
            this.b.e();
            this.b.b(true);
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3292a, 200);
            if (buildSTInfo != null) {
                buildSTInfo.slotId = "15_001";
                l.a(buildSTInfo);
            }
        }
    }
}
