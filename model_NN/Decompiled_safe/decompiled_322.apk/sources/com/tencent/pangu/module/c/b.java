package com.tencent.pangu.module.c;

import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.db.a.a;
import com.tencent.assistant.db.table.j;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.module.p;
import com.tencent.assistant.protocol.jce.UploadControlResponse;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.a.d;
import com.tencent.assistant.utils.aq;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.DownloadManager;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class b extends p implements UIEventListener {
    private static b b = null;

    /* renamed from: a  reason: collision with root package name */
    private AstApp f3929a = AstApp.i();
    /* access modifiers changed from: private */
    public volatile boolean c = false;
    /* access modifiers changed from: private */
    public volatile boolean d = false;
    /* access modifiers changed from: private */
    public int e = -1;
    /* access modifiers changed from: private */
    public a f;
    private ApkResCallback.Stub g = new c(this);
    private j h = null;
    /* access modifiers changed from: private */
    public Map<String, a> i = null;

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            if (b == null) {
                b = new b();
            }
            bVar = b;
        }
        return bVar;
    }

    private b() {
        h();
        b();
    }

    public void b() {
        this.f3929a.k().addUIEventListener(1002, this);
        this.f3929a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        this.f3929a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ADD_APP_INSTALL_TASK, this);
        this.f3929a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ADD_APP_UNINSTALL_TASK, this);
        this.f3929a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        this.f3929a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START, this);
        this.f3929a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_DIFF_MERGE_FAIL_RELATED_FILE_UPLOAD, this);
        this.f3929a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_PAUSE, this);
        ApkResourceManager.getInstance().registerApkResCallback(this.g);
    }

    public void handleUIEvent(Message message) {
        if (message != null) {
            switch (message.what) {
                case 1002:
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING:
                case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START:
                case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START:
                case EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_PAUSE:
                case EventDispatcherEnum.UI_EVENT_ADD_APP_INSTALL_TASK:
                case EventDispatcherEnum.UI_EVENT_ADD_APP_UNINSTALL_TASK:
                    this.c = true;
                    return;
                case EventDispatcherEnum.UI_EVENT_DIFF_MERGE_FAIL_RELATED_FILE_UPLOAD:
                    if (e()) {
                        this.c = false;
                        if (!this.d && d()) {
                            this.d = true;
                            c();
                            return;
                        }
                        return;
                    }
                    this.c = true;
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        if (this.e == i2) {
            if (this.c) {
                this.d = false;
                return;
            }
            if (jceStruct2 != null && (jceStruct2 instanceof UploadControlResponse)) {
                UploadControlResponse uploadControlResponse = (UploadControlResponse) jceStruct2;
                m.a().h(uploadControlResponse.c);
                if (!d()) {
                    AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_CLOSE_DIFF_RELATED_FILE_UPLOAD);
                    i();
                    return;
                } else if (uploadControlResponse.c == 3) {
                    List<String> a2 = a(this.f);
                    if (a2 == null) {
                        b(this.f);
                    } else if (a(uploadControlResponse.d, uploadControlResponse.b, a2)) {
                        b(this.f);
                    }
                }
            }
            this.f = null;
            this.d = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        this.f = null;
        this.d = false;
    }

    public void c() {
        TemporaryThreadManager.get().start(new d(this));
    }

    public List<String> a(a aVar) {
        if (aVar == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        if (!new File(aVar.g).exists()) {
            return null;
        }
        arrayList.add(aVar.g);
        if (new File(aVar.h).exists()) {
            arrayList.add(aVar.h);
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:104:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0151 A[SYNTHETIC, Splitter:B:32:0x0151] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0156 A[SYNTHETIC, Splitter:B:35:0x0156] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x015b  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0199 A[SYNTHETIC, Splitter:B:61:0x0199] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x019e A[SYNTHETIC, Splitter:B:64:0x019e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(java.lang.String r13, java.lang.String r14, java.util.List<java.lang.String> r15) {
        /*
            r12 = this;
            if (r15 == 0) goto L_0x000e
            boolean r0 = r15.isEmpty()
            if (r0 != 0) goto L_0x000e
            boolean r0 = android.text.TextUtils.isEmpty(r13)
            if (r0 == 0) goto L_0x0010
        L_0x000e:
            r0 = 0
        L_0x000f:
            return r0
        L_0x0010:
            r3 = 0
            r2 = 0
            r1 = 0
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()     // Catch:{ Throwable -> 0x01de, all -> 0x0195 }
            java.lang.String r4 = "power"
            java.lang.Object r0 = r0.getSystemService(r4)     // Catch:{ Throwable -> 0x01de, all -> 0x0195 }
            android.os.PowerManager r0 = (android.os.PowerManager) r0     // Catch:{ Throwable -> 0x01de, all -> 0x0195 }
            r4 = 1
            java.lang.Class r5 = r12.getClass()     // Catch:{ Throwable -> 0x01de, all -> 0x0195 }
            java.lang.String r5 = r5.getSimpleName()     // Catch:{ Throwable -> 0x01de, all -> 0x0195 }
            android.os.PowerManager$WakeLock r4 = r0.newWakeLock(r4, r5)     // Catch:{ Throwable -> 0x01de, all -> 0x0195 }
            r0 = 0
            r4.setReferenceCounted(r0)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r4.acquire()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r6 = "---------7d4a6d158c9"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r0.<init>()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r3 = "\r\n--"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r3 = "--\r\n"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            byte[] r7 = r0.getBytes()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            int r8 = r15.size()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r0 = 0
            r5 = r0
        L_0x0058:
            if (r5 >= r8) goto L_0x01ad
            java.lang.Object r0 = r15.get(r5)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.io.File r9 = new java.io.File     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r9.<init>(r0)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            boolean r0 = r9.exists()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            if (r0 != 0) goto L_0x0073
            r0 = r1
            r1 = r2
        L_0x006d:
            int r2 = r5 + 1
            r5 = r2
            r2 = r1
            r1 = r0
            goto L_0x0058
        L_0x0073:
            java.net.URL r0 = new java.net.URL     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r3.<init>()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.StringBuilder r3 = r3.append(r13)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r10 = "?"
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.StringBuilder r3 = r3.append(r14)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r0.<init>(r3)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r3 = 1
            r0.setDoOutput(r3)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r3 = 0
            r0.setDoInput(r3)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r3 = 0
            r0.setUseCaches(r3)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r3 = "POST"
            r0.setRequestMethod(r3)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r3 = "connection"
            java.lang.String r10 = "Keep-Alive"
            r0.setRequestProperty(r3, r10)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r3 = "Charsert"
            java.lang.String r10 = "UTF-8"
            r0.setRequestProperty(r3, r10)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r3 = "Content-Type"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r10.<init>()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r11 = "multipart/form-data; boundary="
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.StringBuilder r10 = r10.append(r6)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r10 = r10.toString()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r0.setRequestProperty(r3, r10)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r3 = 2048(0x800, float:2.87E-42)
            r0.setChunkedStreamingMode(r3)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r3.<init>()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r10 = "--"
            r3.append(r10)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r3.append(r6)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r10 = "\r\n"
            r3.append(r10)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r10.<init>()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r11 = "Content-Disposition: form-data;name=\"file"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.StringBuilder r10 = r10.append(r5)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r11 = "\";filename=\""
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r11 = r9.getName()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r11 = "\"\r\n"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r10 = r10.toString()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r3.append(r10)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r10 = "Content-Type:application/octet-stream\r\n\r\n"
            r3.append(r10)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            byte[] r10 = r3.getBytes()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.io.DataOutputStream r3 = new java.io.DataOutputStream     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            java.io.OutputStream r11 = r0.getOutputStream()     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r3.<init>(r11)     // Catch:{ Throwable -> 0x01e1, all -> 0x01d5 }
            r3.write(r10)     // Catch:{ Throwable -> 0x01e5, all -> 0x01db }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x01e5, all -> 0x01db }
            java.io.FileInputStream r10 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x01e5, all -> 0x01db }
            r10.<init>(r9)     // Catch:{ Throwable -> 0x01e5, all -> 0x01db }
            r2.<init>(r10)     // Catch:{ Throwable -> 0x01e5, all -> 0x01db }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]     // Catch:{ Throwable -> 0x014a, all -> 0x01d7 }
            java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x014a, all -> 0x01d7 }
        L_0x0137:
            boolean r9 = r12.c     // Catch:{ Throwable -> 0x014a, all -> 0x01d7 }
            if (r9 != 0) goto L_0x0166
            int r9 = r2.read(r1)     // Catch:{ Throwable -> 0x014a, all -> 0x01d7 }
            r10 = -1
            if (r9 == r10) goto L_0x0166
            r10 = 0
            r3.write(r1, r10, r9)     // Catch:{ Throwable -> 0x014a, all -> 0x01d7 }
            r3.flush()     // Catch:{ Throwable -> 0x014a, all -> 0x01d7 }
            goto L_0x0137
        L_0x014a:
            r0 = move-exception
            r1 = r2
            r2 = r3
            r3 = r4
        L_0x014e:
            r0 = 0
            if (r1 == 0) goto L_0x0154
            r1.close()     // Catch:{ IOException -> 0x01c5 }
        L_0x0154:
            if (r2 == 0) goto L_0x0159
            r2.close()     // Catch:{ IOException -> 0x01c7 }
        L_0x0159:
            if (r3 == 0) goto L_0x000f
            boolean r1 = r3.isHeld()
            if (r1 == 0) goto L_0x000f
            r3.release()
            goto L_0x000f
        L_0x0166:
            r2.close()     // Catch:{ Throwable -> 0x014a, all -> 0x01d7 }
            r3.write(r7)     // Catch:{ Throwable -> 0x014a, all -> 0x01d7 }
            r3.flush()     // Catch:{ Throwable -> 0x014a, all -> 0x01d7 }
            r3.close()     // Catch:{ Throwable -> 0x014a, all -> 0x01d7 }
            boolean r1 = r12.c     // Catch:{ Throwable -> 0x014a, all -> 0x01d7 }
            if (r1 == 0) goto L_0x018e
            r0 = 0
            if (r2 == 0) goto L_0x017c
            r2.close()     // Catch:{ IOException -> 0x01cd }
        L_0x017c:
            if (r3 == 0) goto L_0x0181
            r3.close()     // Catch:{ IOException -> 0x01cf }
        L_0x0181:
            if (r4 == 0) goto L_0x000f
            boolean r1 = r4.isHeld()
            if (r1 == 0) goto L_0x000f
            r4.release()
            goto L_0x000f
        L_0x018e:
            r0.getResponseCode()     // Catch:{ Throwable -> 0x014a, all -> 0x01d7 }
            r0 = r2
            r1 = r3
            goto L_0x006d
        L_0x0195:
            r0 = move-exception
            r4 = r3
        L_0x0197:
            if (r1 == 0) goto L_0x019c
            r1.close()     // Catch:{ IOException -> 0x01c9 }
        L_0x019c:
            if (r2 == 0) goto L_0x01a1
            r2.close()     // Catch:{ IOException -> 0x01cb }
        L_0x01a1:
            if (r4 == 0) goto L_0x01ac
            boolean r1 = r4.isHeld()
            if (r1 == 0) goto L_0x01ac
            r4.release()
        L_0x01ac:
            throw r0
        L_0x01ad:
            if (r1 == 0) goto L_0x01b2
            r1.close()     // Catch:{ IOException -> 0x01d1 }
        L_0x01b2:
            if (r2 == 0) goto L_0x01b7
            r2.close()     // Catch:{ IOException -> 0x01d3 }
        L_0x01b7:
            if (r4 == 0) goto L_0x01c2
            boolean r0 = r4.isHeld()
            if (r0 == 0) goto L_0x01c2
            r4.release()
        L_0x01c2:
            r0 = 1
            goto L_0x000f
        L_0x01c5:
            r1 = move-exception
            goto L_0x0154
        L_0x01c7:
            r1 = move-exception
            goto L_0x0159
        L_0x01c9:
            r1 = move-exception
            goto L_0x019c
        L_0x01cb:
            r1 = move-exception
            goto L_0x01a1
        L_0x01cd:
            r1 = move-exception
            goto L_0x017c
        L_0x01cf:
            r1 = move-exception
            goto L_0x0181
        L_0x01d1:
            r0 = move-exception
            goto L_0x01b2
        L_0x01d3:
            r0 = move-exception
            goto L_0x01b7
        L_0x01d5:
            r0 = move-exception
            goto L_0x0197
        L_0x01d7:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0197
        L_0x01db:
            r0 = move-exception
            r2 = r3
            goto L_0x0197
        L_0x01de:
            r0 = move-exception
            goto L_0x014e
        L_0x01e1:
            r0 = move-exception
            r3 = r4
            goto L_0x014e
        L_0x01e5:
            r0 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x014e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.module.c.b.a(java.lang.String, java.lang.String, java.util.List):boolean");
    }

    public static boolean d() {
        return m.a().S() != 1;
    }

    public static boolean e() {
        return !d.a() && !DownloadManager.a().d() && !com.tencent.pangu.utils.installuninstall.p.a().c();
    }

    public static String a(long j) {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(j);
        if (localApkInfo != null) {
            return localApkInfo.manifestMd5;
        }
        return Constants.STR_EMPTY;
    }

    private void h() {
        this.h = new j(AstApp.i());
        this.i = Collections.synchronizedMap(new LinkedHashMap());
        ArrayList<a> a2 = this.h.a();
        if (a2 != null && !a2.isEmpty()) {
            for (a next : a2) {
                if (TextUtils.isEmpty(next.g)) {
                    this.h.b(next);
                } else if (!new File(next.g).exists()) {
                    this.h.b(next);
                } else if (!this.i.containsKey(next.d)) {
                    this.i.put(next.d, next);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(a aVar, boolean z) {
        if (aVar != null) {
            if (!this.i.containsKey(aVar.d) || z) {
                this.i.put(aVar.d, aVar);
                this.h.c(aVar);
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(a aVar) {
        if (aVar != null) {
            this.i.remove(aVar.d);
            this.h.b(aVar);
            if (!TextUtils.isEmpty(aVar.h)) {
                FileUtil.deleteFile(aVar.h);
            }
        }
    }

    private void i() {
        if (!this.i.isEmpty()) {
            for (String str : this.i.keySet()) {
                b(this.i.get(str));
            }
        }
    }

    public void a(long j, long j2, String str, String str2, int i2, String str3) {
        TemporaryThreadManager.get().start(new e(this, str2, str, j, j2, i2, str3));
    }

    /* access modifiers changed from: private */
    public boolean c(a aVar) {
        if (aVar == null) {
            return false;
        }
        if (TextUtils.isEmpty(aVar.k)) {
            if (TextUtils.isEmpty(aVar.g)) {
                return false;
            }
            File file = new File(aVar.g);
            if (!file.exists()) {
                return false;
            }
            aVar.i = file.length();
            if (this.c) {
                return false;
            }
            aVar.k = aq.a(file).toLowerCase();
        }
        if (TextUtils.isEmpty(aVar.l)) {
            if (TextUtils.isEmpty(aVar.h)) {
                return false;
            }
            File file2 = new File(aVar.h);
            if (!file2.exists()) {
                return false;
            }
            aVar.j = file2.length();
            if (this.c) {
                return false;
            }
            aVar.l = aq.a(file2).toLowerCase();
        }
        return true;
    }

    public a f() {
        if (this.i.isEmpty()) {
            return null;
        }
        return this.i.get(this.i.keySet().iterator().next());
    }

    public boolean g() {
        return !this.i.isEmpty();
    }
}
