package com.tencent.pangu.module.wisedownload;

import com.tencent.pangu.download.DownloadInfo;
import java.util.Comparator;

/* compiled from: ProGuard */
final class w implements Comparator<DownloadInfo> {
    w() {
    }

    /* renamed from: a */
    public int compare(DownloadInfo downloadInfo, DownloadInfo downloadInfo2) {
        if (downloadInfo == null || downloadInfo2 == null) {
            return 0;
        }
        return (int) ((long) ((int) (downloadInfo2.downloadEndTime - downloadInfo.downloadEndTime)));
    }
}
