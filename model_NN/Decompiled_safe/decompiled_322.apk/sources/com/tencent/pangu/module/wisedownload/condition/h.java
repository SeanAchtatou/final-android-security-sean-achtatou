package com.tencent.pangu.module.wisedownload.condition;

import com.tencent.pangu.module.wisedownload.condition.ThresholdCondition;

/* compiled from: ProGuard */
/* synthetic */ class h {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f3962a = new int[ThresholdCondition.CONDITION_TYPE.values().length];

    static {
        try {
            f3962a[ThresholdCondition.CONDITION_TYPE.CONDITION_SWITCH.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f3962a[ThresholdCondition.CONDITION_TYPE.CONDITION_PRIMARY.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f3962a[ThresholdCondition.CONDITION_TYPE.CONDITION_TIME.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
    }
}
