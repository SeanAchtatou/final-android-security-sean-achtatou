package com.tencent.pangu.module.timer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tencent.assistant.utils.XLog;
import com.tencent.pangu.module.au;

/* compiled from: ProGuard */
public class RecoverAppListReceiver extends BroadcastReceiver {
    static a b;

    /* renamed from: a  reason: collision with root package name */
    Context f3948a;

    /* compiled from: ProGuard */
    public enum RecoverAppListState {
        NOMAL,
        FIRST_SHORT,
        SECONDLY_SHORT
    }

    public void onReceive(Context context, Intent intent) {
        XLog.i("PopUpNecessaryAcitivity", "RecoverAppListReceiver!");
        this.f3948a = context;
        if (intent.getAction().equals("com.tencent.assistant.timer.revcover.app")) {
            a();
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        au auVar = new au();
        b = new a(this);
        auVar.register(b);
        auVar.a(false);
        auVar.a();
    }
}
