package com.tencent.pangu.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.m;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.GetDesktopShortcutRequest;
import com.tencent.assistant.protocol.jce.GetDesktopShortcutResponse;
import com.tencent.pangu.module.a.n;

/* compiled from: ProGuard */
public class be extends BaseEngine<n> {
    public void a() {
        send(new GetDesktopShortcutRequest());
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z = true;
        GetDesktopShortcutResponse getDesktopShortcutResponse = (GetDesktopShortcutResponse) jceStruct2;
        notifyDataChanged(new bf(this, i, getDesktopShortcutResponse));
        m a2 = m.a();
        if (getDesktopShortcutResponse.b() != 1) {
            z = false;
        }
        a2.q(z);
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChanged(new bg(this, i, i2));
    }
}
