package com.tencent.pangu.module.timer;

import com.tencent.assistant.m;
import com.tencent.assistant.manager.notification.v;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.utils.an;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.module.a.g;
import com.tencent.pangu.module.timer.RecoverAppListReceiver;

/* compiled from: ProGuard */
public class a implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RecoverAppListReceiver f3950a;

    public a(RecoverAppListReceiver recoverAppListReceiver) {
        this.f3950a = recoverAppListReceiver;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.v.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
     arg types: [com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, int]
     candidates:
      com.tencent.assistant.manager.notification.v.a(int, long):void
      com.tencent.assistant.manager.notification.v.a(int, android.app.Notification):void
      com.tencent.assistant.manager.notification.v.a(int, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, boolean):void
      com.tencent.assistant.manager.notification.v.a(boolean, int):void
      com.tencent.assistant.manager.notification.v.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void */
    public void a(int i, int i2, GetPhoneUserAppListResponse getPhoneUserAppListResponse) {
        if (i2 != 0 || getPhoneUserAppListResponse == null) {
            r.a(this.f3950a.f3948a, System.currentTimeMillis());
            return;
        }
        m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.FIRST_SHORT.ordinal()));
        MainActivity.z = RecoverAppListReceiver.RecoverAppListState.FIRST_SHORT.ordinal();
        v.a().a(getPhoneUserAppListResponse, true);
        m.a().a("key_re_app_list_first_response", an.a(getPhoneUserAppListResponse));
    }
}
