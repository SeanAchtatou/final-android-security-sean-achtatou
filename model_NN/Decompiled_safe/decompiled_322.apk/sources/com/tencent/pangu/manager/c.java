package com.tencent.pangu.manager;

import android.os.Message;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class c implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f3826a;

    c(b bVar) {
        this.f3826a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.b.a(com.tencent.pangu.manager.b, boolean):boolean
     arg types: [com.tencent.pangu.manager.b, int]
     candidates:
      com.tencent.pangu.manager.b.a(com.tencent.e.a.a.c, int):void
      com.tencent.pangu.manager.b.a(com.tencent.pangu.manager.b, java.util.List):void
      com.tencent.pangu.manager.b.a(com.tencent.pangu.manager.b, boolean):boolean */
    public void handleUIEvent(Message message) {
        switch (message.what) {
            case 1032:
                boolean unused = this.f3826a.g = true;
                return;
            case EventDispatcherEnum.UI_EVENT_APP_GOFRONT:
                if (this.f3826a.g) {
                    TemporaryThreadManager.get().start(new d(this));
                    boolean unused2 = this.f3826a.g = false;
                    return;
                }
                return;
            default:
                return;
        }
    }
}
