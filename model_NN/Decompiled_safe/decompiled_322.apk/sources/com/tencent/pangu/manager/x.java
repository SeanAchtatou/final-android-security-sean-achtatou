package com.tencent.pangu.manager;

import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.y;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.Banner;
import com.tencent.assistant.protocol.jce.EntranceTemplate;
import com.tencent.assistant.protocol.jce.TopBanner;
import com.tencent.assistant.smartcard.c.p;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.pangu.component.banner.f;
import com.tencent.pangu.component.banner.j;
import com.tencent.pangu.component.banner.n;
import com.tencent.pangu.model.b;
import com.tencent.pangu.model.g;
import com.tencent.pangu.module.a.h;
import com.tencent.pangu.module.ad;
import com.tencent.pangu.module.ar;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class x extends y<h> implements NetworkMonitor.ConnectivityChangeListener, UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private static x f3847a;
    /* access modifiers changed from: private */
    public ad b = new ad();
    private aa c = new aa(this, null);
    /* access modifiers changed from: private */
    public long d = 0;
    /* access modifiers changed from: private */
    public int e;
    private List<Long> f = new ArrayList();
    /* access modifiers changed from: private */
    public g g = null;
    private long h = 0;
    private APN i = APN.NO_NETWORK;
    private List<SimpleAppModel> j = new ArrayList();

    public static synchronized x a() {
        x xVar;
        synchronized (x.class) {
            if (f3847a == null) {
                f3847a = new x();
            }
            xVar = f3847a;
        }
        return xVar;
    }

    private x() {
        p.a();
        this.b.register(this.c);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_GOFRONT, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        t.a().a(this);
    }

    public void b() {
        long currentTimeMillis = System.currentTimeMillis();
        if (!this.b.d() && currentTimeMillis - this.h > 5000) {
            this.h = currentTimeMillis;
            TemporaryThreadManager.get().start(new y(this));
        }
    }

    public void a(boolean z, ar arVar) {
        if (!z) {
            this.b.b();
        } else {
            this.b.a(arVar);
        }
    }

    public int c() {
        return this.b.c();
    }

    public ar d() {
        return this.b.e();
    }

    public boolean e() {
        return this.b.e().c;
    }

    public List<b> f() {
        this.e = 0;
        return a(this.b.f(), this.b.g(), 0, this.b.a());
    }

    public boolean g() {
        ArrayList<SimpleAppModel> g2 = this.b.g();
        return g2 != null && g2.size() > 0;
    }

    public List<f> h() {
        return a(this.b.h());
    }

    public g i() {
        return this.g;
    }

    public EntranceTemplate j() {
        return this.b.k();
    }

    public long k() {
        return this.b.l();
    }

    /* access modifiers changed from: private */
    public void a(TopBanner topBanner, long j2) {
        if (topBanner != null) {
            this.g = new g();
            this.g.f3888a = topBanner.f1594a;
            this.g.i = topBanner.i;
            this.g.g = topBanner.g;
            this.g.f = topBanner.f;
            this.g.e = topBanner.e;
            this.g.h = topBanner.h;
            this.g.c = topBanner.c;
            this.g.j = topBanner.j;
            this.g.d = topBanner.d;
            this.g.b = topBanner.b;
            this.g.m = j2;
            this.g.k = com.tencent.pangu.component.topbanner.b.a().e(this.g);
        }
    }

    public com.tencent.assistant.model.b l() {
        return this.b.i();
    }

    public void onConnected(APN apn) {
        if (!this.b.d()) {
            this.b.b();
        } else if (this.i != APN.WIFI && apn == APN.WIFI && !c.l()) {
            this.b.b();
        }
    }

    public void onDisconnected(APN apn) {
        this.i = apn;
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.i = apn2;
        if (apn != APN.WIFI && apn2 == APN.WIFI && !c.l()) {
            this.b.b();
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                this.b.b();
                return;
            default:
                return;
        }
    }

    public void m() {
        if (this.d > 0 && System.currentTimeMillis() - this.d > m.a().ai()) {
            this.b.b();
        }
    }

    public long n() {
        return this.b.j();
    }

    /* access modifiers changed from: private */
    public List<f> a(List<Banner> list) {
        f jVar;
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(6);
        try {
            for (Banner next : list) {
                if (next != null) {
                    switch (next.a()) {
                        case 1:
                            jVar = new com.tencent.pangu.component.banner.h(next);
                            break;
                        case 2:
                            jVar = new n(next);
                            break;
                        case 3:
                            jVar = new j(next);
                            break;
                        default:
                            jVar = null;
                            break;
                    }
                    if (jVar != null) {
                        arrayList.add(jVar);
                    }
                }
            }
            return arrayList;
        } catch (Throwable th) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3, boolean z, ar arVar, boolean z2, List<f> list, g gVar, List<b> list2) {
        a(new z(this, i2, i3, z, arVar, z2, list, gVar, list2));
    }

    /* access modifiers changed from: private */
    public synchronized List<b> a(com.tencent.assistant.smartcard.b.c cVar, List<SimpleAppModel> list, int i2, int i3) {
        ArrayList arrayList;
        int i4;
        boolean z = true;
        synchronized (this) {
            StringBuilder append = new StringBuilder().append("processAppAndCardModelV2 begin- smartCardCacheBase != null: ");
            if (cVar == null) {
                z = false;
            }
            XLog.d("abc", append.append(z).append(", datas size: ").append(list != null ? Integer.valueOf(list.size()) : STConst.ST_STATUS_DEFAULT).append(", lastCardPosition: ").append(i2).append(", smartCardIndex: ").append(i3).toString());
            if (cVar == null || list == null || i2 >= i3) {
                arrayList = null;
            } else {
                if (i2 == 0) {
                    this.f.clear();
                    this.j.clear();
                }
                ArrayList arrayList2 = new ArrayList();
                for (Long next : this.f) {
                    for (SimpleAppModel next2 : list) {
                        if (next2.f938a == next.longValue()) {
                            arrayList2.add(next2);
                        }
                    }
                }
                ArrayList arrayList3 = new ArrayList(list);
                arrayList3.removeAll(arrayList2);
                if (arrayList3 == null || arrayList3.size() <= 0) {
                    arrayList = null;
                } else {
                    this.j.addAll(list);
                    ArrayList arrayList4 = new ArrayList();
                    ArrayList arrayList5 = new ArrayList();
                    int i5 = 0;
                    int i6 = i2;
                    while (i5 < this.j.size() && i6 < i3) {
                        b bVar = new b();
                        XLog.d("SmartCard", "start.....assemble position:" + i6);
                        List<com.tencent.assistant.smartcard.d.n> a2 = cVar.a(i6);
                        if (a2 == null || a2.size() <= 0) {
                            XLog.d("SmartCard", "finish..... position:" + i6 + ",null value.");
                        } else {
                            bVar.g = p.a().a(a2, this.f, i6);
                            if (bVar.g != null) {
                                p.a().a(bVar.g);
                                bVar.b = 2;
                                List<Long> d2 = bVar.g.d();
                                if (d2 != null) {
                                    this.f.addAll(d2);
                                }
                            }
                        }
                        if (bVar.g == null) {
                            do {
                                int i7 = i5;
                                SimpleAppModel simpleAppModel = this.j.get(i7);
                                arrayList4.add(simpleAppModel);
                                if (!this.f.contains(Long.valueOf(simpleAppModel.f938a))) {
                                    bVar.c = simpleAppModel;
                                    bVar.b = 1;
                                    this.f.add(Long.valueOf(simpleAppModel.f938a));
                                }
                                i5 = i7 + 1;
                                if (bVar.c != null) {
                                    break;
                                }
                            } while (i5 < this.j.size());
                        }
                        int i8 = i5;
                        if (bVar.c == null && bVar.g == null) {
                            i4 = i6;
                        } else {
                            i4 = i6 + 1;
                            arrayList5.add(bVar);
                        }
                        i6 = i4;
                        i5 = i8;
                    }
                    if (arrayList5 != null) {
                        this.e += arrayList5.size();
                    }
                    this.j.removeAll(arrayList4);
                    arrayList = arrayList5;
                }
            }
        }
        return arrayList;
    }
}
