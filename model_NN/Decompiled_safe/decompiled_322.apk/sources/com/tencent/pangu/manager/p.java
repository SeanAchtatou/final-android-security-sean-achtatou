package com.tencent.pangu.manager;

import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager;
import com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean;

/* compiled from: ProGuard */
class p implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f3839a;
    final /* synthetic */ o b;

    p(o oVar, DownloadInfo downloadInfo) {
        this.b = oVar;
        this.f3839a = downloadInfo;
    }

    public void run() {
        InstallUninstallTaskBean installUninstallTaskBean = new InstallUninstallTaskBean();
        if (this.f3839a != null) {
            installUninstallTaskBean.fileSize = this.f3839a.fileSize;
        }
        this.b.f3838a.k.a(InstallUninstallDialogManager.DIALOG_DEALWITH_TYPE.DOWNLOAD_SPACE_NOT_ENOUGH, installUninstallTaskBean);
    }
}
