package com.tencent.pangu.mediadownload;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.tencent.assistant.a.a;

/* compiled from: ProGuard */
public class p {

    /* renamed from: a  reason: collision with root package name */
    private String f3867a = null;
    private String b = "DF";
    private String c = null;
    private byte d = 1;

    public String a() {
        return this.f3867a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public static p a(Intent intent) {
        if (intent == null) {
            return null;
        }
        if ("android.intent.action.VIEW".equals(intent.getAction())) {
            Uri data = intent.getData();
            if (data == null) {
                return null;
            }
            p pVar = new p();
            pVar.f3867a = data.toString();
            return pVar;
        }
        Bundle extras = intent.getExtras();
        if (extras == null) {
            return null;
        }
        String string = extras.getString(a.K);
        String string2 = extras.getString(a.L);
        String string3 = extras.getString(a.M);
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        p pVar2 = new p();
        pVar2.f3867a = string;
        if (!TextUtils.isEmpty(string2)) {
            pVar2.b = string2;
        }
        pVar2.c = string3;
        pVar2.d = 2;
        return pVar2;
    }

    public byte d() {
        return this.d;
    }

    public String toString() {
        return "OutterCallDownloadInfo{uriFromOutCall='" + this.f3867a + '\'' + ", callFrom='" + this.b + '\'' + ", urlTicket='" + this.c + '\'' + '}';
    }
}
