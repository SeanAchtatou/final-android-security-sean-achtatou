package com.tencent.pangu.mediadownload;

import com.qq.AppService.AstApp;
import com.tencent.assistant.db.table.h;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class b {
    private static b d;

    /* renamed from: a  reason: collision with root package name */
    private EventDispatcher f3854a = AstApp.i().j();
    private h b = new h();
    private Map<String, c> c = new ConcurrentHashMap(5);

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            if (d == null) {
                d = new b();
            }
            bVar = d;
        }
        return bVar;
    }

    private b() {
        List<c> a2 = this.b.a();
        if (a2 != null && a2.size() > 0) {
            for (c next : a2) {
                this.c.put(next.c, next);
            }
        }
    }

    public List<c> b() {
        ArrayList arrayList = new ArrayList(this.c.size());
        for (Map.Entry<String, c> value : this.c.entrySet()) {
            arrayList.add(value.getValue());
        }
        return arrayList;
    }

    public c a(String str) {
        c remove = this.c.remove(str);
        this.f3854a.sendMessage(this.f3854a.obtainMessage(EventDispatcherEnum.UI_EVENT_QREADER_DELETE, remove));
        this.b.a(str);
        return remove;
    }

    public void a(c cVar) {
        if (cVar != null) {
            c cVar2 = this.c.get(cVar.c);
            if (cVar2 == null) {
                cVar.i = System.currentTimeMillis();
                cVar.j = System.currentTimeMillis();
                this.c.put(cVar.c, cVar);
                this.b.a(cVar);
                this.f3854a.sendMessage(this.f3854a.obtainMessage(EventDispatcherEnum.UI_EVENT_QREADER_ADD, cVar));
            } else if (cVar.g == 1) {
                if (cVar2 != cVar) {
                    cVar2.g = cVar.g;
                    cVar2.d = cVar.d;
                    cVar2.f = cVar.f;
                    cVar2.f3855a = cVar.f3855a;
                    cVar2.e = cVar.e;
                    cVar2.j = System.currentTimeMillis();
                    if (cVar.f == -1) {
                        cVar.h = 1;
                    }
                }
                this.b.a(cVar2);
                this.f3854a.sendMessage(this.f3854a.obtainMessage(EventDispatcherEnum.UI_EVENT_QREADER_UPDATE, cVar2));
            }
        }
    }
}
