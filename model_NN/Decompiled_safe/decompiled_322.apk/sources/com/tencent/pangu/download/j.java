package com.tencent.pangu.download;

import android.text.TextUtils;

/* compiled from: ProGuard */
class j {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3763a;
    private String b;
    private int c;
    private long d = System.currentTimeMillis();

    public j(a aVar, String str, int i) {
        this.f3763a = aVar;
        this.b = str;
        this.c = i;
    }

    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        j jVar = (j) obj;
        if (TextUtils.isEmpty(this.b) || TextUtils.isEmpty(jVar.b) || !this.b.equals(jVar.b) || this.c != jVar.c) {
            return false;
        }
        return true;
    }

    public long a() {
        return this.d;
    }
}
