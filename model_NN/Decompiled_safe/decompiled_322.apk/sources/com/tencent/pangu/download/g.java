package com.tencent.pangu.download;

import android.text.TextUtils;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f3760a;
    final /* synthetic */ a b;

    g(a aVar, String str) {
        this.b = aVar;
        this.f3760a = str;
    }

    public void run() {
        DownloadInfo d;
        if (!TextUtils.isEmpty(this.f3760a) && (d = DownloadProxy.a().d(this.f3760a)) != null) {
            boolean z = false;
            try {
                z = d.makeFinalFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (z) {
                DownloadProxy.a().d(d);
                a.d.sendMessage(a.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this.f3760a));
                return;
            }
            a.a().a(this.f3760a);
        }
    }
}
