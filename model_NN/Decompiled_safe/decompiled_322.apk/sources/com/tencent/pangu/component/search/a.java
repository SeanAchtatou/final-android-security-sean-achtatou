package com.tencent.pangu.component.search;

import android.text.TextUtils;
import android.view.View;

/* compiled from: ProGuard */
class a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NativeSearchResultPage f3699a;

    a(NativeSearchResultPage nativeSearchResultPage) {
        this.f3699a = nativeSearchResultPage;
    }

    public void onClick(View view) {
        this.f3699a.n();
        if (!TextUtils.isEmpty(this.f3699a.f3690a) && this.f3699a.r != null) {
            this.f3699a.r.b(this.f3699a.f3690a, this.f3699a.d);
        }
    }
}
