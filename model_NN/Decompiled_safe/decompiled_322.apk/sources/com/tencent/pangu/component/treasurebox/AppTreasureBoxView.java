package com.tencent.pangu.component.treasurebox;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.by;
import com.tencent.pangu.model.h;
import java.util.List;

/* compiled from: ProGuard */
public class AppTreasureBoxView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private boolean f3732a = true;

    public AppTreasureBoxView(Context context) {
        super(context);
        a(context);
    }

    public AppTreasureBoxView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        setOrientation(1);
    }

    public void a() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt instanceof AppTreasureBoxCell1) {
                ((AppTreasureBoxCell1) childAt).b();
            }
        }
    }

    public void b() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt instanceof AppTreasureBoxCell1) {
                ((AppTreasureBoxCell1) childAt).a();
            }
        }
    }

    public void c() {
        d();
    }

    public void a(List<h> list, a aVar) {
        if (list == null || list.isEmpty()) {
            removeAllViews();
            addView(AppTreasureBoxCell.a(false, null, getContext(), aVar, 0));
            this.f3732a = false;
            return;
        }
        this.f3732a = true;
        int size = list.size();
        removeAllViews();
        for (int i = 0; i < size; i++) {
            AppTreasureBoxCell1 appTreasureBoxCell1 = (AppTreasureBoxCell1) AppTreasureBoxCell.a(true, list.get(i), getContext(), aVar, i);
            if (i == size - 1) {
                if (size == 1) {
                    setPadding(0, 0, 0, by.a(getContext(), 57.0f));
                    appTreasureBoxCell1.a(true);
                } else {
                    setPadding(0, 0, 0, by.a(getContext(), 0.0f));
                    appTreasureBoxCell1.a(false);
                }
            }
            addView(appTreasureBoxCell1);
        }
        m.a().O();
    }

    public void d() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt instanceof AppTreasureBoxCell1) {
                AppTreasureBoxCell1 appTreasureBoxCell1 = (AppTreasureBoxCell1) childAt;
                appTreasureBoxCell1.a();
                removeView(appTreasureBoxCell1);
            }
        }
    }
}
