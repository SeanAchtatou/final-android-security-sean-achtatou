package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.AppTagInfo;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.am;
import com.tencent.nucleus.socialcontact.tagpage.a;

/* compiled from: ProGuard */
public class DetailTagItemView extends RelativeLayout implements TXImageView.ITXImageViewListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3544a;
    private TextView b;
    private TXImageView c;

    public DetailTagItemView(Context context) {
        super(context);
        a(context);
    }

    public DetailTagItemView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public DetailTagItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        this.f3544a = context;
        setGravity(17);
        LayoutInflater.from(this.f3544a).inflate((int) R.layout.app_detail_tag_item_view, this);
        this.c = (TXImageView) findViewById(R.id.tag_image);
        this.b = (TextView) findViewById(R.id.tag_name);
    }

    public DetailTagItemView a(AppTagInfo appTagInfo, long j, String str, int i) {
        if (appTagInfo == null) {
            return null;
        }
        this.c.updateImageView(appTagInfo.g, R.drawable.appdetail_tag_default, TXImageView.TXImageViewType.ROUND_IMAGE);
        this.c.setListener(this);
        this.b.setText(appTagInfo.b);
        if (!TextUtils.isEmpty(appTagInfo.b)) {
            if (appTagInfo.b.length() > 4) {
                this.b.setTextSize(12.0f);
            } else {
                this.b.setTextSize(13.0f);
            }
        }
        setOnClickListener(new aq(this, appTagInfo, j, str, i));
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.appdetail.DetailTagItemView.a(android.graphics.Bitmap, int, int, boolean):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, ?, ?, int]
     candidates:
      com.tencent.pangu.component.appdetail.DetailTagItemView.a(com.tencent.assistant.protocol.jce.AppTagInfo, long, java.lang.String, int):com.tencent.pangu.component.appdetail.DetailTagItemView
      com.tencent.pangu.component.appdetail.DetailTagItemView.a(android.graphics.Bitmap, int, int, boolean):android.graphics.Bitmap */
    public void onTXImageViewLoadImageFinish(TXImageView tXImageView, Bitmap bitmap) {
        if (bitmap != null && tXImageView != null) {
            Bitmap a2 = a.a(bitmap, bitmap.getWidth(), bitmap.getHeight());
            Bitmap a3 = a(a2, (int) STConstAction.ACTION_HIT_OPEN, (int) STConstAction.ACTION_HIT_OPEN, true);
            if (a3 == null) {
                a3 = a2;
            }
            try {
                this.c.setImageDrawable(new BitmapDrawable(am.a(a3, tXImageView.getWidth(), tXImageView.getHeight(), Bitmap.Config.ARGB_4444)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static Bitmap a(Bitmap bitmap, int i, int i2, boolean z) {
        if (bitmap != null) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            if (width > i && height > i2) {
                try {
                    Bitmap createBitmap = Bitmap.createBitmap(bitmap, (width - i) / 2, (height - i2) / 2, i, i);
                    if (z && bitmap != null && !bitmap.equals(createBitmap) && !bitmap.isRecycled()) {
                        bitmap.recycle();
                        return createBitmap;
                    }
                } catch (Throwable th) {
                }
            }
        }
        return null;
    }
}
