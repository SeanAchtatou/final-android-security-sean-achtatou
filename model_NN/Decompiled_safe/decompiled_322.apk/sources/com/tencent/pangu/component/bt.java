package com.tencent.pangu.component;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.pangu.mediadownload.q;
import com.tencent.pangu.mediadownload.r;

/* compiled from: ProGuard */
class bt extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f3660a;
    final /* synthetic */ VideoDownloadButton b;

    bt(VideoDownloadButton videoDownloadButton, q qVar) {
        this.b = videoDownloadButton;
        this.f3660a = qVar;
    }

    public void onLeftBtnClick() {
    }

    public void onRightBtnClick() {
        int c = r.c().c(this.f3660a);
        if (c != 0) {
            DialogUtils.show2BtnDialog(new bw(this.b.getContext(), this.f3660a, c, new bu(this)));
            return;
        }
        r.c().b(this.f3660a.m);
        r.c().a(this.f3660a);
    }

    public void onCancell() {
    }
}
