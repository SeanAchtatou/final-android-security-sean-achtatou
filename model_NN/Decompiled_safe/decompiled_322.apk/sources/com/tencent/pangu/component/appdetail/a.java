package com.tencent.pangu.component.appdetail;

import android.view.View;

/* compiled from: ProGuard */
class a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBarTabView f3560a;

    a(AppBarTabView appBarTabView) {
        this.f3560a = appBarTabView;
    }

    public void onClick(View view) {
        if (this.f3560a.b != null) {
            this.f3560a.b.setVisibility(0);
            this.f3560a.b.reload();
        }
        this.f3560a.g.setVisibility(4);
    }
}
