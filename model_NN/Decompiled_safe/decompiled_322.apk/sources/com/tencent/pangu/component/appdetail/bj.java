package com.tencent.pangu.component.appdetail;

import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.RecommendAppInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class bj extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    int f3587a = this.b;
    final /* synthetic */ int b;
    final /* synthetic */ RecommendAppViewV5 c;
    private byte d = 0;

    bj(RecommendAppViewV5 recommendAppViewV5, int i) {
        this.c = recommendAppViewV5;
        this.b = i;
    }

    public void onTMAClick(View view) {
        SimpleAppModel simpleAppModel = (SimpleAppModel) this.c.o.get(this.f3587a);
        if (simpleAppModel == null || simpleAppModel.b <= 0 || TextUtils.isEmpty(simpleAppModel.i)) {
            SimpleAppModel a2 = this.c.a((RecommendAppInfo) this.c.m.get(this.f3587a));
            if (a2 != null) {
                this.c.k[this.f3587a].setTag(Integer.valueOf(this.c.v.a(a2, this.d).b));
                this.c.k[this.f3587a].a();
                return;
            }
            return;
        }
        this.c.a((SimpleAppModel) this.c.o.get(this.f3587a), view, this.f3587a);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c.c, (SimpleAppModel) this.c.o.get(this.f3587a), this.c.c(this.f3587a), 200, a.a(k.d((SimpleAppModel) this.c.o.get(this.f3587a)), (SimpleAppModel) this.c.o.get(this.f3587a)));
        buildSTInfo.actionId = a.a(k.d((SimpleAppModel) this.c.o.get(this.f3587a)));
        buildSTInfo.recommendId = ((RecommendAppInfo) this.c.m.get(this.f3587a)).g;
        buildSTInfo.isImmediately = ((RecommendAppInfo) this.c.m.get(this.f3587a)).n == 1;
        return buildSTInfo;
    }
}
