package com.tencent.pangu.component.appdetail;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class au extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentTagInfo f3579a;
    final /* synthetic */ FriendTalkView b;

    au(FriendTalkView friendTalkView, CommentTagInfo commentTagInfo) {
        this.b = friendTalkView;
        this.f3579a = commentTagInfo;
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b.d, 200);
        buildSTInfo.scene = STConst.ST_PAGE_APP_DETAIL;
        String str = STConst.ST_DEFAULT_SLOT;
        if (view.getTag(R.id.tma_st_slot_tag) instanceof String) {
            str = this.b.c + "_" + ((String) view.getTag(R.id.tma_st_slot_tag));
        }
        buildSTInfo.slotId = str;
        return buildSTInfo;
    }

    public void onTMAClick(View view) {
        int childCount = this.b.j.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.b.j.getChildAt(i);
            Drawable drawable = (Drawable) childAt.getTag();
            if (drawable != null) {
                childAt.setBackgroundDrawable(drawable);
            }
        }
        if (this.b.k != null) {
            this.b.k.a(view, this.f3579a);
        }
    }
}
