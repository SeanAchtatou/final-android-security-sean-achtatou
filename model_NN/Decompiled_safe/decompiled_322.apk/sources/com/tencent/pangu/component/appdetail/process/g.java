package com.tencent.pangu.component.appdetail.process;

import android.os.Bundle;
import com.tencent.assistant.AppConst;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.nucleus.socialcontact.login.l;

/* compiled from: ProGuard */
class g extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f3609a;

    g(f fVar) {
        this.f3609a = fVar;
    }

    public void onLeftBtnClick() {
    }

    public void onRightBtnClick() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
        bundle.putInt(AppConst.KEY_FROM_TYPE, 12);
        l.a(12);
        j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
    }

    public void onCancell() {
    }
}
