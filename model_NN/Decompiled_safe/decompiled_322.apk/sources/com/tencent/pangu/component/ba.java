package com.tencent.pangu.component;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.text.TextUtils;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
public class ba extends ShapeDrawable {
    private static float b = by.a(2.0f);
    private static float[] c = {b, b, b, b, b, b, b, b};

    /* renamed from: a  reason: collision with root package name */
    private Context f3636a;

    public ba(Context context) {
        super(new RoundRectShape(c, null, null));
        this.f3636a = context;
    }

    public void a(float f) {
        float a2 = by.a(f);
        setShape(new RoundRectShape(new float[]{a2, a2, a2, a2, a2, a2, a2, a2}, null, null));
    }

    public void a(int i) {
        getPaint().setColor(this.f3636a.getResources().getColor(i));
    }

    public void b(int i) {
        getPaint().setColor(i);
    }

    public void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            getPaint().setColor(Color.parseColor(str));
        }
    }
}
