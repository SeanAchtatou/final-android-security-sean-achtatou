package com.tencent.pangu.component.appdetail;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;

/* compiled from: ProGuard */
class bi extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RecommendAppViewV5 f3586a;

    bi(RecommendAppViewV5 recommendAppViewV5) {
        this.f3586a = recommendAppViewV5;
    }

    public void onTMAClick(View view) {
        this.f3586a.a(this.f3586a.d(view.getId()));
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x00af A[Catch:{ Exception -> 0x00bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00bb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.assistantv2.st.page.STInfoV2 getStInfo() {
        /*
            r8 = this;
            r2 = 1
            com.tencent.pangu.component.appdetail.RecommendAppViewV5 r0 = r8.f3586a
            int r1 = r8.clickViewId
            android.view.View r0 = r0.findViewById(r1)
            r1 = 2131165262(0x7f07004e, float:1.7944736E38)
            java.lang.Object r0 = r0.getTag(r1)
            java.lang.String r3 = r0.toString()
            com.tencent.pangu.component.appdetail.RecommendAppViewV5 r0 = r8.f3586a
            int r1 = r8.clickViewId
            android.view.View r0 = r0.findViewById(r1)
            r1 = 2131165263(0x7f07004f, float:1.7944738E38)
            java.lang.Object r0 = r0.getTag(r1)
            java.lang.String r0 = r0.toString()
            int r4 = java.lang.Integer.parseInt(r0)
            r1 = 0
            com.tencent.pangu.component.appdetail.RecommendAppViewV5 r0 = r8.f3586a     // Catch:{ Exception -> 0x00b3 }
            java.util.List r0 = r0.o     // Catch:{ Exception -> 0x00b3 }
            if (r0 == 0) goto L_0x004d
            com.tencent.pangu.component.appdetail.RecommendAppViewV5 r0 = r8.f3586a     // Catch:{ Exception -> 0x00b3 }
            java.util.List r0 = r0.o     // Catch:{ Exception -> 0x00b3 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x00b3 }
            if (r0 <= 0) goto L_0x004d
            com.tencent.pangu.component.appdetail.RecommendAppViewV5 r0 = r8.f3586a     // Catch:{ Exception -> 0x00b3 }
            java.util.List r0 = r0.o     // Catch:{ Exception -> 0x00b3 }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ Exception -> 0x00b3 }
            com.tencent.assistant.model.SimpleAppModel r0 = (com.tencent.assistant.model.SimpleAppModel) r0     // Catch:{ Exception -> 0x00b3 }
            r1 = r0
        L_0x004d:
            if (r1 != 0) goto L_0x00c7
            com.tencent.pangu.component.appdetail.RecommendAppViewV5 r5 = r8.f3586a     // Catch:{ Exception -> 0x00c2 }
            com.tencent.pangu.component.appdetail.RecommendAppViewV5 r0 = r8.f3586a     // Catch:{ Exception -> 0x00c2 }
            java.util.List r0 = r0.m     // Catch:{ Exception -> 0x00c2 }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ Exception -> 0x00c2 }
            com.tencent.assistant.protocol.jce.RecommendAppInfo r0 = (com.tencent.assistant.protocol.jce.RecommendAppInfo) r0     // Catch:{ Exception -> 0x00c2 }
            com.tencent.assistant.model.SimpleAppModel r0 = r5.a(r0)     // Catch:{ Exception -> 0x00c2 }
        L_0x0061:
            com.tencent.pangu.component.appdetail.RecommendAppViewV5 r1 = r8.f3586a
            android.content.Context r1 = r1.c
            r5 = 200(0xc8, float:2.8E-43)
            java.lang.String r6 = "01"
            com.tencent.assistantv2.st.page.STInfoV2 r1 = com.tencent.assistantv2.st.page.STInfoBuilder.buildSTInfo(r1, r0, r3, r5, r6)
            com.tencent.pangu.component.appdetail.RecommendAppViewV5 r0 = r8.f3586a     // Catch:{ Exception -> 0x00bd }
            java.util.List r0 = r0.m     // Catch:{ Exception -> 0x00bd }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ Exception -> 0x00bd }
            com.tencent.assistant.protocol.jce.RecommendAppInfo r0 = (com.tencent.assistant.protocol.jce.RecommendAppInfo) r0     // Catch:{ Exception -> 0x00bd }
            long r5 = r0.f1454a     // Catch:{ Exception -> 0x00bd }
            r1.appId = r5     // Catch:{ Exception -> 0x00bd }
            com.tencent.pangu.component.appdetail.RecommendAppViewV5 r0 = r8.f3586a     // Catch:{ Exception -> 0x00bd }
            java.util.List r0 = r0.m     // Catch:{ Exception -> 0x00bd }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ Exception -> 0x00bd }
            com.tencent.assistant.protocol.jce.RecommendAppInfo r0 = (com.tencent.assistant.protocol.jce.RecommendAppInfo) r0     // Catch:{ Exception -> 0x00bd }
            java.lang.String r0 = r0.b     // Catch:{ Exception -> 0x00bd }
            r1.packageName = r0     // Catch:{ Exception -> 0x00bd }
            com.tencent.pangu.component.appdetail.RecommendAppViewV5 r0 = r8.f3586a     // Catch:{ Exception -> 0x00bd }
            java.util.List r0 = r0.m     // Catch:{ Exception -> 0x00bd }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ Exception -> 0x00bd }
            com.tencent.assistant.protocol.jce.RecommendAppInfo r0 = (com.tencent.assistant.protocol.jce.RecommendAppInfo) r0     // Catch:{ Exception -> 0x00bd }
            byte[] r0 = r0.g     // Catch:{ Exception -> 0x00bd }
            r1.recommendId = r0     // Catch:{ Exception -> 0x00bd }
            com.tencent.pangu.component.appdetail.RecommendAppViewV5 r0 = r8.f3586a     // Catch:{ Exception -> 0x00bd }
            java.util.List r0 = r0.m     // Catch:{ Exception -> 0x00bd }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ Exception -> 0x00bd }
            com.tencent.assistant.protocol.jce.RecommendAppInfo r0 = (com.tencent.assistant.protocol.jce.RecommendAppInfo) r0     // Catch:{ Exception -> 0x00bd }
            byte r0 = r0.n     // Catch:{ Exception -> 0x00bd }
            if (r0 != r2) goto L_0x00bb
            r0 = r2
        L_0x00b0:
            r1.isImmediately = r0     // Catch:{ Exception -> 0x00bd }
        L_0x00b2:
            return r1
        L_0x00b3:
            r0 = move-exception
            r7 = r0
            r0 = r1
            r1 = r7
        L_0x00b7:
            r1.printStackTrace()
            goto L_0x0061
        L_0x00bb:
            r0 = 0
            goto L_0x00b0
        L_0x00bd:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00b2
        L_0x00c2:
            r0 = move-exception
            r7 = r0
            r0 = r1
            r1 = r7
            goto L_0x00b7
        L_0x00c7:
            r0 = r1
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.component.appdetail.bi.getStInfo():com.tencent.assistantv2.st.page.STInfoV2");
    }
}
