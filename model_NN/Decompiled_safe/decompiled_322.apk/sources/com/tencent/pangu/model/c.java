package com.tencent.pangu.model;

import android.text.TextUtils;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private List<DownloadInfo> f3887a = new ArrayList();
    private ConcurrentHashMap<String, DownloadInfo> b = new ConcurrentHashMap<>();
    private Object c = new Object();

    public DownloadInfo a(String str, DownloadInfo downloadInfo) {
        DownloadInfo downloadInfo2;
        synchronized (this.c) {
            if (downloadInfo != null) {
                if (!TextUtils.isEmpty(str)) {
                    downloadInfo2 = this.b.put(str, downloadInfo);
                    if (downloadInfo.fileType == SimpleDownloadInfo.DownloadType.APK) {
                        if (downloadInfo2 != null) {
                            this.f3887a.remove(downloadInfo2);
                        }
                        this.f3887a.add(downloadInfo);
                    }
                }
            }
            downloadInfo2 = null;
        }
        return downloadInfo2;
    }

    public DownloadInfo a(Object obj) {
        DownloadInfo remove;
        synchronized (this.c) {
            remove = this.b.remove(obj);
            if (remove != null && remove.fileType == SimpleDownloadInfo.DownloadType.APK) {
                this.f3887a.remove(remove);
            }
        }
        return remove;
    }

    public boolean a(String str) {
        boolean containsKey;
        synchronized (this.c) {
            containsKey = this.b.containsKey(str);
        }
        return containsKey;
    }

    public DownloadInfo b(String str) {
        DownloadInfo downloadInfo;
        synchronized (this.c) {
            downloadInfo = this.b.get(str);
        }
        return downloadInfo;
    }

    public Set<String> a() {
        Set<String> keySet;
        synchronized (this.c) {
            keySet = this.b.keySet();
        }
        return keySet;
    }

    public Set<Map.Entry<String, DownloadInfo>> b() {
        Set<Map.Entry<String, DownloadInfo>> entrySet;
        synchronized (this.c) {
            entrySet = this.b.entrySet();
        }
        return entrySet;
    }

    public List<DownloadInfo> c() {
        List<DownloadInfo> list;
        synchronized (this.c) {
            list = this.f3887a;
        }
        return list;
    }
}
