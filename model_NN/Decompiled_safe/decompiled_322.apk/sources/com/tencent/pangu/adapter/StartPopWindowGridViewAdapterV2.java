package com.tencent.pangu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class StartPopWindowGridViewAdapterV2 extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f3408a = null;
    private LayoutInflater b = null;
    private ArrayList<SimpleAppModel> c = new ArrayList<>();
    private ArrayList<Boolean> d = new ArrayList<>();
    private int e;
    private long f;
    private int g = 0;
    private int h = 0;

    public StartPopWindowGridViewAdapterV2(Context context, int i) {
        this.f3408a = context;
        this.b = LayoutInflater.from(context);
        this.h = i;
    }

    public void a(int i) {
        this.g = i;
    }

    public int a() {
        return this.g;
    }

    public void a(List<SimpleAppModel> list, int i, boolean z) {
        int i2;
        int i3 = 9;
        this.c.clear();
        this.c.addAll(list);
        this.d.clear();
        int size = list.size();
        if (size > 9) {
            i2 = 9;
        } else {
            i2 = size;
        }
        for (int i4 = 0; i4 < i2; i4++) {
            this.d.add(Boolean.valueOf(z));
            this.f = list.get(i4).k;
        }
        if (z) {
            if (i <= 9) {
                i3 = i;
            }
            this.e = i3;
        }
        notifyDataSetChanged();
    }

    public int getCount() {
        int size = this.c.size();
        if (size > 9) {
            return 9;
        }
        return size;
    }

    public Object getItem(int i) {
        return this.c.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        bo boVar;
        bo boVar2;
        if (this.g == 1) {
            if (view == null) {
                bo boVar3 = new bo(this);
                view = this.b.inflate((int) R.layout.popwindow_newuser_recommend_item_v2, (ViewGroup) null);
                boVar3.f3442a = (TXImageView) view.findViewById(R.id.icon);
                boVar3.b = (TextView) view.findViewById(R.id.name);
                boVar3.c = (TextView) view.findViewById(R.id.description);
                boVar3.d = (ImageView) view.findViewById(R.id.check_box);
                view.setTag(boVar3);
                boVar2 = boVar3;
            } else {
                boVar2 = (bo) view.getTag();
            }
            view.setTag(R.id.tma_st_slot_tag, d(i));
            a(boVar2, i);
        } else {
            if (view == null) {
                bo boVar4 = new bo(this);
                view = this.b.inflate((int) R.layout.popwindow_newuser_recommend_item_v3, (ViewGroup) null);
                boVar4.f3442a = (TXImageView) view.findViewById(R.id.icon);
                boVar4.b = (TextView) view.findViewById(R.id.name);
                boVar4.d = (ImageView) view.findViewById(R.id.check_box);
                view.setTag(boVar4);
                boVar = boVar4;
            } else {
                boVar = (bo) view.getTag();
            }
            view.setTag(R.id.tma_st_slot_tag, d(i));
            a(boVar, i);
        }
        return view;
    }

    private String d(int i) {
        return "03_" + bm.a(i + 1);
    }

    private void a(bo boVar, int i) {
        SimpleAppModel simpleAppModel = this.c.get(i);
        boVar.f3442a.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        boVar.b.setText(simpleAppModel.d);
        if (this.g != 0) {
            boVar.c.setText(simpleAppModel.X);
        }
        boVar.d.setSelected(this.d.get(i).booleanValue());
    }

    public boolean b(int i) {
        return this.d.get(i).booleanValue();
    }

    public void c(int i) {
        SimpleAppModel simpleAppModel = this.c.get(i);
        boolean booleanValue = this.d.get(i).booleanValue();
        this.d.set(i, Boolean.valueOf(!booleanValue));
        if (booleanValue) {
            this.e--;
            this.f -= this.c.get(i).k;
        } else {
            this.e++;
            this.f += this.c.get(i).k;
        }
        notifyDataSetChanged();
        XLog.i("PopWindowReprot", " item position:" + i);
        STInfoV2 e2 = e(200);
        if (e2 != null) {
            e2.extraData = simpleAppModel != null ? simpleAppModel.c + "|" + simpleAppModel.g : Constants.STR_EMPTY;
            e2.slotId = d(i);
            e2.updateWithSimpleAppModel(simpleAppModel);
            e2.status = !booleanValue ? "01" : "02";
            l.a(e2);
        }
    }

    public int b() {
        return this.e;
    }

    public long c() {
        long j = 0;
        int size = this.d.size();
        if (size > 9) {
            size = 9;
        }
        for (int i = 0; i < size; i++) {
            if (this.d.get(i).booleanValue()) {
                j += this.c.get(i).k;
            }
        }
        return j;
    }

    public ArrayList<Boolean> d() {
        return this.d;
    }

    private STInfoV2 e(int i) {
        return new STInfoV2(f(), STConst.ST_DEFAULT_SLOT, 2000, STConst.ST_DEFAULT_SLOT, i);
    }

    private int f() {
        return STConst.ST_PAGE_POP_UP_NECESSRAY_CONTENT_VEIW + this.h;
    }

    public void e() {
        for (int i = 0; i < getCount(); i++) {
            STInfoV2 e2 = e(100);
            if (e2 != null) {
                e2.extraData = this.c.get(i) != null ? this.c.get(i).c + "|" + this.c.get(i).g : Constants.STR_EMPTY;
                e2.slotId = d(i);
                e2.updateWithSimpleAppModel(this.c.get(i));
                e2.isImmediately = false;
                l.a(e2);
            }
        }
    }
}
