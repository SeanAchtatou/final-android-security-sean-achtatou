package com.tencent.pangu.adapter;

import android.widget.ImageView;
import com.tencent.assistantv2.component.k;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;

/* compiled from: ProGuard */
class aq extends k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ al f3419a;

    aq(al alVar) {
        this.f3419a = alVar;
    }

    public void a(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            a.a().a(downloadInfo);
            com.tencent.assistant.utils.a.a((ImageView) this.f3419a.j.findViewWithTag(downloadInfo.downloadTicket));
        }
    }
}
