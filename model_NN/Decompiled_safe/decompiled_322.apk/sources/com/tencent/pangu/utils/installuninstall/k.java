package com.tencent.pangu.utils.installuninstall;

import com.qq.AppService.AstApp;
import com.tencent.assistant.st.STConstAction;

/* compiled from: ProGuard */
class k extends o {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstallUninstallDialogManager f4006a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    k(InstallUninstallDialogManager installUninstallDialogManager) {
        super(installUninstallDialogManager);
        this.f4006a = installUninstallDialogManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onLeftBtnClick() {
        this.f4006a.a(this.pageId, AstApp.m() != null ? AstApp.m().f() : 2000, "04_001", STConstAction.ACTION_HIT_SEARCH_CANCEL);
        boolean unused = this.f4006a.c = false;
        this.f4006a.c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onRightBtnClick() {
        this.f4006a.a(this.pageId, AstApp.m() != null ? AstApp.m().f() : 2000, "03_001", 200);
        boolean unused = this.f4006a.c = true;
        this.f4006a.c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onCancell() {
        this.f4006a.a(this.pageId, AstApp.m() != null ? AstApp.m().f() : 2000, "05_001", 200);
        boolean unused = this.f4006a.c = false;
        this.f4006a.c();
    }
}
