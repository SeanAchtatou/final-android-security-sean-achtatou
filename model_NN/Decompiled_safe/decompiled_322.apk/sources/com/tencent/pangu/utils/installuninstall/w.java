package com.tencent.pangu.utils.installuninstall;

import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.root.e;

/* compiled from: ProGuard */
class w implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f4015a;

    w(v vVar) {
        this.f4015a = vVar;
    }

    public void run() {
        boolean a2 = InstallUninstallUtil.a();
        boolean c = e.a().c();
        XLog.d("InstallUninstallHelper", "handle root switch, permRootReqResult=" + a2 + ", tempRootReqResult=" + c);
        if (a2 || c) {
            m.a().b(true);
            this.f4015a.f4014a.style = c ? 2 : 1;
            if (this.f4015a.f4014a.action == 1) {
                ac.a().b(this.f4015a.f4014a);
            } else if (this.f4015a.f4014a.action == -1) {
                TemporaryThreadManager.get().start(new x(this));
            }
        } else {
            Toast.makeText(AstApp.i().getBaseContext(), AstApp.i().getBaseContext().getString(R.string.toast_root_request_fail), 1).show();
            if (this.f4015a.f4014a.action == 1) {
                ac.a().a(this.f4015a.f4014a);
            } else if (this.f4015a.f4014a.action == -1 && this.f4015a.f4014a.trySystemAfterSilentFail) {
                this.f4015a.b.f4009a.sendMessage(this.f4015a.b.f4009a.obtainMessage(1024, this.f4015a.f4014a.packageName));
                InstallUninstallUtil.a(this.f4015a.f4014a.packageName);
            }
        }
    }
}
