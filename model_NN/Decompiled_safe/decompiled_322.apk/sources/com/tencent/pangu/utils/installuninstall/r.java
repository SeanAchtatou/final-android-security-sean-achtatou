package com.tencent.pangu.utils.installuninstall;

import com.tencent.pangu.download.DownloadInfo;
import java.util.ArrayList;

/* compiled from: ProGuard */
class r implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ArrayList f4011a;
    final /* synthetic */ boolean b;
    final /* synthetic */ p c;

    r(p pVar, ArrayList arrayList, boolean z) {
        this.c = pVar;
        this.f4011a = arrayList;
        this.b = z;
    }

    public void run() {
        if (this.f4011a != null && !this.f4011a.isEmpty()) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.f4011a.size()) {
                    DownloadInfo downloadInfo = (DownloadInfo) this.f4011a.get(i2);
                    if (downloadInfo != null) {
                        this.c.a(downloadInfo.downloadTicket, downloadInfo.packageName, downloadInfo.name, downloadInfo.getFilePath(), downloadInfo.versionCode, downloadInfo.signatrue, downloadInfo.downloadTicket, downloadInfo.fileSize, this.b, downloadInfo);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
