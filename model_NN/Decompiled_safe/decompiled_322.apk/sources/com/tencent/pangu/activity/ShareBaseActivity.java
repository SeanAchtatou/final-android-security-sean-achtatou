package com.tencent.pangu.activity;

import android.os.Bundle;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.pangu.c.e;

/* compiled from: ProGuard */
public class ShareBaseActivity extends BaseActivity {
    private static e n;

    public e b(int i) {
        if (n == null) {
            n = new e(this, i);
        } else {
            n.a(i);
        }
        return n;
    }

    public e E() {
        return b(f());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        E();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (n != null) {
            n.a();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (n != null) {
            n.a(f());
            n.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (n != null) {
            n.c();
            n = null;
        }
    }
}
