package com.tencent.pangu.activity;

import android.view.View;
import android.view.ViewTreeObserver;

/* compiled from: ProGuard */
class ce implements ViewTreeObserver.OnGlobalLayoutListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f3356a;
    final /* synthetic */ SearchActivity b;

    ce(SearchActivity searchActivity, View view) {
        this.b = searchActivity;
        this.f3356a = view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.activity.SearchActivity.a(com.tencent.pangu.activity.SearchActivity, boolean):boolean
     arg types: [com.tencent.pangu.activity.SearchActivity, int]
     candidates:
      com.tencent.pangu.activity.SearchActivity.a(com.tencent.pangu.activity.SearchActivity, int):int
      com.tencent.pangu.activity.SearchActivity.a(com.tencent.pangu.activity.SearchActivity, java.lang.String):java.lang.String
      com.tencent.pangu.activity.SearchActivity.a(com.tencent.pangu.activity.SearchActivity, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.activity.SearchActivity.a(com.tencent.pangu.activity.SearchActivity, com.tencent.assistant.protocol.jce.AdvancedHotWord):void
      com.tencent.pangu.activity.SearchActivity.a(com.tencent.pangu.component.search.SearchResultTabPagesBase$TabType, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.activity.SearchActivity.a(java.lang.String, int):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.activity.SearchActivity.a(com.tencent.pangu.activity.SearchActivity, boolean):boolean */
    public void onGlobalLayout() {
        int height = this.f3356a.getRootView().getHeight() - this.f3356a.getHeight();
        if (this.b.U != height) {
            int unused = this.b.U = height;
            if (height > 100) {
                if (!this.b.T) {
                    boolean unused2 = this.b.T = true;
                    this.b.a("002", 200);
                }
            } else if (this.b.T) {
                boolean unused3 = this.b.T = false;
            }
        }
    }
}
