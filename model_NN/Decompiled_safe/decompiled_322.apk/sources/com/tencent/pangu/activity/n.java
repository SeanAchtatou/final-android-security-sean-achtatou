package com.tencent.pangu.activity;

import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.appdetail.AppDetailViewV5;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class n extends ViewPageScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f3388a;

    n(AppDetailActivityV5 appDetailActivityV5) {
        this.f3388a = appDetailActivityV5;
    }

    public void onPageSelected(int i) {
        this.f3388a.P.selectTab(i, true);
        int unused = this.f3388a.R = i;
        this.f3388a.c(i + 1);
        if (!this.f3388a.N && this.f3388a.R == 1) {
            this.f3388a.M();
        }
        if (this.f3388a.R == 0) {
            this.f3388a.bb.sendEmptyMessage(3);
        }
        if (this.f3388a.R == 2) {
            if (this.f3388a.M != null) {
                this.f3388a.M.a();
            }
            if (!(this.f3388a.n == null || this.f3388a.ae == null || this.f3388a.ae.f941a == null)) {
                this.f3388a.n.a(this.f3388a.u);
                if (this.f3388a.n.a(this.f3388a.ae.f941a.g)) {
                }
            }
        }
        this.f3388a.b(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.activity.AppDetailActivityV5.a(com.tencent.pangu.activity.AppDetailActivityV5, boolean):boolean
     arg types: [com.tencent.pangu.activity.AppDetailActivityV5, int]
     candidates:
      com.tencent.pangu.activity.AppDetailActivityV5.a(com.tencent.pangu.activity.AppDetailActivityV5, int):int
      com.tencent.pangu.activity.AppDetailActivityV5.a(com.tencent.pangu.activity.AppDetailActivityV5, com.tencent.assistant.localres.model.LocalApkInfo):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.pangu.activity.AppDetailActivityV5.a(com.tencent.pangu.activity.AppDetailActivityV5, com.tencent.assistant.model.c):com.tencent.assistant.model.c
      com.tencent.pangu.activity.AppDetailActivityV5.a(com.tencent.assistant.model.c, com.tencent.assistant.model.SimpleAppModel):com.tencent.pangu.model.ShareAppModel
      com.tencent.pangu.activity.AppDetailActivityV5.a(int, com.tencent.assistant.protocol.jce.GetRecommendAppListResponse):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.activity.AppDetailActivityV5.a(com.tencent.pangu.activity.AppDetailActivityV5, boolean):boolean */
    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        String str;
        super.handleMessage(viewInvalidateMessage);
        switch (viewInvalidateMessage.what) {
            case 1:
                if (!this.f3388a.aW) {
                    this.f3388a.v[1] = viewInvalidateMessage;
                    return;
                }
                this.f3388a.f(viewInvalidateMessage.arg1);
                this.f3388a.v[1] = null;
                return;
            case 2:
                if (!this.f3388a.aW) {
                    this.f3388a.v[2] = viewInvalidateMessage;
                    return;
                }
                int i = viewInvalidateMessage.arg1;
                HashMap hashMap = (HashMap) viewInvalidateMessage.params;
                boolean booleanValue = ((Boolean) hashMap.get("hasNext")).booleanValue();
                byte[] bArr = (byte[]) hashMap.get("pageContext");
                if (i == 0 && (this.f3388a.aV & 2) == 0) {
                    this.f3388a.a(i, (List) hashMap.get("authorModel"), booleanValue, bArr);
                } else {
                    this.f3388a.a(i, (List) null, booleanValue, (byte[]) null);
                }
                this.f3388a.v[2] = null;
                return;
            case 3:
                if (this.f3388a.I != null && this.f3388a.ar) {
                    this.f3388a.I.e();
                    this.f3388a.I.requestLayout();
                    boolean unused = this.f3388a.ar = false;
                    return;
                }
                return;
            case 4:
                this.f3388a.I.a(((HashMap) this.f3388a.v[4].params).get("hasNext").toString(), (ArrayList) ((HashMap) this.f3388a.v[4].params).get("pageContext"));
                this.f3388a.v[4] = null;
                return;
            case 5:
                HashMap hashMap2 = (HashMap) this.f3388a.v[5].params;
                this.f3388a.I.b().a((ArrayList) hashMap2.get("selectedCommentList"), (ArrayList) hashMap2.get("goodTagList"));
                this.f3388a.v[5] = null;
                return;
            case 6:
                ArrayList arrayList = (ArrayList) ((HashMap) this.f3388a.v[6].params).get("pageContext");
                this.f3388a.I.b().a((List) ((HashMap) this.f3388a.v[6].params).get("hasNext"), this.f3388a.ac.f938a, this.f3388a.ac.c);
                String obj = ((HashMap) this.f3388a.v[6].params).get("pageText").toString();
                if (arrayList.size() >= 2) {
                    this.f3388a.J.a(obj, arrayList, this.f3388a.aV);
                }
                this.f3388a.v[6] = null;
                return;
            case 7:
                if (!AppDetailActivityV5.a(this.f3388a.ae) || TextUtils.isEmpty(this.f3388a.ae.f941a.f1149a.f1144a.v)) {
                    int intValue = Integer.decode(this.f3388a.v[7].params.toString()).intValue();
                    if (AppDetailActivityV5.a(this.f3388a.ae) && AppDetailViewV5.a(this.f3388a.ae, this.f3388a.ac) == AppDetailViewV5.APPDETAIL_MODE.NEED_UPDATE && this.f3388a.ae.f941a.f1149a.b.get(0).F > 10000) {
                        BigDecimal scale = new BigDecimal(((double) this.f3388a.ae.f941a.f1149a.b.get(0).F) / 10000.0d).setScale(1, 4);
                        str = String.format(this.f3388a.getResources().getString(R.string.appdetail_friends_update), scale);
                    } else if (intValue > 0) {
                        str = String.format(this.f3388a.getResources().getString(R.string.appdetail_friends_using), Integer.valueOf(intValue));
                    } else {
                        str = Constants.STR_EMPTY;
                    }
                } else {
                    str = this.f3388a.ae.f941a.f1149a.f1144a.v;
                }
                this.f3388a.G.a(str);
                this.f3388a.v[7] = null;
                return;
            default:
                return;
        }
    }
}
