package com.tencent.pangu.activity;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class bt extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ReportActivity f3344a;

    bt(ReportActivity reportActivity) {
        this.f3344a = reportActivity;
    }

    public void onTMAClick(View view) {
        boolean z = true;
        switch (view.getId()) {
            case R.id.submit /*2131165869*/:
                if (this.f3344a.v()) {
                    this.f3344a.H.a(this.f3344a.E, this.f3344a.F, this.f3344a.x(), this.f3344a.D.getText().toString());
                    this.f3344a.finish();
                    return;
                }
                Toast.makeText(this.f3344a, (int) R.string.report_no_content, 0).show();
                return;
            case R.id.checkbox_downlaod /*2131166394*/:
                this.f3344a.v.setSelected(!this.f3344a.v.isSelected());
                if (this.f3344a.v.isSelected()) {
                    this.f3344a.w.setEnabled(false);
                    this.f3344a.z.setEnabled(false);
                    this.f3344a.y.setEnabled(false);
                    this.f3344a.A.setEnabled(false);
                    this.f3344a.C.setEnabled(false);
                    this.f3344a.w.setSelected(false);
                    this.f3344a.z.setSelected(false);
                    this.f3344a.y.setSelected(false);
                    this.f3344a.A.setSelected(false);
                    this.f3344a.C.setSelected(false);
                    this.f3344a.u.setEnabled(true);
                    return;
                }
                this.f3344a.w.setEnabled(true);
                this.f3344a.z.setEnabled(true);
                this.f3344a.y.setEnabled(true);
                this.f3344a.A.setEnabled(true);
                this.f3344a.C.setEnabled(true);
                return;
            case R.id.checkbox_version /*2131166395*/:
                TextView p = this.f3344a.x;
                if (this.f3344a.x.isSelected()) {
                    z = false;
                }
                p.setSelected(z);
                return;
            case R.id.checkbox_fee /*2131166396*/:
                TextView j = this.f3344a.z;
                if (this.f3344a.z.isSelected()) {
                    z = false;
                }
                j.setSelected(z);
                this.f3344a.w();
                return;
            case R.id.checkbox_right /*2131166397*/:
                TextView q = this.f3344a.B;
                if (this.f3344a.B.isSelected()) {
                    z = false;
                }
                q.setSelected(z);
                return;
            case R.id.checkbox_install /*2131166398*/:
                this.f3344a.w.setSelected(!this.f3344a.w.isSelected());
                if (this.f3344a.w.isSelected()) {
                    this.f3344a.v.setEnabled(false);
                    this.f3344a.z.setEnabled(false);
                    this.f3344a.y.setEnabled(false);
                    this.f3344a.A.setEnabled(false);
                    this.f3344a.C.setEnabled(false);
                    this.f3344a.v.setSelected(false);
                    this.f3344a.z.setSelected(false);
                    this.f3344a.y.setSelected(false);
                    this.f3344a.A.setSelected(false);
                    this.f3344a.C.setSelected(false);
                    this.f3344a.u.setEnabled(true);
                    return;
                }
                this.f3344a.v.setEnabled(true);
                this.f3344a.z.setEnabled(true);
                this.f3344a.y.setEnabled(true);
                this.f3344a.A.setEnabled(true);
                this.f3344a.C.setEnabled(true);
                return;
            case R.id.checkbox_plugin /*2131166399*/:
                TextView k = this.f3344a.y;
                if (this.f3344a.y.isSelected()) {
                    z = false;
                }
                k.setSelected(z);
                this.f3344a.w();
                return;
            case R.id.checkbox_virus /*2131166400*/:
                TextView l = this.f3344a.A;
                if (this.f3344a.A.isSelected()) {
                    z = false;
                }
                l.setSelected(z);
                this.f3344a.w();
                return;
            case R.id.checkbox_private /*2131166401*/:
                TextView m = this.f3344a.C;
                if (this.f3344a.C.isSelected()) {
                    z = false;
                }
                m.setSelected(z);
                this.f3344a.w();
                return;
            default:
                return;
        }
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3344a, 200);
        if (buildSTInfo == null) {
            return null;
        }
        buildSTInfo.extraData = this.f3344a.D.getText().toString();
        switch (view.getId()) {
            case R.id.submit /*2131165869*/:
                buildSTInfo.slotId = a.a("06", "010");
                break;
            case R.id.checkbox_downlaod /*2131166394*/:
                buildSTInfo.slotId = a.a("06", "001");
                buildSTInfo.status = this.f3344a.v.isSelected() ? "01" : "02";
                break;
            case R.id.checkbox_version /*2131166395*/:
                buildSTInfo.slotId = a.a("06", "007");
                buildSTInfo.status = this.f3344a.x.isSelected() ? "01" : "02";
                break;
            case R.id.checkbox_fee /*2131166396*/:
                buildSTInfo.slotId = a.a("06", "003");
                buildSTInfo.status = this.f3344a.z.isSelected() ? "01" : "02";
                break;
            case R.id.checkbox_right /*2131166397*/:
                buildSTInfo.slotId = a.a("06", "008");
                buildSTInfo.status = this.f3344a.B.isSelected() ? "01" : "02";
                break;
            case R.id.checkbox_install /*2131166398*/:
                buildSTInfo.slotId = a.a("06", "002");
                buildSTInfo.status = this.f3344a.w.isSelected() ? "01" : "02";
                break;
            case R.id.checkbox_plugin /*2131166399*/:
                buildSTInfo.slotId = a.a("06", "004");
                buildSTInfo.status = this.f3344a.y.isSelected() ? "01" : "02";
                break;
            case R.id.checkbox_virus /*2131166400*/:
                buildSTInfo.slotId = a.a("06", "005");
                buildSTInfo.status = this.f3344a.A.isSelected() ? "01" : "02";
                break;
            case R.id.checkbox_private /*2131166401*/:
                buildSTInfo.slotId = a.a("06", "006");
                buildSTInfo.status = this.f3344a.C.isSelected() ? "01" : "02";
                break;
        }
        return buildSTInfo;
    }
}
