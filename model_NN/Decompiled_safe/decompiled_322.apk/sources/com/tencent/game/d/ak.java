package com.tencent.game.d;

import com.tencent.assistant.protocol.jce.CardItemWrapper;
import com.tencent.assistant.protocol.jce.SmartCardWrapper;
import com.tencent.assistant.utils.TemporaryThreadManager;
import java.util.ArrayList;

/* compiled from: ProGuard */
class ak implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aa f2687a;
    private int b = 0;
    /* access modifiers changed from: private */
    public aj c;
    private long d;
    private ArrayList<CardItemWrapper> e;
    private ArrayList<SmartCardWrapper> f;
    /* access modifiers changed from: private */
    public int g = -1;

    public ak(aa aaVar) {
        this.f2687a = aaVar;
        this.c = new aj(aaVar);
    }

    public void a(aj ajVar) {
        if (ajVar != null && ajVar.f2686a.length != 0) {
            b();
            this.b = 1;
            this.c.a(ajVar);
            this.g = this.f2687a.getUniqueId();
            TemporaryThreadManager.get().start(new al(this));
        }
    }

    public int a() {
        return this.g;
    }

    public void b() {
        this.g = -1;
        this.c.f2686a = null;
        this.c.b = 0;
        this.c.c = false;
        this.e = null;
        this.b = 0;
    }

    public boolean c() {
        return this.b == 1;
    }

    public aj d() {
        return this.c;
    }

    public ArrayList<CardItemWrapper> e() {
        return this.e;
    }

    public ArrayList<SmartCardWrapper> f() {
        return this.f;
    }

    public boolean g() {
        return this.c.c;
    }

    public long h() {
        return this.d;
    }

    public void a(long j, ArrayList<CardItemWrapper> arrayList, aj ajVar, ArrayList<SmartCardWrapper> arrayList2) {
        this.d = j;
        this.e = arrayList;
        this.c.a(ajVar);
        this.f = arrayList2;
        this.b = 2;
    }

    public void i() {
        this.b = 2;
    }

    /* renamed from: j */
    public ak clone() {
        try {
            ak akVar = (ak) super.clone();
            if (this.e == null) {
                return akVar;
            }
            akVar.e = (ArrayList) this.e.clone();
            return akVar;
        } catch (CloneNotSupportedException e2) {
            return this;
        }
    }
}
