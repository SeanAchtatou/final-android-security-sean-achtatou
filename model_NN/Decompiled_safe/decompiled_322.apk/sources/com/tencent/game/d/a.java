package com.tencent.game.d;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.ag;
import com.tencent.assistant.module.ak;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.GftGetAppListRequest;
import com.tencent.assistant.protocol.jce.GftGetAppListResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class a extends BaseEngine<com.tencent.assistant.module.callback.a> implements ak {

    /* renamed from: a  reason: collision with root package name */
    public long f2676a;
    public int b;
    private long c;
    private short d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public long f;
    private List<SimpleAppModel> g;
    private byte[] h;
    private boolean i;
    private int j;
    private int k;
    private List<SimpleAppModel> l;
    private byte[] m;
    private boolean n;
    private h o;
    private b p;
    /* access modifiers changed from: private */
    public int q;

    public a(long j2, int i2) {
        boolean z = false;
        this.e = false;
        this.f = -1;
        this.i = true;
        this.j = -1;
        this.k = -1;
        this.l = new ArrayList();
        this.n = true;
        this.p = new b();
        this.q = 0;
        this.f2676a = j2;
        this.b = i2;
        if (this.f2676a == -2 && (this.b == 1 || this.b == 2)) {
            z = true;
        }
        if (z) {
            this.e = true;
            ag.b().a(this);
        }
        this.o = new h(this);
    }

    public a(long j2, int i2, short s) {
        this(j2, i2);
        this.d = s;
    }

    private boolean h() {
        return this.f2676a == 0 && this.b == 99;
    }

    public b a() {
        this.p.b(this.f);
        this.p.b(f());
        return this.p;
    }

    public GftGetAppListResponse b() {
        return i.y().c(this.f2676a, this.b, null);
    }

    public void c() {
        TemporaryThreadManager.get().start(new b(this));
    }

    public void a(boolean z) {
        TemporaryThreadManager.get().start(new c(this, z));
    }

    public void a(long j2) {
        this.c = j2;
        c();
    }

    /* access modifiers changed from: private */
    public boolean i() {
        GftGetAppListResponse b2;
        boolean z = false;
        if (!this.e || (b2 = b()) == null || h()) {
            return false;
        }
        long a2 = m.a().a((byte) 5);
        if (a2 != -11 && b2.e != a2) {
            if (b2.e != a2) {
            }
            return false;
        } else if (b2.b == null || b2.b.size() <= 0) {
            return false;
        } else {
            this.f = b2.e;
            this.p.b(this.f);
            if (b2.d == 1) {
                z = true;
            }
            this.i = z;
            this.h = b2.c;
            this.g = k.b(b2.b);
            this.l.clear();
            this.l.addAll(this.g);
            this.q = this.l.get(this.l.size() - 1).al;
            this.n = this.i;
            this.m = this.h;
            ArrayList arrayList = new ArrayList(this.g);
            if (this.g != null && this.g.size() > 0) {
                notifyDataChangedInMainThread(new d(this, arrayList));
                if (this.n) {
                    this.o.a(this.m);
                }
            }
            return true;
        }
    }

    public void onLocalDataHasUpdate() {
        if (this.e) {
            if (this.f != m.a().a((byte) 5) && !h()) {
                d();
            }
        }
    }

    public void onPromptHasNewNotify(ArrayList<QuickEntranceNotify> arrayList) {
    }

    public int d() {
        if (this.j > 0) {
            cancel(this.j);
        }
        this.j = b((byte[]) null);
        return this.j;
    }

    /* access modifiers changed from: private */
    public int a(byte[] bArr) {
        if (this.k > 0) {
            cancel(this.k);
        }
        this.k = b(bArr);
        return this.k;
    }

    public int e() {
        if (this.m == null || this.m.length == 0) {
            return -1;
        }
        if (this.o.c()) {
            int a2 = this.o.a();
            this.o.b();
            return a2;
        } else if (this.m != this.o.d() || this.o.e() == null) {
            return a(this.m);
        } else {
            h j2 = this.o.clone();
            boolean z = this.f != j2.h();
            this.f = j2.h();
            if (this.f != m.a().a((byte) 5) && !h()) {
                return a(this.m);
            }
            int a3 = j2.a();
            this.p.b(this.f);
            if (z) {
                this.g = j2.e();
                this.h = j2.g();
                this.i = j2.f();
                this.l.clear();
            }
            ArrayList arrayList = new ArrayList(j2.e());
            this.l.addAll(arrayList);
            if (this.l.size() > 0) {
                this.q = this.l.get(this.l.size() - 1).al;
            }
            this.n = j2.f();
            this.m = j2.g();
            XLog.d("voken", "getNextPage mNextPageContext = " + this.m);
            notifyDataChangedInMainThread(new e(this, a3, z, arrayList));
            if (this.o.e) {
                this.o.a(this.o.g());
            }
            return this.o.a();
        }
    }

    private int b(byte[] bArr) {
        return a(-1, bArr);
    }

    /* access modifiers changed from: private */
    public int a(int i2, byte[] bArr) {
        GftGetAppListRequest gftGetAppListRequest = new GftGetAppListRequest();
        if (0 != this.c) {
            gftGetAppListRequest.f1361a = this.c;
            gftGetAppListRequest.b = 4;
        } else {
            gftGetAppListRequest.f1361a = this.f2676a;
            gftGetAppListRequest.b = this.b;
        }
        gftGetAppListRequest.c = this.d > 0 ? this.d : 30;
        gftGetAppListRequest.e = this.q;
        XLog.d("voken", "sendRequest mNextPageContext = " + this.m);
        if (bArr == null) {
            bArr = new byte[0];
        }
        gftGetAppListRequest.d = bArr;
        return send(i2, gftGetAppListRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z = true;
        if (jceStruct2 != null) {
            GftGetAppListResponse gftGetAppListResponse = (GftGetAppListResponse) jceStruct2;
            GftGetAppListRequest gftGetAppListRequest = (GftGetAppListRequest) jceStruct;
            ArrayList<SimpleAppModel> b2 = k.b(gftGetAppListResponse.b);
            if (i2 == this.o.a()) {
                h hVar = this.o;
                long j2 = gftGetAppListResponse.e;
                if (gftGetAppListResponse.d != 1) {
                    z = false;
                }
                hVar.a(j2, b2, z, gftGetAppListResponse.c);
            } else if (this.m != gftGetAppListResponse.c) {
                boolean z2 = gftGetAppListResponse.e != this.f || gftGetAppListRequest.d == null || gftGetAppListRequest.d.length == 0;
                if (gftGetAppListResponse.d != 1) {
                    z = false;
                }
                byte[] bArr = gftGetAppListResponse.c;
                this.f = gftGetAppListResponse.e;
                this.p.b(this.f);
                if (z2) {
                    this.g = b2;
                    this.h = bArr;
                    this.i = z;
                    this.l.clear();
                }
                this.l.addAll(b2);
                if (this.l.size() > 0) {
                    this.q = this.l.get(this.l.size() - 1).al;
                    XLog.d("voken", "onRequestSuccessed pageContext = " + bArr);
                }
                this.n = z;
                this.m = bArr;
                notifyDataChangedInMainThread(new f(this, i2, z2, b2, gftGetAppListResponse));
                if (this.n) {
                    this.o.a(this.m);
                }
            }
            if (this.e) {
                i.y().a(this.f2676a, this.b, gftGetAppListRequest.d, gftGetAppListResponse);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        if (i2 != this.o.a()) {
            GftGetAppListRequest gftGetAppListRequest = (GftGetAppListRequest) jceStruct;
            notifyDataChangedInMainThread(new g(this, i2, i3, gftGetAppListRequest.d == null || gftGetAppListRequest.d.length == 0));
            return;
        }
        this.o.i();
    }

    public List<SimpleAppModel> f() {
        return this.l;
    }

    public boolean g() {
        return this.n;
    }

    public String toString() {
        return "categoryId:" + this.f2676a + " sortId:" + this.b;
    }
}
