package com.tencent.game.d;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.ag;
import com.tencent.assistant.module.ak;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.jce.AppCategory;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.protocol.jce.GftGetAppCategoryRequest;
import com.tencent.assistant.protocol.jce.GftGetAppCategoryResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.game.d.a.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class j extends BaseEngine<a> implements ak, NetworkMonitor.ConnectivityChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private final String f2702a = "CategoryEngine";
    private long b = -1;
    private List<ColorCardItem> c = new ArrayList();
    /* access modifiers changed from: private */
    public List<ColorCardItem> d = new ArrayList();
    private List<AppCategory> e = new ArrayList();
    /* access modifiers changed from: private */
    public List<AppCategory> f = new ArrayList();
    private List<AppCategory> g = new ArrayList();
    /* access modifiers changed from: private */
    public List<AppCategory> h = new ArrayList();
    private int i = -1;

    public j() {
        ag.b().a(this);
    }

    public List<AppCategory> a(long j) {
        if (j == -1) {
            return this.e;
        }
        return this.f;
    }

    public List<AppCategory> b(long j) {
        if (j == -1) {
            return this.g;
        }
        return this.h;
    }

    public List<ColorCardItem> c(long j) {
        if (j == -1) {
            return this.c;
        }
        return this.d;
    }

    public long a() {
        return this.b;
    }

    public void b() {
        XLog.d("CategoryEngine", "loadData:start");
        if (this.g.size() <= 0 || this.h.size() <= 0) {
            XLog.d("CategoryEngine", "loadData:load from server");
            TemporaryThreadManager.get().start(new l(this));
            return;
        }
        XLog.d("CategoryEngine", "loadData:load from local");
        notifyDataChangedInMainThread(new k(this));
    }

    public int c() {
        if (this.i > 0) {
            cancel(this.i);
        }
        this.i = send(new GftGetAppCategoryRequest());
        return this.i;
    }

    private List<com.tencent.assistant.module.a> a(List<AppCategory> list) {
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        ArrayList<com.tencent.assistant.module.a> arrayList2 = new ArrayList<>();
        for (AppCategory next : list) {
            com.tencent.assistant.module.a aVar = new com.tencent.assistant.module.a(next);
            arrayList2.add(aVar);
            hashMap.put(Long.valueOf(next.f1140a), aVar);
        }
        for (com.tencent.assistant.module.a aVar2 : arrayList2) {
            if (aVar2.c.e == 0) {
                arrayList.add(aVar2);
            } else {
                com.tencent.assistant.module.a aVar3 = (com.tencent.assistant.module.a) hashMap.get(Long.valueOf(aVar2.c.e));
                if (aVar3 != null) {
                    aVar3.a(aVar2);
                }
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        XLog.d("CategoryEngine", "(loadLoaclCache)");
        GftGetAppCategoryResponse w = i.y().w();
        if (w != null && w.c != m.a().a((byte) 4)) {
            return false;
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) start");
        if (w == null || w.b == null || w.b.size() <= 0) {
            return false;
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) color 1");
        ArrayList<ColorCardItem> b2 = w.b();
        this.c.clear();
        if (b2 != null && b2.size() > 0) {
            this.c.addAll(b2);
        }
        ArrayList<ColorCardItem> c2 = w.c();
        this.d.clear();
        if (c2 != null && c2.size() > 0) {
            this.d.addAll(c2);
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) common 1");
        ArrayList<AppCategory> arrayList = w.e;
        if (arrayList != null && arrayList.size() > 0) {
            List<AppCategory> b3 = b(arrayList, -1);
            List<AppCategory> b4 = b(arrayList, -2);
            if (b3 != null && b3.size() > 0) {
                this.e.clear();
                this.e.addAll(b3);
            }
            if (b4 != null && b4.size() > 0) {
                this.f.clear();
                this.f.addAll(b4);
            }
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) normal 1");
        ArrayList<AppCategory> arrayList2 = w.b;
        XLog.d("CategoryEngine", "(loadLoaclCache) normal 1:" + arrayList2.size());
        if (arrayList2 == null || arrayList2.size() <= 0) {
            return false;
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) normal 2:");
        List<com.tencent.assistant.module.a> a2 = a(arrayList2);
        if (a2 == null) {
            return false;
        }
        XLog.d("CategoryEngine", "(loadLoaclCache) normal 3:");
        this.b = w.c;
        List<AppCategory> a3 = a(a2, -1);
        List<AppCategory> a4 = a(a2, -2);
        if (a3 != null && a3.size() > 0) {
            this.g.clear();
            this.g.addAll(a3);
        }
        if (a4 != null && a4.size() > 0) {
            this.h.clear();
            this.h.addAll(a4);
        }
        notifyDataChangedInMainThread(new m(this));
        return true;
    }

    private List<AppCategory> a(List<com.tencent.assistant.module.a> list, long j) {
        if (list == null || list.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (com.tencent.assistant.module.a a2 : list) {
            com.tencent.assistant.module.a a3 = a(a2, j);
            if (a3 != null) {
                Iterator<com.tencent.assistant.module.a> it = a3.f949a.iterator();
                while (it.hasNext()) {
                    arrayList.add(it.next().c);
                }
                return arrayList;
            }
        }
        return null;
    }

    private List<AppCategory> b(List<AppCategory> list, long j) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (AppCategory next : list) {
            if (next != null && next.e == j) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    private com.tencent.assistant.module.a a(com.tencent.assistant.module.a aVar, long j) {
        if (aVar.c.a() == j) {
            return aVar;
        }
        if (aVar.f949a != null && !aVar.f949a.isEmpty()) {
            Iterator<com.tencent.assistant.module.a> it = aVar.f949a.iterator();
            while (it.hasNext()) {
                com.tencent.assistant.module.a a2 = a(it.next(), j);
                if (a2 != null) {
                    return a2;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        List<com.tencent.assistant.module.a> a2;
        if (jceStruct2 != null) {
            GftGetAppCategoryResponse gftGetAppCategoryResponse = (GftGetAppCategoryResponse) jceStruct2;
            ArrayList<ColorCardItem> b2 = gftGetAppCategoryResponse.b();
            this.c.clear();
            if (b2 != null && b2.size() > 0) {
                this.c.addAll(b2);
            }
            ArrayList<ColorCardItem> c2 = gftGetAppCategoryResponse.c();
            this.d.clear();
            if (c2 != null && c2.size() > 0) {
                this.d.addAll(c2);
            }
            ArrayList<AppCategory> arrayList = gftGetAppCategoryResponse.e;
            if (arrayList != null && arrayList.size() > 0) {
                List<AppCategory> b3 = b(arrayList, -1);
                List<AppCategory> b4 = b(arrayList, -2);
                if (b3 != null && b3.size() > 0) {
                    this.e.clear();
                    this.e.addAll(b3);
                }
                if (b4 != null && b4.size() > 0) {
                    this.f.clear();
                    this.f.addAll(b4);
                }
            }
            ArrayList<AppCategory> a3 = gftGetAppCategoryResponse.a();
            if (a3 != null && a3.size() > 0 && (a2 = a(a3)) != null) {
                this.b = gftGetAppCategoryResponse.c;
                List<AppCategory> a4 = a(a2, -1);
                List<AppCategory> a5 = a(a2, -2);
                if (a4 != null && a4.size() > 0) {
                    this.g.clear();
                    this.g.addAll(a4);
                }
                if (a5 != null && a5.size() > 0) {
                    this.h.clear();
                    this.h.addAll(a5);
                }
                notifyDataChangedInMainThread(new n(this, i2));
                i.y().a(gftGetAppCategoryResponse);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new o(this, i2, i3));
    }

    public void onLocalDataHasUpdate() {
        if (this.b != m.a().a((byte) 4)) {
            c();
        }
    }

    public void onPromptHasNewNotify(ArrayList<QuickEntranceNotify> arrayList) {
    }

    public void onConnected(APN apn) {
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }
}
