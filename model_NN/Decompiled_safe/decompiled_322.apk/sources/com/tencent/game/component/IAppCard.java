package com.tencent.game.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistantv2.st.b.e;

/* compiled from: ProGuard */
public abstract class IAppCard extends RelativeLayout {
    protected Context c;
    protected LayoutInflater d;
    public n e = null;
    public as f = null;
    public boolean g = false;
    protected IViewInvalidater h;
    protected e i = null;

    /* access modifiers changed from: protected */
    public abstract void a();

    public IAppCard(Context context) {
        super(context);
    }

    public IAppCard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public IAppCard(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }

    public IAppCard(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, null);
        this.e = nVar;
        this.c = context;
        this.f = asVar;
        this.d = (LayoutInflater) context.getSystemService("layout_inflater");
        setWillNotDraw(false);
        this.h = iViewInvalidater;
    }
}
