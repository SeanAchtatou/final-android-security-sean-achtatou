package com.tencent.game.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class w extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f2675a;
    final /* synthetic */ SimpleAppInfo b;
    final /* synthetic */ as c;
    final /* synthetic */ GameSquareAppItem d;

    w(GameSquareAppItem gameSquareAppItem, STInfoV2 sTInfoV2, SimpleAppInfo simpleAppInfo, as asVar) {
        this.d = gameSquareAppItem;
        this.f2675a = sTInfoV2;
        this.b = simpleAppInfo;
        this.c = asVar;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.d.b, AppDetailActivityV5.class);
        if (this.f2675a != null) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f2675a.scene);
        }
        intent.putExtra("com.tencent.assistant.PACKAGE_NAME", this.b.f);
        intent.putExtra("com.tencent.assistant.APP_ID", this.b.f1495a);
        intent.putExtra("com.tencent.assistant.APK_ID", this.b.p);
        this.d.b.startActivity(intent);
        if (this.c != null) {
            this.c.b(0, 0);
        }
    }

    public STInfoV2 getStInfo() {
        if (this.f2675a != null) {
            this.f2675a.actionId = 200;
        }
        return this.f2675a;
    }
}
