package com.tencent.game.activity;

import android.widget.AbsListView;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;

/* compiled from: ProGuard */
class p extends ListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    boolean f2628a = false;
    final /* synthetic */ n b;

    p(n nVar) {
        this.b = nVar;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        super.onScroll(absListView, i, i2, i3);
        if (i == 2) {
            this.f2628a = true;
        } else if (i == 1) {
            this.f2628a = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.game.component.GameRankNormalListView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.game.component.GameRankNormalListView.a(com.tencent.game.component.GameRankNormalListView, java.util.List):boolean
      com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView.a(com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView, boolean):boolean
      com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(int, int):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(android.content.Context, android.view.View):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(int, int):void
      com.tencent.game.component.GameRankNormalListView.a(boolean, boolean):void */
    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
        if (i == 0 && this.f2628a) {
            this.b.V.a(false, false);
        }
    }
}
