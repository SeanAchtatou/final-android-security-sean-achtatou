package com.tencent.wcs.proxy.c;

import com.qq.AppService.r;
import com.tencent.connect.common.Constants;
import com.tencent.wcs.c.b;
import com.tencent.wcs.proxy.b.h;
import com.tencent.wcs.proxy.b.q;
import java.io.DataInputStream;
import java.nio.ByteBuffer;

/* compiled from: ProGuard */
public class f {
    private static StringBuilder e = new StringBuilder();

    /* renamed from: a  reason: collision with root package name */
    private int f4129a;
    private int b;
    private ByteBuffer c;
    private int d;

    public void a(int i) {
        this.f4129a = i;
    }

    public void b(int i) {
        this.b = i;
    }

    public void a(ByteBuffer byteBuffer) {
        this.c = byteBuffer;
    }

    public void c(int i) {
        this.d = i;
    }

    public int a() {
        return this.f4129a;
    }

    public int b() {
        return this.b;
    }

    public ByteBuffer c() {
        return this.c;
    }

    public int d() {
        return this.d;
    }

    public static f a(DataInputStream dataInputStream) {
        String readLine;
        f fVar = null;
        if (!(dataInputStream == null || (readLine = dataInputStream.readLine()) == null || readLine.equals(Constants.STR_EMPTY))) {
            fVar = (f) q.b().c((Object[]) new Void[0]);
            fVar.f4129a = a(readLine);
            while (readLine != null && !readLine.equals(Constants.STR_EMPTY)) {
                e.append(readLine).append("\r\n");
                if (readLine.startsWith("Content-Length")) {
                    fVar.b = b(readLine);
                }
                readLine = dataInputStream.readLine();
            }
            fVar.c = a(dataInputStream, fVar.f4129a, fVar.b);
            if (fVar.c == null) {
                dataInputStream.skip((long) fVar.b());
            } else if (fVar.b <= 4 || fVar.b != fVar.c.limit()) {
                b.a("SessionResponse " + e.toString());
            } else {
                fVar.d = r.a(fVar.c.array());
            }
            e.delete(0, e.length());
        }
        return fVar;
    }

    private static int a(String str) {
        String trim;
        int indexOf;
        String substring;
        String trim2;
        int indexOf2;
        if (str == null || (indexOf = (trim = str.trim()).indexOf(32)) < 0 || indexOf + 1 >= trim.length() || (substring = trim.substring(indexOf + 1)) == null || (indexOf2 = (trim2 = substring.trim()).indexOf(32)) < 0 || indexOf2 >= trim2.length()) {
            return -1;
        }
        try {
            return Integer.parseInt(trim2.substring(0, indexOf2));
        } catch (Exception e2) {
            return 200;
        }
    }

    private static int b(String str) {
        int indexOf;
        String substring;
        if (str == null || (indexOf = str.indexOf(58)) < 0 || indexOf + 1 >= str.length() || (substring = str.substring(indexOf + 1)) == null) {
            return -1;
        }
        try {
            return Integer.parseInt(substring.trim());
        } catch (Exception e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    private static ByteBuffer a(DataInputStream dataInputStream, int i, int i2) {
        int i3;
        int i4 = 0;
        if (dataInputStream == null) {
            b.a("SessionResponse data input stream is null");
            return null;
        } else if (i2 <= 0) {
            return null;
        } else {
            if (i != 200) {
                b.a("SessionResponse response code = " + i);
            }
            ByteBuffer byteBuffer = (ByteBuffer) h.b().c((Object[]) new Integer[]{Integer.valueOf(i2)});
            while (i4 < i2) {
                int i5 = i2 - i4;
                if (i5 > 8192) {
                    i5 = 8192;
                }
                int read = dataInputStream.read(byteBuffer.array(), i4, i5);
                if (read > 0) {
                    i3 = read + i4;
                } else if (read == -1) {
                    break;
                } else {
                    i3 = i4;
                }
                i4 = i3;
            }
            if (i4 < i2) {
                b.a("SessionResponse total readed = " + i4 + " content length = " + i2);
                return null;
            }
            byteBuffer.position(4);
            byteBuffer.limit(i2);
            return byteBuffer;
        }
    }
}
