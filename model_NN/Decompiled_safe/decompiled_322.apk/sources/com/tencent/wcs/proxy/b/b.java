package com.tencent.wcs.proxy.b;

import com.tencent.assistant.utils.q;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private ExecutorService f4116a;

    private b() {
        this.f4116a = new ThreadPoolExecutor(20, 200, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(20), new q("AgentSession"));
    }

    public static b a() {
        return d.f4117a;
    }

    public void a(Runnable runnable) {
        if (this.f4116a != null) {
            this.f4116a.execute(runnable);
        }
    }
}
