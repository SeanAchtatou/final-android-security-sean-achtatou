package com.tencent.wcs.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class ConfigDeviceType implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final ConfigDeviceType f4079a = new ConfigDeviceType(0, 0, "CONFIG_DEVICE_TYPE_PHONE_ANDROID");
    public static final ConfigDeviceType b = new ConfigDeviceType(1, 1, "CONFIG_DEVICE_TYPE_PC");
    static final /* synthetic */ boolean c = (!ConfigDeviceType.class.desiredAssertionStatus());
    private static ConfigDeviceType[] d = new ConfigDeviceType[2];
    private int e;
    private String f = new String();

    public String toString() {
        return this.f;
    }

    private ConfigDeviceType(int i, int i2, String str) {
        this.f = str;
        this.e = i2;
        d[i] = this;
    }
}
