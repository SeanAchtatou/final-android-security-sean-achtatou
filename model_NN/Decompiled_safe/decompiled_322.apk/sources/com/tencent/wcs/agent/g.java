package com.tencent.wcs.agent;

import java.util.ArrayList;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f4052a;

    g(e eVar) {
        this.f4052a = eVar;
    }

    public void run() {
        ArrayList arrayList = new ArrayList();
        synchronized (this.f4052a.e) {
            int size = this.f4052a.e.size();
            for (int i = 0; i < size; i++) {
                l lVar = (l) this.f4052a.e.valueAt(i);
                if (lVar != null) {
                    arrayList.add(lVar);
                }
            }
            this.f4052a.e.clear();
        }
        int size2 = arrayList.size();
        for (int i2 = 0; i2 < size2; i2++) {
            ((l) arrayList.get(i2)).b();
            ((l) arrayList.get(i2)).a((p) null);
        }
        arrayList.clear();
    }
}
