package com.tencent.open;

import android.os.Build;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import com.tencent.connect.common.Constants;
import com.tencent.open.a.n;

/* compiled from: ProGuard */
class f extends WebChromeClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f3265a;

    f(b bVar) {
        this.f3265a = bVar;
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        if (consoleMessage == null) {
            return false;
        }
        n.c("WebConsole", consoleMessage.message() + " -- From  111 line " + consoleMessage.lineNumber() + " of " + consoleMessage.sourceId());
        if (Build.VERSION.SDK_INT > 7) {
            this.f3265a.onConsoleMessage(consoleMessage == null ? Constants.STR_EMPTY : consoleMessage.message());
        }
        return true;
    }

    public void onConsoleMessage(String str, int i, String str2) {
        n.c("WebConsole", str + " -- From 222 line " + i + " of " + str2);
        if (Build.VERSION.SDK_INT == 7) {
            this.f3265a.onConsoleMessage(str);
        }
    }
}
