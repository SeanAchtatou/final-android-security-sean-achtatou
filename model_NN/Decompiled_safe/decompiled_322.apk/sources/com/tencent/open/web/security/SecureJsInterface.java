package com.tencent.open.web.security;

import com.tencent.open.a.n;
import com.tencent.open.e;

/* compiled from: ProGuard */
public class SecureJsInterface extends e {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3287a = (n.d + ".SI");
    public static boolean isPWDEdit = false;
    private String b;

    public boolean customCallback() {
        return true;
    }

    public void curPosFromJS(String str) {
        n.c(f3287a, "-->curPosFromJS: " + str);
        int i = -1;
        try {
            i = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            n.e(f3287a, "-->curPosFromJS number format exception.");
        }
        if (i < 0) {
            throw new RuntimeException("position is illegal.");
        }
        if (!a.c) {
        }
        if (!a.b) {
            this.b = a.f3288a;
            JniInterface.insetTextToArray(i, this.b, this.b.length());
            n.b(f3287a, "mKey: " + this.b);
        } else if (Boolean.valueOf(JniInterface.BackSpaceChar(a.b, i)).booleanValue()) {
            a.b = false;
        }
    }

    public void isPasswordEdit(String str) {
        n.c(f3287a, "-->is pswd edit, flag: " + str);
        int i = -1;
        try {
            i = Integer.parseInt(str);
        } catch (Exception e) {
            n.e(f3287a, "-->is pswd edit exception: " + e.getMessage());
        }
        if (i != 0 && i != 1) {
            throw new RuntimeException("is pswd edit flag is illegal.");
        } else if (i == 0) {
            isPWDEdit = false;
        } else if (i == 1) {
            isPWDEdit = true;
        }
    }

    public void clearAllEdit() {
        n.c(f3287a, "-->clear all edit.");
        try {
            JniInterface.clearAllPWD();
        } catch (Exception e) {
            n.e(f3287a, "-->clear all edit exception: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public String getMD5FromNative() {
        n.c(f3287a, "-->get md5 form native");
        try {
            String pWDKeyToMD5 = JniInterface.getPWDKeyToMD5(null);
            n.b(f3287a, "-->getMD5FromNative, MD5= " + pWDKeyToMD5);
            return pWDKeyToMD5;
        } catch (Exception e) {
            n.e(f3287a, "-->get md5 form native exception: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
