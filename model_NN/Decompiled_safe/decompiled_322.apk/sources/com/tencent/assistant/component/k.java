package com.tencent.assistant.component;

import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f676a;
    final /* synthetic */ DownloadProgressButton b;

    k(DownloadProgressButton downloadProgressButton, int i) {
        this.b = downloadProgressButton;
        this.f676a = i;
    }

    public void run() {
        if (this.b.getDisplayCount() == this.f676a) {
            this.b.downCountText.setText(String.valueOf(this.f676a));
            this.b.downCountText.setVisibility(0);
            this.b.downImage.setImageResource(R.drawable.icon_xiazai_empty);
        }
    }
}
