package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
public class TXGridView extends TXScrollViewBase<GridView> {
    public TXGridView(Context context) {
        super(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.NONE);
    }

    public TXGridView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setAdapter(ListAdapter listAdapter) {
        if (listAdapter != null) {
            ((GridView) this.s).setAdapter(listAdapter);
        }
    }

    public ListAdapter getAdapter() {
        return ((GridView) this.s).getAdapter();
    }

    public void setSelector(Drawable drawable) {
        if (this.s != null && drawable != null) {
            ((GridView) this.s).setSelector(drawable);
        }
    }

    public void setHorizontalSpacing(int i) {
        ((GridView) this.s).setHorizontalSpacing(i);
    }

    public void setVerticalSpacing(int i) {
        ((GridView) this.s).setVerticalSpacing(i);
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        ((GridView) this.s).setOnItemClickListener(onItemClickListener);
    }

    public void setNumColumns(int i) {
        ((GridView) this.s).setNumColumns(i);
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        View childAt;
        ListAdapter adapter = ((GridView) this.s).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        if (((GridView) this.s).getFirstVisiblePosition() > 1 || (childAt = ((GridView) this.s).getChildAt(0)) == null) {
            return false;
        }
        return childAt.getTop() >= ((GridView) this.s).getTop();
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        View childAt;
        ListAdapter adapter = ((GridView) this.s).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        int count = ((GridView) this.s).getCount() - 1;
        int lastVisiblePosition = ((GridView) this.s).getLastVisiblePosition();
        if (lastVisiblePosition < count - 1 || (childAt = ((GridView) this.s).getChildAt(lastVisiblePosition - ((GridView) this.s).getFirstVisiblePosition())) == null) {
            return false;
        }
        return childAt.getBottom() <= ((GridView) this.s).getBottom();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public GridView b(Context context) {
        return new GridView(context);
    }
}
