package com.tencent.assistant.component.dialog;

import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class m implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f659a;
    final /* synthetic */ Dialog b;

    m(AppConst.TwoBtnDialogInfo twoBtnDialogInfo, Dialog dialog) {
        this.f659a = twoBtnDialogInfo;
        this.b = dialog;
    }

    public void onClick(View view) {
        if (this.f659a != null) {
            this.f659a.onRightBtnClick();
            this.b.dismiss();
        }
    }
}
