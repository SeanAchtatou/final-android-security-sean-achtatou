package com.tencent.assistant.component;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class ApkMetaInfoLoader {
    private Map<String, MetaInfo> apkMetaInfoCache = new ConcurrentHashMap();

    /* compiled from: ProGuard */
    public class MetaInfo {
        public Drawable icon;
        public String name;

        public MetaInfo() {
        }
    }

    public MetaInfo getMetaInfoFromCache(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return this.apkMetaInfoCache.get(str);
    }

    public MetaInfo getMetaInfo(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        MetaInfo metaInfo = this.apkMetaInfoCache.get(str);
        if (metaInfo != null) {
            return metaInfo;
        }
        PackageManager packageManager = AstApp.i().getBaseContext().getPackageManager();
        try {
            PackageInfo packageArchiveInfo = packageManager.getPackageArchiveInfo(str, 0);
            if (packageArchiveInfo == null) {
                return metaInfo;
            }
            ApplicationInfo applicationInfo = packageArchiveInfo.applicationInfo;
            applicationInfo.sourceDir = str;
            applicationInfo.publicSourceDir = str;
            Drawable applicationIcon = packageManager.getApplicationIcon(applicationInfo);
            if (applicationIcon == null) {
                return metaInfo;
            }
            MetaInfo metaInfo2 = new MetaInfo();
            try {
                metaInfo2.icon = applicationIcon;
                metaInfo2.name = applicationInfo.loadLabel(packageManager).toString().trim();
                this.apkMetaInfoCache.put(str, metaInfo2);
                return metaInfo2;
            } catch (Throwable th) {
                Throwable th2 = th;
                metaInfo = metaInfo2;
                th = th2;
                th.printStackTrace();
                return metaInfo;
            }
        } catch (Throwable th3) {
            th = th3;
            th.printStackTrace();
            return metaInfo;
        }
    }
}
