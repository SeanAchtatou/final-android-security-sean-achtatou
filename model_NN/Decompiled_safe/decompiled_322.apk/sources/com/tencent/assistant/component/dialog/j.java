package com.tencent.assistant.component.dialog;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
final class j extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f656a;

    j(Activity activity) {
        this.f656a = activity;
    }

    public void onRightBtnClick() {
        Intent intent = new Intent("android.settings.SETTINGS");
        if (b.a(this.f656a, intent)) {
            this.f656a.startActivity(intent);
        } else {
            Toast.makeText(this.f656a, (int) R.string.dialog_not_found_activity, 0).show();
        }
    }

    public void onLeftBtnClick() {
    }

    public void onCancell() {
    }
}
