package com.tencent.assistant.component;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.qq.AppService.AstApp;
import com.qq.k.b;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.PhotoBackupNewActivity;
import com.tencent.assistant.component.WifiTransferMediaAdapter;
import com.tencent.assistant.component.txscrollview.TXGridView;
import com.tencent.assistant.d.j;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.connector.ipc.a;

/* compiled from: ProGuard */
public class PhotoBackupMiddleGridView extends FrameLayout implements UIEventListener {
    /* access modifiers changed from: private */
    public Activity activity;
    private View emptyView;
    boolean isFirstSet = false;
    /* access modifiers changed from: private */
    public PhotoBackupGridViewAdapter mImageAdapter = null;
    /* access modifiers changed from: private */
    public TXGridView mImageGridView = null;
    private WifiTransferMediaAdapter.IWifiTransferMediaAdapterListener mWifiTransferMediaAdapterListener = new ag(this);
    private long timeStamp;

    public PhotoBackupMiddleGridView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public PhotoBackupMiddleGridView(Context context) {
        super(context);
        init();
    }

    public void setActivity(Activity activity2) {
        this.activity = activity2;
        if (this.mImageAdapter != null) {
            this.mImageAdapter.loadData(true);
        }
    }

    public void onResume() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_NEW_PHOTO, this);
    }

    public void onPause() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_NEW_PHOTO, this);
    }

    public void onDestroy() {
        if (this.mImageAdapter != null) {
            this.mImageAdapter.removeRegister();
        }
    }

    public void updateCheckBox() {
        int[] a2;
        if (this.timeStamp > 0 && a.a().d() && (a2 = j.a(AstApp.i(), this.timeStamp)) != null && a2.length > 0) {
            this.mImageAdapter.checkIDs(a2);
            if (this.activity instanceof PhotoBackupNewActivity) {
                ((PhotoBackupNewActivity) this.activity).f(a2.length);
            }
        }
    }

    public void updateCheckBox(int[] iArr) {
        if (this.timeStamp > 0 && a.a().d() && iArr != null && iArr.length > 0) {
            this.mImageAdapter.checkIDs(iArr);
            if (this.activity instanceof PhotoBackupNewActivity) {
                ((PhotoBackupNewActivity) this.activity).f(iArr.length);
            }
        }
    }

    /* access modifiers changed from: private */
    public View createNoDataTipsView() {
        if (this.emptyView == null) {
            this.emptyView = LayoutInflater.from(getContext()).inflate((int) R.layout.activity_photo_backup_is_empty, (ViewGroup) null);
        }
        return this.emptyView;
    }

    public FrameLayout.LayoutParams getLP() {
        return new FrameLayout.LayoutParams(-1, -1);
    }

    private void init() {
        this.timeStamp = b.a(AstApp.i());
        this.mImageAdapter = new PhotoBackupGridViewAdapter(getContext());
        this.mImageAdapter.setListener(this.mWifiTransferMediaAdapterListener);
        this.mImageGridView = new TXGridView(getContext());
        this.mImageGridView.setNumColumns(4);
        this.mImageGridView.setSelector(new ColorDrawable(0));
        this.mImageGridView.setBackgroundColor(getContext().getResources().getColor(R.color.connection_bg));
        this.mImageGridView.setAdapter(this.mImageAdapter);
        addView(this.mImageGridView, 0, getLP());
        listener();
    }

    private void listener() {
        if (this.mImageGridView != null) {
            this.mImageGridView.setOnItemClickListener(new ah(this));
        }
    }

    public void clearChoices() {
        if (this.mImageAdapter != null) {
            this.mImageAdapter.clearChoice();
        }
    }

    public void checkIDs(int[] iArr) {
        if (this.mImageAdapter != null && iArr != null && iArr.length > 0) {
            this.mImageAdapter.checkIDs(iArr);
        }
    }

    public int[] getCheckedItemIds() {
        if (this.mImageAdapter != null) {
            return this.mImageAdapter.getCheckedItemIds();
        }
        return null;
    }

    public int getCheckedSize() {
        if (this.mImageAdapter != null) {
            return this.mImageAdapter.getCheckedSize();
        }
        return 0;
    }

    public int getCount() {
        if (this.mImageAdapter != null) {
            return this.mImageAdapter.getCount();
        }
        return 0;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_NEW_PHOTO /*1039*/:
                if (message.obj != null) {
                    updateCheckBox((int[]) message.obj);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
