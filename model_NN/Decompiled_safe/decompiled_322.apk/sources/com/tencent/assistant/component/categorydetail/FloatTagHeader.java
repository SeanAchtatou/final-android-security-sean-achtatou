package com.tencent.assistant.component.categorydetail;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.assistant.utils.by;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class FloatTagHeader extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private int f642a;
    private List<TagGroup> b;
    private Context c;
    /* access modifiers changed from: private */
    public List<TextView> d;
    private LinearLayout e;
    private LinearLayout[] f;
    private View.OnClickListener g;
    /* access modifiers changed from: private */
    public View.OnClickListener h;
    /* access modifiers changed from: private */
    public int i;
    private boolean j;
    private ImageView k;
    private View l;
    private SmoothShrinkRunnable m;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: android.widget.AbsListView.LayoutParams.<init>(int, int):void in method: com.tencent.assistant.component.categorydetail.FloatTagHeader.d():void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: android.widget.AbsListView.LayoutParams.<init>(int, int):void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    private void d() {
        /*
            r3 = this;
            r0 = 0
            r3.measure(r0, r0)
            android.widget.AbsListView$LayoutParams r0 = new android.widget.AbsListView$LayoutParams
            int r1 = r3.getMeasuredWidth()
            int r2 = r3.getMeasuredHeight()
            r0.<init>(r1, r2)
            android.view.View r1 = r3.l
            if (r1 != 0) goto L_0x0020
            android.view.View r1 = new android.view.View
            android.content.Context r2 = r3.getContext()
            r1.<init>(r2)
            r3.l = r1
        L_0x0020:
            android.view.View r1 = r3.l
            r1.setLayoutParams(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.component.categorydetail.FloatTagHeader.d():void");
    }

    public FloatTagHeader(Context context) {
        this(context, null);
    }

    public FloatTagHeader(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f642a = 0;
        this.b = new ArrayList();
        this.d = new ArrayList();
        this.i = 0;
        this.j = false;
        this.m = null;
        this.c = context;
        setOrientation(1);
        this.e = new LinearLayout(getContext());
        setPadding(0, by.a(this.c, 5.0f), 0, 0);
        addView(this.e, new LinearLayout.LayoutParams(-1, -2));
        this.k = new ImageView(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, by.a(this.c, 3.0f) + 1);
        this.k.setPadding(0, by.a(this.c, 3.0f), 0, 0);
        this.k.setImageResource(R.color.tag_header_underline);
        this.k.setLayoutParams(layoutParams);
        this.k.setVisibility(4);
        addView(this.k, layoutParams);
        a(0);
        this.l = new View(context);
    }

    public void setTagData(List<TagGroup> list, long j2) {
        int i2;
        if (list != null && !list.isEmpty()) {
            this.j = true;
            this.b.clear();
            this.b.add(new TagGroup(0, this.c.getResources().getString(R.string.all), null, 0));
            this.b.addAll(list);
            Iterator<TagGroup> it = this.b.iterator();
            while (true) {
                if (!it.hasNext()) {
                    i2 = 0;
                    break;
                }
                TagGroup next = it.next();
                if (next != null && next.a() == j2) {
                    i2 = this.b.indexOf(next);
                    break;
                }
            }
            a(i2);
            this.k.setVisibility(0);
        }
    }

    private void a(int i2) {
        b();
        a();
        c();
        selectTag(i2);
    }

    private void a() {
        int size;
        b bVar = new b(this);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (this.b.size() > 12) {
                size = 12;
            } else {
                size = this.b.size();
            }
            if (i3 < size) {
                if (this.b.get(i3) != null) {
                    TextView textView = new TextView(getContext());
                    textView.setText(this.b.get(i3).b());
                    textView.setTag(this.b.get(i3));
                    textView.setBackgroundResource(R.drawable.tag_btn_bg_selector);
                    textView.setTextColor(getResources().getColorStateList(R.drawable.tag_btn_txt_selector));
                    textView.setTextSize(2, 14.0f);
                    textView.setGravity(17);
                    textView.setMaxEms(4);
                    textView.setSingleLine();
                    textView.setEllipsize(TextUtils.TruncateAt.END);
                    textView.setOnClickListener(bVar);
                    textView.setTag(R.id.category_detail_btn_index, Integer.valueOf(i3));
                    int a2 = by.a(this.c, 4.0f);
                    textView.setPadding(a2, by.a(this.c, 8.0f), a2, by.a(this.c, 6.0f));
                    textView.setMinWidth(by.a(getContext(), 52.0f));
                    this.d.add(textView);
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private void b() {
        int i2 = 12;
        this.f642a = 0;
        if (this.b != null) {
            if (this.b.size() <= 12) {
                i2 = this.b.size();
            }
            if (i2 <= 0) {
                return;
            }
            if (i2 % 4 == 0) {
                this.f642a = 2;
            } else if (i2 % 3 == 0) {
                this.f642a = 1;
            } else if (i2 % 4 > i2 % 3) {
                this.f642a = 2;
            } else {
                this.f642a = 1;
            }
        }
    }

    private void c() {
        switch (this.f642a) {
            case 0:
                this.e.setVisibility(8);
                return;
            case 1:
                b(3);
                return;
            case 2:
                b(4);
                return;
            default:
                return;
        }
    }

    private void b(int i2) {
        int i3 = 12;
        if (this.d == null || i2 <= 0) {
            this.e.setVisibility(8);
            return;
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, by.a(getContext(), 35.0f), 1.0f);
        this.e.setOrientation(1);
        if (this.d.size() <= 12) {
            i3 = this.d.size();
        }
        int max = Math.max(0, ((i3 + i2) - 1) / i2);
        this.f = new LinearLayout[max];
        this.e.setOrientation(1);
        for (int i4 = 0; i4 < max; i4++) {
            this.f[i4] = new LinearLayout(getContext());
            this.f[i4].setOrientation(0);
            this.f[i4].setWeightSum((float) i2);
            for (int i5 = 0; i5 < i2; i5++) {
                int i6 = (i4 * i2) + i5;
                if (i6 < this.d.size()) {
                    LinearLayout linearLayout = new LinearLayout(getContext());
                    linearLayout.setOrientation(1);
                    linearLayout.setGravity(17);
                    linearLayout.addView(this.d.get(i6), new LinearLayout.LayoutParams(-2, -2));
                    this.f[i4].addView(linearLayout, layoutParams);
                } else {
                    this.f[i4].addView(new View(getContext()), layoutParams);
                }
            }
            this.e.addView(this.f[i4], new LinearLayout.LayoutParams(-1, -2));
        }
        this.e.setVisibility(0);
        setBackgroundResource(R.color.tag_header_bg);
        removeView(this.k);
        addView(this.k);
        d();
    }

    public void selectTag(int i2) {
        if (this.d != null && i2 >= 0 && i2 < this.d.size()) {
            if (this.g != null) {
                this.g.onClick(this.d.get(i2));
            }
            if (this.i != i2) {
                if (this.i >= 0 && this.i < this.d.size()) {
                    this.d.get(this.i).setSelected(false);
                }
                this.i = i2;
                this.d.get(this.i).setSelected(true);
            } else if (!this.d.get(this.i).isSelected()) {
                this.d.get(this.i).setSelected(true);
            }
        }
    }

    public boolean isTagExisted() {
        return this.j;
    }

    public void setTagClickListener(View.OnClickListener onClickListener) {
        this.h = onClickListener;
    }

    public void setTagSelectedListener(View.OnClickListener onClickListener) {
        this.g = onClickListener;
    }

    public View getEmptyHeader() {
        return this.l;
    }

    public int getTagHeaderHeight() {
        measure(0, 0);
        return getMeasuredHeight();
    }

    public int getEmptyHeaderHeight() {
        if (this.l != null) {
            return this.l.getHeight();
        }
        return 0;
    }

    public void hideEmptyHeader() {
        if (this.l != null) {
            this.l.setLayoutParams(new AbsListView.LayoutParams(-1, 0));
            this.l.postInvalidate();
        }
    }

    public void shrinkEmptyHeader(boolean z, SmoothShrinkListener smoothShrinkListener) {
        shrinkEmptyHeader(z, 0, smoothShrinkListener);
    }

    public void shrinkEmptyHeader(boolean z, int i2, SmoothShrinkListener smoothShrinkListener) {
        long j2 = 0;
        if (this.m == null || !this.m.isShinking()) {
            if (getEmptyHeaderHeight() <= 0) {
                this.m = new SmoothShrinkRunnable(this.l, 0, 0, 0);
            } else if (getEmptyHeaderHeight() <= i2) {
                c(getEmptyHeaderHeight() + 0);
                this.m = new SmoothShrinkRunnable(this.l, getEmptyHeaderHeight(), 0, 0);
            } else {
                long c2 = c(getEmptyHeaderHeight() - i2);
                View view = this.l;
                int emptyHeaderHeight = getEmptyHeaderHeight();
                if (z) {
                    j2 = c2;
                }
                this.m = new SmoothShrinkRunnable(view, emptyHeaderHeight, i2, j2);
            }
            this.m.setListener(smoothShrinkListener);
            if (this.l != null) {
                this.l.post(this.m);
            }
        }
    }

    private long c(int i2) {
        return (long) (((((float) i2) * 1.0f) / ((float) by.a(getContext(), 100.0f))) * 400.0f);
    }
}
