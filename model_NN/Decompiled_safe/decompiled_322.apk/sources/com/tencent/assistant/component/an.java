package com.tencent.assistant.component;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
class an extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TotalTabLayout f635a;

    an(TotalTabLayout totalTabLayout) {
        this.f635a = totalTabLayout;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        if (this.f635a.mContext == null) {
            return;
        }
        if (!(this.f635a.mContext instanceof Activity) || !((Activity) this.f635a.mContext).isFinishing()) {
            super.handleMessage(message);
            switch (message.what) {
                case 0:
                    this.f635a.startTranslateAnimation(message.arg1, message.arg2);
                    return;
                case 1:
                    this.f635a.layoutAnimImageOnResume();
                    return;
                default:
                    return;
            }
        }
    }
}
