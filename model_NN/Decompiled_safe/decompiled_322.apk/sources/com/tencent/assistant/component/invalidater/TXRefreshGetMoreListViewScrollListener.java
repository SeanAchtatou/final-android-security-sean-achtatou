package com.tencent.assistant.component.invalidater;

import android.view.View;
import android.widget.AbsListView;
import com.tencent.assistant.component.txscrollview.IScrollListener;

/* compiled from: ProGuard */
public class TXRefreshGetMoreListViewScrollListener extends CommonViewInvalidater implements IScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private int f671a = 0;

    public void onScroll(View view, int i, int i2, int i3) {
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        this.f671a = i;
        if (canHandleMessage()) {
            handleQueueMsg();
        }
    }

    /* access modifiers changed from: protected */
    public boolean canHandleMessage() {
        return this.f671a == 0 || this.f671a == 1;
    }
}
