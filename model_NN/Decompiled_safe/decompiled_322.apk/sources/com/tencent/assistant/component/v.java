package com.tencent.assistant.component;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;
import java.util.HashMap;

/* compiled from: ProGuard */
class v implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NormalErrorRecommendPage f724a;

    v(NormalErrorRecommendPage normalErrorRecommendPage) {
        this.f724a = normalErrorRecommendPage;
    }

    public void onClick(View view) {
        if (30 == this.f724a.currentState) {
            Intent intent = new Intent("android.settings.SETTINGS");
            if (b.a(this.f724a.getContext(), intent)) {
                this.f724a.getContext().startActivity(intent);
            } else {
                Toast.makeText(this.f724a.getContext(), (int) R.string.dialog_not_found_activity, 0).show();
            }
        } else if (50 == this.f724a.currentState || 70 == this.f724a.currentState) {
            new HashMap();
            int activityPageId = this.f724a.getActivityPageId();
            if (this.f724a.context instanceof BaseActivity) {
                BaseActivity baseActivity = (BaseActivity) this.f724a.context;
                if (activityPageId == 2000) {
                    activityPageId = baseActivity.f();
                }
                baseActivity.m();
            }
            l.a(new STInfoV2(activityPageId, "03_001", activityPageId, STConst.ST_DEFAULT_SLOT, 200));
            this.f724a.context.startActivity(new Intent(this.f724a.context, MainActivity.class));
        } else if (this.f724a.listener != null) {
            this.f724a.listener.onClick(view);
        }
    }
}
