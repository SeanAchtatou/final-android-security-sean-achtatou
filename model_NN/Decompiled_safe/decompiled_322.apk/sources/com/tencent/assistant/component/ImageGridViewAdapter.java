package com.tencent.assistant.component;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.WifiTransferMediaAdapter;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.localres.LocalImageLoader;
import com.tencent.assistant.localres.LocalMediaManager;
import com.tencent.assistant.localres.model.LocalImage;

/* compiled from: ProGuard */
public class ImageGridViewAdapter extends WifiTransferMediaAdapter<LocalImage> {
    public ImageGridViewAdapter(Context context) {
        super(context);
        this.mMediaLoader = (LocalImageLoader) LocalMediaManager.getInstance().getLoader(1);
        this.mMediaLoader.registerListener(this);
    }

    /* access modifiers changed from: protected */
    public View createConvertView() {
        View inflate = View.inflate(this.mContext, R.layout.wifitransfer_pic_gridview_item, null);
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.mImageView = (TXImageView) inflate.findViewById(R.id.gridview_item_imageview);
        viewHolder.mCheckBox = (ImageView) inflate.findViewById(R.id.gridview_item_checkbox);
        viewHolder.mTvCheckBox = (TextView) inflate.findViewById(R.id.checkbox);
        inflate.setTag(viewHolder);
        return inflate;
    }

    /* access modifiers changed from: protected */
    public void fillConvertView(View view, LocalImage localImage, int i) {
        ((ViewHolder) view.getTag()).mImageView.updateImageView(localImage.thumbnailPath, R.drawable.pic_default_pic, TXImageView.TXImageViewType.LOCAL_LARGER_IMAGE_THUMBNAIL);
    }

    /* compiled from: ProGuard */
    public class ViewHolder extends WifiTransferMediaAdapter.BaseViewHolder {
        public ViewHolder() {
            super();
        }
    }
}
