package com.tencent.assistant.component.dialog;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.t;

/* compiled from: ProGuard */
public class TwoButtonDialogView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f647a;
    private TextView b;
    private View c;
    private TextView d;
    private View e;
    private RelativeLayout f;
    private TextView g;
    private TextView h;
    private Button i;
    private RelativeLayout j;
    private boolean k;

    public TwoButtonDialogView(Context context) {
        this(context, null);
    }

    public TwoButtonDialogView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.k = true;
        this.f647a = LayoutInflater.from(context);
        a();
    }

    public boolean isInflateSucc() {
        return this.k;
    }

    private void a() {
        this.k = true;
        try {
            this.f647a.inflate((int) R.layout.dialog_2button, this);
            this.b = (TextView) findViewById(R.id.title);
            this.c = findViewById(R.id.titleLayout);
            this.d = (TextView) findViewById(R.id.msg);
            this.e = findViewById(R.id.msgLayout);
            this.i = (Button) findViewById(R.id.nagitive_btn);
            this.f = (RelativeLayout) findViewById(R.id.positive_layout);
            this.g = (TextView) findViewById(R.id.positive_btn_text);
            this.h = (TextView) findViewById(R.id.positive_btn_tail_text);
            this.j = (RelativeLayout) findViewById(R.id.extraLayout);
        } catch (Throwable th) {
            this.k = false;
            t.a().b();
        }
    }

    public void setTitleAndMsg(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            this.b.setText(str);
        }
        if (!TextUtils.isEmpty(str2)) {
            this.d.setText(str2);
        }
    }

    public void setHasTitle(boolean z) {
        if (!z) {
            this.c.setVisibility(8);
        } else {
            this.c.setVisibility(0);
        }
    }

    public void setButton(String str, String str2, View.OnClickListener onClickListener, View.OnClickListener onClickListener2) {
        if (!TextUtils.isEmpty(str)) {
            this.i.setText(str);
        }
        if (!TextUtils.isEmpty(str2)) {
            this.g.setText(str2);
        }
        if (onClickListener != null) {
            this.i.setOnClickListener(onClickListener);
        }
        if (onClickListener2 != null) {
            this.f.setOnClickListener(onClickListener2);
        }
    }

    public void setRightButtonTailText(String str) {
        this.h.setText(str);
    }

    public void setButtonStyle(boolean z, int i2, int i3) {
        if (z) {
            this.i.setTextColor(getResources().getColor(i2));
            this.i.setBackgroundDrawable(getResources().getDrawable(i3));
            return;
        }
        this.g.setTextColor(getResources().getColor(i2));
        this.f.setBackgroundDrawable(getResources().getDrawable(i3));
    }

    public void addExtraMsgView(View view) {
        if (view != null) {
            ViewParent parent = view.getParent();
            if (parent != null && (parent instanceof ViewGroup)) {
                ((ViewGroup) view.getParent()).removeView(view);
            }
            this.j.setVisibility(0);
            this.j.addView(view);
        }
    }
}
