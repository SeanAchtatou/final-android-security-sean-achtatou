package com.tencent.assistant.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.ah;
import java.util.HashMap;

/* compiled from: ProGuard */
class s extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocalPkgSizeTextView f686a;

    private s(LocalPkgSizeTextView localPkgSizeTextView) {
        this.f686a = localPkgSizeTextView;
    }

    /* synthetic */ s(LocalPkgSizeTextView localPkgSizeTextView, r rVar) {
        this(localPkgSizeTextView);
    }

    public void onGetPkgSizeFinish(LocalApkInfo localApkInfo) {
        if (this.f686a.mPkgName != null && localApkInfo.mPackageName != null) {
            if (this.f686a.invalidater != null) {
                ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(0);
                HashMap hashMap = new HashMap();
                hashMap.put("NAME", localApkInfo.mPackageName);
                hashMap.put("SIZE", Long.valueOf(localApkInfo.occupySize));
                viewInvalidateMessage.params = hashMap;
                viewInvalidateMessage.target = this.f686a.invalidateHandler;
                this.f686a.invalidater.dispatchMessage(viewInvalidateMessage);
                return;
            }
            String str = localApkInfo.mPackageName;
            long j = localApkInfo.occupySize;
            if (this.f686a.mPkgName.equals(localApkInfo.mPackageName)) {
                ah.a().post(new t(this, str, j));
            }
        }
    }
}
