package com.tencent.assistant.activity;

import android.util.Log;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.d.a;
import java.util.ArrayList;

/* compiled from: ProGuard */
class bx implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PhotoBackupNewActivity f435a;

    bx(PhotoBackupNewActivity photoBackupNewActivity) {
        this.f435a = photoBackupNewActivity;
    }

    public void a() {
        Log.d("PhotoBackupNewActivity", "usbConnected");
        this.f435a.x();
    }

    public void b() {
        Log.d("PhotoBackupNewActivity", "wifiConnected");
        this.f435a.y();
    }

    public void c() {
        Log.d("PhotoBackupNewActivity", "wifiDisable");
        this.f435a.c(2);
    }

    public void a(int i) {
        Log.d("PhotoBackupNewActivity", "requestFailed： " + i);
        if (!com.tencent.connector.ipc.a.a().d()) {
            this.f435a.d(4);
            Toast.makeText(AstApp.i(), (int) R.string.photo_backup_get_failed, 1).show();
            this.f435a.finish();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.PhotoBackupNewActivity.a(com.tencent.assistant.activity.PhotoBackupNewActivity, boolean):boolean
     arg types: [com.tencent.assistant.activity.PhotoBackupNewActivity, int]
     candidates:
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.PhotoBackupNewActivity.a(com.tencent.assistant.activity.PhotoBackupNewActivity, boolean):boolean */
    public void a(Object obj) {
        Log.d("PhotoBackupNewActivity", "requestSuccess newtag:");
        if (com.tencent.connector.ipc.a.a().d()) {
            if (this.f435a.R) {
                boolean unused = this.f435a.R = false;
            } else {
                return;
            }
        }
        this.f435a.d(4);
        ArrayList arrayList = (ArrayList) obj;
        if (arrayList == null || arrayList.size() == 0) {
            this.f435a.c(2);
            return;
        }
        this.f435a.c(1);
        this.f435a.z.updateList(arrayList);
    }

    public void b(int i) {
        Log.d("PhotoBackupNewActivity", "invitePcFailed:" + i);
        if (!com.tencent.connector.ipc.a.a().d()) {
            this.f435a.u();
            this.f435a.D();
        }
    }

    public void b(Object obj) {
        Log.d("PhotoBackupNewActivity", "invitePcSuccess:");
    }
}
