package com.tencent.assistant.activity;

import android.content.ComponentName;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Message;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.ViewStub;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.ad;
import com.tencent.assistant.module.callback.h;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.component.AppStateButtonV6;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class PanelManagerActivity extends BaseActivity implements UIEventListener, NetworkMonitor.ConnectivityChangeListener, h {
    private int A = 0;
    protected String n = "03_001";
    private SecondNavigationTitleViewV5 u = null;
    private AppStateButtonV6 v;
    private final String w = "000116083231313533393331";
    /* access modifiers changed from: private */
    public SimpleAppModel x;
    private ad y = new ad();
    private TextView z;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        if ((e.a("com.tencent.qlauncher.lite") && b("com.tencent.qlauncher.lite") && e.f(AstApp.i(), "com.tencent.qlauncher.lite")) || (e.a("com.tencent.qlauncher.lite") && b("com.tencent.qlauncher.lite") && e.f(AstApp.i(), "com.tencent.qlauncher.lite"))) {
            this.A = 1;
        }
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_manager_panel);
        t.a().a(this);
        w();
        try {
            if (this.A == 1) {
                ((ViewStub) findViewById(R.id.yes_install)).inflate();
                t();
                return;
            }
            ((ViewStub) findViewById(R.id.no_install)).inflate();
            u();
            v();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int f() {
        if (this.A == 0) {
            return STConst.ST_PAGE_PANEL_CLEAR_INTRO;
        }
        return STConst.ST_PAGE_PANEL_CLEAR_FINISH;
    }

    /* access modifiers changed from: package-private */
    public boolean b(String str) {
        IntentFilter intentFilter = new IntentFilter("android.intent.action.MAIN");
        intentFilter.addCategory("android.intent.category.HOME");
        ArrayList arrayList = new ArrayList();
        arrayList.add(intentFilter);
        ArrayList<ComponentName> arrayList2 = new ArrayList<>();
        getPackageManager().getPreferredActivities(arrayList, arrayList2, null);
        for (ComponentName packageName : arrayList2) {
            if (str.equals(packageName.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        t.a().b(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.u != null) {
            this.u.l();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.u != null) {
            this.u.m();
        }
    }

    private void t() {
        this.z = (TextView) findViewById(R.id.footer_title_txt);
        this.z.setText(Html.fromHtml("<html ><font color='#a6a6a6'>换个主题皮肤，让手机变的美美的吧，</font><font color='#00adff'>去看看></font></html>"));
        this.z.setGravity(3);
        this.z.setOnClickListener(new bo(this));
    }

    private void u() {
        this.v = (AppStateButtonV6) findViewById(R.id.state_app_btn);
        this.v.a((int) getResources().getDimension(R.dimen.app_detail_float_bar_btn_height));
        this.v.b(16);
        this.v.setOnClickListener(new bp(this));
    }

    private void v() {
        if (this.x == null) {
            this.x = new SimpleAppModel();
            this.x.c = "com.tencent.qlauncher.lite";
            this.x.ac = "000116083231313533393331";
        }
        this.v.a(this.x);
        this.y.a(this.x);
        this.y.register(this);
    }

    private void w() {
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.u.d();
        this.u.b(getApplicationContext().getString(R.string.panel_clear_title));
        this.u.c(new bq(this));
    }

    public void handleUIEvent(Message message) {
        ViewStub viewStub;
        ViewStub viewStub2;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_INSTALL /*1011*/:
            case 1013:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC /*1026*/:
                String str = Constants.STR_EMPTY;
                if (message.obj != null && (message.obj instanceof String)) {
                    String str2 = (String) message.obj;
                    str = (String) message.obj;
                } else if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean)) {
                    InstallUninstallTaskBean installUninstallTaskBean = (InstallUninstallTaskBean) message.obj;
                    String str3 = installUninstallTaskBean.downloadTicket;
                    str = installUninstallTaskBean.packageName;
                }
                if ((TextUtils.equals("com.tencent.qlauncher.lite", str) || TextUtils.equals("com.tencent.qlauncher.lite", str)) && (viewStub2 = (ViewStub) findViewById(R.id.yes_install)) != null) {
                    viewStub2.inflate();
                    t();
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_UNINSTALL /*1012*/:
                if (message.obj != null && (message.obj instanceof String)) {
                    String str4 = (String) message.obj;
                    if ((TextUtils.equals("com.tencent.qlauncher.lite", str4) || TextUtils.equals("com.tencent.qlauncher.lite", str4)) && (viewStub = (ViewStub) findViewById(R.id.no_install)) != null) {
                        viewStub.inflate();
                        u();
                        v();
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onConnected(APN apn) {
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }

    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        DownloadInfo downloadInfo = null;
        if (TextUtils.equals(appSimpleDetail.a(), "com.tencent.qlauncher.lite")) {
            this.x = k.a(appSimpleDetail);
            this.v.setVisibility(0);
            this.v.a(this.x);
            DownloadInfo a2 = DownloadProxy.a().a(this.x);
            StatInfo a3 = a.a(STInfoBuilder.buildSTInfo(this, this.x, STConst.ST_DEFAULT_SLOT, 200, null));
            if (a2 == null || !a2.needReCreateInfo(this.x)) {
                downloadInfo = a2;
            } else {
                DownloadProxy.a().b(a2.downloadTicket);
            }
            if (downloadInfo == null) {
                downloadInfo = DownloadInfo.createDownloadInfo(this.x, a3);
            } else {
                downloadInfo.updateDownloadInfoStatInfo(a3);
            }
            AppConst.AppState d = k.d(this.x);
            if (d == AppConst.AppState.DOWNLOADED) {
                this.v.a(getResources().getString(R.string.appbutton_install));
            } else if (d == AppConst.AppState.DOWNLOAD || d == AppConst.AppState.UPDATE || d == AppConst.AppState.ILLEGAL) {
                this.v.a(a(downloadInfo));
                if (d == AppConst.AppState.ILLEGAL) {
                    this.v.a(AppConst.AppState.DOWNLOAD);
                }
            }
        }
    }

    public void onGetAppInfoFail(int i, int i2) {
    }

    private CharSequence a(DownloadInfo downloadInfo) {
        String format;
        boolean z2 = ApkResourceManager.getInstance().getInstalledApkInfo(downloadInfo.packageName) != null;
        if (z2 && downloadInfo.isSslUpdate()) {
            format = String.format(getResources().getString(R.string.mobile_rubbish_clear_btn_update), at.a(downloadInfo.sllFileSize));
        } else if (!z2 || downloadInfo.isUpdate != 1) {
            format = String.format(getResources().getString(R.string.mobile_rubbish_clear_btn_download), at.a(downloadInfo.fileSize));
        } else {
            format = String.format(getResources().getString(R.string.mobile_rubbish_clear_btn_update), at.a(downloadInfo.fileSize));
        }
        SpannableString spannableString = new SpannableString(format);
        spannableString.setSpan(new AbsoluteSizeSpan(14, true), 4, format.length(), 33);
        return spannableString;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.pangu.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.k.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.m, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    /* access modifiers changed from: private */
    public void x() {
        DownloadInfo downloadInfo = null;
        DownloadInfo a2 = DownloadProxy.a().a(this.x);
        StatInfo a3 = a.a(STInfoBuilder.buildSTInfo(this, this.x, STConst.ST_DEFAULT_SLOT, 200, null));
        if (a2 == null || !a2.needReCreateInfo(this.x)) {
            downloadInfo = a2;
        } else {
            DownloadProxy.a().b(a2.downloadTicket);
        }
        if (downloadInfo == null) {
            downloadInfo = DownloadInfo.createDownloadInfo(this.x, a3, this.v);
        } else {
            downloadInfo.updateDownloadInfoStatInfo(a3);
        }
        switch (bs.f430a[k.a(downloadInfo, false, false).ordinal()]) {
            case 1:
            case 2:
                com.tencent.pangu.download.a.a().a(downloadInfo);
                return;
            case 3:
            case 4:
                com.tencent.pangu.download.a.a().b(downloadInfo.downloadTicket);
                return;
            case 5:
                com.tencent.pangu.download.a.a().b(downloadInfo);
                return;
            case 6:
                if (!downloadInfo.isDownloadFileExist()) {
                    AppConst.TwoBtnDialogInfo b = b(downloadInfo);
                    if (b != null) {
                        DialogUtils.show2BtnDialog(b);
                        return;
                    }
                    return;
                }
                com.tencent.pangu.download.a.a().d(downloadInfo);
                return;
            case 7:
                com.tencent.pangu.download.a.a().c(downloadInfo);
                return;
            case 8:
            case 9:
                com.tencent.pangu.download.a.a().a(downloadInfo);
                return;
            case 10:
                Toast.makeText(this, (int) R.string.tips_slicent_install, 0).show();
                return;
            case 11:
                Toast.makeText(this, (int) R.string.tips_slicent_uninstall, 0).show();
                return;
            default:
                return;
        }
    }

    private AppConst.TwoBtnDialogInfo b(DownloadInfo downloadInfo) {
        br brVar = new br(this, downloadInfo);
        brVar.hasTitle = true;
        brVar.titleRes = getResources().getString(R.string.down_page_dialog_title);
        brVar.contentRes = getResources().getString(R.string.down_page_dialog_content);
        brVar.lBtnTxtRes = getResources().getString(R.string.down_page_dialog_left_del);
        brVar.rBtnTxtRes = getResources().getString(R.string.down_page_dialog_right_down);
        return brVar;
    }
}
