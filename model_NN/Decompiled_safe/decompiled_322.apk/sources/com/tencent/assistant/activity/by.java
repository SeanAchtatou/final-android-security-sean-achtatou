package com.tencent.assistant.activity;

import com.tencent.assistant.component.PCListLinearLayout;
import com.tencent.assistant.model.OnlinePCListItemModel;
import com.tencent.connector.ipc.a;

/* compiled from: ProGuard */
class by implements PCListLinearLayout.PCListClickCallback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PhotoBackupNewActivity f436a;

    by(PhotoBackupNewActivity photoBackupNewActivity) {
        this.f436a = photoBackupNewActivity;
    }

    public void call(OnlinePCListItemModel onlinePCListItemModel) {
        this.f436a.d(1);
        if (onlinePCListItemModel == null) {
            this.f436a.v();
            return;
        }
        String g = a.a().g();
        if (g != null && g.equals(onlinePCListItemModel.f936a)) {
            return;
        }
        if (!a.a().d()) {
            this.f436a.b(onlinePCListItemModel.b, onlinePCListItemModel.f936a);
            a.a().a(onlinePCListItemModel.f936a);
            return;
        }
        this.f436a.c(5);
    }
}
