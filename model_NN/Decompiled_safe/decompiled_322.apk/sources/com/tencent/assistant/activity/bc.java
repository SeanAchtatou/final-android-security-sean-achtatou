package com.tencent.assistant.activity;

import android.os.Message;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.List;

/* compiled from: ProGuard */
class bc implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstalledAppManagerActivity f414a;

    bc(InstalledAppManagerActivity installedAppManagerActivity) {
        this.f414a = installedAppManagerActivity;
    }

    public void run() {
        this.f414a.n.registerApkResCallback(this.f414a.w);
        List<LocalApkInfo> localApkInfos = ApkResourceManager.getInstance().getLocalApkInfos();
        if (localApkInfos != null && localApkInfos.size() > 0) {
            Message obtainMessage = this.f414a.X.obtainMessage();
            obtainMessage.obj = localApkInfos;
            obtainMessage.what = 10701;
            this.f414a.X.removeMessages(10701);
            this.f414a.X.sendMessage(obtainMessage);
        }
    }
}
