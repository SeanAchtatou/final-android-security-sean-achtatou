package com.tencent.assistant.protocol.a.b;

import android.content.Context;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.n;

/* compiled from: ProGuard */
public class a extends com.tencent.assistant.protocol.a.a {
    public Context a() {
        return AstApp.i();
    }

    public int b() {
        return n.a();
    }

    public boolean c() {
        return Global.hasInit();
    }

    public void d() {
        Global.init();
    }

    public long e() {
        return (long) m.a().a("auth_protocol_update_period", 3600);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean f() {
        return m.a().a("auth_protocol_log_st", false);
    }

    public int g() {
        return m.a().a("auth_protocol_fail_nottry_period", 60);
    }
}
