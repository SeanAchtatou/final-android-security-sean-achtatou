package com.tencent.assistant.protocol.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class SmartCardType implements Serializable {
    public static final SmartCardType A = new SmartCardType(26, 30, "SMART_CARD_TYPE_KA_CLIENT_ADVERTISE");
    public static final SmartCardType B = new SmartCardType(27, 31, "SMART_CARD_TYPE_LONG_TAIL_ADVERTISE");
    public static final SmartCardType C = new SmartCardType(28, 32, "SMART_CARD_TYPE_PACKAGE");
    public static final SmartCardType D = new SmartCardType(29, 33, "SMART_CARD_TYPE_TASK_MARKET");
    public static final SmartCardType E = new SmartCardType(30, 34, "SMART_CARD_TYPE_TEMPLATE1");
    public static final SmartCardType F = new SmartCardType(31, 35, "SMART_CARD_TYPE_TEMPLATE2");
    public static final SmartCardType G = new SmartCardType(32, 36, "SMART_CARD_TYPE_TEMPLATE3");
    public static final SmartCardType H = new SmartCardType(33, 37, "SMART_CARD_TYPE_TAG");
    public static final SmartCardType I = new SmartCardType(34, 38, "SMART_CARD_TYPE_INTEREST");
    public static final SmartCardType J = new SmartCardType(35, 39, "SMART_CARD_TYPE_FT");
    public static final SmartCardType K = new SmartCardType(36, 40, "SMART_CARD_TYPE_CONTENT_AGGREGATION_SIMPLE");
    public static final SmartCardType L = new SmartCardType(37, 41, "SMART_CARD_TYPE_CONTENT_AGGREGATION_COMPLEX");
    public static final SmartCardType M = new SmartCardType(38, 42, "SMART_CARD_TYPE_GAME_REG");
    static final /* synthetic */ boolean N = (!SmartCardType.class.desiredAssertionStatus());
    private static SmartCardType[] O = new SmartCardType[39];

    /* renamed from: a  reason: collision with root package name */
    public static final SmartCardType f1529a = new SmartCardType(0, 0, "SMART_CARD_TYPE_OP");
    public static final SmartCardType b = new SmartCardType(1, 1, "SMART_CARD_TYPE_LIBAO");
    public static final SmartCardType c = new SmartCardType(2, 2, "SMART_CARD_TYPE_INSTALL");
    public static final SmartCardType d = new SmartCardType(3, 3, "SMART_CARD_TYPE_SMART_UPDATE");
    public static final SmartCardType e = new SmartCardType(4, 4, "SMART_CARD_TYPE_RECOMMENDATION_UPDATE");
    public static final SmartCardType f = new SmartCardType(5, 5, "SMART_CARD_TYPE_RECOMMENDATION_PERSONALIZED");
    public static final SmartCardType g = new SmartCardType(6, 6, "SMART_CARD_TYPE_YYB_UPGRADE");
    public static final SmartCardType h = new SmartCardType(7, 7, "SMART_CARD_TYPE_FRIENDS");
    public static final SmartCardType i = new SmartCardType(8, 8, "SMART_CARD_TYPE_LBS");
    public static final SmartCardType j = new SmartCardType(9, 9, "SMART_CARD_TYPE_TODAY_LBS");
    public static final SmartCardType k = new SmartCardType(10, 10, "SMART_CARD_TYPE_FRIENDS_HOT");
    public static final SmartCardType l = new SmartCardType(11, 11, "SMART_CARD_TYPE_FRIENDS_POP");
    public static final SmartCardType m = new SmartCardType(12, 12, "SMART_CARD_TYPE_TAG_RANK");
    public static final SmartCardType n = new SmartCardType(13, 13, "SMART_CARD_TYPE_USER_PROP");
    public static final SmartCardType o = new SmartCardType(14, 14, "SMART_CARD_TYPE_TODAY_HOT");
    public static final SmartCardType p = new SmartCardType(15, 15, "SMART_CARD_TYPE_WEEK_HOT");
    public static final SmartCardType q = new SmartCardType(16, 16, "SMART_CARD_TYPE_RESERVATION");
    public static final SmartCardType r = new SmartCardType(17, 17, "SMART_CARD_TYPE_GRAB_NUMBER");
    public static final SmartCardType s = new SmartCardType(18, 22, "SMART_CARD_TYPE_HOT_WORD");
    public static final SmartCardType t = new SmartCardType(19, 23, "SMART_CARD_TYPE_PLAYER_SHOW");
    public static final SmartCardType u = new SmartCardType(20, 24, "SMART_CARD_TYPE_CPA_ADVERTISE");
    public static final SmartCardType v = new SmartCardType(21, 25, "SMART_CARD_TYPE_CPA_CASH_ADVERTISE");
    public static final SmartCardType w = new SmartCardType(22, 26, "SMART_CARD_TYPE_NEW_GAME");
    public static final SmartCardType x = new SmartCardType(23, 27, "SMART_CARD_TYPE_TAMPLATE_TOPIC");
    public static final SmartCardType y = new SmartCardType(24, 28, "SMART_CARD_TYPE_VIDEO");
    public static final SmartCardType z = new SmartCardType(25, 29, "SMART_CARD_TYPE_SPECIAL_SUBJECT");
    private int P;
    private String Q = new String();

    public String toString() {
        return this.Q;
    }

    private SmartCardType(int i2, int i3, String str) {
        this.Q = str;
        this.P = i3;
        O[i2] = this;
    }
}
