package com.tencent.assistant.protocol.jce;

import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import java.io.Serializable;

/* compiled from: ProGuard */
public final class JceCmd implements Serializable {
    public static final JceCmd A = new JceCmd(26, 27, "GetActionTask");
    public static final JceCmd B = new JceCmd(27, 28, "CheckActUUid");
    public static final JceCmd C = new JceCmd(28, 29, "SyncVerifyRst");
    public static final JceCmd D = new JceCmd(29, 30, "GetUinByOpenId");
    public static final JceCmd E = new JceCmd(30, 31, "GetAppSimpleDetail");
    public static final JceCmd F = new JceCmd(31, 32, "GetCollection");
    public static final JceCmd G = new JceCmd(32, 33, "CollectionAction");
    public static final JceCmd H = new JceCmd(33, 34, "GetUserInfo");
    public static final JceCmd I = new JceCmd(34, 35, "Share");
    public static final JceCmd J = new JceCmd(35, 36, "GetDomainCapability");
    public static final JceCmd K = new JceCmd(36, 37, "GetOpRegularInfo");
    public static final JceCmd L = new JceCmd(37, 38, "GetRecommendAppList");
    public static final JceCmd M = new JceCmd(38, 39, "GetRecommendTabPage");
    public static final JceCmd N = new JceCmd(39, 40, "GetAutoDownload");
    public static final JceCmd O = new JceCmd(40, 41, "SetUserProfile");
    public static final JceCmd P = new JceCmd(41, 42, "GetSmartCards");
    public static final JceCmd Q = new JceCmd(42, 43, "InquireWhiteList");
    public static final JceCmd R = new JceCmd(43, 44, "GetUnreadMsgNum");
    public static final JceCmd S = new JceCmd(44, 45, "GetTheHottestAppList");
    public static final JceCmd T = new JceCmd(45, 46, "GuessYouLike");
    public static final JceCmd U = new JceCmd(46, 47, "GetDesktopShortcut");
    public static final JceCmd V = new JceCmd(47, 48, "SearchAdvancedHotWords");
    public static final JceCmd W = new JceCmd(48, 49, "GetOneMoreApp");
    public static final JceCmd X = new JceCmd(49, 50, "GetBackupApps");
    public static final JceCmd Y = new JceCmd(50, 51, "UploadControl");
    public static final JceCmd Z = new JceCmd(51, 52, "ModifyAppComment");

    /* renamed from: a  reason: collision with root package name */
    public static final JceCmd f1398a = new JceCmd(0, 0, "Empty");
    public static final JceCmd aA = new JceCmd(78, EventDispatcherEnum.CACHE_EVENT_START, "GetUserTagInfoList");
    public static final JceCmd aB = new JceCmd(79, EventDispatcherEnum.CACHE_EVENT_PIC_LOAD_SUCCESS, "PraiseComment");
    public static final JceCmd aC = new JceCmd(80, 3002, "PraiseReply");
    public static final JceCmd aD = new JceCmd(81, 3003, "GetReplyList");
    public static final JceCmd aE = new JceCmd(82, 200, "Auth");
    public static final JceCmd aF = new JceCmd(83, STConstAction.ACTION_HIT_OPEN, "GetMiniGameCmd");
    public static final JceCmd aG = new JceCmd(84, STConstAction.ACTION_HIT_AUTO_OPEN, "GetQubeThemeCmd");
    public static final JceCmd aH = new JceCmd(85, EventDispatcherEnum.CACHE_EVENT_END, "CFTGetOneMoreApp");
    public static final JceCmd aI = new JceCmd(86, 4001, "SetBatchAppUpload");
    public static final JceCmd aJ = new JceCmd(87, STConst.ST_PAGE_EBOOK, "CftGetAppList");
    public static final JceCmd aK = new JceCmd(88, 4003, "CftGetNavigation");
    public static final JceCmd aL = new JceCmd(89, 1000, "MultiCmd");
    public static final JceCmd aM = new JceCmd(90, 5001, "GftGetAppList");
    public static final JceCmd aN = new JceCmd(91, 5002, "GftGetAppCategory");
    public static final JceCmd aO = new JceCmd(92, 5003, "GftGetSubject");
    public static final JceCmd aP = new JceCmd(93, EventDispatcherEnum.CONNECTION_EVENT_WIFI_CHANGE, "GftGetRecommendTabPage");
    public static final JceCmd aQ = new JceCmd(94, EventDispatcherEnum.CONNECTION_EVENT_SERVICE_DESTROY, "GftGetSmartCards");
    public static final JceCmd aR = new JceCmd(95, EventDispatcherEnum.CONNECTION_EVENT_USB_PLUGIN, "GftGetNavigation");
    public static final JceCmd aS = new JceCmd(96, EventDispatcherEnum.CONNECTION_EVENT_LOGIN, "GftGetTreasureBoxSetting");
    public static final JceCmd aT = new JceCmd(97, EventDispatcherEnum.CONNECTION_EVENT_PC_PING, "GftGetGameGiftFlag");
    public static final JceCmd aU = new JceCmd(98, EventDispatcherEnum.CONNECTION_EVENT_CANCEL_LOGIN, "GftReportGameGiftChecked");
    public static final JceCmd aV = new JceCmd(99, EventDispatcherEnum.CONNECTION_EVENT_WCS_START, "GftGetHomeAppList");
    public static final JceCmd aW = new JceCmd(100, EventDispatcherEnum.CONNECTION_EVENT_WCS_STOP, "GftGetMyDesktop");
    public static final JceCmd aX = new JceCmd(TXTabBarLayout.TABITEM_TIPS_TEXT_ID, 6000, "GetDesktopAppListForOEM");
    static final /* synthetic */ boolean aY = (!JceCmd.class.desiredAssertionStatus());
    private static JceCmd[] aZ = new JceCmd[102];
    public static final JceCmd aa = new JceCmd(52, 53, "GetNavigation");
    public static final JceCmd ab = new JceCmd(53, 54, "GetAppHotFriends");
    public static final JceCmd ac = new JceCmd(54, 55, "AnswerAppComment");
    public static final JceCmd ad = new JceCmd(55, 56, "GetCommentDetail");
    public static final JceCmd ae = new JceCmd(56, 57, "GetDiscover");
    public static final JceCmd af = new JceCmd(57, 58, "GetUnreadCount");
    public static final JceCmd ag = new JceCmd(58, 59, "GetSelectedCommentList");
    public static final JceCmd ah = new JceCmd(59, 60, "GetAppTagInfoList");
    public static final JceCmd ai = new JceCmd(60, 61, "GetAppFromTag");
    public static final JceCmd aj = new JceCmd(61, 62, "GetMyFriendsUsing");
    public static final JceCmd ak = new JceCmd(62, 63, "GetUserActivity");
    public static final JceCmd al = new JceCmd(63, 64, "GetUserMechanisedData");
    public static final JceCmd am = new JceCmd(64, 65, "GetExplicitHotWords");
    public static final JceCmd an = new JceCmd(65, 66, "GetMyFriendCommentCount");
    public static final JceCmd ao = new JceCmd(66, 67, "SetBookReadMark");
    public static final JceCmd ap = new JceCmd(67, 68, "GetWelcomePageInfo");
    public static final JceCmd aq = new JceCmd(68, 69, "GetAppTypeList");
    public static final JceCmd ar = new JceCmd(69, 70, "FreeWifiEvent");
    public static final JceCmd as = new JceCmd(70, 71, "FreeWifiAccount");
    public static final JceCmd at = new JceCmd(71, 72, "FreeWifiAuth");
    public static final JceCmd au = new JceCmd(72, 73, "GetManageInfoList");
    public static final JceCmd av = new JceCmd(73, 74, "FreeWifiTimeAdd");
    public static final JceCmd aw = new JceCmd(74, 75, "FreeWifiStatus");
    public static final JceCmd ax = new JceCmd(75, 77, "GetPhoneUserAppList");
    public static final JceCmd ay = new JceCmd(76, 78, "SourceCheck");
    public static final JceCmd az = new JceCmd(77, 2000, "GetPopupNecessary");
    public static final JceCmd b = new JceCmd(1, 1, "GetAppList");
    public static final JceCmd c = new JceCmd(2, 2, "GetAppCategory");
    public static final JceCmd d = new JceCmd(3, 3, "GetAppDetail");
    public static final JceCmd e = new JceCmd(4, 4, "Search");
    public static final JceCmd f = new JceCmd(5, 5, "Suggest");
    public static final JceCmd g = new JceCmd(6, 6, "GetCommentList");
    public static final JceCmd h = new JceCmd(7, 7, "CommentApp");
    public static final JceCmd i = new JceCmd(8, 8, "GetAppUpdate");
    public static final JceCmd j = new JceCmd(9, 9, "GetSearchHotWords");
    public static final JceCmd k = new JceCmd(10, 10, "ReportApp");
    public static final JceCmd l = new JceCmd(11, 11, "GetFirstReleaseAppList");
    public static final JceCmd m = new JceCmd(12, 12, "GetRelatedAppList");
    public static final JceCmd n = new JceCmd(13, 13, "GetHomepage");
    public static final JceCmd o = new JceCmd(14, 14, "GetGroupApps");
    public static final JceCmd p = new JceCmd(15, 16, "GetPluginList");
    public static final JceCmd q = new JceCmd(16, 17, "GetUnionUpdateInfo");
    public static final JceCmd r = new JceCmd(17, 18, "GetAuthorApps");
    public static final JceCmd s = new JceCmd(18, 19, "GetSetting");
    public static final JceCmd t = new JceCmd(19, 20, "PostFeedback");
    public static final JceCmd u = new JceCmd(20, 21, "GetFeedbackList");
    public static final JceCmd v = new JceCmd(21, 22, "ReportApkFileInfo");
    public static final JceCmd w = new JceCmd(22, 23, "CheckSelfUpdate");
    public static final JceCmd x = new JceCmd(23, 24, "StatReport");
    public static final JceCmd y = new JceCmd(24, 25, "GetAppVerifyCode");
    public static final JceCmd z = new JceCmd(25, 26, "GetSubject");
    private int ba;
    private String bb = new String();

    public static JceCmd a(int i2) {
        for (int i3 = 0; i3 < aZ.length; i3++) {
            if (aZ[i3].a() == i2) {
                return aZ[i3];
            }
        }
        if (aY) {
            return null;
        }
        throw new AssertionError();
    }

    public static JceCmd a(String str) {
        for (int i2 = 0; i2 < aZ.length; i2++) {
            if (aZ[i2].toString().equals(str)) {
                return aZ[i2];
            }
        }
        if (aY) {
            return null;
        }
        throw new AssertionError();
    }

    public int a() {
        return this.ba;
    }

    public String toString() {
        return this.bb;
    }

    private JceCmd(int i2, int i3, String str) {
        this.bb = str;
        this.ba = i3;
        aZ[i2] = this;
    }
}
