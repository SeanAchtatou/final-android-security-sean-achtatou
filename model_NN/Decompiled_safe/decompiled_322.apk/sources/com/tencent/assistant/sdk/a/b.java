package com.tencent.assistant.sdk.a;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* compiled from: ProGuard */
public abstract class b extends Binder implements a {
    public b() {
        attachInterface(this, "com.tencent.assistant.sdk.remote.BaseService");
    }

    public static a a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.assistant.sdk.remote.BaseService");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof a)) {
            return new c(iBinder);
        }
        return (a) queryLocalInterface;
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                parcel.enforceInterface("com.tencent.assistant.sdk.remote.BaseService");
                int a2 = a(parcel.readString(), parcel.readString(), e.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                parcel2.writeInt(a2);
                return true;
            case 2:
                parcel.enforceInterface("com.tencent.assistant.sdk.remote.BaseService");
                int a3 = a(e.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                parcel2.writeInt(a3);
                return true;
            case 3:
                parcel.enforceInterface("com.tencent.assistant.sdk.remote.BaseService");
                byte[] a4 = a(parcel.readString(), parcel.createByteArray());
                parcel2.writeNoException();
                parcel2.writeByteArray(a4);
                return true;
            case 4:
                parcel.enforceInterface("com.tencent.assistant.sdk.remote.BaseService");
                b(parcel.readString(), parcel.createByteArray());
                parcel2.writeNoException();
                return true;
            case 1598968902:
                parcel2.writeString("com.tencent.assistant.sdk.remote.BaseService");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
