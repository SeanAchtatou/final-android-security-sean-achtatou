package com.tencent.assistant.localres;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.qq.AppService.AstApp;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.aq;
import com.tencent.assistant.utils.as;
import com.tencent.assistant.utils.av;
import com.tencent.assistant.utils.e;
import com.tencent.connect.common.Constants;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class aa {

    /* renamed from: a  reason: collision with root package name */
    public static final String f807a = aa.class.getSimpleName();
    private Context b = AstApp.i();
    private boolean c = false;

    protected aa() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    public Map<String, LocalApkInfo> a(Map<String, LocalApkInfo> map, boolean z) {
        this.c = false;
        if (map == null) {
            map = new HashMap<>();
        }
        PackageManager packageManager = this.b.getPackageManager();
        HashMap hashMap = new HashMap();
        List<PackageInfo> installedPackages = packageManager.getInstalledPackages(0);
        if (installedPackages == null) {
            return null;
        }
        long a2 = m.a().a("key_first_load_installed_app_time", 0L);
        if (a2 == 0) {
            a2 = System.currentTimeMillis();
            m.a().b("key_first_load_installed_app_time", Long.valueOf(a2));
        }
        long j = a2;
        for (PackageInfo next : installedPackages) {
            ApplicationInfo applicationInfo = next.applicationInfo;
            LocalApkInfo localApkInfo = map.get(next.packageName);
            File file = new File(applicationInfo.sourceDir);
            if (localApkInfo == null || localApkInfo.mInstallDate != file.lastModified()) {
                this.c = true;
                LocalApkInfo localApkInfo2 = new LocalApkInfo();
                localApkInfo2.mPackageName = next.packageName;
                localApkInfo2.mVersionName = next.versionName == null ? Constants.STR_EMPTY : next.versionName;
                localApkInfo2.mVersionCode = next.versionCode;
                localApkInfo2.mLocalFilePath = applicationInfo.sourceDir;
                localApkInfo2.flags = applicationInfo.flags;
                localApkInfo2.mUid = applicationInfo.uid;
                localApkInfo2.mGrayVersionCode = e.a(packageManager, next.packageName);
                localApkInfo2.mInstalleLocation = (byte) e.a(localApkInfo2);
                localApkInfo2.mAppName = applicationInfo.loadLabel(packageManager).toString().trim();
                localApkInfo2.mIsEnabled = applicationInfo.enabled;
                localApkInfo2.mAppIconRes = applicationInfo.icon;
                if (e.b(applicationInfo.flags)) {
                    localApkInfo2.occupySize = a(file) + a(new File(applicationInfo.sourceDir.replace(".apk", ".odex")));
                } else {
                    localApkInfo2.occupySize = 0;
                }
                if (localApkInfo != null && localApkInfo.mLastLaunchTime > 0) {
                    localApkInfo2.mLastLaunchTime = localApkInfo.mLastLaunchTime;
                    localApkInfo2.mFakeLastLaunchTime = localApkInfo.mLastLaunchTime;
                }
                if (localApkInfo2.mLastLaunchTime <= 0) {
                    localApkInfo2.mLastLaunchTime = 0;
                    localApkInfo2.mFakeLastLaunchTime = j;
                }
                localApkInfo2.mSortKey = av.a(localApkInfo2.mAppName);
                if (!z) {
                    try {
                        localApkInfo2.mInstallDate = file.lastModified();
                        PackageInfo packageInfo = packageManager.getPackageInfo(localApkInfo2.mPackageName, 64);
                        if (packageInfo.signatures.length >= 1) {
                            localApkInfo2.signature = aq.b(packageInfo.signatures[packageInfo.signatures.length - 1].toCharsString());
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    localApkInfo2.manifestMd5 = as.a(localApkInfo2.mLocalFilePath);
                }
                hashMap.put(localApkInfo2.mPackageName, localApkInfo2);
                Thread.yield();
            } else {
                hashMap.put(localApkInfo.mPackageName, localApkInfo);
                localApkInfo.flags = applicationInfo.flags;
                if (e.b(applicationInfo.flags)) {
                    localApkInfo.occupySize = a(file) + a(new File(applicationInfo.sourceDir.replace(".apk", ".odex")));
                } else {
                    localApkInfo.occupySize = 0;
                }
                localApkInfo.mInstalleLocation = (byte) e.a(localApkInfo);
                localApkInfo.mIsEnabled = applicationInfo.enabled;
            }
        }
        if (hashMap.size() != map.size()) {
            this.c = true;
        }
        return hashMap;
    }

    public boolean a() {
        return this.c;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x001e A[SYNTHETIC, Splitter:B:14:0x001e] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0026 A[SYNTHETIC, Splitter:B:20:0x0026] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private long a(java.io.File r5) {
        /*
            r4 = this;
            long r0 = r5.length()
            r2 = 0
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 > 0) goto L_0x001a
            r3 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0022, all -> 0x001b }
            r2.<init>(r5)     // Catch:{ Exception -> 0x0022, all -> 0x001b }
            int r0 = r2.available()     // Catch:{ Exception -> 0x0031, all -> 0x002e }
            long r0 = (long) r0
            if (r2 == 0) goto L_0x001a
            r2.close()     // Catch:{ IOException -> 0x002a }
        L_0x001a:
            return r0
        L_0x001b:
            r0 = move-exception
        L_0x001c:
            if (r3 == 0) goto L_0x0021
            r3.close()     // Catch:{ IOException -> 0x002c }
        L_0x0021:
            throw r0
        L_0x0022:
            r2 = move-exception
            r2 = r3
        L_0x0024:
            if (r2 == 0) goto L_0x001a
            r2.close()     // Catch:{ IOException -> 0x002a }
            goto L_0x001a
        L_0x002a:
            r2 = move-exception
            goto L_0x001a
        L_0x002c:
            r1 = move-exception
            goto L_0x0021
        L_0x002e:
            r0 = move-exception
            r3 = r2
            goto L_0x001c
        L_0x0031:
            r3 = move-exception
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.localres.aa.a(java.io.File):long");
    }
}
