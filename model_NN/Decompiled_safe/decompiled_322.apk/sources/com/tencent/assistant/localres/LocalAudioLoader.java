package com.tencent.assistant.localres;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import com.tencent.assistant.localres.model.LocalAudio;
import com.tencent.assistant.utils.av;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class LocalAudioLoader extends LocalMediaLoader<LocalAudio> {
    public LocalAudioLoader(Context context) {
        super(context);
        this.c = 3;
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.addAll(a(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI));
        a(this, this.b, true);
    }

    private ArrayList<LocalAudio> a(Uri uri) {
        ArrayList<LocalAudio> arrayList = new ArrayList<>();
        Cursor query = this.f804a.getContentResolver().query(uri, null, "duration> 0", null, "date_modified DESC");
        if (query == null || !query.moveToFirst()) {
            if (query != null) {
                query.close();
            }
            return arrayList;
        }
        int columnIndexOrThrow = query.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = query.getColumnIndexOrThrow("_data");
        int columnIndexOrThrow3 = query.getColumnIndexOrThrow("title");
        int columnIndexOrThrow4 = query.getColumnIndexOrThrow("_display_name");
        int columnIndexOrThrow5 = query.getColumnIndexOrThrow("_size");
        int columnIndexOrThrow6 = query.getColumnIndexOrThrow("date_added");
        int columnIndexOrThrow7 = query.getColumnIndexOrThrow("date_modified");
        int columnIndexOrThrow8 = query.getColumnIndexOrThrow("mime_type");
        int columnIndexOrThrow9 = query.getColumnIndexOrThrow("duration");
        int columnIndexOrThrow10 = query.getColumnIndexOrThrow("artist");
        int columnIndexOrThrow11 = query.getColumnIndexOrThrow("artist_key");
        int columnIndexOrThrow12 = query.getColumnIndexOrThrow("composer");
        int columnIndexOrThrow13 = query.getColumnIndexOrThrow("album");
        int columnIndexOrThrow14 = query.getColumnIndexOrThrow("album_key");
        int columnIndexOrThrow15 = query.getColumnIndexOrThrow("album_id");
        int columnIndexOrThrow16 = query.getColumnIndexOrThrow("year");
        int columnIndexOrThrow17 = query.getColumnIndexOrThrow("is_music");
        int columnIndex = query.getColumnIndex("is_podcast");
        int columnIndexOrThrow18 = query.getColumnIndexOrThrow("is_ringtone");
        int columnIndexOrThrow19 = query.getColumnIndexOrThrow("is_alarm");
        int columnIndexOrThrow20 = query.getColumnIndexOrThrow("is_notification");
        do {
            LocalAudio localAudio = new LocalAudio();
            localAudio.id = query.getInt(columnIndexOrThrow);
            localAudio.path = query.getString(columnIndexOrThrow2);
            localAudio.name = query.getString(columnIndexOrThrow3);
            localAudio.description = query.getString(columnIndexOrThrow4);
            localAudio.size = query.getLong(columnIndexOrThrow5);
            localAudio.softKey = av.a(localAudio.name);
            localAudio.createDate = query.getLong(columnIndexOrThrow6);
            localAudio.updateDate = query.getLong(columnIndexOrThrow7);
            localAudio.mimeType = query.getString(columnIndexOrThrow8);
            localAudio.duration = query.getInt(columnIndexOrThrow9);
            localAudio.artist = query.getString(columnIndexOrThrow10);
            localAudio.artistKey = query.getString(columnIndexOrThrow11);
            localAudio.composer = query.getString(columnIndexOrThrow12);
            localAudio.album = query.getString(columnIndexOrThrow13);
            localAudio.albumKey = query.getString(columnIndexOrThrow14);
            localAudio.albumId = query.getString(columnIndexOrThrow15);
            localAudio.createYear = query.getInt(columnIndexOrThrow16);
            localAudio.musicFlag = query.getInt(columnIndexOrThrow17) == 1;
            if (columnIndex != -1) {
                localAudio.podcastFlag = query.getInt(columnIndex) == 1;
            }
            localAudio.ringtoneFlag = query.getInt(columnIndexOrThrow18) == 1;
            localAudio.alarmFlag = query.getInt(columnIndexOrThrow19) == 1;
            localAudio.notificationFlag = query.getInt(columnIndexOrThrow20) == 1;
            arrayList.add(localAudio);
        } while (query.moveToNext());
        query.close();
        return arrayList;
    }
}
