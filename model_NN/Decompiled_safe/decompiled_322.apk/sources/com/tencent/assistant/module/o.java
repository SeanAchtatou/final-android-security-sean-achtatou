package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;

/* compiled from: ProGuard */
class o implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CallbackHelper.Caller f1008a;
    final /* synthetic */ BaseEngine b;

    o(BaseEngine baseEngine, CallbackHelper.Caller caller) {
        this.b = baseEngine;
        this.f1008a = caller;
    }

    public void run() {
        this.b.notifyDataChanged(this.f1008a);
    }
}
