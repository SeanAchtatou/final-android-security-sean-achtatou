package com.tencent.assistant.module.timer.job;

import com.tencent.assistant.m;
import com.tencent.assistant.module.ag;
import com.tencent.assistant.module.timer.SimpleBaseScheduleJob;

/* compiled from: ProGuard */
public class GetUnionUpdateInfoTimerJob extends SimpleBaseScheduleJob {

    /* renamed from: a  reason: collision with root package name */
    private static GetUnionUpdateInfoTimerJob f1019a;

    public static synchronized GetUnionUpdateInfoTimerJob e() {
        GetUnionUpdateInfoTimerJob getUnionUpdateInfoTimerJob;
        synchronized (GetUnionUpdateInfoTimerJob.class) {
            if (f1019a == null) {
                f1019a = new GetUnionUpdateInfoTimerJob();
            }
            getUnionUpdateInfoTimerJob = f1019a;
        }
        return getUnionUpdateInfoTimerJob;
    }

    public void c() {
        ag.b().d();
    }

    public int h() {
        return m.a().a("union_update_interval", 10800);
    }
}
