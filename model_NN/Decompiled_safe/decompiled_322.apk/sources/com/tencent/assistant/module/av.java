package com.tencent.assistant.module;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
class av implements m {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ as f971a;

    av(as asVar) {
        this.f971a = asVar;
    }

    public boolean a(SimpleAppModel simpleAppModel) {
        DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
        AppConst.AppState d = k.d(simpleAppModel);
        return (a2 != null && a2.uiType == SimpleDownloadInfo.UIType.NORMAL) || d == AppConst.AppState.INSTALLED || d == AppConst.AppState.UPDATE;
    }
}
