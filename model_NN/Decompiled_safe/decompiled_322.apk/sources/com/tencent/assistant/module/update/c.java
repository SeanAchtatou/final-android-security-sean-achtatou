package com.tencent.assistant.module.update;

import android.os.Message;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class c implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f1029a;

    c(b bVar) {
        this.f1029a = bVar;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case 1032:
                TemporaryThreadManager.get().start(new e(this));
                return;
            case EventDispatcherEnum.UI_EVENT_APP_GOFRONT:
                TemporaryThreadManager.get().start(new d(this));
                return;
            default:
                return;
        }
    }
}
