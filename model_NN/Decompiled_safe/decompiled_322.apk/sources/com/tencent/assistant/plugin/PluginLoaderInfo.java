package com.tencent.assistant.plugin;

/* compiled from: ProGuard */
public class PluginLoaderInfo {

    /* renamed from: a  reason: collision with root package name */
    private ClassLoader f1070a;
    private PluginContext b;

    public PluginLoaderInfo(ClassLoader classLoader, PluginContext pluginContext) {
        this.f1070a = classLoader;
        this.b = pluginContext;
    }

    public ClassLoader getClassLoader() {
        return this.f1070a;
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.f1070a = classLoader;
    }

    public Class<?> loadClass(String str) {
        if (str != null) {
            return this.f1070a.loadClass(str);
        }
        return null;
    }

    public PluginContext getContext() {
        return this.b;
    }

    public void setContext(PluginContext pluginContext) {
        this.b = pluginContext;
    }

    public String toString() {
        return "PluginLoaderInfo{classLoader=" + this.f1070a + '}';
    }
}
