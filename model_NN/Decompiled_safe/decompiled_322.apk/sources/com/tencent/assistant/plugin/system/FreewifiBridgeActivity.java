package com.tencent.assistant.plugin.system;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;

/* compiled from: ProGuard */
public class FreewifiBridgeActivity extends BaseActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("plugin_package");
        String stringExtra2 = intent.getStringExtra("plugin_activity");
        if (TextUtils.isEmpty(stringExtra)) {
            finish();
            return;
        }
        PluginInfo a2 = i.b().a(stringExtra);
        if (a2 != null) {
            PluginProxyActivity.a(this, a2.getPackageName(), a2.getVersion(), stringExtra2, a2.getInProcess(), intent, null);
        } else {
            Toast.makeText(this, getString(R.string.plugin_freewifi_not_install), 0).show();
        }
        finish();
    }
}
