package com.tencent.assistant.plugin.accelerate;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import com.tencent.pangu.utils.d;

/* compiled from: ProGuard */
public class PluginAccelerateBridgeActivity extends BaseActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            t();
        } catch (Throwable th) {
            finish();
        }
    }

    private void t() {
        Intent intent = getIntent();
        String a2 = d.a(intent, "plugin_package");
        intent.removeExtra("plugin_package");
        String a3 = d.a(intent, "plugin_activity");
        intent.removeExtra("plugin_activity");
        intent.setFlags(0);
        if (TextUtils.isEmpty(a2) || TextUtils.isEmpty(a3)) {
            finish();
            return;
        }
        PluginInfo a4 = i.b().a(a2);
        if (a4 != null) {
            PluginProxyActivity.a(this, a4.getPackageName(), a4.getVersion(), a3, a4.getInProcess(), intent, null);
        } else {
            try {
                Toast.makeText(this, getString(R.string.mobile_accelerate_not_install), 0).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        finish();
    }

    public static void a(Context context) {
        context.getSharedPreferences("share_pref", 0).edit().putBoolean("show_promot_point", false).commit();
    }
}
