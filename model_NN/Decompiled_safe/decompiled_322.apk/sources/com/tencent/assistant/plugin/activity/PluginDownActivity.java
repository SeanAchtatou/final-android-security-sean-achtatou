package com.tencent.assistant.plugin.activity;

import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.module.k;
import com.tencent.assistant.plugin.PluginDownloadInfo;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.c;
import com.tencent.assistant.plugin.mgr.h;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.at;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;

/* compiled from: ProGuard */
public class PluginDownActivity extends BaseActivity implements UIEventListener {
    private Button A;
    private PluginDownloadInfo B;
    private String C;
    private AstApp D = AstApp.i();
    private TextView n;
    private TextView u;
    private RelativeLayout v;
    private TextView w;
    private TextView x;
    private ProgressBar y;
    private Button z;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
        setContentView((int) R.layout.activity_plugin_down);
        this.C = getIntent().getStringExtra("package_name");
        if (TextUtils.isEmpty(this.C)) {
            finish();
            return;
        }
        PluginInfo a2 = i.b().a(this.C);
        this.B = c.a().a(this.C);
        if (a2 != null && (this.B == null || this.B.version <= a2.getVersion() || h.a(this.C) >= 0)) {
            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_DIALOG_NORMAL_INSTALLED, this.C));
            finish();
        } else if (this.B == null) {
            Toast.makeText(this, (int) R.string.plugin_not_cofig, 0).show();
            finish();
        } else {
            this.D.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_SUCC, this);
            this.D.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DOWNLOADING, this);
            this.D.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_QUEUING, this);
            this.D.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_PAUSE, this);
            this.D.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_FAIL, this);
            this.D.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DELETE, this);
            this.D.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL, this);
            this.D.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
            this.D.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_FAIL, this);
            this.D.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_SUCC, this);
            this.D.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FINAL_FAIL, this);
            t();
        }
    }

    private void t() {
        this.n = (TextView) findViewById(R.id.title);
        this.u = (TextView) findViewById(R.id.plugin_desc);
        this.v = (RelativeLayout) findViewById(R.id.downding_zone);
        this.w = (TextView) findViewById(R.id.speed);
        this.x = (TextView) findViewById(R.id.down_size);
        this.y = (ProgressBar) findViewById(R.id.down_progress);
        this.z = (Button) findViewById(R.id.cancel_btn);
        this.A = (Button) findViewById(R.id.down_btn);
        this.z.setOnClickListener(new b(this));
        DownloadInfo u2 = u();
        if (u2 != null) {
            a(u2);
            return;
        }
        Toast.makeText(this, (int) R.string.plugin_not_download, 0).show();
        finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.plugin.activity.PluginDownActivity.a(android.widget.Button, boolean):void
     arg types: [android.widget.Button, int]
     candidates:
      com.tencent.assistant.plugin.activity.PluginDownActivity.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.plugin.activity.PluginDownActivity.a(android.widget.Button, boolean):void */
    private void a(DownloadInfo downloadInfo) {
        AppConst.AppState appState;
        boolean z2 = downloadInfo.uiType == SimpleDownloadInfo.UIType.PLUGIN_PREDOWNLOAD;
        PluginDownloadInfo a2 = c.a().a(this.C);
        if (i.b().a(this.C) != null) {
            this.u.setText(getResources().getString(R.string.plugin_download_update_dialog_default_desc));
        } else {
            this.u.setText(getResources().getString(R.string.plugin_download_dialog_default_desc));
        }
        AppConst.AppState a3 = k.a(downloadInfo, a2, (PluginInfo) null);
        if (z2) {
            appState = AppConst.AppState.DOWNLOAD;
        } else {
            appState = a3;
        }
        if (appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.DOWNLOADED) {
            this.A.setText(getResources().getString(R.string.plugin_down_dialog_downloading));
            a(this.A, false);
            b(downloadInfo);
        } else if (appState == AppConst.AppState.QUEUING) {
            this.A.setText(getResources().getString(R.string.plugin_down_dialog_wait));
            a(this.A, false);
            b(downloadInfo);
        } else if (appState == AppConst.AppState.FAIL || appState == AppConst.AppState.PAUSED) {
            this.A.setText(getResources().getString(R.string.plugin_down_dialog_redownload));
            a(this.A, true);
            a(downloadInfo, a2);
            b(downloadInfo);
        } else if (appState == AppConst.AppState.DOWNLOAD && a2 != null) {
            this.u.setVisibility(0);
            this.v.setVisibility(8);
            this.A.setText(String.format(getResources().getString(R.string.plugin_down_dialog_size), at.a(a2.fileSize)));
            this.A.setEnabled(true);
            a(downloadInfo, a2);
        }
    }

    private void a(Button button, boolean z2) {
        if (z2) {
            button.setEnabled(true);
            button.setTextColor(getResources().getColor(R.color.state_normal));
            button.setBackgroundDrawable(getResources().getDrawable(R.drawable.common_blue_btn_selector));
            return;
        }
        button.setEnabled(false);
        button.setTextColor(getResources().getColor(R.color.plugin_downloading_disabled_color));
        button.setBackgroundDrawable(getResources().getDrawable(R.drawable.common_btn_big_disabled));
    }

    private void a(DownloadInfo downloadInfo, PluginDownloadInfo pluginDownloadInfo) {
        this.A.setOnClickListener(new c(this, downloadInfo, pluginDownloadInfo));
    }

    private void b(DownloadInfo downloadInfo) {
        this.u.setVisibility(8);
        this.v.setVisibility(0);
        this.w.setText(downloadInfo.response.c);
        this.y.setProgress(downloadInfo.getProgress());
        this.y.setSecondaryProgress(0);
        this.x.setText(at.a(downloadInfo.response.f3766a) + "/" + at.a(downloadInfo.response.b));
    }

    private DownloadInfo u() {
        if (this.B == null) {
            return null;
        }
        DownloadInfo b = c.a().b(this.B);
        if (b == null) {
            return c.a().a(this.B, SimpleDownloadInfo.UIType.PLUGIN_PREDOWNLOAD);
        }
        return b;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.D.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_SUCC, this);
        this.D.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DOWNLOADING, this);
        this.D.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_QUEUING, this);
        this.D.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_PAUSE, this);
        this.D.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_FAIL, this);
        this.D.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DELETE, this);
        this.D.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL, this);
        this.D.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
        this.D.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_FAIL, this);
        this.D.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_SUCC, this);
        this.D.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FINAL_FAIL, this);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (isFinishing()) {
            return true;
        }
        finish();
        return true;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC:
                PluginInfo a2 = i.b().a(this.C);
                if (a2 != null) {
                    TemporaryThreadManager.get().start(new e(this, a2));
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL:
            case EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_FAIL:
                String str = (String) message.obj;
                if (str != null && str.equals(this.C)) {
                    Toast.makeText(this, (int) R.string.plugin_install_fail, 0).show();
                    if (!isFinishing()) {
                        finish();
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_PLUGIN_PRELOAD_SUCC:
                String str2 = (String) message.obj;
                if (str2 != null && str2.equals(this.C)) {
                    AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_DIALOG_NORMAL_INSTALLED, this.C));
                    finish();
                    return;
                }
                return;
            default:
                if ((message.obj instanceof String) && ((String) message.obj).equals(this.B.downloadTicket)) {
                    a(u());
                    return;
                }
                return;
        }
    }

    public void finish() {
        super.finish();
    }

    public void overridePendingTransition(int i, int i2) {
    }

    public boolean k() {
        return false;
    }
}
