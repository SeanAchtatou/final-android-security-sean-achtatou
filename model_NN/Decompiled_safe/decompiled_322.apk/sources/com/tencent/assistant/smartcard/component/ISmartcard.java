package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistantv2.st.b.e;

/* compiled from: ProGuard */
public abstract class ISmartcard extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    protected Context f1693a;
    protected LayoutInflater b;
    protected View c;
    public n d = null;
    public as e = null;
    public boolean f = false;
    protected IViewInvalidater g;
    protected e h = null;

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public abstract void b();

    public ISmartcard(Context context) {
        super(context);
    }

    public ISmartcard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ISmartcard(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public ISmartcard(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, null);
        this.d = nVar;
        this.f1693a = context;
        this.e = asVar;
        this.b = (LayoutInflater) context.getSystemService("layout_inflater");
        setWillNotDraw(false);
        try {
            setBackgroundResource(R.drawable.bg_card_selector_padding);
        } catch (Throwable th) {
        }
        this.g = iViewInvalidater;
    }

    public void a(n nVar) {
        this.d = nVar;
        b();
    }
}
