package com.tencent.assistant.smartcard.c;

import android.util.SparseArray;
import com.tencent.assistant.db.table.ac;
import com.tencent.assistant.db.table.ad;
import com.tencent.assistant.db.table.ae;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.SmartCardCfg;
import com.tencent.assistant.protocol.jce.SmartCardCfgList;
import com.tencent.assistant.smartcard.d.a;
import com.tencent.assistant.smartcard.d.aa;
import com.tencent.assistant.smartcard.d.c;
import com.tencent.assistant.smartcard.d.d;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.r;
import com.tencent.assistant.smartcard.d.s;
import com.tencent.assistant.smartcard.d.u;
import com.tencent.assistant.smartcard.d.v;
import com.tencent.assistant.smartcard.d.x;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.bo;
import java.util.List;

/* compiled from: ProGuard */
public class p implements aa {

    /* renamed from: a  reason: collision with root package name */
    private static p f1682a;
    /* access modifiers changed from: private */
    public SparseArray<z> b = new SparseArray<>();
    private ac c = new ac();
    /* access modifiers changed from: private */
    public ae d = new ae();
    /* access modifiers changed from: private */
    public ad e = new ad();

    public static synchronized p a() {
        p pVar;
        synchronized (p.class) {
            if (f1682a == null) {
                f1682a = new p();
            }
            pVar = f1682a;
        }
        return pVar;
    }

    public p() {
        c();
    }

    private void c() {
        TemporaryThreadManager.get().start(new q(this));
    }

    /* access modifiers changed from: private */
    public void d() {
        this.b.put(0, new ad());
        this.b.put(4, new n());
        this.b.put(1, new j());
        this.b.put(2, new h());
        this.b.put(3, new ab());
        this.b.put(6, new o());
        this.b.put(5, new k());
        this.b.put(7, new f());
        this.b.put(8, new i());
        ac acVar = new ac();
        this.b.put(9, acVar);
        this.b.put(10, acVar);
        this.b.put(11, acVar);
        this.b.put(12, acVar);
        this.b.put(13, acVar);
        this.b.put(14, acVar);
        this.b.put(15, acVar);
        this.b.put(27, acVar);
        this.b.put(16, new a());
        this.b.put(17, new m());
        this.b.put(23, new l());
        this.b.put(24, new e());
        this.b.put(22, new g());
        b.a(this.b);
    }

    /* access modifiers changed from: private */
    public void e() {
        z d2;
        List<x> a2 = this.c.a();
        if (a2 != null && a2.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < a2.size()) {
                    x xVar = a2.get(i2);
                    z zVar = this.b.get(xVar.e);
                    if (zVar != null) {
                        zVar.a(xVar);
                    } else {
                        c a3 = b.a(xVar.e);
                        if (!(a3 == null || (d2 = a3.d()) == null)) {
                            d2.a(xVar);
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        z d2;
        List<v> a2 = this.d.a();
        if (a2 != null && a2.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < a2.size()) {
                    v vVar = a2.get(i2);
                    z zVar = this.b.get(vVar.f1765a);
                    if (zVar != null) {
                        zVar.a(vVar);
                    } else {
                        c a3 = b.a(vVar.f1765a);
                        if (!(a3 == null || (d2 = a3.d()) == null)) {
                            d2.a(vVar);
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void b() {
        z d2;
        List<u> a2 = this.e.a();
        if (a2 != null && a2.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < a2.size()) {
                    u uVar = a2.get(i2);
                    if (uVar != null) {
                        z zVar = this.b.get(uVar.f1764a);
                        if (zVar != null) {
                            zVar.a(uVar);
                        } else {
                            c a3 = b.a(uVar.f1764a);
                            if (!(a3 == null || (d2 = a3.d()) == null)) {
                                d2.a(uVar);
                            }
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        this.d.a(bo.b(30));
        this.e.a(bo.b(30));
    }

    public n a(List<n> list, List<Long> list2, int i) {
        z d2;
        if (list == null || list.size() <= 0) {
            return null;
        }
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                return null;
            }
            n nVar = list.get(i3);
            if (nVar.j == 0) {
                b(nVar);
            } else if (nVar.j == 16) {
                c(nVar);
            } else if (nVar.j == 17) {
                d(nVar);
            } else if (nVar.j == 23) {
                e(nVar);
            } else if (nVar.j == 24) {
                f(nVar);
            }
            if (nVar instanceof a) {
                x c2 = ((a) nVar).c();
                if (list2 != null) {
                    nVar.a(list2);
                }
                ((a) nVar).b();
                c a2 = b.a(nVar.j);
                if (!(a2 == null || (d2 = a2.d()) == null)) {
                    if (c2 != null) {
                        TemporaryThreadManager.get().start(new r(this, d2, c2));
                    }
                    if (d2.a(nVar, list2)) {
                        nVar.a(i);
                        return nVar;
                    }
                }
            }
            z zVar = this.b.get(nVar.j);
            if (zVar != null && zVar.a(nVar, list2)) {
                return nVar;
            }
            i2 = i3 + 1;
        }
    }

    public void a(n nVar) {
        z zVar;
        if (nVar != null && (zVar = this.b.get(nVar.j)) != null) {
            zVar.a(nVar);
        }
    }

    public boolean b(n nVar) {
        if (nVar == null || !(nVar instanceof aa)) {
            return false;
        }
        aa aaVar = (aa) nVar;
        x xVar = new x();
        xVar.e = aaVar.j;
        xVar.f = aaVar.k;
        xVar.f1767a = aaVar.d;
        xVar.c = aaVar.e;
        xVar.d = aaVar.b;
        xVar.h = (long) aaVar.c;
        z zVar = this.b.get(xVar.e);
        if (zVar != null) {
            zVar.a(xVar);
        }
        TemporaryThreadManager.get().start(new s(this, xVar));
        return true;
    }

    public boolean c(n nVar) {
        if (nVar == null || !(nVar instanceof c)) {
            return false;
        }
        c cVar = (c) nVar;
        x xVar = new x();
        xVar.e = cVar.j;
        xVar.f = cVar.k;
        xVar.f1767a = cVar.e;
        xVar.c = cVar.f;
        xVar.d = cVar.c;
        xVar.h = (long) cVar.d;
        z zVar = this.b.get(xVar.e);
        if (zVar != null) {
            zVar.a(xVar);
        }
        TemporaryThreadManager.get().start(new t(this, xVar));
        return true;
    }

    public boolean d(n nVar) {
        if (nVar == null || !(nVar instanceof s)) {
            return false;
        }
        s sVar = (s) nVar;
        x xVar = new x();
        xVar.e = sVar.j;
        xVar.f = sVar.k;
        xVar.f1767a = sVar.g;
        xVar.c = sVar.h;
        xVar.d = sVar.e;
        xVar.h = (long) sVar.f;
        z zVar = this.b.get(xVar.e);
        if (zVar != null) {
            zVar.a(xVar);
        }
        TemporaryThreadManager.get().start(new u(this, xVar));
        return true;
    }

    public boolean e(n nVar) {
        if (nVar == null || !(nVar instanceof r)) {
            return false;
        }
        r rVar = (r) nVar;
        x xVar = new x();
        xVar.e = rVar.j;
        xVar.f = rVar.k;
        xVar.d = rVar.b;
        xVar.h = rVar.c;
        z zVar = this.b.get(xVar.e);
        if (zVar != null) {
            zVar.a(xVar);
        }
        TemporaryThreadManager.get().start(new v(this, xVar));
        return true;
    }

    public boolean f(n nVar) {
        if (nVar == null || !(nVar instanceof d)) {
            return false;
        }
        d dVar = (d) nVar;
        x xVar = new x();
        xVar.e = dVar.j;
        xVar.f = dVar.k;
        xVar.f1767a = dVar.e;
        xVar.c = dVar.f;
        xVar.d = dVar.c;
        xVar.h = dVar.d;
        TemporaryThreadManager.get().start(new w(this, xVar));
        return true;
    }

    public boolean a(SmartCardCfgList smartCardCfgList) {
        int i = 0;
        if (smartCardCfgList == null || smartCardCfgList.f1502a == null || smartCardCfgList.f1502a.size() <= 0) {
            return false;
        }
        while (true) {
            int i2 = i;
            if (i2 >= smartCardCfgList.f1502a.size()) {
                return true;
            }
            SmartCardCfg smartCardCfg = smartCardCfgList.f1502a.get(i2);
            if (smartCardCfg != null) {
                x xVar = new x();
                xVar.a(smartCardCfg);
                z zVar = this.b.get(xVar.e);
                if (zVar != null) {
                    zVar.a(xVar);
                }
                a(xVar);
            }
            i = i2 + 1;
        }
    }

    public boolean a(x xVar) {
        return this.c.a(xVar) > 0;
    }

    public void a(int i, int i2) {
        v vVar = new v();
        vVar.b = i2;
        vVar.f1765a = i;
        vVar.c = true;
        vVar.e = System.currentTimeMillis() / 1000;
        z zVar = this.b.get(vVar.f1765a);
        if (zVar != null) {
            zVar.a(vVar);
        }
        a(vVar);
    }

    public void a(int i, int i2, SimpleAppModel simpleAppModel) {
        if (simpleAppModel != null) {
            u uVar = new u();
            uVar.b = i2;
            uVar.f1764a = i;
            uVar.c = simpleAppModel.c;
            uVar.d = simpleAppModel.g;
            uVar.e = simpleAppModel.ad;
            uVar.f = System.currentTimeMillis() / 1000;
            z zVar = this.b.get(uVar.f1764a);
            if (zVar != null) {
                zVar.a(uVar);
            }
            a(uVar);
        }
    }

    public void b(int i, int i2) {
        v vVar = new v();
        vVar.b = i2;
        vVar.f1765a = i;
        vVar.d = true;
        vVar.e = System.currentTimeMillis() / 1000;
        z zVar = this.b.get(vVar.f1765a);
        if (zVar != null) {
            zVar.a(vVar);
        }
        a(vVar);
    }

    public void a(v vVar) {
        TemporaryThreadManager.get().start(new x(this, vVar));
    }

    public void a(u uVar) {
        TemporaryThreadManager.get().start(new y(this, uVar));
    }
}
