package com.tencent.assistant.smartcard.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.net.c;
import com.tencent.assistant.smartcard.d.r;
import com.tencent.assistantv2.component.bg;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class f extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f1723a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ NormalSmartCardPlayerSHowItem c;

    f(NormalSmartCardPlayerSHowItem normalSmartCardPlayerSHowItem, r rVar, STInfoV2 sTInfoV2) {
        this.c = normalSmartCardPlayerSHowItem;
        this.f1723a = rVar;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        if (c.e()) {
            b.b(this.c.getContext(), this.f1723a.e);
        } else if (!c.a()) {
            bg.a(this.c.getContext(), this.c.getContext().getResources().getString(R.string.no_network_please_check), 0);
        } else {
            g gVar = new g(this);
            gVar.contentRes = this.c.getContext().getResources().getString(R.string.smartcard_play_video_23G);
            DialogUtils.show2BtnDialog(gVar);
        }
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.status = "06";
        }
        return this.b;
    }
}
