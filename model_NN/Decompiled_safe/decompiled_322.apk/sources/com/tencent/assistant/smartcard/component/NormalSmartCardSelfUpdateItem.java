package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.t;
import com.tencent.assistantv2.component.BreakTextView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.appdetail.process.s;
import com.tencent.pangu.manager.SelfUpdateManager;

/* compiled from: ProGuard */
public class NormalSmartCardSelfUpdateItem extends NormalSmartcardBaseItem {
    private View i;
    private TextView l;
    private RelativeLayout m;
    private TXAppIconView n;
    private DownloadButton o;
    private TextView p;
    private ListItemInfoView q;
    private TextView r;
    /* access modifiers changed from: private */
    public ImageView s;
    /* access modifiers changed from: private */
    public RelativeLayout t;
    /* access modifiers changed from: private */
    public BreakTextView u;
    /* access modifiers changed from: private */
    public TextView v;
    /* access modifiers changed from: private */
    public long w;

    public NormalSmartCardSelfUpdateItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartCardSelfUpdateItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    public NormalSmartCardSelfUpdateItem(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f) {
            this.f = true;
            d();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.app_updatelist_item_merge, this);
        this.i = this.c;
        this.l = (TextView) findViewById(R.id.update_title);
        this.n = (TXAppIconView) findViewById(R.id.app_icon_img);
        this.n.setInvalidater(this.g);
        this.p = (TextView) findViewById(R.id.title);
        this.o = (DownloadButton) findViewById(R.id.state_app_btn);
        this.q = (ListItemInfoView) findViewById(R.id.download_info);
        this.r = (TextView) findViewById(R.id.desc);
        this.m = (RelativeLayout) findViewById(R.id.title_layout);
        this.t = (RelativeLayout) findViewById(R.id.description_bottomlayout);
        this.u = (BreakTextView) findViewById(R.id.new_feature);
        this.v = (TextView) findViewById(R.id.new_feature_all);
        this.s = (ImageView) findViewById(R.id.show_more);
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        t tVar = (t) this.d;
        if (tVar == null || tVar.f1763a == null || tVar.b == null) {
            a(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.bg_card_selector_padding);
        a(0);
        this.l.setText(tVar.l);
        SimpleAppModel a2 = SelfUpdateManager.a(tVar.b, tVar.f1763a);
        STInfoV2 a3 = a("03_001", 200);
        if (a3 != null) {
            a3.updateWithSimpleAppModel(a2);
        }
        View a4 = a(tVar, a3);
        if (a4 != null) {
            a4.setTag(R.id.tma_st_smartcard_tag, e());
            a4.setOnClickListener(new i(this, a3));
        }
    }

    private View a(t tVar, STInfoV2 sTInfoV2) {
        if (tVar == null || tVar.f1763a == null || tVar.b == null) {
            return null;
        }
        SelfUpdateManager.SelfUpdateInfo selfUpdateInfo = tVar.f1763a;
        LocalApkInfo localApkInfo = tVar.b;
        SimpleAppModel a2 = SelfUpdateManager.a(localApkInfo, selfUpdateInfo);
        this.n.updateImageView(localApkInfo.mPackageName, R.drawable.pic_defaule, TXImageView.TXImageViewType.INSTALL_APK_ICON);
        this.p.setText(selfUpdateInfo.d);
        this.q.a(a2);
        SelfUpdateManager.a();
        SimpleAppModel a3 = SelfUpdateManager.a(localApkInfo, selfUpdateInfo);
        this.o.a(a3);
        if (s.a(a3)) {
            this.o.setClickable(false);
        } else {
            this.o.setClickable(true);
            this.o.a(sTInfoV2);
        }
        this.o.setTag(R.id.tma_st_smartcard_tag, e());
        String str = selfUpdateInfo.q;
        if (!TextUtils.isEmpty(str)) {
            this.r.setText(Html.fromHtml(str));
        } else {
            this.r.setText(Constants.STR_EMPTY);
        }
        String str2 = selfUpdateInfo.h;
        if (TextUtils.isEmpty(str2)) {
            this.t.setVisibility(8);
        } else {
            String format = String.format(this.f1693a.getResources().getString(R.string.update_feature), "\n" + str2);
            this.v.setText(format);
            String replace = format.replaceAll("\r\n", "\n").replaceAll("\n\n", "\n").replace("\n", Constants.STR_EMPTY);
            this.t.setVisibility(0);
            this.t.setOnClickListener(new j(this, selfUpdateInfo, sTInfoV2));
            this.u.setTag(Long.valueOf(a3.f938a));
            this.u.a(new k(this, a3.f938a));
            this.u.setText(replace);
            if (this.w != selfUpdateInfo.f3781a) {
                this.u.setVisibility(0);
                this.v.setVisibility(8);
                this.s.setImageDrawable(this.f1693a.getResources().getDrawable(R.drawable.icon_open));
            } else {
                this.u.setVisibility(8);
                this.v.setVisibility(0);
                this.s.setImageDrawable(this.f1693a.getResources().getDrawable(R.drawable.icon_close));
            }
        }
        return this.i;
    }

    public void a(int i2) {
        this.i.setVisibility(i2);
    }
}
