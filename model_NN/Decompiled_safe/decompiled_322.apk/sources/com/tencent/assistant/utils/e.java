package com.tencent.assistant.utils;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Pair;
import com.qq.AppService.AstApp;
import com.qq.ndk.NativeFileObject;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.db.table.x;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.kapalaiadapter.a;
import com.tencent.assistant.kapalaiadapter.d;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.connect.common.Constants;
import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class e {

    /* renamed from: a  reason: collision with root package name */
    static Class<?> f1844a;
    static Method b;
    static Method c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static boolean a(Context context, String str) {
        Context context2;
        try {
            if (!AstApp.i().getPackageName().equals(str)) {
                context2 = context.createPackageContext(str, 3);
            } else {
                context2 = context;
            }
            PackageManager packageManager = context2.getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 128);
            Intent.ShortcutIconResource fromContext = Intent.ShortcutIconResource.fromContext(context2, applicationInfo.icon);
            String trim = applicationInfo.loadLabel(packageManager).toString().trim();
            Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(str);
            launchIntentForPackage.setPackage(null);
            Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
            intent.putExtra("duplicate", false);
            intent.putExtra("android.intent.extra.shortcut.INTENT", launchIntentForPackage);
            intent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", fromContext);
            intent.putExtra("android.intent.extra.shortcut.NAME", trim);
            context.sendBroadcast(intent);
            XLog.d("donald", "addShortcut -- intent = " + launchIntentForPackage);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Context context, Bitmap bitmap, String str, Intent intent) {
        Intent intent2 = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        intent2.putExtra("duplicate", false);
        intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
        intent2.putExtra("android.intent.extra.shortcut.ICON", bitmap);
        intent2.putExtra("android.intent.extra.shortcut.NAME", str);
        context.sendBroadcast(intent2);
    }

    public static void a(Context context, String str, Intent intent) {
        Intent intent2 = new Intent("com.android.launcher.action.UNINSTALL_SHORTCUT");
        intent2.putExtra("android.intent.extra.shortcut.NAME", str);
        intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
        context.sendBroadcast(intent2);
    }

    public static boolean b(Context context, String str) {
        Exception e;
        boolean z;
        Context context2;
        boolean z2 = false;
        try {
            if (!AstApp.i().getPackageName().equals(str)) {
                context2 = context.createPackageContext(str, 3);
            } else {
                context2 = context;
            }
            PackageManager packageManager = context2.getPackageManager();
            String trim = packageManager.getApplicationInfo(str, 128).loadLabel(packageManager).toString().trim();
            ContentResolver contentResolver = context.getContentResolver();
            ContentResolver contentResolver2 = context.getContentResolver();
            Uri parse = Uri.parse("content://com.android.launcher.settings/favorites?notify=true");
            Uri parse2 = Uri.parse("content://com.android.launcher2.settings/favorites?notify=true");
            Cursor query = contentResolver.query(parse, new String[]{"title", "iconResource"}, "title=?", new String[]{trim.trim()}, null);
            Cursor query2 = contentResolver2.query(parse2, new String[]{"title", "iconResource"}, "title=?", new String[]{trim.trim()}, null);
            if (query != null && query.getCount() > 0) {
                z2 = true;
            }
            if (query != null) {
                query.close();
            }
            if (query2 != null && query2.getCount() > 0) {
                z2 = true;
            }
            if (query2 != null) {
                query2.close();
            }
            if (d.f790a) {
                return z2;
            }
            Cursor query3 = context.getContentResolver().query(Uri.parse(a.a().b()), new String[]{"title", "iconResource"}, "title=?", new String[]{trim.trim()}, null);
            if (query3 == null || query3.getCount() <= 0) {
                z = z2;
            } else {
                z = true;
            }
            if (query3 == null) {
                return z;
            }
            try {
                query3.close();
                return z;
            } catch (Exception e2) {
                e = e2;
            }
        } catch (Exception e3) {
            e = e3;
            z = false;
            e.printStackTrace();
            return z;
        }
    }

    public static void c(Context context, String str) {
        Context context2;
        try {
            if (!AstApp.i().getPackageName().equals(str)) {
                context2 = context.createPackageContext(str, 3);
            } else {
                context2 = context;
            }
            PackageManager packageManager = context2.getPackageManager();
            String trim = packageManager.getApplicationInfo(str, 128).loadLabel(packageManager).toString().trim();
            Intent intent = new Intent("com.android.launcher.action.UNINSTALL_SHORTCUT");
            intent.putExtra("android.intent.extra.shortcut.NAME", trim);
            intent.putExtra("android.intent.extra.shortcut.INTENT", packageManager.getLaunchIntentForPackage(str));
            Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(str);
            context.sendBroadcast(intent);
            XLog.d("donald", "delShortcut -- intent = " + launchIntentForPackage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a() {
        try {
            AstApp i = AstApp.i();
            PackageManager packageManager = i.getPackageManager();
            String packageName = i.getPackageName();
            String trim = packageManager.getApplicationInfo(packageName, 128).loadLabel(packageManager).toString().trim();
            Intent intent = new Intent("com.android.launcher.action.UNINSTALL_SHORTCUT");
            intent.putExtra("android.intent.extra.shortcut.NAME", trim);
            Intent intent2 = new Intent();
            intent2.setAction("android.intent.action.MAIN");
            intent2.setComponent(new ComponentName(packageName, "com.tencent.assistant.activity.SplashActivity"));
            intent.putExtra("android.intent.extra.shortcut.INTENT", intent2);
            i.sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void d(Context context, String str) {
        try {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.setComponent(new ComponentName("com.tencent.android.qqdownloader", "com.tencent.android.ui.SplashActivity"));
            intent.setFlags(270532608);
            intent.addCategory("android.intent.category.DEFAULT");
            Intent intent2 = new Intent("com.android.launcher.action.UNINSTALL_SHORTCUT");
            intent2.putExtra("android.intent.extra.shortcut.NAME", str);
            intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
            context.sendBroadcast(intent2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(Context context, String str, String str2) {
        Context context2;
        try {
            if (!AstApp.i().getPackageName().equals(str)) {
                context2 = context.createPackageContext(str, 3);
            } else {
                context2 = context;
            }
            PackageManager packageManager = context2.getPackageManager();
            Intent intent = new Intent("com.android.launcher.action.UNINSTALL_SHORTCUT");
            intent.putExtra("android.intent.extra.shortcut.NAME", str2);
            intent.putExtra("android.intent.extra.shortcut.INTENT", packageManager.getLaunchIntentForPackage(str));
            context.sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean a(String str, int i) {
        LocalApkInfo localApkInfo;
        if (TextUtils.isEmpty(str) || (localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(str)) == null || localApkInfo.mVersionCode < i) {
            return false;
        }
        return true;
    }

    public static boolean a(String str, int i, int i2) {
        LocalApkInfo localApkInfo;
        if (TextUtils.isEmpty(str) || (localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(str)) == null || localApkInfo.mVersionCode != i || localApkInfo.mGrayVersionCode != i2) {
            return false;
        }
        return true;
    }

    public static boolean b(String str, int i) {
        PackageInfo d;
        if (TextUtils.isEmpty(str) || (d = d(str, 0)) == null || d.versionCode < i) {
            return false;
        }
        return true;
    }

    public static boolean a(String str) {
        if (TextUtils.isEmpty(str) || d(str, 0) == null) {
            return false;
        }
        return true;
    }

    public static boolean c(String str, int i) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(str);
        if (localApkInfo == null || localApkInfo.mVersionCode < i) {
            return true;
        }
        return false;
    }

    public static PackageInfo d(String str, int i) {
        PackageManager packageManager;
        if (TextUtils.isEmpty(str) || (packageManager = AstApp.i().getPackageManager()) == null) {
            return null;
        }
        try {
            return packageManager.getPackageInfo(str, i);
        } catch (PackageManager.NameNotFoundException | RuntimeException e) {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00cd A[SYNTHETIC, Splitter:B:34:0x00cd] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00da A[SYNTHETIC, Splitter:B:41:0x00da] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.tencent.assistant.localres.model.LocalApkInfo b(java.lang.String r10) {
        /*
            r7 = 0
            r2 = 0
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            android.content.Context r0 = r0.getBaseContext()
            android.content.pm.PackageManager r1 = r0.getPackageManager()
            r0 = 64
            android.content.pm.PackageInfo r0 = r1.getPackageInfo(r10, r0)     // Catch:{ Exception -> 0x001a }
            r5 = r0
        L_0x0016:
            if (r5 != 0) goto L_0x0020
            r0 = r2
        L_0x0019:
            return r0
        L_0x001a:
            r0 = move-exception
            r0.printStackTrace()
            r5 = r2
            goto L_0x0016
        L_0x0020:
            android.content.pm.ApplicationInfo r4 = r5.applicationInfo
            com.tencent.assistant.localres.model.LocalApkInfo r3 = new com.tencent.assistant.localres.model.LocalApkInfo
            r3.<init>()
            java.lang.String r0 = r5.packageName
            r3.mPackageName = r0
            java.lang.String r0 = r5.versionName
            if (r0 != 0) goto L_0x00c0
            java.lang.String r0 = ""
        L_0x0031:
            r3.mVersionName = r0
            int r0 = r5.versionCode
            r3.mVersionCode = r0
            java.lang.String r0 = r4.sourceDir
            r3.mLocalFilePath = r0
            int r0 = r4.flags
            r3.flags = r0
            java.lang.String r0 = r5.packageName
            int r0 = a(r1, r0)
            r3.mGrayVersionCode = r0
            boolean r0 = r4.enabled
            r3.mIsEnabled = r0
            int r0 = a(r3)
            byte r0 = (byte) r0
            r3.mInstalleLocation = r0
            int r0 = r4.icon
            r3.mAppIconRes = r0
            java.lang.CharSequence r0 = r4.loadLabel(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r0 = r0.trim()
            r3.mAppName = r0
            java.lang.String r0 = r3.mAppName
            java.lang.String r0 = com.tencent.assistant.utils.av.a(r0)
            r3.mSortKey = r0
            java.io.File r6 = new java.io.File
            java.lang.String r0 = r4.sourceDir
            r6.<init>(r0)
            int r0 = r4.flags
            boolean r0 = b(r0)
            if (r0 == 0) goto L_0x0094
            long r0 = r6.length()
            int r4 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r4 > 0) goto L_0x0092
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00c4, all -> 0x00d6 }
            r4.<init>(r6)     // Catch:{ Exception -> 0x00c4, all -> 0x00d6 }
            int r0 = r4.available()     // Catch:{ Exception -> 0x00e7 }
            long r0 = (long) r0
            if (r4 == 0) goto L_0x0092
            r4.close()     // Catch:{ IOException -> 0x00e3 }
        L_0x0092:
            r3.occupySize = r0
        L_0x0094:
            long r0 = r6.lastModified()
            r3.mInstallDate = r0
            r3.mLastLaunchTime = r7
            android.content.pm.Signature[] r0 = r5.signatures
            int r0 = r0.length
            r1 = 1
            if (r0 < r1) goto L_0x00b5
            android.content.pm.Signature[] r0 = r5.signatures
            android.content.pm.Signature[] r1 = r5.signatures
            int r1 = r1.length
            int r1 = r1 + -1
            r0 = r0[r1]
            java.lang.String r0 = r0.toCharsString()
            java.lang.String r0 = com.tencent.assistant.utils.aq.b(r0)
            r3.signature = r0
        L_0x00b5:
            java.lang.String r0 = r3.mLocalFilePath
            java.lang.String r0 = com.tencent.assistant.utils.as.a(r0)
            r3.manifestMd5 = r0
            r0 = r3
            goto L_0x0019
        L_0x00c0:
            java.lang.String r0 = r5.versionName
            goto L_0x0031
        L_0x00c4:
            r4 = move-exception
            r9 = r4
            r4 = r2
            r2 = r9
        L_0x00c8:
            r2.printStackTrace()     // Catch:{ all -> 0x00e5 }
            if (r4 == 0) goto L_0x0092
            r4.close()     // Catch:{ IOException -> 0x00d1 }
            goto L_0x0092
        L_0x00d1:
            r2 = move-exception
        L_0x00d2:
            r2.printStackTrace()
            goto L_0x0092
        L_0x00d6:
            r0 = move-exception
            r4 = r2
        L_0x00d8:
            if (r4 == 0) goto L_0x00dd
            r4.close()     // Catch:{ IOException -> 0x00de }
        L_0x00dd:
            throw r0
        L_0x00de:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00dd
        L_0x00e3:
            r2 = move-exception
            goto L_0x00d2
        L_0x00e5:
            r0 = move-exception
            goto L_0x00d8
        L_0x00e7:
            r2 = move-exception
            goto L_0x00c8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.e.b(java.lang.String):com.tencent.assistant.localres.model.LocalApkInfo");
    }

    public static LocalApkInfo c(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        PackageInfo a2 = a(AstApp.i().getBaseContext().getPackageManager(), str, 0);
        if (Global.isDev()) {
            b();
        }
        if (a2 == null) {
            return null;
        }
        LocalApkInfo localApkInfo = new LocalApkInfo();
        localApkInfo.mLocalFilePath = str;
        localApkInfo.mPackageName = a2.packageName;
        localApkInfo.mVersionName = a2.versionName;
        localApkInfo.mVersionCode = a2.versionCode;
        File file = new File(str);
        localApkInfo.occupySize = file.length();
        localApkInfo.mLastModified = file.lastModified();
        return localApkInfo;
    }

    public static PackageInfo a(PackageManager packageManager, String str, int i) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (packageManager == null) {
            packageManager = AstApp.i().getBaseContext().getPackageManager();
        }
        try {
            return packageManager.getPackageArchiveInfo(str, i);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } catch (OutOfMemoryError e2) {
            return null;
        }
    }

    public static void b() {
        m.a().b("key_last_parse_apk_path", Constants.STR_EMPTY);
    }

    public static void a(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", str);
        hashMap.put("B2", str2);
        hashMap.put("B3", Global.getPhoneGuidAndGen());
        hashMap.put("B4", Global.getQUAForBeacon());
        hashMap.put("B5", t.g());
        hashMap.put("B6", t.u());
        hashMap.put("B7", Build.VERSION.RELEASE);
        XLog.d("beacon", "beacon report >> CauseCrashApk. " + hashMap.toString());
        com.tencent.beacon.event.a.a("CauseCrashApk", true, -1, -1, hashMap, true);
    }

    public static boolean d(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return new File(str).exists();
    }

    public static Pair<Boolean, String> e(Context context, String str) {
        boolean z = true;
        boolean z2 = false;
        String str2 = Constants.STR_EMPTY;
        if (context != null) {
            try {
                if (!TextUtils.isEmpty(str) && d(str)) {
                    PackageManager packageManager = context.getPackageManager();
                    if (packageManager == null) {
                        l.a((int) EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, "pm is null");
                    } else {
                        PackageInfo a2 = a(packageManager, str, 0);
                        if (!(a2 == null || a2.applicationInfo == null)) {
                            z = false;
                        }
                        if (z) {
                            str2 = a2 == null ? "PackageInfo is null" : "PackageInfo.applicationInfo is null";
                            z2 = z;
                        } else {
                            z2 = z;
                        }
                    }
                    return Pair.create(Boolean.valueOf(z2), str2);
                }
            } catch (Throwable th) {
                l.a((int) EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, "exception: " + th.getMessage());
            }
        }
        if (context == null) {
            str2 = "context == null";
        } else if (TextUtils.isEmpty(str)) {
            str2 = "apkPath is empty";
        } else if (!d(str)) {
            str2 = STConst.ST_INSTALL_FAIL_STR_FILE_NOT_EXIST;
        }
        l.a((int) EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, str2);
        z2 = true;
        return Pair.create(Boolean.valueOf(z2), str2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0065, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0066, code lost:
        r10 = r0;
        r0 = r2;
        r2 = null;
        r1 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0073, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0076, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0073 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0007] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0076  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String e(java.lang.String r11) {
        /*
            r9 = 1
            r3 = 0
            java.lang.String r2 = ""
            r1 = 0
            java.lang.Class<android.content.res.AssetManager> r0 = android.content.res.AssetManager.class
            java.lang.Object r0 = r0.newInstance()     // Catch:{ Throwable -> 0x0065, all -> 0x0073 }
            android.content.res.AssetManager r0 = (android.content.res.AssetManager) r0     // Catch:{ Throwable -> 0x0065, all -> 0x0073 }
            java.lang.Class<android.content.res.AssetManager> r4 = android.content.res.AssetManager.class
            java.lang.String r5 = "addAssetPath"
            r6 = 1
            java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Throwable -> 0x0065, all -> 0x0073 }
            r7 = 0
            java.lang.Class<java.lang.String> r8 = java.lang.String.class
            r6[r7] = r8     // Catch:{ Throwable -> 0x0065, all -> 0x0073 }
            java.lang.reflect.Method r4 = r4.getDeclaredMethod(r5, r6)     // Catch:{ Throwable -> 0x0065, all -> 0x0073 }
            r5 = 1
            r4.setAccessible(r5)     // Catch:{ Throwable -> 0x0065, all -> 0x0073 }
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0065, all -> 0x0073 }
            r6 = 0
            r5[r6] = r11     // Catch:{ Throwable -> 0x0065, all -> 0x0073 }
            r4.invoke(r0, r5)     // Catch:{ Throwable -> 0x0065, all -> 0x0073 }
            java.lang.String r4 = "AndroidManifest.xml"
            android.content.res.XmlResourceParser r1 = r0.openXmlResourceParser(r4)     // Catch:{ Throwable -> 0x0065, all -> 0x0073 }
            if (r1 == 0) goto L_0x007a
            int r0 = r1.getEventType()     // Catch:{ Throwable -> 0x0084, all -> 0x0073 }
            r4 = r3
        L_0x0037:
            if (r0 == r9) goto L_0x007a
            switch(r0) {
                case 2: goto L_0x0047;
                default: goto L_0x003c;
            }     // Catch:{ Throwable -> 0x0084, all -> 0x0073 }
        L_0x003c:
            r0 = r4
        L_0x003d:
            if (r0 != 0) goto L_0x007a
            int r4 = r1.nextToken()     // Catch:{ Throwable -> 0x0084, all -> 0x0073 }
            r10 = r0
            r0 = r4
            r4 = r10
            goto L_0x0037
        L_0x0047:
            r0 = r3
        L_0x0048:
            int r5 = r1.getAttributeCount()     // Catch:{ Throwable -> 0x0084, all -> 0x0073 }
            if (r0 >= r5) goto L_0x003c
            java.lang.String r5 = r1.getAttributeName(r0)     // Catch:{ Throwable -> 0x0084, all -> 0x0073 }
            java.lang.String r6 = "minSdkVersion"
            boolean r5 = r5.equals(r6)     // Catch:{ Throwable -> 0x0084, all -> 0x0073 }
            if (r5 == 0) goto L_0x0062
            int r4 = r4 + 1
            java.lang.String r2 = r1.getAttributeValue(r0)     // Catch:{ Throwable -> 0x0084, all -> 0x0073 }
            r0 = r4
            goto L_0x003d
        L_0x0062:
            int r0 = r0 + 1
            goto L_0x0048
        L_0x0065:
            r0 = move-exception
            r10 = r0
            r0 = r2
            r2 = r1
            r1 = r10
        L_0x006a:
            r1.printStackTrace()     // Catch:{ all -> 0x0081 }
            if (r2 == 0) goto L_0x0072
            r2.close()
        L_0x0072:
            return r0
        L_0x0073:
            r0 = move-exception
        L_0x0074:
            if (r1 == 0) goto L_0x0079
            r1.close()
        L_0x0079:
            throw r0
        L_0x007a:
            r0 = r2
            if (r1 == 0) goto L_0x0072
            r1.close()
            goto L_0x0072
        L_0x0081:
            r0 = move-exception
            r1 = r2
            goto L_0x0074
        L_0x0084:
            r0 = move-exception
            r10 = r0
            r0 = r2
            r2 = r1
            r1 = r10
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.e.e(java.lang.String):java.lang.String");
    }

    public static List<String> a(Context context) {
        List<ActivityManager.RecentTaskInfo> recentTasks = ((ActivityManager) context.getSystemService("activity")).getRecentTasks(Integer.MAX_VALUE, 0);
        ArrayList arrayList = new ArrayList();
        if (recentTasks != null) {
            for (ActivityManager.RecentTaskInfo next : recentTasks) {
                if (!(next.baseIntent == null || next.baseIntent.getComponent() == null)) {
                    arrayList.add(next.baseIntent.getComponent().getPackageName());
                }
            }
        }
        return arrayList;
    }

    private static List<String> a(List<String> list, List<String> list2) {
        int i;
        ArrayList arrayList = new ArrayList();
        if (list == null || list.size() == 0) {
            arrayList.addAll(list2);
        } else {
            HashMap hashMap = new HashMap();
            for (int i2 = 0; i2 < list.size(); i2++) {
                hashMap.put(list.get(i2), Integer.valueOf(i2));
            }
            int size = list2.size() - 1;
            while (true) {
                if (size <= 0) {
                    i = -1;
                    break;
                }
                Integer num = (Integer) hashMap.get(list2.get(size));
                Integer num2 = (Integer) hashMap.get(list2.get(size - 1));
                if (num == null) {
                    i = size;
                    break;
                } else if (num2 == null || num2.intValue() > num.intValue()) {
                    i = size - 1;
                } else {
                    size--;
                }
            }
            i = size - 1;
            if (i > -1) {
                for (int i3 = 0; i3 <= i; i3++) {
                    arrayList.add(list2.get(i3));
                }
            }
        }
        return arrayList;
    }

    public static String c() {
        Throwable th;
        String str;
        try {
            str = Settings.Secure.getString(AstApp.i().getContentResolver(), "default_input_method");
            try {
                XLog.d("ApkUtil", "getDefaultInputmethodPackage. inputMethod before process : " + str);
                if (str == null) {
                    return str;
                }
                String substring = str.substring(0, str.indexOf("/"));
                XLog.d("ApkUtil", "getDefaultInputmethodPackage.inputMethod after process : " + substring);
                return substring;
            } catch (Throwable th2) {
                th = th2;
                XLog.e("ApkUtil", "getDefaultInputmethodPackage throws a exception.");
                th.printStackTrace();
                return str;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            str = null;
            th = th4;
            XLog.e("ApkUtil", "getDefaultInputmethodPackage throws a exception.");
            th.printStackTrace();
            return str;
        }
    }

    public static List<String> b(Context context) {
        x xVar = new x(AstApp.i());
        List<String> a2 = xVar.a();
        List<String> a3 = a(context);
        List<String> a4 = a(a2, a3);
        xVar.a(a3);
        return a4;
    }

    public static boolean f(Context context, String str) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                if (runningAppProcessInfo.processName.equals(str)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static List<String> d() {
        String str;
        ArrayList arrayList = new ArrayList();
        ActivityManager activityManager = (ActivityManager) AstApp.i().getSystemService("activity");
        if (Build.VERSION.SDK_INT <= 19) {
            try {
                List<ActivityManager.RunningTaskInfo> runningTasks = activityManager.getRunningTasks(1);
                if (runningTasks != null && runningTasks.size() > 0) {
                    arrayList.add(runningTasks.get(0).topActivity.getPackageName());
                }
            } catch (Throwable th) {
            }
        } else {
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
            if (runningAppProcesses != null && runningAppProcesses.size() > 0) {
                for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                    if (next.importance == 100) {
                        if (next.processName.indexOf(":") == -1) {
                            str = next.processName;
                        } else {
                            str = next.processName.split(":")[0];
                        }
                        if (!arrayList.contains(str)) {
                            arrayList.add(str);
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    public static boolean a(Context context, String str, IPackageStatsObserver iPackageStatsObserver) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageManager.class.getMethod("getPackageSizeInfo", String.class, IPackageStatsObserver.class).invoke(packageManager, str, iPackageStatsObserver);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static int a(LocalApkInfo localApkInfo) {
        if (Build.VERSION.SDK_INT > 7) {
            if ((localApkInfo.flags & NativeFileObject.S_IFDIR) != 0) {
                return 2;
            }
        } else if (!localApkInfo.mLocalFilePath.startsWith(Environment.getExternalStorageDirectory().getPath())) {
            return 1;
        } else {
            return 2;
        }
        return 1;
    }

    public static long a(int i) {
        long j;
        long j2;
        long j3;
        try {
            if (f1844a == null) {
                f1844a = Class.forName("android.net.TrafficStats");
            }
            if (b == null) {
                b = f1844a.getDeclaredMethod("getUidRxBytes", Integer.TYPE);
            }
            if (c == null) {
                c = f1844a.getDeclaredMethod("getUidTxBytes", Integer.TYPE);
            }
            j = ((Long) b.invoke(null, Integer.valueOf(i))).longValue();
            try {
                j2 = ((Long) c.invoke(null, Integer.valueOf(i))).longValue();
                j3 = j;
            } catch (SecurityException e) {
                e = e;
                e.printStackTrace();
                j2 = -1;
                j3 = j;
                return j2 + j3;
            } catch (NoSuchMethodException e2) {
                e = e2;
                e.printStackTrace();
                j2 = -1;
                j3 = j;
                return j2 + j3;
            } catch (ClassNotFoundException e3) {
                e = e3;
                e.printStackTrace();
                j2 = -1;
                j3 = j;
                return j2 + j3;
            } catch (Exception e4) {
                e = e4;
                e.printStackTrace();
                j2 = -1;
                j3 = j;
                return j2 + j3;
            }
        } catch (SecurityException e5) {
            e = e5;
            j = -1;
            e.printStackTrace();
            j2 = -1;
            j3 = j;
            return j2 + j3;
        } catch (NoSuchMethodException e6) {
            e = e6;
            j = -1;
            e.printStackTrace();
            j2 = -1;
            j3 = j;
            return j2 + j3;
        } catch (ClassNotFoundException e7) {
            e = e7;
            j = -1;
            e.printStackTrace();
            j2 = -1;
            j3 = j;
            return j2 + j3;
        } catch (Exception e8) {
            e = e8;
            j = -1;
            e.printStackTrace();
            j2 = -1;
            j3 = j;
            return j2 + j3;
        }
        return j2 + j3;
    }

    public static List<com.tencent.assistant.localres.model.a> e() {
        List<PackageInfo> list;
        try {
            list = AstApp.i().getPackageManager().getInstalledPackages(0);
        } catch (Throwable th) {
            th.printStackTrace();
            list = null;
        }
        if (list == null) {
            return null;
        }
        long currentTimeMillis = System.currentTimeMillis() - SystemClock.elapsedRealtime();
        ArrayList arrayList = new ArrayList();
        for (PackageInfo packageInfo : list) {
            long a2 = a(packageInfo.applicationInfo.uid);
            if (a2 > 0) {
                com.tencent.assistant.localres.model.a aVar = new com.tencent.assistant.localres.model.a();
                aVar.f845a = packageInfo.packageName;
                aVar.b = packageInfo.versionCode;
                aVar.c = packageInfo.applicationInfo.uid;
                aVar.e = currentTimeMillis;
                aVar.d = currentTimeMillis;
                aVar.f = 0;
                aVar.h = a2;
                aVar.g = System.currentTimeMillis();
                arrayList.add(aVar);
            }
        }
        return arrayList;
    }

    public static long f() {
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return 0;
        }
        StatFs statFs = new StatFs(new File(Environment.getExternalStorageDirectory().getPath()).getPath());
        long blockSize = ((long) statFs.getBlockSize()) * (((long) statFs.getAvailableBlocks()) - 4);
        if (blockSize < 0) {
            return 0;
        }
        return blockSize;
    }

    public static int a(PackageManager packageManager, String str) {
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 128);
            if (applicationInfo == null || applicationInfo.metaData == null) {
                return 0;
            }
            return applicationInfo.metaData.getInt(AppConst.KEY_META_DATA_GRAY_CODE);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static int b(PackageManager packageManager, String str) {
        PackageInfo a2;
        if (TextUtils.isEmpty(str) || (a2 = a(packageManager, str, 128)) == null) {
            return 0;
        }
        try {
            ApplicationInfo applicationInfo = a2.applicationInfo;
            if (applicationInfo == null || applicationInfo.metaData == null) {
                return 0;
            }
            return applicationInfo.metaData.getInt(AppConst.KEY_META_DATA_GRAY_CODE);
        } catch (Exception e) {
            return 0;
        }
    }

    public static List<PackageInfo> c(Context context) {
        try {
            return context.getPackageManager().getInstalledPackages(0);
        } catch (Exception e) {
            return new ArrayList();
        }
    }

    public static boolean b(int i) {
        return (i & 1) != 0 && (i & 128) == 0;
    }
}
