package com.tencent.assistant.utils;

import com.jg.EType;
import com.jg.JgClassChecked;
import java.lang.reflect.Field;
import java.util.HashSet;

@JgClassChecked(author = 81687, fComment = "确认已进行安全校验", lastDate = "20140715", reviewer = 71024, vComment = {EType.JSEXECUTECHECK, EType.ACTIVITYCHECK})
/* compiled from: ProGuard */
public class ca {
    public static void a() {
        try {
            Field declaredField = Class.forName("android.webkit.WebViewClassic").getDeclaredField("sGoogleApps");
            declaredField.setAccessible(true);
            declaredField.set(declaredField, new HashSet());
        } catch (Exception e) {
        }
    }
}
