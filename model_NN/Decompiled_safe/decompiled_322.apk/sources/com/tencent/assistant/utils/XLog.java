package com.tencent.assistant.utils;

import android.content.Context;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.Global;

/* compiled from: ProGuard */
public class XLog {
    public static final boolean LOG_FILE = false;
    public static Context context = AstApp.i();

    public static void v(String str, String str2) {
        if (Global.isDev()) {
            Log.v(str, str2);
        }
    }

    public static void v(String str, String str2, Throwable th) {
        if (Global.isDev()) {
            Log.v(str, str2, th);
        }
    }

    public static void v(String str) {
        v("assistant", str);
    }

    public static void d(String str, String str2) {
        if (Global.isDev()) {
            Log.d(str, a(str2));
        }
    }

    public static void d(String str, String str2, Throwable th) {
        if (Global.isDev()) {
            Log.d(str, a(str2), th);
        }
    }

    private static String a(String str) {
        String str2;
        StackTraceElement[] stackTrace = new Throwable().fillInStackTrace().getStackTrace();
        int i = 2;
        while (true) {
            if (i >= stackTrace.length) {
                str2 = "<unknown>";
                break;
            } else if (!stackTrace[i].getClass().equals(XLog.class)) {
                String className = stackTrace[i].getClassName();
                String substring = className.substring(className.lastIndexOf(46) + 1);
                str2 = substring.substring(substring.lastIndexOf(36) + 1) + "." + stackTrace[i].getMethodName() + "(" + stackTrace[i].getLineNumber() + ")";
                break;
            } else {
                i++;
            }
        }
        return String.format("[%s:%d] %s: %s", Thread.currentThread().getName(), Long.valueOf(Thread.currentThread().getId()), str2, str);
    }

    public static void d(String str) {
        d("assistant", str);
    }

    public static void i(String str, String str2) {
        if (Global.isDev()) {
            Log.i(str, str2);
        }
    }

    public static void i(String str, String str2, Throwable th) {
        if (Global.isDev()) {
            Log.i(str, str2, th);
        }
    }

    public static void i(String str) {
        i("assistant", str);
    }

    public static void w(String str, String str2) {
        if (Global.isDev()) {
            Log.w(str, str2);
        }
    }

    public static void w(String str, String str2, Throwable th) {
        if (Global.isDev()) {
            Log.w(str, str2, th);
        }
    }

    public static void w(String str) {
        w("assistant", str);
    }

    public static void e(String str, String str2) {
        if (Global.isDev()) {
            Log.e(str, str2);
        }
    }

    public static void e(String str, String str2, Throwable th) {
        if (Global.isDev()) {
            Log.e(str, str2, th);
        }
    }

    public static void e(String str) {
        e("assistant", str);
    }

    public static void f(String str, String str2, boolean z) {
        writeToFile(str, str2, z);
    }

    public static void f(String str, String str2) {
        writeToFile(str, str2, false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x005c A[SYNTHETIC, Splitter:B:12:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0064 A[SYNTHETIC, Splitter:B:18:0x0064] */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void writeToFile(java.lang.String r6, java.lang.String r7, boolean r8) {
        /*
            boolean r0 = com.tencent.assistant.Global.isDev()
            if (r0 == 0) goto L_0x0058
            r1 = 0
            java.io.BufferedWriter r0 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x0060, all -> 0x0059 }
            java.io.FileWriter r2 = new java.io.FileWriter     // Catch:{ Exception -> 0x0060, all -> 0x0059 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0060, all -> 0x0059 }
            r3.<init>()     // Catch:{ Exception -> 0x0060, all -> 0x0059 }
            java.lang.String r4 = com.tencent.assistant.utils.FileUtil.getLogDir()     // Catch:{ Exception -> 0x0060, all -> 0x0059 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0060, all -> 0x0059 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0060, all -> 0x0059 }
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Exception -> 0x0060, all -> 0x0059 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0060, all -> 0x0059 }
            r2.<init>(r3, r8)     // Catch:{ Exception -> 0x0060, all -> 0x0059 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0060, all -> 0x0059 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0079, all -> 0x0074 }
            r1.<init>()     // Catch:{ Exception -> 0x0079, all -> 0x0074 }
            java.lang.String r2 = com.tencent.assistant.utils.bo.a()     // Catch:{ Exception -> 0x0079, all -> 0x0074 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0079, all -> 0x0074 }
            java.lang.String r2 = " "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0079, all -> 0x0074 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Exception -> 0x0079, all -> 0x0074 }
            java.lang.String r2 = "\r\n"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0079, all -> 0x0074 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0079, all -> 0x0074 }
            r0.write(r1)     // Catch:{ Exception -> 0x0079, all -> 0x0074 }
            r0.flush()     // Catch:{ Exception -> 0x0079, all -> 0x0074 }
            if (r0 == 0) goto L_0x0058
            r0.close()     // Catch:{ Exception -> 0x0072 }
        L_0x0058:
            return
        L_0x0059:
            r0 = move-exception
        L_0x005a:
            if (r1 == 0) goto L_0x005f
            r1.close()     // Catch:{ Exception -> 0x006d }
        L_0x005f:
            throw r0
        L_0x0060:
            r0 = move-exception
            r0 = r1
        L_0x0062:
            if (r0 == 0) goto L_0x0058
            r0.close()     // Catch:{ Exception -> 0x0068 }
            goto L_0x0058
        L_0x0068:
            r0 = move-exception
        L_0x0069:
            r0.printStackTrace()
            goto L_0x0058
        L_0x006d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005f
        L_0x0072:
            r0 = move-exception
            goto L_0x0069
        L_0x0074:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x005a
        L_0x0079:
            r1 = move-exception
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.XLog.writeToFile(java.lang.String, java.lang.String, boolean):void");
    }

    public static void printCallTraces(String str) {
        if (Global.isDev()) {
            StackTraceElement[] stackTraceElementArr = Thread.getAllStackTraces().get(Thread.currentThread());
            v(str, "======================start============================");
            for (StackTraceElement stackTraceElement : stackTraceElementArr) {
                v(str, stackTraceElement.toString());
            }
            v(str, "=======================end============================");
        }
    }

    public static void fLog(String str, String str2, String str3) {
        fLog(str, str2, str3, true, true, false);
    }

    public static void fLog(String str, String str2, boolean z) {
        fLog(str, str2, null, true, true, z);
    }

    public static void fLog(String str, String str2, String str3, boolean z, boolean z2) {
        fLog(str, str2, str3, z, z2, false);
    }

    public static void fLog(String str, String str2, String str3, boolean z, boolean z2, boolean z3) {
        boolean z4 = false;
        if (Global.isDev()) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            StringBuilder sb = new StringBuilder();
            String str4 = str + " " + bo.b() + " " + Process.myPid() + "(" + AstApp.i().s() + ")/" + Thread.currentThread().getName() + "," + Thread.currentThread().getId() + " ";
            sb.append(str4).append(" -------->start<--------.\n");
            sb.append(str4).append("msg:").append(str2).append("\n");
            if (z2) {
                d(str4, str4 + " msg:" + str2);
            }
            if (z) {
                sb.append(str4).append(" info stack:\n");
                for (StackTraceElement stackTraceElement : stackTrace) {
                    String stackTraceElement2 = stackTraceElement.toString();
                    if (z4) {
                        sb.append(str4).append(" ").append(stackTraceElement2).append("\n");
                    }
                    if (!z4 && stackTraceElement2.contains("XLog")) {
                        z4 = true;
                    }
                }
            }
            sb.append(str4).append(" -------->end<--------.\n\n");
            if (z3) {
                d(str4, sb.toString());
            }
            if (!TextUtils.isEmpty(str3)) {
                f(sb.toString(), str3, true);
            }
        }
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0043 A[SYNTHETIC, Splitter:B:16:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0050 A[SYNTHETIC, Splitter:B:24:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:43:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void f(java.lang.Throwable r8) {
        /*
            r1 = 0
            boolean r0 = com.tencent.assistant.Global.isDev()
            if (r0 == 0) goto L_0x003e
            java.io.StringWriter r2 = new java.io.StringWriter     // Catch:{ Exception -> 0x004c, all -> 0x003f }
            r2.<init>()     // Catch:{ Exception -> 0x004c, all -> 0x003f }
            java.io.PrintWriter r0 = new java.io.PrintWriter     // Catch:{ Exception -> 0x006c, all -> 0x0065 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x006c, all -> 0x0065 }
            r8.printStackTrace(r0)     // Catch:{ Exception -> 0x0070, all -> 0x0067 }
            java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x0070, all -> 0x0067 }
            java.lang.String r3 = "p.com.qq.connect"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0070, all -> 0x0067 }
            r4.<init>()     // Catch:{ Exception -> 0x0070, all -> 0x0067 }
            java.lang.String r5 = "exception INFO: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0070, all -> 0x0067 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x0070, all -> 0x0067 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0070, all -> 0x0067 }
            java.lang.String r4 = "dkevin.txt"
            r5 = 1
            r6 = 0
            fLog(r3, r1, r4, r5, r6)     // Catch:{ Exception -> 0x0070, all -> 0x0067 }
            if (r2 == 0) goto L_0x0039
            r2.close()     // Catch:{ IOException -> 0x0060 }
        L_0x0039:
            if (r0 == 0) goto L_0x003e
        L_0x003b:
            r0.close()
        L_0x003e:
            return
        L_0x003f:
            r0 = move-exception
            r2 = r1
        L_0x0041:
            if (r2 == 0) goto L_0x0046
            r2.close()     // Catch:{ IOException -> 0x005b }
        L_0x0046:
            if (r1 == 0) goto L_0x004b
            r1.close()
        L_0x004b:
            throw r0
        L_0x004c:
            r0 = move-exception
            r0 = r1
        L_0x004e:
            if (r1 == 0) goto L_0x0053
            r1.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0053:
            if (r0 == 0) goto L_0x003e
            goto L_0x003b
        L_0x0056:
            r1 = move-exception
            r8.printStackTrace()
            goto L_0x0053
        L_0x005b:
            r2 = move-exception
            r8.printStackTrace()
            goto L_0x0046
        L_0x0060:
            r1 = move-exception
            r8.printStackTrace()
            goto L_0x0039
        L_0x0065:
            r0 = move-exception
            goto L_0x0041
        L_0x0067:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0041
        L_0x006c:
            r0 = move-exception
            r0 = r1
            r1 = r2
            goto L_0x004e
        L_0x0070:
            r1 = move-exception
            r1 = r2
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.XLog.f(java.lang.Throwable):void");
    }

    public static String getJceString(JceStruct jceStruct) {
        if (jceStruct == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        jceStruct.display(sb, 10);
        return sb.toString();
    }
}
