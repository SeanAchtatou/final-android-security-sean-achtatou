package com.tencent.assistant.utils.a;

import java.io.DataInputStream;
import java.io.DataOutputStream;

/* compiled from: ProGuard */
public class j {

    /* renamed from: a  reason: collision with root package name */
    short f1811a;
    short b;
    short c;
    short d;
    short e;
    int f;
    int g;
    int h;
    short i;
    short j;
    byte[] k;
    byte[] l;

    public static void a(f fVar, DataOutputStream dataOutputStream) {
        fVar.q = dataOutputStream.size();
        dataOutputStream.writeInt(1347093252);
        dataOutputStream.writeShort(e.a(fVar.b));
        dataOutputStream.writeShort(e.a(fVar.c));
        dataOutputStream.writeShort(e.a(fVar.d));
        dataOutputStream.writeShort(e.a(fVar.e));
        dataOutputStream.writeShort(e.a(fVar.f));
        if ((fVar.c & 8) == 8) {
            dataOutputStream.writeInt(e.a(0));
            dataOutputStream.writeInt(e.a(0));
            dataOutputStream.writeInt(e.a(0));
        } else {
            dataOutputStream.writeInt(e.a(fVar.g));
            dataOutputStream.writeInt(e.a(fVar.h));
            dataOutputStream.writeInt(e.a(fVar.i));
        }
        dataOutputStream.writeShort(e.a(fVar.j));
        dataOutputStream.writeShort(e.a(fVar.k));
        if (fVar.j > 0) {
            dataOutputStream.write(fVar.s);
        }
        if (fVar.k > 0) {
            dataOutputStream.write(fVar.t);
        }
    }

    public void a(DataInputStream dataInputStream) {
        this.f1811a = e.a(dataInputStream.readShort());
        this.b = e.a(dataInputStream.readShort());
        this.c = e.a(dataInputStream.readShort());
        this.d = e.a(dataInputStream.readShort());
        this.e = e.a(dataInputStream.readShort());
        this.f = e.a(dataInputStream.readInt());
        this.g = e.a(dataInputStream.readInt());
        this.h = e.a(dataInputStream.readInt());
        this.i = e.a(dataInputStream.readShort());
        this.j = e.a(dataInputStream.readShort());
        this.k = new byte[this.i];
        this.l = new byte[this.j];
        dataInputStream.read(this.k, 0, this.i);
        dataInputStream.read(this.l, 0, this.j);
    }
}
