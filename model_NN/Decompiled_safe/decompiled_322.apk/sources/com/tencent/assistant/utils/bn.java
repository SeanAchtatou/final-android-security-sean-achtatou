package com.tencent.assistant.utils;

import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class bn {

    /* renamed from: a  reason: collision with root package name */
    private long f1839a = System.currentTimeMillis();
    private String b = Constants.STR_EMPTY;

    public String a() {
        return a((String) null);
    }

    public String a(String str) {
        String str2;
        long currentTimeMillis = System.currentTimeMillis();
        long j = currentTimeMillis - this.f1839a;
        if (str != null) {
            this.b = str;
        }
        if (j > 10000) {
            str2 = this.b + " cost:" + (j / 1000) + "s";
        } else {
            str2 = this.b + " cost:" + j + "ms";
        }
        this.f1839a = currentTimeMillis;
        return str2;
    }

    public static String a(long j) {
        long currentTimeMillis = System.currentTimeMillis() - j;
        if (currentTimeMillis > 10000) {
            return (currentTimeMillis / 1000) + "s";
        }
        return currentTimeMillis + "ms";
    }

    public String toString() {
        return a();
    }
}
