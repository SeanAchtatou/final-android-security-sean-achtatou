package com.tencent.assistant.manager;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class j {

    /* renamed from: a  reason: collision with root package name */
    protected ReferenceQueue<k> f860a = new ReferenceQueue<>();
    protected ConcurrentLinkedQueue<WeakReference<k>> b = new ConcurrentLinkedQueue<>();

    protected j() {
    }

    /* access modifiers changed from: protected */
    public void a(k kVar) {
        if (kVar != null) {
            while (true) {
                Reference<? extends k> poll = this.f860a.poll();
                if (poll == null) {
                    break;
                }
                this.b.remove(poll);
            }
            Iterator<WeakReference<k>> it = this.b.iterator();
            while (it.hasNext()) {
                if (((k) it.next().get()) == kVar) {
                    return;
                }
            }
            this.b.add(new WeakReference(kVar, this.f860a));
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        Iterator<WeakReference<k>> it = this.b.iterator();
        while (it.hasNext()) {
            k kVar = (k) it.next().get();
            if (kVar != null) {
                try {
                    kVar.a();
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e2) {
                    e2.printStackTrace();
                }
            }
        }
    }
}
