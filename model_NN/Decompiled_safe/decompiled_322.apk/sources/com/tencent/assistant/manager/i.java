package com.tencent.assistant.manager;

import com.tencent.assistant.db.a;
import com.tencent.wcs.jce.ServerList;

/* compiled from: ProGuard */
public class i extends f {

    /* renamed from: a  reason: collision with root package name */
    private static i f859a = null;

    public static synchronized i y() {
        i iVar;
        synchronized (i.class) {
            if (f859a == null) {
                f859a = new i();
            }
            iVar = f859a;
        }
        return iVar;
    }

    private i() {
    }

    public boolean a(ServerList serverList) {
        return a.a("connect_ip_data_list", serverList);
    }

    public ServerList z() {
        return (ServerList) a.a("connect_ip_data_list", ServerList.class);
    }
}
