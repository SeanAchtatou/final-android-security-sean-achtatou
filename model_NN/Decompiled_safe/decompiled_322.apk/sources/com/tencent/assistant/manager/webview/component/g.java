package com.tencent.assistant.manager.webview.component;

import android.app.Activity;
import com.tencent.assistant.utils.XLog;
import com.tencent.smtt.sdk.WebView;

/* compiled from: ProGuard */
class g implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TxWebViewContainer f914a;

    g(TxWebViewContainer txWebViewContainer) {
        this.f914a = txWebViewContainer;
    }

    public Activity a() {
        return this.f914a.t();
    }

    public void a(WebView webView, int i) {
        this.f914a.d.setProgress(i);
        if (this.f914a.h != null) {
            this.f914a.h.a(webView, i);
        }
    }

    public void a(WebView webView, String str) {
        XLog.d("TxWebViewContainer", "onReceivedTitle--title = " + str);
        if (this.f914a.t() != null) {
            this.f914a.t().setTitle(str);
        }
    }
}
