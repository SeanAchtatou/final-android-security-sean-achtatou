package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.nucleus.manager.setting.SettingActivity;

/* compiled from: ProGuard */
class z extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f610a;
    final /* synthetic */ int b;
    final /* synthetic */ y c;

    z(y yVar, int i, int i2) {
        this.c = yVar;
        this.f610a = i;
        this.b = i2;
    }

    public void onTMAClick(View view) {
        ItemElement a2 = this.c.a(this.f610a, this.b);
        if (a2 != null) {
            ((SettingActivity) this.c.d).a(a2, view, this.b);
        }
    }

    public STInfoV2 getStInfo(View view) {
        if (!(view.getTag() instanceof af)) {
            return null;
        }
        return this.c.a(this.c.d(this.f610a, this.b), this.c.b(((af) view.getTag()).g.b()), 200);
    }
}
