package com.tencent.assistant.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Message;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistant.component.RatingView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.by;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import java.util.List;

/* compiled from: ProGuard */
public class RankClassicListAdapter extends BaseAdapter implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    protected static int f575a = 0;
    protected static int b = (f575a + 1);
    protected static int c = (b + 1);
    protected ListType d;
    protected Context e;
    protected LayoutInflater f;
    protected List<SimpleAppModel> g;
    protected View h;
    protected int i;
    protected long j;
    protected long k;
    protected b l;
    private boolean m;

    /* compiled from: ProGuard */
    public enum ListType {
        LISTTYPENORMAL,
        LISTTYPEGAMESORT
    }

    public int getCount() {
        if (this.g == null) {
            return 0;
        }
        return this.g.size();
    }

    /* renamed from: a */
    public SimpleAppModel getItem(int i2) {
        if (this.g == null) {
            return null;
        }
        return this.g.get(i2);
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        SimpleAppModel simpleAppModel;
        if (this.g == null || i2 >= this.g.size()) {
            simpleAppModel = null;
        } else {
            simpleAppModel = this.g.get(i2);
        }
        if (f575a == getItemViewType(i2)) {
            return a(view, simpleAppModel, i2);
        }
        if (b == getItemViewType(i2)) {
            return b(view, simpleAppModel, i2);
        }
        return view;
    }

    public int getItemViewType(int i2) {
        if (!a()) {
            return f575a;
        }
        SimpleAppModel.CARD_TYPE card_type = this.g.get(i2).U;
        if (SimpleAppModel.CARD_TYPE.NORMAL == card_type) {
            return f575a;
        }
        if (SimpleAppModel.CARD_TYPE.QUALITY == card_type) {
            return b;
        }
        return f575a;
    }

    public int getViewTypeCount() {
        return b + 1;
    }

    /* access modifiers changed from: private */
    public String b(int i2) {
        return "03_" + bm.a(i2 + 1);
    }

    /* access modifiers changed from: protected */
    public View a(View view, SimpleAppModel simpleAppModel, int i2) {
        x xVar;
        int i3;
        if (view == null || ((w) view.getTag()).f607a == null) {
            w wVar = new w(this, null);
            xVar = new x(this, null);
            view = this.f.inflate((int) R.layout.app_item_no_desc, (ViewGroup) null);
            xVar.f608a = view.findViewById(R.id.app_item);
            xVar.c = (TextView) view.findViewById(R.id.text_sort);
            xVar.d = (AppIconView) view.findViewById(R.id.app_icon_img);
            xVar.e = (TextView) view.findViewById(R.id.app_name_txt);
            xVar.h = (DownloadButton) view.findViewById(R.id.state_app_btn);
            xVar.f = (RatingView) view.findViewById(R.id.app_ratingview);
            xVar.g = (TextView) view.findViewById(R.id.download_times_txt);
            xVar.i = view.findViewById(R.id.app_updatesizeinfo);
            xVar.j = (TextView) view.findViewById(R.id.app_size_sumsize);
            xVar.k = (TextView) view.findViewById(R.id.app_score_truesize);
            xVar.l = (TextView) view.findViewById(R.id.app_size_text);
            xVar.b = (ImageView) view.findViewById(R.id.sort_num_image);
            xVar.m = (ImageView) view.findViewById(R.id.last_line);
            wVar.f607a = xVar;
            view.setTag(wVar);
        } else {
            xVar = ((w) view.getTag()).f607a;
        }
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) xVar.e.getLayoutParams();
        if (simpleAppModel == null || (!simpleAppModel.c() && !simpleAppModel.h())) {
            layoutParams.rightMargin = by.b(72.0f);
        } else {
            layoutParams.rightMargin = by.b(101.0f);
        }
        xVar.e.setLayoutParams(layoutParams);
        xVar.f608a.setOnClickListener(new p(this, simpleAppModel, i2));
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) xVar.d.getLayoutParams();
        if (ListType.LISTTYPEGAMESORT == this.d) {
            layoutParams2.leftMargin = 0;
            try {
                xVar.c.setVisibility(0);
                xVar.c.setText((CharSequence) null);
                if (i2 <= 2) {
                    if (i2 == 0) {
                        i3 = R.drawable.sort01;
                    } else if (1 == i2) {
                        i3 = R.drawable.sort02;
                    } else {
                        i3 = R.drawable.sort03;
                    }
                    ImageSpan imageSpan = new ImageSpan(this.e, BitmapFactory.decodeResource(this.e.getResources(), i3));
                    SpannableString spannableString = new SpannableString("icon");
                    spannableString.setSpan(imageSpan, 0, 4, 33);
                    xVar.c.setText(spannableString);
                } else {
                    xVar.c.setBackgroundResource(0);
                    xVar.c.setTextColor(this.e.getResources().getColor(R.color.rank_sort_txt));
                    xVar.c.setText(String.valueOf(i2 + 1));
                }
            } catch (Throwable th) {
                t.a().b();
            }
        } else {
            layoutParams2.leftMargin = by.a(this.e, 9.0f);
            xVar.c.setVisibility(8);
        }
        xVar.d.setLayoutParams(layoutParams2);
        if (i2 == 0) {
            xVar.f608a.setBackgroundResource(R.drawable.bg_card_download_selector);
            xVar.m.setVisibility(0);
        } else if (i2 == getCount() - 1) {
            xVar.f608a.setBackgroundResource(R.drawable.bg_card_download_down_selector);
            xVar.m.setVisibility(8);
        } else {
            xVar.f608a.setBackgroundResource(R.drawable.bg_card_download_middel_selector);
            xVar.m.setVisibility(0);
        }
        a(xVar, simpleAppModel, i2);
        return view;
    }

    private void a(x xVar, SimpleAppModel simpleAppModel, int i2) {
        if (simpleAppModel != null && xVar != null) {
            xVar.e.setText(simpleAppModel.d);
            if (this.m) {
                if (1 == (((int) (simpleAppModel.B >> 2)) & 3)) {
                    Drawable drawable = this.e.getResources().getDrawable(R.drawable.appdownload_icon_original);
                    drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                    xVar.e.setCompoundDrawablePadding(by.b(6.0f));
                    xVar.e.setCompoundDrawables(null, null, drawable, null);
                } else {
                    xVar.e.setCompoundDrawables(null, null, null, null);
                }
            }
            xVar.d.setSimpleAppModel(simpleAppModel, new StatInfo(simpleAppModel.b, this.i, this.l.d(), this.l.b(), this.l.a(), b(i2)), this.j);
            xVar.h.a(simpleAppModel);
            xVar.g.setText(bm.a(simpleAppModel.p, 0));
            xVar.f.setRating(simpleAppModel.q);
            if (simpleAppModel.a()) {
                xVar.i.setVisibility(0);
                xVar.l.setVisibility(8);
                xVar.j.setText(at.a(simpleAppModel.k));
                xVar.k.setText(at.a(simpleAppModel.v));
            } else {
                xVar.i.setVisibility(8);
                xVar.l.setVisibility(0);
                xVar.l.setText(at.a(simpleAppModel.k));
            }
            if (a(simpleAppModel)) {
                xVar.h.setClickable(false);
            } else {
                xVar.h.setClickable(true);
                STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e, simpleAppModel, b(i2), 200, null);
                xVar.h.a(buildSTInfo, new q(this), (d) null, xVar.h);
            }
            xVar.b.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public View b(View view, SimpleAppModel simpleAppModel, int i2) {
        v vVar;
        if (view == null || ((w) view.getTag()).b == null) {
            w wVar = new w(this, null);
            v vVar2 = new v(this, null);
            view = this.f.inflate((int) R.layout.competitive_card, (ViewGroup) null);
            vVar2.f606a = (TextView) view.findViewById(R.id.title);
            vVar2.b = (TXImageView) view.findViewById(R.id.pic);
            vVar2.d = (ImageView) view.findViewById(R.id.vedio);
            vVar2.e = (AppIconView) view.findViewById(R.id.icon);
            vVar2.f = (DownloadButton) view.findViewById(R.id.download_soft_btn);
            vVar2.g = (TextView) view.findViewById(R.id.name);
            vVar2.h = (TextView) view.findViewById(R.id.app_size_sumsize);
            vVar2.i = (ImageView) view.findViewById(R.id.app_size_redline);
            vVar2.j = (TextView) view.findViewById(R.id.app_score_truesize);
            vVar2.k = (TextView) view.findViewById(R.id.app_size_text);
            vVar2.c = (ImageView) view.findViewById(R.id.sort_num_image);
            vVar2.l = (TextView) view.findViewById(R.id.description);
            wVar.b = vVar2;
            view.setTag(wVar);
            vVar = vVar2;
        } else {
            vVar = ((w) view.getTag()).b;
        }
        view.setOnClickListener(new r(this, simpleAppModel, i2));
        a(vVar, simpleAppModel, i2);
        return view;
    }

    private void a(v vVar, SimpleAppModel simpleAppModel, int i2) {
        if (simpleAppModel != null && vVar != null) {
            if (!TextUtils.isEmpty(simpleAppModel.Y)) {
                vVar.b.updateImageView(simpleAppModel.Y, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            } else {
                vVar.b.setImageResource(R.drawable.pic_defaule);
            }
            if (simpleAppModel.Z == null || simpleAppModel.Z.length() == 0) {
                vVar.d.setVisibility(8);
                vVar.b.setDuplicateParentStateEnabled(true);
                vVar.b.setClickable(false);
            } else {
                vVar.d.setVisibility(0);
                vVar.b.setDuplicateParentStateEnabled(false);
                vVar.b.setClickable(true);
                vVar.b.setOnClickListener(new s(this, simpleAppModel));
            }
            vVar.e.setSimpleAppModel(simpleAppModel, new StatInfo(simpleAppModel.b, this.i, this.l.d(), this.l.b(), this.l.a(), b(i2)), this.j);
            vVar.f.a(simpleAppModel);
            vVar.g.setText(simpleAppModel.d);
            if (simpleAppModel.a()) {
                vVar.i.setVisibility(0);
                vVar.h.setVisibility(0);
                vVar.j.setVisibility(0);
                vVar.k.setVisibility(8);
                vVar.h.setText(at.a(simpleAppModel.k));
                vVar.j.setText(at.a(simpleAppModel.v));
            } else {
                vVar.i.setVisibility(8);
                vVar.h.setVisibility(8);
                vVar.j.setVisibility(8);
                vVar.k.setVisibility(0);
                vVar.k.setText(at.a(simpleAppModel.k));
            }
            if (TextUtils.isEmpty(simpleAppModel.X)) {
                vVar.l.setVisibility(8);
            } else {
                vVar.l.setVisibility(0);
                vVar.l.setText(simpleAppModel.X);
            }
            if (a(simpleAppModel)) {
                vVar.f.setClickable(false);
            } else {
                vVar.f.setClickable(true);
                STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e, simpleAppModel, b(i2), 200, null);
                vVar.f.a(buildSTInfo, new t(this), (d) null, vVar.f);
            }
            vVar.c.setVisibility(8);
        }
    }

    public void handleUIEvent(Message message) {
        DownloadInfo downloadInfo;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE /*1009*/:
                if (message.obj instanceof DownloadInfo) {
                    downloadInfo = (DownloadInfo) message.obj;
                } else {
                    downloadInfo = null;
                }
                if (downloadInfo != null && !TextUtils.isEmpty(downloadInfo.downloadTicket)) {
                    for (SimpleAppModel q : this.g) {
                        if (q.q().equals(downloadInfo.downloadTicket)) {
                            b();
                            return;
                        }
                    }
                    return;
                }
                return;
            case 1016:
                k.g(this.g);
                b();
                return;
            case EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY /*1038*/:
                b();
                return;
            default:
                return;
        }
    }

    private void b() {
        ah.a().post(new u(this));
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        if ((!c.e() || c.l()) && m.a().p()) {
            return false;
        }
        return true;
    }

    private boolean a(SimpleAppModel simpleAppModel) {
        return b(simpleAppModel) || c(simpleAppModel);
    }

    private boolean b(SimpleAppModel simpleAppModel) {
        if (simpleAppModel == null) {
            return false;
        }
        DownloadInfo d2 = DownloadProxy.a().d(simpleAppModel.q());
        if ((d2 == null || d2.versionCode != simpleAppModel.g) && simpleAppModel.d() && (simpleAppModel.P & 3) > 0 && !e.a(simpleAppModel.c, simpleAppModel.g, simpleAppModel.ad)) {
            return true;
        }
        return false;
    }

    private boolean c(SimpleAppModel simpleAppModel) {
        if (simpleAppModel == null) {
            return false;
        }
        DownloadInfo d2 = DownloadProxy.a().d(simpleAppModel.q());
        if ((d2 == null || d2.versionCode != simpleAppModel.g) && simpleAppModel.e() && (simpleAppModel.P & 3) > 0 && !e.a(simpleAppModel.c, simpleAppModel.g, simpleAppModel.ad)) {
            return true;
        }
        return false;
    }
}
