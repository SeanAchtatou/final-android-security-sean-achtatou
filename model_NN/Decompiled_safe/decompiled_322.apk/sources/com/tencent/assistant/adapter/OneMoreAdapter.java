package com.tencent.assistant.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.appdetail.process.s;
import java.util.List;

/* compiled from: ProGuard */
public class OneMoreAdapter extends BaseAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f574a;
    private List<SimpleAppModel> b;
    private int c = 2000;
    private String d = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public View e;
    private b f = new b();

    public OneMoreAdapter(Context context, View view, List<SimpleAppModel> list) {
        this.f574a = context;
        this.e = view;
        this.b = list;
        if (context instanceof BaseActivity) {
            this.c = ((BaseActivity) context).f();
        }
    }

    public void a(String str) {
        this.d = str;
    }

    public int getCount() {
        if (this.b != null) {
            return this.b.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        o oVar;
        SimpleAppModel simpleAppModel = this.b.get(i);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f574a, simpleAppModel, a.a(Constants.VIA_REPORT_TYPE_WPA_STATE, i), 100, null);
        if (buildSTInfo != null) {
            buildSTInfo.contentId = this.d;
        }
        if (view == null) {
            view = LayoutInflater.from(this.f574a).inflate((int) R.layout.app_one_more_item, (ViewGroup) null);
            o oVar2 = new o(this, null);
            oVar2.f599a = (TXAppIconView) view.findViewById(R.id.app_icon_img);
            oVar2.b = (TextView) view.findViewById(R.id.app_name_txt);
            oVar2.c = (DownloadButton) view.findViewById(R.id.state_app_btn);
            oVar2.h = (ListItemInfoView) view.findViewById(R.id.download_info);
            oVar2.d = view.findViewById(R.id.app_updatesizeinfo);
            oVar2.e = (TextView) view.findViewById(R.id.app_size_sumsize);
            oVar2.f = (TextView) view.findViewById(R.id.app_score_truesize);
            oVar2.g = (ImageView) view.findViewById(R.id.app_one_more_item_line);
            view.setTag(oVar2);
            oVar = oVar2;
        } else {
            oVar = (o) view.getTag();
        }
        view.setOnClickListener(new m(this, simpleAppModel, buildSTInfo));
        if (simpleAppModel != null) {
            oVar.b.setText(simpleAppModel.d);
            oVar.f599a.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        }
        oVar.c.a(simpleAppModel);
        oVar.h.a(ListItemInfoView.InfoType.ONEMORE_DESC);
        oVar.h.a(simpleAppModel);
        if (i == getCount() - 1) {
            oVar.g.setVisibility(8);
        } else {
            oVar.g.setVisibility(0);
        }
        if (s.a(simpleAppModel)) {
            oVar.c.setClickable(false);
        } else {
            oVar.c.setClickable(true);
            oVar.c.a(buildSTInfo, new n(this, simpleAppModel));
        }
        if (this.f != null) {
            this.f.exposure(buildSTInfo);
        }
        return view;
    }
}
