package com.tencent.cloud.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.l;
import android.support.v4.app.r;
import com.tencent.cloud.d.o;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class d extends r {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppRankActivity f2179a;
    private Activity b;
    private List<o> c;
    private HashMap<String, WeakReference<Fragment>> d = new HashMap<>();

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(AppRankActivity appRankActivity, l lVar, Activity activity, List<o> list) {
        super(lVar);
        this.f2179a = appRankActivity;
        this.b = activity;
        this.c = list;
    }

    public Fragment a(int i) {
        Fragment fragment;
        WeakReference weakReference;
        o oVar = this.c.get(i);
        if (oVar == null || (weakReference = this.d.get(oVar.f2313a)) == null || weakReference.get() == null) {
            fragment = null;
        } else {
            fragment = (Fragment) weakReference.get();
        }
        if (!(fragment != null || oVar == null || (fragment = a(i, oVar)) == null)) {
            this.d.put(oVar.f2313a, new WeakReference(fragment));
        }
        return fragment;
    }

    public int getCount() {
        return this.c.size();
    }

    private Fragment a(int i, o oVar) {
        Bundle bundle = new Bundle();
        bundle.putLong("subId", oVar.e);
        bundle.putInt("subAppListType", oVar.f);
        bundle.putInt("subPageSize", oVar.g);
        bundle.putByte("flag", oVar.h);
        bundle.putInt("content_id", i + 1);
        switch (oVar.b) {
            case 0:
                f fVar = new f(this.b);
                fVar.b(bundle);
                return fVar;
            case 1:
                return new al().a(oVar.d);
            default:
                return null;
        }
    }
}
