package com.tencent.cloud.activity;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.module.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class y extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateListActivity f2206a;

    y(UpdateListActivity updateListActivity) {
        this.f2206a = updateListActivity;
    }

    public void onTMAClick(View view) {
        if (k.b(this.f2206a.J) != 0) {
            this.f2206a.n.startActivity(new Intent(this.f2206a.n, UpdateIgnoreListActivity.class));
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 e = this.f2206a.x.e();
        if (e != null) {
            e.slotId = a.a("06", "001");
        }
        return e;
    }
}
