package com.tencent.cloud.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.adapter.AppAdapter;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.module.b;
import com.tencent.assistant.module.callback.a;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class TencentAppListActivity extends BaseActivity implements ITXRefreshListViewListener {
    private NormalErrorRecommendPage A;
    private ApkResCallback B = new r(this);
    private a C = new t(this);
    private View.OnClickListener D = new u(this);
    /* access modifiers changed from: private */
    public b n;
    /* access modifiers changed from: private */
    public TXGetMoreListView u;
    private SecondNavigationTitleViewV5 v;
    /* access modifiers changed from: private */
    public AppAdapter w;
    private long x = 0;
    private String y = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public LoadingView z;

    public int f() {
        return STConst.ST_PAGE_SOFTWARE_CATEGORY_TENCENT;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.author_applist_layout);
        u();
        v();
    }

    private void u() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.x = extras.getLong("com.tencent.assistant.CATATORY_ID");
            this.y = extras.getString("activityTitleName");
        }
    }

    private void v() {
        this.z = (LoadingView) findViewById(R.id.loading_view);
        this.z.setVisibility(0);
        this.A = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.A.setButtonClickListener(this.D);
        this.v = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.v.b(this.y);
        this.v.a(this);
        this.v.i();
        this.n = new b(-10, 20);
        this.u = (TXGetMoreListView) findViewById(R.id.author_applist);
        this.u.setVisibility(8);
        this.w = new AppAdapter(this, this.u, this.n.a(), true);
        this.w.a(f(), this.x, "03_");
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new AbsListView.LayoutParams(-1, by.a(this, 4.0f)));
        imageView.setBackgroundColor(getResources().getColor(17170445));
        this.u.addHeaderView(imageView);
        this.u.setAdapter(this.w);
        this.u.setDivider(null);
        this.u.setRefreshListViewListener(this);
        t();
    }

    public void t() {
        if (this.w.getCount() > 0) {
            w();
            this.w.notifyDataSetChanged();
        } else {
            this.n.c();
        }
        this.u.onRefreshComplete(this.n.g(), true);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.n.unregister(this.C);
        super.onPause();
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.B);
        this.v.m();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.n.register(this.C);
        this.w.notifyDataSetChanged();
        super.onResume();
        ApkResourceManager.getInstance().registerApkResCallback(this.B);
        this.v.l();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.n = null;
        this.u = null;
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void w() {
        this.u.setVisibility(0);
        this.z.setVisibility(8);
        this.A.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void b(int i) {
        this.u.setVisibility(8);
        this.z.setVisibility(8);
        this.A.setVisibility(0);
        this.A.setErrorType(i);
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        this.n.e();
    }
}
