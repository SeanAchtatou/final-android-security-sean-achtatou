package com.tencent.cloud.activity.debug;

import android.view.View;
import com.tencent.assistant.plugin.QReaderClient;

/* compiled from: ProGuard */
class i implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ QReaderPluginDebugActivity f2189a;

    i(QReaderPluginDebugActivity qReaderPluginDebugActivity) {
        this.f2189a = qReaderPluginDebugActivity;
    }

    public void onClick(View view) {
        QReaderClient.getInstance().downloadQQReader();
    }
}
