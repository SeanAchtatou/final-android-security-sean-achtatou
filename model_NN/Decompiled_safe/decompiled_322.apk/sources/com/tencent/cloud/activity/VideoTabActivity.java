package com.tencent.cloud.activity;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.cloud.component.VideoDetailView;

/* compiled from: ProGuard */
public class VideoTabActivity extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f2166a;
    private LayoutInflater b;
    private VideoDetailView c;
    private FrameLayout d;

    public VideoTabActivity(Context context) {
        this(context, null);
        this.f2166a = context;
    }

    public VideoTabActivity(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = null;
        this.d = null;
        this.f2166a = context;
        this.b = LayoutInflater.from(context);
        d();
    }

    private void d() {
        this.d = (FrameLayout) this.b.inflate((int) R.layout.video_tab_layout, this).findViewById(R.id.video_tab_layout);
    }

    public void a() {
        if (this.c != null) {
            this.c.c();
            return;
        }
        this.c = new VideoDetailView(this.f2166a);
        this.c.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.d.addView(this.c);
        this.c.c();
    }

    public void b() {
        if (this.c != null) {
            this.c.d();
        }
    }

    public int c() {
        return 3002;
    }
}
