package com.tencent.cloud.updaterec;

import com.tencent.assistantv2.component.k;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
class i extends k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateRecOneMoreListView f2340a;

    i(UpdateRecOneMoreListView updateRecOneMoreListView) {
        this.f2340a = updateRecOneMoreListView;
    }

    public void a(DownloadInfo downloadInfo) {
        if (downloadInfo != null && downloadInfo.needReCreateInfo(this.f2340a.f2333a)) {
            DownloadProxy.a().b(downloadInfo.downloadTicket);
            downloadInfo = DownloadInfo.createDownloadInfo(this.f2340a.f2333a, null);
        }
        if (downloadInfo != null) {
            a.a().a(downloadInfo);
        }
    }
}
