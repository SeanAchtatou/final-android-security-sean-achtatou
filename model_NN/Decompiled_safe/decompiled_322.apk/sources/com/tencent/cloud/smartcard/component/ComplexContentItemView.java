package com.tencent.cloud.smartcard.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.ContentAggregationComplexItem;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class ComplexContentItemView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private TXImageView f2323a;
    private TextView b;
    private TextView c;
    private TXImageView d;
    private ListItemInfoView e;
    private TextView f;
    private DownloadButton g;
    private Button h;
    private View i;

    public ComplexContentItemView(Context context) {
        this(context, null);
    }

    public ComplexContentItemView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private void a() {
        LayoutInflater.from(getContext()).inflate((int) R.layout.smartcard_collection_item_withbtn_layout, this);
        this.f2323a = (TXImageView) findViewById(R.id.img);
        this.b = (TextView) findViewById(R.id.title);
        this.c = (TextView) findViewById(R.id.desc);
        this.d = (TXImageView) findViewById(R.id.icon);
        this.e = (ListItemInfoView) findViewById(R.id.app_info);
        this.f = (TextView) findViewById(R.id.app_name);
        this.g = (DownloadButton) findViewById(R.id.app_btn);
        this.h = (Button) findViewById(R.id.action_btn);
        this.i = findViewById(R.id.cut_line);
        setBackgroundResource(R.drawable.smartcard_vertical_item_selector);
    }

    public void a(STInfoV2 sTInfoV2, ContentAggregationComplexItem contentAggregationComplexItem, SimpleAppModel simpleAppModel, boolean z) {
        this.f2323a.updateImageView(contentAggregationComplexItem.f1209a, -1, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
        this.b.setText(contentAggregationComplexItem.b);
        this.d.updateImageView(contentAggregationComplexItem.c, -1, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
        this.c.setText(contentAggregationComplexItem.g);
        if (z) {
            this.i.setVisibility(0);
        } else {
            this.i.setVisibility(4);
        }
        setOnClickListener(new a(this, contentAggregationComplexItem, sTInfoV2));
        if (simpleAppModel != null) {
            this.e.a(simpleAppModel);
            this.e.setVisibility(0);
            this.g.a(simpleAppModel);
            this.g.setVisibility(0);
            this.g.a(sTInfoV2);
            this.f.setVisibility(8);
            this.h.setVisibility(8);
            return;
        }
        this.e.setVisibility(8);
        this.g.setVisibility(8);
        this.f.setVisibility(0);
        this.h.setVisibility(0);
        this.h.setOnClickListener(new b(this, contentAggregationComplexItem, sTInfoV2));
        this.h.setText(contentAggregationComplexItem.h);
        this.f.setText(contentAggregationComplexItem.d);
    }
}
