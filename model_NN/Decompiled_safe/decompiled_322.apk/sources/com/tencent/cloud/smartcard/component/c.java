package com.tencent.cloud.smartcard.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.protocol.jce.ContentAggregationSimpleItem;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class c extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContentAggregationSimpleItem f2327a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ SimpleContentItemView c;

    c(SimpleContentItemView simpleContentItemView, ContentAggregationSimpleItem contentAggregationSimpleItem, STInfoV2 sTInfoV2) {
        this.c = simpleContentItemView;
        this.f2327a = contentAggregationSimpleItem;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        b.a(this.c.getContext(), this.f2327a.f);
    }

    public STInfoV2 getStInfo() {
        if (this.b == null) {
            return null;
        }
        this.b.status = "01";
        this.b.actionId = 200;
        return this.b;
    }
}
