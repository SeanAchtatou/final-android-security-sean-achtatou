package com.tencent.cloud.smartcard.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.protocol.jce.ContentAggregationComplexItem;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class a extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContentAggregationComplexItem f2325a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ ComplexContentItemView c;

    a(ComplexContentItemView complexContentItemView, ContentAggregationComplexItem contentAggregationComplexItem, STInfoV2 sTInfoV2) {
        this.c = complexContentItemView;
        this.f2325a = contentAggregationComplexItem;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        b.a(this.c.getContext(), this.f2325a.f);
    }

    public STInfoV2 getStInfo() {
        if (this.b == null) {
            return null;
        }
        this.b.status = "01";
        this.b.actionId = 200;
        return this.b;
    }
}
