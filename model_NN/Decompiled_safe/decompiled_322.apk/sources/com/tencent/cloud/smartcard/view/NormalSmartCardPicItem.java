package com.tencent.cloud.smartcard.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.protocol.jce.SmartCardPicNode;
import com.tencent.assistant.smartcard.component.NormalSmartCardAppNode;
import com.tencent.assistant.smartcard.component.NormalSmartCardAppTinyNode;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.y;
import com.tencent.assistant.smartcard.f.b;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.b.e;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.cloud.smartcard.b.d;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class NormalSmartCardPicItem extends NormalSmartcardBaseItem {
    private RelativeLayout i;
    private TextView l;
    private TextView m;
    private LinearLayout n;
    private LinearLayout o;
    private NormalSmartCardAppNode p;
    private List<NormalSmartCardAppTinyNode> q;
    private List<NormalSmartCardPicItemPicNode> r;

    public NormalSmartCardPicItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
        setBackgroundResource(R.drawable.common_card_normal);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = inflate(this.f1693a, R.layout.smartcard_pic_frame_layout, this);
        this.l = (TextView) this.c.findViewById(R.id.card_title);
        this.m = (TextView) this.c.findViewById(R.id.more_txt);
        this.i = (RelativeLayout) this.c.findViewById(R.id.card_title_layout);
        this.n = (LinearLayout) this.c.findViewById(R.id.smart_pic_layout);
        this.o = (LinearLayout) this.c.findViewById(R.id.card_app_list_layout);
        f();
    }

    private void f() {
        int i2;
        int i3;
        boolean z;
        d dVar = (d) this.d;
        if (!TextUtils.isEmpty(dVar.l)) {
            this.l.setText(dVar.l);
            int a2 = b.a(dVar.f());
            if (a2 != 0) {
                Drawable drawable = getResources().getDrawable(a2);
                drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                this.l.setCompoundDrawables(drawable, null, null, null);
                this.l.setCompoundDrawablePadding(by.a(this.f1693a, 7.0f));
                this.l.setPadding(0, 0, 0, 0);
            } else {
                this.l.setCompoundDrawables(null, null, null, null);
                this.l.setPadding(by.a(this.f1693a, 7.0f), 0, 0, 0);
            }
            this.i.setVisibility(0);
            if (!TextUtils.isEmpty(dVar.p)) {
                this.m.setText(dVar.p);
                this.m.setOnClickListener(this.k);
                this.m.setVisibility(0);
            } else {
                this.m.setVisibility(8);
            }
        } else {
            this.i.setVisibility(8);
        }
        List<SmartCardPicNode> h = dVar.h();
        if (h == null || h.size() <= 0) {
            this.n.setVisibility(8);
        } else {
            int size = h.size();
            if (size > 3) {
                i3 = 3;
            } else {
                i3 = size;
            }
            if (this.r == null) {
                this.r = new ArrayList(i3);
                a(i3);
            } else if (this.r.size() != i3) {
                this.r.clear();
                this.n.removeAllViews();
                a(i3);
            }
            for (int i4 = 0; i4 < i3; i4++) {
                NormalSmartCardPicItemPicNode normalSmartCardPicItemPicNode = this.r.get(i4);
                SmartCardPicNode smartCardPicNode = h.get(i4);
                STInfoV2 a3 = a(a.a("04", i4), 200);
                int i5 = dVar.i();
                if (i3 == 3) {
                    z = true;
                } else {
                    z = false;
                }
                normalSmartCardPicItemPicNode.a(smartCardPicNode, a3, i5, z, i3 >= 2);
            }
            this.n.setVisibility(0);
        }
        List<y> list = dVar.e;
        if (list == null || list.size() == 0) {
            this.o.setVisibility(8);
            return;
        }
        this.o.setVisibility(0);
        if (list.size() < 3) {
            if (this.p == null) {
                g();
                this.p = new NormalSmartCardAppNode(this.f1693a, (int) R.layout.smartcard_applist_item_for_pic);
                this.p.setMinimumHeight(by.a(getContext(), 67.0f));
                this.o.addView(this.p);
                this.p.a(list.get(0).f1768a, list.get(0).a(), a(list.get(0), 0), c(a(this.f1693a)), false, ListItemInfoView.InfoType.DOWNTIMES_SIZE);
                return;
            }
            this.p.a(list.get(0).f1768a, list.get(0).a(), a(list.get(0), 0), c(a(this.f1693a)), false, ListItemInfoView.InfoType.DOWNTIMES_SIZE);
        } else if (list.size() >= 3) {
            int size2 = list.size();
            if (list.size() > 3) {
                i2 = 3;
            } else {
                i2 = size2;
            }
            if (this.q == null) {
                g();
                this.q = new ArrayList(3);
                for (int i6 = 0; i6 < i2; i6++) {
                    NormalSmartCardAppTinyNode normalSmartCardAppTinyNode = new NormalSmartCardAppTinyNode(this.f1693a);
                    this.q.add(normalSmartCardAppTinyNode);
                    normalSmartCardAppTinyNode.setPadding(0, by.a(getContext(), 12.0f), 0, by.a(getContext(), 2.0f));
                    this.o.addView(normalSmartCardAppTinyNode, new LinearLayout.LayoutParams(0, -2, 1.0f));
                    normalSmartCardAppTinyNode.a(list.get(i6).f1768a, a(list.get(i6), i6), c(a(this.f1693a)));
                }
                return;
            }
            for (int i7 = 0; i7 < i2; i7++) {
                this.q.get(i7).a(list.get(i7).f1768a, a(list.get(i7), i7), c(a(this.f1693a)));
            }
        }
    }

    private void g() {
        this.p = null;
        this.q = null;
        this.o.removeAllViews();
    }

    private void a(int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            NormalSmartCardPicItemPicNode normalSmartCardPicItemPicNode = new NormalSmartCardPicItemPicNode(this.f1693a);
            this.r.add(normalSmartCardPicItemPicNode);
            if (i2 > 1) {
                int a2 = by.a(this.f1693a, 7.5f);
                normalSmartCardPicItemPicNode.setPadding(a2, 0, a2, 0);
            }
            this.n.addView(normalSmartCardPicItemPicNode, new LinearLayout.LayoutParams(-2, -2, 1.0f));
        }
    }

    private STInfoV2 a(y yVar, int i2) {
        STInfoV2 a2 = a(a.a("05", i2), 100);
        if (!(a2 == null || yVar == null)) {
            a2.updateWithSimpleAppModel(yVar.f1768a);
        }
        if (this.h == null) {
            this.h = new e();
        }
        this.h.a(a2);
        return a2;
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    /* access modifiers changed from: protected */
    public String d(int i2) {
        if (this.d instanceof d) {
            return ((d) this.d).e();
        }
        return super.d(i2);
    }
}
