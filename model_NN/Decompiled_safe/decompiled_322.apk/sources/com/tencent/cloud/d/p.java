package com.tencent.cloud.d;

import com.tencent.assistant.protocol.jce.CftGetNavigationResponse;
import com.tencent.assistant.protocol.jce.SubNavigationNode;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class p {

    /* renamed from: a  reason: collision with root package name */
    public long f2314a;
    public List<o> b;
    public int c;

    private p(int i) {
        this.c = i;
        switch (i) {
            case 16:
                this.b = new ArrayList();
                this.b.add(new o("下载榜", 0, 0, null, 0, 0, 30, (byte) 0));
                this.b.add(new o("男生榜", 0, 0, null, 0, 3, 30, (byte) 0));
                this.b.add(new o("女生榜", 0, 0, null, 0, 4, 30, (byte) 0));
                return;
            default:
                return;
        }
    }

    public static p a(int i, CftGetNavigationResponse cftGetNavigationResponse) {
        p pVar = null;
        try {
            pVar = b(i, cftGetNavigationResponse);
        } catch (Exception e) {
            XLog.e("CftGetNavigationEngine", "parse tab container fail,type:.ex:" + e);
            e.printStackTrace();
        }
        if (pVar == null) {
            return new p(i);
        }
        return pVar;
    }

    public static p b(int i, CftGetNavigationResponse cftGetNavigationResponse) {
        if (cftGetNavigationResponse == null || cftGetNavigationResponse.b() == null || cftGetNavigationResponse.b().size() <= 0) {
            return null;
        }
        p pVar = new p(i);
        pVar.c = i;
        pVar.f2314a = cftGetNavigationResponse.a();
        ArrayList<SubNavigationNode> b2 = cftGetNavigationResponse.b();
        ArrayList arrayList = new ArrayList(cftGetNavigationResponse.b().size());
        Iterator<SubNavigationNode> it = b2.iterator();
        while (it.hasNext()) {
            SubNavigationNode next = it.next();
            arrayList.add(new o(next.f1571a, next.b, next.c, next.d, next.e, next.f, next.g, next.h));
        }
        pVar.b = arrayList;
        return pVar;
    }
}
