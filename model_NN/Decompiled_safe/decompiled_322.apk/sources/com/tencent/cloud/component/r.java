package com.tencent.cloud.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.f;

/* compiled from: ProGuard */
class r implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommonCategoryView f2290a;

    r(CommonCategoryView commonCategoryView) {
        this.f2290a = commonCategoryView;
    }

    public void onClick(View view) {
        int i;
        f fVar = (f) view.getTag(R.id.category_data);
        try {
            i = ((Integer) view.getTag(R.id.category_pos)).intValue();
        } catch (Exception e) {
            e.printStackTrace();
            i = 0;
        }
        this.f2290a.a(fVar, i, 200);
        if (this.f2290a.b != null) {
            this.f2290a.b.onClick(view);
        }
    }
}
