package com.tencent.cloud.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.cloud.b.g;
import com.tencent.pangu.adapter.RankNormalListAdapter;

/* compiled from: ProGuard */
public class CftAppRankListPage extends RelativeLayout implements q {

    /* renamed from: a  reason: collision with root package name */
    protected Context f2265a = null;
    protected LayoutInflater b = null;
    protected LoadingView c;
    protected NormalErrorRecommendPage d;
    protected CftAppRankListView e;
    protected View.OnClickListener f = new k(this);

    public CftAppRankListPage(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
    }

    public CftAppRankListPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public void a(g gVar) {
        this.e.a(gVar);
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        this.f2265a = context;
        this.b = LayoutInflater.from(context);
        View inflate = this.b.inflate((int) R.layout.cft_apprank_component_view, this);
        this.e = (CftAppRankListView) inflate.findViewById(R.id.applist);
        this.e.setVisibility(8);
        this.e.a(this);
        this.e.setDivider(null);
        this.e.setTopPaddingSize(8);
        this.e.setShowHeaderFilterInfo(false);
        this.c = (LoadingView) inflate.findViewById(R.id.loading_view);
        this.c.setVisibility(0);
        this.d = (NormalErrorRecommendPage) inflate.findViewById(R.id.error_page);
        this.d.setButtonClickListener(this.f);
    }

    public void a(int i) {
        this.c.setVisibility(8);
        this.e.setVisibility(8);
        this.d.setVisibility(0);
        this.d.setErrorType(i);
    }

    public void a() {
        this.c.setVisibility(0);
        this.e.setVisibility(8);
        this.d.setVisibility(8);
    }

    public void a(RankNormalListAdapter rankNormalListAdapter) {
        this.e.s();
        this.e.setAdapter(rankNormalListAdapter);
        l lVar = new l(this);
        this.e.a(lVar);
        rankNormalListAdapter.a(lVar);
    }

    public void b() {
        this.e.r();
    }

    public void c() {
        this.e.recycleData();
    }

    public void d() {
        this.e.setVisibility(0);
        this.d.setVisibility(8);
        this.c.setVisibility(8);
    }

    public void e() {
    }
}
