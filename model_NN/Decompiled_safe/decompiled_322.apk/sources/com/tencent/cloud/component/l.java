package com.tencent.cloud.component;

import android.widget.AbsListView;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;

/* compiled from: ProGuard */
class l extends ListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    boolean f2285a = false;
    final /* synthetic */ CftAppRankListPage b;

    l(CftAppRankListPage cftAppRankListPage) {
        this.b = cftAppRankListPage;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.cloud.component.CftAppRankListView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView.a(com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView, boolean):boolean
      com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(int, int):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(android.content.Context, android.view.View):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(int, int):void
      com.tencent.cloud.component.CftAppRankListView.a(boolean, boolean):void */
    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
        if (i == 0 && this.f2285a) {
            this.b.e.a(false, false);
        }
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        super.onScroll(absListView, i, i2, i3);
        if (i == 2) {
            this.f2285a = true;
        } else if (i == 1) {
            this.f2285a = false;
        }
    }
}
