package com.tencent.cloud.component;

import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.cloud.component.UpdateListView;
import com.tencent.pangu.manager.bj;

/* compiled from: ProGuard */
class y extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bj f2297a;
    final /* synthetic */ UpdateListView.UpdateAllType b;
    final /* synthetic */ x c;

    y(x xVar, bj bjVar, UpdateListView.UpdateAllType updateAllType) {
        this.c = xVar;
        this.f2297a = bjVar;
        this.b = updateAllType;
    }

    public void onRightBtnClick() {
        TemporaryThreadManager.get().start(new z(this));
        if (this.b == UpdateListView.UpdateAllType.NEEDSTARTDOWNLOAD) {
            Toast.makeText(this.c.f2296a.b, this.c.f2296a.b.getResources().getString(R.string.down_add_tips), 0).show();
        }
    }

    public void onLeftBtnClick() {
        String str;
        if (this.extraMsgView == null || this.c.f2296a.p == null) {
            str = null;
        } else {
            str = this.c.f2296a.p.isChecked() ? "1" : "0";
        }
        STInfoV2 sTInfoV2 = new STInfoV2(STConst.ST_PAGE_UPDATE_UPDATEALL, "03_002", STConst.ST_PAGE_UPDATE, STConst.ST_DEFAULT_SLOT, 200);
        sTInfoV2.status = str;
        sTInfoV2.pushInfo = this.c.f2296a.C;
        l.a(sTInfoV2);
    }

    public void onCancell() {
    }
}
