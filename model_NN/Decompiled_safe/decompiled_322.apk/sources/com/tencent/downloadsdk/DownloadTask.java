package com.tencent.downloadsdk;

import android.os.Handler;
import android.text.TextUtils;
import com.tencent.downloadsdk.a.c;
import com.tencent.downloadsdk.b.f;
import com.tencent.downloadsdk.storage.a.b;
import com.tencent.downloadsdk.storage.a.e;
import com.tencent.downloadsdk.utils.i;
import com.tencent.downloadsdk.utils.j;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

public class DownloadTask implements Runnable {
    private Handler A;
    /* access modifiers changed from: private */
    public DownloadSettingInfo B;
    private f C;
    private String D;
    private HashMap<String, String> E;
    /* access modifiers changed from: private */
    public String F;
    /* access modifiers changed from: private */
    public String G;
    private g H;

    /* renamed from: a  reason: collision with root package name */
    public volatile STATUS f2447a;
    public PRIORITY b;
    public int c;
    public String d;
    public long e;
    public long f;
    public List<String> g;
    public long h;
    public long i;
    public long j;
    public long k;
    public String l;
    public String m;
    public long n;
    public boolean o;
    public List<ao> p;
    protected volatile ae q;
    protected volatile ae r;
    protected a s;
    public c t;
    protected volatile Future<?> u;
    /* access modifiers changed from: private */
    public long v;
    /* access modifiers changed from: private */
    public long w;
    /* access modifiers changed from: private */
    public b x;
    /* access modifiers changed from: private */
    public ArrayList<ae> y;
    private c z;

    public enum PRIORITY {
        LOW,
        NORMAL,
        HIGH,
        URGENT
    }

    public enum STATUS {
        PENDING,
        INITIALIZE,
        RUNNING,
        COMPLETED,
        FAILED,
        PAUSED,
        PAUSING,
        CANCELED
    }

    public DownloadTask(int i2, String str, long j2, long j3, String str2, String str3, List<String> list, Map<String, String> map) {
        this(i2, str, j2, j3, str2, str3, list, map, null);
    }

    public DownloadTask(int i2, String str, long j2, long j3, String str2, String str3, List<String> list, Map<String, String> map, DownloadSettingInfo downloadSettingInfo) {
        this.f2447a = STATUS.PENDING;
        this.b = PRIORITY.NORMAL;
        this.h = -1;
        this.i = 0;
        this.v = 0;
        this.w = 0;
        this.j = -1;
        this.k = 0;
        this.l = null;
        this.m = null;
        this.n = -1;
        this.o = true;
        this.p = null;
        this.x = new b();
        this.y = new ArrayList<>();
        this.E = new HashMap<>();
        this.H = new t(this);
        this.c = i2;
        this.d = str;
        this.e = j2;
        this.f = j3;
        this.g = list;
        this.t = new c(h());
        if (downloadSettingInfo == null) {
            this.B = o.a().b();
        } else {
            this.B = downloadSettingInfo;
        }
        if (this.A == null) {
            this.A = j.b();
        }
        ad adVar = new ad(this, null);
        this.q = adVar;
        this.r = adVar;
        if (!TextUtils.isEmpty(str2)) {
            this.l = str2;
        }
        if (!TextUtils.isEmpty(str3)) {
            this.m = str3;
        }
        boolean z2 = false;
        if (this.x.a(this)) {
            if (f()) {
                com.tencent.downloadsdk.utils.f.d("DownloadTask", "isComplete() in new DownloadTask");
                return;
            } else if (!af.a(d(), this.n)) {
                this.k = 0;
                this.i = 0;
                this.x.b(i2, str);
                new e().b(a(i2, str));
                z2 = true;
            }
        }
        this.D = a(this.c, this.d) + "_" + System.currentTimeMillis();
        this.C = new f(h());
        this.C.b = j2;
        this.C.c = j3;
        this.C.i = i2;
        if (map != null) {
            this.C.q.putAll(map);
        }
        this.C.r = z2;
        this.z = new c(this.D, a(this.c, this.d), this.g, this.l, this.m, this.t, this.H, this.B, this.C, this.E, this.c, this.d);
        this.f2447a = STATUS.PENDING;
    }

    public static String a(int i2, String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(i2);
        stringBuffer.append("-");
        stringBuffer.append(str);
        return stringBuffer.toString();
    }

    /* access modifiers changed from: private */
    public void a(double d2) {
        if (this.f2447a == STATUS.RUNNING) {
            this.A.post(new y(this, d2));
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, byte[] bArr) {
        com.tencent.downloadsdk.utils.f.d("DownloadTask", "notifyUITaskFailed:" + this.d + " code:" + i2);
        k();
        this.f2447a = STATUS.FAILED;
        this.A.post(new aa(this, i2, bArr));
        if (this.s != null) {
            this.s.a(this, this.c, this.d);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, long j2, boolean z2) {
        this.A.post(new w(this, j2));
    }

    /* access modifiers changed from: private */
    public void i() {
        this.f2447a = STATUS.RUNNING;
        this.A.post(new v(this));
    }

    /* access modifiers changed from: private */
    public void j() {
        this.A.post(new x(this));
    }

    /* access modifiers changed from: private */
    public void k() {
        File file = new File(d() + ".yyb");
        if (file.exists()) {
            long lastModified = file.lastModified();
            this.x.a(this.c, this.d, this.i, lastModified);
            this.n = lastModified;
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        com.tencent.downloadsdk.utils.f.b("DownloadTask", "notifyUITaskCompleted:" + this.d);
        k();
        this.f2447a = STATUS.COMPLETED;
        this.A.post(new z(this));
    }

    private void m() {
        com.tencent.downloadsdk.utils.f.b("DownloadTask", "notifyUITaskPaused:" + this.d);
        this.f2447a = STATUS.PAUSED;
        this.A.post(new ab(this));
        if (this.s != null) {
            this.s.a(this, this.c, this.d);
        }
        this.s = null;
    }

    private void n() {
        this.f2447a = STATUS.CANCELED;
        this.A.post(new s(this));
        if (this.s != null) {
            this.s.a(this, this.c, this.d);
        }
        this.s = null;
    }

    public String a() {
        return this.F;
    }

    /* access modifiers changed from: protected */
    public void a(a aVar) {
        this.s = aVar;
    }

    public void a(ae aeVar) {
        this.A.post(new r(this, aeVar));
    }

    /* access modifiers changed from: protected */
    public void b() {
        com.tencent.downloadsdk.utils.f.a("DownloadTask", "doPause:" + this.d);
        Future<?> future = this.u;
        this.u = null;
        if (future != null) {
            future.cancel(true);
        }
        this.f2447a = STATUS.PAUSING;
        if (this.z != null) {
            this.z.c();
        }
        m();
    }

    /* access modifiers changed from: protected */
    public void c() {
        Future<?> future = this.u;
        this.u = null;
        if (future != null) {
            future.cancel(true);
        }
        this.f2447a = STATUS.PAUSING;
        if (this.z != null) {
            this.z.d();
        }
        n();
    }

    public String d() {
        if (TextUtils.isEmpty(this.m)) {
            return null;
        }
        return this.l + "/" + this.m;
    }

    public void e() {
        this.f2447a = STATUS.COMPLETED;
        this.A.post(new u(this));
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof DownloadTask)) {
            return false;
        }
        DownloadTask downloadTask = (DownloadTask) obj;
        return downloadTask.c == this.c && downloadTask.d == this.d;
    }

    public boolean f() {
        if (!TextUtils.isEmpty(d()) && new File(d()).exists()) {
            return this.f2447a == STATUS.COMPLETED || (this.k > 0 && this.k == this.i);
        }
        return false;
    }

    public String g() {
        return this.D;
    }

    public String h() {
        return a(this.c, this.d);
    }

    public int hashCode() {
        return (this.d == null ? 0 : this.d.hashCode()) + ((Integer.valueOf(this.c).hashCode() + 37) * 37);
    }

    public void run() {
        if (this.f2447a != STATUS.PAUSING && this.f2447a != STATUS.CANCELED && this.f2447a != STATUS.PAUSED) {
            boolean a2 = this.x.a(this);
            if (f()) {
                com.tencent.downloadsdk.utils.f.d("DownloadTask", "isComplete() run");
                e();
                return;
            }
            if (a2 && !af.a(d(), this.n)) {
                this.k = 0;
                this.i = 0;
                this.x.b(this.c, this.d);
                if (this.z != null) {
                    this.z.a();
                } else {
                    new e().b(a(this.c, this.d));
                }
                this.C.r = true;
                com.tencent.downloadsdk.utils.f.a("DownloadTask", "File is not Valid ");
            }
            this.f2447a = STATUS.INITIALIZE;
            this.x.a(this.c, this.d, this.g, this.f2447a.ordinal(), this.b.ordinal(), this.l, this.m);
            if (this.i <= 0) {
                i.a(d());
            }
            if (this.f2447a != STATUS.PAUSING) {
                this.z.b = this.j;
                i b2 = this.z.b();
                this.u = null;
                if (b2.f2475a == 0) {
                    this.f2447a = STATUS.RUNNING;
                } else if (b2.f2475a == 2) {
                    this.f2447a = STATUS.PAUSED;
                } else if (b2.f2475a == 1) {
                    this.f2447a = STATUS.CANCELED;
                } else if (b2.f2475a == -33) {
                    e();
                } else {
                    if (this.z != null) {
                        this.r.b(this.c, this.d, ("1,reportKey=" + this.z.h + ",detect=" + this.z.c + "+" + this.z.d + "+" + this.z.g + "+" + this.z.e + "+" + this.z.f) + this.z.i);
                    }
                    com.tencent.downloadsdk.b.b.a(this.D, this.C, DownloadSettingInfo.a(), d(), this.j, 0, this.t.d, this.t.b, this.t.c, b2.b, b2.f2475a, 1);
                    this.H.a(b2.f2475a, b2.b);
                }
            }
        }
    }
}
