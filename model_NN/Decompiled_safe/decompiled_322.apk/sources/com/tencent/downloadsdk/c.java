package com.tencent.downloadsdk;

import android.net.Uri;
import android.os.SystemClock;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.b.f;
import com.tencent.downloadsdk.network.HttpUtils;
import com.tencent.downloadsdk.network.a;
import com.tencent.downloadsdk.storage.a.e;
import com.tencent.downloadsdk.utils.NetInfo;
import com.tencent.downloadsdk.utils.i;
import com.tencent.downloadsdk.utils.k;
import com.tencent.downloadsdk.utils.p;
import com.tencent.securemodule.impl.ErrorCode;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class c implements m {
    private LinkedBlockingQueue<Runnable> A = new LinkedBlockingQueue<>();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<Long, k> B = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public j C;
    private byte[] D;
    private volatile a E;
    /* access modifiers changed from: private */
    public volatile boolean F = false;
    /* access modifiers changed from: private */
    public boolean G = false;
    private boolean H = false;
    /* access modifiers changed from: private */
    public f I;
    /* access modifiers changed from: private */
    public HashMap<String, String> J;
    private String K;
    private String L;
    private long M;
    private final int N = 1;
    private final int O = 2;
    private final int P = 3;
    private final int Q = 4;
    private ah R = new f(this);

    /* renamed from: a  reason: collision with root package name */
    public long f2470a = -1;
    public long b = -1;
    protected long c;
    protected long d;
    protected String e = Constants.STR_EMPTY;
    protected String f = Constants.STR_EMPTY;
    protected int g;
    protected String h;
    protected String i = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public AtomicLong j = new AtomicLong();
    /* access modifiers changed from: private */
    public ArrayList<String> k = new ArrayList<>();
    private String l;
    private String m;
    /* access modifiers changed from: private */
    public String n = null;
    /* access modifiers changed from: private */
    public g o;
    /* access modifiers changed from: private */
    public DownloadSettingInfo p;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public int r;
    /* access modifiers changed from: private */
    public String s;
    /* access modifiers changed from: private */
    public ak t;
    /* access modifiers changed from: private */
    public ThreadPoolExecutor u;
    /* access modifiers changed from: private */
    public volatile int v;
    /* access modifiers changed from: private */
    public byte[] w;
    /* access modifiers changed from: private */
    public volatile boolean x;
    /* access modifiers changed from: private */
    public volatile af y;
    /* access modifiers changed from: private */
    public com.tencent.downloadsdk.a.c z;

    public c(String str, String str2, List<String> list, String str3, String str4, com.tencent.downloadsdk.a.c cVar, g gVar, DownloadSettingInfo downloadSettingInfo, f fVar, HashMap<String, String> hashMap, int i2, String str5) {
        this.q = str2;
        this.r = i2;
        this.s = str5;
        this.h = str;
        if (list != null) {
            this.k.clear();
            for (String next : list) {
                if (!TextUtils.isEmpty(next)) {
                    this.k.add(next);
                }
            }
        }
        this.C = new j(this);
        this.C.f2476a = this.k;
        this.l = str3;
        this.m = str4;
        if (!TextUtils.isEmpty(str4)) {
            this.n = this.l + "/" + this.m;
        }
        this.o = gVar;
        this.z = cVar;
        this.p = downloadSettingInfo;
        this.I = fVar;
        this.J = hashMap;
    }

    private h a(a aVar, String str) {
        boolean z2;
        int i2;
        h hVar = new h(this, null);
        if (TextUtils.isEmpty(str)) {
            hVar.f2474a = -16;
            return hVar;
        }
        for (int i3 = 0; i3 < this.p.d && !h(); i3++) {
            HashMap hashMap = new HashMap();
            hashMap.put("Range", "bytes=0-1");
            String c2 = HttpUtils.c(str);
            if (!TextUtils.isEmpty(c2)) {
                hashMap.put("Host", c2);
            }
            int i4 = 0;
            boolean z3 = false;
            boolean z4 = true;
            while (z4 && i4 < this.p.l) {
                this.I.a("B104", str, true);
                aVar.a(true);
                com.tencent.downloadsdk.network.c a2 = aVar.a(str, hashMap, false, null);
                com.tencent.downloadsdk.utils.f.d("DownloadScheduler", "responseHeader resultcode:" + a2.j);
                String a3 = a2.a();
                if (!TextUtils.isEmpty(a3)) {
                    a3 = URLEncoder.encode(a3);
                }
                this.I.a("B101", a3);
                this.I.a("B101", a2.c + Constants.STR_EMPTY, true);
                if (a2.k != null) {
                    this.I.a("B101", a2.k.getLocalizedMessage(), true);
                }
                if (this.D == null && a2.l != null) {
                    this.D = a2.l;
                }
                if (a2.j == 1) {
                    hVar.f2474a = 1;
                    return hVar;
                }
                if (a2.j != 0) {
                    hVar.f2474a = a2.j;
                    boolean z5 = (a2.j == -16 || a2.j == -21) ? true : z3;
                    if (a2.j == -83 || a2.j == -85 || a2.j == -87 || a2.j == -89) {
                        z5 = true;
                    }
                    z3 = z5;
                    z2 = false;
                    i2 = i4;
                } else {
                    int i5 = a2.c;
                    if (i5 == 200 || i5 == 206 || i5 == 413 || i5 == 416) {
                        long j2 = 0;
                        if (!TextUtils.isEmpty(a2.f)) {
                            j2 = HttpUtils.b(a2.f);
                        } else if (!hashMap.containsKey("Range")) {
                            j2 = a2.e;
                        }
                        if (this.r < 100) {
                            if (HttpUtils.d(a2.g)) {
                                hVar.f2474a = -11;
                                z3 = true;
                                z2 = false;
                            } else if (j2 > 0) {
                                this.C.a(str);
                                this.I.a("B99", a2.e + "," + a2.f);
                                hVar.f2474a = 0;
                                hVar.b = j2;
                                hVar.c = a2.g;
                                hVar.d = a2.a("Content-Disposition");
                                return hVar;
                            } else {
                                z3 = true;
                                z2 = z4;
                            }
                        } else if (j2 > 0) {
                            this.C.a(str);
                            this.I.a("B99", a2.e + "," + a2.f);
                            hVar.f2474a = 0;
                            hVar.b = j2;
                            hVar.c = a2.g;
                            hVar.d = a2.a("Content-Disposition");
                            return hVar;
                        } else if (HttpUtils.d(a2.g)) {
                            hVar.f2474a = -11;
                            z3 = true;
                            z2 = false;
                        } else {
                            z3 = true;
                            z2 = z4;
                        }
                        i2 = i4;
                    } else if (i5 == 301 || i5 == 302 || i5 == 303 || i5 == 307) {
                        int i6 = i4 + 1;
                        String a4 = a2.a("Location");
                        if (TextUtils.isEmpty(a4) || a4.trim().length() == 0) {
                            z3 = true;
                            this.I.a("B102", str, true);
                            hVar.f2474a = -31;
                        } else {
                            try {
                                if (a4.startsWith("/")) {
                                    Uri parse = Uri.parse(str);
                                    String host = parse.getHost();
                                    String scheme = parse.getScheme();
                                    if (!TextUtils.isEmpty(host) && !TextUtils.isEmpty(scheme)) {
                                        a4 = scheme + "://" + host + a4;
                                    }
                                }
                            } catch (Exception e2) {
                            }
                        }
                        str = a4;
                        z2 = z4;
                        i2 = i6;
                    } else if (i5 < 0) {
                        z3 = true;
                        hVar.f2474a = -24;
                        z2 = z4;
                        i2 = i4;
                    } else {
                        z3 = true;
                        hVar.f2474a = -i5;
                        z2 = z4;
                        i2 = i4;
                    }
                }
                if (z3) {
                    z2 = false;
                }
                i4 = i2;
                z4 = z2;
            }
            if (i4 == this.p.l) {
                z3 = true;
                hVar.f2474a = -1;
            }
            this.I.a(hVar.f2474a);
            if (z3) {
                return hVar;
            }
            if (i3 < this.p.d - 1) {
                SystemClock.sleep(2000);
            }
        }
        return hVar;
    }

    private static String a(String str) {
        return !TextUtils.isEmpty(str) ? str.replace("?", Constants.STR_EMPTY).replace("*", Constants.STR_EMPTY).replace(":", Constants.STR_EMPTY).replace("\\", Constants.STR_EMPTY).replace("/", Constants.STR_EMPTY).replace("\"", Constants.STR_EMPTY).replace("<", Constants.STR_EMPTY).replace(">", Constants.STR_EMPTY).replace("|", Constants.STR_EMPTY) : str;
    }

    public static String a(String str, String str2) {
        int indexOf;
        String str3;
        try {
            if (!TextUtils.isEmpty(str) && -1 != (indexOf = str.indexOf("filename="))) {
                int length = "filename=".length() + indexOf;
                int indexOf2 = str.indexOf(";", length);
                if (indexOf2 == -1) {
                    indexOf2 = str.length();
                }
                try {
                    str3 = URLDecoder.decode(str.substring(length, indexOf2), "utf-8");
                } catch (UnsupportedEncodingException e2) {
                    try {
                        str3 = URLDecoder.decode(str3, "gbk");
                    } catch (UnsupportedEncodingException e3) {
                    }
                }
                if (!TextUtils.isEmpty(str3)) {
                    int lastIndexOf = str3.lastIndexOf("/") + 1;
                    String substring = lastIndexOf > 0 ? str3.substring(lastIndexOf) : str3;
                    return !TextUtils.isEmpty(substring) ? c(str3, str2) : substring;
                }
            }
            return null;
        } catch (Exception e4) {
            e4.printStackTrace();
            return null;
        }
    }

    public static String a(String str, String str2, String str3) {
        String str4 = "downloadfile_" + str3;
        return (TextUtils.isEmpty(str2) || !str2.equalsIgnoreCase("application/vnd.android.package-archive")) ? str4 : str4 + ".apk";
    }

    public static String a(String str, String str2, String str3, String str4) {
        String a2 = a(str3, str4);
        if (a2 == null && (a2 = b(str, str4)) == null) {
            a2 = a(str, str2, str4);
        }
        return a(a2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0030 A[Catch:{ Exception -> 0x0035 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String b(java.lang.String r4, java.lang.String r5) {
        /*
            r0 = 0
            java.lang.String r1 = android.net.Uri.decode(r4)     // Catch:{ Exception -> 0x0035 }
            if (r1 == 0) goto L_0x003a
            r2 = 63
            int r2 = r1.indexOf(r2)     // Catch:{ Exception -> 0x0035 }
            if (r2 <= 0) goto L_0x0014
            r3 = 0
            java.lang.String r1 = r1.substring(r3, r2)     // Catch:{ Exception -> 0x0035 }
        L_0x0014:
            java.lang.String r2 = "/"
            boolean r2 = r1.endsWith(r2)     // Catch:{ Exception -> 0x0035 }
            if (r2 != 0) goto L_0x003a
            r2 = 47
            int r2 = r1.lastIndexOf(r2)     // Catch:{ Exception -> 0x0035 }
            int r2 = r2 + 1
            if (r2 <= 0) goto L_0x003a
            java.lang.String r1 = r1.substring(r2)     // Catch:{ Exception -> 0x0035 }
        L_0x002a:
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0035 }
            if (r2 != 0) goto L_0x0034
            java.lang.String r0 = c(r1, r5)     // Catch:{ Exception -> 0x0035 }
        L_0x0034:
            return r0
        L_0x0035:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0034
        L_0x003a:
            r1 = r0
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.c.b(java.lang.String, java.lang.String):java.lang.String");
    }

    private static String c(String str, String str2) {
        int lastIndexOf = str.lastIndexOf(".");
        String str3 = Constants.STR_EMPTY;
        if (lastIndexOf > 0 && str.length() > lastIndexOf + 1) {
            String substring = str.substring(0, lastIndexOf);
            str3 = str.substring(lastIndexOf);
            str = substring;
        }
        return str + "_" + str2 + str3;
    }

    /* access modifiers changed from: private */
    public void e() {
        this.H = true;
        if (this.y != null) {
            this.y.a();
        }
        for (k next : this.B.values()) {
            this.i += ",seg" + next.f2477a.c + "=" + next.e + "+" + next.f + "+" + System.currentTimeMillis() + "+" + next.b.toString();
            next.a();
            this.u.remove(next);
        }
        if (this.u != null) {
            this.u.shutdown();
        }
        if (this.o != null) {
            this.o.a(("4,reportKey=" + this.h + ",detect=" + this.c + "+" + System.currentTimeMillis() + "+" + this.v + "+" + this.e + "+" + this.f) + "," + this.i);
        }
        HashMap hashMap = new HashMap();
        hashMap.put("B110", this.h);
        hashMap.put("B123", "task is internalPause");
        com.tencent.beacon.event.a.a("TMDownloadSDK", true, 0, 0, hashMap, true);
    }

    private h f() {
        h hVar = new h(this, null);
        int b2 = this.C.b();
        if (b2 == 0) {
            hVar.f2474a = -16;
        } else {
            this.E = new a(this.p.g * 1000, this.p.f * 1000);
            this.E.a(this.J);
            int i2 = 0;
            this.M = SystemClock.elapsedRealtime();
            while (true) {
                if (!this.G && !this.F && hVar.b <= 0 && b2 > 0 && !h()) {
                    String b3 = this.C.b(i2 % b2);
                    if (p.a(b3)) {
                        try {
                            b3 = p.a(b3, b.b(), this.q, this.q);
                        } catch (UnsupportedEncodingException e2) {
                        }
                    }
                    if (this.E != null) {
                        hVar = a(this.E, b3);
                        com.tencent.downloadsdk.utils.f.d("DownloadScheduler", "doGetContentLength resultcode:" + hVar.f2474a);
                    }
                    if (hVar.f2474a < 0 || hVar.f2474a == 1 || hVar.f2474a == 2) {
                        this.e += "+" + hVar.f2474a;
                    }
                    if (this.v == 2 || this.v == 1) {
                        hVar.f2474a = this.v;
                    } else if (hVar.f2474a != 0 || hVar.b <= 0) {
                        if (i2 >= b2 && hVar.f2474a == -11) {
                            break;
                        }
                        i2++;
                    } else {
                        this.I.a("B100", Constants.STR_EMPTY + hVar.b);
                        this.I.a("B106", Constants.STR_EMPTY + this.b);
                        if (this.b != -1 && this.b != hVar.b) {
                            hVar.f2474a = -10;
                        }
                    }
                }
            }
            if (hVar.f2474a == -38) {
                if (this.H) {
                    hVar.f2474a = -338;
                }
                if (this.G) {
                    hVar.f2474a = -238;
                }
            }
            if (this.G && hVar.f2474a == -39) {
                if (this.H) {
                    hVar.f2474a = -339;
                }
                if (this.G) {
                    hVar.f2474a = -239;
                }
            }
            if (i2 >= b2) {
                hVar.f2474a += ErrorCode.ERR_POST;
            }
            com.tencent.downloadsdk.utils.f.c("DownloadScheduler", "getFileLength finish, mResultCode:" + hVar.f2474a + ",urlCount:" + b2 + ",retryIndex:" + i2 + ",retryCount:" + this.p.d);
        }
        return hVar;
    }

    /* access modifiers changed from: private */
    public void g() {
        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "checkIfDownloadComplete start mIsDownloadFinished: " + this.x + "id:" + this.q);
        DownloadManager.a().c().post(new d(this));
    }

    private boolean h() {
        long elapsedRealtime = SystemClock.elapsedRealtime() - this.M;
        long j2 = 60000;
        if (k.d()) {
            j2 = 80000;
        } else if (k.c()) {
            j2 = 100000;
        }
        if (elapsedRealtime <= j2) {
            return false;
        }
        com.tencent.downloadsdk.utils.f.c("DownloadScheduler", "detect retry cost time:" + elapsedRealtime + " over max limit:" + j2 + ", stop retry");
        return true;
    }

    public void a() {
        new e().b(this.q);
    }

    public void a(an anVar) {
        if (anVar != null) {
            com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "onSegStarted tid: " + Thread.currentThread().getId() + " segid: " + anVar.c + " mStartPos: " + anVar.f + " mSaveLength: " + anVar.g + " mSegTotalLength: " + anVar.e);
        }
    }

    public void a(an anVar, int i2, byte[] bArr, Throwable th) {
        this.v = i2;
        this.w = bArr;
    }

    public void a(an anVar, long j2, String str) {
        if (j2 != this.f2470a) {
            this.I.a("B103", this.f2470a + "," + j2 + "," + str);
            this.v = -19;
            e();
        }
    }

    public void a(an anVar, com.tencent.downloadsdk.b.a aVar, String str) {
        if (anVar != null) {
            com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "seg.mReadTime: " + anVar.m + " id:" + this.q);
            com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "seg.mReadCount: " + anVar.l + " id:" + this.q);
            com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "onTerminated id:" + this.q);
        }
        if (this.y != null) {
            this.y.a(anVar);
        }
        this.B.remove(Long.valueOf(anVar.c));
        if (!(!this.B.isEmpty() || this.v == 0 || this.y == null)) {
            this.y.a();
        }
        this.I.o.put(Long.valueOf(anVar.c), aVar);
        this.i += ",seg" + anVar.c + "=" + str;
        g();
    }

    public void a(an anVar, String str, String str2) {
        if (this.o != null) {
            this.o.a(str, str2);
        }
    }

    public void a(an anVar, byte[] bArr, int i2) {
        if (this.j.addAndGet((long) i2) > this.f2470a) {
            this.v = -32;
            e();
        }
        if (this.F || this.y == null) {
            for (k a2 : this.B.values()) {
                a2.a();
            }
            if (this.y != null) {
                this.y.a();
                return;
            }
            return;
        }
        this.y.a(anVar, bArr, i2);
    }

    public i b() {
        an c2;
        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "startDownload():" + Thread.currentThread().getName() + " " + this.q);
        Thread currentThread = Thread.currentThread();
        if (this.o != null) {
            this.o.a();
        }
        k.g();
        if (k.f() == NetInfo.APN.NO_NETWORK) {
            return i.a(-15);
        }
        this.c = System.currentTimeMillis();
        boolean z2 = false;
        int i2 = 1;
        while (true) {
            int i3 = i2;
            boolean z3 = z2;
            if (!currentThread.isInterrupted()) {
                com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "stage:" + i3);
                switch (i3) {
                    case 1:
                        this.x = false;
                        this.z.a();
                        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "start GetContentLength");
                        z3 = true;
                        h f2 = f();
                        this.d = System.currentTimeMillis();
                        com.tencent.downloadsdk.utils.f.d("DownloadScheduler", "getFileLength result resultCode:" + f2.f2474a + " FileLength:" + f2.b);
                        this.g = f2.f2474a;
                        if (f2.f2474a >= 0) {
                            if (!(this.v == 2 || this.v == 1)) {
                                this.f2470a = f2.b;
                                this.K = f2.c;
                                this.L = f2.d;
                                this.C.c();
                                break;
                            }
                        } else {
                            this.z.b();
                            return i.a(f2.f2474a - 30000, this.D);
                        }
                    case 2:
                        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "start seg");
                        this.t = new ak(this.q, this.p, this.f2470a, this.C);
                        this.j.set(this.t.a());
                        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "task id:" + this.q + " mTotalReceivedLen init:" + this.j);
                        break;
                    case 3:
                        if (this.j.get() != this.f2470a) {
                            if (i.a(this.l, this.f2470a)) {
                                com.tencent.downloadsdk.utils.f.d("DownloadScheduler", "create DownloadWriteFile:" + Thread.currentThread().getName() + " " + this);
                                this.y = new af();
                                if (TextUtils.isEmpty(this.m)) {
                                    this.m = a(this.C.a(0), this.K, this.L, this.r + "_" + this.s);
                                    this.n = this.l + "/" + this.m;
                                }
                                if (!this.y.a(this.n) && this.j.get() > 0) {
                                    this.y.b(this.q);
                                    af.c(this.n);
                                    this.t = new ak(this.q, this.p, this.f2470a, this.C);
                                    this.j.set(this.t.a());
                                }
                                if (this.y.a(this.q, this.n, this.f2470a, this.j.get(), this.R)) {
                                    break;
                                } else {
                                    this.z.b();
                                    return i.a(this.n, this.f2470a) ? i.a(-13) : i.a(-12);
                                }
                            } else {
                                return i.a(-12);
                            }
                        } else {
                            this.z.b();
                            return i.a(-33);
                        }
                    case 4:
                        if (this.o != null && z3) {
                            this.o.a(this.f2470a, this.K, this.L, this.m);
                        }
                        this.B.clear();
                        this.A.clear();
                        this.u = new ThreadPoolExecutor(this.p.i, this.p.i, 0, TimeUnit.MILLISECONDS, this.A);
                        while (!currentThread.isInterrupted() && (c2 = this.t.c()) != null) {
                            c2.d = this.C.a(0);
                            c2.j = this.n;
                            c2.h = true;
                            c2.c();
                            try {
                                k kVar = new k(this.r, this.s, c2, this.C, this.p, this.z, this, this.J);
                                kVar.d = this.u.submit(kVar);
                                this.B.put(Long.valueOf(c2.c), kVar);
                                if (currentThread.isInterrupted()) {
                                    for (k next : this.B.values()) {
                                        next.a();
                                        this.u.remove(next);
                                    }
                                    this.u.shutdown();
                                }
                            } catch (RejectedExecutionException e2) {
                                e2.printStackTrace();
                                return i.a(-34);
                            } catch (NullPointerException e3) {
                                e3.printStackTrace();
                                return i.a(-35);
                            } catch (Exception e4) {
                                e4.printStackTrace();
                                return i.a(-35);
                            }
                        }
                        if (currentThread.isInterrupted()) {
                            com.tencent.downloadsdk.utils.f.b("interrupted", "DownloadScheduler" + this.q + " currentThread:" + currentThread + ".isInterrupted() in do seg");
                        }
                        return i.a(0);
                }
                z2 = z3;
                i2 = i3 + 1;
                com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "stage++:" + i2);
            } else {
                if (currentThread.isInterrupted()) {
                    e();
                    g();
                    this.f = "DownloadScheduler " + this.q + " currentThread:" + currentThread + ".isInterrupted() & stage:" + i3 + " & mErrCode:" + this.v;
                    com.tencent.downloadsdk.utils.f.b("interrupted", "DownloadScheduler" + this.q + " currentThread:" + currentThread + ".isInterrupted() & stage:" + i3 + " & mErrCode:" + this.v);
                }
                return i.a(this.v);
            }
        }
    }

    public void b(an anVar) {
        if (!(anVar == null || this.j == null || this.y == null)) {
            com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "onSegSucceed tid: " + Thread.currentThread().getId() + "taskid:" + anVar.b + " segid: " + anVar.c + " segTotalLen: " + anVar.e + " mSegRecvLen: " + anVar.i + " fileLen: " + this.f2470a + " totalRecvLen: " + this.j.get() + " totalSavedLen: " + this.y.c());
        }
        DownloadManager.a().c().post(new e(this));
    }

    public void c() {
        this.G = true;
        this.v = 2;
        if (this.E != null) {
            this.E.a();
        }
        this.E = null;
        e();
    }

    public void c(an anVar) {
        if (anVar != null) {
            com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "onSegStop tid: " + Thread.currentThread().getId() + " segid:" + anVar.c + " segSavedLen: " + anVar.g + " segRecvLen: " + anVar.i + " segTotalLen: " + anVar.e);
        }
    }

    public void d() {
        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "cancel() begin");
        this.F = true;
        this.v = 1;
        if (this.E != null) {
            this.E.a();
        }
        this.E = null;
        if (this.y != null) {
            this.y.a();
        }
        this.z.b();
        for (k next : this.B.values()) {
            this.i += ",seg" + next.f2477a.c + "=" + next.e + "+" + next.f + "+" + System.currentTimeMillis() + "+" + next.b.toString();
            next.a();
            this.u.remove(next);
        }
        if (this.u != null) {
            this.u.shutdown();
        }
        if (this.o != null) {
            this.o.a(("3,reportKey=" + this.h + ",detect=" + this.c + "+" + System.currentTimeMillis() + "+" + this.v + "+" + this.e + "+" + this.f) + "," + this.i);
        }
        HashMap hashMap = new HashMap();
        hashMap.put("B110", this.h);
        hashMap.put("B123", "task is cancel");
        com.tencent.beacon.event.a.a("TMDownloadSDK", true, 0, 0, hashMap, true);
        com.tencent.downloadsdk.utils.f.b("DownloadScheduler", "cancel() end");
    }
}
