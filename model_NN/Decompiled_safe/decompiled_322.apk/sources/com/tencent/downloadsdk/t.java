package com.tencent.downloadsdk;

import android.os.SystemClock;
import android.text.TextUtils;
import com.tencent.downloadsdk.utils.f;
import com.tencent.downloadsdk.utils.k;
import java.util.ArrayList;

class t implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadTask f2510a;

    t(DownloadTask downloadTask) {
        this.f2510a = downloadTask;
    }

    public void a() {
        this.f2510a.i();
    }

    public void a(int i, byte[] bArr) {
        this.f2510a.a(i, bArr);
    }

    public void a(long j, double d) {
        this.f2510a.i = j;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (j - this.f2510a.v >= this.f2510a.B.q || elapsedRealtime - this.f2510a.w > 50) {
            long unused = this.f2510a.v = j;
            long unused2 = this.f2510a.w = elapsedRealtime;
            this.f2510a.k();
        }
        this.f2510a.a(d);
    }

    public void a(long j, String str, String str2, String str3) {
        this.f2510a.k = j;
        String unused = this.f2510a.F = str;
        String unused2 = this.f2510a.G = str2;
        if (TextUtils.isEmpty(this.f2510a.m)) {
            this.f2510a.m = str3;
            this.f2510a.j();
        }
        this.f2510a.a(this.f2510a.g.get(0), this.f2510a.k, true);
    }

    public void a(String str) {
        if (this.f2510a.q != null) {
            this.f2510a.q.b(this.f2510a.c, this.f2510a.d, str);
        }
    }

    public void a(String str, String str2) {
        if (this.f2510a.p == null) {
            this.f2510a.p = new ArrayList();
        }
        ao aoVar = new ao();
        aoVar.f2464a = k.f().ordinal();
        aoVar.b = str;
        aoVar.c = str2;
        aoVar.d = System.currentTimeMillis();
        if (!this.f2510a.p.contains(aoVar)) {
            this.f2510a.p.add(aoVar);
            this.f2510a.x.a(this.f2510a.c, this.f2510a.d, ao.a(this.f2510a.p));
        }
    }

    public void b() {
        this.f2510a.l();
    }

    public void c() {
    }

    public void d() {
    }

    public void e() {
        f.b("DownloadTask", "onSaveFileTerminated:" + this.f2510a);
        this.f2510a.k();
    }
}
