package com.google.zxing.c;

import com.google.zxing.c.a.c;
import com.google.zxing.common.i;
import com.google.zxing.d;
import com.google.zxing.g;
import com.google.zxing.h;
import com.google.zxing.j;
import java.util.Hashtable;

public final class a implements g {

    /* renamed from: a  reason: collision with root package name */
    private static final j[] f152a = new j[0];
    private final c b = new c();

    public h a(com.google.zxing.c cVar, Hashtable hashtable) {
        com.google.zxing.common.g a2;
        j[] b2;
        if (hashtable == null || !hashtable.containsKey(d.b)) {
            i a3 = new com.google.zxing.c.b.a(cVar).a();
            a2 = this.b.a(a3.a());
            b2 = a3.b();
        } else {
            a2 = this.b.a(com.google.zxing.d.a.a(cVar.c()));
            b2 = f152a;
        }
        return new h(a2.b(), a2.a(), b2, com.google.zxing.a.n);
    }

    public void a() {
    }
}
