package com.google.zxing.d.a;

import com.google.zxing.FormatException;
import com.google.zxing.common.b;

final class a {

    /* renamed from: a  reason: collision with root package name */
    private final b f174a;
    private r b;
    private p c;

    a(b bVar) {
        int c2 = bVar.c();
        if (c2 < 21 || (c2 & 3) != 1) {
            throw FormatException.a();
        }
        this.f174a = bVar;
    }

    private int a(int i, int i2, int i3) {
        return this.f174a.a(i, i2) ? (i3 << 1) | 1 : i3 << 1;
    }

    /* access modifiers changed from: package-private */
    public p a() {
        int i = 0;
        if (this.c != null) {
            return this.c;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < 6; i3++) {
            i2 = a(i3, 8, i2);
        }
        int a2 = a(8, 7, a(8, 8, a(7, 8, i2)));
        for (int i4 = 5; i4 >= 0; i4--) {
            a2 = a(8, i4, a2);
        }
        int c2 = this.f174a.c();
        int i5 = c2 - 8;
        for (int i6 = c2 - 1; i6 >= i5; i6--) {
            i = a(i6, 8, i);
        }
        for (int i7 = c2 - 7; i7 < c2; i7++) {
            i = a(8, i7, i);
        }
        this.c = p.b(a2, i);
        if (this.c != null) {
            return this.c;
        }
        throw FormatException.a();
    }

    /* access modifiers changed from: package-private */
    public r b() {
        int i = 0;
        if (this.b != null) {
            return this.b;
        }
        int c2 = this.f174a.c();
        int i2 = (c2 - 17) >> 2;
        if (i2 <= 6) {
            return r.b(i2);
        }
        int i3 = c2 - 11;
        int i4 = 0;
        for (int i5 = 5; i5 >= 0; i5--) {
            for (int i6 = c2 - 9; i6 >= i3; i6--) {
                i4 = a(i6, i5, i4);
            }
        }
        this.b = r.c(i4);
        if (this.b != null && this.b.d() == c2) {
            return this.b;
        }
        for (int i7 = 5; i7 >= 0; i7--) {
            for (int i8 = c2 - 9; i8 >= i3; i8--) {
                i = a(i7, i8, i);
            }
        }
        this.b = r.c(i);
        if (this.b != null && this.b.d() == c2) {
            return this.b;
        }
        throw FormatException.a();
    }

    /* access modifiers changed from: package-private */
    public byte[] c() {
        p a2 = a();
        r b2 = b();
        c a3 = c.a(a2.b());
        int c2 = this.f174a.c();
        a3.a(this.f174a, c2);
        b e = b2.e();
        byte[] bArr = new byte[b2.c()];
        int i = c2 - 1;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        boolean z = true;
        while (i > 0) {
            if (i == 6) {
                i--;
            }
            for (int i5 = 0; i5 < c2; i5++) {
                int i6 = z ? (c2 - 1) - i5 : i5;
                for (int i7 = 0; i7 < 2; i7++) {
                    if (!e.a(i - i7, i6)) {
                        i2++;
                        i3 <<= 1;
                        if (this.f174a.a(i - i7, i6)) {
                            i3 |= 1;
                        }
                        if (i2 == 8) {
                            bArr[i4] = (byte) i3;
                            i3 = 0;
                            i4++;
                            i2 = 0;
                        }
                    }
                }
            }
            i -= 2;
            z = !z;
        }
        if (i4 == b2.c()) {
            return bArr;
        }
        throw FormatException.a();
    }
}
