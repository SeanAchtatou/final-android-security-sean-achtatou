package com.google.zxing.b.a.a.a;

import com.google.zxing.common.a;

abstract class i extends h {
    i(a aVar) {
        super(aVar);
    }

    /* access modifiers changed from: protected */
    public abstract int a(int i);

    /* access modifiers changed from: protected */
    public abstract void a(StringBuffer stringBuffer, int i);

    /* access modifiers changed from: protected */
    public void b(StringBuffer stringBuffer, int i, int i2) {
        int a2 = this.b.a(i, i2);
        a(stringBuffer, a2);
        int a3 = a(a2);
        int i3 = 100000;
        for (int i4 = 0; i4 < 5; i4++) {
            if (a3 / i3 == 0) {
                stringBuffer.append('0');
            }
            i3 /= 10;
        }
        stringBuffer.append(a3);
    }
}
