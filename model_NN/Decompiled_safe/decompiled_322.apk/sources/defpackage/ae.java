package defpackage;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.tencent.connect.common.Constants;
import com.tencent.securemodule.impl.ErrorCode;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.URL;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.InflaterInputStream;

/* renamed from: ae  reason: default package */
public class ae {

    /* renamed from: a  reason: collision with root package name */
    protected HttpURLConnection f6a = null;
    protected boolean b = false;
    int c = -1;
    private a d = a.CONN_NONE;

    /* renamed from: ae$a */
    public enum a {
        CONN_WIFI,
        CONN_CMWAP,
        CONN_CMNET,
        CONN_NONE
    }

    public ae(Context context) {
        this.d = c(context);
    }

    private int a(Context context, String str) {
        if (this.d == a.CONN_NONE) {
            return -1052;
        }
        try {
            URL url = new URL(str);
            if (this.d == a.CONN_CMWAP) {
                this.f6a = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, InetSocketAddress.createUnresolved(a(context), b(context))));
            } else {
                this.f6a = (HttpURLConnection) url.openConnection();
            }
            this.f6a.setReadTimeout(15000);
            this.f6a.setConnectTimeout(15000);
            return 0;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return -1053;
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
            return -1057;
        } catch (SecurityException e3) {
            e3.printStackTrace();
            return -1058;
        } catch (UnsupportedOperationException e4) {
            e4.printStackTrace();
            return -1059;
        } catch (IOException e5) {
            e5.printStackTrace();
            return -1056;
        } catch (Exception e6) {
            e6.printStackTrace();
            return -1000;
        }
    }

    public static String a(Context context) {
        String host = android.net.Proxy.getHost(context);
        return (host == null || host.length() == 0) ? android.net.Proxy.getDefaultHost() : host;
    }

    private static byte[] a(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read();
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(read);
        }
    }

    public static int b(Context context) {
        int port = android.net.Proxy.getPort(context);
        return port <= 0 ? android.net.Proxy.getDefaultPort() : port;
    }

    public static a c(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return (activeNetworkInfo == null || !(activeNetworkInfo.getState() == NetworkInfo.State.CONNECTING || activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED)) ? a.CONN_NONE : activeNetworkInfo.getType() == 1 ? a.CONN_WIFI : activeNetworkInfo.getType() == 0 ? (android.net.Proxy.getDefaultHost() == null && android.net.Proxy.getHost(context) == null) ? a.CONN_CMNET : a.CONN_CMWAP : a.CONN_NONE;
    }

    public int a(Context context, String str, byte[] bArr) {
        int a2 = a(context, str);
        if (a2 != 0) {
            return a2;
        }
        this.b = false;
        try {
            int length = bArr.length;
            this.f6a.setDoOutput(true);
            this.f6a.setDoInput(true);
            this.f6a.setUseCaches(false);
            this.f6a.setRequestMethod(Constants.HTTP_POST);
            this.f6a.setRequestProperty("User-Agent", "QQPimSecure");
            this.f6a.setRequestProperty("Accept", "*/*");
            this.f6a.setRequestProperty("Accept-Charset", "utf-8");
            this.f6a.setRequestProperty("Content-Type", "application/octet-stream");
            this.f6a.setRequestProperty("Content-length", Constants.STR_EMPTY + length);
            OutputStream outputStream = this.f6a.getOutputStream();
            outputStream.write(bArr);
            outputStream.close();
            int responseCode = this.f6a.getResponseCode();
            if (responseCode != 200) {
                return responseCode == -1 ? ErrorCode.ERR_POST : -2000 - responseCode;
            }
            this.b = true;
            return 0;
        } catch (IllegalAccessError e) {
            e.printStackTrace();
            return -2060;
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
            return -2061;
        } catch (ProtocolException e3) {
            e3.printStackTrace();
            return -2051;
        } catch (IOException e4) {
            e4.printStackTrace();
            return -2056;
        } catch (Exception e5) {
            e5.printStackTrace();
            return ErrorCode.ERR_POST;
        }
    }

    public int a(boolean z, AtomicReference<byte[]> atomicReference) {
        int i;
        byte[] bArr;
        InputStream inputStream;
        if (this.f6a == null || !this.b) {
            return ErrorCode.ERR_RESPONSE;
        }
        if (z) {
            try {
                inputStream = new InflaterInputStream(this.f6a.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                i = -4056;
                bArr = null;
            } catch (Exception e2) {
                e2.printStackTrace();
                i = -4000;
                bArr = null;
            }
        } else {
            inputStream = this.f6a.getInputStream();
        }
        bArr = a(inputStream);
        i = 0;
        atomicReference.set(bArr);
        if (this.f6a == null) {
            return i;
        }
        this.f6a.disconnect();
        this.f6a = null;
        return i;
    }
}
