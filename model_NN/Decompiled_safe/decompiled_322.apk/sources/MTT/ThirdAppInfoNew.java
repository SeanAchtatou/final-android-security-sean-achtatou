package MTT;

import com.tencent.connect.common.Constants;

public final class ThirdAppInfoNew implements Cloneable {
    public int iCoreType = 0;
    public long iPv = 0;
    public String sAppName = Constants.STR_EMPTY;
    public String sGuid = Constants.STR_EMPTY;
    public String sImei = Constants.STR_EMPTY;
    public String sImsi = Constants.STR_EMPTY;
    public String sLc = Constants.STR_EMPTY;
    public String sMac = Constants.STR_EMPTY;
    public String sQua = Constants.STR_EMPTY;
    public String sTime = Constants.STR_EMPTY;
}
