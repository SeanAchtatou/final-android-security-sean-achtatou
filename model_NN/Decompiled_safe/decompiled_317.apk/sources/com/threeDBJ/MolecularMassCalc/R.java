package com.threeDBJ.MolecularMassCalc;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int input = 2130837505;
        public static final int mmc_input = 2130837506;
        public static final int mmc_result = 2130837507;
        public static final int resholder = 2130837508;
    }

    public static final class id {
        public static final int ad = 2131034128;
        public static final int calc = 2131034118;
        public static final int clear = 2131034116;
        public static final int form_name = 2131034127;
        public static final int form_text = 2131034126;
        public static final int help1 = 2131034113;
        public static final int help2 = 2131034114;
        public static final int input = 2131034115;
        public static final int percent = 2131034124;
        public static final int resText1 = 2131034120;
        public static final int resText2 = 2131034122;
        public static final int result = 2131034121;
        public static final int spc1 = 2131034117;
        public static final int spc2 = 2131034119;
        public static final int spc3 = 2131034123;
        public static final int spc4 = 2131034125;
        public static final int title = 2131034112;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int main2 = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2130968576;
        public static final int formula = 2130968578;
        public static final int name = 2130968579;
        public static final int title = 2130968577;
    }

    public static final class styleable {
        public static final int[] com_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_android_ads_AdView_backgroundColor = 0;
        public static final int com_android_ads_AdView_keywords = 3;
        public static final int com_android_ads_AdView_primaryTextColor = 1;
        public static final int com_android_ads_AdView_refreshInterval = 4;
        public static final int com_android_ads_AdView_secondaryTextColor = 2;
    }
}
