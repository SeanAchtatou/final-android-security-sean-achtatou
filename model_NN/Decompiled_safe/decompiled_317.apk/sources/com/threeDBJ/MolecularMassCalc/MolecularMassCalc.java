package com.threeDBJ.MolecularMassCalc;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdActivity;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MolecularMassCalc extends Activity {
    /* access modifiers changed from: private */
    public static final String[] names = {"H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar", "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr", "Y", "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn", "Sb", "Te", "I", "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd", "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb", "Lu", "Hf", "Ta", "W", "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb", "Bi", "Po", "At", "Rn", "Fr", "Ra", "Ac", "Th", "Pa", "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm", "Md", "No", "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt", "Ds", "Rg"};
    /* access modifiers changed from: private */
    public static final String[] symbols = {"h", "he", "li", "be", "b", "c", "n", AdActivity.ORIENTATION_PARAM, "f", "ne", "na", "mg", "al", "si", "p", "s", "cl", "ar", "k", "ca", "sc", "ti", "v", "cr", "mn", "fe", "co", "ni", "cu", "zn", "ga", "ge", "as", "se", "br", "kr", "rb", "sr", "y", "zr", "nb", "mo", "tc", "ru", "rh", "pd", "ag", "cd", "in", "sn", "sb", "te", AdActivity.INTENT_ACTION_PARAM, "xe", "cs", "ba", "la", "ce", "pr", "nd", "pm", "sm", "eu", "gd", "tb", "dy", "ho", "er", "tm", "yb", "lu", "hf", "ta", "w", "re", "os", "ir", "pt", "au", "hg", "tl", "pb", "bi", "po", "at", "rn", "fr", "ra", "ac", "th", "pa", AdActivity.URL_PARAM, "np", "pu", "am", "cm", "bk", "cf", "es", "fm", "md", "no", "lr", "rf", "db", "sg", "bh", "hs", "mt", "ds", "rg"};
    /* access modifiers changed from: private */
    public static final double[] weights = {1.00794d, 4.002602d, 6.941d, 9.012182d, 10.811d, 12.0107d, 14.0067d, 15.9994d, 18.9984032d, 20.1797d, 22.98976928d, 24.305d, 26.9815386d, 28.0855d, 30.973762d, 32.065d, 35.453d, 39.948d, 39.0983d, 40.078d, 44.955912d, 47.867d, 50.9415d, 51.9961d, 54.938045d, 55.845d, 58.933195d, 58.6934d, 63.546d, 65.38d, 69.723d, 72.64d, 74.9216d, 78.96d, 79.904d, 83.798d, 85.4678d, 87.62d, 88.90585d, 91.224d, 92.90638d, 95.96d, 98.0d, 101.07d, 102.9055d, 106.42d, 107.8682d, 112.411d, 114.818d, 118.71d, 121.76d, 127.6d, 126.90447d, 131.293d, 132.9054519d, 137.327d, 138.90547d, 140.116d, 140.90765d, 144.242d, 145.0d, 150.36d, 151.964d, 157.25d, 158.92535d, 162.5d, 164.93032d, 167.259d, 168.93421d, 173.054d, 174.9668d, 178.49d, 180.94788d, 183.84d, 186.207d, 190.23d, 192.217d, 195.084d, 196.966569d, 200.59d, 204.3833d, 207.2d, 208.9804d, 209.0d, 210.0d, 222.0d, 223.0d, 226.0d, 227.0d, 232.03806d, 231.03588d, 238.02891d, 237.0d, 244.0d, 243.0d, 247.0d, 247.0d, 251.0d, 252.0d, 257.0d, 258.0d, 259.0d, 262.0d, 267.0d, 268.0d, 271.0d, 272.0d, 270.0d, 276.0d, 281.0d, 280.0d};
    private TextView.OnEditorActionListener calcAction = new TextView.OnEditorActionListener() {
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (event == null || event.getKeyCode() != 66) {
                return false;
            }
            ((InputMethodManager) MolecularMassCalc.this.getSystemService("input_method")).hideSoftInputFromWindow(MolecularMassCalc.this.input.getApplicationWindowToken(), 2);
            MolecularMassCalc.this.dispResult();
            return false;
        }
    };
    private View.OnClickListener calcBtn = new View.OnClickListener() {
        public void onClick(View v) {
            MolecularMassCalc.this.dispResult();
        }
    };
    private View.OnClickListener clearBtn = new View.OnClickListener() {
        public void onClick(View v) {
            MolecularMassCalc.this.input.setText("");
        }
    };
    /* access modifiers changed from: private */
    public EditText input;
    /* access modifiers changed from: private */
    public Parser p;
    private View.OnClickListener percentBtn = new View.OnClickListener() {
        public void onClick(View v) {
            if (MolecularMassCalc.this.p.symbs.size() > 0) {
                MolecularMassCalc.this.openContextMenu(MolecularMassCalc.this.findViewById(R.id.percent));
            }
        }
    };
    private TextView result;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("testing...", "made it");
        Configuration config = getResources().getConfiguration();
        if (config.orientation == 1) {
            setContentView((int) R.layout.main);
        } else if (config.orientation == 2) {
            setContentView((int) R.layout.main2);
        }
        this.input = (EditText) findViewById(R.id.input);
        this.input.setOnEditorActionListener(this.calcAction);
        this.result = (TextView) findViewById(R.id.result);
        this.p = new Parser();
        ((Button) findViewById(R.id.calc)).setOnClickListener(this.calcBtn);
        ((Button) findViewById(R.id.clear)).setOnClickListener(this.clearBtn);
        Button n = (Button) findViewById(R.id.percent);
        n.setOnClickListener(this.percentBtn);
        registerForContextMenu(n);
        ((AdView) findViewById(R.id.ad)).loadAd(new AdRequest());
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.percent) {
            menu.setHeaderTitle("Mass Percentages");
            DecimalFormat twoDForm = new DecimalFormat("#.####");
            MenuItem add = menu.add(this.p.getCleanForm());
            for (int i = 0; i < this.p.symbs.size(); i++) {
                MenuItem item = menu.add((this.p.symbs.get(i) + Integer.toString(this.p.nums.get(i).intValue()) + " = ") + Double.toString(Double.valueOf(twoDForm.format((this.p.vals.get(i).doubleValue() / this.p.tot) * 100.0d)).doubleValue()) + "%");
            }
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        return true;
    }

    /* access modifiers changed from: private */
    public void dispResult() {
        String inp = this.input.getText().toString();
        String res = calculate(inp);
        if (res.length() > 11) {
            res = res.substring(0, 11);
        }
        this.result.setText(res);
        ((TextView) findViewById(R.id.form_name)).setText("Loading...");
        new RetrieveFormula().execute(inp);
    }

    private String calculate(String inp) {
        double res = 0.0d;
        this.p.setInput(inp);
        while (true) {
            try {
                double val = this.p.nextVal();
                if (val <= 0.0d) {
                    return Double.toString(Double.valueOf(new DecimalFormat("#.####").format(res)).doubleValue());
                }
                res += val;
            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), 0).show();
                return "";
            }
        }
    }

    private class Parser {
        String input;
        String last;
        Matcher mat;
        public ArrayList<Integer> nums;
        Pattern pat1;
        Pattern pat2;
        public ArrayList<String> symbs;
        public double tot;
        public ArrayList<Double> vals;

        private Parser() {
            this.symbs = new ArrayList<>();
            this.vals = new ArrayList<>();
            this.nums = new ArrayList<>();
            this.pat1 = Pattern.compile("([a-zA-Z]{1,2}).*");
            this.pat2 = Pattern.compile("([0-9]+).*");
            this.tot = 0.0d;
        }

        public void setInput(String s) {
            this.input = s.toLowerCase();
            this.symbs.clear();
            this.vals.clear();
            this.nums.clear();
            this.tot = 0.0d;
            this.last = "";
        }

        public double nextVal() throws Exception {
            if (this.input == null) {
                return 0.0d;
            }
            if (this.input.length() == 0) {
                return 0.0d;
            }
            String sym = "";
            try {
                sym = nextSymb();
                double num = nextNum();
                int ind = indexOf(MolecularMassCalc.symbols, sym);
                double ans = MolecularMassCalc.weights[ind] * num;
                this.symbs.add(MolecularMassCalc.names[ind]);
                this.nums.add(Integer.valueOf((int) num));
                this.vals.add(Double.valueOf(ans));
                this.tot += ans;
                return ans;
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new Exception("Unrecognized element '" + sym + "'");
            } catch (Exception e2) {
                throw e2;
            }
        }

        private String nextSymb() throws Exception {
            this.mat = this.pat1.matcher(this.input);
            if (!this.mat.matches()) {
                throw new Exception("Invalid element");
            }
            this.input = this.input.substring(this.mat.group(1).length());
            this.last = this.mat.group(1);
            return this.last;
        }

        private double nextNum() throws Exception {
            this.mat = this.pat2.matcher(this.input);
            if (!this.mat.matches()) {
                throw new Exception("Could not parse number after " + this.last);
            }
            this.input = this.input.substring(this.mat.group(1).length());
            return (double) Integer.parseInt(this.mat.group(1));
        }

        private int indexOf(String[] arr, String x) {
            for (int i = 0; i < arr.length; i++) {
                if (arr[i].compareTo(x) == 0) {
                    return i;
                }
            }
            return -1;
        }

        public String getCleanForm() {
            String ret = "";
            for (int i = 0; i < this.symbs.size(); i++) {
                ret = ret + this.symbs.get(i);
                int n = this.nums.get(i).intValue();
                if (n > 1) {
                    ret = ret + Integer.toString(n);
                }
            }
            return ret;
        }
    }

    private class RetrieveFormula extends AsyncTask<String, Integer, String> {
        private RetrieveFormula() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... data) {
            try {
                URLConnection con = new URL("http://webbook.nist.gov/cgi/cbook.cgi?Formula=" + data[0] + "&NoIon=on&Units=SI").openConnection();
                con.setDoOutput(true);
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String res = "";
                while (true) {
                    String line = in.readLine();
                    if (line == null) {
                        return res;
                    }
                    res = res + line;
                }
            } catch (MalformedURLException e) {
                Log.v("make_query", e.getMessage());
                return "Error";
            } catch (IOException e2) {
                Log.v("make_query", e2.getMessage());
                return "Error";
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... prog) {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            Log.v("mmc", result);
            Matcher mat = Pattern.compile(".*<title>(.*)</title>.*").matcher(result);
            TextView e = (TextView) MolecularMassCalc.this.findViewById(R.id.form_name);
            if (!mat.matches()) {
                return;
            }
            if (mat.group(1).equals("Search Results")) {
                Matcher mat2 = Pattern.compile(".*?<li><a.*?>(.*?)</a>.*").matcher(result);
                mat2.matches();
                e.setText(mat2.group(1));
                Log.v("mmc", mat2.group(1));
                Log.v("mmc", Integer.toString(mat2.groupCount()));
                return;
            }
            e.setText(mat.group(1));
            Log.v("mmc", mat.group(1));
        }
    }
}
