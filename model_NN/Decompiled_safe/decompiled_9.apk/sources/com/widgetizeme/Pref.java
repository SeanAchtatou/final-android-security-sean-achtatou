package com.widgetizeme;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

public class Pref {
    public static final String ASSETS_LOADED = "assets_loaded";
    public static final String CID = "cid";
    public static final String FEEDS_NAMES = "feed_names";
    public static final String FEED_ENCODING = "feed_encoding_";
    public static final String GCM_APP_VERSION = "gcm_app_version";
    public static final String GCM_ON_SERVER_EXPIRATION_TIME = "gcm_on_server_expiration_time";
    public static final String GCM_REG_ID = "gcm_reg_id";
    public static final String IS_GUIDE_SHOWN = "is_guide_shown";
    public static final String LAST_PING = "last_ping";
    public static final String LAST_UPDATE = "last_update";
    public static final String OFFER_START_INDEX = "offer_start_index";
    private static final String PREF_NAME = "com.widgetizeme";
    public static final String REFERRER = "referrer";
    public static final String RESEND_GCM_TOKEN = "resend_gmc_token";
    public static final String SENT_INSTALL_STAT = "sent_install_stat";
    public static final String SHARE_TEXT = "share_text";
    public static final String START_INDEX = "start_index_";
    public static final String WIDGET_ID = "widget_id";
    public static final String WIDGET_LAST_CREATE = "widget_last_create_";
    private static PBEParameterSpec sPBEParamSpec;
    private static SharedPreferences sPref;

    public static void init(Context context) {
        try {
            sPBEParamSpec = new PBEParameterSpec(Settings.Secure.getString(context.getContentResolver(), "android_id").getBytes("utf-8"), 20);
        } catch (Exception e) {
            Logger.l(e);
        }
        sPref = context.getSharedPreferences(PREF_NAME, 0);
    }

    public static boolean isFirstRun() {
        return sPref.getAll().isEmpty();
    }

    public static void setListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        sPref.registerOnSharedPreferenceChangeListener(listener);
    }

    public static boolean isEncKey(String encKey, String key) {
        return encrypt(key).equals(encKey);
    }

    public static void setPrefString(String name, String value) {
        Logger.l("setPrefString " + name + "=" + value);
        setPrefStringEnc(name, value);
    }

    public static void setPrefLong(String name, long value) {
        setPrefString(name, String.valueOf(value));
    }

    public static void setPrefBoolean(String name, boolean value) {
        setPrefString(name, String.valueOf(value));
    }

    public static void setPrefInt(String name, int value) {
        setPrefString(name, String.valueOf(value));
    }

    public static String getPrefString(String name) {
        return getPrefStringEnc(name);
    }

    public static boolean getPrefBoolean(String name) {
        String value = getPrefStringEnc(name);
        if (!TextUtils.isEmpty(value)) {
            try {
                return Boolean.parseBoolean(value);
            } catch (Exception e) {
                Logger.l(e);
            }
        }
        return false;
    }

    public static long getPrefLong(String name) {
        String value = getPrefStringEnc(name);
        if (!TextUtils.isEmpty(value)) {
            try {
                return Long.parseLong(value);
            } catch (Exception e) {
                Logger.l(e);
            }
        }
        return 0;
    }

    public static int getPrefInt(String name) {
        String value = getPrefStringEnc(name);
        if (!TextUtils.isEmpty(value)) {
            try {
                return Integer.parseInt(value);
            } catch (Exception e) {
                Logger.l(e);
            }
        }
        return 0;
    }

    private static void setPrefStringEnc(String name, String value) {
        String name2 = encrypt(name);
        String value2 = encrypt(value);
        SharedPreferences.Editor editor = sPref.edit();
        editor.putString(name2, value2);
        editor.commit();
    }

    private static String getPrefStringEnc(String name) {
        String value = sPref.getString(encrypt(name), null);
        if (value != null) {
            return decrypt(value);
        }
        return value;
    }

    private static String encrypt(String value) {
        try {
            char[] sod = new char[32];
            for (int i = 0; i < sod.length; i++) {
                sod[i] = (char) (((i * 2) + 40) - (i / 2));
            }
            byte[] bytes = value != null ? value.getBytes("utf-8") : new byte[0];
            SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(new PBEKeySpec(sod));
            Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
            pbeCipher.init(1, key, sPBEParamSpec);
            return new String(Base64.encode(pbeCipher.doFinal(bytes), 2), "utf-8");
        } catch (Exception e) {
            Logger.l(e);
            return null;
        }
    }

    private static String decrypt(String value) {
        try {
            char[] sod = new char[32];
            for (int i = 0; i < sod.length; i++) {
                sod[i] = (char) (((i * 2) + 40) - (i / 2));
            }
            byte[] bytes = value != null ? Base64.decode(value, 0) : new byte[0];
            SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(new PBEKeySpec(sod));
            Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
            pbeCipher.init(2, key, sPBEParamSpec);
            return new String(pbeCipher.doFinal(bytes), "utf-8");
        } catch (Exception e) {
            Logger.l(e);
            return null;
        }
    }
}
