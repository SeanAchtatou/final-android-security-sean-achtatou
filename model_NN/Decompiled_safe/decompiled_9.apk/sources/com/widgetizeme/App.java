package com.widgetizeme;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.plus.PlusShare;
import com.widgetizeme.rss.ImageStore;
import com.widgetizeme.rss.RSSFeed;
import com.widgetizeme.stats.SendStats;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

public class App extends Application {
    public static final int MODE_NORMAL = 0;
    public static final int MODE_RECOMMENDATIONS = 1;
    private static final long REGISTRATION_EXPIRY_TIME_MS = 604800000;
    private static final String SENDER_ID = "494713266773";
    private HashMap<Integer, Integer> mCategoryTracker;
    /* access modifiers changed from: private */
    public GoogleCloudMessaging mGCM;
    private ImageStore mImageStore;
    private boolean mInOffersMode;
    /* access modifiers changed from: private */
    public UnsentStats mUnsentStats;
    private HashSet<Integer> mUpdateTracker;
    private HashMap<Integer, Integer> mWidgetModeTracker;

    @SuppressLint({"UseSparseArrays"})
    public void onCreate() {
        super.onCreate();
        Pref.init(this);
        Strings.init(this);
        Colors.init(this);
        this.mUnsentStats = new UnsentStats(this);
        if (Pref.isFirstRun()) {
            Pref.setPrefString(Pref.SHARE_TEXT, getString(R.string.default_share_text));
        }
        this.mImageStore = new ImageStore(this);
        this.mUpdateTracker = new HashSet<>();
        this.mWidgetModeTracker = new HashMap<>();
        this.mCategoryTracker = new HashMap<>();
        if (TextUtils.isEmpty(getRegistrationId(this))) {
            registerGCM();
        }
        sendUnsentStats();
    }

    public static boolean isExtensation(Context context) {
        String pkgName = context.getPackageName();
        if (pkgName.equals("com.widgetizefb") || pkgName.equals("com.twitto")) {
            return true;
        }
        return false;
    }

    public static void sendInstallStat(Context context) {
        if (!Pref.getPrefBoolean(Pref.SENT_INSTALL_STAT)) {
            Logger.l("sendInstallStat");
            Pref.setPrefBoolean(Pref.SENT_INSTALL_STAT, true);
            new SendStats(context, 3).start();
        }
    }

    public UnsentStats getUnsentStats() {
        return this.mUnsentStats;
    }

    public ImageStore getImageStore() {
        return this.mImageStore;
    }

    public boolean isWidgetUpdating(int widgetId) {
        return this.mUpdateTracker.contains(Integer.valueOf(widgetId));
    }

    public ArrayList<RSSFeed> getFeeds() {
        ArrayList<RSSFeed> feeds = new ArrayList<>();
        String[] feedsList = Strings.get().getStringArray(R.array.feeds);
        for (String rSSFeed : feedsList) {
            feeds.add(new RSSFeed(this, rSSFeed));
        }
        addCustomeFeeds(feeds);
        return feeds;
    }

    public void updateWidgetUpdateState(int widgetId, boolean updating) {
        if (updating) {
            Pref.setPrefLong("last_update_" + widgetId, System.currentTimeMillis());
            this.mUpdateTracker.add(Integer.valueOf(widgetId));
            return;
        }
        this.mUpdateTracker.remove(Integer.valueOf(widgetId));
    }

    public boolean isInOffersMode() {
        return this.mInOffersMode;
    }

    public void setOffersMode(boolean mode) {
        this.mInOffersMode = mode;
    }

    public boolean isInRecommendationMode(int widgetId) {
        Integer value = this.mWidgetModeTracker.get(Integer.valueOf(widgetId));
        if (value == null || 1 != value.intValue()) {
            return false;
        }
        return true;
    }

    public void setRecommendationMode(int widgetId, boolean mode) {
        this.mWidgetModeTracker.put(Integer.valueOf(widgetId), Integer.valueOf(mode ? 1 : 0));
    }

    public void setCurrCategory(int widgetId, int categoryIndex) {
        this.mCategoryTracker.put(Integer.valueOf(widgetId), Integer.valueOf(categoryIndex));
    }

    public int getCurrCategory(int widgetId) {
        Integer value = this.mCategoryTracker.get(Integer.valueOf(widgetId));
        if (value != null) {
            return value.intValue();
        }
        return 0;
    }

    public void sendStats(int action) {
        new SendStats(this, action).start();
    }

    public void sendClickStats(String url) {
        new SendStats(this, url).start();
    }

    public boolean sendGCMToken(String token) {
        boolean retVal = false;
        int iTry = 0;
        while (!retVal && iTry < 3) {
            try {
                HttpParams httpParameters = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParameters, 10000);
                HttpConnectionParams.setSoTimeout(httpParameters, 10000);
                HttpClient httpClient = new DefaultHttpClient(httpParameters);
                HttpPost request = new HttpPost("http://widgetize.me/api/Token");
                request.addHeader("Content-Type", "application/json; charset=utf-8");
                JSONObject json = new JSONObject();
                json.put("PlayStoreId", getPackageName());
                json.put("Token", token);
                json.put("CountryCode", getCountry(this));
                request.setEntity(new StringEntity(json.toString()));
                Logger.l("post: " + "http://widgetize.me/api/Token" + ", data: " + json.toString());
                HttpResponse response = httpClient.execute(request);
                if (response.getStatusLine().getStatusCode() != 200) {
                    Logger.l(response.getStatusLine().getReasonPhrase());
                } else {
                    StringBuilder sb = new StringBuilder();
                    try {
                        InputStream content = response.getEntity().getContent();
                        try {
                            BufferedReader rd = new BufferedReader(new InputStreamReader(content, "utf-8"));
                            try {
                                for (String line = rd.readLine(); line != null; line = rd.readLine()) {
                                    sb.append(line);
                                }
                            } catch (Exception e) {
                                Logger.l(e);
                            }
                            rd.close();
                        } catch (Exception e2) {
                            Logger.l("Error while reading");
                        }
                        content.close();
                    } catch (Exception e3) {
                        Logger.l("Error while opening input stream");
                    }
                    Logger.l("response: " + sb.toString());
                    retVal = true;
                }
                response.getEntity().consumeContent();
            } catch (Exception e4) {
                Logger.l(e4);
            }
            if (!retVal) {
                Logger.l("send token failed - try again");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e5) {
                }
            }
            iTry++;
        }
        return retVal;
    }

    /* access modifiers changed from: protected */
    public void addCustomeFeeds(ArrayList<RSSFeed> feeds) {
        try {
            String selectedFeeds = Pref.getPrefString("selected_feeds");
            if (!TextUtils.isEmpty(selectedFeeds)) {
                JSONArray feedsJSON = new JSONArray(selectedFeeds);
                for (int i = 0; i < feedsJSON.length(); i++) {
                    feeds.add(new RSSFeed(this, feedsJSON.getJSONObject(i).getString(PlusShare.KEY_CALL_TO_ACTION_URL)));
                }
            }
        } catch (Exception e) {
            Logger.l(e);
        }
    }

    private String getRegistrationId(Context context) {
        String registrationId = Pref.getPrefString(Pref.GCM_REG_ID);
        if (TextUtils.isEmpty(registrationId)) {
            Logger.l("Registration not found.");
            return "";
        }
        int registeredVersion = Pref.getPrefInt(Pref.GCM_APP_VERSION);
        if (registeredVersion == 0) {
            registeredVersion = Integer.MIN_VALUE;
        }
        if (registeredVersion == getAppVersion(context) && !isRegistrationExpired()) {
            return registrationId;
        }
        Logger.l("App version changed or registration expired.");
        return "";
    }

    private static int getAppVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private boolean isRegistrationExpired() {
        long expirationTime = Pref.getPrefLong(Pref.GCM_ON_SERVER_EXPIRATION_TIME);
        if (0 == expirationTime) {
            expirationTime = -1;
        }
        return System.currentTimeMillis() > expirationTime;
    }

    private void registerGCM() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    if (App.this.mGCM == null) {
                        App.this.mGCM = GoogleCloudMessaging.getInstance(App.this);
                    }
                    String regid = App.this.mGCM.register(App.SENDER_ID);
                    Logger.l("Device registered, registration id=" + regid);
                    App.this.setRegistrationId(App.this, regid);
                } catch (Exception e) {
                    Logger.l(e);
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void setRegistrationId(Context context, String regId) {
        int appVersion = getAppVersion(context);
        Logger.l("Saving regId on app version " + appVersion);
        Pref.setPrefString(Pref.GCM_REG_ID, regId);
        Pref.setPrefInt(Pref.GCM_APP_VERSION, appVersion);
        long expirationTime = System.currentTimeMillis() + REGISTRATION_EXPIRY_TIME_MS;
        Logger.l("Setting registration expiry time to " + expirationTime);
        Pref.setPrefLong(Pref.GCM_ON_SERVER_EXPIRATION_TIME, expirationTime);
        if (!sendGCMToken(regId)) {
            Pref.setPrefBoolean(Pref.RESEND_GCM_TOKEN, true);
        }
    }

    private String getCountry(Context context) {
        String retVal = ((TelephonyManager) context.getSystemService("phone")).getNetworkCountryIso().toLowerCase();
        if (TextUtils.isEmpty(retVal)) {
            retVal = Locale.getDefault().getCountry();
        }
        if (TextUtils.isEmpty(retVal)) {
            return context.getResources().getConfiguration().locale.getCountry();
        }
        return retVal;
    }

    private void sendUnsentStats() {
        new Thread(new Runnable() {
            public void run() {
                int action = App.this.mUnsentStats.pop();
                while (action != 0 && App.this.isNetworkAvailable()) {
                    int lastAction = action;
                    new SendStats(App.this, action).run();
                    action = App.this.mUnsentStats.pop();
                    if (lastAction == action) {
                        action = 0;
                    }
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public boolean isNetworkAvailable() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
