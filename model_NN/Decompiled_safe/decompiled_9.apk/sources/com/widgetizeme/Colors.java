package com.widgetizeme;

import android.content.Context;
import android.graphics.Color;
import java.io.FileInputStream;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class Colors {
    private static Colors sInstance;
    private HashMap<Integer, Integer> mColors = new HashMap<>();
    private Context mContext;

    public static void init(Context context) {
        sInstance = new Colors(context);
    }

    public static Colors get() {
        return sInstance;
    }

    public boolean isLoaded() {
        return !this.mColors.isEmpty();
    }

    public int getColor(int id) {
        Integer retVal = this.mColors.get(Integer.valueOf(id));
        if (retVal == null) {
            return this.mContext.getResources().getColor(id);
        }
        return retVal.intValue();
    }

    private Colors(Context context) {
        this.mContext = context;
        loadColors();
        Logger.l("colors: " + this.mColors.size());
    }

    private void loadColors() {
        try {
            FileInputStream input = this.mContext.openFileInput("colors.xml");
            try {
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser xpp = factory.newPullParser();
                xpp.setInput(input, "utf-8");
                parseXML(xpp);
            } catch (Exception e) {
                Logger.l(e);
            }
            input.close();
        } catch (Exception e2) {
        }
    }

    private int getColorId(String name) {
        return this.mContext.getResources().getIdentifier(name, "color", this.mContext.getPackageName());
    }

    private void parseXML(XmlPullParser xpp) {
        while (1 != xpp.getEventType()) {
            try {
                String tag = xpp.getName();
                if (2 == xpp.getEventType() && tag.equals("color")) {
                    String name = xpp.getAttributeValue(null, "name");
                    xpp.next();
                    String value = xpp.getText();
                    this.mColors.put(Integer.valueOf(getColorId(name)), Integer.valueOf(parseColorString(value)));
                }
                xpp.next();
            } catch (Exception e) {
                Logger.l(e);
                return;
            }
        }
    }

    private int parseColorString(String str) {
        if (str.length() != 9) {
            str = str.replace("#", "#ff");
        }
        return Color.parseColor(str);
    }
}
