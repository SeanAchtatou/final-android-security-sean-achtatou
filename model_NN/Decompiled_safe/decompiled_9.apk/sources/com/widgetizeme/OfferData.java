package com.widgetizeme;

public class OfferData {
    private OfferDetails[] mItems = createItems();

    public class OfferDetails {
        public String mDesc;
        public int mIcon;
        public String mName;
        public String mPackageName;
        public int mStars;

        public OfferDetails(String name, String pkgName, String desc, int icon, int stars) {
            this.mName = name;
            this.mPackageName = pkgName;
            this.mDesc = desc;
            this.mIcon = icon;
            this.mStars = stars;
        }
    }

    public OfferDetails getOffer(int index) {
        return this.mItems[index];
    }

    public int getCount() {
        return this.mItems.length;
    }

    private OfferDetails[] createItems() {
        return new OfferDetails[9];
    }
}
