package com.widgetizeme.stats;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.widgetizeme.App;
import com.widgetizeme.Logger;
import com.widgetizeme.Pref;
import com.widgetizeme.UnsentStats;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Iterator;
import java.util.Locale;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.util.InetAddressUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class SendStats extends Thread {
    public static final int EVENT_CLICK = 1;
    public static final int EVENT_INSTALL = 3;
    public static final int EVENT_PING = 6;
    public static final int EVENT_SHARE = 2;
    public static final int EVENT_UNINSTALL = 4;
    public static final int EVENT_UNKNOWN = 5;
    private int mActionTypeId;
    private String mBrowser;
    private String mCID;
    private String mClickedURL;
    private String mCountryId;
    private String mIP;
    private String mOSVersion;
    private String mPlayStoreId;
    private UnsentStats mUnsentStats;
    private String mWidgetId;
    private int mWifi;

    public SendStats(Context context, int action) {
        int idStart;
        int idEnd;
        this.mUnsentStats = ((App) context.getApplicationContext()).getUnsentStats();
        this.mPlayStoreId = context.getPackageName();
        this.mActionTypeId = action;
        this.mCountryId = getCountry(context);
        this.mOSVersion = Build.VERSION.RELEASE;
        this.mWifi = isOnWifi(context) ? 1 : 0;
        this.mBrowser = "Android";
        this.mIP = getIPAddress(true);
        this.mCID = Pref.getPrefString(Pref.CID);
        if (TextUtils.isEmpty(this.mCID)) {
            this.mCID = "0";
        }
        this.mWidgetId = Pref.getPrefString("widget_id");
        if (TextUtils.isEmpty(this.mWidgetId)) {
            String pkgName = context.getPackageName();
            if (pkgName.contains("com.widgetizefb")) {
                this.mWidgetId = "3";
            } else if (pkgName.contains("com.twitto")) {
                this.mWidgetId = "2";
            } else {
                this.mWidgetId = "1";
                int idStart2 = pkgName.indexOf("_id");
                if (!(-1 == idStart2 || -1 == (idEnd = pkgName.indexOf("_", (idStart = idStart2 + 3))))) {
                    this.mWidgetId = pkgName.substring(idStart, idEnd);
                }
            }
        }
        this.mClickedURL = "0";
    }

    public SendStats(Context context, String clickURL) {
        this(context, 1);
        this.mClickedURL = clickURL;
    }

    public void run() {
        if (get() == null) {
            this.mUnsentStats.push(this.mActionTypeId);
        }
    }

    private boolean isValidResponse(int resultCode) {
        return 200 == resultCode || 204 == resultCode;
    }

    private String get() {
        String retVal = null;
        int resultCode = 0;
        int iTry = 0;
        while (!isValidResponse(resultCode) && iTry < 3) {
            try {
                HttpParams params = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(params, 10000);
                HttpConnectionParams.setSoTimeout(params, 10000);
                HttpClient httpClient = new DefaultHttpClient(params);
                String url = getURL();
                Logger.l("call: " + url);
                resultCode = httpClient.execute(new HttpGet(url)).getStatusLine().getStatusCode();
                if (isValidResponse(resultCode)) {
                    Logger.l("stat sent");
                    retVal = "";
                }
            } catch (Exception e) {
                Logger.l(e);
            }
            if (!isValidResponse(resultCode)) {
                Logger.l("stat not sent - try again");
                try {
                    Thread.sleep(1000);
                } catch (Exception e2) {
                    Logger.l(e2);
                }
            }
            iTry++;
        }
        return retVal;
    }

    private String getURL() {
        return "http://widgetize.me/api/WidgetActionDailyStat/?" + "parameters=" + this.mPlayStoreId + "," + this.mActionTypeId + "," + this.mCountryId + "," + this.mOSVersion + "," + this.mWifi + "," + this.mBrowser + "," + this.mIP + "," + this.mClickedURL + "," + this.mCID + "," + this.mWidgetId + "," + System.currentTimeMillis();
    }

    private String getCountry(Context context) {
        String retVal = ((TelephonyManager) context.getSystemService("phone")).getNetworkCountryIso().toLowerCase();
        if (TextUtils.isEmpty(retVal)) {
            retVal = Locale.getDefault().getCountry();
        }
        if (TextUtils.isEmpty(retVal)) {
            return context.getResources().getConfiguration().locale.getCountry();
        }
        return retVal;
    }

    private boolean isOnWifi(Context context) {
        NetworkInfo wifiInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1);
        if ((wifiInfo == null || wifiInfo.getState() != NetworkInfo.State.CONNECTED) && wifiInfo.getState() != NetworkInfo.State.CONNECTING) {
            return false;
        }
        return true;
    }

    private String getIPAddress(boolean useIPv4) {
        try {
            for (NetworkInterface intf : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                Iterator it = Collections.list(intf.getInetAddresses()).iterator();
                while (true) {
                    if (it.hasNext()) {
                        InetAddress addr = (InetAddress) it.next();
                        if (!addr.isLoopbackAddress()) {
                            String sAddr = addr.getHostAddress().toUpperCase();
                            boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                            if (useIPv4) {
                                if (isIPv4) {
                                    return sAddr;
                                }
                            } else if (!isIPv4) {
                                int delim = sAddr.indexOf(37);
                                if (delim >= 0) {
                                    return sAddr.substring(0, delim);
                                }
                                return sAddr;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
        return "192.11.23.231";
    }
}
