package com.widgetizeme.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.google.android.gms.plus.PlusShare;
import com.widgetizeme.Logger;
import com.widgetizeme.Pref;
import com.widgetizeme.R;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class SettingsActivity extends Activity {
    /* access modifiers changed from: private */
    public int mAppWidgetId;
    private TextView mSelectText;
    /* access modifiers changed from: private */
    public ArrayList<FeedItem> mSelectedFeeds;

    public class FeedItem {
        private String mName;
        private String mURL;

        public FeedItem(String name, String url) {
            this.mName = name;
            this.mURL = url;
        }

        public String toString() {
            return String.valueOf(this.mName) + "~" + this.mURL;
        }

        public String getName() {
            return this.mName;
        }

        public String getURL() {
            return this.mURL;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.mAppWidgetId = extras.getInt("appWidgetId", 0);
        }
        this.mSelectText = (TextView) findViewById(R.id.select_text);
        this.mSelectedFeeds = getSelectedFeeds();
        loadFeeds();
        findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!SettingsActivity.this.mSelectedFeeds.isEmpty()) {
                    SettingsActivity.this.saveSelectedFeeds();
                    SettingsActivity.this.refreshAllWidgets();
                    if (SettingsActivity.this.mAppWidgetId == 0 && !Pref.getPrefBoolean("seen_finish_dialog")) {
                        Pref.setPrefBoolean("seen_finish_dialog", true);
                        SettingsActivity.this.showFinishAlert();
                    }
                    SettingsActivity.this.finish();
                }
            }
        });
        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingsActivity.this.finish();
            }
        });
    }

    private void showSelectAlert() {
    }

    /* access modifiers changed from: private */
    public void showFinishAlert() {
        startActivity(new Intent(this, GuideActivity.class));
        finish();
    }

    private void loadFeeds() {
        ArrayList<FeedItem> feeds = new ArrayList<>();
        feeds.add(new FeedItem("TechCrunch", "http://feeds.feedburner.com/TechCrunch"));
        feeds.add(new FeedItem("Computerworld", "http://blogs.computerworld.com/rss.xml"));
        feeds.add(new FeedItem("Gigaom", "http://feeds.feedburner.com/ommalik"));
        feeds.add(new FeedItem("Mashable", "http://mashable.com/rss/"));
        feeds.add(new FeedItem("GIZMODO", "http://feeds.gawker.com/gizmodo/full"));
        feeds.add(new FeedItem("Digital Inspiration", "http://feeds.labnol.org/labnol"));
        feeds.add(new FeedItem("Next Big Future", "http://feeds.feedburner.com/blogspot/advancednano"));
        feeds.add(new FeedItem("Social Network by Laurel Papwoth", "http://feeds.feedburner.com/LaurelPapworth-OnlineCommunities-AustraliaAndGlobal"));
        feeds.add(new FeedItem("Reuters Business News", "http://feeds.reuters.com/reuters/businessNews"));
        feeds.add(new FeedItem("Boing Boing", "http://feeds.boingboing.net/boingboing/iBag"));
        onLoadingFeedsDone(feeds);
    }

    private void onLoadingFeedsDone(ArrayList<FeedItem> feeds) {
        ((ListView) findViewById(R.id.list)).setAdapter((ListAdapter) new FeedListAdapter(this, feeds));
        updateSelectedText();
        if (this.mAppWidgetId == 0 && !Pref.getPrefBoolean("seen_select_dialog")) {
            Pref.setPrefBoolean("seen_select_dialog", true);
            showSelectAlert();
        }
    }

    /* access modifiers changed from: private */
    public boolean addSelectedFeed(FeedItem feed) {
        if (isFeedSelected(feed)) {
            return true;
        }
        if (3 == this.mSelectedFeeds.size()) {
            return false;
        }
        this.mSelectedFeeds.add(feed);
        updateSelectedText();
        return true;
    }

    /* access modifiers changed from: private */
    public void removeSelectedFeed(FeedItem feed) {
        for (int i = 0; i < this.mSelectedFeeds.size(); i++) {
            if (this.mSelectedFeeds.get(i).getURL().equals(feed.getURL())) {
                this.mSelectedFeeds.remove(i);
                updateSelectedText();
                return;
            }
        }
    }

    private void updateSelectedText() {
        this.mSelectText.setText(String.valueOf(this.mSelectedFeeds.size()) + "\\3 feeds selected");
    }

    /* access modifiers changed from: private */
    public boolean isFeedSelected(FeedItem feed) {
        for (int i = 0; i < this.mSelectedFeeds.size(); i++) {
            if (this.mSelectedFeeds.get(i).getURL().equals(feed.getURL())) {
                return true;
            }
        }
        return false;
    }

    private ArrayList<FeedItem> getSelectedFeeds() {
        ArrayList<FeedItem> retVal = new ArrayList<>();
        try {
            String cache = Pref.getPrefString("selected_feeds");
            if (!TextUtils.isEmpty(cache)) {
                JSONArray feeds = new JSONArray(cache);
                for (int i = 0; i < feeds.length(); i++) {
                    JSONObject feed = feeds.getJSONObject(i);
                    retVal.add(new FeedItem(feed.getString("name"), feed.getString(PlusShare.KEY_CALL_TO_ACTION_URL)));
                }
            }
        } catch (Exception e) {
            Logger.l(e);
        }
        return retVal;
    }

    /* access modifiers changed from: private */
    public void saveSelectedFeeds() {
        String names = "";
        try {
            JSONArray feeds = new JSONArray();
            for (int i = 0; i < this.mSelectedFeeds.size(); i++) {
                FeedItem g = this.mSelectedFeeds.get(i);
                JSONObject feed = new JSONObject();
                feed.put("name", g.getName());
                feed.put(PlusShare.KEY_CALL_TO_ACTION_URL, g.getURL());
                feeds.put(feed);
                names = String.valueOf(names) + g.getName();
                if (i + 1 < this.mSelectedFeeds.size()) {
                    names = String.valueOf(names) + "~";
                }
            }
            Pref.setPrefString("selected_feeds", feeds.toString());
        } catch (Exception e) {
            Logger.l(e);
        }
        Pref.setPrefString(Pref.FEEDS_NAMES, names);
    }

    /* access modifiers changed from: private */
    public void refreshAllWidgets() {
        Logger.l("refreshAllWidgets");
        setResult(-1);
    }

    private class FeedListAdapter extends BaseAdapter {
        private ArrayList<FeedItem> mFeeds;
        private LayoutInflater mInflater;

        private class ViewHolder {
            /* access modifiers changed from: private */
            public CheckBox mCheck;
            /* access modifiers changed from: private */
            public TextView mName;

            private ViewHolder() {
            }

            /* synthetic */ ViewHolder(FeedListAdapter feedListAdapter, ViewHolder viewHolder) {
                this();
            }
        }

        public FeedListAdapter(Context context, ArrayList<FeedItem> feeds) {
            this.mInflater = LayoutInflater.from(context);
            this.mFeeds = feeds;
        }

        public int getCount() {
            return this.mFeeds.size();
        }

        public Object getItem(int position) {
            return this.mFeeds.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, ?[OBJECT, ARRAY], int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.list_item_feed, (ViewGroup) null, true);
                holder = new ViewHolder(this, null);
                holder.mName = (TextView) convertView.findViewById(R.id.name);
                holder.mCheck = (CheckBox) convertView.findViewById(R.id.check);
                holder.mCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        FeedItem feed = (FeedItem) buttonView.getTag();
                        if (!isChecked) {
                            SettingsActivity.this.removeSelectedFeed(feed);
                        } else if (!SettingsActivity.this.addSelectedFeed(feed)) {
                            buttonView.setChecked(false);
                        }
                    }
                });
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            FeedItem feed = this.mFeeds.get(position);
            holder.mName.setText(feed.getName());
            holder.mCheck.setTag(feed);
            holder.mCheck.setChecked(SettingsActivity.this.isFeedSelected(feed));
            return convertView;
        }
    }
}
