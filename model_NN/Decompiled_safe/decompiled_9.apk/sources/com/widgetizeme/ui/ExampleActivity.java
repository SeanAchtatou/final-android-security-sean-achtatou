package com.widgetizeme.ui;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.widgetizeme.R;

public class ExampleActivity extends Activity {
    public static final String EXTRA_IMAGE_ID = "image_id";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle pSavedInstanceState) {
        super.onCreate(pSavedInstanceState);
        setContentView(R.layout.activity_example);
        int imageId = getIntent().getIntExtra(EXTRA_IMAGE_ID, R.drawable.example1);
        ImageView image = (ImageView) findViewById(R.id.image);
        image.setImageResource(imageId);
        image.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExampleActivity.this.finish();
            }
        });
        TextView title = (TextView) findViewById(R.id.title);
        if (R.drawable.example1 == imageId) {
            title.setText(R.string.guide_1);
            title.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.guide_1_icon), (Drawable) null, (Drawable) null, (Drawable) null);
        } else if (R.drawable.example2 == imageId) {
            title.setText(R.string.guide_2_title);
            title.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.guide_2_logo), (Drawable) null, (Drawable) null, (Drawable) null);
        } else {
            title.setText(R.string.guide_3_title);
            title.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.guide_3_logo), (Drawable) null, (Drawable) null, (Drawable) null);
        }
    }
}
