package com.widgetizeme.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.widgetizeme.Logger;
import com.widgetizeme.Pref;
import com.widgetizeme.R;
import java.util.Locale;

public class AdWallActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle pSavedInstanceState) {
        int idStart;
        int idEnd;
        super.onCreate(pSavedInstanceState);
        setContentView(R.layout.activity_ad_wall);
        WebView webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url == null || !url.startsWith("market://")) {
                    return false;
                }
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse(url));
                AdWallActivity.this.startActivity(intent);
                return true;
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                AdWallActivity.this.findViewById(R.id.progress).setVisibility(0);
            }

            public void onPageFinished(WebView view, String url) {
                AdWallActivity.this.findViewById(R.id.progress).setVisibility(8);
            }
        });
        String wid = Pref.getPrefString("widget_id");
        if (TextUtils.isEmpty(wid)) {
            String pkgName = getPackageName();
            if (pkgName.contains("com.widgetizefb")) {
                wid = "3";
            } else if (pkgName.contains("com.twitto")) {
                wid = "2";
            } else {
                wid = "1";
                int idStart2 = pkgName.indexOf("_id");
                if (!(-1 == idStart2 || -1 == (idEnd = pkgName.indexOf("_", (idStart = idStart2 + 3))))) {
                    wid = pkgName.substring(idStart, idEnd);
                }
            }
        }
        String os = Build.VERSION.RELEASE;
        TelephonyManager tm = (TelephonyManager) getSystemService("phone");
        String country = tm.getNetworkCountryIso().toLowerCase();
        if (TextUtils.isEmpty(country)) {
            country = Locale.getDefault().getCountry();
        }
        if (TextUtils.isEmpty(country)) {
            country = getResources().getConfiguration().locale.getCountry();
        }
        String mac = "";
        try {
            mac = ((WifiManager) getSystemService("wifi")).getConnectionInfo().getMacAddress();
        } catch (Exception e) {
            Logger.l(e);
        }
        String url = "http://widgetize.me/offerwall/appwall.htm?wid=" + wid + "&os_ver=" + os + "&country_code=" + country + "&mac=" + mac + "&imei=" + tm.getDeviceId() + "&android_id=" + Settings.Secure.getString(getContentResolver(), "android_id");
        Logger.l("url=" + url);
        webView.loadUrl(url);
    }
}
