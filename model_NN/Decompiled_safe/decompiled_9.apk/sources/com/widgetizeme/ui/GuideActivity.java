package com.widgetizeme.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import com.widgetizeme.R;
import com.widgetizeme.Strings;

public class GuideActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle pSavedInstanceState) {
        super.onCreate(pSavedInstanceState);
        setContentView(R.layout.activity_guide);
        View close = findViewById(R.id.close);
        if (close != null) {
            close.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    GuideActivity.this.finish();
                }
            });
        }
        String name = Strings.get().getString(R.string.guide_app_name);
        ((TextView) findViewById(R.id.title)).setText(getString(R.string.guide_title).replace("X", name));
        ((TextView) findViewById(R.id.guide_3_title)).setText(getString(R.string.guide_3_title).replace("X", name));
        TextView guide_1 = (TextView) findViewById(R.id.guide_1);
        guide_1.setText(Html.fromHtml("<b>" + getString(R.string.guide_1) + "</b> " + "<a style=\"color: #4dafd8;\" href=\"http://www.google.com\">example</a>"));
        guide_1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GuideActivity.this.startActivity(new Intent(GuideActivity.this, ExampleActivity.class).putExtra(ExampleActivity.EXTRA_IMAGE_ID, R.drawable.example1));
            }
        });
        TextView guide_2_a = (TextView) findViewById(R.id.guide_2_a);
        guide_2_a.setText(Html.fromHtml(String.valueOf(getString(R.string.guide_2_a)) + " " + "<a style=\"color: #4dafd8\" href=\"http://www.google.com\">example</a>"));
        guide_2_a.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GuideActivity.this.startActivity(new Intent(GuideActivity.this, ExampleActivity.class).putExtra(ExampleActivity.EXTRA_IMAGE_ID, R.drawable.example2));
            }
        });
        TextView guide_2_b = (TextView) findViewById(R.id.guide_2_b);
        guide_2_b.setText(Html.fromHtml(String.valueOf(getString(R.string.guide_2_b)) + " " + "<a style=\"color: #4dafd8\" href=\"http://www.google.com\">example</a>"));
        guide_2_b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GuideActivity.this.startActivity(new Intent(GuideActivity.this, ExampleActivity.class).putExtra(ExampleActivity.EXTRA_IMAGE_ID, R.drawable.example2));
            }
        });
        TextView guide_3 = (TextView) findViewById(R.id.guide_3);
        guide_3.setText(Html.fromHtml(String.valueOf(getString(R.string.guide_3)) + " " + "<a style=\"color: #4dafd8\" href=\"http://www.google.com\">example</a>"));
        guide_3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GuideActivity.this.startActivity(new Intent(GuideActivity.this, ExampleActivity.class).putExtra(ExampleActivity.EXTRA_IMAGE_ID, R.drawable.example3));
            }
        });
    }
}
