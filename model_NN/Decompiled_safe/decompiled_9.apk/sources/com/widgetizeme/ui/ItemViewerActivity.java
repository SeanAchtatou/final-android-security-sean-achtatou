package com.widgetizeme.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import com.widgetizeme.App;
import com.widgetizeme.Logger;
import com.widgetizeme.Pref;
import com.widgetizeme.R;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class ItemViewerActivity extends Activity {
    private static final long AD_DURATION = 6000;
    public String mAdURL;
    /* access modifiers changed from: private */
    public Handler mHandler;
    public String mImageURL;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getData() == null) {
            finish();
            return;
        }
        setContentView(R.layout.activity_view);
        this.mHandler = new Handler();
        if (shouldShowAd()) {
            showAd();
        } else {
            openLink(getIntent().getData());
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.mHandler != null) {
            this.mHandler.removeCallbacksAndMessages(null);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    private boolean shouldShowAd() {
        return true;
    }

    private void showAd() {
        if (1 != 0) {
            new AdRequest().start();
        }
        this.mHandler.postDelayed(new Runnable() {
            public void run() {
                ItemViewerActivity.this.openLink(ItemViewerActivity.this.getIntent().getData());
            }
        }, 12000);
    }

    /* access modifiers changed from: private */
    public void openLink(Uri url) {
        if (url != null) {
            ((App) getApplication()).sendClickStats(url.toString());
            try {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(url);
                startActivity(intent);
            } catch (Exception e) {
                Logger.l(e);
            }
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void onShowAdDone(final Bitmap bm) {
        this.mHandler.removeCallbacksAndMessages(null);
        runOnUiThread(new Runnable() {
            public void run() {
                ItemViewerActivity.this.findViewById(R.id.progress).setVisibility(8);
                if (bm != null) {
                    ItemViewerActivity.this.findViewById(R.id.close).setVisibility(0);
                    ItemViewerActivity.this.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            ItemViewerActivity.this.openLink(ItemViewerActivity.this.getIntent().getData());
                        }
                    });
                    ImageView imageView = (ImageView) ItemViewerActivity.this.findViewById(R.id.image);
                    imageView.setImageBitmap(bm);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            ItemViewerActivity.this.openLink(Uri.parse(ItemViewerActivity.this.mAdURL));
                        }
                    });
                    ItemViewerActivity.this.mHandler.postDelayed(new Runnable() {
                        public void run() {
                            ItemViewerActivity.this.openLink(ItemViewerActivity.this.getIntent().getData());
                        }
                    }, ItemViewerActivity.AD_DURATION);
                    return;
                }
                ItemViewerActivity.this.openLink(ItemViewerActivity.this.getIntent().getData());
            }
        });
    }

    private class AdRequest extends Thread {
        public AdRequest() {
        }

        public void run() {
            String deviceId;
            int idStart;
            int idEnd;
            StringBuilder sb = new StringBuilder();
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://ad.taptica.com/aff_ad");
            httppost.addHeader("Content-Type", "application/x-www-form-urlencoded");
            try {
                String android_id = Settings.Secure.getString(ItemViewerActivity.this.getContentResolver(), "android_id");
                String widgetId = Pref.getPrefString("widget_id");
                if (TextUtils.isEmpty(widgetId)) {
                    String pkgName = ItemViewerActivity.this.getPackageName();
                    if (pkgName.contains("com.widgetizefb")) {
                        widgetId = "3";
                    } else if (pkgName.contains("com.twitto")) {
                        widgetId = "2";
                    } else {
                        widgetId = "1";
                        int idStart2 = pkgName.indexOf("_id");
                        if (!(-1 == idStart2 || -1 == (idEnd = pkgName.indexOf("_", (idStart = idStart2 + 3))))) {
                            widgetId = pkgName.substring(idStart, idEnd);
                        }
                    }
                }
                try {
                    deviceId = ((TelephonyManager) ItemViewerActivity.this.getSystemService("phone")).getDeviceId();
                } catch (Exception e) {
                    Logger.l(e);
                }
                String macAddress = "0";
                try {
                    if (((WifiManager) ItemViewerActivity.this.getSystemService("wifi")).getConnectionInfo().getMacAddress() == null) {
                    }
                    macAddress = "0";
                } catch (Exception e2) {
                    Logger.l(e2);
                }
                ArrayList arrayList = new ArrayList();
                arrayList.add(new BasicNameValuePair("t", "3"));
                arrayList.add(new BasicNameValuePair("a", "87efe9c8-6bf1-4560-8a45-34058cfa831a"));
                arrayList.add(new BasicNameValuePair("r", "320x480"));
                arrayList.add(new BasicNameValuePair("u", "Mozilla/5.0 (Linux; U; Android 2.3.4; fr-fr; HTC Desire Build/GRJ22) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"));
                arrayList.add(new BasicNameValuePair("i", "50.57.113.223"));
                arrayList.add(new BasicNameValuePair("pd", widgetId));
                arrayList.add(new BasicNameValuePair("tt_android_id", android_id));
                arrayList.add(new BasicNameValuePair("tt_imei", deviceId));
                arrayList.add(new BasicNameValuePair("tt_mac", macAddress));
                Logger.l("request ad: " + arrayList);
                httppost.setEntity(new UrlEncodedFormEntity(arrayList));
                try {
                    InputStream input = httpclient.execute(httppost).getEntity().getContent();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input));
                    for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
                        sb.append(line);
                    }
                    input.close();
                } catch (Exception e3) {
                    Logger.l(e3);
                }
            } catch (Exception e4) {
                Logger.l(e4);
            }
            String data = sb.toString();
            if (!TextUtils.isEmpty(data)) {
                try {
                    JSONObject json = new JSONObject(data);
                    ItemViewerActivity.this.mImageURL = json.optString("htmlUrl");
                    ItemViewerActivity.this.mAdURL = json.optString("adUrl");
                } catch (Exception e5) {
                    Logger.l(e5);
                }
            }
            Logger.l("------------------");
            Logger.l(data);
            if (!TextUtils.isEmpty(ItemViewerActivity.this.mImageURL)) {
                ItemViewerActivity.this.onShowAdDone(ItemViewerActivity.this.downloadBitmap(ItemViewerActivity.this.mImageURL));
            } else {
                ItemViewerActivity.this.onShowAdDone(null);
            }
        }
    }

    /* access modifiers changed from: private */
    public Bitmap downloadBitmap(String url) {
        Bitmap retVal = null;
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(5000);
            conn.setDoInput(true);
            conn.connect();
            try {
                InputStream is = conn.getInputStream();
                retVal = BitmapFactory.decodeStream(is);
                is.close();
            } catch (Exception e) {
                Logger.l(e);
            }
            conn.disconnect();
        } catch (Exception e2) {
            Logger.l(e2);
        }
        return retVal;
    }
}
