package com.widgetizeme.rss;

import com.widgetizeme.utils.StringUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class RSSItem {
    private static final SimpleDateFormat[] dateFormats = {new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US), new SimpleDateFormat("yyyy-MM-dd", Locale.US)};
    private String mAuthor;
    private String mDesc;
    private String mImageURL;
    private String mLink;
    private long mPubDate;
    private String mTitle;

    public String getTitle() {
        return this.mTitle;
    }

    public String getDesc() {
        return this.mDesc;
    }

    public String getLink() {
        return this.mLink;
    }

    public long getPubDate() {
        return this.mPubDate;
    }

    public String getImageURL() {
        return this.mImageURL;
    }

    public void setTitle(String title) {
        this.mTitle = StringUtils.fromXML(title);
    }

    public void setDesc(String desc) {
        this.mDesc = StringUtils.fromXML(desc);
    }

    public void setLink(String link) {
        this.mLink = StringUtils.fromXML(link);
    }

    public void setPubDate(String pubDate) {
        try {
            this.mPubDate = new Date(pubDate).getTime();
        } catch (Exception e) {
        }
        if (0 == this.mPubDate) {
            int i = 0;
            while (0 == this.mPubDate && i < dateFormats.length) {
                try {
                    this.mPubDate = dateFormats[i].parse(pubDate).getTime();
                } catch (Exception e2) {
                }
                i++;
            }
        }
        if (0 == this.mPubDate) {
            this.mPubDate = System.currentTimeMillis();
        }
    }

    public void setImageURL(String imageURL) {
        this.mImageURL = StringUtils.fromXML(imageURL);
    }

    public void setAuthor(String author) {
        this.mAuthor = StringUtils.fromXML(author);
    }

    public String getAuthor() {
        return this.mAuthor;
    }
}
