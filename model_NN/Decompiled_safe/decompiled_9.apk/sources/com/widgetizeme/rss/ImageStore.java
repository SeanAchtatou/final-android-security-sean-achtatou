package com.widgetizeme.rss;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.text.TextUtils;
import android.view.Display;
import android.view.WindowManager;
import com.widgetizeme.Logger;
import com.widgetizeme.R;
import com.widgetizeme.Strings;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class ImageStore {
    private Context mContext;
    private String mWidgetType = Strings.get().getString(R.string.widget_type);

    public ImageStore(Context context) {
        this.mContext = context;
    }

    public void insert(String imageURL) {
        if (!isExists(imageURL)) {
            try {
                HttpURLConnection conn = (HttpURLConnection) new URL(imageURL).openConnection();
                conn.setConnectTimeout(5000);
                conn.setReadTimeout(5000);
                conn.setDoInput(true);
                conn.connect();
                try {
                    InputStream is = conn.getInputStream();
                    decodeStream(is, imageURL);
                    is.close();
                } catch (Exception e) {
                    Logger.l(e);
                }
                conn.disconnect();
            } catch (Exception e2) {
                Logger.l(e2);
            }
        }
    }

    public Bitmap get(String imageURL) {
        Bitmap retVal = null;
        if (!TextUtils.isEmpty(imageURL)) {
            try {
                InputStream is = openCacheFileInput(imageURL);
                if (is != null) {
                    try {
                        retVal = BitmapFactory.decodeStream(is);
                    } catch (Exception e) {
                        Logger.l(e);
                    }
                    is.close();
                }
            } catch (Exception e2) {
            }
        }
        if (!TextUtils.isEmpty(imageURL) && retVal == null) {
            Logger.l("try loading image again: " + imageURL);
            insertAysnc(imageURL);
        }
        return retVal;
    }

    public void cleanupUnusedCache(ArrayList<String> usedImages) {
        for (String fileName : this.mContext.fileList()) {
            if (fileName.endsWith(".jpg") && !fileName.equals("logo.png")) {
                boolean isUsed = false;
                int i = 0;
                while (!isUsed && i < usedImages.size()) {
                    isUsed = getCachedFileName(usedImages.get(i)).equals(fileName);
                    i++;
                }
                if (!isUsed) {
                    Logger.l("delete old cache image: " + fileName);
                    this.mContext.deleteFile(fileName);
                }
            }
        }
    }

    private boolean isExists(String imageURL) {
        try {
            InputStream is = openCacheFileInput(imageURL);
            if (is == null) {
                return false;
            }
            is.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void insertAysnc(final String imageURL) {
        new Thread(new Runnable() {
            public void run() {
                ImageStore.this.insert(imageURL);
            }
        }).start();
    }

    private Bitmap decodeStream(InputStream is, String url) {
        Bitmap retVal = null;
        try {
            retVal = BitmapFactory.decodeStream(is);
            if (retVal != null) {
                int dstWidth = 70;
                if (this.mWidgetType.equals("flip") || this.mWidgetType.equals("flat")) {
                    dstWidth = (int) (((float) getScreenSize(this.mContext).x) * 0.7f);
                }
                Bitmap scaledBitmap = Bitmap.createScaledBitmap(retVal, dstWidth, (retVal.getHeight() * dstWidth) / retVal.getWidth(), false);
                if (scaledBitmap != null) {
                    retVal.recycle();
                    retVal = scaledBitmap;
                }
                try {
                    FileOutputStream output = openCacheFileOutput(url);
                    retVal.compress(Bitmap.CompressFormat.JPEG, 100, output);
                    output.close();
                } catch (Exception e) {
                    Logger.l(e);
                }
            }
        } catch (Exception e2) {
            Logger.l(e2);
        }
        return retVal;
    }

    private String getCachedFileName(String imageURL) {
        if (imageURL.endsWith("logo.png")) {
            return "logo.png";
        }
        return "img_" + imageURL.hashCode() + ".jpg";
    }

    private FileOutputStream openCacheFileOutput(String url) throws FileNotFoundException {
        return this.mContext.openFileOutput(getCachedFileName(url), 0);
    }

    private FileInputStream openCacheFileInput(String url) throws FileNotFoundException {
        return this.mContext.openFileInput(getCachedFileName(url));
    }

    public static Point getScreenSize(Context context) {
        Point retVal = new Point();
        Display display = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        try {
            Display.class.getMethod("getSize", Point.class).invoke(display, retVal);
            return retVal;
        } catch (Exception e) {
            return new Point(display.getWidth(), display.getHeight());
        }
    }
}
