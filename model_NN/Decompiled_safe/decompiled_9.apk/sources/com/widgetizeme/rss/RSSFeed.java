package com.widgetizeme.rss;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import com.google.android.gms.plus.PlusShare;
import com.widgetizeme.Logger;
import com.widgetizeme.Pref;
import com.widgetizeme.R;
import com.widgetizeme.Strings;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class RSSFeed {
    private Context mContext;
    private ArrayList<RSSItem> mItems;
    private long mPubDate;
    private String mTitle;
    private String mURL;

    public RSSFeed(Context context, String url) {
        this.mContext = context;
        this.mURL = url;
        loadFromCache();
    }

    public void refresh() {
        try {
            download();
            loadFromCache();
        } catch (Exception e) {
            Logger.l(e);
        }
    }

    public String getTitle() {
        return this.mTitle;
    }

    public String getURL() {
        return this.mURL;
    }

    public long getPubDate() {
        return this.mPubDate;
    }

    public ArrayList<RSSItem> getItems() {
        return this.mItems;
    }

    public boolean isEmpty() {
        return this.mItems.isEmpty();
    }

    public boolean isRTL() {
        boolean retVal = false;
        int i = 0;
        while (!retVal && i < this.mItems.size()) {
            RSSItem item = this.mItems.get(i);
            retVal = isRTLString(item.getTitle());
            if (!retVal) {
                retVal = isRTLString(item.getDesc());
            }
            i++;
        }
        return retVal;
    }

    /* access modifiers changed from: protected */
    public void loadFromCache() {
        this.mItems = new ArrayList<>();
        try {
            FileInputStream input = this.mContext.openFileInput(getCacheFileName());
            try {
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser xpp = factory.newPullParser();
                xpp.setInput(input, Pref.getPrefString(Pref.FEED_ENCODING + getURL()));
                parseXML(xpp);
            } catch (Exception e) {
                Logger.l(e);
            }
            input.close();
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public Context getContext() {
        return this.mContext;
    }

    /* access modifiers changed from: protected */
    public void saveToFile(String data, String encoding) {
        try {
            FileOutputStream output = this.mContext.openFileOutput(getCacheFileName(), 0);
            try {
                Pref.setPrefString(Pref.FEED_ENCODING + getURL(), encoding);
                output.write(data.getBytes());
            } catch (Exception e) {
                Logger.l(e);
            }
            output.close();
        } catch (Exception e2) {
            Logger.l(e2);
        }
    }

    private void download() {
        Logger.l("download feed: " + getURL());
        try {
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
            HttpConnectionParams.setSoTimeout(httpParameters, 5000);
            HttpResponse response = new DefaultHttpClient(httpParameters).execute(new HttpGet(getURL()));
            if (200 != response.getStatusLine().getStatusCode()) {
                Logger.l("download feed(" + getURL() + ") failed: " + response.getStatusLine().getReasonPhrase());
            } else {
                Logger.l("save feed: " + getURL());
                saveToFile(response.getEntity().getContent());
            }
            response.getEntity().consumeContent();
        } catch (Exception e) {
            Logger.l(e);
        }
    }

    private void saveToFile(InputStream is) {
        try {
            FileOutputStream output = this.mContext.openFileOutput(getCacheFileName(), 0);
            try {
                byte[] buffer = new byte[1024];
                int bytesRead = is.read(buffer);
                output.write(buffer, 0, bytesRead);
                Pref.setPrefString(Pref.FEED_ENCODING + getURL(), getEncodingFromFeed(buffer, bytesRead));
                int bytesRead2 = is.read(buffer);
                while (bytesRead2 > 0) {
                    output.write(buffer, 0, bytesRead2);
                    bytesRead2 = is.read(buffer);
                }
            } catch (Exception e) {
                Logger.l(e);
            }
            output.close();
        } catch (Exception e2) {
            Logger.l(e2);
        }
    }

    private String getEncodingFromFeed(byte[] buffer, int bytesRead) {
        int startIndex;
        int startIndex2;
        int endIndex;
        String bufString = new String(buffer);
        int startIndex3 = bufString.indexOf("encoding=");
        if (-1 == startIndex3 || -1 == (endIndex = bufString.indexOf(bufString.charAt((startIndex = startIndex3 + "encoding=".length())), (startIndex2 = startIndex + 1)))) {
            return "UTF-8";
        }
        return bufString.substring(startIndex2, endIndex);
    }

    private String getCacheFileName() {
        return ("rss_" + getURL().hashCode() + ".xml").replace("-", "_");
    }

    private void parseXML(XmlPullParser xpp) {
        RSSItem item;
        while (1 != xpp.getEventType()) {
            try {
                String tag = xpp.getName();
                if (2 == xpp.getEventType()) {
                    if (tag.equals(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)) {
                        xpp.next();
                        this.mTitle = xpp.getText();
                    } else if (tag.equals("pubDate")) {
                        xpp.next();
                        try {
                            this.mPubDate = new Date(xpp.getText()).getTime();
                        } catch (Exception e) {
                            Logger.l(e);
                        }
                        if (0 == this.mPubDate) {
                            this.mPubDate = System.currentTimeMillis();
                        }
                    } else if (tag.equals("item") && (item = parseItem(xpp)) != null) {
                        this.mItems.add(item);
                    }
                }
                xpp.next();
            } catch (Exception e2) {
                Logger.l(e2);
                return;
            }
        }
    }

    private RSSItem parseItem(XmlPullParser xpp) {
        RSSItem retVal = new RSSItem();
        retVal.setAuthor(Strings.get().getString(R.string.brand_name));
        String mEnclosureLink = null;
        boolean done = false;
        while (!done) {
            try {
                if (1 == xpp.getEventType()) {
                    break;
                }
                String tag = xpp.getName();
                if (2 == xpp.getEventType()) {
                    if (tag.equals(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)) {
                        xpp.next();
                        String title = xpp.getText();
                        if (TextUtils.isEmpty(retVal.getTitle())) {
                            retVal.setTitle(title);
                        }
                    } else if (tag.equals("pubDate") || tag.equals("dc:date") || tag.equals("date")) {
                        xpp.next();
                        retVal.setPubDate(xpp.getText());
                    } else if (tag.equals(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION)) {
                        xpp.next();
                        String desc = xpp.getText();
                        if (TextUtils.isEmpty(retVal.getDesc())) {
                            retVal.setDesc(cleanupDesc(desc));
                            if (TextUtils.isEmpty(retVal.getImageURL())) {
                                retVal.setImageURL(parseImageFromDesc(desc));
                            }
                        }
                    } else if (tag.equals("enclosure")) {
                        mEnclosureLink = xpp.getAttributeValue(null, PlusShare.KEY_CALL_TO_ACTION_URL);
                    } else if (tag.equals("thumbnail")) {
                        if (TextUtils.isEmpty(retVal.getImageURL())) {
                            retVal.setImageURL(xpp.getAttributeValue(null, PlusShare.KEY_CALL_TO_ACTION_URL));
                        }
                    } else if (tag.equals("link")) {
                        xpp.next();
                        String link = xpp.getText();
                        if (TextUtils.isEmpty(retVal.getLink())) {
                            retVal.setLink(link);
                        }
                    } else if (tag.equals("author") || tag.equals("dc:creator") || tag.equals("creator")) {
                        xpp.next();
                        String author = xpp.getText();
                        if (!TextUtils.isEmpty(author)) {
                            retVal.setAuthor(author);
                        }
                    } else if (tag.equals("encoded") || tag.equals("content:encoded")) {
                        xpp.next();
                        String text = xpp.getText();
                        if (TextUtils.isEmpty(retVal.getImageURL())) {
                            retVal.setImageURL(parseImageFromDesc(text));
                        }
                    }
                } else if (3 == xpp.getEventType()) {
                    done = xpp.getName().equals("item");
                }
                if (!done) {
                    xpp.next();
                }
            } catch (Exception e) {
                Logger.l(e);
                return null;
            }
        }
        if (!TextUtils.isEmpty(retVal.getLink())) {
            return retVal;
        }
        retVal.setLink(mEnclosureLink);
        return retVal;
    }

    private String parseImageFromDesc(String desc) {
        int startIndex;
        int startIndex2;
        int startIndex3;
        int endIndex;
        String retVal = null;
        if (!(TextUtils.isEmpty(desc) || -1 == (startIndex = desc.indexOf("img src=")) || -1 == (endIndex = desc.indexOf(desc.charAt((startIndex2 = startIndex + "img src=".length())), (startIndex3 = startIndex2 + 1))))) {
            String url = desc.substring(startIndex3, endIndex);
            if (url.endsWith(".png") || url.endsWith(".jpg") || url.endsWith(".jpeg") || url.endsWith(".gif")) {
                retVal = url.replace(" ", "%20");
            }
        }
        if (TextUtils.isEmpty(retVal)) {
            return parseImageFromDescFallback(desc);
        }
        return retVal;
    }

    private String parseImageFromDescFallback(String desc) {
        int startIndex;
        int startIndex2;
        int startIndex3;
        int endIndex;
        if (TextUtils.isEmpty(desc) || -1 == (startIndex = desc.indexOf("src=")) || -1 == (endIndex = desc.indexOf(desc.charAt((startIndex2 = startIndex + "src=".length())), (startIndex3 = startIndex2 + 1)))) {
            return null;
        }
        String url = desc.substring(startIndex3, endIndex);
        if (url.contains(".png") || url.contains(".jpg") || url.contains(".jpeg") || url.contains(".gif")) {
            return url.replace(" ", "%20");
        }
        return null;
    }

    private String cleanupDesc(String desc) {
        int endIndex;
        if (TextUtils.isEmpty(desc)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        int startIndex = desc.indexOf(62);
        while (-1 != startIndex) {
            int startIndex2 = startIndex + 1;
            if (startIndex2 < desc.length() && -1 != (endIndex = desc.indexOf(60, startIndex2))) {
                sb.append(desc.substring(startIndex2, endIndex).trim());
                startIndex2 = endIndex;
            }
            int startIndex3 = startIndex2 + 1;
            if (startIndex3 < desc.length()) {
                startIndex = desc.indexOf(62, startIndex3);
            } else {
                startIndex = -1;
            }
        }
        if (sb.length() > 0) {
            desc = sb.toString();
        }
        return Html.fromHtml(desc).toString().trim().replace("￼", "").trim();
    }

    private boolean isRTLString(String str) {
        if (str == null) {
            return false;
        }
        for (int i = 0; i < str.length(); i++) {
            if (isRTLChar(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    private boolean isRTLChar(char ch) {
        return ch >= 1488 && ch <= 1514;
    }
}
