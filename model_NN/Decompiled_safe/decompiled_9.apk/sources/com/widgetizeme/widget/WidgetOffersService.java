package com.widgetizeme.widget;

import android.content.Intent;
import android.widget.RemoteViewsService;

public class WidgetOffersService extends RemoteViewsService {
    public RemoteViewsService.RemoteViewsFactory onGetViewFactory(Intent arg0) {
        return new WMRemoteViewsOffersFactory(getApplicationContext(), arg0);
    }
}
