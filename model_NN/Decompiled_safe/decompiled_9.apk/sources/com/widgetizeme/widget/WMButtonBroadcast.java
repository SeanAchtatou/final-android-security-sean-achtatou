package com.widgetizeme.widget;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.widgetizeme.App;
import com.widgetizeme.Logger;
import com.widgetizeme.Pref;
import com.widgetizeme.R;
import com.widgetizeme.Strings;
import com.widgetizeme.ui.AdWallActivity;
import com.widgetizeme.ui.ItemViewerActivity;

public class WMButtonBroadcast extends BroadcastReceiver {
    public static final int ACTION_AD_WALL = 11;
    public static final int ACTION_CHANGE_CATEGORY = 10;
    public static final int ACTION_ITEM = 3;
    public static final int ACTION_LOGO = 7;
    public static final int ACTION_NEXT = 5;
    public static final int ACTION_NO_ACTION = 9;
    public static final int ACTION_OFFERS = 6;
    public static final int ACTION_PREV = 4;
    public static final int ACTION_RECOMMENDATIONS = 8;
    public static final int ACTION_REFRESH = 0;
    public static final int ACTION_SHARE = 1;
    public static final int ACTION_WIDGETIZEME = 2;
    public static final String EXTRA_ACTION = "action";
    public static final String EXTRA_CATEGORY_INDEX = "category_index";
    public static final String EXTRA_INDEX_NAME = "index_name";
    public static final String EXTRA_WIDGET_ID = "widget_id";

    public void onReceive(Context context, Intent intent) {
        int action = intent.getIntExtra(EXTRA_ACTION, -1);
        int widgetId = intent.getIntExtra("widget_id", 0);
        String indexPref = intent.getStringExtra(EXTRA_INDEX_NAME);
        switch (action) {
            case 0:
                onRefresh(context, widgetId);
                break;
            case 1:
                onShare(context);
                break;
            case 2:
                onWidgetizeMe(context);
                break;
            case 3:
                onItem(context, intent);
                break;
            case 4:
                onPrev(context, widgetId, indexPref);
                break;
            case 5:
                onNext(context, widgetId, indexPref);
                break;
            case 6:
                onOffers(context, widgetId);
                break;
            case 7:
                onLogo(context);
                break;
            case 8:
                onRecommendations(context, widgetId);
                break;
            case 10:
                onChangeCategory(context, widgetId, intent);
                break;
            case 11:
                onAddWall(context);
                break;
        }
        Logger.l("WMButtonBroadcast::onReceive action=" + action);
    }

    private void onRefresh(Context context, int widgetId) {
        Logger.l("onRefresh: widgetId-" + widgetId);
        Pref.setPrefInt(Pref.START_INDEX + widgetId, 0);
        Intent updateIntent = new Intent(context, UpdateService.class);
        updateIntent.putExtra("widget_id", widgetId);
        context.startService(updateIntent);
    }

    private void onShare(Context context) {
        ((App) context.getApplicationContext()).sendStats(2);
        try {
            Intent shareIntent = new Intent();
            shareIntent.setAction("android.intent.action.SEND");
            shareIntent.putExtra("android.intent.extra.TEXT", "http://widgetize.me/Get/Share/?playStoreId=" + context.getPackageName());
            shareIntent.setType("text/plain");
            Intent intent = Intent.createChooser(shareIntent, context.getString(R.string.share_with));
            intent.addFlags(8388608);
            intent.addFlags(268435456);
            context.startActivity(intent);
        } catch (Exception e) {
            Logger.l(e);
        }
    }

    private void onOffers(Context context, int widgetId) {
        App app = (App) context.getApplicationContext();
        if (app.isInOffersMode()) {
            app.setOffersMode(false);
        } else {
            app.setOffersMode(true);
        }
        Intent intent = new Intent(context, WMWidgetProvider.class);
        intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
        intent.putExtra("appWidgetIds", new int[]{widgetId});
        context.sendBroadcast(intent);
    }

    private void onWidgetizeMe(Context context) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse("http://widgetize.me/"));
            intent.addFlags(8388608);
            intent.addFlags(268435456);
            context.startActivity(intent);
        } catch (Exception e) {
            Logger.l(e);
        }
    }

    private void onLogo(Context context) {
        String url = context.getString(R.string.logo_link);
        if (!TextUtils.isEmpty(url)) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse(url));
                intent.addFlags(8388608);
                intent.addFlags(268435456);
                context.startActivity(intent);
            } catch (Exception e) {
                Logger.l(e);
            }
        }
    }

    private void onRecommendations(Context context, int widgetId) {
        App app = (App) context.getApplicationContext();
        if (app.isInRecommendationMode(widgetId)) {
            app.setRecommendationMode(widgetId, false);
        } else {
            app.setRecommendationMode(widgetId, true);
        }
        Intent intent = new Intent(context, WMWidgetProvider.class);
        intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
        intent.putExtra("appWidgetIds", new int[]{widgetId});
        context.sendBroadcast(intent);
    }

    private void onItem(Context context, Intent intent) {
        Logger.l("WMButtonBroadcast::onItem data=" + intent.getDataString());
        try {
            Intent viewIntent = new Intent(context, ItemViewerActivity.class);
            viewIntent.setData(intent.getData());
            viewIntent.addFlags(8388608);
            viewIntent.addFlags(268435456);
            context.startActivity(viewIntent);
        } catch (Exception e) {
            Logger.l(e);
        }
    }

    private void onNext(Context context, int widgetId, String indexPref) {
        if (Strings.get().getString(R.string.widget_type).equals("flat")) {
            Pref.setPrefInt(Pref.START_INDEX + widgetId, Pref.getPrefInt(Pref.START_INDEX + widgetId) + 1);
        } else {
            String prefName = String.valueOf(indexPref) + widgetId;
            Pref.setPrefInt(prefName, Pref.getPrefInt(prefName) + 3);
        }
        AppWidgetManager.getInstance(context).updateAppWidget(widgetId, WMWidgetProvider.createRemoteViews(context, widgetId));
    }

    private void onPrev(Context context, int widgetId, String indexPref) {
        if (Strings.get().getString(R.string.widget_type).equals("flat")) {
            Pref.setPrefInt(Pref.START_INDEX + widgetId, Pref.getPrefInt(Pref.START_INDEX + widgetId) - 1);
        } else {
            String prefName = String.valueOf(indexPref) + widgetId;
            Pref.setPrefInt(prefName, Pref.getPrefInt(prefName) - 3);
        }
        AppWidgetManager.getInstance(context).updateAppWidget(widgetId, WMWidgetProvider.createRemoteViews(context, widgetId));
    }

    private void onChangeCategory(Context context, int widgetId, Intent recevicedIntent) {
        ((App) context.getApplicationContext()).setCurrCategory(widgetId, recevicedIntent.getIntExtra(EXTRA_CATEGORY_INDEX, 0));
        Intent intent = new Intent(context, WMWidgetProvider.class);
        intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
        intent.putExtra("appWidgetIds", new int[]{widgetId});
        context.sendBroadcast(intent);
    }

    private void onAddWall(Context context) {
        Intent intent = new Intent(context, AdWallActivity.class);
        intent.addFlags(268435456);
        context.startActivity(intent);
    }
}
