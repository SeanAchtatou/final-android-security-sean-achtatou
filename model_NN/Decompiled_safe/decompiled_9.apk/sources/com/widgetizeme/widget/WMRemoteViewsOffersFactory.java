package com.widgetizeme.widget;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import com.widgetizeme.Logger;
import com.widgetizeme.OfferData;
import com.widgetizeme.R;

public class WMRemoteViewsOffersFactory implements RemoteViewsService.RemoteViewsFactory {
    private Context mContext;
    private OfferData mOffers = new OfferData();

    public WMRemoteViewsOffersFactory(Context context, Intent intent) {
        this.mContext = context;
    }

    public void onCreate() {
    }

    public void onDestroy() {
    }

    public void onDataSetChanged() {
        Logger.l("WMRemoteViewsFactory::onDataSetChanged");
    }

    public boolean hasStableIds() {
        return false;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public int getCount() {
        return this.mOffers.getCount();
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public RemoteViews getLoadingView() {
        return null;
    }

    public RemoteViews getViewAt(int position) {
        RemoteViews rv = new RemoteViews(this.mContext.getPackageName(), R.layout.offer_item);
        OfferData.OfferDetails item = this.mOffers.getOffer(position);
        rv.setTextViewText(R.id.title, item.mName);
        rv.setTextViewText(R.id.desc, item.mDesc);
        try {
            Intent fillInIntent = new Intent("android.intent.action.VIEW");
            fillInIntent.setData(Uri.parse("market://details?id=" + item.mPackageName));
            rv.setOnClickFillInIntent(R.id.item, fillInIntent);
        } catch (Exception e) {
            Logger.l(e);
        }
        rv.setImageViewResource(R.id.icon, item.mIcon);
        return rv;
    }
}
