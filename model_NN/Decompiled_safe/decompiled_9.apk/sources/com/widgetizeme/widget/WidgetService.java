package com.widgetizeme.widget;

import android.content.Intent;
import android.widget.RemoteViewsService;

public class WidgetService extends RemoteViewsService {
    public RemoteViewsService.RemoteViewsFactory onGetViewFactory(Intent arg0) {
        return new WMRemoteViewsFactory(getApplicationContext(), arg0);
    }
}
