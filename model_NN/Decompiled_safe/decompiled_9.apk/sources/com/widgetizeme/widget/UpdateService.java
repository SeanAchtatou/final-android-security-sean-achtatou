package com.widgetizeme.widget;

import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.widget.RemoteViews;
import com.widgetizeme.App;
import com.widgetizeme.Logger;
import com.widgetizeme.Pref;
import com.widgetizeme.R;
import com.widgetizeme.Strings;
import com.widgetizeme.rss.ImageStore;
import com.widgetizeme.rss.RSSFeed;
import com.widgetizeme.rss.RSSItem;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

public class UpdateService extends IntentService {
    public static final String EXTRA_WIDGET_ID = "widget_id";

    public UpdateService() {
        super("UpdateService");
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        Logger.l("UpdateService::onHandleIntent");
        int widgetId = intent.getIntExtra("widget_id", 0);
        Logger.l("UpdateService::onHandleIntent update widget: " + widgetId);
        if (Pref.getPrefBoolean(Pref.RESEND_GCM_TOKEN)) {
            Logger.l("resend token");
            Pref.setPrefBoolean(Pref.RESEND_GCM_TOKEN, false);
            new Thread(new Runnable() {
                public void run() {
                    if (!((App) UpdateService.this.getApplicationContext()).sendGCMToken(Pref.getPrefString(Pref.GCM_REG_ID))) {
                        Pref.setPrefBoolean(Pref.RESEND_GCM_TOKEN, true);
                    }
                }
            }).start();
        }
        updateShareText();
        App app = (App) getApplicationContext();
        app.updateWidgetUpdateState(widgetId, true);
        ImageStore imageStore = app.getImageStore();
        AppWidgetManager awm = AppWidgetManager.getInstance(this);
        if (Build.VERSION.SDK_INT >= 11) {
            awm.notifyAppWidgetViewDataChanged(widgetId, R.id.list);
        }
        awm.updateAppWidget(widgetId, WMWidgetProvider.createRemoteViews(this, widgetId));
        try {
            Logger.l("UpdateService start update");
            ArrayList<String> usedImages = new ArrayList<>();
            ArrayList<RSSFeed> feeds = new ArrayList<>();
            if (app.isInRecommendationMode(widgetId)) {
                feeds.add(new RSSFeed(app, Strings.get().getString(R.string.recommendation_feed)));
            } else if (getString(R.string.widget_type).equals("wr")) {
                feeds.add(((App) getApplicationContext()).getFeeds().get(app.getCurrCategory(widgetId)));
            } else {
                feeds.addAll(((App) getApplicationContext()).getFeeds());
            }
            Iterator it = feeds.iterator();
            while (it.hasNext()) {
                RSSFeed feed = (RSSFeed) it.next();
                Logger.l("refresh: " + feed.getURL());
                feed.refresh();
                Logger.l("refresh feed done");
                Iterator<RSSItem> it2 = feed.getItems().iterator();
                while (it2.hasNext()) {
                    String imageURL = it2.next().getImageURL();
                    if (!TextUtils.isEmpty(imageURL)) {
                        usedImages.add(imageURL);
                        imageStore.insert(imageURL);
                    }
                }
            }
            imageStore.cleanupUnusedCache(usedImages);
            Logger.l("UpdateService done update");
        } catch (Exception e) {
            Logger.l(e);
        }
        app.updateWidgetUpdateState(widgetId, false);
        Logger.l("UpdateService::onHandleIntent update widget: " + widgetId);
        if (Build.VERSION.SDK_INT >= 11) {
            awm.notifyAppWidgetViewDataChanged(widgetId, R.id.list);
        }
        RemoteViews rv = WMWidgetProvider.createRemoteViews(this, widgetId);
        if (rv == null) {
            Logger.l("error");
        }
        awm.updateAppWidget(widgetId, rv);
    }

    private void updateShareText() {
        new Thread(new Runnable() {
            public void run() {
                String urlString = UpdateService.this.getString(R.string.share_update_url);
                if (!TextUtils.isEmpty(urlString)) {
                    try {
                        HttpURLConnection conn = (HttpURLConnection) new URL(urlString).openConnection();
                        conn.setConnectTimeout(5000);
                        conn.setReadTimeout(5000);
                        conn.setDoInput(true);
                        conn.connect();
                        try {
                            InputStream is = conn.getInputStream();
                            StringBuilder sb = new StringBuilder();
                            try {
                                BufferedReader rd = new BufferedReader(new InputStreamReader(is, "utf-8"));
                                while (true) {
                                    String line = rd.readLine();
                                    if (line == null) {
                                        break;
                                    }
                                    sb.append(line);
                                }
                                rd.close();
                            } catch (Exception e) {
                                Logger.l(e);
                            }
                            String result = sb.toString();
                            if (!TextUtils.isEmpty(result)) {
                                Pref.setPrefString(Pref.SHARE_TEXT, result);
                            }
                            is.close();
                        } catch (Exception e2) {
                            Logger.l(e2);
                        }
                        conn.disconnect();
                    } catch (Exception e3) {
                        Logger.l(e3);
                    }
                }
            }
        }).start();
    }
}
