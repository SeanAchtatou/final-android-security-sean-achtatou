package com.widgetizeme.widget;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import com.widgetizeme.App;
import com.widgetizeme.Logger;
import com.widgetizeme.R;
import com.widgetizeme.Strings;
import com.widgetizeme.rss.ImageStore;
import com.widgetizeme.rss.RSSFeed;
import com.widgetizeme.rss.RSSItem;
import com.widgetizeme.utils.StringUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class WMRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
    private int mAppWidgetId;
    private Context mContext;
    private ImageStore mImageStore;
    private boolean mIncludeImages;
    private boolean mIsRTL;
    private ArrayList<RSSItem> mItems;
    private int mLayoutId;

    public WMRemoteViewsFactory(Context context, Intent intent) {
        this.mContext = context;
        this.mImageStore = ((App) context.getApplicationContext()).getImageStore();
        this.mAppWidgetId = intent.getIntExtra("appWidgetId", 0);
    }

    public void onCreate() {
        this.mLayoutId = R.layout.widget_item;
        if (Strings.get().getString(R.string.widget_type).equals("wr")) {
            this.mLayoutId = R.layout.widget_item_wr;
        }
        this.mIncludeImages = Strings.get().getString(R.string.layout_type).equals("text+image");
    }

    public void onDestroy() {
    }

    public void onDataSetChanged() {
        Logger.l("WMRemoteViewsFactory::onDataSetChanged");
        this.mItems = new ArrayList<>();
        this.mIsRTL = false;
        boolean forceLTR = false;
        ArrayList<RSSFeed> feeds = new ArrayList<>();
        if (((App) this.mContext.getApplicationContext()).isInRecommendationMode(this.mAppWidgetId)) {
            feeds.add(new RSSFeed(this.mContext, Strings.get().getString(R.string.recommendation_feed)));
        } else {
            ArrayList<RSSFeed> myFeeds = ((App) this.mContext.getApplicationContext()).getFeeds();
            if (!myFeeds.isEmpty()) {
                feeds.add(myFeeds.get(((App) this.mContext.getApplicationContext()).getCurrCategory(this.mAppWidgetId)));
            } else {
                return;
            }
        }
        Iterator it = feeds.iterator();
        while (it.hasNext()) {
            RSSFeed feed = (RSSFeed) it.next();
            if (this.mIsRTL) {
                if (!feed.isRTL()) {
                    this.mIsRTL = false;
                    forceLTR = true;
                }
            } else if (!forceLTR) {
                this.mIsRTL = feed.isRTL();
            }
            this.mItems.addAll(feed.getItems());
        }
        Collections.sort(this.mItems, new Comparator<RSSItem>() {
            public int compare(RSSItem lhs, RSSItem rhs) {
                if (lhs.getPubDate() < rhs.getPubDate()) {
                    return 1;
                }
                if (lhs.getPubDate() > rhs.getPubDate()) {
                    return -1;
                }
                return 0;
            }
        });
        if (Strings.get().getString(R.string.widget_type).equals("wr")) {
            if (this.mIsRTL) {
                this.mLayoutId = R.layout.widget_item_rtl_wr;
            } else {
                this.mLayoutId = R.layout.widget_item_wr;
            }
        } else if (this.mIsRTL) {
            this.mLayoutId = R.layout.widget_item_rtl;
        } else {
            this.mLayoutId = R.layout.widget_item;
        }
    }

    public boolean hasStableIds() {
        return false;
    }

    public int getViewTypeCount() {
        return 2;
    }

    public int getCount() {
        if (((App) this.mContext.getApplicationContext()).isWidgetUpdating(this.mAppWidgetId) || this.mItems == null) {
            return 0;
        }
        return this.mItems.size();
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public RemoteViews getLoadingView() {
        return null;
    }

    public RemoteViews getViewAt(int position) {
        RemoteViews rv = new RemoteViews(this.mContext.getPackageName(), this.mLayoutId);
        RSSItem item = this.mItems.get(position);
        String link = item.getLink();
        rv.setTextViewText(R.id.title, item.getTitle());
        rv.setTextViewText(R.id.time, StringUtils.getTimeString(this.mContext, item.getPubDate(), this.mIsRTL));
        rv.setTextViewText(R.id.desc, item.getDesc());
        if (!TextUtils.isEmpty(item.getAuthor())) {
            rv.setViewVisibility(R.id.by_label, 0);
            rv.setViewVisibility(R.id.by, 0);
            rv.setViewVisibility(R.id.clock, 0);
            rv.setTextViewText(R.id.by, item.getAuthor());
        } else {
            rv.setViewVisibility(R.id.by_label, 8);
            rv.setViewVisibility(R.id.by, 8);
            rv.setViewVisibility(R.id.clock, 8);
        }
        if (TextUtils.isEmpty(item.getTitle()) || !TextUtils.isEmpty(item.getDesc())) {
            rv.setInt(R.id.title, "setMaxLines", 1);
            rv.setViewVisibility(R.id.desc, 0);
        } else {
            rv.setInt(R.id.title, "setMaxLines", 2);
            rv.setViewVisibility(R.id.desc, 8);
        }
        try {
            Intent fillInIntent = new Intent();
            fillInIntent.setData(Uri.parse(item.getLink()));
            rv.setOnClickFillInIntent(R.id.item, fillInIntent);
        } catch (Exception e) {
            Logger.l(e);
        }
        if (this.mIncludeImages) {
            rv.setImageViewBitmap(R.id.image, this.mImageStore.get(item.getImageURL()));
        } else {
            rv.setViewVisibility(R.id.image, 8);
        }
        if (!TextUtils.isEmpty(item.getImageURL())) {
            rv.setViewVisibility(R.id.image, 0);
        }
        return rv;
    }
}
