package com.widgetizeme.widget;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class WMAutoRefreshReceiver extends BroadcastReceiver {
    public static final String EXTRA_WIDGET_ID = "widget_id";

    public void onReceive(Context context, Intent intent) {
        int widgetId = intent.getIntExtra("widget_id", 0);
        AppWidgetManager.getInstance(context).updateAppWidget(widgetId, WMWidgetProvider.createRemoteViews(context, widgetId));
    }
}
