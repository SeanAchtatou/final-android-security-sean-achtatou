package com.widgetizeme.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.widget.RemoteViews;
import com.widgetizeme.App;
import com.widgetizeme.Colors;
import com.widgetizeme.Logger;
import com.widgetizeme.OfferData;
import com.widgetizeme.Pref;
import com.widgetizeme.R;
import com.widgetizeme.Strings;
import com.widgetizeme.UnsentStats;
import com.widgetizeme.rss.ImageStore;
import com.widgetizeme.rss.RSSFeed;
import com.widgetizeme.rss.RSSItem;
import com.widgetizeme.utils.StringUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;

public class WMWidgetProvider extends AppWidgetProvider {
    private static final long AUTO_REFRESH_INTERVAL = 10000;
    private static final int ITEMS_IN_PAGE = 2;
    private static HashSet<Integer> sCreatedWidgetsIds = new HashSet<>();

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        App.sendInstallStat(context);
        pingServer(context);
        for (int i = 0; i < appWidgetIds.length; i++) {
            Logger.l("WMWidgetProvider::onUpdate(" + appWidgetIds[i] + ")");
            appWidgetManager.updateAppWidget(appWidgetIds[i], createRemoteViews(context, appWidgetIds[i]));
            Intent intent = new Intent(context, UpdateService.class);
            intent.putExtra("widget_id", appWidgetIds[i]);
            context.startService(intent);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    public static RemoteViews createRemoteViews(Context context, int widgetId) {
        String widgetType = Strings.get().getString(R.string.widget_type);
        if (widgetType.equals("mini") || widgetType.equals("flip")) {
            return createRemoteViewsAutoRefresh(context, widgetId, widgetType);
        }
        if (widgetType.equals("flat")) {
            return createRemoteViewsFlat(context, widgetId);
        }
        if (isLegacy()) {
            return createRemoteViewsLegacy(context, widgetId);
        }
        return createRemoteViewsICS(context, widgetId);
    }

    private static boolean isLegacy() {
        return Build.VERSION.SDK_INT < 11;
    }

    private static RemoteViews createRemoteViewsICS(Context context, int widgetId) {
        Intent intent = new Intent(context, WidgetService.class);
        if (((App) context.getApplicationContext()).isInOffersMode()) {
            intent = new Intent(context, WidgetOffersService.class);
        }
        intent.putExtra("appWidgetId", widgetId);
        intent.setData(Uri.parse(intent.toUri(1)));
        int widgetLayoutId = R.layout.widget;
        String widgetType = Strings.get().getString(R.string.widget_type);
        if (widgetType.equals("wr")) {
            widgetLayoutId = R.layout.widget_wr;
        }
        RemoteViews rv = new RemoteViews(context.getPackageName(), widgetLayoutId);
        rv.setRemoteAdapter(widgetId, R.id.list, intent);
        rv.setEmptyView(R.id.list, R.id.empty_view);
        Intent itemIntent = new Intent(context, WMButtonBroadcast.class);
        itemIntent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 3);
        itemIntent.putExtra("widget_id", widgetId);
        intent.setData(Uri.parse(intent.toUri(1)));
        rv.setPendingIntentTemplate(R.id.list, PendingIntent.getBroadcast(context, R.id.list, itemIntent, 134217728));
        if (((App) context.getApplicationContext()).isInOffersMode()) {
            rv.setTextViewText(R.id.date, getDateString(context, Pref.getPrefLong("last_update_" + widgetId)));
        } else {
            initCategories(context, rv, widgetId);
            if (((App) context.getApplicationContext()).isWidgetUpdating(widgetId)) {
                rv.setTextViewText(R.id.date, context.getString(R.string.updating));
            } else {
                rv.setTextViewText(R.id.date, getDateString(context, Pref.getPrefLong("last_update_" + widgetId)));
                initRefreshButton(context, rv, widgetId);
            }
        }
        if (widgetType.equals("wr") || widgetType.equals("flip") || widgetType.equals("flat")) {
            initRecommendationButton(context, rv, widgetId);
        }
        initOffersButton(context, rv, widgetId);
        initShareButton(context, rv, widgetId);
        initWidgetizeMeButton(context, rv, widgetId);
        initLogoButton(context, rv, widgetId, true);
        initHeader(context, rv);
        initFooter(context, rv);
        return rv;
    }

    private static void initHeader(Context context, RemoteViews rv) {
        int color = Colors.get().getColor(R.color.header_bg_wr);
        if (color == 0) {
            rv.setInt(R.id.header, "setBackgroundResource", R.drawable.header_bg);
        } else {
            rv.setInt(R.id.header, "setBackgroundColor", color);
        }
    }

    private static void initFooter(Context context, RemoteViews rv) {
        int color = Colors.get().getColor(R.color.footer_bg_wr);
        if (color == 0) {
            rv.setInt(R.id.footer_panel, "setBackgroundResource", R.drawable.footer_bg);
        } else {
            rv.setInt(R.id.footer_panel, "setBackgroundColor", color);
        }
    }

    private static RemoteViews createRemoteViewsLegacy(Context context, int widgetId) {
        int widgetLayoutId = R.layout.widget_legacy;
        String widgetType = Strings.get().getString(R.string.widget_type);
        if (widgetType.equals("wr")) {
            widgetLayoutId = R.layout.widget_wr_legacy;
        }
        RemoteViews rv = new RemoteViews(context.getPackageName(), widgetLayoutId);
        String[] staticFeeds = Strings.get().getStringArray(R.array.feeds);
        boolean isReady = (staticFeeds != null && staticFeeds.length > 0) || Strings.get().isLoaded() || (!TextUtils.isEmpty(Pref.getPrefString("selected_feeds")));
        if (context.getPackageName().contains("twitto") || context.getPackageName().equals("com.widgetizefb")) {
            isReady = true;
        }
        if (isReady) {
            if (((App) context.getApplicationContext()).isInOffersMode()) {
                rv.setViewVisibility(R.id.items, 0);
                rv.setViewVisibility(R.id.empty_view, 4);
                initOfferItems(context, rv, widgetId);
                rv.setTextViewText(R.id.date, getDateString(context, Pref.getPrefLong("last_update_" + widgetId)));
            } else {
                if (widgetType.equals("wr")) {
                    initCategories(context, rv, widgetId);
                }
                if (((App) context.getApplicationContext()).isWidgetUpdating(widgetId)) {
                    initEmptyItems(context, rv, widgetId);
                    rv.setViewVisibility(R.id.items, 4);
                    rv.setViewVisibility(R.id.pager, 4);
                    rv.setViewVisibility(R.id.empty_view, 0);
                    rv.setTextViewText(R.id.date, context.getString(R.string.updating));
                } else {
                    rv.setViewVisibility(R.id.items, 0);
                    rv.setViewVisibility(R.id.empty_view, 4);
                    initItems(context, rv, widgetId);
                    rv.setTextViewText(R.id.date, getDateString(context, Pref.getPrefLong("last_update_" + widgetId)));
                    initRefreshButton(context, rv, widgetId);
                }
            }
            if (widgetType.equals("wr")) {
                initRecommendationButton(context, rv, widgetId);
                initHeader(context, rv);
                initFooter(context, rv);
            } else {
                initOffersButton(context, rv, widgetId);
            }
            initShareButton(context, rv, widgetId);
            initWidgetizeMeButton(context, rv, widgetId);
            initLogoButton(context, rv, widgetId, true);
        }
        return rv;
    }

    private static RemoteViews createRemoteViewsAutoRefresh(Context context, int widgetId, String widgetType) {
        RemoteViews rv;
        Bitmap bm;
        Logger.l("createRemoteViewsAutoRefresh");
        if (!sCreatedWidgetsIds.contains(Integer.valueOf(widgetId))) {
            Logger.l("first crate");
            sCreatedWidgetsIds.add(Integer.valueOf(widgetId));
            if (widgetType.equals("mini")) {
                return new RemoteViews(context.getPackageName(), R.layout.widget_mini);
            }
            return new RemoteViews(context.getPackageName(), R.layout.widget_flip);
        }
        boolean isUpdating = ((App) context.getApplicationContext()).isWidgetUpdating(widgetId);
        Logger.l("createRemoteViewsAutoRefresh: isUpdating=" + isUpdating);
        if (!isUpdating) {
            ArrayList<RSSItem> items = new ArrayList<>();
            boolean isRTL = false;
            boolean forceLTR = false;
            Iterator<RSSFeed> it = ((App) context.getApplicationContext()).getFeeds().iterator();
            while (it.hasNext()) {
                RSSFeed feed = it.next();
                if (isRTL) {
                    if (!feed.isRTL()) {
                        isRTL = false;
                        forceLTR = true;
                    }
                } else if (!forceLTR) {
                    isRTL = feed.isRTL();
                }
                items.addAll(feed.getItems());
            }
            Collections.sort(items, new Comparator<RSSItem>() {
                public int compare(RSSItem lhs, RSSItem rhs) {
                    if (lhs.getPubDate() < rhs.getPubDate()) {
                        return 1;
                    }
                    if (lhs.getPubDate() > rhs.getPubDate()) {
                        return -1;
                    }
                    return 0;
                }
            });
            if (widgetType.equals("mini")) {
                if (!isRTL) {
                    rv = new RemoteViews(context.getPackageName(), R.layout.widget_mini);
                } else if (isRTLFixRequired()) {
                    rv = new RemoteViews(context.getPackageName(), R.layout.widget_mini_rtl_gravityfix);
                } else {
                    rv = new RemoteViews(context.getPackageName(), R.layout.widget_mini_rtl);
                }
            } else if (!isRTL) {
                rv = new RemoteViews(context.getPackageName(), R.layout.widget_flip);
            } else if (isRTLFixRequired()) {
                rv = new RemoteViews(context.getPackageName(), R.layout.widget_flip_rtl_gravityfix);
            } else {
                rv = new RemoteViews(context.getPackageName(), R.layout.widget_flip_rtl);
            }
            rv.setViewVisibility(R.id.update_text, 8);
            rv.setViewVisibility(R.id.logo, 0);
            if (widgetType.equals("flip") || widgetType.equals("flat")) {
                initRecommendationButton(context, rv, widgetId);
            }
            initWidgetizeMeButton(context, rv, widgetId);
            initLogoButton(context, rv, widgetId, false);
            if (!items.isEmpty()) {
                int startIndex = (Pref.getPrefInt(Pref.START_INDEX + widgetId) + 1) % items.size();
                Pref.setPrefInt(Pref.START_INDEX + widgetId, startIndex);
                RSSItem item = (RSSItem) items.get(startIndex);
                rv.setTextViewText(R.id.title, item.getTitle());
                String author = item.getAuthor();
                if (!TextUtils.isEmpty(author)) {
                    rv.setViewVisibility(R.id.by_label, 0);
                    rv.setViewVisibility(R.id.by, 0);
                    rv.setViewVisibility(R.id.clock, 0);
                    rv.setTextViewText(R.id.by, author);
                } else {
                    rv.setViewVisibility(R.id.by_label, 8);
                    rv.setViewVisibility(R.id.by, 8);
                    rv.setViewVisibility(R.id.clock, 8);
                }
                rv.setTextViewText(R.id.time, StringUtils.getTimeString(context, item.getPubDate(), isRTL));
                if (widgetType.equals("flip")) {
                    ImageStore imageStore = ((App) context.getApplicationContext()).getImageStore();
                    if (!(imageStore == null || (bm = imageStore.get(item.getImageURL())) == null)) {
                        rv.setImageViewBitmap(R.id.image, bm);
                    }
                    initShareButton(context, rv, widgetId);
                    initRefreshButton(context, rv, widgetId);
                }
                Intent intent = new Intent(context, WMButtonBroadcast.class);
                intent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 3);
                intent.putExtra("widget_id", widgetId);
                intent.setData(Uri.parse(item.getLink()));
                rv.setOnClickPendingIntent(R.id.item, PendingIntent.getBroadcast(context, R.id.item, intent, 268435456));
            }
            startAutoRefreshAlarm(context, widgetId, AUTO_REFRESH_INTERVAL);
            return rv;
        } else if (widgetType.equals("mini")) {
            return new RemoteViews(context.getPackageName(), R.layout.widget_mini);
        } else {
            return new RemoteViews(context.getPackageName(), R.layout.widget_flip);
        }
    }

    private static RemoteViews createRemoteViewsFlat(Context context, int widgetId) {
        RemoteViews rv;
        Bitmap bm;
        if (!sCreatedWidgetsIds.contains(Integer.valueOf(widgetId))) {
            Logger.l("first crate");
            sCreatedWidgetsIds.add(Integer.valueOf(widgetId));
            return new RemoteViews(context.getPackageName(), R.layout.widget_flat);
        }
        boolean isUpdating = ((App) context.getApplicationContext()).isWidgetUpdating(widgetId);
        Logger.l("createRemoteViewsAutoRefresh: isUpdating=" + isUpdating);
        if (isUpdating) {
            return new RemoteViews(context.getPackageName(), R.layout.widget_flat);
        }
        ArrayList<RSSItem> items = new ArrayList<>();
        boolean isRTL = false;
        boolean forceLTR = false;
        Iterator<RSSFeed> it = ((App) context.getApplicationContext()).getFeeds().iterator();
        while (it.hasNext()) {
            RSSFeed feed = it.next();
            if (isRTL) {
                if (!feed.isRTL()) {
                    isRTL = false;
                    forceLTR = true;
                }
            } else if (!forceLTR) {
                isRTL = feed.isRTL();
            }
            items.addAll(feed.getItems());
        }
        Collections.sort(items, new Comparator<RSSItem>() {
            public int compare(RSSItem lhs, RSSItem rhs) {
                if (lhs.getPubDate() < rhs.getPubDate()) {
                    return 1;
                }
                if (lhs.getPubDate() > rhs.getPubDate()) {
                    return -1;
                }
                return 0;
            }
        });
        if (!isRTL) {
            rv = new RemoteViews(context.getPackageName(), R.layout.widget_flat);
        } else if (isRTLFixRequired()) {
            rv = new RemoteViews(context.getPackageName(), R.layout.widget_flat_rtl_gravityfix);
        } else {
            rv = new RemoteViews(context.getPackageName(), R.layout.widget_flat_rtl);
        }
        if (!items.isEmpty()) {
            if (((App) context.getApplicationContext()).isInOffersMode()) {
                rv.setViewVisibility(R.id.items, 0);
                rv.setViewVisibility(R.id.empty_view, 4);
                initOfferItems(context, rv, widgetId);
                rv.setTextViewText(R.id.date, getDateString(context, Pref.getPrefLong("last_update_" + widgetId)));
            } else {
                rv.setViewVisibility(R.id.empty_view, 4);
                rv.setTextViewText(R.id.date, getDateString(context, Pref.getPrefLong("last_update_" + widgetId)));
                int startIndex = Pref.getPrefInt(Pref.START_INDEX + widgetId);
                RSSItem item = (RSSItem) items.get(startIndex);
                rv.setTextViewText(R.id.title, item.getTitle());
                String author = item.getAuthor();
                if (!TextUtils.isEmpty(author)) {
                    rv.setViewVisibility(R.id.by_label, 0);
                    rv.setViewVisibility(R.id.by, 0);
                    rv.setViewVisibility(R.id.clock, 0);
                    rv.setTextViewText(R.id.by, author);
                } else {
                    rv.setViewVisibility(R.id.by_label, 8);
                    rv.setViewVisibility(R.id.by, 8);
                    rv.setViewVisibility(R.id.clock, 8);
                }
                rv.setTextViewText(R.id.time, StringUtils.getTimeString(context, item.getPubDate(), isRTL));
                ImageStore imageStore = ((App) context.getApplicationContext()).getImageStore();
                if (!(imageStore == null || (bm = imageStore.get(item.getImageURL())) == null)) {
                    rv.setImageViewBitmap(R.id.image, bm);
                }
                rv.setViewVisibility(R.id.pager, 0);
                rv.setTextViewText(R.id.count, String.valueOf(startIndex + 1) + " of " + items.size());
                rv.setViewVisibility(R.id.prev, 0);
                rv.setViewVisibility(R.id.next, 0);
                if (startIndex != 0) {
                    rv.setImageViewResource(R.id.prev, R.drawable.arrow_white_l);
                    Intent intent = new Intent(context, WMButtonBroadcast.class);
                    intent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 4);
                    intent.putExtra("widget_id", widgetId);
                    intent.setData(Uri.parse("abcd://widget/" + widgetId));
                    rv.setOnClickPendingIntent(R.id.prev, PendingIntent.getBroadcast(context, R.id.prev, intent, 268435456));
                } else {
                    rv.setImageViewResource(R.id.prev, R.drawable.arrow_disable_l);
                    Intent intent2 = new Intent(context, WMButtonBroadcast.class);
                    intent2.setData(Uri.parse("abcd://widget/" + widgetId));
                    rv.setOnClickPendingIntent(R.id.prev, PendingIntent.getBroadcast(context, R.id.prev, intent2, 268435456));
                }
                if (startIndex + 1 < items.size()) {
                    rv.setImageViewResource(R.id.next, R.drawable.arrow_white_r);
                    Intent intent3 = new Intent(context, WMButtonBroadcast.class);
                    intent3.putExtra(WMButtonBroadcast.EXTRA_ACTION, 5);
                    intent3.putExtra("widget_id", widgetId);
                    intent3.setData(Uri.parse("abcd://widget/" + widgetId));
                    rv.setOnClickPendingIntent(R.id.next, PendingIntent.getBroadcast(context, R.id.next, intent3, 268435456));
                } else {
                    rv.setImageViewResource(R.id.next, R.drawable.arrow_disable_r);
                    Intent intent4 = new Intent(context, WMButtonBroadcast.class);
                    intent4.setData(Uri.parse("abcd://widget/" + widgetId));
                    rv.setOnClickPendingIntent(R.id.next, PendingIntent.getBroadcast(context, R.id.next, intent4, 268435456));
                }
                Intent intent5 = new Intent(context, WMButtonBroadcast.class);
                intent5.putExtra(WMButtonBroadcast.EXTRA_ACTION, 3);
                intent5.putExtra("widget_id", widgetId);
                intent5.setData(Uri.parse(item.getLink()));
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, R.id.image, intent5, 268435456);
                rv.setOnClickPendingIntent(R.id.image, pendingIntent);
                rv.setOnClickPendingIntent(R.id.read_more, pendingIntent);
            }
        }
        initRecommendationButton(context, rv, widgetId);
        initRefreshButton(context, rv, widgetId);
        initShareButton(context, rv, widgetId);
        initWidgetizeMeButton(context, rv, widgetId);
        initLogoButton(context, rv, widgetId, false);
        initHeader(context, rv);
        initFooter(context, rv);
        return rv;
    }

    private static void startAutoRefreshAlarm(Context context, int widgetId, long delta) {
        Logger.l("startAutoRefreshAlarm");
        Intent intent = new Intent(context, WMAutoRefreshReceiver.class);
        intent.putExtra("widget_id", widgetId);
        intent.setData(Uri.parse("abcd://widget/" + widgetId));
        ((AlarmManager) context.getSystemService("alarm")).set(0, System.currentTimeMillis() + delta, PendingIntent.getBroadcast(context, widgetId + 1000, intent, 268435456));
    }

    private static void initCategories(Context context, RemoteViews rv, int widgetId) {
        int[] views = {R.id.category_1, R.id.category_2, R.id.category_3};
        String[] feedNames = Strings.get().getStringArray(R.array.feeds_names);
        String prefFeedNames = Pref.getPrefString(Pref.FEEDS_NAMES);
        if (!TextUtils.isEmpty(prefFeedNames)) {
            feedNames = prefFeedNames.split("~");
        }
        rv.setInt(R.id.categories_panel, "setBackgroundColor", Colors.get().getColor(R.color.header_recommendations_bg));
        int selectedIndex = ((App) context.getApplicationContext()).getCurrCategory(widgetId);
        for (int i = 0; i < feedNames.length; i++) {
            Intent intent = new Intent(context, WMButtonBroadcast.class);
            rv.setTextViewText(views[i], feedNames[i]);
            if (i == selectedIndex) {
                SpannableString ss = new SpannableString(feedNames[i]);
                ss.setSpan(new StyleSpan(1), 0, feedNames[i].length(), 0);
                rv.setTextViewText(views[i], ss);
                rv.setTextColor(views[i], Colors.get().getColor(R.color.category_text_selected));
                intent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 9);
            } else {
                rv.setTextColor(views[i], Colors.get().getColor(R.color.category_text));
                intent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 10);
                intent.putExtra(WMButtonBroadcast.EXTRA_CATEGORY_INDEX, i);
            }
            rv.setViewVisibility(views[i], 0);
            intent.putExtra("widget_id", widgetId);
            intent.setData(Uri.parse("abcd://widget/" + widgetId));
            rv.setOnClickPendingIntent(views[i], PendingIntent.getBroadcast(context, views[i], intent, 268435456));
        }
        for (int i2 = feedNames.length; i2 < views.length; i2++) {
            rv.setViewVisibility(views[i2], 8);
        }
    }

    private static void initRefreshButton(Context context, RemoteViews rv, int widgetId) {
        Intent intent = new Intent(context, WMButtonBroadcast.class);
        intent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 0);
        intent.putExtra("widget_id", widgetId);
        intent.setData(Uri.parse("abcd://widget/" + widgetId));
        rv.setOnClickPendingIntent(R.id.refresh, PendingIntent.getBroadcast(context, R.id.refresh, intent, 268435456));
    }

    private static void initShareButton(Context context, RemoteViews rv, int widgetId) {
        Intent intent = new Intent(context, WMButtonBroadcast.class);
        intent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 1);
        intent.putExtra("widget_id", widgetId);
        intent.setData(Uri.parse("abcd://widget/" + widgetId));
        rv.setOnClickPendingIntent(R.id.share, PendingIntent.getBroadcast(context, R.id.share, intent, 268435456));
    }

    private static void initOffersButton(Context context, RemoteViews rv, int widgetId) {
        Intent intent = new Intent(context, WMButtonBroadcast.class);
        intent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 11);
        intent.putExtra("widget_id", widgetId);
        intent.setData(Uri.parse("abcd://widget/" + widgetId));
        rv.setOnClickPendingIntent(R.id.offers, PendingIntent.getBroadcast(context, R.id.offers, intent, 268435456));
    }

    private static void initRecommendationButton(Context context, RemoteViews rv, int widgetId) {
        rv.setInt(R.id.recommendations, "setBackgroundColor", context.getResources().getColor(17170445));
        rv.setInt(R.id.recommendations, "setImageResource", R.drawable.recommendation_icon);
        rv.setViewVisibility(R.id.recommendations, 0);
        Intent intent = new Intent(context, WMButtonBroadcast.class);
        intent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 11);
        intent.putExtra("widget_id", widgetId);
        intent.setData(Uri.parse("abcd://widget/" + widgetId));
        rv.setOnClickPendingIntent(R.id.recommendations, PendingIntent.getBroadcast(context, R.id.recommendations, intent, 268435456));
    }

    private static void initWidgetizeMeButton(Context context, RemoteViews rv, int widgetId) {
        Intent intent = new Intent(context, WMButtonBroadcast.class);
        intent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 2);
        intent.putExtra("widget_id", widgetId);
        intent.setData(Uri.parse("abcd://widget/" + widgetId));
        rv.setOnClickPendingIntent(R.id.powered_by, PendingIntent.getBroadcast(context, R.id.powered_by, intent, 268435456));
    }

    private static void initLogoButton(Context context, RemoteViews rv, int widgetId, boolean updateLogoBg) {
        Bitmap bm;
        if (updateLogoBg) {
            String logoBG = Strings.get().getString(R.string.logo_bg);
            if (TextUtils.isEmpty(logoBG) || !logoBG.equals("true")) {
                rv.setInt(R.id.logo_bg, "setBackgroundResource", 17170445);
            } else {
                rv.setInt(R.id.logo_bg, "setBackgroundResource", R.drawable.logo_bg);
            }
        }
        String logoFit = Strings.get().getString(R.string.logo_fit);
        if (logoFit != null) {
            logoFit.equals("true");
        }
        String wid = Pref.getPrefString("widget_id");
        if (!App.isExtensation(context) && !TextUtils.isEmpty(wid) && (bm = ((App) context.getApplicationContext()).getImageStore().get("http://widgetize.me/widget/getresource?wid=" + wid + "&resname=logo.png")) != null) {
            rv.setImageViewBitmap(R.id.logo, bm);
        }
        Intent intent = new Intent(context, WMButtonBroadcast.class);
        intent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 7);
        intent.putExtra("widget_id", widgetId);
        intent.setData(Uri.parse("abcd://widget/" + widgetId));
        rv.setOnClickPendingIntent(R.id.logo, PendingIntent.getBroadcast(context, R.id.logo, intent, 268435456));
    }

    private static void initEmptyItems(Context context, RemoteViews rv, int widgetId) {
        rv.removeAllViews(R.id.items);
        for (int i = 0; i < 2; i++) {
            rv.addView(R.id.items, initItem(context, widgetId, null, 0, false, null));
            if (i + 1 < 2) {
                rv.addView(R.id.items, new RemoteViews(context.getPackageName(), R.layout.widget_item_div));
            }
        }
    }

    private static void initItems(Context context, RemoteViews rv, int widgetId) {
        ArrayList<RSSItem> items = new ArrayList<>();
        boolean isRTL = false;
        boolean forceLTR = false;
        Iterator<RSSFeed> it = ((App) context.getApplicationContext()).getFeeds().iterator();
        while (it.hasNext()) {
            RSSFeed feed = it.next();
            if (!feed.isEmpty()) {
                if (isRTL) {
                    if (!feed.isRTL()) {
                        isRTL = false;
                        forceLTR = true;
                    }
                } else if (!forceLTR) {
                    isRTL = feed.isRTL();
                }
                items.addAll(feed.getItems());
            }
        }
        Collections.sort(items, new Comparator<RSSItem>() {
            public int compare(RSSItem lhs, RSSItem rhs) {
                if (lhs.getPubDate() < rhs.getPubDate()) {
                    return 1;
                }
                if (lhs.getPubDate() > rhs.getPubDate()) {
                    return -1;
                }
                return 0;
            }
        });
        rv.removeAllViews(R.id.items);
        ImageStore imageStore = null;
        if (Strings.get().getString(R.string.layout_type).equals("text+image")) {
            imageStore = ((App) context.getApplicationContext()).getImageStore();
        }
        int startIndex = Pref.getPrefInt(Pref.START_INDEX + widgetId);
        if (items.size() > 2) {
            rv.setViewVisibility(R.id.pager, 0);
            rv.setInt(R.id.pager, "setBackgroundColor", Colors.get().getColor(R.color.header_recommendations_bg));
            if (startIndex + 2 < items.size()) {
                initNextButton(context, rv, widgetId, true, Pref.START_INDEX);
            } else {
                initNextButton(context, rv, widgetId, false, Pref.START_INDEX);
            }
            if (startIndex > 0) {
                initPrevButton(context, rv, widgetId, true, Pref.START_INDEX);
            } else {
                initPrevButton(context, rv, widgetId, false, Pref.START_INDEX);
            }
            updatePageIndicators(context, rv, items.size(), startIndex);
        } else {
            rv.setViewVisibility(R.id.pager, 8);
            initPrevButton(context, rv, widgetId, false, Pref.START_INDEX);
            initNextButton(context, rv, widgetId, false, Pref.START_INDEX);
        }
        int count = Math.min(items.size(), 2);
        for (int i = 0; i < count; i++) {
            int itemIndex = startIndex + i;
            RSSItem item = null;
            if (itemIndex < items.size()) {
                item = items.get(itemIndex);
            }
            RemoteViews remoteViews = rv;
            remoteViews.addView(R.id.items, initItem(context, widgetId, item, itemIndex, isRTL, imageStore));
            if (i + 1 < count) {
                RemoteViews remoteViews2 = rv;
                remoteViews2.addView(R.id.items, new RemoteViews(context.getPackageName(), R.layout.widget_item_div));
            }
        }
    }

    private static void initOfferItems(Context context, RemoteViews rv, int widgetId) {
        rv.removeAllViews(R.id.items);
        OfferData offers = new OfferData();
        int startIndex = Pref.getPrefInt(Pref.OFFER_START_INDEX + widgetId);
        if (offers.getCount() > 2) {
            rv.setViewVisibility(R.id.pager, 0);
            if (startIndex + 2 < offers.getCount()) {
                initNextButton(context, rv, widgetId, true, Pref.OFFER_START_INDEX);
            } else {
                initNextButton(context, rv, widgetId, false, Pref.OFFER_START_INDEX);
            }
            if (startIndex > 0) {
                initPrevButton(context, rv, widgetId, true, Pref.OFFER_START_INDEX);
            } else {
                initPrevButton(context, rv, widgetId, false, Pref.OFFER_START_INDEX);
            }
            updatePageIndicators(context, rv, offers.getCount(), startIndex);
        } else {
            rv.setViewVisibility(R.id.pager, 8);
            initPrevButton(context, rv, widgetId, false, Pref.OFFER_START_INDEX);
            initNextButton(context, rv, widgetId, false, Pref.OFFER_START_INDEX);
        }
        int count = Math.min(offers.getCount(), 2);
        for (int i = 0; i < count; i++) {
            int itemIndex = startIndex + i;
            OfferData.OfferDetails item = null;
            if (itemIndex < offers.getCount()) {
                item = offers.getOffer(itemIndex);
            }
            rv.addView(R.id.items, initOfferItem(context, widgetId, item, itemIndex));
            if (i + 1 < count) {
                rv.addView(R.id.items, new RemoteViews(context.getPackageName(), R.layout.widget_item_div));
            }
        }
    }

    private static void updatePageIndicators(Context context, RemoteViews rv, int itemsCount, int startIndex) {
        Logger.l("updatePageIndicators: " + itemsCount + ", " + startIndex);
        int pages = (itemsCount / 2) + (itemsCount % 2 > 0 ? 1 : 0);
        int maxIndicators = Math.min(5, pages);
        Logger.l("pages: " + pages);
        Logger.l("maxIndicators: " + maxIndicators);
        for (int i = maxIndicators; i < 5; i++) {
            rv.setViewVisibility(context.getResources().getIdentifier("page" + i, UnsentStats.KEY_ID, context.getPackageName()), 8);
        }
        int page = startIndex / 2;
        int pageIndex = page % maxIndicators;
        Logger.l("page: " + page);
        Logger.l("pageIndex: " + pageIndex);
        for (int i2 = 0; i2 < maxIndicators; i2++) {
            int id = context.getResources().getIdentifier("page" + (i2 + 1), UnsentStats.KEY_ID, context.getPackageName());
            if (i2 == pageIndex) {
                rv.setImageViewResource(id, R.drawable.page_white);
            } else {
                rv.setImageViewResource(id, R.drawable.page_black);
            }
        }
    }

    private static RemoteViews initItem(Context context, int widgetId, RSSItem item, int itemId, boolean isRTL, ImageStore imageStore) {
        RemoteViews retVal;
        String widgetType = Strings.get().getString(R.string.widget_type);
        if (isRTL) {
            if (widgetType.equals("wr")) {
                if (isRTLFixRequired()) {
                    retVal = new RemoteViews(context.getPackageName(), R.layout.widget_item_rtl_gravityfix_wr);
                } else {
                    retVal = new RemoteViews(context.getPackageName(), R.layout.widget_item_rtl_wr);
                }
            } else if (isRTLFixRequired()) {
                retVal = new RemoteViews(context.getPackageName(), R.layout.widget_item_rtl_gravityfix);
            } else {
                retVal = new RemoteViews(context.getPackageName(), R.layout.widget_item_rtl);
            }
        } else if (widgetType.equals("wr")) {
            retVal = new RemoteViews(context.getPackageName(), R.layout.widget_item_wr_lagacy);
        } else {
            retVal = new RemoteViews(context.getPackageName(), R.layout.widget_item);
        }
        if (item == null) {
            retVal.setViewVisibility(R.id.item, 4);
        } else {
            retVal.setInt(R.id.item, "setBackgroundResource", R.drawable.widget_item_bg_legacy);
            retVal.setTextViewText(R.id.title, item.getTitle());
            retVal.setTextViewText(R.id.time, StringUtils.getTimeString(context, item.getPubDate(), isRTL));
            retVal.setTextViewText(R.id.desc, item.getDesc());
            if (!(context.getPackageName().contains("twitto") || context.getPackageName().equals("com.widgetizefb")) && !widgetType.equals("normal")) {
                if (!TextUtils.isEmpty(item.getAuthor())) {
                    retVal.setViewVisibility(R.id.by_label, 0);
                    retVal.setViewVisibility(R.id.by, 0);
                    retVal.setViewVisibility(R.id.clock, 0);
                    retVal.setTextViewText(R.id.by, item.getAuthor());
                } else {
                    retVal.setViewVisibility(R.id.by_label, 8);
                    retVal.setViewVisibility(R.id.by, 8);
                    retVal.setViewVisibility(R.id.clock, 8);
                }
            }
            if (TextUtils.isEmpty(item.getTitle()) || !TextUtils.isEmpty(item.getDesc())) {
                retVal.setInt(R.id.title, "setMaxLines", 1);
                retVal.setViewVisibility(R.id.desc, 0);
            } else {
                retVal.setInt(R.id.title, "setMaxLines", 2);
                retVal.setViewVisibility(R.id.desc, 8);
            }
            if (imageStore != null) {
                Bitmap bm = imageStore.get(item.getImageURL());
                if (bm != null) {
                    retVal.setImageViewBitmap(R.id.image, bm);
                } else {
                    retVal.setImageViewResource(R.id.image, R.drawable.empty_image);
                }
            }
            Intent intent = new Intent(context, WMButtonBroadcast.class);
            intent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 3);
            intent.putExtra("widget_id", widgetId);
            if (item.getLink() != null) {
                intent.setData(Uri.parse(item.getLink()));
            }
            retVal.setOnClickPendingIntent(R.id.item, PendingIntent.getBroadcast(context, itemId, intent, 268435456));
        }
        return retVal;
    }

    private static RemoteViews initOfferItem(Context context, int widgetId, OfferData.OfferDetails item, int itemId) {
        RemoteViews retVal = new RemoteViews(context.getPackageName(), R.layout.offer_item);
        if (item == null) {
            retVal.setViewVisibility(R.id.item, 4);
        } else {
            retVal.setInt(R.id.item, "setBackgroundResource", R.drawable.widget_item_bg_legacy);
            retVal.setTextViewText(R.id.title, item.mName);
            retVal.setTextViewText(R.id.desc, item.mDesc);
            try {
                Intent fillInIntent = new Intent("android.intent.action.VIEW");
                fillInIntent.setData(Uri.parse("market://details?id=" + item.mPackageName));
                retVal.setOnClickFillInIntent(R.id.item, fillInIntent);
            } catch (Exception e) {
                Logger.l(e);
            }
            retVal.setImageViewResource(R.id.icon, item.mIcon);
        }
        return retVal;
    }

    private static void initPrevButton(Context context, RemoteViews rv, int widgetId, boolean enabled, String indexName) {
        if (!enabled) {
            rv.setViewVisibility(R.id.prev, 4);
            return;
        }
        rv.setViewVisibility(R.id.prev, 0);
        Intent intent = new Intent(context, WMButtonBroadcast.class);
        intent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 4);
        intent.putExtra("widget_id", widgetId);
        intent.putExtra(WMButtonBroadcast.EXTRA_INDEX_NAME, indexName);
        intent.setData(Uri.parse("abcd://widget/" + widgetId));
        rv.setOnClickPendingIntent(R.id.prev, PendingIntent.getBroadcast(context, R.id.prev, intent, 268435456));
    }

    private static void initNextButton(Context context, RemoteViews rv, int widgetId, boolean enabled, String indexName) {
        if (!enabled) {
            rv.setViewVisibility(R.id.next, 4);
            return;
        }
        rv.setViewVisibility(R.id.next, 0);
        Intent intent = new Intent(context, WMButtonBroadcast.class);
        intent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 5);
        intent.putExtra("widget_id", widgetId);
        intent.putExtra(WMButtonBroadcast.EXTRA_INDEX_NAME, indexName);
        intent.setData(Uri.parse("abcd://widget/" + widgetId));
        rv.setOnClickPendingIntent(R.id.next, PendingIntent.getBroadcast(context, R.id.next, intent, 268435456));
    }

    private static String getDateString(Context context, long time) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        Resources res = context.getResources();
        String monthString = res.getStringArray(R.array.months)[c.get(2)];
        int day = c.get(5);
        String dayOfWeekString = res.getStringArray(R.array.days)[c.get(7) - 1];
        int hour = c.get(10);
        int minute = c.get(12);
        String minuteString = String.valueOf(minute);
        if (minute < 10) {
            minuteString = "0" + minuteString;
        }
        String ampm = "PM";
        if (c.get(9) == 0) {
            ampm = "AM";
        }
        return String.valueOf(dayOfWeekString) + " " + day + " " + monthString + " " + hour + ":" + minuteString + " " + ampm;
    }

    private void pingServer(Context context) {
        if (System.currentTimeMillis() - Pref.getPrefLong(Pref.LAST_PING) > 86400000) {
            Pref.setPrefLong(Pref.LAST_PING, System.currentTimeMillis());
            ((App) context.getApplicationContext()).sendStats(6);
        }
    }

    private static boolean isRTLFixRequired() {
        if (Build.VERSION.SDK_INT >= 11 || !Build.DEVICE.equals("GT-I9000") || !Build.FINGERPRINT.equals("samsung/GT-I9000/GT-I9000:2.3.3/GINGERBREAD/JIJV8:user/release-keys") || !Build.HOST.equals("ssadmin5-desktop")) {
            return false;
        }
        return true;
    }
}
