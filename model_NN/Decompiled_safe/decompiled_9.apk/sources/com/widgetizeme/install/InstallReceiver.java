package com.widgetizeme.install;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.widgetizeme.App;
import com.widgetizeme.Logger;
import com.widgetizeme.Pref;
import com.widgetizeme.utils.SearchString;

public class InstallReceiver extends BroadcastReceiver {
    public static final String EXTRA_REFERRER = "referrer";

    public void onReceive(Context context, Intent intent) {
        Logger.l("InstallReceiver onReceive");
        if (intent != null) {
            try {
                if (intent.getExtras() != null) {
                    String referrer = intent.getExtras().getString("referrer");
                    if (!TextUtils.isEmpty(referrer)) {
                        Logger.l("catch referrer: " + referrer);
                        Pref.setPrefString("referrer", referrer);
                        SearchString ss = new SearchString(referrer);
                        String wid = ss.findString("wid=", "&", 0).getRawString();
                        Pref.setPrefString("widget_id", wid);
                        Pref.setPrefString(Pref.CID, ss.findString("cid=", "&", 0).getRawString());
                        Logger.l("widgetId=" + wid);
                        String pkgName = context.getPackageName();
                        Logger.l("pkgName: " + pkgName);
                        if (pkgName.equals("com.widgetizefb") || pkgName.equals("com.twitto")) {
                            Logger.l("ignore install assets");
                        } else {
                            Logger.l("start install service");
                            Intent installIntent = new Intent(context, InstallService.class);
                            installIntent.putExtra("widget_id", wid);
                            context.startService(installIntent);
                        }
                        App.sendInstallStat(context);
                    }
                }
            } catch (Exception e) {
                Logger.l(e);
            }
        }
    }
}
