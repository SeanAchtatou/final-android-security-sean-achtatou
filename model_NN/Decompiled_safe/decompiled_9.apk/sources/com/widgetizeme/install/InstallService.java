package com.widgetizeme.install;

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import com.widgetizeme.App;
import com.widgetizeme.Colors;
import com.widgetizeme.Logger;
import com.widgetizeme.Pref;
import com.widgetizeme.Strings;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class InstallService extends IntentService {
    public static final String EXTRA_WIDGET_ID = "widget_id";

    public InstallService() {
        super("InstallService");
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        Logger.l("InstallService: handle intent");
        String widgetId = intent.getStringExtra("widget_id");
        Logger.l("InstallService. onHandleIntent, widgetId:" + widgetId);
        if (!TextUtils.isEmpty(widgetId) && download("http://widgetize.me/widget/getresource?wid=" + widgetId + "&resname=strings.xml") && download("http://widgetize.me/widget/getresource?wid=" + widgetId + "&resname=colors.xml")) {
            String logoURL = "http://widgetize.me/widget/getresource?wid=" + widgetId + "&resname=logo.png";
            Logger.l("save: " + logoURL);
            ((App) getApplication()).getImageStore().insert(logoURL);
            Bitmap bm = ((App) getApplication()).getImageStore().get(logoURL);
            if (bm != null) {
                bm.recycle();
                Logger.l("load strings");
                Strings.init(getApplicationContext());
                Logger.l("load colors");
                Colors.init(getApplicationContext());
                if (!Strings.get().isLoaded() || !Colors.get().isLoaded()) {
                    Logger.l("assets not loaded!");
                    return;
                }
                Logger.l("assets ready!");
                Pref.setPrefBoolean(Pref.ASSETS_LOADED, true);
                return;
            }
            Logger.l("failed to load logo");
        }
    }

    private boolean download(String url) {
        boolean retVal = false;
        int i = 0;
        while (!retVal && i < 3) {
            try {
                HttpParams httpParameters = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
                HttpConnectionParams.setSoTimeout(httpParameters, 5000);
                HttpResponse response = new DefaultHttpClient(httpParameters).execute(new HttpGet(url));
                if (200 != response.getStatusLine().getStatusCode()) {
                    Logger.l("download asset(" + url + ") failed: " + response.getStatusLine().getReasonPhrase());
                } else {
                    Logger.l("save asset: " + url);
                    saveToFile(response.getEntity().getContent(), url);
                    retVal = true;
                }
                response.getEntity().consumeContent();
            } catch (Exception e) {
                Logger.l(e);
            }
            if (!retVal) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e2) {
                }
            }
            i++;
        }
        return retVal;
    }

    private void saveToFile(InputStream is, String url) {
        try {
            FileOutputStream output = openFileOutput(url.substring(url.lastIndexOf(61) + 1), 0);
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is, "utf-8"));
                String line = br.readLine();
                while (line != null) {
                    int startIndex = line.indexOf(60);
                    if (-1 != startIndex) {
                        line = line.substring(startIndex);
                    }
                    output.write(line.getBytes());
                    line = br.readLine();
                }
            } catch (Exception e) {
                Logger.l(e);
            }
            output.close();
            is.close();
        } catch (Exception e2) {
            Logger.l(e2);
        }
    }
}
