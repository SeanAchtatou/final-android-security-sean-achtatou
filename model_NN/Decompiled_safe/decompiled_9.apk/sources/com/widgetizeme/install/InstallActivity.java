package com.widgetizeme.install;

import android.app.Activity;
import android.app.ProgressDialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import com.widgetizeme.App;
import com.widgetizeme.Logger;
import com.widgetizeme.Pref;
import com.widgetizeme.R;
import com.widgetizeme.ui.GuideActivity;
import com.widgetizeme.ui.SettingsActivity;
import com.widgetizeme.widget.WMButtonBroadcast;
import com.widgetizeme.widget.WMWidgetProvider;

public class InstallActivity extends Activity {
    public static final int PICK_FEEDS = 0;
    private LoadingThread mLoadingThread;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_install);
        Logger.l("InstallActivity created");
        if (TextUtils.isEmpty(Pref.getPrefString("referrer"))) {
            Logger.l("referrer not detected");
            App.sendInstallStat(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        int appWidgetId = getIntent().getIntExtra("appWidgetId", 0);
        if (Pref.getPrefBoolean(Pref.ASSETS_LOADED)) {
            refreshAllWidgets();
            if (appWidgetId == 0) {
                startActivity(new Intent(this, GuideActivity.class));
            }
            finish();
            return;
        }
        String widgetId = Pref.getPrefString("widget_id");
        if (TextUtils.isEmpty(widgetId)) {
            String[] feeds = getResources().getStringArray(R.array.feeds);
            if (feeds == null || feeds.length == 0) {
                startActivityForResult(new Intent(this, SettingsActivity.class), 0);
                overridePendingTransition(0, 0);
                return;
            }
            Pref.setPrefBoolean(Pref.ASSETS_LOADED, true);
            refreshAllWidgets();
            if (appWidgetId == 0) {
                startActivity(new Intent(this, GuideActivity.class));
            }
            finish();
            return;
        }
        try {
            Logger.l("install widgetId=" + widgetId);
            Intent installIntent = new Intent(this, InstallService.class);
            installIntent.putExtra("widget_id", widgetId);
            startService(installIntent);
        } catch (Exception e) {
            Logger.l(e);
        }
        this.mLoadingThread = new LoadingThread(this);
        this.mLoadingThread.start();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.mLoadingThread != null) {
            this.mLoadingThread.cancel();
            this.mLoadingThread = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != 0) {
            return;
        }
        if (-1 == resultCode) {
            refreshAllWidgets();
            finish();
            return;
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void refreshAllWidgets() {
        Logger.l("refreshAllWidgets");
        final int appWidgetId = getIntent().getIntExtra("appWidgetId", 0);
        if (appWidgetId == 0) {
            Intent intent = new Intent(this, WMWidgetProvider.class);
            intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
            intent.putExtra("appWidgetIds", AppWidgetManager.getInstance(this).getAppWidgetIds(new ComponentName(this, WMWidgetProvider.class)));
            sendBroadcast(intent);
            return;
        }
        Intent resultValue = new Intent();
        resultValue.putExtra("appWidgetId", appWidgetId);
        setResult(-1, resultValue);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Intent intent = new Intent(InstallActivity.this, WMButtonBroadcast.class);
                intent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 0);
                intent.putExtra("widget_id", appWidgetId);
                intent.setData(Uri.parse("abcd://widget/" + appWidgetId));
                InstallActivity.this.sendBroadcast(intent);
            }
        }, 1000);
    }

    private class LoadingThread extends Thread {
        /* access modifiers changed from: private */
        public boolean mCancelled;
        private ProgressDialog mProgress;

        public LoadingThread(Context context) {
            this.mProgress = ProgressDialog.show(context, null, context.getString(R.string.preparing_widget));
            this.mProgress.setCancelable(true);
            this.mProgress.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    LoadingThread.this.mCancelled = true;
                }
            });
        }

        public void cancel() {
            this.mCancelled = true;
            this.mProgress.dismiss();
        }

        public void run() {
            while (!this.mCancelled && !Pref.getPrefBoolean(Pref.ASSETS_LOADED)) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }
            }
            this.mProgress.dismiss();
            if (!this.mCancelled) {
                InstallActivity.this.refreshAllWidgets();
            }
            InstallActivity.this.finish();
        }
    }
}
