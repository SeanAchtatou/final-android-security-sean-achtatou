package com.widgetizeme;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.Arrays;

public class UnsentStats {
    private static final String DATABASE_NAME = "com.stats";
    private static final int DATABASE_VERSION = 1;
    public static final String KEY_ACTION = "action_id";
    public static final String KEY_ID = "id";
    private static final String TABLE_MY = "unsent";
    private SQLiteDatabase mDB;
    private DatabaseHelper mDBHelper;

    public UnsentStats(Context context) {
        this.mDBHelper = new DatabaseHelper(context);
    }

    public synchronized void push(int actionId) {
        try {
            open();
            try {
                ContentValues cv = new ContentValues();
                cv.put(KEY_ID, Long.valueOf(System.currentTimeMillis()));
                cv.put(KEY_ACTION, Integer.valueOf(actionId));
                Logger.l("insert unsent stats: " + cv + " : " + this.mDB.insert(TABLE_MY, null, cv));
            } catch (Exception e) {
                Logger.l(e);
            }
            close();
        } catch (Exception e2) {
            Logger.l(e2);
        }
        return;
    }

    public synchronized int pop() {
        int retVal;
        retVal = 0;
        try {
            open();
            try {
                Cursor c = this.mDB.query(TABLE_MY, null, null, null, null, null, null);
                if (c != null && c.moveToFirst()) {
                    long id = c.getLong(c.getColumnIndex(KEY_ID));
                    retVal = c.getInt(c.getColumnIndex(KEY_ACTION));
                    c.close();
                    try {
                        retVal = this.mDB.delete(TABLE_MY, "id=" + id, null);
                        Logger.l("delete result " + retVal);
                    } catch (Exception e) {
                        Logger.l(e);
                    }
                }
            } catch (Exception e2) {
                Logger.l(e2);
            }
            close();
        } catch (Exception e3) {
            Logger.l(e3);
        }
        return retVal;
    }

    private UnsentStats open() throws SQLException {
        this.mDB = this.mDBHelper.getWritableDatabase();
        return this;
    }

    private void close() {
        this.mDBHelper.close();
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        private static final String TABLE_CREATE_PARAMS = "id integer primary key, action_id integer);";

        public DatabaseHelper(Context mContext) {
            super(mContext, UnsentStats.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL("create table unsent (id integer primary key, action_id integer);");
            } catch (Exception e) {
                Logger.l(e);
            }
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Logger.l("Upgrading database from version " + oldVersion + " to " + newVersion);
            try {
                upgradeTable(db, UnsentStats.TABLE_MY, TABLE_CREATE_PARAMS);
            } catch (Exception e) {
                Logger.l(e);
            }
        }

        private void upgradeTable(SQLiteDatabase db, String tableName, String creationParam) {
            ArrayList<String> columns = getColumns(db, tableName);
            db.execSQL("ALTER table " + tableName + " RENAME TO temp_" + tableName);
            Logger.l("ALTER table " + tableName + " RENAME TO temp_" + tableName);
            db.execSQL("create table " + tableName + " (" + creationParam);
            Logger.l("create table " + tableName + " (" + creationParam);
            String cols = join(columns, ",");
            db.execSQL("INSERT INTO " + tableName + " (" + cols + ") SELECT " + cols + " from temp_" + tableName);
            Logger.l("INSERT INTO " + tableName + " (" + cols + ") SELECT " + cols + " from temp_" + tableName);
            db.execSQL("DROP table temp_" + tableName);
            Logger.l("DROP table temp_" + tableName);
        }

        private ArrayList<String> getColumns(SQLiteDatabase db, String tableName) {
            ArrayList<String> ar = null;
            Cursor c = null;
            try {
                Cursor c2 = db.rawQuery("select * from " + tableName + " limit 1", null);
                if (c2 != null) {
                    ar = new ArrayList<>(Arrays.asList(c2.getColumnNames()));
                }
                if (c2 != null) {
                    c2.close();
                }
            } catch (Exception e) {
                Logger.l(e);
                if (c != null) {
                    c.close();
                }
            } catch (Throwable th) {
                if (c != null) {
                    c.close();
                }
                throw th;
            }
            return ar;
        }

        private String join(ArrayList<String> list, String delim) {
            StringBuilder buf = new StringBuilder();
            int num = list.size();
            for (int i = 0; i < num; i++) {
                if (i != 0) {
                    buf.append(delim);
                }
                buf.append(list.get(i));
            }
            return buf.toString();
        }
    }
}
