package com.widgetizeme.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.plus.PlusShare;
import com.widgetizeme.Logger;
import com.widgetizeme.R;

public class GCMBroadcastReceiver extends BroadcastReceiver {
    private static final int NOTIFICATION_ID = 1;

    public void onReceive(Context context, Intent intent) {
        String messageType = GoogleCloudMessaging.getInstance(context).getMessageType(intent);
        Logger.l("onReceive: " + messageType);
        if (!GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType) && !GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
            sendNotification(context, intent.getExtras());
        }
        setResultCode(-1);
    }

    private void sendNotification(Context context, Bundle bundle) {
        try {
            String msg = bundle.getString("alert");
            String link = bundle.getString(PlusShare.KEY_CALL_TO_ACTION_URL);
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(link));
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, 0);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setAutoCancel(true);
            builder.setSmallIcon(R.drawable.ic_launcher);
            builder.setContentTitle(msg);
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(msg));
            builder.setContentText(msg);
            builder.setContentIntent(contentIntent);
            ((NotificationManager) context.getSystemService("notification")).notify(1, builder.build());
        } catch (Exception e) {
            Logger.l(e);
        }
    }
}
