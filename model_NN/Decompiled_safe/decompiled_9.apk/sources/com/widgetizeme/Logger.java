package com.widgetizeme;

public class Logger {
    private static final boolean DEBUG = false;
    private static final int SEVERITY = 5;
    public static final String TAG = "widgetizeme";

    public static void l(String tag, int severity, String str) {
    }

    public static void l(int severity, String str) {
        l(null, severity, str);
    }

    public static void l(String str) {
        l(0, str);
    }

    public static void l(Exception e) {
    }
}
