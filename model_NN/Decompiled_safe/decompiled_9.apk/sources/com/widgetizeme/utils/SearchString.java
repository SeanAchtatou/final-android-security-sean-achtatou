package com.widgetizeme.utils;

public class SearchString {
    public static boolean sDoReverse = false;
    private int m_endIndex;
    private int m_startIndex;
    private String m_string;

    public SearchString() {
        this.m_string = "";
        this.m_startIndex = -1;
        this.m_endIndex = -1;
    }

    public SearchString(String str) {
        this.m_string = str.trim();
        this.m_startIndex = 0;
        this.m_endIndex = str.length();
    }

    public SearchString(String source, int startIndex, int endIndex) {
        this.m_string = source.trim();
        this.m_startIndex = startIndex;
        this.m_endIndex = endIndex;
    }

    public String toString() {
        if (-1 == this.m_startIndex) {
            return "";
        }
        return this.m_string.substring(this.m_startIndex, this.m_endIndex).replaceAll("\\&.*?\\;", "").trim();
    }

    public String getRawString() {
        if (-1 == this.m_startIndex) {
            return "";
        }
        return this.m_string.substring(this.m_startIndex, this.m_endIndex).trim();
    }

    public int getStartIndex() {
        return this.m_startIndex;
    }

    public int getEndIndex() {
        return this.m_endIndex;
    }

    public int length() {
        return this.m_endIndex - this.m_startIndex;
    }

    public boolean isEmpty() {
        return this.m_string == null || this.m_string.length() == 0;
    }

    public boolean isNumStr() {
        boolean isNumChar = true;
        int i = this.m_startIndex;
        while (isNumChar && i < this.m_endIndex) {
            char ch = this.m_string.charAt(i);
            isNumChar = isNumChar(ch) || isWhiteSpace(ch);
            i++;
        }
        return isNumChar;
    }

    public int indexOf(String str, int startIndex) {
        if (startIndex < this.m_startIndex) {
            return -1;
        }
        int index = this.m_string.indexOf(str, startIndex);
        if (index >= this.m_endIndex) {
            return -1;
        }
        return index;
    }

    public boolean contains(String text) {
        int index = this.m_string.indexOf(text, this.m_startIndex);
        return -1 != index && index < this.m_endIndex;
    }

    public SearchString findString(String[] pre, String stop, int startIndex) {
        SearchString result = new SearchString();
        if (startIndex >= this.m_endIndex) {
            startIndex = -1;
        }
        if (startIndex == 0) {
            startIndex = this.m_startIndex;
        }
        int i = 0;
        while (-1 != startIndex && i < pre.length) {
            startIndex = this.m_string.indexOf(pre[i], startIndex);
            if (-1 != startIndex && (startIndex = startIndex + pre[i].length()) >= this.m_endIndex) {
                startIndex = -1;
            }
            i++;
        }
        if (-1 == startIndex) {
            return result;
        }
        int endIndex = this.m_string.length();
        if (stop != null && -1 == (endIndex = this.m_string.indexOf(stop, startIndex))) {
            endIndex = this.m_string.length();
        }
        return new SearchString(this.m_string, startIndex, endIndex);
    }

    public SearchString findString(String pre, String stop, int startIndex) {
        return findString(new String[]{pre}, stop, startIndex);
    }

    public SearchString findString(String pre1, String pre2, String stop, int startIndex) {
        return findString(new String[]{pre1, pre2}, stop, startIndex);
    }

    public SearchString findString(String pre1, String pre2, String pre3, String stop, int startIndex) {
        return findString(new String[]{pre1, pre2, pre3}, stop, startIndex);
    }

    public static String removeHtmlProperties(String text) {
        String retVal = text;
        int startIndex = retVal.indexOf("<");
        while (-1 != startIndex) {
            int endIndex = retVal.indexOf(">", startIndex);
            if (-1 != endIndex) {
                retVal = retVal.replace(retVal.substring(startIndex, endIndex + 1), "");
                startIndex = retVal.indexOf("<", endIndex);
            } else {
                startIndex = -1;
            }
        }
        return retVal.trim();
    }

    public static boolean startsWithDigits(String text) {
        char ch;
        if (text != null && (ch = text.charAt(0)) >= '0' && ch <= '9') {
            return true;
        }
        return false;
    }

    public static boolean hasLetters(String text) {
        if (text == null) {
            return false;
        }
        for (int i = 0; i < text.length(); i++) {
            char ch = text.charAt(i);
            if (ch < '0' || ch > '9') {
                return true;
            }
        }
        return false;
    }

    public static String reverseDigits(String s) {
        if (s == null) {
            return null;
        }
        if (!sDoReverse) {
            return s;
        }
        StringBuilder builder = new StringBuilder();
        int startIndex = 0;
        while (startIndex < s.length()) {
            int endIndex = startIndex;
            if (isNumChar(s.charAt(endIndex))) {
                while (endIndex < s.length() && revChar(s.charAt(endIndex))) {
                    endIndex++;
                }
            }
            if (endIndex != startIndex) {
                char lastChar = s.charAt(endIndex - 1);
                if (',' == lastChar) {
                    endIndex--;
                }
                boolean ignoreRev = false;
                if (' ' == lastChar) {
                    ignoreRev = true;
                }
                if (ignoreRev) {
                    builder.append(s.subSequence(startIndex, endIndex));
                } else {
                    builder.append(getReverse(s, startIndex, endIndex));
                }
                startIndex = endIndex;
            } else {
                builder.append(s.charAt(startIndex));
                startIndex++;
            }
        }
        return builder.toString();
    }

    public static String getReverse(String source, int startIndex, int endIndex) {
        if (source == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = endIndex - 1; i >= startIndex; i--) {
            sb.append(source.charAt(i));
        }
        return sb.toString();
    }

    public static boolean hasHebChars(String s) {
        if (s == null) {
            return false;
        }
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch >= 1488 && ch <= 1514) {
                return true;
            }
        }
        return false;
    }

    public static boolean hasOnlyHebChars(String s) {
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch < 1488 || ch > 1514) {
                return false;
            }
        }
        return true;
    }

    private static boolean revChar(char ch) {
        return isNumChar(ch) || ':' == ch || ',' == ch || '.' == ch || '%' == ch || 'x' == ch || 'X' == ch || '-' == ch;
    }

    private static boolean isNumChar(char ch) {
        return ch >= '0' && ch <= '9';
    }

    private boolean isWhiteSpace(char ch) {
        return ch == ' ' || ch == 10 || ch == 't';
    }
}
