package com.widgetizeme.utils;

import android.content.Context;
import android.text.TextUtils;
import com.widgetizeme.R;

public class StringUtils {
    public static String fixToXML(String text) {
        if (!TextUtils.isEmpty(text)) {
            return text.replace("<", "&lt;").replace(">", "&gt;").replace("&", "&amp;").replace("\\", "&apos;").replace("\"", "&quot;");
        }
        return text;
    }

    public static String fromXML(String text) {
        if (!TextUtils.isEmpty(text)) {
            return text.replace("&lt;", "<").replace("&gt;", ">").replace("&amp;", "&").replace("&apos;", "\\").replace("&quot;", "\"");
        }
        return text;
    }

    public static String getTimeString(Context context, long time, boolean isHeb) {
        long delta = System.currentTimeMillis() - time;
        int days = (int) (delta / 86400000);
        if (1 == days) {
            return getTimeStringResource(context, R.string.one_day_ago, 0, isHeb);
        }
        if (days > 1) {
            return getTimeStringResource(context, R.string.x_days_ago, days, isHeb);
        }
        long delta2 = delta - (((long) days) * 86400000);
        int hours = (int) (delta2 / 3600000);
        if (1 == hours) {
            return getTimeStringResource(context, R.string.one_hour_ago, 0, isHeb);
        }
        if (hours > 1) {
            return getTimeStringResource(context, R.string.x_hours_ago, hours, isHeb);
        }
        int minutes = (int) ((delta2 - (((long) hours) * 3600000)) / 60000);
        if (1 == minutes) {
            return getTimeStringResource(context, R.string.one_minute_ago, 0, isHeb);
        }
        if (minutes > 1) {
            return getTimeStringResource(context, R.string.x_minutes_ago, minutes, isHeb);
        }
        return getTimeStringResource(context, R.string.just_now, 0, isHeb);
    }

    private static String getTimeStringResource(Context context, int res, int value, boolean isHeb) {
        if (isHeb) {
            if (R.string.one_day_ago == res) {
                res = R.string.one_day_ago_hb;
            } else if (R.string.x_days_ago == res) {
                res = R.string.x_days_ago_hb;
            } else if (R.string.one_hour_ago == res) {
                res = R.string.one_hour_ago_hb;
            } else if (R.string.x_hours_ago == res) {
                res = R.string.x_hours_ago_hb;
            } else if (R.string.one_minute_ago == res) {
                res = R.string.one_minute_ago_hb;
            } else if (R.string.x_minutes_ago == res) {
                res = R.string.x_minutes_ago_hb;
            } else {
                res = R.string.just_now_hb;
            }
        }
        String retVal = context.getResources().getString(res);
        if (value > 0) {
            return retVal.replace("X", String.valueOf(value));
        }
        return retVal;
    }
}
