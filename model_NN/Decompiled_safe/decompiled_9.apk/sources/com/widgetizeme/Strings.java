package com.widgetizeme;

import android.content.Context;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class Strings {
    private static Strings sInstance;
    private HashMap<Integer, String[]> mArrays = new HashMap<>();
    private Context mContext;
    private HashMap<Integer, String> mStrings = new HashMap<>();

    public static void init(Context context) {
        sInstance = new Strings(context);
    }

    public static Strings get() {
        return sInstance;
    }

    public String getString(int id) {
        String retVal = this.mStrings.get(Integer.valueOf(id));
        if (retVal == null) {
            return this.mContext.getString(id);
        }
        return retVal;
    }

    public String[] getStringArray(int id) {
        String[] retVal = this.mArrays.get(Integer.valueOf(id));
        if (retVal == null) {
            return this.mContext.getResources().getStringArray(id);
        }
        return retVal;
    }

    public boolean isLoaded() {
        return !this.mStrings.isEmpty();
    }

    private Strings(Context context) {
        this.mContext = context;
        loadStrings();
        Logger.l("strings: " + this.mStrings.size());
    }

    private void loadStrings() {
        try {
            FileInputStream input = this.mContext.openFileInput("strings.xml");
            try {
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser xpp = factory.newPullParser();
                xpp.setInput(input, "utf-8");
                parseXML(xpp);
            } catch (Exception e) {
                Logger.l(e);
            }
            input.close();
        } catch (Exception e2) {
        }
    }

    private int getStringId(String type, String name) {
        return this.mContext.getResources().getIdentifier(name, type, this.mContext.getPackageName());
    }

    private void parseXML(XmlPullParser xpp) {
        while (1 != xpp.getEventType()) {
            try {
                String tag = xpp.getName();
                if (2 == xpp.getEventType()) {
                    if (tag.equals("string")) {
                        String name = xpp.getAttributeValue(null, "name");
                        xpp.next();
                        this.mStrings.put(Integer.valueOf(getStringId("string", name)), xpp.getText());
                    } else if (tag.equals("string-array")) {
                        String name2 = xpp.getAttributeValue(null, "name");
                        this.mArrays.put(Integer.valueOf(getStringId("array", name2)), parseArray(xpp));
                    }
                }
                xpp.next();
            } catch (Exception e) {
                Logger.l(e);
                return;
            }
        }
    }

    private String[] parseArray(XmlPullParser xpp) {
        ArrayList<String> array = new ArrayList<>();
        boolean done = false;
        while (!done) {
            try {
                if (1 == xpp.getEventType()) {
                    break;
                }
                String tag = xpp.getName();
                if (2 == xpp.getEventType()) {
                    if (tag.equals("item")) {
                        xpp.next();
                        array.add(xpp.getText());
                    }
                } else if (3 == xpp.getEventType()) {
                    done = xpp.getName().equals("string-array");
                }
                if (!done) {
                    xpp.next();
                }
            } catch (Exception e) {
                Logger.l(e);
            }
        }
        String[] retVal = new String[array.size()];
        array.toArray(retVal);
        return retVal;
    }
}
