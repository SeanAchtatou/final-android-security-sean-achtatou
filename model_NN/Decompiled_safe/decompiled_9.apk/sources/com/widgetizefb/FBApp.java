package com.widgetizefb;

import com.widgetizeme.App;
import com.widgetizeme.Logger;
import com.widgetizeme.Pref;
import com.widgetizeme.UnsentStats;
import com.widgetizeme.rss.RSSFeed;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class FBApp extends App {
    /* access modifiers changed from: protected */
    public void addCustomeFeeds(ArrayList<RSSFeed> feeds) {
        try {
            JSONArray groups = new JSONArray(Pref.getPrefString("selected_groups"));
            for (int i = 0; i < groups.length(); i++) {
                JSONObject group = groups.getJSONObject(i);
                feeds.add(new GroupFeed(this, new FacebookGroup(group.getString(UnsentStats.KEY_ID), group.getString("name"), group.getString("image"))));
            }
        } catch (Exception e) {
            Logger.l(e);
        }
    }
}
