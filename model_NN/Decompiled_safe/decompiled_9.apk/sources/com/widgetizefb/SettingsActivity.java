package com.widgetizefb;

import android.app.Activity;
import android.app.AlertDialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.squareup.picasso.Picasso;
import com.widgetizeme.App;
import com.widgetizeme.Logger;
import com.widgetizeme.Pref;
import com.widgetizeme.UnsentStats;
import com.widgetizeme.widget.WMButtonBroadcast;
import com.widgetizeme.widget.WMWidgetProvider;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class SettingsActivity extends Activity {
    /* access modifiers changed from: private */
    public int mAppWidgetId;
    private View mGroupsLayer;
    /* access modifiers changed from: private */
    public View mProgress;
    private TextView mSelectText;
    /* access modifiers changed from: private */
    public ArrayList<FacebookGroup> mSelectedGroups;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_settings);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.mAppWidgetId = extras.getInt("appWidgetId", 0);
        }
        this.mProgress = findViewById(R.id.progress);
        this.mGroupsLayer = findViewById(R.id.groups_layer);
        View connectLayer = findViewById(R.id.connect_layer);
        this.mSelectText = (TextView) findViewById(R.id.select_text);
        this.mSelectedGroups = getSelectedGroups();
        if (Pref.getPrefBoolean("is_connected")) {
            connectLayer.setVisibility(8);
            if (Session.getActiveSession() == null || !Session.getActiveSession().isOpened()) {
                connect();
            } else if (this.mAppWidgetId == 0 || this.mSelectedGroups.isEmpty()) {
                loadGroups();
            } else {
                refreshAllWidgets();
                finish();
            }
        } else {
            connectLayer.setVisibility(0);
            connectLayer.findViewById(R.id.connect).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    SettingsActivity.this.connect();
                }
            });
        }
        findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!SettingsActivity.this.mSelectedGroups.isEmpty()) {
                    SettingsActivity.this.saveSelectedGroups();
                    SettingsActivity.this.refreshAllWidgets();
                    if (8 == SettingsActivity.this.findViewById(R.id.connect_layer).getVisibility() && SettingsActivity.this.mAppWidgetId == 0 && !Pref.getPrefBoolean("seen_finish_dialog")) {
                        Pref.setPrefBoolean("seen_finish_dialog", true);
                        SettingsActivity.this.showFinishAlert();
                        return;
                    }
                    SettingsActivity.this.finish();
                }
            }
        });
        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingsActivity.this.finish();
            }
        });
        App.sendInstallStat(this);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Session.getActiveSession() != null) {
            Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
        }
    }

    private void showSelectAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(17301659);
        builder.setTitle((int) R.string.select_dialog_title);
        builder.setMessage((int) R.string.select_dialog_message);
        builder.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();
    }

    /* access modifiers changed from: private */
    public void showFinishAlert() {
        startActivity(new Intent(this, TutorialActivity.class));
        finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.Session.openActiveSession(android.app.Activity, boolean, java.util.List<java.lang.String>, com.facebook.Session$StatusCallback):com.facebook.Session
     arg types: [com.widgetizefb.SettingsActivity, int, java.util.ArrayList<java.lang.String>, com.widgetizefb.SettingsActivity$5]
     candidates:
      com.facebook.Session.openActiveSession(android.content.Context, android.support.v4.app.Fragment, boolean, com.facebook.Session$StatusCallback):com.facebook.Session
      com.facebook.Session.openActiveSession(android.app.Activity, boolean, java.util.List<java.lang.String>, com.facebook.Session$StatusCallback):com.facebook.Session */
    /* access modifiers changed from: private */
    public void connect() {
        this.mProgress.setVisibility(0);
        findViewById(R.id.connect_layer).setVisibility(8);
        ArrayList<String> permissions = new ArrayList<>();
        permissions.add("user_groups");
        Session.openActiveSession((Activity) this, true, (List<String>) permissions, (Session.StatusCallback) new Session.StatusCallback() {
            public void call(Session session, SessionState state, Exception exception) {
                if (session != null && session.isOpened()) {
                    SettingsActivity.this.mProgress.setVisibility(8);
                    Pref.setPrefBoolean("is_connected", true);
                    SettingsActivity.this.loadGroups();
                } else if (exception != null) {
                    boolean z = exception instanceof FacebookOperationCanceledException;
                    SettingsActivity.this.findViewById(R.id.connect_layer).setVisibility(0);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void loadGroups() {
        this.mProgress.setVisibility(0);
        try {
            Bundle params = new Bundle();
            params.putString("fields", "name,icon");
            new Request(Session.getActiveSession(), "/me/groups", params, null, new Request.Callback() {
                ArrayList<FacebookGroup> groups = new ArrayList<>();

                public void onCompleted(Response response) {
                    if (response.getError() == null) {
                        try {
                            JSONArray data = response.getGraphObject().getInnerJSONObject().getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject item = data.getJSONObject(i);
                                String id = item.getString(UnsentStats.KEY_ID);
                                String name = item.getString("name");
                                String imageURL = "";
                                if (item.has("icon")) {
                                    imageURL = item.getString("icon");
                                }
                                this.groups.add(new FacebookGroup(id, name, imageURL));
                            }
                        } catch (Exception e) {
                            Logger.l(e);
                        }
                    }
                    SettingsActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            SettingsActivity.this.onLoadingGroupsDone(AnonymousClass6.this.groups);
                        }
                    });
                }
            }).executeAsync();
        } catch (Exception e) {
            Logger.l(e);
        }
    }

    /* access modifiers changed from: private */
    public void onLoadingGroupsDone(ArrayList<FacebookGroup> groups) {
        this.mProgress.setVisibility(8);
        this.mGroupsLayer.setVisibility(0);
        ((ListView) findViewById(R.id.list)).setAdapter((ListAdapter) new GroupListAdapter(this, groups));
        updateSelectedText();
        if (8 == findViewById(R.id.connect_layer).getVisibility() && this.mAppWidgetId == 0 && !Pref.getPrefBoolean("seen_select_dialog")) {
            Pref.setPrefBoolean("seen_select_dialog", true);
            showSelectAlert();
        }
    }

    /* access modifiers changed from: private */
    public boolean addSelectedGroup(FacebookGroup group) {
        if (isGroupSelected(group)) {
            return true;
        }
        if (3 == this.mSelectedGroups.size()) {
            return false;
        }
        this.mSelectedGroups.add(group);
        updateSelectedText();
        return true;
    }

    /* access modifiers changed from: private */
    public void removeSelectedGroup(FacebookGroup group) {
        for (int i = 0; i < this.mSelectedGroups.size(); i++) {
            if (this.mSelectedGroups.get(i).getId().equals(group.getId())) {
                this.mSelectedGroups.remove(i);
                updateSelectedText();
                return;
            }
        }
    }

    private void updateSelectedText() {
        this.mSelectText.setText(String.valueOf(this.mSelectedGroups.size()) + "\\3 groups selected");
    }

    /* access modifiers changed from: private */
    public boolean isGroupSelected(FacebookGroup group) {
        for (int i = 0; i < this.mSelectedGroups.size(); i++) {
            if (this.mSelectedGroups.get(i).getId().equals(group.getId())) {
                return true;
            }
        }
        return false;
    }

    private ArrayList<FacebookGroup> getSelectedGroups() {
        ArrayList<FacebookGroup> retVal = new ArrayList<>();
        try {
            String cache = Pref.getPrefString("selected_groups");
            if (!TextUtils.isEmpty(cache)) {
                JSONArray groups = new JSONArray(cache);
                for (int i = 0; i < groups.length(); i++) {
                    JSONObject group = groups.getJSONObject(i);
                    retVal.add(new FacebookGroup(group.getString(UnsentStats.KEY_ID), group.getString("name"), group.getString("image")));
                }
            }
        } catch (Exception e) {
            Logger.l(e);
        }
        return retVal;
    }

    /* access modifiers changed from: private */
    public void saveSelectedGroups() {
        String names = "";
        try {
            JSONArray groups = new JSONArray();
            for (int i = 0; i < this.mSelectedGroups.size(); i++) {
                FacebookGroup g = this.mSelectedGroups.get(i);
                JSONObject group = new JSONObject();
                group.put(UnsentStats.KEY_ID, g.getId());
                group.put("name", g.getName());
                group.put("image", g.getImageURL());
                groups.put(group);
                String name = g.getName();
                if (name.length() > 12) {
                    name = String.valueOf(name.substring(0, 10)) + "...";
                }
                names = String.valueOf(names) + name;
                if (i + 1 < this.mSelectedGroups.size()) {
                    names = String.valueOf(names) + "~";
                }
            }
            Pref.setPrefString("selected_groups", groups.toString());
        } catch (Exception e) {
            Logger.l(e);
        }
        Pref.setPrefString(Pref.FEEDS_NAMES, names);
    }

    /* access modifiers changed from: private */
    public void refreshAllWidgets() {
        Logger.l("refreshAllWidgets");
        if (this.mAppWidgetId == 0) {
            Intent intent = new Intent(this, WMWidgetProvider.class);
            intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
            intent.putExtra("appWidgetIds", AppWidgetManager.getInstance(this).getAppWidgetIds(new ComponentName(this, WMWidgetProvider.class)));
            sendBroadcast(intent);
            return;
        }
        Intent resultValue = new Intent();
        resultValue.putExtra("appWidgetId", this.mAppWidgetId);
        setResult(-1, resultValue);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Intent intent = new Intent(SettingsActivity.this, WMButtonBroadcast.class);
                intent.putExtra(WMButtonBroadcast.EXTRA_ACTION, 0);
                intent.putExtra("widget_id", SettingsActivity.this.mAppWidgetId);
                intent.setData(Uri.parse("abcd://widget/" + SettingsActivity.this.mAppWidgetId));
                SettingsActivity.this.sendBroadcast(intent);
            }
        }, 1000);
    }

    private class GroupListAdapter extends BaseAdapter {
        private ArrayList<FacebookGroup> mGroups;
        private LayoutInflater mInflater;

        private class ViewHolder {
            /* access modifiers changed from: private */
            public CheckBox mCheck;
            /* access modifiers changed from: private */
            public ImageView mImage;
            /* access modifiers changed from: private */
            public TextView mName;

            private ViewHolder() {
            }

            /* synthetic */ ViewHolder(GroupListAdapter groupListAdapter, ViewHolder viewHolder) {
                this();
            }
        }

        public GroupListAdapter(Context context, ArrayList<FacebookGroup> groups) {
            this.mInflater = LayoutInflater.from(context);
            this.mGroups = groups;
        }

        public int getCount() {
            return this.mGroups.size();
        }

        public Object getItem(int position) {
            return this.mGroups.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, ?[OBJECT, ARRAY], int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.list_item_group, (ViewGroup) null, true);
                holder = new ViewHolder(this, null);
                holder.mName = (TextView) convertView.findViewById(R.id.name);
                holder.mImage = (ImageView) convertView.findViewById(R.id.image);
                holder.mCheck = (CheckBox) convertView.findViewById(R.id.check);
                holder.mCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        FacebookGroup group = (FacebookGroup) buttonView.getTag();
                        if (!isChecked) {
                            SettingsActivity.this.removeSelectedGroup(group);
                        } else if (!SettingsActivity.this.addSelectedGroup(group)) {
                            buttonView.setChecked(false);
                        }
                    }
                });
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            FacebookGroup group = this.mGroups.get(position);
            holder.mName.setText(group.getName());
            holder.mCheck.setTag(group);
            holder.mCheck.setChecked(SettingsActivity.this.isGroupSelected(group));
            if (group.hasImage()) {
                Picasso.with(SettingsActivity.this.getApplicationContext()).load(group.getImageURL()).placeholder((int) R.drawable.group_icon).into(holder.mImage);
            } else {
                holder.mImage.setImageResource(R.drawable.group_icon);
            }
            return convertView;
        }
    }
}
