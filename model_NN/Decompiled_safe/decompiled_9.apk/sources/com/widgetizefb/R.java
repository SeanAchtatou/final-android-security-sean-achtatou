package com.widgetizefb;

public final class R {

    public static final class array {
        public static final int days = 2131361793;
        public static final int feeds = 2131361794;
        public static final int feeds_names = 2131361795;
        public static final int months = 2131361792;
    }

    public static final class attr {
        public static final int confirm_logout = 2130771980;
        public static final int done_button_background = 2130771974;
        public static final int done_button_text = 2130771972;
        public static final int extra_fields = 2130771969;
        public static final int fetch_user_info = 2130771981;
        public static final int is_cropped = 2130771985;
        public static final int login_text = 2130771982;
        public static final int logout_text = 2130771983;
        public static final int multi_select = 2130771975;
        public static final int preset_size = 2130771984;
        public static final int radius_in_meters = 2130771976;
        public static final int results_limit = 2130771977;
        public static final int search_text = 2130771978;
        public static final int show_pictures = 2130771968;
        public static final int show_search_box = 2130771979;
        public static final int show_title_bar = 2130771970;
        public static final int title_bar_background = 2130771973;
        public static final int title_text = 2130771971;
    }

    public static final class color {
        public static final int category_text = 2131165212;
        public static final int category_text_selected = 2131165213;
        public static final int com_facebook_blue = 2131165184;
        public static final int com_facebook_loginview_text_color = 2131165188;
        public static final int com_facebook_usersettingsfragment_connected_shadow_color = 2131165186;
        public static final int com_facebook_usersettingsfragment_connected_text_color = 2131165185;
        public static final int com_facebook_usersettingsfragment_not_connected_text_color = 2131165187;
        public static final int empty_list = 2131165194;
        public static final int footer_bg_wr = 2131165200;
        public static final int footer_gradient_end = 2131165204;
        public static final int footer_gradient_start = 2131165203;
        public static final int guide_bg = 2131165216;
        public static final int guide_text = 2131165219;
        public static final int guide_text_bg = 2131165218;
        public static final int guide_title = 2131165217;
        public static final int header_bg_wr = 2131165199;
        public static final int header_gradient_end = 2131165202;
        public static final int header_gradient_start = 2131165201;
        public static final int header_recommendations_bg = 2131165210;
        public static final int item_desc = 2131165193;
        public static final int item_time = 2131165192;
        public static final int item_title = 2131165191;
        public static final int mini_widget_bg = 2131165214;
        public static final int powered_by = 2131165196;
        public static final int recommmend_header_text = 2131165211;
        public static final int refresh_button_end = 2131165206;
        public static final int refresh_button_start = 2131165205;
        public static final int select_color_legacy = 2131165198;
        public static final int semi_transparent = 2131165215;
        public static final int share_button_end = 2131165208;
        public static final int share_button_start = 2131165207;
        public static final int transperent = 2131165197;
        public static final int widget_bg = 2131165189;
        public static final int widget_border = 2131165190;
        public static final int widget_date = 2131165195;
        public static final int widget_title = 2131165209;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131230732;
        public static final int activity_vertical_margin = 2131230733;
        public static final int category_text = 2131230747;
        public static final int com_facebook_loginview_height = 2131230727;
        public static final int com_facebook_loginview_padding_bottom = 2131230725;
        public static final int com_facebook_loginview_padding_left = 2131230722;
        public static final int com_facebook_loginview_padding_right = 2131230723;
        public static final int com_facebook_loginview_padding_top = 2131230724;
        public static final int com_facebook_loginview_text_size = 2131230728;
        public static final int com_facebook_loginview_width = 2131230726;
        public static final int com_facebook_profilepictureview_preset_size_large = 2131230731;
        public static final int com_facebook_profilepictureview_preset_size_normal = 2131230730;
        public static final int com_facebook_profilepictureview_preset_size_small = 2131230729;
        public static final int com_facebook_usersettingsfragment_profile_picture_height = 2131230721;
        public static final int com_facebook_usersettingsfragment_profile_picture_width = 2131230720;
        public static final int empty_list = 2131230743;
        public static final int item_desc = 2131230741;
        public static final int item_desc_wr = 2131230742;
        public static final int item_padding_v = 2131230748;
        public static final int item_time = 2131230738;
        public static final int item_time_mini = 2131230740;
        public static final int item_time_wr = 2131230739;
        public static final int item_title = 2131230735;
        public static final int item_title_mini = 2131230737;
        public static final int item_title_wr = 2131230736;
        public static final int powered_by = 2131230745;
        public static final int widget_date = 2131230744;
        public static final int widget_margin = 2131230734;
        public static final int widget_title = 2131230746;
    }

    public static final class drawable {
        public static final int arrow = 2130837504;
        public static final int arrow_disable_l = 2130837505;
        public static final int arrow_disable_r = 2130837506;
        public static final int arrow_rtl = 2130837507;
        public static final int arrow_white_l = 2130837508;
        public static final int arrow_white_r = 2130837509;
        public static final int cancel_btn = 2130837510;
        public static final int checkbox = 2130837511;
        public static final int circle = 2130837512;
        public static final int clock = 2130837513;
        public static final int close_button = 2130837514;
        public static final int com_facebook_button_check = 2130837515;
        public static final int com_facebook_button_check_off = 2130837516;
        public static final int com_facebook_button_check_on = 2130837517;
        public static final int com_facebook_button_grey_focused = 2130837518;
        public static final int com_facebook_button_grey_normal = 2130837519;
        public static final int com_facebook_button_grey_pressed = 2130837520;
        public static final int com_facebook_close = 2130837521;
        public static final int com_facebook_icon = 2130837522;
        public static final int com_facebook_list_divider = 2130837523;
        public static final int com_facebook_list_section_header_background = 2130837524;
        public static final int com_facebook_loginbutton_blue = 2130837525;
        public static final int com_facebook_loginbutton_blue_focused = 2130837526;
        public static final int com_facebook_loginbutton_blue_normal = 2130837527;
        public static final int com_facebook_loginbutton_blue_pressed = 2130837528;
        public static final int com_facebook_loginbutton_silver = 2130837529;
        public static final int com_facebook_logo = 2130837530;
        public static final int com_facebook_picker_item_background = 2130837531;
        public static final int com_facebook_picker_list_focused = 2130837532;
        public static final int com_facebook_picker_list_longpressed = 2130837533;
        public static final int com_facebook_picker_list_pressed = 2130837534;
        public static final int com_facebook_picker_list_selector = 2130837535;
        public static final int com_facebook_picker_list_selector_background_transition = 2130837536;
        public static final int com_facebook_picker_list_selector_disabled = 2130837537;
        public static final int com_facebook_picker_top_button = 2130837538;
        public static final int com_facebook_place_default_icon = 2130837539;
        public static final int com_facebook_profile_default_icon = 2130837540;
        public static final int com_facebook_profile_picture_blank_portrait = 2130837541;
        public static final int com_facebook_profile_picture_blank_square = 2130837542;
        public static final int com_facebook_top_background = 2130837543;
        public static final int com_facebook_top_button = 2130837544;
        public static final int com_facebook_usersettingsfragment_background_gradient = 2130837545;
        public static final int default_icon = 2130837546;
        public static final int empty_image = 2130837547;
        public static final int example1 = 2130837548;
        public static final int example2 = 2130837549;
        public static final int example3 = 2130837550;
        public static final int example_dialog_widgetizeme = 2130837551;
        public static final int facebook_button = 2130837552;
        public static final int fb_normal = 2130837553;
        public static final int fb_pressed = 2130837554;
        public static final int finish_btn = 2130837555;
        public static final int flip_image_overlay = 2130837556;
        public static final int footer_bg = 2130837557;
        public static final int group_icon = 2130837558;
        public static final int group_icon_empty = 2130837559;
        public static final int guide_1 = 2130837560;
        public static final int guide_1_icon = 2130837561;
        public static final int guide_2 = 2130837562;
        public static final int guide_2_logo = 2130837563;
        public static final int guide_3 = 2130837564;
        public static final int guide_3_logo = 2130837565;
        public static final int guide_logo = 2130837566;
        public static final int header = 2130837567;
        public static final int header_bg = 2130837568;
        public static final int header_button_flip = 2130837569;
        public static final int ic_launcher = 2130837570;
        public static final int like_comment_follow = 2130837571;
        public static final int logo = 2130837572;
        public static final int logo_bg = 2130837573;
        public static final int nav_button = 2130837574;
        public static final int nav_button_normal = 2130837575;
        public static final int nav_button_pressed = 2130837576;
        public static final int next = 2130837577;
        public static final int next_arrow = 2130837578;
        public static final int offers_icon = 2130837579;
        public static final int outbrain = 2130837580;
        public static final int page_black = 2130837581;
        public static final int page_white = 2130837582;
        public static final int pager_bg = 2130837583;
        public static final int prev = 2130837584;
        public static final int prev_arrow = 2130837585;
        public static final int read_more = 2130837586;
        public static final int recommend_service_icon = 2130837587;
        public static final int recommendation_icon = 2130837588;
        public static final int recommendation_icon_selected = 2130837589;
        public static final int refresh = 2130837590;
        public static final int refresh_bg = 2130837591;
        public static final int refresh_button = 2130837592;
        public static final int refresh_button_normal = 2130837593;
        public static final int refresh_button_pressed = 2130837594;
        public static final int refresh_wr = 2130837595;
        public static final int share = 2130837596;
        public static final int share_button = 2130837597;
        public static final int share_button_normal = 2130837598;
        public static final int share_button_pressed = 2130837599;
        public static final int share_button_wr = 2130837600;
        public static final int taboola = 2130837601;
        public static final int user_image = 2130837602;
        public static final int v = 2130837603;
        public static final int v_empty = 2130837604;
        public static final int welcome_logo = 2130837605;
        public static final int widget_bg = 2130837606;
        public static final int widget_footer_bg = 2130837607;
        public static final int widget_header_bg = 2130837608;
        public static final int widget_item_bg_legacy = 2130837609;
        public static final int widget_mini_bg = 2130837610;
        public static final int widget_mini_bottom = 2130837611;
        public static final int widget_preview = 2130837612;
    }

    public static final class id {
        public static final int by = 2131034185;
        public static final int by_label = 2131034186;
        public static final int cancel = 2131034127;
        public static final int categories_panel = 2131034196;
        public static final int category_1 = 2131034197;
        public static final int category_2 = 2131034198;
        public static final int category_3 = 2131034199;
        public static final int check = 2131034154;
        public static final int clock = 2131034184;
        public static final int close = 2131034119;
        public static final int com_facebook_login_activity_progress_bar = 2131034134;
        public static final int com_facebook_picker_activity_circle = 2131034133;
        public static final int com_facebook_picker_checkbox = 2131034136;
        public static final int com_facebook_picker_checkbox_stub = 2131034140;
        public static final int com_facebook_picker_divider = 2131034144;
        public static final int com_facebook_picker_done_button = 2131034143;
        public static final int com_facebook_picker_image = 2131034137;
        public static final int com_facebook_picker_list_section_header = 2131034141;
        public static final int com_facebook_picker_list_view = 2131034132;
        public static final int com_facebook_picker_profile_pic_stub = 2131034138;
        public static final int com_facebook_picker_row_activity_circle = 2131034135;
        public static final int com_facebook_picker_title = 2131034139;
        public static final int com_facebook_picker_title_bar = 2131034146;
        public static final int com_facebook_picker_title_bar_stub = 2131034145;
        public static final int com_facebook_picker_top_bar = 2131034142;
        public static final int com_facebook_placepickerfragment_search_box_stub = 2131034147;
        public static final int com_facebook_usersettingsfragment_login_button = 2131034152;
        public static final int com_facebook_usersettingsfragment_logo_image = 2131034150;
        public static final int com_facebook_usersettingsfragment_profile_name = 2131034151;
        public static final int connect = 2131034131;
        public static final int connect_layer = 2131034130;
        public static final int count = 2131034177;
        public static final int date = 2131034157;
        public static final int desc = 2131034163;
        public static final int empty_view = 2131034168;
        public static final int footer_panel = 2131034180;
        public static final int groups_layer = 2131034125;
        public static final int guide_1 = 2131034120;
        public static final int guide_2_a = 2131034121;
        public static final int guide_2_b = 2131034122;
        public static final int guide_3 = 2131034124;
        public static final int guide_3_title = 2131034123;
        public static final int header = 2131034171;
        public static final int icon = 2131034156;
        public static final int image = 2131034118;
        public static final int item = 2131034155;
        public static final int item_text = 2131034174;
        public static final int items = 2131034187;
        public static final int large = 2131034114;
        public static final int list = 2131034126;
        public static final int logo = 2131034165;
        public static final int logo_bg = 2131034164;
        public static final int name = 2131034153;
        public static final int next = 2131034178;
        public static final int normal = 2131034113;
        public static final int offers = 2131034166;
        public static final int ok = 2131034129;
        public static final int page1 = 2131034188;
        public static final int page2 = 2131034189;
        public static final int page3 = 2131034190;
        public static final int page4 = 2131034191;
        public static final int page5 = 2131034192;
        public static final int pager = 2131034175;
        public static final int picker_subtitle = 2131034149;
        public static final int powered_by = 2131034170;
        public static final int prev = 2131034176;
        public static final int progress = 2131034116;
        public static final int read_more = 2131034179;
        public static final int recommend_text = 2131034195;
        public static final int recommendation_panel = 2131034193;
        public static final int recommendations = 2131034173;
        public static final int recommendations_service_icon = 2131034194;
        public static final int refresh = 2131034169;
        public static final int search_box = 2131034148;
        public static final int select_text = 2131034128;
        public static final int share = 2131034167;
        public static final int small = 2131034112;
        public static final int star1 = 2131034158;
        public static final int star2 = 2131034159;
        public static final int star3 = 2131034160;
        public static final int star4 = 2131034161;
        public static final int star5 = 2131034162;
        public static final int subtitle = 2131034183;
        public static final int time = 2131034182;
        public static final int title = 2131034117;
        public static final int update_text = 2131034181;
        public static final int webview = 2131034115;
        public static final int widget_title = 2131034172;
    }

    public static final class layout {
        public static final int activity_ad_wall = 2130903040;
        public static final int activity_example = 2130903041;
        public static final int activity_guide = 2130903042;
        public static final int activity_install = 2130903043;
        public static final int activity_settings = 2130903044;
        public static final int activity_tutorial = 2130903045;
        public static final int activity_view = 2130903046;
        public static final int com_facebook_friendpickerfragment = 2130903047;
        public static final int com_facebook_login_activity_layout = 2130903048;
        public static final int com_facebook_picker_activity_circle_row = 2130903049;
        public static final int com_facebook_picker_checkbox = 2130903050;
        public static final int com_facebook_picker_image = 2130903051;
        public static final int com_facebook_picker_list_row = 2130903052;
        public static final int com_facebook_picker_list_section_header = 2130903053;
        public static final int com_facebook_picker_search_box = 2130903054;
        public static final int com_facebook_picker_title_bar = 2130903055;
        public static final int com_facebook_picker_title_bar_stub = 2130903056;
        public static final int com_facebook_placepickerfragment = 2130903057;
        public static final int com_facebook_placepickerfragment_list_row = 2130903058;
        public static final int com_facebook_usersettingsfragment = 2130903059;
        public static final int list_item_feed = 2130903060;
        public static final int list_item_group = 2130903061;
        public static final int offer_item = 2130903062;
        public static final int widget = 2130903063;
        public static final int widget_flat = 2130903064;
        public static final int widget_flat_rtl = 2130903065;
        public static final int widget_flat_rtl_gravityfix = 2130903066;
        public static final int widget_flip = 2130903067;
        public static final int widget_flip_rtl = 2130903068;
        public static final int widget_flip_rtl_gravityfix = 2130903069;
        public static final int widget_item = 2130903070;
        public static final int widget_item_div = 2130903071;
        public static final int widget_item_rtl = 2130903072;
        public static final int widget_item_rtl_gravityfix = 2130903073;
        public static final int widget_item_rtl_gravityfix_wr = 2130903074;
        public static final int widget_item_rtl_wr = 2130903075;
        public static final int widget_item_wr = 2130903076;
        public static final int widget_item_wr_fb = 2130903077;
        public static final int widget_item_wr_lagacy = 2130903078;
        public static final int widget_legacy = 2130903079;
        public static final int widget_mini = 2130903080;
        public static final int widget_mini_rtl = 2130903081;
        public static final int widget_mini_rtl_gravityfix = 2130903082;
        public static final int widget_single_item = 2130903083;
        public static final int widget_single_item_rtl = 2130903084;
        public static final int widget_single_item_rtl_gravityfix = 2130903085;
        public static final int widget_wr = 2130903086;
        public static final int widget_wr_legacy = 2130903087;
    }

    public static final class string {
        public static final int app_id = 2131099736;
        public static final int app_name = 2131099673;
        public static final int brand_name = 2131099682;
        public static final int by = 2131099680;
        public static final int by_heb = 2131099681;
        public static final int com_facebook_choose_friends = 2131099663;
        public static final int com_facebook_dialogloginactivity_ok_button = 2131099648;
        public static final int com_facebook_internet_permission_error_message = 2131099667;
        public static final int com_facebook_internet_permission_error_title = 2131099666;
        public static final int com_facebook_loading = 2131099665;
        public static final int com_facebook_loginview_cancel_action = 2131099654;
        public static final int com_facebook_loginview_log_in_button = 2131099650;
        public static final int com_facebook_loginview_log_out_action = 2131099653;
        public static final int com_facebook_loginview_log_out_button = 2131099649;
        public static final int com_facebook_loginview_logged_in_as = 2131099651;
        public static final int com_facebook_loginview_logged_in_using_facebook = 2131099652;
        public static final int com_facebook_logo_content_description = 2131099655;
        public static final int com_facebook_nearby = 2131099664;
        public static final int com_facebook_picker_done_button_text = 2131099662;
        public static final int com_facebook_placepicker_subtitle_catetory_only_format = 2131099660;
        public static final int com_facebook_placepicker_subtitle_format = 2131099659;
        public static final int com_facebook_placepicker_subtitle_were_here_only_format = 2131099661;
        public static final int com_facebook_requesterror_password_changed = 2131099670;
        public static final int com_facebook_requesterror_permissions = 2131099672;
        public static final int com_facebook_requesterror_reconnect = 2131099671;
        public static final int com_facebook_requesterror_relogin = 2131099669;
        public static final int com_facebook_requesterror_web_login = 2131099668;
        public static final int com_facebook_usersettingsfragment_log_in_button = 2131099656;
        public static final int com_facebook_usersettingsfragment_logged_in = 2131099657;
        public static final int com_facebook_usersettingsfragment_not_logged_in = 2131099658;
        public static final int comment = 2131099689;
        public static final int comment_heb = 2131099692;
        public static final int connect = 2131099734;
        public static final int connect_message = 2131099733;
        public static final int default_share_text = 2131099677;
        public static final int empty_view_text = 2131099674;
        public static final int feeds_selected = 2131099722;
        public static final int finish_dialog_message = 2131099731;
        public static final int finish_dialog_title = 2131099730;
        public static final int follow_post = 2131099690;
        public static final int follow_post_heb = 2131099693;
        public static final int ga_code = 2131099726;
        public static final int groupie_facebook_groups = 2131099732;
        public static final int guide_1 = 2131099712;
        public static final int guide_2_a = 2131099714;
        public static final int guide_2_b = 2131099715;
        public static final int guide_2_title = 2131099713;
        public static final int guide_3 = 2131099718;
        public static final int guide_3_title = 2131099717;
        public static final int guide_app_name = 2131099710;
        public static final int guide_title = 2131099711;
        public static final int just_now = 2131099702;
        public static final int just_now_hb = 2131099709;
        public static final int layout_type = 2131099694;
        public static final int like = 2131099688;
        public static final int like_heb = 2131099691;
        public static final int logo_bg = 2131099685;
        public static final int logo_fit = 2131099686;
        public static final int logo_link = 2131099684;
        public static final int most_popular_biztech_feeds = 2131099720;
        public static final int ok = 2131099725;
        public static final int one_day_ago = 2131099696;
        public static final int one_day_ago_hb = 2131099703;
        public static final int one_hour_ago = 2131099698;
        public static final int one_hour_ago_hb = 2131099705;
        public static final int one_minute_ago = 2131099700;
        public static final int one_minute_ago_hb = 2131099707;
        public static final int or = 2131099716;
        public static final int pick_groups = 2131099735;
        public static final int powered_by = 2131099676;
        public static final int preparing_widget = 2131099719;
        public static final int read_more = 2131099683;
        public static final int recommend_text = 2131099728;
        public static final int recommendation_feed = 2131099729;
        public static final int select_dialog_message = 2131099724;
        public static final int select_dialog_title = 2131099723;
        public static final int select_up_to_3_feeds = 2131099721;
        public static final int share_update_url = 2131099727;
        public static final int share_with = 2131099678;
        public static final int updating = 2131099675;
        public static final int widget_activation_guide = 2131099687;
        public static final int widget_title = 2131099679;
        public static final int widget_type = 2131099695;
        public static final int x_days_ago = 2131099697;
        public static final int x_days_ago_hb = 2131099704;
        public static final int x_hours_ago = 2131099699;
        public static final int x_hours_ago_hb = 2131099706;
        public static final int x_minutes_ago = 2131099701;
        public static final int x_minutes_ago_hb = 2131099708;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296258;
        public static final int AppTheme = 2131296259;
        public static final int EmptyList = 2131296268;
        public static final int ItemDesc = 2131296263;
        public static final int ItemDesc_wr = 2131296264;
        public static final int ItemTime = 2131296265;
        public static final int ItemTime_mini = 2131296267;
        public static final int ItemTime_wr = 2131296266;
        public static final int ItemTitle = 2131296260;
        public static final int ItemTitle_mini = 2131296262;
        public static final int ItemTitle_wr = 2131296261;
        public static final int PoweredBy = 2131296270;
        public static final int Theme_Transparent = 2131296272;
        public static final int WidgetDate = 2131296269;
        public static final int WidgetTitle = 2131296271;
        public static final int com_facebook_loginview_default_style = 2131296256;
        public static final int com_facebook_loginview_silver_style = 2131296257;
    }

    public static final class styleable {
        public static final int[] com_facebook_friend_picker_fragment = {R.attr.multi_select};
        public static final int com_facebook_friend_picker_fragment_multi_select = 0;
        public static final int[] com_facebook_login_view = {R.attr.confirm_logout, R.attr.fetch_user_info, R.attr.login_text, R.attr.logout_text};
        public static final int com_facebook_login_view_confirm_logout = 0;
        public static final int com_facebook_login_view_fetch_user_info = 1;
        public static final int com_facebook_login_view_login_text = 2;
        public static final int com_facebook_login_view_logout_text = 3;
        public static final int[] com_facebook_picker_fragment = {R.attr.show_pictures, R.attr.extra_fields, R.attr.show_title_bar, R.attr.title_text, R.attr.done_button_text, R.attr.title_bar_background, R.attr.done_button_background};
        public static final int com_facebook_picker_fragment_done_button_background = 6;
        public static final int com_facebook_picker_fragment_done_button_text = 4;
        public static final int com_facebook_picker_fragment_extra_fields = 1;
        public static final int com_facebook_picker_fragment_show_pictures = 0;
        public static final int com_facebook_picker_fragment_show_title_bar = 2;
        public static final int com_facebook_picker_fragment_title_bar_background = 5;
        public static final int com_facebook_picker_fragment_title_text = 3;
        public static final int[] com_facebook_place_picker_fragment = {R.attr.radius_in_meters, R.attr.results_limit, R.attr.search_text, R.attr.show_search_box};
        public static final int com_facebook_place_picker_fragment_radius_in_meters = 0;
        public static final int com_facebook_place_picker_fragment_results_limit = 1;
        public static final int com_facebook_place_picker_fragment_search_text = 2;
        public static final int com_facebook_place_picker_fragment_show_search_box = 3;
        public static final int[] com_facebook_profile_picture_view = {R.attr.preset_size, R.attr.is_cropped};
        public static final int com_facebook_profile_picture_view_is_cropped = 1;
        public static final int com_facebook_profile_picture_view_preset_size = 0;
    }

    public static final class xml {
        public static final int my_appwidget_info = 2130968576;
    }
}
