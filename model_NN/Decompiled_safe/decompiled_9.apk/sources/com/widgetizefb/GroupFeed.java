package com.widgetizefb;

import android.content.Context;
import android.text.TextUtils;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.google.android.gms.plus.PlusShare;
import com.widgetizeme.Logger;
import com.widgetizeme.UnsentStats;
import com.widgetizeme.rss.RSSFeed;
import com.widgetizeme.utils.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class GroupFeed extends RSSFeed {
    private FacebookGroup mGroup;
    /* access modifiers changed from: private */
    public long mStartTime;

    public GroupFeed(Context context, FacebookGroup facebookGroup) {
        super(context, "facebook_" + facebookGroup.getId());
        this.mGroup = facebookGroup;
    }

    public void refresh() {
        try {
            Logger.l("refresh group: " + this.mGroup.getName());
            if (Session.getActiveSession() == null || !Session.getActiveSession().isOpened()) {
                Session.openActiveSessionFromCache(getContext());
            }
            this.mStartTime = System.currentTimeMillis();
            loadGroup();
        } catch (Exception e) {
            Logger.l(e);
        }
        Logger.l("finish group feed refresh");
        loadFromCache();
    }

    private void loadGroup() {
        try {
            new Request(Session.getActiveSession(), "/" + this.mGroup.getId() + "/feed", null, null, new Request.Callback() {
                public void onCompleted(Response response) {
                    StringBuilder xml = new StringBuilder();
                    xml.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                    xml.append("<items>");
                    if (response.getError() == null) {
                        try {
                            JSONArray data = response.getGraphObject().getInnerJSONObject().getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject item = data.getJSONObject(i);
                                xml.append("<item>");
                                if (item.has("from")) {
                                    JSONObject from = item.getJSONObject("from");
                                    xml.append("<title>" + from.getString("name") + "</title>");
                                    xml.append("<thumbnail url=\"" + StringUtils.fixToXML("http://graph.facebook.com/" + from.getString(UnsentStats.KEY_ID) + "/picture?width=160&height=160") + "\" />");
                                }
                                String message = item.optString("message", "");
                                xml.append("<description>" + StringUtils.fixToXML(message) + "</description>");
                                xml.append("<link>" + StringUtils.fixToXML("https://www.facebook.com/groups/" + item.getString(UnsentStats.KEY_ID).replace("_", "/permalink/") + "/") + "</link>");
                                if (TextUtils.isEmpty(message) && item.has(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION)) {
                                    xml.append("<description>" + StringUtils.fixToXML(item.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION)) + "</description>");
                                }
                                String createdTime = item.getString("created_time");
                                if (item.has("updated_time")) {
                                    createdTime = item.getString("updated_time");
                                }
                                xml.append("<pubDate>" + createdTime + "</pubDate>");
                                xml.append("</item>");
                            }
                        } catch (Exception e) {
                            Logger.l(e);
                        }
                    }
                    xml.append("</items>");
                    String sb = xml.toString();
                    GroupFeed.this.saveToFile(xml.toString(), "utf-8");
                    GroupFeed.this.mStartTime = 0;
                }
            }).executeAndWait();
        } catch (Exception e) {
            Logger.l(e);
        }
    }
}
