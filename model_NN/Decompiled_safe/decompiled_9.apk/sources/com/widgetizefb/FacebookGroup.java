package com.widgetizefb;

import android.text.TextUtils;

public class FacebookGroup {
    private boolean mChecked;
    private String mId;
    private String mImageURL;
    private String mName;

    public FacebookGroup(String id, String name, String imageURL) {
        this.mId = id;
        this.mName = name;
        this.mImageURL = imageURL;
    }

    public String getId() {
        return this.mId;
    }

    public String getName() {
        return this.mName;
    }

    public String getImageURL() {
        if (this.mImageURL == null) {
            return "";
        }
        return this.mImageURL;
    }

    public boolean hasImage() {
        return !TextUtils.isEmpty(this.mImageURL);
    }

    public boolean isChecked() {
        return this.mChecked;
    }

    public void setChecked(boolean checked) {
        this.mChecked = checked;
    }
}
