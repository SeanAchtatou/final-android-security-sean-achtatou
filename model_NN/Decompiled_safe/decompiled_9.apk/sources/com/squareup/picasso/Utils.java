package com.squareup.picasso;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import android.os.Process;
import android.os.StatFs;
import android.provider.ContactsContract;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ThreadFactory;

final class Utils {
    private static final String[] CONTENT_ORIENTATION = {"orientation"};
    private static final int KEY_PADDING = 50;
    private static final int MAX_DISK_CACHE_SIZE = 52428800;
    private static final int MAX_MEM_CACHE_SIZE = 20971520;
    private static final int MIN_DISK_CACHE_SIZE = 5242880;
    private static final String PICASSO_CACHE = "picasso-cache";
    static final String THREAD_IDLE_NAME = "Picasso-Idle";
    static final String THREAD_PREFIX = "Picasso-";

    private Utils() {
    }

    static int getContentProviderExifRotation(ContentResolver contentResolver, Uri uri) {
        Cursor cursor = null;
        try {
            cursor = contentResolver.query(uri, CONTENT_ORIENTATION, null, null, null);
            if (cursor == null || !cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return 0;
            }
            int i = cursor.getInt(0);
            if (cursor == null) {
                return i;
            }
            cursor.close();
            return i;
        } catch (IllegalArgumentException e) {
            if (cursor != null) {
                cursor.close();
            }
            return 0;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    static int getBitmapBytes(Bitmap bitmap) {
        int result;
        if (Build.VERSION.SDK_INT >= 12) {
            result = BitmapHoneycombMR1.getByteCount(bitmap);
        } else {
            result = bitmap.getRowBytes() * bitmap.getHeight();
        }
        if (result >= 0) {
            return result;
        }
        throw new IllegalStateException("Negative size: " + bitmap);
    }

    static int getFileExifRotation(String path) throws IOException {
        switch (new ExifInterface(path).getAttributeInt("Orientation", 1)) {
            case 3:
                return 180;
            case 4:
            case 5:
            case 7:
            default:
                return 0;
            case 6:
                return 90;
            case 8:
                return 270;
        }
    }

    static void checkNotMain() {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            throw new IllegalStateException("Method call should not happen from the main thread.");
        }
    }

    static String createKey(Request request) {
        return createKey(request.uri, request.resourceId, request.options, request.transformations);
    }

    static String createKey(Uri uri, int resourceId, PicassoBitmapOptions options, List<Transformation> transformations) {
        StringBuilder builder;
        if (uri != null) {
            String path = uri.toString();
            builder = new StringBuilder(path.length() + 50);
            builder.append(path);
        } else {
            builder = new StringBuilder(50);
            builder.append(resourceId);
        }
        builder.append(10);
        if (options != null) {
            float targetRotation = options.targetRotation;
            if (targetRotation != BitmapDescriptorFactory.HUE_RED) {
                builder.append("rotation:").append(targetRotation);
                if (options.hasRotationPivot) {
                    builder.append('@').append(options.targetPivotX).append('x').append(options.targetPivotY);
                }
                builder.append(10);
            }
            int targetWidth = options.targetWidth;
            int targetHeight = options.targetHeight;
            if (targetWidth != 0) {
                builder.append("resize:").append(targetWidth).append('x').append(targetHeight);
                builder.append(10);
            }
            if (options.centerCrop) {
                builder.append("centerCrop\n");
            }
            if (options.centerInside) {
                builder.append("centerInside\n");
            }
            float targetScaleX = options.targetScaleX;
            float targetScaleY = options.targetScaleY;
            if (targetScaleX != BitmapDescriptorFactory.HUE_RED) {
                builder.append("scale:").append(targetScaleX).append('x').append(targetScaleY);
                builder.append(10);
            }
        }
        if (transformations != null) {
            int count = transformations.size();
            for (int i = 0; i < count; i++) {
                builder.append(transformations.get(i).key());
                builder.append(10);
            }
        }
        return builder.toString();
    }

    static void calculateInSampleSize(PicassoBitmapOptions options) {
        int height = options.outHeight;
        int width = options.outWidth;
        int reqHeight = options.targetHeight;
        int reqWidth = options.targetWidth;
        int sampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int heightRatio = Math.round(((float) height) / ((float) reqHeight));
            int widthRatio = Math.round(((float) width) / ((float) reqWidth));
            if (heightRatio < widthRatio) {
                sampleSize = heightRatio;
            } else {
                sampleSize = widthRatio;
            }
        }
        options.inSampleSize = sampleSize;
        options.inJustDecodeBounds = false;
    }

    static boolean parseResponseSourceHeader(String header) {
        boolean z = true;
        if (header == null) {
            return false;
        }
        String[] parts = header.split(" ", 2);
        if ("CACHE".equals(parts[0])) {
            return true;
        }
        if (parts.length == 1) {
            return false;
        }
        try {
            if (!"CONDITIONAL_CACHE".equals(parts[0]) || Integer.parseInt(parts[1]) != 304) {
                z = false;
            }
            return z;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    static Loader createDefaultLoader(Context context) {
        try {
            Class.forName("com.squareup.okhttp.OkHttpClient");
            return OkHttpLoaderCreator.create(context);
        } catch (ClassNotFoundException e) {
            return new UrlConnectionLoader(context);
        }
    }

    static File createDefaultCacheDir(Context context) {
        File cache = new File(context.getApplicationContext().getCacheDir(), PICASSO_CACHE);
        if (!cache.exists()) {
            cache.mkdirs();
        }
        return cache;
    }

    static int calculateDiskCacheSize(File dir) {
        StatFs statFs = new StatFs(dir.getAbsolutePath());
        return Math.max(Math.min((statFs.getBlockCount() * statFs.getBlockSize()) / 50, (int) MAX_DISK_CACHE_SIZE), (int) MIN_DISK_CACHE_SIZE);
    }

    static int calculateMemoryCacheSize(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService("activity");
        boolean largeHeap = (context.getApplicationInfo().flags & AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START) != 0;
        int memoryClass = am.getMemoryClass();
        if (largeHeap && Build.VERSION.SDK_INT >= 11) {
            memoryClass = ActivityManagerHoneycomb.getLargeMemoryClass(am);
        }
        return Math.min((AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START * memoryClass) / 7, (int) MAX_MEM_CACHE_SIZE);
    }

    public static InputStream getContactPhotoStream(ContentResolver contentResolver, Uri uri) {
        if (Build.VERSION.SDK_INT >= 14) {
            return ContactPhotoStreamIcs.get(contentResolver, uri);
        }
        if (uri.toString().startsWith(ContactsContract.Contacts.CONTENT_LOOKUP_URI.toString())) {
            uri = ContactsContract.Contacts.lookupContact(contentResolver, uri);
        }
        return ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri);
    }

    @TargetApi(11)
    private static class ActivityManagerHoneycomb {
        private ActivityManagerHoneycomb() {
        }

        static int getLargeMemoryClass(ActivityManager activityManager) {
            return activityManager.getLargeMemoryClass();
        }
    }

    static class PicassoThreadFactory implements ThreadFactory {
        PicassoThreadFactory() {
        }

        public Thread newThread(Runnable r) {
            return new PicassoThread(r);
        }
    }

    private static class PicassoThread extends Thread {
        public PicassoThread(Runnable r) {
            super(r);
        }

        public void run() {
            Process.setThreadPriority(10);
            super.run();
        }
    }

    @TargetApi(12)
    private static class BitmapHoneycombMR1 {
        private BitmapHoneycombMR1() {
        }

        static int getByteCount(Bitmap bitmap) {
            return bitmap.getByteCount();
        }
    }

    @TargetApi(14)
    private static class ContactPhotoStreamIcs {
        private ContactPhotoStreamIcs() {
        }

        static InputStream get(ContentResolver contentResolver, Uri uri) {
            return ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri, true);
        }
    }

    private static class OkHttpLoaderCreator {
        private OkHttpLoaderCreator() {
        }

        static Loader create(Context context) {
            return new OkHttpLoader(context);
        }
    }
}
