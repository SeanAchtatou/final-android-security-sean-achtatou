package com.squareup.picasso;

import android.content.Context;
import android.net.Uri;
import com.squareup.okhttp.HttpResponseCache;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.Loader;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class OkHttpLoader implements Loader {
    static final String RESPONSE_SOURCE = "X-Android-Response-Source";
    private final OkHttpClient client;

    public OkHttpLoader(Context context) {
        this(Utils.createDefaultCacheDir(context));
    }

    public OkHttpLoader(File cacheDir) {
        this(cacheDir, Utils.calculateDiskCacheSize(cacheDir));
    }

    public OkHttpLoader(Context context, int maxSize) {
        this(Utils.createDefaultCacheDir(context), maxSize);
    }

    public OkHttpLoader(File cacheDir, int maxSize) {
        this(new OkHttpClient());
        try {
            this.client.setResponseCache(new HttpResponseCache(cacheDir, (long) maxSize));
        } catch (IOException e) {
        }
    }

    public OkHttpLoader(OkHttpClient client2) {
        this.client = client2;
    }

    public Loader.Response load(Uri uri, boolean localCacheOnly) throws IOException {
        HttpURLConnection connection = this.client.open(new URL(uri.toString()));
        connection.setUseCaches(true);
        if (localCacheOnly) {
            connection.setRequestProperty("Cache-Control", "only-if-cached");
        }
        return new Loader.Response(connection.getInputStream(), Utils.parseResponseSourceHeader(connection.getHeaderField(RESPONSE_SOURCE)));
    }
}
