package com.squareup.picasso;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

class Stats {
    private static final int BITMAP_DECODE_FINISHED = 3;
    private static final int BITMAP_TRANSFORMED_FINISHED = 4;
    private static final int CACHE_HIT = 1;
    private static final int CACHE_MISS = 2;
    private static final int REQUESTED_COMPLETED = 0;
    private static final String STATS_THREAD_NAME = "Picasso-Stats";
    long averageOriginalBitmapSize;
    long averageTransformedBitmapSize;
    final Cache cache;
    long cacheHits;
    long cacheMisses;
    final Handler handler;
    int originalBitmapCount;
    long totalOriginalBitmapSize;
    long totalTransformedBitmapSize;
    int transformedBitmapCount;

    Stats(Cache cache2) {
        this.cache = cache2;
        HandlerThread statsThread = new HandlerThread(STATS_THREAD_NAME, 10);
        statsThread.start();
        this.handler = new StatsHandler(statsThread.getLooper());
    }

    /* access modifiers changed from: package-private */
    public void bitmapDecoded(Bitmap bitmap) {
        processBitmap(bitmap, 3);
    }

    /* access modifiers changed from: package-private */
    public void bitmapTransformed(Bitmap bitmap) {
        processBitmap(bitmap, 4);
    }

    /* access modifiers changed from: package-private */
    public void cacheHit() {
        this.handler.sendEmptyMessage(1);
    }

    /* access modifiers changed from: package-private */
    public void cacheMiss() {
        this.handler.sendEmptyMessage(2);
    }

    /* access modifiers changed from: package-private */
    public synchronized StatsSnapshot createSnapshot() {
        return new StatsSnapshot(this.cache.maxSize(), this.cache.size(), this.cacheHits, this.cacheMisses, this.totalOriginalBitmapSize, this.totalTransformedBitmapSize, this.averageOriginalBitmapSize, this.averageTransformedBitmapSize, this.originalBitmapCount, this.transformedBitmapCount, System.currentTimeMillis());
    }

    private void processBitmap(Bitmap bitmap, int what) {
        this.handler.sendMessage(this.handler.obtainMessage(what, Utils.getBitmapBytes(bitmap), 0));
    }

    /* access modifiers changed from: private */
    public static long getAverage(int count, long totalSize) {
        return totalSize / ((long) count);
    }

    private class StatsHandler extends Handler {
        public StatsHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(final Message msg) {
            synchronized (Stats.this) {
                switch (msg.what) {
                    case 0:
                        break;
                    case 1:
                        Stats.this.cacheHits++;
                        break;
                    case 2:
                        Stats.this.cacheMisses++;
                        break;
                    case 3:
                        Stats.this.originalBitmapCount++;
                        Stats.this.totalOriginalBitmapSize += (long) msg.arg1;
                        Stats.this.averageOriginalBitmapSize = Stats.getAverage(Stats.this.originalBitmapCount, Stats.this.totalOriginalBitmapSize);
                        break;
                    case 4:
                        Stats.this.transformedBitmapCount++;
                        Stats.this.totalTransformedBitmapSize += (long) msg.arg1;
                        Stats.this.averageTransformedBitmapSize = Stats.getAverage(Stats.this.originalBitmapCount, Stats.this.totalTransformedBitmapSize);
                        break;
                    default:
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            public void run() {
                                throw new AssertionError("Unhandled stats message." + msg.what);
                            }
                        });
                        break;
                }
            }
        }
    }
}
