package com.squareup.picasso;

import android.graphics.Bitmap;

public interface Target {
    void onError();

    void onSuccess(Bitmap bitmap);
}
