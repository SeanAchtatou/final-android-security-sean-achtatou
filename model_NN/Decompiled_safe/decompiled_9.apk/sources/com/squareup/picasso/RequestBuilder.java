package com.squareup.picasso;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.squareup.picasso.Request;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RequestBuilder {
    private Drawable errorDrawable;
    private int errorResId;
    private boolean hasNullPlaceholder;
    private boolean noFade;
    PicassoBitmapOptions options;
    private final Picasso picasso;
    private Drawable placeholderDrawable;
    private int placeholderResId;
    private final int resourceId;
    private boolean skipCache;
    private List<Transformation> transformations;
    private final Uri uri;

    RequestBuilder(Picasso picasso2, Uri uri2, int resourceId2) {
        this.picasso = picasso2;
        this.uri = uri2;
        this.resourceId = resourceId2;
    }

    RequestBuilder() {
        this.picasso = null;
        this.uri = null;
        this.resourceId = 0;
    }

    private PicassoBitmapOptions getOptions() {
        if (this.options == null) {
            this.options = new PicassoBitmapOptions();
        }
        return this.options;
    }

    public RequestBuilder placeholder(int placeholderResId2) {
        if (placeholderResId2 == 0) {
            throw new IllegalArgumentException("Placeholder image resource invalid.");
        } else if (this.placeholderDrawable != null) {
            throw new IllegalStateException("Placeholder image already set.");
        } else {
            this.placeholderResId = placeholderResId2;
            return this;
        }
    }

    public RequestBuilder placeholder(Drawable placeholderDrawable2) {
        if (this.placeholderResId != 0) {
            throw new IllegalStateException("Placeholder image already set.");
        }
        this.hasNullPlaceholder = placeholderDrawable2 == null;
        this.placeholderDrawable = placeholderDrawable2;
        return this;
    }

    public RequestBuilder error(int errorResId2) {
        if (errorResId2 == 0) {
            throw new IllegalArgumentException("Error image resource invalid.");
        } else if (this.errorDrawable != null) {
            throw new IllegalStateException("Error image already set.");
        } else {
            this.errorResId = errorResId2;
            return this;
        }
    }

    public RequestBuilder error(Drawable errorDrawable2) {
        if (errorDrawable2 == null) {
            throw new IllegalArgumentException("Error image may not be null.");
        } else if (this.errorResId != 0) {
            throw new IllegalStateException("Error image already set.");
        } else {
            this.errorDrawable = errorDrawable2;
            return this;
        }
    }

    public RequestBuilder fit() {
        PicassoBitmapOptions options2 = getOptions();
        if (options2.targetWidth == 0 && options2.targetHeight == 0) {
            options2.deferredResize = true;
            return this;
        }
        throw new IllegalStateException("Fit cannot be used with resize.");
    }

    public RequestBuilder resizeDimen(int targetWidthResId, int targetHeightResId) {
        Resources resources = this.picasso.context.getResources();
        return resize(resources.getDimensionPixelSize(targetWidthResId), resources.getDimensionPixelSize(targetHeightResId));
    }

    public RequestBuilder resize(int targetWidth, int targetHeight) {
        if (targetWidth <= 0) {
            throw new IllegalArgumentException("Width must be positive number.");
        } else if (targetHeight <= 0) {
            throw new IllegalArgumentException("Height must be positive number.");
        } else {
            PicassoBitmapOptions options2 = getOptions();
            if (options2.targetWidth != 0 || options2.targetHeight != 0) {
                throw new IllegalStateException("Resize may only be called once.");
            } else if (options2.deferredResize) {
                throw new IllegalStateException("Resize cannot be used with fit.");
            } else {
                options2.targetWidth = targetWidth;
                options2.targetHeight = targetHeight;
                options2.inJustDecodeBounds = true;
                return this;
            }
        }
    }

    public RequestBuilder centerCrop() {
        PicassoBitmapOptions options2 = getOptions();
        if (options2.targetWidth == 0 || options2.targetHeight == 0) {
            throw new IllegalStateException("Center crop can only be used after calling resize.");
        } else if (options2.centerInside) {
            throw new IllegalStateException("Center crop can not be used after calling centerInside");
        } else {
            options2.centerCrop = true;
            return this;
        }
    }

    public RequestBuilder centerInside() {
        PicassoBitmapOptions options2 = getOptions();
        if (options2.targetWidth == 0 || options2.targetHeight == 0) {
            throw new IllegalStateException("Center inside can only be used after calling resize.");
        } else if (options2.centerCrop) {
            throw new IllegalStateException("Center inside can not be used after calling centerCrop");
        } else {
            options2.centerInside = true;
            return this;
        }
    }

    public RequestBuilder scale(float factor) {
        if (factor != 1.0f) {
            scale(factor, factor);
        }
        return this;
    }

    public RequestBuilder scale(float factorX, float factorY) {
        if (factorX == BitmapDescriptorFactory.HUE_RED || factorY == BitmapDescriptorFactory.HUE_RED) {
            throw new IllegalArgumentException("Scale factor must be positive number.");
        }
        if (!(factorX == 1.0f || factorY == 1.0f)) {
            PicassoBitmapOptions options2 = getOptions();
            if (options2.targetScaleX == BitmapDescriptorFactory.HUE_RED && options2.targetScaleY == BitmapDescriptorFactory.HUE_RED) {
                options2.targetScaleX = factorX;
                options2.targetScaleY = factorY;
            } else {
                throw new IllegalStateException("Scale may only be called once.");
            }
        }
        return this;
    }

    public RequestBuilder rotate(float degrees) {
        if (degrees != BitmapDescriptorFactory.HUE_RED) {
            getOptions().targetRotation = degrees;
        }
        return this;
    }

    public RequestBuilder rotate(float degrees, float pivotX, float pivotY) {
        if (degrees != BitmapDescriptorFactory.HUE_RED) {
            PicassoBitmapOptions options2 = getOptions();
            options2.targetRotation = degrees;
            options2.targetPivotX = pivotX;
            options2.targetPivotY = pivotY;
            options2.hasRotationPivot = true;
        }
        return this;
    }

    public RequestBuilder transform(Transformation transformation) {
        if (transformation == null) {
            throw new IllegalArgumentException("Transformation must not be null.");
        }
        if (this.transformations == null) {
            this.transformations = new ArrayList(2);
        }
        this.transformations.add(transformation);
        return this;
    }

    public RequestBuilder skipCache() {
        this.skipCache = true;
        return this;
    }

    public RequestBuilder noFade() {
        this.noFade = true;
        return this;
    }

    public Bitmap get() throws IOException {
        Utils.checkNotMain();
        if (this.uri == null && this.resourceId == 0) {
            return null;
        }
        return this.picasso.resolveRequest(new Request(this.picasso, this.uri, this.resourceId, null, this.options, this.transformations, this.skipCache, false, 0, null));
    }

    public void fetch(Target target) {
        makeTargetRequest(target, true);
    }

    public void into(Target target) {
        makeTargetRequest(target, false);
    }

    public void into(ImageView target) {
        Bitmap bitmap;
        if (target == null) {
            throw new IllegalArgumentException("Target must not be null.");
        }
        boolean hasItemToLoad = (this.uri == null && this.resourceId == 0) ? false : true;
        if (!hasItemToLoad || (bitmap = this.picasso.quickMemoryCacheCheck(target, this.uri, Utils.createKey(this.uri, this.resourceId, this.options, this.transformations))) == null) {
            if (this.placeholderResId != 0 || this.placeholderDrawable != null) {
                PicassoDrawable.setPlaceholder(target, this.picasso.context, this.placeholderResId, this.placeholderDrawable, this.picasso.debugging);
            } else if (this.hasNullPlaceholder) {
                target.setImageDrawable(null);
            }
            if (hasItemToLoad) {
                this.picasso.submit(new Request(this.picasso, this.uri, this.resourceId, target, this.options, this.transformations, this.skipCache, this.noFade, this.errorResId, this.errorDrawable));
            } else {
                this.picasso.cancelRequest(target);
            }
        } else {
            PicassoDrawable.setBitmap(target, this.picasso.context, bitmap, Request.LoadedFrom.MEMORY, this.noFade, this.picasso.debugging);
        }
    }

    private void makeTargetRequest(Target target, boolean strong) {
        if (target == null) {
            throw new IllegalArgumentException("Target must not be null.");
        } else if (this.uri == null && this.resourceId == 0) {
            this.picasso.cancelRequest(target);
        } else {
            Bitmap bitmap = this.picasso.quickMemoryCacheCheck(target, this.uri, Utils.createKey(this.uri, this.resourceId, this.options, this.transformations));
            if (bitmap != null) {
                target.onSuccess(bitmap);
                return;
            }
            this.picasso.submit(new TargetRequest(this.picasso, this.uri, this.resourceId, target, strong, this.options, this.transformations, this.skipCache));
        }
    }
}
