package com.squareup.picasso;

import android.graphics.BitmapFactory;

class PicassoBitmapOptions extends BitmapFactory.Options {
    boolean centerCrop;
    boolean centerInside;
    boolean deferredResize;
    boolean hasRotationPivot;
    int targetHeight;
    float targetPivotX;
    float targetPivotY;
    float targetRotation;
    float targetScaleX;
    float targetScaleY;
    int targetWidth;

    PicassoBitmapOptions() {
    }
}
