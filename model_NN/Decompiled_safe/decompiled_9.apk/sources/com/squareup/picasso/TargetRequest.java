package com.squareup.picasso;

import android.net.Uri;
import java.lang.ref.WeakReference;
import java.util.List;

final class TargetRequest extends Request {
    private final Target strongTarget;
    private final WeakReference<Target> weakTarget;

    TargetRequest(Picasso picasso, Uri uri, int resourceId, Target target, boolean strong, PicassoBitmapOptions bitmapOptions, List<Transformation> transformations, boolean skipCache) {
        super(picasso, uri, resourceId, null, bitmapOptions, transformations, skipCache, false, 0, null);
        this.weakTarget = strong ? null : new WeakReference<>(target);
        this.strongTarget = !strong ? null : target;
    }

    /* access modifiers changed from: package-private */
    public Target getTarget() {
        return this.strongTarget != null ? this.strongTarget : this.weakTarget.get();
    }

    /* access modifiers changed from: package-private */
    public void complete() {
        if (this.result == null) {
            throw new AssertionError(String.format("Attempted to complete request with no result!\n%s", this));
        }
        Target target = getTarget();
        if (target != null) {
            target.onSuccess(this.result);
            if (this.result.isRecycled()) {
                throw new IllegalStateException("Target callback must not recycle bitmap!");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void error() {
        Target target = getTarget();
        if (target != null) {
            target.onError();
        }
    }
}
