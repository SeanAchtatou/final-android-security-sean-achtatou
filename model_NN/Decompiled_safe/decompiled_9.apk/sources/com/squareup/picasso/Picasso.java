package com.squareup.picasso;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.ContactsContract;
import android.widget.ImageView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.squareup.picasso.Loader;
import com.squareup.picasso.Request;
import com.squareup.picasso.Utils;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Picasso {
    private static final Object DECODE_LOCK = new Object();
    private static final int REQUEST_COMPLETE = 1;
    private static final int REQUEST_DECODE_FAILED = 3;
    private static final int REQUEST_RETRY = 2;
    private static final int RETRY_DELAY = 500;
    static Picasso singleton = null;
    final Cache cache;
    final Context context;
    boolean debugging;
    final Handler handler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            Request request = (Request) msg.obj;
            if (!request.future.isCancelled() && !request.retryCancelled) {
                Picasso picasso = request.picasso;
                switch (msg.what) {
                    case 1:
                        picasso.targetsToRequests.remove(request.getTarget());
                        request.complete();
                        return;
                    case 2:
                        picasso.retry(request);
                        return;
                    case 3:
                        picasso.error(request);
                        return;
                    default:
                        throw new AssertionError("Unknown handler message received: " + msg.what);
                }
            }
        }
    };
    final Listener listener;
    final Loader loader;
    final ExecutorService service;
    final Stats stats;
    final Map<Object, Request> targetsToRequests;

    public interface Listener {
        void onImageLoadFailed(Picasso picasso, Uri uri, Exception exc);
    }

    Picasso(Context context2, Loader loader2, ExecutorService service2, Cache cache2, Listener listener2, Stats stats2) {
        this.context = context2;
        this.loader = loader2;
        this.service = service2;
        this.cache = cache2;
        this.listener = listener2;
        this.stats = stats2;
        this.targetsToRequests = new WeakHashMap();
    }

    public void cancelRequest(ImageView view) {
        cancelExistingRequest(view, null);
    }

    public void cancelRequest(Target target) {
        cancelExistingRequest(target, null);
    }

    public RequestBuilder load(Uri uri) {
        return new RequestBuilder(this, uri, 0);
    }

    public RequestBuilder load(String path) {
        if (path == null) {
            return new RequestBuilder(this, null, 0);
        }
        if (path.trim().length() != 0) {
            return load(Uri.parse(path));
        }
        throw new IllegalArgumentException("Path must not be empty.");
    }

    public RequestBuilder load(File file) {
        if (file == null) {
            return new RequestBuilder(this, null, 0);
        }
        return load(Uri.fromFile(file));
    }

    public RequestBuilder load(int resourceId) {
        if (resourceId != 0) {
            return new RequestBuilder(this, null, resourceId);
        }
        throw new IllegalArgumentException("Resource ID must not be zero.");
    }

    public boolean isDebugging() {
        return this.debugging;
    }

    public void setDebugging(boolean debugging2) {
        this.debugging = debugging2;
    }

    public StatsSnapshot getSnapshot() {
        return this.stats.createSnapshot();
    }

    /* access modifiers changed from: package-private */
    public void submit(Request request) {
        Object target = request.getTarget();
        if (target != null) {
            cancelExistingRequest(target, request.uri);
            this.targetsToRequests.put(target, request);
            request.future = this.service.submit(request);
        }
    }

    /* access modifiers changed from: package-private */
    public void run(Request request) {
        try {
            Bitmap result = resolveRequest(request);
            if (result == null) {
                this.handler.sendMessage(this.handler.obtainMessage(3, request));
                return;
            }
            request.result = result;
            this.handler.sendMessage(this.handler.obtainMessage(1, request));
        } catch (IOException e) {
            if (!(this.listener == null || request.uri == null)) {
                this.listener.onImageLoadFailed(this, request.uri, e);
            }
            this.handler.sendMessageDelayed(this.handler.obtainMessage(2, request), 500);
        }
    }

    /* access modifiers changed from: package-private */
    public Bitmap resolveRequest(Request request) throws IOException {
        Bitmap bitmap = loadFromCache(request);
        if (bitmap == null) {
            this.stats.cacheMiss();
            try {
                bitmap = loadFromType(request);
                if (bitmap != null && !request.skipCache) {
                    this.cache.set(request.key, bitmap);
                }
            } catch (OutOfMemoryError e) {
                throw new IOException("Failed to decode request: " + request, e);
            }
        } else {
            this.stats.cacheHit();
        }
        return bitmap;
    }

    /* access modifiers changed from: package-private */
    public Bitmap quickMemoryCacheCheck(Object target, Uri uri, String key) {
        Bitmap cached = this.cache.get(key);
        cancelExistingRequest(target, uri);
        if (cached != null) {
            this.stats.cacheHit();
        }
        return cached;
    }

    /* access modifiers changed from: package-private */
    public void retry(Request request) {
        if (!request.retryCancelled) {
            if (request.retryCount > 0) {
                request.retryCount--;
                submit(request);
                return;
            }
            this.targetsToRequests.remove(request.getTarget());
            request.error();
        }
    }

    /* access modifiers changed from: package-private */
    public void error(Request request) {
        this.targetsToRequests.remove(request.getTarget());
        request.error();
    }

    /* access modifiers changed from: package-private */
    public Bitmap decodeStream(InputStream stream, PicassoBitmapOptions bitmapOptions) {
        if (stream == null) {
            return null;
        }
        if (bitmapOptions != null) {
            bitmapOptions.inJustDecodeBounds = false;
        }
        return BitmapFactory.decodeStream(stream, null, bitmapOptions);
    }

    /* access modifiers changed from: package-private */
    public Bitmap decodeContentStream(Uri path, PicassoBitmapOptions bitmapOptions) throws IOException {
        ContentResolver contentResolver = this.context.getContentResolver();
        if (bitmapOptions != null && bitmapOptions.inJustDecodeBounds) {
            BitmapFactory.decodeStream(contentResolver.openInputStream(path), null, bitmapOptions);
            Utils.calculateInSampleSize(bitmapOptions);
        }
        return BitmapFactory.decodeStream(contentResolver.openInputStream(path), null, bitmapOptions);
    }

    /* access modifiers changed from: package-private */
    public Bitmap decodeResource(Resources resources, int resourceId, PicassoBitmapOptions bitmapOptions) {
        if (bitmapOptions != null && bitmapOptions.inJustDecodeBounds) {
            BitmapFactory.decodeResource(resources, resourceId, bitmapOptions);
            Utils.calculateInSampleSize(bitmapOptions);
        }
        return BitmapFactory.decodeResource(resources, resourceId, bitmapOptions);
    }

    private void cancelExistingRequest(Object target, Uri uri) {
        Request existing = this.targetsToRequests.remove(target);
        if (existing == null) {
            return;
        }
        if (!existing.future.isDone()) {
            existing.future.cancel(true);
        } else if (uri == null || !uri.equals(existing.uri)) {
            existing.retryCancelled = true;
        }
    }

    private Bitmap loadFromCache(Request request) {
        if (request.skipCache) {
            return null;
        }
        Bitmap cached = this.cache.get(request.key);
        if (cached == null) {
            return cached;
        }
        request.loadedFrom = Request.LoadedFrom.MEMORY;
        return cached;
    }

    private Bitmap loadFromType(Request request) throws IOException {
        Bitmap result;
        Request.LoadedFrom loadedFrom;
        InputStream inputStream;
        ImageView target;
        PicassoBitmapOptions options = request.options;
        int exifRotation = 0;
        Uri uri = request.uri;
        int resourceId = request.resourceId;
        if (resourceId != 0) {
            result = decodeResource(this.context.getResources(), resourceId, options);
            request.loadedFrom = Request.LoadedFrom.DISK;
        } else {
            String scheme = uri.getScheme();
            if ("content".equals(scheme)) {
                ContentResolver contentResolver = this.context.getContentResolver();
                if (!ContactsContract.Contacts.CONTENT_URI.getHost().equals(uri.getHost()) || uri.getPathSegments().contains("photo")) {
                    exifRotation = Utils.getContentProviderExifRotation(contentResolver, uri);
                    result = decodeContentStream(uri, options);
                } else {
                    result = decodeStream(Utils.getContactPhotoStream(contentResolver, uri), options);
                }
                request.loadedFrom = Request.LoadedFrom.DISK;
            } else if ("file".equals(scheme)) {
                exifRotation = Utils.getFileExifRotation(uri.getPath());
                result = decodeContentStream(uri, options);
                request.loadedFrom = Request.LoadedFrom.DISK;
            } else if ("android.resource".equals(scheme)) {
                result = decodeContentStream(uri, options);
                request.loadedFrom = Request.LoadedFrom.DISK;
            } else {
                Loader.Response response = null;
                try {
                    response = this.loader.load(uri, request.retryCount == 0);
                    if (response != null) {
                        result = decodeStream(response.stream, options);
                        if (!(response == null || response.stream == null)) {
                            try {
                                response.stream.close();
                            } catch (IOException e) {
                            }
                        }
                        if (response.cached) {
                            loadedFrom = Request.LoadedFrom.DISK;
                        } else {
                            loadedFrom = Request.LoadedFrom.NETWORK;
                        }
                        request.loadedFrom = loadedFrom;
                    } else if (response == null || inputStream == null) {
                        return null;
                    } else {
                        try {
                            return null;
                        } catch (IOException e2) {
                            return null;
                        }
                    }
                } finally {
                    if (!(response == null || response.stream == null)) {
                        try {
                            response.stream.close();
                        } catch (IOException e3) {
                        }
                    }
                }
            }
        }
        if (result == null) {
            return null;
        }
        this.stats.bitmapDecoded(result);
        if (!(options == null || !options.deferredResize || (target = request.target.get()) == null)) {
            int targetWidth = target.getMeasuredWidth();
            int targetHeight = target.getMeasuredHeight();
            if (!(targetWidth == 0 || targetHeight == 0)) {
                options.targetWidth = targetWidth;
                options.targetHeight = targetHeight;
            }
        }
        if (!(options == null && exifRotation == 0)) {
            result = transformResult(options, result, exifRotation);
        }
        List<Transformation> transformations = request.transformations;
        if (transformations != null) {
            result = applyCustomTransformations(transformations, result);
            this.stats.bitmapTransformed(result);
        }
        return result;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    static Bitmap transformResult(PicassoBitmapOptions options, Bitmap result, int exifRotation) {
        float scale;
        float scale2;
        int inWidth = result.getWidth();
        int inHeight = result.getHeight();
        int drawX = 0;
        int drawY = 0;
        int drawWidth = inWidth;
        int drawHeight = inHeight;
        Matrix matrix = new Matrix();
        if (options != null) {
            int targetWidth = options.targetWidth;
            int targetHeight = options.targetHeight;
            float targetRotation = options.targetRotation;
            if (targetRotation != BitmapDescriptorFactory.HUE_RED) {
                if (options.hasRotationPivot) {
                    matrix.setRotate(targetRotation, options.targetPivotX, options.targetPivotY);
                } else {
                    matrix.setRotate(targetRotation);
                }
            }
            if (options.centerCrop) {
                float widthRatio = ((float) targetWidth) / ((float) inWidth);
                float heightRatio = ((float) targetHeight) / ((float) inHeight);
                if (widthRatio > heightRatio) {
                    scale2 = widthRatio;
                    int newSize = (int) Math.ceil((double) (((float) inHeight) * (heightRatio / widthRatio)));
                    drawY = (inHeight - newSize) / 2;
                    drawHeight = newSize;
                } else {
                    scale2 = heightRatio;
                    int newSize2 = (int) Math.ceil((double) (((float) inWidth) * (widthRatio / heightRatio)));
                    drawX = (inWidth - newSize2) / 2;
                    drawWidth = newSize2;
                }
                matrix.preScale(scale2, scale2);
            } else if (options.centerInside) {
                float widthRatio2 = ((float) targetWidth) / ((float) inWidth);
                float heightRatio2 = ((float) targetHeight) / ((float) inHeight);
                if (widthRatio2 < heightRatio2) {
                    scale = widthRatio2;
                } else {
                    scale = heightRatio2;
                }
                matrix.preScale(scale, scale);
            } else if (!(targetWidth == 0 || targetHeight == 0 || (targetWidth == inWidth && targetHeight == inHeight))) {
                matrix.preScale(((float) targetWidth) / ((float) inWidth), ((float) targetHeight) / ((float) inHeight));
            }
            float targetScaleX = options.targetScaleX;
            float targetScaleY = options.targetScaleY;
            if (!(targetScaleX == BitmapDescriptorFactory.HUE_RED && targetScaleY == BitmapDescriptorFactory.HUE_RED)) {
                matrix.setScale(targetScaleX, targetScaleY);
            }
        }
        if (exifRotation != 0) {
            matrix.preRotate((float) exifRotation);
        }
        synchronized (DECODE_LOCK) {
            Bitmap newResult = Bitmap.createBitmap(result, drawX, drawY, drawWidth, drawHeight, matrix, false);
            if (newResult != result) {
                result.recycle();
                result = newResult;
            }
        }
        return result;
    }

    static Bitmap applyCustomTransformations(List<Transformation> transformations, Bitmap result) {
        int i = 0;
        int count = transformations.size();
        while (i < count) {
            Transformation transformation = transformations.get(i);
            Bitmap newResult = transformation.transform(result);
            if (newResult == null) {
                StringBuilder builder = new StringBuilder().append("Transformation ").append(transformation.key()).append(" returned null after ").append(i).append(" previous transformation(s).\n\nTransformation list:\n");
                for (Transformation t : transformations) {
                    builder.append(t.key()).append(10);
                }
                throw new NullPointerException(builder.toString());
            } else if (newResult == result && result.isRecycled()) {
                throw new IllegalStateException("Transformation " + transformation.key() + " returned input Bitmap but recycled it.");
            } else if (newResult == result || result.isRecycled()) {
                result = newResult;
                i++;
            } else {
                throw new IllegalStateException("Transformation " + transformation.key() + " mutated input Bitmap but failed to recycle the original.");
            }
        }
        return result;
    }

    public static Picasso with(Context context2) {
        if (singleton == null) {
            singleton = new Builder(context2).build();
        }
        return singleton;
    }

    public static class Builder {
        private final Context context;
        private Listener listener;
        private Loader loader;
        private Cache memoryCache;
        private ExecutorService service;

        public Builder(Context context2) {
            if (context2 == null) {
                throw new IllegalArgumentException("Context must not be null.");
            }
            this.context = context2.getApplicationContext();
        }

        public Builder loader(Loader loader2) {
            if (loader2 == null) {
                throw new IllegalArgumentException("Loader must not be null.");
            } else if (this.loader != null) {
                throw new IllegalStateException("Loader already set.");
            } else {
                this.loader = loader2;
                return this;
            }
        }

        public Builder executor(ExecutorService executorService) {
            if (executorService == null) {
                throw new IllegalArgumentException("Executor service must not be null.");
            } else if (this.service != null) {
                throw new IllegalStateException("Executor service already set.");
            } else {
                this.service = executorService;
                return this;
            }
        }

        public Builder memoryCache(Cache memoryCache2) {
            if (memoryCache2 == null) {
                throw new IllegalArgumentException("Memory cache must not be null.");
            } else if (this.memoryCache != null) {
                throw new IllegalStateException("Memory cache already set.");
            } else {
                this.memoryCache = memoryCache2;
                return this;
            }
        }

        public Builder listener(Listener listener2) {
            if (listener2 == null) {
                throw new IllegalArgumentException("Listener must not be null.");
            } else if (this.listener != null) {
                throw new IllegalStateException("Listener already set.");
            } else {
                this.listener = listener2;
                return this;
            }
        }

        public Picasso build() {
            Context context2 = this.context;
            if (this.loader == null) {
                this.loader = Utils.createDefaultLoader(context2);
            }
            if (this.memoryCache == null) {
                this.memoryCache = new LruCache(context2);
            }
            if (this.service == null) {
                this.service = Executors.newFixedThreadPool(3, new Utils.PicassoThreadFactory());
            }
            return new Picasso(context2, this.loader, this.service, this.memoryCache, this.listener, new Stats(this.memoryCache));
        }
    }
}
