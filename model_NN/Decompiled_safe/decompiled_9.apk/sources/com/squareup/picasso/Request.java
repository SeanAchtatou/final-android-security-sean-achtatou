package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.Future;

class Request implements Runnable {
    static final int DEFAULT_RETRY_COUNT = 2;
    final Drawable errorDrawable;
    final int errorResId;
    Future<?> future;
    final String key = Utils.createKey(this);
    LoadedFrom loadedFrom;
    final boolean noFade;
    final PicassoBitmapOptions options;
    final Picasso picasso;
    final int resourceId;
    Bitmap result;
    boolean retryCancelled;
    int retryCount = 2;
    final boolean skipCache;
    final WeakReference<ImageView> target;
    final List<Transformation> transformations;
    final Uri uri;

    enum LoadedFrom {
        MEMORY(-16711936),
        DISK(-256),
        NETWORK(-65536);
        
        final int debugColor;

        private LoadedFrom(int debugColor2) {
            this.debugColor = debugColor2;
        }
    }

    Request(Picasso picasso2, Uri uri2, int resourceId2, ImageView imageView, PicassoBitmapOptions options2, List<Transformation> transformations2, boolean skipCache2, boolean noFade2, int errorResId2, Drawable errorDrawable2) {
        this.picasso = picasso2;
        this.uri = uri2;
        this.resourceId = resourceId2;
        this.target = new WeakReference<>(imageView);
        this.options = options2;
        this.transformations = transformations2;
        this.skipCache = skipCache2;
        this.noFade = noFade2;
        this.errorResId = errorResId2;
        this.errorDrawable = errorDrawable2;
    }

    /* access modifiers changed from: package-private */
    public Object getTarget() {
        return this.target.get();
    }

    /* access modifiers changed from: package-private */
    public void complete() {
        if (this.result == null) {
            throw new AssertionError(String.format("Attempted to complete request with no result!\n%s", this));
        }
        ImageView target2 = this.target.get();
        if (target2 != null) {
            PicassoDrawable.setBitmap(target2, this.picasso.context, this.result, this.loadedFrom, this.noFade, this.picasso.debugging);
        }
    }

    /* access modifiers changed from: package-private */
    public void error() {
        ImageView target2 = this.target.get();
        if (target2 != null) {
            if (this.errorResId != 0) {
                target2.setImageResource(this.errorResId);
            } else if (this.errorDrawable != null) {
                target2.setImageDrawable(this.errorDrawable);
            }
        }
    }

    public void run() {
        String str;
        try {
            Thread.currentThread().setName("Picasso-" + getName());
            this.picasso.run(this);
        } catch (Throwable e) {
            this.picasso.handler.post(new Runnable() {
                public void run() {
                    throw new RuntimeException("An unexpected exception occurred", e);
                }
            });
        } finally {
            str = "Picasso-Idle";
            Thread.currentThread().setName(str);
        }
    }

    private String getName() {
        Uri uri2 = this.uri;
        return uri2 != null ? uri2.getPath() : Integer.toString(this.resourceId);
    }

    public String toString() {
        return "Request[hashCode=" + hashCode() + ", picasso=" + this.picasso + ", uri=" + this.uri + ", resourceId=" + this.resourceId + ", target=" + this.target + ", options=" + this.options + ", transformations=" + transformationKeys() + ", future=" + this.future + ", result=" + this.result + ", retryCount=" + this.retryCount + ", loadedFrom=" + this.loadedFrom + ']';
    }

    /* access modifiers changed from: package-private */
    public String transformationKeys() {
        if (this.transformations == null) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder(this.transformations.size() * 16);
        sb.append('[');
        boolean first = true;
        for (Transformation transformation : this.transformations) {
            if (first) {
                first = false;
            } else {
                sb.append(", ");
            }
            sb.append(transformation.key());
        }
        sb.append(']');
        return sb.toString();
    }
}
