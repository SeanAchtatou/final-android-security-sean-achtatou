package com.squareup.picasso;

import android.net.Uri;
import java.io.IOException;
import java.io.InputStream;

public interface Loader {
    Response load(Uri uri, boolean z) throws IOException;

    public static class Response {
        final boolean cached;
        final InputStream stream;

        public Response(InputStream stream2, boolean loadedFromCache) {
            this.stream = stream2;
            this.cached = loadedFromCache;
        }
    }
}
