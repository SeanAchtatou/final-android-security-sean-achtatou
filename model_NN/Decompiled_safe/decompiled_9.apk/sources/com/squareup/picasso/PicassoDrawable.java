package com.squareup.picasso;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.support.v4.view.MotionEventCompat;
import android.widget.ImageView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.squareup.picasso.Request;

final class PicassoDrawable extends Drawable {
    private static final Paint DEBUG_PAINT = new Paint();
    private static final float FADE_DURATION = 200.0f;
    private int alpha;
    boolean animating;
    BitmapDrawable bitmapDrawable;
    private final Context context;
    private final boolean debugging;
    private final float density;
    private Request.LoadedFrom loadedFrom;
    Drawable placeHolderDrawable;
    int placeholderResId;
    private long startTimeMillis;

    static void setBitmap(ImageView target, Context context2, Bitmap bitmap, Request.LoadedFrom loadedFrom2, boolean noFade, boolean debugging2) {
        PicassoDrawable picassoDrawable = extractPicassoDrawable(target);
        if (picassoDrawable != null) {
            picassoDrawable.setBitmap(bitmap, loadedFrom2, noFade);
        } else {
            target.setImageDrawable(new PicassoDrawable(context2, bitmap, loadedFrom2, noFade, debugging2));
        }
    }

    static void setPlaceholder(ImageView target, Context context2, int placeholderResId2, Drawable placeholderDrawable, boolean debugging2) {
        PicassoDrawable picassoDrawable = extractPicassoDrawable(target);
        if (picassoDrawable != null) {
            picassoDrawable.setPlaceholder(placeholderResId2, placeholderDrawable);
        } else {
            target.setImageDrawable(new PicassoDrawable(context2, placeholderResId2, placeholderDrawable, debugging2));
        }
    }

    private static PicassoDrawable extractPicassoDrawable(ImageView target) {
        Drawable targetDrawable = target.getDrawable();
        if (targetDrawable instanceof PicassoDrawable) {
            return (PicassoDrawable) targetDrawable;
        }
        return null;
    }

    PicassoDrawable(Context context2, int placeholderResId2, Drawable placeholderDrawable, boolean debugging2) {
        Resources resources = context2.getResources();
        this.context = context2.getApplicationContext();
        this.density = resources.getDisplayMetrics().density;
        this.placeholderResId = placeholderResId2;
        this.placeHolderDrawable = placeholderResId2 != 0 ? resources.getDrawable(placeholderResId2) : placeholderDrawable;
        this.debugging = debugging2;
    }

    PicassoDrawable(Context context2, Bitmap bitmap, Request.LoadedFrom loadedFrom2, boolean noFade, boolean debugging2) {
        Resources resources = context2.getResources();
        this.context = context2.getApplicationContext();
        this.loadedFrom = loadedFrom2;
        this.density = resources.getDisplayMetrics().density;
        this.bitmapDrawable = new BitmapDrawable(resources, bitmap);
        this.debugging = debugging2;
        if (loadedFrom2 != Request.LoadedFrom.MEMORY && !noFade) {
            this.startTimeMillis = 0;
            this.animating = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void draw(Canvas canvas) {
        if (this.bitmapDrawable == null) {
            this.placeHolderDrawable.draw(canvas);
            return;
        }
        boolean done = true;
        if (this.animating) {
            if (this.startTimeMillis == 0) {
                this.startTimeMillis = SystemClock.uptimeMillis();
                done = false;
                this.alpha = 0;
            } else {
                float normalized = ((float) (SystemClock.uptimeMillis() - this.startTimeMillis)) / FADE_DURATION;
                if (normalized >= 1.0f) {
                    done = true;
                } else {
                    done = false;
                }
                this.alpha = (int) (255.0f * Math.min(normalized, 1.0f));
            }
        }
        if (done) {
            this.bitmapDrawable.draw(canvas);
        } else {
            if (this.placeHolderDrawable != null) {
                this.placeHolderDrawable.draw(canvas);
            }
            if (this.alpha > 0) {
                this.bitmapDrawable.setAlpha(this.alpha);
                this.bitmapDrawable.draw(canvas);
                this.bitmapDrawable.setAlpha(MotionEventCompat.ACTION_MASK);
            }
            invalidateSelf();
        }
        if (this.debugging) {
            drawDebugIndicator(canvas);
        }
    }

    public int getIntrinsicWidth() {
        if (this.bitmapDrawable != null) {
            return this.bitmapDrawable.getIntrinsicWidth();
        }
        return -1;
    }

    public int getIntrinsicHeight() {
        if (this.bitmapDrawable != null) {
            return this.bitmapDrawable.getIntrinsicHeight();
        }
        return -1;
    }

    public void setAlpha(int alpha2) {
    }

    public void setColorFilter(ColorFilter cf) {
    }

    public int getOpacity() {
        return -1;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        if (this.bitmapDrawable != null) {
            setBounds(this.bitmapDrawable);
        }
        if (this.placeHolderDrawable != null) {
            this.placeHolderDrawable.setBounds(getBounds());
        }
    }

    /* access modifiers changed from: package-private */
    public void setPlaceholder(int placeholderResId2, Drawable placeHolderDrawable2) {
        this.bitmapDrawable = null;
        this.loadedFrom = null;
        if (placeholderResId2 != 0) {
            if (this.placeholderResId != placeholderResId2) {
                this.placeHolderDrawable = this.context.getResources().getDrawable(placeholderResId2);
                this.placeHolderDrawable.setBounds(getBounds());
            }
        } else if (this.placeHolderDrawable != placeHolderDrawable2) {
            this.placeHolderDrawable = placeHolderDrawable2;
            this.placeHolderDrawable.setBounds(getBounds());
        }
        invalidateSelf();
    }

    /* access modifiers changed from: package-private */
    public void setBitmap(Bitmap bitmap, Request.LoadedFrom loadedFrom2, boolean noFade) {
        boolean fade = loadedFrom2 != Request.LoadedFrom.MEMORY && !noFade;
        if (this.bitmapDrawable != null && fade) {
            this.placeHolderDrawable = this.bitmapDrawable;
        }
        this.bitmapDrawable = new BitmapDrawable(this.context.getResources(), bitmap);
        setBounds(this.bitmapDrawable);
        this.loadedFrom = loadedFrom2;
        this.startTimeMillis = 0;
        this.animating = fade;
        invalidateSelf();
    }

    private void setBounds(Drawable drawable) {
        Rect bounds = getBounds();
        int width = bounds.width();
        int height = bounds.height();
        float ratio = ((float) width) / ((float) height);
        int drawableWidth = drawable.getIntrinsicWidth();
        int drawableHeight = drawable.getIntrinsicHeight();
        if (((float) drawableWidth) / ((float) drawableHeight) < ratio) {
            int scaledDrawableWidth = (int) (((float) drawableWidth) * (((float) height) / ((float) drawableHeight)));
            int drawableLeft = bounds.left - ((scaledDrawableWidth - width) / 2);
            int i = bounds.top;
            Drawable drawable2 = drawable;
            int i2 = i;
            drawable2.setBounds(drawableLeft, i2, drawableLeft + scaledDrawableWidth, bounds.bottom);
            return;
        }
        int scaledDrawableHeight = (int) (((float) drawableHeight) * (((float) width) / ((float) drawableWidth)));
        int drawableTop = bounds.top - ((scaledDrawableHeight - height) / 2);
        int i3 = bounds.left;
        drawable.setBounds(i3, drawableTop, bounds.right, drawableTop + scaledDrawableHeight);
    }

    private void drawDebugIndicator(Canvas canvas) {
        canvas.save();
        canvas.rotate(45.0f);
        DEBUG_PAINT.setColor(-1);
        canvas.drawRect(BitmapDescriptorFactory.HUE_RED, this.density * -10.0f, this.density * 7.5f, this.density * 10.0f, DEBUG_PAINT);
        DEBUG_PAINT.setColor(this.loadedFrom.debugColor);
        canvas.drawRect(BitmapDescriptorFactory.HUE_RED, this.density * -9.0f, this.density * 6.5f, this.density * 9.0f, DEBUG_PAINT);
        canvas.restore();
    }
}
