package com.google.android.apps.analytics;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;
import java.util.Random;

class PersistentHitStore implements HitStore {
    private static final String ACCOUNT_ID = "account_id";
    private static final String ACTION = "action";
    private static final String CATEGORY = "category";
    /* access modifiers changed from: private */
    public static final String CREATE_CUSTOM_VARIABLES_TABLE = ("CREATE TABLE custom_variables (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", CUSTOMVAR_ID) + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' INTEGER NOT NULL,", CUSTOMVAR_INDEX) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_NAME) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_VALUE) + String.format(" '%s' INTEGER NOT NULL);", CUSTOMVAR_SCOPE));
    /* access modifiers changed from: private */
    public static final String CREATE_CUSTOM_VAR_VISITOR_CACHE_TABLE = ("CREATE TABLE IF NOT EXISTS custom_var_visitor_cache (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", CUSTOMVAR_ID) + String.format(" '%s' INTEGER NOT NULL,", CUSTOMVAR_INDEX) + String.format(" '%s' %s,", CUSTOMVAR_NAME, CUSTOM_VARIABLE_COLUMN_TYPE) + String.format(" '%s' %s);", CUSTOMVAR_VALUE, CUSTOM_VARIABLE_COLUMN_TYPE));
    /* access modifiers changed from: private */
    public static final String CREATE_EVENTS_TABLE = ("CREATE TABLE events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", EVENT_ID) + String.format(" '%s' INTEGER NOT NULL,", USER_ID) + String.format(" '%s' CHAR(256) NOT NULL,", ACCOUNT_ID) + String.format(" '%s' INTEGER NOT NULL,", RANDOM_VAL) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_FIRST) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_PREVIOUS) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_CURRENT) + String.format(" '%s' INTEGER NOT NULL,", VISITS) + String.format(" '%s' CHAR(256) NOT NULL,", CATEGORY) + String.format(" '%s' CHAR(256) NOT NULL,", "action") + String.format(" '%s' CHAR(256), ", "label") + String.format(" '%s' INTEGER,", VALUE) + String.format(" '%s' INTEGER,", SCREEN_WIDTH) + String.format(" '%s' INTEGER);", SCREEN_HEIGHT));
    /* access modifiers changed from: private */
    public static final String CREATE_HITS_TABLE = ("CREATE TABLE IF NOT EXISTS hits (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", HIT_ID) + String.format(" '%s' TEXT NOT NULL,", HIT_STRING) + String.format(" '%s' INTEGER NOT NULL);", HIT_TIMESTAMP));
    private static final String CREATE_INSTALL_REFERRER_TABLE = "CREATE TABLE install_referrer (referrer TEXT PRIMARY KEY NOT NULL);";
    /* access modifiers changed from: private */
    public static final String CREATE_ITEM_EVENTS_TABLE = ("CREATE TABLE item_events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", ITEM_ID) + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' TEXT NOT NULL,", ORDER_ID) + String.format(" '%s' TEXT NOT NULL,", ITEM_SKU) + String.format(" '%s' TEXT,", ITEM_NAME) + String.format(" '%s' TEXT,", ITEM_CATEGORY) + String.format(" '%s' TEXT NOT NULL,", ITEM_PRICE) + String.format(" '%s' TEXT NOT NULL);", ITEM_COUNT));
    /* access modifiers changed from: private */
    public static final String CREATE_OLD_CUSTOM_VAR_CACHE_TABLE = ("CREATE TABLE IF NOT EXISTS custom_var_cache (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", CUSTOMVAR_ID) + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' INTEGER NOT NULL,", CUSTOMVAR_INDEX) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_NAME) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_VALUE) + String.format(" '%s' INTEGER NOT NULL);", CUSTOMVAR_SCOPE));
    private static final String CREATE_REFERRER_TABLE = "CREATE TABLE IF NOT EXISTS referrer (referrer TEXT PRIMARY KEY NOT NULL,timestamp_referrer INTEGER NOT NULL,referrer_visit INTEGER NOT NULL DEFAULT 1,referrer_index INTEGER NOT NULL DEFAULT 1);";
    /* access modifiers changed from: private */
    public static final String CREATE_SESSION_TABLE = ("CREATE TABLE IF NOT EXISTS session (" + String.format(" '%s' INTEGER PRIMARY KEY,", TIMESTAMP_FIRST) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_PREVIOUS) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_CURRENT) + String.format(" '%s' INTEGER NOT NULL,", VISITS) + String.format(" '%s' INTEGER NOT NULL);", STORE_ID));
    /* access modifiers changed from: private */
    public static final String CREATE_TRANSACTION_EVENTS_TABLE = ("CREATE TABLE transaction_events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", TRANSACTION_ID) + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' TEXT NOT NULL,", ORDER_ID) + String.format(" '%s' TEXT,", STORE_NAME) + String.format(" '%s' TEXT NOT NULL,", TOTAL_COST) + String.format(" '%s' TEXT,", TOTAL_TAX) + String.format(" '%s' TEXT);", SHIPPING_COST));
    private static final String CUSTOMVAR_ID = "cv_id";
    private static final String CUSTOMVAR_INDEX = "cv_index";
    private static final String CUSTOMVAR_NAME = "cv_name";
    private static final String CUSTOMVAR_SCOPE = "cv_scope";
    private static final String CUSTOMVAR_VALUE = "cv_value";
    private static final String CUSTOM_VARIABLE_COLUMN_TYPE = "CHAR(64) NOT NULL";
    static final String CUSTOM_VAR_VISITOR_CACHE_NAME = "custom_var_visitor_cache";
    private static final String DATABASE_NAME = "google_analytics.db";
    private static final int DATABASE_VERSION = 6;
    private static final String EVENT_ID = "event_id";
    private static final String HIT_ID = "hit_id";
    private static final String HIT_STRING = "hit_string";
    private static final String HIT_TIMESTAMP = "hit_time";
    private static final String ITEM_CATEGORY = "item_category";
    private static final String ITEM_COUNT = "item_count";
    private static final String ITEM_ID = "item_id";
    private static final String ITEM_NAME = "item_name";
    private static final String ITEM_PRICE = "item_price";
    private static final String ITEM_SKU = "item_sku";
    private static final String LABEL = "label";
    private static final int MAX_HITS = 1000;
    private static final String OLD_CUSTOM_VAR_CACHE_NAME = "custom_var_cache";
    private static final String ORDER_ID = "order_id";
    private static final String RANDOM_VAL = "random_val";
    static final String REFERRER = "referrer";
    static final String REFERRER_COLUMN = "referrer";
    static final String REFERRER_INDEX = "referrer_index";
    static final String REFERRER_VISIT = "referrer_visit";
    private static final String SCREEN_HEIGHT = "screen_height";
    private static final String SCREEN_WIDTH = "screen_width";
    private static final String SHIPPING_COST = "tran_shippingcost";
    private static final String STORE_ID = "store_id";
    private static final String STORE_NAME = "tran_storename";
    private static final String TIMESTAMP_CURRENT = "timestamp_current";
    private static final String TIMESTAMP_FIRST = "timestamp_first";
    private static final String TIMESTAMP_PREVIOUS = "timestamp_previous";
    static final String TIMESTAMP_REFERRER = "timestamp_referrer";
    private static final String TOTAL_COST = "tran_totalcost";
    private static final String TOTAL_TAX = "tran_totaltax";
    private static final String TRANSACTION_ID = "tran_id";
    private static final String USER_ID = "user_id";
    private static final String VALUE = "value";
    private static final String VISITS = "visits";
    private boolean anonymizeIp;
    private DataBaseHelper databaseHelper;
    private volatile int numStoredHits;
    private Random random;
    private int sampleRate;
    private boolean sessionStarted;
    private int storeId;
    private long timestampCurrent;
    private long timestampFirst;
    private long timestampPrevious;
    private boolean useStoredVisitorVars;
    /* access modifiers changed from: private */
    public CustomVariableBuffer visitorCVCache;
    private int visits;

    static class DataBaseHelper extends SQLiteOpenHelper {
        private final int databaseVersion;
        private final PersistentHitStore store;

        public DataBaseHelper(Context context, PersistentHitStore persistentHitStore) {
            this(context, PersistentHitStore.DATABASE_NAME, 6, persistentHitStore);
        }

        DataBaseHelper(Context context, String str, int i, PersistentHitStore persistentHitStore) {
            super(context, str, (SQLiteDatabase.CursorFactory) null, i);
            this.databaseVersion = i;
            this.store = persistentHitStore;
        }

        public DataBaseHelper(Context context, String str, PersistentHitStore persistentHitStore) {
            this(context, str, 6, persistentHitStore);
        }

        private void createECommerceTables(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS transaction_events;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_TRANSACTION_EVENTS_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS item_events;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_ITEM_EVENTS_TABLE);
        }

        private void createHitTable(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS hits;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_HITS_TABLE);
        }

        private void createReferrerTable(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS referrer;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_REFERRER_TABLE);
        }

        private void fixCVTables(SQLiteDatabase sQLiteDatabase) {
            if (!tablePresent(PersistentHitStore.CREATE_CUSTOM_VAR_VISITOR_CACHE_TABLE, sQLiteDatabase)) {
                sQLiteDatabase.execSQL(PersistentHitStore.CREATE_CUSTOM_VAR_VISITOR_CACHE_TABLE);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:49:0x00e7  */
        /* JADX WARNING: Removed duplicated region for block: B:52:0x00f0  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void fixReferrerTable(android.database.sqlite.SQLiteDatabase r13) {
            /*
                r12 = this;
                r11 = -1
                r10 = 0
                r8 = 1
                r9 = 0
                java.lang.String r1 = "referrer"
                r2 = 0
                r3 = 0
                r4 = 0
                r5 = 0
                r6 = 0
                r7 = 0
                r0 = r13
                android.database.Cursor r6 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00c9, all -> 0x00e3 }
                java.lang.String[] r3 = r6.getColumnNames()     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                r2 = r10
                r0 = r10
                r1 = r10
            L_0x0018:
                int r4 = r3.length     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                if (r2 >= r4) goto L_0x0035
                r4 = r3[r2]     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                java.lang.String r5 = "referrer_index"
                boolean r4 = r4.equals(r5)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                if (r4 == 0) goto L_0x0029
                r1 = r8
            L_0x0026:
                int r2 = r2 + 1
                goto L_0x0018
            L_0x0029:
                r4 = r3[r2]     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                java.lang.String r5 = "referrer_visit"
                boolean r4 = r4.equals(r5)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                if (r4 == 0) goto L_0x0026
                r0 = r8
                goto L_0x0026
            L_0x0035:
                if (r1 == 0) goto L_0x0039
                if (r0 != 0) goto L_0x00b0
            L_0x0039:
                boolean r0 = r6.moveToFirst()     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                if (r0 == 0) goto L_0x00fc
                java.lang.String r0 = "referrer_visit"
                int r4 = r6.getColumnIndex(r0)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                java.lang.String r0 = "referrer_index"
                int r5 = r6.getColumnIndex(r0)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                com.google.android.apps.analytics.Referrer r0 = new com.google.android.apps.analytics.Referrer     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                java.lang.String r1 = "referrer"
                int r1 = r6.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                java.lang.String r1 = r6.getString(r1)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                java.lang.String r2 = "timestamp_referrer"
                int r2 = r6.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                long r2 = r6.getLong(r2)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                if (r4 != r11) goto L_0x00bf
                r4 = r8
            L_0x0064:
                if (r5 != r11) goto L_0x00c4
                r5 = r8
            L_0x0067:
                r0.<init>(r1, r2, r4, r5)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
            L_0x006a:
                r13.beginTransaction()     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                r12.createReferrerTable(r13)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                if (r0 == 0) goto L_0x00ad
                android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                r1.<init>()     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                java.lang.String r2 = "referrer"
                java.lang.String r3 = r0.getReferrerString()     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                java.lang.String r2 = "timestamp_referrer"
                long r3 = r0.getTimeStamp()     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                java.lang.String r2 = "referrer_visit"
                int r3 = r0.getVisit()     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                java.lang.String r2 = "referrer_index"
                int r0 = r0.getIndex()     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                r1.put(r2, r0)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                java.lang.String r0 = "referrer"
                r2 = 0
                r13.insert(r0, r2, r1)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
            L_0x00ad:
                r13.setTransactionSuccessful()     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
            L_0x00b0:
                if (r6 == 0) goto L_0x00b5
                r6.close()
            L_0x00b5:
                boolean r0 = r13.inTransaction()
                if (r0 == 0) goto L_0x00be
                boolean unused = com.google.android.apps.analytics.PersistentHitStore.endTransaction(r13)
            L_0x00be:
                return
            L_0x00bf:
                int r4 = r6.getInt(r4)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                goto L_0x0064
            L_0x00c4:
                int r5 = r6.getInt(r5)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x00f4 }
                goto L_0x0067
            L_0x00c9:
                r0 = move-exception
                r1 = r9
            L_0x00cb:
                java.lang.String r2 = "GoogleAnalyticsTracker"
                java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f6 }
                android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00f6 }
                if (r1 == 0) goto L_0x00d9
                r1.close()
            L_0x00d9:
                boolean r0 = r13.inTransaction()
                if (r0 == 0) goto L_0x00be
                boolean unused = com.google.android.apps.analytics.PersistentHitStore.endTransaction(r13)
                goto L_0x00be
            L_0x00e3:
                r0 = move-exception
                r6 = r9
            L_0x00e5:
                if (r6 == 0) goto L_0x00ea
                r6.close()
            L_0x00ea:
                boolean r1 = r13.inTransaction()
                if (r1 == 0) goto L_0x00f3
                boolean unused = com.google.android.apps.analytics.PersistentHitStore.endTransaction(r13)
            L_0x00f3:
                throw r0
            L_0x00f4:
                r0 = move-exception
                goto L_0x00e5
            L_0x00f6:
                r0 = move-exception
                r6 = r1
                goto L_0x00e5
            L_0x00f9:
                r0 = move-exception
                r1 = r6
                goto L_0x00cb
            L_0x00fc:
                r0 = r9
                goto L_0x006a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.DataBaseHelper.fixReferrerTable(android.database.sqlite.SQLiteDatabase):void");
        }

        private void migrateEventsToHits(SQLiteDatabase sQLiteDatabase, int i) {
            this.store.loadExistingSession(sQLiteDatabase);
            CustomVariableBuffer unused = this.store.visitorCVCache = this.store.getVisitorVarBuffer(sQLiteDatabase);
            Event[] peekEvents = this.store.peekEvents(1000, sQLiteDatabase, i);
            for (Event access$900 : peekEvents) {
                this.store.putEvent(access$900, sQLiteDatabase, false);
            }
            sQLiteDatabase.execSQL("DELETE from events;");
            sQLiteDatabase.execSQL("DELETE from item_events;");
            sQLiteDatabase.execSQL("DELETE from transaction_events;");
            sQLiteDatabase.execSQL("DELETE from custom_variables;");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x007f  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0084  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x008d  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0092  */
        /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void migratePreV4Referrer(android.database.sqlite.SQLiteDatabase r14) {
            /*
                r13 = this;
                r8 = 0
                java.lang.String r1 = "install_referrer"
                r0 = 1
                java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0072, all -> 0x0088 }
                r0 = 0
                java.lang.String r3 = "referrer"
                r2[r0] = r3     // Catch:{ SQLiteException -> 0x0072, all -> 0x0088 }
                r3 = 0
                r4 = 0
                r5 = 0
                r6 = 0
                r7 = 0
                r0 = r14
                android.database.Cursor r9 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0072, all -> 0x0088 }
                r10 = 0
                boolean r0 = r9.moveToFirst()     // Catch:{ SQLiteException -> 0x009e, all -> 0x0096 }
                if (r0 == 0) goto L_0x00a7
                r0 = 0
                java.lang.String r12 = r9.getString(r0)     // Catch:{ SQLiteException -> 0x009e, all -> 0x0096 }
                java.lang.String r1 = "session"
                r2 = 0
                r3 = 0
                r4 = 0
                r5 = 0
                r6 = 0
                r7 = 0
                r0 = r14
                android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x009e, all -> 0x0096 }
                boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x00a2, all -> 0x0099 }
                if (r0 == 0) goto L_0x00a5
                r0 = 0
                long r2 = r1.getLong(r0)     // Catch:{ SQLiteException -> 0x00a2, all -> 0x0099 }
            L_0x003a:
                android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x00a2, all -> 0x0099 }
                r0.<init>()     // Catch:{ SQLiteException -> 0x00a2, all -> 0x0099 }
                java.lang.String r4 = "referrer"
                r0.put(r4, r12)     // Catch:{ SQLiteException -> 0x00a2, all -> 0x0099 }
                java.lang.String r4 = "timestamp_referrer"
                java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ SQLiteException -> 0x00a2, all -> 0x0099 }
                r0.put(r4, r2)     // Catch:{ SQLiteException -> 0x00a2, all -> 0x0099 }
                java.lang.String r2 = "referrer_visit"
                r3 = 1
                java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x00a2, all -> 0x0099 }
                r0.put(r2, r3)     // Catch:{ SQLiteException -> 0x00a2, all -> 0x0099 }
                java.lang.String r2 = "referrer_index"
                r3 = 1
                java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x00a2, all -> 0x0099 }
                r0.put(r2, r3)     // Catch:{ SQLiteException -> 0x00a2, all -> 0x0099 }
                java.lang.String r2 = "referrer"
                r3 = 0
                r14.insert(r2, r3, r0)     // Catch:{ SQLiteException -> 0x00a2, all -> 0x0099 }
            L_0x0067:
                if (r9 == 0) goto L_0x006c
                r9.close()
            L_0x006c:
                if (r1 == 0) goto L_0x0071
                r1.close()
            L_0x0071:
                return
            L_0x0072:
                r0 = move-exception
                r1 = r8
            L_0x0074:
                java.lang.String r2 = "GoogleAnalyticsTracker"
                java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x009b }
                android.util.Log.e(r2, r0)     // Catch:{ all -> 0x009b }
                if (r8 == 0) goto L_0x0082
                r8.close()
            L_0x0082:
                if (r1 == 0) goto L_0x0071
                r1.close()
                goto L_0x0071
            L_0x0088:
                r0 = move-exception
                r1 = r8
                r9 = r8
            L_0x008b:
                if (r9 == 0) goto L_0x0090
                r9.close()
            L_0x0090:
                if (r1 == 0) goto L_0x0095
                r1.close()
            L_0x0095:
                throw r0
            L_0x0096:
                r0 = move-exception
                r1 = r8
                goto L_0x008b
            L_0x0099:
                r0 = move-exception
                goto L_0x008b
            L_0x009b:
                r0 = move-exception
                r9 = r8
                goto L_0x008b
            L_0x009e:
                r0 = move-exception
                r1 = r8
                r8 = r9
                goto L_0x0074
            L_0x00a2:
                r0 = move-exception
                r8 = r9
                goto L_0x0074
            L_0x00a5:
                r2 = r10
                goto L_0x003a
            L_0x00a7:
                r1 = r8
                goto L_0x0067
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.DataBaseHelper.migratePreV4Referrer(android.database.sqlite.SQLiteDatabase):void");
        }

        private void migrateVisitorLevelCustomVars(SQLiteDatabase sQLiteDatabase) {
            Cursor query = sQLiteDatabase.query(PersistentHitStore.OLD_CUSTOM_VAR_CACHE_NAME, null, "cv_scope= ?", new String[]{Integer.toString(1)}, null, null, null);
            while (query.moveToNext()) {
                int i = query.getInt(query.getColumnIndex(PersistentHitStore.CUSTOMVAR_INDEX));
                String string = query.getString(query.getColumnIndex(PersistentHitStore.CUSTOMVAR_NAME));
                String string2 = query.getString(query.getColumnIndex(PersistentHitStore.CUSTOMVAR_VALUE));
                if (i > 0 && i <= 50) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(PersistentHitStore.CUSTOMVAR_INDEX, Integer.valueOf(i));
                    contentValues.put(PersistentHitStore.CUSTOMVAR_NAME, string);
                    contentValues.put(PersistentHitStore.CUSTOMVAR_VALUE, string2);
                    sQLiteDatabase.insert(PersistentHitStore.CUSTOM_VAR_VISITOR_CACHE_NAME, null, contentValues);
                }
            }
            if (query != null) {
                query.close();
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:17:0x004a  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private boolean tablePresent(java.lang.String r11, android.database.sqlite.SQLiteDatabase r12) {
            /*
                r10 = this;
                r8 = 0
                r9 = 0
                java.lang.String r1 = "SQLITE_MASTER"
                r0 = 1
                java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0026, all -> 0x0047 }
                r0 = 0
                java.lang.String r3 = "name"
                r2[r0] = r3     // Catch:{ SQLiteException -> 0x0026, all -> 0x0047 }
                java.lang.String r3 = "name=?"
                r0 = 1
                java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0026, all -> 0x0047 }
                r0 = 0
                r4[r0] = r11     // Catch:{ SQLiteException -> 0x0026, all -> 0x0047 }
                r5 = 0
                r6 = 0
                r7 = 0
                r0 = r12
                android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0026, all -> 0x0047 }
                boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0055, all -> 0x004e }
                if (r1 == 0) goto L_0x0025
                r1.close()
            L_0x0025:
                return r0
            L_0x0026:
                r0 = move-exception
                r0 = r9
            L_0x0028:
                java.lang.String r1 = "GoogleAnalyticsTracker"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0051 }
                r2.<init>()     // Catch:{ all -> 0x0051 }
                java.lang.String r3 = "error querying for table "
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0051 }
                java.lang.StringBuilder r2 = r2.append(r11)     // Catch:{ all -> 0x0051 }
                java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0051 }
                android.util.Log.w(r1, r2)     // Catch:{ all -> 0x0051 }
                if (r0 == 0) goto L_0x0045
                r0.close()
            L_0x0045:
                r0 = r8
                goto L_0x0025
            L_0x0047:
                r0 = move-exception
            L_0x0048:
                if (r9 == 0) goto L_0x004d
                r9.close()
            L_0x004d:
                throw r0
            L_0x004e:
                r0 = move-exception
                r9 = r1
                goto L_0x0048
            L_0x0051:
                r1 = move-exception
                r9 = r0
                r0 = r1
                goto L_0x0048
            L_0x0055:
                r0 = move-exception
                r0 = r1
                goto L_0x0028
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.DataBaseHelper.tablePresent(java.lang.String, android.database.sqlite.SQLiteDatabase):boolean");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        /* access modifiers changed from: package-private */
        public void createCustomVariableTables(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS custom_variables;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_CUSTOM_VARIABLES_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS custom_var_cache;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_OLD_CUSTOM_VAR_CACHE_TABLE);
            for (int i = 1; i <= 5; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(PersistentHitStore.EVENT_ID, (Integer) 0);
                contentValues.put(PersistentHitStore.CUSTOMVAR_INDEX, Integer.valueOf(i));
                contentValues.put(PersistentHitStore.CUSTOMVAR_NAME, "");
                contentValues.put(PersistentHitStore.CUSTOMVAR_SCOPE, (Integer) 3);
                contentValues.put(PersistentHitStore.CUSTOMVAR_VALUE, "");
                sQLiteDatabase.insert(PersistentHitStore.OLD_CUSTOM_VAR_CACHE_NAME, PersistentHitStore.EVENT_ID, contentValues);
            }
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS custom_var_visitor_cache;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_CUSTOM_VAR_VISITOR_CACHE_TABLE);
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS events;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_EVENTS_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS install_referrer;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_INSTALL_REFERRER_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS session;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_SESSION_TABLE);
            if (this.databaseVersion > 1) {
                createCustomVariableTables(sQLiteDatabase);
            }
            if (this.databaseVersion > 2) {
                createECommerceTables(sQLiteDatabase);
            }
            if (this.databaseVersion > 3) {
                createHitTable(sQLiteDatabase);
                createReferrerTable(sQLiteDatabase);
            }
        }

        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            Log.w(GoogleAnalyticsTracker.LOG_TAG, "Downgrading database version from " + i + " to " + i2 + " not recommended.");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_REFERRER_TABLE);
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_HITS_TABLE);
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_CUSTOM_VAR_VISITOR_CACHE_TABLE);
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_SESSION_TABLE);
        }

        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (sQLiteDatabase.isReadOnly()) {
                Log.w(GoogleAnalyticsTracker.LOG_TAG, "Warning: Need to update database, but it's read only.");
                return;
            }
            fixReferrerTable(sQLiteDatabase);
            fixCVTables(sQLiteDatabase);
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            if (i > i2) {
                onDowngrade(sQLiteDatabase, i, i2);
                return;
            }
            if (i < 2 && i2 > 1) {
                createCustomVariableTables(sQLiteDatabase);
            }
            if (i < 3 && i2 > 2) {
                createECommerceTables(sQLiteDatabase);
            }
            if (i < 4 && i2 > 3) {
                createHitTable(sQLiteDatabase);
                createReferrerTable(sQLiteDatabase);
                migrateEventsToHits(sQLiteDatabase, i);
                migratePreV4Referrer(sQLiteDatabase);
            }
            if (i < 6 && i2 > 5) {
                sQLiteDatabase.execSQL(PersistentHitStore.CREATE_CUSTOM_VAR_VISITOR_CACHE_TABLE);
                migrateVisitorLevelCustomVars(sQLiteDatabase);
            }
        }
    }

    PersistentHitStore(Context context) {
        this(context, DATABASE_NAME, 6);
    }

    PersistentHitStore(Context context, String str) {
        this(context, str, 6);
    }

    PersistentHitStore(Context context, String str, int i) {
        this.sampleRate = 100;
        this.random = new Random();
        this.databaseHelper = new DataBaseHelper(context, str, i, this);
        loadExistingSession();
        this.visitorCVCache = getVisitorVarBuffer();
    }

    PersistentHitStore(DataBaseHelper dataBaseHelper) {
        this.sampleRate = 100;
        this.random = new Random();
        this.databaseHelper = dataBaseHelper;
        loadExistingSession();
        this.visitorCVCache = getVisitorVarBuffer();
    }

    /* access modifiers changed from: private */
    public static boolean endTransaction(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.endTransaction();
            return true;
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, "exception ending transaction:" + e.toString());
            return false;
        }
    }

    static String formatReferrer(String str) {
        if (str == null) {
            return null;
        }
        if (!str.contains("=")) {
            if (!str.contains("%3D")) {
                return null;
            }
            try {
                str = URLDecoder.decode(str, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                return null;
            }
        }
        Map<String, String> parseURLParameters = Utils.parseURLParameters(str);
        boolean z = parseURLParameters.get("utm_campaign") != null;
        boolean z2 = parseURLParameters.get("utm_medium") != null;
        boolean z3 = parseURLParameters.get("utm_source") != null;
        if ((parseURLParameters.get("gclid") != null) || (z && z2 && z3)) {
            String[][] strArr = {new String[]{"utmcid", parseURLParameters.get("utm_id")}, new String[]{"utmcsr", parseURLParameters.get("utm_source")}, new String[]{"utmgclid", parseURLParameters.get("gclid")}, new String[]{"utmccn", parseURLParameters.get("utm_campaign")}, new String[]{"utmcmd", parseURLParameters.get("utm_medium")}, new String[]{"utmctr", parseURLParameters.get("utm_term")}, new String[]{"utmcct", parseURLParameters.get("utm_content")}};
            StringBuilder sb = new StringBuilder();
            boolean z4 = true;
            for (int i = 0; i < strArr.length; i++) {
                if (strArr[i][1] != null) {
                    String replace = strArr[i][1].replace("+", "%20").replace(" ", "%20");
                    if (z4) {
                        z4 = false;
                    } else {
                        sb.append("|");
                    }
                    sb.append(strArr[i][0]).append("=").append(replace);
                }
            }
            return sb.toString();
        }
        Log.w(GoogleAnalyticsTracker.LOG_TAG, "Badly formatted referrer missing campaign, medium and source or click ID");
        return null;
    }

    private Referrer getAndUpdateReferrer(SQLiteDatabase sQLiteDatabase) {
        Referrer readCurrentReferrer = readCurrentReferrer(sQLiteDatabase);
        if (readCurrentReferrer == null) {
            return null;
        }
        if (readCurrentReferrer.getTimeStamp() != 0) {
            return readCurrentReferrer;
        }
        int index = readCurrentReferrer.getIndex();
        String referrerString = readCurrentReferrer.getReferrerString();
        ContentValues contentValues = new ContentValues();
        contentValues.put("referrer", referrerString);
        contentValues.put(TIMESTAMP_REFERRER, Long.valueOf(this.timestampCurrent));
        contentValues.put(REFERRER_VISIT, Integer.valueOf(this.visits));
        contentValues.put(REFERRER_INDEX, Integer.valueOf(index));
        if (setReferrerDatabase(sQLiteDatabase, contentValues)) {
            return new Referrer(referrerString, this.timestampCurrent, this.visits, index);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void putEvent(Event event, SQLiteDatabase sQLiteDatabase, boolean z) {
        if (!event.isSessionInitialized()) {
            event.setRandomVal(this.random.nextInt(Integer.MAX_VALUE));
            event.setTimestampFirst((int) this.timestampFirst);
            event.setTimestampPrevious((int) this.timestampPrevious);
            event.setTimestampCurrent((int) this.timestampCurrent);
            event.setVisits(this.visits);
        }
        event.setAnonymizeIp(this.anonymizeIp);
        if (event.getUserId() == -1) {
            event.setUserId(this.storeId);
        }
        putCustomVariables(event, sQLiteDatabase);
        Referrer andUpdateReferrer = getAndUpdateReferrer(sQLiteDatabase);
        String[] split = event.accountId.split(",");
        if (split.length == 1) {
            writeEventToDatabase(event, andUpdateReferrer, sQLiteDatabase, z);
            return;
        }
        for (String event2 : split) {
            writeEventToDatabase(new Event(event, event2), andUpdateReferrer, sQLiteDatabase, z);
        }
    }

    private boolean setReferrerDatabase(SQLiteDatabase sQLiteDatabase, ContentValues contentValues) {
        try {
            sQLiteDatabase.beginTransaction();
            sQLiteDatabase.delete("referrer", null, null);
            sQLiteDatabase.insert("referrer", null, contentValues);
            sQLiteDatabase.setTransactionSuccessful();
            return !sQLiteDatabase.inTransaction() || endTransaction(sQLiteDatabase);
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
            if (!sQLiteDatabase.inTransaction() || !endTransaction(sQLiteDatabase)) {
            }
            return false;
        } catch (Throwable th) {
            if (sQLiteDatabase.inTransaction() && !endTransaction(sQLiteDatabase)) {
                return false;
            }
            throw th;
        }
    }

    public void clearReferrer() {
        try {
            this.databaseHelper.getWritableDatabase().delete("referrer", null, null);
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
    }

    public synchronized void deleteHit(long j) {
        try {
            this.numStoredHits -= this.databaseHelper.getWritableDatabase().delete("hits", "hit_id = ?", new String[]{Long.toString(j)});
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
        return;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.CustomVariableBuffer getCustomVariables(long r11, android.database.sqlite.SQLiteDatabase r13) {
        /*
            r10 = this;
            r8 = 0
            com.google.android.apps.analytics.CustomVariableBuffer r9 = new com.google.android.apps.analytics.CustomVariableBuffer
            r9.<init>()
            java.lang.String r1 = "custom_variables"
            r2 = 0
            java.lang.String r3 = "event_id= ?"
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0074, all -> 0x006a }
            r0 = 0
            java.lang.String r5 = java.lang.Long.toString(r11)     // Catch:{ SQLiteException -> 0x0074, all -> 0x006a }
            r4[r0] = r5     // Catch:{ SQLiteException -> 0x0074, all -> 0x006a }
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r13
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0074, all -> 0x006a }
        L_0x001d:
            boolean r0 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x0054 }
            if (r0 == 0) goto L_0x0064
            com.google.android.apps.analytics.CustomVariable r0 = new com.google.android.apps.analytics.CustomVariable     // Catch:{ SQLiteException -> 0x0054 }
            java.lang.String r2 = "cv_index"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x0054 }
            int r2 = r1.getInt(r2)     // Catch:{ SQLiteException -> 0x0054 }
            java.lang.String r3 = "cv_name"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x0054 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ SQLiteException -> 0x0054 }
            java.lang.String r4 = "cv_value"
            int r4 = r1.getColumnIndex(r4)     // Catch:{ SQLiteException -> 0x0054 }
            java.lang.String r4 = r1.getString(r4)     // Catch:{ SQLiteException -> 0x0054 }
            java.lang.String r5 = "cv_scope"
            int r5 = r1.getColumnIndex(r5)     // Catch:{ SQLiteException -> 0x0054 }
            int r5 = r1.getInt(r5)     // Catch:{ SQLiteException -> 0x0054 }
            r0.<init>(r2, r3, r4, r5)     // Catch:{ SQLiteException -> 0x0054 }
            r9.setCustomVariable(r0)     // Catch:{ SQLiteException -> 0x0054 }
            goto L_0x001d
        L_0x0054:
            r0 = move-exception
        L_0x0055:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0072 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0072 }
            if (r1 == 0) goto L_0x0063
            r1.close()
        L_0x0063:
            return r9
        L_0x0064:
            if (r1 == 0) goto L_0x0063
            r1.close()
            goto L_0x0063
        L_0x006a:
            r0 = move-exception
            r1 = r8
        L_0x006c:
            if (r1 == 0) goto L_0x0071
            r1.close()
        L_0x0071:
            throw r0
        L_0x0072:
            r0 = move-exception
            goto L_0x006c
        L_0x0074:
            r0 = move-exception
            r1 = r8
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getCustomVariables(long, android.database.sqlite.SQLiteDatabase):com.google.android.apps.analytics.CustomVariableBuffer");
    }

    /* access modifiers changed from: package-private */
    public DataBaseHelper getDatabaseHelper() {
        return this.databaseHelper;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Item getItem(long r10, android.database.sqlite.SQLiteDatabase r12) {
        /*
            r9 = this;
            r8 = 0
            java.lang.String r1 = "item_events"
            r2 = 0
            java.lang.String r3 = "event_id= ?"
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0078, all -> 0x0089 }
            r0 = 0
            java.lang.String r5 = java.lang.Long.toString(r10)     // Catch:{ SQLiteException -> 0x0078, all -> 0x0089 }
            r4[r0] = r5     // Catch:{ SQLiteException -> 0x0078, all -> 0x0089 }
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r12
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0078, all -> 0x0089 }
            boolean r0 = r7.moveToFirst()     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            if (r0 == 0) goto L_0x0071
            com.google.android.apps.analytics.Item$Builder r0 = new com.google.android.apps.analytics.Item$Builder     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r1 = "order_id"
            int r1 = r7.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r2 = "item_sku"
            int r2 = r7.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r2 = r7.getString(r2)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r3 = "item_price"
            int r3 = r7.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            double r3 = r7.getDouble(r3)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r5 = "item_count"
            int r5 = r7.getColumnIndex(r5)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            long r5 = r7.getLong(r5)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            r0.<init>(r1, r2, r3, r5)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r1 = "item_name"
            int r1 = r7.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            com.google.android.apps.analytics.Item$Builder r0 = r0.setItemName(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r1 = "item_category"
            int r1 = r7.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            com.google.android.apps.analytics.Item$Builder r0 = r0.setItemCategory(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            com.google.android.apps.analytics.Item r0 = r0.build()     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            if (r7 == 0) goto L_0x0070
            r7.close()
        L_0x0070:
            return r0
        L_0x0071:
            if (r7 == 0) goto L_0x0076
            r7.close()
        L_0x0076:
            r0 = r8
            goto L_0x0070
        L_0x0078:
            r0 = move-exception
            r1 = r8
        L_0x007a:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0093 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0093 }
            if (r1 == 0) goto L_0x0076
            r1.close()
            goto L_0x0076
        L_0x0089:
            r0 = move-exception
            r7 = r8
        L_0x008b:
            if (r7 == 0) goto L_0x0090
            r7.close()
        L_0x0090:
            throw r0
        L_0x0091:
            r0 = move-exception
            goto L_0x008b
        L_0x0093:
            r0 = move-exception
            r7 = r1
            goto L_0x008b
        L_0x0096:
            r0 = move-exception
            r1 = r7
            goto L_0x007a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getItem(long, android.database.sqlite.SQLiteDatabase):com.google.android.apps.analytics.Item");
    }

    public int getNumStoredHits() {
        return this.numStoredHits;
    }

    public int getNumStoredHitsFromDb() {
        Cursor cursor = null;
        int i = 0;
        try {
            Cursor rawQuery = this.databaseHelper.getReadableDatabase().rawQuery("SELECT COUNT(*) from hits", null);
            if (rawQuery.moveToFirst()) {
                i = (int) rawQuery.getLong(0);
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return i;
    }

    public Referrer getReferrer() {
        try {
            return readCurrentReferrer(this.databaseHelper.getReadableDatabase());
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
            return null;
        }
    }

    public String getSessionId() {
        if (!this.sessionStarted) {
            return null;
        }
        return Integer.toString((int) this.timestampCurrent);
    }

    public int getStoreId() {
        return this.storeId;
    }

    /* access modifiers changed from: package-private */
    public long getTimestampCurrent() {
        return this.timestampCurrent;
    }

    /* access modifiers changed from: package-private */
    public long getTimestampFirst() {
        return this.timestampFirst;
    }

    /* access modifiers changed from: package-private */
    public long getTimestampPrevious() {
        return this.timestampPrevious;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0087  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Transaction getTransaction(long r10, android.database.sqlite.SQLiteDatabase r12) {
        /*
            r9 = this;
            r8 = 0
            java.lang.String r1 = "transaction_events"
            r2 = 0
            java.lang.String r3 = "event_id= ?"
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0072, all -> 0x0083 }
            r0 = 0
            java.lang.String r5 = java.lang.Long.toString(r10)     // Catch:{ SQLiteException -> 0x0072, all -> 0x0083 }
            r4[r0] = r5     // Catch:{ SQLiteException -> 0x0072, all -> 0x0083 }
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r12
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0072, all -> 0x0083 }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x008d }
            if (r0 == 0) goto L_0x006b
            com.google.android.apps.analytics.Transaction$Builder r0 = new com.google.android.apps.analytics.Transaction$Builder     // Catch:{ SQLiteException -> 0x008d }
            java.lang.String r2 = "order_id"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x008d }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ SQLiteException -> 0x008d }
            java.lang.String r3 = "tran_totalcost"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x008d }
            double r3 = r1.getDouble(r3)     // Catch:{ SQLiteException -> 0x008d }
            r0.<init>(r2, r3)     // Catch:{ SQLiteException -> 0x008d }
            java.lang.String r2 = "tran_storename"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x008d }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ SQLiteException -> 0x008d }
            com.google.android.apps.analytics.Transaction$Builder r0 = r0.setStoreName(r2)     // Catch:{ SQLiteException -> 0x008d }
            java.lang.String r2 = "tran_totaltax"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x008d }
            double r2 = r1.getDouble(r2)     // Catch:{ SQLiteException -> 0x008d }
            com.google.android.apps.analytics.Transaction$Builder r0 = r0.setTotalTax(r2)     // Catch:{ SQLiteException -> 0x008d }
            java.lang.String r2 = "tran_shippingcost"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x008d }
            double r2 = r1.getDouble(r2)     // Catch:{ SQLiteException -> 0x008d }
            com.google.android.apps.analytics.Transaction$Builder r0 = r0.setShippingCost(r2)     // Catch:{ SQLiteException -> 0x008d }
            com.google.android.apps.analytics.Transaction r0 = r0.build()     // Catch:{ SQLiteException -> 0x008d }
            if (r1 == 0) goto L_0x006a
            r1.close()
        L_0x006a:
            return r0
        L_0x006b:
            if (r1 == 0) goto L_0x0070
            r1.close()
        L_0x0070:
            r0 = r8
            goto L_0x006a
        L_0x0072:
            r0 = move-exception
            r1 = r8
        L_0x0074:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x008b }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x008b }
            if (r1 == 0) goto L_0x0070
            r1.close()
            goto L_0x0070
        L_0x0083:
            r0 = move-exception
            r1 = r8
        L_0x0085:
            if (r1 == 0) goto L_0x008a
            r1.close()
        L_0x008a:
            throw r0
        L_0x008b:
            r0 = move-exception
            goto L_0x0085
        L_0x008d:
            r0 = move-exception
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getTransaction(long, android.database.sqlite.SQLiteDatabase):com.google.android.apps.analytics.Transaction");
    }

    public String getVisitorCustomVar(int i) {
        CustomVariable customVariableAt = this.visitorCVCache.getCustomVariableAt(i);
        if (customVariableAt == null || customVariableAt.getScope() != 1) {
            return null;
        }
        return customVariableAt.getValue();
    }

    public String getVisitorId() {
        if (!this.sessionStarted) {
            return null;
        }
        return String.format("%d.%d", Integer.valueOf(this.storeId), Long.valueOf(this.timestampFirst));
    }

    /* access modifiers changed from: package-private */
    public CustomVariableBuffer getVisitorVarBuffer() {
        try {
            return getVisitorVarBuffer(this.databaseHelper.getReadableDatabase());
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
            return new CustomVariableBuffer();
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x006d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.CustomVariableBuffer getVisitorVarBuffer(android.database.sqlite.SQLiteDatabase r11) {
        /*
            r10 = this;
            r8 = 0
            com.google.android.apps.analytics.CustomVariableBuffer r9 = new com.google.android.apps.analytics.CustomVariableBuffer
            r9.<init>()
            java.lang.String r1 = "custom_var_visitor_cache"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r11
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0073, all -> 0x0069 }
        L_0x0013:
            boolean r0 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x0053 }
            if (r0 == 0) goto L_0x0063
            java.lang.String r0 = "cv_index"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ SQLiteException -> 0x0053 }
            if (r0 <= 0) goto L_0x0013
            java.lang.String r0 = "cv_index"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ SQLiteException -> 0x0053 }
            r2 = 50
            if (r0 > r2) goto L_0x0013
            com.google.android.apps.analytics.CustomVariable r0 = new com.google.android.apps.analytics.CustomVariable     // Catch:{ SQLiteException -> 0x0053 }
            java.lang.String r2 = "cv_index"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x0053 }
            int r2 = r1.getInt(r2)     // Catch:{ SQLiteException -> 0x0053 }
            java.lang.String r3 = "cv_name"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x0053 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ SQLiteException -> 0x0053 }
            java.lang.String r4 = "cv_value"
            int r4 = r1.getColumnIndex(r4)     // Catch:{ SQLiteException -> 0x0053 }
            java.lang.String r4 = r1.getString(r4)     // Catch:{ SQLiteException -> 0x0053 }
            r5 = 1
            r0.<init>(r2, r3, r4, r5)     // Catch:{ SQLiteException -> 0x0053 }
            r9.setCustomVariable(r0)     // Catch:{ SQLiteException -> 0x0053 }
            goto L_0x0013
        L_0x0053:
            r0 = move-exception
        L_0x0054:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0071 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0071 }
            if (r1 == 0) goto L_0x0062
            r1.close()
        L_0x0062:
            return r9
        L_0x0063:
            if (r1 == 0) goto L_0x0062
            r1.close()
            goto L_0x0062
        L_0x0069:
            r0 = move-exception
            r1 = r8
        L_0x006b:
            if (r1 == 0) goto L_0x0070
            r1.close()
        L_0x0070:
            throw r0
        L_0x0071:
            r0 = move-exception
            goto L_0x006b
        L_0x0073:
            r0 = move-exception
            r1 = r8
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getVisitorVarBuffer(android.database.sqlite.SQLiteDatabase):com.google.android.apps.analytics.CustomVariableBuffer");
    }

    /* access modifiers changed from: package-private */
    public int getVisits() {
        return this.visits;
    }

    public void loadExistingSession() {
        try {
            loadExistingSession(this.databaseHelper.getWritableDatabase());
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
     arg types: [java.lang.String, long]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00ca  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadExistingSession(android.database.sqlite.SQLiteDatabase r14) {
        /*
            r13 = this;
            r9 = 1
            r10 = 0
            r11 = 0
            r8 = 0
            java.lang.String r1 = "session"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r14
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00b5, all -> 0x00c6 }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x00d0 }
            if (r0 == 0) goto L_0x005a
            r0 = 0
            long r2 = r1.getLong(r0)     // Catch:{ SQLiteException -> 0x00d0 }
            r13.timestampFirst = r2     // Catch:{ SQLiteException -> 0x00d0 }
            r0 = 1
            long r2 = r1.getLong(r0)     // Catch:{ SQLiteException -> 0x00d0 }
            r13.timestampPrevious = r2     // Catch:{ SQLiteException -> 0x00d0 }
            r0 = 2
            long r2 = r1.getLong(r0)     // Catch:{ SQLiteException -> 0x00d0 }
            r13.timestampCurrent = r2     // Catch:{ SQLiteException -> 0x00d0 }
            r0 = 3
            int r0 = r1.getInt(r0)     // Catch:{ SQLiteException -> 0x00d0 }
            r13.visits = r0     // Catch:{ SQLiteException -> 0x00d0 }
            r0 = 4
            int r0 = r1.getInt(r0)     // Catch:{ SQLiteException -> 0x00d0 }
            r13.storeId = r0     // Catch:{ SQLiteException -> 0x00d0 }
            com.google.android.apps.analytics.Referrer r0 = r13.readCurrentReferrer(r14)     // Catch:{ SQLiteException -> 0x00d0 }
            long r2 = r13.timestampFirst     // Catch:{ SQLiteException -> 0x00d0 }
            int r2 = (r2 > r11 ? 1 : (r2 == r11 ? 0 : -1))
            if (r2 == 0) goto L_0x0058
            if (r0 == 0) goto L_0x004f
            long r2 = r0.getTimeStamp()     // Catch:{ SQLiteException -> 0x00d0 }
            int r0 = (r2 > r11 ? 1 : (r2 == r11 ? 0 : -1))
            if (r0 == 0) goto L_0x0058
        L_0x004f:
            r0 = r9
        L_0x0050:
            r13.sessionStarted = r0     // Catch:{ SQLiteException -> 0x00d0 }
        L_0x0052:
            if (r1 == 0) goto L_0x0057
            r1.close()
        L_0x0057:
            return
        L_0x0058:
            r0 = r10
            goto L_0x0050
        L_0x005a:
            r0 = 0
            r13.sessionStarted = r0     // Catch:{ SQLiteException -> 0x00d0 }
            r0 = 1
            r13.useStoredVisitorVars = r0     // Catch:{ SQLiteException -> 0x00d0 }
            java.security.SecureRandom r0 = new java.security.SecureRandom     // Catch:{ SQLiteException -> 0x00d0 }
            r0.<init>()     // Catch:{ SQLiteException -> 0x00d0 }
            int r0 = r0.nextInt()     // Catch:{ SQLiteException -> 0x00d0 }
            r2 = 2147483647(0x7fffffff, float:NaN)
            r0 = r0 & r2
            r13.storeId = r0     // Catch:{ SQLiteException -> 0x00d0 }
            r1.close()     // Catch:{ SQLiteException -> 0x00d0 }
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x00b5, all -> 0x00c6 }
            r0.<init>()     // Catch:{ SQLiteException -> 0x00b5, all -> 0x00c6 }
            java.lang.String r1 = "timestamp_first"
            r2 = 0
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ SQLiteException -> 0x00b5, all -> 0x00c6 }
            r0.put(r1, r2)     // Catch:{ SQLiteException -> 0x00b5, all -> 0x00c6 }
            java.lang.String r1 = "timestamp_previous"
            r2 = 0
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ SQLiteException -> 0x00b5, all -> 0x00c6 }
            r0.put(r1, r2)     // Catch:{ SQLiteException -> 0x00b5, all -> 0x00c6 }
            java.lang.String r1 = "timestamp_current"
            r2 = 0
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ SQLiteException -> 0x00b5, all -> 0x00c6 }
            r0.put(r1, r2)     // Catch:{ SQLiteException -> 0x00b5, all -> 0x00c6 }
            java.lang.String r1 = "visits"
            r2 = 0
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x00b5, all -> 0x00c6 }
            r0.put(r1, r2)     // Catch:{ SQLiteException -> 0x00b5, all -> 0x00c6 }
            java.lang.String r1 = "store_id"
            int r2 = r13.storeId     // Catch:{ SQLiteException -> 0x00b5, all -> 0x00c6 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x00b5, all -> 0x00c6 }
            r0.put(r1, r2)     // Catch:{ SQLiteException -> 0x00b5, all -> 0x00c6 }
            java.lang.String r1 = "session"
            r2 = 0
            r14.insert(r1, r2, r0)     // Catch:{ SQLiteException -> 0x00b5, all -> 0x00c6 }
            r1 = r8
            goto L_0x0052
        L_0x00b5:
            r0 = move-exception
            r1 = r8
        L_0x00b7:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00ce }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00ce }
            if (r1 == 0) goto L_0x0057
            r1.close()
            goto L_0x0057
        L_0x00c6:
            r0 = move-exception
            r1 = r8
        L_0x00c8:
            if (r1 == 0) goto L_0x00cd
            r1.close()
        L_0x00cd:
            throw r0
        L_0x00ce:
            r0 = move-exception
            goto L_0x00c8
        L_0x00d0:
            r0 = move-exception
            goto L_0x00b7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.loadExistingSession(android.database.sqlite.SQLiteDatabase):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0129  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Event[] peekEvents(int r21, android.database.sqlite.SQLiteDatabase r22, int r23) {
        /*
            r20 = this;
            java.util.ArrayList r19 = new java.util.ArrayList
            r19.<init>()
            r11 = 0
            java.lang.String r3 = "events"
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = "event_id"
            java.lang.String r10 = java.lang.Integer.toString(r21)     // Catch:{ SQLiteException -> 0x0160, all -> 0x0158 }
            r2 = r22
            android.database.Cursor r18 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ SQLiteException -> 0x0160, all -> 0x0158 }
        L_0x0019:
            boolean r2 = r18.moveToNext()     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            if (r2 == 0) goto L_0x0144
            r2 = 8
            r0 = r18
            java.lang.String r12 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r2 = 9
            r0 = r18
            java.lang.String r11 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r2 = 0
            java.lang.String r3 = "__##GOOGLEPAGEVIEW##__"
            boolean r3 = r3.equals(r12)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            if (r3 == 0) goto L_0x0163
            r13 = 0
        L_0x0039:
            com.google.android.apps.analytics.Event r2 = new com.google.android.apps.analytics.Event     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r3 = 0
            r0 = r18
            long r3 = r0.getLong(r3)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r5 = 2
            r0 = r18
            java.lang.String r5 = r0.getString(r5)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r6 = 3
            r0 = r18
            int r6 = r0.getInt(r6)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r7 = 4
            r0 = r18
            int r7 = r0.getInt(r7)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r8 = 5
            r0 = r18
            int r8 = r0.getInt(r8)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r9 = 6
            r0 = r18
            int r9 = r0.getInt(r9)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r10 = 7
            r0 = r18
            int r10 = r0.getInt(r10)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r14 = 10
            r0 = r18
            java.lang.String r14 = r0.getString(r14)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r15 = 11
            r0 = r18
            int r15 = r0.getInt(r15)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r16 = 12
            r0 = r18
            r1 = r16
            int r16 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r17 = 13
            r0 = r18
            r1 = r17
            int r17 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r2.<init>(r3, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r3 = 1
            r0 = r18
            int r3 = r0.getInt(r3)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r2.setUserId(r3)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            java.lang.String r3 = "event_id"
            r0 = r18
            int r3 = r0.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r0 = r18
            long r3 = r0.getLong(r3)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            java.lang.String r5 = "__##GOOGLETRANSACTION##__"
            java.lang.String r6 = r2.category     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            boolean r5 = r5.equals(r6)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            if (r5 == 0) goto L_0x00f6
            r0 = r20
            r1 = r22
            com.google.android.apps.analytics.Transaction r5 = r0.getTransaction(r3, r1)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            if (r5 != 0) goto L_0x00d7
            java.lang.String r6 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r7.<init>()     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            java.lang.String r8 = "missing expected transaction for event "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            java.lang.StringBuilder r3 = r7.append(r3)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            android.util.Log.w(r6, r3)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
        L_0x00d7:
            r2.setTransaction(r5)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
        L_0x00da:
            r0 = r19
            r0.add(r2)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            goto L_0x0019
        L_0x00e1:
            r2 = move-exception
            r3 = r18
        L_0x00e4:
            java.lang.String r4 = "GoogleAnalyticsTracker"
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x015c }
            android.util.Log.e(r4, r2)     // Catch:{ all -> 0x015c }
            r2 = 0
            com.google.android.apps.analytics.Event[] r2 = new com.google.android.apps.analytics.Event[r2]     // Catch:{ all -> 0x015c }
            if (r3 == 0) goto L_0x00f5
            r3.close()
        L_0x00f5:
            return r2
        L_0x00f6:
            java.lang.String r5 = "__##GOOGLEITEM##__"
            java.lang.String r6 = r2.category     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            boolean r5 = r5.equals(r6)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            if (r5 == 0) goto L_0x012d
            r0 = r20
            r1 = r22
            com.google.android.apps.analytics.Item r5 = r0.getItem(r3, r1)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            if (r5 != 0) goto L_0x0122
            java.lang.String r6 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r7.<init>()     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            java.lang.String r8 = "missing expected item for event "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            java.lang.StringBuilder r3 = r7.append(r3)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            android.util.Log.w(r6, r3)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
        L_0x0122:
            r2.setItem(r5)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            goto L_0x00da
        L_0x0126:
            r2 = move-exception
        L_0x0127:
            if (r18 == 0) goto L_0x012c
            r18.close()
        L_0x012c:
            throw r2
        L_0x012d:
            r5 = 1
            r0 = r23
            if (r0 <= r5) goto L_0x013e
            r0 = r20
            r1 = r22
            com.google.android.apps.analytics.CustomVariableBuffer r3 = r0.getCustomVariables(r3, r1)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
        L_0x013a:
            r2.setCustomVariableBuffer(r3)     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            goto L_0x00da
        L_0x013e:
            com.google.android.apps.analytics.CustomVariableBuffer r3 = new com.google.android.apps.analytics.CustomVariableBuffer     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x00e1, all -> 0x0126 }
            goto L_0x013a
        L_0x0144:
            if (r18 == 0) goto L_0x0149
            r18.close()
        L_0x0149:
            int r2 = r19.size()
            com.google.android.apps.analytics.Event[] r2 = new com.google.android.apps.analytics.Event[r2]
            r0 = r19
            java.lang.Object[] r2 = r0.toArray(r2)
            com.google.android.apps.analytics.Event[] r2 = (com.google.android.apps.analytics.Event[]) r2
            goto L_0x00f5
        L_0x0158:
            r2 = move-exception
            r18 = r11
            goto L_0x0127
        L_0x015c:
            r2 = move-exception
            r18 = r3
            goto L_0x0127
        L_0x0160:
            r2 = move-exception
            r3 = r11
            goto L_0x00e4
        L_0x0163:
            r13 = r11
            r11 = r2
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.peekEvents(int, android.database.sqlite.SQLiteDatabase, int):com.google.android.apps.analytics.Event[]");
    }

    public Hit[] peekHits() {
        return peekHits(1000);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x005f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Hit[] peekHits(int r12) {
        /*
            r11 = this;
            r9 = 0
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            com.google.android.apps.analytics.PersistentHitStore$DataBaseHelper r0 = r11.databaseHelper     // Catch:{ SQLiteException -> 0x0065, all -> 0x005b }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0065, all -> 0x005b }
            java.lang.String r1 = "hits"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.String r7 = "hit_id"
            java.lang.String r8 = java.lang.Integer.toString(r12)     // Catch:{ SQLiteException -> 0x0065, all -> 0x005b }
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x0065, all -> 0x005b }
        L_0x001d:
            boolean r0 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x0036 }
            if (r0 == 0) goto L_0x0049
            com.google.android.apps.analytics.Hit r0 = new com.google.android.apps.analytics.Hit     // Catch:{ SQLiteException -> 0x0036 }
            r2 = 1
            java.lang.String r2 = r1.getString(r2)     // Catch:{ SQLiteException -> 0x0036 }
            r3 = 0
            long r3 = r1.getLong(r3)     // Catch:{ SQLiteException -> 0x0036 }
            r0.<init>(r2, r3)     // Catch:{ SQLiteException -> 0x0036 }
            r10.add(r0)     // Catch:{ SQLiteException -> 0x0036 }
            goto L_0x001d
        L_0x0036:
            r0 = move-exception
        L_0x0037:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0063 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0063 }
            r0 = 0
            com.google.android.apps.analytics.Hit[] r0 = new com.google.android.apps.analytics.Hit[r0]     // Catch:{ all -> 0x0063 }
            if (r1 == 0) goto L_0x0048
            r1.close()
        L_0x0048:
            return r0
        L_0x0049:
            if (r1 == 0) goto L_0x004e
            r1.close()
        L_0x004e:
            int r0 = r10.size()
            com.google.android.apps.analytics.Hit[] r0 = new com.google.android.apps.analytics.Hit[r0]
            java.lang.Object[] r0 = r10.toArray(r0)
            com.google.android.apps.analytics.Hit[] r0 = (com.google.android.apps.analytics.Hit[]) r0
            goto L_0x0048
        L_0x005b:
            r0 = move-exception
            r1 = r9
        L_0x005d:
            if (r1 == 0) goto L_0x0062
            r1.close()
        L_0x0062:
            throw r0
        L_0x0063:
            r0 = move-exception
            goto L_0x005d
        L_0x0065:
            r0 = move-exception
            r1 = r9
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.peekHits(int):com.google.android.apps.analytics.Hit[]");
    }

    /* access modifiers changed from: package-private */
    public void putCustomVariables(Event event, SQLiteDatabase sQLiteDatabase) {
        CustomVariableBuffer customVariableBuffer;
        if (!"__##GOOGLEITEM##__".equals(event.category) && !"__##GOOGLETRANSACTION##__".equals(event.category)) {
            CustomVariableBuffer customVariableBuffer2 = event.getCustomVariableBuffer();
            if (this.useStoredVisitorVars) {
                if (customVariableBuffer2 == null) {
                    customVariableBuffer2 = new CustomVariableBuffer();
                    event.setCustomVariableBuffer(customVariableBuffer2);
                }
                for (int i = 1; i <= 50; i++) {
                    CustomVariable customVariableAt = this.visitorCVCache.getCustomVariableAt(i);
                    CustomVariable customVariableAt2 = customVariableBuffer2.getCustomVariableAt(i);
                    if (customVariableAt != null && customVariableAt2 == null) {
                        customVariableBuffer2.setCustomVariable(customVariableAt);
                    }
                }
                this.useStoredVisitorVars = false;
                customVariableBuffer = customVariableBuffer2;
            } else {
                customVariableBuffer = customVariableBuffer2;
            }
            if (customVariableBuffer != null) {
                for (int i2 = 1; i2 <= 50; i2++) {
                    if (!customVariableBuffer.isIndexAvailable(i2)) {
                        CustomVariable customVariableAt3 = customVariableBuffer.getCustomVariableAt(i2);
                        sQLiteDatabase.delete(CUSTOM_VAR_VISITOR_CACHE_NAME, "cv_index = ?", new String[]{Integer.toString(customVariableAt3.getIndex())});
                        if (customVariableAt3.getScope() == 1) {
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(CUSTOMVAR_INDEX, Integer.valueOf(customVariableAt3.getIndex()));
                            contentValues.put(CUSTOMVAR_NAME, customVariableAt3.getName());
                            contentValues.put(CUSTOMVAR_VALUE, customVariableAt3.getValue());
                            sQLiteDatabase.insert(CUSTOM_VAR_VISITOR_CACHE_NAME, null, contentValues);
                            this.visitorCVCache.setCustomVariable(customVariableAt3);
                        } else {
                            this.visitorCVCache.clearCustomVariableAt(customVariableAt3.getIndex());
                        }
                    }
                }
            }
        }
    }

    public void putEvent(Event event) {
        if (this.numStoredHits >= 1000) {
            Log.w(GoogleAnalyticsTracker.LOG_TAG, "Store full. Not storing last event.");
            return;
        }
        if (this.sampleRate != 100) {
            if ((event.getUserId() == -1 ? this.storeId : event.getUserId()) % 10000 >= this.sampleRate * 100) {
                if (GoogleAnalyticsTracker.getInstance().getDebug()) {
                    Log.v(GoogleAnalyticsTracker.LOG_TAG, "User has been sampled out. Aborting hit.");
                    return;
                }
                return;
            }
        }
        synchronized (this) {
            try {
                SQLiteDatabase writableDatabase = this.databaseHelper.getWritableDatabase();
                try {
                    writableDatabase.beginTransaction();
                    if (!this.sessionStarted) {
                        storeUpdatedSession(writableDatabase);
                    }
                    putEvent(event, writableDatabase, true);
                    writableDatabase.setTransactionSuccessful();
                    if (writableDatabase.inTransaction()) {
                        endTransaction(writableDatabase);
                    }
                } catch (SQLiteException e) {
                    Log.e(GoogleAnalyticsTracker.LOG_TAG, "putEventOuter:" + e.toString());
                    if (writableDatabase.inTransaction()) {
                        endTransaction(writableDatabase);
                    }
                } catch (Throwable th) {
                    if (writableDatabase.inTransaction()) {
                        endTransaction(writableDatabase);
                    }
                    throw th;
                }
            } catch (SQLiteException e2) {
                Log.e(GoogleAnalyticsTracker.LOG_TAG, "Can't get db: " + e2.toString());
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0073  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Referrer readCurrentReferrer(android.database.sqlite.SQLiteDatabase r10) {
        /*
            r9 = this;
            r8 = 0
            java.lang.String r1 = "referrer"
            r0 = 4
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x005d, all -> 0x006f }
            r0 = 0
            java.lang.String r3 = "referrer"
            r2[r0] = r3     // Catch:{ SQLiteException -> 0x005d, all -> 0x006f }
            r0 = 1
            java.lang.String r3 = "timestamp_referrer"
            r2[r0] = r3     // Catch:{ SQLiteException -> 0x005d, all -> 0x006f }
            r0 = 2
            java.lang.String r3 = "referrer_visit"
            r2[r0] = r3     // Catch:{ SQLiteException -> 0x005d, all -> 0x006f }
            r0 = 3
            java.lang.String r3 = "referrer_index"
            r2[r0] = r3     // Catch:{ SQLiteException -> 0x005d, all -> 0x006f }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r10
            android.database.Cursor r6 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x005d, all -> 0x006f }
            boolean r0 = r6.moveToFirst()     // Catch:{ SQLiteException -> 0x007c, all -> 0x0077 }
            if (r0 == 0) goto L_0x007f
            java.lang.String r0 = "timestamp_referrer"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ SQLiteException -> 0x007c, all -> 0x0077 }
            long r2 = r6.getLong(r0)     // Catch:{ SQLiteException -> 0x007c, all -> 0x0077 }
            java.lang.String r0 = "referrer_visit"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ SQLiteException -> 0x007c, all -> 0x0077 }
            int r4 = r6.getInt(r0)     // Catch:{ SQLiteException -> 0x007c, all -> 0x0077 }
            java.lang.String r0 = "referrer_index"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ SQLiteException -> 0x007c, all -> 0x0077 }
            int r5 = r6.getInt(r0)     // Catch:{ SQLiteException -> 0x007c, all -> 0x0077 }
            java.lang.String r0 = "referrer"
            int r0 = r6.getColumnIndex(r0)     // Catch:{ SQLiteException -> 0x007c, all -> 0x0077 }
            java.lang.String r1 = r6.getString(r0)     // Catch:{ SQLiteException -> 0x007c, all -> 0x0077 }
            com.google.android.apps.analytics.Referrer r0 = new com.google.android.apps.analytics.Referrer     // Catch:{ SQLiteException -> 0x007c, all -> 0x0077 }
            r0.<init>(r1, r2, r4, r5)     // Catch:{ SQLiteException -> 0x007c, all -> 0x0077 }
        L_0x0057:
            if (r6 == 0) goto L_0x005c
            r6.close()
        L_0x005c:
            return r0
        L_0x005d:
            r0 = move-exception
            r1 = r8
        L_0x005f:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0079 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0079 }
            if (r1 == 0) goto L_0x006d
            r1.close()
        L_0x006d:
            r0 = r8
            goto L_0x005c
        L_0x006f:
            r0 = move-exception
            r6 = r8
        L_0x0071:
            if (r6 == 0) goto L_0x0076
            r6.close()
        L_0x0076:
            throw r0
        L_0x0077:
            r0 = move-exception
            goto L_0x0071
        L_0x0079:
            r0 = move-exception
            r6 = r1
            goto L_0x0071
        L_0x007c:
            r0 = move-exception
            r1 = r6
            goto L_0x005f
        L_0x007f:
            r0 = r8
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.readCurrentReferrer(android.database.sqlite.SQLiteDatabase):com.google.android.apps.analytics.Referrer");
    }

    public void setAnonymizeIp(boolean z) {
        this.anonymizeIp = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
     arg types: [java.lang.String, long]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean setReferrer(String str) {
        long j;
        String formatReferrer = formatReferrer(str);
        if (formatReferrer == null) {
            return false;
        }
        try {
            SQLiteDatabase writableDatabase = this.databaseHelper.getWritableDatabase();
            Referrer readCurrentReferrer = readCurrentReferrer(writableDatabase);
            ContentValues contentValues = new ContentValues();
            contentValues.put("referrer", formatReferrer);
            contentValues.put(TIMESTAMP_REFERRER, (Long) 0L);
            contentValues.put(REFERRER_VISIT, (Integer) 0);
            if (readCurrentReferrer != null) {
                j = (long) readCurrentReferrer.getIndex();
                if (readCurrentReferrer.getTimeStamp() > 0) {
                    j++;
                }
            } else {
                j = 1;
            }
            contentValues.put(REFERRER_INDEX, Long.valueOf(j));
            if (!setReferrerDatabase(writableDatabase, contentValues)) {
                return false;
            }
            startNewVisit();
            return true;
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
            return false;
        }
    }

    public void setSampleRate(int i) {
        this.sampleRate = i;
    }

    public synchronized void startNewVisit() {
        this.sessionStarted = false;
        this.useStoredVisitorVars = true;
        this.numStoredHits = getNumStoredHitsFromDb();
    }

    /* access modifiers changed from: package-private */
    public void storeUpdatedSession(SQLiteDatabase sQLiteDatabase) {
        SQLiteDatabase writableDatabase = this.databaseHelper.getWritableDatabase();
        writableDatabase.delete("session", null, null);
        if (this.timestampFirst == 0) {
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            this.timestampFirst = currentTimeMillis;
            this.timestampPrevious = currentTimeMillis;
            this.timestampCurrent = currentTimeMillis;
            this.visits = 1;
        } else {
            this.timestampPrevious = this.timestampCurrent;
            this.timestampCurrent = System.currentTimeMillis() / 1000;
            if (this.timestampCurrent == this.timestampPrevious) {
                this.timestampCurrent++;
            }
            this.visits++;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(TIMESTAMP_FIRST, Long.valueOf(this.timestampFirst));
        contentValues.put(TIMESTAMP_PREVIOUS, Long.valueOf(this.timestampPrevious));
        contentValues.put(TIMESTAMP_CURRENT, Long.valueOf(this.timestampCurrent));
        contentValues.put(VISITS, Integer.valueOf(this.visits));
        contentValues.put(STORE_ID, Integer.valueOf(this.storeId));
        writableDatabase.insert("session", null, contentValues);
        this.sessionStarted = true;
    }

    /* access modifiers changed from: package-private */
    public void writeEventToDatabase(Event event, Referrer referrer, SQLiteDatabase sQLiteDatabase, boolean z) throws SQLiteException {
        ContentValues contentValues = new ContentValues();
        contentValues.put(HIT_STRING, HitBuilder.constructHitRequestPath(event, referrer));
        contentValues.put(HIT_TIMESTAMP, Long.valueOf(z ? System.currentTimeMillis() : 0));
        sQLiteDatabase.insert("hits", null, contentValues);
        this.numStoredHits++;
    }
}
