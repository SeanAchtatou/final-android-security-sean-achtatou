package com.google.android.apps.analytics;

import android.content.pm.PackageManager;

class DeviceOptOutImpl implements OptOut {
    static final String OPTOUT_ID = "com.google.analytics.optout";
    static final int OPTOUT_TIME_CHECK_IN_SECONDS = 300;
    private final long checkIntervalInMillis = 300000;
    private long lastTimeChecked;
    private final String optOutApp = OPTOUT_ID;
    private boolean optedOut = false;
    private final PackageManager pm;

    DeviceOptOutImpl(PackageManager packageManager) {
        this.pm = packageManager;
    }

    public boolean optedOut() {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis > this.lastTimeChecked + this.checkIntervalInMillis) {
            try {
                this.pm.getPackageInfo(this.optOutApp, 0);
                this.optedOut = true;
            } catch (PackageManager.NameNotFoundException e) {
                this.optedOut = false;
            }
            this.lastTimeChecked = currentTimeMillis;
        }
        return this.optedOut;
    }

    /* access modifiers changed from: package-private */
    public void resetLastTimeChecked() {
        this.lastTimeChecked = 0;
    }
}
