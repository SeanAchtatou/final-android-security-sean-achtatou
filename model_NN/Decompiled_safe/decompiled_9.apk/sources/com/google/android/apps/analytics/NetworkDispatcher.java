package com.google.android.apps.analytics;

import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.google.android.apps.analytics.Dispatcher;
import com.google.android.apps.analytics.PipelinedRequester;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Locale;
import org.apache.http.Header;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.ParseException;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;

class NetworkDispatcher implements Dispatcher {
    private static final String GOOGLE_ANALYTICS_HOST_NAME = "www.google-analytics.com";
    private static final int GOOGLE_ANALYTICS_HOST_PORT = 80;
    private static final int MAX_EVENTS_PER_PIPELINE = 30;
    private static final int MAX_GET_LENGTH = 2036;
    private static final int MAX_POST_LENGTH = 8192;
    private static final int MAX_SEQUENTIAL_REQUESTS = 5;
    private static final long MIN_RETRY_INTERVAL = 2;
    private static final String USER_AGENT_TEMPLATE = "%s/%s (Linux; U; Android %s; %s-%s; %s Build/%s)";
    private DispatcherThread dispatcherThread;
    private boolean dryRun;
    /* access modifiers changed from: private */
    public final HttpHost googleAnalyticsHost;
    private final String userAgent;

    private static class DispatcherThread extends HandlerThread {
        /* access modifiers changed from: private */
        public final Dispatcher.Callbacks callbacks;
        /* access modifiers changed from: private */
        public AsyncDispatchTask currentTask;
        volatile Handler handlerExecuteOnDispatcherThread;
        /* access modifiers changed from: private */
        public int lastStatusCode;
        /* access modifiers changed from: private */
        public int maxEventsPerRequest;
        /* access modifiers changed from: private */
        public final NetworkDispatcher parent;
        /* access modifiers changed from: private */
        public final PipelinedRequester pipelinedRequester;
        /* access modifiers changed from: private */
        public final RequesterCallbacks requesterCallBacks;
        /* access modifiers changed from: private */
        public long retryInterval;
        /* access modifiers changed from: private */
        public final String userAgent;

        private class AsyncDispatchTask implements Runnable {
            private final LinkedList<Hit> hits = new LinkedList<>();

            public AsyncDispatchTask(Hit[] hitArr) {
                Collections.addAll(this.hits, hitArr);
            }

            private void dispatchSomePendingHits(boolean z) throws IOException, ParseException, HttpException {
                String str;
                String str2;
                HttpEntityEnclosingRequest httpEntityEnclosingRequest;
                if (GoogleAnalyticsTracker.getInstance().getDebug() && z) {
                    Log.v(GoogleAnalyticsTracker.LOG_TAG, "dispatching hits in dry run mode");
                }
                Hit[] hitArr = (Hit[]) this.hits.toArray(new Hit[0]);
                int i = 0;
                while (i < hitArr.length && i < DispatcherThread.this.maxEventsPerRequest) {
                    String addQueueTimeParameter = Utils.addQueueTimeParameter(hitArr[i].hitString, System.currentTimeMillis());
                    int indexOf = addQueueTimeParameter.indexOf(63);
                    if (indexOf < 0) {
                        str2 = "";
                        str = addQueueTimeParameter;
                    } else {
                        String substring = indexOf > 0 ? addQueueTimeParameter.substring(0, indexOf) : "";
                        if (indexOf < addQueueTimeParameter.length() - 2) {
                            String substring2 = addQueueTimeParameter.substring(indexOf + 1);
                            str = substring;
                            str2 = substring2;
                        } else {
                            str = substring;
                            str2 = "";
                        }
                    }
                    if (str2.length() < NetworkDispatcher.MAX_GET_LENGTH) {
                        httpEntityEnclosingRequest = new BasicHttpEntityEnclosingRequest("GET", addQueueTimeParameter);
                    } else {
                        HttpEntityEnclosingRequest basicHttpEntityEnclosingRequest = new BasicHttpEntityEnclosingRequest("POST", "/p" + str);
                        basicHttpEntityEnclosingRequest.addHeader("Content-Length", Integer.toString(str2.length()));
                        basicHttpEntityEnclosingRequest.addHeader("Content-Type", "text/plain");
                        basicHttpEntityEnclosingRequest.setEntity(new StringEntity(str2));
                        httpEntityEnclosingRequest = basicHttpEntityEnclosingRequest;
                    }
                    String hostName = DispatcherThread.this.parent.googleAnalyticsHost.getHostName();
                    if (DispatcherThread.this.parent.googleAnalyticsHost.getPort() != NetworkDispatcher.GOOGLE_ANALYTICS_HOST_PORT) {
                        hostName = hostName + ":" + DispatcherThread.this.parent.googleAnalyticsHost.getPort();
                    }
                    httpEntityEnclosingRequest.addHeader("Host", hostName);
                    httpEntityEnclosingRequest.addHeader("User-Agent", DispatcherThread.this.userAgent);
                    if (GoogleAnalyticsTracker.getInstance().getDebug()) {
                        StringBuffer stringBuffer = new StringBuffer();
                        for (Header obj : httpEntityEnclosingRequest.getAllHeaders()) {
                            stringBuffer.append(obj.toString()).append("\n");
                        }
                        stringBuffer.append(httpEntityEnclosingRequest.getRequestLine().toString()).append("\n");
                        Log.i(GoogleAnalyticsTracker.LOG_TAG, stringBuffer.toString());
                    }
                    if (str2.length() > 8192) {
                        Log.w(GoogleAnalyticsTracker.LOG_TAG, "Hit too long (> 8192 bytes)--not sent");
                        DispatcherThread.this.requesterCallBacks.requestSent();
                    } else if (z) {
                        DispatcherThread.this.requesterCallBacks.requestSent();
                    } else {
                        DispatcherThread.this.pipelinedRequester.addRequest(httpEntityEnclosingRequest);
                    }
                    i++;
                }
                if (!z) {
                    DispatcherThread.this.pipelinedRequester.sendRequests();
                }
            }

            public Hit removeNextHit() {
                return this.hits.poll();
            }

            public void run() {
                AsyncDispatchTask unused = DispatcherThread.this.currentTask = this;
                int i = 0;
                while (i < 5 && this.hits.size() > 0) {
                    long j = 0;
                    try {
                        if (DispatcherThread.this.lastStatusCode == 500 || DispatcherThread.this.lastStatusCode == 503) {
                            j = (long) (Math.random() * ((double) DispatcherThread.this.retryInterval));
                            if (DispatcherThread.this.retryInterval < 256) {
                                DispatcherThread.access$630(DispatcherThread.this, NetworkDispatcher.MIN_RETRY_INTERVAL);
                            }
                        } else {
                            long unused2 = DispatcherThread.this.retryInterval = NetworkDispatcher.MIN_RETRY_INTERVAL;
                        }
                        Thread.sleep(j * 1000);
                        dispatchSomePendingHits(DispatcherThread.this.parent.isDryRun());
                        i++;
                    } catch (InterruptedException e) {
                        Log.w(GoogleAnalyticsTracker.LOG_TAG, "Couldn't sleep.", e);
                    } catch (IOException e2) {
                        Log.w(GoogleAnalyticsTracker.LOG_TAG, "Problem with socket or streams.", e2);
                    } catch (HttpException e3) {
                        Log.w(GoogleAnalyticsTracker.LOG_TAG, "Problem with http streams.", e3);
                    }
                }
                DispatcherThread.this.pipelinedRequester.finishedCurrentRequests();
                DispatcherThread.this.callbacks.dispatchFinished();
                AsyncDispatchTask unused3 = DispatcherThread.this.currentTask = null;
            }
        }

        private class RequesterCallbacks implements PipelinedRequester.Callbacks {
            private RequesterCallbacks() {
            }

            public void pipelineModeChanged(boolean z) {
                if (z) {
                    int unused = DispatcherThread.this.maxEventsPerRequest = NetworkDispatcher.MAX_EVENTS_PER_PIPELINE;
                } else {
                    int unused2 = DispatcherThread.this.maxEventsPerRequest = 1;
                }
            }

            public void requestSent() {
                Hit removeNextHit;
                if (DispatcherThread.this.currentTask != null && (removeNextHit = DispatcherThread.this.currentTask.removeNextHit()) != null) {
                    DispatcherThread.this.callbacks.hitDispatched(removeNextHit.hitId);
                }
            }

            public void serverError(int i) {
                int unused = DispatcherThread.this.lastStatusCode = i;
            }
        }

        private DispatcherThread(Dispatcher.Callbacks callbacks2, PipelinedRequester pipelinedRequester2, String str, NetworkDispatcher networkDispatcher) {
            super("DispatcherThread");
            this.maxEventsPerRequest = NetworkDispatcher.MAX_EVENTS_PER_PIPELINE;
            this.currentTask = null;
            this.callbacks = callbacks2;
            this.userAgent = str;
            this.pipelinedRequester = pipelinedRequester2;
            this.requesterCallBacks = new RequesterCallbacks();
            this.pipelinedRequester.installCallbacks(this.requesterCallBacks);
            this.parent = networkDispatcher;
        }

        private DispatcherThread(Dispatcher.Callbacks callbacks2, String str, NetworkDispatcher networkDispatcher) {
            this(callbacks2, new PipelinedRequester(networkDispatcher.googleAnalyticsHost), str, networkDispatcher);
        }

        static /* synthetic */ long access$630(DispatcherThread dispatcherThread, long j) {
            long j2 = dispatcherThread.retryInterval * j;
            dispatcherThread.retryInterval = j2;
            return j2;
        }

        public void dispatchHits(Hit[] hitArr) {
            if (this.handlerExecuteOnDispatcherThread != null) {
                this.handlerExecuteOnDispatcherThread.post(new AsyncDispatchTask(hitArr));
            }
        }

        /* access modifiers changed from: protected */
        public void onLooperPrepared() {
            this.handlerExecuteOnDispatcherThread = new Handler();
        }
    }

    public NetworkDispatcher() {
        this(GoogleAnalyticsTracker.PRODUCT, GoogleAnalyticsTracker.VERSION);
    }

    public NetworkDispatcher(String str, String str2) {
        this(str, str2, GOOGLE_ANALYTICS_HOST_NAME, GOOGLE_ANALYTICS_HOST_PORT);
    }

    NetworkDispatcher(String str, String str2, String str3, int i) {
        this.dryRun = false;
        this.googleAnalyticsHost = new HttpHost(str3, i);
        Locale locale = Locale.getDefault();
        Object[] objArr = new Object[7];
        objArr[0] = str;
        objArr[1] = str2;
        objArr[2] = Build.VERSION.RELEASE;
        objArr[3] = locale.getLanguage() != null ? locale.getLanguage().toLowerCase() : "en";
        objArr[4] = locale.getCountry() != null ? locale.getCountry().toLowerCase() : "";
        objArr[5] = Build.MODEL;
        objArr[6] = Build.ID;
        this.userAgent = String.format(USER_AGENT_TEMPLATE, objArr);
    }

    public void dispatchHits(Hit[] hitArr) {
        if (this.dispatcherThread != null) {
            this.dispatcherThread.dispatchHits(hitArr);
        }
    }

    /* access modifiers changed from: package-private */
    public String getUserAgent() {
        return this.userAgent;
    }

    public void init(Dispatcher.Callbacks callbacks) {
        stop();
        this.dispatcherThread = new DispatcherThread(callbacks, this.userAgent, this);
        this.dispatcherThread.start();
    }

    public void init(Dispatcher.Callbacks callbacks, PipelinedRequester pipelinedRequester, HitStore hitStore) {
        stop();
        this.dispatcherThread = new DispatcherThread(callbacks, pipelinedRequester, this.userAgent, this);
        this.dispatcherThread.start();
    }

    public boolean isDryRun() {
        return this.dryRun;
    }

    public void setDryRun(boolean z) {
        this.dryRun = z;
    }

    public void stop() {
        if (this.dispatcherThread != null && this.dispatcherThread.getLooper() != null) {
            this.dispatcherThread.getLooper().quit();
            this.dispatcherThread = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void waitForThreadLooper() {
        this.dispatcherThread.getLooper();
        while (this.dispatcherThread.handlerExecuteOnDispatcherThread == null) {
            Thread.yield();
        }
    }
}
