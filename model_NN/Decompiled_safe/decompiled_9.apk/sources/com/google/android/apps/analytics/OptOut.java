package com.google.android.apps.analytics;

interface OptOut {
    boolean optedOut();
}
