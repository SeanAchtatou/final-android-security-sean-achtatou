package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.internal.bs;
import com.google.android.gms.internal.bu;
import com.google.android.gms.internal.k;

public final class LeaderboardScoreBuffer extends DataBuffer<LeaderboardScore> {
    private final bs dp;

    public LeaderboardScoreBuffer(k dataHolder) {
        super(dataHolder);
        this.dp = new bs(dataHolder.h());
    }

    public bs aq() {
        return this.dp;
    }

    public LeaderboardScore get(int position) {
        return new bu(this.O, position);
    }
}
