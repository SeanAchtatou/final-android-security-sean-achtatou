package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.internal.an;

public class ai implements ae {
    public static final aj CREATOR = new aj();
    private final int T;
    private final ak bo;

    ai(int i, ak akVar) {
        this.T = i;
        this.bo = akVar;
    }

    private ai(ak akVar) {
        this.T = 1;
        this.bo = akVar;
    }

    public static ai a(an.b<?, ?> bVar) {
        if (bVar instanceof ak) {
            return new ai((ak) bVar);
        }
        throw new IllegalArgumentException("Unsupported safe parcelable field converter class.");
    }

    /* access modifiers changed from: package-private */
    public ak B() {
        return this.bo;
    }

    public an.b<?, ?> C() {
        if (this.bo != null) {
            return this.bo;
        }
        throw new IllegalStateException("There was no converter wrapped in this ConverterWrapper.");
    }

    public int describeContents() {
        aj ajVar = CREATOR;
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int u() {
        return this.T;
    }

    public void writeToParcel(Parcel out, int flags) {
        aj ajVar = CREATOR;
        aj.a(this, out, flags);
    }
}
