package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.view.View;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.v;

public final class z {
    private static Context aZ;
    private static v ba;

    public static class a extends Exception {
        public a(String str) {
            super(str);
        }
    }

    public static View d(Context context, int i, int i2) throws a {
        try {
            return (View) bd.a(f(context).a(bd.f(context), i, i2));
        } catch (Exception e) {
            throw new a("Could not get button with size " + i + " and color " + i2);
        }
    }

    private static v f(Context context) throws a {
        x.d(context);
        if (ba == null) {
            if (aZ == null) {
                aZ = GooglePlayServicesUtil.getRemoteContext(context);
                if (aZ == null) {
                    throw new a("Could not get remote context.");
                }
            }
            try {
                ba = v.a.i((IBinder) aZ.getClassLoader().loadClass("com.google.android.gms.common.ui.SignInButtonCreatorImpl").newInstance());
            } catch (ClassNotFoundException e) {
                throw new a("Could not load creator class.");
            } catch (InstantiationException e2) {
                throw new a("Could not instantiate creator.");
            } catch (IllegalAccessException e3) {
                throw new a("Could not access creator.");
            }
        }
        return ba;
    }
}
