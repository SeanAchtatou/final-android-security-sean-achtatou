package com.google.android.gms.internal;

import android.os.Parcel;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.internal.an;
import com.google.android.gms.plus.model.moments.ItemScope;
import com.google.android.gms.plus.model.moments.Moment;
import com.widgetizeme.UnsentStats;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public final class ef extends an implements ae, Moment {
    public static final eg CREATOR = new eg();
    private static final HashMap<String, an.a<?, ?>> hR = new HashMap<>();
    private final int T;
    private final Set<Integer> hS;
    private String iH;
    private String iN;
    private ed iQ;
    private ed iR;
    private String iw;

    static {
        hR.put(UnsentStats.KEY_ID, an.a.f(UnsentStats.KEY_ID, 2));
        hR.put("result", an.a.a("result", 4, ed.class));
        hR.put("startDate", an.a.f("startDate", 5));
        hR.put("target", an.a.a("target", 6, ed.class));
        hR.put(ServerProtocol.DIALOG_PARAM_TYPE, an.a.f(ServerProtocol.DIALOG_PARAM_TYPE, 7));
    }

    public ef() {
        this.T = 1;
        this.hS = new HashSet();
    }

    ef(Set<Integer> set, int i, String str, ed edVar, String str2, ed edVar2, String str3) {
        this.hS = set;
        this.T = i;
        this.iw = str;
        this.iQ = edVar;
        this.iH = str2;
        this.iR = edVar2;
        this.iN = str3;
    }

    public ef(Set<Integer> set, String str, ed edVar, String str2, ed edVar2, String str3) {
        this.hS = set;
        this.T = 1;
        this.iw = str;
        this.iQ = edVar;
        this.iH = str2;
        this.iR = edVar2;
        this.iN = str3;
    }

    public HashMap<String, an.a<?, ?>> G() {
        return hR;
    }

    /* access modifiers changed from: protected */
    public boolean a(an.a aVar) {
        return this.hS.contains(Integer.valueOf(aVar.N()));
    }

    /* access modifiers changed from: protected */
    public Object b(an.a aVar) {
        switch (aVar.N()) {
            case 2:
                return this.iw;
            case 3:
            default:
                throw new IllegalStateException("Unknown safe parcelable id=" + aVar.N());
            case 4:
                return this.iQ;
            case 5:
                return this.iH;
            case 6:
                return this.iR;
            case 7:
                return this.iN;
        }
    }

    /* access modifiers changed from: package-private */
    public ed bP() {
        return this.iQ;
    }

    /* access modifiers changed from: package-private */
    public ed bQ() {
        return this.iR;
    }

    /* renamed from: bR */
    public ef freeze() {
        return this;
    }

    /* access modifiers changed from: package-private */
    public Set<Integer> by() {
        return this.hS;
    }

    public int describeContents() {
        eg egVar = CREATOR;
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ef)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        ef efVar = (ef) obj;
        for (an.a next : hR.values()) {
            if (a(next)) {
                if (!efVar.a(next)) {
                    return false;
                }
                if (!b(next).equals(efVar.b(next))) {
                    return false;
                }
            } else if (efVar.a(next)) {
                return false;
            }
        }
        return true;
    }

    public String getId() {
        return this.iw;
    }

    public ItemScope getResult() {
        return this.iQ;
    }

    public String getStartDate() {
        return this.iH;
    }

    public ItemScope getTarget() {
        return this.iR;
    }

    public String getType() {
        return this.iN;
    }

    public boolean hasId() {
        return this.hS.contains(2);
    }

    public boolean hasResult() {
        return this.hS.contains(4);
    }

    public boolean hasStartDate() {
        return this.hS.contains(5);
    }

    public boolean hasTarget() {
        return this.hS.contains(6);
    }

    public boolean hasType() {
        return this.hS.contains(7);
    }

    public int hashCode() {
        int i = 0;
        Iterator<an.a<?, ?>> it = hR.values().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            an.a next = it.next();
            if (a(next)) {
                i = b(next).hashCode() + i2 + next.N();
            } else {
                i = i2;
            }
        }
    }

    /* access modifiers changed from: protected */
    public Object j(String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean k(String str) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public int u() {
        return this.T;
    }

    public void writeToParcel(Parcel out, int flags) {
        eg egVar = CREATOR;
        eg.a(this, out, flags);
    }
}
