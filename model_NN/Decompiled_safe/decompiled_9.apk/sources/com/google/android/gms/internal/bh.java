package com.google.android.gms.internal;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.achievement.Achievement;
import com.google.android.gms.internal.w;
import com.google.android.gms.plus.PlusShare;
import com.widgetizeme.UnsentStats;

public final class bh extends j implements Achievement {
    public bh(k kVar, int i) {
        super(kVar, i);
    }

    public String getAchievementId() {
        return getString("external_achievement_id");
    }

    public int getCurrentSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        n.a(z);
        return getInteger("current_steps");
    }

    public String getDescription() {
        return getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION);
    }

    public void getDescription(CharArrayBuffer dataOut) {
        a(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, dataOut);
    }

    public String getFormattedCurrentSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        n.a(z);
        return getString("formatted_current_steps");
    }

    public void getFormattedCurrentSteps(CharArrayBuffer dataOut) {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        n.a(z);
        a("formatted_current_steps", dataOut);
    }

    public String getFormattedTotalSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        n.a(z);
        return getString("formatted_total_steps");
    }

    public void getFormattedTotalSteps(CharArrayBuffer dataOut) {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        n.a(z);
        a("formatted_total_steps", dataOut);
    }

    public long getLastUpdatedTimestamp() {
        return getLong("last_updated_timestamp");
    }

    public String getName() {
        return getString("name");
    }

    public void getName(CharArrayBuffer dataOut) {
        a("name", dataOut);
    }

    public Player getPlayer() {
        return new bg(this.O, this.R);
    }

    public Uri getRevealedImageUri() {
        return c("revealed_icon_image_uri");
    }

    public int getState() {
        return getInteger("state");
    }

    public int getTotalSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        n.a(z);
        return getInteger("total_steps");
    }

    public int getType() {
        return getInteger(ServerProtocol.DIALOG_PARAM_TYPE);
    }

    public Uri getUnlockedImageUri() {
        return c("unlocked_icon_image_uri");
    }

    public String toString() {
        w.a a = w.c(this).a(UnsentStats.KEY_ID, getAchievementId()).a("name", getName()).a("state", Integer.valueOf(getState())).a(ServerProtocol.DIALOG_PARAM_TYPE, Integer.valueOf(getType()));
        if (getType() == 1) {
            a.a("steps", getCurrentSteps() + "/" + getTotalSteps());
        }
        return a.toString();
    }
}
