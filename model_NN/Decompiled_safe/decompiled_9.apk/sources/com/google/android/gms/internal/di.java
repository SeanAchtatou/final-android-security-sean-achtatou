package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.VisibleRegion;

public class di {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLngBounds, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public static void a(VisibleRegion visibleRegion, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, visibleRegion.u());
        ad.a(parcel, 2, (Parcelable) visibleRegion.nearLeft, i, false);
        ad.a(parcel, 3, (Parcelable) visibleRegion.nearRight, i, false);
        ad.a(parcel, 4, (Parcelable) visibleRegion.farLeft, i, false);
        ad.a(parcel, 5, (Parcelable) visibleRegion.farRight, i, false);
        ad.a(parcel, 6, (Parcelable) visibleRegion.latLngBounds, i, false);
        ad.C(parcel, d);
    }
}
