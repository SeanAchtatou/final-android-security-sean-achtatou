package com.google.android.gms.maps;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.internal.cl;
import com.google.android.gms.internal.cw;
import com.google.android.gms.internal.x;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.RuntimeRemoteException;

public final class MapsInitializer {
    private MapsInitializer() {
    }

    public static void initialize(Context context) throws GooglePlayServicesNotAvailableException {
        x.d(context);
        cl g = cw.g(context);
        try {
            CameraUpdateFactory.a(g.aR());
            BitmapDescriptorFactory.a(g.aS());
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
