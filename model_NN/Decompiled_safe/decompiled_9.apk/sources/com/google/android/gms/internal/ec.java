package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import java.util.ArrayList;

public class ec implements Parcelable.Creator<eb> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(eb ebVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.a(parcel, 1, ebVar.getDescription(), false);
        ad.c(parcel, 1000, ebVar.u());
        ad.b(parcel, 2, ebVar.bv(), false);
        ad.b(parcel, 3, ebVar.bw(), false);
        ad.a(parcel, 4, ebVar.bx());
        ad.C(parcel, d);
    }

    /* renamed from: O */
    public eb[] newArray(int i) {
        return new eb[i];
    }

    /* renamed from: u */
    public eb createFromParcel(Parcel parcel) {
        boolean z = false;
        ArrayList arrayList = null;
        int c = ac.c(parcel);
        ArrayList arrayList2 = null;
        String str = null;
        int i = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    str = ac.l(parcel, b);
                    break;
                case 2:
                    arrayList2 = ac.c(parcel, b, ag.CREATOR);
                    break;
                case 3:
                    arrayList = ac.c(parcel, b, ag.CREATOR);
                    break;
                case 4:
                    z = ac.c(parcel, b);
                    break;
                case 1000:
                    i = ac.f(parcel, b);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new eb(i, str, arrayList2, arrayList, z);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }
}
