package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ad;

public class CircleOptionsCreator implements Parcelable.Creator<CircleOptions> {
    public static final int CONTENT_DESCRIPTION = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(CircleOptions circleOptions, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, circleOptions.u());
        ad.a(parcel, 2, (Parcelable) circleOptions.getCenter(), i, false);
        ad.a(parcel, 3, circleOptions.getRadius());
        ad.a(parcel, 4, circleOptions.getStrokeWidth());
        ad.c(parcel, 5, circleOptions.getStrokeColor());
        ad.c(parcel, 6, circleOptions.getFillColor());
        ad.a(parcel, 7, circleOptions.getZIndex());
        ad.a(parcel, 8, circleOptions.isVisible());
        ad.C(parcel, d);
    }

    public CircleOptions createFromParcel(Parcel parcel) {
        float f = BitmapDescriptorFactory.HUE_RED;
        boolean z = false;
        int c = ac.c(parcel);
        LatLng latLng = null;
        double d = 0.0d;
        int i = 0;
        int i2 = 0;
        float f2 = 0.0f;
        int i3 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i3 = ac.f(parcel, b);
                    break;
                case 2:
                    latLng = (LatLng) ac.a(parcel, b, LatLng.CREATOR);
                    break;
                case 3:
                    d = ac.j(parcel, b);
                    break;
                case 4:
                    f2 = ac.i(parcel, b);
                    break;
                case 5:
                    i2 = ac.f(parcel, b);
                    break;
                case 6:
                    i = ac.f(parcel, b);
                    break;
                case 7:
                    f = ac.i(parcel, b);
                    break;
                case 8:
                    z = ac.c(parcel, b);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new CircleOptions(i3, latLng, d, f2, i2, i, f, z);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    public CircleOptions[] newArray(int size) {
        return new CircleOptions[size];
    }
}
