package com.google.android.gms.location;

import android.os.Parcel;
import android.os.SystemClock;
import com.google.android.gms.internal.ae;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

public final class LocationRequest implements ae {
    public static final LocationRequestCreator CREATOR = new LocationRequestCreator();
    public static final int PRIORITY_BALANCED_POWER_ACCURACY = 102;
    public static final int PRIORITY_HIGH_ACCURACY = 100;
    public static final int PRIORITY_NO_POWER = 105;
    int T;
    long eA = ((long) (((double) this.ez) / 6.0d));
    boolean eB = false;
    int eC = Integer.MAX_VALUE;
    float eD = BitmapDescriptorFactory.HUE_RED;
    long eu = Long.MAX_VALUE;
    long ez = 3600000;
    int mPriority = PRIORITY_BALANCED_POWER_ACCURACY;

    private static void H(int i) {
        switch (i) {
            case 100:
            case PRIORITY_BALANCED_POWER_ACCURACY /*102*/:
            case 104:
            case PRIORITY_NO_POWER /*105*/:
                break;
            case 101:
            case 103:
            default:
                throw new IllegalArgumentException("invalid quality: " + i);
        }
    }

    public static String I(int i) {
        switch (i) {
            case 100:
                return "PRIORITY_HIGH_ACCURACY";
            case 101:
            case 103:
            default:
                return "???";
            case PRIORITY_BALANCED_POWER_ACCURACY /*102*/:
                return "PRIORITY_BALANCED_POWER_ACCURACY";
            case 104:
                return "PRIORITY_LOW_POWER";
        }
    }

    private static void a(float f) {
        if (f < BitmapDescriptorFactory.HUE_RED) {
            throw new IllegalArgumentException("invalid displacement: " + f);
        }
    }

    private static void c(long j) {
        if (j < 0) {
            throw new IllegalArgumentException("invalid interval: " + j);
        }
    }

    public static LocationRequest create() {
        return new LocationRequest();
    }

    public int describeContents() {
        return 0;
    }

    public long getExpirationTime() {
        return this.eu;
    }

    public long getFastestInterval() {
        return this.eA;
    }

    public long getInterval() {
        return this.ez;
    }

    public int getNumUpdates() {
        return this.eC;
    }

    public int getPriority() {
        return this.mPriority;
    }

    public float getSmallestDisplacement() {
        return this.eD;
    }

    public LocationRequest setExpirationDuration(long millis) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (millis > Long.MAX_VALUE - elapsedRealtime) {
            this.eu = Long.MAX_VALUE;
        } else {
            this.eu = elapsedRealtime + millis;
        }
        if (this.eu < 0) {
            this.eu = 0;
        }
        return this;
    }

    public LocationRequest setExpirationTime(long millis) {
        this.eu = millis;
        if (this.eu < 0) {
            this.eu = 0;
        }
        return this;
    }

    public LocationRequest setFastestInterval(long millis) {
        c(millis);
        this.eB = true;
        this.eA = millis;
        return this;
    }

    public LocationRequest setInterval(long millis) {
        c(millis);
        this.ez = millis;
        if (!this.eB) {
            this.eA = (long) (((double) this.ez) / 6.0d);
        }
        return this;
    }

    public LocationRequest setNumUpdates(int numUpdates) {
        if (numUpdates <= 0) {
            throw new IllegalArgumentException("invalid numUpdates: " + numUpdates);
        }
        this.eC = numUpdates;
        return this;
    }

    public LocationRequest setPriority(int priority) {
        H(priority);
        this.mPriority = priority;
        return this;
    }

    public LocationRequest setSmallestDisplacement(float smallestDisplacementMeters) {
        a(smallestDisplacementMeters);
        this.eD = smallestDisplacementMeters;
        return this;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Request[").append(I(this.mPriority));
        if (this.mPriority != 105) {
            sb.append(" requested=");
            sb.append(this.ez + "ms");
        }
        sb.append(" fastest=");
        sb.append(this.eA + "ms");
        if (this.eu != Long.MAX_VALUE) {
            long elapsedRealtime = this.eu - SystemClock.elapsedRealtime();
            sb.append(" expireIn=");
            sb.append(elapsedRealtime + "ms");
        }
        if (this.eC != Integer.MAX_VALUE) {
            sb.append(" num=").append(this.eC);
        }
        sb.append(']');
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int flags) {
        LocationRequestCreator locationRequestCreator = CREATOR;
        LocationRequestCreator.a(this, parcel, flags);
    }
}
