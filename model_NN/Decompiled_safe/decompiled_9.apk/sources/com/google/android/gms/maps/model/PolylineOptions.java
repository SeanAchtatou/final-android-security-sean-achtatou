package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.cx;
import com.google.android.gms.internal.df;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class PolylineOptions implements ae {
    public static final PolylineOptionsCreator CREATOR = new PolylineOptionsCreator();
    private int L;
    private final int T;
    private float fU;
    private boolean fV;
    private float fZ;
    private final List<LatLng> gq;
    private boolean gs;

    public PolylineOptions() {
        this.fZ = 10.0f;
        this.L = -16777216;
        this.fU = BitmapDescriptorFactory.HUE_RED;
        this.fV = true;
        this.gs = false;
        this.T = 1;
        this.gq = new ArrayList();
    }

    PolylineOptions(int versionCode, List points, float width, int color, float zIndex, boolean visible, boolean geodesic) {
        this.fZ = 10.0f;
        this.L = -16777216;
        this.fU = BitmapDescriptorFactory.HUE_RED;
        this.fV = true;
        this.gs = false;
        this.T = versionCode;
        this.gq = points;
        this.fZ = width;
        this.L = color;
        this.fU = zIndex;
        this.fV = visible;
        this.gs = geodesic;
    }

    public PolylineOptions add(LatLng point) {
        this.gq.add(point);
        return this;
    }

    public PolylineOptions add(LatLng... points) {
        this.gq.addAll(Arrays.asList(points));
        return this;
    }

    public PolylineOptions addAll(Iterable<LatLng> points) {
        for (LatLng add : points) {
            this.gq.add(add);
        }
        return this;
    }

    public PolylineOptions color(int color) {
        this.L = color;
        return this;
    }

    public int describeContents() {
        return 0;
    }

    public PolylineOptions geodesic(boolean geodesic) {
        this.gs = geodesic;
        return this;
    }

    public int getColor() {
        return this.L;
    }

    public List<LatLng> getPoints() {
        return this.gq;
    }

    public float getWidth() {
        return this.fZ;
    }

    public float getZIndex() {
        return this.fU;
    }

    public boolean isGeodesic() {
        return this.gs;
    }

    public boolean isVisible() {
        return this.fV;
    }

    public int u() {
        return this.T;
    }

    public PolylineOptions visible(boolean visible) {
        this.fV = visible;
        return this;
    }

    public PolylineOptions width(float width) {
        this.fZ = width;
        return this;
    }

    public void writeToParcel(Parcel out, int flags) {
        if (cx.aV()) {
            df.a(this, out, flags);
        } else {
            PolylineOptionsCreator.a(this, out, flags);
        }
    }

    public PolylineOptions zIndex(float zIndex) {
        this.fU = zIndex;
        return this;
    }
}
