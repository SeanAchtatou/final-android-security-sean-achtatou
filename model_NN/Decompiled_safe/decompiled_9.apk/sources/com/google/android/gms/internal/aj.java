package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;

public class aj implements Parcelable.Creator<ai> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.ak, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(ai aiVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, aiVar.u());
        ad.a(parcel, 2, (Parcelable) aiVar.B(), i, false);
        ad.C(parcel, d);
    }

    /* renamed from: f */
    public ai createFromParcel(Parcel parcel) {
        int c = ac.c(parcel);
        int i = 0;
        ak akVar = null;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i = ac.f(parcel, b);
                    break;
                case 2:
                    akVar = (ak) ac.a(parcel, b, ak.CREATOR);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new ai(i, akVar);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    /* renamed from: l */
    public ai[] newArray(int i) {
        return new ai[i];
    }
}
