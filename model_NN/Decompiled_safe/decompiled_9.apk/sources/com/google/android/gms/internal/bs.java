package com.google.android.gms.internal;

import android.os.Bundle;

public final class bs {
    private final Bundle dq;

    public bs(Bundle bundle) {
        this.dq = bundle == null ? new Bundle() : bundle;
    }

    public Bundle ar() {
        return this.dq;
    }
}
