package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ad;

public class CameraPositionCreator implements Parcelable.Creator<CameraPosition> {
    public static final int CONTENT_DESCRIPTION = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(CameraPosition cameraPosition, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, cameraPosition.u());
        ad.a(parcel, 2, (Parcelable) cameraPosition.target, i, false);
        ad.a(parcel, 3, cameraPosition.zoom);
        ad.a(parcel, 4, cameraPosition.tilt);
        ad.a(parcel, 5, cameraPosition.bearing);
        ad.C(parcel, d);
    }

    public CameraPosition createFromParcel(Parcel parcel) {
        float f = BitmapDescriptorFactory.HUE_RED;
        int c = ac.c(parcel);
        int i = 0;
        LatLng latLng = null;
        float f2 = 0.0f;
        float f3 = 0.0f;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i = ac.f(parcel, b);
                    break;
                case 2:
                    latLng = (LatLng) ac.a(parcel, b, LatLng.CREATOR);
                    break;
                case 3:
                    f3 = ac.i(parcel, b);
                    break;
                case 4:
                    f2 = ac.i(parcel, b);
                    break;
                case 5:
                    f = ac.i(parcel, b);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new CameraPosition(i, latLng, f3, f2, f);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    public CameraPosition[] newArray(int size) {
        return new CameraPosition[size];
    }
}
