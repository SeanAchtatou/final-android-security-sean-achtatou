package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.dynamic.LifecycleDelegate;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class bb<T extends LifecycleDelegate> {
    /* access modifiers changed from: private */
    public T bQ;
    /* access modifiers changed from: private */
    public Bundle bR;
    /* access modifiers changed from: private */
    public LinkedList<a> bS;
    private final be<T> bT = new be<T>() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.bb.a(com.google.android.gms.internal.bb, com.google.android.gms.dynamic.LifecycleDelegate):com.google.android.gms.dynamic.LifecycleDelegate
         arg types: [com.google.android.gms.internal.bb, T]
         candidates:
          com.google.android.gms.internal.bb.a(com.google.android.gms.internal.bb, android.os.Bundle):android.os.Bundle
          com.google.android.gms.internal.bb.a(android.os.Bundle, com.google.android.gms.internal.bb$a):void
          com.google.android.gms.internal.bb.a(com.google.android.gms.internal.bb, com.google.android.gms.dynamic.LifecycleDelegate):com.google.android.gms.dynamic.LifecycleDelegate */
        public void a(T t) {
            LifecycleDelegate unused = bb.this.bQ = (LifecycleDelegate) t;
            Iterator it = bb.this.bS.iterator();
            while (it.hasNext()) {
                ((a) it.next()).b(bb.this.bQ);
            }
            bb.this.bS.clear();
            Bundle unused2 = bb.this.bR = (Bundle) null;
        }
    };

    interface a {
        void b(LifecycleDelegate lifecycleDelegate);

        int getState();
    }

    private void a(Bundle bundle, a aVar) {
        if (this.bQ != null) {
            aVar.b(this.bQ);
            return;
        }
        if (this.bS == null) {
            this.bS = new LinkedList<>();
        }
        this.bS.add(aVar);
        if (bundle != null) {
            if (this.bR == null) {
                this.bR = (Bundle) bundle.clone();
            } else {
                this.bR.putAll(bundle);
            }
        }
        a(this.bT);
    }

    private void u(int i) {
        while (!this.bS.isEmpty() && this.bS.getLast().getState() >= i) {
            this.bS.removeLast();
        }
    }

    public void a(FrameLayout frameLayout) {
        final Context context = frameLayout.getContext();
        final int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        String b = GooglePlayServicesUtil.b(context, isGooglePlayServicesAvailable, -1);
        String a2 = GooglePlayServicesUtil.a(context, isGooglePlayServicesAvailable);
        LinearLayout linearLayout = new LinearLayout(frameLayout.getContext());
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        frameLayout.addView(linearLayout);
        TextView textView = new TextView(frameLayout.getContext());
        textView.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        textView.setText(b);
        linearLayout.addView(textView);
        if (a2 != null) {
            Button button = new Button(context);
            button.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
            button.setText(a2);
            linearLayout.addView(button);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    context.startActivity(GooglePlayServicesUtil.a(context, isGooglePlayServicesAvailable, -1));
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(be<T> beVar);

    public T ag() {
        return this.bQ;
    }

    public void onCreate(final Bundle savedInstanceState) {
        a(savedInstanceState, new a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                bb.this.bQ.onCreate(savedInstanceState);
            }

            public int getState() {
                return 1;
            }
        });
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final FrameLayout frameLayout = new FrameLayout(inflater.getContext());
        final LayoutInflater layoutInflater = inflater;
        final ViewGroup viewGroup = container;
        final Bundle bundle = savedInstanceState;
        a(savedInstanceState, new a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                frameLayout.removeAllViews();
                frameLayout.addView(bb.this.bQ.onCreateView(layoutInflater, viewGroup, bundle));
            }

            public int getState() {
                return 2;
            }
        });
        if (this.bQ == null) {
            a(frameLayout);
        }
        return frameLayout;
    }

    public void onDestroy() {
        if (this.bQ != null) {
            this.bQ.onDestroy();
        } else {
            u(1);
        }
    }

    public void onDestroyView() {
        if (this.bQ != null) {
            this.bQ.onDestroyView();
        } else {
            u(2);
        }
    }

    public void onInflate(final Activity activity, final Bundle attrs, final Bundle savedInstanceState) {
        a(savedInstanceState, new a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                bb.this.bQ.onInflate(activity, attrs, savedInstanceState);
            }

            public int getState() {
                return 0;
            }
        });
    }

    public void onLowMemory() {
        if (this.bQ != null) {
            this.bQ.onLowMemory();
        }
    }

    public void onPause() {
        if (this.bQ != null) {
            this.bQ.onPause();
        } else {
            u(3);
        }
    }

    public void onResume() {
        a((Bundle) null, new a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                bb.this.bQ.onResume();
            }

            public int getState() {
                return 3;
            }
        });
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onSaveInstanceState(android.os.Bundle r2) {
        /*
            r1 = this;
            T r0 = r1.bQ
            if (r0 == 0) goto L_0x000a
            T r0 = r1.bQ
            r0.onSaveInstanceState(r2)
        L_0x0009:
            return
        L_0x000a:
            android.os.Bundle r0 = r1.bR
            if (r0 == 0) goto L_0x0009
            android.os.Bundle r0 = r1.bR
            r2.putAll(r0)
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.bb.onSaveInstanceState(android.os.Bundle):void");
    }
}
