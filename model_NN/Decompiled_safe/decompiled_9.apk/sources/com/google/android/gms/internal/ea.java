package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Paint;
import android.net.Uri;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class ea extends FrameLayout {
    private String[] hL = null;
    private final ImageView hM;
    private final TextView hN;

    public ea(Context context) {
        super(context);
        this.hM = new ImageView(context);
        addView(this.hM, new FrameLayout.LayoutParams(-2, -2, 17));
        this.hN = new TextView(context);
        addView(this.hN, new FrameLayout.LayoutParams(-2, -2, 17));
        bringChildToFront(this.hN);
    }

    public void a(Uri uri) {
        this.hM.setImageURI(uri);
    }

    public void f(String[] strArr) {
        this.hL = strArr;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int measureText;
        int size = View.MeasureSpec.getSize(widthMeasureSpec);
        Paint paint = new Paint();
        paint.setTextSize(this.hN.getTextSize());
        paint.setTypeface(this.hN.getTypeface());
        int length = this.hL != null ? this.hL.length : 0;
        int i = 0;
        String str = null;
        for (int i2 = 0; i2 < length; i2++) {
            if (this.hL[i2] != null && (measureText = (int) paint.measureText(this.hL[i2])) <= size && measureText >= i) {
                str = this.hL[i2];
                i = measureText;
            }
        }
        if (str == null || !str.equals(this.hN.getText())) {
            this.hN.setText(str);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setGravity(int gravity) {
        this.hN.setGravity(gravity);
    }

    public void setSingleLine() {
        this.hN.setSingleLine();
    }

    public void setTextColor(int color) {
        this.hN.setTextColor(color);
    }

    public void setTextSize(int unit, float size) {
        this.hN.setTextSize(unit, size);
    }
}
