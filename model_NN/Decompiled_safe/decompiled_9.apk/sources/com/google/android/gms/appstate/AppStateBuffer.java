package com.google.android.gms.appstate;

import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.internal.c;
import com.google.android.gms.internal.k;

public final class AppStateBuffer extends DataBuffer<AppState> {
    public AppStateBuffer(k dataHolder) {
        super(dataHolder);
    }

    public AppState get(int position) {
        return new c(this.O, position);
    }
}
