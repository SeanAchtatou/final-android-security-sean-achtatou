package com.google.android.gms.location;

import android.os.Parcel;
import com.google.android.gms.internal.ae;

public class DetectedActivity implements ae {
    public static final DetectedActivityCreator CREATOR = new DetectedActivityCreator();
    public static final int IN_VEHICLE = 0;
    public static final int ON_BICYCLE = 1;
    public static final int ON_FOOT = 2;
    public static final int STILL = 3;
    public static final int TILTING = 5;
    public static final int UNKNOWN = 4;
    int T;
    int eq;
    int er;

    public DetectedActivity() {
        this.T = 1;
    }

    public DetectedActivity(int activityType, int confidence) {
        this();
        this.eq = activityType;
        this.er = confidence;
    }

    private int G(int i) {
        if (i > 5) {
            return 4;
        }
        return i;
    }

    public int describeContents() {
        return 0;
    }

    public int getConfidence() {
        return this.er;
    }

    public int getType() {
        return G(this.eq);
    }

    public String toString() {
        return "DetectedActivity [type=" + getType() + ", confidence=" + this.er + "]";
    }

    public void writeToParcel(Parcel out, int flags) {
        DetectedActivityCreator.a(this, out, flags);
    }
}
