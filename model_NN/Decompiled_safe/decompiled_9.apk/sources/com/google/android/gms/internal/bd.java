package com.google.android.gms.internal;

import android.os.IBinder;
import com.google.android.gms.internal.bc;
import java.lang.reflect.Field;

public final class bd<T> extends bc.a {
    private final T cd;

    private bd(T t) {
        this.cd = t;
    }

    public static <T> T a(bc bcVar) {
        if (bcVar instanceof bd) {
            return ((bd) bcVar).cd;
        }
        IBinder asBinder = bcVar.asBinder();
        Field[] declaredFields = asBinder.getClass().getDeclaredFields();
        if (declaredFields.length == 1) {
            Field field = declaredFields[0];
            if (!field.isAccessible()) {
                field.setAccessible(true);
                try {
                    return field.get(asBinder);
                } catch (NullPointerException e) {
                    throw new IllegalArgumentException("Binder object is null.", e);
                } catch (IllegalArgumentException e2) {
                    throw new IllegalArgumentException("remoteBinder is the wrong class.", e2);
                } catch (IllegalAccessException e3) {
                    throw new IllegalArgumentException("Could not access the field in remoteBinder.", e3);
                }
            } else {
                throw new IllegalArgumentException("The concrete class implementing IObjectWrapper must have exactly one declared *private* field for the wrapped object. Preferably, this is an instance of the ObjectWrapper<T> class.");
            }
        } else {
            throw new IllegalArgumentException("The concrete class implementing IObjectWrapper must have exactly *one* declared private field for the wrapped object.  Preferably, this is an instance of the ObjectWrapper<T> class.");
        }
    }

    public static <T> bc f(T t) {
        return new bd(t);
    }
}
