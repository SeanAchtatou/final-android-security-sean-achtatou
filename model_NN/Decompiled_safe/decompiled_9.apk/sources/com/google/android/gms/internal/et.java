package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.eq;
import java.util.HashSet;
import java.util.Set;

public class et implements Parcelable.Creator<eq.h> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(eq.h hVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        Set<Integer> by = hVar.by();
        if (by.contains(1)) {
            ad.c(parcel, 1, hVar.u());
        }
        if (by.contains(2)) {
            ad.a(parcel, 2, hVar.isPrimary());
        }
        if (by.contains(3)) {
            ad.a(parcel, 3, hVar.getValue(), true);
        }
        ad.C(parcel, d);
    }

    /* renamed from: G */
    public eq.h createFromParcel(Parcel parcel) {
        boolean z = false;
        int c = ac.c(parcel);
        HashSet hashSet = new HashSet();
        String str = null;
        int i = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i = ac.f(parcel, b);
                    hashSet.add(1);
                    break;
                case 2:
                    z = ac.c(parcel, b);
                    hashSet.add(2);
                    break;
                case 3:
                    str = ac.l(parcel, b);
                    hashSet.add(3);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new eq.h(hashSet, i, z, str);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    /* renamed from: aa */
    public eq.h[] newArray(int i) {
        return new eq.h[i];
    }
}
