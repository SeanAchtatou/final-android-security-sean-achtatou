package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.eq;
import java.util.HashSet;
import java.util.Set;

public class eo implements Parcelable.Creator<eq.e> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(eq.e eVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        Set<Integer> by = eVar.by();
        if (by.contains(1)) {
            ad.c(parcel, 1, eVar.u());
        }
        if (by.contains(2)) {
            ad.a(parcel, 2, eVar.getFamilyName(), true);
        }
        if (by.contains(3)) {
            ad.a(parcel, 3, eVar.getFormatted(), true);
        }
        if (by.contains(4)) {
            ad.a(parcel, 4, eVar.getGivenName(), true);
        }
        if (by.contains(5)) {
            ad.a(parcel, 5, eVar.getHonorificPrefix(), true);
        }
        if (by.contains(6)) {
            ad.a(parcel, 6, eVar.getHonorificSuffix(), true);
        }
        if (by.contains(7)) {
            ad.a(parcel, 7, eVar.getMiddleName(), true);
        }
        ad.C(parcel, d);
    }

    /* renamed from: D */
    public eq.e createFromParcel(Parcel parcel) {
        String str = null;
        int c = ac.c(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i = ac.f(parcel, b);
                    hashSet.add(1);
                    break;
                case 2:
                    str6 = ac.l(parcel, b);
                    hashSet.add(2);
                    break;
                case 3:
                    str5 = ac.l(parcel, b);
                    hashSet.add(3);
                    break;
                case 4:
                    str4 = ac.l(parcel, b);
                    hashSet.add(4);
                    break;
                case 5:
                    str3 = ac.l(parcel, b);
                    hashSet.add(5);
                    break;
                case 6:
                    str2 = ac.l(parcel, b);
                    hashSet.add(6);
                    break;
                case 7:
                    str = ac.l(parcel, b);
                    hashSet.add(7);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new eq.e(hashSet, i, str6, str5, str4, str3, str2, str);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    /* renamed from: X */
    public eq.e[] newArray(int i) {
        return new eq.e[i];
    }
}
