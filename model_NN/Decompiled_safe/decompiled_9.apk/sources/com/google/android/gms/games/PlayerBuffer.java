package com.google.android.gms.games;

import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.internal.bg;
import com.google.android.gms.internal.k;

public final class PlayerBuffer extends DataBuffer<Player> {
    public PlayerBuffer(k dataHolder) {
        super(dataHolder);
    }

    public Player get(int position) {
        return new bg(this.O, position);
    }
}
