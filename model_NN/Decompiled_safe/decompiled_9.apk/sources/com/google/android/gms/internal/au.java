package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;

public class au implements Parcelable.Creator<at> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
     arg types: [android.os.Parcel, int, android.os.Parcel, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.aq, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(at atVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, atVar.u());
        ad.a(parcel, 2, atVar.Y(), false);
        ad.a(parcel, 3, (Parcelable) atVar.Z(), i, false);
        ad.C(parcel, d);
    }

    /* renamed from: m */
    public at createFromParcel(Parcel parcel) {
        aq aqVar = null;
        int c = ac.c(parcel);
        int i = 0;
        Parcel parcel2 = null;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i = ac.f(parcel, b);
                    break;
                case 2:
                    parcel2 = ac.y(parcel, b);
                    break;
                case 3:
                    aqVar = (aq) ac.a(parcel, b, aq.CREATOR);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new at(i, parcel2, aqVar);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    /* renamed from: s */
    public at[] newArray(int i) {
        return new at[i];
    }
}
