package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.PlusOneButton;

public class dz extends LinearLayout implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {
    private static final int hr = Color.parseColor("#666666");
    private int K = 3;
    protected int bi = 1;
    protected PlusClient gY;
    private int hA = 2;
    private Uri[] hB;
    private String[] hC;
    private String[] hD;
    protected String hE;
    protected du hF;
    protected final Resources hG;
    protected final LayoutInflater hH;
    private b hI = new b();
    protected boolean hs;
    protected int ht = 0;
    protected final LinearLayout hu;
    protected final FrameLayout hv;
    protected final CompoundButton hw;
    private final ProgressBar hx;
    protected final ea hy;
    private final dt[] hz = new dt[4];

    class a implements View.OnClickListener, PlusOneButton.OnPlusOneClickListener {
        private final PlusOneButton.OnPlusOneClickListener hJ;

        public a(PlusOneButton.OnPlusOneClickListener onPlusOneClickListener) {
            this.hJ = onPlusOneClickListener;
        }

        public void onClick(View view) {
            if (view == dz.this.hw || view == dz.this.hy) {
                Intent intent = dz.this.hF == null ? null : dz.this.hF.getIntent();
                if (this.hJ != null) {
                    this.hJ.onPlusOneClick(intent);
                } else {
                    onPlusOneClick(intent);
                }
            }
        }

        public void onPlusOneClick(Intent intent) {
            Context context = dz.this.getContext();
            if ((context instanceof Activity) && intent != null) {
                ((Activity) context).startActivityForResult(intent, dz.this.ht);
            }
        }
    }

    public class b implements PlusClient.b {
        protected b() {
        }

        public void a(ConnectionResult connectionResult, du duVar) {
            if (dz.this.hs) {
                dz.this.hs = false;
                dz.this.hw.refreshDrawableState();
            }
            if (!connectionResult.isSuccess() || duVar == null) {
                dz.this.bl();
                return;
            }
            dz.this.hF = duVar;
            dz.this.bc();
            dz.this.bi();
        }
    }

    class c extends CompoundButton {
        public c(Context context) {
            super(context);
        }

        public void toggle() {
            if (dz.this.hs) {
                super.toggle();
                return;
            }
            dz.this.hs = true;
            dz.this.bk();
        }
    }

    public dz(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        x.b(context, "Context must not be null.");
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(context) != 0) {
            this.hG = null;
            this.hH = null;
        } else {
            Context k = k(context);
            this.hG = k.getResources();
            this.hH = (LayoutInflater) k.getSystemService("layout_inflater");
        }
        this.K = a(context, attributeSet);
        this.hA = b(context, attributeSet);
        Point point = new Point();
        a(point);
        if (isInEditMode()) {
            TextView textView = new TextView(context);
            textView.setGravity(17);
            textView.setText("[ +1 ]");
            addView(textView, new LinearLayout.LayoutParams(point.x, point.y));
            this.hy = null;
            this.hx = null;
            this.hw = null;
            this.hv = null;
            this.hu = null;
            return;
        }
        setFocusable(true);
        this.hu = new LinearLayout(context);
        this.hu.setGravity(17);
        this.hu.setOrientation(0);
        addView(this.hu);
        this.hw = new c(context);
        this.hw.setBackgroundDrawable(null);
        this.hy = n(context);
        this.hv = l(context);
        this.hv.addView(this.hw, new FrameLayout.LayoutParams(point.x, point.y, 17));
        b(point);
        this.hx = m(context);
        this.hx.setVisibility(4);
        this.hv.addView(this.hx, new FrameLayout.LayoutParams(point.x, point.y, 17));
        int length = this.hz.length;
        for (int i = 0; i < length; i++) {
            this.hz[i] = o(getContext());
        }
        bm();
    }

    private int a(Context context, AttributeSet attributeSet) {
        String a2 = ab.a("http://schemas.android.com/apk/lib/com.google.android.gms.plus", "size", context, attributeSet, true, false, "PlusOneButton");
        if ("SMALL".equalsIgnoreCase(a2)) {
            return 0;
        }
        if ("MEDIUM".equalsIgnoreCase(a2)) {
            return 1;
        }
        if ("TALL".equalsIgnoreCase(a2)) {
            return 2;
        }
        return "STANDARD".equalsIgnoreCase(a2) ? 3 : 3;
    }

    private void a(Point point) {
        int i = 24;
        int i2 = 20;
        switch (this.K) {
            case 0:
                i2 = 14;
                break;
            case 1:
                i = 32;
                break;
            case 2:
                i = 50;
                break;
            default:
                i = 38;
                i2 = 24;
                break;
        }
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float applyDimension = TypedValue.applyDimension(1, (float) i, displayMetrics);
        float applyDimension2 = TypedValue.applyDimension(1, (float) i2, displayMetrics);
        point.x = (int) (((double) applyDimension) + 0.5d);
        point.y = (int) (((double) applyDimension2) + 0.5d);
    }

    private void a(Uri[] uriArr) {
        this.hB = uriArr;
        bq();
    }

    private int b(Context context, AttributeSet attributeSet) {
        String a2 = ab.a("http://schemas.android.com/apk/lib/com.google.android.gms.plus", "annotation", context, attributeSet, true, false, "PlusOneButton");
        if ("INLINE".equalsIgnoreCase(a2)) {
            return 2;
        }
        if ("NONE".equalsIgnoreCase(a2)) {
            return 0;
        }
        if ("BUBBLE".equalsIgnoreCase(a2)) {
        }
        return 1;
    }

    private void b(Point point) {
        point.y = (int) (((float) point.y) - TypedValue.applyDimension(1, 6.0f, getResources().getDisplayMetrics()));
        point.x = point.y;
    }

    private void bh() {
        boolean z = true;
        int applyDimension = (int) TypedValue.applyDimension(1, 5.0f, getContext().getResources().getDisplayMetrics());
        int applyDimension2 = (int) TypedValue.applyDimension(1, 1.0f, getContext().getResources().getDisplayMetrics());
        int length = this.hz.length;
        for (int i = 0; i < length; i++) {
            if (this.hz[i].getVisibility() == 0) {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.hz[i].getLayoutParams());
                if (z) {
                    layoutParams.setMargins(applyDimension, 0, applyDimension2, 0);
                    z = false;
                } else {
                    layoutParams.setMargins(applyDimension2, 0, applyDimension2, 0);
                }
                this.hz[i].setLayoutParams(layoutParams);
            }
        }
    }

    private LinearLayout.LayoutParams bj() {
        LinearLayout.LayoutParams layoutParams;
        int i = 0;
        switch (this.hA) {
            case 1:
                layoutParams = new LinearLayout.LayoutParams(-2, -2);
                break;
            case 2:
                layoutParams = new LinearLayout.LayoutParams(-2, -1);
                break;
            default:
                layoutParams = new LinearLayout.LayoutParams(-2, -2);
                break;
        }
        layoutParams.bottomMargin = this.K == 2 ? 1 : 0;
        if (this.K != 2) {
            i = 1;
        }
        layoutParams.leftMargin = i;
        return layoutParams;
    }

    private void bp() {
        switch (this.hA) {
            case 1:
                this.hy.f(this.hD);
                this.hy.setVisibility(0);
                return;
            case 2:
                this.hy.f(this.hC);
                this.hy.setVisibility(0);
                return;
            default:
                this.hy.f(null);
                this.hy.setVisibility(8);
                return;
        }
    }

    private void bq() {
        if (this.hB == null || this.hA != 2) {
            for (dt visibility : this.hz) {
                visibility.setVisibility(8);
            }
        } else {
            Point point = new Point();
            a(point);
            point.x = point.y;
            int length = this.hz.length;
            int length2 = this.hB.length;
            int i = 0;
            while (i < length) {
                Uri uri = i < length2 ? this.hB[i] : null;
                if (uri == null) {
                    this.hz[i].setVisibility(8);
                } else {
                    this.hz[i].setLayoutParams(new LinearLayout.LayoutParams(point.x, point.y));
                    this.hz[i].a(uri, point.y);
                    this.hz[i].setVisibility(0);
                }
                i++;
            }
        }
        bh();
    }

    private Drawable br() {
        if (this.hG == null) {
            return null;
        }
        return this.hG.getDrawable(this.hG.getIdentifier(bs(), "drawable", GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE));
    }

    private String bs() {
        switch (this.K) {
            case 0:
                return "ic_plusone_small";
            case 1:
                return "ic_plusone_medium";
            case 2:
                return "ic_plusone_tall";
            default:
                return "ic_plusone_standard";
        }
    }

    private Uri bt() {
        return y.i(bu());
    }

    private String bu() {
        switch (this.K) {
            case 0:
                return "global_count_bubble_small";
            case 1:
                return "global_count_bubble_medium";
            case 2:
                return "global_count_bubble_tall";
            default:
                return "global_count_bubble_standard";
        }
    }

    private void c(int i, int i2) {
        this.bi = i2;
        this.K = i;
        bi();
    }

    private void c(View view) {
        int applyDimension = (int) TypedValue.applyDimension(1, 3.0f, getContext().getResources().getDisplayMetrics());
        int applyDimension2 = (int) TypedValue.applyDimension(1, 5.0f, getContext().getResources().getDisplayMetrics());
        if (this.hA != 2) {
            applyDimension2 = 0;
        }
        if (!(this.K == 2 && this.hA == 1)) {
            applyDimension = 0;
        }
        view.setPadding(applyDimension2, 0, 0, applyDimension);
    }

    private static int d(int i, int i2) {
        switch (i) {
            case 0:
                return 11;
            case 1:
            default:
                return 13;
            case 2:
                return i2 != 2 ? 15 : 13;
        }
    }

    private void d(String[] strArr) {
        this.hC = strArr;
        bp();
    }

    private void e(String[] strArr) {
        this.hD = strArr;
        bp();
    }

    private Context k(Context context) {
        try {
            return getContext().createPackageContext(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, 4);
        } catch (PackageManager.NameNotFoundException e) {
            if (Log.isLoggable("PlusOneButton", 5)) {
                Log.w("PlusOneButton", "Google Play services is not installed");
            }
            return null;
        }
    }

    private FrameLayout l(Context context) {
        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setFocusable(false);
        return frameLayout;
    }

    private ProgressBar m(Context context) {
        ProgressBar progressBar = new ProgressBar(context, null, 16843400);
        progressBar.setFocusable(false);
        progressBar.setIndeterminate(true);
        return progressBar;
    }

    private ea n(Context context) {
        ea eaVar = new ea(context);
        eaVar.setFocusable(false);
        eaVar.setGravity(17);
        eaVar.setSingleLine();
        eaVar.setTextSize(0, TypedValue.applyDimension(2, (float) d(this.K, this.hA), context.getResources().getDisplayMetrics()));
        eaVar.setTextColor(hr);
        eaVar.setVisibility(0);
        return eaVar;
    }

    private dt o(Context context) {
        dt dtVar = new dt(context);
        dtVar.setVisibility(8);
        return dtVar;
    }

    /* access modifiers changed from: protected */
    public void bc() {
        if (this.hF != null) {
            d(this.hF.bf());
            e(new String[]{this.hF.be()});
            a(this.hF.bg());
            if (this.hF.bd()) {
                bn();
            } else {
                bm();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void bi() {
        if (!isInEditMode()) {
            this.hu.removeAllViews();
            Point point = new Point();
            a(point);
            this.hw.setLayoutParams(new FrameLayout.LayoutParams(point.x, point.y, 17));
            b(point);
            this.hx.setLayoutParams(new FrameLayout.LayoutParams(point.x, point.y, 17));
            if (this.hA == 1) {
                this.hy.a(bt());
            } else {
                this.hy.a(null);
            }
            bq();
            this.hy.setLayoutParams(bj());
            this.hy.setTextSize(0, TypedValue.applyDimension(2, (float) d(this.K, this.hA), getContext().getResources().getDisplayMetrics()));
            c(this.hy);
            if (this.K == 2 && this.hA == 1) {
                this.hu.setOrientation(1);
                this.hu.addView(this.hy);
                this.hu.addView(this.hv);
            } else {
                this.hu.setOrientation(0);
                this.hu.addView(this.hv);
                for (dt addView : this.hz) {
                    this.hu.addView(addView);
                }
                this.hu.addView(this.hy);
            }
            requestLayout();
        }
    }

    public void bk() {
        setType(2);
        this.hx.setVisibility(0);
        bo();
    }

    public void bl() {
        setType(3);
        this.hx.setVisibility(4);
        bo();
    }

    /* access modifiers changed from: protected */
    public void bm() {
        setType(1);
        this.hx.setVisibility(4);
        bo();
    }

    /* access modifiers changed from: protected */
    public void bn() {
        setType(0);
        this.hx.setVisibility(4);
        bo();
    }

    /* access modifiers changed from: protected */
    public void bo() {
        this.hw.setButtonDrawable(br());
        switch (this.bi) {
            case 0:
                this.hw.setEnabled(true);
                this.hw.setChecked(true);
                return;
            case 1:
                this.hw.setEnabled(true);
                this.hw.setChecked(false);
                return;
            case 2:
                this.hw.setEnabled(false);
                this.hw.setChecked(true);
                return;
            default:
                this.hw.setEnabled(false);
                this.hw.setChecked(false);
                return;
        }
    }

    public void initialize(PlusClient plusClient, String url, int activityRequestCode) {
        x.b(plusClient, "Plus client must not be null.");
        x.b(url, "URL must not be null.");
        x.a(activityRequestCode >= 0 && activityRequestCode <= 65535, "activityRequestCode must be an unsigned 16 bit integer.");
        this.ht = activityRequestCode;
        this.hE = url;
        if (plusClient != this.gY) {
            if (this.gY != null) {
                this.gY.unregisterConnectionCallbacks(this);
                this.gY.unregisterConnectionFailedListener(this);
            }
            this.gY = plusClient;
            this.gY.registerConnectionCallbacks(this);
            this.gY.registerConnectionFailedListener(this);
            for (dt a2 : this.hz) {
                a2.a(plusClient);
            }
        } else if (this.gY.isConnected()) {
            onConnected(null);
        }
        bi();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.gY != null) {
            if (!this.gY.isConnectionCallbacksRegistered(this)) {
                this.gY.registerConnectionCallbacks(this);
            }
            if (!this.gY.isConnectionFailedListenerRegistered(this)) {
                this.gY.registerConnectionFailedListener(this);
            }
        }
    }

    public void onConnected(Bundle connectionHint) {
        if (this.hE != null) {
            this.gY.a(this.hI, this.hE);
        }
    }

    public void onConnectionFailed(ConnectionResult status) {
        bl();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.gY != null) {
            if (this.gY.isConnectionCallbacksRegistered(this)) {
                this.gY.unregisterConnectionCallbacks(this);
            }
            if (this.gY.isConnectionFailedListenerRegistered(this)) {
                this.gY.unregisterConnectionFailedListener(this);
            }
        }
    }

    public void onDisconnected() {
    }

    public boolean performClick() {
        return this.hw.performClick();
    }

    public void setAnnotation(int annotation) {
        x.b(Integer.valueOf(annotation), "Annotation must not be null.");
        this.hA = annotation;
        bp();
        bi();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.hw.setOnClickListener(listener);
        this.hy.setOnClickListener(listener);
    }

    public void setOnPlusOneClickListener(PlusOneButton.OnPlusOneClickListener listener) {
        setOnClickListener(new a(listener));
    }

    public void setSize(int size) {
        c(size, this.bi);
    }

    public void setType(int type) {
        c(this.K, type);
    }
}
