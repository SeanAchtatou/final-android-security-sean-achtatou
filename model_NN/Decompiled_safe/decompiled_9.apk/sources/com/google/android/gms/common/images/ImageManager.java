package com.google.android.gms.common.images;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.ResultReceiver;
import android.util.Log;
import android.widget.ImageView;
import com.google.android.gms.internal.af;
import com.google.android.gms.internal.ba;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class ImageManager {
    private static ImageManager ai;
    /* access modifiers changed from: private */
    public final af<Uri, WeakReference<Drawable.ConstantState>> aj = new af<>(50);
    /* access modifiers changed from: private */
    public final Map<ListenerHolder, ImageReceiver> ak;
    /* access modifiers changed from: private */
    public final Map<Uri, ImageReceiver> al;
    /* access modifiers changed from: private */
    public final Context mContext;

    public final class ImageReceiver extends ResultReceiver {
        private final ArrayList<ListenerHolder> ao = new ArrayList<>();
        private final Uri mUri;

        ImageReceiver(Uri uri) {
            super(new Handler(Looper.getMainLooper()));
            this.mUri = uri;
        }

        public void addOnImageLoadedListenerHolder(ListenerHolder imageViewLoadListener) {
            this.ao.add(imageViewLoadListener);
        }

        public Uri getUri() {
            return this.mUri;
        }

        public void onReceiveResult(int resultCode, Bundle resultData) {
            BitmapDrawable bitmapDrawable = null;
            ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor) resultData.getParcelable("com.google.android.gms.extra.fileDescriptor");
            if (parcelFileDescriptor != null) {
                Bitmap decodeFileDescriptor = BitmapFactory.decodeFileDescriptor(parcelFileDescriptor.getFileDescriptor());
                try {
                    parcelFileDescriptor.close();
                } catch (IOException e) {
                    Log.e("ImageManager", "closed failed", e);
                }
                BitmapDrawable bitmapDrawable2 = new BitmapDrawable(ImageManager.this.mContext.getResources(), decodeFileDescriptor);
                ImageManager.this.aj.put(this.mUri, new WeakReference(bitmapDrawable2.getConstantState()));
                bitmapDrawable = bitmapDrawable2;
            }
            ImageManager.this.al.remove(this.mUri);
            int size = this.ao.size();
            for (int i = 0; i < size; i++) {
                this.ao.get(i).onImageLoaded(this.mUri, bitmapDrawable);
            }
        }

        public void removeOnImageLoadedListenerHolder(ListenerHolder imageViewLoadListener) {
            this.ao.remove(imageViewLoadListener);
        }
    }

    abstract class ListenerHolder implements OnImageLoadedListener {
        protected final int mDefaultResId;
        protected final int mHashCode;

        private ListenerHolder(int hashCode, int defaultResId) {
            this.mHashCode = hashCode;
            this.mDefaultResId = defaultResId;
        }

        public abstract void handleCachedDrawable(Uri uri, Drawable drawable);

        public int hashCode() {
            return this.mHashCode;
        }

        public abstract void onImageLoaded(Uri uri, Drawable drawable);

        public abstract boolean shouldLoadImage(Uri uri);
    }

    public interface OnImageLoadedListener {
        void onImageLoaded(Uri uri, Drawable drawable);
    }

    final class a extends ListenerHolder {
        private final WeakReference<OnImageLoadedListener> am;

        private a(OnImageLoadedListener onImageLoadedListener, int i) {
            super(onImageLoadedListener.hashCode(), i);
            this.am = new WeakReference<>(onImageLoadedListener);
        }

        public boolean equals(Object o) {
            if (!(o instanceof a)) {
                return false;
            }
            a aVar = (a) o;
            return (this.am == null || aVar.am == null || this.mHashCode != aVar.mHashCode) ? false : true;
        }

        public void handleCachedDrawable(Uri uri, Drawable drawable) {
            OnImageLoadedListener onImageLoadedListener = this.am.get();
            if (onImageLoadedListener != null) {
                onImageLoadedListener.onImageLoaded(uri, drawable);
            }
        }

        public void onImageLoaded(Uri uri, Drawable drawable) {
            ImageReceiver imageReceiver = (ImageReceiver) ImageManager.this.ak.remove(this);
            OnImageLoadedListener onImageLoadedListener = this.am.get();
            if (onImageLoadedListener != null) {
                onImageLoadedListener.onImageLoaded(uri, drawable);
            }
        }

        public boolean shouldLoadImage(Uri uri) {
            if (uri != null) {
                return true;
            }
            OnImageLoadedListener onImageLoadedListener = this.am.get();
            if (onImageLoadedListener != null) {
                if (this.mDefaultResId == 0) {
                    onImageLoadedListener.onImageLoaded(uri, null);
                } else {
                    onImageLoadedListener.onImageLoaded(uri, ImageManager.this.mContext.getResources().getDrawable(this.mDefaultResId));
                }
            }
            return false;
        }
    }

    final class b extends ListenerHolder {
        private final WeakReference<ImageView> ap;

        private b(ImageView imageView, int i) {
            super(imageView.hashCode(), i);
            this.ap = new WeakReference<>(imageView);
        }

        public boolean equals(Object o) {
            if (!(o instanceof b)) {
                return false;
            }
            b bVar = (b) o;
            return (this.ap == null || bVar.ap == null || this.mHashCode != bVar.mHashCode) ? false : true;
        }

        public void handleCachedDrawable(Uri uri, Drawable drawable) {
            ImageView imageView = this.ap.get();
            if (imageView != null) {
                imageView.setImageDrawable(drawable);
            }
            ImageReceiver imageReceiver = (ImageReceiver) ImageManager.this.ak.remove(this);
            if (imageReceiver != null) {
                imageReceiver.removeOnImageLoadedListenerHolder(this);
            }
        }

        public void onImageLoaded(Uri uri, Drawable drawable) {
            ImageReceiver imageReceiver = (ImageReceiver) ImageManager.this.ak.remove(this);
            ImageView imageView = this.ap.get();
            if (imageView != null && imageReceiver != null && imageReceiver.getUri().equals(uri)) {
                imageView.setImageDrawable(drawable);
            }
        }

        public boolean shouldLoadImage(Uri uri) {
            ImageView imageView = this.ap.get();
            if (imageView != null) {
                if (this.mDefaultResId == 0) {
                    imageView.setImageDrawable(null);
                } else {
                    imageView.setImageResource(this.mDefaultResId);
                }
            }
            if (uri != null) {
                return true;
            }
            ImageReceiver imageReceiver = (ImageReceiver) ImageManager.this.ak.remove(this);
            if (imageReceiver != null) {
                imageReceiver.removeOnImageLoadedListenerHolder(this);
            }
            return false;
        }
    }

    static final class c implements ComponentCallbacks2 {
        private final af<Uri, WeakReference<Drawable.ConstantState>> aq;

        public c(af<Uri, WeakReference<Drawable.ConstantState>> afVar) {
            this.aq = afVar;
        }

        public void onConfigurationChanged(Configuration newConfig) {
        }

        public void onLowMemory() {
            this.aq.evictAll();
        }

        public void onTrimMemory(int level) {
            if (level >= 60) {
                this.aq.evictAll();
            } else if (level >= 40) {
                this.aq.trimToSize(this.aq.size() / 2);
            }
        }
    }

    private ImageManager(Context context) {
        this.mContext = context.getApplicationContext();
        if (ba.ad()) {
            this.mContext.registerComponentCallbacks(new c(this.aj));
        }
        this.ak = new HashMap();
        this.al = new HashMap();
    }

    private void a(ListenerHolder listenerHolder, Uri uri) {
        WeakReference weakReference;
        Drawable.ConstantState constantState;
        if (uri != null && (weakReference = this.aj.get(uri)) != null && (constantState = (Drawable.ConstantState) weakReference.get()) != null) {
            listenerHolder.handleCachedDrawable(uri, constantState.newDrawable());
        } else if (listenerHolder.shouldLoadImage(uri)) {
            ImageReceiver imageReceiver = this.al.get(uri);
            if (imageReceiver == null) {
                imageReceiver = new ImageReceiver(uri);
                this.al.put(uri, imageReceiver);
            }
            imageReceiver.addOnImageLoadedListenerHolder(listenerHolder);
            this.ak.put(listenerHolder, imageReceiver);
            Intent intent = new Intent("com.google.android.gms.common.images.LOAD_IMAGE");
            intent.putExtra("com.google.android.gms.extras.uri", uri);
            intent.putExtra("com.google.android.gms.extras.resultReceiver", imageReceiver);
            intent.putExtra("com.google.android.gms.extras.priority", 3);
            this.mContext.sendBroadcast(intent);
        }
    }

    public static ImageManager create(Context context) {
        if (ai == null) {
            ai = new ImageManager(context);
        }
        return ai;
    }

    public void loadImage(ImageView imageView, int resId) {
        loadImage(imageView, (Uri) null, resId);
    }

    public void loadImage(ImageView imageView, Uri uri) {
        loadImage(imageView, uri, 0);
    }

    public void loadImage(ImageView imageView, Uri uri, int defaultResId) {
        a(new b(imageView, defaultResId), uri);
    }

    public void loadImage(OnImageLoadedListener listener, Uri uri) {
        loadImage(listener, uri, 0);
    }

    public void loadImage(OnImageLoadedListener listener, Uri uri, int defaultResId) {
        a(new a(listener, defaultResId), uri);
    }
}
