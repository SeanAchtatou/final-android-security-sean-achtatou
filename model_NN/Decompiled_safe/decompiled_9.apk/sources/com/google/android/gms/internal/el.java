package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.eq;
import java.util.HashSet;
import java.util.Set;

public class el implements Parcelable.Creator<eq.b.C0031b> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(eq.b.C0031b bVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        Set<Integer> by = bVar.by();
        if (by.contains(1)) {
            ad.c(parcel, 1, bVar.u());
        }
        if (by.contains(2)) {
            ad.c(parcel, 2, bVar.getHeight());
        }
        if (by.contains(3)) {
            ad.a(parcel, 3, bVar.getUrl(), true);
        }
        if (by.contains(4)) {
            ad.c(parcel, 4, bVar.getWidth());
        }
        ad.C(parcel, d);
    }

    /* renamed from: A */
    public eq.b.C0031b createFromParcel(Parcel parcel) {
        int i = 0;
        int c = ac.c(parcel);
        HashSet hashSet = new HashSet();
        String str = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i3 = ac.f(parcel, b);
                    hashSet.add(1);
                    break;
                case 2:
                    i2 = ac.f(parcel, b);
                    hashSet.add(2);
                    break;
                case 3:
                    str = ac.l(parcel, b);
                    hashSet.add(3);
                    break;
                case 4:
                    i = ac.f(parcel, b);
                    hashSet.add(4);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new eq.b.C0031b(hashSet, i3, i2, str, i);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    /* renamed from: U */
    public eq.b.C0031b[] newArray(int i) {
        return new eq.b.C0031b[i];
    }
}
