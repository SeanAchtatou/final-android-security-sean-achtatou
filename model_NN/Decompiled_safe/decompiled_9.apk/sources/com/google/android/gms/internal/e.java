package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.appstate.AppState;
import com.google.android.gms.appstate.AppStateBuffer;
import com.google.android.gms.appstate.OnSignOutCompleteListener;
import com.google.android.gms.appstate.OnStateDeletedListener;
import com.google.android.gms.appstate.OnStateListLoadedListener;
import com.google.android.gms.appstate.OnStateLoadedListener;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.internal.g;

public final class e extends p<g> {
    private final String g;

    final class a extends d {
        private final OnStateDeletedListener n;

        public a(OnStateDeletedListener onStateDeletedListener) {
            this.n = (OnStateDeletedListener) x.b(onStateDeletedListener, "Listener must not be null");
        }

        public void onStateDeleted(int statusCode, int stateKey) {
            e.this.a(new b(this.n, statusCode, stateKey));
        }
    }

    final class b extends p<g>.b<OnStateDeletedListener> {
        private final int p;
        private final int q;

        public b(OnStateDeletedListener onStateDeletedListener, int i, int i2) {
            super(onStateDeletedListener);
            this.p = i;
            this.q = i2;
        }

        public void a(OnStateDeletedListener onStateDeletedListener) {
            onStateDeletedListener.onStateDeleted(this.p, this.q);
        }
    }

    final class c extends d {
        private final OnStateListLoadedListener r;

        public c(OnStateListLoadedListener onStateListLoadedListener) {
            this.r = (OnStateListLoadedListener) x.b(onStateListLoadedListener, "Listener must not be null");
        }

        public void a(k kVar) {
            e.this.a(new d(this.r, kVar));
        }
    }

    final class d extends p<g>.c<OnStateListLoadedListener> {
        public d(OnStateListLoadedListener onStateListLoadedListener, k kVar) {
            super(onStateListLoadedListener, kVar);
        }

        public void a(OnStateListLoadedListener onStateListLoadedListener) {
            onStateListLoadedListener.onStateListLoaded(this.O.getStatusCode(), new AppStateBuffer(this.O));
        }
    }

    /* renamed from: com.google.android.gms.internal.e$e  reason: collision with other inner class name */
    final class C0030e extends d {
        private final OnStateLoadedListener s;

        public C0030e(OnStateLoadedListener onStateLoadedListener) {
            this.s = (OnStateLoadedListener) x.b(onStateLoadedListener, "Listener must not be null");
        }

        public void a(int i, k kVar) {
            e.this.a(new f(this.s, i, kVar));
        }
    }

    final class f extends p<g>.c<OnStateLoadedListener> {
        private final int q;

        public f(OnStateLoadedListener onStateLoadedListener, int i, k kVar) {
            super(onStateLoadedListener, kVar);
            this.q = i;
        }

        /* JADX INFO: finally extract failed */
        public void a(OnStateLoadedListener onStateLoadedListener) {
            byte[] bArr;
            String str;
            byte[] bArr2 = null;
            AppStateBuffer appStateBuffer = new AppStateBuffer(this.O);
            try {
                if (appStateBuffer.getCount() > 0) {
                    AppState appState = appStateBuffer.get(0);
                    str = appState.getConflictVersion();
                    bArr = appState.getLocalData();
                    bArr2 = appState.getConflictData();
                } else {
                    bArr = null;
                    str = null;
                }
                appStateBuffer.close();
                int statusCode = this.O.getStatusCode();
                if (statusCode == 2000) {
                    onStateLoadedListener.onStateConflict(this.q, str, bArr, bArr2);
                } else {
                    onStateLoadedListener.onStateLoaded(statusCode, this.q, bArr);
                }
            } catch (Throwable th) {
                appStateBuffer.close();
                throw th;
            }
        }
    }

    final class g extends d {
        private final OnSignOutCompleteListener t;

        public g(OnSignOutCompleteListener onSignOutCompleteListener) {
            this.t = (OnSignOutCompleteListener) x.b(onSignOutCompleteListener, "Listener must not be null");
        }

        public void onSignOutComplete() {
            e.this.a(new h(this.t));
        }
    }

    final class h extends p<g>.b<OnSignOutCompleteListener> {
        public h(OnSignOutCompleteListener onSignOutCompleteListener) {
            super(onSignOutCompleteListener);
        }

        public void a(OnSignOutCompleteListener onSignOutCompleteListener) {
            onSignOutCompleteListener.onSignOutComplete();
        }
    }

    public e(Context context, GooglePlayServicesClient.ConnectionCallbacks connectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener onConnectionFailedListener, String str, String[] strArr) {
        super(context, connectionCallbacks, onConnectionFailedListener, strArr);
        this.g = (String) x.d(str);
    }

    public void a(OnStateLoadedListener onStateLoadedListener, int i, byte[] bArr) {
        try {
            ((g) o()).a(onStateLoadedListener == null ? null : new C0030e(onStateLoadedListener), i, bArr);
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
        }
    }

    /* access modifiers changed from: protected */
    public void a(u uVar, p<g>.d dVar) throws RemoteException {
        uVar.a(dVar, GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, getContext().getPackageName(), this.g, j());
    }

    /* access modifiers changed from: protected */
    public void a(String... strArr) {
        boolean z = false;
        for (String equals : strArr) {
            if (equals.equals(Scopes.APP_STATE)) {
                z = true;
            }
        }
        x.a(z, String.format("AppStateClient requires %s to function.", Scopes.APP_STATE));
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public g c(IBinder iBinder) {
        return g.a.e(iBinder);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "com.google.android.gms.appstate.service.START";
    }

    /* access modifiers changed from: protected */
    public String c() {
        return "com.google.android.gms.appstate.internal.IAppStateService";
    }

    public void deleteState(OnStateDeletedListener listener, int stateKey) {
        try {
            ((g) o()).b(new a(listener), stateKey);
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
        }
    }

    public int getMaxNumKeys() {
        try {
            return ((g) o()).getMaxNumKeys();
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
            return 2;
        }
    }

    public int getMaxStateSize() {
        try {
            return ((g) o()).getMaxStateSize();
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
            return 2;
        }
    }

    public void listStates(OnStateListLoadedListener listener) {
        try {
            ((g) o()).a(new c(listener));
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
        }
    }

    public void loadState(OnStateLoadedListener listener, int stateKey) {
        try {
            ((g) o()).a(new C0030e(listener), stateKey);
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
        }
    }

    public void resolveState(OnStateLoadedListener listener, int stateKey, String resolvedVersion, byte[] resolvedData) {
        try {
            ((g) o()).a(new C0030e(listener), stateKey, resolvedVersion, resolvedData);
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
        }
    }

    public void signOut(OnSignOutCompleteListener listener) {
        try {
            ((g) o()).b(listener == null ? null : new g(listener));
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
        }
    }
}
