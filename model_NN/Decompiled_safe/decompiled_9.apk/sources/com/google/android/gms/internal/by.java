package com.google.android.gms.internal;

import com.google.android.gms.games.multiplayer.realtime.Room;

public final class by extends m<Room> {
    public by(k kVar) {
        super(kVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Room a(int i, int i2) {
        return new bz(this.O, i, i2);
    }

    /* access modifiers changed from: protected */
    public String getPrimaryDataMarkerColumn() {
        return "external_match_id";
    }
}
