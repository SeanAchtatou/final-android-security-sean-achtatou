package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ad;

public class LocationRequestCreator implements Parcelable.Creator<LocationRequest> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void a(LocationRequest locationRequest, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, locationRequest.mPriority);
        ad.c(parcel, 1000, locationRequest.T);
        ad.a(parcel, 2, locationRequest.ez);
        ad.a(parcel, 3, locationRequest.eA);
        ad.a(parcel, 4, locationRequest.eB);
        ad.a(parcel, 5, locationRequest.eu);
        ad.c(parcel, 6, locationRequest.eC);
        ad.a(parcel, 7, locationRequest.eD);
        ad.C(parcel, d);
    }

    public LocationRequest createFromParcel(Parcel parcel) {
        LocationRequest locationRequest = new LocationRequest();
        int c = ac.c(parcel);
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    locationRequest.mPriority = ac.f(parcel, b);
                    break;
                case 2:
                    locationRequest.ez = ac.g(parcel, b);
                    break;
                case 3:
                    locationRequest.eA = ac.g(parcel, b);
                    break;
                case 4:
                    locationRequest.eB = ac.c(parcel, b);
                    break;
                case 5:
                    locationRequest.eu = ac.g(parcel, b);
                    break;
                case 6:
                    locationRequest.eC = ac.f(parcel, b);
                    break;
                case 7:
                    locationRequest.eD = ac.i(parcel, b);
                    break;
                case 1000:
                    locationRequest.T = ac.f(parcel, b);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return locationRequest;
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    public LocationRequest[] newArray(int size) {
        return new LocationRequest[size];
    }
}
