package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.util.TimeUtils;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.eq;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class er implements Parcelable.Creator<eq> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.eq$a, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.eq$b, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.eq$d, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.eq$e, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(eq eqVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        Set<Integer> by = eqVar.by();
        if (by.contains(1)) {
            ad.c(parcel, 1, eqVar.u());
        }
        if (by.contains(2)) {
            ad.a(parcel, 2, eqVar.getAboutMe(), true);
        }
        if (by.contains(3)) {
            ad.a(parcel, 3, (Parcelable) eqVar.bT(), i, true);
        }
        if (by.contains(4)) {
            ad.a(parcel, 4, eqVar.getBirthday(), true);
        }
        if (by.contains(5)) {
            ad.a(parcel, 5, eqVar.getBraggingRights(), true);
        }
        if (by.contains(6)) {
            ad.c(parcel, 6, eqVar.getCircledByCount());
        }
        if (by.contains(7)) {
            ad.a(parcel, 7, (Parcelable) eqVar.bU(), i, true);
        }
        if (by.contains(8)) {
            ad.a(parcel, 8, eqVar.getCurrentLocation(), true);
        }
        if (by.contains(9)) {
            ad.a(parcel, 9, eqVar.getDisplayName(), true);
        }
        if (by.contains(10)) {
            ad.b(parcel, 10, eqVar.bV(), true);
        }
        if (by.contains(11)) {
            ad.a(parcel, 11, eqVar.bW(), true);
        }
        if (by.contains(12)) {
            ad.c(parcel, 12, eqVar.getGender());
        }
        if (by.contains(13)) {
            ad.a(parcel, 13, eqVar.isHasApp());
        }
        if (by.contains(14)) {
            ad.a(parcel, 14, eqVar.getId(), true);
        }
        if (by.contains(15)) {
            ad.a(parcel, 15, (Parcelable) eqVar.bX(), i, true);
        }
        if (by.contains(16)) {
            ad.a(parcel, 16, eqVar.isPlusUser());
        }
        if (by.contains(19)) {
            ad.a(parcel, 19, (Parcelable) eqVar.bY(), i, true);
        }
        if (by.contains(18)) {
            ad.a(parcel, 18, eqVar.getLanguage(), true);
        }
        if (by.contains(21)) {
            ad.c(parcel, 21, eqVar.getObjectType());
        }
        if (by.contains(20)) {
            ad.a(parcel, 20, eqVar.getNickname(), true);
        }
        if (by.contains(23)) {
            ad.b(parcel, 23, eqVar.ca(), true);
        }
        if (by.contains(22)) {
            ad.b(parcel, 22, eqVar.bZ(), true);
        }
        if (by.contains(25)) {
            ad.c(parcel, 25, eqVar.getRelationshipStatus());
        }
        if (by.contains(24)) {
            ad.c(parcel, 24, eqVar.getPlusOneCount());
        }
        if (by.contains(27)) {
            ad.a(parcel, 27, eqVar.getUrl(), true);
        }
        if (by.contains(26)) {
            ad.a(parcel, 26, eqVar.getTagline(), true);
        }
        if (by.contains(29)) {
            ad.a(parcel, 29, eqVar.isVerified());
        }
        if (by.contains(28)) {
            ad.b(parcel, 28, eqVar.cb(), true);
        }
        ad.C(parcel, d);
    }

    /* renamed from: F */
    public eq createFromParcel(Parcel parcel) {
        int c = ac.c(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        String str = null;
        eq.a aVar = null;
        String str2 = null;
        String str3 = null;
        int i2 = 0;
        eq.b bVar = null;
        String str4 = null;
        String str5 = null;
        ArrayList arrayList = null;
        String str6 = null;
        int i3 = 0;
        boolean z = false;
        String str7 = null;
        eq.d dVar = null;
        boolean z2 = false;
        String str8 = null;
        eq.e eVar = null;
        String str9 = null;
        int i4 = 0;
        ArrayList arrayList2 = null;
        ArrayList arrayList3 = null;
        int i5 = 0;
        int i6 = 0;
        String str10 = null;
        String str11 = null;
        ArrayList arrayList4 = null;
        boolean z3 = false;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i = ac.f(parcel, b);
                    hashSet.add(1);
                    break;
                case 2:
                    str = ac.l(parcel, b);
                    hashSet.add(2);
                    break;
                case 3:
                    hashSet.add(3);
                    aVar = (eq.a) ac.a(parcel, b, eq.a.CREATOR);
                    break;
                case 4:
                    str2 = ac.l(parcel, b);
                    hashSet.add(4);
                    break;
                case 5:
                    str3 = ac.l(parcel, b);
                    hashSet.add(5);
                    break;
                case 6:
                    i2 = ac.f(parcel, b);
                    hashSet.add(6);
                    break;
                case 7:
                    hashSet.add(7);
                    bVar = (eq.b) ac.a(parcel, b, eq.b.CREATOR);
                    break;
                case 8:
                    str4 = ac.l(parcel, b);
                    hashSet.add(8);
                    break;
                case 9:
                    str5 = ac.l(parcel, b);
                    hashSet.add(9);
                    break;
                case 10:
                    arrayList = ac.c(parcel, b, eq.c.CREATOR);
                    hashSet.add(10);
                    break;
                case 11:
                    str6 = ac.l(parcel, b);
                    hashSet.add(11);
                    break;
                case 12:
                    i3 = ac.f(parcel, b);
                    hashSet.add(12);
                    break;
                case 13:
                    z = ac.c(parcel, b);
                    hashSet.add(13);
                    break;
                case 14:
                    str7 = ac.l(parcel, b);
                    hashSet.add(14);
                    break;
                case 15:
                    hashSet.add(15);
                    dVar = (eq.d) ac.a(parcel, b, eq.d.CREATOR);
                    break;
                case 16:
                    z2 = ac.c(parcel, b);
                    hashSet.add(16);
                    break;
                case 17:
                default:
                    ac.b(parcel, b);
                    break;
                case 18:
                    str8 = ac.l(parcel, b);
                    hashSet.add(18);
                    break;
                case TimeUtils.HUNDRED_DAY_FIELD_LEN:
                    hashSet.add(19);
                    eVar = (eq.e) ac.a(parcel, b, eq.e.CREATOR);
                    break;
                case 20:
                    str9 = ac.l(parcel, b);
                    hashSet.add(20);
                    break;
                case 21:
                    i4 = ac.f(parcel, b);
                    hashSet.add(21);
                    break;
                case 22:
                    arrayList2 = ac.c(parcel, b, eq.g.CREATOR);
                    hashSet.add(22);
                    break;
                case 23:
                    arrayList3 = ac.c(parcel, b, eq.h.CREATOR);
                    hashSet.add(23);
                    break;
                case 24:
                    i5 = ac.f(parcel, b);
                    hashSet.add(24);
                    break;
                case 25:
                    i6 = ac.f(parcel, b);
                    hashSet.add(25);
                    break;
                case 26:
                    str10 = ac.l(parcel, b);
                    hashSet.add(26);
                    break;
                case 27:
                    str11 = ac.l(parcel, b);
                    hashSet.add(27);
                    break;
                case 28:
                    arrayList4 = ac.c(parcel, b, eq.i.CREATOR);
                    hashSet.add(28);
                    break;
                case 29:
                    z3 = ac.c(parcel, b);
                    hashSet.add(29);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new eq(hashSet, i, str, aVar, str2, str3, i2, bVar, str4, str5, arrayList, str6, i3, z, str7, dVar, z2, str8, eVar, str9, i4, arrayList2, arrayList3, i5, i6, str10, str11, arrayList4, z3);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    /* renamed from: Z */
    public eq[] newArray(int i) {
        return new eq[i];
    }
}
