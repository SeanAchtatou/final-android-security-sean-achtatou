package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ad;
import java.util.ArrayList;

public class PolylineOptionsCreator implements Parcelable.Creator<PolylineOptions> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void a(PolylineOptions polylineOptions, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, polylineOptions.u());
        ad.b(parcel, 2, polylineOptions.getPoints(), false);
        ad.a(parcel, 3, polylineOptions.getWidth());
        ad.c(parcel, 4, polylineOptions.getColor());
        ad.a(parcel, 5, polylineOptions.getZIndex());
        ad.a(parcel, 6, polylineOptions.isVisible());
        ad.a(parcel, 7, polylineOptions.isGeodesic());
        ad.C(parcel, d);
    }

    public PolylineOptions createFromParcel(Parcel parcel) {
        float f = BitmapDescriptorFactory.HUE_RED;
        boolean z = false;
        int c = ac.c(parcel);
        ArrayList arrayList = null;
        boolean z2 = false;
        int i = 0;
        float f2 = 0.0f;
        int i2 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i2 = ac.f(parcel, b);
                    break;
                case 2:
                    arrayList = ac.c(parcel, b, LatLng.CREATOR);
                    break;
                case 3:
                    f2 = ac.i(parcel, b);
                    break;
                case 4:
                    i = ac.f(parcel, b);
                    break;
                case 5:
                    f = ac.i(parcel, b);
                    break;
                case 6:
                    z2 = ac.c(parcel, b);
                    break;
                case 7:
                    z = ac.c(parcel, b);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new PolylineOptions(i2, arrayList, f2, i, f, z2, z);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    public PolylineOptions[] newArray(int size) {
        return new PolylineOptions[size];
    }
}
