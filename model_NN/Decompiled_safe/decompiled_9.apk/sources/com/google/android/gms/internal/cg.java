package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

public class cg implements Parcelable.Creator<cf> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(cf cfVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.a(parcel, 1, cfVar.getRequestId(), false);
        ad.c(parcel, 1000, cfVar.u());
        ad.a(parcel, 2, cfVar.getExpirationTime());
        ad.a(parcel, 3, cfVar.aA());
        ad.a(parcel, 4, cfVar.getLatitude());
        ad.a(parcel, 5, cfVar.getLongitude());
        ad.a(parcel, 6, cfVar.aB());
        ad.c(parcel, 7, cfVar.aC());
        ad.C(parcel, d);
    }

    /* renamed from: M */
    public cf[] newArray(int i) {
        return new cf[i];
    }

    /* renamed from: t */
    public cf createFromParcel(Parcel parcel) {
        double d = 0.0d;
        short s = 0;
        int c = ac.c(parcel);
        String str = null;
        float f = BitmapDescriptorFactory.HUE_RED;
        long j = 0;
        double d2 = 0.0d;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    str = ac.l(parcel, b);
                    break;
                case 2:
                    j = ac.g(parcel, b);
                    break;
                case 3:
                    s = ac.e(parcel, b);
                    break;
                case 4:
                    d2 = ac.j(parcel, b);
                    break;
                case 5:
                    d = ac.j(parcel, b);
                    break;
                case 6:
                    f = ac.i(parcel, b);
                    break;
                case 7:
                    i = ac.f(parcel, b);
                    break;
                case 1000:
                    i2 = ac.f(parcel, b);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new cf(i2, str, i, s, d2, d, f, j);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }
}
