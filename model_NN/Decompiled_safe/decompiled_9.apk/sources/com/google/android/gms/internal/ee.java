package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.util.TimeUtils;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import com.google.android.gms.internal.ac;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ee implements Parcelable.Creator<ed> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.ed, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
     arg types: [android.os.Parcel, int, java.util.List<java.lang.String>, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(ed edVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        Set<Integer> by = edVar.by();
        if (by.contains(1)) {
            ad.c(parcel, 1, edVar.u());
        }
        if (by.contains(2)) {
            ad.a(parcel, 2, (Parcelable) edVar.bz(), i, true);
        }
        if (by.contains(3)) {
            ad.a(parcel, 3, edVar.getAdditionalName(), true);
        }
        if (by.contains(4)) {
            ad.a(parcel, 4, (Parcelable) edVar.bA(), i, true);
        }
        if (by.contains(5)) {
            ad.a(parcel, 5, edVar.getAddressCountry(), true);
        }
        if (by.contains(6)) {
            ad.a(parcel, 6, edVar.getAddressLocality(), true);
        }
        if (by.contains(7)) {
            ad.a(parcel, 7, edVar.getAddressRegion(), true);
        }
        if (by.contains(8)) {
            ad.b(parcel, 8, edVar.bB(), true);
        }
        if (by.contains(9)) {
            ad.c(parcel, 9, edVar.getAttendeeCount());
        }
        if (by.contains(10)) {
            ad.b(parcel, 10, edVar.bC(), true);
        }
        if (by.contains(11)) {
            ad.a(parcel, 11, (Parcelable) edVar.bD(), i, true);
        }
        if (by.contains(12)) {
            ad.b(parcel, 12, edVar.bE(), true);
        }
        if (by.contains(13)) {
            ad.a(parcel, 13, edVar.getBestRating(), true);
        }
        if (by.contains(14)) {
            ad.a(parcel, 14, edVar.getBirthDate(), true);
        }
        if (by.contains(15)) {
            ad.a(parcel, 15, (Parcelable) edVar.bF(), i, true);
        }
        if (by.contains(17)) {
            ad.a(parcel, 17, edVar.getContentSize(), true);
        }
        if (by.contains(16)) {
            ad.a(parcel, 16, edVar.getCaption(), true);
        }
        if (by.contains(19)) {
            ad.b(parcel, 19, edVar.bG(), true);
        }
        if (by.contains(18)) {
            ad.a(parcel, 18, edVar.getContentUrl(), true);
        }
        if (by.contains(21)) {
            ad.a(parcel, 21, edVar.getDateModified(), true);
        }
        if (by.contains(20)) {
            ad.a(parcel, 20, edVar.getDateCreated(), true);
        }
        if (by.contains(23)) {
            ad.a(parcel, 23, edVar.getDescription(), true);
        }
        if (by.contains(22)) {
            ad.a(parcel, 22, edVar.getDatePublished(), true);
        }
        if (by.contains(25)) {
            ad.a(parcel, 25, edVar.getEmbedUrl(), true);
        }
        if (by.contains(24)) {
            ad.a(parcel, 24, edVar.getDuration(), true);
        }
        if (by.contains(27)) {
            ad.a(parcel, 27, edVar.getFamilyName(), true);
        }
        if (by.contains(26)) {
            ad.a(parcel, 26, edVar.getEndDate(), true);
        }
        if (by.contains(29)) {
            ad.a(parcel, 29, (Parcelable) edVar.bH(), i, true);
        }
        if (by.contains(28)) {
            ad.a(parcel, 28, edVar.getGender(), true);
        }
        if (by.contains(31)) {
            ad.a(parcel, 31, edVar.getHeight(), true);
        }
        if (by.contains(30)) {
            ad.a(parcel, 30, edVar.getGivenName(), true);
        }
        if (by.contains(34)) {
            ad.a(parcel, 34, (Parcelable) edVar.bI(), i, true);
        }
        if (by.contains(32)) {
            ad.a(parcel, 32, edVar.getId(), true);
        }
        if (by.contains(33)) {
            ad.a(parcel, 33, edVar.getImage(), true);
        }
        if (by.contains(38)) {
            ad.a(parcel, 38, edVar.getLongitude());
        }
        if (by.contains(39)) {
            ad.a(parcel, 39, edVar.getName(), true);
        }
        if (by.contains(36)) {
            ad.a(parcel, 36, edVar.getLatitude());
        }
        if (by.contains(37)) {
            ad.a(parcel, 37, (Parcelable) edVar.bJ(), i, true);
        }
        if (by.contains(42)) {
            ad.a(parcel, 42, edVar.getPlayerType(), true);
        }
        if (by.contains(43)) {
            ad.a(parcel, 43, edVar.getPostOfficeBoxNumber(), true);
        }
        if (by.contains(40)) {
            ad.a(parcel, 40, (Parcelable) edVar.bK(), i, true);
        }
        if (by.contains(41)) {
            ad.b(parcel, 41, edVar.bL(), true);
        }
        if (by.contains(46)) {
            ad.a(parcel, 46, (Parcelable) edVar.bM(), i, true);
        }
        if (by.contains(47)) {
            ad.a(parcel, 47, edVar.getStartDate(), true);
        }
        if (by.contains(44)) {
            ad.a(parcel, 44, edVar.getPostalCode(), true);
        }
        if (by.contains(45)) {
            ad.a(parcel, 45, edVar.getRatingValue(), true);
        }
        if (by.contains(51)) {
            ad.a(parcel, 51, edVar.getThumbnailUrl(), true);
        }
        if (by.contains(50)) {
            ad.a(parcel, 50, (Parcelable) edVar.bN(), i, true);
        }
        if (by.contains(49)) {
            ad.a(parcel, 49, edVar.getText(), true);
        }
        if (by.contains(48)) {
            ad.a(parcel, 48, edVar.getStreetAddress(), true);
        }
        if (by.contains(55)) {
            ad.a(parcel, 55, edVar.getWidth(), true);
        }
        if (by.contains(54)) {
            ad.a(parcel, 54, edVar.getUrl(), true);
        }
        if (by.contains(53)) {
            ad.a(parcel, 53, edVar.getType(), true);
        }
        if (by.contains(52)) {
            ad.a(parcel, 52, edVar.getTickerSymbol(), true);
        }
        if (by.contains(56)) {
            ad.a(parcel, 56, edVar.getWorstRating(), true);
        }
        ad.C(parcel, d);
    }

    /* renamed from: P */
    public ed[] newArray(int i) {
        return new ed[i];
    }

    /* renamed from: v */
    public ed createFromParcel(Parcel parcel) {
        int c = ac.c(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        ed edVar = null;
        ArrayList<String> arrayList = null;
        ed edVar2 = null;
        String str = null;
        String str2 = null;
        String str3 = null;
        ArrayList arrayList2 = null;
        int i2 = 0;
        ArrayList arrayList3 = null;
        ed edVar3 = null;
        ArrayList arrayList4 = null;
        String str4 = null;
        String str5 = null;
        ed edVar4 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        ArrayList arrayList5 = null;
        String str9 = null;
        String str10 = null;
        String str11 = null;
        String str12 = null;
        String str13 = null;
        String str14 = null;
        String str15 = null;
        String str16 = null;
        String str17 = null;
        ed edVar5 = null;
        String str18 = null;
        String str19 = null;
        String str20 = null;
        String str21 = null;
        ed edVar6 = null;
        double d = 0.0d;
        ed edVar7 = null;
        double d2 = 0.0d;
        String str22 = null;
        ed edVar8 = null;
        ArrayList arrayList6 = null;
        String str23 = null;
        String str24 = null;
        String str25 = null;
        String str26 = null;
        ed edVar9 = null;
        String str27 = null;
        String str28 = null;
        String str29 = null;
        ed edVar10 = null;
        String str30 = null;
        String str31 = null;
        String str32 = null;
        String str33 = null;
        String str34 = null;
        String str35 = null;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i = ac.f(parcel, b);
                    hashSet.add(1);
                    break;
                case 2:
                    hashSet.add(2);
                    edVar = (ed) ac.a(parcel, b, ed.CREATOR);
                    break;
                case 3:
                    arrayList = ac.x(parcel, b);
                    hashSet.add(3);
                    break;
                case 4:
                    hashSet.add(4);
                    edVar2 = (ed) ac.a(parcel, b, ed.CREATOR);
                    break;
                case 5:
                    str = ac.l(parcel, b);
                    hashSet.add(5);
                    break;
                case 6:
                    str2 = ac.l(parcel, b);
                    hashSet.add(6);
                    break;
                case 7:
                    str3 = ac.l(parcel, b);
                    hashSet.add(7);
                    break;
                case 8:
                    arrayList2 = ac.c(parcel, b, ed.CREATOR);
                    hashSet.add(8);
                    break;
                case 9:
                    i2 = ac.f(parcel, b);
                    hashSet.add(9);
                    break;
                case 10:
                    arrayList3 = ac.c(parcel, b, ed.CREATOR);
                    hashSet.add(10);
                    break;
                case 11:
                    hashSet.add(11);
                    edVar3 = (ed) ac.a(parcel, b, ed.CREATOR);
                    break;
                case 12:
                    arrayList4 = ac.c(parcel, b, ed.CREATOR);
                    hashSet.add(12);
                    break;
                case 13:
                    str4 = ac.l(parcel, b);
                    hashSet.add(13);
                    break;
                case 14:
                    str5 = ac.l(parcel, b);
                    hashSet.add(14);
                    break;
                case 15:
                    hashSet.add(15);
                    edVar4 = (ed) ac.a(parcel, b, ed.CREATOR);
                    break;
                case 16:
                    str6 = ac.l(parcel, b);
                    hashSet.add(16);
                    break;
                case 17:
                    str7 = ac.l(parcel, b);
                    hashSet.add(17);
                    break;
                case 18:
                    str8 = ac.l(parcel, b);
                    hashSet.add(18);
                    break;
                case TimeUtils.HUNDRED_DAY_FIELD_LEN:
                    arrayList5 = ac.c(parcel, b, ed.CREATOR);
                    hashSet.add(19);
                    break;
                case 20:
                    str9 = ac.l(parcel, b);
                    hashSet.add(20);
                    break;
                case 21:
                    str10 = ac.l(parcel, b);
                    hashSet.add(21);
                    break;
                case 22:
                    str11 = ac.l(parcel, b);
                    hashSet.add(22);
                    break;
                case 23:
                    str12 = ac.l(parcel, b);
                    hashSet.add(23);
                    break;
                case 24:
                    str13 = ac.l(parcel, b);
                    hashSet.add(24);
                    break;
                case 25:
                    str14 = ac.l(parcel, b);
                    hashSet.add(25);
                    break;
                case 26:
                    str15 = ac.l(parcel, b);
                    hashSet.add(26);
                    break;
                case 27:
                    str16 = ac.l(parcel, b);
                    hashSet.add(27);
                    break;
                case 28:
                    str17 = ac.l(parcel, b);
                    hashSet.add(28);
                    break;
                case 29:
                    hashSet.add(29);
                    edVar5 = (ed) ac.a(parcel, b, ed.CREATOR);
                    break;
                case 30:
                    str18 = ac.l(parcel, b);
                    hashSet.add(30);
                    break;
                case 31:
                    str19 = ac.l(parcel, b);
                    hashSet.add(31);
                    break;
                case AccessibilityNodeInfoCompat.ACTION_LONG_CLICK:
                    str20 = ac.l(parcel, b);
                    hashSet.add(32);
                    break;
                case 33:
                    str21 = ac.l(parcel, b);
                    hashSet.add(33);
                    break;
                case 34:
                    hashSet.add(34);
                    edVar6 = (ed) ac.a(parcel, b, ed.CREATOR);
                    break;
                case 35:
                default:
                    ac.b(parcel, b);
                    break;
                case 36:
                    d = ac.j(parcel, b);
                    hashSet.add(36);
                    break;
                case 37:
                    hashSet.add(37);
                    edVar7 = (ed) ac.a(parcel, b, ed.CREATOR);
                    break;
                case 38:
                    d2 = ac.j(parcel, b);
                    hashSet.add(38);
                    break;
                case 39:
                    str22 = ac.l(parcel, b);
                    hashSet.add(39);
                    break;
                case 40:
                    hashSet.add(40);
                    edVar8 = (ed) ac.a(parcel, b, ed.CREATOR);
                    break;
                case 41:
                    arrayList6 = ac.c(parcel, b, ed.CREATOR);
                    hashSet.add(41);
                    break;
                case 42:
                    str23 = ac.l(parcel, b);
                    hashSet.add(42);
                    break;
                case 43:
                    str24 = ac.l(parcel, b);
                    hashSet.add(43);
                    break;
                case 44:
                    str25 = ac.l(parcel, b);
                    hashSet.add(44);
                    break;
                case 45:
                    str26 = ac.l(parcel, b);
                    hashSet.add(45);
                    break;
                case 46:
                    hashSet.add(46);
                    edVar9 = (ed) ac.a(parcel, b, ed.CREATOR);
                    break;
                case 47:
                    str27 = ac.l(parcel, b);
                    hashSet.add(47);
                    break;
                case 48:
                    str28 = ac.l(parcel, b);
                    hashSet.add(48);
                    break;
                case 49:
                    str29 = ac.l(parcel, b);
                    hashSet.add(49);
                    break;
                case 50:
                    hashSet.add(50);
                    edVar10 = (ed) ac.a(parcel, b, ed.CREATOR);
                    break;
                case 51:
                    str30 = ac.l(parcel, b);
                    hashSet.add(51);
                    break;
                case 52:
                    str31 = ac.l(parcel, b);
                    hashSet.add(52);
                    break;
                case 53:
                    str32 = ac.l(parcel, b);
                    hashSet.add(53);
                    break;
                case 54:
                    str33 = ac.l(parcel, b);
                    hashSet.add(54);
                    break;
                case 55:
                    str34 = ac.l(parcel, b);
                    hashSet.add(55);
                    break;
                case 56:
                    str35 = ac.l(parcel, b);
                    hashSet.add(56);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new ed(hashSet, i, edVar, arrayList, edVar2, str, str2, str3, arrayList2, i2, arrayList3, edVar3, arrayList4, str4, str5, edVar4, str6, str7, str8, arrayList5, str9, str10, str11, str12, str13, str14, str15, str16, str17, edVar5, str18, str19, str20, str21, edVar6, d, edVar7, d2, str22, edVar8, arrayList6, str23, str24, str25, str26, edVar9, str27, str28, str29, edVar10, str30, str31, str32, str33, str34, str35);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }
}
