package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.internal.an;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public final class ak implements ae, an.b<String, Integer> {
    public static final al CREATOR = new al();
    private final int T;
    private final HashMap<String, Integer> bp;
    private final HashMap<Integer, String> bq;
    private final ArrayList<a> br;

    public static final class a implements ae {
        public static final am CREATOR = new am();
        final String bs;
        final int bt;
        final int versionCode;

        a(int i, String str, int i2) {
            this.versionCode = i;
            this.bs = str;
            this.bt = i2;
        }

        a(String str, int i) {
            this.versionCode = 1;
            this.bs = str;
            this.bt = i;
        }

        public int describeContents() {
            am amVar = CREATOR;
            return 0;
        }

        public void writeToParcel(Parcel out, int flags) {
            am amVar = CREATOR;
            am.a(this, out, flags);
        }
    }

    public ak() {
        this.T = 1;
        this.bp = new HashMap<>();
        this.bq = new HashMap<>();
        this.br = null;
    }

    ak(int i, ArrayList<a> arrayList) {
        this.T = i;
        this.bp = new HashMap<>();
        this.bq = new HashMap<>();
        this.br = null;
        a(arrayList);
    }

    private void a(ArrayList<a> arrayList) {
        Iterator<a> it = arrayList.iterator();
        while (it.hasNext()) {
            a next = it.next();
            b(next.bs, next.bt);
        }
    }

    /* access modifiers changed from: package-private */
    public ArrayList<a> D() {
        ArrayList<a> arrayList = new ArrayList<>();
        for (String next : this.bp.keySet()) {
            arrayList.add(new a(next, this.bp.get(next).intValue()));
        }
        return arrayList;
    }

    public int E() {
        return 7;
    }

    public int F() {
        return 0;
    }

    /* renamed from: a */
    public String e(Integer num) {
        return this.bq.get(num);
    }

    public ak b(String str, int i) {
        this.bp.put(str, Integer.valueOf(i));
        this.bq.put(Integer.valueOf(i), str);
        return this;
    }

    public int describeContents() {
        al alVar = CREATOR;
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int u() {
        return this.T;
    }

    public void writeToParcel(Parcel out, int flags) {
        al alVar = CREATOR;
        al.a(this, out, flags);
    }
}
