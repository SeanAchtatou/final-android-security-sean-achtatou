package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import com.google.android.gms.internal.p;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public final class q implements Handler.Callback {
    private static q aI;
    private final Context aJ;
    /* access modifiers changed from: private */
    public final HashMap<String, a> aK = new HashMap<>();
    private final Handler mHandler;

    final class a {
        private final String aL;
        private final C0034a aM = new C0034a();
        /* access modifiers changed from: private */
        public final HashSet<p<?>.e> aN = new HashSet<>();
        private boolean aO;
        /* access modifiers changed from: private */
        public IBinder aP;
        /* access modifiers changed from: private */
        public ComponentName aQ;
        /* access modifiers changed from: private */
        public int mState = 0;

        /* renamed from: com.google.android.gms.internal.q$a$a  reason: collision with other inner class name */
        public class C0034a implements ServiceConnection {
            public C0034a() {
            }

            public void onServiceConnected(ComponentName component, IBinder binder) {
                synchronized (q.this.aK) {
                    IBinder unused = a.this.aP = binder;
                    ComponentName unused2 = a.this.aQ = component;
                    Iterator it = a.this.aN.iterator();
                    while (it.hasNext()) {
                        ((p.e) it.next()).onServiceConnected(component, binder);
                    }
                    int unused3 = a.this.mState = 1;
                }
            }

            public void onServiceDisconnected(ComponentName component) {
                synchronized (q.this.aK) {
                    IBinder unused = a.this.aP = (IBinder) null;
                    ComponentName unused2 = a.this.aQ = component;
                    Iterator it = a.this.aN.iterator();
                    while (it.hasNext()) {
                        ((p.e) it.next()).onServiceDisconnected(component);
                    }
                    int unused3 = a.this.mState = 2;
                }
            }
        }

        public a(String str) {
            this.aL = str;
        }

        public void a(p<?>.e eVar) {
            this.aN.add(eVar);
        }

        public void b(p<?>.e eVar) {
            this.aN.remove(eVar);
        }

        public void b(boolean z) {
            this.aO = z;
        }

        public boolean c(p<?>.e eVar) {
            return this.aN.contains(eVar);
        }

        public IBinder getBinder() {
            return this.aP;
        }

        public ComponentName getComponentName() {
            return this.aQ;
        }

        public int getState() {
            return this.mState;
        }

        public boolean isBound() {
            return this.aO;
        }

        public C0034a r() {
            return this.aM;
        }

        public String s() {
            return this.aL;
        }

        public boolean t() {
            return this.aN.isEmpty();
        }
    }

    private q(Context context) {
        this.mHandler = new Handler(context.getMainLooper(), this);
        this.aJ = context.getApplicationContext();
    }

    public static q e(Context context) {
        if (aI == null) {
            aI = new q(context.getApplicationContext());
        }
        return aI;
    }

    public boolean a(String str, p<?>.e eVar) {
        boolean isBound;
        synchronized (this.aK) {
            a aVar = this.aK.get(str);
            if (aVar != null) {
                this.mHandler.removeMessages(0, aVar);
                if (!aVar.c(eVar)) {
                    aVar.a(eVar);
                    switch (aVar.getState()) {
                        case 1:
                            eVar.onServiceConnected(aVar.getComponentName(), aVar.getBinder());
                            break;
                        case 2:
                            aVar.b(this.aJ.bindService(new Intent(str), aVar.r(), 129));
                            break;
                    }
                } else {
                    throw new IllegalStateException("Trying to bind a GmsServiceConnection that was already connected before.  startServiceAction=" + str);
                }
            } else {
                aVar = new a(str);
                aVar.a(eVar);
                aVar.b(this.aJ.bindService(new Intent(str), aVar.r(), 129));
                this.aK.put(str, aVar);
            }
            isBound = aVar.isBound();
        }
        return isBound;
    }

    public void b(String str, p<?>.e eVar) {
        synchronized (this.aK) {
            a aVar = this.aK.get(str);
            if (aVar == null) {
                throw new IllegalStateException("Nonexistent connection status for service action: " + str);
            } else if (!aVar.c(eVar)) {
                throw new IllegalStateException("Trying to unbind a GmsServiceConnection  that was not bound before.  startServiceAction=" + str);
            } else {
                aVar.b(eVar);
                if (aVar.t()) {
                    this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(0, aVar), 5000);
                }
            }
        }
    }

    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case 0:
                a aVar = (a) msg.obj;
                synchronized (this.aK) {
                    if (aVar.t()) {
                        this.aJ.unbindService(aVar.r());
                        this.aK.remove(aVar.s());
                    }
                }
                return true;
            default:
                return false;
        }
    }
}
