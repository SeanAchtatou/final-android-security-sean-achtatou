package com.google.android.gms.internal;

import android.os.Parcel;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.InvitationEntity;
import com.google.android.gms.games.multiplayer.Participant;
import java.util.ArrayList;

public final class bw extends j implements Invitation {
    private final ArrayList<Participant> dJ;
    private final Game dK;
    private final bx dL;

    public bw(k kVar, int i, int i2) {
        super(kVar, i);
        this.dK = new bf(kVar, i);
        this.dJ = new ArrayList<>(i2);
        String string = getString("external_inviter_id");
        bx bxVar = null;
        for (int i3 = 0; i3 < i2; i3++) {
            bx bxVar2 = new bx(this.O, this.R + i3);
            if (bxVar2.getParticipantId().equals(string)) {
                bxVar = bxVar2;
            }
            this.dJ.add(bxVar2);
        }
        this.dL = (bx) x.b(bxVar, "Must have a valid inviter!");
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return InvitationEntity.a(this, obj);
    }

    public Invitation freeze() {
        return new InvitationEntity(this);
    }

    public long getCreationTimestamp() {
        return getLong("creation_timestamp");
    }

    public Game getGame() {
        return this.dK;
    }

    public String getInvitationId() {
        return getString("external_invitation_id");
    }

    public int getInvitationType() {
        return getInteger(ServerProtocol.DIALOG_PARAM_TYPE);
    }

    public Participant getInviter() {
        return this.dL;
    }

    public ArrayList<Participant> getParticipants() {
        return this.dJ;
    }

    public int hashCode() {
        return InvitationEntity.a(this);
    }

    public String toString() {
        return InvitationEntity.b(this);
    }

    public void writeToParcel(Parcel dest, int flags) {
        ((InvitationEntity) freeze()).writeToParcel(dest, flags);
    }
}
