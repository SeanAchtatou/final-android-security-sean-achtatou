package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.eq;
import java.util.HashSet;
import java.util.Set;

public class ej implements Parcelable.Creator<eq.b> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.eq$b$a, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.eq$b$b, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(eq.b bVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        Set<Integer> by = bVar.by();
        if (by.contains(1)) {
            ad.c(parcel, 1, bVar.u());
        }
        if (by.contains(2)) {
            ad.a(parcel, 2, (Parcelable) bVar.ce(), i, true);
        }
        if (by.contains(3)) {
            ad.a(parcel, 3, (Parcelable) bVar.cf(), i, true);
        }
        if (by.contains(4)) {
            ad.c(parcel, 4, bVar.getLayout());
        }
        ad.C(parcel, d);
    }

    /* renamed from: S */
    public eq.b[] newArray(int i) {
        return new eq.b[i];
    }

    /* renamed from: y */
    public eq.b createFromParcel(Parcel parcel) {
        eq.b.C0031b bVar = null;
        int i = 0;
        int c = ac.c(parcel);
        HashSet hashSet = new HashSet();
        eq.b.a aVar = null;
        int i2 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i2 = ac.f(parcel, b);
                    hashSet.add(1);
                    break;
                case 2:
                    hashSet.add(2);
                    aVar = (eq.b.a) ac.a(parcel, b, eq.b.a.CREATOR);
                    break;
                case 3:
                    hashSet.add(3);
                    bVar = (eq.b.C0031b) ac.a(parcel, b, eq.b.C0031b.CREATOR);
                    break;
                case 4:
                    i = ac.f(parcel, b);
                    hashSet.add(4);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new eq.b(hashSet, i2, aVar, bVar, i);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }
}
