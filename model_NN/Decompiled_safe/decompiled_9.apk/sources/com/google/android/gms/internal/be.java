package com.google.android.gms.internal;

import com.google.android.gms.dynamic.LifecycleDelegate;

public interface be<T extends LifecycleDelegate> {
    void a(T t);
}
