package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.an;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class at extends an implements ae {
    public static final au CREATOR = new au();
    private final int T;
    private final aq bC;
    private final Parcel bK;
    private final int bL;
    private int bM;
    private int bN;
    private final String mClassName;

    at(int i, Parcel parcel, aq aqVar) {
        this.T = i;
        this.bK = (Parcel) x.d(parcel);
        this.bL = 2;
        this.bC = aqVar;
        if (this.bC == null) {
            this.mClassName = null;
        } else {
            this.mClassName = this.bC.W();
        }
        this.bM = 2;
    }

    private at(ae aeVar, aq aqVar, String str) {
        this.T = 1;
        this.bK = Parcel.obtain();
        aeVar.writeToParcel(this.bK, 0);
        this.bL = 1;
        this.bC = (aq) x.d(aqVar);
        this.mClassName = (String) x.d(str);
        this.bM = 2;
    }

    public static <T extends an & ae> at a(T t) {
        String canonicalName = t.getClass().getCanonicalName();
        return new at((ae) t, b((an) t), canonicalName);
    }

    public static HashMap<String, String> a(Bundle bundle) {
        HashMap<String, String> hashMap = new HashMap<>();
        for (String next : bundle.keySet()) {
            hashMap.put(next, bundle.getString(next));
        }
        return hashMap;
    }

    private static void a(aq aqVar, an anVar) {
        Class<?> cls = anVar.getClass();
        if (!aqVar.a(cls)) {
            HashMap<String, an.a<?, ?>> G = anVar.G();
            aqVar.a(cls, anVar.G());
            for (String str : G.keySet()) {
                an.a aVar = G.get(str);
                Class<? extends an> O = aVar.O();
                if (O != null) {
                    try {
                        a(aqVar, (an) O.newInstance());
                    } catch (InstantiationException e) {
                        throw new IllegalStateException("Could not instantiate an object of type " + aVar.O().getCanonicalName(), e);
                    } catch (IllegalAccessException e2) {
                        throw new IllegalStateException("Could not access object of type " + aVar.O().getCanonicalName(), e2);
                    }
                }
            }
        }
    }

    private void a(StringBuilder sb, int i, Object obj) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                sb.append(obj);
                return;
            case 7:
                sb.append("\"").append(ay.o(obj.toString())).append("\"");
                return;
            case 8:
                sb.append("\"").append(aw.a((byte[]) obj)).append("\"");
                return;
            case 9:
                sb.append("\"").append(aw.b((byte[]) obj));
                sb.append("\"");
                return;
            case 10:
                az.a(sb, (HashMap) obj);
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                throw new IllegalArgumentException("Unknown type = " + i);
        }
    }

    private void a(StringBuilder sb, an.a<?, ?> aVar, Parcel parcel, int i) {
        switch (aVar.F()) {
            case 0:
                b(sb, aVar, a(aVar, Integer.valueOf(ac.f(parcel, i))));
                return;
            case 1:
                b(sb, aVar, a(aVar, ac.h(parcel, i)));
                return;
            case 2:
                b(sb, aVar, a(aVar, Long.valueOf(ac.g(parcel, i))));
                return;
            case 3:
                b(sb, aVar, a(aVar, Float.valueOf(ac.i(parcel, i))));
                return;
            case 4:
                b(sb, aVar, a(aVar, Double.valueOf(ac.j(parcel, i))));
                return;
            case 5:
                b(sb, aVar, a(aVar, ac.k(parcel, i)));
                return;
            case 6:
                b(sb, aVar, a(aVar, Boolean.valueOf(ac.c(parcel, i))));
                return;
            case 7:
                b(sb, aVar, a(aVar, ac.l(parcel, i)));
                return;
            case 8:
            case 9:
                b(sb, aVar, a(aVar, ac.o(parcel, i)));
                return;
            case 10:
                b(sb, aVar, a(aVar, a(ac.n(parcel, i))));
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                throw new IllegalArgumentException("Unknown field out type = " + aVar.F());
        }
    }

    private void a(StringBuilder sb, String str, an.a<?, ?> aVar, Parcel parcel, int i) {
        sb.append("\"").append(str).append("\":");
        if (aVar.Q()) {
            a(sb, aVar, parcel, i);
        } else {
            b(sb, aVar, parcel, i);
        }
    }

    private void a(StringBuilder sb, HashMap<String, an.a<?, ?>> hashMap, Parcel parcel) {
        HashMap<Integer, Map.Entry<String, an.a<?, ?>>> b = b(hashMap);
        sb.append('{');
        int c = ac.c(parcel);
        boolean z = false;
        while (parcel.dataPosition() < c) {
            int b2 = ac.b(parcel);
            Map.Entry entry = b.get(Integer.valueOf(ac.j(b2)));
            if (entry != null) {
                if (z) {
                    sb.append(",");
                }
                a(sb, (String) entry.getKey(), (an.a) entry.getValue(), parcel, b2);
                z = true;
            }
        }
        if (parcel.dataPosition() != c) {
            throw new ac.a("Overread allowed size end=" + c, parcel);
        }
        sb.append('}');
    }

    private static aq b(an anVar) {
        aq aqVar = new aq(anVar.getClass());
        a(aqVar, anVar);
        aqVar.U();
        aqVar.T();
        return aqVar;
    }

    private static HashMap<Integer, Map.Entry<String, an.a<?, ?>>> b(HashMap<String, an.a<?, ?>> hashMap) {
        HashMap<Integer, Map.Entry<String, an.a<?, ?>>> hashMap2 = new HashMap<>();
        for (Map.Entry next : hashMap.entrySet()) {
            hashMap2.put(Integer.valueOf(((an.a) next.getValue()).N()), next);
        }
        return hashMap2;
    }

    private void b(StringBuilder sb, an.a<?, ?> aVar, Parcel parcel, int i) {
        if (aVar.L()) {
            sb.append("[");
            switch (aVar.F()) {
                case 0:
                    av.a(sb, ac.q(parcel, i));
                    break;
                case 1:
                    av.a(sb, ac.s(parcel, i));
                    break;
                case 2:
                    av.a(sb, ac.r(parcel, i));
                    break;
                case 3:
                    av.a(sb, ac.t(parcel, i));
                    break;
                case 4:
                    av.a(sb, ac.u(parcel, i));
                    break;
                case 5:
                    av.a(sb, ac.v(parcel, i));
                    break;
                case 6:
                    av.a(sb, ac.p(parcel, i));
                    break;
                case 7:
                    av.a(sb, ac.w(parcel, i));
                    break;
                case 8:
                case 9:
                case 10:
                    throw new UnsupportedOperationException("List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported");
                case 11:
                    Parcel[] z = ac.z(parcel, i);
                    int length = z.length;
                    for (int i2 = 0; i2 < length; i2++) {
                        if (i2 > 0) {
                            sb.append(",");
                        }
                        z[i2].setDataPosition(0);
                        a(sb, aVar.S(), z[i2]);
                    }
                    break;
                default:
                    throw new IllegalStateException("Unknown field type out.");
            }
            sb.append("]");
            return;
        }
        switch (aVar.F()) {
            case 0:
                sb.append(ac.f(parcel, i));
                return;
            case 1:
                sb.append(ac.h(parcel, i));
                return;
            case 2:
                sb.append(ac.g(parcel, i));
                return;
            case 3:
                sb.append(ac.i(parcel, i));
                return;
            case 4:
                sb.append(ac.j(parcel, i));
                return;
            case 5:
                sb.append(ac.k(parcel, i));
                return;
            case 6:
                sb.append(ac.c(parcel, i));
                return;
            case 7:
                sb.append("\"").append(ay.o(ac.l(parcel, i))).append("\"");
                return;
            case 8:
                sb.append("\"").append(aw.a(ac.o(parcel, i))).append("\"");
                return;
            case 9:
                sb.append("\"").append(aw.b(ac.o(parcel, i)));
                sb.append("\"");
                return;
            case 10:
                Bundle n = ac.n(parcel, i);
                Set<String> keySet = n.keySet();
                keySet.size();
                sb.append("{");
                boolean z2 = true;
                for (String next : keySet) {
                    if (!z2) {
                        sb.append(",");
                    }
                    sb.append("\"").append(next).append("\"");
                    sb.append(":");
                    sb.append("\"").append(ay.o(n.getString(next))).append("\"");
                    z2 = false;
                }
                sb.append("}");
                return;
            case 11:
                Parcel y = ac.y(parcel, i);
                y.setDataPosition(0);
                a(sb, aVar.S(), y);
                return;
            default:
                throw new IllegalStateException("Unknown field type out");
        }
    }

    private void b(StringBuilder sb, an.a<?, ?> aVar, Object obj) {
        if (aVar.K()) {
            b(sb, aVar, (ArrayList<?>) ((ArrayList) obj));
        } else {
            a(sb, aVar.E(), obj);
        }
    }

    private void b(StringBuilder sb, an.a<?, ?> aVar, ArrayList<?> arrayList) {
        sb.append("[");
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                sb.append(",");
            }
            a(sb, aVar.E(), arrayList.get(i));
        }
        sb.append("]");
    }

    public HashMap<String, an.a<?, ?>> G() {
        if (this.bC == null) {
            return null;
        }
        return this.bC.n(this.mClassName);
    }

    public Parcel Y() {
        switch (this.bM) {
            case 0:
                this.bN = ad.d(this.bK);
                ad.C(this.bK, this.bN);
                this.bM = 2;
                break;
            case 1:
                ad.C(this.bK, this.bN);
                this.bM = 2;
                break;
        }
        return this.bK;
    }

    /* access modifiers changed from: package-private */
    public aq Z() {
        switch (this.bL) {
            case 0:
                return null;
            case 1:
                return this.bC;
            case 2:
                return this.bC;
            default:
                throw new IllegalStateException("Invalid creation type: " + this.bL);
        }
    }

    public <T extends ae> T a(Parcelable.Creator<T> creator) {
        Parcel Y = Y();
        Y.setDataPosition(0);
        return (ae) creator.createFromParcel(Y);
    }

    public int describeContents() {
        au auVar = CREATOR;
        return 0;
    }

    /* access modifiers changed from: protected */
    public Object j(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    /* access modifiers changed from: protected */
    public boolean k(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    public String toString() {
        x.b(this.bC, "Cannot convert to JSON on client side.");
        Parcel Y = Y();
        Y.setDataPosition(0);
        StringBuilder sb = new StringBuilder(100);
        a(sb, this.bC.n(this.mClassName), Y);
        return sb.toString();
    }

    public int u() {
        return this.T;
    }

    public void writeToParcel(Parcel out, int flags) {
        au auVar = CREATOR;
        au.a(this, out, flags);
    }
}
