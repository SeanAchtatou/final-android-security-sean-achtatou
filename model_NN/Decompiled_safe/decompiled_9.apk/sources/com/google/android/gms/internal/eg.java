package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import java.util.HashSet;
import java.util.Set;

public class eg implements Parcelable.Creator<ef> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.ed, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(ef efVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        Set<Integer> by = efVar.by();
        if (by.contains(1)) {
            ad.c(parcel, 1, efVar.u());
        }
        if (by.contains(2)) {
            ad.a(parcel, 2, efVar.getId(), true);
        }
        if (by.contains(4)) {
            ad.a(parcel, 4, (Parcelable) efVar.bP(), i, true);
        }
        if (by.contains(5)) {
            ad.a(parcel, 5, efVar.getStartDate(), true);
        }
        if (by.contains(6)) {
            ad.a(parcel, 6, (Parcelable) efVar.bQ(), i, true);
        }
        if (by.contains(7)) {
            ad.a(parcel, 7, efVar.getType(), true);
        }
        ad.C(parcel, d);
    }

    /* renamed from: Q */
    public ef[] newArray(int i) {
        return new ef[i];
    }

    /* renamed from: w */
    public ef createFromParcel(Parcel parcel) {
        String str = null;
        int c = ac.c(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        ed edVar = null;
        String str2 = null;
        ed edVar2 = null;
        String str3 = null;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i = ac.f(parcel, b);
                    hashSet.add(1);
                    break;
                case 2:
                    str3 = ac.l(parcel, b);
                    hashSet.add(2);
                    break;
                case 3:
                default:
                    ac.b(parcel, b);
                    break;
                case 4:
                    hashSet.add(4);
                    edVar2 = (ed) ac.a(parcel, b, ed.CREATOR);
                    break;
                case 5:
                    str2 = ac.l(parcel, b);
                    hashSet.add(5);
                    break;
                case 6:
                    hashSet.add(6);
                    edVar = (ed) ac.a(parcel, b, ed.CREATOR);
                    break;
                case 7:
                    str = ac.l(parcel, b);
                    hashSet.add(7);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new ef(hashSet, i, str3, edVar2, str2, edVar, str);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }
}
