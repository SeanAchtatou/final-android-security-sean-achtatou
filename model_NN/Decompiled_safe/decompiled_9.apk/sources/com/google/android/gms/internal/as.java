package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.aq;
import java.util.ArrayList;

public class as implements Parcelable.Creator<aq.a> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(aq.a aVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, aVar.versionCode);
        ad.a(parcel, 2, aVar.className, false);
        ad.b(parcel, 3, aVar.bH, false);
        ad.C(parcel, d);
    }

    /* renamed from: l */
    public aq.a createFromParcel(Parcel parcel) {
        ArrayList arrayList = null;
        int c = ac.c(parcel);
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i = ac.f(parcel, b);
                    break;
                case 2:
                    str = ac.l(parcel, b);
                    break;
                case 3:
                    arrayList = ac.c(parcel, b, aq.b.CREATOR);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new aq.a(i, str, arrayList);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    /* renamed from: r */
    public aq.a[] newArray(int i) {
        return new aq.a[i];
    }
}
