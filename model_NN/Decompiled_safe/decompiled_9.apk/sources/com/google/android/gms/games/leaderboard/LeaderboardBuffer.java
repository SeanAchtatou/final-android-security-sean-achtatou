package com.google.android.gms.games.leaderboard;

import com.google.android.gms.internal.br;
import com.google.android.gms.internal.k;
import com.google.android.gms.internal.m;

public final class LeaderboardBuffer extends m<Leaderboard> {
    public LeaderboardBuffer(k dataHolder) {
        super(dataHolder);
    }

    /* access modifiers changed from: protected */
    /* renamed from: getEntry */
    public Leaderboard a(int rowIndex, int numChildren) {
        return new br(this.O, rowIndex, numChildren);
    }

    /* access modifiers changed from: protected */
    public String getPrimaryDataMarkerColumn() {
        return "external_leaderboard_id";
    }
}
