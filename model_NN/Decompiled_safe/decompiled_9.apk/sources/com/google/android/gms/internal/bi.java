package com.google.android.gms.internal;

import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.internal.bl;

public abstract class bi extends bl.a {
    public void A(int i) {
    }

    public void a(int i, int i2, String str) {
    }

    public void a(int i, String str) {
    }

    public void a(int i, String str, boolean z) {
    }

    public void a(k kVar, k kVar2) {
    }

    public void a(k kVar, String[] strArr) {
    }

    public void b(k kVar) {
    }

    public void b(k kVar, String[] strArr) {
    }

    public void c(k kVar) {
    }

    public void c(k kVar, String[] strArr) {
    }

    public void d(k kVar) {
    }

    public void d(k kVar, String[] strArr) {
    }

    public void e(k kVar) {
    }

    public void e(k kVar, String[] strArr) {
    }

    public void f(k kVar) {
    }

    public void f(k kVar, String[] strArr) {
    }

    public void g(k kVar) {
    }

    public void h(k kVar) {
    }

    public void i(k kVar) {
    }

    public void j(k kVar) {
    }

    public void k(k kVar) {
    }

    public void l(k kVar) {
    }

    public void m(k kVar) {
    }

    public void n(k kVar) {
    }

    public void o(k kVar) {
    }

    public void onAchievementUpdated(int statusCode, String achievementId) {
    }

    public void onLeftRoom(int statusCode, String roomId) {
    }

    public void onRealTimeMessageReceived(RealTimeMessage message) {
    }

    public void onSignOutComplete() {
    }

    public void p(k kVar) {
    }

    public void q(k kVar) {
    }

    public void r(k kVar) {
    }

    public void s(k kVar) {
    }

    public void t(k kVar) {
    }

    public void u(k kVar) {
    }

    public void v(k kVar) {
    }

    public void w(k kVar) {
    }

    public void x(int i) {
    }

    public void y(int i) {
    }

    public void z(int i) {
    }
}
