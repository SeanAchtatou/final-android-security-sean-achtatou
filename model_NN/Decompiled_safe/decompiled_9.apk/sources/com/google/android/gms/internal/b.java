package com.google.android.gms.internal;

import com.google.android.gms.appstate.AppState;

public final class b implements AppState {
    private final int h;
    private final String i;
    private final byte[] j;
    private final boolean k;
    private final String l;
    private final byte[] m;

    public b(AppState appState) {
        this.h = appState.getKey();
        this.i = appState.getLocalVersion();
        this.j = appState.getLocalData();
        this.k = appState.hasConflict();
        this.l = appState.getConflictVersion();
        this.m = appState.getConflictData();
    }

    static int a(AppState appState) {
        return w.hashCode(Integer.valueOf(appState.getKey()), appState.getLocalVersion(), appState.getLocalData(), Boolean.valueOf(appState.hasConflict()), appState.getConflictVersion(), appState.getConflictData());
    }

    static boolean a(AppState appState, Object obj) {
        if (!(obj instanceof AppState)) {
            return false;
        }
        if (appState == obj) {
            return true;
        }
        AppState appState2 = (AppState) obj;
        return w.a(Integer.valueOf(appState2.getKey()), Integer.valueOf(appState.getKey())) && w.a(appState2.getLocalVersion(), appState.getLocalVersion()) && w.a(appState2.getLocalData(), appState.getLocalData()) && w.a(Boolean.valueOf(appState2.hasConflict()), Boolean.valueOf(appState.hasConflict())) && w.a(appState2.getConflictVersion(), appState.getConflictVersion()) && w.a(appState2.getConflictData(), appState.getConflictData());
    }

    static String b(AppState appState) {
        return w.c(appState).a("Key", Integer.valueOf(appState.getKey())).a("LocalVersion", appState.getLocalVersion()).a("LocalData", appState.getLocalData()).a("HasConflict", Boolean.valueOf(appState.hasConflict())).a("ConflictVersion", appState.getConflictVersion()).a("ConflictData", appState.getConflictData()).toString();
    }

    /* renamed from: a */
    public AppState freeze() {
        return this;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public byte[] getConflictData() {
        return this.m;
    }

    public String getConflictVersion() {
        return this.l;
    }

    public int getKey() {
        return this.h;
    }

    public byte[] getLocalData() {
        return this.j;
    }

    public String getLocalVersion() {
        return this.i;
    }

    public boolean hasConflict() {
        return this.k;
    }

    public int hashCode() {
        return a(this);
    }

    public String toString() {
        return b(this);
    }
}
