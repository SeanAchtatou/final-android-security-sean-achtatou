package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ad;

public class GroundOverlayOptionsCreator implements Parcelable.Creator<GroundOverlayOptions> {
    public static final int CONTENT_DESCRIPTION = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
     arg types: [android.os.Parcel, int, android.os.IBinder, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLngBounds, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(GroundOverlayOptions groundOverlayOptions, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, groundOverlayOptions.u());
        ad.a(parcel, 2, groundOverlayOptions.aX(), false);
        ad.a(parcel, 3, (Parcelable) groundOverlayOptions.getLocation(), i, false);
        ad.a(parcel, 4, groundOverlayOptions.getWidth());
        ad.a(parcel, 5, groundOverlayOptions.getHeight());
        ad.a(parcel, 6, (Parcelable) groundOverlayOptions.getBounds(), i, false);
        ad.a(parcel, 7, groundOverlayOptions.getBearing());
        ad.a(parcel, 8, groundOverlayOptions.getZIndex());
        ad.a(parcel, 9, groundOverlayOptions.isVisible());
        ad.a(parcel, 10, groundOverlayOptions.getTransparency());
        ad.a(parcel, 11, groundOverlayOptions.getAnchorU());
        ad.a(parcel, 12, groundOverlayOptions.getAnchorV());
        ad.C(parcel, d);
    }

    public GroundOverlayOptions createFromParcel(Parcel parcel) {
        int c = ac.c(parcel);
        int i = 0;
        IBinder iBinder = null;
        LatLng latLng = null;
        float f = BitmapDescriptorFactory.HUE_RED;
        float f2 = BitmapDescriptorFactory.HUE_RED;
        LatLngBounds latLngBounds = null;
        float f3 = BitmapDescriptorFactory.HUE_RED;
        float f4 = BitmapDescriptorFactory.HUE_RED;
        boolean z = false;
        float f5 = BitmapDescriptorFactory.HUE_RED;
        float f6 = BitmapDescriptorFactory.HUE_RED;
        float f7 = BitmapDescriptorFactory.HUE_RED;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i = ac.f(parcel, b);
                    break;
                case 2:
                    iBinder = ac.m(parcel, b);
                    break;
                case 3:
                    latLng = (LatLng) ac.a(parcel, b, LatLng.CREATOR);
                    break;
                case 4:
                    f = ac.i(parcel, b);
                    break;
                case 5:
                    f2 = ac.i(parcel, b);
                    break;
                case 6:
                    latLngBounds = (LatLngBounds) ac.a(parcel, b, LatLngBounds.CREATOR);
                    break;
                case 7:
                    f3 = ac.i(parcel, b);
                    break;
                case 8:
                    f4 = ac.i(parcel, b);
                    break;
                case 9:
                    z = ac.c(parcel, b);
                    break;
                case 10:
                    f5 = ac.i(parcel, b);
                    break;
                case 11:
                    f6 = ac.i(parcel, b);
                    break;
                case 12:
                    f7 = ac.i(parcel, b);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new GroundOverlayOptions(i, iBinder, latLng, f, f2, latLngBounds, f3, f4, z, f5, f6, f7);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    public GroundOverlayOptions[] newArray(int size) {
        return new GroundOverlayOptions[size];
    }
}
