package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.cx;
import com.google.android.gms.internal.dg;

public final class Tile implements ae {
    public static final TileCreator CREATOR = new TileCreator();
    private final int T;
    public final byte[] data;
    public final int height;
    public final int width;

    Tile(int versionCode, int width2, int height2, byte[] data2) {
        this.T = versionCode;
        this.width = width2;
        this.height = height2;
        this.data = data2;
    }

    public Tile(int width2, int height2, byte[] data2) {
        this(1, width2, height2, data2);
    }

    public int describeContents() {
        return 0;
    }

    public int u() {
        return this.T;
    }

    public void writeToParcel(Parcel out, int flags) {
        if (cx.aV()) {
            dg.a(this, out, flags);
        } else {
            TileCreator.a(this, out, flags);
        }
    }
}
