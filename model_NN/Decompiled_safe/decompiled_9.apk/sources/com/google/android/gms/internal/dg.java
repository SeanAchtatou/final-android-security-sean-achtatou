package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.Tile;

public class dg {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
     arg types: [android.os.Parcel, int, byte[], int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void */
    public static void a(Tile tile, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, tile.u());
        ad.c(parcel, 2, tile.width);
        ad.c(parcel, 3, tile.height);
        ad.a(parcel, 4, tile.data, false);
        ad.C(parcel, d);
    }
}
