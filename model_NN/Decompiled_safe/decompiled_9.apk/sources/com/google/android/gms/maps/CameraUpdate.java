package com.google.android.gms.maps;

import com.google.android.gms.internal.bc;
import com.google.android.gms.internal.x;

public final class CameraUpdate {
    private final bc eR;

    CameraUpdate(bc remoteObject) {
        this.eR = (bc) x.d(remoteObject);
    }

    public bc aD() {
        return this.eR;
    }
}
