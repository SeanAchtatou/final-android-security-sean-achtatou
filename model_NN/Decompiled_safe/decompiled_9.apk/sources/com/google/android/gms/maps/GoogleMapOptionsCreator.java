package com.google.android.gms.maps;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ad;
import com.google.android.gms.maps.model.CameraPosition;

public class GoogleMapOptionsCreator implements Parcelable.Creator<GoogleMapOptions> {
    public static final int CONTENT_DESCRIPTION = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.CameraPosition, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(GoogleMapOptions googleMapOptions, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, googleMapOptions.u());
        ad.a(parcel, 2, googleMapOptions.aG());
        ad.a(parcel, 3, googleMapOptions.aH());
        ad.c(parcel, 4, googleMapOptions.getMapType());
        ad.a(parcel, 5, (Parcelable) googleMapOptions.getCamera(), i, false);
        ad.a(parcel, 6, googleMapOptions.aI());
        ad.a(parcel, 7, googleMapOptions.aJ());
        ad.a(parcel, 8, googleMapOptions.aK());
        ad.a(parcel, 9, googleMapOptions.aL());
        ad.a(parcel, 10, googleMapOptions.aM());
        ad.a(parcel, 11, googleMapOptions.aN());
        ad.C(parcel, d);
    }

    public GoogleMapOptions createFromParcel(Parcel parcel) {
        byte b = 0;
        int c = ac.c(parcel);
        CameraPosition cameraPosition = null;
        byte b2 = 0;
        byte b3 = 0;
        byte b4 = 0;
        byte b5 = 0;
        byte b6 = 0;
        int i = 0;
        byte b7 = 0;
        byte b8 = 0;
        int i2 = 0;
        while (parcel.dataPosition() < c) {
            int b9 = ac.b(parcel);
            switch (ac.j(b9)) {
                case 1:
                    i2 = ac.f(parcel, b9);
                    break;
                case 2:
                    b8 = ac.d(parcel, b9);
                    break;
                case 3:
                    b7 = ac.d(parcel, b9);
                    break;
                case 4:
                    i = ac.f(parcel, b9);
                    break;
                case 5:
                    cameraPosition = (CameraPosition) ac.a(parcel, b9, CameraPosition.CREATOR);
                    break;
                case 6:
                    b6 = ac.d(parcel, b9);
                    break;
                case 7:
                    b5 = ac.d(parcel, b9);
                    break;
                case 8:
                    b4 = ac.d(parcel, b9);
                    break;
                case 9:
                    b3 = ac.d(parcel, b9);
                    break;
                case 10:
                    b2 = ac.d(parcel, b9);
                    break;
                case 11:
                    b = ac.d(parcel, b9);
                    break;
                default:
                    ac.b(parcel, b9);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new GoogleMapOptions(i2, b8, b7, i, cameraPosition, b6, b5, b4, b3, b2, b);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    public GoogleMapOptions[] newArray(int size) {
        return new GoogleMapOptions[size];
    }
}
