package com.google.android.gms.common;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import com.google.android.gms.internal.aa;
import com.google.android.gms.internal.x;
import com.google.android.gms.internal.z;

public final class SignInButton extends FrameLayout implements View.OnClickListener {
    public static final int COLOR_DARK = 0;
    public static final int COLOR_LIGHT = 1;
    public static final int SIZE_ICON_ONLY = 2;
    public static final int SIZE_STANDARD = 0;
    public static final int SIZE_WIDE = 1;
    private int K;
    private int L;
    private View M;
    private View.OnClickListener N;

    public SignInButton(Context context) {
        this(context, null);
    }

    public SignInButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SignInButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.N = null;
        setStyle(0, 0);
    }

    private static Button c(Context context, int i, int i2) {
        aa aaVar = new aa(context);
        aaVar.a(context.getResources(), i, i2);
        return aaVar;
    }

    private void d(Context context) {
        if (this.M != null) {
            removeView(this.M);
        }
        try {
            this.M = z.d(context, this.K, this.L);
        } catch (z.a e) {
            Log.w("SignInButton", "Sign in button not found, using placeholder instead");
            this.M = c(context, this.K, this.L);
        }
        addView(this.M);
        this.M.setEnabled(isEnabled());
        this.M.setOnClickListener(this);
    }

    public void onClick(View view) {
        if (this.N != null && view == this.M) {
            this.N.onClick(this);
        }
    }

    public void setColorScheme(int colorScheme) {
        setStyle(this.K, colorScheme);
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        this.M.setEnabled(enabled);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.N = listener;
        if (this.M != null) {
            this.M.setOnClickListener(this);
        }
    }

    public void setSize(int buttonSize) {
        setStyle(buttonSize, this.L);
    }

    public void setStyle(int buttonSize, int colorScheme) {
        boolean z = true;
        x.a(buttonSize >= 0 && buttonSize < 3, "Unknown button size " + buttonSize);
        if (colorScheme < 0 || colorScheme >= 2) {
            z = false;
        }
        x.a(z, "Unknown color scheme " + colorScheme);
        this.K = buttonSize;
        this.L = colorScheme;
        d(getContext());
    }
}
