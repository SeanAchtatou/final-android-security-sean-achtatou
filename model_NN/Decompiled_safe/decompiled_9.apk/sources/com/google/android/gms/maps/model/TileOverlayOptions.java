package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.cx;
import com.google.android.gms.internal.dh;
import com.google.android.gms.internal.dp;

public final class TileOverlayOptions implements ae {
    public static final TileOverlayOptionsCreator CREATOR = new TileOverlayOptionsCreator();
    private final int T;
    private float fU;
    private boolean fV;
    /* access modifiers changed from: private */
    public dp gv;
    private TileProvider gw;

    public TileOverlayOptions() {
        this.fV = true;
        this.T = 1;
    }

    TileOverlayOptions(int versionCode, IBinder delegate, boolean visible, float zIndex) {
        this.fV = true;
        this.T = versionCode;
        this.gv = dp.a.Q(delegate);
        this.gw = this.gv == null ? null : new TileProvider() {
            private final dp gx = TileOverlayOptions.this.gv;

            public Tile getTile(int x, int y, int zoom) {
                try {
                    return this.gx.getTile(x, y, zoom);
                } catch (RemoteException e) {
                    return null;
                }
            }
        };
        this.fV = visible;
        this.fU = zIndex;
    }

    public IBinder ba() {
        return this.gv.asBinder();
    }

    public int describeContents() {
        return 0;
    }

    public TileProvider getTileProvider() {
        return this.gw;
    }

    public float getZIndex() {
        return this.fU;
    }

    public boolean isVisible() {
        return this.fV;
    }

    public TileOverlayOptions tileProvider(final TileProvider tileProvider) {
        this.gw = tileProvider;
        this.gv = this.gw == null ? null : new dp.a() {
            public Tile getTile(int x, int y, int zoom) {
                return tileProvider.getTile(x, y, zoom);
            }
        };
        return this;
    }

    public int u() {
        return this.T;
    }

    public TileOverlayOptions visible(boolean visible) {
        this.fV = visible;
        return this;
    }

    public void writeToParcel(Parcel out, int flags) {
        if (cx.aV()) {
            dh.a(this, out, flags);
        } else {
            TileOverlayOptionsCreator.a(this, out, flags);
        }
    }

    public TileOverlayOptions zIndex(float zIndex) {
        this.fU = zIndex;
        return this;
    }
}
