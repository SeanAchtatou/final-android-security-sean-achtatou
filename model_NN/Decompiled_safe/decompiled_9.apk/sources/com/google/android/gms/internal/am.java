package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ak;

public class am implements Parcelable.Creator<ak.a> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(ak.a aVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, aVar.versionCode);
        ad.a(parcel, 2, aVar.bs, false);
        ad.c(parcel, 3, aVar.bt);
        ad.C(parcel, d);
    }

    /* renamed from: h */
    public ak.a createFromParcel(Parcel parcel) {
        int i = 0;
        int c = ac.c(parcel);
        String str = null;
        int i2 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i2 = ac.f(parcel, b);
                    break;
                case 2:
                    str = ac.l(parcel, b);
                    break;
                case 3:
                    i = ac.f(parcel, b);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new ak.a(i2, str, i);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    /* renamed from: n */
    public ak.a[] newArray(int i) {
        return new ak.a[i];
    }
}
