package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.PolygonOptions;

public class de {
    public static void a(PolygonOptions polygonOptions, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, polygonOptions.u());
        ad.b(parcel, 2, polygonOptions.getPoints(), false);
        ad.c(parcel, 3, polygonOptions.aZ(), false);
        ad.a(parcel, 4, polygonOptions.getStrokeWidth());
        ad.c(parcel, 5, polygonOptions.getStrokeColor());
        ad.c(parcel, 6, polygonOptions.getFillColor());
        ad.a(parcel, 7, polygonOptions.getZIndex());
        ad.a(parcel, 8, polygonOptions.isVisible());
        ad.a(parcel, 9, polygonOptions.isGeodesic());
        ad.C(parcel, d);
    }
}
