package com.google.android.gms.internal;

import android.os.Parcel;
import android.support.v4.util.TimeUtils;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.internal.an;
import com.google.android.gms.plus.PlusShare;
import com.google.android.gms.plus.model.moments.ItemScope;
import com.widgetizeme.UnsentStats;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class ed extends an implements ae, ItemScope {
    public static final ee CREATOR = new ee();
    private static final HashMap<String, an.a<?, ?>> hR = new HashMap<>();
    private final int T;
    private String ch;
    private double ew;
    private double ex;
    private String hE;
    private final Set<Integer> hS;
    private ed hT;
    private List<String> hU;
    private ed hV;
    private String hW;
    private String hX;
    private String hY;
    private List<ed> hZ;
    private ed iA;
    private List<ed> iB;
    private String iC;
    private String iD;
    private String iE;
    private String iF;
    private ed iG;
    private String iH;
    private String iI;
    private String iJ;
    private ed iK;
    private String iL;
    private String iM;
    private String iN;
    private String iO;
    private String iP;
    private int ia;
    private List<ed> ib;
    private ed ic;
    private List<ed> id;
    private String ie;

    /* renamed from: if  reason: not valid java name */
    private String f2if;
    private ed ig;
    private String ih;
    private String ii;
    private String ij;
    private List<ed> ik;
    private String il;
    private String im;
    private String in;
    private String io;
    private String ip;
    private String iq;
    private String ir;
    private String is;
    private ed it;
    private String iu;
    private String iv;
    private String iw;
    private String ix;
    private ed iy;
    private ed iz;
    private String mName;

    static {
        hR.put("about", an.a.a("about", 2, ed.class));
        hR.put("additionalName", an.a.g("additionalName", 3));
        hR.put("address", an.a.a("address", 4, ed.class));
        hR.put("addressCountry", an.a.f("addressCountry", 5));
        hR.put("addressLocality", an.a.f("addressLocality", 6));
        hR.put("addressRegion", an.a.f("addressRegion", 7));
        hR.put("associated_media", an.a.b("associated_media", 8, ed.class));
        hR.put("attendeeCount", an.a.c("attendeeCount", 9));
        hR.put("attendees", an.a.b("attendees", 10, ed.class));
        hR.put("audio", an.a.a("audio", 11, ed.class));
        hR.put("author", an.a.b("author", 12, ed.class));
        hR.put("bestRating", an.a.f("bestRating", 13));
        hR.put("birthDate", an.a.f("birthDate", 14));
        hR.put("byArtist", an.a.a("byArtist", 15, ed.class));
        hR.put("caption", an.a.f("caption", 16));
        hR.put("contentSize", an.a.f("contentSize", 17));
        hR.put("contentUrl", an.a.f("contentUrl", 18));
        hR.put("contributor", an.a.b("contributor", 19, ed.class));
        hR.put("dateCreated", an.a.f("dateCreated", 20));
        hR.put("dateModified", an.a.f("dateModified", 21));
        hR.put("datePublished", an.a.f("datePublished", 22));
        hR.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, an.a.f(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, 23));
        hR.put("duration", an.a.f("duration", 24));
        hR.put("embedUrl", an.a.f("embedUrl", 25));
        hR.put("endDate", an.a.f("endDate", 26));
        hR.put("familyName", an.a.f("familyName", 27));
        hR.put("gender", an.a.f("gender", 28));
        hR.put("geo", an.a.a("geo", 29, ed.class));
        hR.put("givenName", an.a.f("givenName", 30));
        hR.put("height", an.a.f("height", 31));
        hR.put(UnsentStats.KEY_ID, an.a.f(UnsentStats.KEY_ID, 32));
        hR.put("image", an.a.f("image", 33));
        hR.put("inAlbum", an.a.a("inAlbum", 34, ed.class));
        hR.put("latitude", an.a.d("latitude", 36));
        hR.put("location", an.a.a("location", 37, ed.class));
        hR.put("longitude", an.a.d("longitude", 38));
        hR.put("name", an.a.f("name", 39));
        hR.put("partOfTVSeries", an.a.a("partOfTVSeries", 40, ed.class));
        hR.put("performers", an.a.b("performers", 41, ed.class));
        hR.put("playerType", an.a.f("playerType", 42));
        hR.put("postOfficeBoxNumber", an.a.f("postOfficeBoxNumber", 43));
        hR.put("postalCode", an.a.f("postalCode", 44));
        hR.put("ratingValue", an.a.f("ratingValue", 45));
        hR.put("reviewRating", an.a.a("reviewRating", 46, ed.class));
        hR.put("startDate", an.a.f("startDate", 47));
        hR.put("streetAddress", an.a.f("streetAddress", 48));
        hR.put("text", an.a.f("text", 49));
        hR.put("thumbnail", an.a.a("thumbnail", 50, ed.class));
        hR.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_THUMBNAIL_URL, an.a.f(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_THUMBNAIL_URL, 51));
        hR.put("tickerSymbol", an.a.f("tickerSymbol", 52));
        hR.put(ServerProtocol.DIALOG_PARAM_TYPE, an.a.f(ServerProtocol.DIALOG_PARAM_TYPE, 53));
        hR.put(PlusShare.KEY_CALL_TO_ACTION_URL, an.a.f(PlusShare.KEY_CALL_TO_ACTION_URL, 54));
        hR.put("width", an.a.f("width", 55));
        hR.put("worstRating", an.a.f("worstRating", 56));
    }

    public ed() {
        this.T = 1;
        this.hS = new HashSet();
    }

    ed(Set<Integer> set, int i, ed edVar, List<String> list, ed edVar2, String str, String str2, String str3, List<ed> list2, int i2, List<ed> list3, ed edVar3, List<ed> list4, String str4, String str5, ed edVar4, String str6, String str7, String str8, List<ed> list5, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, String str17, ed edVar5, String str18, String str19, String str20, String str21, ed edVar6, double d, ed edVar7, double d2, String str22, ed edVar8, List<ed> list6, String str23, String str24, String str25, String str26, ed edVar9, String str27, String str28, String str29, ed edVar10, String str30, String str31, String str32, String str33, String str34, String str35) {
        this.hS = set;
        this.T = i;
        this.hT = edVar;
        this.hU = list;
        this.hV = edVar2;
        this.hW = str;
        this.hX = str2;
        this.hY = str3;
        this.hZ = list2;
        this.ia = i2;
        this.ib = list3;
        this.ic = edVar3;
        this.id = list4;
        this.ie = str4;
        this.f2if = str5;
        this.ig = edVar4;
        this.ih = str6;
        this.ii = str7;
        this.ij = str8;
        this.ik = list5;
        this.il = str9;
        this.im = str10;
        this.in = str11;
        this.ch = str12;
        this.io = str13;
        this.ip = str14;
        this.iq = str15;
        this.ir = str16;
        this.is = str17;
        this.it = edVar5;
        this.iu = str18;
        this.iv = str19;
        this.iw = str20;
        this.ix = str21;
        this.iy = edVar6;
        this.ew = d;
        this.iz = edVar7;
        this.ex = d2;
        this.mName = str22;
        this.iA = edVar8;
        this.iB = list6;
        this.iC = str23;
        this.iD = str24;
        this.iE = str25;
        this.iF = str26;
        this.iG = edVar9;
        this.iH = str27;
        this.iI = str28;
        this.iJ = str29;
        this.iK = edVar10;
        this.iL = str30;
        this.iM = str31;
        this.iN = str32;
        this.hE = str33;
        this.iO = str34;
        this.iP = str35;
    }

    public ed(Set<Integer> set, ed edVar, List<String> list, ed edVar2, String str, String str2, String str3, List<ed> list2, int i, List<ed> list3, ed edVar3, List<ed> list4, String str4, String str5, ed edVar4, String str6, String str7, String str8, List<ed> list5, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, String str17, ed edVar5, String str18, String str19, String str20, String str21, ed edVar6, double d, ed edVar7, double d2, String str22, ed edVar8, List<ed> list6, String str23, String str24, String str25, String str26, ed edVar9, String str27, String str28, String str29, ed edVar10, String str30, String str31, String str32, String str33, String str34, String str35) {
        this.hS = set;
        this.T = 1;
        this.hT = edVar;
        this.hU = list;
        this.hV = edVar2;
        this.hW = str;
        this.hX = str2;
        this.hY = str3;
        this.hZ = list2;
        this.ia = i;
        this.ib = list3;
        this.ic = edVar3;
        this.id = list4;
        this.ie = str4;
        this.f2if = str5;
        this.ig = edVar4;
        this.ih = str6;
        this.ii = str7;
        this.ij = str8;
        this.ik = list5;
        this.il = str9;
        this.im = str10;
        this.in = str11;
        this.ch = str12;
        this.io = str13;
        this.ip = str14;
        this.iq = str15;
        this.ir = str16;
        this.is = str17;
        this.it = edVar5;
        this.iu = str18;
        this.iv = str19;
        this.iw = str20;
        this.ix = str21;
        this.iy = edVar6;
        this.ew = d;
        this.iz = edVar7;
        this.ex = d2;
        this.mName = str22;
        this.iA = edVar8;
        this.iB = list6;
        this.iC = str23;
        this.iD = str24;
        this.iE = str25;
        this.iF = str26;
        this.iG = edVar9;
        this.iH = str27;
        this.iI = str28;
        this.iJ = str29;
        this.iK = edVar10;
        this.iL = str30;
        this.iM = str31;
        this.iN = str32;
        this.hE = str33;
        this.iO = str34;
        this.iP = str35;
    }

    public HashMap<String, an.a<?, ?>> G() {
        return hR;
    }

    /* access modifiers changed from: protected */
    public boolean a(an.a aVar) {
        return this.hS.contains(Integer.valueOf(aVar.N()));
    }

    /* access modifiers changed from: protected */
    public Object b(an.a aVar) {
        switch (aVar.N()) {
            case 2:
                return this.hT;
            case 3:
                return this.hU;
            case 4:
                return this.hV;
            case 5:
                return this.hW;
            case 6:
                return this.hX;
            case 7:
                return this.hY;
            case 8:
                return this.hZ;
            case 9:
                return Integer.valueOf(this.ia);
            case 10:
                return this.ib;
            case 11:
                return this.ic;
            case 12:
                return this.id;
            case 13:
                return this.ie;
            case 14:
                return this.f2if;
            case 15:
                return this.ig;
            case 16:
                return this.ih;
            case 17:
                return this.ii;
            case 18:
                return this.ij;
            case TimeUtils.HUNDRED_DAY_FIELD_LEN:
                return this.ik;
            case 20:
                return this.il;
            case 21:
                return this.im;
            case 22:
                return this.in;
            case 23:
                return this.ch;
            case 24:
                return this.io;
            case 25:
                return this.ip;
            case 26:
                return this.iq;
            case 27:
                return this.ir;
            case 28:
                return this.is;
            case 29:
                return this.it;
            case 30:
                return this.iu;
            case 31:
                return this.iv;
            case AccessibilityNodeInfoCompat.ACTION_LONG_CLICK:
                return this.iw;
            case 33:
                return this.ix;
            case 34:
                return this.iy;
            case 35:
            default:
                throw new IllegalStateException("Unknown safe parcelable id=" + aVar.N());
            case 36:
                return Double.valueOf(this.ew);
            case 37:
                return this.iz;
            case 38:
                return Double.valueOf(this.ex);
            case 39:
                return this.mName;
            case 40:
                return this.iA;
            case 41:
                return this.iB;
            case 42:
                return this.iC;
            case 43:
                return this.iD;
            case 44:
                return this.iE;
            case 45:
                return this.iF;
            case 46:
                return this.iG;
            case 47:
                return this.iH;
            case 48:
                return this.iI;
            case 49:
                return this.iJ;
            case 50:
                return this.iK;
            case 51:
                return this.iL;
            case 52:
                return this.iM;
            case 53:
                return this.iN;
            case 54:
                return this.hE;
            case 55:
                return this.iO;
            case 56:
                return this.iP;
        }
    }

    /* access modifiers changed from: package-private */
    public ed bA() {
        return this.hV;
    }

    /* access modifiers changed from: package-private */
    public List<ed> bB() {
        return this.hZ;
    }

    /* access modifiers changed from: package-private */
    public List<ed> bC() {
        return this.ib;
    }

    /* access modifiers changed from: package-private */
    public ed bD() {
        return this.ic;
    }

    /* access modifiers changed from: package-private */
    public List<ed> bE() {
        return this.id;
    }

    /* access modifiers changed from: package-private */
    public ed bF() {
        return this.ig;
    }

    /* access modifiers changed from: package-private */
    public List<ed> bG() {
        return this.ik;
    }

    /* access modifiers changed from: package-private */
    public ed bH() {
        return this.it;
    }

    /* access modifiers changed from: package-private */
    public ed bI() {
        return this.iy;
    }

    /* access modifiers changed from: package-private */
    public ed bJ() {
        return this.iz;
    }

    /* access modifiers changed from: package-private */
    public ed bK() {
        return this.iA;
    }

    /* access modifiers changed from: package-private */
    public List<ed> bL() {
        return this.iB;
    }

    /* access modifiers changed from: package-private */
    public ed bM() {
        return this.iG;
    }

    /* access modifiers changed from: package-private */
    public ed bN() {
        return this.iK;
    }

    /* renamed from: bO */
    public ed freeze() {
        return this;
    }

    /* access modifiers changed from: package-private */
    public Set<Integer> by() {
        return this.hS;
    }

    /* access modifiers changed from: package-private */
    public ed bz() {
        return this.hT;
    }

    public int describeContents() {
        ee eeVar = CREATOR;
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ed)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        ed edVar = (ed) obj;
        for (an.a next : hR.values()) {
            if (a(next)) {
                if (!edVar.a(next)) {
                    return false;
                }
                if (!b(next).equals(edVar.b(next))) {
                    return false;
                }
            } else if (edVar.a(next)) {
                return false;
            }
        }
        return true;
    }

    public ItemScope getAbout() {
        return this.hT;
    }

    public List<String> getAdditionalName() {
        return this.hU;
    }

    public ItemScope getAddress() {
        return this.hV;
    }

    public String getAddressCountry() {
        return this.hW;
    }

    public String getAddressLocality() {
        return this.hX;
    }

    public String getAddressRegion() {
        return this.hY;
    }

    public List<ItemScope> getAssociated_media() {
        return (ArrayList) this.hZ;
    }

    public int getAttendeeCount() {
        return this.ia;
    }

    public List<ItemScope> getAttendees() {
        return (ArrayList) this.ib;
    }

    public ItemScope getAudio() {
        return this.ic;
    }

    public List<ItemScope> getAuthor() {
        return (ArrayList) this.id;
    }

    public String getBestRating() {
        return this.ie;
    }

    public String getBirthDate() {
        return this.f2if;
    }

    public ItemScope getByArtist() {
        return this.ig;
    }

    public String getCaption() {
        return this.ih;
    }

    public String getContentSize() {
        return this.ii;
    }

    public String getContentUrl() {
        return this.ij;
    }

    public List<ItemScope> getContributor() {
        return (ArrayList) this.ik;
    }

    public String getDateCreated() {
        return this.il;
    }

    public String getDateModified() {
        return this.im;
    }

    public String getDatePublished() {
        return this.in;
    }

    public String getDescription() {
        return this.ch;
    }

    public String getDuration() {
        return this.io;
    }

    public String getEmbedUrl() {
        return this.ip;
    }

    public String getEndDate() {
        return this.iq;
    }

    public String getFamilyName() {
        return this.ir;
    }

    public String getGender() {
        return this.is;
    }

    public ItemScope getGeo() {
        return this.it;
    }

    public String getGivenName() {
        return this.iu;
    }

    public String getHeight() {
        return this.iv;
    }

    public String getId() {
        return this.iw;
    }

    public String getImage() {
        return this.ix;
    }

    public ItemScope getInAlbum() {
        return this.iy;
    }

    public double getLatitude() {
        return this.ew;
    }

    public ItemScope getLocation() {
        return this.iz;
    }

    public double getLongitude() {
        return this.ex;
    }

    public String getName() {
        return this.mName;
    }

    public ItemScope getPartOfTVSeries() {
        return this.iA;
    }

    public List<ItemScope> getPerformers() {
        return (ArrayList) this.iB;
    }

    public String getPlayerType() {
        return this.iC;
    }

    public String getPostOfficeBoxNumber() {
        return this.iD;
    }

    public String getPostalCode() {
        return this.iE;
    }

    public String getRatingValue() {
        return this.iF;
    }

    public ItemScope getReviewRating() {
        return this.iG;
    }

    public String getStartDate() {
        return this.iH;
    }

    public String getStreetAddress() {
        return this.iI;
    }

    public String getText() {
        return this.iJ;
    }

    public ItemScope getThumbnail() {
        return this.iK;
    }

    public String getThumbnailUrl() {
        return this.iL;
    }

    public String getTickerSymbol() {
        return this.iM;
    }

    public String getType() {
        return this.iN;
    }

    public String getUrl() {
        return this.hE;
    }

    public String getWidth() {
        return this.iO;
    }

    public String getWorstRating() {
        return this.iP;
    }

    public boolean hasAbout() {
        return this.hS.contains(2);
    }

    public boolean hasAdditionalName() {
        return this.hS.contains(3);
    }

    public boolean hasAddress() {
        return this.hS.contains(4);
    }

    public boolean hasAddressCountry() {
        return this.hS.contains(5);
    }

    public boolean hasAddressLocality() {
        return this.hS.contains(6);
    }

    public boolean hasAddressRegion() {
        return this.hS.contains(7);
    }

    public boolean hasAssociated_media() {
        return this.hS.contains(8);
    }

    public boolean hasAttendeeCount() {
        return this.hS.contains(9);
    }

    public boolean hasAttendees() {
        return this.hS.contains(10);
    }

    public boolean hasAudio() {
        return this.hS.contains(11);
    }

    public boolean hasAuthor() {
        return this.hS.contains(12);
    }

    public boolean hasBestRating() {
        return this.hS.contains(13);
    }

    public boolean hasBirthDate() {
        return this.hS.contains(14);
    }

    public boolean hasByArtist() {
        return this.hS.contains(15);
    }

    public boolean hasCaption() {
        return this.hS.contains(16);
    }

    public boolean hasContentSize() {
        return this.hS.contains(17);
    }

    public boolean hasContentUrl() {
        return this.hS.contains(18);
    }

    public boolean hasContributor() {
        return this.hS.contains(19);
    }

    public boolean hasDateCreated() {
        return this.hS.contains(20);
    }

    public boolean hasDateModified() {
        return this.hS.contains(21);
    }

    public boolean hasDatePublished() {
        return this.hS.contains(22);
    }

    public boolean hasDescription() {
        return this.hS.contains(23);
    }

    public boolean hasDuration() {
        return this.hS.contains(24);
    }

    public boolean hasEmbedUrl() {
        return this.hS.contains(25);
    }

    public boolean hasEndDate() {
        return this.hS.contains(26);
    }

    public boolean hasFamilyName() {
        return this.hS.contains(27);
    }

    public boolean hasGender() {
        return this.hS.contains(28);
    }

    public boolean hasGeo() {
        return this.hS.contains(29);
    }

    public boolean hasGivenName() {
        return this.hS.contains(30);
    }

    public boolean hasHeight() {
        return this.hS.contains(31);
    }

    public boolean hasId() {
        return this.hS.contains(32);
    }

    public boolean hasImage() {
        return this.hS.contains(33);
    }

    public boolean hasInAlbum() {
        return this.hS.contains(34);
    }

    public boolean hasLatitude() {
        return this.hS.contains(36);
    }

    public boolean hasLocation() {
        return this.hS.contains(37);
    }

    public boolean hasLongitude() {
        return this.hS.contains(38);
    }

    public boolean hasName() {
        return this.hS.contains(39);
    }

    public boolean hasPartOfTVSeries() {
        return this.hS.contains(40);
    }

    public boolean hasPerformers() {
        return this.hS.contains(41);
    }

    public boolean hasPlayerType() {
        return this.hS.contains(42);
    }

    public boolean hasPostOfficeBoxNumber() {
        return this.hS.contains(43);
    }

    public boolean hasPostalCode() {
        return this.hS.contains(44);
    }

    public boolean hasRatingValue() {
        return this.hS.contains(45);
    }

    public boolean hasReviewRating() {
        return this.hS.contains(46);
    }

    public boolean hasStartDate() {
        return this.hS.contains(47);
    }

    public boolean hasStreetAddress() {
        return this.hS.contains(48);
    }

    public boolean hasText() {
        return this.hS.contains(49);
    }

    public boolean hasThumbnail() {
        return this.hS.contains(50);
    }

    public boolean hasThumbnailUrl() {
        return this.hS.contains(51);
    }

    public boolean hasTickerSymbol() {
        return this.hS.contains(52);
    }

    public boolean hasType() {
        return this.hS.contains(53);
    }

    public boolean hasUrl() {
        return this.hS.contains(54);
    }

    public boolean hasWidth() {
        return this.hS.contains(55);
    }

    public boolean hasWorstRating() {
        return this.hS.contains(56);
    }

    public int hashCode() {
        int i = 0;
        Iterator<an.a<?, ?>> it2 = hR.values().iterator();
        while (true) {
            int i2 = i;
            if (!it2.hasNext()) {
                return i2;
            }
            an.a next = it2.next();
            if (a(next)) {
                i = b(next).hashCode() + i2 + next.N();
            } else {
                i = i2;
            }
        }
    }

    /* access modifiers changed from: protected */
    public Object j(String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean k(String str) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public int u() {
        return this.T;
    }

    public void writeToParcel(Parcel out, int flags) {
        ee eeVar = CREATOR;
        ee.a(this, out, flags);
    }
}
