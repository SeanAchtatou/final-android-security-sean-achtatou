package com.google.android.gms.internal;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.games.leaderboard.Leaderboard;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import java.util.ArrayList;

public final class br extends j implements Leaderboard {

    /* renamed from: do  reason: not valid java name */
    private final int f0do;

    public br(k kVar, int i, int i2) {
        super(kVar, i);
        this.f0do = i2;
    }

    public String getDisplayName() {
        return getString("name");
    }

    public void getDisplayName(CharArrayBuffer dataOut) {
        a("name", dataOut);
    }

    public Uri getIconImageUri() {
        return c("board_icon_image_uri");
    }

    public String getLeaderboardId() {
        return getString("external_leaderboard_id");
    }

    public int getScoreOrder() {
        return getInteger("score_order");
    }

    public ArrayList<LeaderboardVariant> getVariants() {
        ArrayList<LeaderboardVariant> arrayList = new ArrayList<>(this.f0do);
        for (int i = 0; i < this.f0do; i++) {
            arrayList.add(new bv(this.O, this.R + i));
        }
        return arrayList;
    }

    public String toString() {
        return w.c(this).a("ID", getLeaderboardId()).a("DisplayName", getDisplayName()).a("IconImageURI", getIconImageUri()).a("ScoreOrder", Integer.valueOf(getScoreOrder())).toString();
    }
}
