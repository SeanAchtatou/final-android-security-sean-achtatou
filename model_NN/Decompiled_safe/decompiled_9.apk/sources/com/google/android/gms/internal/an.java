package com.google.android.gms.internal;

import android.os.Parcel;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class an {

    public static class a<I, O> implements ae {
        public static final ao CREATOR = new ao();
        private final int T;
        protected final Class<? extends an> bA;
        protected final String bB;
        private aq bC;
        /* access modifiers changed from: private */
        public b<I, O> bD;
        protected final int bu;
        protected final boolean bv;
        protected final int bw;
        protected final boolean bx;
        protected final String by;
        protected final int bz;

        a(int i, int i2, boolean z, int i3, boolean z2, String str, int i4, String str2, ai aiVar) {
            this.T = i;
            this.bu = i2;
            this.bv = z;
            this.bw = i3;
            this.bx = z2;
            this.by = str;
            this.bz = i4;
            if (str2 == null) {
                this.bA = null;
                this.bB = null;
            } else {
                this.bA = at.class;
                this.bB = str2;
            }
            if (aiVar == null) {
                this.bD = null;
            } else {
                this.bD = aiVar.C();
            }
        }

        protected a(int i, boolean z, int i2, boolean z2, String str, int i3, Class<? extends an> cls, b<I, O> bVar) {
            this.T = 1;
            this.bu = i;
            this.bv = z;
            this.bw = i2;
            this.bx = z2;
            this.by = str;
            this.bz = i3;
            this.bA = cls;
            if (cls == null) {
                this.bB = null;
            } else {
                this.bB = cls.getCanonicalName();
            }
            this.bD = bVar;
        }

        public static a a(String str, int i, b<?, ?> bVar, boolean z) {
            return new a(bVar.E(), z, bVar.F(), false, str, i, null, bVar);
        }

        public static <T extends an> a<T, T> a(String str, int i, Class<T> cls) {
            return new a<>(11, false, 11, false, str, i, cls, null);
        }

        public static <T extends an> a<ArrayList<T>, ArrayList<T>> b(String str, int i, Class<T> cls) {
            return new a<>(11, true, 11, true, str, i, cls, null);
        }

        public static a<Integer, Integer> c(String str, int i) {
            return new a<>(0, false, 0, false, str, i, null, null);
        }

        public static a<Double, Double> d(String str, int i) {
            return new a<>(4, false, 4, false, str, i, null, null);
        }

        public static a<Boolean, Boolean> e(String str, int i) {
            return new a<>(6, false, 6, false, str, i, null, null);
        }

        public static a<String, String> f(String str, int i) {
            return new a<>(7, false, 7, false, str, i, null, null);
        }

        public static a<ArrayList<String>, ArrayList<String>> g(String str, int i) {
            return new a<>(7, true, 7, true, str, i, null, null);
        }

        public int E() {
            return this.bu;
        }

        public int F() {
            return this.bw;
        }

        public a<I, O> J() {
            return new a<>(this.T, this.bu, this.bv, this.bw, this.bx, this.by, this.bz, this.bB, R());
        }

        public boolean K() {
            return this.bv;
        }

        public boolean L() {
            return this.bx;
        }

        public String M() {
            return this.by;
        }

        public int N() {
            return this.bz;
        }

        public Class<? extends an> O() {
            return this.bA;
        }

        /* access modifiers changed from: package-private */
        public String P() {
            if (this.bB == null) {
                return null;
            }
            return this.bB;
        }

        public boolean Q() {
            return this.bD != null;
        }

        /* access modifiers changed from: package-private */
        public ai R() {
            if (this.bD == null) {
                return null;
            }
            return ai.a(this.bD);
        }

        public HashMap<String, a<?, ?>> S() {
            x.d(this.bB);
            x.d(this.bC);
            return this.bC.n(this.bB);
        }

        public void a(aq aqVar) {
            this.bC = aqVar;
        }

        public int describeContents() {
            ao aoVar = CREATOR;
            return 0;
        }

        public I e(O o) {
            return this.bD.e(o);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("Field\n");
            sb.append("            versionCode=").append(this.T).append(10);
            sb.append("                 typeIn=").append(this.bu).append(10);
            sb.append("            typeInArray=").append(this.bv).append(10);
            sb.append("                typeOut=").append(this.bw).append(10);
            sb.append("           typeOutArray=").append(this.bx).append(10);
            sb.append("        outputFieldName=").append(this.by).append(10);
            sb.append("      safeParcelFieldId=").append(this.bz).append(10);
            sb.append("       concreteTypeName=").append(P()).append(10);
            if (O() != null) {
                sb.append("     concreteType.class=").append(O().getCanonicalName()).append(10);
            }
            sb.append("          converterName=").append(this.bD == null ? "null" : this.bD.getClass().getCanonicalName()).append(10);
            return sb.toString();
        }

        public int u() {
            return this.T;
        }

        public void writeToParcel(Parcel out, int flags) {
            ao aoVar = CREATOR;
            ao.a(this, out, flags);
        }
    }

    public interface b<I, O> {
        int E();

        int F();

        I e(O o);
    }

    private void a(StringBuilder sb, a aVar, Object obj) {
        if (aVar.E() == 11) {
            sb.append(((an) aVar.O().cast(obj)).toString());
        } else if (aVar.E() == 7) {
            sb.append("\"");
            sb.append(ay.o((String) obj));
            sb.append("\"");
        } else {
            sb.append(obj);
        }
    }

    private void a(StringBuilder sb, a aVar, ArrayList<Object> arrayList) {
        sb.append("[");
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append(",");
            }
            Object obj = arrayList.get(i);
            if (obj != null) {
                a(sb, aVar, obj);
            }
        }
        sb.append("]");
    }

    public abstract HashMap<String, a<?, ?>> G();

    public HashMap<String, Object> H() {
        return null;
    }

    public HashMap<String, Object> I() {
        return null;
    }

    /* access modifiers changed from: protected */
    public <O, I> I a(a<I, O> aVar, Object obj) {
        return aVar.bD != null ? aVar.e(obj) : obj;
    }

    /* access modifiers changed from: protected */
    public boolean a(a aVar) {
        return aVar.F() == 11 ? aVar.L() ? m(aVar.M()) : l(aVar.M()) : k(aVar.M());
    }

    /* access modifiers changed from: protected */
    public Object b(a aVar) {
        boolean z = true;
        String M = aVar.M();
        if (aVar.O() == null) {
            return j(aVar.M());
        }
        if (j(aVar.M()) != null) {
            z = false;
        }
        x.a(z, "Concrete field shouldn't be value object: " + aVar.M());
        HashMap<String, Object> I = aVar.L() ? I() : H();
        if (I != null) {
            return I.get(M);
        }
        try {
            return getClass().getMethod("get" + Character.toUpperCase(M.charAt(0)) + M.substring(1), new Class[0]).invoke(this, new Object[0]);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /* access modifiers changed from: protected */
    public abstract Object j(String str);

    /* access modifiers changed from: protected */
    public abstract boolean k(String str);

    /* access modifiers changed from: protected */
    public boolean l(String str) {
        throw new UnsupportedOperationException("Concrete types not supported");
    }

    /* access modifiers changed from: protected */
    public boolean m(String str) {
        throw new UnsupportedOperationException("Concrete type arrays not supported");
    }

    public String toString() {
        HashMap<String, a<?, ?>> G = G();
        StringBuilder sb = new StringBuilder(100);
        for (String next : G.keySet()) {
            a aVar = G.get(next);
            if (a(aVar)) {
                Object a2 = a(aVar, b(aVar));
                if (sb.length() == 0) {
                    sb.append("{");
                } else {
                    sb.append(",");
                }
                sb.append("\"").append(next).append("\":");
                if (a2 != null) {
                    switch (aVar.F()) {
                        case 8:
                            sb.append("\"").append(aw.a((byte[]) a2)).append("\"");
                            continue;
                        case 9:
                            sb.append("\"").append(aw.b((byte[]) a2)).append("\"");
                            continue;
                        case 10:
                            az.a(sb, (HashMap) a2);
                            continue;
                        default:
                            if (!aVar.K()) {
                                a(sb, aVar, a2);
                                break;
                            } else {
                                a(sb, aVar, (ArrayList<Object>) ((ArrayList) a2));
                                continue;
                            }
                    }
                } else {
                    sb.append("null");
                }
            }
        }
        if (sb.length() > 0) {
            sb.append("}");
        } else {
            sb.append("{}");
        }
        return sb.toString();
    }
}
