package com.google.android.gms.internal;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantEntity;

public final class bx extends j implements Participant {
    private final bg dQ;

    public bx(k kVar, int i) {
        super(kVar, i);
        this.dQ = new bg(kVar, i);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return ParticipantEntity.a(this, obj);
    }

    public Participant freeze() {
        return new ParticipantEntity(this);
    }

    public String getClientAddress() {
        return getString("client_address");
    }

    public String getDisplayName() {
        return d("external_player_id") ? getString("default_display_name") : this.dQ.getDisplayName();
    }

    public void getDisplayName(CharArrayBuffer dataOut) {
        if (d("external_player_id")) {
            a("default_display_name", dataOut);
        } else {
            this.dQ.getDisplayName(dataOut);
        }
    }

    public Uri getHiResImageUri() {
        if (d("external_player_id")) {
            return null;
        }
        return this.dQ.getHiResImageUri();
    }

    public Uri getIconImageUri() {
        return d("external_player_id") ? c("default_display_image_uri") : this.dQ.getIconImageUri();
    }

    public String getParticipantId() {
        return getString("external_participant_id");
    }

    public Player getPlayer() {
        if (d("external_player_id")) {
            return null;
        }
        return this.dQ;
    }

    public int getStatus() {
        return getInteger("player_status");
    }

    public int hashCode() {
        return ParticipantEntity.a(this);
    }

    public boolean isConnectedToRoom() {
        return getInteger("connected") > 0;
    }

    public String toString() {
        return ParticipantEntity.b(this);
    }

    public void writeToParcel(Parcel dest, int flags) {
        ((ParticipantEntity) freeze()).writeToParcel(dest, flags);
    }
}
