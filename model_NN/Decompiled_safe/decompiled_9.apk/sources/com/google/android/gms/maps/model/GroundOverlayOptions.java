package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.bc;
import com.google.android.gms.internal.cx;
import com.google.android.gms.internal.da;
import com.google.android.gms.internal.x;

public final class GroundOverlayOptions implements ae {
    public static final GroundOverlayOptionsCreator CREATOR = new GroundOverlayOptionsCreator();
    public static final float NO_DIMENSION = -1.0f;
    private final int T;
    private float fN;
    private float fU;
    private boolean fV;
    private BitmapDescriptor fX;
    private LatLng fY;
    private float fZ;
    private float ga;
    private LatLngBounds gb;
    private float gc;
    private float gd;
    private float ge;

    public GroundOverlayOptions() {
        this.fV = true;
        this.gc = BitmapDescriptorFactory.HUE_RED;
        this.gd = 0.5f;
        this.ge = 0.5f;
        this.T = 1;
    }

    GroundOverlayOptions(int versionCode, IBinder wrappedImage, LatLng location, float width, float height, LatLngBounds bounds, float bearing, float zIndex, boolean visible, float transparency, float anchorU, float anchorV) {
        this.fV = true;
        this.gc = BitmapDescriptorFactory.HUE_RED;
        this.gd = 0.5f;
        this.ge = 0.5f;
        this.T = versionCode;
        this.fX = new BitmapDescriptor(bc.a.j(wrappedImage));
        this.fY = location;
        this.fZ = width;
        this.ga = height;
        this.gb = bounds;
        this.fN = bearing;
        this.fU = zIndex;
        this.fV = visible;
        this.gc = transparency;
        this.gd = anchorU;
        this.ge = anchorV;
    }

    private GroundOverlayOptions a(LatLng latLng, float f, float f2) {
        this.fY = latLng;
        this.fZ = f;
        this.ga = f2;
        return this;
    }

    public IBinder aX() {
        return this.fX.aD().asBinder();
    }

    public GroundOverlayOptions anchor(float u, float v) {
        this.gd = u;
        this.ge = v;
        return this;
    }

    public GroundOverlayOptions bearing(float bearing) {
        this.fN = ((bearing % 360.0f) + 360.0f) % 360.0f;
        return this;
    }

    public int describeContents() {
        return 0;
    }

    public float getAnchorU() {
        return this.gd;
    }

    public float getAnchorV() {
        return this.ge;
    }

    public float getBearing() {
        return this.fN;
    }

    public LatLngBounds getBounds() {
        return this.gb;
    }

    public float getHeight() {
        return this.ga;
    }

    public BitmapDescriptor getImage() {
        return this.fX;
    }

    public LatLng getLocation() {
        return this.fY;
    }

    public float getTransparency() {
        return this.gc;
    }

    public float getWidth() {
        return this.fZ;
    }

    public float getZIndex() {
        return this.fU;
    }

    public GroundOverlayOptions image(BitmapDescriptor image) {
        this.fX = image;
        return this;
    }

    public boolean isVisible() {
        return this.fV;
    }

    public GroundOverlayOptions position(LatLng location, float width) {
        boolean z = true;
        x.a(this.gb == null, "Position has already been set using positionFromBounds");
        x.b(location != null, "Location must be specified");
        if (width < BitmapDescriptorFactory.HUE_RED) {
            z = false;
        }
        x.b(z, "Width must be non-negative");
        return a(location, width, -1.0f);
    }

    public GroundOverlayOptions position(LatLng location, float width, float height) {
        boolean z = true;
        x.a(this.gb == null, "Position has already been set using positionFromBounds");
        x.b(location != null, "Location must be specified");
        x.b(width >= BitmapDescriptorFactory.HUE_RED, "Width must be non-negative");
        if (height < BitmapDescriptorFactory.HUE_RED) {
            z = false;
        }
        x.b(z, "Height must be non-negative");
        return a(location, width, height);
    }

    public GroundOverlayOptions positionFromBounds(LatLngBounds bounds) {
        x.a(this.fY == null, "Position has already been set using position: " + this.fY);
        this.gb = bounds;
        return this;
    }

    public GroundOverlayOptions transparency(float transparency) {
        x.b(transparency >= BitmapDescriptorFactory.HUE_RED && transparency <= 1.0f, "Transparency must be in the range [0..1]");
        this.gc = transparency;
        return this;
    }

    public int u() {
        return this.T;
    }

    public GroundOverlayOptions visible(boolean visible) {
        this.fV = visible;
        return this;
    }

    public void writeToParcel(Parcel out, int flags) {
        if (cx.aV()) {
            da.a(this, out, flags);
        } else {
            GroundOverlayOptionsCreator.a(this, out, flags);
        }
    }

    public GroundOverlayOptions zIndex(float zIndex) {
        this.fU = zIndex;
        return this;
    }
}
