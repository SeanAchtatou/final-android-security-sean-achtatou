package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;

public class ah implements Parcelable.Creator<ag> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(ag agVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, agVar.getType());
        ad.c(parcel, 1000, agVar.u());
        ad.c(parcel, 2, agVar.v());
        ad.a(parcel, 3, agVar.w(), false);
        ad.a(parcel, 4, agVar.x(), false);
        ad.a(parcel, 5, agVar.getDisplayName(), false);
        ad.a(parcel, 6, agVar.y(), false);
        ad.C(parcel, d);
    }

    /* renamed from: e */
    public ag createFromParcel(Parcel parcel) {
        int i = 0;
        String str = null;
        int c = ac.c(parcel);
        String str2 = null;
        String str3 = null;
        String str4 = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i2 = ac.f(parcel, b);
                    break;
                case 2:
                    i = ac.f(parcel, b);
                    break;
                case 3:
                    str4 = ac.l(parcel, b);
                    break;
                case 4:
                    str3 = ac.l(parcel, b);
                    break;
                case 5:
                    str2 = ac.l(parcel, b);
                    break;
                case 6:
                    str = ac.l(parcel, b);
                    break;
                case 1000:
                    i3 = ac.f(parcel, b);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new ag(i3, i2, i, str4, str3, str2, str);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    /* renamed from: k */
    public ag[] newArray(int i) {
        return new ag[i];
    }
}
