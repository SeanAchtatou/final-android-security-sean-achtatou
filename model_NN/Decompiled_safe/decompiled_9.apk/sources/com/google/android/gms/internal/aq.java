package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.internal.an;
import java.util.ArrayList;
import java.util.HashMap;

public class aq implements ae {
    public static final ar CREATOR = new ar();
    private final int T;
    private final HashMap<String, HashMap<String, an.a<?, ?>>> bE;
    private final ArrayList<a> bF;
    private final String bG;

    public static class a implements ae {
        public static final as CREATOR = new as();
        final ArrayList<b> bH;
        final String className;
        final int versionCode;

        a(int i, String str, ArrayList<b> arrayList) {
            this.versionCode = i;
            this.className = str;
            this.bH = arrayList;
        }

        a(String str, HashMap<String, an.a<?, ?>> hashMap) {
            this.versionCode = 1;
            this.className = str;
            this.bH = a(hashMap);
        }

        private static ArrayList<b> a(HashMap<String, an.a<?, ?>> hashMap) {
            if (hashMap == null) {
                return null;
            }
            ArrayList<b> arrayList = new ArrayList<>();
            for (String next : hashMap.keySet()) {
                arrayList.add(new b(next, hashMap.get(next)));
            }
            return arrayList;
        }

        /* access modifiers changed from: package-private */
        public HashMap<String, an.a<?, ?>> X() {
            HashMap<String, an.a<?, ?>> hashMap = new HashMap<>();
            int size = this.bH.size();
            for (int i = 0; i < size; i++) {
                b bVar = this.bH.get(i);
                hashMap.put(bVar.bI, bVar.bJ);
            }
            return hashMap;
        }

        public int describeContents() {
            as asVar = CREATOR;
            return 0;
        }

        public void writeToParcel(Parcel out, int flags) {
            as asVar = CREATOR;
            as.a(this, out, flags);
        }
    }

    public static class b implements ae {
        public static final ap CREATOR = new ap();
        final String bI;
        final an.a<?, ?> bJ;
        final int versionCode;

        b(int i, String str, an.a<?, ?> aVar) {
            this.versionCode = i;
            this.bI = str;
            this.bJ = aVar;
        }

        b(String str, an.a<?, ?> aVar) {
            this.versionCode = 1;
            this.bI = str;
            this.bJ = aVar;
        }

        public int describeContents() {
            ap apVar = CREATOR;
            return 0;
        }

        public void writeToParcel(Parcel out, int flags) {
            ap apVar = CREATOR;
            ap.a(this, out, flags);
        }
    }

    aq(int i, ArrayList<a> arrayList, String str) {
        this.T = i;
        this.bF = null;
        this.bE = b(arrayList);
        this.bG = (String) x.d(str);
        T();
    }

    public aq(Class<? extends an> cls) {
        this.T = 1;
        this.bF = null;
        this.bE = new HashMap<>();
        this.bG = cls.getCanonicalName();
    }

    private static HashMap<String, HashMap<String, an.a<?, ?>>> b(ArrayList<a> arrayList) {
        HashMap<String, HashMap<String, an.a<?, ?>>> hashMap = new HashMap<>();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            a aVar = arrayList.get(i);
            hashMap.put(aVar.className, aVar.X());
        }
        return hashMap;
    }

    public void T() {
        for (String str : this.bE.keySet()) {
            HashMap hashMap = this.bE.get(str);
            for (String str2 : hashMap.keySet()) {
                ((an.a) hashMap.get(str2)).a(this);
            }
        }
    }

    public void U() {
        for (String next : this.bE.keySet()) {
            HashMap hashMap = this.bE.get(next);
            HashMap hashMap2 = new HashMap();
            for (String str : hashMap.keySet()) {
                hashMap2.put(str, ((an.a) hashMap.get(str)).J());
            }
            this.bE.put(next, hashMap2);
        }
    }

    /* access modifiers changed from: package-private */
    public ArrayList<a> V() {
        ArrayList<a> arrayList = new ArrayList<>();
        for (String next : this.bE.keySet()) {
            arrayList.add(new a(next, this.bE.get(next)));
        }
        return arrayList;
    }

    public String W() {
        return this.bG;
    }

    public void a(Class<? extends an> cls, HashMap<String, an.a<?, ?>> hashMap) {
        this.bE.put(cls.getCanonicalName(), hashMap);
    }

    public boolean a(Class<? extends an> cls) {
        return this.bE.containsKey(cls.getCanonicalName());
    }

    public int describeContents() {
        ar arVar = CREATOR;
        return 0;
    }

    public HashMap<String, an.a<?, ?>> n(String str) {
        return this.bE.get(str);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String next : this.bE.keySet()) {
            sb.append(next).append(":\n");
            HashMap hashMap = this.bE.get(next);
            for (String str : hashMap.keySet()) {
                sb.append("  ").append(str).append(": ");
                sb.append(hashMap.get(str));
            }
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public int u() {
        return this.T;
    }

    public void writeToParcel(Parcel out, int flags) {
        ar arVar = CREATOR;
        ar.a(this, out, flags);
    }
}
