package com.google.android.gms.internal;

import com.google.android.gms.games.leaderboard.LeaderboardVariant;

public final class bv extends j implements LeaderboardVariant {
    bv(k kVar, int i) {
        super(kVar, i);
    }

    public String at() {
        return getString("top_page_token_next");
    }

    public String au() {
        return getString("window_page_token_prev");
    }

    public String av() {
        return getString("window_page_token_next");
    }

    public int getCollection() {
        return getInteger("collection");
    }

    public String getDisplayPlayerRank() {
        return getString("player_display_rank");
    }

    public String getDisplayPlayerScore() {
        return getString("player_display_score");
    }

    public long getNumScores() {
        if (d("total_scores")) {
            return -1;
        }
        return getLong("total_scores");
    }

    public long getPlayerRank() {
        if (d("player_rank")) {
            return -1;
        }
        return getLong("player_rank");
    }

    public long getRawPlayerScore() {
        if (d("player_raw_score")) {
            return -1;
        }
        return getLong("player_raw_score");
    }

    public int getTimeSpan() {
        return getInteger("timespan");
    }

    public boolean hasPlayerInfo() {
        return !d("player_raw_score");
    }

    public String toString() {
        return w.c(this).a("TimeSpan", bq.B(getTimeSpan())).a("Collection", bp.B(getCollection())).a("RawPlayerScore", hasPlayerInfo() ? Long.valueOf(getRawPlayerScore()) : "none").a("DisplayPlayerScore", hasPlayerInfo() ? getDisplayPlayerScore() : "none").a("PlayerRank", hasPlayerInfo() ? Long.valueOf(getPlayerRank()) : "none").a("DisplayPlayerRank", hasPlayerInfo() ? getDisplayPlayerRank() : "none").a("NumScores", Long.valueOf(getNumScores())).a("TopPageNextToken", at()).a("WindowPageNextToken", av()).a("WindowPagePrevToken", au()).toString();
    }
}
