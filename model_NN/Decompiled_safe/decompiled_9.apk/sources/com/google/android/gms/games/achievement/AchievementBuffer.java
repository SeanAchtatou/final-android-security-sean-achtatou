package com.google.android.gms.games.achievement;

import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.internal.bh;
import com.google.android.gms.internal.k;

public final class AchievementBuffer extends DataBuffer<Achievement> {
    public AchievementBuffer(k dataHolder) {
        super(dataHolder);
    }

    public Achievement get(int position) {
        return new bh(this.O, position);
    }
}
