package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ak;
import java.util.ArrayList;

public class al implements Parcelable.Creator<ak> {
    static void a(ak akVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, akVar.u());
        ad.b(parcel, 2, akVar.D(), false);
        ad.C(parcel, d);
    }

    /* renamed from: g */
    public ak createFromParcel(Parcel parcel) {
        int c = ac.c(parcel);
        int i = 0;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i = ac.f(parcel, b);
                    break;
                case 2:
                    arrayList = ac.c(parcel, b, ak.a.CREATOR);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new ak(i, arrayList);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    /* renamed from: m */
    public ak[] newArray(int i) {
        return new ak[i];
    }
}
