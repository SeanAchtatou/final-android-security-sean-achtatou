package com.google.android.gms.internal;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.leaderboard.LeaderboardScore;

public final class bu extends j implements LeaderboardScore {
    private final bg dA;

    public bu(k kVar, int i) {
        super(kVar, i);
        this.dA = new bg(kVar, i);
    }

    /* renamed from: as */
    public LeaderboardScore freeze() {
        return new bt(this);
    }

    public boolean equals(Object obj) {
        return bt.a(this, obj);
    }

    public String getDisplayRank() {
        return getString("display_rank");
    }

    public void getDisplayRank(CharArrayBuffer dataOut) {
        a("display_rank", dataOut);
    }

    public String getDisplayScore() {
        return getString("display_score");
    }

    public void getDisplayScore(CharArrayBuffer dataOut) {
        a("display_score", dataOut);
    }

    public long getRank() {
        return getLong("rank");
    }

    public long getRawScore() {
        return getLong("raw_score");
    }

    public Player getScoreHolder() {
        if (d("external_player_id")) {
            return null;
        }
        return this.dA;
    }

    public String getScoreHolderDisplayName() {
        return d("external_player_id") ? getString("default_display_name") : this.dA.getDisplayName();
    }

    public void getScoreHolderDisplayName(CharArrayBuffer dataOut) {
        if (d("external_player_id")) {
            a("default_display_name", dataOut);
        } else {
            this.dA.getDisplayName(dataOut);
        }
    }

    public Uri getScoreHolderHiResImageUri() {
        if (d("external_player_id")) {
            return null;
        }
        return this.dA.getHiResImageUri();
    }

    public Uri getScoreHolderIconImageUri() {
        return d("external_player_id") ? c("default_display_image_uri") : this.dA.getIconImageUri();
    }

    public long getTimestampMillis() {
        return getLong("achieved_timestamp");
    }

    public int hashCode() {
        return bt.a(this);
    }

    public String toString() {
        return bt.b(this);
    }
}
