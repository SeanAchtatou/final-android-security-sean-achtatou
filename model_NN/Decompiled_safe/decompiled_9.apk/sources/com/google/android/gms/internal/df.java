package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.PolylineOptions;

public class df {
    public static void a(PolylineOptions polylineOptions, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, polylineOptions.u());
        ad.b(parcel, 2, polylineOptions.getPoints(), false);
        ad.a(parcel, 3, polylineOptions.getWidth());
        ad.c(parcel, 4, polylineOptions.getColor());
        ad.a(parcel, 5, polylineOptions.getZIndex());
        ad.a(parcel, 6, polylineOptions.isVisible());
        ad.a(parcel, 7, polylineOptions.isGeodesic());
        ad.C(parcel, d);
    }
}
