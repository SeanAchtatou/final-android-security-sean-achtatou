package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.cl;
import com.google.android.gms.maps.model.RuntimeRemoteException;

public class cw {
    private static Context aZ;
    private static cl fH;

    private static <T> T a(ClassLoader classLoader, String str) {
        try {
            return b(((ClassLoader) x.d(classLoader)).loadClass(str));
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Unable to find dynamic class " + str);
        }
    }

    public static boolean aT() {
        return aU() != null;
    }

    private static Class<?> aU() {
        try {
            return Class.forName("com.google.android.gms.maps.internal.CreatorImpl");
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    private static <T> T b(Class<?> cls) {
        try {
            return cls.newInstance();
        } catch (InstantiationException e) {
            throw new IllegalStateException("Unable to instantiate the dynamic class " + cls.getName());
        } catch (IllegalAccessException e2) {
            throw new IllegalStateException("Unable to call the default constructor of " + cls.getName());
        }
    }

    public static cl g(Context context) throws GooglePlayServicesNotAvailableException {
        x.d(context);
        i(context);
        if (fH == null) {
            j(context);
        }
        if (fH != null) {
            return fH;
        }
        fH = cl.a.t((IBinder) a(getRemoteContext(context).getClassLoader(), "com.google.android.gms.maps.internal.CreatorImpl"));
        h(context);
        return fH;
    }

    private static Context getRemoteContext(Context context) {
        if (aZ == null) {
            if (aU() != null) {
                aZ = context;
            } else {
                aZ = GooglePlayServicesUtil.getRemoteContext(context);
            }
        }
        return aZ;
    }

    private static void h(Context context) {
        try {
            fH.a(bd.f(getRemoteContext(context).getResources()), (int) GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE);
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public static void i(Context context) throws GooglePlayServicesNotAvailableException {
        int isGooglePlayServicesAvailable;
        if (!aT() && (isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context)) != 0) {
            throw new GooglePlayServicesNotAvailableException(isGooglePlayServicesAvailable);
        }
    }

    private static void j(Context context) {
        Class<?> aU = aU();
        if (aU != null) {
            Log.i(cw.class.getSimpleName(), "Making Creator statically");
            fH = (cl) b(aU);
            h(context);
        }
    }
}
