package com.google.android.gms.internal;

import com.google.android.gms.appstate.AppState;

public final class c extends j implements AppState {
    public c(k kVar, int i) {
        super(kVar, i);
    }

    /* renamed from: a */
    public AppState freeze() {
        return new b(this);
    }

    public boolean equals(Object obj) {
        return b.a(this, obj);
    }

    public byte[] getConflictData() {
        return getByteArray("conflict_data");
    }

    public String getConflictVersion() {
        return getString("conflict_version");
    }

    public int getKey() {
        return getInteger("key");
    }

    public byte[] getLocalData() {
        return getByteArray("local_data");
    }

    public String getLocalVersion() {
        return getString("local_version");
    }

    public boolean hasConflict() {
        return !d("conflict_version");
    }

    public int hashCode() {
        return b.a(this);
    }

    public String toString() {
        return b.b(this);
    }
}
