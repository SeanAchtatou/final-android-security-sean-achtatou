package com.google.android.gms.plus;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.o;
import com.google.android.gms.internal.r;

public class GooglePlusUtil {
    public static final int APP_DISABLED = 3;
    public static final int APP_MISSING = 1;
    public static final int APP_UPDATE_REQUIRED = 2;
    public static final String GOOGLE_PLUS_PACKAGE = "com.google.android.apps.plus";
    public static final String PLATFORM_LOGGING_TAG = "GooglePlusPlatform";
    public static final int SUCCESS = 0;

    private GooglePlusUtil() {
        throw new AssertionError("Cannot instantiate GooglePlusUtil");
    }

    static Dialog a(AlertDialog.Builder builder, int i, Activity activity, int i2) {
        if (i2 < 0) {
            throw new IllegalArgumentException("Request code must not be negative.");
        }
        switch (i) {
            case 0:
                return null;
            case 1:
                return builder.setTitle(a(activity, "install_google_plus_title", "Get Google+")).setMessage(a(activity, "install_google_plus_text", "Download Google+ from Google Play so you can share this?")).setPositiveButton(a(activity, "install_google_plus_button", "Get Google+"), new o(activity, r.g(GOOGLE_PLUS_PACKAGE), i2)).create();
            case 2:
                return builder.setTitle(a(activity, "update_google_plus_title", "Update Google+")).setMessage(a(activity, "update_google_plus_text", "Update Google+ from Google Play so you can share this?")).setPositiveButton(a(activity, "update_google_plus_button", "Update"), new o(activity, r.g(GOOGLE_PLUS_PACKAGE), i2)).create();
            case 3:
                return builder.setTitle(a(activity, "enable_google_plus_title", "Enable Google+")).setMessage(a(activity, "enable_google_plus_text", "Enable Google+ from Google Play so you can share this?")).setPositiveButton(a(activity, "enable_google_plus_button", "Enable Google+"), new o(activity, r.e(GOOGLE_PLUS_PACKAGE), i2)).create();
            default:
                throw new IllegalArgumentException("Unexpected errorCode " + i);
        }
    }

    static String a(Context context, String str, String str2) {
        try {
            Resources resources = context.createPackageContext(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, 0).getResources();
            return resources.getString(resources.getIdentifier(str, "string", GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE));
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("GooglePlusUtil", "Unable to load resources from GMS: GMS not installed.", e);
            return str2;
        } catch (Resources.NotFoundException e2) {
            Log.e("GooglePlusUtil", "Unable to load resources from GMS: Resource \"" + str + "\" not found.", e2);
            return str2;
        } catch (SecurityException e3) {
            Log.e("GooglePlusUtil", "Unable to load resources from GMS.", e3);
            return str2;
        }
    }

    private static int b(Context context, int i) {
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(GOOGLE_PLUS_PACKAGE, 0);
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(GOOGLE_PLUS_PACKAGE, 0);
            if (packageInfo.versionCode < i) {
                return 2;
            }
            return !applicationInfo.enabled ? 3 : 0;
        } catch (PackageManager.NameNotFoundException e) {
            return 1;
        }
    }

    public static int checkGooglePlusApp(Context context) {
        return b(context, 330000000);
    }

    public static Dialog getErrorDialog(int errorCode, Activity activity, int requestCode) {
        return a(new AlertDialog.Builder(activity), errorCode, activity, requestCode);
    }
}
