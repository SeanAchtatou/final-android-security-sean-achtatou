package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.CircleOptions;

public class cz {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public static void a(CircleOptions circleOptions, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, circleOptions.u());
        ad.a(parcel, 2, (Parcelable) circleOptions.getCenter(), i, false);
        ad.a(parcel, 3, circleOptions.getRadius());
        ad.a(parcel, 4, circleOptions.getStrokeWidth());
        ad.c(parcel, 5, circleOptions.getStrokeColor());
        ad.c(parcel, 6, circleOptions.getFillColor());
        ad.a(parcel, 7, circleOptions.getZIndex());
        ad.a(parcel, 8, circleOptions.isVisible());
        ad.C(parcel, d);
    }
}
