package com.google.android.gms.games.multiplayer;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.internal.ax;
import com.google.android.gms.internal.w;

public final class ParticipantEntity implements Parcelable, Participant {
    public static final Parcelable.Creator<ParticipantEntity> CREATOR = new Parcelable.Creator<ParticipantEntity>() {
        /* renamed from: D */
        public ParticipantEntity[] newArray(int i) {
            return new ParticipantEntity[i];
        }

        /* renamed from: q */
        public ParticipantEntity createFromParcel(Parcel parcel) {
            boolean z = true;
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            Uri parse = readString3 == null ? null : Uri.parse(readString3);
            String readString4 = parcel.readString();
            Uri parse2 = readString4 == null ? null : Uri.parse(readString4);
            int readInt = parcel.readInt();
            String readString5 = parcel.readString();
            boolean z2 = parcel.readInt() > 0;
            if (parcel.readInt() <= 0) {
                z = false;
            }
            return new ParticipantEntity(readString, readString2, parse, parse2, readInt, readString5, z2, z ? PlayerEntity.CREATOR.createFromParcel(parcel) : null);
        }
    };
    private final String bm;
    private final Uri cj;
    private final Uri ck;
    private final PlayerEntity dM;
    private final int dN;
    private final String dO;
    private final boolean dP;
    private final String dm;

    public ParticipantEntity(Participant participant) {
        Player player = participant.getPlayer();
        this.dM = player == null ? null : new PlayerEntity(player);
        this.dm = participant.getParticipantId();
        this.bm = participant.getDisplayName();
        this.cj = participant.getIconImageUri();
        this.ck = participant.getHiResImageUri();
        this.dN = participant.getStatus();
        this.dO = participant.getClientAddress();
        this.dP = participant.isConnectedToRoom();
    }

    private ParticipantEntity(String participantId, String displayName, Uri iconImageUri, Uri hiResImageUri, int status, String clientAddress, boolean connectedToRoom, PlayerEntity player) {
        this.dm = participantId;
        this.bm = displayName;
        this.cj = iconImageUri;
        this.ck = hiResImageUri;
        this.dN = status;
        this.dO = clientAddress;
        this.dP = connectedToRoom;
        this.dM = player;
    }

    public static int a(Participant participant) {
        return w.hashCode(participant.getPlayer(), Integer.valueOf(participant.getStatus()), participant.getClientAddress(), Boolean.valueOf(participant.isConnectedToRoom()), participant.getDisplayName(), participant.getIconImageUri(), participant.getHiResImageUri());
    }

    public static boolean a(Participant participant, Object obj) {
        if (!(obj instanceof Participant)) {
            return false;
        }
        if (participant == obj) {
            return true;
        }
        Participant participant2 = (Participant) obj;
        return w.a(participant2.getPlayer(), participant.getPlayer()) && w.a(Integer.valueOf(participant2.getStatus()), Integer.valueOf(participant.getStatus())) && w.a(participant2.getClientAddress(), participant.getClientAddress()) && w.a(Boolean.valueOf(participant2.isConnectedToRoom()), Boolean.valueOf(participant.isConnectedToRoom())) && w.a(participant2.getDisplayName(), participant.getDisplayName()) && w.a(participant2.getIconImageUri(), participant.getIconImageUri()) && w.a(participant2.getHiResImageUri(), participant.getHiResImageUri());
    }

    public static String b(Participant participant) {
        return w.c(participant).a("Player", participant.getPlayer()).a("Status", Integer.valueOf(participant.getStatus())).a("ClientAddress", participant.getClientAddress()).a("ConnectedToRoom", Boolean.valueOf(participant.isConnectedToRoom())).a("DisplayName", participant.getDisplayName()).a("IconImage", participant.getIconImageUri()).a("HiResImage", participant.getHiResImageUri()).toString();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public Participant freeze() {
        return this;
    }

    public String getClientAddress() {
        return this.dO;
    }

    public String getDisplayName() {
        return this.dM == null ? this.bm : this.dM.getDisplayName();
    }

    public void getDisplayName(CharArrayBuffer dataOut) {
        if (this.dM == null) {
            ax.b(this.bm, dataOut);
        } else {
            this.dM.getDisplayName(dataOut);
        }
    }

    public Uri getHiResImageUri() {
        return this.dM == null ? this.ck : this.dM.getHiResImageUri();
    }

    public Uri getIconImageUri() {
        return this.dM == null ? this.cj : this.dM.getIconImageUri();
    }

    public String getParticipantId() {
        return this.dm;
    }

    public Player getPlayer() {
        return this.dM;
    }

    public int getStatus() {
        return this.dN;
    }

    public int hashCode() {
        return a(this);
    }

    public boolean isConnectedToRoom() {
        return this.dP;
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel dest, int flags) {
        String str = null;
        int i = 0;
        dest.writeString(this.dm);
        dest.writeString(this.bm);
        dest.writeString(this.cj == null ? null : this.cj.toString());
        if (this.ck != null) {
            str = this.ck.toString();
        }
        dest.writeString(str);
        dest.writeInt(this.dN);
        dest.writeString(this.dO);
        dest.writeInt(this.dP ? 1 : 0);
        if (this.dM != null) {
            i = 1;
        }
        dest.writeInt(i);
        if (this.dM != null) {
            this.dM.writeToParcel(dest, flags);
        }
    }
}
