package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class w {

    public static final class a {
        private final List<String> aW;
        private final Object aX;

        private a(Object obj) {
            this.aX = x.d(obj);
            this.aW = new ArrayList();
        }

        public a a(String str, Object obj) {
            this.aW.add(((String) x.d(str)) + "=" + String.valueOf(obj));
            return this;
        }

        public String toString() {
            StringBuilder append = new StringBuilder(100).append(this.aX.getClass().getSimpleName()).append('{');
            int size = this.aW.size();
            for (int i = 0; i < size; i++) {
                append.append(this.aW.get(i));
                if (i < size - 1) {
                    append.append(", ");
                }
            }
            return append.append('}').toString();
        }
    }

    public static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static a c(Object obj) {
        return new a(obj);
    }

    public static int hashCode(Object... objects) {
        return Arrays.hashCode(objects);
    }
}
