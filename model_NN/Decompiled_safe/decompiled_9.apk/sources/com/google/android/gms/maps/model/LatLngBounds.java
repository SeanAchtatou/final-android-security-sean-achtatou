package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.cx;
import com.google.android.gms.internal.db;
import com.google.android.gms.internal.w;
import com.google.android.gms.internal.x;

public final class LatLngBounds implements ae {
    public static final LatLngBoundsCreator CREATOR = new LatLngBoundsCreator();
    private final int T;
    public final LatLng northeast;
    public final LatLng southwest;

    public static final class Builder {
        private double gf = Double.POSITIVE_INFINITY;
        private double gg = Double.NEGATIVE_INFINITY;
        private double gh = Double.NaN;
        private double gi = Double.NaN;

        private boolean b(double d) {
            boolean z = false;
            if (this.gh <= this.gi) {
                return this.gh <= d && d <= this.gi;
            }
            if (this.gh <= d || d <= this.gi) {
                z = true;
            }
            return z;
        }

        public LatLngBounds build() {
            x.a(!Double.isNaN(this.gh), "no included points");
            return new LatLngBounds(new LatLng(this.gf, this.gh), new LatLng(this.gg, this.gi));
        }

        public Builder include(LatLng point) {
            this.gf = Math.min(this.gf, point.latitude);
            this.gg = Math.max(this.gg, point.latitude);
            double d = point.longitude;
            if (Double.isNaN(this.gh)) {
                this.gh = d;
                this.gi = d;
            } else if (!b(d)) {
                if (LatLngBounds.b(this.gh, d) < LatLngBounds.c(this.gi, d)) {
                    this.gh = d;
                } else {
                    this.gi = d;
                }
            }
            return this;
        }
    }

    LatLngBounds(int versionCode, LatLng southwest2, LatLng northeast2) {
        x.b(southwest2, "null southwest");
        x.b(northeast2, "null northeast");
        x.a(northeast2.latitude >= southwest2.latitude, "southern latitude exceeds northern latitude (%s > %s)", Double.valueOf(southwest2.latitude), Double.valueOf(northeast2.latitude));
        this.T = versionCode;
        this.southwest = southwest2;
        this.northeast = northeast2;
    }

    public LatLngBounds(LatLng southwest2, LatLng northeast2) {
        this(1, southwest2, northeast2);
    }

    private boolean a(double d) {
        return this.southwest.latitude <= d && d <= this.northeast.latitude;
    }

    /* access modifiers changed from: private */
    public static double b(double d, double d2) {
        return ((d - d2) + 360.0d) % 360.0d;
    }

    private boolean b(double d) {
        boolean z = false;
        if (this.southwest.longitude <= this.northeast.longitude) {
            return this.southwest.longitude <= d && d <= this.northeast.longitude;
        }
        if (this.southwest.longitude <= d || d <= this.northeast.longitude) {
            z = true;
        }
        return z;
    }

    public static Builder builder() {
        return new Builder();
    }

    /* access modifiers changed from: private */
    public static double c(double d, double d2) {
        return ((d2 - d) + 360.0d) % 360.0d;
    }

    public boolean contains(LatLng point) {
        return a(point.latitude) && b(point.longitude);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LatLngBounds)) {
            return false;
        }
        LatLngBounds latLngBounds = (LatLngBounds) o;
        return this.southwest.equals(latLngBounds.southwest) && this.northeast.equals(latLngBounds.northeast);
    }

    public int hashCode() {
        return w.hashCode(this.southwest, this.northeast);
    }

    public LatLngBounds including(LatLng point) {
        double d;
        double min = Math.min(this.southwest.latitude, point.latitude);
        double max = Math.max(this.northeast.latitude, point.latitude);
        double d2 = this.northeast.longitude;
        double d3 = this.southwest.longitude;
        double d4 = point.longitude;
        if (b(d4)) {
            d4 = d3;
            d = d2;
        } else if (b(d3, d4) < c(d2, d4)) {
            d = d2;
        } else {
            double d5 = d3;
            d = d4;
            d4 = d5;
        }
        return new LatLngBounds(new LatLng(min, d4), new LatLng(max, d));
    }

    public String toString() {
        return w.c(this).a("southwest", this.southwest).a("northeast", this.northeast).toString();
    }

    public int u() {
        return this.T;
    }

    public void writeToParcel(Parcel out, int flags) {
        if (cx.aV()) {
            db.a(this, out, flags);
        } else {
            LatLngBoundsCreator.a(this, out, flags);
        }
    }
}
