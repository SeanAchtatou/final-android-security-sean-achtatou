package com.google.android.gms.games.multiplayer;

import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.internal.bx;

public final class ParticipantBuffer extends DataBuffer<Participant> {
    public Participant get(int position) {
        return new bx(this.O, position);
    }
}
