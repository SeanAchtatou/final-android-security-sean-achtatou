package com.google.android.gms.games.multiplayer;

import com.google.android.gms.internal.bw;
import com.google.android.gms.internal.k;
import com.google.android.gms.internal.m;

public final class InvitationBuffer extends m<Invitation> {
    public InvitationBuffer(k dataHolder) {
        super(dataHolder);
    }

    /* access modifiers changed from: protected */
    /* renamed from: getEntry */
    public Invitation a(int rowIndex, int numChildren) {
        return new bw(this.O, rowIndex, numChildren);
    }

    /* access modifiers changed from: protected */
    public String getPrimaryDataMarkerColumn() {
        return "external_invitation_id";
    }
}
