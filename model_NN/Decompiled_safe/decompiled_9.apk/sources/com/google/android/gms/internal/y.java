package com.google.android.gms.internal;

import android.net.Uri;
import com.google.android.gms.common.GooglePlayServicesUtil;

public final class y {
    private static final Uri aY = new Uri.Builder().scheme("android.resource").authority(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE).appendPath("drawable").build();

    public static Uri i(String str) {
        x.b(str, "Resource name must not be null.");
        return aY.buildUpon().appendPath(str).build();
    }
}
