package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ad;

public class DetectedActivityCreator implements Parcelable.Creator<DetectedActivity> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void a(DetectedActivity detectedActivity, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, detectedActivity.eq);
        ad.c(parcel, 1000, detectedActivity.T);
        ad.c(parcel, 2, detectedActivity.er);
        ad.C(parcel, d);
    }

    public DetectedActivity createFromParcel(Parcel parcel) {
        DetectedActivity detectedActivity = new DetectedActivity();
        int c = ac.c(parcel);
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    detectedActivity.eq = ac.f(parcel, b);
                    break;
                case 2:
                    detectedActivity.er = ac.f(parcel, b);
                    break;
                case 1000:
                    detectedActivity.T = ac.f(parcel, b);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return detectedActivity;
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    public DetectedActivity[] newArray(int size) {
        return new DetectedActivity[size];
    }
}
