package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.plus.model.moments.ItemScope;
import com.google.android.gms.plus.model.moments.Moment;

public final class eh extends j implements Moment {
    private ef iS;

    public eh(k kVar, int i) {
        super(kVar, i);
    }

    private ef bS() {
        synchronized (this) {
            if (this.iS == null) {
                byte[] byteArray = getByteArray("momentImpl");
                Parcel obtain = Parcel.obtain();
                obtain.unmarshall(byteArray, 0, byteArray.length);
                obtain.setDataPosition(0);
                this.iS = ef.CREATOR.createFromParcel(obtain);
                obtain.recycle();
            }
        }
        return this.iS;
    }

    /* renamed from: bR */
    public ef freeze() {
        return bS();
    }

    public String getId() {
        return bS().getId();
    }

    public ItemScope getResult() {
        return bS().getResult();
    }

    public String getStartDate() {
        return bS().getStartDate();
    }

    public ItemScope getTarget() {
        return bS().getTarget();
    }

    public String getType() {
        return bS().getType();
    }

    public boolean hasId() {
        return bS().hasId();
    }

    public boolean hasResult() {
        return bS().hasId();
    }

    public boolean hasStartDate() {
        return bS().hasStartDate();
    }

    public boolean hasTarget() {
        return bS().hasTarget();
    }

    public boolean hasType() {
        return bS().hasType();
    }
}
