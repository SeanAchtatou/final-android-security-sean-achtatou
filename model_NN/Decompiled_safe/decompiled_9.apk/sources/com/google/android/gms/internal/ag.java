package com.google.android.gms.internal;

import android.os.Parcel;

public final class ag implements ae {
    public static final ah CREATOR = new ah();
    private final int T;
    private final int bi;
    private final int bj;
    private final String bk;
    private final String bl;
    private final String bm;
    private final String bn;

    public ag(int i, int i2, int i3, String str, String str2, String str3, String str4) {
        this.T = i;
        this.bi = i2;
        this.bj = i3;
        this.bk = str;
        this.bl = str2;
        this.bm = str3;
        this.bn = str4;
    }

    public boolean A() {
        return this.bi == 2;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ag)) {
            return false;
        }
        ag agVar = (ag) obj;
        return this.T == agVar.T && this.bi == agVar.bi && this.bj == agVar.bj && w.a(this.bk, agVar.bk) && w.a(this.bl, agVar.bl);
    }

    public String getDisplayName() {
        return this.bm;
    }

    public int getType() {
        return this.bi;
    }

    public int hashCode() {
        return w.hashCode(Integer.valueOf(this.T), Integer.valueOf(this.bi), Integer.valueOf(this.bj), this.bk, this.bl);
    }

    public String toString() {
        if (A()) {
            return String.format("Person [%s] %s", x(), getDisplayName());
        } else if (z()) {
            return String.format("Circle [%s] %s", w(), getDisplayName());
        } else {
            return String.format("Group [%s] %s", w(), getDisplayName());
        }
    }

    public int u() {
        return this.T;
    }

    public int v() {
        return this.bj;
    }

    public String w() {
        return this.bk;
    }

    public void writeToParcel(Parcel out, int flags) {
        ah.a(this, out, flags);
    }

    public String x() {
        return this.bl;
    }

    public String y() {
        return this.bn;
    }

    public boolean z() {
        return this.bi == 1 && this.bj == -1;
    }
}
