package com.google.android.gms.games;

public final class GamesClientSettings {
    public static final int POPUP_POSITION_BOTTOM = 2;
    public static final int POPUP_POSITION_TOP = 1;

    private GamesClientSettings() {
    }
}
