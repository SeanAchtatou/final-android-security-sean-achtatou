package com.google.android.gms.maps;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.dynamic.LifecycleDelegate;
import com.google.android.gms.internal.bb;
import com.google.android.gms.internal.bd;
import com.google.android.gms.internal.be;
import com.google.android.gms.internal.cw;
import com.google.android.gms.internal.x;
import com.google.android.gms.maps.internal.IMapViewDelegate;
import com.google.android.gms.maps.model.RuntimeRemoteException;

public class MapView extends FrameLayout {
    private GoogleMap ft;
    private final b fx;

    static class a implements LifecycleDelegate {
        private View fA;
        private final ViewGroup fy;
        private final IMapViewDelegate fz;

        public a(ViewGroup viewGroup, IMapViewDelegate iMapViewDelegate) {
            this.fz = (IMapViewDelegate) x.d(iMapViewDelegate);
            this.fy = (ViewGroup) x.d(viewGroup);
        }

        public IMapViewDelegate aQ() {
            return this.fz;
        }

        public void onCreate(Bundle savedInstanceState) {
            try {
                this.fz.onCreate(savedInstanceState);
                this.fA = (View) bd.a(this.fz.getView());
                this.fy.removeAllViews();
                this.fy.addView(this.fA);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            throw new UnsupportedOperationException("onCreateView not allowed on MapViewDelegate");
        }

        public void onDestroy() {
            try {
                this.fz.onDestroy();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onDestroyView() {
            throw new UnsupportedOperationException("onDestroyView not allowed on MapViewDelegate");
        }

        public void onInflate(Activity activity, Bundle attrs, Bundle savedInstanceState) {
            throw new UnsupportedOperationException("onInflate not allowed on MapViewDelegate");
        }

        public void onLowMemory() {
            try {
                this.fz.onLowMemory();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onPause() {
            try {
                this.fz.onPause();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onResume() {
            try {
                this.fz.onResume();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onSaveInstanceState(Bundle outState) {
            try {
                this.fz.onSaveInstanceState(outState);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
    }

    static class b extends bb<a> {
        private final ViewGroup fB;
        private final GoogleMapOptions fC;
        protected be<a> fw;
        private final Context mContext;

        b(ViewGroup viewGroup, Context context, GoogleMapOptions googleMapOptions) {
            this.fB = viewGroup;
            this.mContext = context;
            this.fC = googleMapOptions;
        }

        /* access modifiers changed from: protected */
        public void a(be<a> beVar) {
            this.fw = beVar;
            aP();
        }

        public void aP() {
            if (this.fw != null && ag() == null) {
                try {
                    this.fw.a(new a(this.fB, cw.g(this.mContext).a(bd.f(this.mContext), this.fC)));
                } catch (RemoteException e) {
                    throw new RuntimeRemoteException(e);
                } catch (GooglePlayServicesNotAvailableException e2) {
                }
            }
        }
    }

    public MapView(Context context) {
        super(context);
        this.fx = new b(this, context, null);
    }

    public MapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.fx = new b(this, context, GoogleMapOptions.createFromAttributes(context, attrs));
    }

    public MapView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.fx = new b(this, context, GoogleMapOptions.createFromAttributes(context, attrs));
    }

    public MapView(Context context, GoogleMapOptions options) {
        super(context);
        this.fx = new b(this, context, options);
    }

    public final GoogleMap getMap() {
        if (this.ft != null) {
            return this.ft;
        }
        this.fx.aP();
        if (this.fx.ag() == null) {
            return null;
        }
        try {
            this.ft = new GoogleMap(((a) this.fx.ag()).aQ().getMap());
            return this.ft;
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final void onCreate(Bundle savedInstanceState) {
        this.fx.onCreate(savedInstanceState);
        if (this.fx.ag() == null) {
            this.fx.a(this);
        }
    }

    public final void onDestroy() {
        this.fx.onDestroy();
    }

    public final void onLowMemory() {
        this.fx.onLowMemory();
    }

    public final void onPause() {
        this.fx.onPause();
    }

    public final void onResume() {
        this.fx.onResume();
    }

    public final void onSaveInstanceState(Bundle outState) {
        this.fx.onSaveInstanceState(outState);
    }
}
