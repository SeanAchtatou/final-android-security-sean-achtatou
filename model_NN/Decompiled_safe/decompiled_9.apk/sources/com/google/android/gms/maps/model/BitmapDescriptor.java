package com.google.android.gms.maps.model;

import com.google.android.gms.internal.bc;
import com.google.android.gms.internal.x;

public final class BitmapDescriptor {
    private final bc eR;

    public BitmapDescriptor(bc remoteObject) {
        this.eR = (bc) x.d(remoteObject);
    }

    public bc aD() {
        return this.eR;
    }
}
