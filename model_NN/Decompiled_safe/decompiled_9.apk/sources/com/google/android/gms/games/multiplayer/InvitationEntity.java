package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.internal.w;
import com.google.android.gms.internal.x;
import java.util.ArrayList;

public final class InvitationEntity implements Invitation {
    public static final Parcelable.Creator<InvitationEntity> CREATOR = new Parcelable.Creator<InvitationEntity>() {
        /* renamed from: C */
        public InvitationEntity[] newArray(int i) {
            return new InvitationEntity[i];
        }

        /* renamed from: p */
        public InvitationEntity createFromParcel(Parcel parcel) {
            GameEntity createFromParcel = GameEntity.CREATOR.createFromParcel(parcel);
            String readString = parcel.readString();
            long readLong = parcel.readLong();
            int readInt = parcel.readInt();
            Participant createFromParcel2 = ParticipantEntity.CREATOR.createFromParcel(parcel);
            int readInt2 = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt2);
            for (int i = 0; i < readInt2; i++) {
                arrayList.add(ParticipantEntity.CREATOR.createFromParcel(parcel));
            }
            return new InvitationEntity(createFromParcel, readString, readLong, readInt, createFromParcel2, arrayList);
        }
    };
    private final GameEntity dE;
    private final String dF;
    private final long dG;
    private final int dH;
    private final Participant dI;
    private final ArrayList<Participant> dJ;

    private InvitationEntity(GameEntity game, String invitationId, long creationTimestamp, int invitationType, Participant inviter, ArrayList<Participant> participants) {
        this.dE = game;
        this.dF = invitationId;
        this.dG = creationTimestamp;
        this.dH = invitationType;
        this.dI = inviter;
        this.dJ = participants;
    }

    public InvitationEntity(Invitation invitation) {
        this.dE = new GameEntity(invitation.getGame());
        this.dF = invitation.getInvitationId();
        this.dG = invitation.getCreationTimestamp();
        this.dH = invitation.getInvitationType();
        String participantId = invitation.getInviter().getParticipantId();
        Participant participant = null;
        ArrayList<Participant> participants = invitation.getParticipants();
        int size = participants.size();
        this.dJ = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            Participant participant2 = participants.get(i);
            if (participant2.getParticipantId().equals(participantId)) {
                participant = participant2;
            }
            this.dJ.add(participant2.freeze());
        }
        x.b(participant, "Must have a valid inviter!");
        this.dI = (Participant) participant.freeze();
    }

    public static int a(Invitation invitation) {
        return w.hashCode(invitation.getGame(), invitation.getInvitationId(), Long.valueOf(invitation.getCreationTimestamp()), Integer.valueOf(invitation.getInvitationType()), invitation.getInviter(), invitation.getParticipants());
    }

    public static boolean a(Invitation invitation, Object obj) {
        if (!(obj instanceof Invitation)) {
            return false;
        }
        if (invitation == obj) {
            return true;
        }
        Invitation invitation2 = (Invitation) obj;
        return w.a(invitation2.getGame(), invitation.getGame()) && w.a(invitation2.getInvitationId(), invitation.getInvitationId()) && w.a(Long.valueOf(invitation2.getCreationTimestamp()), Long.valueOf(invitation.getCreationTimestamp())) && w.a(Integer.valueOf(invitation2.getInvitationType()), Integer.valueOf(invitation.getInvitationType())) && w.a(invitation2.getInviter(), invitation.getInviter()) && w.a(invitation2.getParticipants(), invitation.getParticipants());
    }

    public static String b(Invitation invitation) {
        return w.c(invitation).a("Game", invitation.getGame()).a("InvitationId", invitation.getInvitationId()).a("CreationTimestamp", Long.valueOf(invitation.getCreationTimestamp())).a("InvitationType", Integer.valueOf(invitation.getInvitationType())).a("Inviter", invitation.getInviter()).a("Participants", invitation.getParticipants()).toString();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public Invitation freeze() {
        return this;
    }

    public long getCreationTimestamp() {
        return this.dG;
    }

    public Game getGame() {
        return this.dE;
    }

    public String getInvitationId() {
        return this.dF;
    }

    public int getInvitationType() {
        return this.dH;
    }

    public Participant getInviter() {
        return this.dI;
    }

    public ArrayList<Participant> getParticipants() {
        return this.dJ;
    }

    public int hashCode() {
        return a(this);
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel dest, int flags) {
        this.dE.writeToParcel(dest, flags);
        dest.writeString(this.dF);
        dest.writeLong(this.dG);
        dest.writeInt(this.dH);
        this.dI.writeToParcel(dest, flags);
        int size = this.dJ.size();
        dest.writeInt(size);
        for (int i = 0; i < size; i++) {
            this.dJ.get(i).writeToParcel(dest, flags);
        }
    }
}
