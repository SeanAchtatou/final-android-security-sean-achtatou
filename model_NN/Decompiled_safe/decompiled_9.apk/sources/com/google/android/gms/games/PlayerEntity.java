package com.google.android.gms.games;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ax;
import com.google.android.gms.internal.n;
import com.google.android.gms.internal.w;

public final class PlayerEntity implements Player {
    public static final Parcelable.Creator<PlayerEntity> CREATOR = new Parcelable.Creator<PlayerEntity>() {
        /* renamed from: o */
        public PlayerEntity createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            return new PlayerEntity(readString, readString2, readString3 == null ? null : Uri.parse(readString3), readString4 == null ? null : Uri.parse(readString4), parcel.readLong());
        }

        /* renamed from: w */
        public PlayerEntity[] newArray(int i) {
            return new PlayerEntity[i];
        }
    };
    private final String bm;
    private final Uri cj;
    private final Uri ck;
    private final String cw;
    private final long cx;

    public PlayerEntity(Player player) {
        this.cw = player.getPlayerId();
        this.bm = player.getDisplayName();
        this.cj = player.getIconImageUri();
        this.ck = player.getHiResImageUri();
        this.cx = player.getRetrievedTimestamp();
        n.b(this.cw);
        n.b(this.bm);
        n.a(this.cx > 0);
    }

    private PlayerEntity(String playerId, String displayName, Uri iconImageUri, Uri hiResImageUri, long retrievedTimestamp) {
        this.cw = playerId;
        this.bm = displayName;
        this.cj = iconImageUri;
        this.ck = hiResImageUri;
        this.cx = retrievedTimestamp;
    }

    public static int a(Player player) {
        return w.hashCode(player.getPlayerId(), player.getDisplayName(), player.getIconImageUri(), player.getHiResImageUri(), Long.valueOf(player.getRetrievedTimestamp()));
    }

    public static boolean a(Player player, Object obj) {
        if (!(obj instanceof Player)) {
            return false;
        }
        if (player == obj) {
            return true;
        }
        Player player2 = (Player) obj;
        return w.a(player2.getPlayerId(), player.getPlayerId()) && w.a(player2.getDisplayName(), player.getDisplayName()) && w.a(player2.getIconImageUri(), player.getIconImageUri()) && w.a(player2.getHiResImageUri(), player.getHiResImageUri()) && w.a(Long.valueOf(player2.getRetrievedTimestamp()), Long.valueOf(player.getRetrievedTimestamp()));
    }

    public static String b(Player player) {
        return w.c(player).a("PlayerId", player.getPlayerId()).a("DisplayName", player.getDisplayName()).a("IconImageUri", player.getIconImageUri()).a("HiResImageUri", player.getHiResImageUri()).a("RetrievedTimestamp", Long.valueOf(player.getRetrievedTimestamp())).toString();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public Player freeze() {
        return this;
    }

    public String getDisplayName() {
        return this.bm;
    }

    public void getDisplayName(CharArrayBuffer dataOut) {
        ax.b(this.bm, dataOut);
    }

    public Uri getHiResImageUri() {
        return this.ck;
    }

    public Uri getIconImageUri() {
        return this.cj;
    }

    public String getPlayerId() {
        return this.cw;
    }

    public long getRetrievedTimestamp() {
        return this.cx;
    }

    public boolean hasHiResImage() {
        return getHiResImageUri() != null;
    }

    public boolean hasIconImage() {
        return getIconImageUri() != null;
    }

    public int hashCode() {
        return a(this);
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel dest, int flags) {
        String str = null;
        dest.writeString(this.cw);
        dest.writeString(this.bm);
        dest.writeString(this.cj == null ? null : this.cj.toString());
        if (this.ck != null) {
            str = this.ck.toString();
        }
        dest.writeString(str);
        dest.writeLong(this.cx);
    }
}
