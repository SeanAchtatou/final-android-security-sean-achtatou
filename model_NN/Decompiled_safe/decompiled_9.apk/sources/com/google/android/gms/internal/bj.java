package com.google.android.gms.internal;

import android.content.Context;
import android.content.Intent;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.games.GameBuffer;
import com.google.android.gms.games.GamesClient;
import com.google.android.gms.games.OnGamesLoadedListener;
import com.google.android.gms.games.OnPlayersLoadedListener;
import com.google.android.gms.games.OnSignOutCompleteListener;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerBuffer;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.RealTimeSocket;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.OnAchievementUpdatedListener;
import com.google.android.gms.games.achievement.OnAchievementsLoadedListener;
import com.google.android.gms.games.leaderboard.LeaderboardBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.OnLeaderboardMetadataLoadedListener;
import com.google.android.gms.games.leaderboard.OnLeaderboardScoresLoadedListener;
import com.google.android.gms.games.leaderboard.OnScoreSubmittedListener;
import com.google.android.gms.games.leaderboard.SubmitScoreResult;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.InvitationBuffer;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.OnInvitationsLoadedListener;
import com.google.android.gms.games.multiplayer.ParticipantUtils;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.RealTimeReliableMessageSentListener;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.android.gms.internal.bm;
import com.google.android.gms.internal.p;
import com.google.android.gms.internal.t;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class bj extends p<bm> {
    private PlayerEntity cA;
    private final bn cB;
    /* access modifiers changed from: private */
    public boolean cC = false;
    private final Binder cD;
    private final long cE;
    private final boolean cF;
    private final String cy;
    private final Map<String, bo> cz;
    private final String g;

    abstract class a extends c {
        private final ArrayList<String> cG = new ArrayList<>();

        a(RoomStatusUpdateListener roomStatusUpdateListener, k kVar, String[] strArr) {
            super(roomStatusUpdateListener, kVar);
            for (String add : strArr) {
                this.cG.add(add);
            }
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            if (roomStatusUpdateListener != null) {
                a(roomStatusUpdateListener, room, this.cG);
            }
        }

        /* access modifiers changed from: protected */
        public abstract void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList);
    }

    final class aa extends a {
        aa(RoomStatusUpdateListener roomStatusUpdateListener, k kVar, String[] strArr) {
            super(roomStatusUpdateListener, kVar, strArr);
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onPeerInvitedToRoom(room, arrayList);
            }
        }
    }

    final class ab extends a {
        ab(RoomStatusUpdateListener roomStatusUpdateListener, k kVar, String[] strArr) {
            super(roomStatusUpdateListener, kVar, strArr);
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onPeerJoined(room, arrayList);
            }
        }
    }

    final class ac extends a {
        ac(RoomStatusUpdateListener roomStatusUpdateListener, k kVar, String[] strArr) {
            super(roomStatusUpdateListener, kVar, strArr);
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onPeerLeft(room, arrayList);
            }
        }
    }

    final class ad extends bi {
        private final OnPlayersLoadedListener cW;

        ad(OnPlayersLoadedListener onPlayersLoadedListener) {
            this.cW = (OnPlayersLoadedListener) x.b(onPlayersLoadedListener, "Listener must not be null");
        }

        public void e(k kVar) {
            bj.this.a(new ae(this.cW, kVar));
        }
    }

    final class ae extends p<bm>.c<OnPlayersLoadedListener> {
        ae(OnPlayersLoadedListener onPlayersLoadedListener, k kVar) {
            super(onPlayersLoadedListener, kVar);
        }

        /* access modifiers changed from: protected */
        public void a(OnPlayersLoadedListener onPlayersLoadedListener) {
            onPlayersLoadedListener.onPlayersLoaded(this.O.getStatusCode(), new PlayerBuffer(this.O));
        }
    }

    final class af extends p<bm>.b<RealTimeReliableMessageSentListener> {
        private final String cX;
        private final int cY;
        private final int p;

        af(RealTimeReliableMessageSentListener realTimeReliableMessageSentListener, int i, int i2, String str) {
            super(realTimeReliableMessageSentListener);
            this.p = i;
            this.cY = i2;
            this.cX = str;
        }

        public void a(RealTimeReliableMessageSentListener realTimeReliableMessageSentListener) {
            if (realTimeReliableMessageSentListener != null) {
                realTimeReliableMessageSentListener.onRealTimeMessageSent(this.p, this.cY, this.cX);
            }
        }
    }

    final class ag extends bi {
        final RealTimeReliableMessageSentListener cZ;

        public ag(RealTimeReliableMessageSentListener realTimeReliableMessageSentListener) {
            this.cZ = realTimeReliableMessageSentListener;
        }

        public void a(int i, int i2, String str) {
            bj.this.a(new af(this.cZ, i, i2, str));
        }
    }

    final class ah extends c {
        ah(RoomStatusUpdateListener roomStatusUpdateListener, k kVar) {
            super(roomStatusUpdateListener, kVar);
        }

        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onRoomAutoMatching(room);
            }
        }
    }

    final class ai extends bi {
        private final RoomUpdateListener da;
        private final RoomStatusUpdateListener db;
        private final RealTimeMessageReceivedListener dc;

        public ai(RoomUpdateListener roomUpdateListener) {
            this.da = (RoomUpdateListener) x.b(roomUpdateListener, "Callbacks must not be null");
            this.db = null;
            this.dc = null;
        }

        public ai(RoomUpdateListener roomUpdateListener, RoomStatusUpdateListener roomStatusUpdateListener, RealTimeMessageReceivedListener realTimeMessageReceivedListener) {
            this.da = (RoomUpdateListener) x.b(roomUpdateListener, "Callbacks must not be null");
            this.db = roomStatusUpdateListener;
            this.dc = realTimeMessageReceivedListener;
        }

        public void a(k kVar, String[] strArr) {
            bj.this.a(new aa(this.db, kVar, strArr));
        }

        public void b(k kVar, String[] strArr) {
            bj.this.a(new ab(this.db, kVar, strArr));
        }

        public void c(k kVar, String[] strArr) {
            bj.this.a(new ac(this.db, kVar, strArr));
        }

        public void d(k kVar, String[] strArr) {
            bj.this.a(new y(this.db, kVar, strArr));
        }

        public void e(k kVar, String[] strArr) {
            bj.this.a(new x(this.db, kVar, strArr));
        }

        public void f(k kVar, String[] strArr) {
            bj.this.a(new z(this.db, kVar, strArr));
        }

        public void n(k kVar) {
            bj.this.a(new al(this.da, kVar));
        }

        public void o(k kVar) {
            bj.this.a(new q(this.da, kVar));
        }

        public void onLeftRoom(int statusCode, String externalRoomId) {
            bj.this.a(new v(this.da, statusCode, externalRoomId));
        }

        public void onRealTimeMessageReceived(RealTimeMessage message) {
            bk.a("GamesClient", "RoomBinderCallbacks: onRealTimeMessageReceived");
            bj.this.a(new w(this.dc, message));
        }

        public void p(k kVar) {
            bj.this.a(new ak(this.db, kVar));
        }

        public void q(k kVar) {
            bj.this.a(new ah(this.db, kVar));
        }

        public void r(k kVar) {
            bj.this.a(new aj(this.da, kVar));
        }

        public void s(k kVar) {
            bj.this.a(new h(this.db, kVar));
        }

        public void t(k kVar) {
            bj.this.a(new i(this.db, kVar));
        }
    }

    final class aj extends b {
        aj(RoomUpdateListener roomUpdateListener, k kVar) {
            super(roomUpdateListener, kVar);
        }

        public void a(RoomUpdateListener roomUpdateListener, Room room) {
            if (roomUpdateListener != null) {
                roomUpdateListener.onRoomConnected(this.O.getStatusCode(), room);
            }
        }
    }

    final class ak extends c {
        ak(RoomStatusUpdateListener roomStatusUpdateListener, k kVar) {
            super(roomStatusUpdateListener, kVar);
        }

        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onRoomConnecting(room);
            }
        }
    }

    final class al extends b {
        public al(RoomUpdateListener roomUpdateListener, k kVar) {
            super(roomUpdateListener, kVar);
        }

        public void a(RoomUpdateListener roomUpdateListener, Room room) {
            roomUpdateListener.onRoomCreated(this.O.getStatusCode(), room);
        }
    }

    final class am extends bi {
        private final OnSignOutCompleteListener dd;

        public am(OnSignOutCompleteListener onSignOutCompleteListener) {
            this.dd = (OnSignOutCompleteListener) x.b(onSignOutCompleteListener, "Listener must not be null");
        }

        public void onSignOutComplete() {
            bj.this.a(new an(this.dd));
        }
    }

    final class an extends p<bm>.b<OnSignOutCompleteListener> {
        public an(OnSignOutCompleteListener onSignOutCompleteListener) {
            super(onSignOutCompleteListener);
        }

        public void a(OnSignOutCompleteListener onSignOutCompleteListener) {
            onSignOutCompleteListener.onSignOutComplete();
        }
    }

    final class ao extends bi {
        private final OnScoreSubmittedListener de;

        public ao(OnScoreSubmittedListener onScoreSubmittedListener) {
            this.de = (OnScoreSubmittedListener) x.b(onScoreSubmittedListener, "Listener must not be null");
        }

        public void d(k kVar) {
            bj.this.a(new ap(this.de, new SubmitScoreResult(kVar)));
        }
    }

    final class ap extends p<bm>.b<OnScoreSubmittedListener> {
        private final SubmitScoreResult df;

        public ap(OnScoreSubmittedListener onScoreSubmittedListener, SubmitScoreResult submitScoreResult) {
            super(onScoreSubmittedListener);
            this.df = submitScoreResult;
        }

        public void a(OnScoreSubmittedListener onScoreSubmittedListener) {
            onScoreSubmittedListener.onScoreSubmitted(this.df.getStatusCode(), this.df);
        }
    }

    abstract class b extends p<bm>.c<RoomUpdateListener> {
        b(RoomUpdateListener roomUpdateListener, k kVar) {
            super(roomUpdateListener, kVar);
        }

        /* access modifiers changed from: protected */
        public void a(RoomUpdateListener roomUpdateListener) {
            a(roomUpdateListener, bj.this.x(this.O));
        }

        /* access modifiers changed from: protected */
        public abstract void a(RoomUpdateListener roomUpdateListener, Room room);
    }

    abstract class c extends p<bm>.c<RoomStatusUpdateListener> {
        c(RoomStatusUpdateListener roomStatusUpdateListener, k kVar) {
            super(roomStatusUpdateListener, kVar);
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener) {
            if (roomStatusUpdateListener != null) {
                a(roomStatusUpdateListener, bj.this.x(this.O));
            }
        }

        /* access modifiers changed from: protected */
        public abstract void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room);
    }

    final class d extends bi {
        private final OnAchievementUpdatedListener cI;

        d(OnAchievementUpdatedListener onAchievementUpdatedListener) {
            this.cI = (OnAchievementUpdatedListener) x.b(onAchievementUpdatedListener, "Listener must not be null");
        }

        public void onAchievementUpdated(int statusCode, String achievementId) {
            bj.this.a(new e(this.cI, statusCode, achievementId));
        }
    }

    final class e extends p<bm>.b<OnAchievementUpdatedListener> {
        private final String cJ;
        private final int p;

        e(OnAchievementUpdatedListener onAchievementUpdatedListener, int i, String str) {
            super(onAchievementUpdatedListener);
            this.p = i;
            this.cJ = str;
        }

        /* access modifiers changed from: protected */
        public void a(OnAchievementUpdatedListener onAchievementUpdatedListener) {
            onAchievementUpdatedListener.onAchievementUpdated(this.p, this.cJ);
        }
    }

    final class f extends bi {
        private final OnAchievementsLoadedListener cK;

        f(OnAchievementsLoadedListener onAchievementsLoadedListener) {
            this.cK = (OnAchievementsLoadedListener) x.b(onAchievementsLoadedListener, "Listener must not be null");
        }

        public void b(k kVar) {
            bj.this.a(new g(this.cK, kVar));
        }
    }

    final class g extends p<bm>.c<OnAchievementsLoadedListener> {
        g(OnAchievementsLoadedListener onAchievementsLoadedListener, k kVar) {
            super(onAchievementsLoadedListener, kVar);
        }

        /* access modifiers changed from: protected */
        public void a(OnAchievementsLoadedListener onAchievementsLoadedListener) {
            onAchievementsLoadedListener.onAchievementsLoaded(this.O.getStatusCode(), new AchievementBuffer(this.O));
        }
    }

    final class h extends c {
        h(RoomStatusUpdateListener roomStatusUpdateListener, k kVar) {
            super(roomStatusUpdateListener, kVar);
        }

        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onConnectedToRoom(room);
            }
        }
    }

    final class i extends c {
        i(RoomStatusUpdateListener roomStatusUpdateListener, k kVar) {
            super(roomStatusUpdateListener, kVar);
        }

        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onDisconnectedFromRoom(room);
            }
        }
    }

    public final class j extends t.a {
        private final p.d cL;

        public j(p.d dVar) {
            this.cL = dVar;
        }

        public void a(int i, IBinder iBinder, Bundle bundle) {
            this.cL.a(i, iBinder, bundle);
            if (i == 0 && bundle != null) {
                boolean unused = bj.this.cC = bundle.getBoolean("show_welcome_popup");
            }
        }
    }

    final class k extends bi {
        private final OnGamesLoadedListener cM;

        k(OnGamesLoadedListener onGamesLoadedListener) {
            this.cM = (OnGamesLoadedListener) x.b(onGamesLoadedListener, "Listener must not be null");
        }

        public void g(k kVar) {
            bj.this.a(new l(this.cM, kVar));
        }
    }

    final class l extends p<bm>.c<OnGamesLoadedListener> {
        l(OnGamesLoadedListener onGamesLoadedListener, k kVar) {
            super(onGamesLoadedListener, kVar);
        }

        /* access modifiers changed from: protected */
        public void a(OnGamesLoadedListener onGamesLoadedListener) {
            onGamesLoadedListener.onGamesLoaded(this.O.getStatusCode(), new GameBuffer(this.O));
        }
    }

    final class m extends bi {
        private final OnInvitationReceivedListener cN;

        m(OnInvitationReceivedListener onInvitationReceivedListener) {
            this.cN = onInvitationReceivedListener;
        }

        public void k(k kVar) {
            InvitationBuffer invitationBuffer = new InvitationBuffer(kVar);
            Invitation invitation = null;
            try {
                if (invitationBuffer.getCount() > 0) {
                    invitation = (Invitation) ((Invitation) invitationBuffer.get(0)).freeze();
                }
                if (invitation != null) {
                    bj.this.a(new n(this.cN, invitation));
                }
            } finally {
                invitationBuffer.close();
            }
        }
    }

    final class n extends p<bm>.b<OnInvitationReceivedListener> {
        private final Invitation cO;

        n(OnInvitationReceivedListener onInvitationReceivedListener, Invitation invitation) {
            super(onInvitationReceivedListener);
            this.cO = invitation;
        }

        /* access modifiers changed from: protected */
        public void a(OnInvitationReceivedListener onInvitationReceivedListener) {
            onInvitationReceivedListener.onInvitationReceived(this.cO);
        }
    }

    final class o extends bi {
        private final OnInvitationsLoadedListener cP;

        o(OnInvitationsLoadedListener onInvitationsLoadedListener) {
            this.cP = onInvitationsLoadedListener;
        }

        public void j(k kVar) {
            bj.this.a(new p(this.cP, kVar));
        }
    }

    final class p extends p<bm>.c<OnInvitationsLoadedListener> {
        p(OnInvitationsLoadedListener onInvitationsLoadedListener, k kVar) {
            super(onInvitationsLoadedListener, kVar);
        }

        /* access modifiers changed from: protected */
        public void a(OnInvitationsLoadedListener onInvitationsLoadedListener) {
            onInvitationsLoadedListener.onInvitationsLoaded(this.O.getStatusCode(), new InvitationBuffer(this.O));
        }
    }

    final class q extends b {
        public q(RoomUpdateListener roomUpdateListener, k kVar) {
            super(roomUpdateListener, kVar);
        }

        public void a(RoomUpdateListener roomUpdateListener, Room room) {
            roomUpdateListener.onJoinedRoom(this.O.getStatusCode(), room);
        }
    }

    final class r extends bi {
        private final OnLeaderboardScoresLoadedListener cQ;

        r(OnLeaderboardScoresLoadedListener onLeaderboardScoresLoadedListener) {
            this.cQ = (OnLeaderboardScoresLoadedListener) x.b(onLeaderboardScoresLoadedListener, "Listener must not be null");
        }

        public void a(k kVar, k kVar2) {
            bj.this.a(new s(this.cQ, kVar, kVar2));
        }
    }

    final class s extends p<bm>.b<OnLeaderboardScoresLoadedListener> {
        private final k cR;
        private final k cS;

        s(OnLeaderboardScoresLoadedListener onLeaderboardScoresLoadedListener, k kVar, k kVar2) {
            super(onLeaderboardScoresLoadedListener);
            this.cR = kVar;
            this.cS = kVar2;
        }

        /* access modifiers changed from: protected */
        public void a(OnLeaderboardScoresLoadedListener onLeaderboardScoresLoadedListener) {
            onLeaderboardScoresLoadedListener.onLeaderboardScoresLoaded(this.cS.getStatusCode(), new LeaderboardBuffer(this.cR), new LeaderboardScoreBuffer(this.cS));
        }
    }

    final class t extends bi {
        private final OnLeaderboardMetadataLoadedListener cT;

        t(OnLeaderboardMetadataLoadedListener onLeaderboardMetadataLoadedListener) {
            this.cT = (OnLeaderboardMetadataLoadedListener) x.b(onLeaderboardMetadataLoadedListener, "Listener must not be null");
        }

        public void c(k kVar) {
            bj.this.a(new u(this.cT, kVar));
        }
    }

    final class u extends p<bm>.c<OnLeaderboardMetadataLoadedListener> {
        u(OnLeaderboardMetadataLoadedListener onLeaderboardMetadataLoadedListener, k kVar) {
            super(onLeaderboardMetadataLoadedListener, kVar);
        }

        /* access modifiers changed from: protected */
        public void a(OnLeaderboardMetadataLoadedListener onLeaderboardMetadataLoadedListener) {
            onLeaderboardMetadataLoadedListener.onLeaderboardMetadataLoaded(this.O.getStatusCode(), new LeaderboardBuffer(this.O));
        }
    }

    final class v extends p<bm>.b<RoomUpdateListener> {
        private final String cU;
        private final int p;

        v(RoomUpdateListener roomUpdateListener, int i, String str) {
            super(roomUpdateListener);
            this.p = i;
            this.cU = str;
        }

        public void a(RoomUpdateListener roomUpdateListener) {
            roomUpdateListener.onLeftRoom(this.p, this.cU);
        }
    }

    final class w extends p<bm>.b<RealTimeMessageReceivedListener> {
        private final RealTimeMessage cV;

        w(RealTimeMessageReceivedListener realTimeMessageReceivedListener, RealTimeMessage realTimeMessage) {
            super(realTimeMessageReceivedListener);
            this.cV = realTimeMessage;
        }

        public void a(RealTimeMessageReceivedListener realTimeMessageReceivedListener) {
            bk.a("GamesClient", "Deliver Message received callback");
            if (realTimeMessageReceivedListener != null) {
                realTimeMessageReceivedListener.onRealTimeMessageReceived(this.cV);
            }
        }
    }

    final class x extends a {
        x(RoomStatusUpdateListener roomStatusUpdateListener, k kVar, String[] strArr) {
            super(roomStatusUpdateListener, kVar, strArr);
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onPeersConnected(room, arrayList);
            }
        }
    }

    final class y extends a {
        y(RoomStatusUpdateListener roomStatusUpdateListener, k kVar, String[] strArr) {
            super(roomStatusUpdateListener, kVar, strArr);
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onPeerDeclined(room, arrayList);
            }
        }
    }

    final class z extends a {
        z(RoomStatusUpdateListener roomStatusUpdateListener, k kVar, String[] strArr) {
            super(roomStatusUpdateListener, kVar, strArr);
        }

        /* access modifiers changed from: protected */
        public void a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onPeersDisconnected(room, arrayList);
            }
        }
    }

    public bj(Context context, String str, String str2, GooglePlayServicesClient.ConnectionCallbacks connectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener onConnectionFailedListener, String[] strArr, int i2, View view, boolean z2) {
        super(context, connectionCallbacks, onConnectionFailedListener, strArr);
        this.cy = str;
        this.g = (String) x.d(str2);
        this.cD = new Binder();
        this.cz = new HashMap();
        this.cB = bn.a(this, i2);
        setViewForPopups(view);
        this.cE = (long) hashCode();
        this.cF = z2;
    }

    private void ah() {
        this.cA = null;
    }

    private void ai() {
        for (bo close : this.cz.values()) {
            try {
                close.close();
            } catch (IOException e2) {
                bk.a("GamesClient", "IOException:", e2);
            }
        }
        this.cz.clear();
    }

    private bo p(String str) {
        try {
            String r2 = ((bm) o()).r(str);
            if (r2 == null) {
                return null;
            }
            bk.d("GamesClient", "Creating a socket to bind to:" + r2);
            LocalSocket localSocket = new LocalSocket();
            try {
                localSocket.connect(new LocalSocketAddress(r2));
                bo boVar = new bo(localSocket, str);
                this.cz.put(str, boVar);
                return boVar;
            } catch (IOException e2) {
                bk.c("GamesClient", "connect() call failed on socket: " + e2.getMessage());
                return null;
            }
        } catch (RemoteException e3) {
            bk.c("GamesClient", "Unable to create socket. Service died.");
            return null;
        }
    }

    /* access modifiers changed from: private */
    public Room x(k kVar) {
        by byVar = new by(kVar);
        Room room = null;
        try {
            if (byVar.getCount() > 0) {
                room = (Room) ((Room) byVar.get(0)).freeze();
            }
            return room;
        } finally {
            byVar.close();
        }
    }

    public int a(byte[] bArr, String str, String[] strArr) {
        x.b(strArr, "Participant IDs must not be null");
        try {
            return ((bm) o()).b(bArr, str, strArr);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
            return -1;
        }
    }

    public void a(IBinder iBinder, Bundle bundle) {
        if (isConnected()) {
            try {
                ((bm) o()).a(iBinder, bundle);
            } catch (RemoteException e2) {
                bk.b("GamesClient", "service died");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(ConnectionResult connectionResult) {
        super.a(connectionResult);
        this.cC = false;
    }

    public void a(OnPlayersLoadedListener onPlayersLoadedListener, int i2, boolean z2, boolean z3) {
        try {
            ((bm) o()).a(new ad(onPlayersLoadedListener), i2, z2, z3);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void a(OnAchievementUpdatedListener onAchievementUpdatedListener, String str) {
        try {
            ((bm) o()).a(onAchievementUpdatedListener == null ? null : new d(onAchievementUpdatedListener), str, this.cB.ao(), this.cB.an());
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void a(OnAchievementUpdatedListener onAchievementUpdatedListener, String str, int i2) {
        try {
            ((bm) o()).a(onAchievementUpdatedListener == null ? null : new d(onAchievementUpdatedListener), str, i2, this.cB.ao(), this.cB.an());
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void a(OnScoreSubmittedListener onScoreSubmittedListener, String str, long j2) {
        try {
            ((bm) o()).a(onScoreSubmittedListener == null ? null : new ao(onScoreSubmittedListener), str, j2);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    /* access modifiers changed from: protected */
    public void a(u uVar, p<bm>.d dVar) throws RemoteException {
        String locale = getContext().getResources().getConfiguration().locale.toString();
        j jVar = new j(dVar);
        Bundle bundle = new Bundle();
        bundle.putBoolean("com.google.android.gms.games.key.isHeadless", this.cF);
        uVar.a(jVar, GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, getContext().getPackageName(), this.g, j(), this.cy, this.cB.ao(), locale, bundle);
    }

    /* access modifiers changed from: protected */
    public void a(String... strArr) {
        boolean z2 = false;
        boolean z3 = false;
        for (String str : strArr) {
            if (str.equals(Scopes.GAMES)) {
                z3 = true;
            } else if (str.equals("https://www.googleapis.com/auth/games.firstparty")) {
                z2 = true;
            }
        }
        if (z2) {
            x.a(!z3, String.format("Cannot have both %s and %s!", Scopes.GAMES, "https://www.googleapis.com/auth/games.firstparty"));
            return;
        }
        x.a(z3, String.format("GamesClient requires %s to function.", Scopes.GAMES));
    }

    public void aj() {
        if (isConnected()) {
            try {
                ((bm) o()).aj();
            } catch (RemoteException e2) {
                bk.b("GamesClient", "service died");
            }
        }
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "com.google.android.gms.games.service.START";
    }

    public void b(OnAchievementUpdatedListener onAchievementUpdatedListener, String str) {
        try {
            ((bm) o()).b(onAchievementUpdatedListener == null ? null : new d(onAchievementUpdatedListener), str, this.cB.ao(), this.cB.an());
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    /* access modifiers changed from: protected */
    public String c() {
        return "com.google.android.gms.games.internal.IGamesService";
    }

    public void clearNotifications(int notificationTypes) {
        try {
            ((bm) o()).clearNotifications(notificationTypes);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void connect() {
        ah();
        super.connect();
    }

    public void createRoom(RoomConfig config) {
        try {
            ((bm) o()).a(new ai(config.getRoomUpdateListener(), config.getRoomStatusUpdateListener(), config.getMessageReceivedListener()), this.cD, config.getVariant(), config.getInvitedPlayerIds(), config.getAutoMatchCriteria(), config.isSocketEnabled(), this.cE);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void disconnect() {
        this.cC = false;
        if (isConnected()) {
            try {
                bm bmVar = (bm) o();
                bmVar.aj();
                bmVar.b(this.cE);
                bmVar.a(this.cE);
            } catch (RemoteException e2) {
                bk.b("GamesClient", "Failed to notify client disconnect.");
            }
        }
        ai();
        super.disconnect();
    }

    public Intent getAchievementsIntent() {
        n();
        Intent intent = new Intent("com.google.android.gms.games.VIEW_ACHIEVEMENTS");
        intent.addFlags(67108864);
        return intent;
    }

    public Intent getAllLeaderboardsIntent() {
        n();
        Intent intent = new Intent("com.google.android.gms.games.VIEW_LEADERBOARDS");
        intent.putExtra("com.google.android.gms.games.GAME_PACKAGE_NAME", this.cy);
        intent.addFlags(67108864);
        return intent;
    }

    public String getAppId() {
        try {
            return ((bm) o()).getAppId();
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
            return null;
        }
    }

    public String getCurrentAccountName() {
        try {
            return ((bm) o()).getCurrentAccountName();
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
            return null;
        }
    }

    public Player getCurrentPlayer() {
        PlayerBuffer playerBuffer;
        n();
        synchronized (this) {
            if (this.cA == null) {
                try {
                    playerBuffer = new PlayerBuffer(((bm) o()).ak());
                    if (playerBuffer.getCount() > 0) {
                        this.cA = (PlayerEntity) playerBuffer.get(0).freeze();
                    }
                    playerBuffer.close();
                } catch (RemoteException e2) {
                    bk.b("GamesClient", "service died");
                } catch (Throwable th) {
                    playerBuffer.close();
                    throw th;
                }
            }
        }
        return this.cA;
    }

    public String getCurrentPlayerId() {
        try {
            return ((bm) o()).getCurrentPlayerId();
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
            return null;
        }
    }

    public Intent getInvitationInboxIntent() {
        n();
        Intent intent = new Intent("com.google.android.gms.games.SHOW_INVITATIONS");
        intent.putExtra("com.google.android.gms.games.GAME_PACKAGE_NAME", this.cy);
        return intent;
    }

    public Intent getLeaderboardIntent(String leaderboardId) {
        n();
        Intent intent = new Intent("com.google.android.gms.games.VIEW_LEADERBOARD_SCORES");
        intent.putExtra("com.google.android.gms.games.LEADERBOARD_ID", leaderboardId);
        intent.addFlags(67108864);
        return intent;
    }

    public RealTimeSocket getRealTimeSocketForParticipant(String roomId, String participantId) {
        if (participantId == null || !ParticipantUtils.v(participantId)) {
            throw new IllegalArgumentException("Bad participant ID");
        }
        bo boVar = this.cz.get(participantId);
        return (boVar == null || boVar.isClosed()) ? p(participantId) : boVar;
    }

    public Intent getRealTimeWaitingRoomIntent(Room room, int minParticipantsToStart) {
        n();
        Intent intent = new Intent("com.google.android.gms.games.SHOW_REAL_TIME_WAITING_ROOM");
        x.b(room, "Room parameter must not be null");
        intent.putExtra(GamesClient.EXTRA_ROOM, (Parcelable) room.freeze());
        x.a(minParticipantsToStart >= 0, "minParticipantsToStart must be >= 0");
        intent.putExtra("com.google.android.gms.games.MIN_PARTICIPANTS_TO_START", minParticipantsToStart);
        return intent;
    }

    public Intent getSelectPlayersIntent(int minPlayers, int maxPlayers) {
        n();
        Intent intent = new Intent("com.google.android.gms.games.SELECT_PLAYERS");
        intent.putExtra("com.google.android.gms.games.MIN_SELECTIONS", minPlayers);
        intent.putExtra("com.google.android.gms.games.MAX_SELECTIONS", maxPlayers);
        return intent;
    }

    public Intent getSettingsIntent() {
        n();
        Intent intent = new Intent("com.google.android.gms.games.SHOW_SETTINGS");
        intent.putExtra("com.google.android.gms.games.GAME_PACKAGE_NAME", this.cy);
        intent.addFlags(67108864);
        return intent;
    }

    public void h(String str, int i2) {
        try {
            ((bm) o()).h(str, i2);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void i(String str, int i2) {
        try {
            ((bm) o()).i(str, i2);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void joinRoom(RoomConfig config) {
        try {
            ((bm) o()).a(new ai(config.getRoomUpdateListener(), config.getRoomStatusUpdateListener(), config.getMessageReceivedListener()), this.cD, config.getInvitationId(), config.isSocketEnabled(), this.cE);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public bm c(IBinder iBinder) {
        return bm.a.m(iBinder);
    }

    /* access modifiers changed from: protected */
    public void k() {
        super.k();
        if (this.cC) {
            this.cB.am();
            this.cC = false;
        }
    }

    /* access modifiers changed from: protected */
    public Bundle l() {
        try {
            Bundle l2 = ((bm) o()).l();
            if (l2 == null) {
                return l2;
            }
            l2.setClassLoader(bj.class.getClassLoader());
            return l2;
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
            return null;
        }
    }

    public void leaveRoom(RoomUpdateListener listener, String roomId) {
        try {
            ((bm) o()).e(new ai(listener), roomId);
            ai();
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void loadAchievements(OnAchievementsLoadedListener listener) {
        try {
            ((bm) o()).c(new f(listener));
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void loadGame(OnGamesLoadedListener listener) {
        try {
            ((bm) o()).d(new k(listener));
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void loadInvitations(OnInvitationsLoadedListener listener) {
        try {
            ((bm) o()).e(new o(listener));
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void loadLeaderboardMetadata(OnLeaderboardMetadataLoadedListener listener) {
        try {
            ((bm) o()).b(new t(listener));
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void loadLeaderboardMetadata(OnLeaderboardMetadataLoadedListener listener, String leaderboardId) {
        try {
            ((bm) o()).d(new t(listener), leaderboardId);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void loadMoreScores(OnLeaderboardScoresLoadedListener listener, LeaderboardScoreBuffer buffer, int maxResults, int pageDirection) {
        try {
            ((bm) o()).a(new r(listener), buffer.aq().ar(), maxResults, pageDirection);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void loadPlayer(OnPlayersLoadedListener listener, String playerId) {
        try {
            ((bm) o()).c(new ad(listener), playerId);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void loadPlayerCenteredScores(OnLeaderboardScoresLoadedListener listener, String leaderboardId, int span, int leaderboardCollection, int maxResults, boolean forceReload) {
        try {
            ((bm) o()).b(new r(listener), leaderboardId, span, leaderboardCollection, maxResults, forceReload);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void loadTopScores(OnLeaderboardScoresLoadedListener listener, String leaderboardId, int span, int leaderboardCollection, int maxResults, boolean forceReload) {
        try {
            ((bm) o()).a(new r(listener), leaderboardId, span, leaderboardCollection, maxResults, forceReload);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void registerInvitationListener(OnInvitationReceivedListener listener) {
        try {
            ((bm) o()).a(new m(listener), this.cE);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public int sendReliableRealTimeMessage(RealTimeReliableMessageSentListener listener, byte[] messageData, String roomId, String recipientParticipantId) {
        try {
            return ((bm) o()).a(new ag(listener), messageData, roomId, recipientParticipantId);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
            return -1;
        }
    }

    public int sendUnreliableRealTimeMessageToAll(byte[] messageData, String roomId) {
        try {
            return ((bm) o()).b(messageData, roomId, (String[]) null);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
            return -1;
        }
    }

    public void setGravityForPopups(int gravity) {
        this.cB.setGravity(gravity);
    }

    public void setUseNewPlayerNotificationsFirstParty(boolean newPlayerStyle) {
        try {
            ((bm) o()).setUseNewPlayerNotificationsFirstParty(newPlayerStyle);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void setViewForPopups(View gamesContentView) {
        this.cB.a(gamesContentView);
    }

    public void signOut(OnSignOutCompleteListener listener) {
        try {
            ((bm) o()).a(listener == null ? null : new am(listener));
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }

    public void unregisterInvitationListener() {
        try {
            ((bm) o()).b(this.cE);
        } catch (RemoteException e2) {
            bk.b("GamesClient", "service died");
        }
    }
}
