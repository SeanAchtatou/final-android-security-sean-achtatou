package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.GoogleMapOptions;

public class ci {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.CameraPosition, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public static void a(GoogleMapOptions googleMapOptions, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, googleMapOptions.u());
        ad.a(parcel, 2, googleMapOptions.aG());
        ad.a(parcel, 3, googleMapOptions.aH());
        ad.c(parcel, 4, googleMapOptions.getMapType());
        ad.a(parcel, 5, (Parcelable) googleMapOptions.getCamera(), i, false);
        ad.a(parcel, 6, googleMapOptions.aI());
        ad.a(parcel, 7, googleMapOptions.aJ());
        ad.a(parcel, 8, googleMapOptions.aK());
        ad.a(parcel, 9, googleMapOptions.aL());
        ad.a(parcel, 10, googleMapOptions.aM());
        ad.a(parcel, 11, googleMapOptions.aN());
        ad.C(parcel, d);
    }
}
