package com.google.android.gms.plus.model.moments;

import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.internal.eh;
import com.google.android.gms.internal.k;

public final class MomentBuffer extends DataBuffer<Moment> {
    public MomentBuffer(k dataHolder) {
        super(dataHolder);
    }

    public Moment get(int position) {
        return new eh(this.O, position);
    }
}
