package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.widget.ImageView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.plus.PlusClient;
import java.io.IOException;

public class dt extends ImageView implements GooglePlayServicesClient.ConnectionCallbacks, PlusClient.a {
    private int gU;
    /* access modifiers changed from: private */
    public boolean gV;
    private boolean gW;
    /* access modifiers changed from: private */
    public Bitmap gX;
    private PlusClient gY;
    private Uri mUri;

    class a extends AsyncTask<ParcelFileDescriptor, Void, Bitmap> {
        private final int gU;

        a(int i) {
            this.gU = i;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Bitmap doInBackground(ParcelFileDescriptor... parcelFileDescriptorArr) {
            ParcelFileDescriptor parcelFileDescriptor = parcelFileDescriptorArr[0];
            try {
                Bitmap decodeFileDescriptor = BitmapFactory.decodeFileDescriptor(parcelFileDescriptor.getFileDescriptor());
                if (this.gU > 0) {
                    decodeFileDescriptor = dt.a(decodeFileDescriptor, this.gU);
                    try {
                    } catch (IOException e) {
                        Log.e("PlusImageView", "closed failed", e);
                    }
                } else {
                    try {
                        parcelFileDescriptor.close();
                    } catch (IOException e2) {
                        Log.e("PlusImageView", "closed failed", e2);
                    }
                }
                return decodeFileDescriptor;
            } finally {
                try {
                    parcelFileDescriptor.close();
                } catch (IOException e3) {
                    Log.e("PlusImageView", "closed failed", e3);
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void onPostExecute(Bitmap bitmap) {
            Bitmap unused = dt.this.gX = bitmap;
            if (dt.this.gV) {
                dt.this.setImageBitmap(dt.this.gX);
            }
        }
    }

    public dt(Context context) {
        super(context);
    }

    /* access modifiers changed from: private */
    public static Bitmap a(Bitmap bitmap, int i) {
        double width = (double) bitmap.getWidth();
        double height = (double) bitmap.getHeight();
        double d = width > height ? ((double) i) / width : ((double) i) / height;
        return Bitmap.createScaledBitmap(bitmap, (int) ((width * d) + 0.5d), (int) ((d * height) + 0.5d), true);
    }

    private void bc() {
        boolean z = this.mUri != null && "android.resource".equals(this.mUri.getScheme());
        if (this.gW) {
            if (this.mUri == null) {
                setImageBitmap(null);
            } else if (z || (this.gY != null && this.gY.isConnected())) {
                if (z) {
                    setImageURI(this.mUri);
                } else {
                    this.gY.a(this, this.mUri, this.gU);
                }
                this.gW = false;
            }
        }
    }

    public void a(Uri uri, int i) {
        boolean z = false;
        boolean equals = this.mUri == null ? uri == null : this.mUri.equals(uri);
        if (this.gU == i) {
            z = true;
        }
        if (!equals || !z) {
            this.mUri = uri;
            this.gU = i;
            this.gW = true;
            bc();
        }
    }

    public void a(ConnectionResult connectionResult, ParcelFileDescriptor parcelFileDescriptor) {
        if (connectionResult.isSuccess()) {
            this.gW = false;
            if (parcelFileDescriptor != null) {
                new a(this.gU).execute(parcelFileDescriptor);
            }
        }
    }

    public void a(PlusClient plusClient) {
        if (plusClient != this.gY) {
            if (this.gY != null && this.gY.isConnectionCallbacksRegistered(this)) {
                this.gY.unregisterConnectionCallbacks(this);
            }
            this.gY = plusClient;
            this.gY.registerConnectionCallbacks(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.gV = true;
        if (this.gY != null && !this.gY.isConnectionCallbacksRegistered(this)) {
            this.gY.registerConnectionCallbacks(this);
        }
        if (this.gX != null) {
            setImageBitmap(this.gX);
        }
    }

    public void onConnected(Bundle connectionHint) {
        bc();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.gV = false;
        if (this.gY != null && this.gY.isConnectionCallbacksRegistered(this)) {
            this.gY.unregisterConnectionCallbacks(this);
        }
    }

    public void onDisconnected() {
    }
}
