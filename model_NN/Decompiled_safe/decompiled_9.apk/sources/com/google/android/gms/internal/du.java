package com.google.android.gms.internal;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;

public class du {
    Bundle dq;

    public du(Bundle bundle) {
        this.dq = bundle;
    }

    public boolean bd() {
        return this.dq.getBoolean("has_plus_one", false);
    }

    public String be() {
        return this.dq.getString("bubble_text");
    }

    public String[] bf() {
        return this.dq.getStringArray("inline_annotations");
    }

    public Uri[] bg() {
        Parcelable[] parcelableArray = this.dq.getParcelableArray("profile_photo_uris");
        if (parcelableArray == null) {
            return null;
        }
        Uri[] uriArr = new Uri[parcelableArray.length];
        System.arraycopy(parcelableArray, 0, uriArr, 0, parcelableArray.length);
        return uriArr;
    }

    public Intent getIntent() {
        return (Intent) this.dq.getParcelable("intent");
    }
}
