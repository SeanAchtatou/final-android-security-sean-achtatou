package com.google.android.gms.location;

import android.content.Intent;
import android.os.Parcel;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.x;
import java.util.Collections;
import java.util.List;

public class ActivityRecognitionResult implements ae {
    public static final ActivityRecognitionResultCreator CREATOR = new ActivityRecognitionResultCreator();
    public static final String EXTRA_ACTIVITY_RESULT = "com.google.android.location.internal.EXTRA_ACTIVITY_RESULT";
    int T;
    List<DetectedActivity> en;
    long eo;
    long ep;

    public ActivityRecognitionResult() {
        this.T = 1;
    }

    public ActivityRecognitionResult(DetectedActivity mostProbableActivity, long time, long elapsedRealtimeMillis) {
        this(Collections.singletonList(mostProbableActivity), time, elapsedRealtimeMillis);
    }

    public ActivityRecognitionResult(List<DetectedActivity> probableActivities, long time, long elapsedRealtimeMillis) {
        this();
        x.b(probableActivities != null && probableActivities.size() > 0, "Must have at least 1 detected activity");
        this.en = probableActivities;
        this.eo = time;
        this.ep = elapsedRealtimeMillis;
    }

    public static ActivityRecognitionResult extractResult(Intent intent) {
        if (!hasResult(intent)) {
            return null;
        }
        return (ActivityRecognitionResult) intent.getExtras().get(EXTRA_ACTIVITY_RESULT);
    }

    public static boolean hasResult(Intent intent) {
        if (intent == null) {
            return false;
        }
        return intent.hasExtra(EXTRA_ACTIVITY_RESULT);
    }

    public int describeContents() {
        return 0;
    }

    public int getActivityConfidence(int activityType) {
        for (DetectedActivity next : this.en) {
            if (next.getType() == activityType) {
                return next.getConfidence();
            }
        }
        return 0;
    }

    public long getElapsedRealtimeMillis() {
        return this.ep;
    }

    public DetectedActivity getMostProbableActivity() {
        return this.en.get(0);
    }

    public List<DetectedActivity> getProbableActivities() {
        return this.en;
    }

    public long getTime() {
        return this.eo;
    }

    public String toString() {
        return "ActivityRecognitionResult [probableActivities=" + this.en + ", timeMillis=" + this.eo + ", elapsedRealtimeMillis=" + this.ep + "]";
    }

    public void writeToParcel(Parcel out, int flags) {
        ActivityRecognitionResultCreator.a(this, out, flags);
    }
}
