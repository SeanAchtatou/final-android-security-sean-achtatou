package com.google.android.gms.internal;

public final class bk {
    private static final s dg = new s("Games");

    public static void a(String str, String str2) {
        dg.a(str, str2);
    }

    public static void a(String str, String str2, Throwable th) {
        dg.a(str, str2, th);
    }

    public static void b(String str, String str2) {
        dg.b(str, str2);
    }

    public static void c(String str, String str2) {
        dg.c(str, str2);
    }

    public static void d(String str, String str2) {
        dg.d(str, str2);
    }
}
