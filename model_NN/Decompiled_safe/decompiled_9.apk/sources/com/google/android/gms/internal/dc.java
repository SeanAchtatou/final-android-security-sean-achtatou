package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.LatLng;

public class dc {
    public static void a(LatLng latLng, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, latLng.u());
        ad.a(parcel, 2, latLng.latitude);
        ad.a(parcel, 3, latLng.longitude);
        ad.C(parcel, d);
    }
}
