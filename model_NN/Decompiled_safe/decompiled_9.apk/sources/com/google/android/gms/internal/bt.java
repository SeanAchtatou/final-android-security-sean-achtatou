package com.google.android.gms.internal;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.leaderboard.LeaderboardScore;

public final class bt implements LeaderboardScore {
    private final long dr;
    private final String ds;
    private final String dt;
    private final long du;
    private final long dv;
    private final String dw;
    private final Uri dx;
    private final Uri dy;
    private final PlayerEntity dz;

    public bt(LeaderboardScore leaderboardScore) {
        this.dr = leaderboardScore.getRank();
        this.ds = (String) x.d(leaderboardScore.getDisplayRank());
        this.dt = (String) x.d(leaderboardScore.getDisplayScore());
        this.du = leaderboardScore.getRawScore();
        this.dv = leaderboardScore.getTimestampMillis();
        this.dw = leaderboardScore.getScoreHolderDisplayName();
        this.dx = leaderboardScore.getScoreHolderIconImageUri();
        this.dy = leaderboardScore.getScoreHolderHiResImageUri();
        Player scoreHolder = leaderboardScore.getScoreHolder();
        this.dz = scoreHolder == null ? null : (PlayerEntity) scoreHolder.freeze();
    }

    static int a(LeaderboardScore leaderboardScore) {
        return w.hashCode(Long.valueOf(leaderboardScore.getRank()), leaderboardScore.getDisplayRank(), Long.valueOf(leaderboardScore.getRawScore()), leaderboardScore.getDisplayScore(), Long.valueOf(leaderboardScore.getTimestampMillis()), leaderboardScore.getScoreHolderDisplayName(), leaderboardScore.getScoreHolderIconImageUri(), leaderboardScore.getScoreHolderHiResImageUri(), leaderboardScore.getScoreHolder());
    }

    static boolean a(LeaderboardScore leaderboardScore, Object obj) {
        if (!(obj instanceof LeaderboardScore)) {
            return false;
        }
        if (leaderboardScore == obj) {
            return true;
        }
        LeaderboardScore leaderboardScore2 = (LeaderboardScore) obj;
        return w.a(Long.valueOf(leaderboardScore2.getRank()), Long.valueOf(leaderboardScore.getRank())) && w.a(leaderboardScore2.getDisplayRank(), leaderboardScore.getDisplayRank()) && w.a(Long.valueOf(leaderboardScore2.getRawScore()), Long.valueOf(leaderboardScore.getRawScore())) && w.a(leaderboardScore2.getDisplayScore(), leaderboardScore.getDisplayScore()) && w.a(Long.valueOf(leaderboardScore2.getTimestampMillis()), Long.valueOf(leaderboardScore.getTimestampMillis())) && w.a(leaderboardScore2.getScoreHolderDisplayName(), leaderboardScore.getScoreHolderDisplayName()) && w.a(leaderboardScore2.getScoreHolderIconImageUri(), leaderboardScore.getScoreHolderIconImageUri()) && w.a(leaderboardScore2.getScoreHolderHiResImageUri(), leaderboardScore.getScoreHolderHiResImageUri()) && w.a(leaderboardScore2.getScoreHolder(), leaderboardScore.getScoreHolder());
    }

    static String b(LeaderboardScore leaderboardScore) {
        return w.c(leaderboardScore).a("Rank", Long.valueOf(leaderboardScore.getRank())).a("DisplayRank", leaderboardScore.getDisplayRank()).a("Score", Long.valueOf(leaderboardScore.getRawScore())).a("DisplayScore", leaderboardScore.getDisplayScore()).a("Timestamp", Long.valueOf(leaderboardScore.getTimestampMillis())).a("DisplayName", leaderboardScore.getScoreHolderDisplayName()).a("IconImageUri", leaderboardScore.getScoreHolderIconImageUri()).a("HiResImageUri", leaderboardScore.getScoreHolderHiResImageUri()).a("Player", leaderboardScore.getScoreHolder() == null ? null : leaderboardScore.getScoreHolder()).toString();
    }

    /* renamed from: as */
    public LeaderboardScore freeze() {
        return this;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public String getDisplayRank() {
        return this.ds;
    }

    public void getDisplayRank(CharArrayBuffer dataOut) {
        ax.b(this.ds, dataOut);
    }

    public String getDisplayScore() {
        return this.dt;
    }

    public void getDisplayScore(CharArrayBuffer dataOut) {
        ax.b(this.dt, dataOut);
    }

    public long getRank() {
        return this.dr;
    }

    public long getRawScore() {
        return this.du;
    }

    public Player getScoreHolder() {
        return this.dz;
    }

    public String getScoreHolderDisplayName() {
        return this.dz == null ? this.dw : this.dz.getDisplayName();
    }

    public void getScoreHolderDisplayName(CharArrayBuffer dataOut) {
        if (this.dz == null) {
            ax.b(this.dw, dataOut);
        } else {
            this.dz.getDisplayName(dataOut);
        }
    }

    public Uri getScoreHolderHiResImageUri() {
        return this.dz == null ? this.dy : this.dz.getHiResImageUri();
    }

    public Uri getScoreHolderIconImageUri() {
        return this.dz == null ? this.dx : this.dz.getIconImageUri();
    }

    public long getTimestampMillis() {
        return this.dv;
    }

    public int hashCode() {
        return a(this);
    }

    public String toString() {
        return b(this);
    }
}
