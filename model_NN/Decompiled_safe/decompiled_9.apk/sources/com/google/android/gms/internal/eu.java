package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.eq;
import java.util.HashSet;
import java.util.Set;

public class eu implements Parcelable.Creator<eq.i> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(eq.i iVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        Set<Integer> by = iVar.by();
        if (by.contains(1)) {
            ad.c(parcel, 1, iVar.u());
        }
        if (by.contains(2)) {
            ad.a(parcel, 2, iVar.isPrimary());
        }
        if (by.contains(3)) {
            ad.c(parcel, 3, iVar.getType());
        }
        if (by.contains(4)) {
            ad.a(parcel, 4, iVar.getValue(), true);
        }
        ad.C(parcel, d);
    }

    /* renamed from: H */
    public eq.i createFromParcel(Parcel parcel) {
        int i = 0;
        int c = ac.c(parcel);
        HashSet hashSet = new HashSet();
        String str = null;
        boolean z = false;
        int i2 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i2 = ac.f(parcel, b);
                    hashSet.add(1);
                    break;
                case 2:
                    z = ac.c(parcel, b);
                    hashSet.add(2);
                    break;
                case 3:
                    i = ac.f(parcel, b);
                    hashSet.add(3);
                    break;
                case 4:
                    str = ac.l(parcel, b);
                    hashSet.add(4);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new eq.i(hashSet, i2, z, i, str);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    /* renamed from: ab */
    public eq.i[] newArray(int i) {
        return new eq.i[i];
    }
}
