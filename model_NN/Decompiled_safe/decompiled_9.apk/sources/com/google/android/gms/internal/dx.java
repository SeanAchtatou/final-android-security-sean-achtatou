package com.google.android.gms.internal;

import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface dx extends IInterface {

    public static abstract class a extends Binder implements dx {

        /* renamed from: com.google.android.gms.internal.dx$a$a  reason: collision with other inner class name */
        static class C0029a implements dx {
            private IBinder a;

            C0029a(IBinder iBinder) {
                this.a = iBinder;
            }

            public void a(at atVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    if (atVar != null) {
                        obtain.writeInt(1);
                        atVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.a.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(dw dwVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    this.a.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(dw dwVar, int i, int i2, int i3, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    obtain.writeString(str);
                    this.a.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(dw dwVar, int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.a.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(dw dwVar, int i, String str, Uri uri, String str2, String str3) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str2);
                    obtain.writeString(str3);
                    this.a.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(dw dwVar, Uri uri, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.a.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(dw dwVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    this.a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(dw dwVar, String str, eb ebVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    if (ebVar != null) {
                        obtain.writeInt(1);
                        ebVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.a.transact(25, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(dw dwVar, String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(dw dwVar, String str, String str2, int i, String str3) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    obtain.writeString(str3);
                    this.a.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(dw dwVar, String str, List<String> list, List<String> list2, List<String> list3) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeStringList(list);
                    obtain.writeStringList(list2);
                    obtain.writeStringList(list3);
                    this.a.transact(23, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(dw dwVar, String str, boolean z) throws RemoteException {
                int i = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    this.a.transact(21, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(dw dwVar, boolean z, boolean z2) throws RemoteException {
                int i = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeInt(z ? 1 : 0);
                    if (!z2) {
                        i = 0;
                    }
                    obtain.writeInt(i);
                    this.a.transact(22, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.a;
            }

            public void b(dw dwVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    this.a.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(dw dwVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    this.a.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(dw dwVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    this.a.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(dw dwVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    this.a.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void clearDefaultAccount() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    this.a.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void d(dw dwVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    this.a.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void e(dw dwVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    this.a.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void f(dw dwVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    this.a.transact(24, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void f(String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.a.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void g(dw dwVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    this.a.transact(26, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getAccountName() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    this.a.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void removeMoment(String momentId) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeString(momentId);
                    this.a.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public static dx V(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.plus.internal.IPlusService");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof dx)) ? new C0029a(iBinder) : (dx) queryLocalInterface;
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: com.google.android.gms.internal.eb} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: android.net.Uri} */
        /* JADX WARN: Type inference failed for: r4v0 */
        /* JADX WARN: Type inference failed for: r4v6 */
        /* JADX WARN: Type inference failed for: r4v8 */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onTransact(int r9, android.os.Parcel r10, android.os.Parcel r11, int r12) throws android.os.RemoteException {
            /*
                r8 = this;
                r0 = 0
                r4 = 0
                r7 = 1
                switch(r9) {
                    case 1: goto L_0x0011;
                    case 2: goto L_0x0029;
                    case 3: goto L_0x0045;
                    case 4: goto L_0x005d;
                    case 5: goto L_0x0077;
                    case 6: goto L_0x0087;
                    case 7: goto L_0x0094;
                    case 8: goto L_0x00ad;
                    case 9: goto L_0x00c2;
                    case 10: goto L_0x00f8;
                    case 11: goto L_0x0111;
                    case 12: goto L_0x0126;
                    case 13: goto L_0x014c;
                    case 14: goto L_0x0161;
                    case 16: goto L_0x0196;
                    case 17: goto L_0x01bc;
                    case 18: goto L_0x01cd;
                    case 19: goto L_0x01e6;
                    case 20: goto L_0x01fb;
                    case 21: goto L_0x0218;
                    case 22: goto L_0x0238;
                    case 23: goto L_0x025d;
                    case 24: goto L_0x0283;
                    case 25: goto L_0x029c;
                    case 26: goto L_0x02c1;
                    case 1598968902: goto L_0x000b;
                    default: goto L_0x0006;
                }
            L_0x0006:
                boolean r7 = super.onTransact(r9, r10, r11, r12)
            L_0x000a:
                return r7
            L_0x000b:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r11.writeString(r0)
                goto L_0x000a
            L_0x0011:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r0 = com.google.android.gms.internal.dw.a.U(r0)
                java.lang.String r1 = r10.readString()
                r8.a(r0, r1)
                r11.writeNoException()
                goto L_0x000a
            L_0x0029:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r0 = com.google.android.gms.internal.dw.a.U(r0)
                java.lang.String r1 = r10.readString()
                java.lang.String r2 = r10.readString()
                r8.a(r0, r1, r2)
                r11.writeNoException()
                goto L_0x000a
            L_0x0045:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r0 = com.google.android.gms.internal.dw.a.U(r0)
                java.lang.String r1 = r10.readString()
                r8.b(r0, r1)
                r11.writeNoException()
                goto L_0x000a
            L_0x005d:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                int r0 = r10.readInt()
                if (r0 == 0) goto L_0x0075
                com.google.android.gms.internal.au r0 = com.google.android.gms.internal.at.CREATOR
                com.google.android.gms.internal.at r0 = r0.createFromParcel(r10)
            L_0x006e:
                r8.a(r0)
                r11.writeNoException()
                goto L_0x000a
            L_0x0075:
                r0 = r4
                goto L_0x006e
            L_0x0077:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                java.lang.String r0 = r8.getAccountName()
                r11.writeNoException()
                r11.writeString(r0)
                goto L_0x000a
            L_0x0087:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                r8.clearDefaultAccount()
                r11.writeNoException()
                goto L_0x000a
            L_0x0094:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r0 = com.google.android.gms.internal.dw.a.U(r0)
                java.lang.String r1 = r10.readString()
                r8.c(r0, r1)
                r11.writeNoException()
                goto L_0x000a
            L_0x00ad:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r0 = com.google.android.gms.internal.dw.a.U(r0)
                r8.a(r0)
                r11.writeNoException()
                goto L_0x000a
            L_0x00c2:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r2 = com.google.android.gms.internal.dw.a.U(r0)
                int r0 = r10.readInt()
                if (r0 == 0) goto L_0x00f4
                android.os.Parcelable$Creator r0 = android.net.Uri.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r10)
                android.net.Uri r0 = (android.net.Uri) r0
                r1 = r0
            L_0x00de:
                int r0 = r10.readInt()
                if (r0 == 0) goto L_0x00f6
                android.os.Parcelable$Creator r0 = android.os.Bundle.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r10)
                android.os.Bundle r0 = (android.os.Bundle) r0
            L_0x00ec:
                r8.a(r2, r1, r0)
                r11.writeNoException()
                goto L_0x000a
            L_0x00f4:
                r1 = r4
                goto L_0x00de
            L_0x00f6:
                r0 = r4
                goto L_0x00ec
            L_0x00f8:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r0 = com.google.android.gms.internal.dw.a.U(r0)
                java.lang.String r1 = r10.readString()
                r8.d(r0, r1)
                r11.writeNoException()
                goto L_0x000a
            L_0x0111:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                java.lang.String r0 = r10.readString()
                java.lang.String r1 = r10.readString()
                r8.f(r0, r1)
                r11.writeNoException()
                goto L_0x000a
            L_0x0126:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r1 = com.google.android.gms.internal.dw.a.U(r0)
                java.lang.String r2 = r10.readString()
                java.lang.String r3 = r10.readString()
                int r4 = r10.readInt()
                java.lang.String r5 = r10.readString()
                r0 = r8
                r0.a(r1, r2, r3, r4, r5)
                r11.writeNoException()
                goto L_0x000a
            L_0x014c:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r0 = com.google.android.gms.internal.dw.a.U(r0)
                r8.b(r0)
                r11.writeNoException()
                goto L_0x000a
            L_0x0161:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r1 = com.google.android.gms.internal.dw.a.U(r0)
                int r2 = r10.readInt()
                java.lang.String r3 = r10.readString()
                int r0 = r10.readInt()
                if (r0 == 0) goto L_0x0185
                android.os.Parcelable$Creator r0 = android.net.Uri.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r10)
                android.net.Uri r0 = (android.net.Uri) r0
                r4 = r0
            L_0x0185:
                java.lang.String r5 = r10.readString()
                java.lang.String r6 = r10.readString()
                r0 = r8
                r0.a(r1, r2, r3, r4, r5, r6)
                r11.writeNoException()
                goto L_0x000a
            L_0x0196:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r1 = com.google.android.gms.internal.dw.a.U(r0)
                int r2 = r10.readInt()
                int r3 = r10.readInt()
                int r4 = r10.readInt()
                java.lang.String r5 = r10.readString()
                r0 = r8
                r0.a(r1, r2, r3, r4, r5)
                r11.writeNoException()
                goto L_0x000a
            L_0x01bc:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                java.lang.String r0 = r10.readString()
                r8.removeMoment(r0)
                r11.writeNoException()
                goto L_0x000a
            L_0x01cd:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r0 = com.google.android.gms.internal.dw.a.U(r0)
                java.lang.String r1 = r10.readString()
                r8.e(r0, r1)
                r11.writeNoException()
                goto L_0x000a
            L_0x01e6:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r0 = com.google.android.gms.internal.dw.a.U(r0)
                r8.c(r0)
                r11.writeNoException()
                goto L_0x000a
            L_0x01fb:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r0 = com.google.android.gms.internal.dw.a.U(r0)
                int r1 = r10.readInt()
                java.lang.String r2 = r10.readString()
                r8.a(r0, r1, r2)
                r11.writeNoException()
                goto L_0x000a
            L_0x0218:
                java.lang.String r1 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r1)
                android.os.IBinder r1 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r1 = com.google.android.gms.internal.dw.a.U(r1)
                java.lang.String r2 = r10.readString()
                int r3 = r10.readInt()
                if (r3 == 0) goto L_0x0230
                r0 = r7
            L_0x0230:
                r8.a(r1, r2, r0)
                r11.writeNoException()
                goto L_0x000a
            L_0x0238:
                java.lang.String r1 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r1)
                android.os.IBinder r1 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r2 = com.google.android.gms.internal.dw.a.U(r1)
                int r1 = r10.readInt()
                if (r1 == 0) goto L_0x025b
                r1 = r7
            L_0x024c:
                int r3 = r10.readInt()
                if (r3 == 0) goto L_0x0253
                r0 = r7
            L_0x0253:
                r8.a(r2, r1, r0)
                r11.writeNoException()
                goto L_0x000a
            L_0x025b:
                r1 = r0
                goto L_0x024c
            L_0x025d:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r1 = com.google.android.gms.internal.dw.a.U(r0)
                java.lang.String r2 = r10.readString()
                java.util.ArrayList r3 = r10.createStringArrayList()
                java.util.ArrayList r4 = r10.createStringArrayList()
                java.util.ArrayList r5 = r10.createStringArrayList()
                r0 = r8
                r0.a(r1, r2, r3, r4, r5)
                r11.writeNoException()
                goto L_0x000a
            L_0x0283:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r0 = com.google.android.gms.internal.dw.a.U(r0)
                java.lang.String r1 = r10.readString()
                r8.f(r0, r1)
                r11.writeNoException()
                goto L_0x000a
            L_0x029c:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r0 = com.google.android.gms.internal.dw.a.U(r0)
                java.lang.String r1 = r10.readString()
                int r2 = r10.readInt()
                if (r2 == 0) goto L_0x02b9
                com.google.android.gms.internal.ec r2 = com.google.android.gms.internal.eb.CREATOR
                com.google.android.gms.internal.eb r4 = r2.createFromParcel(r10)
            L_0x02b9:
                r8.a(r0, r1, r4)
                r11.writeNoException()
                goto L_0x000a
            L_0x02c1:
                java.lang.String r0 = "com.google.android.gms.plus.internal.IPlusService"
                r10.enforceInterface(r0)
                android.os.IBinder r0 = r10.readStrongBinder()
                com.google.android.gms.internal.dw r0 = com.google.android.gms.internal.dw.a.U(r0)
                java.lang.String r1 = r10.readString()
                r8.g(r0, r1)
                r11.writeNoException()
                goto L_0x000a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.dx.a.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
        }
    }

    void a(at atVar) throws RemoteException;

    void a(dw dwVar) throws RemoteException;

    void a(dw dwVar, int i, int i2, int i3, String str) throws RemoteException;

    void a(dw dwVar, int i, String str) throws RemoteException;

    void a(dw dwVar, int i, String str, Uri uri, String str2, String str3) throws RemoteException;

    void a(dw dwVar, Uri uri, Bundle bundle) throws RemoteException;

    void a(dw dwVar, String str) throws RemoteException;

    void a(dw dwVar, String str, eb ebVar) throws RemoteException;

    void a(dw dwVar, String str, String str2) throws RemoteException;

    void a(dw dwVar, String str, String str2, int i, String str3) throws RemoteException;

    void a(dw dwVar, String str, List<String> list, List<String> list2, List<String> list3) throws RemoteException;

    void a(dw dwVar, String str, boolean z) throws RemoteException;

    void a(dw dwVar, boolean z, boolean z2) throws RemoteException;

    void b(dw dwVar) throws RemoteException;

    void b(dw dwVar, String str) throws RemoteException;

    void c(dw dwVar) throws RemoteException;

    void c(dw dwVar, String str) throws RemoteException;

    void clearDefaultAccount() throws RemoteException;

    void d(dw dwVar, String str) throws RemoteException;

    void e(dw dwVar, String str) throws RemoteException;

    void f(dw dwVar, String str) throws RemoteException;

    void f(String str, String str2) throws RemoteException;

    void g(dw dwVar, String str) throws RemoteException;

    String getAccountName() throws RemoteException;

    void removeMoment(String str) throws RemoteException;
}
