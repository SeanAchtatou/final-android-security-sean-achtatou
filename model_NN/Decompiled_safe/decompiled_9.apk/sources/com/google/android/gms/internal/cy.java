package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.CameraPosition;

public class cy {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public static void a(CameraPosition cameraPosition, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, cameraPosition.u());
        ad.a(parcel, 2, (Parcelable) cameraPosition.target, i, false);
        ad.a(parcel, 3, cameraPosition.zoom);
        ad.a(parcel, 4, cameraPosition.tilt);
        ad.a(parcel, 5, cameraPosition.bearing);
        ad.C(parcel, d);
    }
}
