package com.google.android.gms.games.leaderboard;

import com.google.android.gms.internal.bq;
import com.google.android.gms.internal.k;
import com.google.android.gms.internal.w;
import com.google.android.gms.internal.x;
import java.util.HashMap;

public final class SubmitScoreResult {
    private static final String[] dB = {"leaderboardId", "playerId", "timeSpan", "hasResult", "rawScore", "formattedScore", "newBest"};
    private String cw;
    private String dC;
    private HashMap<Integer, Result> dD;
    private int p;

    public static final class Result {
        public final String formattedScore;
        public final boolean newBest;
        public final long rawScore;

        public Result(long rawScore2, String formattedScore2, boolean newBest2) {
            this.rawScore = rawScore2;
            this.formattedScore = formattedScore2;
            this.newBest = newBest2;
        }

        public String toString() {
            return w.c(this).a("RawScore", Long.valueOf(this.rawScore)).a("FormattedScore", this.formattedScore).a("NewBest", Boolean.valueOf(this.newBest)).toString();
        }
    }

    public SubmitScoreResult(int statusCode, String leaderboardId, String playerId) {
        this(statusCode, leaderboardId, playerId, new HashMap());
    }

    public SubmitScoreResult(int statusCode, String leaderboardId, String playerId, HashMap<Integer, Result> results) {
        this.p = statusCode;
        this.dC = leaderboardId;
        this.cw = playerId;
        this.dD = results;
    }

    public SubmitScoreResult(k dataHolder) {
        this.p = dataHolder.getStatusCode();
        this.dD = new HashMap<>();
        int count = dataHolder.getCount();
        x.c(count == 3);
        for (int i = 0; i < count; i++) {
            int d = dataHolder.d(i);
            if (i == 0) {
                this.dC = dataHolder.c("leaderboardId", i, d);
                this.cw = dataHolder.c("playerId", i, d);
            }
            if (dataHolder.d("hasResult", i, d)) {
                a(new Result(dataHolder.a("rawScore", i, d), dataHolder.c("formattedScore", i, d), dataHolder.d("newBest", i, d)), dataHolder.b("timeSpan", i, d));
            }
        }
    }

    private void a(Result result, int i) {
        this.dD.put(Integer.valueOf(i), result);
    }

    public String getLeaderboardId() {
        return this.dC;
    }

    public String getPlayerId() {
        return this.cw;
    }

    public Result getScoreResult(int timeSpan) {
        return this.dD.get(Integer.valueOf(timeSpan));
    }

    public int getStatusCode() {
        return this.p;
    }

    public String toString() {
        w.a a = w.c(this).a("PlayerId", this.cw).a("StatusCode", Integer.valueOf(this.p));
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= 3) {
                return a.toString();
            }
            Result result = this.dD.get(Integer.valueOf(i2));
            a.a("TimesSpan", bq.B(i2));
            a.a("Result", result == null ? "null" : result.toString());
            i = i2 + 1;
        }
    }
}
