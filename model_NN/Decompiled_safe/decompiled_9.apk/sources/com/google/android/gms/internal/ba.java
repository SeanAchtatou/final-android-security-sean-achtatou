package com.google.android.gms.internal;

import android.os.Build;

public final class ba {
    public static boolean aa() {
        return t(11);
    }

    public static boolean ab() {
        return t(12);
    }

    public static boolean ac() {
        return t(13);
    }

    public static boolean ad() {
        return t(14);
    }

    public static boolean ae() {
        return t(16);
    }

    public static boolean af() {
        return t(17);
    }

    private static boolean t(int i) {
        return Build.VERSION.SDK_INT >= i;
    }
}
