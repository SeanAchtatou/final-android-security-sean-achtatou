package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.bc;
import com.google.android.gms.internal.cx;
import com.google.android.gms.internal.dd;

public final class MarkerOptions implements ae {
    public static final MarkerOptionsCreator CREATOR = new MarkerOptionsCreator();
    private final int T;
    private boolean fV;
    private float gd;
    private float ge;
    private LatLng gk;
    private String gl;
    private String gm;
    private BitmapDescriptor gn;
    private boolean go;

    public MarkerOptions() {
        this.gd = 0.5f;
        this.ge = 1.0f;
        this.fV = true;
        this.T = 1;
    }

    MarkerOptions(int versionCode, LatLng position, String title, String snippet, IBinder wrappedIcon, float anchorU, float anchorV, boolean draggable, boolean visible) {
        this.gd = 0.5f;
        this.ge = 1.0f;
        this.fV = true;
        this.T = versionCode;
        this.gk = position;
        this.gl = title;
        this.gm = snippet;
        this.gn = wrappedIcon == null ? null : new BitmapDescriptor(bc.a.j(wrappedIcon));
        this.gd = anchorU;
        this.ge = anchorV;
        this.go = draggable;
        this.fV = visible;
    }

    public IBinder aY() {
        if (this.gn == null) {
            return null;
        }
        return this.gn.aD().asBinder();
    }

    public MarkerOptions anchor(float u, float v) {
        this.gd = u;
        this.ge = v;
        return this;
    }

    public int describeContents() {
        return 0;
    }

    public MarkerOptions draggable(boolean draggable) {
        this.go = draggable;
        return this;
    }

    public float getAnchorU() {
        return this.gd;
    }

    public float getAnchorV() {
        return this.ge;
    }

    public BitmapDescriptor getIcon() {
        return this.gn;
    }

    public LatLng getPosition() {
        return this.gk;
    }

    public String getSnippet() {
        return this.gm;
    }

    public String getTitle() {
        return this.gl;
    }

    public MarkerOptions icon(BitmapDescriptor icon) {
        this.gn = icon;
        return this;
    }

    public boolean isDraggable() {
        return this.go;
    }

    public boolean isVisible() {
        return this.fV;
    }

    public MarkerOptions position(LatLng position) {
        this.gk = position;
        return this;
    }

    public MarkerOptions snippet(String snippet) {
        this.gm = snippet;
        return this;
    }

    public MarkerOptions title(String title) {
        this.gl = title;
        return this;
    }

    public int u() {
        return this.T;
    }

    public MarkerOptions visible(boolean visible) {
        this.fV = visible;
        return this;
    }

    public void writeToParcel(Parcel out, int flags) {
        if (cx.aV()) {
            dd.a(this, out, flags);
        } else {
            MarkerOptionsCreator.a(this, out, flags);
        }
    }
}
