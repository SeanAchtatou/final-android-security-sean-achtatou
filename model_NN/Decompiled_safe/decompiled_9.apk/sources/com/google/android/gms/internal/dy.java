package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.dx;
import com.google.android.gms.internal.p;
import com.google.android.gms.internal.t;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.model.moments.Moment;
import com.google.android.gms.plus.model.moments.MomentBuffer;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import java.io.IOException;

public class dy extends p<dx> {
    private final String g;
    private final String[] gN;
    private final String ha;
    private final String hb;
    /* access modifiers changed from: private */
    public Person hc;
    private final String[] hd;

    final class a extends dv {
        private final PlusClient.OnMomentsLoadedListener he;

        public a(PlusClient.OnMomentsLoadedListener onMomentsLoadedListener) {
            this.he = onMomentsLoadedListener;
        }

        public void a(k kVar, String str, String str2) {
            if (kVar.h() != null) {
                kVar.h().getParcelable("pendingIntent");
            }
            dy.this.a(new b(this.he, new ConnectionResult(kVar.getStatusCode(), null), kVar, str, str2));
        }
    }

    final class b extends p<dx>.c<PlusClient.OnMomentsLoadedListener> {
        private final ConnectionResult hg;
        private final String hh;
        private final String hi;

        public b(PlusClient.OnMomentsLoadedListener onMomentsLoadedListener, ConnectionResult connectionResult, k kVar, String str, String str2) {
            super(onMomentsLoadedListener, kVar);
            this.hg = connectionResult;
            this.hh = str;
            this.hi = str2;
        }

        /* access modifiers changed from: protected */
        public void a(PlusClient.OnMomentsLoadedListener onMomentsLoadedListener) {
            if (onMomentsLoadedListener != null) {
                onMomentsLoadedListener.onMomentsLoaded(this.hg, new MomentBuffer(this.O), this.hh, this.hi);
            }
        }
    }

    final class c extends dv {
        private final PlusClient.a hj;

        public c(PlusClient.a aVar) {
            this.hj = aVar;
        }

        public void a(int i, Bundle bundle, ParcelFileDescriptor parcelFileDescriptor) {
            PendingIntent pendingIntent = null;
            if (bundle != null) {
                pendingIntent = (PendingIntent) bundle.getParcelable("pendingIntent");
            }
            dy.this.a(new d(this.hj, new ConnectionResult(i, pendingIntent), parcelFileDescriptor));
        }
    }

    final class d extends p<dx>.b<PlusClient.a> {
        private final ConnectionResult hg;
        private final ParcelFileDescriptor hk;

        public d(PlusClient.a aVar, ConnectionResult connectionResult, ParcelFileDescriptor parcelFileDescriptor) {
            super(aVar);
            this.hg = connectionResult;
            this.hk = parcelFileDescriptor;
        }

        public void a(PlusClient.a aVar) {
            if (aVar != null) {
                aVar.a(this.hg, this.hk);
                return;
            }
            try {
                this.hk.close();
            } catch (IOException e) {
                Log.e("PlusClientImpl", "failed close", e);
            }
        }

        public void q() {
            super.q();
        }
    }

    final class e extends dv {
        private final PlusClient.OnPeopleLoadedListener hl;

        public e(PlusClient.OnPeopleLoadedListener onPeopleLoadedListener) {
            this.hl = onPeopleLoadedListener;
        }

        public void a(k kVar, String str) {
            if (kVar.h() != null) {
                kVar.h().getParcelable("pendingIntent");
            }
            dy.this.a(new f(this.hl, new ConnectionResult(kVar.getStatusCode(), null), kVar, str));
        }
    }

    final class f extends p<dx>.c<PlusClient.OnPeopleLoadedListener> {
        private final ConnectionResult hg;
        private final String hh;

        public f(PlusClient.OnPeopleLoadedListener onPeopleLoadedListener, ConnectionResult connectionResult, k kVar, String str) {
            super(onPeopleLoadedListener, kVar);
            this.hg = connectionResult;
            this.hh = str;
        }

        /* access modifiers changed from: protected */
        public void a(PlusClient.OnPeopleLoadedListener onPeopleLoadedListener) {
            if (onPeopleLoadedListener != null) {
                onPeopleLoadedListener.onPeopleLoaded(this.hg, new PersonBuffer(this.O), this.hh);
            }
        }
    }

    final class g extends dv {
        private final PlusClient.OnPersonLoadedListener hm;

        public g(PlusClient.OnPersonLoadedListener onPersonLoadedListener) {
            this.hm = onPersonLoadedListener;
        }

        public void a(int i, Bundle bundle, at atVar) {
            ConnectionResult connectionResult = new ConnectionResult(i, bundle != null ? (PendingIntent) bundle.getParcelable("pendingIntent") : null);
            if (atVar == null) {
                dy.this.a(new h(this.hm, connectionResult, null));
            } else {
                dy.this.a(new h(this.hm, connectionResult, (eq) atVar.a(eq.CREATOR)));
            }
        }
    }

    final class h extends p<dx>.b<PlusClient.OnPersonLoadedListener> {
        private final ConnectionResult hg;
        private final Person hn;

        public h(PlusClient.OnPersonLoadedListener onPersonLoadedListener, ConnectionResult connectionResult, Person person) {
            super(onPersonLoadedListener);
            this.hg = connectionResult;
            this.hn = person;
        }

        /* access modifiers changed from: protected */
        public void a(PlusClient.OnPersonLoadedListener onPersonLoadedListener) {
            if (onPersonLoadedListener != null) {
                onPersonLoadedListener.onPersonLoaded(this.hg, this.hn);
            }
        }
    }

    public final class i extends t.a {
        private p.d cL;

        public i(p.d dVar) {
            this.cL = dVar;
        }

        public void a(int i, IBinder iBinder, Bundle bundle) {
            if (i == 0 && bundle != null && bundle.containsKey("loaded_person")) {
                Person unused = dy.this.hc = eq.d(bundle.getByteArray("loaded_person"));
            }
            this.cL.a(i, iBinder, bundle);
        }
    }

    final class j extends dv {
        private final PlusClient.b ho;

        public j(PlusClient.b bVar) {
            this.ho = bVar;
        }

        public void a(int i, Bundle bundle, Bundle bundle2) {
            du duVar = null;
            ConnectionResult connectionResult = new ConnectionResult(i, bundle != null ? (PendingIntent) bundle.getParcelable("pendingIntent") : null);
            if (bundle2 != null) {
                duVar = new du(bundle2);
            }
            dy.this.a(new k(this.ho, connectionResult, duVar));
        }
    }

    final class k extends p<dx>.b<PlusClient.b> {
        public final ConnectionResult gD;
        public final du hp;

        public k(PlusClient.b bVar, ConnectionResult connectionResult, du duVar) {
            super(bVar);
            this.gD = connectionResult;
            this.hp = duVar;
        }

        /* access modifiers changed from: protected */
        public void a(PlusClient.b bVar) {
            if (bVar != null) {
                bVar.a(this.gD, this.hp);
            }
        }
    }

    final class l extends dv {
        private final PlusClient.OnAccessRevokedListener hq;

        public l(PlusClient.OnAccessRevokedListener onAccessRevokedListener) {
            this.hq = onAccessRevokedListener;
        }

        public void b(int i, Bundle bundle) {
            PendingIntent pendingIntent = null;
            if (bundle != null) {
                pendingIntent = (PendingIntent) bundle.getParcelable("pendingIntent");
            }
            dy.this.a(new m(this.hq, new ConnectionResult(i, pendingIntent)));
        }
    }

    final class m extends p<dx>.b<PlusClient.OnAccessRevokedListener> {
        private final ConnectionResult hg;

        public m(PlusClient.OnAccessRevokedListener onAccessRevokedListener, ConnectionResult connectionResult) {
            super(onAccessRevokedListener);
            this.hg = connectionResult;
        }

        /* access modifiers changed from: protected */
        public void a(PlusClient.OnAccessRevokedListener onAccessRevokedListener) {
            dy.this.disconnect();
            if (onAccessRevokedListener != null) {
                onAccessRevokedListener.onAccessRevoked(this.hg);
            }
        }
    }

    public dy(Context context, String str, String str2, String str3, GooglePlayServicesClient.ConnectionCallbacks connectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener onConnectionFailedListener, String[] strArr, String[] strArr2, String[] strArr3) {
        super(context, connectionCallbacks, onConnectionFailedListener, strArr3);
        this.ha = str;
        this.hb = str2;
        this.g = str3;
        this.hd = strArr;
        this.gN = strArr2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: W */
    public dx c(IBinder iBinder) {
        return dx.a.V(iBinder);
    }

    /* access modifiers changed from: protected */
    public void a(u uVar, p<dx>.d dVar) throws RemoteException {
        Bundle bundle = new Bundle();
        bundle.putBoolean("skip_oob", false);
        bundle.putStringArray("request_visible_actions", this.hd);
        if (this.gN != null) {
            bundle.putStringArray("required_features", this.gN);
        }
        uVar.a(new i(dVar), GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, this.ha, this.hb, j(), this.g, bundle);
    }

    public void a(PlusClient.a aVar, Uri uri, int i2) {
        n();
        Bundle bundle = new Bundle();
        bundle.putInt("bounding_box", i2);
        c cVar = new c(aVar);
        try {
            ((dx) o()).a(cVar, uri, bundle);
        } catch (RemoteException e2) {
            cVar.a(8, (Bundle) null, (ParcelFileDescriptor) null);
        }
    }

    public void a(PlusClient.b bVar, String str) {
        n();
        j jVar = new j(bVar);
        try {
            ((dx) o()).a(jVar, str);
        } catch (RemoteException e2) {
            jVar.a(8, (Bundle) null, (Bundle) null);
        }
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "com.google.android.gms.plus.service.START";
    }

    /* access modifiers changed from: protected */
    public String c() {
        return "com.google.android.gms.plus.internal.IPlusService";
    }

    public void clearDefaultAccount() {
        n();
        try {
            this.hc = null;
            ((dx) o()).clearDefaultAccount();
        } catch (RemoteException e2) {
            throw new IllegalStateException(e2);
        }
    }

    public String getAccountName() {
        n();
        try {
            return ((dx) o()).getAccountName();
        } catch (RemoteException e2) {
            throw new IllegalStateException(e2);
        }
    }

    public Person getCurrentPerson() {
        n();
        return this.hc;
    }

    public void loadMoments(PlusClient.OnMomentsLoadedListener listener, int maxResults, String pageToken, Uri targetUrl, String type, String userId) {
        n();
        a aVar = new a(listener);
        try {
            ((dx) o()).a(aVar, maxResults, pageToken, targetUrl, type, userId);
        } catch (RemoteException e2) {
            aVar.a(k.e(8), (String) null, (String) null);
        }
    }

    public void loadPeople(PlusClient.OnPeopleLoadedListener listener, int collection, int orderBy, int maxResults, String pageToken) {
        n();
        e eVar = new e(listener);
        try {
            ((dx) o()).a(eVar, collection, orderBy, maxResults, pageToken);
        } catch (RemoteException e2) {
            eVar.a(k.e(8), null);
        }
    }

    public void loadPerson(PlusClient.OnPersonLoadedListener listener, String userId) {
        n();
        g gVar = new g(listener);
        try {
            ((dx) o()).e(gVar, userId);
        } catch (RemoteException e2) {
            gVar.a(8, (Bundle) null, (at) null);
        }
    }

    public void removeMoment(String momentId) {
        n();
        try {
            ((dx) o()).removeMoment(momentId);
        } catch (RemoteException e2) {
            throw new IllegalStateException(e2);
        }
    }

    public void revokeAccessAndDisconnect(PlusClient.OnAccessRevokedListener listener) {
        n();
        clearDefaultAccount();
        try {
            ((dx) o()).c(new l(listener));
        } catch (RemoteException e2) {
            throw new IllegalStateException(e2);
        }
    }

    public void writeMoment(Moment moment) {
        n();
        try {
            ((dx) o()).a(at.a((ef) moment));
        } catch (RemoteException e2) {
            throw new IllegalStateException(e2);
        }
    }
}
