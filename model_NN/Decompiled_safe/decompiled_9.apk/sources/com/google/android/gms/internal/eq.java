package com.google.android.gms.internal;

import android.os.Parcel;
import android.support.v4.util.TimeUtils;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.internal.an;
import com.google.android.gms.plus.PlusShare;
import com.google.android.gms.plus.model.people.Person;
import com.widgetizeme.UnsentStats;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class eq extends an implements ae, Person {
    public static final er CREATOR = new er();
    private static final HashMap<String, an.a<?, ?>> hR = new HashMap<>();
    private final int T;
    private String bm;
    private String hE;
    private final Set<Integer> hS;
    private String iT;
    private a iU;
    private String iV;
    private String iW;
    private int iX;
    private b iY;
    private String iZ;
    private String iw;
    private List<c> ja;
    private String jb;
    private int jc;
    private boolean jd;
    private d je;
    private boolean jf;
    private String jg;
    private e jh;
    private String ji;
    private int jj;
    private List<g> jk;
    private List<h> jl;
    private int jm;
    private int jn;
    private String jo;
    private List<i> jp;
    private boolean jq;

    public static final class a extends an implements ae, Person.AgeRange {
        public static final ei CREATOR = new ei();
        private static final HashMap<String, an.a<?, ?>> hR = new HashMap<>();
        private final int T;
        private final Set<Integer> hS;
        private int jr;
        private int js;

        static {
            hR.put("max", an.a.c("max", 2));
            hR.put("min", an.a.c("min", 3));
        }

        public a() {
            this.T = 1;
            this.hS = new HashSet();
        }

        a(Set<Integer> set, int i, int i2, int i3) {
            this.hS = set;
            this.T = i;
            this.jr = i2;
            this.js = i3;
        }

        public HashMap<String, an.a<?, ?>> G() {
            return hR;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.hS.contains(Integer.valueOf(aVar.N()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.N()) {
                case 2:
                    return Integer.valueOf(this.jr);
                case 3:
                    return Integer.valueOf(this.js);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.N());
            }
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> by() {
            return this.hS;
        }

        /* renamed from: cd */
        public a freeze() {
            return this;
        }

        public int describeContents() {
            ei eiVar = CREATOR;
            return 0;
        }

        public int getMax() {
            return this.jr;
        }

        public int getMin() {
            return this.js;
        }

        public boolean hasMax() {
            return this.hS.contains(2);
        }

        public boolean hasMin() {
            return this.hS.contains(3);
        }

        /* access modifiers changed from: protected */
        public Object j(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean k(String str) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public int u() {
            return this.T;
        }

        public void writeToParcel(Parcel out, int flags) {
            ei eiVar = CREATOR;
            ei.a(this, out, flags);
        }
    }

    public static final class b extends an implements ae, Person.Cover {
        public static final ej CREATOR = new ej();
        private static final HashMap<String, an.a<?, ?>> hR = new HashMap<>();
        private final int T;
        private final Set<Integer> hS;
        private a jt;
        private C0031b ju;
        private int jv;

        public static final class a extends an implements ae, Person.Cover.CoverInfo {
            public static final ek CREATOR = new ek();
            private static final HashMap<String, an.a<?, ?>> hR = new HashMap<>();
            private final int T;
            private final Set<Integer> hS;
            private int jw;
            private int jx;

            static {
                hR.put("leftImageOffset", an.a.c("leftImageOffset", 2));
                hR.put("topImageOffset", an.a.c("topImageOffset", 3));
            }

            public a() {
                this.T = 1;
                this.hS = new HashSet();
            }

            a(Set<Integer> set, int i, int i2, int i3) {
                this.hS = set;
                this.T = i;
                this.jw = i2;
                this.jx = i3;
            }

            public HashMap<String, an.a<?, ?>> G() {
                return hR;
            }

            /* access modifiers changed from: protected */
            public boolean a(an.a aVar) {
                return this.hS.contains(Integer.valueOf(aVar.N()));
            }

            /* access modifiers changed from: protected */
            public Object b(an.a aVar) {
                switch (aVar.N()) {
                    case 2:
                        return Integer.valueOf(this.jw);
                    case 3:
                        return Integer.valueOf(this.jx);
                    default:
                        throw new IllegalStateException("Unknown safe parcelable id=" + aVar.N());
                }
            }

            /* access modifiers changed from: package-private */
            public Set<Integer> by() {
                return this.hS;
            }

            /* renamed from: ch */
            public a freeze() {
                return this;
            }

            public int describeContents() {
                ek ekVar = CREATOR;
                return 0;
            }

            public boolean equals(Object obj) {
                if (!(obj instanceof a)) {
                    return false;
                }
                if (this == obj) {
                    return true;
                }
                a aVar = (a) obj;
                for (an.a next : hR.values()) {
                    if (a(next)) {
                        if (!aVar.a(next)) {
                            return false;
                        }
                        if (!b(next).equals(aVar.b(next))) {
                            return false;
                        }
                    } else if (aVar.a(next)) {
                        return false;
                    }
                }
                return true;
            }

            public int getLeftImageOffset() {
                return this.jw;
            }

            public int getTopImageOffset() {
                return this.jx;
            }

            public boolean hasLeftImageOffset() {
                return this.hS.contains(2);
            }

            public boolean hasTopImageOffset() {
                return this.hS.contains(3);
            }

            public int hashCode() {
                int i = 0;
                Iterator<an.a<?, ?>> it = hR.values().iterator();
                while (true) {
                    int i2 = i;
                    if (!it.hasNext()) {
                        return i2;
                    }
                    an.a next = it.next();
                    if (a(next)) {
                        i = b(next).hashCode() + i2 + next.N();
                    } else {
                        i = i2;
                    }
                }
            }

            /* access modifiers changed from: protected */
            public Object j(String str) {
                return null;
            }

            /* access modifiers changed from: protected */
            public boolean k(String str) {
                return false;
            }

            /* access modifiers changed from: package-private */
            public int u() {
                return this.T;
            }

            public void writeToParcel(Parcel out, int flags) {
                ek ekVar = CREATOR;
                ek.a(this, out, flags);
            }
        }

        /* renamed from: com.google.android.gms.internal.eq$b$b  reason: collision with other inner class name */
        public static final class C0031b extends an implements ae, Person.Cover.CoverPhoto {
            public static final el CREATOR = new el();
            private static final HashMap<String, an.a<?, ?>> hR = new HashMap<>();
            private final int T;
            private int gA;
            private int gB;
            private String hE;
            private final Set<Integer> hS;

            static {
                hR.put("height", an.a.c("height", 2));
                hR.put(PlusShare.KEY_CALL_TO_ACTION_URL, an.a.f(PlusShare.KEY_CALL_TO_ACTION_URL, 3));
                hR.put("width", an.a.c("width", 4));
            }

            public C0031b() {
                this.T = 1;
                this.hS = new HashSet();
            }

            C0031b(Set<Integer> set, int i, int i2, String str, int i3) {
                this.hS = set;
                this.T = i;
                this.gB = i2;
                this.hE = str;
                this.gA = i3;
            }

            public HashMap<String, an.a<?, ?>> G() {
                return hR;
            }

            /* access modifiers changed from: protected */
            public boolean a(an.a aVar) {
                return this.hS.contains(Integer.valueOf(aVar.N()));
            }

            /* access modifiers changed from: protected */
            public Object b(an.a aVar) {
                switch (aVar.N()) {
                    case 2:
                        return Integer.valueOf(this.gB);
                    case 3:
                        return this.hE;
                    case 4:
                        return Integer.valueOf(this.gA);
                    default:
                        throw new IllegalStateException("Unknown safe parcelable id=" + aVar.N());
                }
            }

            /* access modifiers changed from: package-private */
            public Set<Integer> by() {
                return this.hS;
            }

            /* renamed from: ci */
            public C0031b freeze() {
                return this;
            }

            public int describeContents() {
                el elVar = CREATOR;
                return 0;
            }

            public boolean equals(Object obj) {
                if (!(obj instanceof C0031b)) {
                    return false;
                }
                if (this == obj) {
                    return true;
                }
                C0031b bVar = (C0031b) obj;
                for (an.a next : hR.values()) {
                    if (a(next)) {
                        if (!bVar.a(next)) {
                            return false;
                        }
                        if (!b(next).equals(bVar.b(next))) {
                            return false;
                        }
                    } else if (bVar.a(next)) {
                        return false;
                    }
                }
                return true;
            }

            public int getHeight() {
                return this.gB;
            }

            public String getUrl() {
                return this.hE;
            }

            public int getWidth() {
                return this.gA;
            }

            public boolean hasHeight() {
                return this.hS.contains(2);
            }

            public boolean hasUrl() {
                return this.hS.contains(3);
            }

            public boolean hasWidth() {
                return this.hS.contains(4);
            }

            public int hashCode() {
                int i = 0;
                Iterator<an.a<?, ?>> it = hR.values().iterator();
                while (true) {
                    int i2 = i;
                    if (!it.hasNext()) {
                        return i2;
                    }
                    an.a next = it.next();
                    if (a(next)) {
                        i = b(next).hashCode() + i2 + next.N();
                    } else {
                        i = i2;
                    }
                }
            }

            /* access modifiers changed from: protected */
            public Object j(String str) {
                return null;
            }

            /* access modifiers changed from: protected */
            public boolean k(String str) {
                return false;
            }

            /* access modifiers changed from: package-private */
            public int u() {
                return this.T;
            }

            public void writeToParcel(Parcel out, int flags) {
                el elVar = CREATOR;
                el.a(this, out, flags);
            }
        }

        static {
            hR.put("coverInfo", an.a.a("coverInfo", 2, a.class));
            hR.put("coverPhoto", an.a.a("coverPhoto", 3, C0031b.class));
            hR.put("layout", an.a.a("layout", 4, new ak().b("banner", 0), false));
        }

        public b() {
            this.T = 1;
            this.hS = new HashSet();
        }

        b(Set<Integer> set, int i, a aVar, C0031b bVar, int i2) {
            this.hS = set;
            this.T = i;
            this.jt = aVar;
            this.ju = bVar;
            this.jv = i2;
        }

        public HashMap<String, an.a<?, ?>> G() {
            return hR;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.hS.contains(Integer.valueOf(aVar.N()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.N()) {
                case 2:
                    return this.jt;
                case 3:
                    return this.ju;
                case 4:
                    return Integer.valueOf(this.jv);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.N());
            }
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> by() {
            return this.hS;
        }

        /* access modifiers changed from: package-private */
        public a ce() {
            return this.jt;
        }

        /* access modifiers changed from: package-private */
        public C0031b cf() {
            return this.ju;
        }

        /* renamed from: cg */
        public b freeze() {
            return this;
        }

        public int describeContents() {
            ej ejVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            b bVar = (b) obj;
            for (an.a next : hR.values()) {
                if (a(next)) {
                    if (!bVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(bVar.b(next))) {
                        return false;
                    }
                } else if (bVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        public Person.Cover.CoverInfo getCoverInfo() {
            return this.jt;
        }

        public Person.Cover.CoverPhoto getCoverPhoto() {
            return this.ju;
        }

        public int getLayout() {
            return this.jv;
        }

        public boolean hasCoverInfo() {
            return this.hS.contains(2);
        }

        public boolean hasCoverPhoto() {
            return this.hS.contains(3);
        }

        public boolean hasLayout() {
            return this.hS.contains(4);
        }

        public int hashCode() {
            int i = 0;
            Iterator<an.a<?, ?>> it = hR.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                an.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.N();
                } else {
                    i = i2;
                }
            }
        }

        /* access modifiers changed from: protected */
        public Object j(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean k(String str) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public int u() {
            return this.T;
        }

        public void writeToParcel(Parcel out, int flags) {
            ej ejVar = CREATOR;
            ej.a(this, out, flags);
        }
    }

    public static final class c extends an implements ae, Person.Emails {
        public static final em CREATOR = new em();
        private static final HashMap<String, an.a<?, ?>> hR = new HashMap<>();
        private final int T;
        private int bi;
        private final Set<Integer> hS;
        private boolean jy;
        private String mValue;

        static {
            hR.put("primary", an.a.e("primary", 2));
            hR.put(ServerProtocol.DIALOG_PARAM_TYPE, an.a.a(ServerProtocol.DIALOG_PARAM_TYPE, 3, new ak().b("home", 0).b("work", 1).b("other", 2), false));
            hR.put("value", an.a.f("value", 4));
        }

        public c() {
            this.T = 1;
            this.hS = new HashSet();
        }

        c(Set<Integer> set, int i, boolean z, int i2, String str) {
            this.hS = set;
            this.T = i;
            this.jy = z;
            this.bi = i2;
            this.mValue = str;
        }

        public HashMap<String, an.a<?, ?>> G() {
            return hR;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.hS.contains(Integer.valueOf(aVar.N()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.N()) {
                case 2:
                    return Boolean.valueOf(this.jy);
                case 3:
                    return Integer.valueOf(this.bi);
                case 4:
                    return this.mValue;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.N());
            }
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> by() {
            return this.hS;
        }

        /* renamed from: cj */
        public c freeze() {
            return this;
        }

        public int describeContents() {
            em emVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof c)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            c cVar = (c) obj;
            for (an.a next : hR.values()) {
                if (a(next)) {
                    if (!cVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(cVar.b(next))) {
                        return false;
                    }
                } else if (cVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        public int getType() {
            return this.bi;
        }

        public String getValue() {
            return this.mValue;
        }

        public boolean hasPrimary() {
            return this.hS.contains(2);
        }

        public boolean hasType() {
            return this.hS.contains(3);
        }

        public boolean hasValue() {
            return this.hS.contains(4);
        }

        public int hashCode() {
            int i = 0;
            Iterator<an.a<?, ?>> it = hR.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                an.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.N();
                } else {
                    i = i2;
                }
            }
        }

        public boolean isPrimary() {
            return this.jy;
        }

        /* access modifiers changed from: protected */
        public Object j(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean k(String str) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public int u() {
            return this.T;
        }

        public void writeToParcel(Parcel out, int flags) {
            em emVar = CREATOR;
            em.a(this, out, flags);
        }
    }

    public static final class d extends an implements ae, Person.Image {
        public static final en CREATOR = new en();
        private static final HashMap<String, an.a<?, ?>> hR = new HashMap<>();
        private final int T;
        private String hE;
        private final Set<Integer> hS;

        static {
            hR.put(PlusShare.KEY_CALL_TO_ACTION_URL, an.a.f(PlusShare.KEY_CALL_TO_ACTION_URL, 2));
        }

        public d() {
            this.T = 1;
            this.hS = new HashSet();
        }

        public d(String str) {
            this.hS = new HashSet();
            this.T = 1;
            this.hE = str;
            this.hS.add(2);
        }

        d(Set<Integer> set, int i, String str) {
            this.hS = set;
            this.T = i;
            this.hE = str;
        }

        public HashMap<String, an.a<?, ?>> G() {
            return hR;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.hS.contains(Integer.valueOf(aVar.N()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.N()) {
                case 2:
                    break;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.N());
            }
            return this.hE;
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> by() {
            return this.hS;
        }

        /* renamed from: ck */
        public d freeze() {
            return this;
        }

        public int describeContents() {
            en enVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof d)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            d dVar = (d) obj;
            for (an.a next : hR.values()) {
                if (a(next)) {
                    if (!dVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(dVar.b(next))) {
                        return false;
                    }
                } else if (dVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        public String getUrl() {
            return this.hE;
        }

        public boolean hasUrl() {
            return this.hS.contains(2);
        }

        public int hashCode() {
            int i = 0;
            Iterator<an.a<?, ?>> it = hR.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                an.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.N();
                } else {
                    i = i2;
                }
            }
        }

        /* access modifiers changed from: protected */
        public Object j(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean k(String str) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public int u() {
            return this.T;
        }

        public void writeToParcel(Parcel out, int flags) {
            en enVar = CREATOR;
            en.a(this, out, flags);
        }
    }

    public static final class e extends an implements ae, Person.Name {
        public static final eo CREATOR = new eo();
        private static final HashMap<String, an.a<?, ?>> hR = new HashMap<>();
        private final int T;
        private final Set<Integer> hS;
        private String ir;
        private String iu;
        private String jA;
        private String jB;
        private String jC;
        private String jz;

        static {
            hR.put("familyName", an.a.f("familyName", 2));
            hR.put("formatted", an.a.f("formatted", 3));
            hR.put("givenName", an.a.f("givenName", 4));
            hR.put("honorificPrefix", an.a.f("honorificPrefix", 5));
            hR.put("honorificSuffix", an.a.f("honorificSuffix", 6));
            hR.put("middleName", an.a.f("middleName", 7));
        }

        public e() {
            this.T = 1;
            this.hS = new HashSet();
        }

        e(Set<Integer> set, int i, String str, String str2, String str3, String str4, String str5, String str6) {
            this.hS = set;
            this.T = i;
            this.ir = str;
            this.jz = str2;
            this.iu = str3;
            this.jA = str4;
            this.jB = str5;
            this.jC = str6;
        }

        public HashMap<String, an.a<?, ?>> G() {
            return hR;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.hS.contains(Integer.valueOf(aVar.N()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.N()) {
                case 2:
                    return this.ir;
                case 3:
                    return this.jz;
                case 4:
                    return this.iu;
                case 5:
                    return this.jA;
                case 6:
                    return this.jB;
                case 7:
                    return this.jC;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.N());
            }
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> by() {
            return this.hS;
        }

        /* renamed from: cl */
        public e freeze() {
            return this;
        }

        public int describeContents() {
            eo eoVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof e)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            e eVar = (e) obj;
            for (an.a next : hR.values()) {
                if (a(next)) {
                    if (!eVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(eVar.b(next))) {
                        return false;
                    }
                } else if (eVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        public String getFamilyName() {
            return this.ir;
        }

        public String getFormatted() {
            return this.jz;
        }

        public String getGivenName() {
            return this.iu;
        }

        public String getHonorificPrefix() {
            return this.jA;
        }

        public String getHonorificSuffix() {
            return this.jB;
        }

        public String getMiddleName() {
            return this.jC;
        }

        public boolean hasFamilyName() {
            return this.hS.contains(2);
        }

        public boolean hasFormatted() {
            return this.hS.contains(3);
        }

        public boolean hasGivenName() {
            return this.hS.contains(4);
        }

        public boolean hasHonorificPrefix() {
            return this.hS.contains(5);
        }

        public boolean hasHonorificSuffix() {
            return this.hS.contains(6);
        }

        public boolean hasMiddleName() {
            return this.hS.contains(7);
        }

        public int hashCode() {
            int i = 0;
            Iterator<an.a<?, ?>> it = hR.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                an.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.N();
                } else {
                    i = i2;
                }
            }
        }

        /* access modifiers changed from: protected */
        public Object j(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean k(String str) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public int u() {
            return this.T;
        }

        public void writeToParcel(Parcel out, int flags) {
            eo eoVar = CREATOR;
            eo.a(this, out, flags);
        }
    }

    public static class f {
        public static int C(String str) {
            if (str.equals("person")) {
                return 0;
            }
            if (str.equals("page")) {
                return 1;
            }
            throw new IllegalArgumentException("Unknown objectType string: " + str);
        }
    }

    public static final class g extends an implements ae, Person.Organizations {
        public static final ep CREATOR = new ep();
        private static final HashMap<String, an.a<?, ?>> hR = new HashMap<>();
        private final int T;
        private int bi;
        private String ch;
        private String gl;
        private final Set<Integer> hS;
        private String iH;
        private String iq;
        private String jD;
        private String jE;
        private boolean jy;
        private String mName;

        static {
            hR.put("department", an.a.f("department", 2));
            hR.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, an.a.f(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, 3));
            hR.put("endDate", an.a.f("endDate", 4));
            hR.put("location", an.a.f("location", 5));
            hR.put("name", an.a.f("name", 6));
            hR.put("primary", an.a.e("primary", 7));
            hR.put("startDate", an.a.f("startDate", 8));
            hR.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, an.a.f(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, 9));
            hR.put(ServerProtocol.DIALOG_PARAM_TYPE, an.a.a(ServerProtocol.DIALOG_PARAM_TYPE, 10, new ak().b("work", 0).b("school", 1), false));
        }

        public g() {
            this.T = 1;
            this.hS = new HashSet();
        }

        g(Set<Integer> set, int i, String str, String str2, String str3, String str4, String str5, boolean z, String str6, String str7, int i2) {
            this.hS = set;
            this.T = i;
            this.jD = str;
            this.ch = str2;
            this.iq = str3;
            this.jE = str4;
            this.mName = str5;
            this.jy = z;
            this.iH = str6;
            this.gl = str7;
            this.bi = i2;
        }

        public HashMap<String, an.a<?, ?>> G() {
            return hR;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.hS.contains(Integer.valueOf(aVar.N()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.N()) {
                case 2:
                    return this.jD;
                case 3:
                    return this.ch;
                case 4:
                    return this.iq;
                case 5:
                    return this.jE;
                case 6:
                    return this.mName;
                case 7:
                    return Boolean.valueOf(this.jy);
                case 8:
                    return this.iH;
                case 9:
                    return this.gl;
                case 10:
                    return Integer.valueOf(this.bi);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.N());
            }
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> by() {
            return this.hS;
        }

        /* renamed from: cm */
        public g freeze() {
            return this;
        }

        public int describeContents() {
            ep epVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof g)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            g gVar = (g) obj;
            for (an.a next : hR.values()) {
                if (a(next)) {
                    if (!gVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(gVar.b(next))) {
                        return false;
                    }
                } else if (gVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        public String getDepartment() {
            return this.jD;
        }

        public String getDescription() {
            return this.ch;
        }

        public String getEndDate() {
            return this.iq;
        }

        public String getLocation() {
            return this.jE;
        }

        public String getName() {
            return this.mName;
        }

        public String getStartDate() {
            return this.iH;
        }

        public String getTitle() {
            return this.gl;
        }

        public int getType() {
            return this.bi;
        }

        public boolean hasDepartment() {
            return this.hS.contains(2);
        }

        public boolean hasDescription() {
            return this.hS.contains(3);
        }

        public boolean hasEndDate() {
            return this.hS.contains(4);
        }

        public boolean hasLocation() {
            return this.hS.contains(5);
        }

        public boolean hasName() {
            return this.hS.contains(6);
        }

        public boolean hasPrimary() {
            return this.hS.contains(7);
        }

        public boolean hasStartDate() {
            return this.hS.contains(8);
        }

        public boolean hasTitle() {
            return this.hS.contains(9);
        }

        public boolean hasType() {
            return this.hS.contains(10);
        }

        public int hashCode() {
            int i = 0;
            Iterator<an.a<?, ?>> it = hR.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                an.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.N();
                } else {
                    i = i2;
                }
            }
        }

        public boolean isPrimary() {
            return this.jy;
        }

        /* access modifiers changed from: protected */
        public Object j(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean k(String str) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public int u() {
            return this.T;
        }

        public void writeToParcel(Parcel out, int flags) {
            ep epVar = CREATOR;
            ep.a(this, out, flags);
        }
    }

    public static final class h extends an implements ae, Person.PlacesLived {
        public static final et CREATOR = new et();
        private static final HashMap<String, an.a<?, ?>> hR = new HashMap<>();
        private final int T;
        private final Set<Integer> hS;
        private boolean jy;
        private String mValue;

        static {
            hR.put("primary", an.a.e("primary", 2));
            hR.put("value", an.a.f("value", 3));
        }

        public h() {
            this.T = 1;
            this.hS = new HashSet();
        }

        h(Set<Integer> set, int i, boolean z, String str) {
            this.hS = set;
            this.T = i;
            this.jy = z;
            this.mValue = str;
        }

        public HashMap<String, an.a<?, ?>> G() {
            return hR;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.hS.contains(Integer.valueOf(aVar.N()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.N()) {
                case 2:
                    return Boolean.valueOf(this.jy);
                case 3:
                    return this.mValue;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.N());
            }
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> by() {
            return this.hS;
        }

        /* renamed from: cn */
        public h freeze() {
            return this;
        }

        public int describeContents() {
            et etVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof h)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            h hVar = (h) obj;
            for (an.a next : hR.values()) {
                if (a(next)) {
                    if (!hVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(hVar.b(next))) {
                        return false;
                    }
                } else if (hVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        public String getValue() {
            return this.mValue;
        }

        public boolean hasPrimary() {
            return this.hS.contains(2);
        }

        public boolean hasValue() {
            return this.hS.contains(3);
        }

        public int hashCode() {
            int i = 0;
            Iterator<an.a<?, ?>> it = hR.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                an.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.N();
                } else {
                    i = i2;
                }
            }
        }

        public boolean isPrimary() {
            return this.jy;
        }

        /* access modifiers changed from: protected */
        public Object j(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean k(String str) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public int u() {
            return this.T;
        }

        public void writeToParcel(Parcel out, int flags) {
            et etVar = CREATOR;
            et.a(this, out, flags);
        }
    }

    public static final class i extends an implements ae, Person.Urls {
        public static final eu CREATOR = new eu();
        private static final HashMap<String, an.a<?, ?>> hR = new HashMap<>();
        private final int T;
        private int bi;
        private final Set<Integer> hS;
        private boolean jy;
        private String mValue;

        static {
            hR.put("primary", an.a.e("primary", 2));
            hR.put(ServerProtocol.DIALOG_PARAM_TYPE, an.a.a(ServerProtocol.DIALOG_PARAM_TYPE, 3, new ak().b("home", 0).b("work", 1).b("blog", 2).b("profile", 3).b("other", 4), false));
            hR.put("value", an.a.f("value", 4));
        }

        public i() {
            this.T = 1;
            this.hS = new HashSet();
        }

        i(Set<Integer> set, int i, boolean z, int i2, String str) {
            this.hS = set;
            this.T = i;
            this.jy = z;
            this.bi = i2;
            this.mValue = str;
        }

        public HashMap<String, an.a<?, ?>> G() {
            return hR;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.hS.contains(Integer.valueOf(aVar.N()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.N()) {
                case 2:
                    return Boolean.valueOf(this.jy);
                case 3:
                    return Integer.valueOf(this.bi);
                case 4:
                    return this.mValue;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.N());
            }
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> by() {
            return this.hS;
        }

        /* renamed from: co */
        public i freeze() {
            return this;
        }

        public int describeContents() {
            eu euVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof i)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            i iVar = (i) obj;
            for (an.a next : hR.values()) {
                if (a(next)) {
                    if (!iVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(iVar.b(next))) {
                        return false;
                    }
                } else if (iVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        public int getType() {
            return this.bi;
        }

        public String getValue() {
            return this.mValue;
        }

        public boolean hasPrimary() {
            return this.hS.contains(2);
        }

        public boolean hasType() {
            return this.hS.contains(3);
        }

        public boolean hasValue() {
            return this.hS.contains(4);
        }

        public int hashCode() {
            int i = 0;
            Iterator<an.a<?, ?>> it = hR.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                an.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.N();
                } else {
                    i = i2;
                }
            }
        }

        public boolean isPrimary() {
            return this.jy;
        }

        /* access modifiers changed from: protected */
        public Object j(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean k(String str) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public int u() {
            return this.T;
        }

        public void writeToParcel(Parcel out, int flags) {
            eu euVar = CREATOR;
            eu.a(this, out, flags);
        }
    }

    static {
        hR.put("aboutMe", an.a.f("aboutMe", 2));
        hR.put("ageRange", an.a.a("ageRange", 3, a.class));
        hR.put("birthday", an.a.f("birthday", 4));
        hR.put("braggingRights", an.a.f("braggingRights", 5));
        hR.put("circledByCount", an.a.c("circledByCount", 6));
        hR.put("cover", an.a.a("cover", 7, b.class));
        hR.put("currentLocation", an.a.f("currentLocation", 8));
        hR.put("displayName", an.a.f("displayName", 9));
        hR.put("emails", an.a.b("emails", 10, c.class));
        hR.put("etag", an.a.f("etag", 11));
        hR.put("gender", an.a.a("gender", 12, new ak().b("male", 0).b("female", 1).b("other", 2), false));
        hR.put("hasApp", an.a.e("hasApp", 13));
        hR.put(UnsentStats.KEY_ID, an.a.f(UnsentStats.KEY_ID, 14));
        hR.put("image", an.a.a("image", 15, d.class));
        hR.put("isPlusUser", an.a.e("isPlusUser", 16));
        hR.put("language", an.a.f("language", 18));
        hR.put("name", an.a.a("name", 19, e.class));
        hR.put("nickname", an.a.f("nickname", 20));
        hR.put("objectType", an.a.a("objectType", 21, new ak().b("person", 0).b("page", 1), false));
        hR.put("organizations", an.a.b("organizations", 22, g.class));
        hR.put("placesLived", an.a.b("placesLived", 23, h.class));
        hR.put("plusOneCount", an.a.c("plusOneCount", 24));
        hR.put("relationshipStatus", an.a.a("relationshipStatus", 25, new ak().b("single", 0).b("in_a_relationship", 1).b("engaged", 2).b("married", 3).b("its_complicated", 4).b("open_relationship", 5).b("widowed", 6).b("in_domestic_partnership", 7).b("in_civil_union", 8), false));
        hR.put("tagline", an.a.f("tagline", 26));
        hR.put(PlusShare.KEY_CALL_TO_ACTION_URL, an.a.f(PlusShare.KEY_CALL_TO_ACTION_URL, 27));
        hR.put("urls", an.a.b("urls", 28, i.class));
        hR.put("verified", an.a.e("verified", 29));
    }

    public eq() {
        this.T = 1;
        this.hS = new HashSet();
    }

    public eq(String str, String str2, d dVar, int i2, String str3) {
        this.T = 1;
        this.hS = new HashSet();
        this.bm = str;
        this.hS.add(9);
        this.iw = str2;
        this.hS.add(14);
        this.je = dVar;
        this.hS.add(15);
        this.jj = i2;
        this.hS.add(21);
        this.hE = str3;
        this.hS.add(27);
    }

    eq(Set<Integer> set, int i2, String str, a aVar, String str2, String str3, int i3, b bVar, String str4, String str5, List<c> list, String str6, int i4, boolean z, String str7, d dVar, boolean z2, String str8, e eVar, String str9, int i5, List<g> list2, List<h> list3, int i6, int i7, String str10, String str11, List<i> list4, boolean z3) {
        this.hS = set;
        this.T = i2;
        this.iT = str;
        this.iU = aVar;
        this.iV = str2;
        this.iW = str3;
        this.iX = i3;
        this.iY = bVar;
        this.iZ = str4;
        this.bm = str5;
        this.ja = list;
        this.jb = str6;
        this.jc = i4;
        this.jd = z;
        this.iw = str7;
        this.je = dVar;
        this.jf = z2;
        this.jg = str8;
        this.jh = eVar;
        this.ji = str9;
        this.jj = i5;
        this.jk = list2;
        this.jl = list3;
        this.jm = i6;
        this.jn = i7;
        this.jo = str10;
        this.hE = str11;
        this.jp = list4;
        this.jq = z3;
    }

    public static eq d(byte[] bArr) {
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(bArr, 0, bArr.length);
        obtain.setDataPosition(0);
        eq F = CREATOR.createFromParcel(obtain);
        obtain.recycle();
        return F;
    }

    public HashMap<String, an.a<?, ?>> G() {
        return hR;
    }

    /* access modifiers changed from: protected */
    public boolean a(an.a aVar) {
        return this.hS.contains(Integer.valueOf(aVar.N()));
    }

    /* access modifiers changed from: protected */
    public Object b(an.a aVar) {
        switch (aVar.N()) {
            case 2:
                return this.iT;
            case 3:
                return this.iU;
            case 4:
                return this.iV;
            case 5:
                return this.iW;
            case 6:
                return Integer.valueOf(this.iX);
            case 7:
                return this.iY;
            case 8:
                return this.iZ;
            case 9:
                return this.bm;
            case 10:
                return this.ja;
            case 11:
                return this.jb;
            case 12:
                return Integer.valueOf(this.jc);
            case 13:
                return Boolean.valueOf(this.jd);
            case 14:
                return this.iw;
            case 15:
                return this.je;
            case 16:
                return Boolean.valueOf(this.jf);
            case 17:
            default:
                throw new IllegalStateException("Unknown safe parcelable id=" + aVar.N());
            case 18:
                return this.jg;
            case TimeUtils.HUNDRED_DAY_FIELD_LEN:
                return this.jh;
            case 20:
                return this.ji;
            case 21:
                return Integer.valueOf(this.jj);
            case 22:
                return this.jk;
            case 23:
                return this.jl;
            case 24:
                return Integer.valueOf(this.jm);
            case 25:
                return Integer.valueOf(this.jn);
            case 26:
                return this.jo;
            case 27:
                return this.hE;
            case 28:
                return this.jp;
            case 29:
                return Boolean.valueOf(this.jq);
        }
    }

    /* access modifiers changed from: package-private */
    public a bT() {
        return this.iU;
    }

    /* access modifiers changed from: package-private */
    public b bU() {
        return this.iY;
    }

    /* access modifiers changed from: package-private */
    public List<c> bV() {
        return this.ja;
    }

    public String bW() {
        return this.jb;
    }

    /* access modifiers changed from: package-private */
    public d bX() {
        return this.je;
    }

    /* access modifiers changed from: package-private */
    public e bY() {
        return this.jh;
    }

    /* access modifiers changed from: package-private */
    public List<g> bZ() {
        return this.jk;
    }

    /* access modifiers changed from: package-private */
    public Set<Integer> by() {
        return this.hS;
    }

    /* access modifiers changed from: package-private */
    public List<h> ca() {
        return this.jl;
    }

    /* access modifiers changed from: package-private */
    public List<i> cb() {
        return this.jp;
    }

    /* renamed from: cc */
    public eq freeze() {
        return this;
    }

    public int describeContents() {
        er erVar = CREATOR;
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof eq)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        eq eqVar = (eq) obj;
        for (an.a next : hR.values()) {
            if (a(next)) {
                if (!eqVar.a(next)) {
                    return false;
                }
                if (!b(next).equals(eqVar.b(next))) {
                    return false;
                }
            } else if (eqVar.a(next)) {
                return false;
            }
        }
        return true;
    }

    public String getAboutMe() {
        return this.iT;
    }

    public Person.AgeRange getAgeRange() {
        return this.iU;
    }

    public String getBirthday() {
        return this.iV;
    }

    public String getBraggingRights() {
        return this.iW;
    }

    public int getCircledByCount() {
        return this.iX;
    }

    public Person.Cover getCover() {
        return this.iY;
    }

    public String getCurrentLocation() {
        return this.iZ;
    }

    public String getDisplayName() {
        return this.bm;
    }

    public List<Person.Emails> getEmails() {
        return (ArrayList) this.ja;
    }

    public int getGender() {
        return this.jc;
    }

    public String getId() {
        return this.iw;
    }

    public Person.Image getImage() {
        return this.je;
    }

    public String getLanguage() {
        return this.jg;
    }

    public Person.Name getName() {
        return this.jh;
    }

    public String getNickname() {
        return this.ji;
    }

    public int getObjectType() {
        return this.jj;
    }

    public List<Person.Organizations> getOrganizations() {
        return (ArrayList) this.jk;
    }

    public List<Person.PlacesLived> getPlacesLived() {
        return (ArrayList) this.jl;
    }

    public int getPlusOneCount() {
        return this.jm;
    }

    public int getRelationshipStatus() {
        return this.jn;
    }

    public String getTagline() {
        return this.jo;
    }

    public String getUrl() {
        return this.hE;
    }

    public List<Person.Urls> getUrls() {
        return (ArrayList) this.jp;
    }

    public boolean hasAboutMe() {
        return this.hS.contains(2);
    }

    public boolean hasAgeRange() {
        return this.hS.contains(3);
    }

    public boolean hasBirthday() {
        return this.hS.contains(4);
    }

    public boolean hasBraggingRights() {
        return this.hS.contains(5);
    }

    public boolean hasCircledByCount() {
        return this.hS.contains(6);
    }

    public boolean hasCover() {
        return this.hS.contains(7);
    }

    public boolean hasCurrentLocation() {
        return this.hS.contains(8);
    }

    public boolean hasDisplayName() {
        return this.hS.contains(9);
    }

    public boolean hasEmails() {
        return this.hS.contains(10);
    }

    public boolean hasGender() {
        return this.hS.contains(12);
    }

    public boolean hasHasApp() {
        return this.hS.contains(13);
    }

    public boolean hasId() {
        return this.hS.contains(14);
    }

    public boolean hasImage() {
        return this.hS.contains(15);
    }

    public boolean hasIsPlusUser() {
        return this.hS.contains(16);
    }

    public boolean hasLanguage() {
        return this.hS.contains(18);
    }

    public boolean hasName() {
        return this.hS.contains(19);
    }

    public boolean hasNickname() {
        return this.hS.contains(20);
    }

    public boolean hasObjectType() {
        return this.hS.contains(21);
    }

    public boolean hasOrganizations() {
        return this.hS.contains(22);
    }

    public boolean hasPlacesLived() {
        return this.hS.contains(23);
    }

    public boolean hasPlusOneCount() {
        return this.hS.contains(24);
    }

    public boolean hasRelationshipStatus() {
        return this.hS.contains(25);
    }

    public boolean hasTagline() {
        return this.hS.contains(26);
    }

    public boolean hasUrl() {
        return this.hS.contains(27);
    }

    public boolean hasUrls() {
        return this.hS.contains(28);
    }

    public boolean hasVerified() {
        return this.hS.contains(29);
    }

    public int hashCode() {
        int i2 = 0;
        Iterator<an.a<?, ?>> it = hR.values().iterator();
        while (true) {
            int i3 = i2;
            if (!it.hasNext()) {
                return i3;
            }
            an.a next = it.next();
            if (a(next)) {
                i2 = b(next).hashCode() + i3 + next.N();
            } else {
                i2 = i3;
            }
        }
    }

    public boolean isHasApp() {
        return this.jd;
    }

    public boolean isPlusUser() {
        return this.jf;
    }

    public boolean isVerified() {
        return this.jq;
    }

    /* access modifiers changed from: protected */
    public Object j(String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean k(String str) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public int u() {
        return this.T;
    }

    public void writeToParcel(Parcel out, int flags) {
        er erVar = CREATOR;
        er.a(this, out, flags);
    }
}
