package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.ViewTreeObserver;
import java.lang.ref.WeakReference;

public class bn {
    protected bj cs;
    protected a dh;

    public static final class a {
        public int bottom;
        public IBinder di;
        public int dj;
        public int gravity;
        public int left;
        public int right;
        public int top;

        private a(int i) {
            this.di = null;
            this.dj = -1;
            this.left = 0;
            this.top = 0;
            this.right = 0;
            this.bottom = 0;
            this.gravity = i;
            this.di = new Binder();
        }

        public Bundle ap() {
            Bundle bundle = new Bundle();
            bundle.putInt("popupLocationInfo.gravity", this.gravity);
            bundle.putInt("popupLocationInfo.displayId", this.dj);
            bundle.putInt("popupLocationInfo.left", this.left);
            bundle.putInt("popupLocationInfo.top", this.top);
            bundle.putInt("popupLocationInfo.right", this.right);
            bundle.putInt("popupLocationInfo.bottom", this.bottom);
            return bundle;
        }
    }

    static final class b extends bn implements View.OnAttachStateChangeListener, ViewTreeObserver.OnGlobalLayoutListener {
        private boolean cC = false;
        private WeakReference<View> dk;

        protected b(bj bjVar, int i) {
            super(bjVar, i);
        }

        private void b(View view) {
            int i = -1;
            if (ba.af()) {
                i = view.getDisplay().getDisplayId();
            }
            IBinder windowToken = view.getWindowToken();
            int[] iArr = new int[2];
            view.getLocationInWindow(iArr);
            int width = view.getWidth();
            int height = view.getHeight();
            this.dh.dj = i;
            this.dh.di = windowToken;
            this.dh.left = iArr[0];
            this.dh.top = iArr[1];
            this.dh.right = iArr[0] + width;
            this.dh.bottom = iArr[1] + height;
            if (this.cC) {
                am();
                this.cC = false;
            }
        }

        public void a(View view) {
            if (this.dk != null) {
                View view2 = this.dk.get();
                Context context = this.cs.getContext();
                if (view2 == null && (context instanceof Activity)) {
                    view2 = ((Activity) context).getWindow().getDecorView();
                }
                if (view2 != null) {
                    ViewTreeObserver viewTreeObserver = view2.getViewTreeObserver();
                    if (ba.ae()) {
                        viewTreeObserver.removeOnGlobalLayoutListener(this);
                    } else {
                        viewTreeObserver.removeGlobalOnLayoutListener(this);
                    }
                }
            }
            this.dk = null;
            Context context2 = this.cs.getContext();
            if (view == null && (context2 instanceof Activity)) {
                View findViewById = ((Activity) context2).findViewById(16908290);
                if (findViewById == null) {
                    findViewById = ((Activity) context2).getWindow().getDecorView();
                }
                bk.b("PopupManager", "You have not specified a View to use as content view for popups. Falling back to the Activity content view which may not work properly in future versions of the API. Use setViewForPopups() to set your content view.");
                view = findViewById;
            }
            if (view != null) {
                this.dk = new WeakReference<>(view);
                view.addOnAttachStateChangeListener(this);
                view.getViewTreeObserver().addOnGlobalLayoutListener(this);
                return;
            }
            bk.c("PopupManager", "No content view usable to display popups. Popups will not be displayed in response to this client's calls. Use setViewForPopups() to set your content view.");
        }

        public void am() {
            if (this.dh.di != null) {
                bn.super.am();
            } else {
                this.cC = this.dk != null;
            }
        }

        public void onGlobalLayout() {
            View view;
            if (this.dk != null && (view = this.dk.get()) != null) {
                b(view);
            }
        }

        public void onViewAttachedToWindow(View v) {
            b(v);
        }

        public void onViewDetachedFromWindow(View v) {
            this.cs.aj();
            v.removeOnAttachStateChangeListener(this);
        }
    }

    private bn(bj bjVar, int i) {
        this.cs = bjVar;
        this.dh = new a(i);
    }

    public static bn a(bj bjVar, int i) {
        return ba.ab() ? new b(bjVar, i) : new bn(bjVar, i);
    }

    public void a(View view) {
    }

    public void am() {
        this.cs.a(this.dh.di, this.dh.ap());
    }

    public Bundle an() {
        return this.dh.ap();
    }

    public IBinder ao() {
        return this.dh.di;
    }

    public void setGravity(int gravity) {
        this.dh.gravity = gravity;
    }
}
