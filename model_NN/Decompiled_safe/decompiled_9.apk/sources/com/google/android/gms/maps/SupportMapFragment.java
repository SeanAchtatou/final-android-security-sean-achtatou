package com.google.android.gms.maps;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.dynamic.LifecycleDelegate;
import com.google.android.gms.internal.bb;
import com.google.android.gms.internal.bd;
import com.google.android.gms.internal.be;
import com.google.android.gms.internal.cv;
import com.google.android.gms.internal.cw;
import com.google.android.gms.internal.x;
import com.google.android.gms.maps.internal.IGoogleMapDelegate;
import com.google.android.gms.maps.internal.IMapFragmentDelegate;
import com.google.android.gms.maps.model.RuntimeRemoteException;

public class SupportMapFragment extends Fragment {
    private final b fE = new b(this);
    private GoogleMap ft;

    static class a implements LifecycleDelegate {
        private final Fragment fF;
        private final IMapFragmentDelegate fv;

        public a(Fragment fragment, IMapFragmentDelegate iMapFragmentDelegate) {
            this.fv = (IMapFragmentDelegate) x.d(iMapFragmentDelegate);
            this.fF = (Fragment) x.d(fragment);
        }

        public IMapFragmentDelegate aO() {
            return this.fv;
        }

        public void onCreate(Bundle savedInstanceState) {
            if (savedInstanceState == null) {
                try {
                    savedInstanceState = new Bundle();
                } catch (RemoteException e) {
                    throw new RuntimeRemoteException(e);
                }
            }
            Bundle arguments = this.fF.getArguments();
            if (arguments != null && arguments.containsKey("MapOptions")) {
                cv.a(savedInstanceState, "MapOptions", arguments.getParcelable("MapOptions"));
            }
            this.fv.onCreate(savedInstanceState);
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            try {
                return (View) bd.a(this.fv.onCreateView(bd.f(inflater), bd.f(container), savedInstanceState));
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onDestroy() {
            try {
                this.fv.onDestroy();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onDestroyView() {
            try {
                this.fv.onDestroyView();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onInflate(Activity activity, Bundle attrs, Bundle savedInstanceState) {
            try {
                this.fv.onInflate(bd.f(activity), (GoogleMapOptions) attrs.getParcelable("MapOptions"), savedInstanceState);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onLowMemory() {
            try {
                this.fv.onLowMemory();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onPause() {
            try {
                this.fv.onPause();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onResume() {
            try {
                this.fv.onResume();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onSaveInstanceState(Bundle outState) {
            try {
                this.fv.onSaveInstanceState(outState);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
    }

    static class b extends bb<a> {
        private Activity ar;
        private final Fragment fF;
        protected be<a> fw;

        b(Fragment fragment) {
            this.fF = fragment;
        }

        /* access modifiers changed from: private */
        public void setActivity(Activity activity) {
            this.ar = activity;
            aP();
        }

        /* access modifiers changed from: protected */
        public void a(be<a> beVar) {
            this.fw = beVar;
            aP();
        }

        public void aP() {
            if (this.ar != null && this.fw != null && ag() == null) {
                try {
                    MapsInitializer.initialize(this.ar);
                    this.fw.a(new a(this.fF, cw.g(this.ar).d(bd.f(this.ar))));
                } catch (RemoteException e) {
                    throw new RuntimeRemoteException(e);
                } catch (GooglePlayServicesNotAvailableException e2) {
                }
            }
        }
    }

    public static SupportMapFragment newInstance() {
        return new SupportMapFragment();
    }

    public static SupportMapFragment newInstance(GoogleMapOptions options) {
        SupportMapFragment supportMapFragment = new SupportMapFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("MapOptions", options);
        supportMapFragment.setArguments(bundle);
        return supportMapFragment;
    }

    /* access modifiers changed from: protected */
    public IMapFragmentDelegate aO() {
        this.fE.aP();
        if (this.fE.ag() == null) {
            return null;
        }
        return ((a) this.fE.ag()).aO();
    }

    public final GoogleMap getMap() {
        IMapFragmentDelegate aO = aO();
        if (aO == null) {
            return null;
        }
        try {
            IGoogleMapDelegate map = aO.getMap();
            if (map == null) {
                return null;
            }
            if (this.ft == null || this.ft.aF().asBinder() != map.asBinder()) {
                this.ft = new GoogleMap(map);
            }
            return this.ft;
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.fE.setActivity(activity);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.fE.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.fE.onCreateView(inflater, container, savedInstanceState);
    }

    public void onDestroy() {
        this.fE.onDestroy();
        super.onDestroy();
    }

    public void onDestroyView() {
        this.fE.onDestroyView();
        super.onDestroyView();
    }

    public void onInflate(Activity activity, AttributeSet attrs, Bundle savedInstanceState) {
        super.onInflate(activity, attrs, savedInstanceState);
        this.fE.setActivity(activity);
        GoogleMapOptions createFromAttributes = GoogleMapOptions.createFromAttributes(activity, attrs);
        Bundle bundle = new Bundle();
        bundle.putParcelable("MapOptions", createFromAttributes);
        this.fE.onInflate(activity, bundle, savedInstanceState);
    }

    public void onLowMemory() {
        this.fE.onLowMemory();
        super.onLowMemory();
    }

    public void onPause() {
        this.fE.onPause();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.fE.onResume();
    }

    public void onSaveInstanceState(Bundle outState) {
        this.fE.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    public void setArguments(Bundle args) {
        super.setArguments(args);
    }
}
