package com.google.android.gms.internal;

import com.google.android.gms.common.data.DataBuffer;
import java.util.ArrayList;

public abstract class m<T> extends DataBuffer<T> {
    private boolean ag = false;
    private ArrayList<Integer> ah;

    public m(k kVar) {
        super(kVar);
    }

    private int g(int i) {
        n.a(i >= 0 && i < this.ah.size(), "Position " + i + " is out of bounds for this buffer");
        return this.ah.get(i).intValue();
    }

    private int h(int i) {
        if (i < 0 || i == this.ah.size()) {
            return 0;
        }
        return i == this.ah.size() + -1 ? this.O.getCount() - this.ah.get(i).intValue() : this.ah.get(i + 1).intValue() - this.ah.get(i).intValue();
    }

    private void i() {
        synchronized (this) {
            if (!this.ag) {
                int count = this.O.getCount();
                this.ah = new ArrayList<>();
                if (count > 0) {
                    this.ah.add(0);
                    String primaryDataMarkerColumn = getPrimaryDataMarkerColumn();
                    String c = this.O.c(primaryDataMarkerColumn, 0, this.O.d(0));
                    int i = 1;
                    while (i < count) {
                        String c2 = this.O.c(primaryDataMarkerColumn, i, this.O.d(i));
                        if (!c2.equals(c)) {
                            this.ah.add(Integer.valueOf(i));
                        } else {
                            c2 = c;
                        }
                        i++;
                        c = c2;
                    }
                }
                this.ag = true;
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract T a(int i, int i2);

    public final T get(int position) {
        i();
        return a(g(position), h(position));
    }

    public int getCount() {
        i();
        return this.ah.size();
    }

    /* access modifiers changed from: protected */
    public abstract String getPrimaryDataMarkerColumn();
}
