package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.eq;
import java.util.HashSet;
import java.util.Set;

public class en implements Parcelable.Creator<eq.d> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(eq.d dVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        Set<Integer> by = dVar.by();
        if (by.contains(1)) {
            ad.c(parcel, 1, dVar.u());
        }
        if (by.contains(2)) {
            ad.a(parcel, 2, dVar.getUrl(), true);
        }
        ad.C(parcel, d);
    }

    /* renamed from: C */
    public eq.d createFromParcel(Parcel parcel) {
        int c = ac.c(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i = ac.f(parcel, b);
                    hashSet.add(1);
                    break;
                case 2:
                    str = ac.l(parcel, b);
                    hashSet.add(2);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new eq.d(hashSet, i, str);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    /* renamed from: W */
    public eq.d[] newArray(int i) {
        return new eq.d[i];
    }
}
