package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.dq;
import com.google.android.gms.internal.dr;
import com.google.android.gms.panorama.PanoramaClient;

public class ds extends p<dr> {

    final class a extends p<dr>.b<PanoramaClient.a> {
        public final ConnectionResult gD;
        public final Intent gE;
        public final int type;

        public a(PanoramaClient.a aVar, ConnectionResult connectionResult, int i, Intent intent) {
            super(aVar);
            this.gD = connectionResult;
            this.type = i;
            this.gE = intent;
        }

        /* access modifiers changed from: protected */
        public void a(PanoramaClient.a aVar) {
            if (aVar != null) {
                aVar.a(this.gD, this.type, this.gE);
            }
        }
    }

    final class b extends dq.a {
        private final PanoramaClient.a gG = null;
        private final PanoramaClient.OnPanoramaInfoLoadedListener gH;
        private final Uri gI;

        public b(PanoramaClient.OnPanoramaInfoLoadedListener onPanoramaInfoLoadedListener, Uri uri) {
            this.gH = onPanoramaInfoLoadedListener;
            this.gI = uri;
        }

        public void a(int i, Bundle bundle, int i2, Intent intent) {
            if (this.gI != null) {
                ds.this.getContext().revokeUriPermission(this.gI, 1);
            }
            PendingIntent pendingIntent = null;
            if (bundle != null) {
                pendingIntent = (PendingIntent) bundle.getParcelable("pendingIntent");
            }
            ConnectionResult connectionResult = new ConnectionResult(i, pendingIntent);
            if (this.gG != null) {
                ds.this.a(new a(this.gG, connectionResult, i2, intent));
            } else {
                ds.this.a(new c(this.gH, connectionResult, intent));
            }
        }
    }

    final class c extends p<dr>.b<PanoramaClient.OnPanoramaInfoLoadedListener> {
        private final ConnectionResult gD;
        private final Intent gE;

        public c(PanoramaClient.OnPanoramaInfoLoadedListener onPanoramaInfoLoadedListener, ConnectionResult connectionResult, Intent intent) {
            super(onPanoramaInfoLoadedListener);
            this.gD = connectionResult;
            this.gE = intent;
        }

        /* access modifiers changed from: protected */
        public void a(PanoramaClient.OnPanoramaInfoLoadedListener onPanoramaInfoLoadedListener) {
            if (onPanoramaInfoLoadedListener != null) {
                onPanoramaInfoLoadedListener.onPanoramaInfoLoaded(this.gD, this.gE);
            }
        }
    }

    public ds(Context context, GooglePlayServicesClient.ConnectionCallbacks connectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, connectionCallbacks, onConnectionFailedListener, null);
    }

    /* renamed from: T */
    public dr c(IBinder iBinder) {
        return dr.a.S(iBinder);
    }

    public void a(b bVar, Uri uri, Bundle bundle, boolean z) {
        n();
        if (z) {
            getContext().grantUriPermission(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, uri, 1);
        }
        try {
            ((dr) o()).a(bVar, uri, bundle, z);
        } catch (RemoteException e) {
            bVar.a(8, null, 0, null);
        }
    }

    /* access modifiers changed from: protected */
    public void a(u uVar, p<dr>.d dVar) throws RemoteException {
        uVar.a(dVar, GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, getContext().getPackageName(), new Bundle());
    }

    public void a(PanoramaClient.OnPanoramaInfoLoadedListener onPanoramaInfoLoadedListener, Uri uri, boolean z) {
        a(new b(onPanoramaInfoLoadedListener, z ? uri : null), uri, null, z);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "com.google.android.gms.panorama.service.START";
    }

    /* access modifiers changed from: protected */
    public String c() {
        return "com.google.android.gms.panorama.internal.IPanoramaService";
    }
}
