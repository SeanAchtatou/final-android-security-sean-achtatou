package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ad;

public class ActivityRecognitionResultCreator implements Parcelable.Creator<ActivityRecognitionResult> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void a(ActivityRecognitionResult activityRecognitionResult, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.b(parcel, 1, activityRecognitionResult.en, false);
        ad.c(parcel, 1000, activityRecognitionResult.T);
        ad.a(parcel, 2, activityRecognitionResult.eo);
        ad.a(parcel, 3, activityRecognitionResult.ep);
        ad.C(parcel, d);
    }

    public ActivityRecognitionResult createFromParcel(Parcel parcel) {
        ActivityRecognitionResult activityRecognitionResult = new ActivityRecognitionResult();
        int c = ac.c(parcel);
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    activityRecognitionResult.en = ac.c(parcel, b, DetectedActivity.CREATOR);
                    break;
                case 2:
                    activityRecognitionResult.eo = ac.g(parcel, b);
                    break;
                case 3:
                    activityRecognitionResult.ep = ac.g(parcel, b);
                    break;
                case 1000:
                    activityRecognitionResult.T = ac.f(parcel, b);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return activityRecognitionResult;
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    public ActivityRecognitionResult[] newArray(int size) {
        return new ActivityRecognitionResult[size];
    }
}
