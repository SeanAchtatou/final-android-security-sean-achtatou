package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.an;

public class ao implements Parcelable.Creator<an.a> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.ai, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(an.a aVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, aVar.u());
        ad.c(parcel, 2, aVar.E());
        ad.a(parcel, 3, aVar.K());
        ad.c(parcel, 4, aVar.F());
        ad.a(parcel, 5, aVar.L());
        ad.a(parcel, 6, aVar.M(), false);
        ad.c(parcel, 7, aVar.N());
        ad.a(parcel, 8, aVar.P(), false);
        ad.a(parcel, 9, (Parcelable) aVar.R(), i, false);
        ad.C(parcel, d);
    }

    /* renamed from: i */
    public an.a createFromParcel(Parcel parcel) {
        ai aiVar = null;
        int i = 0;
        int c = ac.c(parcel);
        String str = null;
        String str2 = null;
        boolean z = false;
        int i2 = 0;
        boolean z2 = false;
        int i3 = 0;
        int i4 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i4 = ac.f(parcel, b);
                    break;
                case 2:
                    i3 = ac.f(parcel, b);
                    break;
                case 3:
                    z2 = ac.c(parcel, b);
                    break;
                case 4:
                    i2 = ac.f(parcel, b);
                    break;
                case 5:
                    z = ac.c(parcel, b);
                    break;
                case 6:
                    str2 = ac.l(parcel, b);
                    break;
                case 7:
                    i = ac.f(parcel, b);
                    break;
                case 8:
                    str = ac.l(parcel, b);
                    break;
                case 9:
                    aiVar = (ai) ac.a(parcel, b, ai.CREATOR);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new an.a(i4, i3, z2, i2, z, str2, i, str, aiVar);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    /* renamed from: o */
    public an.a[] newArray(int i) {
        return new an.a[i];
    }
}
