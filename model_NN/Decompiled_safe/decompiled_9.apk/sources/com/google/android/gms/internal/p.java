package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.t;
import com.google.android.gms.internal.u;
import java.util.ArrayList;

public abstract class p<T extends IInterface> implements GooglePlayServicesClient {
    public static final String[] aE = {"service_esmobile", "service_googleme"};
    /* access modifiers changed from: private */
    public p<T>.e aA;
    boolean aB = false;
    boolean aC = false;
    /* access modifiers changed from: private */
    public final Object aD = new Object();
    /* access modifiers changed from: private */
    public T at;
    /* access modifiers changed from: private */
    public ArrayList<GooglePlayServicesClient.ConnectionCallbacks> au;
    final ArrayList<GooglePlayServicesClient.ConnectionCallbacks> av = new ArrayList<>();
    private boolean aw = false;
    private ArrayList<GooglePlayServicesClient.OnConnectionFailedListener> ax;
    private boolean ay = false;
    /* access modifiers changed from: private */
    public final ArrayList<p<T>.b<?>> az = new ArrayList<>();
    private final String[] f;
    /* access modifiers changed from: private */
    public final Context mContext;
    final Handler mHandler;

    final class a extends Handler {
        public a(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            synchronized (p.this.aD) {
                p.this.aC = false;
            }
            if (msg.what == 3) {
                p.this.a(new ConnectionResult(((Integer) msg.obj).intValue(), null));
            } else if (msg.what == 4) {
                synchronized (p.this.au) {
                    if (p.this.aB && p.this.isConnected() && p.this.au.contains(msg.obj)) {
                        ((GooglePlayServicesClient.ConnectionCallbacks) msg.obj).onConnected(p.this.l());
                    }
                }
            } else if (msg.what == 2 && !p.this.isConnected()) {
            } else {
                if (msg.what == 2 || msg.what == 1) {
                    ((b) msg.obj).p();
                }
            }
        }
    }

    public abstract class b<TListener> {
        private TListener mListener;

        public b(TListener tlistener) {
            this.mListener = tlistener;
            synchronized (p.this.az) {
                p.this.az.add(this);
            }
        }

        /* access modifiers changed from: protected */
        public abstract void a(Object obj);

        public void p() {
            TListener tlistener;
            synchronized (this) {
                tlistener = this.mListener;
            }
            a(tlistener);
        }

        public void q() {
            synchronized (this) {
                this.mListener = null;
            }
        }
    }

    public abstract class c<TListener> extends p<T>.b<TListener> {
        protected final k O;

        public c(TListener tlistener, k kVar) {
            super(tlistener);
            this.O = kVar;
        }

        /* access modifiers changed from: protected */
        public abstract void a(Object obj);

        public /* bridge */ /* synthetic */ void p() {
            super.p();
        }

        public /* bridge */ /* synthetic */ void q() {
            super.q();
        }
    }

    public final class d extends t.a {
        protected d() {
        }

        public void a(int i, IBinder iBinder, Bundle bundle) {
            p.this.mHandler.sendMessage(p.this.mHandler.obtainMessage(1, new f(i, iBinder, bundle)));
        }
    }

    final class e implements ServiceConnection {
        e() {
        }

        public void onServiceConnected(ComponentName component, IBinder binder) {
            p.this.f(binder);
        }

        public void onServiceDisconnected(ComponentName component) {
            IInterface unused = p.this.at = (IInterface) null;
            p.this.m();
        }
    }

    public final class f extends p<T>.b<Boolean> {
        public final Bundle aG;
        public final IBinder aH;
        public final int statusCode;

        public f(int i, IBinder iBinder, Bundle bundle) {
            super(true);
            this.statusCode = i;
            this.aH = iBinder;
            this.aG = bundle;
        }

        /* access modifiers changed from: protected */
        public void a(Boolean bool) {
            PendingIntent pendingIntent = null;
            if (bool != null) {
                switch (this.statusCode) {
                    case 0:
                        try {
                            if (p.this.c().equals(this.aH.getInterfaceDescriptor())) {
                                IInterface unused = p.this.at = p.this.c(this.aH);
                                if (p.this.at != null) {
                                    p.this.k();
                                    return;
                                }
                            }
                        } catch (RemoteException e) {
                        }
                        q.e(p.this.mContext).b(p.this.b(), p.this.aA);
                        e unused2 = p.this.aA = (e) null;
                        IInterface unused3 = p.this.at = (IInterface) null;
                        p.this.a(new ConnectionResult(8, null));
                        return;
                    case 10:
                        throw new IllegalStateException("A fatal developer error has occurred. Check the logs for further information.");
                    default:
                        if (this.aG != null) {
                            pendingIntent = (PendingIntent) this.aG.getParcelable("pendingIntent");
                        }
                        p.this.a(new ConnectionResult(this.statusCode, pendingIntent));
                        return;
                }
            }
        }
    }

    protected p(Context context, GooglePlayServicesClient.ConnectionCallbacks connectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener onConnectionFailedListener, String... strArr) {
        this.mContext = (Context) x.d(context);
        this.au = new ArrayList<>();
        this.au.add(x.d(connectionCallbacks));
        this.ax = new ArrayList<>();
        this.ax.add(x.d(onConnectionFailedListener));
        this.mHandler = new a(context.getMainLooper());
        a(strArr);
        this.f = strArr;
    }

    /* access modifiers changed from: protected */
    public void a(ConnectionResult connectionResult) {
        this.mHandler.removeMessages(4);
        synchronized (this.ax) {
            this.ay = true;
            ArrayList<GooglePlayServicesClient.OnConnectionFailedListener> arrayList = this.ax;
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                if (this.aB) {
                    if (this.ax.contains(arrayList.get(i))) {
                        arrayList.get(i).onConnectionFailed(connectionResult);
                    }
                    i++;
                } else {
                    return;
                }
            }
            this.ay = false;
        }
    }

    public final void a(p<T>.b<?> bVar) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(2, bVar));
    }

    /* access modifiers changed from: protected */
    public abstract void a(u uVar, p<T>.d dVar) throws RemoteException;

    /* access modifiers changed from: protected */
    public void a(String... strArr) {
    }

    /* access modifiers changed from: protected */
    public abstract String b();

    /* access modifiers changed from: protected */
    public abstract T c(IBinder iBinder);

    /* access modifiers changed from: protected */
    public abstract String c();

    public void connect() {
        this.aB = true;
        synchronized (this.aD) {
            this.aC = true;
        }
        int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.mContext);
        if (isGooglePlayServicesAvailable != 0) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(3, Integer.valueOf(isGooglePlayServicesAvailable)));
            return;
        }
        if (this.aA != null) {
            Log.e("GmsClient", "Calling connect() while still connected, missing disconnect().");
            this.at = null;
            q.e(this.mContext).b(b(), this.aA);
        }
        this.aA = new e();
        if (!q.e(this.mContext).a(b(), this.aA)) {
            Log.e("GmsClient", "unable to connect to service: " + b());
            this.mHandler.sendMessage(this.mHandler.obtainMessage(3, 9));
        }
    }

    public void disconnect() {
        this.aB = false;
        synchronized (this.aD) {
            this.aC = false;
        }
        synchronized (this.az) {
            int size = this.az.size();
            for (int i = 0; i < size; i++) {
                this.az.get(i).q();
            }
            this.az.clear();
        }
        this.at = null;
        if (this.aA != null) {
            q.e(this.mContext).b(b(), this.aA);
            this.aA = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void f(IBinder iBinder) {
        try {
            a(u.a.h(iBinder), new d());
        } catch (RemoteException e2) {
            Log.w("GmsClient", "service died");
        }
    }

    public final Context getContext() {
        return this.mContext;
    }

    public boolean isConnected() {
        return this.at != null;
    }

    public boolean isConnecting() {
        boolean z;
        synchronized (this.aD) {
            z = this.aC;
        }
        return z;
    }

    public boolean isConnectionCallbacksRegistered(GooglePlayServicesClient.ConnectionCallbacks listener) {
        boolean contains;
        x.d(listener);
        synchronized (this.au) {
            contains = this.au.contains(listener);
        }
        return contains;
    }

    public boolean isConnectionFailedListenerRegistered(GooglePlayServicesClient.OnConnectionFailedListener listener) {
        boolean contains;
        x.d(listener);
        synchronized (this.ax) {
            contains = this.ax.contains(listener);
        }
        return contains;
    }

    public final String[] j() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public void k() {
        boolean z = true;
        synchronized (this.au) {
            x.a(!this.aw);
            this.mHandler.removeMessages(4);
            this.aw = true;
            if (this.av.size() != 0) {
                z = false;
            }
            x.a(z);
            Bundle l = l();
            ArrayList<GooglePlayServicesClient.ConnectionCallbacks> arrayList = this.au;
            int size = arrayList.size();
            for (int i = 0; i < size && this.aB && isConnected(); i++) {
                this.av.size();
                if (!this.av.contains(arrayList.get(i))) {
                    arrayList.get(i).onConnected(l);
                }
            }
            this.av.clear();
            this.aw = false;
        }
    }

    /* access modifiers changed from: protected */
    public Bundle l() {
        return null;
    }

    /* access modifiers changed from: protected */
    public final void m() {
        this.mHandler.removeMessages(4);
        synchronized (this.au) {
            this.aw = true;
            ArrayList<GooglePlayServicesClient.ConnectionCallbacks> arrayList = this.au;
            int size = arrayList.size();
            for (int i = 0; i < size && this.aB; i++) {
                if (this.au.contains(arrayList.get(i))) {
                    arrayList.get(i).onDisconnected();
                }
            }
            this.aw = false;
        }
    }

    /* access modifiers changed from: protected */
    public final void n() {
        if (!isConnected()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    /* access modifiers changed from: protected */
    public final T o() {
        n();
        return this.at;
    }

    public void registerConnectionCallbacks(GooglePlayServicesClient.ConnectionCallbacks listener) {
        x.d(listener);
        synchronized (this.au) {
            if (this.au.contains(listener)) {
                Log.w("GmsClient", "registerConnectionCallbacks(): listener " + listener + " is already registered");
            } else {
                if (this.aw) {
                    this.au = new ArrayList<>(this.au);
                }
                this.au.add(listener);
            }
        }
        if (isConnected()) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(4, listener));
        }
    }

    public void registerConnectionFailedListener(GooglePlayServicesClient.OnConnectionFailedListener listener) {
        x.d(listener);
        synchronized (this.ax) {
            if (this.ax.contains(listener)) {
                Log.w("GmsClient", "registerConnectionFailedListener(): listener " + listener + " is already registered");
            } else {
                if (this.ay) {
                    this.ax = new ArrayList<>(this.ax);
                }
                this.ax.add(listener);
            }
        }
    }

    public void unregisterConnectionCallbacks(GooglePlayServicesClient.ConnectionCallbacks listener) {
        x.d(listener);
        synchronized (this.au) {
            if (this.au != null) {
                if (this.aw) {
                    this.au = new ArrayList<>(this.au);
                }
                if (!this.au.remove(listener)) {
                    Log.w("GmsClient", "unregisterConnectionCallbacks(): listener " + listener + " not found");
                } else if (this.aw && !this.av.contains(listener)) {
                    this.av.add(listener);
                }
            }
        }
    }

    public void unregisterConnectionFailedListener(GooglePlayServicesClient.OnConnectionFailedListener listener) {
        x.d(listener);
        synchronized (this.ax) {
            if (this.ax != null) {
                if (this.ay) {
                    this.ax = new ArrayList<>(this.ax);
                }
                if (!this.ax.remove(listener)) {
                    Log.w("GmsClient", "unregisterConnectionFailedListener(): listener " + listener + " not found");
                }
            }
        }
    }
}
