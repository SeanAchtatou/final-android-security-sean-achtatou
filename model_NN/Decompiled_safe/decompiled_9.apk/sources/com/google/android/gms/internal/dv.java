package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import com.google.android.gms.internal.dw;
import java.util.List;

public abstract class dv extends dw.a {
    public void B(String str) throws RemoteException {
    }

    public void a(int i, Bundle bundle, Bundle bundle2) throws RemoteException {
    }

    public void a(int i, Bundle bundle, ParcelFileDescriptor parcelFileDescriptor) throws RemoteException {
    }

    public void a(int i, Bundle bundle, at atVar) {
    }

    public void a(int i, Bundle bundle, String str, eb ebVar) {
    }

    public void a(int i, Bundle bundle, String str, List<String> list, List<String> list2, List<String> list3) {
    }

    public void a(k kVar, String str) {
    }

    public void a(k kVar, String str, String str2) {
    }

    public void b(int i, Bundle bundle) {
    }

    public void b(int i, Bundle bundle, Bundle bundle2) {
    }

    public void b(k kVar, String str) {
    }

    public void c(int i, Bundle bundle) {
    }
}
