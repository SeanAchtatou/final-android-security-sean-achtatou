package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

public class cf implements ae, Geofence {
    public static final cg CREATOR = new cg();
    private final int T;
    private final long eQ;
    private final String es;
    private final int et;
    private final short ev;
    private final double ew;
    private final double ex;
    private final float ey;

    public cf(int i, String str, int i2, short s, double d, double d2, float f, long j) {
        w(str);
        b(f);
        a(d, d2);
        int K = K(i2);
        this.T = i;
        this.ev = s;
        this.es = str;
        this.ew = d;
        this.ex = d2;
        this.ey = f;
        this.eQ = j;
        this.et = K;
    }

    public cf(String str, int i, short s, double d, double d2, float f, long j) {
        this(1, str, i, s, d, d2, f, j);
    }

    private static int K(int i) {
        int i2 = i & 3;
        if (i2 != 0) {
            return i2;
        }
        throw new IllegalArgumentException("No supported transition specified: " + i);
    }

    private static String L(int i) {
        switch (i) {
            case 1:
                return "CIRCLE";
            default:
                return null;
        }
    }

    private static void a(double d, double d2) {
        if (d > 90.0d || d < -90.0d) {
            throw new IllegalArgumentException("invalid latitude: " + d);
        } else if (d2 > 180.0d || d2 < -180.0d) {
            throw new IllegalArgumentException("invalid longitude: " + d2);
        }
    }

    private static void b(float f) {
        if (f <= BitmapDescriptorFactory.HUE_RED) {
            throw new IllegalArgumentException("invalid radius: " + f);
        }
    }

    public static cf c(byte[] bArr) {
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(bArr, 0, bArr.length);
        obtain.setDataPosition(0);
        cf t = CREATOR.createFromParcel(obtain);
        obtain.recycle();
        return t;
    }

    private static void w(String str) {
        if (str == null || str.length() > 100) {
            throw new IllegalArgumentException("requestId is null or too long: " + str);
        }
    }

    public short aA() {
        return this.ev;
    }

    public float aB() {
        return this.ey;
    }

    public int aC() {
        return this.et;
    }

    public int describeContents() {
        cg cgVar = CREATOR;
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof cf)) {
            return false;
        }
        cf cfVar = (cf) obj;
        if (this.ey != cfVar.ey) {
            return false;
        }
        if (this.ew != cfVar.ew) {
            return false;
        }
        if (this.ex != cfVar.ex) {
            return false;
        }
        return this.ev == cfVar.ev;
    }

    public long getExpirationTime() {
        return this.eQ;
    }

    public double getLatitude() {
        return this.ew;
    }

    public double getLongitude() {
        return this.ex;
    }

    public String getRequestId() {
        return this.es;
    }

    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.ew);
        long doubleToLongBits2 = Double.doubleToLongBits(this.ex);
        return ((((((((((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) + 31) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)))) * 31) + Float.floatToIntBits(this.ey)) * 31) + this.ev) * 31) + this.et;
    }

    public String toString() {
        return String.format("Geofence[%s id:%s transitions:%d %.6f, %.6f %.0fm, @%d]", L(this.ev), this.es, Integer.valueOf(this.et), Double.valueOf(this.ew), Double.valueOf(this.ex), Float.valueOf(this.ey), Long.valueOf(this.eQ));
    }

    public int u() {
        return this.T;
    }

    public void writeToParcel(Parcel parcel, int flags) {
        cg cgVar = CREATOR;
        cg.a(this, parcel, flags);
    }
}
