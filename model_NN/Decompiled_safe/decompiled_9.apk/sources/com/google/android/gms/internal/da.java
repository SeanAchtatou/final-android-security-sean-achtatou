package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.GroundOverlayOptions;

public class da {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
     arg types: [android.os.Parcel, int, android.os.IBinder, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLngBounds, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public static void a(GroundOverlayOptions groundOverlayOptions, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, groundOverlayOptions.u());
        ad.a(parcel, 2, groundOverlayOptions.aX(), false);
        ad.a(parcel, 3, (Parcelable) groundOverlayOptions.getLocation(), i, false);
        ad.a(parcel, 4, groundOverlayOptions.getWidth());
        ad.a(parcel, 5, groundOverlayOptions.getHeight());
        ad.a(parcel, 6, (Parcelable) groundOverlayOptions.getBounds(), i, false);
        ad.a(parcel, 7, groundOverlayOptions.getBearing());
        ad.a(parcel, 8, groundOverlayOptions.getZIndex());
        ad.a(parcel, 9, groundOverlayOptions.isVisible());
        ad.a(parcel, 10, groundOverlayOptions.getTransparency());
        ad.a(parcel, 11, groundOverlayOptions.getAnchorU());
        ad.a(parcel, 12, groundOverlayOptions.getAnchorV());
        ad.C(parcel, d);
    }
}
