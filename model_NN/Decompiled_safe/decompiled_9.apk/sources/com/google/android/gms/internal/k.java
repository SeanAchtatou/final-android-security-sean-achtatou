package com.google.android.gms.internal;

import android.database.CharArrayBuffer;
import android.database.CursorIndexOutOfBoundsException;
import android.database.CursorWindow;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class k implements ae {
    public static final l CREATOR = new l();
    private static final a aa = new a(new String[0], null) {
    };
    int T;
    String[] U;
    Bundle V;
    CursorWindow[] W;
    Bundle X;
    int[] Y;
    int Z;
    boolean mClosed;
    int p;

    public static class a {
        /* access modifiers changed from: private */
        public final String[] U;
        /* access modifiers changed from: private */
        public final ArrayList<HashMap<String, Object>> ab;
        private final String ac;
        private final HashMap<Object, Integer> ad;
        private boolean ae;
        private String af;

        private a(String[] strArr, String str) {
            this.U = (String[]) x.d(strArr);
            this.ab = new ArrayList<>();
            this.ac = str;
            this.ad = new HashMap<>();
            this.ae = false;
            this.af = null;
        }
    }

    k() {
        this.mClosed = false;
    }

    private k(a aVar, int i, Bundle bundle) {
        this(aVar.U, a(aVar), i, bundle);
    }

    public k(String[] strArr, CursorWindow[] cursorWindowArr, int i, Bundle bundle) {
        this.mClosed = false;
        this.T = 1;
        this.U = (String[]) x.d(strArr);
        this.W = (CursorWindow[]) x.d(cursorWindowArr);
        this.p = i;
        this.X = bundle;
        g();
    }

    public static k a(int i, Bundle bundle) {
        return new k(aa, i, bundle);
    }

    private void a(String str, int i) {
        if (this.V == null || !this.V.containsKey(str)) {
            throw new IllegalArgumentException("No such column: " + str);
        } else if (isClosed()) {
            throw new IllegalArgumentException("Buffer is closed.");
        } else if (i < 0 || i >= this.Z) {
            throw new CursorIndexOutOfBoundsException(i, this.Z);
        }
    }

    private static CursorWindow[] a(a aVar) {
        if (aVar.U.length == 0) {
            return new CursorWindow[0];
        }
        ArrayList c = aVar.ab;
        int size = c.size();
        CursorWindow cursorWindow = new CursorWindow(false);
        CursorWindow[] cursorWindowArr = {cursorWindow};
        cursorWindow.setNumColumns(aVar.U.length);
        int i = 0;
        while (i < size) {
            try {
                if (!cursorWindow.allocRow()) {
                    throw new RuntimeException("Cursor window out of memory");
                }
                Map map = (Map) c.get(i);
                for (int i2 = 0; i2 < aVar.U.length; i2++) {
                    String str = aVar.U[i2];
                    Object obj = map.get(str);
                    if (obj == null) {
                        cursorWindow.putNull(i, i2);
                    } else if (obj instanceof String) {
                        cursorWindow.putString((String) obj, i, i2);
                    } else if (obj instanceof Long) {
                        cursorWindow.putLong(((Long) obj).longValue(), i, i2);
                    } else if (obj instanceof Integer) {
                        cursorWindow.putLong((long) ((Integer) obj).intValue(), i, i2);
                    } else if (obj instanceof Boolean) {
                        cursorWindow.putLong(((Boolean) obj).booleanValue() ? 1 : 0, i, i2);
                    } else if (obj instanceof byte[]) {
                        cursorWindow.putBlob((byte[]) obj, i, i2);
                    } else {
                        throw new IllegalArgumentException("Unsupported object for column " + str + ": " + obj);
                    }
                }
                i++;
            } catch (RuntimeException e) {
                cursorWindow.close();
                throw e;
            }
        }
        return cursorWindowArr;
    }

    public static k e(int i) {
        return a(i, (Bundle) null);
    }

    public long a(String str, int i, int i2) {
        a(str, i);
        return this.W[i2].getLong(i - this.Y[i2], this.V.getInt(str));
    }

    public void a(String str, int i, int i2, CharArrayBuffer charArrayBuffer) {
        a(str, i);
        this.W[i2].copyStringToBuffer(i - this.Y[i2], this.V.getInt(str), charArrayBuffer);
    }

    public int b(String str, int i, int i2) {
        a(str, i);
        return this.W[i2].getInt(i - this.Y[i2], this.V.getInt(str));
    }

    public String c(String str, int i, int i2) {
        a(str, i);
        return this.W[i2].getString(i - this.Y[i2], this.V.getInt(str));
    }

    public void close() {
        synchronized (this) {
            if (!this.mClosed) {
                this.mClosed = true;
                for (CursorWindow close : this.W) {
                    close.close();
                }
            }
        }
    }

    public int d(int i) {
        int i2 = 0;
        x.a(i >= 0 && i < this.Z);
        while (true) {
            if (i2 >= this.Y.length) {
                break;
            } else if (i < this.Y[i2]) {
                i2--;
                break;
            } else {
                i2++;
            }
        }
        return i2 == this.Y.length ? i2 - 1 : i2;
    }

    public boolean d(String str, int i, int i2) {
        a(str, i);
        return Long.valueOf(this.W[i2].getLong(i - this.Y[i2], this.V.getInt(str))).longValue() == 1;
    }

    public int describeContents() {
        l lVar = CREATOR;
        return 0;
    }

    public byte[] e(String str, int i, int i2) {
        a(str, i);
        return this.W[i2].getBlob(i - this.Y[i2], this.V.getInt(str));
    }

    public Uri f(String str, int i, int i2) {
        String c = c(str, i, i2);
        if (c == null) {
            return null;
        }
        return Uri.parse(c);
    }

    public void g() {
        this.V = new Bundle();
        for (int i = 0; i < this.U.length; i++) {
            this.V.putInt(this.U[i], i);
        }
        this.Y = new int[this.W.length];
        int i2 = 0;
        for (int i3 = 0; i3 < this.W.length; i3++) {
            this.Y[i3] = i2;
            i2 += this.W[i3].getNumRows();
        }
        this.Z = i2;
    }

    public boolean g(String str, int i, int i2) {
        a(str, i);
        return this.W[i2].isNull(i - this.Y[i2], this.V.getInt(str));
    }

    public int getCount() {
        return this.Z;
    }

    public int getStatusCode() {
        return this.p;
    }

    public Bundle h() {
        return this.X;
    }

    public boolean isClosed() {
        boolean z;
        synchronized (this) {
            z = this.mClosed;
        }
        return z;
    }

    public void writeToParcel(Parcel dest, int flags) {
        l lVar = CREATOR;
        l.a(this, dest, flags);
    }
}
