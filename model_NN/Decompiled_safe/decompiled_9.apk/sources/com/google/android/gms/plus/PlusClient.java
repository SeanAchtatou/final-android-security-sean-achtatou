package com.google.android.gms.plus;

import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.internal.av;
import com.google.android.gms.internal.du;
import com.google.android.gms.internal.dy;
import com.google.android.gms.plus.model.moments.Moment;
import com.google.android.gms.plus.model.moments.MomentBuffer;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import java.util.ArrayList;
import java.util.Arrays;

public class PlusClient implements GooglePlayServicesClient {
    public static final String KEY_REQUEST_VISIBLE_ACTIVITIES = "request_visible_actions";
    final dy gJ;

    public static class Builder {
        private GooglePlayServicesClient.OnConnectionFailedListener e;
        private String g;
        private GooglePlayServicesClient.ConnectionCallbacks gK;
        private ArrayList<String> gL = new ArrayList<>();
        private String[] gM;
        private String[] gN;
        private String gO = this.mContext.getPackageName();
        private String gP = this.mContext.getPackageName();
        private Context mContext;

        public Builder(Context context, GooglePlayServicesClient.ConnectionCallbacks connectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener connectionFailedListener) {
            this.mContext = context;
            this.gK = connectionCallbacks;
            this.e = connectionFailedListener;
            this.gL.add(Scopes.PLUS_LOGIN);
        }

        public PlusClient build() {
            if (this.g == null) {
                this.g = "<<default account>>";
            }
            return new PlusClient(this.mContext, this.gP, this.gO, this.g, this.gK, this.e, this.gM, this.gN, (String[]) this.gL.toArray(new String[this.gL.size()]));
        }

        public Builder clearScopes() {
            this.gL.clear();
            return this;
        }

        public Builder setAccountName(String accountName) {
            this.g = accountName;
            return this;
        }

        public Builder setScopes(String... scopes) {
            this.gL.clear();
            this.gL.addAll(Arrays.asList(scopes));
            return this;
        }

        public Builder setVisibleActivities(String... visibleActivities) {
            this.gM = visibleActivities;
            return this;
        }
    }

    public interface OnAccessRevokedListener {
        void onAccessRevoked(ConnectionResult connectionResult);
    }

    public interface OnMomentsLoadedListener {
        void onMomentsLoaded(ConnectionResult connectionResult, MomentBuffer momentBuffer, String str, String str2);
    }

    public interface OnPeopleLoadedListener {
        void onPeopleLoaded(ConnectionResult connectionResult, PersonBuffer personBuffer, String str);
    }

    public interface OnPersonLoadedListener {
        void onPersonLoaded(ConnectionResult connectionResult, Person person);
    }

    public interface a {
        void a(ConnectionResult connectionResult, ParcelFileDescriptor parcelFileDescriptor);
    }

    public interface b {
        void a(ConnectionResult connectionResult, du duVar);
    }

    PlusClient(Context context, String callingPackage, String authPackage, String accountName, GooglePlayServicesClient.ConnectionCallbacks connectedListener, GooglePlayServicesClient.OnConnectionFailedListener connectionFailedListener, String[] visibleActions, String[] requiredFeatures, String[] scopes) {
        this.gJ = new dy(context, callingPackage, authPackage, accountName, connectedListener, connectionFailedListener, visibleActions, requiredFeatures, scopes);
    }

    public boolean A(String str) {
        return av.a(this.gJ.j(), str);
    }

    public void a(a aVar, Uri uri, int i) {
        this.gJ.a(aVar, uri, i);
    }

    public void a(b bVar, String str) {
        this.gJ.a(bVar, str);
    }

    public void clearDefaultAccount() {
        this.gJ.clearDefaultAccount();
    }

    public void connect() {
        this.gJ.connect();
    }

    public void disconnect() {
        this.gJ.disconnect();
    }

    public String getAccountName() {
        return this.gJ.getAccountName();
    }

    public Person getCurrentPerson() {
        return this.gJ.getCurrentPerson();
    }

    public boolean isConnected() {
        return this.gJ.isConnected();
    }

    public boolean isConnecting() {
        return this.gJ.isConnecting();
    }

    public boolean isConnectionCallbacksRegistered(GooglePlayServicesClient.ConnectionCallbacks listener) {
        return this.gJ.isConnectionCallbacksRegistered(listener);
    }

    public boolean isConnectionFailedListenerRegistered(GooglePlayServicesClient.OnConnectionFailedListener listener) {
        return this.gJ.isConnectionFailedListenerRegistered(listener);
    }

    public void loadMoments(OnMomentsLoadedListener listener) {
        this.gJ.loadMoments(listener, 20, null, null, null, "me");
    }

    public void loadMoments(OnMomentsLoadedListener listener, int maxResults, String pageToken, Uri targetUrl, String type, String userId) {
        this.gJ.loadMoments(listener, maxResults, pageToken, targetUrl, type, userId);
    }

    public void loadPeople(OnPeopleLoadedListener listener, int collection) {
        this.gJ.loadPeople(listener, collection, 0, 100, null);
    }

    public void loadPeople(OnPeopleLoadedListener listener, int collection, int orderBy, int maxResults, String pageToken) {
        this.gJ.loadPeople(listener, collection, orderBy, maxResults, pageToken);
    }

    public void loadPerson(OnPersonLoadedListener listener, String userId) {
        this.gJ.loadPerson(listener, userId);
    }

    public void registerConnectionCallbacks(GooglePlayServicesClient.ConnectionCallbacks listener) {
        this.gJ.registerConnectionCallbacks(listener);
    }

    public void registerConnectionFailedListener(GooglePlayServicesClient.OnConnectionFailedListener listener) {
        this.gJ.registerConnectionFailedListener(listener);
    }

    public void removeMoment(String momentId) {
        this.gJ.removeMoment(momentId);
    }

    public void revokeAccessAndDisconnect(OnAccessRevokedListener listener) {
        this.gJ.revokeAccessAndDisconnect(listener);
    }

    public void unregisterConnectionCallbacks(GooglePlayServicesClient.ConnectionCallbacks listener) {
        this.gJ.unregisterConnectionCallbacks(listener);
    }

    public void unregisterConnectionFailedListener(GooglePlayServicesClient.OnConnectionFailedListener listener) {
        this.gJ.unregisterConnectionFailedListener(listener);
    }

    public void writeMoment(Moment moment) {
        this.gJ.writeMoment(moment);
    }
}
