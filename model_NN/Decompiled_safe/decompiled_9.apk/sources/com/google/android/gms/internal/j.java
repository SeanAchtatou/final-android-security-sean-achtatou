package com.google.android.gms.internal;

import android.database.CharArrayBuffer;
import android.net.Uri;

public abstract class j {
    protected final k O;
    protected final int R;
    private final int S;

    public j(k kVar, int i) {
        this.O = (k) x.d(kVar);
        x.a(i >= 0 && i < kVar.getCount());
        this.R = i;
        this.S = kVar.d(this.R);
    }

    /* access modifiers changed from: protected */
    public void a(String str, CharArrayBuffer charArrayBuffer) {
        this.O.a(str, this.R, this.S, charArrayBuffer);
    }

    /* access modifiers changed from: protected */
    public Uri c(String str) {
        return this.O.f(str, this.R, this.S);
    }

    /* access modifiers changed from: protected */
    public boolean d(String str) {
        return this.O.g(str, this.R, this.S);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof j)) {
            return false;
        }
        j jVar = (j) obj;
        return w.a(Integer.valueOf(jVar.R), Integer.valueOf(this.R)) && w.a(Integer.valueOf(jVar.S), Integer.valueOf(this.S)) && jVar.O == this.O;
    }

    /* access modifiers changed from: protected */
    public boolean getBoolean(String column) {
        return this.O.d(column, this.R, this.S);
    }

    /* access modifiers changed from: protected */
    public byte[] getByteArray(String column) {
        return this.O.e(column, this.R, this.S);
    }

    /* access modifiers changed from: protected */
    public int getInteger(String column) {
        return this.O.b(column, this.R, this.S);
    }

    /* access modifiers changed from: protected */
    public long getLong(String column) {
        return this.O.a(column, this.R, this.S);
    }

    /* access modifiers changed from: protected */
    public String getString(String column) {
        return this.O.c(column, this.R, this.S);
    }

    public int hashCode() {
        return w.hashCode(Integer.valueOf(this.R), Integer.valueOf(this.S), this.O);
    }
}
