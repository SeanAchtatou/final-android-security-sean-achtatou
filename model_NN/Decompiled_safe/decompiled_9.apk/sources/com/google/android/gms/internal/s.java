package com.google.android.gms.internal;

import android.util.Log;

public final class s {
    private final String aV;

    public s(String str) {
        this.aV = (String) x.d(str);
    }

    public void a(String str, String str2) {
        if (i(3)) {
            Log.d(str, str2);
        }
    }

    public void a(String str, String str2, Throwable th) {
        if (i(6)) {
            Log.e(str, str2, th);
        }
    }

    public void b(String str, String str2) {
        if (i(5)) {
            Log.w(str, str2);
        }
    }

    public void c(String str, String str2) {
        if (i(6)) {
            Log.e(str, str2);
        }
    }

    public void d(String str, String str2) {
        if (i(4)) {
        }
    }

    public boolean i(int i) {
        return Log.isLoggable(this.aV, i);
    }
}
