package com.google.android.gms.games.multiplayer.realtime;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.x;

public final class RealTimeMessage implements Parcelable {
    public static final Parcelable.Creator<RealTimeMessage> CREATOR = new Parcelable.Creator<RealTimeMessage>() {
        /* renamed from: E */
        public RealTimeMessage[] newArray(int i) {
            return new RealTimeMessage[i];
        }

        /* renamed from: r */
        public RealTimeMessage createFromParcel(Parcel parcel) {
            return new RealTimeMessage(parcel);
        }
    };
    public static final int RELIABLE = 1;
    public static final int UNRELIABLE = 0;
    private final String dR;
    private final byte[] dS;
    private final int dT;

    private RealTimeMessage(Parcel parcel) {
        this(parcel.readString(), parcel.createByteArray(), parcel.readInt());
    }

    public RealTimeMessage(String senderParticipantId, byte[] messageData, int isReliable) {
        this.dR = (String) x.d(senderParticipantId);
        this.dS = (byte[]) ((byte[]) x.d(messageData)).clone();
        this.dT = isReliable;
    }

    public int describeContents() {
        return 0;
    }

    public byte[] getMessageData() {
        return this.dS;
    }

    public String getSenderParticipantId() {
        return this.dR;
    }

    public boolean isReliable() {
        return this.dT == 1;
    }

    public void writeToParcel(Parcel parcel, int flag) {
        parcel.writeString(this.dR);
        parcel.writeByteArray(this.dS);
        parcel.writeInt(this.dT);
    }
}
