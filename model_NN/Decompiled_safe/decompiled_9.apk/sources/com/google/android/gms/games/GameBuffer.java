package com.google.android.gms.games;

import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.internal.bf;
import com.google.android.gms.internal.k;

public final class GameBuffer extends DataBuffer<Game> {
    public GameBuffer(k dataHolder) {
        super(dataHolder);
    }

    public Game get(int position) {
        return new bf(this.O, position);
    }
}
