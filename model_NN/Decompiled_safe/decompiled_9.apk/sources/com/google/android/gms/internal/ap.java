package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.an;
import com.google.android.gms.internal.aq;

public class ap implements Parcelable.Creator<aq.b> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.an$a<?, ?>, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(aq.b bVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, bVar.versionCode);
        ad.a(parcel, 2, bVar.bI, false);
        ad.a(parcel, 3, (Parcelable) bVar.bJ, i, false);
        ad.C(parcel, d);
    }

    /* renamed from: j */
    public aq.b createFromParcel(Parcel parcel) {
        an.a aVar = null;
        int c = ac.c(parcel);
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i = ac.f(parcel, b);
                    break;
                case 2:
                    str = ac.l(parcel, b);
                    break;
                case 3:
                    aVar = (an.a) ac.a(parcel, b, an.a.CREATOR);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new aq.b(i, str, aVar);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    /* renamed from: p */
    public aq.b[] newArray(int i) {
        return new aq.b[i];
    }
}
