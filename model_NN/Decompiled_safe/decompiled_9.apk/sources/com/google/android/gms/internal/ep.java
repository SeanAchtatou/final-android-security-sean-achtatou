package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.eq;
import java.util.HashSet;
import java.util.Set;

public class ep implements Parcelable.Creator<eq.g> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(eq.g gVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        Set<Integer> by = gVar.by();
        if (by.contains(1)) {
            ad.c(parcel, 1, gVar.u());
        }
        if (by.contains(2)) {
            ad.a(parcel, 2, gVar.getDepartment(), true);
        }
        if (by.contains(3)) {
            ad.a(parcel, 3, gVar.getDescription(), true);
        }
        if (by.contains(4)) {
            ad.a(parcel, 4, gVar.getEndDate(), true);
        }
        if (by.contains(5)) {
            ad.a(parcel, 5, gVar.getLocation(), true);
        }
        if (by.contains(6)) {
            ad.a(parcel, 6, gVar.getName(), true);
        }
        if (by.contains(7)) {
            ad.a(parcel, 7, gVar.isPrimary());
        }
        if (by.contains(8)) {
            ad.a(parcel, 8, gVar.getStartDate(), true);
        }
        if (by.contains(9)) {
            ad.a(parcel, 9, gVar.getTitle(), true);
        }
        if (by.contains(10)) {
            ad.c(parcel, 10, gVar.getType());
        }
        ad.C(parcel, d);
    }

    /* renamed from: E */
    public eq.g createFromParcel(Parcel parcel) {
        int i = 0;
        String str = null;
        int c = ac.c(parcel);
        HashSet hashSet = new HashSet();
        String str2 = null;
        boolean z = false;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        int i2 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i2 = ac.f(parcel, b);
                    hashSet.add(1);
                    break;
                case 2:
                    str7 = ac.l(parcel, b);
                    hashSet.add(2);
                    break;
                case 3:
                    str6 = ac.l(parcel, b);
                    hashSet.add(3);
                    break;
                case 4:
                    str5 = ac.l(parcel, b);
                    hashSet.add(4);
                    break;
                case 5:
                    str4 = ac.l(parcel, b);
                    hashSet.add(5);
                    break;
                case 6:
                    str3 = ac.l(parcel, b);
                    hashSet.add(6);
                    break;
                case 7:
                    z = ac.c(parcel, b);
                    hashSet.add(7);
                    break;
                case 8:
                    str2 = ac.l(parcel, b);
                    hashSet.add(8);
                    break;
                case 9:
                    str = ac.l(parcel, b);
                    hashSet.add(9);
                    break;
                case 10:
                    i = ac.f(parcel, b);
                    hashSet.add(10);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new eq.g(hashSet, i2, str7, str6, str5, str4, str3, z, str2, str, i);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    /* renamed from: Y */
    public eq.g[] newArray(int i) {
        return new eq.g[i];
    }
}
