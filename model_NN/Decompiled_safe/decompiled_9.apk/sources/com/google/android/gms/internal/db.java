package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLngBounds;

public class db {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.ad.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public static void a(LatLngBounds latLngBounds, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, latLngBounds.u());
        ad.a(parcel, 2, (Parcelable) latLngBounds.southwest, i, false);
        ad.a(parcel, 3, (Parcelable) latLngBounds.northeast, i, false);
        ad.C(parcel, d);
    }
}
