package com.google.android.gms.internal;

import com.google.android.gms.common.data.DataBuffer;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class i<T> implements Iterator<T> {
    private final DataBuffer<T> P;
    private int Q = -1;

    public i(DataBuffer<T> dataBuffer) {
        this.P = (DataBuffer) x.d(dataBuffer);
    }

    public boolean hasNext() {
        return this.Q < this.P.getCount() + -1;
    }

    public T next() {
        if (!hasNext()) {
            throw new NoSuchElementException("Cannot advance the iterator beyond " + this.Q);
        }
        DataBuffer<T> dataBuffer = this.P;
        int i = this.Q + 1;
        this.Q = i;
        return dataBuffer.get(i);
    }

    public void remove() {
        throw new UnsupportedOperationException("Cannot remove elements from a DataBufferIterator");
    }
}
