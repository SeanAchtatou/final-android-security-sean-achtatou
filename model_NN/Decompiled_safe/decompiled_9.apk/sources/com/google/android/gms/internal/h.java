package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class h implements ServiceConnection {
    boolean u = false;
    private final BlockingQueue<IBinder> v = new LinkedBlockingQueue();

    public IBinder d() throws InterruptedException {
        if (this.u) {
            throw new IllegalStateException();
        }
        this.u = true;
        return this.v.take();
    }

    public void onServiceConnected(ComponentName name, IBinder service) {
        try {
            this.v.put(service);
        } catch (InterruptedException e) {
        }
    }

    public void onServiceDisconnected(ComponentName name) {
    }
}
