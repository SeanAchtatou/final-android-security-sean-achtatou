package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.eq;
import java.util.HashSet;
import java.util.Set;

public class ek implements Parcelable.Creator<eq.b.a> {
    static void a(eq.b.a aVar, Parcel parcel, int i) {
        int d = ad.d(parcel);
        Set<Integer> by = aVar.by();
        if (by.contains(1)) {
            ad.c(parcel, 1, aVar.u());
        }
        if (by.contains(2)) {
            ad.c(parcel, 2, aVar.getLeftImageOffset());
        }
        if (by.contains(3)) {
            ad.c(parcel, 3, aVar.getTopImageOffset());
        }
        ad.C(parcel, d);
    }

    /* renamed from: T */
    public eq.b.a[] newArray(int i) {
        return new eq.b.a[i];
    }

    /* renamed from: z */
    public eq.b.a createFromParcel(Parcel parcel) {
        int i = 0;
        int c = ac.c(parcel);
        HashSet hashSet = new HashSet();
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i3 = ac.f(parcel, b);
                    hashSet.add(1);
                    break;
                case 2:
                    i2 = ac.f(parcel, b);
                    hashSet.add(2);
                    break;
                case 3:
                    i = ac.f(parcel, b);
                    hashSet.add(3);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new eq.b.a(hashSet, i3, i2, i);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }
}
