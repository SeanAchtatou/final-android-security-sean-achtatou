package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.internal.ca;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import java.util.HashMap;

public class cd {
    private final ch<cc> eE;
    private ContentProviderClient eF = null;
    private HashMap<LocationListener, b> eG = new HashMap<>();
    private final ContentResolver mContentResolver;

    static class a extends Handler {
        private final LocationListener eH;

        public a(LocationListener locationListener) {
            this.eH = locationListener;
        }

        public a(LocationListener locationListener, Looper looper) {
            super(looper);
            this.eH = locationListener;
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    this.eH.onLocationChanged(new Location((Location) msg.obj));
                    return;
                default:
                    Log.e("LocationClientHelper", "unknown message in LocationHandler.handleMessage");
                    return;
            }
        }
    }

    static class b extends ca.a {
        private final Handler eI;

        b(LocationListener locationListener, Looper looper) {
            this.eI = looper == null ? new a(locationListener) : new a(locationListener, looper);
        }

        public void onLocationChanged(Location location) {
            Message obtain = Message.obtain();
            obtain.what = 1;
            obtain.obj = location;
            this.eI.sendMessage(obtain);
        }
    }

    public cd(Context context, ch<cc> chVar) {
        this.eE = chVar;
        this.mContentResolver = context.getContentResolver();
    }

    public Location getLastLocation() {
        this.eE.n();
        try {
            return this.eE.o().ay();
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeAllListeners() {
        try {
            synchronized (this.eG) {
                for (b next : this.eG.values()) {
                    if (next != null) {
                        this.eE.o().a(next);
                    }
                }
                this.eG.clear();
            }
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeLocationUpdates(PendingIntent callbackIntent) {
        this.eE.n();
        try {
            this.eE.o().a(callbackIntent);
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeLocationUpdates(LocationListener listener) {
        this.eE.n();
        x.b(listener, "Invalid null listener");
        synchronized (this.eG) {
            b remove = this.eG.remove(listener);
            if (this.eF != null && this.eG.isEmpty()) {
                this.eF.release();
                this.eF = null;
            }
            if (remove != null) {
                try {
                    this.eE.o().a(remove);
                } catch (RemoteException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }

    public void requestLocationUpdates(LocationRequest request, PendingIntent callbackIntent) {
        this.eE.n();
        try {
            this.eE.o().a(request, callbackIntent);
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public void requestLocationUpdates(LocationRequest request, LocationListener listener, Looper looper) {
        this.eE.n();
        if (looper == null) {
            x.b(Looper.myLooper(), "Can't create handler inside thread that has not called Looper.prepare()");
        }
        synchronized (this.eG) {
            b bVar = this.eG.get(listener);
            b bVar2 = bVar == null ? new b(listener, looper) : bVar;
            this.eG.put(listener, bVar2);
            try {
                this.eE.o().a(request, bVar2);
            } catch (RemoteException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
