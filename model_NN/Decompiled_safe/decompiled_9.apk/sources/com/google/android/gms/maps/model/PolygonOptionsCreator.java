package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ad;
import java.util.ArrayList;

public class PolygonOptionsCreator implements Parcelable.Creator<PolygonOptions> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void a(PolygonOptions polygonOptions, Parcel parcel, int i) {
        int d = ad.d(parcel);
        ad.c(parcel, 1, polygonOptions.u());
        ad.b(parcel, 2, polygonOptions.getPoints(), false);
        ad.c(parcel, 3, polygonOptions.aZ(), false);
        ad.a(parcel, 4, polygonOptions.getStrokeWidth());
        ad.c(parcel, 5, polygonOptions.getStrokeColor());
        ad.c(parcel, 6, polygonOptions.getFillColor());
        ad.a(parcel, 7, polygonOptions.getZIndex());
        ad.a(parcel, 8, polygonOptions.isVisible());
        ad.a(parcel, 9, polygonOptions.isGeodesic());
        ad.C(parcel, d);
    }

    public PolygonOptions createFromParcel(Parcel parcel) {
        float f = BitmapDescriptorFactory.HUE_RED;
        boolean z = false;
        int c = ac.c(parcel);
        ArrayList arrayList = null;
        ArrayList arrayList2 = new ArrayList();
        boolean z2 = false;
        int i = 0;
        int i2 = 0;
        float f2 = 0.0f;
        int i3 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.b(parcel);
            switch (ac.j(b)) {
                case 1:
                    i3 = ac.f(parcel, b);
                    break;
                case 2:
                    arrayList = ac.c(parcel, b, LatLng.CREATOR);
                    break;
                case 3:
                    ac.a(parcel, b, arrayList2, getClass().getClassLoader());
                    break;
                case 4:
                    f2 = ac.i(parcel, b);
                    break;
                case 5:
                    i2 = ac.f(parcel, b);
                    break;
                case 6:
                    i = ac.f(parcel, b);
                    break;
                case 7:
                    f = ac.i(parcel, b);
                    break;
                case 8:
                    z2 = ac.c(parcel, b);
                    break;
                case 9:
                    z = ac.c(parcel, b);
                    break;
                default:
                    ac.b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new PolygonOptions(i3, arrayList, arrayList2, f2, i2, i, f, z2, z);
        }
        throw new ac.a("Overread allowed size end=" + c, parcel);
    }

    public PolygonOptions[] newArray(int size) {
        return new PolygonOptions[size];
    }
}
