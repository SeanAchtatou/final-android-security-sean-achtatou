package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.cb;
import com.google.android.gms.internal.cc;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationStatusCodes;
import java.util.List;

public class ce extends p<cc> {
    private final ch<cc> eE = new c();
    private final cd eJ;
    private final String eK;

    final class a extends p<cc>.b<LocationClient.OnAddGeofencesResultListener> {
        private final String[] eL;
        private final int p;

        public a(LocationClient.OnAddGeofencesResultListener onAddGeofencesResultListener, int i, String[] strArr) {
            super(onAddGeofencesResultListener);
            this.p = LocationStatusCodes.J(i);
            this.eL = strArr;
        }

        /* access modifiers changed from: protected */
        public void a(LocationClient.OnAddGeofencesResultListener onAddGeofencesResultListener) {
            if (onAddGeofencesResultListener != null) {
                onAddGeofencesResultListener.onAddGeofencesResult(this.p, this.eL);
            }
        }
    }

    final class b extends cb.a {
        private final LocationClient.OnAddGeofencesResultListener eN;
        private final LocationClient.OnRemoveGeofencesResultListener eO;

        public b(LocationClient.OnAddGeofencesResultListener onAddGeofencesResultListener) {
            this.eN = onAddGeofencesResultListener;
            this.eO = null;
        }

        public b(LocationClient.OnRemoveGeofencesResultListener onRemoveGeofencesResultListener) {
            this.eO = onRemoveGeofencesResultListener;
            this.eN = null;
        }

        public void onAddGeofencesResult(int statusCode, String[] geofenceRequestIds) throws RemoteException {
            ce.this.a(new a(this.eN, statusCode, geofenceRequestIds));
        }

        public void onRemoveGeofencesByPendingIntentResult(int statusCode, PendingIntent pendingIntent) {
            ce.this.a(new d(ce.this, 1, this.eO, statusCode, pendingIntent));
        }

        public void onRemoveGeofencesByRequestIdsResult(int statusCode, String[] geofenceRequestIds) {
            ce.this.a(new d(2, this.eO, statusCode, geofenceRequestIds));
        }
    }

    final class c implements ch<cc> {
        private c() {
        }

        /* renamed from: az */
        public cc o() {
            return (cc) ce.this.o();
        }

        public void n() {
            ce.this.n();
        }
    }

    final class d extends p<cc>.b<LocationClient.OnRemoveGeofencesResultListener> {
        private final String[] eL;
        private final int eP;
        private final PendingIntent mPendingIntent;
        private final int p;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ce ceVar, int i, LocationClient.OnRemoveGeofencesResultListener onRemoveGeofencesResultListener, int i2, PendingIntent pendingIntent) {
            super(onRemoveGeofencesResultListener);
            boolean z = true;
            ce.this = ceVar;
            n.a(i != 1 ? false : z);
            this.eP = i;
            this.p = LocationStatusCodes.J(i2);
            this.mPendingIntent = pendingIntent;
            this.eL = null;
        }

        public d(int i, LocationClient.OnRemoveGeofencesResultListener onRemoveGeofencesResultListener, int i2, String[] strArr) {
            super(onRemoveGeofencesResultListener);
            n.a(i == 2);
            this.eP = i;
            this.p = LocationStatusCodes.J(i2);
            this.eL = strArr;
            this.mPendingIntent = null;
        }

        /* access modifiers changed from: protected */
        public void a(LocationClient.OnRemoveGeofencesResultListener onRemoveGeofencesResultListener) {
            if (onRemoveGeofencesResultListener != null) {
                switch (this.eP) {
                    case 1:
                        onRemoveGeofencesResultListener.onRemoveGeofencesByPendingIntentResult(this.p, this.mPendingIntent);
                        return;
                    case 2:
                        onRemoveGeofencesResultListener.onRemoveGeofencesByRequestIdsResult(this.p, this.eL);
                        return;
                    default:
                        Log.wtf("LocationClientImpl", "Unsupported action: " + this.eP);
                        return;
                }
            }
        }
    }

    public ce(Context context, GooglePlayServicesClient.ConnectionCallbacks connectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener onConnectionFailedListener, String str) {
        super(context, connectionCallbacks, onConnectionFailedListener, new String[0]);
        this.eJ = new cd(context, this.eE);
        this.eK = str;
    }

    /* access modifiers changed from: protected */
    public void a(u uVar, p<cc>.d dVar) throws RemoteException {
        Bundle bundle = new Bundle();
        bundle.putString("client_name", this.eK);
        uVar.e(dVar, GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, getContext().getPackageName(), bundle);
    }

    public void addGeofences(List<cf> geofences, PendingIntent pendingIntent, LocationClient.OnAddGeofencesResultListener listener) {
        n();
        x.b(geofences != null && geofences.size() > 0, "At least one geofence must be specified.");
        x.b(pendingIntent, "PendingIntent must be specified.");
        x.b(listener, "OnAddGeofencesResultListener not provided.");
        try {
            ((cc) o()).a(geofences, pendingIntent, listener == null ? null : new b(listener), getContext().getPackageName());
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "com.google.android.location.internal.GoogleLocationManagerService.START";
    }

    /* access modifiers changed from: protected */
    public String c() {
        return "com.google.android.gms.location.internal.IGoogleLocationManagerService";
    }

    public void disconnect() {
        synchronized (this.eJ) {
            if (isConnected()) {
                this.eJ.removeAllListeners();
            }
            super.disconnect();
        }
    }

    public Location getLastLocation() {
        return this.eJ.getLastLocation();
    }

    /* access modifiers changed from: protected */
    /* renamed from: q */
    public cc c(IBinder iBinder) {
        return cc.a.p(iBinder);
    }

    public void removeActivityUpdates(PendingIntent callbackIntent) {
        n();
        x.d(callbackIntent);
        try {
            ((cc) o()).removeActivityUpdates(callbackIntent);
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeGeofences(PendingIntent pendingIntent, LocationClient.OnRemoveGeofencesResultListener listener) {
        n();
        x.b(pendingIntent, "PendingIntent must be specified.");
        x.b(listener, "OnRemoveGeofencesResultListener not provided.");
        try {
            ((cc) o()).a(pendingIntent, listener == null ? null : new b(listener), getContext().getPackageName());
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeGeofences(List<String> geofenceRequestIds, LocationClient.OnRemoveGeofencesResultListener listener) {
        n();
        x.b(geofenceRequestIds != null && geofenceRequestIds.size() > 0, "geofenceRequestIds can't be null nor empty.");
        x.b(listener, "OnRemoveGeofencesResultListener not provided.");
        try {
            ((cc) o()).a((String[]) geofenceRequestIds.toArray(new String[0]), listener == null ? null : new b(listener), getContext().getPackageName());
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeLocationUpdates(PendingIntent callbackIntent) {
        this.eJ.removeLocationUpdates(callbackIntent);
    }

    public void removeLocationUpdates(LocationListener listener) {
        this.eJ.removeLocationUpdates(listener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.cc.a(long, boolean, android.app.PendingIntent):void
     arg types: [long, int, android.app.PendingIntent]
     candidates:
      com.google.android.gms.internal.cc.a(android.app.PendingIntent, com.google.android.gms.internal.cb, java.lang.String):void
      com.google.android.gms.internal.cc.a(java.lang.String[], com.google.android.gms.internal.cb, java.lang.String):void
      com.google.android.gms.internal.cc.a(long, boolean, android.app.PendingIntent):void */
    public void requestActivityUpdates(long detectionIntervalMillis, PendingIntent callbackIntent) {
        boolean z = true;
        n();
        x.d(callbackIntent);
        if (detectionIntervalMillis < 0) {
            z = false;
        }
        x.b(z, "detectionIntervalMillis must be >= 0");
        try {
            ((cc) o()).a(detectionIntervalMillis, true, callbackIntent);
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public void requestLocationUpdates(LocationRequest request, PendingIntent callbackIntent) {
        this.eJ.requestLocationUpdates(request, callbackIntent);
    }

    public void requestLocationUpdates(LocationRequest request, LocationListener listener) {
        requestLocationUpdates(request, listener, null);
    }

    public void requestLocationUpdates(LocationRequest request, LocationListener listener, Looper looper) {
        synchronized (this.eJ) {
            this.eJ.requestLocationUpdates(request, listener, looper);
        }
    }
}
