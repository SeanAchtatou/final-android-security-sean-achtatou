package softkos.moveme;

public class Egg extends GameObject {
    int board_x;
    int board_y;
    int state;
    int type;

    public Egg() {
        this.state = 0;
        this.type = 0;
        this.state = 0;
    }

    public void setState(int s) {
        this.state = s;
    }

    public int getState() {
        return this.state;
    }

    public void setBoardPos(int x, int y) {
        this.board_x = x;
        this.board_y = y;
    }

    public int getBoardPosX() {
        return this.board_x;
    }

    public int getBoardPosY() {
        return this.board_y;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int t) {
        this.type = t;
    }

    public boolean update() {
        boolean moved = false;
        int speed = 20;
        if (Vars.getInstance().getScreenWidth() > 400) {
            speed = 35;
        }
        if (getPy() != getDestPy()) {
            if (Math.abs(getPy() - getDestPy()) < speed) {
                SetPos(getPx(), getDestPy());
            }
            if (getPy() < getDestPy()) {
                SetPos(getPx(), getPy() + speed);
            }
            if (getPy() > getDestPy()) {
                SetPos(getPx(), getPy() - speed);
            }
            moved = true;
        }
        if (getPx() == getDestPx()) {
            return moved;
        }
        if (Math.abs(getPx() - getDestPx()) < speed) {
            SetPos(getDestPx(), getPy());
        }
        if (getPx() < getDestPx()) {
            SetPos(getPx() + speed, getPy());
        }
        if (getPx() > getDestPx()) {
            SetPos(getPx() - speed, getPy());
        }
        return true;
    }
}
