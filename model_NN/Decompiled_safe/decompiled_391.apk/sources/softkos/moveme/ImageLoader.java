package softkos.moveme;

import android.graphics.drawable.Drawable;

public class ImageLoader {
    static ImageLoader instance = null;
    String[] image_names = {"240x50off", "240x50on", "gamename", "twitter", "cell_0", "cell_1", "cell_2", "cell_3", "cell_4", "cell_5", "cell_6", "cell_7", "cell_8", "full_cell", "small_circle", "star_0", "arrow_left", "arrow_right", "arrow_down", "arrow_up", "stage", "solved", "rotateme_ad", "frogs_jump_ad", "untangle_ad", "cubix_ad", "turnmeon_ad", "boxit_ad", "tripeaks_ad", "ume_ad"};
    Drawable[] images;
    int[] img_id = {R.drawable.button_off, R.drawable.button_on, R.drawable.gamename, R.drawable.twitter, R.drawable.cell_9, R.drawable.cell_1, R.drawable.cell_2, R.drawable.cell_3, R.drawable.cell_4, R.drawable.cell_5, R.drawable.cell_6, R.drawable.cell_7, R.drawable.cell_8, R.drawable.full_cell, R.drawable.smallcircle, R.drawable.star_0, R.drawable.key_left, R.drawable.key_right, R.drawable.key_down, R.drawable.key_up, R.drawable.stage, R.drawable.solved, R.drawable.ad_rotateme, R.drawable.ad_frogs_jump, R.drawable.ad_untangle, R.drawable.ad_cubix, R.drawable.ad_turnmeon, R.drawable.ad_boxit, R.drawable.tripeaks_ad, R.drawable.ume_ad};

    public static ImageLoader getInstance() {
        if (instance == null) {
            instance = new ImageLoader();
        }
        return instance;
    }

    public void LoadImages() {
        int len = this.img_id.length;
        this.images = new Drawable[this.image_names.length];
        for (int i = 0; i < len; i++) {
            this.images[i] = MainActivity.getInstance().getResources().getDrawable(this.img_id[i]);
        }
    }

    public Drawable getImage(String img) {
        for (int i = 0; i < this.image_names.length; i++) {
            if (img.equals(this.image_names[i])) {
                return this.images[i];
            }
        }
        return null;
    }
}
