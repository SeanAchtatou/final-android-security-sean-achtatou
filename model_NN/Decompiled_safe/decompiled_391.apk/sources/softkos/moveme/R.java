package softkos.moveme;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int ad_boxit = 2130837504;
        public static final int ad_cubix = 2130837505;
        public static final int ad_frogs_jump = 2130837506;
        public static final int ad_rotateme = 2130837507;
        public static final int ad_turnmeon = 2130837508;
        public static final int ad_untangle = 2130837509;
        public static final int arrow_left = 2130837510;
        public static final int arrow_right = 2130837511;
        public static final int bomb_it_ad = 2130837512;
        public static final int button_off = 2130837513;
        public static final int button_on = 2130837514;
        public static final int cell_1 = 2130837515;
        public static final int cell_2 = 2130837516;
        public static final int cell_3 = 2130837517;
        public static final int cell_4 = 2130837518;
        public static final int cell_5 = 2130837519;
        public static final int cell_6 = 2130837520;
        public static final int cell_7 = 2130837521;
        public static final int cell_8 = 2130837522;
        public static final int cell_9 = 2130837523;
        public static final int full_cell = 2130837524;
        public static final int gamename = 2130837525;
        public static final int icon = 2130837526;
        public static final int key_down = 2130837527;
        public static final int key_left = 2130837528;
        public static final int key_right = 2130837529;
        public static final int key_up = 2130837530;
        public static final int smallcircle = 2130837531;
        public static final int solved = 2130837532;
        public static final int stage = 2130837533;
        public static final int star_0 = 2130837534;
        public static final int star_1 = 2130837535;
        public static final int star_2 = 2130837536;
        public static final int star_3 = 2130837537;
        public static final int star_4 = 2130837538;
        public static final int star_5 = 2130837539;
        public static final int tripeaks_ad = 2130837540;
        public static final int twitter = 2130837541;
        public static final int ume_ad = 2130837542;
    }

    public static final class id {
        public static final int cancel = 2131099660;
        public static final int close_dlg = 2131099654;
        public static final int how_to_play_text1 = 2131099649;
        public static final int how_to_play_text2 = 2131099650;
        public static final int how_to_play_text3 = 2131099651;
        public static final int how_to_play_text4 = 2131099652;
        public static final int how_to_play_text_n = 2131099653;
        public static final int layout_root = 2131099648;
        public static final int layout_root3 = 2131099658;
        public static final int next_level = 2131099657;
        public static final int ok = 2131099659;
        public static final int prev_level = 2131099655;
        public static final int selected_level = 2131099656;
    }

    public static final class layout {
        public static final int how_to_play = 2130903040;
        public static final int level_selection = 2130903041;
        public static final int main = 2130903042;
    }

    public static final class raw {
        public static final int break_egg = 2130968576;
    }

    public static final class string {
        public static final int app_name = 2131034112;
    }
}
