package softkos.moveme;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

public class GameCanvas extends View {
    static GameCanvas instance = null;
    gButton downBtn = null;
    gButton leftBtn = null;
    AlertDialog lvlSelectDlg = null;
    int mouseDownX = 0;
    int mouseDownY = 0;
    int mouseX = -1;
    int mouseY = -1;
    Paint paint;
    PaintManager paintMgr;
    gButton playNextBtn = null;
    gButton rightBtn = null;
    gButton upBtn = null;
    Vars vars;
    int vpx;
    int vpy;

    public GameCanvas(Context context) {
        super(context);
        instance = this;
        this.paint = new Paint();
        this.paintMgr = PaintManager.getInstance();
        this.vars = Vars.getInstance();
        initUI();
    }

    public void initUI() {
        int btn_s = 70;
        int btn_w = 260;
        int btn_h = 50;
        if (this.vars.getScreenWidth() > 400) {
            btn_w = 380;
            btn_h = 70;
            btn_s = 90;
        }
        this.playNextBtn = new gButton();
        this.playNextBtn.setSize(btn_w, btn_h);
        this.playNextBtn.setId(Vars.getInstance().PLAY_NEXT);
        this.playNextBtn.hide();
        this.playNextBtn.setText("Play next");
        this.playNextBtn.setBackImages("240x50off", "240x50on");
        Vars.getInstance().addButton(this.playNextBtn);
        this.upBtn = new gButton();
        this.upBtn.setSize(btn_s, btn_s);
        this.upBtn.setId(Vars.getInstance().UP);
        this.upBtn.hide();
        this.upBtn.setImage("arrow_up");
        Vars.getInstance().addButton(this.upBtn);
        this.downBtn = new gButton();
        this.downBtn.setSize(btn_s, btn_s);
        this.downBtn.setId(Vars.getInstance().DOWN);
        this.downBtn.hide();
        this.downBtn.setImage("arrow_down");
        Vars.getInstance().addButton(this.downBtn);
        this.leftBtn = new gButton();
        this.leftBtn.setSize(btn_s, btn_s);
        this.leftBtn.setId(Vars.getInstance().LEFT);
        this.leftBtn.hide();
        this.leftBtn.setImage("arrow_left");
        Vars.getInstance().addButton(this.leftBtn);
        this.rightBtn = new gButton();
        this.rightBtn.setSize(btn_s, btn_s);
        this.rightBtn.setId(Vars.getInstance().RIGHT);
        this.rightBtn.hide();
        this.rightBtn.setImage("arrow_right");
        Vars.getInstance().addButton(this.rightBtn);
    }

    public void showUI(boolean show) {
        this.vars.hideAllButtons();
    }

    public void layoutUI() {
        this.playNextBtn.setPosition((getWidth() / 2) - (this.playNextBtn.getWidth() / 2), (((getHeight() / 2) + (getWidth() / 2)) - this.playNextBtn.getHeight()) - 10);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        layoutUI();
        invalidate();
    }

    public void drawGame(Canvas canvas) {
        int curr_c;
        int curr_c2;
        canvas.drawColor(Color.rgb(62, 29, 3));
        int c1 = Color.rgb(82, 49, 23);
        int c2 = Color.rgb(123, 81, 51);
        int csx = this.vars.getCellSizeX();
        int csy = this.vars.getCellSizeY();
        for (int y = 0; y < this.vars.getBoardHeight(); y++) {
            if (y % 2 == 0) {
                curr_c = c1;
            } else {
                curr_c = c2;
            }
            for (int x = 0; x < this.vars.getBoardWidth(); x++) {
                this.paintMgr.setColor(curr_c2);
                this.paintMgr.fillRectangle(canvas, this.vars.getBoardPosX() + (x * csx), this.vars.getBoardPosY() + (y * csy), csx, csy);
                if (curr_c2 == c2) {
                    curr_c2 = c1;
                } else {
                    curr_c2 = c2;
                }
            }
        }
        int bx = this.vars.getBoardPosX();
        int by = this.vars.getBoardPosY();
        for (int y2 = 0; y2 < this.vars.getBoardHeight(); y2++) {
            for (int x2 = 0; x2 < this.vars.getBoardWidth(); x2++) {
                if (this.vars.getBoardAt(x2, y2) == 9) {
                    this.paintMgr.drawImage(canvas, "full_cell", (x2 * csx) + bx, (y2 * csy) + by, csx, csy);
                } else if (this.vars.getScoresBoardAt(x2, y2) > 0) {
                    this.paintMgr.drawImage(canvas, "small_circle", (x2 * csx) + bx + ((csx * 2) / 5), (y2 * csy) + by + ((csy * 2) / 5), csx / 5, csy / 5);
                }
            }
        }
        for (int i = 0; i < this.vars.getEggList().size(); i++) {
            if (!(this.vars.getEgg(i) == null || this.vars.getBoardAt(this.vars.getEgg(i).getBoardPosX(), this.vars.getEgg(i).getBoardPosY()) == 9)) {
                String img = String.valueOf("cell_") + this.vars.getEgg(i).getType();
                if (this.vars.getEgg(i).getState() > 0) {
                    img = "cell_0";
                }
                this.paintMgr.drawImage(canvas, img, this.vars.getEgg(i).getPx() - 0, this.vars.getEgg(i).getPy() - 0, csx, csy);
            }
        }
        for (int i2 = 0; i2 < this.vars.getStarList().size(); i2++) {
            this.paintMgr.drawImage(canvas, "star_0", this.vars.getStar(i2).getPx(), this.vars.getStar(i2).getPy(), this.vars.getStar(i2).getWidth(), this.vars.getStar(i2).getHeight());
        }
        this.playNextBtn.hide();
        if (Levels.getInstance().isSolved(this.vars.getLevel()) != 0) {
            this.paintMgr.drawImage(canvas, "solved", 0, (getHeight() / 2) - (getWidth() / 2), getWidth(), getWidth());
            this.playNextBtn.show();
        }
        this.paintMgr.setTextSize(22);
        this.paintMgr.drawImage(canvas, "stage", 0, 0, getWidth(), getWidth() / 6);
        this.paintMgr.setColor(-16777216);
        this.paintMgr.drawString(canvas, "Level: " + (this.vars.getLevel() + 1) + "   ", 0, 4, getWidth(), getWidth() / 6, PaintManager.STR_CENTER);
        this.paintMgr.setColor(-256);
        this.paintMgr.setTextSize(14);
        this.paintMgr.drawString(canvas, "Press menu for options", 3, this.vars.getBoardPosY() + (this.vars.getCellSizeY() * this.vars.getBoardHeight()) + 10, getWidth(), 20, PaintManager.STR_CENTER);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.paint == null) {
            this.paint = new Paint();
        }
        if (canvas != null) {
            drawGame(canvas);
            for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
                Vars.getInstance().getButton(i).draw(canvas, this.paint);
            }
        }
    }

    public void showArrows() {
        this.vars.hideAllButtons();
        boolean up = true;
        boolean down = true;
        boolean left = true;
        boolean right = true;
        if (this.vars.getSelectedEgg() >= 0) {
            int se = this.vars.getSelectedEgg();
            int x = this.vars.getEgg(se).getBoardPosX();
            int y = this.vars.getEgg(se).getBoardPosY();
            int w = this.vars.getEgg(se).getWidth();
            int h = this.vars.getEgg(se).getHeight();
            if (x == 0) {
                left = false;
            }
            if (x == this.vars.getBoardWidth() - 1) {
                right = false;
            }
            if (y == 0) {
                up = false;
            }
            if (y == this.vars.getBoardHeight() - 1) {
                down = false;
            }
            if (this.vars.getBoardAt(x - 1, y) == 9) {
                left = false;
            }
            if (this.vars.getBoardAt(x + 1, y) == 9) {
                right = false;
            }
            if (this.vars.getBoardAt(x, y - 1) == 9) {
                up = false;
            }
            if (this.vars.getBoardAt(x, y + 1) == 9) {
                down = false;
            }
            if (this.vars.isEggOnBoardPos(x - 1, y) >= 0) {
                left = false;
            }
            if (this.vars.isEggOnBoardPos(x + 1, y) >= 0) {
                right = false;
            }
            if (this.vars.isEggOnBoardPos(x, y - 1) >= 0) {
                up = false;
            }
            if (this.vars.isEggOnBoardPos(x, y + 1) >= 0) {
                down = false;
            }
            if (left) {
                this.leftBtn.setPosition(((x * w) + this.vars.getBoardPosX()) - this.leftBtn.getWidth(), (((y * h) + this.vars.getBoardPosY()) + (h / 2)) - (this.leftBtn.getHeight() / 2));
                this.leftBtn.show();
            }
            if (right) {
                this.rightBtn.setPosition((x * w) + w + this.vars.getBoardPosX(), (((y * h) + this.vars.getBoardPosY()) + (h / 2)) - (this.leftBtn.getHeight() / 2));
                this.rightBtn.show();
            }
            if (up) {
                this.upBtn.setPosition((((x * w) + this.vars.getBoardPosX()) + (w / 2)) - (this.upBtn.getWidth() / 2), ((y * h) + this.vars.getBoardPosY()) - this.upBtn.getHeight());
                this.upBtn.show();
            }
            if (down) {
                this.downBtn.setPosition((((x * w) + this.vars.getBoardPosX()) + (w / 2)) - (this.upBtn.getWidth() / 2), (y * h) + h + this.vars.getBoardPosY());
                this.downBtn.show();
            }
        }
    }

    public void mouseDown(int x, int y) {
        this.mouseDownX = x;
        this.mouseDownY = y;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void mouseDrag(int x, int y) {
        this.mouseX = x;
        this.mouseY = y;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void mouseUp(int x, int y) {
        this.mouseX = -1;
        this.mouseY = -1;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
        this.vars.selectEggAt(x, y);
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    public void handleCommand(int id) {
        if (id == this.vars.UP) {
            this.vars.moveUp();
        }
        if (id == this.vars.DOWN) {
            this.vars.moveDown();
        }
        if (id == this.vars.LEFT) {
            this.vars.moveLeft();
        }
        if (id == this.vars.RIGHT) {
            this.vars.moveRight();
        }
        if (id == this.vars.PLAY_NEXT) {
            this.vars.nextLevel();
        }
        this.vars.hideAllButtons();
    }

    public static GameCanvas getInstance() {
        return instance;
    }

    public AlertDialog getLevelSelectionDialog() {
        return this.lvlSelectDlg;
    }

    public void dismissDialogs() {
        this.lvlSelectDlg = null;
    }

    /* access modifiers changed from: package-private */
    public void selectLevelDialog() {
        String[] items = new String[Levels.getInstance().getLevelCount()];
        for (int i = 0; i < Levels.getInstance().getLevelCount(); i++) {
            items[i] = "Level " + (i + 1);
            if (Levels.getInstance().isSolved(i) != 0) {
                items[i] = String.valueOf(items[i]) + ": solved";
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.getInstance());
        builder.setTitle("Select level");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Vars.getInstance().setLevel(item);
                Vars.getInstance().nextLevel();
                Vars.getInstance().prevLevel();
                GameCanvas.this.lvlSelectDlg.dismiss();
                GameCanvas.this.lvlSelectDlg = null;
            }
        });
        this.lvlSelectDlg = builder.create();
        this.lvlSelectDlg.show();
    }
}
