package softkos.moveme;

import java.util.Random;

public class OtherApp {
    static OtherApp instance = null;
    public String[] button_text = {"   Untangle me", "   Try Frogs Jump", "    Get Cubix game", "    Turn me on ;)", "    Play BoxIt", "   Play Tripeaks!", "     UntangleMe Extr."};
    public int current;
    public int[] games = new int[7];
    public String[] images = {"untangle_ad", "frogs_jump_ad", "cubix_ad", "turnmeon_ad", "boxit_ad", "tripeaks_ad", "ume_ad"};
    Random r = new Random();
    public String[] urls = {"market://details?id=softkos.untangleme", "market://details?id=sofkos.frogsjump", "market://details?id=softkos.cubix", "market://details?id=softkos.turnmeon", "market://details?id=softkos.boxit", "market://details?id=softkos.tripeaks", "market://details?id=softkos.untanglemeextreme"};

    public OtherApp() {
        random();
    }

    public void random() {
        this.current = this.r.nextInt(this.button_text.length);
        for (int i = 0; i < this.games.length; i++) {
            this.games[i] = this.r.nextInt(this.images.length);
        }
    }

    public int getAppAt(int i) {
        return this.games[i];
    }

    public String getUrlAt(int i) {
        return this.urls[i];
    }

    public String getButtonTextAt(int i) {
        return this.button_text[i];
    }

    public String getImageAt(int i) {
        return this.images[i];
    }

    public static OtherApp getInstance() {
        if (instance == null) {
            instance = new OtherApp();
        }
        return instance;
    }
}
