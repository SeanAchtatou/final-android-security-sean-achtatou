package softkos.moveme;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;

public class Vars {
    static Vars instance = null;
    public int DOWN;
    public int GO_TO_MOBILSOFT;
    public int HOWTOPLAY;
    public int LEFT;
    public int OTHER_APP;
    public int OTHER_APP1;
    public int OTHER_APP2;
    public int OTHER_APP3;
    public int OTHER_APP4;
    public int OTHER_APP5;
    public int OTHER_APP6;
    public int PLAY_NEXT;
    public int RESET_ALL_LEVELS;
    public int RIGHT;
    public int SOUND_SETTINGS;
    public int START_GAME;
    public int TWITTER;
    public int UP;
    int[][] board;
    int board_h;
    int board_pos_x;
    int board_pos_y;
    int board_w;
    ArrayList<gButton> buttonList;
    int cell_size_x;
    int cell_size_y;
    ArrayList<Egg> eggList;
    public GameState gameState;
    int level;
    public int painting;
    Random random;
    int[][] scores_board;
    int screenHeight;
    int screenWidth;
    int selectedEgg;
    ArrayList<Star> starList;
    Timer updateTimer;

    public enum GameState {
        Menu,
        Game,
        Highscores,
        HowPlay,
        Settings
    }

    public Vars() {
        this.painting = 0;
        this.level = 0;
        this.board_w = 6;
        this.board_h = 6;
        this.buttonList = null;
        this.eggList = null;
        this.starList = null;
        this.cell_size_x = 50;
        this.cell_size_y = 40;
        this.selectedEgg = -1;
        this.SOUND_SETTINGS = 1001;
        this.RESET_ALL_LEVELS = 1002;
        this.OTHER_APP1 = 1003;
        this.OTHER_APP2 = 1004;
        this.OTHER_APP3 = 1005;
        this.OTHER_APP4 = 1006;
        this.OTHER_APP5 = 1007;
        this.OTHER_APP6 = 1008;
        this.START_GAME = 100;
        this.GO_TO_MOBILSOFT = 101;
        this.HOWTOPLAY = 102;
        this.TWITTER = 103;
        this.OTHER_APP = 104;
        this.UP = 200;
        this.DOWN = 201;
        this.LEFT = 202;
        this.RIGHT = 204;
        this.PLAY_NEXT = 205;
        this.buttonList = new ArrayList<>();
        this.buttonList.clear();
        this.eggList = new ArrayList<>();
        this.eggList.clear();
        this.starList = new ArrayList<>();
        this.starList.clear();
        this.random = new Random();
        this.updateTimer = new Timer();
        this.updateTimer.schedule(new UpdateTimer(), 10, 50);
        this.board = (int[][]) Array.newInstance(Integer.TYPE, this.board_w, this.board_h);
        this.level = 0;
    }

    public int getBoardWidth() {
        return this.board_w;
    }

    public int getBoardHeight() {
        return this.board_h;
    }

    public void advanceFrame() {
        if (this.gameState == GameState.Menu || this.gameState == GameState.Game) {
            if (this.gameState == GameState.Game) {
                boolean moved = false;
                for (int i = 0; i < getEggList().size(); i++) {
                    boolean m = getEgg(i).update();
                    moved |= m;
                    if (m) {
                        checkPoints(i);
                    }
                }
                for (int i2 = 0; i2 < this.starList.size(); i2++) {
                    this.starList.get(i2).update();
                }
                if (!moved) {
                    checkEggs();
                }
                checkEndLevel();
            }
            if (this.gameState == GameState.Menu) {
                for (int i3 = 0; i3 < this.buttonList.size(); i3++) {
                    getButton(i3).updatePosition();
                }
            }
        }
    }

    public void checkPoints(int i) {
        int bx = ((getEgg(i).getPx() - getBoardPosX()) + (getCellSizeX() / 2)) / getCellSizeX();
        int by = ((getEgg(i).getPy() - getBoardPosY()) + (getCellSizeY() / 2)) / getCellSizeY();
        if (getScoresBoardAt(bx, by) > 0) {
            setScoresBoardAt(bx, by, 0);
        }
    }

    public void checkEggs() {
        for (int i = 0; i < this.eggList.size(); i++) {
            if (getEgg(i).getState() != 2) {
                int[] e = {isEggOnBoardPos(getEgg(i).getBoardPosX() - 1, getEgg(i).getBoardPosY()), isEggOnBoardPos(getEgg(i).getBoardPosX() + 1, getEgg(i).getBoardPosY()), isEggOnBoardPos(getEgg(i).getBoardPosX(), getEgg(i).getBoardPosY() - 1), isEggOnBoardPos(getEgg(i).getBoardPosX(), getEgg(i).getBoardPosY() + 1)};
                for (int j = 0; j < 4; j++) {
                    if (e[j] >= 0 && i != e[j] && getEgg(e[j]).getState() != 2 && getEgg(i).getType() == getEgg(e[j]).getType()) {
                        getEgg(i).setState(1);
                        getEgg(e[j]).setState(1);
                        for (int z = 0; z < 15; z++) {
                            Star s = new Star();
                            s.setSize(this.screenWidth / 12, this.screenWidth / 12);
                            s.setPos((getEgg(i).getPx() + getEgg(e[j]).getPx()) / 2, (getEgg(i).getPy() + getEgg(e[j]).getPy()) / 2);
                            this.starList.add(s);
                        }
                        SoundManager.getInstance().playSound(1);
                    }
                }
            }
        }
        for (int i2 = 0; i2 < this.eggList.size(); i2++) {
            if (getEgg(i2).getState() == 1) {
                getEgg(i2).setState(2);
            }
        }
    }

    public ArrayList<Star> getStarList() {
        return this.starList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Star getStar(int i) {
        if (i < 0 || i >= this.starList.size()) {
            return null;
        }
        return this.starList.get(i);
    }

    public boolean checkEndLevel() {
        int solved = Levels.getInstance().isSolved(getLevel());
        for (int i = 0; i < this.eggList.size(); i++) {
            if (getEgg(i).getState() == 0) {
                return false;
            }
        }
        if (solved == 0) {
            Levels.getInstance().setSolved(getLevel());
            FileWR.getInstance().saveGame();
        }
        return true;
    }

    public void hideAllButtons() {
        for (int i = 0; i < this.buttonList.size(); i++) {
            getButton(i).hide();
        }
    }

    public void calcBoard() {
        this.cell_size_x = this.screenWidth / this.board_w;
        this.cell_size_y = this.cell_size_x;
        this.board_pos_y = ((this.screenHeight - (this.cell_size_y * this.board_h)) / 2) - (this.cell_size_y / 5);
        this.board_pos_x = (this.screenWidth - (this.cell_size_x * this.board_w)) / 2;
    }

    public int getCellSizeX() {
        return this.cell_size_x;
    }

    public int getCellSizeY() {
        return this.cell_size_y;
    }

    public int getBoardPosY() {
        return this.board_pos_y;
    }

    public int getBoardPosX() {
        return this.board_pos_x;
    }

    public void moveUp() {
        if (this.selectedEgg >= 0) {
            int ex = getEgg(this.selectedEgg).getBoardPosX();
            int ey = getEgg(this.selectedEgg).getBoardPosY();
            int dy = ey;
            int y = ey - 1;
            while (y >= 0 && getBoardAt(ex, y) != 9 && isEggOnBoardPos(ex, y) < 0) {
                dy = y;
                y--;
            }
            getEgg(this.selectedEgg).setDestPos(getEgg(this.selectedEgg).getPx(), (getCellSizeY() * dy) + getBoardPosY());
            getEgg(this.selectedEgg).setBoardPos(ex, dy);
        }
    }

    public void moveDown() {
        if (this.selectedEgg >= 0) {
            int ex = getEgg(this.selectedEgg).getBoardPosX();
            int ey = getEgg(this.selectedEgg).getBoardPosY();
            int dy = ey;
            int y = ey + 1;
            while (y < this.board_h && getBoardAt(ex, y) != 9 && isEggOnBoardPos(ex, y) < 0) {
                dy = y;
                y++;
            }
            getEgg(this.selectedEgg).setDestPos(getEgg(this.selectedEgg).getPx(), (getCellSizeY() * dy) + getBoardPosY());
            getEgg(this.selectedEgg).setBoardPos(ex, dy);
        }
    }

    public void moveLeft() {
        if (this.selectedEgg >= 0) {
            int ex = getEgg(this.selectedEgg).getBoardPosX();
            int ey = getEgg(this.selectedEgg).getBoardPosY();
            int dx = ex;
            int x = ex - 1;
            while (x >= 0 && getBoardAt(x, ey) != 9 && isEggOnBoardPos(x, ey) < 0) {
                dx = x;
                x--;
            }
            getEgg(this.selectedEgg).setDestPos((getCellSizeX() * dx) + getBoardPosX(), getEgg(this.selectedEgg).getPy());
            getEgg(this.selectedEgg).setBoardPos(dx, ey);
        }
    }

    public void moveRight() {
        if (this.selectedEgg >= 0) {
            int ex = getEgg(this.selectedEgg).getBoardPosX();
            int ey = getEgg(this.selectedEgg).getBoardPosY();
            int dx = ex;
            int x = ex + 1;
            while (x < this.board_w && getBoardAt(x, ey) != 9 && isEggOnBoardPos(x, ey) < 0) {
                dx = x;
                x++;
            }
            getEgg(this.selectedEgg).setDestPos((getCellSizeX() * dx) + getBoardPosX(), getEgg(this.selectedEgg).getPy());
            getEgg(this.selectedEgg).setBoardPos(dx, ey);
        }
    }

    public void initLevel() {
        this.eggList.clear();
        this.starList.clear();
        this.selectedEgg = -1;
        if (GameCanvas.getInstance() != null) {
            GameCanvas.getInstance().showArrows();
        }
        int[] data = Levels.getInstance().getLevel(this.level);
        this.board_w = data[0];
        this.board_h = data[1];
        this.board = (int[][]) Array.newInstance(Integer.TYPE, this.board_w, this.board_h);
        this.scores_board = (int[][]) Array.newInstance(Integer.TYPE, this.board_w, this.board_h);
        int index = 2;
        calcBoard();
        for (int y = 0; y < this.board_h; y++) {
            int x = 0;
            while (x < this.board_w) {
                int index2 = index + 1;
                this.board[x][y] = data[index];
                if (this.board[x][y] > 0) {
                    Egg nc = new Egg();
                    nc.setSize(getCellSizeX(), getCellSizeY());
                    nc.SetPos((getCellSizeX() * x) + getBoardPosX(), (getCellSizeY() * y) + getBoardPosY());
                    nc.setDestPos(nc.getPx(), nc.getPy());
                    nc.setType(this.board[x][y]);
                    nc.setBoardPos(x, y);
                    if (this.board[x][y] == 9) {
                        nc.setState(2);
                    }
                    this.eggList.add(nc);
                } else {
                    this.scores_board[x][y] = 1;
                }
                x++;
                index = index2;
            }
        }
    }

    public void selectEggAt(int x, int y) {
        this.selectedEgg = -1;
        int i = 0;
        while (true) {
            if (i >= this.eggList.size()) {
                break;
            } else if (getEgg(i).getType() != 9 && x > getEgg(i).getPx() && x < getEgg(i).getPx() + getEgg(i).getWidth() && y > getEgg(i).getPy() && y < getEgg(i).getPy() + getEgg(i).getHeight() && getEgg(i).getState() == 0) {
                this.selectedEgg = i;
                break;
            } else {
                i++;
            }
        }
        GameCanvas.getInstance().showArrows();
    }

    public int getSelectedEgg() {
        return this.selectedEgg;
    }

    public int getEggAt(int x, int y) {
        for (int i = 0; i < getEggList().size(); i++) {
            if (x == getEgg(i).getBoardPosX() && y == getEgg(i).getBoardPosY()) {
                return i;
            }
        }
        return -1;
    }

    public int getBoardAt(int x, int y) {
        if (x < 0 || x >= this.board_w || y < 0 || y >= this.board_h) {
            return -1;
        }
        return this.board[x][y];
    }

    public int getScoresBoardAt(int x, int y) {
        if (x < 0 || x >= this.board_w || y < 0 || y >= this.board_h) {
            return 0;
        }
        return this.scores_board[x][y];
    }

    public void setScoresBoardAt(int x, int y, int v) {
        if (x >= 0 && x < this.board_w && y >= 0 && y < this.board_h) {
            this.scores_board[x][y] = v;
        }
    }

    public double dist(int x1, int y1, int x2, int y2) {
        return Math.sqrt((double) (((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2))));
    }

    public void setLevel(int l) {
        this.level = l;
    }

    public void nextLevel() {
        this.level++;
        if (this.level >= Levels.getInstance().getLevelCount()) {
            this.level = Levels.getInstance().getLevelCount() - 1;
        }
        initLevel();
    }

    public int isEggOnBoardPos(int x, int y) {
        for (int i = 0; i < this.eggList.size(); i++) {
            if (getEgg(i).getBoardPosX() == x && getEgg(i).getBoardPosY() == y) {
                return i;
            }
        }
        return -1;
    }

    public void prevLevel() {
        this.level--;
        if (this.level < 0) {
            this.level = 0;
        }
        initLevel();
    }

    public void resetLevel() {
        initLevel();
    }

    public int getLevel() {
        return this.level;
    }

    public void newGame() {
        this.level = 0;
        getEggList().clear();
        FileWR.getInstance().loadGame();
        int i = 0;
        while (true) {
            if (i >= Levels.getInstance().getLevelCount()) {
                break;
            } else if (Levels.getInstance().isSolved(i) == 0) {
                setLevel(i);
                break;
            } else {
                i++;
            }
        }
        initLevel();
    }

    public int getButtonListSize() {
        return this.buttonList.size();
    }

    public void addButton(gButton b) {
        this.buttonList.add(b);
    }

    public ArrayList<gButton> getButtonList() {
        return this.buttonList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public gButton getButton(int b) {
        if (b < 0 || b >= this.buttonList.size()) {
            return null;
        }
        return this.buttonList.get(b);
    }

    public ArrayList<Egg> getEggList() {
        return this.eggList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Egg getEgg(int b) {
        if (b < 0 || b >= this.eggList.size()) {
            return null;
        }
        return this.eggList.get(b);
    }

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public void init() {
        this.random = new Random();
    }

    public int GetNextInt(int max) {
        if (max == 0) {
            max = 1;
        }
        return Math.abs(this.random.nextInt() % max);
    }

    public Random getRandom() {
        return this.random;
    }

    public void screenSize(int w, int h) {
        this.screenWidth = w;
        this.screenHeight = h;
    }

    public static Vars getInstance() {
        if (instance == null) {
            instance = new Vars();
            instance.init();
        }
        return instance;
    }

    public int nextInt(int max) {
        if (max < 1) {
            return 0;
        }
        return this.random.nextInt(max);
    }
}
