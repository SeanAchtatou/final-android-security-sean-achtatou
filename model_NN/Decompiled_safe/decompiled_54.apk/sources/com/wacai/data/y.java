package com.wacai.data;

import android.util.Log;
import com.wacai.a.g;
import com.wacai.e;
import org.w3c.dom.Element;

public final class y extends k {
    private long a = -1;
    private boolean b = false;
    private int c = 0;

    public y() {
        super("TBL_OUTGOSUBTYPEINFO");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0041  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private long a() {
        /*
            r13 = this;
            r10 = 1
            r8 = 0
            r7 = 0
            r4 = 0
            r5 = 10000(0x2710, double:4.9407E-320)
            java.lang.String r0 = "select max(id) from TBL_OUTGOSUBTYPEINFO where id >= %d AND id < %d"
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]
            long r2 = r13.a
            long r2 = r2 * r5
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            r1[r4] = r2
            r2 = 1
            long r3 = r13.a
            long r3 = r3 + r10
            long r3 = r3 * r5
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            r1[r2] = r3
            java.lang.String r0 = java.lang.String.format(r0, r1)
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ all -> 0x004d }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ all -> 0x004d }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ all -> 0x004d }
            if (r0 == 0) goto L_0x005c
            boolean r1 = r0.moveToFirst()     // Catch:{ all -> 0x0055 }
            if (r1 == 0) goto L_0x005c
            r1 = 0
            long r1 = r0.getLong(r1)     // Catch:{ all -> 0x0055 }
        L_0x003f:
            if (r0 == 0) goto L_0x0044
            r0.close()
        L_0x0044:
            int r0 = (r8 > r1 ? 1 : (r8 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x005a
            long r0 = r13.a
            long r0 = r0 * r5
        L_0x004b:
            long r0 = r0 + r10
            return r0
        L_0x004d:
            r0 = move-exception
            r1 = r7
        L_0x004f:
            if (r1 == 0) goto L_0x0054
            r1.close()
        L_0x0054:
            throw r0
        L_0x0055:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x004f
        L_0x005a:
            r0 = r1
            goto L_0x004b
        L_0x005c:
            r1 = r8
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.y.a():long");
    }

    public static y a(Element element, boolean z) {
        if (element == null) {
            return null;
        }
        return (y) ab.a(element, new y(), z);
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00d8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.y b(long r10) {
        /*
            r7 = 0
            r6 = 0
            r5 = 1
            r4 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select * from TBL_OUTGOSUBTYPEINFO where id = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r10)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x00ca, all -> 0x00d4 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x00ca, all -> 0x00d4 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x00ca, all -> 0x00d4 }
            if (r0 == 0) goto L_0x002d
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            if (r1 != 0) goto L_0x0034
        L_0x002d:
            if (r0 == 0) goto L_0x0032
            r0.close()
        L_0x0032:
            r0 = r6
        L_0x0033:
            return r0
        L_0x0034:
            com.wacai.data.y r1 = new com.wacai.data.y     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            r1.<init>()     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            r1.k(r10)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            java.lang.String r2 = "name"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            r1.b(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            java.lang.String r2 = "enable"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00c2
            r2 = r5
        L_0x0058:
            r1.d(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            java.lang.String r2 = "isdefault"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00c4
            r2 = r5
        L_0x006a:
            r1.e(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            java.lang.String r2 = "uuid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            r1.g(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            java.lang.String r2 = "orderno"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            r1.f(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            java.lang.String r2 = "updatestatus"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00c6
            r2 = r5
        L_0x0096:
            r1.f(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            java.lang.String r2 = "star"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            if (r2 <= 0) goto L_0x00c8
            r2 = r5
        L_0x00a6:
            r1.b = r2     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            java.lang.String r2 = "refcount"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            r1.c = r2     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            r2 = 10000(0x2710, double:4.9407E-320)
            long r2 = r10 / r2
            r1.a = r2     // Catch:{ Exception -> 0x00e1, all -> 0x00dc }
            if (r0 == 0) goto L_0x00bf
            r0.close()
        L_0x00bf:
            r0 = r1
            goto L_0x0033
        L_0x00c2:
            r2 = r4
            goto L_0x0058
        L_0x00c4:
            r2 = r4
            goto L_0x006a
        L_0x00c6:
            r2 = r4
            goto L_0x0096
        L_0x00c8:
            r2 = r4
            goto L_0x00a6
        L_0x00ca:
            r0 = move-exception
            r0 = r6
        L_0x00cc:
            if (r0 == 0) goto L_0x00d1
            r0.close()
        L_0x00d1:
            r0 = r6
            goto L_0x0033
        L_0x00d4:
            r0 = move-exception
            r1 = r6
        L_0x00d6:
            if (r1 == 0) goto L_0x00db
            r1.close()
        L_0x00db:
            throw r0
        L_0x00dc:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00d6
        L_0x00e1:
            r1 = move-exception
            goto L_0x00cc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.y.b(long):com.wacai.data.y");
    }

    public static String c(long j) {
        return aa.a("TBL_OUTGOSUBTYPEINFO", "name", (int) j);
    }

    public final void a(long j) {
        this.a = j;
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str != null && str2 != null) {
            if (str.equalsIgnoreCase("r")) {
                g(str2);
            } else if (str.equalsIgnoreCase("s")) {
                b(str2);
            } else if (str.equalsIgnoreCase("w")) {
                f(Long.parseLong(str2));
            } else if (str.equalsIgnoreCase("u")) {
                d(Integer.parseInt(str2) > 0);
            } else if (str.equalsIgnoreCase("v")) {
                e(Integer.parseInt(str2) > 0);
            } else if (str.equalsIgnoreCase("aa")) {
                this.a = aa.a("TBL_OUTGOMAINTYPEINFO", "id", str2, 1);
            }
        }
    }

    public final void a(StringBuffer stringBuffer) {
        stringBuffer.append("<e><r>");
        stringBuffer.append(B());
        stringBuffer.append("</r><aa>");
        stringBuffer.append(p.a(this.a).B());
        stringBuffer.append("</aa><s>");
        stringBuffer.append(h(j()));
        stringBuffer.append("</s><w>");
        stringBuffer.append(k());
        stringBuffer.append("</w><u>");
        stringBuffer.append(l() ? 1 : 0);
        stringBuffer.append("</u></e>");
    }

    public final void c() {
        String format;
        if (j() == null || j().length() <= 0) {
            Log.e("WAC_OutgoSubType", "Invalidate name when save data.");
            return;
        }
        if (C()) {
            Object[] objArr = new Object[9];
            objArr[0] = w();
            objArr[1] = e(j());
            objArr[2] = e(g.a(j()));
            objArr[3] = Integer.valueOf(l() ? 1 : 0);
            objArr[4] = Integer.valueOf(m() ? 1 : 0);
            objArr[5] = B();
            objArr[6] = Long.valueOf(k());
            objArr[7] = Integer.valueOf(A() ? 1 : 0);
            objArr[8] = Long.valueOf(x());
            format = String.format("UPDATE %s SET name = '%s', pinyin = '%s', enable = %d, isdefault = %d, uuid = '%s', orderno = %d, updatestatus = %d WHERE id = %d", objArr);
        } else {
            k(a());
            Object[] objArr2 = new Object[9];
            objArr2[0] = w();
            objArr2[1] = Long.valueOf(x());
            objArr2[2] = B();
            objArr2[3] = e(j());
            objArr2[4] = e(g.a(j()));
            objArr2[5] = Integer.valueOf(l() ? 1 : 0);
            objArr2[6] = Long.valueOf(k());
            objArr2[7] = Integer.valueOf(m() ? 1 : 0);
            objArr2[8] = Integer.valueOf(A() ? 1 : 0);
            format = String.format("INSERT INTO %s (id, uuid, name, pinyin, enable, orderno, isdefault, updatestatus) VALUES (%d, '%s', '%s', '%s', %d, %d, %d, %d)", objArr2);
        }
        e.c().b().execSQL(format);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x004e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean c(java.lang.String r10) {
        /*
            r9 = this;
            r7 = 0
            r6 = 1
            r5 = 0
            if (r10 == 0) goto L_0x000b
            int r0 = r10.length()
            if (r0 > 0) goto L_0x000d
        L_0x000b:
            r0 = r5
        L_0x000c:
            return r0
        L_0x000d:
            java.lang.String r0 = "select * from %s where LOWER(name) = LOWER('%s') and id <> %d and id / 10000 = %d and enable = 1"
            r1 = 4
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r2 = r9.w()
            r1[r5] = r2
            java.lang.String r2 = com.wacai.data.aa.e(r10)
            r1[r6] = r2
            r2 = 2
            long r3 = r9.x()
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            r1[r2] = r3
            r2 = 3
            long r3 = r9.a
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            r1[r2] = r3
            java.lang.String r0 = java.lang.String.format(r0, r1)
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ all -> 0x0055 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ all -> 0x0055 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ all -> 0x0055 }
            if (r0 == 0) goto L_0x0053
            boolean r1 = r0.moveToFirst()     // Catch:{ all -> 0x005d }
            if (r1 == 0) goto L_0x0053
            r1 = r6
        L_0x004c:
            if (r0 == 0) goto L_0x0051
            r0.close()
        L_0x0051:
            r0 = r1
            goto L_0x000c
        L_0x0053:
            r1 = r5
            goto L_0x004c
        L_0x0055:
            r0 = move-exception
            r1 = r7
        L_0x0057:
            if (r1 == 0) goto L_0x005c
            r1.close()
        L_0x005c:
            throw r0
        L_0x005d:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.y.c(java.lang.String):boolean");
    }
}
