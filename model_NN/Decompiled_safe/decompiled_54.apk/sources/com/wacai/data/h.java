package com.wacai.data;

import android.database.Cursor;
import android.util.Log;
import com.wacai.a.g;
import com.wacai.b;
import com.wacai.e;
import org.w3c.dom.Element;

public final class h extends k {
    private long a = 0;
    private int b = 0;
    private int c = 1;
    private long d = 0;
    private boolean e = false;
    private long f = 0;
    private boolean g = false;

    public h() {
        super("TBL_ACCOUNTINFO");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x006c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long a(java.lang.String r7) {
        /*
            r5 = 0
            r4 = 0
            java.lang.String r0 = "TBL_OUTGOINFO"
            boolean r0 = r7.equals(r0)     // Catch:{ Exception -> 0x005d, all -> 0x0068 }
            if (r0 == 0) goto L_0x0045
            java.lang.String r0 = " and scheduleoutgoid=0 order by outgodate DESC "
        L_0x000c:
            java.lang.String r1 = "select _accid, count(_accid) as _freq from (select a.accountid as _accid from %s a join TBL_ACCOUNTINFO b on b.id=a.accountid and b.enable=1 where a.isdelete=0 %s limit 0, 10) group by _accid order by _freq DESC"
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x005d, all -> 0x0068 }
            r3 = 0
            r2[r3] = r7     // Catch:{ Exception -> 0x005d, all -> 0x0068 }
            r3 = 1
            r2[r3] = r0     // Catch:{ Exception -> 0x005d, all -> 0x0068 }
            java.lang.String r0 = java.lang.String.format(r1, r2)     // Catch:{ Exception -> 0x005d, all -> 0x0068 }
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x005d, all -> 0x0068 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x005d, all -> 0x0068 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x005d, all -> 0x0068 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x005d, all -> 0x0068 }
            if (r0 == 0) goto L_0x0053
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x0077, all -> 0x0070 }
            if (r1 == 0) goto L_0x0053
            java.lang.String r1 = "_accid"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x0077, all -> 0x0070 }
            long r1 = r0.getLong(r1)     // Catch:{ Exception -> 0x0077, all -> 0x0070 }
            if (r0 == 0) goto L_0x0043
            r0.close()
        L_0x0043:
            r0 = r1
        L_0x0044:
            return r0
        L_0x0045:
            java.lang.String r0 = "TBL_INCOMEINFO"
            boolean r0 = r7.equals(r0)     // Catch:{ Exception -> 0x005d, all -> 0x0068 }
            if (r0 == 0) goto L_0x0050
            java.lang.String r0 = " and scheduleincomeid=0 order by incomedate DESC "
            goto L_0x000c
        L_0x0050:
            java.lang.String r0 = " order by startdate DESC "
            goto L_0x000c
        L_0x0053:
            if (r0 == 0) goto L_0x0058
            r0.close()
        L_0x0058:
            long r0 = c(r4)
            goto L_0x0044
        L_0x005d:
            r0 = move-exception
            r1 = r5
        L_0x005f:
            r0.printStackTrace()     // Catch:{ all -> 0x0075 }
            if (r1 == 0) goto L_0x0058
            r1.close()
            goto L_0x0058
        L_0x0068:
            r0 = move-exception
            r1 = r5
        L_0x006a:
            if (r1 == 0) goto L_0x006f
            r1.close()
        L_0x006f:
            throw r0
        L_0x0070:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x006a
        L_0x0075:
            r0 = move-exception
            goto L_0x006a
        L_0x0077:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.h.a(java.lang.String):long");
    }

    public static h a(Element element, boolean z) {
        if (element == null) {
            return null;
        }
        return (h) ab.a(element, new h(), z);
    }

    public static long c(boolean z) {
        Cursor cursor;
        StringBuilder sb = new StringBuilder("select id from ");
        sb.append("TBL_ACCOUNTINFO");
        sb.append(" where enable = 1 ");
        if (z) {
            sb.append("AND type = 3");
        } else {
            sb.append("AND type <> 3");
        }
        sb.append(" order by orderno ASC");
        try {
            Cursor rawQuery = e.c().b().rawQuery(sb.toString(), null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        long j = rawQuery.getLong(0);
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return j;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return -1;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    public static String c(int i) {
        Cursor cursor;
        try {
            Cursor rawQuery = e.c().b().rawQuery("select b.flag from TBL_ACCOUNTINFO a join TBL_MONEYTYPE b on b.id=a.moneytype where a.id = " + i, null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        String string = rawQuery.getString(0);
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return string;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return "";
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00f7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.h d(long r10) {
        /*
            r7 = 0
            r6 = 0
            r5 = 1
            r4 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select * from TBL_ACCOUNTINFO where id = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r10)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x00e9, all -> 0x00f3 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x00e9, all -> 0x00f3 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x00e9, all -> 0x00f3 }
            if (r0 == 0) goto L_0x002d
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            if (r1 != 0) goto L_0x0034
        L_0x002d:
            if (r0 == 0) goto L_0x0032
            r0.close()
        L_0x0032:
            r0 = r6
        L_0x0033:
            return r0
        L_0x0034:
            com.wacai.data.h r1 = new com.wacai.data.h     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            r1.<init>()     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            r1.k(r10)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            java.lang.String r2 = "isdefault"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00e0
            r2 = r5
        L_0x004b:
            r1.e(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            java.lang.String r2 = "name"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            r1.b(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            java.lang.String r2 = "uuid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            r1.g(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            java.lang.String r2 = "enable"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00e3
            r2 = r5
        L_0x0077:
            r1.d(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            java.lang.String r2 = "type"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            r1.b = r2     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            java.lang.String r2 = "moneytype"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            r1.c = r2     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            java.lang.String r2 = "hasbalance"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            int r2 = (r7 > r2 ? 1 : (r7 == r2 ? 0 : -1))
            if (r2 == 0) goto L_0x00e5
            r2 = r5
        L_0x00a1:
            r1.e = r2     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            java.lang.String r2 = "balance"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            r1.a = r2     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            java.lang.String r2 = "balancedate"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            r1.d = r2     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            java.lang.String r2 = "haswarning"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            int r2 = (r7 > r2 ? 1 : (r7 == r2 ? 0 : -1))
            if (r2 == 0) goto L_0x00e7
            r2 = r5
        L_0x00ca:
            r1.g = r2     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            java.lang.String r2 = "warningbalance"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            r1.f = r2     // Catch:{ Exception -> 0x0100, all -> 0x00fb }
            if (r0 == 0) goto L_0x00dd
            r0.close()
        L_0x00dd:
            r0 = r1
            goto L_0x0033
        L_0x00e0:
            r2 = r4
            goto L_0x004b
        L_0x00e3:
            r2 = r4
            goto L_0x0077
        L_0x00e5:
            r2 = r4
            goto L_0x00a1
        L_0x00e7:
            r2 = r4
            goto L_0x00ca
        L_0x00e9:
            r0 = move-exception
            r0 = r6
        L_0x00eb:
            if (r0 == 0) goto L_0x00f0
            r0.close()
        L_0x00f0:
            r0 = r6
            goto L_0x0033
        L_0x00f3:
            r0 = move-exception
            r1 = r6
        L_0x00f5:
            if (r1 == 0) goto L_0x00fa
            r1.close()
        L_0x00fa:
            throw r0
        L_0x00fb:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00f5
        L_0x0100:
            r1 = move-exception
            goto L_0x00eb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.h.d(long):com.wacai.data.h");
    }

    public static String e(long j) {
        return aa.a("TBL_ACCOUNTINFO", "name", (int) j);
    }

    public final long a() {
        return this.a;
    }

    public final void a(int i) {
        this.b = i;
    }

    public final void a(long j) {
        this.a = j;
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str != null && str2 != null) {
            if (str.equalsIgnoreCase("r")) {
                g(str2);
            } else if (str.equalsIgnoreCase("s")) {
                b(str2);
            } else if (str.equalsIgnoreCase("t")) {
                this.b = Integer.parseInt(str2);
            } else if (str.equalsIgnoreCase("w")) {
                f(Long.parseLong(str2));
            } else if (str.equalsIgnoreCase("u")) {
                d(Integer.parseInt(str2) > 0);
            } else if (str.equalsIgnoreCase("v")) {
                e(Integer.parseInt(str2) > 0);
            } else if (str.equalsIgnoreCase("x")) {
                this.a = a(Double.parseDouble(str2));
            } else if (str.equalsIgnoreCase("y")) {
                this.e = true;
                this.d = Long.parseLong(str2);
            } else if (str.equalsIgnoreCase("z")) {
                this.c = (int) aa.a("TBL_MONEYTYPE", "id", str2, 1);
            }
        }
    }

    public final void a(StringBuffer stringBuffer) {
        if (stringBuffer != null) {
            stringBuffer.append("<a><r>");
            stringBuffer.append(B());
            stringBuffer.append("</r><s>");
            stringBuffer.append(h(j()));
            stringBuffer.append("</s><w>");
            stringBuffer.append(k());
            stringBuffer.append("</w><t>");
            stringBuffer.append(this.b);
            stringBuffer.append("</t><z>");
            stringBuffer.append(a("TBL_MONEYTYPE", "uuid", this.c));
            stringBuffer.append("</z><u>");
            stringBuffer.append(l() ? 1 : 0);
            stringBuffer.append("</u>");
            if (this.e) {
                stringBuffer.append("<x>");
                stringBuffer.append(aa.a(aa.l(this.a), 2));
                stringBuffer.append("</x><y>");
                stringBuffer.append(this.d);
                stringBuffer.append("</y>");
            } else {
                stringBuffer.append("<y>");
                stringBuffer.append("</y>");
            }
            stringBuffer.append("</a>");
        }
    }

    public final void a(boolean z) {
        this.e = z;
    }

    public final long b() {
        return this.d;
    }

    public final void b(int i) {
        this.c = i;
    }

    public final void b(long j) {
        this.d = j;
    }

    public final void b(boolean z) {
        this.g = z;
    }

    public final void c() {
        if (j() == null || j().length() <= 0) {
            Log.e("WAC_Account", "Invalide name when save account");
        } else if (!b("TBL_MONEYTYPE", (long) this.c)) {
            Log.e("WAC_Account", "Invalide money type when save account");
        } else {
            if (C()) {
                Object[] objArr = new Object[12];
                objArr[0] = w();
                objArr[1] = e(j());
                objArr[2] = Integer.valueOf(l() ? 1 : 0);
                objArr[3] = Integer.valueOf(this.b);
                objArr[4] = Long.valueOf(this.a);
                objArr[5] = Long.valueOf(this.d);
                objArr[6] = Integer.valueOf(this.e ? 1 : 0);
                objArr[7] = Long.valueOf(this.f);
                objArr[8] = Integer.valueOf(this.g ? 1 : 0);
                objArr[9] = Integer.valueOf(this.c);
                objArr[10] = e(g.a(j()));
                objArr[11] = Long.valueOf(x());
                e.c().b().execSQL(String.format("UPDATE %s SET name = '%s', enable = %d, type = %d, updatestatus = 0, balance = %d, balancedate = %d, hasbalance = %d, warningbalance = %d, haswarning = %d, moneytype = %d, pinyin = '%s' WHERE id = %d", objArr));
            } else {
                Object[] objArr2 = new Object[15];
                objArr2[0] = w();
                objArr2[1] = B();
                objArr2[2] = e(j());
                objArr2[3] = Integer.valueOf(l() ? 1 : 0);
                objArr2[4] = Integer.valueOf(this.b);
                objArr2[5] = Long.valueOf(k());
                objArr2[6] = Integer.valueOf(m() ? 1 : 0);
                objArr2[7] = Integer.valueOf(A() ? 1 : 0);
                objArr2[8] = Long.valueOf(this.a);
                objArr2[9] = Long.valueOf(this.d);
                objArr2[10] = Integer.valueOf(this.e ? 1 : 0);
                objArr2[11] = Long.valueOf(this.f);
                objArr2[12] = Integer.valueOf(this.g ? 1 : 0);
                objArr2[13] = Integer.valueOf(this.c);
                objArr2[14] = e(g.a(j()));
                e.c().b().execSQL(String.format("INSERT INTO %s (uuid, name, enable, type, orderno, isdefault, updatestatus, balance, balancedate, hasbalance, warningbalance, haswarning, moneytype, pinyin) VALUES ('%s', '%s', %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, '%s')", objArr2));
                k((long) d(w()));
            }
            b.o();
        }
    }

    public final void c(long j) {
        this.f = j;
    }

    public final boolean d() {
        return this.e;
    }

    public final int e() {
        return this.b;
    }

    public final int g() {
        return this.c;
    }

    public final long h() {
        return this.f;
    }

    public final boolean i() {
        return this.g;
    }
}
