package com.wacai.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.wacai.e;
import java.util.ArrayList;

public abstract class s extends ab {
    private long a = 0;
    private long b;
    private long c;
    private long d = 0;
    private boolean e;
    private ArrayList f = new ArrayList();

    protected s(String str, boolean z) {
        super(str);
        if (z) {
            this.b = h.a(str);
            this.c = aa.f("TBL_PROJECTINFO");
            return;
        }
        this.b = 1;
        this.c = 1;
    }

    /* access modifiers changed from: protected */
    public void a(Cursor cursor) {
        if (cursor != null) {
            g(cursor.getLong(cursor.getColumnIndexOrThrow("money")));
            this.b = cursor.getLong(cursor.getColumnIndexOrThrow("accountid"));
            this.c = cursor.getLong(cursor.getColumnIndexOrThrow("projectid"));
            this.d = cursor.getLong(cursor.getColumnIndexOrThrow("targetid"));
            this.e = cursor.getLong(cursor.getColumnIndexOrThrow("isdelete")) > 0;
            f(cursor.getLong(cursor.getColumnIndexOrThrow("updatestatus")) > 0);
            g(cursor.getString(cursor.getColumnIndexOrThrow("uuid")));
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2) {
        if (str != null && str2 != null) {
            if (str.equalsIgnoreCase("r")) {
                g(str2);
            } else if (str.equalsIgnoreCase("ab")) {
                g(aa.a(Double.parseDouble(str2)));
            } else if (str.equalsIgnoreCase("af")) {
                this.b = aa.a("TBL_ACCOUNTINFO", "id", str2, 1);
            } else if (str.equalsIgnoreCase("ag")) {
                this.c = aa.a("TBL_PROJECTINFO", "id", str2, 1);
            } else if (str.equalsIgnoreCase("ah")) {
                this.d = aa.a("TBL_TRADETARGET", "id", str2, 0);
            } else if (str.equalsIgnoreCase("al")) {
                this.e = Long.parseLong(str2) > 0;
            }
        }
    }

    public final void a(ArrayList arrayList) {
        this.f = arrayList;
    }

    public void f() {
        if (x() > 0) {
            l();
            if (B() == null || B().length() <= 0) {
                e.c().b().execSQL(String.format("DELETE FROM %s WHERE %s = %d", i(), h(), Long.valueOf(x())));
                this.f.clear();
                e.c().b().execSQL(String.format("DELETE FROM %s WHERE id = %d", w(), Long.valueOf(x())));
                l();
                return;
            }
            this.e = true;
            f(false);
            c();
        }
    }

    public final void g(long j) {
        this.a = j;
        if (this.f.size() == 1) {
            ((x) this.f.get(0)).b(j);
        }
    }

    public abstract String h();

    public final void h(long j) {
        this.b = j;
    }

    public abstract String i();

    public final void i(long j) {
        this.c = j;
    }

    /* access modifiers changed from: protected */
    public abstract String j();

    public final void j(long j) {
        this.d = j;
    }

    /* access modifiers changed from: protected */
    public abstract String k();

    /* access modifiers changed from: protected */
    public abstract void l();

    public final long o() {
        return this.a;
    }

    public final long p() {
        return this.b;
    }

    public final long q() {
        return this.c;
    }

    public final long r() {
        return this.d;
    }

    public final ArrayList s() {
        return this.f;
    }

    public final boolean t() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public final boolean u() {
        if (this.f == null || this.f.size() <= 0) {
            return false;
        }
        long j = 0;
        for (int i = 0; i < this.f.size(); i++) {
            j += ((x) this.f.get(i)).b();
        }
        return this.a == j;
    }

    /* access modifiers changed from: protected */
    public final void v() {
        SQLiteDatabase b2 = e.c().b();
        boolean C = C();
        if (C || !this.e) {
            try {
                b2.beginTransaction();
                if (C) {
                    b2.execSQL(k());
                } else {
                    b2.execSQL(j());
                    k((long) d(w()));
                }
                e.c().b().execSQL(String.format("DELETE FROM %s WHERE %s = %d", i(), h(), Long.valueOf(x())));
                for (int i = 0; i < this.f.size(); i++) {
                    if (((x) this.f.get(i)).a() > 0 && ((x) this.f.get(i)).b() > 0) {
                        ((x) this.f.get(i)).c();
                    }
                }
                b2.setTransactionSuccessful();
            } finally {
                b2.endTransaction();
            }
        }
    }
}
