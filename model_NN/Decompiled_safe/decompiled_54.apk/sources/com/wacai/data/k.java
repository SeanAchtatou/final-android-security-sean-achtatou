package com.wacai.data;

import android.util.Log;
import com.wacai.a.g;
import com.wacai.e;

public abstract class k extends ab {
    private String a = "";
    private long b;
    private boolean c = true;
    private boolean d;

    protected k(String str) {
        super(str);
    }

    public abstract void a(StringBuffer stringBuffer);

    public final void b(String str) {
        this.a = str;
    }

    public void c() {
        if (this.a == null || this.a.length() <= 0) {
            Log.e(getClass().getName(), "Invalidate name when save data.");
        } else if (C()) {
            Object[] objArr = new Object[9];
            objArr[0] = w();
            objArr[1] = e(this.a);
            objArr[2] = Integer.valueOf(this.c ? 1 : 0);
            objArr[3] = Integer.valueOf(this.d ? 1 : 0);
            objArr[4] = B();
            objArr[5] = Long.valueOf(this.b);
            objArr[6] = Integer.valueOf(A() ? 1 : 0);
            objArr[7] = e(g.a(this.a));
            objArr[8] = Long.valueOf(x());
            e.c().b().execSQL(String.format("UPDATE %s SET name = '%s', enable = %d, isdefault = %d, uuid = '%s', orderno = %d, updatestatus = %d, pinyin = '%s' WHERE id = %d", objArr));
        } else {
            Object[] objArr2 = new Object[8];
            objArr2[0] = w();
            objArr2[1] = B();
            objArr2[2] = e(this.a);
            objArr2[3] = Integer.valueOf(this.c ? 1 : 0);
            objArr2[4] = Long.valueOf(this.b);
            objArr2[5] = Integer.valueOf(this.d ? 1 : 0);
            objArr2[6] = Integer.valueOf(A() ? 1 : 0);
            objArr2[7] = e(g.a(this.a));
            e.c().b().execSQL(String.format("INSERT INTO %s (uuid, name, enable, orderno, isdefault, updatestatus, pinyin) VALUES ('%s', '%s', %d, %d, %d, %d, '%s')", objArr2));
            k((long) d(w()));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0063  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean c(java.lang.String r7) {
        /*
            r6 = this;
            r4 = 0
            r3 = 0
            if (r7 == 0) goto L_0x000a
            int r0 = r7.length()
            if (r0 > 0) goto L_0x000c
        L_0x000a:
            r0 = r3
        L_0x000b:
            return r0
        L_0x000c:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select * from "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r6.w()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = " where LOWER(name) = LOWER('"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.wacai.data.aa.e(r7)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "') and "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "id <> "
            java.lang.StringBuilder r0 = r0.append(r1)
            long r1 = r6.x()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = " and enable = 1"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ all -> 0x006a }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ all -> 0x006a }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ all -> 0x006a }
            if (r0 == 0) goto L_0x0068
            boolean r1 = r0.moveToFirst()     // Catch:{ all -> 0x0072 }
            if (r1 == 0) goto L_0x0068
            r1 = 1
        L_0x0061:
            if (r0 == 0) goto L_0x0066
            r0.close()
        L_0x0066:
            r0 = r1
            goto L_0x000b
        L_0x0068:
            r1 = r3
            goto L_0x0061
        L_0x006a:
            r0 = move-exception
            r1 = r4
        L_0x006c:
            if (r1 == 0) goto L_0x0071
            r1.close()
        L_0x0071:
            throw r0
        L_0x0072:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.k.c(java.lang.String):boolean");
    }

    public final void d(boolean z) {
        this.c = z;
    }

    public final void e(boolean z) {
        this.d = z;
    }

    public final void f(long j) {
        this.b = j;
    }

    public final String j() {
        return this.a;
    }

    public final long k() {
        return this.b;
    }

    public final boolean l() {
        return this.c;
    }

    public final boolean m() {
        return this.d;
    }
}
