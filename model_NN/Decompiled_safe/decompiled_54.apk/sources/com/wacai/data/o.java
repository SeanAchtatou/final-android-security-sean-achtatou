package com.wacai.data;

import android.os.Parcel;
import android.os.Parcelable;

final class o implements Parcelable.Creator {
    o() {
    }

    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new VersionItem(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new VersionItem[i];
    }
}
