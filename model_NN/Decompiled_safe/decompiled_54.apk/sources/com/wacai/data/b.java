package com.wacai.data;

import android.database.Cursor;
import android.util.Log;
import com.wacai.a.a;
import com.wacai.e;
import java.util.Date;

public final class b extends aa {
    private int a = 0;
    private int b = 0;
    private int c = 0;
    private long d = 0;
    private String e = "";
    private long f = 0;
    private boolean g = false;

    public b() {
        super("TBL_ALERT");
    }

    public static b a(long j, int i) {
        if (j <= 0) {
            return null;
        }
        return b(String.format("select * from %s where destid = %d and alertstyle = %d", "TBL_ALERT", Long.valueOf(j), Integer.valueOf(i)));
    }

    public static void a(long j, String str) {
        if (j > 0) {
            d(j);
            if (str != null && str.length() > 0) {
                e.c().b().execSQL("UPDATE " + str + " SET alertid = 0 where alertid = " + j);
            }
        }
    }

    public static boolean a() {
        Cursor cursor;
        try {
            Cursor rawQuery = e.c().b().rawQuery("select count(*) from TBL_ALERT where isread = 0 and nextday <= " + (System.currentTimeMillis() / 1000), null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToNext()) {
                        boolean z = rawQuery.getLong(0) > 0;
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return z;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return false;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x00c1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.wacai.data.b b(java.lang.String r7) {
        /*
            r4 = 1
            r5 = 0
            if (r7 != 0) goto L_0x0006
            r0 = r5
        L_0x0005:
            return r0
        L_0x0006:
            com.wacai.e r0 = com.wacai.e.c()     // Catch:{ Exception -> 0x0097, all -> 0x00bd }
            android.database.sqlite.SQLiteDatabase r0 = r0.b()     // Catch:{ Exception -> 0x0097, all -> 0x00bd }
            r1 = 0
            android.database.Cursor r0 = r0.rawQuery(r7, r1)     // Catch:{ Exception -> 0x0097, all -> 0x00bd }
            if (r0 == 0) goto L_0x001b
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            if (r1 != 0) goto L_0x0022
        L_0x001b:
            if (r0 == 0) goto L_0x0020
            r0.close()
        L_0x0020:
            r0 = r5
            goto L_0x0005
        L_0x0022:
            com.wacai.data.b r1 = new com.wacai.data.b     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            r1.<init>()     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            if (r0 == 0) goto L_0x008d
            java.lang.String r2 = "id"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            r1.k(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            java.lang.String r2 = "type"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            r1.a = r2     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            java.lang.String r2 = "alertstyle"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            r1.b = r2     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            java.lang.String r2 = "day"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            r1.c = r2     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            java.lang.String r2 = "nextday"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            r1.d = r2     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            java.lang.String r2 = "destid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            r1.f = r2     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            java.lang.String r2 = "comment"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            r1.e = r2     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            java.lang.String r2 = "isread"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
            if (r2 != r4) goto L_0x0095
            r2 = r4
        L_0x008b:
            r1.g = r2     // Catch:{ Exception -> 0x00cc, all -> 0x00c5 }
        L_0x008d:
            if (r0 == 0) goto L_0x0092
            r0.close()
        L_0x0092:
            r0 = r1
            goto L_0x0005
        L_0x0095:
            r2 = 0
            goto L_0x008b
        L_0x0097:
            r0 = move-exception
            r1 = r5
        L_0x0099:
            java.lang.String r2 = "Alert"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ca }
            r3.<init>()     // Catch:{ all -> 0x00ca }
            java.lang.String r4 = "findFirstAlert exception: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00ca }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00ca }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00ca }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x00ba
            r1.close()
        L_0x00ba:
            r0 = r5
            goto L_0x0005
        L_0x00bd:
            r0 = move-exception
            r1 = r5
        L_0x00bf:
            if (r1 == 0) goto L_0x00c4
            r1.close()
        L_0x00c4:
            throw r0
        L_0x00c5:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00bf
        L_0x00ca:
            r0 = move-exception
            goto L_0x00bf
        L_0x00cc:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0099
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.b.b(java.lang.String):com.wacai.data.b");
    }

    public static b c(long j) {
        if (j <= 0) {
            return null;
        }
        return b(String.format("select * from %s where id = %d", "TBL_ALERT", Long.valueOf(j)));
    }

    static void d(long j) {
        if (j > 0) {
            e.c().b().execSQL("DELETE FROM TBL_ALERT WHERE id = " + j);
        }
    }

    public final void a(int i) {
        this.a = i;
    }

    public final void a(long j) {
        this.d = j;
    }

    public final void a(String str) {
        this.e = str;
    }

    public final void a(boolean z) {
        this.g = z;
    }

    public final int b() {
        return this.a;
    }

    public final void b(int i) {
        this.b = i;
    }

    /* access modifiers changed from: package-private */
    public final void b(long j) {
        this.f = j;
    }

    public final void c() {
        if (x() > 0) {
            Object[] objArr = new Object[9];
            objArr[0] = "TBL_ALERT";
            objArr[1] = Integer.valueOf(this.a);
            objArr[2] = Integer.valueOf(this.b);
            objArr[3] = Integer.valueOf(this.c);
            objArr[4] = Long.valueOf(this.d);
            objArr[5] = Long.valueOf(this.f);
            objArr[6] = e(this.e);
            objArr[7] = Integer.valueOf(this.g ? 1 : 0);
            objArr[8] = Long.valueOf(x());
            e.c().b().execSQL(String.format("UPDATE %s SET type = %d, alertstyle = %d, day = %d, nextday = %d, destid = %d, comment = '%s', isread = %d WHERE id = %d", objArr));
            return;
        }
        Object[] objArr2 = new Object[8];
        objArr2[0] = "TBL_ALERT";
        objArr2[1] = Integer.valueOf(this.a);
        objArr2[2] = Integer.valueOf(this.b);
        objArr2[3] = Integer.valueOf(this.c);
        objArr2[4] = Long.valueOf(this.d);
        objArr2[5] = Long.valueOf(this.f);
        objArr2[6] = e(this.e);
        objArr2[7] = Integer.valueOf(this.g ? 1 : 0);
        e.c().b().execSQL(String.format("INSERT INTO %s (type, alertstyle, day, nextday, destid, comment, isread) VALUES (%d, %d, %d, %d, %d, '%s', %d)", objArr2));
        k((long) d(w()));
    }

    public final void c(int i) {
        this.c = i;
    }

    public final int d() {
        return this.c;
    }

    public final long e() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final boolean e(long j) {
        if (this.a != 0 || j >= 0) {
            if (this.a == 0) {
                a aVar = new a((j - ((((long) this.c) * 3600) * 24)) * 1000);
                aVar.f = 0;
                aVar.g = 0;
                aVar.h = 0;
                this.d = aVar.b() / 1000;
            } else if (this.a != 1 && this.a == 2) {
                a aVar2 = new a(new Date());
                if (this.c == 0) {
                    aVar2.c();
                    aVar2.e = 1;
                    aVar2.f = 0;
                    aVar2.g = 0;
                    aVar2.h = 0;
                    this.d = (aVar2.b() / 1000) - 86400;
                } else {
                    if (aVar2.e > this.c) {
                        aVar2.c();
                    }
                    aVar2.e = this.c;
                    aVar2.f = 0;
                    aVar2.g = 0;
                    aVar2.h = 0;
                    this.d = aVar2.b() / 1000;
                }
            }
            Log.d("Alert", String.valueOf(a.a(this.d * 1000)));
            return true;
        }
        Log.d("Alert", "Type = TYPE_ONCE but expectDate illegal! valuse = " + j);
        return false;
    }

    public final void f() {
        d(x());
    }

    public final boolean g() {
        if (this.a == 0) {
            return false;
        }
        if (this.a != 1 && this.a == 2) {
            Date date = new Date();
            a aVar = new a(date);
            if ((this.c != 0 && aVar.e >= this.c) || (this.c == 0 && a.a(date))) {
                aVar.c();
            }
            if (this.c == 0) {
                aVar.c();
                aVar.e = 1;
                aVar.f = 0;
                aVar.g = 0;
                aVar.h = 0;
                this.d = (aVar.b() / 1000) - 86400;
            } else {
                aVar.e = this.c;
                aVar.f = 0;
                aVar.g = 0;
                aVar.h = 0;
                this.d = aVar.b() / 1000;
            }
        }
        Log.d("Alert", String.valueOf(a.a(this.d * 1000)));
        return true;
    }
}
