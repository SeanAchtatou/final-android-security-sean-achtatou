package com.wacai.data;

import android.database.Cursor;
import android.util.Log;
import com.wacai.a.a;
import com.wacai.b;
import com.wacai.e;
import java.util.Date;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class r extends s {
    private long a;
    private long b;
    private String c;
    private long d;
    private long e;

    public r() {
        this(true);
    }

    private r(boolean z) {
        super("TBL_INCOMEINFO", z);
        if (z) {
            this.a = f("TBL_INCOMEMAINTYPEINFO");
            this.b = new Date().getTime() / 1000;
        } else {
            this.a = 1;
        }
        this.d = 0;
        this.c = "";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.b.a(long, long, boolean):long
     arg types: [long, long, int]
     candidates:
      com.wacai.b.a(long, long, long):void
      com.wacai.b.a(long, long, boolean):long */
    public static int a(long j, long j2, StringBuffer stringBuffer) {
        Cursor cursor;
        if (stringBuffer == null) {
            return 0;
        }
        try {
            Cursor rawQuery = e.c().b().rawQuery("select TBL_INCOMEINFO.id as id, TBL_INCOMEINFO.uuid as uuid, TBL_INCOMEINFO.money as money, TBL_INCOMEMAINTYPEINFO.uuid as tuuid, TBL_INCOMEINFO.incomedate as dt, TBL_ACCOUNTINFO.uuid as accuuid, TBL_PROJECTINFO.uuid as prjuuid, TBL_INCOMEINFO.targetid as tgtid, TBL_INCOMEINFO.comment as comment, TBL_INCOMEINFO.isdelete as isdelete from TBL_INCOMEINFO, TBL_INCOMEMAINTYPEINFO, TBL_PROJECTINFO, TBL_ACCOUNTINFO where TBL_INCOMEINFO.uuid IS NOT NULL AND TBL_INCOMEINFO.uuid <> ''  AND TBL_INCOMEINFO.paybackid=0 AND TBL_INCOMEINFO.ymd >= " + ((10000 * j) + (100 * j2)) + " AND TBL_INCOMEINFO.ymd < " + ((10000 * j) + (100 * j2) + 99) + " AND TBL_INCOMEINFO.updatestatus = 0 AND TBL_INCOMEINFO.scheduleincomeid = 0 " + "AND TBL_INCOMEMAINTYPEINFO.id = TBL_INCOMEINFO.typeid AND " + "TBL_PROJECTINFO.id = TBL_INCOMEINFO.projectid AND TBL_ACCOUNTINFO.id = TBL_INCOMEINFO.accountid " + "GROUP BY TBL_INCOMEINFO.id", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        int count = rawQuery.getCount();
                        long a2 = count > 0 ? b.a(j, j2, false) : 0;
                        r rVar = new r(false);
                        for (int i = 0; i < count; i++) {
                            stringBuffer.append("<p><r>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("uuid")));
                            stringBuffer.append("</r><ab>");
                            stringBuffer.append(a(l(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("money"))), 2));
                            stringBuffer.append("</ab><t>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("tuuid")));
                            stringBuffer.append("</t><ap>");
                            stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("dt")));
                            stringBuffer.append("</ap><af>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("accuuid")));
                            stringBuffer.append("</af><ag>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("prjuuid")));
                            stringBuffer.append("</ag><ah>");
                            long j3 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("tgtid"));
                            if (j3 > 0) {
                                stringBuffer.append(aa.a("TBL_TRADETARGET", "uuid", (int) j3));
                            }
                            stringBuffer.append("</ah><aq>");
                            stringBuffer.append(h(rawQuery.getString(rawQuery.getColumnIndexOrThrow("comment"))));
                            stringBuffer.append("</aq><al>");
                            stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("isdelete")));
                            stringBuffer.append("</al><am>");
                            x.a(rVar, rawQuery.getInt(rawQuery.getColumnIndexOrThrow("id")), stringBuffer);
                            stringBuffer.append("</am><ar>");
                            stringBuffer.append(a2);
                            stringBuffer.append("</ar></p>");
                            rawQuery.moveToNext();
                        }
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return count;
                    }
                } catch (Throwable th) {
                    th = th;
                    cursor = rawQuery;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return 0;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab
     arg types: [org.w3c.dom.Element, com.wacai.data.r, int]
     candidates:
      com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int
      com.wacai.data.aa.a(java.lang.String, java.lang.String, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab */
    public static r a(Element element) {
        if (element == null) {
            return null;
        }
        try {
            r rVar = (r) ab.a(element, (ab) new r(false), false);
            NodeList elementsByTagName = element.getElementsByTagName("an");
            int length = elementsByTagName.getLength();
            for (int i = 0; i < length; i++) {
                rVar.s().add(x.a((Element) elementsByTagName.item(i), rVar));
            }
            return rVar;
        } catch (Exception e2) {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.r c(long r5) {
        /*
            r3 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select * from TBL_INCOMEINFO where id = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x0046, all -> 0x004f }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x0046, all -> 0x004f }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x0046, all -> 0x004f }
            if (r0 == 0) goto L_0x0029
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            if (r1 != 0) goto L_0x0030
        L_0x0029:
            if (r0 == 0) goto L_0x002e
            r0.close()
        L_0x002e:
            r0 = r3
        L_0x002f:
            return r0
        L_0x0030:
            com.wacai.data.r r1 = new com.wacai.data.r     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            r2 = 0
            r1.<init>(r2)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            r1.k(r5)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            r1.a(r0)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            com.wacai.data.x.a(r1)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            if (r0 == 0) goto L_0x0044
            r0.close()
        L_0x0044:
            r0 = r1
            goto L_0x002f
        L_0x0046:
            r0 = move-exception
            r0 = r3
        L_0x0048:
            if (r0 == 0) goto L_0x004d
            r0.close()
        L_0x004d:
            r0 = r3
            goto L_0x002f
        L_0x004f:
            r0 = move-exception
            r1 = r3
        L_0x0051:
            if (r1 == 0) goto L_0x0056
            r1.close()
        L_0x0056:
            throw r0
        L_0x0057:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0051
        L_0x005c:
            r1 = move-exception
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.r.c(long):com.wacai.data.r");
    }

    public static void d(long j) {
        r c2;
        if (j > 0 && (c2 = c(j)) != null) {
            c2.f();
        }
    }

    public final long a() {
        return this.a;
    }

    public final void a(long j) {
        this.a = j;
    }

    /* access modifiers changed from: protected */
    public final void a(Cursor cursor) {
        if (cursor != null) {
            this.a = cursor.getLong(cursor.getColumnIndexOrThrow("typeid"));
            this.b = cursor.getLong(cursor.getColumnIndexOrThrow("incomedate"));
            this.c = cursor.getString(cursor.getColumnIndexOrThrow("comment"));
            this.d = cursor.getLong(cursor.getColumnIndexOrThrow("scheduleincomeid"));
            this.e = cursor.getLong(cursor.getColumnIndexOrThrow("paybackid"));
            super.a(cursor);
        }
    }

    public final void a(String str) {
        this.c = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.s.a(java.lang.String, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.wacai.data.aa.a(double, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.aa.a(java.lang.String, long):void
      com.wacai.data.s.a(java.lang.String, java.lang.String):void */
    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str != null && str2 != null) {
            if (str.equalsIgnoreCase("t")) {
                this.a = aa.a("TBL_INCOMEMAINTYPEINFO", "id", str2, 1);
            } else if (str.equalsIgnoreCase("ap")) {
                this.b = Long.parseLong(str2);
            } else if (str.equalsIgnoreCase("aq")) {
                this.c = str2;
            } else {
                super.a(str, str2);
            }
        }
    }

    public final long b() {
        return this.b;
    }

    public final void b(long j) {
        this.b = j;
    }

    public final void c() {
        if (!b("TBL_INCOMEMAINTYPEINFO", this.a)) {
            Log.e("WAC_Income", "Invalide income type when save data");
        } else if (!b("TBL_ACCOUNTINFO", p())) {
            Log.e("WAC_Income", "Invalide account when save data");
        } else if (!b("TBL_PROJECTINFO", q())) {
            Log.e("WAC_Income", "Invalide project when save data");
        } else if (t() || u()) {
            v();
            if (r() > 0) {
                e.c().b().execSQL("UPDATE TBL_TRADETARGET SET refcount = refcount+1 WHERE id = " + r());
            }
        } else {
            Log.e("WAC_Income", "Money conflict with member shares!");
        }
    }

    public final String d() {
        return this.c;
    }

    public final long e() {
        return this.d;
    }

    public final void f() {
        if (this.e > 0) {
            d.h(this.e);
        } else {
            super.f();
        }
    }

    public final String h() {
        return "incomeid";
    }

    public final String i() {
        return "TBL_INCOMEMEMBERINFO";
    }

    /* access modifiers changed from: protected */
    public final String j() {
        Object[] objArr = new Object[12];
        objArr[0] = w();
        objArr[1] = Long.valueOf(o());
        objArr[2] = Long.valueOf(this.a);
        objArr[3] = Long.valueOf(this.b);
        objArr[4] = Long.valueOf(p());
        objArr[5] = Long.valueOf(q());
        objArr[6] = e(this.c);
        objArr[7] = Integer.valueOf(t() ? 1 : 0);
        objArr[8] = Integer.valueOf(A() ? 1 : 0);
        objArr[9] = B();
        objArr[10] = Long.valueOf(a.a(this.b * 1000));
        objArr[11] = Long.valueOf(r());
        return String.format("INSERT INTO %s (money, typeid, incomedate, accountid, projectid, comment, isdelete, updatestatus, uuid, ymd, targetid) VALUES (%d, %d, %d, %d, %d, '%s', %d, %d, '%s', %d, %d)", objArr);
    }

    /* access modifiers changed from: protected */
    public final String k() {
        Object[] objArr = new Object[13];
        objArr[0] = w();
        objArr[1] = Long.valueOf(o());
        objArr[2] = Long.valueOf(this.a);
        objArr[3] = Long.valueOf(this.b);
        objArr[4] = Long.valueOf(p());
        objArr[5] = Long.valueOf(q());
        objArr[6] = Long.valueOf(r());
        objArr[7] = e(this.c);
        objArr[8] = Integer.valueOf(t() ? 1 : 0);
        objArr[9] = Integer.valueOf(A() ? 1 : 0);
        objArr[10] = B();
        objArr[11] = Long.valueOf(a.a(this.b * 1000));
        objArr[12] = Long.valueOf(x());
        return String.format("UPDATE %s SET money = %d, typeid = %d, incomedate = %d, accountid = %d, projectid = %d, targetid = %d, comment = '%s', isdelete = %d, updatestatus = %d, uuid = '%s', ymd = %d WHERE id = %d", objArr);
    }

    /* access modifiers changed from: protected */
    public final void l() {
    }
}
