package com.wacai.data;

import android.database.Cursor;
import com.wacai.e;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class ab extends aa {
    private boolean a = true;
    private String b = "";
    private boolean c;

    protected ab(String str) {
        super(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.ab.a(java.lang.String, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.wacai.data.aa.a(double, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.aa.a(java.lang.String, long):void
      com.wacai.data.ab.a(java.lang.String, java.lang.String):void */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00bc A[Catch:{ Exception -> 0x00ac }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static com.wacai.data.ab a(org.w3c.dom.Element r8, com.wacai.data.ab r9, boolean r10) {
        /*
            r3 = 0
            r6 = 0
            if (r8 == 0) goto L_0x0006
            if (r9 != 0) goto L_0x0008
        L_0x0006:
            r1 = r6
        L_0x0007:
            return r1
        L_0x0008:
            org.w3c.dom.NodeList r1 = r8.getChildNodes()
            if (r1 == 0) goto L_0x0014
            int r2 = r1.getLength()
            if (r2 != 0) goto L_0x0016
        L_0x0014:
            r1 = r6
            goto L_0x0007
        L_0x0016:
            int r2 = r1.getLength()     // Catch:{ Exception -> 0x00ac }
        L_0x001a:
            if (r3 >= r2) goto L_0x0044
            org.w3c.dom.Node r4 = r1.item(r3)     // Catch:{ Exception -> 0x00ac }
            if (r4 == 0) goto L_0x0041
            java.lang.String r5 = r4.getNodeName()     // Catch:{ Exception -> 0x00ac }
            if (r5 == 0) goto L_0x0041
            java.lang.Class<org.w3c.dom.Element> r5 = org.w3c.dom.Element.class
            boolean r5 = r5.isInstance(r4)     // Catch:{ Exception -> 0x00ac }
            if (r5 == 0) goto L_0x0041
            org.w3c.dom.Node r5 = r4.getFirstChild()     // Catch:{ Exception -> 0x00ac }
            if (r5 == 0) goto L_0x0041
            java.lang.String r5 = r4.getNodeName()     // Catch:{ Exception -> 0x00ac }
            java.lang.String r4 = a(r4)     // Catch:{ Exception -> 0x00ac }
            r9.a(r5, r4)     // Catch:{ Exception -> 0x00ac }
        L_0x0041:
            int r3 = r3 + 1
            goto L_0x001a
        L_0x0044:
            if (r10 == 0) goto L_0x0060
            java.lang.Class<com.wacai.data.k> r1 = com.wacai.data.k.class
            boolean r1 = r1.isInstance(r9)     // Catch:{ Exception -> 0x00ac }
            if (r1 == 0) goto L_0x0060
            boolean r1 = r9.C()     // Catch:{ Exception -> 0x00ac }
            if (r1 == 0) goto L_0x0056
            r1 = r6
            goto L_0x0007
        L_0x0056:
            if (r9 == 0) goto L_0x0060
            java.lang.Class<com.wacai.data.k> r1 = com.wacai.data.k.class
            boolean r1 = r1.isInstance(r9)     // Catch:{ Exception -> 0x00ac }
            if (r1 != 0) goto L_0x0065
        L_0x0060:
            r1 = 1
            r9.c = r1     // Catch:{ Exception -> 0x00ac }
            r1 = r9
            goto L_0x0007
        L_0x0065:
            r0 = r9
            com.wacai.data.k r0 = (com.wacai.data.k) r0     // Catch:{ Exception -> 0x00ac }
            r1 = r0
            java.lang.String r2 = "select id from %s where LOWER(name) = LOWER('%s') AND (uuid IS NULL OR uuid = '')"
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x00ac }
            r4 = 0
            java.lang.String r5 = r1.w()     // Catch:{ Exception -> 0x00ac }
            r3[r4] = r5     // Catch:{ Exception -> 0x00ac }
            r4 = 1
            java.lang.String r1 = r1.j()     // Catch:{ Exception -> 0x00ac }
            java.lang.String r1 = com.wacai.data.aa.e(r1)     // Catch:{ Exception -> 0x00ac }
            r3[r4] = r1     // Catch:{ Exception -> 0x00ac }
            java.lang.String r1 = java.lang.String.format(r2, r3)     // Catch:{ Exception -> 0x00ac }
            com.wacai.e r2 = com.wacai.e.c()     // Catch:{ Exception -> 0x00b0, all -> 0x00b8 }
            android.database.sqlite.SQLiteDatabase r2 = r2.b()     // Catch:{ Exception -> 0x00b0, all -> 0x00b8 }
            r3 = 0
            android.database.Cursor r1 = r2.rawQuery(r1, r3)     // Catch:{ Exception -> 0x00b0, all -> 0x00b8 }
            if (r1 == 0) goto L_0x00a6
            boolean r2 = r1.moveToFirst()     // Catch:{ Exception -> 0x00c5, all -> 0x00c0 }
            if (r2 == 0) goto L_0x00a6
            java.lang.String r2 = "id"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00c5, all -> 0x00c0 }
            long r2 = r1.getLong(r2)     // Catch:{ Exception -> 0x00c5, all -> 0x00c0 }
            r9.k(r2)     // Catch:{ Exception -> 0x00c5, all -> 0x00c0 }
        L_0x00a6:
            if (r1 == 0) goto L_0x0060
            r1.close()     // Catch:{ Exception -> 0x00ac }
            goto L_0x0060
        L_0x00ac:
            r1 = move-exception
            r1 = r6
            goto L_0x0007
        L_0x00b0:
            r1 = move-exception
            r1 = r6
        L_0x00b2:
            if (r1 == 0) goto L_0x0060
            r1.close()     // Catch:{ Exception -> 0x00ac }
            goto L_0x0060
        L_0x00b8:
            r1 = move-exception
            r2 = r6
        L_0x00ba:
            if (r2 == 0) goto L_0x00bf
            r2.close()     // Catch:{ Exception -> 0x00ac }
        L_0x00bf:
            throw r1     // Catch:{ Exception -> 0x00ac }
        L_0x00c0:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x00ba
        L_0x00c5:
            r2 = move-exception
            goto L_0x00b2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab");
    }

    public static String a(Node node) {
        if (node == null) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        NodeList childNodes = node.getChildNodes();
        if (childNodes == null) {
            return "";
        }
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node item = childNodes.item(i);
            if (item != null) {
                stringBuffer.append(item.getNodeValue());
            }
        }
        return stringBuffer.toString();
    }

    public static String b(String str, String str2, String str3) {
        if (str3 == null || str == null || str2 == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        String str4 = str3;
        while (true) {
            int indexOf = str4.indexOf(str);
            if (indexOf != -1) {
                stringBuffer.append(str4.substring(0, indexOf) + str2);
                str4 = str4.substring(indexOf + str.length());
            } else {
                stringBuffer.append(str4);
                return stringBuffer.toString();
            }
        }
    }

    public static String h(String str) {
        String[] strArr = {"&", "<", ">", "\"", "'"};
        String[] strArr2 = {"&amp;", "&lt;", "&gt;", "&quot;", "&apos;"};
        if (str == null) {
            return str;
        }
        String str2 = str;
        for (int i = 0; i < strArr.length; i++) {
            str2 = b(strArr[i], strArr2[i], str2);
        }
        return str2;
    }

    public final boolean A() {
        return this.a;
    }

    public final String B() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public final boolean C() {
        Cursor cursor;
        if (x() > 0) {
            return true;
        }
        if (this.b == null || this.b.length() == 0) {
            return false;
        }
        try {
            Cursor rawQuery = e.c().b().rawQuery(String.format("select * from %s where uuid = '%s'", w(), this.b), null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.getCount() > 0) {
                        rawQuery.moveToFirst();
                        k(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("id")));
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return true;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return false;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(String str, String str2);

    public final void f(boolean z) {
        this.a = z;
    }

    public final void g(String str) {
        this.b = str;
    }

    /* access modifiers changed from: protected */
    public final boolean z() {
        return this.c;
    }
}
