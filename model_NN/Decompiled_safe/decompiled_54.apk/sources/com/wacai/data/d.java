package com.wacai.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.wacai.e;
import org.w3c.dom.Element;

public class d extends ab {
    private static long i = 0;
    private long a;
    private long b;
    private long c;
    private int d = 0;
    private boolean e;
    private long f;
    private long g;
    private String h = "";

    public d() {
        super("TBL_PAYBACK");
    }

    public static int a(StringBuffer stringBuffer) {
        Cursor cursor;
        if (stringBuffer == null) {
            return 0;
        }
        try {
            Cursor rawQuery = e.c().b().rawQuery("select * from TBL_PAYBACK where  ( uuid IS NOT NULL AND uuid <> '' ) AND updatestatus = 0", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        int count = rawQuery.getCount();
                        for (int i2 = 0; i2 < count; i2++) {
                            stringBuffer.append("<bk><r>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("uuid")));
                            stringBuffer.append("</r><t>");
                            stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("type")));
                            stringBuffer.append("</t><bl>");
                            stringBuffer.append(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("date")));
                            stringBuffer.append("</bl><ab>");
                            stringBuffer.append(aa.a(aa.l(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("money"))), 2));
                            stringBuffer.append("</ab><bj>");
                            stringBuffer.append(aa.a("TBL_ACCOUNTINFO", "uuid", rawQuery.getInt(rawQuery.getColumnIndexOrThrow("accountid"))));
                            stringBuffer.append("</bj><bi>");
                            stringBuffer.append(aa.a("TBL_ACCOUNTINFO", "uuid", rawQuery.getInt(rawQuery.getColumnIndexOrThrow("debtid"))));
                            stringBuffer.append("</bi><aq>");
                            stringBuffer.append(h(rawQuery.getString(rawQuery.getColumnIndexOrThrow("comment"))));
                            stringBuffer.append("</aq><al>");
                            stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("isdelete")));
                            stringBuffer.append("</al><bm>");
                            stringBuffer.append(aa.a(aa.l(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("profit"))), 2));
                            stringBuffer.append("</bm></bk>");
                            rawQuery.moveToNext();
                        }
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return count;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return 0;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab
     arg types: [org.w3c.dom.Element, com.wacai.data.d, int]
     candidates:
      com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int
      com.wacai.data.aa.a(java.lang.String, java.lang.String, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab */
    public static d a(Element element) {
        if (element == null) {
            return null;
        }
        return (d) ab.a(element, (ab) new d(), false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x00db  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.d f(long r10) {
        /*
            r8 = 1
            r7 = 0
            r6 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select * from TBL_PAYBACK where id = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r10)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x00cd, all -> 0x00d7 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x00cd, all -> 0x00d7 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00d7 }
            if (r0 == 0) goto L_0x002b
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            if (r1 != 0) goto L_0x0032
        L_0x002b:
            if (r0 == 0) goto L_0x0030
            r0.close()
        L_0x0030:
            r0 = r6
        L_0x0031:
            return r0
        L_0x0032:
            com.wacai.data.d r1 = new com.wacai.data.d     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            r1.<init>()     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            r1.k(r10)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            if (r0 == 0) goto L_0x00c1
            java.lang.String r2 = "type"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            r1.d = r2     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            java.lang.String r2 = "date"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            r1.a = r2     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            java.lang.String r2 = "money"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            r1.b = r2     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            java.lang.String r2 = "profit"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            r1.c = r2     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            java.lang.String r2 = "accountid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            r1.f = r2     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            java.lang.String r2 = "debtid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            r1.g = r2     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            java.lang.String r2 = "comment"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            r1.a(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            java.lang.String r2 = "isdelete"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            if (r2 <= 0) goto L_0x00c9
            r2 = r8
        L_0x009e:
            r1.e = r2     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            java.lang.String r2 = "updatestatus"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            r4 = 0
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x00cb
            r2 = r8
        L_0x00b1:
            r1.f(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            java.lang.String r2 = "uuid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
            r1.g(r2)     // Catch:{ Exception -> 0x00e4, all -> 0x00df }
        L_0x00c1:
            if (r0 == 0) goto L_0x00c6
            r0.close()
        L_0x00c6:
            r0 = r1
            goto L_0x0031
        L_0x00c9:
            r2 = r7
            goto L_0x009e
        L_0x00cb:
            r2 = r7
            goto L_0x00b1
        L_0x00cd:
            r0 = move-exception
            r0 = r6
        L_0x00cf:
            if (r0 == 0) goto L_0x00d4
            r0.close()
        L_0x00d4:
            r0 = r6
            goto L_0x0031
        L_0x00d7:
            r0 = move-exception
            r1 = r6
        L_0x00d9:
            if (r1 == 0) goto L_0x00de
            r1.close()
        L_0x00de:
            throw r0
        L_0x00df:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00d9
        L_0x00e4:
            r1 = move-exception
            goto L_0x00cf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.d.f(long):com.wacai.data.d");
    }

    public static void g(long j) {
        if (j > 0) {
            f(j).f();
        }
    }

    static void h(long j) {
        d f2;
        if (j > 0 && (f2 = f(j)) != null) {
            f2.c = 0;
            f2.f(false);
            f2.c();
        }
    }

    private void j() {
        u.a(2, x());
        SQLiteDatabase b2 = e.c().b();
        b2.execSQL("DELETE FROM TBL_OUTGOINFO WHERE paybackid = " + x());
        b2.execSQL("DELETE FROM TBL_INCOMEINFO WHERE paybackid = " + x());
    }

    public final long a() {
        return this.a;
    }

    public final void a(int i2) {
        this.d = i2;
    }

    public final void a(long j) {
        this.a = j;
    }

    public final void a(String str) {
        this.h = str;
        if (this.h == null) {
            this.h = "";
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str != null && str2 != null) {
            if (str.equalsIgnoreCase("r")) {
                g(str2);
            } else if (str.equalsIgnoreCase("ab")) {
                this.b = aa.a(Double.parseDouble(str2));
            } else if (str.equalsIgnoreCase("bl")) {
                this.a = Long.parseLong(str2);
            } else if (str.equalsIgnoreCase("bj")) {
                this.f = aa.a("TBL_ACCOUNTINFO", "id", str2, 1);
            } else if (str.equalsIgnoreCase("bi")) {
                this.g = aa.a("TBL_ACCOUNTINFO", "id", str2, 0);
            } else if (str.equalsIgnoreCase("t")) {
                this.d = Integer.parseInt(str2);
            } else if (str.equalsIgnoreCase("aq")) {
                a(str2);
            } else if (str.equalsIgnoreCase("al")) {
                this.e = Long.parseLong(str2) > 0;
            } else if (str.equalsIgnoreCase("bm")) {
                this.c = aa.a(Double.parseDouble(str2));
            }
        }
    }

    public final void a(boolean z) {
        this.e = z;
    }

    public final long b() {
        return this.b;
    }

    public final void b(long j) {
        this.b = j;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x00f8 A[Catch:{ all -> 0x020e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c() {
        /*
            r12 = this;
            r8 = 0
            r7 = 1
            boolean r0 = r12.C()
            if (r0 != 0) goto L_0x000d
            boolean r1 = r12.e
            if (r1 == 0) goto L_0x000d
        L_0x000c:
            return
        L_0x000d:
            com.wacai.e r1 = com.wacai.e.c()
            android.database.sqlite.SQLiteDatabase r11 = r1.b()
            r11.beginTransaction()     // Catch:{ all -> 0x020e }
            if (r0 == 0) goto L_0x017a
            java.lang.String r0 = "UPDATE %s SET type = %d, money = %d, profit = %d, date = %d, comment = '%s', accountid = %d, debtid = %d, updatestatus = %d, ymd = %d, isdelete = %d WHERE id = %d"
            r1 = 12
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x020e }
            r2 = 0
            java.lang.String r3 = "TBL_PAYBACK"
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 1
            int r3 = r12.d     // Catch:{ all -> 0x020e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 2
            long r3 = r12.b     // Catch:{ all -> 0x020e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 3
            long r3 = r12.c     // Catch:{ all -> 0x020e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 4
            long r3 = r12.a     // Catch:{ all -> 0x020e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 5
            java.lang.String r3 = r12.h     // Catch:{ all -> 0x020e }
            java.lang.String r3 = e(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 6
            long r3 = r12.f     // Catch:{ all -> 0x020e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 7
            long r3 = r12.g     // Catch:{ all -> 0x020e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 8
            boolean r3 = r12.A()     // Catch:{ all -> 0x020e }
            if (r3 == 0) goto L_0x0174
            r3 = r7
        L_0x006d:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 9
            long r3 = r12.a     // Catch:{ all -> 0x020e }
            r5 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 * r5
            long r3 = com.wacai.a.a.a(r3)     // Catch:{ all -> 0x020e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 10
            boolean r3 = r12.e     // Catch:{ all -> 0x020e }
            if (r3 == 0) goto L_0x0177
            r3 = r7
        L_0x008b:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 11
            long r3 = r12.x()     // Catch:{ all -> 0x020e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            java.lang.String r0 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x020e }
            r11.execSQL(r0)     // Catch:{ all -> 0x020e }
            boolean r0 = r12.e     // Catch:{ all -> 0x020e }
            if (r0 == 0) goto L_0x00ab
            r12.j()     // Catch:{ all -> 0x020e }
        L_0x00ab:
            boolean r0 = r12.e     // Catch:{ all -> 0x020e }
            if (r0 != 0) goto L_0x016c
            int r0 = r12.d     // Catch:{ all -> 0x020e }
            switch(r0) {
                case 0: goto L_0x0217;
                case 1: goto L_0x022e;
                default: goto L_0x00b4;
            }     // Catch:{ all -> 0x020e }
        L_0x00b4:
            com.wacai.e r0 = com.wacai.e.c()     // Catch:{ all -> 0x020e }
            android.database.sqlite.SQLiteDatabase r0 = r0.b()     // Catch:{ all -> 0x020e }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x020e }
            r1.<init>()     // Catch:{ all -> 0x020e }
            java.lang.String r2 = "DELETE FROM TBL_OUTGOINFO WHERE paybackid = "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x020e }
            long r2 = r12.x()     // Catch:{ all -> 0x020e }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x020e }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x020e }
            r0.execSQL(r1)     // Catch:{ all -> 0x020e }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x020e }
            r1.<init>()     // Catch:{ all -> 0x020e }
            java.lang.String r2 = "DELETE FROM TBL_INCOMEINFO WHERE paybackid = "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x020e }
            long r2 = r12.x()     // Catch:{ all -> 0x020e }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x020e }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x020e }
            r0.execSQL(r1)     // Catch:{ all -> 0x020e }
            long r1 = r12.c     // Catch:{ all -> 0x020e }
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 == 0) goto L_0x016c
            int r1 = r12.d     // Catch:{ all -> 0x020e }
            if (r1 != 0) goto L_0x0235
            long r1 = r12.c     // Catch:{ all -> 0x020e }
        L_0x00fe:
            r3 = 0
            int r3 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r3 <= 0) goto L_0x023c
            java.lang.String r3 = "INSERT INTO TBL_INCOMEINFO (money, typeid, incomedate, accountid, projectid, comment, isdelete, updatestatus, paybackid, ymd) VALUES (%d, 6, %d, %d, 1, '', 0, 1, %d, %d)"
            r4 = 5
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x020e }
            r5 = 0
            java.lang.Long r6 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x020e }
            r4[r5] = r6     // Catch:{ all -> 0x020e }
            r5 = 1
            long r6 = r12.a     // Catch:{ all -> 0x020e }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x020e }
            r4[r5] = r6     // Catch:{ all -> 0x020e }
            r5 = 2
            long r6 = r12.f     // Catch:{ all -> 0x020e }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x020e }
            r4[r5] = r6     // Catch:{ all -> 0x020e }
            r5 = 3
            long r6 = r12.x()     // Catch:{ all -> 0x020e }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x020e }
            r4[r5] = r6     // Catch:{ all -> 0x020e }
            r5 = 4
            long r6 = r12.a     // Catch:{ all -> 0x020e }
            r8 = 1000(0x3e8, double:4.94E-321)
            long r6 = r6 * r8
            long r6 = com.wacai.a.a.a(r6)     // Catch:{ all -> 0x020e }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x020e }
            r4[r5] = r6     // Catch:{ all -> 0x020e }
            java.lang.String r3 = java.lang.String.format(r3, r4)     // Catch:{ all -> 0x020e }
            r0.execSQL(r3)     // Catch:{ all -> 0x020e }
            java.lang.String r3 = "TBL_INCOMEINFO"
            int r3 = com.wacai.data.aa.d(r3)     // Catch:{ all -> 0x020e }
            java.lang.String r4 = "INSERT INTO TBL_INCOMEMEMBERINFO (incomeid, memberid, sharemoney) VALUES (%d, %d, %d)"
            r5 = 3
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x020e }
            r6 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x020e }
            r5[r6] = r3     // Catch:{ all -> 0x020e }
            r3 = 1
            r6 = 1
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x020e }
            r5[r3] = r6     // Catch:{ all -> 0x020e }
            r3 = 2
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x020e }
            r5[r3] = r1     // Catch:{ all -> 0x020e }
            java.lang.String r1 = java.lang.String.format(r4, r5)     // Catch:{ all -> 0x020e }
            r0.execSQL(r1)     // Catch:{ all -> 0x020e }
        L_0x016c:
            r11.setTransactionSuccessful()     // Catch:{ all -> 0x020e }
            r11.endTransaction()
            goto L_0x000c
        L_0x0174:
            r3 = r8
            goto L_0x006d
        L_0x0177:
            r3 = r8
            goto L_0x008b
        L_0x017a:
            java.lang.String r0 = "INSERT INTO %s (type, money, profit, date, accountid, debtid, updatestatus, ymd, isdelete, comment, uuid) VALUES (%d, %d, %d, %d, %d, %d, %d, %d, %d, '%s', '%s')"
            r1 = 12
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x020e }
            r2 = 0
            java.lang.String r3 = "TBL_PAYBACK"
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 1
            int r3 = r12.d     // Catch:{ all -> 0x020e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 2
            long r3 = r12.b     // Catch:{ all -> 0x020e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 3
            long r3 = r12.c     // Catch:{ all -> 0x020e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 4
            long r3 = r12.a     // Catch:{ all -> 0x020e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 5
            long r3 = r12.f     // Catch:{ all -> 0x020e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 6
            long r3 = r12.g     // Catch:{ all -> 0x020e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 7
            boolean r3 = r12.A()     // Catch:{ all -> 0x020e }
            if (r3 == 0) goto L_0x0213
            r3 = r7
        L_0x01c3:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 8
            long r3 = r12.a     // Catch:{ all -> 0x020e }
            r5 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 * r5
            long r3 = com.wacai.a.a.a(r3)     // Catch:{ all -> 0x020e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 9
            boolean r3 = r12.e     // Catch:{ all -> 0x020e }
            if (r3 == 0) goto L_0x0215
            r3 = r7
        L_0x01e1:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 10
            java.lang.String r3 = r12.h     // Catch:{ all -> 0x020e }
            java.lang.String r3 = e(r3)     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            r2 = 11
            java.lang.String r3 = r12.B()     // Catch:{ all -> 0x020e }
            r1[r2] = r3     // Catch:{ all -> 0x020e }
            java.lang.String r0 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x020e }
            r11.execSQL(r0)     // Catch:{ all -> 0x020e }
            java.lang.String r0 = r12.w()     // Catch:{ all -> 0x020e }
            int r0 = d(r0)     // Catch:{ all -> 0x020e }
            long r0 = (long) r0     // Catch:{ all -> 0x020e }
            r12.k(r0)     // Catch:{ all -> 0x020e }
            goto L_0x00ab
        L_0x020e:
            r0 = move-exception
            r11.endTransaction()
            throw r0
        L_0x0213:
            r3 = r8
            goto L_0x01c3
        L_0x0215:
            r3 = r8
            goto L_0x01e1
        L_0x0217:
            long r0 = r12.g     // Catch:{ all -> 0x020e }
            long r2 = r12.f     // Catch:{ all -> 0x020e }
            r6 = r2
            r4 = r0
        L_0x021d:
            long r0 = r12.b     // Catch:{ all -> 0x020e }
            long r2 = r12.c     // Catch:{ all -> 0x020e }
            long r0 = r0 - r2
            long r2 = r12.a     // Catch:{ all -> 0x020e }
            r8 = 2
            long r9 = r12.x()     // Catch:{ all -> 0x020e }
            com.wacai.data.u.a(r0, r2, r4, r6, r8, r9)     // Catch:{ all -> 0x020e }
            goto L_0x00b4
        L_0x022e:
            long r0 = r12.f     // Catch:{ all -> 0x020e }
            long r2 = r12.g     // Catch:{ all -> 0x020e }
            r6 = r2
            r4 = r0
            goto L_0x021d
        L_0x0235:
            long r1 = r12.c     // Catch:{ all -> 0x020e }
            r3 = -1
            long r1 = r1 * r3
            goto L_0x00fe
        L_0x023c:
            r3 = -1
            long r1 = r1 * r3
            java.lang.String r3 = "INSERT INTO TBL_OUTGOINFO (money, subtypeid, outgodate, accountid, projectid, comment, isdelete, updatestatus, paybackid, ymd) VALUES (%d, %d, %d, %d, 1, '', 0, 1, %d, %d)"
            r4 = 6
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x020e }
            r5 = 0
            java.lang.Long r6 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x020e }
            r4[r5] = r6     // Catch:{ all -> 0x020e }
            r5 = 1
            long r6 = com.wacai.data.d.i     // Catch:{ all -> 0x020e }
            r8 = 0
            int r6 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r6 <= 0) goto L_0x02ba
            long r6 = com.wacai.data.d.i     // Catch:{ all -> 0x020e }
        L_0x0256:
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x020e }
            r4[r5] = r6     // Catch:{ all -> 0x020e }
            r5 = 2
            long r6 = r12.a     // Catch:{ all -> 0x020e }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x020e }
            r4[r5] = r6     // Catch:{ all -> 0x020e }
            r5 = 3
            long r6 = r12.f     // Catch:{ all -> 0x020e }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x020e }
            r4[r5] = r6     // Catch:{ all -> 0x020e }
            r5 = 4
            long r6 = r12.x()     // Catch:{ all -> 0x020e }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x020e }
            r4[r5] = r6     // Catch:{ all -> 0x020e }
            r5 = 5
            long r6 = r12.a     // Catch:{ all -> 0x020e }
            r8 = 1000(0x3e8, double:4.94E-321)
            long r6 = r6 * r8
            long r6 = com.wacai.a.a.a(r6)     // Catch:{ all -> 0x020e }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x020e }
            r4[r5] = r6     // Catch:{ all -> 0x020e }
            java.lang.String r3 = java.lang.String.format(r3, r4)     // Catch:{ all -> 0x020e }
            r0.execSQL(r3)     // Catch:{ all -> 0x020e }
            java.lang.String r3 = "TBL_OUTGOINFO"
            int r3 = com.wacai.data.aa.d(r3)     // Catch:{ all -> 0x020e }
            java.lang.String r4 = "INSERT INTO TBL_OUTGOMEMBERINFO (outgoid, memberid, sharemoney) VALUES (%d, %d, %d)"
            r5 = 3
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x020e }
            r6 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x020e }
            r5[r6] = r3     // Catch:{ all -> 0x020e }
            r3 = 1
            r6 = 1
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x020e }
            r5[r3] = r6     // Catch:{ all -> 0x020e }
            r3 = 2
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x020e }
            r5[r3] = r1     // Catch:{ all -> 0x020e }
            java.lang.String r1 = java.lang.String.format(r4, r5)     // Catch:{ all -> 0x020e }
            r0.execSQL(r1)     // Catch:{ all -> 0x020e }
            goto L_0x016c
        L_0x02ba:
            java.lang.String r6 = "TBL_OUTGOSUBTYPEINFO"
            java.lang.String r7 = "id"
            java.lang.String r8 = "1610"
            r9 = 70009(0x11179, double:3.4589E-319)
            long r6 = com.wacai.data.aa.a(r6, r7, r8, r9)     // Catch:{ all -> 0x020e }
            com.wacai.data.d.i = r6     // Catch:{ all -> 0x020e }
            goto L_0x0256
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.d.c():void");
    }

    public final void c(long j) {
        this.c = j;
    }

    public final long d() {
        return this.c;
    }

    public final void d(long j) {
        this.f = j;
    }

    public final int e() {
        return this.d;
    }

    public final void e(long j) {
        this.g = j;
    }

    public final void f() {
        if (x() > 0) {
            try {
                e.c().b().beginTransaction();
                if (B() == null || B().length() <= 0) {
                    a("TBL_PAYBACK", x());
                } else {
                    this.e = true;
                    f(false);
                    c();
                }
                j();
                e.c().b().setTransactionSuccessful();
            } finally {
                e.c().b().endTransaction();
            }
        }
    }

    public final long g() {
        return this.f;
    }

    public final long h() {
        return this.g;
    }

    public final String i() {
        return this.h;
    }
}
