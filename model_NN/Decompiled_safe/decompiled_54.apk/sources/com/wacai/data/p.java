package com.wacai.data;

import org.w3c.dom.Element;

public final class p extends k {
    public p() {
        super("TBL_OUTGOMAINTYPEINFO");
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00b6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.p a(long r10) {
        /*
            r7 = 0
            r6 = 1
            r5 = 0
            r4 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select * from TBL_OUTGOMAINTYPEINFO where id = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r10)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x00a9, all -> 0x00b2 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x00a9, all -> 0x00b2 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x00a9, all -> 0x00b2 }
            if (r0 == 0) goto L_0x002d
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            if (r1 > 0) goto L_0x0034
        L_0x002d:
            if (r0 == 0) goto L_0x0032
            r0.close()
        L_0x0032:
            r0 = r4
        L_0x0033:
            return r0
        L_0x0034:
            r0.moveToFirst()     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            com.wacai.data.p r1 = new com.wacai.data.p     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            r1.<init>()     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            r1.k(r10)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            java.lang.String r2 = "name"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            r1.b(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            java.lang.String r2 = "enable"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00a3
            r2 = r6
        L_0x005b:
            r1.d(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            java.lang.String r2 = "isdefault"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00a5
            r2 = r6
        L_0x006d:
            r1.e(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            java.lang.String r2 = "uuid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            r1.g(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            java.lang.String r2 = "orderno"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            r1.f(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            java.lang.String r2 = "updatestatus"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00a7
            r2 = r6
        L_0x0099:
            r1.f(r2)     // Catch:{ Exception -> 0x00bf, all -> 0x00ba }
            if (r0 == 0) goto L_0x00a1
            r0.close()
        L_0x00a1:
            r0 = r1
            goto L_0x0033
        L_0x00a3:
            r2 = r5
            goto L_0x005b
        L_0x00a5:
            r2 = r5
            goto L_0x006d
        L_0x00a7:
            r2 = r5
            goto L_0x0099
        L_0x00a9:
            r0 = move-exception
            r0 = r4
        L_0x00ab:
            if (r0 == 0) goto L_0x00b0
            r0.close()
        L_0x00b0:
            r0 = r4
            goto L_0x0033
        L_0x00b2:
            r0 = move-exception
            r1 = r4
        L_0x00b4:
            if (r1 == 0) goto L_0x00b9
            r1.close()
        L_0x00b9:
            throw r0
        L_0x00ba:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00b4
        L_0x00bf:
            r1 = move-exception
            goto L_0x00ab
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.p.a(long):com.wacai.data.p");
    }

    public static p a(Element element, boolean z) {
        if (element == null) {
            return null;
        }
        return (p) ab.a(element, new p(), z);
    }

    public static String b(long j) {
        return aa.a("TBL_OUTGOMAINTYPEINFO", "name", (int) j);
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str != null && str2 != null) {
            if (str.equalsIgnoreCase("r")) {
                g(str2);
            } else if (str.equalsIgnoreCase("s")) {
                b(str2);
            } else if (str.equalsIgnoreCase("w")) {
                f(Long.parseLong(str2));
            } else if (str.equalsIgnoreCase("u")) {
                d(Integer.parseInt(str2) > 0);
            } else if (str.equalsIgnoreCase("v")) {
                e(Integer.parseInt(str2) > 0);
            }
        }
    }

    public final void a(StringBuffer stringBuffer) {
        stringBuffer.append("<d><r>");
        stringBuffer.append(B());
        stringBuffer.append("</r><s>");
        stringBuffer.append(h(j()));
        stringBuffer.append("</s><w>");
        stringBuffer.append(k());
        stringBuffer.append("</w><u>");
        stringBuffer.append(l() ? 1 : 0);
        stringBuffer.append("</u></d>");
    }
}
