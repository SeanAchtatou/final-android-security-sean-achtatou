package com.wacai.data;

import android.util.Log;
import org.w3c.dom.Element;

public final class e extends k {
    private String a = "";
    private String b = "";

    public e() {
        super("TBL_MONEYTYPE");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab
     arg types: [org.w3c.dom.Element, com.wacai.data.e, int]
     candidates:
      com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int
      com.wacai.data.aa.a(java.lang.String, java.lang.String, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab */
    public static e a(Element element) {
        if (element == null) {
            return null;
        }
        return (e) ab.a(element, (ab) new e(), false);
    }

    public static String a(long j) {
        return aa.a("TBL_MONEYTYPE", "name", (int) j);
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str != null && str2 != null) {
            try {
                if (str.equalsIgnoreCase("r")) {
                    g(str2);
                } else if (str.equalsIgnoreCase("s")) {
                    b(str2);
                } else if (str.equalsIgnoreCase("w")) {
                    f(Long.parseLong(str2));
                } else if (str.equalsIgnoreCase("az")) {
                    this.a = str2;
                } else if (str.equalsIgnoreCase("ba")) {
                    this.b = str2;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public final void a(StringBuffer stringBuffer) {
    }

    public final void c() {
        String format;
        if (j() == null || j().length() <= 0) {
            Log.e("WAC_MoneyType", "Invalide name when save data");
        } else if (this.a == null || this.a.length() <= 0) {
            Log.e("WAC_MoneyType", "Invalide shortname when save data");
        } else if (this.b == null || this.b.length() <= 0) {
            Log.e("WAC_MoneyType", "Invalide flag when save data");
        } else {
            if (C()) {
                format = String.format("UPDATE %s SET name = '%s', shortname = '%s', flag = '%s', uuid = '%s', orderno = %d WHERE id = %d", w(), e(j()), e(this.a), e(this.b), B(), Long.valueOf(k()), Long.valueOf(x()));
            } else {
                format = String.format("INSERT INTO %s (name, shortname, flag, uuid, orderno) VALUES ('%s', '%s', '%s', '%s', %d)", w(), e(j()), e(this.a), e(this.b), B(), Long.valueOf(k()));
            }
            com.wacai.e.c().b().execSQL(format);
            if (x() <= 0) {
                k((long) d(w()));
            }
        }
    }
}
