package com.wacai.data;

import android.database.Cursor;
import com.wacai.a.g;
import com.wacai.e;
import org.w3c.dom.Element;

public final class j extends k {
    public j() {
        super("TBL_TRADETARGET");
    }

    public static int a(StringBuffer stringBuffer, boolean z) {
        Cursor cursor;
        if (stringBuffer == null) {
            return 0;
        }
        try {
            Cursor rawQuery = e.c().b().rawQuery(z ? "select * from TBL_TRADETARGET where updatestatus = 0 AND (uuid IS NULL OR uuid = '')" : "select * from TBL_TRADETARGET where updatestatus = 0 AND (uuid IS NOT NULL AND uuid != '')", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        int count = rawQuery.getCount();
                        j jVar = new j();
                        for (int i = 0; i < count; i++) {
                            jVar.b(rawQuery.getString(rawQuery.getColumnIndexOrThrow("name")));
                            jVar.d(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("enable")) > 0);
                            String string = rawQuery.getString(rawQuery.getColumnIndexOrThrow("uuid"));
                            if (string == null) {
                                string = "";
                            }
                            jVar.g(string);
                            jVar.f(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("orderno")));
                            jVar.a(stringBuffer);
                            rawQuery.moveToNext();
                        }
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return count;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return 0;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.j a(long r10) {
        /*
            r8 = 1
            r7 = 0
            r5 = 0
            r4 = 0
            int r0 = (r10 > r5 ? 1 : (r10 == r5 ? 0 : -1))
            if (r0 > 0) goto L_0x000b
            r0 = r4
        L_0x000a:
            return r0
        L_0x000b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select * from TBL_TRADETARGET where id = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r10)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x0099, all -> 0x00a3 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x0099, all -> 0x00a3 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x0099, all -> 0x00a3 }
            if (r0 == 0) goto L_0x0033
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            if (r1 != 0) goto L_0x003a
        L_0x0033:
            if (r0 == 0) goto L_0x0038
            r0.close()
        L_0x0038:
            r0 = r4
            goto L_0x000a
        L_0x003a:
            com.wacai.data.j r1 = new com.wacai.data.j     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            r1.<init>()     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            r1.k(r10)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            java.lang.String r2 = "name"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            r1.b(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            java.lang.String r2 = "enable"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            int r2 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r2 <= 0) goto L_0x0095
            r2 = r8
        L_0x005e:
            r1.d(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            java.lang.String r2 = "uuid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            r1.g(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            java.lang.String r2 = "orderno"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            r1.f(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            java.lang.String r2 = "updatestatus"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            int r2 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r2 <= 0) goto L_0x0097
            r2 = r8
        L_0x008a:
            r1.f(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            if (r0 == 0) goto L_0x0092
            r0.close()
        L_0x0092:
            r0 = r1
            goto L_0x000a
        L_0x0095:
            r2 = r7
            goto L_0x005e
        L_0x0097:
            r2 = r7
            goto L_0x008a
        L_0x0099:
            r0 = move-exception
            r0 = r4
        L_0x009b:
            if (r0 == 0) goto L_0x00a0
            r0.close()
        L_0x00a0:
            r0 = r4
            goto L_0x000a
        L_0x00a3:
            r0 = move-exception
            r1 = r4
        L_0x00a5:
            if (r1 == 0) goto L_0x00aa
            r1.close()
        L_0x00aa:
            throw r0
        L_0x00ab:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00a5
        L_0x00b0:
            r1 = move-exception
            goto L_0x009b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.j.a(long):com.wacai.data.j");
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00cc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.j a(java.lang.String r10) {
        /*
            r7 = 0
            r6 = 1
            r5 = 0
            r4 = 0
            if (r10 == 0) goto L_0x000d
            int r0 = r10.length()
            if (r0 > 0) goto L_0x000f
        L_0x000d:
            r0 = r4
        L_0x000e:
            return r0
        L_0x000f:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00be, all -> 0x00c8 }
            r0.<init>()     // Catch:{ Exception -> 0x00be, all -> 0x00c8 }
            java.lang.String r1 = "select * from TBL_TRADETARGET where LOWER(name) = LOWER('"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00be, all -> 0x00c8 }
            java.lang.String r1 = e(r10)     // Catch:{ Exception -> 0x00be, all -> 0x00c8 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00be, all -> 0x00c8 }
            java.lang.String r1 = "')"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00be, all -> 0x00c8 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00be, all -> 0x00c8 }
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x00be, all -> 0x00c8 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x00be, all -> 0x00c8 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x00be, all -> 0x00c8 }
            if (r0 == 0) goto L_0x0041
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            if (r1 != 0) goto L_0x0048
        L_0x0041:
            if (r0 == 0) goto L_0x0046
            r0.close()
        L_0x0046:
            r0 = r4
            goto L_0x000e
        L_0x0048:
            com.wacai.data.j r1 = new com.wacai.data.j     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            r1.<init>()     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
        L_0x004d:
            java.lang.String r2 = "id"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            long r2 = (long) r2     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            r1.k(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            java.lang.String r2 = "name"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            r1.b(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            java.lang.String r2 = "enable"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00ba
            r2 = r6
        L_0x0077:
            r1.d(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            java.lang.String r2 = "uuid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            r1.g(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            java.lang.String r2 = "orderno"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            r1.f(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            java.lang.String r2 = "updatestatus"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00bc
            r2 = r6
        L_0x00a3:
            r1.f(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            boolean r2 = r1.l()     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            if (r2 != 0) goto L_0x00b2
            boolean r2 = r0.moveToNext()     // Catch:{ Exception -> 0x00d5, all -> 0x00d0 }
            if (r2 != 0) goto L_0x004d
        L_0x00b2:
            if (r0 == 0) goto L_0x00b7
            r0.close()
        L_0x00b7:
            r0 = r1
            goto L_0x000e
        L_0x00ba:
            r2 = r5
            goto L_0x0077
        L_0x00bc:
            r2 = r5
            goto L_0x00a3
        L_0x00be:
            r0 = move-exception
            r0 = r4
        L_0x00c0:
            if (r0 == 0) goto L_0x00c5
            r0.close()
        L_0x00c5:
            r0 = r4
            goto L_0x000e
        L_0x00c8:
            r0 = move-exception
            r1 = r4
        L_0x00ca:
            if (r1 == 0) goto L_0x00cf
            r1.close()
        L_0x00cf:
            throw r0
        L_0x00d0:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00ca
        L_0x00d5:
            r1 = move-exception
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.j.a(java.lang.String):com.wacai.data.j");
    }

    public static j a(Element element, boolean z) {
        if (element == null) {
            return null;
        }
        return (j) ab.a(element, new j(), z);
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0091  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(java.lang.String r8, java.lang.String r9) {
        /*
            r7 = this;
            r3 = 0
            r5 = 0
            if (r8 == 0) goto L_0x0012
            int r0 = r8.length()
            if (r0 <= 0) goto L_0x0012
            if (r9 == 0) goto L_0x0012
            int r0 = r9.length()
            if (r0 > 0) goto L_0x0014
        L_0x0012:
            r0 = r5
        L_0x0013:
            return r0
        L_0x0014:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0068, all -> 0x008d }
            java.lang.String r1 = "select count(*) from "
            r0.<init>(r1)     // Catch:{ Exception -> 0x0068, all -> 0x008d }
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ Exception -> 0x0068, all -> 0x008d }
            java.lang.String r1 = " where "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0068, all -> 0x008d }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ Exception -> 0x0068, all -> 0x008d }
            java.lang.String r1 = " = "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0068, all -> 0x008d }
            long r1 = r7.x()     // Catch:{ Exception -> 0x0068, all -> 0x008d }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0068, all -> 0x008d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0068, all -> 0x008d }
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x0068, all -> 0x008d }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x0068, all -> 0x008d }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x0068, all -> 0x008d }
            if (r0 == 0) goto L_0x0061
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x009c, all -> 0x0095 }
            if (r1 == 0) goto L_0x0061
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ Exception -> 0x009c, all -> 0x0095 }
            if (r1 <= 0) goto L_0x005f
            r1 = 1
        L_0x0058:
            if (r0 == 0) goto L_0x005d
            r0.close()
        L_0x005d:
            r0 = r1
            goto L_0x0013
        L_0x005f:
            r1 = r5
            goto L_0x0058
        L_0x0061:
            if (r0 == 0) goto L_0x0066
            r0.close()
        L_0x0066:
            r0 = r5
            goto L_0x0013
        L_0x0068:
            r0 = move-exception
            r1 = r3
        L_0x006a:
            java.lang.String r2 = "TradeTarget"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x009a }
            r3.<init>()     // Catch:{ all -> 0x009a }
            java.lang.String r4 = "isReferredInTable e="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x009a }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x009a }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x009a }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x009a }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x009a }
            if (r1 == 0) goto L_0x008b
            r1.close()
        L_0x008b:
            r0 = r5
            goto L_0x0013
        L_0x008d:
            r0 = move-exception
            r1 = r3
        L_0x008f:
            if (r1 == 0) goto L_0x0094
            r1.close()
        L_0x0094:
            throw r0
        L_0x0095:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x008f
        L_0x009a:
            r0 = move-exception
            goto L_0x008f
        L_0x009c:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.j.b(java.lang.String, java.lang.String):boolean");
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str != null && str2 != null) {
            if (str.equalsIgnoreCase("r")) {
                g(str2);
            } else if (str.equalsIgnoreCase("s")) {
                b(str2);
            } else if (str.equalsIgnoreCase("w")) {
                f(Long.parseLong(str2));
            } else if (str.equalsIgnoreCase("u")) {
                d(Integer.parseInt(str2) > 0);
            }
        }
    }

    public final void a(StringBuffer stringBuffer) {
        stringBuffer.append("<g><r>");
        stringBuffer.append(B());
        stringBuffer.append("</r><s>");
        stringBuffer.append(h(j()));
        stringBuffer.append("</s><w>");
        stringBuffer.append(k());
        stringBuffer.append("</w><u>");
        stringBuffer.append(l() ? 1 : 0);
        stringBuffer.append("</u></g>");
    }

    public final boolean a() {
        if (x() <= 0) {
            return false;
        }
        if (B() == null || B().length() <= 0) {
            return b("TBL_OUTGOINFO", "targetid") || b("TBL_INCOMEINFO", "targetid") || b("TBL_SCHEDULEOUTGOINFO", "targetid") || b("TBL_SCHEDULEINCOMEINFO", "targetid");
        }
        return true;
    }

    public final void c() {
        if (j() != null && j().length() > 0) {
            if (C()) {
                Object[] objArr = new Object[8];
                objArr[0] = w();
                objArr[1] = e(j());
                objArr[2] = e(g.a(j()));
                objArr[3] = Integer.valueOf(l() ? 1 : 0);
                objArr[4] = B();
                objArr[5] = Long.valueOf(k());
                objArr[6] = Integer.valueOf(A() ? 1 : 0);
                objArr[7] = Long.valueOf(x());
                e.c().b().execSQL(String.format("UPDATE %s SET name = '%s', pinyin = '%s', enable = %d, uuid = '%s', orderno = %d, updatestatus = %d WHERE id = %d", objArr));
                return;
            }
            Object[] objArr2 = new Object[7];
            objArr2[0] = w();
            objArr2[1] = B();
            objArr2[2] = e(j());
            objArr2[3] = e(g.a(j()));
            objArr2[4] = Integer.valueOf(l() ? 1 : 0);
            objArr2[5] = Long.valueOf(k());
            objArr2[6] = Integer.valueOf(A() ? 1 : 0);
            e.c().b().execSQL(String.format("INSERT INTO %s (uuid, name, pinyin, enable, orderno, updatestatus) VALUES ('%s', '%s', '%s', %d, %d, %d)", objArr2));
            k((long) d(w()));
        }
    }

    public final void f() {
        if (x() > 0) {
            if (a()) {
                d(false);
                f(false);
                c();
                return;
            }
            a("TBL_TRADETARGET", x());
        }
    }
}
