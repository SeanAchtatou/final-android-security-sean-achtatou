package com.wacai.data;

import android.database.Cursor;
import android.util.Log;
import com.wacai.e;

public abstract class aa {
    private String a;
    private long b;

    protected aa(String str) {
        this.a = str;
    }

    private static int a(StringBuffer stringBuffer, int i, boolean z) {
        int i2;
        int i3 = 0;
        if (stringBuffer == null) {
            return 0;
        }
        int indexOf = stringBuffer.indexOf(".");
        if (indexOf < 0) {
            return stringBuffer.length();
        }
        if (i > 0) {
            stringBuffer.delete(indexOf + i + 1, stringBuffer.length());
        }
        int length = stringBuffer.length();
        int i4 = length - 1;
        while (true) {
            if (!z || i4 < indexOf) {
                break;
            }
            char charAt = stringBuffer.charAt(i4);
            if (charAt == '0') {
                i4--;
                i3++;
            } else if (charAt == '.') {
                i2 = i3 + 1;
            }
        }
        i2 = i3;
        return length - i2;
    }

    public static long a(double d) {
        return d >= 0.0d ? (long) ((d * 100.0d) + 0.5d) : (long) ((d * 100.0d) - 0.5d);
    }

    public static long a(String str, String str2, int i, long j) {
        Cursor cursor;
        if (str == null || str.length() <= 0 || str2 == null || str2.length() <= 0 || i <= 0) {
            return j;
        }
        try {
            Cursor rawQuery = e.c().b().rawQuery("select " + str2 + " from " + str + " where id = " + i, null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        long j2 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow(str2));
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return j2;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return j;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    public static long a(String str, String str2, String str3, long j) {
        Cursor cursor;
        if (str == null || str.length() <= 0 || str2 == null || str2.length() <= 0 || str3 == null || str3.length() <= 0) {
            return j;
        }
        try {
            Cursor rawQuery = e.c().b().rawQuery("select " + str2 + " from " + str + " where uuid = '" + str3 + "'", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        long j2 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow(str2));
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return j2;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return j;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuffer.insert(int, char):java.lang.StringBuffer}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuffer.insert(int, float):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, boolean):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, double):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.Object):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, long):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, int):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.CharSequence):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.String):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, char[]):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, char):java.lang.StringBuffer} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int
     arg types: [java.lang.StringBuffer, int, int]
     candidates:
      com.wacai.data.aa.a(java.lang.String, java.lang.String, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int */
    public static String a(double d, int i) {
        boolean z;
        double d2;
        StringBuffer stringBuffer;
        int i2;
        if (d == 0.0d) {
            return "0.00";
        }
        if (d < 0.0d) {
            d2 = -1.0d * d;
            z = true;
        } else {
            z = true;
            d2 = d;
        }
        if (i > 0) {
            d2 += 0.5d * Math.pow(0.1d, (double) i);
        }
        StringBuffer stringBuffer2 = new StringBuffer(64);
        stringBuffer2.append(d2);
        int indexOf = stringBuffer2.indexOf("E");
        if (indexOf >= 0) {
            StringBuffer stringBuffer3 = new StringBuffer(64);
            String d3 = Double.toString(d2);
            int parseInt = Integer.parseInt(d3.substring(indexOf + 1));
            if (parseInt < 0) {
                return "0.00";
            }
            int indexOf2 = d3.indexOf(".");
            stringBuffer3.append(d3.substring(0, indexOf));
            if (indexOf2 >= 0) {
                stringBuffer3.deleteCharAt(indexOf2);
                i2 = (indexOf - (indexOf2 + 1)) - parseInt;
            } else {
                i2 = parseInt;
            }
            if (i2 > 0) {
                stringBuffer3.insert(stringBuffer3.length() - i2, '.');
            } else {
                int i3 = i2 * -1;
                for (int i4 = 0; i4 < i3; i4++) {
                    stringBuffer3.append("0");
                }
            }
            stringBuffer = stringBuffer3;
        } else {
            stringBuffer = stringBuffer2;
        }
        if (z) {
            stringBuffer.insert(0, "-");
        }
        return stringBuffer.substring(0, a(stringBuffer, i, false));
    }

    private static String a(String str, String str2) {
        Cursor cursor;
        if (str == null || str.length() <= 0 || str2 == null || str2.length() <= 0) {
            return "";
        }
        try {
            Cursor rawQuery = e.c().b().rawQuery("select " + str2 + " from " + str + " where enable = 1 order by orderno ASC", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        String string = rawQuery.getString(rawQuery.getColumnIndexOrThrow(str2));
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return string;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return "";
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    public static String a(String str, String str2, int i) {
        Cursor cursor;
        if (str == null || str.length() <= 0 || str2 == null || str2.length() <= 0 || i <= 0) {
            return "";
        }
        try {
            Cursor rawQuery = e.c().b().rawQuery("select " + str2 + " from " + str + " where id = " + i, null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        String string = rawQuery.getString(rawQuery.getColumnIndexOrThrow(str2));
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return string;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return "";
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    public static String a(String str, String str2, String str3) {
        Cursor cursor;
        if (str == null || str.length() <= 0 || str2 == null || str2.length() <= 0 || str3 == null || str3.length() <= 0) {
            return "";
        }
        try {
            Cursor rawQuery = e.c().b().rawQuery("select " + str2 + " from " + str + " where uuid = '" + str3 + "'", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        String string = rawQuery.getString(rawQuery.getColumnIndexOrThrow(str2));
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return string;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return "";
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    public static void a(String str, long j) {
        if (str != null && str.length() > 0 && j > 0) {
            e.c().b().execSQL("DELETE FROM " + str + " WHERE id = " + j);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean b(java.lang.String r6, long r7) {
        /*
            r4 = 0
            r3 = 0
            r0 = 0
            int r0 = (r7 > r0 ? 1 : (r7 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x0010
            if (r6 == 0) goto L_0x0010
            int r0 = r6.length()
            if (r0 > 0) goto L_0x0012
        L_0x0010:
            r0 = r3
        L_0x0011:
            return r0
        L_0x0012:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x004e }
            r0.<init>()     // Catch:{ all -> 0x004e }
            java.lang.String r1 = "select * from "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x004e }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ all -> 0x004e }
            java.lang.String r1 = " where id = "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x004e }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ all -> 0x004e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x004e }
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ all -> 0x004e }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ all -> 0x004e }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ all -> 0x004e }
            if (r0 == 0) goto L_0x004c
            int r1 = r0.getCount()     // Catch:{ all -> 0x0056 }
            if (r1 <= 0) goto L_0x004c
            r1 = 1
        L_0x0045:
            if (r0 == 0) goto L_0x004a
            r0.close()
        L_0x004a:
            r0 = r1
            goto L_0x0011
        L_0x004c:
            r1 = r3
            goto L_0x0045
        L_0x004e:
            r0 = move-exception
            r1 = r4
        L_0x0050:
            if (r1 == 0) goto L_0x0055
            r1.close()
        L_0x0055:
            throw r0
        L_0x0056:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.aa.b(java.lang.String, long):boolean");
    }

    public static int d(String str) {
        Cursor cursor;
        if (str == null || str.length() <= 0) {
            return -1;
        }
        try {
            Cursor rawQuery = e.c().b().rawQuery("select max(id) from " + str, null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToNext()) {
                        int i = (int) rawQuery.getLong(0);
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return i;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return -1;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    public static String e(String str) {
        return str == null ? "" : str.replace("'", "''");
    }

    public static long f(String str) {
        String a2 = a(str, "id");
        if (a2 == null || a2.length() == 0) {
            return 0;
        }
        return Long.parseLong(a2);
    }

    public static double l(long j) {
        return Double.valueOf(m(j).toString()).doubleValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int
     arg types: [java.lang.StringBuffer, int, int]
     candidates:
      com.wacai.data.aa.a(java.lang.String, java.lang.String, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int */
    public static String m(long j) {
        StringBuffer stringBuffer = new StringBuffer(20);
        stringBuffer.append(j);
        int length = stringBuffer.length();
        for (int i = 3 - length; i > 0 && length <= 2; i--) {
            stringBuffer.insert(0, "0");
        }
        stringBuffer.insert(stringBuffer.length() - 2, ".");
        return stringBuffer.substring(0, a(stringBuffer, 2, false));
    }

    public static long y() {
        Cursor cursor;
        try {
            Cursor rawQuery = e.c().b().rawQuery("select a.id as _id from TBL_OUTGOSUBTYPEINFO a, TBL_OUTGOMAINTYPEINFO b where a.id / 10000 = b.id and a.enable = 1 and b.enable = 1 order by a.id ASC", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        long j = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_id"));
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return j;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return -1;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    public abstract void c();

    public void f() {
        Log.e("WacaiData", "Abstract Class not support delete!");
    }

    public final void k(long j) {
        this.b = j;
    }

    public final String w() {
        return this.a;
    }

    public final long x() {
        return this.b;
    }
}
