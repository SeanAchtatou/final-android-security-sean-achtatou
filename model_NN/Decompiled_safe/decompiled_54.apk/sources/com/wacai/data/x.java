package com.wacai.data;

import android.database.Cursor;
import android.util.Log;
import com.wacai.e;
import java.util.ArrayList;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class x extends aa {
    private s a;
    private long b;
    private long c;

    public x(s sVar) {
        this(sVar.i());
        this.a = sVar;
    }

    public x(s sVar, long j, long j2) {
        this(sVar);
        this.b = j;
        this.c = j2;
    }

    private x(String str) {
        super(str);
        this.b = aa.f("TBL_MEMBERINFO");
    }

    public static x a(Element element, s sVar) {
        if (element == null || sVar == null) {
            return null;
        }
        NodeList childNodes = element.getChildNodes();
        if (childNodes == null || childNodes.getLength() == 0) {
            return null;
        }
        try {
            x xVar = new x(sVar);
            int length = childNodes.getLength();
            for (int i = 0; i < length; i++) {
                Node item = childNodes.item(i);
                if (!(item == null || item.getNodeName() == null || !Element.class.isInstance(item) || item.getFirstChild() == null)) {
                    String nodeName = item.getNodeName();
                    String a2 = ab.a(item);
                    if (!(nodeName == null || a2 == null)) {
                        if (nodeName.equalsIgnoreCase("ao")) {
                            xVar.b = aa.a("TBL_MEMBERINFO", "id", a2, 1);
                        } else if (nodeName.equalsIgnoreCase("ab")) {
                            xVar.c = aa.a(Double.parseDouble(a2));
                        }
                    }
                }
            }
            return xVar;
        } catch (Exception e) {
            return null;
        }
    }

    static void a(s sVar) {
        Cursor cursor;
        if (sVar != null) {
            sVar.s().clear();
            try {
                Cursor rawQuery = e.c().b().rawQuery(String.format("select * from %s where %s = %d", sVar.i(), sVar.h(), Long.valueOf(sVar.x())), null);
                if (rawQuery != null) {
                    try {
                        rawQuery.moveToFirst();
                        int count = rawQuery.getCount();
                        for (int i = 0; i < count; i++) {
                            x xVar = new x(sVar);
                            xVar.k(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("id")));
                            xVar.b = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("memberid"));
                            xVar.c = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("sharemoney"));
                            sVar.s().add(xVar);
                            rawQuery.moveToNext();
                        }
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        cursor = rawQuery;
                        th = th2;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (Throwable th3) {
                th = th3;
                cursor = null;
            }
        }
    }

    static void a(s sVar, int i, StringBuffer stringBuffer) {
        Cursor cursor;
        if (sVar != null && stringBuffer != null && i > 0) {
            try {
                Cursor rawQuery = e.c().b().rawQuery(String.format("select b.uuid as memuuid, a.sharemoney as sharemoney from %s a, TBL_MEMBERINFO b where a.%s = %d AND a.memberid = b.id", sVar.i(), sVar.h(), Integer.valueOf(i)), null);
                if (rawQuery != null) {
                    try {
                        if (rawQuery.moveToFirst()) {
                            int count = rawQuery.getCount();
                            for (int i2 = 0; i2 < count; i2++) {
                                stringBuffer.append("<an><ao>");
                                stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("memuuid")));
                                stringBuffer.append("</ao><ab>");
                                stringBuffer.append(a(l(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("sharemoney"))), 2));
                                stringBuffer.append("</ab></an>");
                                rawQuery.moveToNext();
                            }
                            if (rawQuery != null) {
                                rawQuery.close();
                                return;
                            }
                            return;
                        }
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        cursor = rawQuery;
                        th = th2;
                    }
                }
                if (rawQuery != null) {
                    rawQuery.close();
                    return;
                }
                return;
            } catch (Throwable th3) {
                th = th3;
                cursor = null;
            }
        } else {
            return;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    public static boolean a(ArrayList arrayList) {
        if (arrayList == null || arrayList.isEmpty()) {
            return false;
        }
        x xVar = (x) arrayList.get(0);
        return xVar != null && 0 < xVar.b;
    }

    public final long a() {
        return this.b;
    }

    public final void a(long j) {
        this.b = j;
    }

    public final long b() {
        return this.c;
    }

    public final void b(long j) {
        this.c = j;
    }

    public final void c() {
        if (this.a == null) {
            Log.e("WAC_MemberShareInfo", "no parent when save data");
        } else if (!b("TBL_MEMBERINFO", this.b)) {
            Log.e("WAC_MemberShareInfo", "Invalide member when save sharedata");
        } else {
            e.c().b().execSQL(String.format("INSERT INTO %s (%s, memberid, sharemoney) VALUES (%d, %d, %d)", w(), this.a.h(), Long.valueOf(this.a.x()), Long.valueOf(this.b), Long.valueOf(this.c)));
            k((long) d(w()));
        }
    }
}
