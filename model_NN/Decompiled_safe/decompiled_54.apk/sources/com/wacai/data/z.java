package com.wacai.data;

import android.database.Cursor;
import android.util.Log;
import com.wacai.a.a;
import com.wacai.b;
import com.wacai.e;
import java.util.Date;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class z extends s {
    private long a;
    private long b;
    private long c;
    private String d;
    private long e;
    private long f;

    public z() {
        this(true);
    }

    private z(boolean z) {
        super("TBL_OUTGOINFO", z);
        this.c = 0;
        if (z) {
            this.a = y.y();
            this.b = new Date().getTime() / 1000;
        } else {
            this.a = 10001;
        }
        this.d = "";
        this.e = 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.b.a(long, long, boolean):long
     arg types: [long, long, int]
     candidates:
      com.wacai.b.a(long, long, long):void
      com.wacai.b.a(long, long, boolean):long */
    public static int a(long j, long j2, StringBuffer stringBuffer) {
        Cursor cursor;
        try {
            Cursor rawQuery = e.c().b().rawQuery("select TBL_OUTGOINFO.id as id, TBL_OUTGOINFO.uuid as uuid, TBL_OUTGOINFO.money as money, TBL_OUTGOSUBTYPEINFO.uuid as stuuid, TBL_OUTGOINFO.outgodate as dt, TBL_ACCOUNTINFO.uuid as accuuid, TBL_PROJECTINFO.uuid as prjuuid, TBL_OUTGOINFO.targetid as tgtid, TBL_OUTGOINFO.reimburse as reimburse, TBL_OUTGOINFO.comment as comment, TBL_OUTGOINFO.isdelete as isdelete from TBL_OUTGOINFO, TBL_OUTGOSUBTYPEINFO, TBL_PROJECTINFO, TBL_ACCOUNTINFO where TBL_OUTGOINFO.uuid IS NOT NULL AND TBL_OUTGOINFO.uuid <> ''  AND TBL_OUTGOINFO.paybackid=0 AND TBL_OUTGOINFO.ymd >= " + ((10000 * j) + (100 * j2)) + " AND TBL_OUTGOINFO.ymd < " + ((10000 * j) + (100 * j2) + 99) + " AND TBL_OUTGOINFO.updatestatus = 0 AND TBL_OUTGOINFO.scheduleoutgoid = 0 " + " AND TBL_OUTGOSUBTYPEINFO.id = TBL_OUTGOINFO.subtypeid AND " + "TBL_PROJECTINFO.id = TBL_OUTGOINFO.projectid AND " + "TBL_ACCOUNTINFO.id = TBL_OUTGOINFO.accountid " + "GROUP BY TBL_OUTGOINFO.id", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        int count = rawQuery.getCount();
                        long a2 = count > 0 ? b.a(j, j2, false) : 0;
                        z zVar = new z(false);
                        for (int i = 0; i < count; i++) {
                            stringBuffer.append("<o><r>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("uuid")));
                            stringBuffer.append("</r><ab>");
                            stringBuffer.append(a(l(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("money"))), 2));
                            stringBuffer.append("</ab><ac>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("stuuid")));
                            stringBuffer.append("</ac><ap>");
                            stringBuffer.append(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("dt")));
                            stringBuffer.append("</ap><af>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("accuuid")));
                            stringBuffer.append("</af><ag>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("prjuuid")));
                            stringBuffer.append("</ag><ah>");
                            long j3 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("tgtid"));
                            if (j3 > 0) {
                                stringBuffer.append(aa.a("TBL_TRADETARGET", "uuid", (int) j3));
                            }
                            stringBuffer.append("</ah><ak>");
                            stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("reimburse")));
                            stringBuffer.append("</ak><aq>");
                            stringBuffer.append(h(rawQuery.getString(rawQuery.getColumnIndexOrThrow("comment"))));
                            stringBuffer.append("</aq><al>");
                            stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("isdelete")));
                            stringBuffer.append("</al><am>");
                            x.a(zVar, rawQuery.getInt(rawQuery.getColumnIndexOrThrow("id")), stringBuffer);
                            stringBuffer.append("</am><ar>");
                            stringBuffer.append(a2);
                            stringBuffer.append("</ar></o>");
                            rawQuery.moveToNext();
                        }
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return count;
                    }
                } catch (Throwable th) {
                    th = th;
                    cursor = rawQuery;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return 0;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab
     arg types: [org.w3c.dom.Element, com.wacai.data.z, int]
     candidates:
      com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int
      com.wacai.data.aa.a(java.lang.String, java.lang.String, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab */
    public static z a(Element element) {
        if (element == null) {
            return null;
        }
        try {
            z zVar = (z) ab.a(element, (ab) new z(false), false);
            NodeList elementsByTagName = element.getElementsByTagName("an");
            int length = elementsByTagName.getLength();
            for (int i = 0; i < length; i++) {
                zVar.s().add(x.a((Element) elementsByTagName.item(i), zVar));
            }
            return zVar;
        } catch (Exception e2) {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.z d(long r5) {
        /*
            r3 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select * from TBL_OUTGOINFO where id = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x0046, all -> 0x004f }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x0046, all -> 0x004f }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x0046, all -> 0x004f }
            if (r0 == 0) goto L_0x0029
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            if (r1 != 0) goto L_0x0030
        L_0x0029:
            if (r0 == 0) goto L_0x002e
            r0.close()
        L_0x002e:
            r0 = r3
        L_0x002f:
            return r0
        L_0x0030:
            com.wacai.data.z r1 = new com.wacai.data.z     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            r2 = 0
            r1.<init>(r2)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            r1.k(r5)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            r1.a(r0)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            com.wacai.data.x.a(r1)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            if (r0 == 0) goto L_0x0044
            r0.close()
        L_0x0044:
            r0 = r1
            goto L_0x002f
        L_0x0046:
            r0 = move-exception
            r0 = r3
        L_0x0048:
            if (r0 == 0) goto L_0x004d
            r0.close()
        L_0x004d:
            r0 = r3
            goto L_0x002f
        L_0x004f:
            r0 = move-exception
            r1 = r3
        L_0x0051:
            if (r1 == 0) goto L_0x0056
            r1.close()
        L_0x0056:
            throw r0
        L_0x0057:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0051
        L_0x005c:
            r1 = move-exception
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.z.d(long):com.wacai.data.z");
    }

    public static void e(long j) {
        z d2;
        if (j > 0 && (d2 = d(j)) != null) {
            d2.f();
        }
    }

    public final long a() {
        return this.a;
    }

    public final void a(long j) {
        this.a = j;
    }

    /* access modifiers changed from: protected */
    public final void a(Cursor cursor) {
        if (cursor != null) {
            this.a = cursor.getLong(cursor.getColumnIndexOrThrow("subtypeid"));
            this.b = cursor.getLong(cursor.getColumnIndexOrThrow("outgodate"));
            this.c = cursor.getLong(cursor.getColumnIndexOrThrow("reimburse"));
            this.d = cursor.getString(cursor.getColumnIndexOrThrow("comment"));
            this.e = cursor.getLong(cursor.getColumnIndexOrThrow("scheduleoutgoid"));
            this.f = cursor.getLong(cursor.getColumnIndexOrThrow("paybackid"));
            super.a(cursor);
        }
    }

    public final void a(String str) {
        this.d = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.s.a(java.lang.String, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.wacai.data.aa.a(double, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.aa.a(java.lang.String, long):void
      com.wacai.data.s.a(java.lang.String, java.lang.String):void */
    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str != null && str2 != null) {
            if (str.equalsIgnoreCase("ac")) {
                this.a = aa.a("TBL_OUTGOSUBTYPEINFO", "id", str2, 10001);
            } else if (str.equalsIgnoreCase("ap")) {
                this.b = Long.parseLong(str2);
            } else if (str.equalsIgnoreCase("aq")) {
                this.d = str2;
            } else if (str.equalsIgnoreCase("ak")) {
                this.c = Long.parseLong(str2);
            } else {
                super.a(str, str2);
            }
        }
    }

    public final long b() {
        return this.b;
    }

    public final void b(long j) {
        this.b = j;
    }

    public final void c() {
        if (!b("TBL_OUTGOSUBTYPEINFO", this.a)) {
            Log.e("WAC_Outgo", "Invalide outgo sub type when save data");
        } else if (!b("TBL_ACCOUNTINFO", p())) {
            Log.e("WAC_Outgo", "Invalide account when save data");
        } else if (!b("TBL_PROJECTINFO", q())) {
            Log.e("WAC_Outgo", "Invalide project when save data");
        } else if (t() || u()) {
            v();
            if (!t()) {
                e.c().b().execSQL("UPDATE TBL_OUTGOSUBTYPEINFO SET refcount = refcount+1 WHERE id = " + this.a);
                if (r() > 0) {
                    e.c().b().execSQL("UPDATE TBL_TRADETARGET SET refcount = refcount+1 WHERE id = " + r());
                }
            }
        } else {
            Log.e("WAC_Outgo", "Money conflict with member shares!");
        }
    }

    public final void c(long j) {
        this.c = j;
    }

    public final long d() {
        return this.c;
    }

    public final String e() {
        return this.d;
    }

    public final void f() {
        if (this.f > 0) {
            d.h(this.f);
        } else {
            super.f();
        }
    }

    public final long g() {
        return this.e;
    }

    public final String h() {
        return "outgoid";
    }

    public final String i() {
        return "TBL_OUTGOMEMBERINFO";
    }

    /* access modifiers changed from: protected */
    public final String j() {
        Object[] objArr = new Object[13];
        objArr[0] = w();
        objArr[1] = Long.valueOf(o());
        objArr[2] = Long.valueOf(this.a);
        objArr[3] = Long.valueOf(this.b);
        objArr[4] = Long.valueOf(p());
        objArr[5] = Long.valueOf(q());
        objArr[6] = Long.valueOf(this.c);
        objArr[7] = e(this.d);
        objArr[8] = Integer.valueOf(t() ? 1 : 0);
        objArr[9] = Integer.valueOf(A() ? 1 : 0);
        objArr[10] = B();
        objArr[11] = Long.valueOf(a.a(this.b * 1000));
        objArr[12] = Long.valueOf(r());
        return String.format("INSERT INTO %s (money, subtypeid, outgodate, accountid, projectid, reimburse, comment, isdelete, updatestatus, uuid, ymd, targetid) VALUES (%d, %d, %d, %d, %d, %d, '%s', %d, %d, '%s', %d, %d)", objArr);
    }

    /* access modifiers changed from: protected */
    public final String k() {
        Object[] objArr = new Object[14];
        objArr[0] = w();
        objArr[1] = Long.valueOf(o());
        objArr[2] = Long.valueOf(this.a);
        objArr[3] = Long.valueOf(this.b);
        objArr[4] = Long.valueOf(p());
        objArr[5] = Long.valueOf(q());
        objArr[6] = Long.valueOf(this.c);
        objArr[7] = e(this.d);
        objArr[8] = Integer.valueOf(t() ? 1 : 0);
        objArr[9] = Integer.valueOf(A() ? 1 : 0);
        objArr[10] = B();
        objArr[11] = Long.valueOf(a.a(this.b * 1000));
        objArr[12] = Long.valueOf(r());
        objArr[13] = Long.valueOf(x());
        return String.format("UPDATE %s SET money = %d, subtypeid = %d, outgodate = %d, accountid = %d, projectid = %d, reimburse = %d, comment = '%s', isdelete = %d, updatestatus = %d, uuid = '%s', ymd = %d , targetid = %d WHERE id = %d", objArr);
    }

    /* access modifiers changed from: protected */
    public final void l() {
    }
}
