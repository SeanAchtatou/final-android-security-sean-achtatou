package com.wacai.data;

import android.util.Log;
import com.wacai.e;
import org.w3c.dom.Element;

public final class t extends k {
    protected t() {
        super("TBL_ACCOUNTTYPE");
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0073  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.t a(long r6) {
        /*
            r4 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select * from TBL_ACCOUNTTYPE where id = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x0066, all -> 0x006f }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x0066, all -> 0x006f }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x0066, all -> 0x006f }
            if (r0 == 0) goto L_0x0029
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            if (r1 != 0) goto L_0x0030
        L_0x0029:
            if (r0 == 0) goto L_0x002e
            r0.close()
        L_0x002e:
            r0 = r4
        L_0x002f:
            return r0
        L_0x0030:
            com.wacai.data.t r1 = new com.wacai.data.t     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            r1.<init>()     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            r1.k(r6)     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            java.lang.String r2 = "name"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            r1.b(r2)     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            java.lang.String r2 = "uuid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            r1.g(r2)     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            java.lang.String r2 = "orderno"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            r1.f(r2)     // Catch:{ Exception -> 0x007c, all -> 0x0077 }
            if (r0 == 0) goto L_0x0064
            r0.close()
        L_0x0064:
            r0 = r1
            goto L_0x002f
        L_0x0066:
            r0 = move-exception
            r0 = r4
        L_0x0068:
            if (r0 == 0) goto L_0x006d
            r0.close()
        L_0x006d:
            r0 = r4
            goto L_0x002f
        L_0x006f:
            r0 = move-exception
            r1 = r4
        L_0x0071:
            if (r1 == 0) goto L_0x0076
            r1.close()
        L_0x0076:
            throw r0
        L_0x0077:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0071
        L_0x007c:
            r1 = move-exception
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.t.a(long):com.wacai.data.t");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab
     arg types: [org.w3c.dom.Element, com.wacai.data.t, int]
     candidates:
      com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int
      com.wacai.data.aa.a(java.lang.String, java.lang.String, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab */
    public static t a(Element element) {
        if (element == null) {
            return null;
        }
        return (t) ab.a(element, (ab) new t(), false);
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str != null && str2 != null) {
            if (str.equalsIgnoreCase("r")) {
                g(str2);
            } else if (str.equalsIgnoreCase("s")) {
                b(str2);
            } else if (str.equalsIgnoreCase("w")) {
                f(Long.parseLong(str2));
            }
        }
    }

    public final void a(StringBuffer stringBuffer) {
    }

    public final void c() {
        if (j() == null || j().length() <= 0) {
            Log.e("WAC_AccountType", "Invalide name when save account type");
        } else if (C()) {
            e.c().b().execSQL(String.format("UPDATE %s SET name = '%s', uuid = '%s', orderno = %d WHERE id = %d", w(), e(j()), B(), Long.valueOf(k()), Long.valueOf(x())));
        } else {
            e.c().b().execSQL(String.format("INSERT INTO %s (name, uuid, orderno) VALUES ('%s', '%s', %d)", w(), e(j()), B(), Long.valueOf(k())));
            k((long) d(w()));
        }
    }
}
