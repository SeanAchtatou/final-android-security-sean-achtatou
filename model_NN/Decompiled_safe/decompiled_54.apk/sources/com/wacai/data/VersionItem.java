package com.wacai.data;

import android.os.Parcel;
import android.os.Parcelable;

public final class VersionItem implements Parcelable {
    public static final Parcelable.Creator CREATOR = new o();
    public String a;
    public String b;
    public String c;
    public String d;

    public VersionItem() {
        this.a = "";
        this.b = "";
        this.c = "";
        this.d = "";
    }

    /* synthetic */ VersionItem(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private VersionItem(Parcel parcel, byte b2) {
        this.a = "";
        this.b = "";
        this.c = "";
        this.d = "";
        this.a = parcel.readString();
        this.b = parcel.readString();
        this.c = parcel.readString();
        this.d = parcel.readString();
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
    }
}
