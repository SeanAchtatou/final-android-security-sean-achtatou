package com.wacai.data;

import android.database.Cursor;
import com.wacai.a.a;
import com.wacai.e;
import org.w3c.dom.Element;

public final class v extends ab {
    private long a = 0;
    private String b = "";
    private boolean c = false;

    public v() {
        super("TBL_MYNOTE");
    }

    public static int a(long j, long j2, StringBuffer stringBuffer) {
        Cursor cursor;
        if (stringBuffer == null) {
            throw new NullPointerException("the buffer can not be null!");
        }
        try {
            Cursor rawQuery = e.c().b().rawQuery(String.format("SELECT * FROM %s WHERE ( uuid IS NOT NULL AND uuid <> '' AND updatestatus = 0 ) AND TBL_MYNOTE.ymd >= " + ((10000 * j) + (100 * j2)) + " AND TBL_MYNOTE.ymd < " + ((j * 10000) + (j2 * 100) + 99), "TBL_MYNOTE"), null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        v vVar = new v();
                        do {
                            vVar.g(rawQuery.getString(rawQuery.getColumnIndexOrThrow("uuid")));
                            vVar.b = rawQuery.getString(rawQuery.getColumnIndexOrThrow("comment"));
                            vVar.c = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("isdelete")) > 0;
                            vVar.a = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("createdate"));
                            if (stringBuffer == null) {
                                throw new NullPointerException("buf can not be null !");
                            }
                            stringBuffer.append("<bn>");
                            stringBuffer.append("<r>");
                            stringBuffer.append(vVar.B());
                            stringBuffer.append("</r><aq>");
                            stringBuffer.append(h(vVar.b));
                            stringBuffer.append("</aq><al>");
                            stringBuffer.append(vVar.c ? 1 : 0);
                            stringBuffer.append("</al><ap>");
                            stringBuffer.append(vVar.a);
                            stringBuffer.append("</ap>");
                            stringBuffer.append("</bn>");
                        } while (rawQuery.moveToNext());
                        int count = rawQuery.getCount();
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return count;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return 0;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab
     arg types: [org.w3c.dom.Element, com.wacai.data.v, int]
     candidates:
      com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int
      com.wacai.data.aa.a(java.lang.String, java.lang.String, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab */
    public static v a(Element element) {
        if (element == null) {
            return null;
        }
        return (v) ab.a(element, (ab) new v(), false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x009c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.v b(long r10) {
        /*
            r7 = 0
            r6 = 1
            r5 = 0
            r4 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select * from TBL_MYNOTE where id = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r10)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x008f, all -> 0x0098 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x008f, all -> 0x0098 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x008f, all -> 0x0098 }
            if (r0 == 0) goto L_0x002d
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            if (r1 != 0) goto L_0x0034
        L_0x002d:
            if (r0 == 0) goto L_0x0032
            r0.close()
        L_0x0032:
            r0 = r4
        L_0x0033:
            return r0
        L_0x0034:
            com.wacai.data.v r1 = new com.wacai.data.v     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            r1.<init>()     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            r1.k(r10)     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            java.lang.String r2 = "createdate"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            r1.a = r2     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            java.lang.String r2 = "comment"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            r1.b = r2     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            java.lang.String r2 = "isdelete"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x008b
            r2 = r6
        L_0x0063:
            r1.c = r2     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            java.lang.String r2 = "updatestatus"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x008d
            r2 = r6
        L_0x0074:
            r1.f(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            java.lang.String r2 = "uuid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            r1.g(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x00a0 }
            if (r0 == 0) goto L_0x0089
            r0.close()
        L_0x0089:
            r0 = r1
            goto L_0x0033
        L_0x008b:
            r2 = r5
            goto L_0x0063
        L_0x008d:
            r2 = r5
            goto L_0x0074
        L_0x008f:
            r0 = move-exception
            r0 = r4
        L_0x0091:
            if (r0 == 0) goto L_0x0096
            r0.close()
        L_0x0096:
            r0 = r4
            goto L_0x0033
        L_0x0098:
            r0 = move-exception
            r1 = r4
        L_0x009a:
            if (r1 == 0) goto L_0x009f
            r1.close()
        L_0x009f:
            throw r0
        L_0x00a0:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x009a
        L_0x00a5:
            r1 = move-exception
            goto L_0x0091
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.v.b(long):com.wacai.data.v");
    }

    public final long a() {
        return this.a;
    }

    public final void a(long j) {
        this.a = j;
    }

    public final void a(String str) {
        this.b = str;
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str.equalsIgnoreCase("r")) {
            g(str2);
        } else if (str.equalsIgnoreCase("aq")) {
            this.b = str2;
        } else if (str.equalsIgnoreCase("al")) {
            this.c = Long.parseLong(str2) > 0;
        } else if (str.equalsIgnoreCase("ap")) {
            this.a = Long.parseLong(str2);
        }
    }

    public final String b() {
        return this.b;
    }

    public final void c() {
        boolean C = C();
        if (!this.c || !z()) {
            if (C) {
                Object[] objArr = new Object[6];
                objArr[0] = w();
                objArr[1] = Long.valueOf(this.a);
                objArr[2] = e(this.b);
                objArr[3] = Integer.valueOf(this.c ? 1 : 0);
                objArr[4] = Long.valueOf(a.a(this.a * 1000));
                objArr[5] = Long.valueOf(x());
                e.c().b().execSQL(String.format("UPDATE %s SET createdate = %d, comment = '%s', isdelete = %d, updatestatus = 0, ymd = %d WHERE id = %d", objArr));
                return;
            }
            Object[] objArr2 = new Object[7];
            objArr2[0] = w();
            objArr2[1] = Long.valueOf(this.a);
            objArr2[2] = e(this.b);
            objArr2[3] = Integer.valueOf(this.c ? 1 : 0);
            objArr2[4] = Integer.valueOf(A() ? 1 : 0);
            objArr2[5] = B();
            objArr2[6] = Long.valueOf(a.a(this.a * 1000));
            e.c().b().execSQL(String.format("INSERT INTO %s (createdate, comment, isdelete, updatestatus, uuid, ymd) VALUES (%d, '%s', %d, %d, '%s', %d)", objArr2));
            k((long) d(w()));
        } else if (C) {
            aa.a("TBL_MYNOTE", x());
        }
    }

    public final void f() {
        if (x() > 0) {
            String B = B();
            if (B == null || B.length() <= 0) {
                aa.a("TBL_MYNOTE", x());
                return;
            }
            this.c = true;
            f(false);
            c();
        }
    }
}
