package com.wacai.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.wacai.a.a;
import com.wacai.e;
import java.util.Calendar;
import java.util.Date;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class q extends s {
    private String a;
    private long b;
    private long c;
    private long d;
    private long e;
    private long f;
    private boolean g;

    public q() {
        this(true);
    }

    private q(boolean z) {
        super("TBL_SCHEDULEOUTGOINFO", z);
        if (z) {
            this.b = y.y();
            a aVar = new a(new Date());
            aVar.f = 0;
            aVar.g = 0;
            aVar.h = 0;
            this.e = aVar.b() / 1000;
            this.f = this.e;
        } else {
            this.b = 10001;
        }
        this.c = 0;
        this.d = 1;
    }

    public static int a(StringBuffer stringBuffer) {
        Cursor cursor;
        try {
            Cursor rawQuery = e.c().b().rawQuery("select TBL_SCHEDULEOUTGOINFO.id as id, TBL_SCHEDULEOUTGOINFO.uuid as uuid,TBL_SCHEDULEOUTGOINFO.name as name, TBL_SCHEDULEOUTGOINFO.money as money, TBL_OUTGOSUBTYPEINFO.uuid as stuuid, TBL_SCHEDULEOUTGOINFO.cycle as cycle, TBL_SCHEDULEOUTGOINFO.occurday as occurday, TBL_ACCOUNTINFO.uuid as accuuid, TBL_PROJECTINFO.uuid as prjuuid, TBL_SCHEDULEOUTGOINFO.targetid as tgtid, TBL_SCHEDULEOUTGOINFO.startdate as startdate, TBL_SCHEDULEOUTGOINFO.enddate as enddate, TBL_SCHEDULEOUTGOINFO.reimburse as reimburse, TBL_SCHEDULEOUTGOINFO.isdelete as isdelete from TBL_SCHEDULEOUTGOINFO, TBL_OUTGOSUBTYPEINFO, TBL_PROJECTINFO, TBL_ACCOUNTINFO where ( TBL_SCHEDULEOUTGOINFO.uuid IS NOT NULL AND TBL_SCHEDULEOUTGOINFO.uuid <> '' ) AND TBL_SCHEDULEOUTGOINFO.updatestatus = 0 AND TBL_OUTGOSUBTYPEINFO.id = TBL_SCHEDULEOUTGOINFO.subtypeid AND TBL_PROJECTINFO.id = TBL_SCHEDULEOUTGOINFO.projectid AND TBL_ACCOUNTINFO.id = TBL_SCHEDULEOUTGOINFO.accountid GROUP BY TBL_SCHEDULEOUTGOINFO.id", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        int count = rawQuery.getCount();
                        q qVar = new q(false);
                        for (int i = 0; i < count; i++) {
                            stringBuffer.append("<m><r>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("uuid")));
                            stringBuffer.append("</r><s>");
                            stringBuffer.append(h(rawQuery.getString(rawQuery.getColumnIndexOrThrow("name"))));
                            stringBuffer.append("</s><ab>");
                            stringBuffer.append(a(l(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("money"))), 2));
                            stringBuffer.append("</ab><ac>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("stuuid")));
                            stringBuffer.append("</ac><ad>");
                            int i2 = rawQuery.getInt(rawQuery.getColumnIndexOrThrow("cycle"));
                            stringBuffer.append(i2);
                            stringBuffer.append("</ad>");
                            if (i2 == 2) {
                                stringBuffer.append("<ae>");
                                stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("occurday")));
                                stringBuffer.append("</ae>");
                            }
                            stringBuffer.append("<af>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("accuuid")));
                            stringBuffer.append("</af><ag>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("prjuuid")));
                            stringBuffer.append("</ag><ah>");
                            long j = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("tgtid"));
                            if (j > 0) {
                                stringBuffer.append(aa.a("TBL_TRADETARGET", "uuid", (int) j));
                            }
                            stringBuffer.append("</ah><ai>");
                            stringBuffer.append(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("startdate")));
                            stringBuffer.append("</ai><aj>");
                            stringBuffer.append(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("enddate")));
                            stringBuffer.append("</aj><ak>");
                            stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("reimburse")));
                            stringBuffer.append("</ak><al>");
                            stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("isdelete")));
                            stringBuffer.append("</al><am>");
                            x.a(qVar, rawQuery.getInt(rawQuery.getColumnIndexOrThrow("id")), stringBuffer);
                            stringBuffer.append("</am></m>");
                            rawQuery.moveToNext();
                        }
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return count;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return 0;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab
     arg types: [org.w3c.dom.Element, com.wacai.data.q, int]
     candidates:
      com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int
      com.wacai.data.aa.a(java.lang.String, java.lang.String, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab */
    public static q a(Element element) {
        if (element == null) {
            return null;
        }
        try {
            q qVar = (q) ab.a(element, (ab) new q(false), false);
            NodeList elementsByTagName = element.getElementsByTagName("an");
            int length = elementsByTagName.getLength();
            for (int i = 0; i < length; i++) {
                qVar.s().add(x.a((Element) elementsByTagName.item(i), qVar));
            }
            return qVar;
        } catch (Exception e2) {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.q f(long r5) {
        /*
            r3 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select * from TBL_SCHEDULEOUTGOINFO where id = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x0046, all -> 0x004f }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x0046, all -> 0x004f }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x0046, all -> 0x004f }
            if (r0 == 0) goto L_0x0029
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            if (r1 != 0) goto L_0x0030
        L_0x0029:
            if (r0 == 0) goto L_0x002e
            r0.close()
        L_0x002e:
            r0 = r3
        L_0x002f:
            return r0
        L_0x0030:
            com.wacai.data.q r1 = new com.wacai.data.q     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            r2 = 0
            r1.<init>(r2)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            r1.k(r5)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            r1.a(r0)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            com.wacai.data.x.a(r1)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            if (r0 == 0) goto L_0x0044
            r0.close()
        L_0x0044:
            r0 = r1
            goto L_0x002f
        L_0x0046:
            r0 = move-exception
            r0 = r3
        L_0x0048:
            if (r0 == 0) goto L_0x004d
            r0.close()
        L_0x004d:
            r0 = r3
            goto L_0x002f
        L_0x004f:
            r0 = move-exception
            r1 = r3
        L_0x0051:
            if (r1 == 0) goto L_0x0056
            r1.close()
        L_0x0056:
            throw r0
        L_0x0057:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0051
        L_0x005c:
            r1 = move-exception
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.q.f(long):com.wacai.data.q");
    }

    private void n(long j) {
        SQLiteDatabase b2 = e.c().b();
        Object[] objArr = new Object[9];
        objArr[0] = Long.valueOf(o());
        objArr[1] = Long.valueOf(this.b);
        objArr[2] = Long.valueOf(j);
        objArr[3] = Long.valueOf(p());
        objArr[4] = Long.valueOf(q());
        objArr[5] = Integer.valueOf(this.g ? 1 : 0);
        objArr[6] = Long.valueOf(x());
        objArr[7] = Long.valueOf(a.a(1000 * j));
        objArr[8] = Long.valueOf(r());
        String format = String.format("INSERT INTO TBL_OUTGOINFO (money, subtypeid, outgodate, accountid, projectid, reimburse, comment, isdelete, updatestatus, scheduleoutgoid, ymd, targetid) VALUES (%d, %d, %d, %d, %d, %d, '', 0, 1, %d, %d, %d)", objArr);
        try {
            b2.beginTransaction();
            b2.execSQL(format);
            int d2 = aa.d("TBL_OUTGOINFO");
            int size = s().size();
            for (int i = 0; i < size; i++) {
                x xVar = (x) s().get(i);
                if (xVar != null) {
                    b2.execSQL(String.format("INSERT INTO TBL_OUTGOMEMBERINFO (outgoid, memberid, sharemoney) VALUES (%d, %d, %d)", Integer.valueOf(d2), Long.valueOf(xVar.a()), Long.valueOf(xVar.b())));
                }
            }
            b2.setTransactionSuccessful();
        } finally {
            b2.endTransaction();
        }
    }

    public final String a() {
        return this.a;
    }

    public final void a(long j) {
        this.b = j;
    }

    /* access modifiers changed from: protected */
    public final void a(Cursor cursor) {
        if (cursor != null) {
            this.a = cursor.getString(cursor.getColumnIndexOrThrow("name"));
            this.b = cursor.getLong(cursor.getColumnIndexOrThrow("subtypeid"));
            this.c = cursor.getLong(cursor.getColumnIndexOrThrow("cycle"));
            this.d = cursor.getLong(cursor.getColumnIndexOrThrow("occurday"));
            this.e = cursor.getLong(cursor.getColumnIndexOrThrow("startdate"));
            this.f = cursor.getLong(cursor.getColumnIndexOrThrow("enddate"));
            this.g = cursor.getLong(cursor.getColumnIndexOrThrow("reimburse")) > 0;
            super.a(cursor);
        }
    }

    public final void a(String str) {
        this.a = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.s.a(java.lang.String, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.wacai.data.aa.a(double, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.aa.a(java.lang.String, long):void
      com.wacai.data.s.a(java.lang.String, java.lang.String):void */
    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str != null && str2 != null) {
            if (str.equalsIgnoreCase("s")) {
                this.a = str2;
            } else if (str.equalsIgnoreCase("ac")) {
                this.b = aa.a("TBL_OUTGOSUBTYPEINFO", "id", str2, 10001);
            } else if (str.equalsIgnoreCase("ad")) {
                this.c = Long.parseLong(str2);
            } else if (str.equalsIgnoreCase("ae")) {
                this.d = Long.parseLong(str2);
            } else if (str.equalsIgnoreCase("ai")) {
                this.e = Long.parseLong(str2);
            } else if (str.equalsIgnoreCase("aj")) {
                this.f = Long.parseLong(str2);
            } else if (str.equalsIgnoreCase("ak")) {
                this.g = Long.parseLong(str2) > 0;
            } else {
                super.a(str, str2);
            }
        }
    }

    public final void a(boolean z) {
        this.g = z;
    }

    public final long b() {
        return this.b;
    }

    public final void b(long j) {
        this.c = j;
    }

    public final void c() {
        if (this.a == null || this.a.length() <= 0) {
            Log.e("WAC_RegularOutgo", "Invalide name when save data");
        } else if (!b("TBL_OUTGOSUBTYPEINFO", this.b)) {
            Log.e("WAC_RegularOutgo", "Invalide outgo sub type when save data");
        } else if (!b("TBL_ACCOUNTINFO", p())) {
            Log.e("WAC_RegularOutgo", "Invalide account when save data");
        } else if (!b("TBL_PROJECTINFO", q())) {
            Log.e("WAC_RegularOutgo", "Invalide project when save data");
        } else if (t() || u()) {
            v();
            l();
            if (!t() && x() > 0) {
                switch ((int) this.c) {
                    case 0:
                        for (long j = this.e; j <= this.f; j += 86400) {
                            n(j);
                        }
                        return;
                    case 1:
                        long j2 = this.e;
                        while (j2 <= this.f) {
                            Calendar instance = Calendar.getInstance();
                            instance.setTimeInMillis(j2 * 1000);
                            int i = instance.get(7);
                            if (!(i == 7 || i == 1)) {
                                n(j2);
                                if (i == 6) {
                                    j2 += 172800;
                                }
                            }
                            j2 += 86400;
                        }
                        return;
                    case 2:
                        long j3 = this.e;
                        while (j3 <= this.f) {
                            if (a.a(j3 * 1000) % 100 == this.d) {
                                n(j3);
                                j3 += 2332800;
                            }
                            j3 += 86400;
                        }
                        return;
                    default:
                        return;
                }
            }
        } else {
            Log.e("WAC_RegularOutgo", "Money conflict with member shares!");
        }
    }

    public final void c(long j) {
        this.d = j;
    }

    public final long d() {
        return this.c;
    }

    public final void d(long j) {
        this.e = j;
    }

    public final long e() {
        return this.d;
    }

    public final void e(long j) {
        this.f = j;
    }

    public final long g() {
        return this.e;
    }

    public final String h() {
        return "scheduleoutgoid";
    }

    public final String i() {
        return "TBL_SCHEDULEOUTGOMEMBERINFO";
    }

    /* access modifiers changed from: protected */
    public final String j() {
        Object[] objArr = new Object[15];
        objArr[0] = w();
        objArr[1] = e(this.a);
        objArr[2] = Long.valueOf(o());
        objArr[3] = Long.valueOf(this.b);
        objArr[4] = Long.valueOf(this.c);
        objArr[5] = Long.valueOf(this.d);
        objArr[6] = Long.valueOf(p());
        objArr[7] = Long.valueOf(q());
        objArr[8] = Long.valueOf(this.e);
        objArr[9] = Long.valueOf(this.f);
        objArr[10] = Integer.valueOf(this.g ? 1 : 0);
        objArr[11] = Integer.valueOf(t() ? 1 : 0);
        objArr[12] = Integer.valueOf(A() ? 1 : 0);
        objArr[13] = B();
        objArr[14] = Long.valueOf(r());
        return String.format("INSERT INTO %s (name, money, subtypeid, cycle, occurday, accountid, projectid, startdate, enddate, reimburse, isdelete, updatestatus, uuid, targetid) VALUES ('%s', %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, '%s', %d)", objArr);
    }

    /* access modifiers changed from: protected */
    public final String k() {
        Object[] objArr = new Object[16];
        objArr[0] = w();
        objArr[1] = e(this.a);
        objArr[2] = Long.valueOf(o());
        objArr[3] = Long.valueOf(this.b);
        objArr[4] = Long.valueOf(this.c);
        objArr[5] = Long.valueOf(this.d);
        objArr[6] = Long.valueOf(p());
        objArr[7] = Long.valueOf(q());
        objArr[8] = Long.valueOf(this.e);
        objArr[9] = Long.valueOf(this.f);
        objArr[10] = Integer.valueOf(this.g ? 1 : 0);
        objArr[11] = Integer.valueOf(t() ? 1 : 0);
        objArr[12] = Integer.valueOf(A() ? 1 : 0);
        objArr[13] = B();
        objArr[14] = Long.valueOf(r());
        objArr[15] = Long.valueOf(x());
        return String.format("UPDATE %s SET name = '%s', money = %d, subtypeid = %d, cycle = %d, occurday = %d, accountid = %d, projectid = %d, startdate = %d, enddate = %d, reimburse = %d, isdelete = %d, updatestatus = %d, uuid = '%s', targetid = %d WHERE id = %d", objArr);
    }

    /* access modifiers changed from: protected */
    public final void l() {
        if (x() > 0) {
            e.c().b().execSQL("DELETE FROM TBL_OUTGOMEMBERINFO WHERE outgoid in (SELECT id FROM TBL_OUTGOINFO WHERE scheduleoutgoid = " + x() + ")");
            e.c().b().execSQL("DELETE FROM TBL_OUTGOINFO WHERE scheduleoutgoid = " + x());
        }
    }

    public final long m() {
        return this.f;
    }

    public final boolean n() {
        return this.g;
    }
}
