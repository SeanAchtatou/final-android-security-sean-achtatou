package com.wacai.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.wacai.a.a;
import com.wacai.e;
import java.util.Date;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class g extends s {
    private String a;
    private long b;
    private long c;
    private long d;
    private long e;
    private long f;

    public g() {
        this(true);
    }

    private g(boolean z) {
        super("TBL_SCHEDULEINCOMEINFO", z);
        if (z) {
            a aVar = new a(new Date());
            aVar.f = 0;
            aVar.g = 0;
            aVar.h = 0;
            this.e = aVar.b() / 1000;
            this.f = this.e;
            this.b = f("TBL_INCOMEMAINTYPEINFO");
        } else {
            this.b = 1;
        }
        this.c = 0;
        this.d = 1;
    }

    public static int a(StringBuffer stringBuffer) {
        Cursor cursor;
        try {
            Cursor rawQuery = e.c().b().rawQuery("select TBL_SCHEDULEINCOMEINFO.id as id, TBL_SCHEDULEINCOMEINFO.uuid as uuid, TBL_SCHEDULEINCOMEINFO.name as name, TBL_SCHEDULEINCOMEINFO.money as money, TBL_INCOMEMAINTYPEINFO.uuid as typeuuid, TBL_SCHEDULEINCOMEINFO.cycle as cycle, TBL_SCHEDULEINCOMEINFO.occurday as occurday, TBL_ACCOUNTINFO.uuid as accuuid, TBL_PROJECTINFO.uuid as prjuuid, TBL_SCHEDULEINCOMEINFO.targetid as tgtid, TBL_SCHEDULEINCOMEINFO.startdate as startdate, TBL_SCHEDULEINCOMEINFO.enddate as enddate, TBL_SCHEDULEINCOMEINFO.isdelete as isdelete from TBL_SCHEDULEINCOMEINFO, TBL_INCOMEMAINTYPEINFO, TBL_PROJECTINFO, TBL_ACCOUNTINFO where ( TBL_SCHEDULEINCOMEINFO.uuid IS NOT NULL AND TBL_SCHEDULEINCOMEINFO.uuid <> '' ) AND TBL_SCHEDULEINCOMEINFO.updatestatus = 0 AND TBL_INCOMEMAINTYPEINFO.id = TBL_SCHEDULEINCOMEINFO.typeid AND TBL_PROJECTINFO.id = TBL_SCHEDULEINCOMEINFO.projectid AND TBL_ACCOUNTINFO.id = TBL_SCHEDULEINCOMEINFO.accountid GROUP BY TBL_SCHEDULEINCOMEINFO.id", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        int count = rawQuery.getCount();
                        g gVar = new g(false);
                        for (int i = 0; i < count; i++) {
                            stringBuffer.append("<n><r>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("uuid")));
                            stringBuffer.append("</r><s>");
                            stringBuffer.append(h(rawQuery.getString(rawQuery.getColumnIndexOrThrow("name"))));
                            stringBuffer.append("</s><ab>");
                            stringBuffer.append(a(l(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("money"))), 2));
                            stringBuffer.append("</ab><t>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("typeuuid")));
                            stringBuffer.append("</t><ad>");
                            stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("cycle")));
                            stringBuffer.append("</ad><ae>");
                            stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("occurday")));
                            stringBuffer.append("</ae><af>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("accuuid")));
                            stringBuffer.append("</af><ag>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("prjuuid")));
                            stringBuffer.append("</ag><ah>");
                            long j = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("tgtid"));
                            if (j > 0) {
                                stringBuffer.append(aa.a("TBL_TRADETARGET", "uuid", (int) j));
                            }
                            stringBuffer.append("</ah><ai>");
                            stringBuffer.append(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("startdate")));
                            stringBuffer.append("</ai><aj>");
                            stringBuffer.append(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("enddate")));
                            stringBuffer.append("</aj><al>");
                            stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("isdelete")));
                            stringBuffer.append("</al><am>");
                            x.a(gVar, rawQuery.getInt(rawQuery.getColumnIndexOrThrow("id")), stringBuffer);
                            stringBuffer.append("</am></n>");
                            rawQuery.moveToNext();
                        }
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return count;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return 0;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab
     arg types: [org.w3c.dom.Element, com.wacai.data.g, int]
     candidates:
      com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int
      com.wacai.data.aa.a(java.lang.String, java.lang.String, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab */
    public static g a(Element element) {
        if (element == null) {
            return null;
        }
        try {
            g gVar = (g) ab.a(element, (ab) new g(false), false);
            NodeList elementsByTagName = element.getElementsByTagName("an");
            int length = elementsByTagName.getLength();
            for (int i = 0; i < length; i++) {
                gVar.s().add(x.a((Element) elementsByTagName.item(i), gVar));
            }
            return gVar;
        } catch (Exception e2) {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.g f(long r5) {
        /*
            r3 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select * from TBL_SCHEDULEINCOMEINFO where id = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x0046, all -> 0x004f }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x0046, all -> 0x004f }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x0046, all -> 0x004f }
            if (r0 == 0) goto L_0x0029
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            if (r1 != 0) goto L_0x0030
        L_0x0029:
            if (r0 == 0) goto L_0x002e
            r0.close()
        L_0x002e:
            r0 = r3
        L_0x002f:
            return r0
        L_0x0030:
            com.wacai.data.g r1 = new com.wacai.data.g     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            r2 = 0
            r1.<init>(r2)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            r1.k(r5)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            r1.a(r0)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            com.wacai.data.x.a(r1)     // Catch:{ Exception -> 0x005c, all -> 0x0057 }
            if (r0 == 0) goto L_0x0044
            r0.close()
        L_0x0044:
            r0 = r1
            goto L_0x002f
        L_0x0046:
            r0 = move-exception
            r0 = r3
        L_0x0048:
            if (r0 == 0) goto L_0x004d
            r0.close()
        L_0x004d:
            r0 = r3
            goto L_0x002f
        L_0x004f:
            r0 = move-exception
            r1 = r3
        L_0x0051:
            if (r1 == 0) goto L_0x0056
            r1.close()
        L_0x0056:
            throw r0
        L_0x0057:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0051
        L_0x005c:
            r1 = move-exception
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.g.f(long):com.wacai.data.g");
    }

    public final String a() {
        return this.a;
    }

    public final void a(long j) {
        this.b = j;
    }

    /* access modifiers changed from: protected */
    public final void a(Cursor cursor) {
        if (cursor != null) {
            this.a = cursor.getString(cursor.getColumnIndexOrThrow("name"));
            this.b = cursor.getLong(cursor.getColumnIndexOrThrow("typeid"));
            this.c = cursor.getLong(cursor.getColumnIndexOrThrow("cycle"));
            this.d = cursor.getLong(cursor.getColumnIndexOrThrow("occurday"));
            this.e = cursor.getLong(cursor.getColumnIndexOrThrow("startdate"));
            this.f = cursor.getLong(cursor.getColumnIndexOrThrow("enddate"));
            super.a(cursor);
        }
    }

    public final void a(String str) {
        this.a = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.s.a(java.lang.String, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.wacai.data.aa.a(double, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.aa.a(java.lang.String, long):void
      com.wacai.data.s.a(java.lang.String, java.lang.String):void */
    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str != null && str2 != null) {
            if (str.equalsIgnoreCase("s")) {
                this.a = str2;
            } else if (str.equalsIgnoreCase("t")) {
                this.b = aa.a("TBL_INCOMEMAINTYPEINFO", "id", str2, 1);
            } else if (str.equalsIgnoreCase("ad")) {
                this.c = Long.parseLong(str2);
            } else if (str.equalsIgnoreCase("ae")) {
                this.d = Long.parseLong(str2);
            } else if (str.equalsIgnoreCase("ai")) {
                this.e = Long.parseLong(str2);
            } else if (str.equalsIgnoreCase("aj")) {
                this.f = Long.parseLong(str2);
            } else {
                super.a(str, str2);
            }
        }
    }

    public final long b() {
        return this.b;
    }

    public final void b(long j) {
        this.c = j;
    }

    /* JADX INFO: finally extract failed */
    public final void c() {
        long j;
        if (this.a == null || this.a.length() <= 0) {
            Log.e("WAC_RegularIncome", "Invalide name when save data");
        } else if (!b("TBL_INCOMEMAINTYPEINFO", this.b)) {
            Log.e("WAC_RegularIncome", "Invalide income type when save data");
        } else if (!b("TBL_ACCOUNTINFO", p())) {
            Log.e("WAC_RegularIncome", "Invalide account when save data");
        } else if (!b("TBL_PROJECTINFO", q())) {
            Log.e("WAC_RegularIncome", "Invalide project when save data");
        } else if (t() || u()) {
            v();
            l();
            if (!t() && x() > 0) {
                switch ((int) this.c) {
                    case 0:
                        long j2 = this.e;
                        while (true) {
                            long j3 = j2;
                            if (j3 <= this.f) {
                                long a2 = a.a(1000 * j3);
                                if (a2 % 100 == this.d) {
                                    SQLiteDatabase b2 = e.c().b();
                                    String format = String.format("INSERT INTO TBL_INCOMEINFO (money, typeid, incomedate, accountid, projectid, comment, isdelete, updatestatus, scheduleincomeid, ymd, targetid) VALUES (%d, %d, %d, %d, %d, '', 0, 1, %d, %d, %d)", Long.valueOf(o()), Long.valueOf(this.b), Long.valueOf(j3), Long.valueOf(p()), Long.valueOf(q()), Long.valueOf(x()), Long.valueOf(a2), Long.valueOf(r()));
                                    try {
                                        b2.beginTransaction();
                                        b2.execSQL(format);
                                        int d2 = aa.d("TBL_INCOMEINFO");
                                        int size = s().size();
                                        for (int i = 0; i < size; i++) {
                                            x xVar = (x) s().get(i);
                                            if (xVar != null) {
                                                b2.execSQL(String.format("INSERT INTO TBL_INCOMEMEMBERINFO (incomeid, memberid, sharemoney) VALUES (%d, %d, %d)", Integer.valueOf(d2), Long.valueOf(xVar.a()), Long.valueOf(xVar.b())));
                                            }
                                        }
                                        b2.setTransactionSuccessful();
                                        b2.endTransaction();
                                        j = j3 + 2332800;
                                    } catch (Throwable th) {
                                        b2.endTransaction();
                                        throw th;
                                    }
                                } else {
                                    j = j3;
                                }
                                j2 = j + 86400;
                            } else {
                                return;
                            }
                        }
                    default:
                        return;
                }
            }
        } else {
            Log.e("WAC_RegularIncome", "Money conflict with member shares!");
        }
    }

    public final void c(long j) {
        this.d = j;
    }

    public final long d() {
        return this.d;
    }

    public final void d(long j) {
        this.e = j;
    }

    public final long e() {
        return this.e;
    }

    public final void e(long j) {
        this.f = j;
    }

    public final long g() {
        return this.f;
    }

    public final String h() {
        return "scheduleincomeid";
    }

    public final String i() {
        return "TBL_SCHEDULEINCOMEMEMBERINFO";
    }

    /* access modifiers changed from: protected */
    public final String j() {
        Object[] objArr = new Object[14];
        objArr[0] = w();
        objArr[1] = e(this.a);
        objArr[2] = Long.valueOf(o());
        objArr[3] = Long.valueOf(this.b);
        objArr[4] = Long.valueOf(this.c);
        objArr[5] = Long.valueOf(this.d);
        objArr[6] = Long.valueOf(p());
        objArr[7] = Long.valueOf(q());
        objArr[8] = Long.valueOf(this.e);
        objArr[9] = Long.valueOf(this.f);
        objArr[10] = Integer.valueOf(t() ? 1 : 0);
        objArr[11] = Integer.valueOf(A() ? 1 : 0);
        objArr[12] = B();
        objArr[13] = Long.valueOf(r());
        return String.format("INSERT INTO %s (name, money, typeid, cycle, occurday, accountid, projectid, startdate, enddate, isdelete, updatestatus, uuid, targetid) VALUES ('%s', %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, '%s', %d)", objArr);
    }

    /* access modifiers changed from: protected */
    public final String k() {
        Object[] objArr = new Object[15];
        objArr[0] = w();
        objArr[1] = e(this.a);
        objArr[2] = Long.valueOf(o());
        objArr[3] = Long.valueOf(this.b);
        objArr[4] = Long.valueOf(this.c);
        objArr[5] = Long.valueOf(this.d);
        objArr[6] = Long.valueOf(r());
        objArr[7] = Long.valueOf(p());
        objArr[8] = Long.valueOf(q());
        objArr[9] = Long.valueOf(this.e);
        objArr[10] = Long.valueOf(this.f);
        objArr[11] = Integer.valueOf(t() ? 1 : 0);
        objArr[12] = Integer.valueOf(A() ? 1 : 0);
        objArr[13] = B();
        objArr[14] = Long.valueOf(x());
        return String.format("UPDATE %s SET name = '%s', money = %d, typeid = %d, cycle = %d, occurday = %d, targetid = %d, accountid = %d, projectid = %d, startdate = %d, enddate = %d, isdelete = %d, updatestatus = %d, uuid = '%s' WHERE id = %d", objArr);
    }

    /* access modifiers changed from: protected */
    public final void l() {
        if (x() > 0) {
            e.c().b().execSQL("DELETE FROM TBL_INCOMEMEMBERINFO WHERE incomeid in (SELECT id FROM TBL_INCOMEINFO WHERE scheduleincomeid = " + x() + ")");
            e.c().b().execSQL("DELETE FROM TBL_INCOMEINFO WHERE scheduleincomeid = " + x());
        }
    }
}
