package com.wacai.data;

import android.database.Cursor;
import com.wacai.e;

public final class a extends aa {
    protected a() {
        super("TBL_USESTATISTIC");
    }

    public static int a(StringBuffer stringBuffer) {
        Cursor cursor;
        if (stringBuffer == null) {
            throw new NullPointerException("the buffer can not be null!");
        }
        try {
            Cursor rawQuery = e.c().b().rawQuery(String.format("SELECT * FROM %s where %s < %d", "TBL_USESTATISTIC", "ymd", Long.valueOf(com.wacai.a.a.a(System.currentTimeMillis()))), null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        int count = rawQuery.getCount();
                        do {
                            stringBuffer.append("<s t=\"1\" c=\"");
                            stringBuffer.append(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("usecount")));
                            stringBuffer.append("\" d=\"");
                            stringBuffer.append(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("ymd")));
                            stringBuffer.append("\"/>");
                        } while (rawQuery.moveToNext());
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return count;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return 0;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x007b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a() {
        /*
            r5 = 0
            r7 = 1
            r6 = 0
            com.wacai.e r0 = com.wacai.e.c()     // Catch:{ all -> 0x007f }
            android.database.sqlite.SQLiteDatabase r0 = r0.b()     // Catch:{ all -> 0x007f }
            java.lang.String r1 = "SELECT min(%s) as %s FROM %s"
            r2 = 3
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x007f }
            r3 = 0
            java.lang.String r4 = "ymd"
            r2[r3] = r4     // Catch:{ all -> 0x007f }
            r3 = 1
            java.lang.String r4 = "ymd"
            r2[r3] = r4     // Catch:{ all -> 0x007f }
            r3 = 2
            java.lang.String r4 = "TBL_USESTATISTIC"
            r2[r3] = r4     // Catch:{ all -> 0x007f }
            java.lang.String r1 = java.lang.String.format(r1, r2)     // Catch:{ all -> 0x007f }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ all -> 0x007f }
            if (r0 == 0) goto L_0x0082
            boolean r1 = r0.moveToFirst()     // Catch:{ all -> 0x0067 }
            if (r1 == 0) goto L_0x0082
            java.lang.String r1 = "ymd"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ all -> 0x0067 }
            long r1 = r0.getLong(r1)     // Catch:{ all -> 0x0067 }
            long r1 = com.wacai.a.a.b(r1)     // Catch:{ all -> 0x0067 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0067 }
            long r3 = com.wacai.a.a.a(r3)     // Catch:{ all -> 0x0067 }
            long r3 = com.wacai.a.a.b(r3)     // Catch:{ all -> 0x0067 }
            java.util.Calendar r5 = java.util.Calendar.getInstance()     // Catch:{ all -> 0x0067 }
            r5.setTimeInMillis(r1)     // Catch:{ all -> 0x0067 }
            java.util.Calendar r1 = java.util.Calendar.getInstance()     // Catch:{ all -> 0x0067 }
            r1.setTimeInMillis(r3)     // Catch:{ all -> 0x0067 }
            r2 = r6
        L_0x0059:
            boolean r3 = r5.before(r1)     // Catch:{ all -> 0x0067 }
            if (r3 == 0) goto L_0x0071
            int r2 = r2 + 1
            r3 = 6
            r4 = 1
            r5.add(r3, r4)     // Catch:{ all -> 0x0067 }
            goto L_0x0059
        L_0x0067:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x006b:
            if (r1 == 0) goto L_0x0070
            r1.close()
        L_0x0070:
            throw r0
        L_0x0071:
            long r1 = (long) r2
            r3 = 7
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 < 0) goto L_0x0082
            r1 = r7
        L_0x0079:
            if (r0 == 0) goto L_0x007e
            r0.close()
        L_0x007e:
            return r1
        L_0x007f:
            r0 = move-exception
            r1 = r5
            goto L_0x006b
        L_0x0082:
            r1 = r6
            goto L_0x0079
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.a.a():boolean");
    }

    public static void b() {
        long a = com.wacai.a.a.a(System.currentTimeMillis());
        e.c().b().execSQL(String.format("DELETE FROM %s WHERE %s < %d", "TBL_USESTATISTIC", "ymd", Long.valueOf(a)));
    }

    public final void c() {
    }
}
