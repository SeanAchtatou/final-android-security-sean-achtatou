package com.wacai.data;

import android.database.Cursor;
import com.wacai.e;
import org.w3c.dom.Element;

public class m extends ab {
    private int a = 1;
    private long b;
    private long c;
    private boolean d;
    private long e;
    private long f = 0;
    private long g;
    private long h;
    private String i = "";
    private long j;
    private b k = null;
    private int l;
    private int m;

    public m() {
        super("TBL_LOAN");
    }

    public static int a(StringBuffer stringBuffer) {
        Cursor cursor;
        if (stringBuffer == null) {
            return 0;
        }
        try {
            Cursor rawQuery = e.c().b().rawQuery("select a.uuid as _uuid, a.type as _type, a.date as _date, a.money as _money, a.expectprofit as _exprofit, a.expectdate as _exdate, a.accountid as _accid, a.debtid as _debtid, a.comment as _comment, a.isdelete as _isdelete, a.alertid as _alertid, b.type as _alerttype, b.day as _alertday from TBL_LOAN a LEFT JOIN TBL_ALERT b ON a.alertid = b.id  where ( a.uuid IS NOT NULL AND a.uuid <> '' ) AND a.updatestatus = 0", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        int count = rawQuery.getCount();
                        for (int i2 = 0; i2 < count; i2++) {
                            stringBuffer.append("<bc><r>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("_uuid")));
                            stringBuffer.append("</r><t>");
                            stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("_type")));
                            stringBuffer.append("</t><bd>");
                            stringBuffer.append(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_date")));
                            stringBuffer.append("</bd><ab>");
                            stringBuffer.append(aa.a(aa.l(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_money"))), 2));
                            stringBuffer.append("</ab><bf>");
                            if (rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_exdate")) != 0) {
                                stringBuffer.append(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_exdate")));
                            }
                            stringBuffer.append("</bf><be>");
                            long j2 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_exprofit"));
                            stringBuffer.append(j2 == 0 ? 0 : aa.a(aa.l(j2), 2));
                            stringBuffer.append("</be><bj>");
                            stringBuffer.append(aa.a("TBL_ACCOUNTINFO", "uuid", rawQuery.getInt(rawQuery.getColumnIndexOrThrow("_accid"))));
                            stringBuffer.append("</bj><bi>");
                            stringBuffer.append(aa.a("TBL_ACCOUNTINFO", "uuid", rawQuery.getInt(rawQuery.getColumnIndexOrThrow("_debtid"))));
                            stringBuffer.append("</bi><aq>");
                            stringBuffer.append(h(rawQuery.getString(rawQuery.getColumnIndexOrThrow("_comment"))));
                            stringBuffer.append("</aq><al>");
                            stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("_isdelete")));
                            stringBuffer.append("</al><bg>");
                            if (rawQuery.getInt(rawQuery.getColumnIndexOrThrow("_alertid")) == 0) {
                                stringBuffer.append(0);
                            } else if (rawQuery.getInt(rawQuery.getColumnIndexOrThrow("_alerttype")) == 0) {
                                stringBuffer.append(1);
                            } else {
                                stringBuffer.append(2);
                            }
                            stringBuffer.append("</bg><bh>");
                            stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("_alertday")));
                            stringBuffer.append("</bh></bc>");
                            rawQuery.moveToNext();
                        }
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return count;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return 0;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab
     arg types: [org.w3c.dom.Element, com.wacai.data.m, int]
     candidates:
      com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int
      com.wacai.data.aa.a(java.lang.String, java.lang.String, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab */
    public static m a(Element element) {
        if (element == null) {
            return null;
        }
        m mVar = (m) ab.a(element, (ab) new m(), false);
        if (mVar.l == 0) {
            mVar.k = null;
        } else {
            if (mVar.k == null) {
                mVar.k = new b();
            }
            if (mVar.l == 1) {
                mVar.k.a(0);
            } else if (mVar.l == 2) {
                mVar.k.a(2);
            }
            mVar.k.b(1);
            mVar.k.c(mVar.m);
        }
        return mVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0101  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.m f(long r10) {
        /*
            r7 = 0
            r6 = 1
            r5 = 0
            r4 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select * from TBL_LOAN where id = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r10)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x00f3, all -> 0x00fd }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x00f3, all -> 0x00fd }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x00f3, all -> 0x00fd }
            if (r0 == 0) goto L_0x002d
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            if (r1 != 0) goto L_0x0034
        L_0x002d:
            if (r0 == 0) goto L_0x0032
            r0.close()
        L_0x0032:
            r0 = r4
        L_0x0033:
            return r0
        L_0x0034:
            com.wacai.data.m r1 = new com.wacai.data.m     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r1.<init>()     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r1.k(r10)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            if (r0 == 0) goto L_0x00e7
            java.lang.String r2 = "type"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r1.a = r2     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            java.lang.String r2 = "date"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r1.b = r2     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            java.lang.String r2 = "money"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r1.c = r2     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            java.lang.String r2 = "expectprofit"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r1.e = r2     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            java.lang.String r2 = "expectdate"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r1.f = r2     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            java.lang.String r2 = "accountid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r1.g = r2     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            java.lang.String r2 = "debtid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r1.h = r2     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            java.lang.String r2 = "comment"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r1.a(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            java.lang.String r2 = "uuid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r1.g(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            java.lang.String r2 = "isdelete"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            if (r2 <= 0) goto L_0x00ef
            r2 = r6
        L_0x00b9:
            r1.d = r2     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            java.lang.String r2 = "updatestatus"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00f1
            r2 = r6
        L_0x00ca:
            r1.f(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            java.lang.String r2 = "alertid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r1.j = r2     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            long r2 = r1.j     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00e7
            long r2 = r1.j     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            com.wacai.data.b r2 = com.wacai.data.b.c(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r1.k = r2     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
        L_0x00e7:
            if (r0 == 0) goto L_0x00ec
            r0.close()
        L_0x00ec:
            r0 = r1
            goto L_0x0033
        L_0x00ef:
            r2 = r5
            goto L_0x00b9
        L_0x00f1:
            r2 = r5
            goto L_0x00ca
        L_0x00f3:
            r0 = move-exception
            r0 = r4
        L_0x00f5:
            if (r0 == 0) goto L_0x00fa
            r0.close()
        L_0x00fa:
            r0 = r4
            goto L_0x0033
        L_0x00fd:
            r0 = move-exception
            r1 = r4
        L_0x00ff:
            if (r1 == 0) goto L_0x0104
            r1.close()
        L_0x0104:
            throw r0
        L_0x0105:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00ff
        L_0x010a:
            r1 = move-exception
            goto L_0x00f5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.m.f(long):com.wacai.data.m");
    }

    public static void g(long j2) {
        if (j2 > 0) {
            f(j2).f();
        }
    }

    private void k() {
        u.a(1, x());
        if (this.j > 0) {
            b.d(this.j);
            this.j = 0;
        }
    }

    public final int a() {
        return this.a;
    }

    public final void a(int i2) {
        this.a = i2;
    }

    public final void a(long j2) {
        this.b = j2;
    }

    public final void a(b bVar) {
        this.k = bVar;
    }

    public final void a(String str) {
        this.i = str;
        if (this.i == null) {
            this.i = "";
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str != null && str2 != null) {
            if (str.equalsIgnoreCase("r")) {
                g(str2);
            } else if (str.equalsIgnoreCase("ab")) {
                this.c = aa.a(Double.parseDouble(str2));
            } else if (str.equalsIgnoreCase("bd")) {
                this.b = Long.parseLong(str2);
            } else if (str.equalsIgnoreCase("bj")) {
                this.g = aa.a("TBL_ACCOUNTINFO", "id", str2, 1);
            } else if (str.equalsIgnoreCase("bi")) {
                this.h = aa.a("TBL_ACCOUNTINFO", "id", str2, 0);
            } else if (str.equalsIgnoreCase("t")) {
                this.a = Integer.parseInt(str2);
            } else if (str.equalsIgnoreCase("aq")) {
                a(str2);
            } else if (str.equalsIgnoreCase("al")) {
                this.d = Long.parseLong(str2) > 0;
            } else if (str.equalsIgnoreCase("bf")) {
                this.f = Long.parseLong(str2);
            } else if (str.equalsIgnoreCase("be")) {
                this.e = aa.a(Double.parseDouble(str2));
            } else if (str.equalsIgnoreCase("bg")) {
                this.l = Integer.parseInt(str2);
            } else if (str.equalsIgnoreCase("bh")) {
                this.m = Integer.parseInt(str2);
            }
        }
    }

    public final void a(boolean z) {
        this.d = z;
    }

    public final long b() {
        return this.b;
    }

    public final void b(long j2) {
        this.c = j2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fc A[Catch:{ all -> 0x0263 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c() {
        /*
            r15 = this;
            boolean r0 = r15.C()
            if (r0 != 0) goto L_0x000b
            boolean r1 = r15.d
            if (r1 == 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            r1 = 0
            if (r0 == 0) goto L_0x028a
            boolean r2 = r15.d
            if (r2 != 0) goto L_0x028a
            long r1 = r15.j
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x0193
            long r1 = r15.j
            com.wacai.data.b r1 = com.wacai.data.b.c(r1)
            r11 = r1
        L_0x0021:
            long r1 = r15.j
            com.wacai.data.b r3 = r15.k
            if (r3 != 0) goto L_0x01a7
            r1 = 0
            r12 = r1
        L_0x002a:
            com.wacai.e r1 = com.wacai.e.c()
            android.database.sqlite.SQLiteDatabase r14 = r1.b()
            r14.beginTransaction()     // Catch:{ all -> 0x0263 }
            if (r0 == 0) goto L_0x01bf
            java.lang.String r0 = "UPDATE %s SET type = %d, money = %d, alertid = %d, date = %d, expectprofit = %d, expectdate = %d, accountid = %d, debtid = %d, comment = '%s', updatestatus = %d, ymd = %d, isdelete = %d WHERE id = %d"
            r1 = 14
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0263 }
            r2 = 0
            java.lang.String r3 = "TBL_LOAN"
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 1
            int r3 = r15.a     // Catch:{ all -> 0x0263 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 2
            long r3 = r15.c     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 3
            java.lang.Long r3 = java.lang.Long.valueOf(r12)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 4
            long r3 = r15.b     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 5
            long r3 = r15.e     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 6
            long r3 = r15.f     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 7
            long r3 = r15.g     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 8
            long r3 = r15.h     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 9
            java.lang.String r3 = r15.i     // Catch:{ all -> 0x0263 }
            java.lang.String r3 = e(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 10
            boolean r3 = r15.A()     // Catch:{ all -> 0x0263 }
            if (r3 == 0) goto L_0x01b9
            r3 = 1
        L_0x009c:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 11
            long r3 = r15.b     // Catch:{ all -> 0x0263 }
            r5 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 * r5
            long r3 = com.wacai.a.a.a(r3)     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 12
            boolean r3 = r15.d     // Catch:{ all -> 0x0263 }
            if (r3 == 0) goto L_0x01bc
            r3 = 1
        L_0x00ba:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 13
            long r3 = r15.x()     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            java.lang.String r0 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x0263 }
            r14.execSQL(r0)     // Catch:{ all -> 0x0263 }
            boolean r0 = r15.d     // Catch:{ all -> 0x0263 }
            if (r0 == 0) goto L_0x00da
            r15.k()     // Catch:{ all -> 0x0263 }
        L_0x00da:
            boolean r0 = r15.d     // Catch:{ all -> 0x0263 }
            if (r0 != 0) goto L_0x018b
            int r0 = r15.a     // Catch:{ all -> 0x0263 }
            switch(r0) {
                case 0: goto L_0x0280;
                case 1: goto L_0x026c;
                default: goto L_0x00e3;
            }     // Catch:{ all -> 0x0263 }
        L_0x00e3:
            long r0 = r15.j     // Catch:{ all -> 0x0263 }
            r2 = 0
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x00f8
            com.wacai.data.b r0 = r15.k     // Catch:{ all -> 0x0263 }
            if (r0 != 0) goto L_0x00f8
            long r0 = r15.j     // Catch:{ all -> 0x0263 }
            com.wacai.data.b.d(r0)     // Catch:{ all -> 0x0263 }
            r0 = 0
            r15.j = r0     // Catch:{ all -> 0x0263 }
        L_0x00f8:
            com.wacai.data.b r0 = r15.k     // Catch:{ all -> 0x0263 }
            if (r0 == 0) goto L_0x0148
            com.wacai.data.b r0 = r15.k     // Catch:{ all -> 0x0263 }
            long r1 = r15.j     // Catch:{ all -> 0x0263 }
            r0.k(r1)     // Catch:{ all -> 0x0263 }
            com.wacai.data.b r0 = r15.k     // Catch:{ all -> 0x0263 }
            long r1 = r15.x()     // Catch:{ all -> 0x0263 }
            r0.b(r1)     // Catch:{ all -> 0x0263 }
            com.wacai.data.b r0 = r15.k     // Catch:{ all -> 0x0263 }
            long r1 = r15.f     // Catch:{ all -> 0x0263 }
            r0.e(r1)     // Catch:{ all -> 0x0263 }
            if (r11 == 0) goto L_0x013b
            com.wacai.data.b r0 = r15.k     // Catch:{ all -> 0x0263 }
            int r0 = r0.b()     // Catch:{ all -> 0x0263 }
            int r1 = r11.b()     // Catch:{ all -> 0x0263 }
            if (r0 != r1) goto L_0x013b
            com.wacai.data.b r0 = r15.k     // Catch:{ all -> 0x0263 }
            int r0 = r0.d()     // Catch:{ all -> 0x0263 }
            int r1 = r11.d()     // Catch:{ all -> 0x0263 }
            if (r0 != r1) goto L_0x013b
            com.wacai.data.b r0 = r15.k     // Catch:{ all -> 0x0263 }
            long r0 = r0.e()     // Catch:{ all -> 0x0263 }
            long r2 = r11.e()     // Catch:{ all -> 0x0263 }
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x0148
        L_0x013b:
            com.wacai.data.b r0 = r15.k     // Catch:{ all -> 0x0263 }
            r0.c()     // Catch:{ all -> 0x0263 }
            com.wacai.data.b r0 = r15.k     // Catch:{ all -> 0x0263 }
            long r0 = r0.x()     // Catch:{ all -> 0x0263 }
            r15.j = r0     // Catch:{ all -> 0x0263 }
        L_0x0148:
            com.wacai.data.b r0 = r15.k     // Catch:{ all -> 0x0263 }
            if (r0 == 0) goto L_0x018b
            com.wacai.data.b r0 = r15.k     // Catch:{ all -> 0x0263 }
            long r0 = r0.x()     // Catch:{ all -> 0x0263 }
            int r0 = (r12 > r0 ? 1 : (r12 == r0 ? 0 : -1))
            if (r0 == 0) goto L_0x018b
            com.wacai.data.b r0 = r15.k     // Catch:{ all -> 0x0263 }
            if (r0 == 0) goto L_0x018b
            java.lang.String r0 = "UPDATE %s SET alertid = %d WHERE id = %d"
            r1 = 3
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0263 }
            r2 = 0
            java.lang.String r3 = "TBL_LOAN"
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 1
            com.wacai.data.b r3 = r15.k     // Catch:{ all -> 0x0263 }
            long r3 = r3.x()     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 2
            long r3 = r15.x()     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            java.lang.String r0 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x0263 }
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ all -> 0x0263 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ all -> 0x0263 }
            r1.execSQL(r0)     // Catch:{ all -> 0x0263 }
        L_0x018b:
            r14.setTransactionSuccessful()     // Catch:{ all -> 0x0263 }
            r14.endTransaction()
            goto L_0x000a
        L_0x0193:
            long r1 = r15.x()
            r3 = 1
            com.wacai.data.b r1 = com.wacai.data.b.a(r1, r3)
            if (r1 == 0) goto L_0x01a4
            long r2 = r1.x()
            r15.j = r2
        L_0x01a4:
            r11 = r1
            goto L_0x0021
        L_0x01a7:
            r3 = 0
            int r3 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r3 > 0) goto L_0x0287
            java.lang.String r1 = "TBL_ALERT"
            int r1 = d(r1)
            int r1 = r1 + 1
            long r1 = (long) r1
            r12 = r1
            goto L_0x002a
        L_0x01b9:
            r3 = 0
            goto L_0x009c
        L_0x01bc:
            r3 = 0
            goto L_0x00ba
        L_0x01bf:
            java.lang.String r0 = "INSERT INTO %s (type, money, alertid, date, expectprofit, expectdate, accountid, debtid, comment, updatestatus, ymd, isdelete, uuid) VALUES (%d, %d, %d, %d, %d, %d, %d, %d, '%s', %d, %d, %d, '%s')"
            r1 = 14
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0263 }
            r2 = 0
            java.lang.String r3 = "TBL_LOAN"
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 1
            int r3 = r15.a     // Catch:{ all -> 0x0263 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 2
            long r3 = r15.c     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 3
            java.lang.Long r3 = java.lang.Long.valueOf(r12)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 4
            long r3 = r15.b     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 5
            long r3 = r15.e     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 6
            long r3 = r15.f     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 7
            long r3 = r15.g     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 8
            long r3 = r15.h     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 9
            java.lang.String r3 = r15.i     // Catch:{ all -> 0x0263 }
            java.lang.String r3 = e(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 10
            boolean r3 = r15.A()     // Catch:{ all -> 0x0263 }
            if (r3 == 0) goto L_0x0268
            r3 = 1
        L_0x0224:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 11
            long r3 = r15.b     // Catch:{ all -> 0x0263 }
            r5 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 * r5
            long r3 = com.wacai.a.a.a(r3)     // Catch:{ all -> 0x0263 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 12
            boolean r3 = r15.d     // Catch:{ all -> 0x0263 }
            if (r3 == 0) goto L_0x026a
            r3 = 1
        L_0x0242:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            r2 = 13
            java.lang.String r3 = r15.B()     // Catch:{ all -> 0x0263 }
            r1[r2] = r3     // Catch:{ all -> 0x0263 }
            java.lang.String r0 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x0263 }
            r14.execSQL(r0)     // Catch:{ all -> 0x0263 }
            java.lang.String r0 = "TBL_LOAN"
            int r0 = d(r0)     // Catch:{ all -> 0x0263 }
            long r0 = (long) r0     // Catch:{ all -> 0x0263 }
            r15.k(r0)     // Catch:{ all -> 0x0263 }
            goto L_0x00da
        L_0x0263:
            r0 = move-exception
            r14.endTransaction()
            throw r0
        L_0x0268:
            r3 = 0
            goto L_0x0224
        L_0x026a:
            r3 = 0
            goto L_0x0242
        L_0x026c:
            long r0 = r15.g     // Catch:{ all -> 0x0263 }
            long r2 = r15.h     // Catch:{ all -> 0x0263 }
            r6 = r2
            r4 = r0
        L_0x0272:
            long r0 = r15.c     // Catch:{ all -> 0x0263 }
            long r2 = r15.b     // Catch:{ all -> 0x0263 }
            r8 = 1
            long r9 = r15.x()     // Catch:{ all -> 0x0263 }
            com.wacai.data.u.a(r0, r2, r4, r6, r8, r9)     // Catch:{ all -> 0x0263 }
            goto L_0x00e3
        L_0x0280:
            long r0 = r15.h     // Catch:{ all -> 0x0263 }
            long r2 = r15.g     // Catch:{ all -> 0x0263 }
            r6 = r2
            r4 = r0
            goto L_0x0272
        L_0x0287:
            r12 = r1
            goto L_0x002a
        L_0x028a:
            r11 = r1
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.m.c():void");
    }

    public final void c(long j2) {
        this.f = j2;
    }

    public final long d() {
        return this.c;
    }

    public final void d(long j2) {
        this.g = j2;
    }

    public final long e() {
        return this.f;
    }

    public final void e(long j2) {
        this.h = j2;
    }

    public final void f() {
        if (x() > 0) {
            try {
                e.c().b().beginTransaction();
                if (B() == null || B().length() <= 0) {
                    a("TBL_LOAN", x());
                } else {
                    this.d = true;
                    f(false);
                    c();
                }
                k();
                e.c().b().setTransactionSuccessful();
            } finally {
                e.c().b().endTransaction();
            }
        }
    }

    public final long g() {
        return this.g;
    }

    public final long h() {
        return this.h;
    }

    public final String i() {
        return this.i;
    }

    public final b j() {
        return this.k;
    }
}
