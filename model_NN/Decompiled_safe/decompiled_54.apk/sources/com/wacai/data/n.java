package com.wacai.data;

import android.database.Cursor;
import com.wacai.e;
import org.w3c.dom.Element;

public final class n extends ab {
    private String a = "";
    private int b = 0;
    private long c = aa.y();
    private long d = aa.f("TBL_MEMBERINFO");
    private long e = h.c(false);
    private long f = aa.f("TBL_PROJECTINFO");
    private long g;
    private boolean h;
    private long i = 0;

    public n() {
        super("TBL_SHORTCUTSINFO");
    }

    public static int a(StringBuffer stringBuffer) {
        Cursor cursor;
        if (stringBuffer == null) {
            throw new NullPointerException("the buffer can not be null!");
        }
        try {
            Cursor rawQuery = e.c().b().rawQuery("select sc.uuid as scuuid, sc.type as sctype, sc.name as scname, sc.isdelete as scdelete, sc.reimburse as screimburse, ost.uuid as ostuuid, imt.uuid as imtuuid, mb.uuid as mbuuid, acc.uuid as accuuid, prj.uuid as prjuuid, tgt.uuid as tgtuuid from TBL_SHORTCUTSINFO sc left join TBL_OUTGOSUBTYPEINFO ost on sc.typeid = ost.id left join TBL_INCOMEMAINTYPEINFO imt on sc.typeid = imt.id left join TBL_MEMBERINFO mb on sc.memberid = mb.id left join TBL_ACCOUNTINFO acc on sc.accountid = acc.id left join TBL_PROJECTINFO prj on sc.projectid = prj.id left join TBL_TRADETARGET tgt on sc.targetid = tgt.id where sc.uuid is not null and sc.uuid <> '' and sc.updatestatus = 0 ", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        int count = rawQuery.getCount();
                        do {
                            int i2 = rawQuery.getInt(rawQuery.getColumnIndexOrThrow("sctype"));
                            stringBuffer.append("<bo><r>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("scuuid")));
                            stringBuffer.append("</r><t>");
                            stringBuffer.append(i2);
                            stringBuffer.append("</t><s>");
                            stringBuffer.append(h(rawQuery.getString(rawQuery.getColumnIndexOrThrow("scname"))));
                            stringBuffer.append("</s><ac>");
                            if (i2 == 0) {
                                stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("ostuuid")));
                            } else {
                                stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("imtuuid")));
                            }
                            stringBuffer.append("</ac><ao>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("mbuuid")));
                            stringBuffer.append("</ao><af>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("accuuid")));
                            stringBuffer.append("</af><ag>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("prjuuid")));
                            stringBuffer.append("</ag><ah>");
                            String string = rawQuery.getString(rawQuery.getColumnIndexOrThrow("tgtuuid"));
                            if (string != null) {
                                stringBuffer.append(string);
                            }
                            stringBuffer.append("</ah><ak>");
                            stringBuffer.append(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("screimburse")));
                            stringBuffer.append("</ak><u>");
                            stringBuffer.append(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("scdelete")) == 1 ? 0 : 1);
                            stringBuffer.append("</u>");
                            stringBuffer.append("</bo>");
                        } while (rawQuery.moveToNext());
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return count;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return 0;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab
     arg types: [org.w3c.dom.Element, com.wacai.data.n, int]
     candidates:
      com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int
      com.wacai.data.aa.a(java.lang.String, java.lang.String, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab */
    public static n a(Element element) {
        if (element == null) {
            return null;
        }
        return (n) ab.a(element, (ab) new n(), false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x00ed  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.n g(long r8) {
        /*
            r6 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select * from TBL_SHORTCUTSINFO where id = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x00c3, all -> 0x00e9 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x00c3, all -> 0x00e9 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x00c3, all -> 0x00e9 }
            if (r0 == 0) goto L_0x0029
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            if (r1 != 0) goto L_0x0030
        L_0x0029:
            if (r0 == 0) goto L_0x002e
            r0.close()
        L_0x002e:
            r0 = r6
        L_0x002f:
            return r0
        L_0x0030:
            com.wacai.data.n r1 = new com.wacai.data.n     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            r1.<init>()     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            r1.k(r8)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            java.lang.String r2 = "name"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            r1.a = r2     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            java.lang.String r2 = "type"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            r1.a(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            java.lang.String r2 = "typeid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            r1.c = r2     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            java.lang.String r2 = "memberid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            r1.d = r2     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            java.lang.String r2 = "accountid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            r1.e = r2     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            java.lang.String r2 = "projectid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            r1.f = r2     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            java.lang.String r2 = "reimburse"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            r1.i = r2     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            java.lang.String r2 = "targetid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            r1.g = r2     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            java.lang.String r2 = "isdelete"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            r4 = 0
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x00c1
            r2 = 1
        L_0x00aa:
            r1.h = r2     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            java.lang.String r2 = "uuid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            r1.g(r2)     // Catch:{ Exception -> 0x00f8, all -> 0x00f1 }
            if (r0 == 0) goto L_0x00be
            r0.close()
        L_0x00be:
            r0 = r1
            goto L_0x002f
        L_0x00c1:
            r2 = 0
            goto L_0x00aa
        L_0x00c3:
            r0 = move-exception
            r1 = r6
        L_0x00c5:
            java.lang.String r2 = "WAC_Shortcut"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f6 }
            r3.<init>()     // Catch:{ all -> 0x00f6 }
            java.lang.String r4 = "initById failed: exception = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00f6 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00f6 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00f6 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f6 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00f6 }
            if (r1 == 0) goto L_0x00e6
            r1.close()
        L_0x00e6:
            r0 = r6
            goto L_0x002f
        L_0x00e9:
            r0 = move-exception
            r1 = r6
        L_0x00eb:
            if (r1 == 0) goto L_0x00f0
            r1.close()
        L_0x00f0:
            throw r0
        L_0x00f1:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x00eb
        L_0x00f6:
            r0 = move-exception
            goto L_0x00eb
        L_0x00f8:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x00c5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.n.g(long):com.wacai.data.n");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: com.wacai.data.r} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: com.wacai.data.r} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v7, resolved type: com.wacai.data.z} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: com.wacai.data.r} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.s h(long r8) {
        /*
            r4 = 0
            r0 = 0
            com.wacai.data.n r2 = g(r8)
            if (r2 == 0) goto L_0x0046
            int r0 = r2.b
            if (r0 != 0) goto L_0x0047
            com.wacai.data.z r0 = new com.wacai.data.z
            r0.<init>()
            long r6 = r2.c
            r0.a(r6)
            long r6 = r2.i
            r0.c(r6)
            java.lang.String r1 = r2.a
            r0.a(r1)
            r1 = r0
        L_0x0022:
            long r6 = r2.e
            r1.h(r6)
            long r6 = r2.f
            r1.i(r6)
            long r6 = r2.g
            r1.j(r6)
            long r6 = r2.d
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x0058
            long r2 = r2.d
        L_0x0039:
            com.wacai.data.x r0 = new com.wacai.data.x
            r0.<init>(r1, r2, r4)
            java.util.ArrayList r2 = r1.s()
            r2.add(r0)
            r0 = r1
        L_0x0046:
            return r0
        L_0x0047:
            com.wacai.data.r r0 = new com.wacai.data.r
            r0.<init>()
            long r6 = r2.c
            r0.a(r6)
            java.lang.String r1 = r2.a
            r0.a(r1)
            r1 = r0
            goto L_0x0022
        L_0x0058:
            java.lang.String r0 = "TBL_MEMBERINFO"
            long r2 = com.wacai.data.aa.f(r0)
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.n.h(long):com.wacai.data.s");
    }

    public final String a() {
        return this.a;
    }

    public final void a(int i2) {
        if (this.b != i2) {
            this.b = i2;
            if (i2 == 0) {
                this.c = aa.y();
            } else if (i2 == 1) {
                this.c = aa.f("TBL_INCOMEMAINTYPEINFO");
            }
        }
    }

    public final void a(long j) {
        this.c = j;
    }

    public final void a(String str) {
        this.a = str;
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str.equalsIgnoreCase("r")) {
            g(str2);
        } else if (str.equalsIgnoreCase("t")) {
            a(Integer.parseInt(str2));
        } else if (str.equalsIgnoreCase("s")) {
            this.a = str2;
        } else if (str.equalsIgnoreCase("ac")) {
            if (this.b == 0) {
                this.c = aa.a("TBL_OUTGOSUBTYPEINFO", "id", str2, 10001);
            } else {
                this.c = aa.a("TBL_INCOMEMAINTYPEINFO", "id", str2, 1);
            }
        } else if (str.equalsIgnoreCase("ao")) {
            this.d = aa.a("TBL_MEMBERINFO", "id", str2, 1);
        } else if (str.equalsIgnoreCase("af")) {
            this.e = aa.a("TBL_ACCOUNTINFO", "id", str2, 1);
        } else if (str.equalsIgnoreCase("ag")) {
            this.f = aa.a("TBL_PROJECTINFO", "id", str2, 1);
        } else if (str.equalsIgnoreCase("ah")) {
            this.g = aa.a("TBL_TRADETARGET", "id", str2, 0);
        } else if (str.equalsIgnoreCase("ak")) {
            this.i = Long.parseLong(str2);
        } else if (str.equalsIgnoreCase("u")) {
            this.h = Long.parseLong(str2) <= 0;
        }
    }

    public final int b() {
        return this.b;
    }

    public final void b(long j) {
        this.d = j;
    }

    public final void c() {
        boolean C = C();
        if (!this.h || !z()) {
            if (C) {
                Object[] objArr = new Object[12];
                objArr[0] = w();
                objArr[1] = e(this.a);
                objArr[2] = Integer.valueOf(this.b);
                objArr[3] = Long.valueOf(this.c);
                objArr[4] = Long.valueOf(this.d);
                objArr[5] = Long.valueOf(this.e);
                objArr[6] = Long.valueOf(this.f);
                objArr[7] = Long.valueOf(this.i);
                objArr[8] = Integer.valueOf(A() ? 1 : 0);
                objArr[9] = Integer.valueOf(this.h ? 1 : 0);
                objArr[10] = Long.valueOf(this.g);
                objArr[11] = Long.valueOf(x());
                e.c().b().execSQL(String.format("UPDATE %s SET name = '%s', type = %d, typeid = %d, memberid = %d, accountid = %d, projectid = %d, reimburse = %d, updatestatus = %d, isdelete = %d, targetid = %d WHERE id = %d", objArr));
                return;
            }
            Object[] objArr2 = new Object[12];
            objArr2[0] = w();
            objArr2[1] = e(this.a);
            objArr2[2] = Integer.valueOf(this.b);
            objArr2[3] = Long.valueOf(this.c);
            objArr2[4] = Long.valueOf(this.d);
            objArr2[5] = Long.valueOf(this.e);
            objArr2[6] = Long.valueOf(this.f);
            objArr2[7] = Long.valueOf(this.i);
            objArr2[8] = Integer.valueOf(A() ? 1 : 0);
            objArr2[9] = Integer.valueOf(this.h ? 1 : 0);
            objArr2[10] = Long.valueOf(this.g);
            objArr2[11] = B();
            e.c().b().execSQL(String.format("INSERT INTO %s (name, type, typeid, memberid, accountid, projectid, reimburse, updatestatus, isdelete, targetid, uuid) VALUES ('%s', %d, %d, %d, %d, %d, %d, %d, %d, %d, '%s')", objArr2));
            k((long) d(w()));
        } else if (C) {
            aa.a("TBL_SHORTCUTSINFO", x());
        }
    }

    public final void c(long j) {
        this.e = j;
    }

    public final long d() {
        return this.c;
    }

    public final void d(long j) {
        this.f = j;
    }

    public final long e() {
        return this.d;
    }

    public final void e(long j) {
        this.g = j;
    }

    public final void f() {
        if (x() > 0) {
            String B = B();
            if (B == null || B.length() <= 0) {
                a("TBL_SHORTCUTSINFO", x());
                return;
            }
            this.h = true;
            f(false);
            c();
        }
    }

    public final void f(long j) {
        this.i = j;
    }

    public final long g() {
        return this.e;
    }

    public final long h() {
        return this.f;
    }

    public final long i() {
        return this.g;
    }

    public final long j() {
        return this.i;
    }
}
