package com.wacai.data;

import android.database.Cursor;
import android.util.Log;
import com.wacai.a.a;
import com.wacai.b;
import com.wacai.e;
import java.util.Date;
import org.w3c.dom.Element;

public class u extends ab {
    private long a;
    private long b;
    private long c;
    private boolean d;
    private long e;
    private long f;
    private String g;

    public u() {
        this(true);
    }

    private u(boolean z) {
        super("TBL_TRANSFERINFO");
        this.b = 0;
        this.c = 0;
        this.d = false;
        this.e = 0;
        this.f = 0;
        this.g = "";
        if (z) {
            this.e = h.c(false);
            this.f = h.c(false);
            this.a = new Date().getTime() / 1000;
            return;
        }
        this.f = 1;
        this.e = 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.b.a(long, long, boolean):long
     arg types: [long, long, int]
     candidates:
      com.wacai.b.a(long, long, long):void
      com.wacai.b.a(long, long, boolean):long */
    public static int a(long j, long j2, StringBuffer stringBuffer) {
        Cursor cursor;
        try {
            Cursor rawQuery = e.c().b().rawQuery("select TBL_TRANSFERINFO.id as id, TBL_TRANSFERINFO.uuid as uuid, TBL_TRANSFERINFO.transferoutmoney as transferoutmoney, TBL_TRANSFERINFO.transferinmoney as transferinmoney, TBL_TRANSFERINFO.date as date, TBL_TRANSFERINFO.transferoutaccountid as transferoutaccountid, TBL_TRANSFERINFO.transferinaccountid as transferinaccountid, TBL_TRANSFERINFO.comment as comment, TBL_TRANSFERINFO.isdelete as isdelete from TBL_TRANSFERINFO where TBL_TRANSFERINFO.uuid IS NOT NULL AND TBL_TRANSFERINFO.uuid <> '' AND TBL_TRANSFERINFO.ymd >= " + ((10000 * j) + (100 * j2)) + " AND TBL_TRANSFERINFO.ymd < " + ((10000 * j) + (100 * j2) + 99) + " AND TBL_TRANSFERINFO.type = 0 AND TBL_TRANSFERINFO.updatestatus = 0 ", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        int count = rawQuery.getCount();
                        long a2 = count > 0 ? b.a(j, j2, false) : 0;
                        for (int i = 0; i < count; i++) {
                            stringBuffer.append("<q><r>");
                            stringBuffer.append(rawQuery.getString(rawQuery.getColumnIndexOrThrow("uuid")));
                            stringBuffer.append("</r><as>");
                            stringBuffer.append(aa.a("TBL_ACCOUNTINFO", "uuid", rawQuery.getInt(rawQuery.getColumnIndexOrThrow("transferoutaccountid"))));
                            stringBuffer.append("</as><aw>");
                            stringBuffer.append(aa.a("TBL_ACCOUNTINFO", "uuid", rawQuery.getInt(rawQuery.getColumnIndexOrThrow("transferinaccountid"))));
                            stringBuffer.append("</aw><ax>");
                            stringBuffer.append(aa.a(aa.l(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("transferoutmoney"))), 2));
                            stringBuffer.append("</ax><ay>");
                            stringBuffer.append(aa.a(aa.l(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("transferinmoney"))), 2));
                            stringBuffer.append("</ay><ap>");
                            stringBuffer.append(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("date")));
                            stringBuffer.append("</ap><aq>");
                            stringBuffer.append(h(rawQuery.getString(rawQuery.getColumnIndexOrThrow("comment"))));
                            stringBuffer.append("</aq><al>");
                            stringBuffer.append(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("isdelete")));
                            stringBuffer.append("</al><ar>");
                            stringBuffer.append(a2);
                            stringBuffer.append("</ar></q>");
                            rawQuery.moveToNext();
                        }
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return count;
                    }
                } catch (Throwable th) {
                    th = th;
                    cursor = rawQuery;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return 0;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab
     arg types: [org.w3c.dom.Element, com.wacai.data.u, int]
     candidates:
      com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int
      com.wacai.data.aa.a(java.lang.String, java.lang.String, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab */
    public static u a(Element element) {
        if (element == null) {
            return null;
        }
        return (u) ab.a(element, (ab) new u(false), false);
    }

    static void a(int i, long j) {
        if (i != 0 && j > 0) {
            e.c().b().execSQL("DELETE FROM " + "TBL_TRANSFERINFO" + " WHERE type = " + i + " AND ownerid = " + j);
        }
    }

    static boolean a(long j, long j2, long j3, long j4, int i, long j5) {
        String format;
        if (j <= 0 || j3 <= 0 || j4 <= 0 || i == 0 || j5 <= 0) {
            Log.e("TransferItem", "Save Loan related tranfer illegal!");
            return false;
        }
        if (b(i, j5)) {
            format = String.format("UPDATE %s SET transferoutmoney = %d, transferinmoney = %d, date = %d, transferoutaccountid = %d, transferinaccountid = %d, comment = '', updatestatus = 1, ymd = %d WHERE type = %d AND ownerid = %d", "TBL_TRANSFERINFO", Long.valueOf(j), Long.valueOf(j), Long.valueOf(j2), Long.valueOf(j3), Long.valueOf(j4), Long.valueOf(a.a(j2 * 1000)), Integer.valueOf(i), Long.valueOf(j5));
            e.c().b().execSQL(format);
        } else {
            format = String.format("INSERT INTO %s (transferoutmoney, transferinmoney, date, transferoutaccountid, transferinaccountid, updatestatus, ymd, type, ownerid) VALUES (%d, %d, %d, %d, %d, 1, %d, %d, %d)", "TBL_TRANSFERINFO", Long.valueOf(j), Long.valueOf(j), Long.valueOf(j2), Long.valueOf(j3), Long.valueOf(j4), Long.valueOf(a.a(j2 * 1000)), Integer.valueOf(i), Long.valueOf(j5));
        }
        e.c().b().execSQL(format);
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x003a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean b(int r7, long r8) {
        /*
            r5 = 0
            r4 = 1
            r3 = 0
            r0 = 0
            int r0 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x000b
            if (r7 != 0) goto L_0x000d
        L_0x000b:
            r0 = r3
        L_0x000c:
            return r0
        L_0x000d:
            java.lang.String r0 = "SELECT id from TBL_TRANSFERINFO WHERE type = %d AND ownerid = %d"
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.Integer r2 = java.lang.Integer.valueOf(r7)
            r1[r3] = r2
            java.lang.Long r2 = java.lang.Long.valueOf(r8)
            r1[r4] = r2
            java.lang.String r0 = java.lang.String.format(r0, r1)
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ all -> 0x0041 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ all -> 0x0041 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x003f
            boolean r1 = r0.moveToFirst()     // Catch:{ all -> 0x0049 }
            if (r1 == 0) goto L_0x003f
            r1 = r4
        L_0x0038:
            if (r0 == 0) goto L_0x003d
            r0.close()
        L_0x003d:
            r0 = r1
            goto L_0x000c
        L_0x003f:
            r1 = r3
            goto L_0x0038
        L_0x0041:
            r0 = move-exception
            r1 = r5
        L_0x0043:
            if (r1 == 0) goto L_0x0048
            r1.close()
        L_0x0048:
            throw r0
        L_0x0049:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.u.b(int, long):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00cf  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.data.u f(long r10) {
        /*
            r7 = 0
            r6 = 1
            r5 = 0
            r4 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select * from TBL_TRANSFERINFO where id = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r10)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x00c1, all -> 0x00cb }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x00c1, all -> 0x00cb }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x00c1, all -> 0x00cb }
            if (r0 == 0) goto L_0x002d
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            if (r1 != 0) goto L_0x0034
        L_0x002d:
            if (r0 == 0) goto L_0x0032
            r0.close()
        L_0x0032:
            r0 = r4
        L_0x0033:
            return r0
        L_0x0034:
            com.wacai.data.u r1 = new com.wacai.data.u     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            r2 = 0
            r1.<init>(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            r1.k(r10)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            java.lang.String r2 = "transferoutaccountid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            r1.e = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            java.lang.String r2 = "transferinaccountid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            r1.f = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            java.lang.String r2 = "transferoutmoney"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            r1.b = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            java.lang.String r2 = "transferinmoney"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            r1.c = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            java.lang.String r2 = "uuid"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            r1.g(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            java.lang.String r2 = "date"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            r1.a = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            java.lang.String r2 = "comment"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            r1.g = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            java.lang.String r2 = "updatestatus"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00bd
            r2 = r6
        L_0x00a1:
            r1.f(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            java.lang.String r2 = "isdelete"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00bf
            r2 = r6
        L_0x00b3:
            r1.d = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00d3 }
            if (r0 == 0) goto L_0x00ba
            r0.close()
        L_0x00ba:
            r0 = r1
            goto L_0x0033
        L_0x00bd:
            r2 = r5
            goto L_0x00a1
        L_0x00bf:
            r2 = r5
            goto L_0x00b3
        L_0x00c1:
            r0 = move-exception
            r0 = r4
        L_0x00c3:
            if (r0 == 0) goto L_0x00c8
            r0.close()
        L_0x00c8:
            r0 = r4
            goto L_0x0033
        L_0x00cb:
            r0 = move-exception
            r1 = r4
        L_0x00cd:
            if (r1 == 0) goto L_0x00d2
            r1.close()
        L_0x00d2:
            throw r0
        L_0x00d3:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00cd
        L_0x00d8:
            r1 = move-exception
            goto L_0x00c3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.data.u.f(long):com.wacai.data.u");
    }

    public static void g(long j) {
        if (j > 0) {
            f(j).f();
        }
    }

    public final long a() {
        return this.a;
    }

    public final void a(long j) {
        this.a = j;
    }

    public final void a(String str) {
        this.g = str;
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str != null && str2 != null) {
            if (str.equalsIgnoreCase("r")) {
                g(str2);
            } else if (str.equalsIgnoreCase("ax")) {
                this.b = aa.a(Double.parseDouble(str2));
            } else if (str.equalsIgnoreCase("ay")) {
                this.c = aa.a(Double.parseDouble(str2));
            } else if (str.equalsIgnoreCase("as")) {
                this.e = aa.a("TBL_ACCOUNTINFO", "id", str2, 1);
            } else if (str.equalsIgnoreCase("aw")) {
                this.f = aa.a("TBL_ACCOUNTINFO", "id", str2, 1);
            } else if (str.equalsIgnoreCase("ap")) {
                this.a = Long.parseLong(str2);
            } else if (str.equalsIgnoreCase("aq")) {
                this.g = str2;
            } else if (str.equalsIgnoreCase("al")) {
                this.d = Long.parseLong(str2) > 0;
            }
        }
    }

    public final long b() {
        return this.b;
    }

    public final void b(long j) {
        this.b = j;
    }

    public final void c() {
        if (C()) {
            Object[] objArr = new Object[11];
            objArr[0] = w();
            objArr[1] = Long.valueOf(this.b);
            objArr[2] = Long.valueOf(this.c);
            objArr[3] = Long.valueOf(this.a);
            objArr[4] = Long.valueOf(this.e);
            objArr[5] = Long.valueOf(this.f);
            objArr[6] = e(this.g);
            objArr[7] = Integer.valueOf(A() ? 1 : 0);
            objArr[8] = Long.valueOf(a.a(this.a * 1000));
            objArr[9] = Integer.valueOf(this.d ? 1 : 0);
            objArr[10] = Long.valueOf(x());
            e.c().b().execSQL(String.format("UPDATE %s SET transferoutmoney = %d, transferinmoney = %d, date = %d, transferoutaccountid = %d, transferinaccountid = %d, comment = '%s', updatestatus = %d, ymd = %d, isdelete = %d WHERE id = %d", objArr));
            return;
        }
        Object[] objArr2 = new Object[11];
        objArr2[0] = w();
        objArr2[1] = Long.valueOf(this.b);
        objArr2[2] = Long.valueOf(this.c);
        objArr2[3] = Long.valueOf(this.a);
        objArr2[4] = Long.valueOf(this.e);
        objArr2[5] = Long.valueOf(this.f);
        objArr2[6] = e(this.g);
        objArr2[7] = Integer.valueOf(this.d ? 1 : 0);
        objArr2[8] = Integer.valueOf(A() ? 1 : 0);
        objArr2[9] = Long.valueOf(a.a(this.a * 1000));
        objArr2[10] = B();
        e.c().b().execSQL(String.format("INSERT INTO %s (transferoutmoney, transferinmoney, date, transferoutaccountid, transferinaccountid, comment, isdelete, updatestatus, ymd, uuid) VALUES (%d, %d, %d, %d, %d, '%s', %d, %d, %d, '%s')", objArr2));
        k((long) d(w()));
    }

    public final void c(long j) {
        this.c = j;
    }

    public final long d() {
        return this.c;
    }

    public final void d(long j) {
        this.e = j;
    }

    public final long e() {
        return this.e;
    }

    public final void e(long j) {
        this.f = j;
    }

    public final void f() {
        if (x() > 0) {
            if (B() == null || B().length() <= 0) {
                a("TBL_TRANSFERINFO", x());
                return;
            }
            this.d = true;
            f(false);
            c();
        }
    }

    public final long g() {
        return this.f;
    }

    public final String h() {
        return this.g;
    }
}
