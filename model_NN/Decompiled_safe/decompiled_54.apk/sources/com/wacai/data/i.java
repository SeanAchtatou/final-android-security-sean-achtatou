package com.wacai.data;

import android.database.Cursor;
import com.wacai.e;
import org.w3c.dom.Element;

public final class i extends ab {
    public String a = "";
    public long b = 0;
    public boolean c = true;
    private long d = 0;

    public i() {
        super("TBL_NEWSTYPE");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab
     arg types: [org.w3c.dom.Element, com.wacai.data.i, int]
     candidates:
      com.wacai.data.aa.a(java.lang.StringBuffer, int, boolean):int
      com.wacai.data.aa.a(java.lang.String, java.lang.String, int):java.lang.String
      com.wacai.data.aa.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai.data.ab.a(org.w3c.dom.Element, com.wacai.data.ab, boolean):com.wacai.data.ab */
    public static i a(Element element) {
        if (element == null) {
            return null;
        }
        return (i) ab.a(element, (ab) new i(), false);
    }

    public static void a(StringBuffer stringBuffer) {
        Cursor cursor;
        try {
            Cursor rawQuery = e.c().b().rawQuery(String.format("select * from TBL_NEWSTYPE where enable != 0", new Object[0]), null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        do {
                            String string = rawQuery.getString(rawQuery.getColumnIndexOrThrow("uuid"));
                            if (string != null) {
                                stringBuffer.append("<wac-uuid>");
                                stringBuffer.append(string);
                                stringBuffer.append("</wac-uuid>");
                            }
                        } while (rawQuery.moveToNext());
                        if (rawQuery != null) {
                            rawQuery.close();
                            return;
                        }
                        return;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
                return;
            }
            return;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        if (str != null && str2 != null) {
            if (str.equalsIgnoreCase("r")) {
                g(str2);
            } else if (str.equalsIgnoreCase("s")) {
                this.a = str2;
            } else if (str.equalsIgnoreCase("w")) {
                this.d = Long.parseLong(str2);
            } else if (str.equalsIgnoreCase("at")) {
                this.b = Long.parseLong(str2);
            }
        }
    }

    public final void c() {
        if (C()) {
            Object[] objArr = new Object[7];
            objArr[0] = w();
            objArr[1] = e(this.a);
            objArr[2] = Long.valueOf(this.b);
            objArr[3] = B();
            objArr[4] = Long.valueOf(this.d);
            objArr[5] = Integer.valueOf(this.c ? 1 : 0);
            objArr[6] = Long.valueOf(x());
            e.c().b().execSQL(String.format("UPDATE %s SET name = '%s', mandatory = %d, uuid = '%s', orderno = %d, enable = %d WHERE id = %d", objArr));
            return;
        }
        Object[] objArr2 = new Object[6];
        objArr2[0] = w();
        objArr2[1] = e(this.a);
        objArr2[2] = Long.valueOf(this.b);
        objArr2[3] = B();
        objArr2[4] = Long.valueOf(this.d);
        objArr2[5] = Integer.valueOf(this.c ? 1 : 0);
        e.c().b().execSQL(String.format("INSERT INTO %s (name, mandatory, uuid, orderno, enable) VALUES ('%s', %d, '%s', %d, %d)", objArr2));
        k((long) d(w()));
    }
}
