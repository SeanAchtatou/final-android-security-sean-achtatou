package com.wacai;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.telephony.TelephonyManager;
import com.wacai.data.aa;

public final class b {
    public static boolean a = false;
    private static b b;
    private String c = "";
    private String d = "";
    private String e = "";
    private String f = "";
    private boolean g = false;
    private String h = "";
    private long i;
    private long j;
    private long k = 1;
    private String l;

    private b() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0036  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long a(long r10, long r12, boolean r14) {
        /*
            r7 = 0
            r6 = 0
            r0 = 10000(0x2710, double:4.9407E-320)
            long r0 = r0 * r10
            r2 = 100
            long r2 = r2 * r12
            long r0 = r0 + r2
            com.wacai.e r2 = com.wacai.e.c()     // Catch:{ Exception -> 0x0043, all -> 0x004c }
            android.database.sqlite.SQLiteDatabase r2 = r2.b()     // Catch:{ Exception -> 0x0043, all -> 0x004c }
            java.lang.String r3 = "select modifytime from TBL_UPDATETIME where recorddate = %d"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0043, all -> 0x004c }
            r5 = 0
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ Exception -> 0x0043, all -> 0x004c }
            r4[r5] = r0     // Catch:{ Exception -> 0x0043, all -> 0x004c }
            java.lang.String r0 = java.lang.String.format(r3, r4)     // Catch:{ Exception -> 0x0043, all -> 0x004c }
            r1 = 0
            android.database.Cursor r0 = r2.rawQuery(r0, r1)     // Catch:{ Exception -> 0x0043, all -> 0x004c }
            if (r0 == 0) goto L_0x005f
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x0059, all -> 0x0054 }
            if (r1 == 0) goto L_0x005f
            r1 = 0
            long r1 = r0.getLong(r1)     // Catch:{ Exception -> 0x0059, all -> 0x0054 }
        L_0x0034:
            if (r0 == 0) goto L_0x005d
            r0.close()
            r0 = r1
        L_0x003a:
            if (r14 == 0) goto L_0x0042
            int r2 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r2 != 0) goto L_0x0042
            r0 = -1
        L_0x0042:
            return r0
        L_0x0043:
            r0 = move-exception
            r0 = r6
        L_0x0045:
            if (r0 == 0) goto L_0x005b
            r0.close()
            r0 = r7
            goto L_0x003a
        L_0x004c:
            r0 = move-exception
            r1 = r6
        L_0x004e:
            if (r1 == 0) goto L_0x0053
            r1.close()
        L_0x0053:
            throw r0
        L_0x0054:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x004e
        L_0x0059:
            r1 = move-exception
            goto L_0x0045
        L_0x005b:
            r0 = r7
            goto L_0x003a
        L_0x005d:
            r0 = r1
            goto L_0x003a
        L_0x005f:
            r1 = r7
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.b.a(long, long, boolean):long");
    }

    public static String a() {
        try {
            Context a2 = e.c().a();
            return a2.getPackageManager().getPackageInfo(a2.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            return "unknown";
        }
    }

    public static void a(long j2, long j3) {
        SQLiteDatabase b2 = e.c().b();
        if (b2 != null) {
            b2.execSQL(String.format("delete from TBL_UPDATETIME where recorddate = %d", Long.valueOf((10000 * j2) + (100 * j3))));
        }
    }

    public static void a(long j2, long j3, long j4) {
        Cursor cursor;
        long j5 = (10000 * j2) + (100 * j3);
        String format = String.format("select * from TBL_UPDATETIME where recorddate = %d", Long.valueOf(j5));
        SQLiteDatabase b2 = e.c().b();
        if (b2 != null) {
            try {
                Cursor rawQuery = b2.rawQuery(format, null);
                try {
                    b2.execSQL(rawQuery.getCount() > 0 ? String.format("update TBL_UPDATETIME set modifytime = %d where recorddate = %d", Long.valueOf(j4), Long.valueOf(j5)) : String.format("insert into TBL_UPDATETIME (recorddate, modifytime) VALUES (%d, %d)", Long.valueOf(j5), Long.valueOf(j4)));
                    if (rawQuery != null) {
                        rawQuery.close();
                        return;
                    }
                    return;
                } catch (Throwable th) {
                    th = th;
                    cursor = rawQuery;
                }
            } catch (Throwable th2) {
                th = th2;
                cursor = null;
            }
        } else {
            return;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00c6 A[SYNTHETIC, Splitter:B:40:0x00c6] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized com.wacai.b m() {
        /*
            r5 = 0
            java.lang.Class<com.wacai.b> r0 = com.wacai.b.class
            monitor-enter(r0)
            com.wacai.b r1 = com.wacai.b.b     // Catch:{ all -> 0x00bf }
            if (r1 != 0) goto L_0x00b1
            com.wacai.b r1 = new com.wacai.b     // Catch:{ all -> 0x00bf }
            r1.<init>()     // Catch:{ all -> 0x00bf }
            com.wacai.b.b = r1     // Catch:{ all -> 0x00bf }
            com.wacai.e r2 = com.wacai.e.c()     // Catch:{ Exception -> 0x00b7, all -> 0x00c2 }
            android.database.sqlite.SQLiteDatabase r2 = r2.b()     // Catch:{ Exception -> 0x00b7, all -> 0x00c2 }
            java.lang.String r3 = "select * from TBL_USERINFO"
            r4 = 0
            android.database.Cursor r2 = r2.rawQuery(r3, r4)     // Catch:{ Exception -> 0x00b7, all -> 0x00c2 }
            if (r2 == 0) goto L_0x00a9
            boolean r3 = r2.moveToFirst()     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            if (r3 == 0) goto L_0x00a9
            java.lang.String r3 = "username"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            java.lang.String r3 = r3.trim()     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            r1.c = r3     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            java.lang.String r3 = "password"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            r1.d = r3     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            java.lang.String r3 = "basemodifytime"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            long r3 = r2.getLong(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            r1.i = r3     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            java.lang.String r3 = "newsbasemodifytime"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            long r3 = r2.getLong(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            r1.j = r3     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            java.lang.String r3 = "defaultmoneytype"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            long r3 = r2.getLong(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            r1.k = r3     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            java.lang.String r3 = "isneedlocalpassword"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            int r3 = r2.getInt(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            if (r3 == 0) goto L_0x00b5
            r3 = 1
        L_0x0073:
            r1.g = r3     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            java.lang.String r3 = "localpassword"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            r1.h = r3     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            java.lang.String r3 = "uid"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            r1.e = r3     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            java.lang.String r3 = r1.e     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            if (r3 != 0) goto L_0x0095
            java.lang.String r3 = ""
            r1.e = r3     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
        L_0x0095:
            java.lang.String r3 = "email"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            r1.f = r3     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            java.lang.String r3 = r1.f     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
            if (r3 != 0) goto L_0x00a9
            java.lang.String r3 = ""
            r1.f = r3     // Catch:{ Exception -> 0x00cc, all -> 0x00ca }
        L_0x00a9:
            if (r2 == 0) goto L_0x00ae
            r2.close()     // Catch:{ all -> 0x00bf }
        L_0x00ae:
            o()     // Catch:{ all -> 0x00bf }
        L_0x00b1:
            com.wacai.b r1 = com.wacai.b.b     // Catch:{ all -> 0x00bf }
            monitor-exit(r0)
            return r1
        L_0x00b5:
            r3 = 0
            goto L_0x0073
        L_0x00b7:
            r1 = move-exception
            r1 = r5
        L_0x00b9:
            if (r1 == 0) goto L_0x00ae
            r1.close()     // Catch:{ all -> 0x00bf }
            goto L_0x00ae
        L_0x00bf:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x00c2:
            r1 = move-exception
            r2 = r5
        L_0x00c4:
            if (r2 == 0) goto L_0x00c9
            r2.close()     // Catch:{ all -> 0x00bf }
        L_0x00c9:
            throw r1     // Catch:{ all -> 0x00bf }
        L_0x00ca:
            r1 = move-exception
            goto L_0x00c4
        L_0x00cc:
            r1 = move-exception
            r1 = r2
            goto L_0x00b9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.b.m():com.wacai.b");
    }

    static synchronized void n() {
        synchronized (b.class) {
            b = null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x003c A[Catch:{ Exception -> 0x005f }] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void o() {
        /*
            r7 = 0
            r6 = 0
            r5 = 1
            r4 = 0
            java.lang.String r0 = "select count(distinct moneytype) as ct from tbl_accountinfo where moneytype <> %d"
            java.lang.Object[] r1 = new java.lang.Object[r5]
            com.wacai.b r2 = m()
            long r2 = r2.k
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            r1[r4] = r2
            java.lang.String r0 = java.lang.String.format(r0, r1)
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x0047, all -> 0x0052 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x0047, all -> 0x0052 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x0047, all -> 0x0052 }
            if (r0 == 0) goto L_0x0061
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x005f }
            if (r1 == 0) goto L_0x0061
            java.lang.String r1 = "ct"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x005f }
            long r1 = r0.getLong(r1)     // Catch:{ Exception -> 0x005f }
        L_0x0038:
            int r1 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r1 <= 0) goto L_0x0045
            r1 = r5
        L_0x003d:
            com.wacai.b.a = r1     // Catch:{ Exception -> 0x005f }
            if (r0 == 0) goto L_0x0044
            r0.close()
        L_0x0044:
            return
        L_0x0045:
            r1 = r4
            goto L_0x003d
        L_0x0047:
            r0 = move-exception
            r0 = r6
        L_0x0049:
            r1 = 0
            com.wacai.b.a = r1     // Catch:{ all -> 0x005a }
            if (r0 == 0) goto L_0x0044
            r0.close()
            goto L_0x0044
        L_0x0052:
            r0 = move-exception
            r1 = r6
        L_0x0054:
            if (r1 == 0) goto L_0x0059
            r1.close()
        L_0x0059:
            throw r0
        L_0x005a:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0054
        L_0x005f:
            r1 = move-exception
            goto L_0x0049
        L_0x0061:
            r1 = r7
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.b.o():void");
    }

    public final void a(long j2) {
        this.i = j2;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final void a(boolean z) {
        this.g = z;
        if (!this.g) {
            this.h = "";
        }
    }

    public final long b(boolean z) {
        if (!z || this.i != 0) {
            return this.i;
        }
        return -1;
    }

    public final String b() {
        return this.c;
    }

    public final void b(long j2) {
        this.j = j2;
    }

    public final void b(String str) {
        this.d = str;
    }

    public final String c() {
        return this.d;
    }

    public final void c(long j2) {
        this.k = j2;
    }

    public final void c(String str) {
        this.e = str;
    }

    public final String d() {
        return this.e;
    }

    public final void d(String str) {
        if (str != null) {
            this.f = str;
        }
    }

    public final String e() {
        return this.f;
    }

    public final void e(String str) {
        this.h = str;
        a(str != null && str.length() > 0);
    }

    public final void f(String str) {
        this.k = aa.a("TBL_MONEYTYPE", "id", str, 1);
    }

    public final boolean f() {
        return this.g;
    }

    public final String g() {
        return this.h;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0046  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final long h() {
        /*
            r6 = this;
            r3 = 0
            java.lang.String r0 = "select defaultmodifytime from TBL_USERINFO where defaultmodifytime IS NOT NULL"
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x0038, all -> 0x0042 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x0038, all -> 0x0042 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x0038, all -> 0x0042 }
            if (r0 == 0) goto L_0x0018
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x004f, all -> 0x004a }
            if (r1 != 0) goto L_0x0021
        L_0x0018:
            long r1 = r6.i     // Catch:{ Exception -> 0x004f, all -> 0x004a }
            if (r0 == 0) goto L_0x001f
            r0.close()
        L_0x001f:
            r0 = r1
        L_0x0020:
            return r0
        L_0x0021:
            r1 = 0
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x004f, all -> 0x004a }
            java.sql.Date r1 = java.sql.Date.valueOf(r1)     // Catch:{ Exception -> 0x004f, all -> 0x004a }
            long r1 = r1.getTime()     // Catch:{ Exception -> 0x004f, all -> 0x004a }
            r3 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 / r3
            if (r0 == 0) goto L_0x0036
            r0.close()
        L_0x0036:
            r0 = r1
            goto L_0x0020
        L_0x0038:
            r0 = move-exception
            r0 = r3
        L_0x003a:
            if (r0 == 0) goto L_0x003f
            r0.close()
        L_0x003f:
            r0 = -1
            goto L_0x0020
        L_0x0042:
            r0 = move-exception
            r1 = r3
        L_0x0044:
            if (r1 == 0) goto L_0x0049
            r1.close()
        L_0x0049:
            throw r0
        L_0x004a:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0044
        L_0x004f:
            r1 = move-exception
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.b.h():long");
    }

    public final long i() {
        return this.j;
    }

    public final long j() {
        return this.k;
    }

    public final String k() {
        if (this.l == null) {
            try {
                this.l = ((TelephonyManager) e.c().a().getSystemService("phone")).getDeviceId();
            } catch (Exception e2) {
            }
        }
        return this.l;
    }

    public final void l() {
        Object[] objArr = new Object[9];
        objArr[0] = aa.e(this.c.trim());
        objArr[1] = aa.e(this.d);
        objArr[2] = Long.valueOf(this.i);
        objArr[3] = Long.valueOf(this.j);
        objArr[4] = Long.valueOf(this.k);
        objArr[5] = Integer.valueOf(this.g ? 1 : 0);
        objArr[6] = this.g ? aa.e(this.h) : "";
        objArr[7] = this.e;
        objArr[8] = this.f;
        e.c().b().execSQL(String.format("update tbl_userinfo set username = '%s', password = '%s', basemodifytime = %d, newsbasemodifytime = %d, defaultmoneytype = %d, isneedlocalpassword = %d, localpassword = '%s', uid = '%s', email = '%s'", objArr));
        o();
    }

    public final boolean p() {
        return this.e != null && this.e.length() > 0 && (this.c == null || this.c.length() <= 0);
    }
}
