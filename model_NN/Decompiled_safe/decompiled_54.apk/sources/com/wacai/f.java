package com.wacai;

import com.wacai.data.ab;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class f {
    private static f a = null;
    private String b = "0";
    private String c = "";
    private String d = null;
    private String e = "";

    private f() {
    }

    private void a(InputStream inputStream) {
        NodeList elementsByTagName;
        if (inputStream != null && (elementsByTagName = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream).getElementsByTagName("configs")) != null && elementsByTagName.getLength() > 0) {
            Node item = elementsByTagName.item(0);
            if (Element.class.isInstance(item)) {
                NodeList childNodes = item.getChildNodes();
                int length = childNodes.getLength();
                for (int i = 0; i < length; i++) {
                    Node item2 = childNodes.item(i);
                    if (item2 != null && Element.class.isInstance(item2)) {
                        String nodeName = item2.getNodeName();
                        String a2 = ab.a(item2);
                        if (nodeName != null && a2.length() > 0) {
                            if ("platform".equalsIgnoreCase(nodeName)) {
                                e.b = a2;
                            } else if ("server_url".equalsIgnoreCase(nodeName)) {
                                e.a = a2;
                            } else if ("mc".equalsIgnoreCase(nodeName)) {
                                this.b = a2;
                            } else if ("build_ver".equalsIgnoreCase(nodeName)) {
                                this.c = a2;
                            } else if ("locale".equalsIgnoreCase(nodeName)) {
                                this.d = a2;
                            } else if ("wacaiserviceurl".equalsIgnoreCase(nodeName)) {
                                this.e = a2;
                            }
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0076, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0077, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x009a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x009b, code lost:
        r1 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0050 A[SYNTHETIC, Splitter:B:22:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0055 A[SYNTHETIC, Splitter:B:25:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0076 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0002] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x007a A[SYNTHETIC, Splitter:B:43:0x007a] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x007f A[SYNTHETIC, Splitter:B:46:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:67:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(java.lang.String r7) {
        /*
            r2 = 0
            r3 = 0
            com.wacai.e r0 = com.wacai.e.c()     // Catch:{ Exception -> 0x009a, all -> 0x0076 }
            android.content.Context r0 = r0.a()     // Catch:{ Exception -> 0x009a, all -> 0x0076 }
            android.content.res.AssetManager r0 = r0.getAssets()     // Catch:{ Exception -> 0x009a, all -> 0x0076 }
            java.lang.String r1 = "config.xml"
            java.io.InputStream r0 = r0.open(r1)     // Catch:{ Exception -> 0x00a3, all -> 0x0076 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x009d, all -> 0x008d }
            r1.<init>(r7)     // Catch:{ Exception -> 0x009d, all -> 0x008d }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x002e, all -> 0x0093 }
        L_0x001d:
            r4 = -1
            if (r3 == r4) goto L_0x0059
            r3 = 0
            r4 = 1024(0x400, float:1.435E-42)
            int r3 = r0.read(r2, r3, r4)     // Catch:{ Exception -> 0x002e, all -> 0x0093 }
            if (r3 <= 0) goto L_0x001d
            r4 = 0
            r1.write(r2, r4, r3)     // Catch:{ Exception -> 0x002e, all -> 0x0093 }
            goto L_0x001d
        L_0x002e:
            r2 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
        L_0x0032:
            java.lang.String r3 = "Config"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0098 }
            r4.<init>()     // Catch:{ all -> 0x0098 }
            java.lang.String r5 = "initConfigFile: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0098 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0098 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0098 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0098 }
            android.util.Log.i(r3, r0)     // Catch:{ all -> 0x0098 }
            if (r1 == 0) goto L_0x0053
            r1.close()     // Catch:{ IOException -> 0x006c }
        L_0x0053:
            if (r2 == 0) goto L_0x0058
            r2.close()     // Catch:{ IOException -> 0x0071 }
        L_0x0058:
            return
        L_0x0059:
            r1.close()     // Catch:{ IOException -> 0x0067 }
        L_0x005c:
            if (r0 == 0) goto L_0x0058
            r0.close()     // Catch:{ IOException -> 0x0062 }
            goto L_0x0058
        L_0x0062:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0058
        L_0x0067:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005c
        L_0x006c:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0053
        L_0x0071:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0058
        L_0x0076:
            r0 = move-exception
            r1 = r2
        L_0x0078:
            if (r1 == 0) goto L_0x007d
            r1.close()     // Catch:{ IOException -> 0x0083 }
        L_0x007d:
            if (r2 == 0) goto L_0x0082
            r2.close()     // Catch:{ IOException -> 0x0088 }
        L_0x0082:
            throw r0
        L_0x0083:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x007d
        L_0x0088:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0082
        L_0x008d:
            r1 = move-exception
            r6 = r1
            r1 = r2
            r2 = r0
            r0 = r6
            goto L_0x0078
        L_0x0093:
            r2 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
            goto L_0x0078
        L_0x0098:
            r0 = move-exception
            goto L_0x0078
        L_0x009a:
            r0 = move-exception
            r1 = r2
            goto L_0x0032
        L_0x009d:
            r1 = move-exception
            r6 = r1
            r1 = r2
            r2 = r0
            r0 = r6
            goto L_0x0032
        L_0x00a3:
            r0 = move-exception
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.f.a(java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0057 A[SYNTHETIC, Splitter:B:16:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x007a A[SYNTHETIC, Splitter:B:40:0x007a] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:19:0x005d=Splitter:B:19:0x005d, B:42:0x007d=Splitter:B:42:0x007d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized com.wacai.f e() {
        /*
            r5 = 0
            java.lang.Class<com.wacai.f> r0 = com.wacai.f.class
            monitor-enter(r0)
            com.wacai.f r1 = com.wacai.f.a     // Catch:{ all -> 0x0066 }
            if (r1 != 0) goto L_0x005d
            com.wacai.f r1 = new com.wacai.f     // Catch:{ all -> 0x0066 }
            r1.<init>()     // Catch:{ all -> 0x0066 }
            com.wacai.f.a = r1     // Catch:{ all -> 0x0066 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0066 }
            r2.<init>()     // Catch:{ all -> 0x0066 }
            r3 = 0
            java.lang.String r3 = com.wacai.e.b(r3)     // Catch:{ all -> 0x0066 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0066 }
            java.lang.String r3 = "/"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0066 }
            java.lang.String r3 = "config.xml"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0066 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0066 }
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x0066 }
            r3.<init>(r2)     // Catch:{ all -> 0x0066 }
            boolean r3 = r3.exists()     // Catch:{ all -> 0x0066 }
            if (r3 != 0) goto L_0x003b
            a(r2)     // Catch:{ all -> 0x0066 }
        L_0x003b:
            com.wacai.e r3 = com.wacai.e.c()     // Catch:{ Exception -> 0x0069, all -> 0x0076 }
            android.content.Context r3 = r3.a()     // Catch:{ Exception -> 0x0069, all -> 0x0076 }
            android.content.res.AssetManager r3 = r3.getAssets()     // Catch:{ Exception -> 0x0069, all -> 0x0076 }
            java.lang.String r4 = "config_flush.xml"
            java.io.InputStream r3 = r3.open(r4)     // Catch:{ Exception -> 0x0069, all -> 0x0076 }
            r1.a(r3)     // Catch:{ Exception -> 0x00b4, all -> 0x00b1 }
            if (r3 == 0) goto L_0x0055
            r3.close()     // Catch:{ IOException -> 0x0061 }
        L_0x0055:
            if (r2 == 0) goto L_0x005d
            int r3 = r2.length()     // Catch:{ Exception -> 0x0093 }
            if (r3 > 0) goto L_0x0083
        L_0x005d:
            com.wacai.f r1 = com.wacai.f.a     // Catch:{ all -> 0x0066 }
            monitor-exit(r0)
            return r1
        L_0x0061:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x0066 }
            goto L_0x0055
        L_0x0066:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x0069:
            r3 = move-exception
            r3 = r5
        L_0x006b:
            if (r3 == 0) goto L_0x0055
            r3.close()     // Catch:{ IOException -> 0x0071 }
            goto L_0x0055
        L_0x0071:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x0066 }
            goto L_0x0055
        L_0x0076:
            r1 = move-exception
            r2 = r5
        L_0x0078:
            if (r2 == 0) goto L_0x007d
            r2.close()     // Catch:{ IOException -> 0x007e }
        L_0x007d:
            throw r1     // Catch:{ all -> 0x0066 }
        L_0x007e:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x0066 }
            goto L_0x007d
        L_0x0083:
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0093 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0093 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x0093 }
            r2 = 8192(0x2000, float:1.14794E-41)
            r3.<init>(r4, r2)     // Catch:{ Exception -> 0x0093 }
            r1.a(r3)     // Catch:{ Exception -> 0x0093 }
            goto L_0x005d
        L_0x0093:
            r1 = move-exception
            java.lang.String r2 = "Config"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0066 }
            r3.<init>()     // Catch:{ all -> 0x0066 }
            java.lang.String r4 = "init: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0066 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0066 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x0066 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0066 }
            android.util.Log.e(r2, r1)     // Catch:{ all -> 0x0066 }
            goto L_0x005d
        L_0x00b1:
            r1 = move-exception
            r2 = r3
            goto L_0x0078
        L_0x00b4:
            r4 = move-exception
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.f.e():com.wacai.f");
    }

    public final String a() {
        return this.b;
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.d;
    }

    public final String d() {
        return this.e;
    }
}
