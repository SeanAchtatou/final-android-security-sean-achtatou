package com.wacai;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.io.File;
import java.util.Date;

public final class e {
    public static String a = "http://www.wacai.com";
    public static String b = "2";
    public static String c = "3.0";
    private static String d;
    private static String e;
    private static e f = null;
    private Context g;
    private SQLiteDatabase h = null;
    private d i;

    private e() {
    }

    public static void a(String str) {
        if (str != null) {
            c().b().execSQL(String.format("update TBL_USERINFO set appversion = '%s'", str));
        }
    }

    public static String b(Context context) {
        if (d != null) {
            return d;
        }
        Context context2 = context == null ? c().g : context;
        try {
            String str = context2.getPackageManager().getPackageInfo(context2.getPackageName(), 0).applicationInfo.dataDir;
            d = str;
            return str;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e("Frame", "getDataPath exception: " + e2.getMessage());
            return "/data/data/com.wacai365";
        }
    }

    public static synchronized e c() {
        e eVar;
        synchronized (e.class) {
            if (f == null) {
                f = new e();
            }
            eVar = f;
        }
        return eVar;
    }

    private static String d(Context context) {
        if (e == null) {
            e = b(context) + "/" + "wacai365.so";
        }
        return e;
    }

    public static synchronized void e() {
        synchronized (e.class) {
            if (f != null) {
                e eVar = f;
                if (eVar.h != null && eVar.h.isOpen()) {
                    eVar.h.close();
                    eVar.h = null;
                }
                f = null;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x006f A[SYNTHETIC, Splitter:B:27:0x006f] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0074 A[Catch:{ IOException -> 0x0088 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0092 A[SYNTHETIC, Splitter:B:44:0x0092] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0097 A[Catch:{ IOException -> 0x009b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean e(android.content.Context r9) {
        /*
            r8 = this;
            r3 = 0
            r6 = 0
            android.database.sqlite.SQLiteDatabase r0 = r8.h     // Catch:{ Exception -> 0x00ab, all -> 0x008d }
            if (r0 == 0) goto L_0x0016
            android.database.sqlite.SQLiteDatabase r0 = r8.h     // Catch:{ Exception -> 0x00ab, all -> 0x008d }
            boolean r0 = r0.isOpen()     // Catch:{ Exception -> 0x00ab, all -> 0x008d }
            if (r0 == 0) goto L_0x0016
            android.database.sqlite.SQLiteDatabase r0 = r8.h     // Catch:{ Exception -> 0x00ab, all -> 0x008d }
            r0.close()     // Catch:{ Exception -> 0x00ab, all -> 0x008d }
            r0 = 0
            r8.h = r0     // Catch:{ Exception -> 0x00ab, all -> 0x008d }
        L_0x0016:
            java.lang.String r0 = d(r9)     // Catch:{ Exception -> 0x00ab, all -> 0x008d }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x00ab, all -> 0x008d }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00ab, all -> 0x008d }
            boolean r2 = r1.exists()     // Catch:{ Exception -> 0x00ab, all -> 0x008d }
            if (r2 == 0) goto L_0x0028
            r1.delete()     // Catch:{ Exception -> 0x00ab, all -> 0x008d }
        L_0x0028:
            android.content.res.AssetManager r1 = r9.getAssets()     // Catch:{ Exception -> 0x00ab, all -> 0x008d }
            java.lang.String r2 = "wacai365.so"
            java.io.InputStream r1 = r1.open(r2)     // Catch:{ Exception -> 0x00ab, all -> 0x008d }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00af, all -> 0x00a0 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x00af, all -> 0x00a0 }
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x004d, all -> 0x00a4 }
            r3 = r6
        L_0x003c:
            r4 = -1
            if (r3 == r4) goto L_0x0079
            r3 = 0
            r4 = 1024(0x400, float:1.435E-42)
            int r3 = r1.read(r0, r3, r4)     // Catch:{ Exception -> 0x004d, all -> 0x00a4 }
            if (r3 <= 0) goto L_0x003c
            r4 = 0
            r2.write(r0, r4, r3)     // Catch:{ Exception -> 0x004d, all -> 0x00a4 }
            goto L_0x003c
        L_0x004d:
            r0 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
        L_0x0051:
            java.lang.String r3 = "Frame"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a9 }
            r4.<init>()     // Catch:{ all -> 0x00a9 }
            java.lang.String r5 = "GenerateDB e="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00a9 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00a9 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x00a9 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00a9 }
            android.util.Log.e(r3, r0)     // Catch:{ all -> 0x00a9 }
            if (r1 == 0) goto L_0x0072
            r1.close()     // Catch:{ IOException -> 0x0088 }
        L_0x0072:
            if (r2 == 0) goto L_0x0077
            r2.close()     // Catch:{ IOException -> 0x0088 }
        L_0x0077:
            r0 = r6
        L_0x0078:
            return r0
        L_0x0079:
            r2.close()     // Catch:{ IOException -> 0x0083 }
            if (r1 == 0) goto L_0x0081
            r1.close()     // Catch:{ IOException -> 0x0083 }
        L_0x0081:
            r0 = 1
            goto L_0x0078
        L_0x0083:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0081
        L_0x0088:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0077
        L_0x008d:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0090:
            if (r1 == 0) goto L_0x0095
            r1.close()     // Catch:{ IOException -> 0x009b }
        L_0x0095:
            if (r2 == 0) goto L_0x009a
            r2.close()     // Catch:{ IOException -> 0x009b }
        L_0x009a:
            throw r0
        L_0x009b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x009a
        L_0x00a0:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0090
        L_0x00a4:
            r0 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x0090
        L_0x00a9:
            r0 = move-exception
            goto L_0x0090
        L_0x00ab:
            r0 = move-exception
            r1 = r3
            r2 = r3
            goto L_0x0051
        L_0x00af:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.e.e(android.content.Context):boolean");
    }

    public final Context a() {
        return this.g;
    }

    public final void a(Context context) {
        if (context != null) {
            this.g = context.getApplicationContext();
        }
    }

    public final void a(d dVar) {
        this.i = dVar;
    }

    public final synchronized SQLiteDatabase b() {
        if (this.h == null || !this.h.isOpen()) {
            try {
                this.h = SQLiteDatabase.openDatabase(d(this.g), null, 0);
            } catch (Exception e2) {
                Log.e("Frame", "Frame Open DB: error = " + e2.getMessage());
            }
        }
        return this.h;
    }

    public final synchronized boolean c(Context context) {
        boolean z;
        if (context == null) {
            z = false;
        } else {
            this.g = context;
            if (new File(d(this.g)).exists()) {
                c.a(b(), this.i);
            } else if (!e(context)) {
                z = false;
            }
            f.e();
            z = true;
        }
        return z;
    }

    public final synchronized void d() {
        long a2 = a.a("PropActivated", 0);
        long a3 = a.a("PropNextUpdateTime", new Date().getTime() + 1296000000);
        e(this.g);
        a.b("PropAcceptCopyright", 1);
        a.b("PropNextUpdateTime", a3);
        a.b("PropActivated", a2);
        b.n();
    }
}
