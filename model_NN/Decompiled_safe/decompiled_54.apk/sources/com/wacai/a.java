package com.wacai;

public final class a {
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long a(java.lang.String r5, long r6) {
        /*
            r3 = 0
            java.lang.String r0 = "select propertyvalue from TBL_USERPROFILE where propertyname = '%s'"
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0041, all -> 0x004a }
            r2 = 0
            r1[r2] = r5     // Catch:{ Exception -> 0x0041, all -> 0x004a }
            java.lang.String r0 = java.lang.String.format(r0, r1)     // Catch:{ Exception -> 0x0041, all -> 0x004a }
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x0041, all -> 0x004a }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x0041, all -> 0x004a }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x0041, all -> 0x004a }
            if (r0 == 0) goto L_0x0022
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x0057, all -> 0x0052 }
            if (r1 > 0) goto L_0x0029
        L_0x0022:
            if (r0 == 0) goto L_0x0027
            r0.close()
        L_0x0027:
            r0 = r6
        L_0x0028:
            return r0
        L_0x0029:
            r0.moveToFirst()     // Catch:{ Exception -> 0x0057, all -> 0x0052 }
            java.lang.String r1 = "propertyvalue"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x0057, all -> 0x0052 }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x0057, all -> 0x0052 }
            long r1 = java.lang.Long.parseLong(r1)     // Catch:{ Exception -> 0x0057, all -> 0x0052 }
            if (r0 == 0) goto L_0x003f
            r0.close()
        L_0x003f:
            r0 = r1
            goto L_0x0028
        L_0x0041:
            r0 = move-exception
            r0 = r3
        L_0x0043:
            if (r0 == 0) goto L_0x0048
            r0.close()
        L_0x0048:
            r0 = r6
            goto L_0x0028
        L_0x004a:
            r0 = move-exception
            r1 = r3
        L_0x004c:
            if (r1 == 0) goto L_0x0051
            r1.close()
        L_0x0051:
            throw r0
        L_0x0052:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x004c
        L_0x0057:
            r1 = move-exception
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.a.a(java.lang.String, long):long");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void b(java.lang.String r7, long r8) {
        /*
            r4 = 0
            com.wacai.e r0 = com.wacai.e.c()     // Catch:{ all -> 0x006f }
            android.database.sqlite.SQLiteDatabase r0 = r0.b()     // Catch:{ all -> 0x006f }
            java.lang.String r1 = "select propertyvalue from TBL_USERPROFILE where propertyname = '%s'"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x006f }
            r3 = 0
            r2[r3] = r7     // Catch:{ all -> 0x006f }
            java.lang.String r1 = java.lang.String.format(r1, r2)     // Catch:{ all -> 0x006f }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ all -> 0x006f }
            if (r0 == 0) goto L_0x0046
            int r1 = r0.getCount()     // Catch:{ all -> 0x0065 }
            if (r1 <= 0) goto L_0x0046
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ all -> 0x0065 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ all -> 0x0065 }
            java.lang.String r2 = "update TBL_USERPROFILE set propertyvalue = '%d' where propertyname = '%s'"
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0065 }
            r4 = 0
            java.lang.Long r5 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x0065 }
            r3[r4] = r5     // Catch:{ all -> 0x0065 }
            r4 = 1
            r3[r4] = r7     // Catch:{ all -> 0x0065 }
            java.lang.String r2 = java.lang.String.format(r2, r3)     // Catch:{ all -> 0x0065 }
            r1.execSQL(r2)     // Catch:{ all -> 0x0065 }
        L_0x0040:
            if (r0 == 0) goto L_0x0045
            r0.close()
        L_0x0045:
            return
        L_0x0046:
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ all -> 0x0065 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ all -> 0x0065 }
            java.lang.String r2 = "insert into TBL_USERPROFILE (propertyname, propertyvalue) VALUES ('%s', '%d')"
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0065 }
            r4 = 0
            r3[r4] = r7     // Catch:{ all -> 0x0065 }
            r4 = 1
            java.lang.Long r5 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x0065 }
            r3[r4] = r5     // Catch:{ all -> 0x0065 }
            java.lang.String r2 = java.lang.String.format(r2, r3)     // Catch:{ all -> 0x0065 }
            r1.execSQL(r2)     // Catch:{ all -> 0x0065 }
            goto L_0x0040
        L_0x0065:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0069:
            if (r1 == 0) goto L_0x006e
            r1.close()
        L_0x006e:
            throw r0
        L_0x006f:
            r0 = move-exception
            r1 = r4
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.a.b(java.lang.String, long):void");
    }
}
