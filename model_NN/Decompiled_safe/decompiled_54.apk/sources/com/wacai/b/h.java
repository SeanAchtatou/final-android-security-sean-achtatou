package com.wacai.b;

import com.wacai.data.VersionItem;
import com.wacai.data.ab;
import java.util.ArrayList;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class h extends g {
    private ArrayList d = new ArrayList();

    public final void a(ArrayList arrayList) {
        this.d = arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a(Element element) {
        if (element != null) {
            NodeList elementsByTagName = element.getElementsByTagName("wac-version");
            int length = elementsByTagName.getLength();
            for (int i = 0; i < length; i++) {
                VersionItem versionItem = new VersionItem();
                Node item = elementsByTagName.item(i);
                NamedNodeMap attributes = item.getAttributes();
                if (attributes != null) {
                    int length2 = attributes.getLength();
                    for (int i2 = 0; i2 < length2; i2++) {
                        Attr attr = (Attr) attributes.item(i2);
                        if (!(attr == null || attr.getName() == null)) {
                            if (attr.getName().equals("verid")) {
                                versionItem.a = attr.getValue();
                            } else if (attr.getName().equals("description")) {
                                versionItem.b = attr.getValue();
                            } else if (attr.getName().equals("ver")) {
                                versionItem.c = attr.getValue();
                            }
                        }
                    }
                }
                versionItem.d = ab.a(item);
                this.d.add(versionItem);
            }
        }
    }
}
