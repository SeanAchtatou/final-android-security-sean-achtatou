package com.wacai.b;

import com.wacai.e;
import com.wacai365.C0000R;

public final class n extends s {
    private boolean d = false;

    private void d() {
        e(e.c().a().getResources().getString(C0000R.string.promptGenerateFixItem));
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (3) to help type inference */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0155 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x01af A[SYNTHETIC, Splitter:B:96:0x01af] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.wacai.data.ab a(java.lang.String r12, org.w3c.dom.Element r13) {
        /*
            r11 = this;
            r2 = 0
            r10 = 0
            if (r12 == 0) goto L_0x0006
            if (r13 != 0) goto L_0x0008
        L_0x0006:
            r0 = r10
        L_0x0007:
            return r0
        L_0x0008:
            java.lang.String r0 = "h"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x0026
            com.wacai.data.e r0 = com.wacai.data.e.a(r13)
        L_0x0014:
            boolean r1 = r11.d
            if (r1 == 0) goto L_0x0007
            if (r0 == 0) goto L_0x0007
            long r1 = r0.x()
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 > 0) goto L_0x0007
            r0 = r10
            goto L_0x0007
        L_0x0026:
            java.lang.String r0 = "i"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x0033
            com.wacai.data.t r0 = com.wacai.data.t.a(r13)
            goto L_0x0014
        L_0x0033:
            java.lang.String r0 = "a"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x0042
            boolean r0 = r11.d
            com.wacai.data.h r0 = com.wacai.data.h.a(r13, r0)
            goto L_0x0014
        L_0x0042:
            java.lang.String r0 = "b"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x0051
            boolean r0 = r11.d
            com.wacai.data.l r0 = com.wacai.data.l.a(r13, r0)
            goto L_0x0014
        L_0x0051:
            java.lang.String r0 = "f"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x0060
            boolean r0 = r11.d
            com.wacai.data.f r0 = com.wacai.data.f.a(r13, r0)
            goto L_0x0014
        L_0x0060:
            java.lang.String r0 = "g"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x006f
            boolean r0 = r11.d
            com.wacai.data.j r0 = com.wacai.data.j.a(r13, r0)
            goto L_0x0014
        L_0x006f:
            java.lang.String r0 = "d"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x007e
            boolean r0 = r11.d
            com.wacai.data.p r0 = com.wacai.data.p.a(r13, r0)
            goto L_0x0014
        L_0x007e:
            java.lang.String r0 = "e"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x008d
            boolean r0 = r11.d
            com.wacai.data.y r0 = com.wacai.data.y.a(r13, r0)
            goto L_0x0014
        L_0x008d:
            java.lang.String r0 = "c"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x009d
            boolean r0 = r11.d
            com.wacai.data.w r0 = com.wacai.data.w.a(r13, r0)
            goto L_0x0014
        L_0x009d:
            java.lang.String r0 = "m"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x00b6
            com.wacai.data.q r0 = com.wacai.data.q.a(r13)
            if (r0 == 0) goto L_0x0014
            boolean r1 = r0.t()
            if (r1 != 0) goto L_0x0014
            r11.d()
            goto L_0x0014
        L_0x00b6:
            java.lang.String r0 = "n"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x00cf
            com.wacai.data.g r0 = com.wacai.data.g.a(r13)
            if (r0 == 0) goto L_0x0014
            boolean r1 = r0.t()
            if (r1 != 0) goto L_0x0014
            r11.d()
            goto L_0x0014
        L_0x00cf:
            java.lang.String r0 = "bc"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x00dd
            com.wacai.data.m r0 = com.wacai.data.m.a(r13)
            goto L_0x0014
        L_0x00dd:
            java.lang.String r0 = "bk"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x00eb
            com.wacai.data.d r0 = com.wacai.data.d.a(r13)
            goto L_0x0014
        L_0x00eb:
            java.lang.String r0 = "bo"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x00f9
            com.wacai.data.n r0 = com.wacai.data.n.a(r13)
            goto L_0x0014
        L_0x00f9:
            java.lang.String r0 = "bb"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x0107
            com.wacai.data.i r0 = com.wacai.data.i.a(r13)
            goto L_0x0014
        L_0x0107:
            java.lang.String r0 = "wac-prefer"
            boolean r0 = r12.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x01e5
            org.w3c.dom.NamedNodeMap r0 = r13.getAttributes()     // Catch:{ all -> 0x01b3 }
            if (r0 != 0) goto L_0x0118
            r0 = r10
            goto L_0x0014
        L_0x0118:
            int r1 = r0.getLength()     // Catch:{ all -> 0x01b3 }
        L_0x011c:
            if (r2 >= r1) goto L_0x01d8
            org.w3c.dom.Node r3 = r0.item(r2)     // Catch:{ all -> 0x01b3 }
            java.lang.String r3 = r3.getNodeName()     // Catch:{ all -> 0x01b3 }
            org.w3c.dom.Node r4 = r0.item(r2)     // Catch:{ all -> 0x01b3 }
            java.lang.String r4 = r4.getNodeValue()     // Catch:{ all -> 0x01b3 }
            java.lang.String r5 = "key"
            boolean r3 = r3.equalsIgnoreCase(r5)     // Catch:{ all -> 0x01b3 }
            if (r3 == 0) goto L_0x0155
            java.lang.String r3 = "moneyType"
            boolean r3 = r4.equalsIgnoreCase(r3)     // Catch:{ all -> 0x01b3 }
            if (r3 == 0) goto L_0x0158
            org.w3c.dom.Node r3 = r13.getFirstChild()     // Catch:{ all -> 0x01b3 }
            if (r3 == 0) goto L_0x0155
            java.lang.String r4 = r3.getNodeValue()     // Catch:{ all -> 0x01b3 }
            if (r4 == 0) goto L_0x0155
            com.wacai.b r4 = com.wacai.b.m()     // Catch:{ all -> 0x01b3 }
            java.lang.String r3 = r3.getNodeValue()     // Catch:{ all -> 0x01b3 }
            r4.f(r3)     // Catch:{ all -> 0x01b3 }
        L_0x0155:
            int r2 = r2 + 1
            goto L_0x011c
        L_0x0158:
            java.lang.String r3 = "multiPhone"
            boolean r3 = r4.equalsIgnoreCase(r3)     // Catch:{ all -> 0x01b3 }
            if (r3 == 0) goto L_0x0155
            org.w3c.dom.Node r3 = r13.getFirstChild()     // Catch:{ all -> 0x01b3 }
            if (r3 == 0) goto L_0x0155
            java.lang.String r4 = r3.getNodeValue()     // Catch:{ all -> 0x01b3 }
            if (r4 == 0) goto L_0x0155
            java.lang.String r4 = "MultiPhoneAccount"
            java.lang.String r3 = r3.getNodeValue()     // Catch:{ all -> 0x01b3 }
            com.wacai.e r5 = com.wacai.e.c()     // Catch:{ all -> 0x01e2 }
            android.database.sqlite.SQLiteDatabase r5 = r5.b()     // Catch:{ all -> 0x01e2 }
            java.lang.String r6 = "select propertyvalue from TBL_USERPROFILE where propertyname = '%s'"
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x01e2 }
            r8 = 0
            r7[r8] = r4     // Catch:{ all -> 0x01e2 }
            java.lang.String r6 = java.lang.String.format(r6, r7)     // Catch:{ all -> 0x01e2 }
            r7 = 0
            android.database.Cursor r5 = r5.rawQuery(r6, r7)     // Catch:{ all -> 0x01e2 }
            if (r5 == 0) goto L_0x01b5
            int r6 = r5.getCount()     // Catch:{ all -> 0x01d0 }
            if (r6 <= 0) goto L_0x01b5
            com.wacai.e r6 = com.wacai.e.c()     // Catch:{ all -> 0x01d0 }
            android.database.sqlite.SQLiteDatabase r6 = r6.b()     // Catch:{ all -> 0x01d0 }
            java.lang.String r7 = "update TBL_USERPROFILE set propertyvalue = '%s' where propertyname = '%s'"
            r8 = 2
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ all -> 0x01d0 }
            r9 = 0
            r8[r9] = r3     // Catch:{ all -> 0x01d0 }
            r3 = 1
            r8[r3] = r4     // Catch:{ all -> 0x01d0 }
            java.lang.String r3 = java.lang.String.format(r7, r8)     // Catch:{ all -> 0x01d0 }
            r6.execSQL(r3)     // Catch:{ all -> 0x01d0 }
        L_0x01ad:
            if (r5 == 0) goto L_0x0155
            r5.close()     // Catch:{ all -> 0x01b3 }
            goto L_0x0155
        L_0x01b3:
            r0 = move-exception
            throw r0
        L_0x01b5:
            com.wacai.e r6 = com.wacai.e.c()     // Catch:{ all -> 0x01d0 }
            android.database.sqlite.SQLiteDatabase r6 = r6.b()     // Catch:{ all -> 0x01d0 }
            java.lang.String r7 = "insert into TBL_USERPROFILE (propertyname, propertyvalue) VALUES ('%s', '%s')"
            r8 = 2
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ all -> 0x01d0 }
            r9 = 0
            r8[r9] = r4     // Catch:{ all -> 0x01d0 }
            r4 = 1
            r8[r4] = r3     // Catch:{ all -> 0x01d0 }
            java.lang.String r3 = java.lang.String.format(r7, r8)     // Catch:{ all -> 0x01d0 }
            r6.execSQL(r3)     // Catch:{ all -> 0x01d0 }
            goto L_0x01ad
        L_0x01d0:
            r0 = move-exception
            r1 = r5
        L_0x01d2:
            if (r1 == 0) goto L_0x01d7
            r1.close()     // Catch:{ all -> 0x01b3 }
        L_0x01d7:
            throw r0     // Catch:{ all -> 0x01b3 }
        L_0x01d8:
            com.wacai.b r0 = com.wacai.b.m()     // Catch:{ all -> 0x01b3 }
            r0.l()     // Catch:{ all -> 0x01b3 }
            r0 = r10
            goto L_0x0014
        L_0x01e2:
            r0 = move-exception
            r1 = r10
            goto L_0x01d2
        L_0x01e5:
            r0 = r10
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.b.n.a(java.lang.String, org.w3c.dom.Element):com.wacai.data.ab");
    }

    public final void a(boolean z) {
        this.d = z;
    }
}
