package com.wacai.b;

import com.wacai.e;
import com.wacai365.C0000R;
import java.util.ArrayList;
import java.util.Hashtable;

public class w extends o {
    private ArrayList d = null;
    private ArrayList e = null;

    public w() {
        super(false);
    }

    public final void a(String str, String str2) {
        if (this.e == null) {
            this.e = new ArrayList();
        }
        Hashtable hashtable = new Hashtable();
        hashtable.put("TABLE", str);
        hashtable.put("ID", str2);
        this.e.add(hashtable);
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        try {
            if (this.d == null || this.d.size() < this.e.size()) {
                this.a.a = false;
                this.a.d = -7;
                this.a.c = "获取新建对象验证码失败，请稍后重新操作!";
                return false;
            }
            for (int i = 0; i < this.e.size(); i++) {
                Hashtable hashtable = (Hashtable) this.e.get(i);
                if (hashtable != null) {
                    e.c().b().execSQL(String.format("UPDATE %s SET uuid = '%s', updatestatus = 0 WHERE id = %s", hashtable.get("TABLE"), this.d.get(i), hashtable.get("ID")));
                }
            }
            return true;
        } catch (Exception e2) {
            if (this.a.a) {
                this.a.a = false;
                this.a.d = -7;
                this.a.c = e.c().a().getString(C0000R.string.txtExceptionOper);
                if (e2.getLocalizedMessage() != null) {
                    StringBuilder sb = new StringBuilder();
                    k kVar = this.a;
                    kVar.c = sb.append(kVar.c).append(e2.getLocalizedMessage()).toString();
                } else if (e2.getMessage() != null) {
                    StringBuilder sb2 = new StringBuilder();
                    k kVar2 = this.a;
                    kVar2.c = sb2.append(kVar2.c).append(e2.getMessage()).toString();
                }
            }
            return false;
        }
    }

    public final void a_(Object obj) {
        if (ArrayList.class.isInstance(obj)) {
            this.d = (ArrayList) obj;
        }
    }
}
