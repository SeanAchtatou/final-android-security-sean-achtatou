package com.wacai.b;

import android.util.Log;
import com.wacai.e;
import com.wacai365.C0000R;
import java.util.ArrayList;
import java.util.Iterator;

public final class c implements a, v {
    private int a;
    private o b;
    private ArrayList c;
    private k d;
    private e e;
    private Object f;
    private int g;

    private c() {
        this.a = -1;
        this.b = null;
        this.c = new ArrayList();
        this.d = new k();
        this.g = 0;
    }

    public c(e eVar) {
        this();
        this.e = eVar;
    }

    static /* synthetic */ void a(c cVar) {
        while (cVar.e()) {
            Log.i("TaskManager", "Run next task: " + cVar.b.getClass().getName());
            if (cVar.f != null) {
                cVar.b.a_(cVar.f);
                cVar.f = null;
            }
            String j = cVar.b.j();
            if (j != null && j.length() > 0) {
                cVar.a(cVar.b.j());
            }
            if (cVar.e != null) {
                if (cVar.c.size() == 0) {
                    cVar.e.a(1, 1);
                } else {
                    cVar.e.a(cVar.a + 1, cVar.c.size());
                }
            }
            if (cVar.b.k() == null || cVar.b.k().a(cVar.b)) {
                synchronized (cVar.b) {
                    if (cVar.b.m() && cVar.b.i()) {
                        try {
                            cVar.b.wait();
                        } catch (InterruptedException e2) {
                            Log.e("TaskManager", "action waai interruptedException!");
                        }
                    }
                }
            } else {
                cVar.d.a = false;
                cVar.d.d = -2;
                cVar.d.c = "初始化上传信息错误：" + cVar.b.getClass().getName() + cVar.b.j();
                cVar.d();
                return;
            }
        }
        return;
    }

    private void d() {
        if (this.e != null && this.g == 1) {
            try {
                this.e.a(this.d);
            } catch (Exception e2) {
                this.d.a = false;
                this.d.d = -1;
                this.d.c = e.c().a().getString(C0000R.string.txtExceptionOper);
                if (e2.getLocalizedMessage() != null) {
                    StringBuilder sb = new StringBuilder();
                    k kVar = this.d;
                    kVar.c = sb.append(kVar.c).append(e2.getLocalizedMessage()).toString();
                } else if (e2.getMessage() != null) {
                    StringBuilder sb2 = new StringBuilder();
                    k kVar2 = this.d;
                    kVar2.c = sb2.append(kVar2.c).append(e2.getMessage()).toString();
                }
                this.e.a(this.d);
            }
        }
        this.g = 0;
        f();
    }

    private boolean e() {
        while (true) {
            if (!this.d.a && this.d.d < 10000) {
                break;
            }
            this.a++;
            if (this.g != 1 || this.a >= this.c.size()) {
                break;
            }
            this.b = (o) this.c.get(this.a);
            if (this.b != null && !this.b.l()) {
                return true;
            }
        }
        d();
        return false;
    }

    private void f() {
        Iterator it = this.c.iterator();
        while (it.hasNext()) {
            o oVar = (o) it.next();
            if (oVar != null) {
                oVar.g();
            }
        }
        int i = this.d.d;
        this.d = new k();
        if (i == 50 || i == 51) {
            Iterator it2 = this.c.iterator();
            while (it2.hasNext()) {
                o oVar2 = (o) it2.next();
                if (oVar2 != null) {
                    oVar2.a(this.d);
                }
            }
            Log.i("TaskManager", "Reset task result status to enable restart if User/Password wrong.");
        } else {
            this.c.clear();
            Log.i("TaskManager", "Clean up. All tasks removed from task manager!");
        }
        this.b = null;
        this.a = -1;
    }

    public final void a(o oVar) {
        if (oVar != null) {
            oVar.a(this.d);
            oVar.a(this);
            this.c.add(oVar);
        }
    }

    public final void a(o oVar, int i) {
        if (oVar != null && i >= 0 && i <= this.c.size()) {
            oVar.a(this.d);
            oVar.a(this);
            this.c.add(i, oVar);
        }
    }

    public final void a(Object obj) {
        if (this.g != 0 && this.b != null) {
            this.f = obj;
            synchronized (this.b) {
                if (this.b != null && this.b.i()) {
                    this.b.notifyAll();
                }
            }
        }
    }

    public final void a(String str) {
        if (this.e != null && str != null && str.length() > 0) {
            this.e.a(str);
        }
    }

    public final boolean a() {
        return this.g == 1;
    }

    public final void a_(Object obj) {
        boolean z;
        Iterator it = this.c.iterator();
        boolean z2 = false;
        while (true) {
            if (!it.hasNext()) {
                z = z2;
                break;
            }
            o oVar = (o) it.next();
            z = oVar instanceof l ? !((l) oVar).d() : z2;
            if (z) {
                break;
            }
            z2 = z;
        }
        String f2 = m.f();
        if (z && f2 != null) {
            l lVar = new l();
            lVar.a(true);
            lVar.a(f2);
            lVar.a(new y(this));
            a((o) lVar);
            a((o) new g());
        }
    }

    public final boolean b() {
        this.g = 1;
        if (this.c.size() <= 0) {
            Log.i("TaskManager", "No task to run, finished!");
            d();
            return false;
        }
        a_(null);
        new x(this).start();
        return true;
    }

    public final void c() {
        this.g = 2;
        if (this.b != null) {
            Log.i("TaskManager", "Abort task: " + this.b.getClass().getName());
            this.b.n();
        }
    }
}
