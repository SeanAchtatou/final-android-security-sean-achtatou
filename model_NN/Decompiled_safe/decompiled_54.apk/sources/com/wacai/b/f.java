package com.wacai.b;

import java.util.ArrayList;
import java.util.Hashtable;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class f extends g {
    private boolean d = true;
    private int e = 0;
    private String f;
    private ArrayList g = null;

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0068 A[Catch:{ Exception -> 0x00ab }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00b2 A[Catch:{ Exception -> 0x00ab }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00c0 A[Catch:{ Exception -> 0x00ab }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.Hashtable b(org.w3c.dom.Element r8) {
        /*
            r7 = this;
            r6 = 0
            java.util.Hashtable r0 = new java.util.Hashtable
            r0.<init>()
            java.lang.String r1 = "wac-mark"
            org.w3c.dom.NodeList r1 = r8.getElementsByTagName(r1)     // Catch:{ Exception -> 0x00ab }
            r2 = 0
            org.w3c.dom.Node r1 = r1.item(r2)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r2 = r7.f     // Catch:{ Exception -> 0x00ab }
            if (r2 == 0) goto L_0x00b6
            java.lang.String r1 = com.wacai.data.ab.a(r1)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r2 = "select name from %s where uuid = '%s'"
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x009c, all -> 0x00ae }
            r4 = 0
            java.lang.String r5 = r7.f     // Catch:{ Exception -> 0x009c, all -> 0x00ae }
            r3[r4] = r5     // Catch:{ Exception -> 0x009c, all -> 0x00ae }
            r4 = 1
            r3[r4] = r1     // Catch:{ Exception -> 0x009c, all -> 0x00ae }
            java.lang.String r1 = java.lang.String.format(r2, r3)     // Catch:{ Exception -> 0x009c, all -> 0x00ae }
            com.wacai.e r2 = com.wacai.e.c()     // Catch:{ Exception -> 0x009c, all -> 0x00ae }
            android.database.sqlite.SQLiteDatabase r2 = r2.b()     // Catch:{ Exception -> 0x009c, all -> 0x00ae }
            r3 = 0
            android.database.Cursor r1 = r2.rawQuery(r1, r3)     // Catch:{ Exception -> 0x009c, all -> 0x00ae }
            if (r1 == 0) goto L_0x0054
            boolean r2 = r1.moveToFirst()     // Catch:{ Exception -> 0x00c5 }
            if (r2 == 0) goto L_0x0054
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x00c5 }
            if (r2 <= 0) goto L_0x0054
            java.lang.String r2 = "TAG_LABLE"
            java.lang.String r3 = "name"
            int r3 = r1.getColumnIndexOrThrow(r3)     // Catch:{ Exception -> 0x00c5 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x00c5 }
            r0.put(r2, r3)     // Catch:{ Exception -> 0x00c5 }
        L_0x0054:
            if (r1 == 0) goto L_0x0059
            r1.close()     // Catch:{ Exception -> 0x00ab }
        L_0x0059:
            java.lang.String r1 = "wac-value"
            org.w3c.dom.NodeList r1 = r8.getElementsByTagName(r1)     // Catch:{ Exception -> 0x00ab }
            r2 = 0
            org.w3c.dom.Node r1 = r1.item(r2)     // Catch:{ Exception -> 0x00ab }
            boolean r2 = r7.d     // Catch:{ Exception -> 0x00ab }
            if (r2 == 0) goto L_0x00c0
            java.lang.String r2 = "TAG_FIRST"
        L_0x006a:
            java.lang.String r1 = com.wacai.data.ab.a(r1)     // Catch:{ Exception -> 0x00ab }
            r0.put(r2, r1)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r1 = "wac-inited"
            org.w3c.dom.NodeList r1 = r8.getElementsByTagName(r1)     // Catch:{ Exception -> 0x00ab }
            if (r1 == 0) goto L_0x009b
            int r2 = r1.getLength()     // Catch:{ Exception -> 0x00ab }
            if (r2 <= 0) goto L_0x009b
            r2 = 0
            org.w3c.dom.Node r1 = r1.item(r2)     // Catch:{ Exception -> 0x00ab }
            if (r1 == 0) goto L_0x009b
            org.w3c.dom.Node r2 = r1.getFirstChild()     // Catch:{ Exception -> 0x00ab }
            if (r2 == 0) goto L_0x009b
            java.lang.String r2 = "TAG_INITED"
            org.w3c.dom.Node r7 = r1.getFirstChild()     // Catch:{ Exception -> 0x00ab }
            org.w3c.dom.Text r7 = (org.w3c.dom.Text) r7     // Catch:{ Exception -> 0x00ab }
            java.lang.String r1 = r7.getNodeValue()     // Catch:{ Exception -> 0x00ab }
            r0.put(r2, r1)     // Catch:{ Exception -> 0x00ab }
        L_0x009b:
            return r0
        L_0x009c:
            r1 = move-exception
            r1 = r6
        L_0x009e:
            java.lang.String r2 = "TAG_LABLE"
            java.lang.String r3 = "unknown"
            r0.put(r2, r3)     // Catch:{ all -> 0x00c3 }
            if (r1 == 0) goto L_0x0059
            r1.close()     // Catch:{ Exception -> 0x00ab }
            goto L_0x0059
        L_0x00ab:
            r0 = move-exception
            r0 = r6
            goto L_0x009b
        L_0x00ae:
            r0 = move-exception
            r1 = r6
        L_0x00b0:
            if (r1 == 0) goto L_0x00b5
            r1.close()     // Catch:{ Exception -> 0x00ab }
        L_0x00b5:
            throw r0     // Catch:{ Exception -> 0x00ab }
        L_0x00b6:
            java.lang.String r2 = "TAG_LABLE"
            java.lang.String r1 = com.wacai.data.ab.a(r1)     // Catch:{ Exception -> 0x00ab }
            r0.put(r2, r1)     // Catch:{ Exception -> 0x00ab }
            goto L_0x0059
        L_0x00c0:
            java.lang.String r2 = "TAG_SECOND"
            goto L_0x006a
        L_0x00c3:
            r0 = move-exception
            goto L_0x00b0
        L_0x00c5:
            r2 = move-exception
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.b.f.b(org.w3c.dom.Element):java.util.Hashtable");
    }

    public final void a(int i) {
        this.e = i;
    }

    public final void a(String str) {
        this.f = str;
    }

    public final void a(ArrayList arrayList) {
        this.g = arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a(Element element) {
        Hashtable b;
        if (element != null) {
            NodeList elementsByTagName = element.getElementsByTagName("wac-record");
            int length = elementsByTagName.getLength();
            for (int i = 0; i < length; i++) {
                Node item = elementsByTagName.item(i);
                if (item != null && item.getNodeName() != null && Element.class.isInstance(item) && (b = b((Element) item)) != null) {
                    String str = (String) b.get("TAG_LABLE");
                    if (str != null && str.length() != 0) {
                        if (this.e == 0) {
                            this.g.add(b);
                        } else if (this.e == 1) {
                            String str2 = (String) b.get(this.d ? "TAG_FIRST" : "TAG_SECOND");
                            if (str2 != null && str2.length() > 0 && str2 != null && str != null) {
                                int i2 = 0;
                                while (true) {
                                    if (i2 >= this.g.size()) {
                                        break;
                                    }
                                    Hashtable hashtable = (Hashtable) this.g.get(i2);
                                    if (hashtable == null || str.compareTo((String) hashtable.get("TAG_LABLE")) != 0) {
                                        i2++;
                                    } else {
                                        hashtable.put(this.d ? "TAG_FIRST" : "TAG_SECOND", str2);
                                    }
                                }
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public final void a(boolean z) {
        this.d = z;
    }
}
