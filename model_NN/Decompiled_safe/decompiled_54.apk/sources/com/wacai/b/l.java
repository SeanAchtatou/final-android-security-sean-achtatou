package com.wacai.b;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import com.wacai.a.d;
import com.wacai.a.h;
import com.wacai.b;
import com.wacai.e;
import com.wacai365.C0000R;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;

public class l extends o {
    private File d = null;
    private String e;
    private boolean f;
    private String g;
    private String h;
    private long i;
    private long j;

    public l() {
        super(true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x004f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.io.ByteArrayOutputStream a(byte[] r8, int r9) {
        /*
            r7 = this;
            r6 = 0
            java.lang.String r0 = r7.e
            java.lang.String r1 = "utf-8"
            byte[] r0 = r0.getBytes(r1)
            com.wacai.b.d r1 = new com.wacai.b.d
            r1.<init>(r7)
            if (r0 == 0) goto L_0x0029
            r2 = 0
            java.util.zip.ZipOutputStream r3 = new java.util.zip.ZipOutputStream     // Catch:{ all -> 0x004b }
            r3.<init>(r1)     // Catch:{ all -> 0x004b }
            java.util.zip.ZipEntry r2 = new java.util.zip.ZipEntry     // Catch:{ all -> 0x0054 }
            java.lang.String r4 = "content"
            r2.<init>(r4)     // Catch:{ all -> 0x0054 }
            r3.putNextEntry(r2)     // Catch:{ all -> 0x0054 }
            r3.write(r0)     // Catch:{ all -> 0x0054 }
            r3.flush()     // Catch:{ all -> 0x0054 }
            r3.close()
        L_0x0029:
            byte[] r0 = r1.toByteArray()
            int r2 = r0.length
            double r2 = (double) r2
            double r4 = (double) r9
            double r2 = r2 / r4
            double r2 = java.lang.Math.ceil(r2)
            int r2 = (int) r2
            r3 = r6
            r4 = r6
        L_0x0038:
            if (r4 >= r2) goto L_0x0053
            int r5 = r0.length
            int r6 = r4 * r9
            int r5 = r5 - r6
            int r5 = java.lang.Math.min(r9, r5)
            if (r5 <= 0) goto L_0x0053
            com.wacai.a.h.a(r0, r3, r5, r8)
            int r4 = r4 + 1
            int r3 = r3 + r9
            goto L_0x0038
        L_0x004b:
            r0 = move-exception
            r1 = r2
        L_0x004d:
            if (r1 == 0) goto L_0x0052
            r1.close()
        L_0x0052:
            throw r0
        L_0x0053:
            return r1
        L_0x0054:
            r0 = move-exception
            r1 = r3
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.b.l.a(byte[], int):java.io.ByteArrayOutputStream");
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x009b A[SYNTHETIC, Splitter:B:31:0x009b] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00c6 A[SYNTHETIC, Splitter:B:44:0x00c6] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.apache.http.client.methods.HttpPost a(byte[] r7) {
        /*
            r6 = this;
            r5 = 0
            if (r7 != 0) goto L_0x0005
            r0 = r5
        L_0x0004:
            return r0
        L_0x0005:
            org.apache.http.client.methods.HttpPost r0 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            java.lang.String r1 = "/transfer"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            java.lang.String r1 = "Connection"
            java.lang.String r2 = "Keep-Alive"
            r0.setHeader(r1, r2)     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            java.lang.String r1 = "content-type"
            java.lang.String r2 = "application/octet-stream"
            r0.setHeader(r1, r2)     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            java.lang.String r1 = "X-format"
            java.lang.String r2 = "1"
            r0.setHeader(r1, r2)     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            java.lang.String r1 = "Accept-Encoding"
            java.lang.String r2 = "identity"
            r0.setHeader(r1, r2)     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            com.wacai.b r1 = com.wacai.b.m()     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            java.lang.String r1 = r1.d()     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            boolean r2 = r6.f     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            if (r2 == 0) goto L_0x0055
            java.lang.String r1 = "anonymous"
            java.lang.String r2 = "1"
            r0.setHeader(r1, r2)     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
        L_0x003b:
            r1 = 4096(0x1000, float:5.74E-42)
            java.io.ByteArrayOutputStream r1 = r6.a(r7, r1)     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            org.apache.http.entity.ByteArrayEntity r2 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ Exception -> 0x00da, all -> 0x00d8 }
            byte[] r3 = r1.toByteArray()     // Catch:{ Exception -> 0x00da, all -> 0x00d8 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x00da, all -> 0x00d8 }
            r0.setEntity(r2)     // Catch:{ Exception -> 0x00da, all -> 0x00d8 }
            if (r1 == 0) goto L_0x0004
            r1.close()     // Catch:{ Exception -> 0x0053 }
            goto L_0x0004
        L_0x0053:
            r1 = move-exception
            goto L_0x0004
        L_0x0055:
            java.lang.String r2 = r6.g     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            if (r2 == 0) goto L_0x00a1
            java.lang.String r2 = r6.g     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
        L_0x005b:
            java.lang.String r3 = r6.h     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            if (r3 == 0) goto L_0x00aa
            java.lang.String r3 = r6.h     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
        L_0x0061:
            if (r2 == 0) goto L_0x00b3
            int r4 = r2.length()     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            if (r4 <= 0) goto L_0x00b3
            if (r3 == 0) goto L_0x00b3
            int r4 = r3.length()     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            if (r4 <= 0) goto L_0x00b3
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            r1.<init>()     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            java.lang.String r4 = "==="
            r1.append(r4)     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            java.lang.String r4 = "utf-8"
            byte[] r2 = r2.getBytes(r4)     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            java.lang.String r2 = com.wacai.a.d.a(r2)     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            r1.append(r2)     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            java.lang.String r2 = "Username"
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            r0.setHeader(r2, r1)     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            java.lang.String r1 = "Password"
            r0.setHeader(r1, r3)     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            goto L_0x003b
        L_0x0097:
            r0 = move-exception
            r0 = r5
        L_0x0099:
            if (r0 == 0) goto L_0x009e
            r0.close()     // Catch:{ Exception -> 0x00d4 }
        L_0x009e:
            r0 = r5
            goto L_0x0004
        L_0x00a1:
            com.wacai.b r2 = com.wacai.b.m()     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            java.lang.String r2 = r2.b()     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            goto L_0x005b
        L_0x00aa:
            com.wacai.b r3 = com.wacai.b.m()     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            java.lang.String r3 = r3.c()     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            goto L_0x0061
        L_0x00b3:
            if (r1 == 0) goto L_0x00ca
            int r2 = r1.length()     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            if (r2 <= 0) goto L_0x00ca
            java.lang.String r2 = "uid"
            r0.setHeader(r2, r1)     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            goto L_0x003b
        L_0x00c2:
            r0 = move-exception
            r1 = r5
        L_0x00c4:
            if (r1 == 0) goto L_0x00c9
            r1.close()     // Catch:{ Exception -> 0x00d6 }
        L_0x00c9:
            throw r0
        L_0x00ca:
            java.lang.String r0 = "HttpTask"
            java.lang.String r1 = "none of user & password & uid exception!"
            android.util.Log.e(r0, r1)     // Catch:{ Exception -> 0x0097, all -> 0x00c2 }
            r0 = r5
            goto L_0x0004
        L_0x00d4:
            r0 = move-exception
            goto L_0x009e
        L_0x00d6:
            r1 = move-exception
            goto L_0x00c9
        L_0x00d8:
            r0 = move-exception
            goto L_0x00c4
        L_0x00da:
            r0 = move-exception
            r0 = r1
            goto L_0x0099
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.b.l.a(byte[]):org.apache.http.client.methods.HttpPost");
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00bc A[Catch:{ all -> 0x012a }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00f0 A[SYNTHETIC, Splitter:B:55:0x00f0] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0106 A[SYNTHETIC, Splitter:B:65:0x0106] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0083 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(org.apache.http.HttpResponse r13, byte[] r14) {
        /*
            r12 = this;
            r11 = 0
            r10 = 4096(0x1000, float:5.74E-42)
            r9 = 0
            if (r13 != 0) goto L_0x0008
            r0 = r9
        L_0x0007:
            return r0
        L_0x0008:
            java.io.File r0 = r12.d
            if (r0 == 0) goto L_0x000f
            r12.q()
        L_0x000f:
            org.apache.http.HttpEntity r0 = r13.getEntity()     // Catch:{ Exception -> 0x00b3, all -> 0x0101 }
            java.io.InputStream r0 = r0.getContent()     // Catch:{ Exception -> 0x00b3, all -> 0x0101 }
            java.io.File r1 = p()     // Catch:{ Exception -> 0x00b3, all -> 0x0101 }
            java.util.UUID r2 = java.util.UUID.randomUUID()     // Catch:{ Exception -> 0x00b3, all -> 0x0101 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00b3, all -> 0x0101 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b3, all -> 0x0101 }
            r3.<init>()     // Catch:{ Exception -> 0x00b3, all -> 0x0101 }
            r4 = 0
            int r5 = r2.length()     // Catch:{ Exception -> 0x00b3, all -> 0x0101 }
            r6 = 3
            int r5 = r5 - r6
            java.lang.String r4 = r2.substring(r4, r5)     // Catch:{ Exception -> 0x00b3, all -> 0x0101 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00b3, all -> 0x0101 }
            java.lang.String r4 = "zp"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00b3, all -> 0x0101 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00b3, all -> 0x0101 }
            java.lang.String r4 = ".txt"
            java.io.File r3 = java.io.File.createTempFile(r3, r4, r1)     // Catch:{ Exception -> 0x00b3, all -> 0x0101 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x012c, all -> 0x011e }
            r4.<init>(r3)     // Catch:{ Exception -> 0x012c, all -> 0x011e }
            r5 = 4096(0x1000, float:5.74E-42)
            byte[] r5 = new byte[r5]     // Catch:{ Exception -> 0x0130, all -> 0x0122 }
        L_0x0050:
            r6 = r9
        L_0x0051:
            int r7 = r10 - r6
            int r7 = r0.read(r5, r6, r7)     // Catch:{ Exception -> 0x0130, all -> 0x0122 }
            if (r7 <= 0) goto L_0x0079
            r8 = -1
            if (r7 == r8) goto L_0x0079
            if (r6 != 0) goto L_0x0076
            if (r7 <= 0) goto L_0x0076
            boolean r8 = r12.b(r5)     // Catch:{ Exception -> 0x0130, all -> 0x0122 }
            if (r8 == 0) goto L_0x0076
            r4.close()     // Catch:{ Exception -> 0x0115 }
        L_0x0069:
            if (r3 == 0) goto L_0x0074
            boolean r0 = r3.exists()
            if (r0 == 0) goto L_0x0074
            r3.delete()
        L_0x0074:
            r0 = r9
            goto L_0x0007
        L_0x0076:
            int r6 = r6 + r7
            if (r6 < r10) goto L_0x0051
        L_0x0079:
            if (r6 <= 0) goto L_0x0083
            r8 = 0
            com.wacai.a.h.a(r5, r8, r6, r14)     // Catch:{ Exception -> 0x0130, all -> 0x0122 }
            r8 = 0
            r4.write(r5, r8, r6)     // Catch:{ Exception -> 0x0130, all -> 0x0122 }
        L_0x0083:
            if (r7 > 0) goto L_0x0050
            r4.close()     // Catch:{ Exception -> 0x0130, all -> 0x0122 }
            r0.close()     // Catch:{ Exception -> 0x012c, all -> 0x011e }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x012c, all -> 0x011e }
            r0.<init>(r3)     // Catch:{ Exception -> 0x012c, all -> 0x011e }
            java.lang.String r4 = ".txt"
            java.io.File r1 = java.io.File.createTempFile(r2, r4, r1)     // Catch:{ Exception -> 0x012c, all -> 0x011e }
            r12.d = r1     // Catch:{ Exception -> 0x012c, all -> 0x011e }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x012c, all -> 0x011e }
            java.io.File r2 = r12.d     // Catch:{ Exception -> 0x012c, all -> 0x011e }
            r1.<init>(r2)     // Catch:{ Exception -> 0x012c, all -> 0x011e }
            com.wacai.a.b.a(r0, r1)     // Catch:{ Exception -> 0x0134, all -> 0x0126 }
            r1.close()     // Catch:{ Exception -> 0x0118 }
        L_0x00a5:
            if (r3 == 0) goto L_0x00b0
            boolean r0 = r3.exists()
            if (r0 == 0) goto L_0x00b0
            r3.delete()
        L_0x00b0:
            r0 = 1
            goto L_0x0007
        L_0x00b3:
            r0 = move-exception
            r1 = r11
            r2 = r11
        L_0x00b6:
            com.wacai.b.k r3 = r12.a     // Catch:{ all -> 0x012a }
            boolean r3 = r3.a     // Catch:{ all -> 0x012a }
            if (r3 == 0) goto L_0x00ee
            com.wacai.b.k r3 = r12.a     // Catch:{ all -> 0x012a }
            r4 = 0
            r3.a = r4     // Catch:{ all -> 0x012a }
            com.wacai.b.k r3 = r12.a     // Catch:{ all -> 0x012a }
            r4 = -2
            r3.d = r4     // Catch:{ all -> 0x012a }
            com.wacai.b.k r3 = r12.a     // Catch:{ all -> 0x012a }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x012a }
            r4.<init>()     // Catch:{ all -> 0x012a }
            com.wacai.e r5 = com.wacai.e.c()     // Catch:{ all -> 0x012a }
            android.content.Context r5 = r5.a()     // Catch:{ all -> 0x012a }
            r6 = 2131296261(0x7f090005, float:1.8210434E38)
            java.lang.String r5 = r5.getString(r6)     // Catch:{ all -> 0x012a }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x012a }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x012a }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x012a }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x012a }
            r3.c = r0     // Catch:{ all -> 0x012a }
        L_0x00ee:
            if (r2 == 0) goto L_0x00f3
            r2.close()     // Catch:{ Exception -> 0x011a }
        L_0x00f3:
            if (r1 == 0) goto L_0x00fe
            boolean r0 = r1.exists()
            if (r0 == 0) goto L_0x00fe
            r1.delete()
        L_0x00fe:
            r0 = r9
            goto L_0x0007
        L_0x0101:
            r0 = move-exception
            r1 = r11
            r2 = r11
        L_0x0104:
            if (r2 == 0) goto L_0x0109
            r2.close()     // Catch:{ Exception -> 0x011c }
        L_0x0109:
            if (r1 == 0) goto L_0x0114
            boolean r2 = r1.exists()
            if (r2 == 0) goto L_0x0114
            r1.delete()
        L_0x0114:
            throw r0
        L_0x0115:
            r0 = move-exception
            goto L_0x0069
        L_0x0118:
            r0 = move-exception
            goto L_0x00a5
        L_0x011a:
            r0 = move-exception
            goto L_0x00f3
        L_0x011c:
            r2 = move-exception
            goto L_0x0109
        L_0x011e:
            r0 = move-exception
            r1 = r3
            r2 = r11
            goto L_0x0104
        L_0x0122:
            r0 = move-exception
            r1 = r3
            r2 = r4
            goto L_0x0104
        L_0x0126:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0104
        L_0x012a:
            r0 = move-exception
            goto L_0x0104
        L_0x012c:
            r0 = move-exception
            r1 = r3
            r2 = r11
            goto L_0x00b6
        L_0x0130:
            r0 = move-exception
            r1 = r3
            r2 = r4
            goto L_0x00b6
        L_0x0134:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x00b6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.b.l.a(org.apache.http.HttpResponse, byte[]):boolean");
    }

    private boolean a(HttpUriRequest httpUriRequest, byte[] bArr, int i2) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) e.c().a().getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                throw new HttpException(e.c().a().getString(C0000R.string.networkOffline));
            }
            HttpResponse a = com.wacai.a.e.a(URI.create(e.a), httpUriRequest, i2);
            if (a.getStatusLine().getStatusCode() == 200) {
                return a(a, bArr);
            }
            this.a.a = false;
            this.a.d = -3;
            this.a.c = e.c().a().getString(C0000R.string.netwrokConnectError);
            return false;
        } catch (HttpException e2) {
            if (this.a.a) {
                this.a.a = false;
                this.a.d = -4;
                this.a.c = e2.getMessage();
            }
            return false;
        } catch (Exception e3) {
            if (this.a.a) {
                this.a.a = false;
                String message = e3.getMessage();
                if (message == null || ((!message.contains("time") || !message.contains("out")) && !message.contains("超时"))) {
                    Log.e("HttpTask", "send exception: " + message);
                    this.a.d = -4;
                    this.a.c = e.c().a().getString(C0000R.string.netwrokConnectError);
                } else {
                    this.a.d = -10;
                    this.a.c = e.c().a().getString(C0000R.string.networkTimeout);
                }
            }
            return false;
        }
    }

    private boolean b(byte[] bArr) {
        try {
            String str = new String(bArr, "utf-8");
            str.toLowerCase();
            if (str.indexOf("html") >= 0) {
                this.a.a = false;
                this.a.d = -8;
                this.a.c = e.c().a().getString(C0000R.string.netRedirected);
                return true;
            }
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        return false;
    }

    private byte[] o() {
        byte[] bytes;
        try {
            String d2 = b.m().d();
            if (this.f) {
                bytes = "anonymous".getBytes("utf-8");
            } else {
                String b = this.g != null ? this.g : b.m().b();
                String c = this.h != null ? this.h : b.m().c();
                if (b != null && b.length() > 0 && c != null && c.length() > 0) {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("===");
                    byte[] bytes2 = b.getBytes("utf-8");
                    stringBuffer.append(d.a(bytes2));
                    bytes = bytes2;
                } else if (d2 == null || d2.length() <= 0) {
                    Log.e("HttpTask", "none of user & password & uid exception!");
                    return null;
                } else {
                    bytes = "anonymous".getBytes("utf-8");
                }
            }
            return h.a(h.a(), 0, bytes.length, bytes);
        } catch (Exception e2) {
            Log.e("HttpTask", "getSpice exception = " + e2.getMessage());
            return null;
        }
    }

    private static File p() {
        try {
            if (!Environment.getExternalStorageState().equalsIgnoreCase("mounted")) {
                return new File(e.b(null));
            }
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/wacai");
            if (file.exists()) {
                return file;
            }
            file.mkdirs();
            return file;
        } catch (Exception e2) {
            return new File(e.b(null));
        }
    }

    private void q() {
        if (this.d != null && this.d.exists()) {
            this.d.delete();
        }
    }

    public final void a(long j2) {
        this.i = j2;
    }

    public final void a(String str) {
        this.e = str;
    }

    public final void a(boolean z) {
        this.f = z;
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        byte[] o = o();
        HttpPost a = a(o);
        if (o == null || a == null) {
            this.a.a = false;
            this.a.d = -3;
            this.a.c = "联网失败！请联系挖财客服";
            if (o != null) {
                StringBuilder sb = new StringBuilder();
                k kVar = this.a;
                kVar.c = sb.append(kVar.c).append(".").toString();
            }
        }
        int i2 = 0;
        int i3 = 60000;
        while (this.c == 1) {
            if (a(a, o, i3)) {
                return true;
            }
            if (this.a.d != -10 || i2 >= 2) {
                return false;
            }
            g();
            i3 += 60000;
            this.a.a = true;
            this.a.d = 0;
            this.a.c = "";
            i2++;
        }
        return false;
    }

    public final void a_(Object obj) {
        if (obj instanceof String) {
            this.e = (String) obj;
        }
    }

    public final void b(long j2) {
        this.j = j2;
    }

    public final void b(String str) {
        this.g = str;
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        if (this.e == null || this.e.length() <= 0) {
            this.a.a = false;
            this.a.d = -1;
            this.a.c = e.c().a().getString(C0000R.string.txtParamError);
            return false;
        } else if (com.wacai.a.e.a()) {
            return true;
        } else {
            this.a.a = false;
            this.a.d = -8;
            this.a.c = e.c().a().getString(C0000R.string.txtNoNetworkPrompt);
            return false;
        }
    }

    public final void c(String str) {
        this.h = str;
    }

    public final boolean d() {
        return this.f;
    }

    public final long e() {
        return this.i;
    }

    public final long f() {
        return this.j;
    }

    public final void g() {
        q();
        super.g();
    }

    /* access modifiers changed from: protected */
    public final Object h() {
        return this.d;
    }
}
