package com.wacai.b;

public abstract class o implements v {
    protected k a;
    protected boolean b;
    int c;
    private boolean d;
    private boolean e;
    private String f;
    private a g;
    private q h;
    private j i;

    protected o() {
        this.e = true;
        this.f = "";
        this.a = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.b = true;
        this.c = 0;
        this.d = false;
    }

    protected o(boolean z) {
        this.e = true;
        this.f = "";
        this.a = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.b = true;
        this.c = 0;
        this.d = z;
    }

    private void a(boolean z) {
        if (this.b && this.c == 1 && this.h != null) {
            this.h.a(this, this.a);
        }
        if (this.g != null) {
            this.g.a(z ? h() : null);
        }
        this.c = 0;
    }

    /* access modifiers changed from: private */
    public boolean d() {
        boolean z;
        Throwable th;
        try {
            boolean a2 = a();
            try {
                c();
                a(a2);
                return a2;
            } catch (Throwable th2) {
                Throwable th3 = th2;
                z = a2;
                th = th3;
                a(z);
                throw th;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            z = true;
            th = th5;
        }
    }

    public final void a(a aVar) {
        this.g = aVar;
    }

    public final void a(j jVar) {
        this.i = jVar;
    }

    public final void a(k kVar) {
        this.a = kVar;
    }

    public final void a(q qVar) {
        this.h = qVar;
    }

    /* access modifiers changed from: protected */
    public abstract boolean a();

    public final void b(boolean z) {
        this.e = z;
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    public final void d(String str) {
        this.f = str;
    }

    /* access modifiers changed from: protected */
    public final void e(String str) {
        if (this.e && this.g != null) {
            this.g.a(str);
        }
    }

    public void g() {
    }

    /* access modifiers changed from: protected */
    public Object h() {
        return null;
    }

    public final boolean i() {
        return this.d;
    }

    public final String j() {
        return this.f;
    }

    public final j k() {
        return this.i;
    }

    public final boolean l() {
        if (this.i == null) {
            return false;
        }
        return this.i.a();
    }

    public final boolean m() {
        this.c = 1;
        if (this.a == null) {
            this.a = new k();
        }
        if (!b()) {
            a(false);
            return false;
        } else if (!this.d) {
            return d();
        } else {
            new Thread(new b(this)).start();
            return true;
        }
    }

    public final void n() {
        this.c = 2;
        a(false);
    }
}
