package com.wacai.b;

import com.wacai.e;
import com.wacai365.C0000R;
import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class g extends o {
    private File d = null;

    public g() {
        super(true);
    }

    private void b(Element element) {
        if (element == null) {
            throw new NullPointerException("parserResult root = null");
        } else if (this.a != null) {
            NamedNodeMap attributes = element.getAttributes();
            if (attributes == null) {
                throw new Exception("There's no result info in Http response");
            }
            int length = attributes.getLength();
            for (int i = 0; i < length; i++) {
                Attr attr = (Attr) attributes.item(i);
                if (!(attr == null || attr.getName() == null)) {
                    if (attr.getName().equals("result")) {
                        this.a.d = Integer.parseInt(attr.getValue());
                        this.a.a = this.a.d == 0 || this.a.d == 10001;
                    } else if (attr.getName().equals("note")) {
                        this.a.c = attr.getValue();
                    } else if (attr.getName().equals("lastmodifytime")) {
                        this.a.b = Long.parseLong(attr.getValue());
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Element element) {
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        try {
            NodeList elementsByTagName = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(this.d).getElementsByTagName("wac-response");
            if (elementsByTagName == null || elementsByTagName.getLength() <= 0) {
                throw new Exception();
            }
            Node item = elementsByTagName.item(0);
            if (!Element.class.isInstance(item)) {
                throw new Exception();
            }
            Element element = (Element) item;
            b(element);
            if (this.a.a) {
                a(element);
            }
            return this.a.a;
        } catch (Exception e) {
            if (this.a.a) {
                this.a.a = false;
                this.a.d = -9;
                this.a.c = e.c().a().getString(C0000R.string.txtExceptionOper);
                if (e.getLocalizedMessage() != null) {
                    StringBuilder sb = new StringBuilder();
                    k kVar = this.a;
                    kVar.c = sb.append(kVar.c).append(e.getLocalizedMessage()).toString();
                } else if (e.getMessage() != null) {
                    StringBuilder sb2 = new StringBuilder();
                    k kVar2 = this.a;
                    kVar2.c = sb2.append(kVar2.c).append(e.getMessage()).toString();
                }
            }
        }
    }

    public final void a_(Object obj) {
        if (obj instanceof File) {
            this.d = (File) obj;
        }
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        if (this.d != null) {
            return true;
        }
        this.a.a = false;
        this.a.d = -2;
        this.a.c = "缓存文件不存在，操作失败！";
        return false;
    }
}
