package com.wacai.b;

import com.wacai.data.k;
import com.wacai.e;
import com.wacai365.C0000R;
import java.util.ArrayList;

public final class t extends o {
    private String d;
    private k e;

    public t() {
        super(false);
    }

    public final void a(k kVar) {
        this.e = kVar;
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        try {
            if (this.d == null || this.d.length() == 0) {
                this.a.a = false;
                this.a.d = -7;
                this.a.c = "获取新建对象验证码失败，请稍后重新操作!";
                return false;
            }
            this.e.g(this.d);
            return true;
        } catch (Exception e2) {
            if (this.a.a) {
                this.a.a = false;
                this.a.d = -7;
                this.a.c = e.c().a().getString(C0000R.string.txtExceptionOper);
                if (e2.getLocalizedMessage() != null) {
                    StringBuilder sb = new StringBuilder();
                    k kVar = this.a;
                    kVar.c = sb.append(kVar.c).append(e2.getLocalizedMessage()).toString();
                } else if (e2.getMessage() != null) {
                    StringBuilder sb2 = new StringBuilder();
                    k kVar2 = this.a;
                    kVar2.c = sb2.append(kVar2.c).append(e2.getMessage()).toString();
                }
            }
            return false;
        }
    }

    public final void a_(Object obj) {
        ArrayList arrayList;
        if ((obj instanceof ArrayList) && (arrayList = (ArrayList) obj) != null && !arrayList.isEmpty()) {
            this.d = (String) arrayList.get(0);
        }
    }

    /* access modifiers changed from: protected */
    public final Object h() {
        if (this.e == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        this.e.a(stringBuffer);
        return m.a(stringBuffer.toString());
    }
}
