package com.wacai.b;

import android.util.Log;
import com.wacai.b;
import com.wacai.data.ab;
import com.wacai.data.c;
import org.w3c.dom.Element;

public final class i extends s {
    /* access modifiers changed from: protected */
    public final ab a(String str, Element element) {
        if (str == null || element == null) {
            return null;
        }
        if (!str.equalsIgnoreCase("k")) {
            return null;
        }
        this.a.e = true;
        return c.a(element);
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (this.a.a) {
            try {
                b.m().b(this.a.b);
                b.m().l();
            } catch (Exception e) {
                Log.e("XmlParserNewsTask", "postAction exception: " + e.getMessage());
            }
        }
        super.c();
    }
}
