package com.wacai.b;

import android.util.Log;
import com.wacai.b;
import com.wacai.data.ab;
import com.wacai.data.r;
import com.wacai.data.u;
import com.wacai.data.v;
import com.wacai.data.z;
import org.w3c.dom.Element;

public final class p extends s {
    private long d;
    private long e;

    /* access modifiers changed from: protected */
    public final ab a(String str, Element element) {
        if (str == null || element == null) {
            return null;
        }
        if (str.equalsIgnoreCase("o")) {
            return z.a(element);
        }
        if (str.equalsIgnoreCase("p")) {
            return r.a(element);
        }
        if (str.equalsIgnoreCase("q")) {
            return u.a(element);
        }
        if (str.equalsIgnoreCase("bn")) {
            return v.a(element);
        }
        return null;
    }

    public final void a(long j) {
        this.d = j;
    }

    public final void b(long j) {
        this.e = j;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (this.a.a) {
            try {
                b.a(this.d, this.e, this.a.b);
            } catch (Exception e2) {
                Log.e("XmlParserTradeTask", "postAction exception: " + e2.getMessage());
            }
        }
        super.c();
    }
}
