package com.wacai.b;

import android.util.Log;
import com.wacai.b;
import com.wacai.e;
import java.util.Vector;

public final class z extends o {
    private int d;
    private int e;
    private int f;
    private int g;

    public z() {
        super(false);
    }

    public final void a(int i) {
        this.d = i;
    }

    public final boolean a() {
        int i = this.f;
        int i2 = this.d;
        int i3 = (((this.e - this.d) * 12) + this.g) - this.f;
        int i4 = i;
        for (int i5 = 0; i5 <= i3; i5++) {
            try {
                b.a((long) i2, (long) i4);
            } catch (Exception e2) {
                Log.e("ClearBDTask", String.format("Remove modefytime in %4d-%2d failed", Integer.valueOf(i2), Integer.valueOf(i4)));
            }
            i4++;
            if (i4 > 12) {
                i2++;
                i4 = 1;
            }
        }
        long j = (long) ((this.d * 10000) + (this.f * 100));
        long j2 = (long) ((this.e * 10000) + (this.g * 100) + 99);
        Vector vector = new Vector();
        vector.add(String.format("delete from tbl_outgomemberinfo where outgoid in (select id from tbl_outgoinfo where paybackid = 0 and ymd >= %d and ymd <= %d and scheduleoutgoid = 0)", Long.valueOf(j), Long.valueOf(j2)));
        vector.add(String.format("delete from tbl_outgoinfo where paybackid = 0 and ymd >= %d and ymd <= %d and scheduleoutgoid = 0", Long.valueOf(j), Long.valueOf(j2)));
        vector.add(String.format("delete from tbl_incomememberinfo where incomeid in (select id from tbl_incomeinfo where paybackid = 0 and ymd >= %d and ymd <= %d and scheduleincomeid = 0)", Long.valueOf(j), Long.valueOf(j2)));
        vector.add(String.format("delete from tbl_incomeinfo where paybackid = 0 and ymd >= %d and ymd <= %d and scheduleincomeid = 0", Long.valueOf(j), Long.valueOf(j2)));
        vector.add(String.format("delete from TBL_TRANSFERINFO where type = 0 and ymd >= %d and ymd <= %d", Long.valueOf(j), Long.valueOf(j2)));
        for (int i6 = 0; i6 < vector.size(); i6++) {
            try {
                e.c().b().execSQL((String) vector.elementAt(i6));
            } catch (Exception e3) {
                Log.e("ClearDBTask", String.format("Sql failed: %s", vector.elementAt(i6)));
            }
        }
        return true;
    }

    public final void a_(Object obj) {
    }

    public final void b(int i) {
        this.e = i;
    }

    public final void c(int i) {
        this.f = i;
    }

    public final void d(int i) {
        this.g = i;
    }
}
