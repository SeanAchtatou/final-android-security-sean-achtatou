package com.wacai.b;

import android.util.Log;
import com.wacai.data.ab;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class s extends g {
    /* access modifiers changed from: protected */
    public abstract ab a(String str, Element element);

    /* access modifiers changed from: protected */
    public final void a(Element element) {
        if (element != null) {
            NodeList childNodes = element.getChildNodes();
            int length = childNodes.getLength();
            for (int i = 0; i < length; i++) {
                Node item = childNodes.item(i);
                if (!(item == null || item.getNodeName() == null || !Element.class.isInstance(item))) {
                    try {
                        ab a = a(item.getNodeName(), (Element) item);
                        if (a != null) {
                            a.f(true);
                            a.c();
                        } else if (!item.getNodeName().equalsIgnoreCase("wac-prefer")) {
                            Log.e("XmlParserDataTask", "Parse failed for node:" + item.getNodeName());
                        }
                    } catch (Exception e) {
                        Log.e("XmlParserDataTask", "Parse&save failed exception:" + e.getMessage());
                        this.b = false;
                    }
                }
            }
        }
    }
}
