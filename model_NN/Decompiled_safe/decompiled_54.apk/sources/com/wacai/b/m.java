package com.wacai.b;

import com.wacai.b;
import com.wacai.data.a;
import com.wacai.data.d;
import com.wacai.data.g;
import com.wacai.data.i;
import com.wacai.data.j;
import com.wacai.data.n;
import com.wacai.data.q;
import com.wacai.data.r;
import com.wacai.data.u;
import com.wacai.data.v;
import com.wacai.data.z;
import com.wacai.e;
import com.wacai.f;

public abstract class m {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.b.a(long, long, boolean):long
     arg types: [long, long, int]
     candidates:
      com.wacai.b.a(long, long, long):void
      com.wacai.b.a(long, long, boolean):long */
    public static long a(long j, long j2, StringBuffer stringBuffer) {
        if (stringBuffer == null) {
            return 0;
        }
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"1\" basemodifytime=\"");
        stringBuffer.append(b.a(j, j2, false));
        stringBuffer.append("\">");
        long a = ((long) z.a(j, j2, stringBuffer)) + 0 + ((long) r.a(j, j2, stringBuffer)) + ((long) u.a(j, j2, stringBuffer)) + ((long) v.a(j, j2, stringBuffer));
        stringBuffer.append("</wac-command></wac>");
        return a;
    }

    public static long a(StringBuffer stringBuffer) {
        if (stringBuffer == null) {
            return 0;
        }
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"1\" basemodifytime=\"");
        stringBuffer.append(b.m().b(false));
        stringBuffer.append("\">");
        long a = ((long) g.a(stringBuffer)) + 0 + ((long) q.a(stringBuffer)) + ((long) com.wacai.data.m.a(stringBuffer)) + ((long) d.a(stringBuffer)) + ((long) n.a(stringBuffer));
        stringBuffer.append("</wac-command></wac>");
        stringBuffer.toString() + "";
        return a;
    }

    public static long a(StringBuffer stringBuffer, boolean z) {
        if (stringBuffer == null) {
            return 0;
        }
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"1\" basemodifytime=\"");
        stringBuffer.append(b.m().b(false));
        stringBuffer.append("\">");
        long a = ((long) j.a(stringBuffer, z)) + 0;
        stringBuffer.append("</wac-command></wac>");
        return a;
    }

    public static String a() {
        return a(0);
    }

    public static String a(int i) {
        StringBuffer stringBuffer = new StringBuffer();
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"0\" count=\"");
        stringBuffer.append(i);
        stringBuffer.append("\"/></wac>");
        return stringBuffer.toString();
    }

    public static String a(long j) {
        if (j < 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer(100);
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"10\"><wac-prefer key='multiPhone'>");
        stringBuffer.append(1 & j);
        stringBuffer.append("</wac-prefer></wac-command></wac>");
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai.b.a(long, long, boolean):long
     arg types: [long, long, int]
     candidates:
      com.wacai.b.a(long, long, long):void
      com.wacai.b.a(long, long, boolean):long */
    public static String a(long j, long j2) {
        StringBuffer stringBuffer = new StringBuffer();
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"2\" startline=\"0\" maxrecord=\"99999\">");
        stringBuffer.append("<wac-data year=\"");
        stringBuffer.append(j);
        stringBuffer.append("\" month=\"");
        stringBuffer.append(j2);
        stringBuffer.append("\" lastmodifytime=\"");
        stringBuffer.append(b.a(j, j2, true));
        stringBuffer.append("\"/></wac-command></wac>");
        return stringBuffer.toString();
    }

    public static String a(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"1\" basemodifytime=\"");
        stringBuffer.append(b.m().b(false));
        stringBuffer.append("\">");
        stringBuffer.append(str);
        stringBuffer.append("</wac-command></wac>");
        return stringBuffer.toString();
    }

    public static String a(String str, String str2, String str3) {
        StringBuffer stringBuffer = new StringBuffer(256);
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"9\">");
        if (!(str == null || str2 == null)) {
            stringBuffer.append("<wac-account>");
            stringBuffer.append(e(str));
            stringBuffer.append("</wac-account><wac-pwd>");
            stringBuffer.append(e(str2));
            stringBuffer.append("</wac-pwd>");
        }
        stringBuffer.append("<wac-email>");
        stringBuffer.append(e(str3));
        stringBuffer.append("</wac-email>");
        String k = b.m().k();
        if (k != null) {
            stringBuffer.append("<wac-imei>");
            stringBuffer.append(k);
            stringBuffer.append("</wac-imei>");
        }
        stringBuffer.append("</wac-command></wac>");
        return stringBuffer.toString();
    }

    public static String b() {
        StringBuffer stringBuffer = new StringBuffer(100);
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"4\"><wac-system>");
        stringBuffer.append(e.b);
        stringBuffer.append("</wac-system><wac-version>");
        b.m();
        stringBuffer.append(b.a());
        stringBuffer.append("</wac-version><wac-resolution>4</wac-resolution></wac-command></wac>");
        return stringBuffer.toString();
    }

    public static String b(int i) {
        StringBuffer stringBuffer = new StringBuffer(100);
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"12\">");
        stringBuffer.append("<s t=\"0\" c=\"");
        stringBuffer.append(i);
        stringBuffer.append("\"/>");
        stringBuffer.append("</wac-command></wac>");
        return stringBuffer.toString();
    }

    public static String b(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"3\" ");
        stringBuffer.append(str);
        stringBuffer.append("</wac-command></wac>");
        return stringBuffer.toString();
    }

    private static void b(StringBuffer stringBuffer) {
        if (stringBuffer != null) {
            stringBuffer.append("<wac version=\"" + e.c + "\" platform=\"" + e.b + "\" appVer=\"");
            b.m();
            stringBuffer.append(b.a());
            stringBuffer.append("\" mc=\"");
            stringBuffer.append(f.e().a());
            String c = f.e().c();
            if (c != null) {
                stringBuffer.append("\" locale=\"");
                stringBuffer.append(c);
            }
            stringBuffer.append("\">");
        }
    }

    public static String c() {
        StringBuffer stringBuffer = new StringBuffer(100);
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"5\" basemodifytime=\"");
        long b = b.m().b(true);
        stringBuffer.append(b);
        if (b <= 0) {
            stringBuffer.append("\" defaultmodifytime=\"");
            stringBuffer.append(b.m().h());
        }
        stringBuffer.append("\"></wac-command></wac>");
        return stringBuffer.toString();
    }

    public static String c(String str) {
        StringBuffer stringBuffer = new StringBuffer(256);
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"7\" basemodifytime=\"");
        stringBuffer.append(b.m().b(false));
        stringBuffer.append("\"><wac-system>");
        stringBuffer.append(e.b);
        stringBuffer.append("</wac-system><wac-version>");
        b.m();
        stringBuffer.append(b.a());
        stringBuffer.append("</wac-version><wac-resolution>1</wac-resolution><wac-feedback>");
        stringBuffer.append(e(str));
        stringBuffer.append("</wac-feedback ></wac-command></wac>");
        return stringBuffer.toString();
    }

    public static String d() {
        StringBuffer stringBuffer = new StringBuffer(256);
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"6\" basemodifytime=\"");
        stringBuffer.append(b.m().i());
        stringBuffer.append("\"><wac-system>");
        stringBuffer.append(e.b);
        stringBuffer.append("</wac-system><wac-version>");
        b.m();
        stringBuffer.append(b.a());
        stringBuffer.append("</wac-version><wac-resolution>1</wac-resolution><wac-newstype>");
        i.a(stringBuffer);
        stringBuffer.append("</wac-newstype></wac-command></wac>");
        return stringBuffer.toString();
    }

    public static String d(String str) {
        StringBuffer stringBuffer = new StringBuffer(100);
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"10\"><wac-prefer key='moneyType'>");
        stringBuffer.append(str);
        stringBuffer.append("</wac-prefer></wac-command></wac>");
        return stringBuffer.toString();
    }

    public static String e() {
        StringBuffer stringBuffer = new StringBuffer(100);
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"11\"><wac-resolution>");
        stringBuffer.append(4);
        stringBuffer.append("</wac-resolution>");
        String k = b.m().k();
        if (k != null) {
            stringBuffer.append("<wac-imei>");
            stringBuffer.append(k);
            stringBuffer.append("</wac-imei>");
        }
        stringBuffer.append("</wac-command></wac>");
        return stringBuffer.toString();
    }

    private static String e(String str) {
        String[] strArr = {"&", "<", ">", "\"", "'"};
        String[] strArr2 = {"&amp;", "&lt;", "&gt;", "&quot;", "&apos;"};
        if (str == null) {
            return str;
        }
        String str2 = str;
        for (int i = 0; i < strArr.length; i++) {
            String str3 = strArr[i];
            String str4 = strArr2[i];
            if (str2 == null || str3 == null || str4 == null) {
                str2 = null;
            } else {
                StringBuffer stringBuffer = new StringBuffer();
                while (true) {
                    int indexOf = str2.indexOf(str3);
                    if (indexOf == -1) {
                        break;
                    }
                    stringBuffer.append(str2.substring(0, indexOf) + str4);
                    str2 = str2.substring(indexOf + str3.length());
                }
                stringBuffer.append(str2);
                str2 = stringBuffer.toString();
            }
        }
        return str2;
    }

    public static String f() {
        StringBuffer stringBuffer = new StringBuffer(100);
        b(stringBuffer);
        stringBuffer.append("<wac-command request=\"12\">");
        int a = a.a(stringBuffer);
        stringBuffer.append("</wac-command></wac>");
        if (a > 0) {
            return stringBuffer.toString();
        }
        return null;
    }
}
