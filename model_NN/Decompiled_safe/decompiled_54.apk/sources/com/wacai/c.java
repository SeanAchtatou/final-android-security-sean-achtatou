package com.wacai;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.wacai.a.g;
import com.wacai.data.aa;

public abstract class c {
    private static String a(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor;
        String str = "";
        try {
            cursor = sQLiteDatabase.rawQuery("select version from TBL_DBINFO", null);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        str = cursor.getString(0);
                    }
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            return str;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0426, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0427, code lost:
        r9.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x042a, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01d7, code lost:
        if (r0.equalsIgnoreCase("1.0.0.8") != false) goto L_0x01d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x01e7, code lost:
        if (r0.equalsIgnoreCase("1.0.0.9") != false) goto L_0x01e9;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0404 A[Catch:{ Exception -> 0x0455, Exception -> 0x0458, all -> 0x0426 }] */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x042f A[SYNTHETIC, Splitter:B:128:0x042f] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x01b6 A[Catch:{ Exception -> 0x0455, Exception -> 0x0458, all -> 0x0426 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01d1  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01e1  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01fc A[SYNTHETIC, Splitter:B:71:0x01fc] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x023e A[SYNTHETIC, Splitter:B:77:0x023e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void a(android.database.sqlite.SQLiteDatabase r9, com.wacai.d r10) {
        /*
            r4 = 0
            r2 = 0
            r8 = 1
            if (r9 != 0) goto L_0x0006
        L_0x0005:
            return
        L_0x0006:
            java.lang.String r0 = a(r9)
            r9.beginTransaction()     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "1.0.0.1"
            boolean r1 = r0.equalsIgnoreCase(r1)     // Catch:{ all -> 0x0426 }
            if (r1 == 0) goto L_0x0462
            java.lang.String r1 = "ALTER TABLE TBL_ACCOUNTINFO ADD balance decimal(10,2) DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_ACCOUNTINFO ADD balancedate INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_ACCOUNTINFO ADD hasbalance INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE TABLE TBL_TRANSFERINFO (id INTEGER PRIMARY KEY AUTOINCREMENT, transferoutmoney decimal(10,2) DEFAULT 0, transferinmoney decimal(10,2) DEFAULT 0, date INTEGER DEFAULT 0, transferoutaccountid INTEGER DEFAULT 0, transferinaccountid INTEGER DEFAULT 0, comment varchar(20), isdelete INTEGER DEFAULT 0, updatestatus INTEGER DEFAULT 0, uuid varchar(36), ymd INTEGER DEFAULT 0)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            r1 = r8
        L_0x002a:
            if (r1 != 0) goto L_0x0034
            java.lang.String r2 = "1.0.0.2"
            boolean r2 = r0.equalsIgnoreCase(r2)     // Catch:{ all -> 0x0426 }
            if (r2 == 0) goto L_0x003a
        L_0x0034:
            java.lang.String r1 = "CREATE TABLE TBL_USERPROFILE (id INTEGER PRIMARY KEY, propertyname varchar(40), propertyvalue varchar(300))"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            r1 = r8
        L_0x003a:
            if (r1 != 0) goto L_0x0044
            java.lang.String r2 = "1.0.0.3"
            boolean r2 = r0.equalsIgnoreCase(r2)     // Catch:{ all -> 0x0426 }
            if (r2 == 0) goto L_0x004f
        L_0x0044:
            java.lang.String r1 = "ALTER TABLE TBL_ACCOUNTINFO ADD warningbalance decimal(10,2) DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_ACCOUNTINFO ADD haswarning INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            r1 = r8
        L_0x004f:
            if (r1 != 0) goto L_0x0059
            java.lang.String r2 = "1.0.0.4"
            boolean r2 = r0.equalsIgnoreCase(r2)     // Catch:{ all -> 0x0426 }
            if (r2 == 0) goto L_0x0069
        L_0x0059:
            java.lang.String r1 = "CREATE TABLE TBL_MYNOTE (id INTEGER PRIMARY KEY AUTOINCREMENT, createdate INTEGER DEFAULT 0, comment varchar(100), isdelete INTEGER DEFAULT 0, updatestatus INTEGER DEFAULT 0, uuid varchar(36), ymd INTEGER DEFAULT 0)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE TABLE TBL_SMS (id INTEGER PRIMARY KEY AUTOINCREMENT, content varchar(500), smsdate INTEGER DEFAULT 0, sender varchar(100))"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE TABLE TBL_WHITELIST (id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(50), address varchar(50))"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            r1 = r8
        L_0x0069:
            if (r1 != 0) goto L_0x0073
            java.lang.String r2 = "1.0.0.5"
            boolean r2 = r0.equalsIgnoreCase(r2)     // Catch:{ all -> 0x0426 }
            if (r2 == 0) goto L_0x010f
        L_0x0073:
            java.lang.String r1 = "CREATE TABLE TBL_ACCOUNTTYPE (id INTEGER PRIMARY KEY, name varchar(20), uuid varchar(36), orderno INTEGER DEFAULT 0)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_ACCOUNTTYPE (name, uuid, orderno) SELECT DISTINCT '现金', '0', 0 WHERE NOT EXISTS (SELECT uuid FROM TBL_ACCOUNTTYPE WHERE uuid = '0')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_ACCOUNTTYPE (name, uuid, orderno) SELECT DISTINCT '信用卡', '1', 1 WHERE NOT EXISTS (SELECT uuid FROM TBL_ACCOUNTTYPE WHERE uuid = '1')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_ACCOUNTTYPE (name, uuid, orderno) SELECT DISTINCT '储蓄', '2', 2 WHERE NOT EXISTS (SELECT uuid FROM TBL_ACCOUNTTYPE WHERE uuid = '2')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_OUTGOMAINTYPEINFO ADD enable INTEGER DEFAULT 1"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_OUTGOMAINTYPEINFO ADD isdefault INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_OUTGOMAINTYPEINFO ADD updatestatus INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "UPDATE TBL_OUTGOMAINTYPEINFO set isdefault = 1, updatestatus = 1"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_INCOMEMAINTYPEINFO ADD enable INTEGER DEFAULT 1"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_INCOMEMAINTYPEINFO ADD isdefault INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_INCOMEMAINTYPEINFO ADD updatestatus INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "UPDATE TBL_INCOMEMAINTYPEINFO set isdefault = 1, updatestatus = 1"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "DELETE FROM TBL_OUTGOSUBTYPEINFO where id < 10000"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "UPDATE TBL_USERINFO set basemodifytime = 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_ACCOUNTINFO ADD COLUMN moneytype INTEGER DEFAULT 1"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE TABLE TBL_MONEYTYPE (id INTEGER PRIMARY KEY, name varchar(20), shortname varchar(10), flag varchar(10), uuid varchar(36), orderno INTEGER DEFAULT 0)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_MONEYTYPE (name, shortname, flag, uuid, orderno) SELECT DISTINCT '人民币', 'CNY', '￥', '0', 1 WHERE NOT EXISTS (SELECT uuid FROM TBL_MONEYTYPE WHERE uuid = '0')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_MONEYTYPE (name, shortname, flag, uuid, orderno) SELECT DISTINCT '美元', 'USD', '$', '1', 5 WHERE NOT EXISTS (SELECT uuid FROM TBL_MONEYTYPE WHERE uuid = '1')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_MONEYTYPE (name, shortname, flag, uuid, orderno) SELECT DISTINCT '港币', 'HKD', '$', '2', 10 WHERE NOT EXISTS (SELECT uuid FROM TBL_MONEYTYPE WHERE uuid = '2')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_MONEYTYPE (name, shortname, flag, uuid, orderno) SELECT DISTINCT '欧元', 'EUR', '€', '3', 15 WHERE NOT EXISTS (SELECT uuid FROM TBL_MONEYTYPE WHERE uuid = '3')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_MONEYTYPE (name, shortname, flag, uuid, orderno) SELECT DISTINCT '澳门元', 'MOP', '$', '4', 20 WHERE NOT EXISTS (SELECT uuid FROM TBL_MONEYTYPE WHERE uuid = '4')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_MONEYTYPE (name, shortname, flag, uuid, orderno) SELECT DISTINCT '新台币', 'TWD', '$', '5', 25 WHERE NOT EXISTS (SELECT uuid FROM TBL_MONEYTYPE WHERE uuid = '5')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_MONEYTYPE (name, shortname, flag, uuid, orderno) SELECT DISTINCT '日元', 'JPY', '￥', '6', 30 WHERE NOT EXISTS (SELECT uuid FROM TBL_MONEYTYPE WHERE uuid = '6')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_MONEYTYPE (name, shortname, flag, uuid, orderno) SELECT DISTINCT '韩元', 'KRW', '$', '7', 35 WHERE NOT EXISTS (SELECT uuid FROM TBL_MONEYTYPE WHERE uuid = '7')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_MONEYTYPE (name, shortname, flag, uuid, orderno) SELECT DISTINCT '英镑', 'GBP', '￡', '8', 40 WHERE NOT EXISTS (SELECT uuid FROM TBL_MONEYTYPE WHERE uuid = '8')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_MONEYTYPE (name, shortname, flag, uuid, orderno) SELECT DISTINCT '新加坡元', 'SGD', '$', '9', 45 WHERE NOT EXISTS (SELECT uuid FROM TBL_MONEYTYPE WHERE uuid = '9')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_MONEYTYPE (name, shortname, flag, uuid, orderno) SELECT DISTINCT '澳大利亚元', 'AUD', '$', '10', 50 WHERE NOT EXISTS (SELECT uuid FROM TBL_MONEYTYPE WHERE uuid = '10')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_MONEYTYPE (name, shortname, flag, uuid, orderno) SELECT DISTINCT '俄罗斯卢布', 'SUR', '$', '11', 55 WHERE NOT EXISTS (SELECT uuid FROM TBL_MONEYTYPE WHERE uuid = '11')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_MONEYTYPE (name, shortname, flag, uuid, orderno) SELECT DISTINCT '加拿大元', 'CAD', '$', '12', 60 WHERE NOT EXISTS (SELECT uuid FROM TBL_MONEYTYPE WHERE uuid = '12')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_MONEYTYPE (name, shortname, flag, uuid, orderno) SELECT DISTINCT '虚拟币', 'XNB', '◎', '13', 11 WHERE NOT EXISTS (SELECT uuid FROM TBL_MONEYTYPE WHERE uuid = '13')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_USERINFO ADD COLUMN defaultmoneytype INTEGER DEFAULT 1"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            r1 = r8
        L_0x010f:
            if (r1 != 0) goto L_0x0119
            java.lang.String r2 = "1.0.0.6"
            boolean r2 = r0.equalsIgnoreCase(r2)     // Catch:{ all -> 0x0426 }
            if (r2 == 0) goto L_0x01ba
        L_0x0119:
            java.lang.String r1 = "ALTER TABLE TBL_NEWS ADD isread INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_outgo_date on tbl_outgoinfo(outgodate)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_outgo_ymd on tbl_outgoinfo(ymd)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_outgo_account_id ON TBL_OUTGOINFO ( accountid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_outgo_project_id ON TBL_OUTGOINFO ( projectid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_outgo_type_id ON TBL_OUTGOINFO ( subtypeid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_income_date on tbl_incomeinfo(incomedate)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_income_ymd on tbl_incomeinfo(ymd)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_income_account_id ON TBL_INCOMEINFO ( accountid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_income_project_id ON TBL_INCOMEINFO ( projectid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_income_type_id ON TBL_INCOMEINFO ( typeid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_transfer_date on TBL_TRANSFERINFO(date)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_transfer_ymd on TBL_TRANSFERINFO(ymd)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_transfer_in_account_id ON TBL_TRANSFERINFO ( transferinaccountid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_transfer_out_account_id ON TBL_TRANSFERINFO ( transferoutaccountid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE TABLE TBL_TRADETARGET (id INTEGER PRIMARY KEY, name varchar(20), enable INTEGER DEFAULT 1, uuid varchar(36), orderno INTEGER DEFAULT 0, updatestatus INTEGER DEFAULT 0)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_INCOMEINFO ADD targetid INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_OUTGOINFO ADD targetid INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_SCHEDULEINCOMEINFO ADD targetid INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_SCHEDULEOUTGOINFO ADD targetid INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_outgo_target_id ON TBL_OUTGOINFO ( targetid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_income_target_id ON TBL_INCOMEINFO ( targetid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = ""
            java.lang.String r2 = "select * from TBL_USERINFO"
            r3 = 0
            android.database.Cursor r2 = r9.rawQuery(r2, r3)     // Catch:{ Exception -> 0x041d, all -> 0x042b }
            if (r2 == 0) goto L_0x01a2
            boolean r3 = r2.moveToFirst()     // Catch:{ Exception -> 0x045e, all -> 0x045b }
            if (r3 == 0) goto L_0x01a2
            java.lang.String r3 = "localpassword"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x045e, all -> 0x045b }
            java.lang.String r1 = r2.getString(r3)     // Catch:{ Exception -> 0x045e, all -> 0x045b }
        L_0x01a2:
            if (r2 == 0) goto L_0x01a7
            r2.close()     // Catch:{ all -> 0x0426 }
        L_0x01a7:
            if (r1 == 0) goto L_0x01b9
            int r1 = r1.length()     // Catch:{ all -> 0x0426 }
            if (r1 <= 0) goto L_0x01b9
            java.lang.String r1 = "UPDATE TBL_USERINFO set localpassword = '', isneedlocalpassword = 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            if (r10 == 0) goto L_0x01b9
            r10.a()     // Catch:{ all -> 0x0426 }
        L_0x01b9:
            r1 = r8
        L_0x01ba:
            if (r1 != 0) goto L_0x01c4
            java.lang.String r2 = "1.0.0.7"
            boolean r2 = r0.equalsIgnoreCase(r2)     // Catch:{ all -> 0x0426 }
            if (r2 == 0) goto L_0x01cf
        L_0x01c4:
            java.lang.String r1 = "CREATE INDEX index_income_parent_id ON TBL_INCOMEMEMBERINFO ( incomeid )"
            r9.execSQL(r1)     // Catch:{ Exception -> 0x0458, all -> 0x0433 }
            java.lang.String r1 = "CREATE INDEX index_outgo_parent_id ON TBL_OUTGOMEMBERINFO ( outgoid )"
            r9.execSQL(r1)     // Catch:{ Exception -> 0x0458, all -> 0x0433 }
        L_0x01ce:
            r1 = r8
        L_0x01cf:
            if (r1 != 0) goto L_0x01d9
            java.lang.String r2 = "1.0.0.8"
            boolean r2 = r0.equalsIgnoreCase(r2)     // Catch:{ all -> 0x0426 }
            if (r2 == 0) goto L_0x01df
        L_0x01d9:
            java.lang.String r1 = "ALTER TABLE TBL_USERINFO ADD appversion varchar(36)"
            r9.execSQL(r1)     // Catch:{ Exception -> 0x0455, all -> 0x0435 }
        L_0x01de:
            r1 = r8
        L_0x01df:
            if (r1 != 0) goto L_0x01e9
            java.lang.String r2 = "1.0.0.9"
            boolean r2 = r0.equalsIgnoreCase(r2)     // Catch:{ all -> 0x0426 }
            if (r2 == 0) goto L_0x02ec
        L_0x01e9:
            java.lang.String r1 = "ALTER TABLE TBL_OUTGOSUBTYPEINFO ADD star INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_OUTGOSUBTYPEINFO ADD refcount INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "SELECT subtypeid as _sid, count(*) as _count from TBL_OUTGOINFO where scheduleoutgoid=0 GROUP BY subtypeid ORDER BY _count DESC "
            r2 = 0
            android.database.Cursor r1 = r9.rawQuery(r1, r2)     // Catch:{ all -> 0x0437 }
            if (r1 == 0) goto L_0x023c
            boolean r2 = r1.moveToFirst()     // Catch:{ all -> 0x0453 }
            if (r2 == 0) goto L_0x023c
        L_0x0202:
            java.lang.String r2 = "_sid"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ all -> 0x0453 }
            long r2 = r1.getLong(r2)     // Catch:{ all -> 0x0453 }
            java.lang.String r4 = "_count"
            int r4 = r1.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x0453 }
            long r4 = r1.getLong(r4)     // Catch:{ all -> 0x0453 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0453 }
            r6.<init>()     // Catch:{ all -> 0x0453 }
            java.lang.String r7 = "UPDATE TBL_OUTGOSUBTYPEINFO SET refcount="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0453 }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ all -> 0x0453 }
            java.lang.String r5 = " WHERE id="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0453 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ all -> 0x0453 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0453 }
            r9.execSQL(r2)     // Catch:{ all -> 0x0453 }
            boolean r2 = r1.moveToNext()     // Catch:{ all -> 0x0453 }
            if (r2 != 0) goto L_0x0202
        L_0x023c:
            if (r1 == 0) goto L_0x0241
            r1.close()     // Catch:{ all -> 0x0426 }
        L_0x0241:
            java.lang.String r1 = "TBL_ACCOUNTINFO"
            a(r9, r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "TBL_PROJECTINFO"
            a(r9, r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "TBL_MEMBERINFO"
            a(r9, r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "TBL_INCOMEMAINTYPEINFO"
            a(r9, r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "TBL_OUTGOMAINTYPEINFO"
            a(r9, r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "TBL_OUTGOSUBTYPEINFO"
            a(r9, r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "TBL_TRADETARGET"
            a(r9, r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO tbl_accounttype (name, uuid, orderno) SELECT DISTINCT '债权债务', '3', 3) WHERE NOT EXISTS (SELECT uuid FROM tbl_accounttype WHERE uuid = '3')"
            r9.execSQL(r1)     // Catch:{ Exception -> 0x043f }
        L_0x0269:
            java.lang.String r1 = "INSERT INTO TBL_OUTGOSUBTYPEINFO (id, uuid, name, pinyin, enable, orderno, isdefault, updatestatus) SELECT DISTINCT 70009, '1610', '利息支出', 'lixizhichu', 1, 8, 1, 1 WHERE NOT EXISTS (SELECT uuid FROM TBL_OUTGOSUBTYPEINFO WHERE uuid = '1610')"
            r9.execSQL(r1)     // Catch:{ Exception -> 0x0449 }
        L_0x026e:
            java.lang.String r1 = "ALTER TABLE TBL_TRANSFERINFO ADD type INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_TRANSFERINFO ADD ownerid INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_OUTGOINFO ADD paybackid INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_INCOMEINFO ADD paybackid INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE TABLE TBL_LOAN (id INTEGER PRIMARY KEY AUTOINCREMENT, uuid varchar(36), type INTEGER DEFAULT 0, date INTEGER DEFAULT 0, ymd INTEGER DEFAULT 0, money decimal(16,2) DEFAULT 0, expectprofit decimal(16,2) DEFAULT 0, expectdate INTEGER DEFAULT NULL, updatestatus INTEGER DEFAULT 0, accountid INTEGER DEFAULT 0, debtid INTEGER DEFAULT 0, alertid INTEGER DEFAULT 0, comment varchar(100), isdelete INTEGER DEFAULT 0)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE TABLE TBL_PAYBACK (id INTEGER PRIMARY KEY AUTOINCREMENT, uuid varchar(36), type INTEGER DEFAULT 0, money decimal(16,2) DEFAULT 0, date INTEGER DEFAULT 0, ymd INTEGER DEFAULT 0, updatestatus INTEGER DEFAULT 0, profit decimal(16,2) DEFAULT 0, accountid INTEGER DEFAULT 0, debtid INTEGER DEFAULT 0, comment varchar(100), isdelete INTEGER DEFAULT 0)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE TABLE TBL_ALERT (id INTEGER PRIMARY KEY AUTOINCREMENT, type INTEGER DEFAULT 0, day INTEGER DEFAULT 0, nextday INTEGER DEFAULT 0, alertstyle INTEGER DEFAULT 0, destid INTEGER DEFAULT 0, comment varchar(100))"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_outgo_paybackid ON TBL_OUTGOINFO ( paybackid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_income_paybackid ON TBL_INCOMEINFO ( paybackid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_trans_type ON TBL_TRANSFERINFO ( type )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_trans_ownerid ON TBL_TRANSFERINFO ( ownerid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_loan_uuid ON TBL_LOAN ( uuid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_loan_date ON TBL_LOAN ( date )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_loan_ymd ON TBL_LOAN ( ymd )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_loan_updatestatus ON TBL_LOAN ( updatestatus )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_loan_accountid ON TBL_LOAN ( accountid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_loan_debtid ON TBL_LOAN ( debtid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_payback_uuid ON TBL_PAYBACK ( uuid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_payback_date ON TBL_PAYBACK ( date )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_payback_ymd ON TBL_PAYBACK ( ymd )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_payback_updatestatus ON TBL_PAYBACK ( updatestatus )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_payback_accountid ON TBL_PAYBACK ( accountid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_payback_debtid ON TBL_PAYBACK ( debtid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_alert_style ON TBL_ALERT ( alertstyle )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "CREATE INDEX index_alert_destid ON TBL_ALERT ( destid )"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            r1 = r8
        L_0x02ec:
            if (r1 != 0) goto L_0x02f6
            java.lang.String r2 = "1.0.1.0"
            boolean r2 = r0.equalsIgnoreCase(r2)     // Catch:{ all -> 0x0426 }
            if (r2 == 0) goto L_0x0305
        L_0x02f6:
            java.lang.String r1 = "SMSFilter"
            r2 = 0
            com.wacai.a.b(r1, r2)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "PropAcceptCopyright"
            r2 = 1
            com.wacai.a.b(r1, r2)     // Catch:{ all -> 0x0426 }
            r1 = r8
        L_0x0305:
            if (r1 != 0) goto L_0x030f
            java.lang.String r2 = "1.0.1.1"
            boolean r2 = r0.equalsIgnoreCase(r2)     // Catch:{ all -> 0x0426 }
            if (r2 == 0) goto L_0x0333
        L_0x030f:
            java.lang.String r1 = "CREATE TABLE TBL_WEIBOINFO (id INTEGER PRIMARY KEY AUTOINCREMENT, type INTEGER, name varchar(16), apk varchar(16), aps varchar(48), tk varchar(48) DEFAULT NULL, tks varchar(48) DEFAULT NULL, wac varchar(20) DEFAULT NULL, isrwc INTEGER DEFAULT 0)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "2153644806"
            java.lang.String r1 = com.wacai.a.h.a(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r2 = "5885e41e8c905053c51b828a12ee924c"
            java.lang.String r2 = com.wacai.a.h.a(r2)     // Catch:{ all -> 0x0426 }
            java.lang.String r3 = "INSERT INTO TBL_WEIBOINFO (type, name, apk, aps, wac) VALUES (1, '新浪微博', '%s', '%s', '1741280725')"
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0426 }
            r5 = 0
            r4[r5] = r1     // Catch:{ all -> 0x0426 }
            r1 = 1
            r4[r1] = r2     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = java.lang.String.format(r3, r4)     // Catch:{ all -> 0x0426 }
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            r1 = r8
        L_0x0333:
            if (r1 != 0) goto L_0x033d
            java.lang.String r2 = "1.0.1.2"
            boolean r2 = r0.equalsIgnoreCase(r2)     // Catch:{ all -> 0x0426 }
            if (r2 == 0) goto L_0x0341
        L_0x033d:
            b(r9)     // Catch:{ all -> 0x0426 }
            r1 = r8
        L_0x0341:
            if (r1 != 0) goto L_0x034b
            java.lang.String r2 = "1.0.1.3"
            boolean r2 = r0.equalsIgnoreCase(r2)     // Catch:{ all -> 0x0426 }
            if (r2 == 0) goto L_0x037e
        L_0x034b:
            java.lang.String r1 = "ALTER TABLE TBL_USERINFO ADD uid varchar(36)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "ALTER TABLE TBL_USERINFO ADD email varchar(36)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "c48d9039f0c6410c9cf1a65acec93907"
            java.lang.String r1 = com.wacai.a.h.a(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r2 = "3720c0df8054dca31299b8647a710af0"
            java.lang.String r2 = com.wacai.a.h.a(r2)     // Catch:{ all -> 0x0426 }
            java.lang.String r3 = "INSERT INTO TBL_WEIBOINFO (type, name, apk, aps, wac) VALUES (2, '腾讯微博', '%s', '%s', 'xiaowamm')"
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0426 }
            r5 = 0
            r4[r5] = r1     // Catch:{ all -> 0x0426 }
            r1 = 1
            r4[r1] = r2     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = java.lang.String.format(r3, r4)     // Catch:{ all -> 0x0426 }
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "UPDATE tbl_incomemaintypeinfo SET orderno = 13 WHERE uuid = '13'"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO tbl_incomemaintypeinfo (name, orderno, uuid, isdefault, updatestatus, pinyin) SELECT DISTINCT '报销款', 12, '14', 1, 1, 'baoxiaokuan' WHERE NOT EXISTS (SELECT uuid FROM tbl_incomemaintypeinfo WHERE uuid = '14')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            r1 = r8
        L_0x037e:
            if (r1 != 0) goto L_0x0388
            java.lang.String r2 = "1.0.1.4"
            boolean r2 = r0.equalsIgnoreCase(r2)     // Catch:{ all -> 0x0426 }
            if (r2 == 0) goto L_0x03cf
        L_0x0388:
            java.lang.String r1 = "ALTER TABLE TBL_ACCOUNTINFO ADD star INTEGER DEFAULT 0"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "UPDATE TBL_ACCOUNTINFO SET star = 1 WHERE uuid IN (1,2,3,4)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_ACCOUNTTYPE (name, orderno, uuid) SELECT DISTINCT '投资账户', 4, '4' FROM TBL_ACCOUNTTYPE WHERE NOT EXISTS (SELECT uuid FROM TBL_ACCOUNTTYPE WHERE uuid = '4')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_ACCOUNTTYPE (name, orderno, uuid) SELECT DISTINCT '储值卡', 5, '5' FROM TBL_ACCOUNTTYPE WHERE NOT EXISTS (SELECT uuid FROM TBL_ACCOUNTTYPE WHERE uuid = '5')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_ACCOUNTTYPE (name, orderno, uuid) SELECT DISTINCT '网上支付', 6, '6'FROM TBL_ACCOUNTTYPE WHERE NOT EXISTS (SELECT uuid FROM TBL_ACCOUNTTYPE WHERE uuid = '6')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "INSERT INTO TBL_ACCOUNTTYPE (name, orderno, uuid) SELECT DISTINCT '虚拟账户', 7, '7' FROM TBL_ACCOUNTTYPE WHERE NOT EXISTS (SELECT uuid FROM TBL_ACCOUNTTYPE WHERE uuid = '7')"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "update TBL_ACCOUNTINFO set type=6 where uuid='4'"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "UPDATE TBL_TRADETARGET SET pinyin = lower(pinyin)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "UPDATE TBL_PROJECTINFO SET pinyin = lower(pinyin)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "UPDATE TBL_OUTGOSUBTYPEINFO SET pinyin = lower(pinyin)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "UPDATE TBL_OUTGOMAINTYPEINFO SET pinyin = lower(pinyin)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "UPDATE TBL_MEMBERINFO SET pinyin = lower(pinyin)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "UPDATE TBL_INCOMEMAINTYPEINFO SET pinyin = lower(pinyin)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            java.lang.String r1 = "UPDATE TBL_ACCOUNTINFO SET pinyin = lower(pinyin)"
            r9.execSQL(r1)     // Catch:{ all -> 0x0426 }
            r1 = r8
        L_0x03cf:
            if (r1 != 0) goto L_0x03d9
            java.lang.String r2 = "1.0.1.5"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ all -> 0x0426 }
            if (r0 == 0) goto L_0x0460
        L_0x03d9:
            java.lang.String r0 = "ALTER TABLE TBL_USERINFO ADD defaultmodifytime DATE DEFAULT NULL"
            r9.execSQL(r0)     // Catch:{ all -> 0x0426 }
            java.lang.String r0 = "ALTER TABLE TBL_SHORTCUTSINFO ADD isdelete INTEGER DEFAULT 0"
            r9.execSQL(r0)     // Catch:{ all -> 0x0426 }
            java.lang.String r0 = "ALTER TABLE TBL_SHORTCUTSINFO ADD updatestatus INTEGER DEFAULT 0"
            r9.execSQL(r0)     // Catch:{ all -> 0x0426 }
            java.lang.String r0 = "ALTER TABLE TBL_SHORTCUTSINFO ADD uuid varchar(36)"
            r9.execSQL(r0)     // Catch:{ all -> 0x0426 }
            java.lang.String r0 = "ALTER TABLE TBL_SHORTCUTSINFO ADD targetid INTEGER DEFAULT 0"
            r9.execSQL(r0)     // Catch:{ all -> 0x0426 }
            java.lang.String r0 = "CREATE TABLE TBL_USESTATISTIC(id INTEGER PRIMARY KEY AUTOINCREMENT, ymd INTEGER, usecount INTEGER)"
            r9.execSQL(r0)     // Catch:{ all -> 0x0426 }
            java.lang.String r0 = "ALTER TABLE TBL_ALERT ADD isread INTEGER DEFAULT 0"
            r9.execSQL(r0)     // Catch:{ all -> 0x0426 }
            java.lang.String r0 = "UPDATE TBL_MYNOTE SET updatestatus = 0"
            r9.execSQL(r0)     // Catch:{ all -> 0x0426 }
            r0 = r8
        L_0x0402:
            if (r0 == 0) goto L_0x0415
            java.lang.String r0 = "UPDATE TBL_DBINFO SET version = '%s'"
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0426 }
            r2 = 0
            java.lang.String r3 = "1.0.1.6"
            r1[r2] = r3     // Catch:{ all -> 0x0426 }
            java.lang.String r0 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x0426 }
            r9.execSQL(r0)     // Catch:{ all -> 0x0426 }
        L_0x0415:
            r9.setTransactionSuccessful()     // Catch:{ all -> 0x0426 }
            r9.endTransaction()
            goto L_0x0005
        L_0x041d:
            r2 = move-exception
            r2 = r4
        L_0x041f:
            if (r2 == 0) goto L_0x01a7
            r2.close()     // Catch:{ all -> 0x0426 }
            goto L_0x01a7
        L_0x0426:
            r0 = move-exception
            r9.endTransaction()
            throw r0
        L_0x042b:
            r0 = move-exception
            r1 = r4
        L_0x042d:
            if (r1 == 0) goto L_0x0432
            r1.close()     // Catch:{ all -> 0x0426 }
        L_0x0432:
            throw r0     // Catch:{ all -> 0x0426 }
        L_0x0433:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0426 }
        L_0x0435:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0426 }
        L_0x0437:
            r0 = move-exception
            r1 = r4
        L_0x0439:
            if (r1 == 0) goto L_0x043e
            r1.close()     // Catch:{ all -> 0x0426 }
        L_0x043e:
            throw r0     // Catch:{ all -> 0x0426 }
        L_0x043f:
            r1 = move-exception
            java.lang.String r1 = "Frame"
            java.lang.String r2 = "Update Account '债权债务' already exist!"
            android.util.Log.e(r1, r2)     // Catch:{ all -> 0x0426 }
            goto L_0x0269
        L_0x0449:
            r1 = move-exception
            java.lang.String r1 = "Frame"
            java.lang.String r2 = "Update OutgoSubType '利息支出' already exist!"
            android.util.Log.e(r1, r2)     // Catch:{ all -> 0x0426 }
            goto L_0x026e
        L_0x0453:
            r0 = move-exception
            goto L_0x0439
        L_0x0455:
            r1 = move-exception
            goto L_0x01de
        L_0x0458:
            r1 = move-exception
            goto L_0x01ce
        L_0x045b:
            r0 = move-exception
            r1 = r2
            goto L_0x042d
        L_0x045e:
            r3 = move-exception
            goto L_0x041f
        L_0x0460:
            r0 = r1
            goto L_0x0402
        L_0x0462:
            r1 = r2
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.c.a(android.database.sqlite.SQLiteDatabase, com.wacai.d):void");
    }

    private static void a(SQLiteDatabase sQLiteDatabase, String str) {
        Cursor cursor;
        sQLiteDatabase.execSQL(String.format("ALTER TABLE %s ADD pinyin varchar(36)", str));
        try {
            Cursor rawQuery = sQLiteDatabase.rawQuery(String.format("select id, name from %s", str), null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        do {
                            sQLiteDatabase.execSQL(String.format("update %s set pinyin = '%s' where id = %d", str, aa.e(g.a(rawQuery.getString(rawQuery.getColumnIndexOrThrow("name")))), Integer.valueOf(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("id")))));
                        } while (rawQuery.moveToNext());
                        if (rawQuery != null) {
                            rawQuery.close();
                            return;
                        }
                        return;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
                return;
            }
            return;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    private static void b(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor;
        sQLiteDatabase.execSQL("ALTER TABLE TBL_TRADETARGET ADD star INTEGER DEFAULT 0");
        sQLiteDatabase.execSQL("ALTER TABLE TBL_TRADETARGET ADD refcount INTEGER DEFAULT 0");
        try {
            Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT _tid as _targetid, sum(_count) as _usecount from  (SELECT targetid as _tid, count(*) as _count FROM TBL_OUTGOINFO  WHERE targetid <> 0 AND scheduleoutgoid=0 GROUP BY targetid  UNION ALL  SELECT targetid as _tid, count(*) as _count from TBL_INCOMEINFO  WHERE targetid <> 0 AND scheduleincomeid=0 GROUP BY targetid )  GROUP BY _targetid", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        do {
                            sQLiteDatabase.execSQL("UPDATE TBL_TRADETARGET SET refcount=" + rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_usecount")) + " WHERE id=" + rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_targetid")));
                        } while (rawQuery.moveToNext());
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }
}
