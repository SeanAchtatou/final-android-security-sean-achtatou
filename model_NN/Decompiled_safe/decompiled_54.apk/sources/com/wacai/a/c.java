package com.wacai.a;

import android.net.Uri;

public final class c {
    private static final Uri b = Uri.parse("content://telephony/carriers/preferapn");
    public String a;
    private String c;
    private int d;
    private String e;
    private String f;
    private String g;
    private String h;

    /* JADX WARNING: Removed duplicated region for block: B:38:0x00a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.wacai.a.c a() {
        /*
            r10 = 0
            r9 = -1
            com.wacai.e r0 = com.wacai.e.c()     // Catch:{ Exception -> 0x0095, all -> 0x009e }
            android.content.Context r0 = r0.a()     // Catch:{ Exception -> 0x0095, all -> 0x009e }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0095, all -> 0x009e }
            android.net.Uri r1 = com.wacai.a.c.b     // Catch:{ Exception -> 0x0095, all -> 0x009e }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0095, all -> 0x009e }
            if (r0 == 0) goto L_0x0020
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            if (r1 != 0) goto L_0x0027
        L_0x0020:
            if (r0 == 0) goto L_0x0025
            r0.close()
        L_0x0025:
            r0 = r10
        L_0x0026:
            return r0
        L_0x0027:
            java.lang.String r1 = "_id"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            java.lang.String r2 = "name"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            java.lang.String r3 = "apn"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            java.lang.String r4 = "proxy"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            java.lang.String r5 = "port"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            java.lang.String r6 = "user"
            int r6 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            java.lang.String r7 = "password"
            int r7 = r0.getColumnIndex(r7)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            com.wacai.a.c r8 = new com.wacai.a.c     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            r8.<init>()     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            if (r1 == r9) goto L_0x005e
            short r1 = r0.getShort(r1)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            r8.d = r1     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
        L_0x005e:
            if (r2 == r9) goto L_0x0066
            java.lang.String r1 = r0.getString(r2)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            r8.c = r1     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
        L_0x0066:
            if (r3 == r9) goto L_0x006e
            java.lang.String r1 = r0.getString(r3)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            r8.a = r1     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
        L_0x006e:
            if (r4 == r9) goto L_0x0076
            java.lang.String r1 = r0.getString(r4)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            r8.e = r1     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
        L_0x0076:
            if (r5 == r9) goto L_0x007e
            java.lang.String r1 = r0.getString(r5)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            r8.f = r1     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
        L_0x007e:
            if (r6 == r9) goto L_0x0086
            java.lang.String r1 = r0.getString(r6)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            r8.g = r1     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
        L_0x0086:
            if (r7 == r9) goto L_0x008e
            java.lang.String r1 = r0.getString(r7)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            r8.h = r1     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
        L_0x008e:
            if (r0 == 0) goto L_0x0093
            r0.close()
        L_0x0093:
            r0 = r8
            goto L_0x0026
        L_0x0095:
            r0 = move-exception
            r0 = r10
        L_0x0097:
            if (r0 == 0) goto L_0x009c
            r0.close()
        L_0x009c:
            r0 = r10
            goto L_0x0026
        L_0x009e:
            r0 = move-exception
            r1 = r10
        L_0x00a0:
            if (r1 == 0) goto L_0x00a5
            r1.close()
        L_0x00a5:
            throw r0
        L_0x00a6:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x00a0
        L_0x00ab:
            r1 = move-exception
            goto L_0x0097
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.a.c.a():com.wacai.a.c");
    }
}
