package com.wacai.a;

public abstract class h {
    public static String a(String str) {
        if (str == null || str.length() <= 0) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer(str.length());
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt >= '0' && charAt <= '9') {
                stringBuffer.insert(0, (char) ('i' - str.charAt(i)));
            } else if (charAt >= 'a' && charAt <= 'z') {
                stringBuffer.insert(0, (char) (219 - str.charAt(i)));
            } else if (charAt < 'A' || charAt > 'Z') {
                stringBuffer.insert(0, str.charAt(i));
            } else {
                stringBuffer.insert(0, (char) (155 - str.charAt(i)));
            }
        }
        return stringBuffer.toString();
    }

    public static byte[] a() {
        return new byte[]{101, 54, 53, 98, 55, 57, 101, 64, 52, 35, 100, 36, 50, 37, 57, 94, 48, 38, 102, 42, 98, 56, 97, 99, 51, 102, 63, 38, 41, 36, 37, 40, 42, 64, 33, 126, 33};
    }

    public static byte[] a(byte[] bArr, int i, int i2, byte[] bArr2) {
        if (bArr == null || i2 <= 0 || i >= bArr.length) {
            return bArr;
        }
        byte[] a = bArr2 == null ? a() : bArr2;
        int min = Math.min(i2, bArr.length - i);
        int i3 = 0;
        int i4 = 0;
        while (i4 < min) {
            if (i3 >= a.length) {
                i3 = 0;
            }
            int i5 = i4 + i;
            bArr[i5] = (byte) (bArr[i5] ^ a[i3]);
            i4++;
            i3++;
        }
        return bArr;
    }
}
