package com.wacai.a;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URI;
import java.util.Enumeration;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParamBean;
import org.apache.http.params.HttpParams;

public abstract class e {
    public static HttpResponse a(URI uri, HttpUriRequest httpUriRequest, int i) {
        NetworkInfo activeNetworkInfo;
        c a;
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpParams params = defaultHttpClient.getParams();
        HttpConnectionParamBean httpConnectionParamBean = new HttpConnectionParamBean(params);
        httpConnectionParamBean.setSoTimeout(i);
        httpConnectionParamBean.setConnectionTimeout(i);
        if (!(params == null || (activeNetworkInfo = ((ConnectivityManager) com.wacai.e.c().a().getSystemService("connectivity")).getActiveNetworkInfo()) == null || !activeNetworkInfo.isConnected() || activeNetworkInfo.getType() == 1 || (a = c.a()) == null)) {
            String str = a.a;
            if (c() == i.China_Unicom && (str.startsWith("uniwap") || str.startsWith("cmwap"))) {
                params.setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80));
            }
        }
        HttpHost httpHost = null;
        if (uri != null) {
            String scheme = uri.getScheme();
            httpHost = (scheme == null || !uri.getScheme().equals("https")) ? new HttpHost(uri.getHost(), 80) : new HttpHost(uri.getHost(), 443, scheme);
        }
        return defaultHttpClient.execute(httpHost, httpUriRequest);
    }

    public static boolean a() {
        ConnectivityManager connectivityManager = (ConnectivityManager) com.wacai.e.c().a().getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected();
    }

    public static String b() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
        }
        return null;
    }

    public static i c() {
        String simOperator = ((TelephonyManager) com.wacai.e.c().a().getSystemService("phone")).getSimOperator();
        String substring = (simOperator == null || simOperator.length() < 5) ? null : simOperator.substring(3, 5);
        if (substring != null) {
            if (substring.equals("00") || substring.equals("02")) {
                return i.China_Mobile;
            }
            if (substring.equals("01")) {
                return i.China_Unicom;
            }
            if (substring.equals("03") || substring.equals("05")) {
                return i.China_Telecom;
            }
        }
        return i.Unknown;
    }
}
