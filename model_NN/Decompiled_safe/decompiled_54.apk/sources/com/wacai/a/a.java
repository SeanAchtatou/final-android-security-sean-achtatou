package com.wacai.a;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public final class a {
    public static final int[] a = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    public static final int[] b = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    public int c;
    public int d;
    public int e;
    public int f;
    public int g;
    public int h;

    private a() {
    }

    public a(long j) {
        this(new Date(j));
    }

    public a(Date date) {
        this();
        if (date != null) {
            Calendar instance = Calendar.getInstance();
            instance.setTime(date);
            this.c = instance.get(1);
            this.d = instance.get(2) + 1;
            this.e = instance.get(5);
            this.f = instance.get(11);
            this.g = instance.get(12);
            this.h = instance.get(13);
        }
    }

    private static int a(int i) {
        switch (i) {
            case 1:
                return 6;
            case 2:
            default:
                return 0;
            case 3:
                return 1;
            case 4:
                return 2;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 5;
        }
    }

    public static int a(int i, int i2) {
        int[] iArr = new GregorianCalendar().isLeapYear(i) ? b : a;
        if (i2 > iArr.length) {
            return -1;
        }
        return iArr[i2 - 1];
    }

    public static long a(long j) {
        return new a(j).a();
    }

    public static a a(int i, int i2, int i3) {
        a aVar = new a(0);
        aVar.c = i;
        aVar.d = i2;
        aVar.e = i3;
        aVar.f = 0;
        aVar.g = 0;
        aVar.h = 0;
        return aVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:73:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:75:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0039  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(int r11, java.util.Date r12, java.util.Date r13) {
        /*
            r9 = 1000(0x3e8, double:4.94E-321)
            r5 = 23
            r4 = 1
            r3 = 59
            r8 = 0
            java.util.Calendar r0 = java.util.Calendar.getInstance()
            java.util.Date r1 = r0.getTime()
            switch(r11) {
                case 0: goto L_0x001f;
                case 1: goto L_0x0014;
                case 2: goto L_0x0051;
                case 3: goto L_0x0046;
                case 4: goto L_0x0094;
                case 5: goto L_0x0094;
                case 6: goto L_0x00cf;
                case 7: goto L_0x00cf;
                case 8: goto L_0x0126;
                case 9: goto L_0x0154;
                case 10: goto L_0x0181;
                case 11: goto L_0x01ae;
                case 12: goto L_0x01dd;
                case 13: goto L_0x020a;
                default: goto L_0x0013;
            }
        L_0x0013:
            return
        L_0x0014:
            long r2 = r1.getTime()
            r4 = 86400000(0x5265c00, double:4.2687272E-316)
            long r2 = r2 - r4
            r1.setTime(r2)
        L_0x001f:
            com.wacai.a.a r0 = new com.wacai.a.a
            long r1 = r1.getTime()
            r0.<init>(r1)
            r0.f = r8
            r0.g = r8
            r0.h = r8
            if (r12 == 0) goto L_0x0037
            long r1 = r0.b()
            r12.setTime(r1)
        L_0x0037:
            if (r13 == 0) goto L_0x0013
            long r0 = r0.b()
            r2 = 86400000(0x5265c00, double:4.2687272E-316)
            long r0 = r0 + r2
            long r0 = r0 - r9
            r13.setTime(r0)
            goto L_0x0013
        L_0x0046:
            long r2 = r1.getTime()
            r4 = 604800000(0x240c8400, double:2.988109026E-315)
            long r2 = r2 - r4
            r1.setTime(r2)
        L_0x0051:
            r0.setTime(r1)
            r2 = 7
            int r0 = r0.get(r2)
            int r0 = a(r0)
            long r2 = r1.getTime()
            r4 = 3600000(0x36ee80, double:1.7786363E-317)
            long r6 = (long) r0
            long r4 = r4 * r6
            r6 = 24
            long r4 = r4 * r6
            long r2 = r2 - r4
            r1.setTime(r2)
            com.wacai.a.a r0 = new com.wacai.a.a
            long r1 = r1.getTime()
            r0.<init>(r1)
            r0.f = r8
            r0.g = r8
            r0.h = r8
            if (r12 == 0) goto L_0x0085
            long r1 = r0.b()
            r12.setTime(r1)
        L_0x0085:
            if (r13 == 0) goto L_0x0013
            long r0 = r0.b()
            r2 = 604800000(0x240c8400, double:2.988109026E-315)
            long r0 = r0 + r2
            long r0 = r0 - r9
            r13.setTime(r0)
            goto L_0x0013
        L_0x0094:
            com.wacai.a.a r0 = new com.wacai.a.a
            long r1 = r1.getTime()
            r0.<init>(r1)
            r1 = 5
            if (r11 != r1) goto L_0x00a3
            r0.d()
        L_0x00a3:
            r0.e = r4
            r0.f = r8
            r0.g = r8
            r0.h = r8
            if (r12 == 0) goto L_0x00b4
            long r1 = r0.b()
            r12.setTime(r1)
        L_0x00b4:
            int r1 = r0.c
            int r2 = r0.d
            int r1 = a(r1, r2)
            r0.e = r1
            r0.f = r5
            r0.g = r3
            r0.h = r3
            if (r13 == 0) goto L_0x0013
            long r0 = r0.b()
            r13.setTime(r0)
            goto L_0x0013
        L_0x00cf:
            com.wacai.a.a r0 = new com.wacai.a.a
            long r1 = r1.getTime()
            r0.<init>(r1)
            int r1 = r0.d
            int r2 = r1 % 3
            if (r2 == 0) goto L_0x0121
            int r1 = r1 - r2
            int r1 = r1 + 1
            r0.d = r1
        L_0x00e3:
            r1 = 7
            if (r11 != r1) goto L_0x00ef
            r0.d()
            r0.d()
            r0.d()
        L_0x00ef:
            r0.e = r4
            r0.f = r8
            r0.g = r8
            r0.h = r8
            if (r12 == 0) goto L_0x0100
            long r1 = r0.b()
            r12.setTime(r1)
        L_0x0100:
            r0.c()
            r0.c()
            int r1 = r0.c
            int r2 = r0.d
            int r1 = a(r1, r2)
            r0.e = r1
            r0.f = r5
            r0.g = r3
            r0.h = r3
            if (r13 == 0) goto L_0x0013
            long r0 = r0.b()
            r13.setTime(r0)
            goto L_0x0013
        L_0x0121:
            r2 = 2
            int r1 = r1 - r2
            r0.d = r1
            goto L_0x00e3
        L_0x0126:
            com.wacai.a.a r0 = new com.wacai.a.a
            long r1 = r1.getTime()
            r0.<init>(r1)
            r0.d = r4
            r0.e = r4
            r0.f = r8
            r0.g = r8
            r0.h = r8
            if (r12 == 0) goto L_0x0142
            long r1 = r0.b()
            r12.setTime(r1)
        L_0x0142:
            int r1 = r0.c
            int r1 = r1 + 1
            r0.c = r1
            if (r13 == 0) goto L_0x0013
            long r0 = r0.b()
            long r0 = r0 - r9
            r13.setTime(r0)
            goto L_0x0013
        L_0x0154:
            com.wacai.a.a r0 = new com.wacai.a.a
            long r1 = r1.getTime()
            r0.<init>(r1)
            r0.d = r4
            r0.e = r4
            r0.f = r8
            r0.g = r8
            r0.h = r8
            if (r13 == 0) goto L_0x0171
            long r1 = r0.b()
            long r1 = r1 - r9
            r13.setTime(r1)
        L_0x0171:
            int r1 = r0.c
            int r1 = r1 - r4
            r0.c = r1
            if (r12 == 0) goto L_0x0013
            long r0 = r0.b()
            r12.setTime(r0)
            goto L_0x0013
        L_0x0181:
            com.wacai.a.a r0 = new com.wacai.a.a
            long r1 = r1.getTime()
            r0.<init>(r1)
            r0.f = r5
            r0.g = r3
            r0.h = r3
            if (r13 == 0) goto L_0x0199
            long r1 = r0.b()
            r13.setTime(r1)
        L_0x0199:
            r0.f = r8
            r0.g = r8
            r0.h = r8
            if (r12 == 0) goto L_0x0013
            long r0 = r0.b()
            r2 = 518400000(0x1ee62800, double:2.56123631E-315)
            long r0 = r0 - r2
            r12.setTime(r0)
            goto L_0x0013
        L_0x01ae:
            com.wacai.a.a r0 = new com.wacai.a.a
            long r1 = r1.getTime()
            r0.<init>(r1)
            r0.f = r5
            r0.g = r3
            r0.h = r3
            if (r13 == 0) goto L_0x01c6
            long r1 = r0.b()
            r13.setTime(r1)
        L_0x01c6:
            r0.f = r8
            r0.g = r8
            r0.h = r8
            if (r12 == 0) goto L_0x0013
            long r0 = r0.b()
            r2 = 2505600000(0x95586c00, double:1.237930882E-314)
            long r0 = r0 - r2
            r12.setTime(r0)
            goto L_0x0013
        L_0x01dd:
            com.wacai.a.a r0 = new com.wacai.a.a
            long r1 = r1.getTime()
            r0.<init>(r1)
            r0.f = r5
            r0.g = r3
            r0.h = r3
            if (r13 == 0) goto L_0x01f5
            long r1 = r0.b()
            r13.setTime(r1)
        L_0x01f5:
            r0.f = r8
            r0.g = r8
            r0.h = r8
            if (r12 == 0) goto L_0x0013
            long r0 = r0.b()
            r2 = 172800000(0xa4cb800, double:8.53745436E-316)
            long r0 = r0 - r2
            r12.setTime(r0)
            goto L_0x0013
        L_0x020a:
            com.wacai.a.a r0 = new com.wacai.a.a
            long r1 = r1.getTime()
            r0.<init>(r1)
            r0.f = r5
            r0.g = r3
            r0.h = r3
            if (r13 == 0) goto L_0x0222
            long r1 = r0.b()
            r13.setTime(r1)
        L_0x0222:
            r0.f = r8
            r0.g = r8
            r0.h = r8
            if (r12 == 0) goto L_0x0013
            long r0 = r0.b()
            r2 = 86400000(0x5265c00, double:4.2687272E-316)
            long r0 = r0 - r2
            r12.setTime(r0)
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai.a.a.a(int, java.util.Date, java.util.Date):void");
    }

    public static boolean a(Date date) {
        if (date == null) {
            return false;
        }
        return date.getDate() == a(date.getYear(), date.getMonth() + 1);
    }

    public static int b(int i, int i2, int i3) {
        Calendar instance = Calendar.getInstance();
        instance.set(i, i2 - 1, i3);
        return a(instance.get(7));
    }

    public static long b(long j) {
        return a((int) (j / 10000), (int) ((j % 10000) / 100), (int) (j % 100)).b();
    }

    public static a c(long j) {
        return a((int) (j / 10000), (int) ((j % 10000) / 100), (int) (j % 100));
    }

    public final long a() {
        return (long) ((this.c * 10000) + (this.d * 100) + this.e);
    }

    public final long b() {
        Calendar instance = Calendar.getInstance();
        instance.set(1, this.c);
        instance.set(2, this.d - 1);
        instance.set(5, this.e);
        instance.set(11, this.f);
        instance.set(12, this.g);
        instance.set(13, this.h);
        instance.set(14, 0);
        return instance.getTime().getTime();
    }

    public final void c() {
        if (this.d < 12) {
            this.d++;
            return;
        }
        this.c++;
        this.d = 1;
    }

    public final void d() {
        if (this.d > 1) {
            this.d--;
            return;
        }
        this.c--;
        this.d = 12;
    }
}
