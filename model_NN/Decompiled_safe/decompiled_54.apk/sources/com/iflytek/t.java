package com.iflytek;

import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

final class t extends p {
    private ConcurrentLinkedQueue b = new ConcurrentLinkedQueue();
    private /* synthetic */ b c;

    t(b bVar) {
        this.c = bVar;
    }

    /* access modifiers changed from: private */
    public void b() {
        try {
            this.b.clear();
        } catch (Exception e) {
        }
    }

    private boolean c() {
        switch (s.b[this.c.d.f().ordinal()]) {
            case 1:
            default:
                return false;
            case 2:
                d();
                return false;
            case 3:
                d();
                return true;
            case 4:
                this.c.d();
                this.c.b.a(3);
                return true;
        }
    }

    private boolean d() {
        String e = this.c.d.e();
        if (e != null) {
            if (this.c.d.d()) {
                this.c.b.b(e);
            } else {
                this.c.b.a(e);
            }
            return true;
        }
        this.c.b.b("");
        this.c.d();
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        byte[] bArr;
        setPriority(10);
        while (this.a) {
            a aVar = (a) this.b.poll();
            if (aVar != null) {
                switch (s.a[aVar.a.ordinal()]) {
                    case 1:
                        int intValue = ((Integer) aVar.c).intValue();
                        if (!(this.c.c == r.ready || this.c.c == r.beginRec || this.c.c == r.abortRec || (bArr = (byte[]) this.c.g.poll()) == null)) {
                            if (this.c.d.a(bArr, intValue)) {
                                if (this.c.d.c()) {
                                    c();
                                    break;
                                }
                            } else {
                                this.c.d();
                                this.c.b.a(2);
                                break;
                            }
                        }
                        break;
                    case 2:
                        String str = (String) aVar.b;
                        if (this.c.c == r.beginRec) {
                            if (this.c.a(b.f.b())) {
                                this.c.a.a();
                                if (this.c.d.a(str, b.f)) {
                                    this.c.c = r.recording;
                                    break;
                                } else {
                                    this.c.f();
                                    this.c.b.a(3);
                                    this.c.c = r.ready;
                                    break;
                                }
                            } else {
                                this.c.b.a(4);
                                break;
                            }
                        }
                        break;
                    case 3:
                        if (this.c.c == r.waitresult || this.c.c == r.abortRec) {
                            this.c.f();
                            if (this.c.c == r.waitresult && !this.c.d.b()) {
                                this.c.d();
                                this.c.b.a(2);
                                break;
                            } else {
                                Date date = new Date();
                                while (true) {
                                    if (this.a && this.c.c != r.abortRec && !c()) {
                                        if (new Date().getTime() - date.getTime() > 40000) {
                                            this.c.d();
                                            this.c.b.a(2);
                                            break;
                                        } else {
                                            b(20);
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case 4:
                        if (!(this.c.c == r.ready || this.c.c == r.uninit)) {
                            this.c.f();
                            b();
                            this.c.d.a("user abort\u0000");
                            this.c.c = r.ready;
                            break;
                        }
                }
                b(0);
            } else {
                b(20);
            }
        }
    }

    public final boolean a(a aVar) {
        return this.b.add(aVar);
    }
}
