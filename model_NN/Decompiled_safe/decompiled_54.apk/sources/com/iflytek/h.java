package com.iflytek;

import android.os.Build;
import java.lang.reflect.Field;

public final class h {
    private String a;
    private String b = "";
    private String c = "Android";
    private String d = (b("MANUFACTURER") + "|" + b("MODEL") + "|" + b("PRODUCT") + "|ANDROID" + Build.VERSION.RELEASE);
    private boolean e = true;
    private int f = 16000;

    public h() {
        if (b("MODEL").toLowerCase().startsWith("oms")) {
            this.c = "oms";
        }
    }

    private static String b(String str) {
        try {
            Field field = Build.class.getField(str);
            return field != null ? field.get(new Build()).toString() : "";
        } catch (Exception e2) {
            return "";
        }
    }

    public final String a() {
        return this.a;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final int b() {
        return this.f;
    }
}
