package com.iflytek;

import android.util.Log;
import com.iflytek.msc.MSC;
import com.iflytek.msc.MSCSessionInfo;
import java.io.UnsupportedEncodingException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public final class l {
    private static String c = "\u0000";
    private MSCSessionInfo a = new MSCSessionInfo();
    private char[] b = null;
    private boolean d = false;
    private String e = null;

    public static void a() {
        MSC.QISRFini();
    }

    public static boolean a(h hVar) {
        int i;
        String dVar = (new n().a() == 1 ? d.wifi : new m().a()).toString();
        c = (dVar.equalsIgnoreCase("net") ? "wap_proxy=none," : "wap_proxy=" + dVar + ",") + "auth=1,appid=" + hVar.a() + "," + "server_url=" + "http://dev.voicecloud.cn/index.htm" + ",timeout=" + 10000 + "\u0000";
        Log.d("wacai", "sessionBegin enter grammarlist:\u0000 init params:" + c);
        try {
            i = MSC.QISRInit(c.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            i = 0;
        }
        return i == 0;
    }

    private boolean a(byte[] bArr, int i, int i2) {
        if (this.b == null) {
            return false;
        }
        int QISRAudioWrite = MSC.QISRAudioWrite(this.b, bArr, i, i2, this.a);
        Log.d("test", "QISRAudioWrite complete, error code is :" + this.a.getQisrErrCode());
        return QISRAudioWrite == 0;
    }

    public final boolean a(String str) {
        boolean z;
        boolean z2;
        if (this.b == null) {
            return false;
        }
        try {
            z2 = MSC.QISRSessionEnd(this.b, str != null ? str.getBytes("UTF-8") : null) == 0;
            try {
                Log.d("test", "session end complete, error code is :" + this.a.getQisrErrCode());
            } catch (UnsupportedEncodingException e2) {
                UnsupportedEncodingException unsupportedEncodingException = e2;
                z = z2;
                e = unsupportedEncodingException;
                e.printStackTrace();
                z2 = z;
                this.b = null;
                return z2;
            }
        } catch (UnsupportedEncodingException e3) {
            e = e3;
            z = false;
            e.printStackTrace();
            z2 = z;
            this.b = null;
            return z2;
        }
        this.b = null;
        return z2;
    }

    public final boolean a(String str, h hVar) {
        String str2 = "" + "aue=speex-wb,ssm=1,sub=iat,auf=audio/L16,";
        String str3 = (str.contains("vsearch") ? str2 + "acous=vsearch," : str2 + "acous=anhui,") + "lang=" + str + ",";
        String str4 = (hVar.b() == 8000 ? str3 + "rate=8k" : str3 + "rate=16k") + "\u0000";
        this.d = false;
        Log.d("wacai", "sessionBegin enter grammarlist:\u0000 params:" + str4);
        try {
            this.b = MSC.QISRSessionBegin("\u0000".getBytes("UTF-8"), str4.getBytes("UTF-8"), this.a);
            Log.d("test", "session begin complete, error code : " + this.a.getQisrErrCode());
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        if (this.a.getQisrErrCode() == 10111) {
            try {
                MSC.QISRInit(c.getBytes("UTF-8"));
                this.b = MSC.QISRSessionBegin("\u0000".getBytes("UTF-8"), str4.getBytes("UTF-8"), this.a);
            } catch (UnsupportedEncodingException e3) {
                e3.printStackTrace();
            }
        }
        return this.a.getQisrErrCode() == 0;
    }

    public final boolean a(byte[] bArr, int i) {
        return a(bArr, i, 2);
    }

    public final boolean b() {
        return a(new byte[0], 0, 4);
    }

    public final boolean c() {
        return this.a.getQisrRecogStatus() == 0;
    }

    public final boolean d() {
        return this.d;
    }

    public final String e() {
        String str = "";
        if (this.e != null) {
            str = this.e;
            Log.d("wacai", "JSON Result:" + str);
        }
        try {
            JSONObject jSONObject = new JSONObject(new JSONTokener(str));
            this.d = jSONObject.getBoolean("ls");
            JSONArray jSONArray = jSONObject.getJSONArray("ws");
            int length = jSONArray.length();
            String str2 = "";
            for (int i = 0; i < length; i++) {
                JSONArray jSONArray2 = jSONArray.getJSONObject(i).getJSONArray("cw");
                if (jSONArray2.length() > 0) {
                    str2 = str2 + jSONArray2.getJSONObject(0).getString("w");
                }
            }
            return str2;
        } catch (JSONException e2) {
            return null;
        }
    }

    public final q f() {
        if (this.d) {
            return q.resultOver;
        }
        byte[] QISRGetResult = MSC.QISRGetResult(this.b, this.a);
        Log.d("test", "QISRGetResult complete, error code is :" + this.a.getQisrErrCode());
        if (QISRGetResult != null) {
            try {
                this.e = new String(QISRGetResult, "UTF-8");
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
        } else {
            this.e = null;
        }
        if (this.a.getQisrErrCode() == 0) {
            switch (this.a.getQisrRsltStatus()) {
                case 0:
                    return this.e == null ? q.noResult : q.hasResult;
                case 2:
                    return q.noResult;
                case 5:
                    return q.resultOver;
            }
        }
        return q.error;
    }
}
