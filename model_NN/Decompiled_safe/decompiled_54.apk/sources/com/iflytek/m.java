package com.iflytek;

import android.database.Cursor;
import android.net.Uri;
import com.wacai.a.e;
import com.wacai.a.i;
import com.wacai365.MyApp;

public final class m {
    private static final Uri b = Uri.parse("content://telephony/carriers/preferapn");
    private i a = e.c();

    private o b() {
        Cursor cursor;
        try {
            Cursor query = MyApp.b.getContentResolver().query(b, null, null, null, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        int columnIndex = query.getColumnIndex("_id");
                        int columnIndex2 = query.getColumnIndex("name");
                        int columnIndex3 = query.getColumnIndex("apn");
                        int columnIndex4 = query.getColumnIndex("proxy");
                        int columnIndex5 = query.getColumnIndex("port");
                        int columnIndex6 = query.getColumnIndex("user");
                        int columnIndex7 = query.getColumnIndex("password");
                        o oVar = new o(this);
                        if (columnIndex != -1) {
                            oVar.a(query.getShort(columnIndex));
                        }
                        if (columnIndex2 != -1) {
                            oVar.a(query.getString(columnIndex2));
                        }
                        if (columnIndex3 != -1) {
                            oVar.b(query.getString(columnIndex3));
                        }
                        if (columnIndex4 != -1) {
                            oVar.c(query.getString(columnIndex4));
                        }
                        if (columnIndex5 != -1) {
                            oVar.d(query.getString(columnIndex5));
                        }
                        if (columnIndex6 != -1) {
                            oVar.e(query.getString(columnIndex6));
                        }
                        if (columnIndex7 != -1) {
                            oVar.f(query.getString(columnIndex7));
                        }
                        if (query != null) {
                            query.close();
                        }
                        return oVar;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = query;
                    th = th2;
                }
            }
            if (query != null) {
                query.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final d a() {
        try {
            o b2 = b();
            if (b2 != null) {
                String a2 = b2.a();
                String b3 = b2.b();
                switch (g.a[this.a.ordinal()]) {
                    case 1:
                        return a2.startsWith("cmwap") ? d.cmwap : d.cmnet;
                    case 2:
                        return (a2.startsWith("uniwap") || a2.startsWith("3gwap") || a2.startsWith("cmwap")) ? d.uniwap : d.uninet;
                    case 3:
                        return (a2.startsWith("ctwap") || (b3 != null && b3.startsWith("ctwap"))) ? d.ctwap : d.ctnet;
                }
            }
        } catch (Exception e) {
            System.out.print("");
        }
        return d.wifi;
    }
}
