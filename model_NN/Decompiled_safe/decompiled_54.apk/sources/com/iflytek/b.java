package com.iflytek;

import java.util.concurrent.ConcurrentLinkedQueue;

public final class b implements i {
    /* access modifiers changed from: private */
    public static h f = null;
    c a = null;
    k b = null;
    /* access modifiers changed from: package-private */
    public r c = r.uninit;
    l d = new l();
    private t e = null;
    /* access modifiers changed from: private */
    public ConcurrentLinkedQueue g = new ConcurrentLinkedQueue();
    private Object h = new Object();

    public static void a(h hVar) {
        f = hVar;
    }

    /* access modifiers changed from: private */
    public boolean a(int i) {
        if (this.a != null) {
            return true;
        }
        synchronized (this.h) {
            try {
                this.a = new c(1, 16, i, 150);
                this.a.a(this);
                if (this.g == null) {
                    this.g = new ConcurrentLinkedQueue();
                }
                this.g.clear();
            } catch (Exception e2) {
                e2.printStackTrace();
                this.a = null;
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void f() {
        synchronized (this.h) {
            if (this.a != null) {
                this.a.b();
                this.a = null;
            }
        }
    }

    public final void a(byte[] bArr) {
        if ((r.beginRec == this.c || r.recording == this.c) && bArr.length > 0) {
            int length = bArr.length;
            byte[] bArr2 = new byte[length];
            System.arraycopy(bArr, 0, bArr2, 0, length);
            this.g.add(bArr2);
            a aVar = new a(this);
            aVar.a = j.UploadData;
            aVar.b = null;
            aVar.c = Integer.valueOf(length);
            this.e.a(aVar);
            this.b.a(bArr);
        }
    }

    public final synchronized boolean a() {
        boolean z;
        if (r.uninit != this.c) {
            z = true;
        } else if (f == null) {
            z = false;
        } else {
            try {
                l.a(f);
            } catch (Exception e2) {
            }
            this.e = new t(this);
            this.e.start();
            this.e.setName("MessageProcessThread");
            this.c = r.ready;
            z = true;
        }
        return z;
    }

    public final synchronized boolean a(String str, k kVar) {
        boolean z;
        this.b = kVar;
        if (this.e == null) {
            this.b.a(1);
            z = true;
        } else {
            if (this.c != r.ready) {
                p.b(200);
                if (this.c != r.ready) {
                    this.b.a(1);
                    z = true;
                }
            }
            this.c = r.beginRec;
            this.e.b();
            a aVar = new a(this);
            aVar.a = j.BeginAsr;
            aVar.b = str;
            this.e.a(aVar);
            z = true;
        }
        return z;
    }

    public final synchronized void b() {
        if (!(this.e == null || r.uninit == this.c)) {
            if (this.g != null) {
                this.g.clear();
            }
            this.e.a(0);
            this.e.b();
            this.e.a(50);
            this.e = null;
            f();
            l.a();
            this.c = r.uninit;
        }
    }

    public final synchronized boolean c() {
        boolean z;
        if (this.c == r.beginRec || this.c == r.recording) {
            f();
            this.c = r.waitresult;
            a aVar = new a(this);
            aVar.a = j.StopAsr;
            this.e.a(aVar);
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public final synchronized boolean d() {
        this.e.b();
        this.c = r.abortRec;
        a aVar = new a(this);
        aVar.a = j.AbortAsr;
        this.e.a(aVar);
        return false;
    }
}
