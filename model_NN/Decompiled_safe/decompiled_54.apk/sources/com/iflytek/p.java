package com.iflytek;

import android.util.Log;

public abstract class p extends Thread {
    protected boolean a = false;

    public static final void b(int i) {
        int i2 = (i / 20) + 1;
        for (int i3 = 0; i3 < i2; i3++) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public final void a(int i) {
        if (this.a) {
            this.a = false;
            if (i != 0) {
                try {
                    super.join((long) i);
                } catch (InterruptedException e) {
                    Log.e(getClass().getSimpleName(), "stop", e);
                }
            }
        }
    }

    public void run() {
        a();
        Log.d(getClass().getSimpleName(), "thread " + getName() + " Exit with code:" + 0 + "\n");
    }

    public final void start() {
        this.a = true;
        super.start();
    }
}
