package com.iflytek;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.wacai365.MyApp;

public final class n {
    private ConnectivityManager a = ((ConnectivityManager) MyApp.b.getSystemService("connectivity"));
    private NetworkInfo b = this.a.getActiveNetworkInfo();

    public final int a() {
        NetworkInfo[] allNetworkInfo = this.a.getAllNetworkInfo();
        if (allNetworkInfo == null || allNetworkInfo.length <= 0) {
            return -1;
        }
        int i = -1;
        for (NetworkInfo networkInfo : allNetworkInfo) {
            if (networkInfo.isConnected()) {
                if (i == 1) {
                    return i;
                }
                i = networkInfo.getType();
            }
        }
        return i;
    }
}
