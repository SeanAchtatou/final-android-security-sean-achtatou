package com.iflytek;

import com.wacai365.C0000R;
import java.util.HashMap;

public final class f {
    private static f c = null;
    private b a = null;
    private HashMap b = new HashMap(3);

    private f() {
        h hVar = new h();
        hVar.a("4dc8df5c");
        b.a(hVar);
        this.a = new b();
        this.b.put(1, Integer.valueOf((int) C0000R.string.voiceErrorUnknow));
        this.b.put(2, Integer.valueOf((int) C0000R.string.voiceErrorNetworkBusy));
        this.b.put(3, Integer.valueOf((int) C0000R.string.voiceErrorrecognizeFailed));
        this.b.put(4, Integer.valueOf((int) C0000R.string.voiceErrorMicInitFailed));
        this.b.put(5, Integer.valueOf((int) C0000R.string.voiceErrorNetworkTimeout));
    }

    public static synchronized f a() {
        f fVar;
        synchronized (f.class) {
            if (c == null) {
                c = new f();
            }
            fVar = c;
        }
        return fVar;
    }

    public final boolean a(String str, k kVar) {
        if (kVar == null) {
            return false;
        }
        if (str != null && !str.equals("poi")) {
            str.equals("vsearch");
        }
        return this.a.a(str, kVar);
    }

    public final boolean b() {
        if (this.a == null) {
            return false;
        }
        return this.a.a();
    }

    public final void c() {
        if (this.a != null) {
            this.a.d();
        }
    }

    public final void d() {
        if (this.a != null) {
            this.a.c();
        }
    }

    public final void e() {
        if (this.a != null) {
            this.a.b();
        }
        if (this.b != null) {
            this.b.clear();
            this.b = null;
        }
    }
}
