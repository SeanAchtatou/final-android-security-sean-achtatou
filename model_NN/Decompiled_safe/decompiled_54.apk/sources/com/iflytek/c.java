package com.iflytek;

import android.media.AudioRecord;
import android.os.Environment;
import android.util.Log;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public final class c {
    private byte[] a;
    /* access modifiers changed from: private */
    public AudioRecord b;
    private i c;
    private int d;
    private boolean e;
    private File f;
    private BufferedOutputStream g;
    private AudioRecord.OnRecordPositionUpdateListener h;

    public c() {
        this(1, 16, 16000, 20);
    }

    public c(short s, short s2, int i, int i2) {
        int i3;
        int i4 = 2;
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = 0;
        this.g = null;
        this.h = new e(this);
        this.d = (i * i2) / 1000;
        int i5 = (((this.d * 10) * s2) * s) / 8;
        int i6 = s == 1 ? 2 : 3;
        i4 = s2 != 16 ? 3 : i4;
        int minBufferSize = AudioRecord.getMinBufferSize(i, i6, i4);
        if (i5 < minBufferSize) {
            Log.w("PCM recorder", "Increasing buffer size to " + Integer.toString(minBufferSize));
            i3 = minBufferSize;
        } else {
            i3 = i5;
        }
        this.b = new AudioRecord(1, i, i6, i4, i3);
        if (this.b.getState() != 1) {
            throw new Exception("create AudioRecord error");
        }
        this.b.setRecordPositionUpdateListener(this.h);
        this.b.setPositionNotificationPeriod(this.d);
        this.a = new byte[(((this.d * s) * s2) / 8)];
        if (this.e) {
            this.f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/msc/test.pcm");
            if (this.f.exists()) {
                this.f.delete();
            }
            try {
                this.f.createNewFile();
            } catch (IOException e2) {
                throw new IllegalStateException("Failed to create " + this.f.toString());
            }
        }
    }

    /* access modifiers changed from: private */
    public int c() {
        int read = this.b.read(this.a, 0, this.a.length);
        if (read > 0 && this.c != null) {
            this.c.a(this.a);
            if (this.e) {
                try {
                    this.g.write(this.a);
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
        return read;
    }

    public final void a() {
        if (this.e) {
            try {
                this.g = new BufferedOutputStream(new FileOutputStream(this.f));
            } catch (FileNotFoundException e2) {
            }
        }
        this.b.startRecording();
        int i = 0;
        while (i < this.d * 2) {
            int c2 = c();
            if (c2 > 0) {
                i += c2;
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e3) {
                e3.printStackTrace();
            }
        }
    }

    public final void a(i iVar) {
        this.c = iVar;
    }

    public final void b() {
        this.b.release();
    }
}
