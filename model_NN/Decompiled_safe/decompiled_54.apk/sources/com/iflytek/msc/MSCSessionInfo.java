package com.iflytek.msc;

public class MSCSessionInfo {
    private int qhcrErrCode;
    private int qhcrRsltStatus;
    private int qisrEpStatus;
    private int qisrErrCode;
    private char[] qisrParamValue;
    private int qisrRecogStatus;
    private int qisrRsltStatus;
    private int qisrValueLen;
    private byte[] qttsAudioData;
    private int qttsAudioLen;
    private int qttsErrCode;
    private char[] qttsParamValue;
    private int qttsSynthStatus;

    public MSCSessionInfo() {
        setQttsAudioData(null);
        setQttsAudioLen(-1);
        setQttsParamValue(null);
        setQttsSynthStatus(-1);
        setQisrRecogStatus(2);
    }

    public int getQhcrErrCode() {
        return this.qhcrErrCode;
    }

    public int getQhcrRsltStatus() {
        return this.qhcrRsltStatus;
    }

    public int getQisrEpStatus() {
        return this.qisrEpStatus;
    }

    public int getQisrErrCode() {
        return this.qisrErrCode;
    }

    public char[] getQisrParamValue() {
        return this.qisrParamValue;
    }

    public int getQisrRecogStatus() {
        return this.qisrRecogStatus;
    }

    public int getQisrRsltStatus() {
        return this.qisrRsltStatus;
    }

    public int getQisrValueLen() {
        return this.qisrValueLen;
    }

    public byte[] getQttsAudioData() {
        return this.qttsAudioData;
    }

    public int getQttsAudioLen() {
        return this.qttsAudioLen;
    }

    public int getQttsErrCode() {
        return this.qttsErrCode;
    }

    public char[] getQttsParamValue() {
        return this.qttsParamValue;
    }

    public int getQttsSynthStatus() {
        return this.qttsSynthStatus;
    }

    public void setQisrEpStatus(int i) {
        this.qisrEpStatus = i;
    }

    public void setQisrParamValue(char[] cArr) {
        this.qisrParamValue = cArr;
    }

    public void setQisrRecogStatus(int i) {
        this.qisrRecogStatus = i;
    }

    public void setQttsAudioData(byte[] bArr) {
        this.qttsAudioData = bArr;
    }

    public void setQttsAudioLen(int i) {
        this.qttsAudioLen = i;
    }

    public void setQttsParamValue(char[] cArr) {
        this.qttsParamValue = cArr;
    }

    public void setQttsSynthStatus(int i) {
        this.qttsSynthStatus = i;
    }
}
