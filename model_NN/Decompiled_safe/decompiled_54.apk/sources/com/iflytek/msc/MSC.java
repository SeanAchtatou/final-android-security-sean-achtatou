package com.iflytek.msc;

public class MSC {
    static {
        System.loadLibrary("msc");
    }

    public static native int QISRAudioWrite(char[] cArr, byte[] bArr, int i, int i2, MSCSessionInfo mSCSessionInfo);

    public static native int QISRFini();

    public static final native byte[] QISRGetResult(char[] cArr, MSCSessionInfo mSCSessionInfo);

    public static native int QISRInit(byte[] bArr);

    public static final native char[] QISRSessionBegin(byte[] bArr, byte[] bArr2, MSCSessionInfo mSCSessionInfo);

    public static native int QISRSessionEnd(char[] cArr, byte[] bArr);
}
