package com.wacai365;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

final class qh implements AdapterView.OnItemClickListener {
    private /* synthetic */ SettingLoanAccountMgr a;

    qh(SettingLoanAccountMgr settingLoanAccountMgr) {
        this.a = settingLoanAccountMgr;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.a, LoanList.class);
        intent.putExtra("LOANACCOUNTID", j);
        this.a.startActivityForResult(intent, 0);
    }
}
