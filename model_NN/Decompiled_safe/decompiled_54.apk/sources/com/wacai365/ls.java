package com.wacai365;

import android.content.DialogInterface;

final class ls implements DialogInterface.OnClickListener {
    private /* synthetic */ wz a;

    ls(wz wzVar) {
        this.a = wzVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.c.h = "";
        this.a.a.c.i = "";
        if (!(this.a.a.s == null || this.a.a.s.b == null || this.a.a.s.b.length <= 0)) {
            int length = this.a.a.s.b.length;
            for (int i2 = 0; i2 < length; i2++) {
                if (this.a.a.s.b[i2]) {
                    if (this.a.a.c.h.length() == 0) {
                        StringBuilder sb = new StringBuilder();
                        QueryInfo queryInfo = this.a.a.c;
                        queryInfo.h = sb.append(queryInfo.h).append(String.format("%d", Integer.valueOf(this.a.a.s.a[i2]))).toString();
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        QueryInfo queryInfo2 = this.a.a.c;
                        queryInfo2.h = sb2.append(queryInfo2.h).append(",").append(String.format("%d", Integer.valueOf(this.a.a.s.a[i2]))).toString();
                    }
                }
            }
        }
        this.a.a.c.i = m.b(this.a.a.c.h, "name", "TBL_MEMBERINFO");
        this.a.a.y.setText(this.a.a.c.h.length() <= 0 ? this.a.a.getResources().getText(C0000R.string.txtFullString).toString() : this.a.a.c.i);
        dialogInterface.dismiss();
    }
}
