package com.wacai365;

import android.view.View;
import android.view.animation.Animation;
import android.widget.AdapterView;

final class hb implements AdapterView.OnItemClickListener {
    final /* synthetic */ qf a;
    final /* synthetic */ MyShortcuts b;

    hb(MyShortcuts myShortcuts, qf qfVar) {
        this.b = myShortcuts;
        this.a = qfVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        long unused = this.b.a = j;
        new nl(this.b, new oh(this, i), 0.0d, false).show();
        m.a(this.b, (Animation) null, 0, (View) null, (int) C0000R.string.inputMoneyHint);
    }
}
