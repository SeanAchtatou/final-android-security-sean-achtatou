package com.wacai365;

import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.AdapterView;
import java.util.Hashtable;

final class hm implements AdapterView.OnItemClickListener {
    private /* synthetic */ StatByBalance a;

    hm(StatByBalance statByBalance) {
        this.a = statByBalance;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.a.i.booleanValue()) {
            m.a(this.a, (Animation) null, 0, (View) null, (int) C0000R.string.txtRemoteListPrompt);
            return;
        }
        Hashtable hashtable = (Hashtable) this.a.j.get(i);
        if (hashtable != null) {
            long parseLong = Long.parseLong((String) hashtable.get("itemid"));
            if (parseLong >= 0) {
                Intent intent = new Intent(this.a, InputAccount.class);
                intent.putExtra("Record_Id", parseLong);
                this.a.startActivityForResult(intent, 14);
            }
        }
    }
}
