package com.wacai365;

import android.content.Context;
import com.wacai.a.h;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public abstract class abw {
    /* access modifiers changed from: private */
    public static final String a = (m.i + "/data.db");

    private static File a(String str, InputStream inputStream) {
        FileOutputStream fileOutputStream;
        Throwable th;
        try {
            File file = new File(str);
            file.createNewFile();
            FileOutputStream fileOutputStream2 = new FileOutputStream(file);
            try {
                byte[] bArr = new byte[4096];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    h.a(bArr, 0, read, h.a());
                    fileOutputStream2.write(bArr, 0, read);
                }
                fileOutputStream2.flush();
                try {
                    fileOutputStream2.close();
                } catch (Exception e) {
                }
                return file;
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream = fileOutputStream2;
                try {
                    fileOutputStream.close();
                } catch (Exception e2) {
                }
                throw th;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            fileOutputStream = null;
            th = th4;
            fileOutputStream.close();
            throw th;
        }
    }

    public static void a(Context context) {
        if (context == null) {
            throw new Exception("illegal parameter!");
        } else if (m.d()) {
            File file = new File(m.i);
            if (!(!file.exists() ? file.mkdirs() : true)) {
                return;
            }
            if (c(a)) {
                m.a(context, (int) C0000R.string.txtAlertTitleInfo, (int) C0000R.string.txtBackupAlertMessage, (int) C0000R.string.txtOK, (int) C0000R.string.txtCancel, new mh(context));
            } else {
                e(context);
            }
        }
    }

    public static boolean a(String str) {
        if (str == null || str.length() <= 0) {
            return false;
        }
        return new File(str).delete();
    }

    public static void b(Context context) {
        if (context == null) {
            throw new Exception("illegal parameter!");
        }
        m.a(context, (int) C0000R.string.txtAlertTitleInfo, (int) C0000R.string.txtSyncAlertMessage, (int) C0000R.string.txtOK, (int) C0000R.string.txtCancel, new mi(context));
    }

    /* access modifiers changed from: private */
    public static boolean c(String str) {
        if (str == null || str.length() <= 0) {
            return false;
        }
        return new File(str).exists();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void
     arg types: [android.content.Context, java.lang.String, int, ?[OBJECT, ARRAY]]
     candidates:
      com.wacai365.m.a(android.view.animation.Animation, int, android.view.View, java.lang.String):android.view.animation.Animation
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, java.util.ArrayList, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, int[], android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.app.ProgressDialog, boolean, boolean, android.content.DialogInterface$OnClickListener):com.wacai365.tp
      com.wacai365.m.a(android.content.Context, long, android.content.DialogInterface$OnClickListener, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, long, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, boolean, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0052 A[SYNTHETIC, Splitter:B:13:0x0052] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005c A[SYNTHETIC, Splitter:B:19:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void d(android.content.Context r4) {
        /*
            r2 = 0
            com.wacai.e.c()     // Catch:{ Exception -> 0x002d, all -> 0x0058 }
            com.wacai.e.e()     // Catch:{ Exception -> 0x002d, all -> 0x0058 }
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x002d, all -> 0x0058 }
            java.lang.String r1 = com.wacai365.abw.a     // Catch:{ Exception -> 0x002d, all -> 0x0058 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x002d, all -> 0x0058 }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x002d, all -> 0x0058 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x002d, all -> 0x0058 }
            java.lang.String r0 = "/data/data/com.wacai365/wacai365.so"
            a(r0)     // Catch:{ Exception -> 0x0066 }
            java.lang.String r0 = "/data/data/com.wacai365/wacai365.so"
            a(r0, r1)     // Catch:{ Exception -> 0x0066 }
            r0 = 2131296783(0x7f09020f, float:1.8211492E38)
            java.lang.String r0 = r4.getString(r0)     // Catch:{ Exception -> 0x0066 }
            r2 = 1
            r3 = 0
            com.wacai365.m.a(r4, r0, r2, r3)     // Catch:{ Exception -> 0x0066 }
            r1.close()     // Catch:{ Exception -> 0x0060 }
        L_0x002c:
            return
        L_0x002d:
            r0 = move-exception
            r1 = r2
        L_0x002f:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0064 }
            r2.<init>()     // Catch:{ all -> 0x0064 }
            r3 = 2131296263(0x7f090007, float:1.8210438E38)
            java.lang.String r3 = r4.getString(r3)     // Catch:{ all -> 0x0064 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0064 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0064 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x0064 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0064 }
            r2 = 0
            r3 = 0
            com.wacai365.m.a(r4, r0, r2, r3)     // Catch:{ all -> 0x0064 }
            if (r1 == 0) goto L_0x002c
            r1.close()     // Catch:{ Exception -> 0x0056 }
            goto L_0x002c
        L_0x0056:
            r0 = move-exception
            goto L_0x002c
        L_0x0058:
            r0 = move-exception
            r1 = r2
        L_0x005a:
            if (r1 == 0) goto L_0x005f
            r1.close()     // Catch:{ Exception -> 0x0062 }
        L_0x005f:
            throw r0
        L_0x0060:
            r0 = move-exception
            goto L_0x002c
        L_0x0062:
            r1 = move-exception
            goto L_0x005f
        L_0x0064:
            r0 = move-exception
            goto L_0x005a
        L_0x0066:
            r0 = move-exception
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.abw.d(android.content.Context):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void
     arg types: [android.content.Context, java.lang.String, int, ?[OBJECT, ARRAY]]
     candidates:
      com.wacai365.m.a(android.view.animation.Animation, int, android.view.View, java.lang.String):android.view.animation.Animation
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, java.util.ArrayList, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, int[], android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.app.ProgressDialog, boolean, boolean, android.content.DialogInterface$OnClickListener):com.wacai365.tp
      com.wacai365.m.a(android.content.Context, long, android.content.DialogInterface$OnClickListener, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, long, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, boolean, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void */
    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0058 A[SYNTHETIC, Splitter:B:12:0x0058] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0062 A[SYNTHETIC, Splitter:B:18:0x0062] */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void e(android.content.Context r5) {
        /*
            r2 = 0
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0033, all -> 0x005e }
            java.lang.String r1 = "/data/data/com.wacai365/wacai365.so"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0033, all -> 0x005e }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0033, all -> 0x005e }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0033, all -> 0x005e }
            java.lang.String r0 = com.wacai365.abw.a     // Catch:{ Exception -> 0x006c }
            a(r0)     // Catch:{ Exception -> 0x006c }
            java.lang.String r0 = com.wacai365.abw.a     // Catch:{ Exception -> 0x006c }
            a(r0, r1)     // Catch:{ Exception -> 0x006c }
            r0 = 2131296782(0x7f09020e, float:1.821149E38)
            java.lang.String r0 = r5.getString(r0)     // Catch:{ Exception -> 0x006c }
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x006c }
            r3 = 0
            java.lang.String r4 = com.wacai365.abw.a     // Catch:{ Exception -> 0x006c }
            r2[r3] = r4     // Catch:{ Exception -> 0x006c }
            java.lang.String r0 = java.lang.String.format(r0, r2)     // Catch:{ Exception -> 0x006c }
            r2 = 1
            r3 = 0
            com.wacai365.m.a(r5, r0, r2, r3)     // Catch:{ Exception -> 0x006c }
            r1.close()     // Catch:{ Exception -> 0x0066 }
        L_0x0032:
            return
        L_0x0033:
            r0 = move-exception
            r1 = r2
        L_0x0035:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x006a }
            r2.<init>()     // Catch:{ all -> 0x006a }
            r3 = 2131296263(0x7f090007, float:1.8210438E38)
            java.lang.String r3 = r5.getString(r3)     // Catch:{ all -> 0x006a }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x006a }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x006a }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x006a }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x006a }
            r2 = 0
            r3 = 0
            com.wacai365.m.a(r5, r0, r2, r3)     // Catch:{ all -> 0x006a }
            if (r1 == 0) goto L_0x0032
            r1.close()     // Catch:{ Exception -> 0x005c }
            goto L_0x0032
        L_0x005c:
            r0 = move-exception
            goto L_0x0032
        L_0x005e:
            r0 = move-exception
            r1 = r2
        L_0x0060:
            if (r1 == 0) goto L_0x0065
            r1.close()     // Catch:{ Exception -> 0x0068 }
        L_0x0065:
            throw r0
        L_0x0066:
            r0 = move-exception
            goto L_0x0032
        L_0x0068:
            r1 = move-exception
            goto L_0x0065
        L_0x006a:
            r0 = move-exception
            goto L_0x0060
        L_0x006c:
            r0 = move-exception
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.abw.e(android.content.Context):void");
    }
}
