package com.wacai365;

import android.view.ContextMenu;
import android.view.View;

final class qb implements View.OnCreateContextMenuListener {
    private /* synthetic */ MyScheduleTrade a;

    qb(MyScheduleTrade myScheduleTrade) {
        this.a = myScheduleTrade;
    }

    public final void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.clear();
        this.a.getMenuInflater().inflate(C0000R.menu.white_list_context, contextMenu);
    }
}
