package com.wacai365;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import com.wacai.a.e;
import com.wacai365.a.c;

public class WeiboHome extends WacaiActivity implements View.OnClickListener {
    ll a;
    c b;
    CheckBox c;
    /* access modifiers changed from: private */
    public ProgressDialog d;
    /* access modifiers changed from: private */
    public Handler e = new ss(this);
    private DialogInterface.OnClickListener f = new sv(this);

    private void a(int i) {
        if (!e.a()) {
            m.a(this, (Animation) null, -1, (View) null, (int) C0000R.string.txtNoNetworkPrompt);
            return;
        }
        Intent intent = new Intent(this, WeiboPublish.class);
        intent.putExtra("view-type", i);
        intent.putExtra("Extra-webo-type", this.a.b);
        startActivity(intent);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.modify_connection /*2131493320*/:
                Intent intent = new Intent(this, WeiboConnect.class);
                intent.putExtra("Extra-webo-type", this.a.b);
                startActivityForResult(intent, 0);
                return;
            case C0000R.id.disconnect /*2131493321*/:
                m.a(this, (int) C0000R.string.weiboDisconnectConfirm, this.f);
                return;
            case C0000R.id.commit_suggestion_via_microblog /*2131493322*/:
                a(101);
                return;
            case C0000R.id.share_to_friends /*2131493323*/:
                a(100);
                return;
            case C0000R.id.watch_wacai /*2131493324*/:
                boolean isChecked = this.c.isChecked();
                if (this.d == null) {
                    this.d = new ProgressDialog(this);
                    this.d.setProgressStyle(0);
                    this.d.setMessage(getResources().getString(C0000R.string.txtOperating));
                    this.d.setCancelable(false);
                }
                this.d.show();
                new Thread((ThreadGroup) null, new sw(this, isChecked)).start();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.weibo_home);
        long longExtra = getIntent().getLongExtra("Extra-webo-type", -1);
        this.a = aaj.a().a(longExtra);
        this.b = c.a(this.a);
        if (this.b == null) {
            finish();
        }
        setTitle(this.a.c);
        TextView textView = (TextView) findViewById(C0000R.id.weibo_title);
        switch ((int) longExtra) {
            case 1:
                textView.setCompoundDrawablesWithIntrinsicBounds((int) C0000R.drawable.sina_logo, 0, 0, 0);
                break;
            case 2:
                textView.setCompoundDrawablesWithIntrinsicBounds((int) C0000R.drawable.tencent_logo, 0, 0, 0);
                break;
        }
        findViewById(C0000R.id.modify_connection).setOnClickListener(this);
        findViewById(C0000R.id.disconnect).setOnClickListener(this);
        findViewById(C0000R.id.commit_suggestion_via_microblog).setOnClickListener(this);
        findViewById(C0000R.id.share_to_friends).setOnClickListener(this);
        this.c = (CheckBox) findViewById(C0000R.id.watch_wacai);
        this.c.setChecked(this.a.i);
        this.c.setOnClickListener(this);
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(new su(this));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.c != null && this.a != null) {
            this.c.setChecked(this.a.i);
        }
    }
}
