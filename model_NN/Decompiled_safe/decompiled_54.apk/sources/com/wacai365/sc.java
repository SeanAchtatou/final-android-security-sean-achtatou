package com.wacai365;

import android.content.DialogInterface;
import com.wacai.a.a;
import java.util.Date;

final class sc implements DialogInterface.OnClickListener {
    private /* synthetic */ xn a;

    sc(xn xnVar) {
        this.a = xnVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.f.setText(m.c(this.a.a, i));
        Date date = new Date();
        Date date2 = new Date();
        a.a(i, date, date2);
        this.a.a.c.b = a.a(date.getTime());
        this.a.a.c.c = a.a(date2.getTime());
        String format = m.c.format(date);
        String format2 = m.c.format(date2);
        this.a.a.u.setText(format);
        this.a.a.w.setText(format2);
        int unused = this.a.a.F = i;
        dialogInterface.dismiss();
    }
}
