package com.wacai365;

import android.view.View;
import android.widget.AdapterView;
import java.util.ArrayList;
import java.util.Map;

final class n implements AdapterView.OnItemClickListener {
    private /* synthetic */ ArrayList a;
    private /* synthetic */ ArrayList b;
    private /* synthetic */ ChooseAccountType c;

    n(ChooseAccountType chooseAccountType, ArrayList arrayList, ArrayList arrayList2) {
        this.c = chooseAccountType;
        this.a = arrayList;
        this.b = arrayList2;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        long longValue;
        if (i <= this.a.size()) {
            longValue = Long.valueOf((String) ((Map) this.a.get(i - 1)).get("ID")).longValue();
        } else {
            int i2 = 2;
            if (this.a.size() == 0) {
                i2 = 2 + 1;
            }
            longValue = Long.valueOf((String) ((Map) this.b.get((i - this.a.size()) - i2)).get("ID")).longValue();
        }
        this.c.a(longValue);
    }
}
