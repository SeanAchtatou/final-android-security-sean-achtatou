package com.wacai365;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

public final class hd extends Dialog implements DialogInterface {
    protected ListView a = null;
    protected boolean[] b = null;
    protected DialogInterface.OnClickListener c = null;

    public hd(Context context, String[] strArr, boolean[] zArr, DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener, DialogInterface.OnClickListener onClickListener) {
        super(context, C0000R.style.choose_dialog);
        this.b = zArr;
        this.c = onClickListener;
        View inflate = LayoutInflater.from(context).inflate((int) C0000R.layout.choose_item_list_withconfim, (ViewGroup) null);
        if (inflate != null) {
            ((Button) inflate.findViewById(C0000R.id.btnOK)).setOnClickListener(new ic(this));
            ((Button) inflate.findViewById(C0000R.id.btnCancel)).setOnClickListener(new iq(this));
            this.a = (ListView) inflate.findViewById(C0000R.id.itemList);
            if (this.a != null) {
                this.a.setAdapter((ListAdapter) new tl(this, context, C0000R.layout.list_item_1_small_withcheckbox, strArr));
                this.a.setOnItemClickListener(new ip(this, onMultiChoiceClickListener));
            }
            setContentView(inflate);
        }
    }
}
