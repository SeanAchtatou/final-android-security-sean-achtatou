package com.wacai365;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.ImageButton;

public class NumberPickerButton extends ImageButton {
    private NumberPicker a;

    public NumberPickerButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NumberPickerButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private void a() {
        if (C0000R.id.increment == getId()) {
            this.a.a();
        } else if (C0000R.id.decrement == getId()) {
            this.a.b();
        }
    }

    private void a(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 3 || motionEvent.getAction() == 1) {
            a();
        }
    }

    public final void a(NumberPicker numberPicker) {
        this.a = numberPicker;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i == 23 || i == 66) {
            a();
        }
        return super.onKeyUp(i, keyEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        a(motionEvent);
        return super.onTouchEvent(motionEvent);
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        a(motionEvent);
        return super.onTrackballEvent(motionEvent);
    }
}
