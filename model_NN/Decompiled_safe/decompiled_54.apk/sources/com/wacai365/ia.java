package com.wacai365;

import android.content.DialogInterface;
import android.widget.CheckBox;
import com.wacai.a;

final class ia implements DialogInterface.OnClickListener {
    private /* synthetic */ CheckBox a;
    private /* synthetic */ DialogInterface.OnClickListener b;

    ia(CheckBox checkBox, DialogInterface.OnClickListener onClickListener) {
        this.a = checkBox;
        this.b = onClickListener;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case -1:
                a.b("ShowNetworkWarning", this.a.isChecked() ? 0 : 1);
                break;
        }
        this.b.onClick(dialogInterface, i);
    }
}
