package com.wacai365;

import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

public class MoreOptionsTab extends WacaiActivity {
    private static fr[] b = {new fr(C0000R.string.txtMyNote, C0000R.string.txtMyNoteDetail, "com.wacai365.MyNote"), new fr(C0000R.string.txtMySMS, C0000R.string.txtMySMSDetail, "com.wacai365.MySMS"), new fr(C0000R.string.txtSoftIntroduce, C0000R.string.txtSoftIntroduceDetails, "com.wacai365.SoftIntroduce"), new fr(C0000R.string.txtSettingNews, C0000R.string.txtSettingNewsDetail, "com.wacai365.SettingNews"), new fr(C0000R.string.txtSendToFriends, C0000R.string.txtSendToFriendsDetail, ""), new fr(C0000R.string.txtSettingFeedback, C0000R.string.txtSettingFeedbackDetail, "com.wacai365.SettingFeedback"), new fr(C0000R.string.txtSettingAbout, C0000R.string.txtSettingAboutDetail, "com.wacai365.SettingAbout")};
    ListView a;
    private AdapterView.OnItemClickListener c = new zh(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.setting_item_list_withoutbutton);
        this.a = (ListView) findViewById(C0000R.id.IOList);
        this.a.setAdapter((ListAdapter) new xw(this, (int) C0000R.layout.list_item_line2, b));
        this.a.setOnItemClickListener(this.c);
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(new zd(this));
    }
}
