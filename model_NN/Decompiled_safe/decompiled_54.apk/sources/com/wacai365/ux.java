package com.wacai365;

import android.content.DialogInterface;
import com.wacai.a.a;
import java.util.Date;

final class ux implements DialogInterface.OnClickListener {
    private /* synthetic */ eg a;

    ux(eg egVar) {
        this.a = egVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.i.setText(this.a.a.getResources().getStringArray(C0000R.array.Times)[i]);
        Date date = new Date();
        Date date2 = new Date();
        a.a(i, date, date2);
        this.a.a.b.b = a.a(date.getTime());
        this.a.a.b.c = a.a(date2.getTime());
        String format = m.c.format(date);
        String format2 = m.c.format(date2);
        this.a.a.j.setText(format);
        this.a.a.k.setText(format2);
        int unused = this.a.a.B = i;
        dialogInterface.dismiss();
    }
}
