package com.wacai365;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

public class PersonalSettingMgr extends WacaiActivity {
    private ListView a;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.setting_item_list_withoutbutton);
        this.a = (ListView) findViewById(C0000R.id.IOList);
        this.a.setAdapter((ListAdapter) new ArrayAdapter(this, (int) C0000R.layout.list_item_withoutcheckable, (int) C0000R.id.listitem1, getResources().getTextArray(C0000R.array.PersonalSetting)));
        this.a.setOnItemClickListener(new ni(this));
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(new nj(this));
    }
}
