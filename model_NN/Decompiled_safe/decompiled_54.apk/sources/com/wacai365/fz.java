package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.view.View;
import android.widget.AdapterView;

final class fz implements AdapterView.OnItemClickListener {
    private /* synthetic */ SettingIncomeMainTypeMgr a;

    fz(SettingIncomeMainTypeMgr settingIncomeMainTypeMgr) {
        this.a = settingIncomeMainTypeMgr;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Cursor cursor = (Cursor) this.a.c.getItemAtPosition(i);
        long j2 = cursor != null ? cursor.getLong(cursor.getColumnIndexOrThrow("_id")) : -1;
        Intent intent = new Intent(this.a, InputIncomeMainType.class);
        intent.putExtra("Record_Id", j2);
        this.a.startActivityForResult(intent, 0);
    }
}
