package com.wacai365;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import java.util.Date;

public final class by extends SimpleCursorAdapter {
    private String[] a;
    private int[] b;

    public by(Context context, int i, Cursor cursor, String[] strArr, int[] iArr) {
        super(context, i, cursor, strArr, iArr);
        this.b = iArr;
        this.a = strArr;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        View findViewById = view.findViewById(this.b[0]);
        if (findViewById != null) {
            cursor.getLong(cursor.getColumnIndexOrThrow(this.a[1]));
            String string = cursor.getString(cursor.getColumnIndexOrThrow(this.a[3]));
            if (string != null) {
                setViewText((TextView) findViewById, String.format(context.getResources().getString(C0000R.string.txtTypePrefix), string));
            }
        }
        View findViewById2 = view.findViewById(this.b[1]);
        if (findViewById2 != null) {
            setViewText((TextView) findViewById2, new Date().getTime() / 1000 > cursor.getLong(cursor.getColumnIndexOrThrow(this.a[2])) ? context.getResources().getString(C0000R.string.txtExpiration) : "");
        }
        View findViewById3 = view.findViewById(this.b[2]);
        if (findViewById3 != null) {
            String string2 = cursor.getString(cursor.getColumnIndexOrThrow(this.a[0]));
            if (string2 == null) {
                string2 = "";
            }
            setViewText((TextView) findViewById3, string2);
        }
    }
}
