package com.wacai365;

import android.app.Activity;
import android.content.DialogInterface;

final class qw implements DialogInterface.OnClickListener {
    private /* synthetic */ String a;
    private /* synthetic */ Activity b;

    qw(String str, Activity activity) {
        this.a = str;
        this.b = activity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.AlertCenter.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.wacai365.AlertCenter.a(com.wacai365.AlertCenter, com.wacai365.fy):com.wacai365.fy
      com.wacai365.AlertCenter.a(com.wacai365.AlertCenter, int):void
      com.wacai365.AlertCenter.a(boolean, int):void
      com.wacai365.AlertCenter.a(java.lang.String, boolean):void */
    public final void onClick(DialogInterface dialogInterface, int i) {
        AlertCenter.a(this.a, true);
        this.b.finish();
    }
}
