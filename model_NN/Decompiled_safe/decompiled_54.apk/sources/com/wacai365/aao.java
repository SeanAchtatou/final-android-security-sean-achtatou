package com.wacai365;

import android.view.View;
import android.view.animation.Animation;
import com.wacai.a;

final class aao implements View.OnClickListener {
    private /* synthetic */ HomeScreenSetting a;

    aao(HomeScreenSetting homeScreenSetting) {
        this.a = homeScreenSetting;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.f)) {
            this.a.finish();
        } else if (!view.equals(this.a.e)) {
        } else {
            if (9 == this.a.i && this.a.k <= 0) {
                m.a(this.a, (Animation) null, 0, (View) null, (int) C0000R.string.txtStatChooseAccount);
            } else if (9 != this.a.j || this.a.l > 0) {
                a.b("HomeScreenTitleType1", (long) this.a.i);
                a.b("HomeScreenTitleType2", (long) this.a.j);
                a.b("HomeScreenAccountID1", (long) this.a.k);
                a.b("HomeScreenAccountID2", (long) this.a.l);
                this.a.finish();
            } else {
                m.a(this.a, (Animation) null, 0, (View) null, (int) C0000R.string.txtStatChooseAccount);
            }
        }
    }
}
