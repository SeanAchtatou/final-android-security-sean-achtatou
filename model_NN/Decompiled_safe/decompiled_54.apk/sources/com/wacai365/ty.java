package com.wacai365;

import android.view.View;

final class ty implements View.OnClickListener {
    private /* synthetic */ InputTradeTarget a;

    ty(InputTradeTarget inputTradeTarget) {
        this.a = inputTradeTarget;
    }

    public final void onClick(View view) {
        InputTradeTarget inputTradeTarget = this.a;
        switch (view.getId()) {
            case C0000R.id.btnCancel /*2131492870*/:
                m.a(this.a, this.a.a);
                inputTradeTarget.finish();
                return;
            case C0000R.id.btnOK /*2131492903*/:
                boolean unused = this.a.c = false;
                InputTradeTarget.d(this.a);
                return;
            case C0000R.id.btnContinue /*2131492906*/:
                if (this.a.e.x() > 0) {
                    m.a(inputTradeTarget, (int) C0000R.string.txtDeleteConfirm, this.a.g);
                    return;
                }
                boolean unused2 = this.a.c = true;
                InputTradeTarget.d(this.a);
                return;
            default:
                return;
        }
    }
}
