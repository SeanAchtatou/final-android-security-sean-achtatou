package com.wacai365;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import com.wacai.data.aa;

public class InputScheduleTrade extends WacaiActivity {
    /* access modifiers changed from: private */
    public RadioButton a;
    /* access modifiers changed from: private */
    public RadioButton b;
    /* access modifiers changed from: private */
    public Button c;
    /* access modifiers changed from: private */
    public Button d;
    /* access modifiers changed from: private */
    public Button e;
    private long f = -1;
    /* access modifiers changed from: private */
    public int g;
    private int h = 1;
    private ScrollView i = null;
    private LinearLayout j = null;
    private LinearLayout k = null;
    /* access modifiers changed from: private */
    public wg l = null;
    private wc m;
    private uu n;
    private CompoundButton.OnCheckedChangeListener o = new jf(this);
    private View.OnClickListener p = new je(this);

    static Intent a(Activity activity, String str, long j2) {
        if (j2 > 0) {
            return new Intent(activity, InputScheduleTrade.class);
        }
        Intent intent = new Intent(activity, InputScheduleTrade.class);
        if (str == null || str.length() <= 0) {
            return intent;
        }
        intent.putExtra("Extra_Money", str);
        return intent;
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        boolean z2;
        boolean z3;
        wg wgVar;
        ca caVar;
        boolean z4;
        if (this.g == 0) {
            if (!uu.class.isInstance(this.l)) {
                if (z) {
                    this.b.setChecked(true);
                }
                if (this.n == null) {
                    this.n = new uu(this.f);
                    z4 = true;
                } else {
                    z4 = false;
                }
                z3 = z4;
                wgVar = this.n;
            } else {
                return;
            }
        } else if (!wc.class.isInstance(this.l)) {
            if (z) {
                this.a.setChecked(true);
            }
            if (this.m == null) {
                this.m = new wc(this.f);
                z2 = true;
            } else {
                z2 = false;
            }
            z3 = z2;
            wgVar = this.m;
        } else {
            return;
        }
        if (wgVar != null) {
            if (this.l != null) {
                wgVar.a(this.l.b());
                caVar = this.l.e();
            } else {
                caVar = null;
            }
            this.l = wgVar;
            this.l.a(this, this.j, this.k, this, false);
            if (z3) {
                this.l.a(this.i, m.h(this, 50));
            }
            this.l.a(caVar);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (-1 == i3 && intent != null) {
            this.l.a(i2, i3, intent);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.wg.a(com.wacai365.y, boolean):void
     arg types: [com.wacai365.y, int]
     candidates:
      com.wacai365.wg.a(android.widget.ScrollView, int):void
      com.wacai365.wg.a(com.wacai365.y, boolean):void */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.h = getIntent().getIntExtra("LaunchedByApplication", 0);
        setContentView((int) C0000R.layout.input_schedule_trade);
        this.g = getIntent().getIntExtra("Extra_Type", 0);
        this.f = getIntent().getLongExtra("Extra_Id", -1);
        this.a = (RadioButton) findViewById(C0000R.id.btnIncome);
        this.b = (RadioButton) findViewById(C0000R.id.btnOutgo);
        this.j = (LinearLayout) findViewById(C0000R.id.baselayout);
        this.c = (Button) findViewById(C0000R.id.btnSave);
        this.d = (Button) findViewById(C0000R.id.btnCancel);
        this.e = (Button) findViewById(C0000R.id.btnDelete);
        this.i = (ScrollView) findViewById(C0000R.id.baseScrollLayout);
        this.k = (LinearLayout) findViewById(C0000R.id.id_popupframe);
        a(true);
        this.a.setOnCheckedChangeListener(this.o);
        this.b.setOnCheckedChangeListener(this.o);
        this.c.setOnClickListener(this.p);
        this.d.setOnClickListener(this.p);
        this.e.setOnClickListener(this.p);
        String stringExtra = getIntent().getStringExtra("Extra_Money");
        if (stringExtra != null && stringExtra.length() > 0) {
            this.l.b(aa.a(Double.parseDouble(stringExtra)));
        }
        if (this.f > 0) {
            setTitle((int) C0000R.string.txtEditScheduleIOTitle);
            this.a.setEnabled(false);
            this.b.setEnabled(false);
            return;
        }
        this.l.a(y.PROPERTY_LAUNCHMONEYFIRST, true);
        m.a(this.e, getResources());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        WidgetProvider.a(this);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (this.l != null && this.l.h()) {
            return true;
        }
        finish();
        return true;
    }
}
