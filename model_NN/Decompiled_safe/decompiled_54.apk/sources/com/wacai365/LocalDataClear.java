package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.a.a;
import com.wacai.b.c;
import com.wacai.b.o;
import com.wacai.b.z;
import java.util.Calendar;
import java.util.Date;

public class LocalDataClear extends WacaiActivity implements tt {
    /* access modifiers changed from: private */
    public LinearLayout a = null;
    /* access modifiers changed from: private */
    public TextView b = null;
    /* access modifiers changed from: private */
    public LinearLayout c = null;
    /* access modifiers changed from: private */
    public TextView d = null;
    /* access modifiers changed from: private */
    public Button e = null;
    /* access modifiers changed from: private */
    public Button f = null;
    private CheckBox g = null;
    private fy h = null;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public int k;
    /* access modifiers changed from: private */
    public int l;
    /* access modifiers changed from: private */
    public Date m;
    /* access modifiers changed from: private */
    public Date n;
    private View.OnClickListener o = new fv(this);
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener p = new fx(this);
    private DialogInterface.OnClickListener q = new fw(this);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(com.wacai.b.c, boolean):void
     arg types: [com.wacai.b.c, int]
     candidates:
      com.wacai365.ft.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(com.wacai.b.c, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void n(LocalDataClear localDataClear) {
        if ((((localDataClear.j - localDataClear.i) * 12) + localDataClear.l) - localDataClear.k >= 0) {
            ft ftVar = new ft(localDataClear);
            ftVar.a((tt) localDataClear);
            ftVar.a(localDataClear.q);
            localDataClear.h = ftVar;
            c cVar = new c(ftVar);
            if (localDataClear.g.isChecked()) {
                ftVar.b(C0000R.string.txtUploadProgress, C0000R.string.txtPreparing);
                ft.a(cVar, false);
            } else {
                ftVar.b(C0000R.string.txtCleardata, C0000R.string.txtClearing);
                ftVar.b(false);
                ftVar.c(false);
            }
            z zVar = new z();
            zVar.a(localDataClear.i);
            zVar.c(localDataClear.k);
            zVar.b(localDataClear.j);
            zVar.d(localDataClear.l);
            zVar.d(localDataClear.getResources().getString(C0000R.string.txtClearing));
            cVar.a((o) zVar);
            ftVar.a(cVar);
            ftVar.a(true, false);
        }
    }

    public final void a(int i2) {
        WidgetProvider.a(this);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && this.h != null) {
            this.h.a(i2, i3, intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.local_data_mana);
        this.m = new Date();
        this.n = new Date();
        a.a(5, this.m, this.n);
        Calendar instance = Calendar.getInstance();
        instance.setTime(this.m);
        this.i = instance.get(1);
        this.k = instance.get(2) + 1;
        instance.setTime(this.n);
        this.j = instance.get(1);
        this.l = instance.get(2) + 1;
        this.b = (TextView) findViewById(C0000R.id.tvStartDate);
        this.b.setText(m.g.format(this.m));
        this.d = (TextView) findViewById(C0000R.id.tvEndDate);
        this.d.setText(m.g.format(this.n));
        this.e = (Button) findViewById(C0000R.id.btnOK);
        this.e.setOnClickListener(this.o);
        this.f = (Button) findViewById(C0000R.id.btnCancel);
        this.f.setOnClickListener(this.o);
        this.a = (LinearLayout) findViewById(C0000R.id.layoutDateStart);
        this.a.setOnClickListener(this.o);
        this.c = (LinearLayout) findViewById(C0000R.id.layoutDateEnd);
        this.c.setOnClickListener(this.o);
        this.g = (CheckBox) findViewById(C0000R.id.cbUpload);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }
}
