package com.wacai365;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.b;
import com.wacai.data.aa;
import java.util.Hashtable;

final class bz extends BaseAdapter {
    private Context a;
    private int b;
    private LayoutInflater c = ((LayoutInflater) this.a.getSystemService("layout_inflater"));
    private String d;
    private /* synthetic */ StatByProperty e;

    public bz(StatByProperty statByProperty, Context context, int i) {
        this.e = statByProperty;
        this.a = context;
        this.b = i;
    }

    public final int getCount() {
        return this.e.e.size();
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        Hashtable hashtable = (Hashtable) this.e.e.get(i);
        if (hashtable == null) {
            return null;
        }
        LinearLayout linearLayout = view == null ? (LinearLayout) this.c.inflate(this.b, (ViewGroup) null) : (LinearLayout) view;
        ((TextView) linearLayout.findViewById(C0000R.id.listitem1)).setText((CharSequence) hashtable.get("TAG_LABLE"));
        TextView textView = (TextView) linearLayout.findViewById(C0000R.id.listitem2);
        if (b.a && this.d == null) {
            this.d = aa.a("TBL_MONEYTYPE", "flag", (int) b.m().j());
        }
        if (this.d != null) {
            textView.setText(this.d + ((String) hashtable.get("TAG_FIRST")));
        } else {
            textView.setText((CharSequence) hashtable.get("TAG_FIRST"));
        }
        return linearLayout;
    }
}
