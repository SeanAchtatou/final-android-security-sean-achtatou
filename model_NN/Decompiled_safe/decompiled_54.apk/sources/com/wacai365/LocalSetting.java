package com.wacai365;

import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.a;

public class LocalSetting extends WacaiActivity {
    private LinearLayout a;
    /* access modifiers changed from: private */
    public TextView b;
    /* access modifiers changed from: private */
    public int[] c;
    /* access modifiers changed from: private */
    public CheckBox d;
    private Button e = null;
    private Button f = null;
    /* access modifiers changed from: private */
    public int g = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.local_setting);
        this.g = (int) a.a("BasicSortStyle", 0);
        this.b = (TextView) findViewById(C0000R.id.sortMode);
        this.a = (LinearLayout) findViewById(C0000R.id.basicSortMode);
        this.a.setOnClickListener(new g(this));
        this.b.setText(m.e(this, this.g));
        this.d = (CheckBox) findViewById(C0000R.id.cbNetworkAccess);
        this.d.setChecked(a.a("ShowNetworkWarning", 1) > 0);
        this.e = (Button) findViewById(C0000R.id.btnSave);
        this.e.setOnClickListener(new h(this));
        this.f = (Button) findViewById(C0000R.id.btnCancel);
        this.f.setOnClickListener(new e(this));
    }
}
