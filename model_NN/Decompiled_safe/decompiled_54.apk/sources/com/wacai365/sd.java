package com.wacai365;

import android.util.Log;
import com.wacai.b.k;
import com.wacai.b.o;
import java.util.Vector;

public final class sd {
    private int a = -1;
    private o b = null;
    private Vector c = new Vector();
    private k d = new k();
    /* access modifiers changed from: private */
    public Thread e;
    private int f = 0;

    /* access modifiers changed from: private */
    public void f() {
        boolean z;
        while (this.f == 1 && this.e == Thread.currentThread()) {
            this.a++;
            if (this.f != 1 || this.a >= this.c.size()) {
                g();
                z = false;
            } else {
                this.b = (o) this.c.elementAt(this.a);
                z = true;
            }
            if (z) {
                Log.i("TaskManager", "Run next task: " + this.b.getClass().getName());
                this.b.m();
            } else {
                return;
            }
        }
    }

    private void g() {
        for (int i = 0; i < this.c.size(); i++) {
            o oVar = (o) this.c.elementAt(i);
            if (oVar != null) {
                oVar.g();
            }
        }
        int i2 = this.d.d;
        this.d = new k();
        if (i2 == 50 || i2 == 51) {
            for (int i3 = 0; i3 < this.c.size(); i3++) {
                o oVar2 = (o) this.c.elementAt(i3);
                if (oVar2 != null) {
                    oVar2.a(this.d);
                }
            }
            Log.i("TaskManager", "Reset task result status to enable restart if User/Password wrong.");
        } else {
            this.c.removeAllElements();
            Log.i("TaskManager", "Clean up. All tasks removed from task manager!");
        }
        this.a = -1;
    }

    public final void a(o oVar) {
        oVar.a(this.d);
        this.c.addElement(oVar);
    }

    public final boolean a() {
        this.f = 1;
        if (this.c.size() <= 0) {
            Log.i("TaskManager", "No task to run, finished!");
            g();
            return false;
        }
        this.e = Thread.currentThread();
        f();
        return true;
    }

    public final void b() {
        this.f = 1;
        if (this.c.size() <= 0) {
            Log.i("TaskManager", "No task to run, finished!");
            g();
            return;
        }
        new yt(this).start();
    }

    public final void c() {
        this.f = 2;
        if (this.b != null) {
            this.b.n();
            this.b = null;
        }
        g();
    }

    public final void d() {
        if (this.f == 1) {
            this.f = 3;
        }
    }

    public final void e() {
        if (this.f == 3) {
            this.f = 1;
            a();
        }
    }
}
