package com.wacai365;

import android.widget.CompoundButton;

final class zt implements CompoundButton.OnCheckedChangeListener {
    private /* synthetic */ InputShortcut a;

    zt(InputShortcut inputShortcut) {
        this.a = inputShortcut;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (z) {
            this.a.o.f(1);
        } else {
            this.a.o.f(0);
        }
    }
}
