package com.wacai365;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

public final class xw extends ArrayAdapter {
    private Context a = null;

    public xw(Context context, int i, List list) {
        super(context, i, list);
        this.a = context;
    }

    public xw(Context context, int i, fr[] frVarArr) {
        super(context, i, frVarArr);
        this.a = context;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? ((LayoutInflater) this.a.getSystemService("layout_inflater")).inflate((int) C0000R.layout.list_item_line2, (ViewGroup) null) : view;
        fr frVar = (fr) getItem(i);
        if (frVar != null) {
            ((TextView) inflate.findViewById(C0000R.id.listitem1)).setText(this.a.getResources().getText(frVar.a).toString());
            ((TextView) inflate.findViewById(C0000R.id.listitem2)).setText(this.a.getResources().getText(frVar.b).toString());
        }
        return inflate;
    }
}
