package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.text.InputFilter;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import com.wacai.b.c;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.n;
import com.wacai.b.o;
import com.wacai.b.r;
import com.wacai.b.t;
import com.wacai.data.k;
import com.wacai.e;

public abstract class InputBasicItem extends WacaiActivity implements tt {
    protected Button a = null;
    protected Button b = null;
    protected EditText c = null;
    protected boolean d = false;
    private Button e = null;
    private Animation f;
    /* access modifiers changed from: private */
    public k g;
    private fy h = null;
    private DialogInterface.OnClickListener i = new rw(this);
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener j = new sb(this);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void c(InputBasicItem inputBasicItem) {
        if (inputBasicItem.d()) {
            ft ftVar = new ft(inputBasicItem);
            ftVar.a((tt) inputBasicItem);
            inputBasicItem.h = ftVar;
            c cVar = new c(ftVar);
            if (inputBasicItem.g.B().length() > 0) {
                l lVar = new l();
                StringBuffer stringBuffer = new StringBuffer();
                inputBasicItem.g.a(stringBuffer);
                lVar.a(m.a(stringBuffer.toString()));
                cVar.a((o) lVar);
                cVar.a((o) new n());
            } else {
                l lVar2 = new l();
                lVar2.a(m.a(1));
                cVar.a((o) lVar2);
                cVar.a((o) new r());
                t tVar = new t();
                tVar.a(inputBasicItem.g);
                cVar.a((o) tVar);
                cVar.a((o) new l());
                cVar.a((o) new n());
            }
            ftVar.a(inputBasicItem.i);
            ftVar.a(cVar);
            ftVar.b(C0000R.string.networkProgress, C0000R.string.txtTransformData);
            ftVar.a(false, true);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x006c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(android.widget.TextView r10) {
        /*
            r9 = this;
            r4 = 2131296395(0x7f09008b, float:1.8210705E38)
            r7 = -1
            r6 = 2130968579(0x7f040003, float:1.7545816E38)
            r5 = 0
            if (r10 != 0) goto L_0x000c
            r1 = r5
        L_0x000b:
            return r1
        L_0x000c:
            java.lang.Class<android.widget.EditText> r1 = android.widget.EditText.class
            boolean r1 = r1.isInstance(r10)     // Catch:{ all -> 0x0067 }
            if (r1 == 0) goto L_0x0081
            r0 = r10
            android.widget.EditText r0 = (android.widget.EditText) r0     // Catch:{ all -> 0x0067 }
            r1 = r0
        L_0x0018:
            java.lang.CharSequence r2 = r10.getText()     // Catch:{ all -> 0x0075 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0075 }
            if (r2 == 0) goto L_0x002c
            java.lang.String r2 = r2.trim()     // Catch:{ all -> 0x0075 }
            int r3 = r2.length()     // Catch:{ all -> 0x0075 }
            if (r3 > 0) goto L_0x003b
        L_0x002c:
            java.lang.String r2 = ""
            r10.setText(r2)     // Catch:{ all -> 0x007b }
            android.view.animation.Animation r2 = r9.f
            android.view.animation.Animation r1 = com.wacai365.m.a(r9, r2, r6, r1, r4)
            r9.f = r1
            r1 = r5
            goto L_0x000b
        L_0x003b:
            r3 = 20
            int r4 = r2.length()     // Catch:{ all -> 0x0075 }
            if (r3 >= r4) goto L_0x0050
            android.view.animation.Animation r2 = r9.f
            r3 = 2131296396(0x7f09008c, float:1.8210707E38)
            android.view.animation.Animation r1 = com.wacai365.m.a(r9, r2, r6, r1, r3)
            r9.f = r1
            r1 = r5
            goto L_0x000b
        L_0x0050:
            com.wacai.data.k r3 = r9.g     // Catch:{ all -> 0x0075 }
            boolean r3 = r3.c(r2)     // Catch:{ all -> 0x0075 }
            if (r3 == 0) goto L_0x0065
            android.view.animation.Animation r2 = r9.f
            r3 = 2131296397(0x7f09008d, float:1.821071E38)
            android.view.animation.Animation r1 = com.wacai365.m.a(r9, r2, r6, r1, r3)
            r9.f = r1
            r1 = r5
            goto L_0x000b
        L_0x0065:
            r1 = r2
            goto L_0x000b
        L_0x0067:
            r1 = move-exception
            r2 = r5
            r3 = r7
        L_0x006a:
            if (r3 == r7) goto L_0x0074
            android.view.animation.Animation r4 = r9.f
            android.view.animation.Animation r2 = com.wacai365.m.a(r9, r4, r6, r2, r3)
            r9.f = r2
        L_0x0074:
            throw r1
        L_0x0075:
            r2 = move-exception
            r3 = r7
            r8 = r1
            r1 = r2
            r2 = r8
            goto L_0x006a
        L_0x007b:
            r2 = move-exception
            r3 = r4
            r8 = r1
            r1 = r2
            r2 = r8
            goto L_0x006a
        L_0x0081:
            r1 = r5
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.InputBasicItem.a(android.widget.TextView):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public final void a(int i2) {
        Cursor cursor;
        if (i2 != 10001) {
            this.g.c();
            return;
        }
        try {
            Cursor rawQuery = e.c().b().rawQuery(String.format("select id from %s where name = '%s'", this.g.w(), this.g.j()), null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        this.g.k(rawQuery.getLong(0));
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(k kVar) {
        this.g = kVar;
    }

    /* access modifiers changed from: protected */
    public abstract InputFilter[] b();

    /* access modifiers changed from: protected */
    public void c() {
        this.b = (Button) findViewById(C0000R.id.btnCancel);
        this.e = (Button) findViewById(C0000R.id.btnContinue);
        this.a = (Button) findViewById(C0000R.id.btnOK);
        this.b.setOnClickListener(new rv(this));
        this.e.setOnClickListener(new rz(this));
        this.a.setOnClickListener(new rx(this));
        this.c = (EditText) findViewById(C0000R.id.etName);
        if (this.c != null) {
            this.c.setText(this.g.j());
            this.c.setFilters(b());
            if (this.g.m()) {
                this.c.setEnabled(false);
                this.c.setFocusable(false);
            }
        }
        if (this.g.x() > 0) {
            this.e.setText((int) C0000R.string.txtMenuDelete);
        }
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        String a2 = a(this.c);
        if (a2 == null) {
            return false;
        }
        this.g.b(a2);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && this.h != null) {
            this.h.a(i2, i3, intent);
        }
    }
}
