package com.wacai365;

import android.os.Message;
import com.iflytek.k;

final class cs implements k {
    final /* synthetic */ VoiceInput a;

    cs(VoiceInput voiceInput) {
        this.a = voiceInput;
    }

    public final void a(int i) {
        Message obtain = Message.obtain();
        obtain.what = 30;
        obtain.arg1 = i;
        this.a.z.sendMessage(obtain);
    }

    public final void a(String str) {
        Message obtain = Message.obtain();
        obtain.what = 31;
        obtain.obj = str;
        this.a.z.sendMessage(obtain);
    }

    public final void a(byte[] bArr) {
        this.a.runOnUiThread(new wa(this, bArr));
    }

    public final void b(String str) {
        a(str);
        this.a.z.sendEmptyMessage(32);
    }
}
