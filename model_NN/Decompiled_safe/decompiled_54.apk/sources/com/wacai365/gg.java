package com.wacai365;

import android.app.Activity;
import android.view.View;
import android.view.animation.Animation;

final class gg implements View.OnClickListener {
    private /* synthetic */ InputAccount a;

    gg(InputAccount inputAccount) {
        this.a = inputAccount;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void
     arg types: [com.wacai365.InputAccount, java.lang.String, java.lang.String, int, int, int]
     candidates:
      com.wacai365.m.a(android.content.Context, int, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[]
      com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void */
    public final void onClick(View view) {
        if (this.a.C.m()) {
            m.a(this.a, (Animation) null, 0, (View) null, (int) C0000R.string.txtDisableNamePrompt);
        } else {
            m.a((Activity) this.a, this.a.C.j(), this.a.getResources().getString(C0000R.string.txtEditName), 20, 18, true);
        }
    }
}
