package com.wacai365;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.e;
import com.wacai.b.k;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.r;

public final class ft implements DialogInterface.OnClickListener, e, fy {
    private Activity a = null;
    private ProgressDialog b;
    private tp c;
    private c d;
    private boolean e = false;
    private boolean f = true;
    private boolean g = true;
    private boolean h = false;
    private boolean i = false;
    private boolean j = true;
    private int k;
    private int l;
    private tt m = null;
    private boolean n = true;
    private int o;
    private boolean p;
    private DialogInterface.OnClickListener q = null;
    private DialogInterface.OnClickListener r = new fa(this);

    public ft(Activity activity) {
        this.a = activity;
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.d != null && !this.d.a()) {
            if (!this.p) {
                this.p = b.m().p();
            }
            if (this.p || !this.g || m.a(this.a, this.e)) {
                this.b = new ProgressDialog(this.a);
                this.c = m.a(this.b, this.h, this.i, this);
                if (this.k != 0) {
                    this.b.setTitle(this.k);
                }
                this.b.setMessage(this.a.getResources().getText(this.l));
                this.b.show();
                this.d.b();
            }
        }
    }

    public static void a(c cVar, boolean z) {
        Cursor cursor;
        if (cVar != null) {
            is.a(cVar, true);
            is.a(cVar, false);
            is.a(cVar);
            if (z) {
                is.c(cVar, false);
                is.b(cVar, false);
            }
            try {
                Cursor rawQuery = com.wacai.e.c().b().rawQuery("select ymd / 100 as _ymd from (select ymd from TBL_OUTGOINFO where updatestatus = 0 AND scheduleoutgoid = 0 AND paybackid = 0 UNION ALL select ymd from TBL_INCOMEINFO where updatestatus = 0 AND scheduleincomeid = 0 AND paybackid = 0 UNION ALL select ymd from TBL_TRANSFERINFO where updatestatus = 0 AND type = 0 UNION ALL select ymd from TBL_MYNOTE where updatestatus = 0) group by _ymd", null);
                if (rawQuery != null) {
                    try {
                        if (rawQuery.moveToFirst()) {
                            do {
                                long j2 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_ymd"));
                                is.a(cVar, j2 / 100, j2 % 100);
                            } while (rawQuery.moveToNext());
                            if (rawQuery != null) {
                                rawQuery.close();
                                return;
                            }
                            return;
                        }
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        cursor = rawQuery;
                        th = th2;
                    }
                }
                if (rawQuery != null) {
                    rawQuery.close();
                    return;
                }
                return;
            } catch (Throwable th3) {
                th = th3;
                cursor = null;
            }
        } else {
            return;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.d != null) {
            this.d.c();
        }
        if (this.m != null && (this.m instanceof lb)) {
            ((lb) this.m).a();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final Boolean a(Activity activity) {
        String e2 = b.m().e();
        if (!this.p) {
            this.p = b.m().p();
        }
        if (this.p && (e2 == null || e2.length() == 0)) {
            Intent intent = new Intent(activity, AccountRegister.class);
            intent.putExtra("Extra_Anonymous", false);
            intent.putExtra("LaunchedByApplication", 1);
            activity.startActivityForResult(intent, 0);
            return false;
        } else if (!this.p || e2 == null || e2.length() <= 0) {
            return this.g && m.a(this.a, false, true);
        } else {
            return true;
        }
    }

    public final void a(int i2) {
        this.o = i2;
    }

    public final void a(int i2, int i3) {
        if (this.c != null) {
            this.c.b = i2;
            this.c.c = i3;
            this.a.runOnUiThread(this.c);
        }
    }

    public final void a(int i2, int i3, Intent intent) {
        if (i3 == -1 && i2 == 2) {
            this.p = intent.getBooleanExtra("Extra_Anonymous", false);
            if (this.e && this.p) {
                l lVar = new l();
                lVar.a(true);
                lVar.a(m.a(1));
                this.d.a(lVar, 0);
                r rVar = new r();
                this.d.a(rVar, 1);
                rVar.a(new ey(this));
            }
            a();
        }
    }

    public final void a(DialogInterface.OnClickListener onClickListener) {
        this.q = onClickListener;
    }

    public final void a(c cVar) {
        this.d = cVar;
    }

    public final void a(k kVar) {
        if (this.b != null && this.b.isShowing()) {
            this.b.cancel();
        }
        if (kVar.a && this.m != null) {
            this.m.a(kVar.d);
        } else if (!kVar.a && (this.m instanceof lb)) {
            ((lb) this.m).a(kVar);
        }
        if ((this.n || !kVar.a) && !(this.a instanceof AccountEmailConfig)) {
            String e2 = b.m().e();
            this.p = b.m().p();
            if (!this.e || !this.p || !this.f || !kVar.a || (e2 != null && e2.length() > 0)) {
                we weVar = new we(this.a, this.q);
                weVar.c = kVar;
                weVar.b = this.n;
                weVar.a = this.j;
                if (this.o != 0) {
                    weVar.d = this.o;
                }
                this.a.runOnUiThread(weVar);
            } else {
                this.a.startActivityForResult(new Intent(this.a, AccountEmailConfig.class), 0);
            }
        }
        if (kVar.a && kVar.e) {
            m.a(this.a, 0);
        }
    }

    public final void a(tt ttVar) {
        this.m = ttVar;
    }

    public final void a(String str) {
        if (str != null && this.c != null) {
            this.c.a = str;
            this.a.runOnUiThread(this.c);
        }
    }

    public final void a(boolean z) {
        this.e = z;
        if (this.e) {
            boolean p2 = b.m().p();
            String e2 = b.m().e();
            if (!p2 || e2 == null || e2.length() <= 0) {
                this.o = C0000R.string.backupSuccess;
            } else {
                this.o = C0000R.string.backupSuccessWithEmail;
            }
        }
    }

    public final void a(boolean z, boolean z2) {
        if (this.d != null) {
            this.h = z;
            this.i = z2;
            if (this.f) {
                m.b(this.a, (int) C0000R.string.txtNetworkWarning, this.r);
            } else {
                a();
            }
        }
    }

    public final void b(int i2, int i3) {
        this.k = i2;
        this.l = i3;
    }

    public final void b(boolean z) {
        this.f = z;
    }

    public final void c(boolean z) {
        this.g = z;
    }

    public final void d(boolean z) {
        this.j = z;
    }

    public final void e(boolean z) {
        this.n = z;
    }

    public final void onClick(DialogInterface dialogInterface, int i2) {
        if (dialogInterface.equals(this.b)) {
            this.b.dismiss();
            b();
        }
    }
}
