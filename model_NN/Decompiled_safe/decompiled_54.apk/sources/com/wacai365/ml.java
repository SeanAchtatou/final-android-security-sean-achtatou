package com.wacai365;

import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.wacai.data.aa;
import com.wacai.data.d;
import com.wacai.data.m;
import com.wacai.data.s;
import com.wacai.data.u;

public class ml extends oj {
    /* access modifiers changed from: private */
    public va A = new yw(this);
    private yn B = new yv(this);
    private View.OnClickListener C = new yu(this);
    /* access modifiers changed from: private */
    public LinearLayout a;
    /* access modifiers changed from: private */
    public TextView b;
    private TextView c;
    /* access modifiers changed from: private */
    public TextView j;
    private TextView k;
    /* access modifiers changed from: private */
    public TextView l;
    private TextView m;
    /* access modifiers changed from: private */
    public Button r;
    /* access modifiers changed from: private */
    public View s;
    /* access modifiers changed from: private */
    public View t;
    /* access modifiers changed from: private */
    public View u;
    /* access modifiers changed from: private */
    public View v;
    /* access modifiers changed from: private */
    public View w;
    private int x = 1;
    /* access modifiers changed from: private */
    public u y;
    /* access modifiers changed from: private */
    public va z = new yx(this);

    public ml(long j2) {
        if (j2 > 0) {
            this.y = u.f(j2);
        } else {
            this.y = new u();
        }
        this.d = C0000R.layout.input_transfer_tab;
    }

    private void k() {
        if (l()) {
            this.y.c(this.y.b());
            this.j.setText(aa.a(aa.l(this.y.d()), 2));
            this.v.setVisibility(8);
            return;
        }
        this.v.setVisibility(0);
    }

    static /* synthetic */ void l(ml mlVar) {
        if (mlVar.y != null) {
            mlVar.x = 2;
            m.a(mlVar.o);
        }
    }

    /* access modifiers changed from: private */
    public boolean l() {
        return aa.a("TBL_ACCOUNTINFO", "moneytype", (int) this.y.e(), 1) == aa.a("TBL_ACCOUNTINFO", "moneytype", (int) this.y.g(), 1);
    }

    static /* synthetic */ void n(ml mlVar) {
        if (mlVar.y != null) {
            mlVar.x = 1;
            m.a(mlVar.o);
        }
    }

    public final long a() {
        if (this.y == null) {
            return 0;
        }
        return this.y.a();
    }

    public final void a(int i, int i2, Intent intent) {
        super.a(i, i2, intent);
        if (-1 == i2 && intent != null) {
            switch (i) {
                case 16:
                case 34:
                    String string = intent.getExtras().getString("Text_String");
                    double a2 = m.a(string, this.b, this.m);
                    String obj = this.j.getText().toString();
                    if (obj != null && Double.valueOf(obj).doubleValue() <= 0.0d) {
                        this.j.setText(m.a(a2, 2));
                    }
                    if (string != null) {
                        if (this.y.d() == 0 && vk.a(a2)) {
                            this.y.c((long) (a2 * 100.0d));
                        }
                        if (this.y.b() == 0 && vk.a(a2)) {
                            this.y.b((long) (a2 * 100.0d));
                        }
                        this.y.a(string == null ? "" : string);
                        m.a(string, this.m);
                        return;
                    }
                    return;
                case 37:
                    long longExtra = intent.getLongExtra("account_Sel_Id", this.y.g());
                    if (1 == this.x) {
                        this.y.e(longExtra);
                        m.a(this.o, this.y.g(), this.k);
                        k();
                        return;
                    } else if (2 == this.x) {
                        this.y.d(longExtra);
                        m.a(this.o, this.y.e(), this.c);
                        k();
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public final void a(long j2) {
        if (this.y != null) {
            this.y.a(j2);
        }
        if (this.l != null) {
            m.d(1000 * j2, this.l);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(LinearLayout linearLayout) {
        this.g = linearLayout;
        this.a = (LinearLayout) linearLayout.findViewById(C0000R.id.moneyOutItem);
        this.b = (TextView) linearLayout.findViewById(C0000R.id.viewMoneyOut);
        this.c = (TextView) linearLayout.findViewById(C0000R.id.viewAccountOut);
        this.j = (TextView) linearLayout.findViewById(C0000R.id.viewMoneyIn);
        this.k = (TextView) linearLayout.findViewById(C0000R.id.viewAccountIn);
        this.l = (TextView) linearLayout.findViewById(C0000R.id.viewDate);
        this.m = (TextView) linearLayout.findViewById(C0000R.id.viewComment);
        this.b.setText(aa.a(aa.l(this.y.b()), 2));
        m.a("TBL_ACCOUNTINFO", this.y.e(), this.c);
        this.j.setText(aa.a(aa.l(this.y.d()), 2));
        m.a("TBL_ACCOUNTINFO", this.y.g(), this.k);
        m.d(this.y.a() * 1000, this.l);
        m.a(this.y.h(), this.m);
        this.a.setOnClickListener(this.C);
        this.s = linearLayout.findViewById(C0000R.id.accountOutItem);
        this.s.setOnClickListener(this.C);
        this.v = (LinearLayout) linearLayout.findViewById(C0000R.id.moneyInItem);
        this.v.setOnClickListener(this.C);
        this.t = linearLayout.findViewById(C0000R.id.accountInItem);
        this.t.setOnClickListener(this.C);
        this.u = linearLayout.findViewById(C0000R.id.commentItem);
        this.u.setOnClickListener(this.C);
        this.w = linearLayout.findViewById(C0000R.id.dateItem);
        this.w.setOnClickListener(this.C);
        this.r = (Button) linearLayout.findViewById(C0000R.id.voice_input);
        this.r.setOnClickListener(this.C);
        k();
    }

    public final void a(ScrollView scrollView, int i) {
        this.h = scrollView;
        this.i = i;
    }

    public final void a(Object obj) {
        if (u.class.isInstance(obj)) {
            this.y = (u) obj;
            return;
        }
        if (this.y == null) {
            this.y = new u();
        }
        if (s.class.isInstance(obj)) {
            long o = ((s) obj).o();
            this.y.c(o);
            this.y.b(o);
        } else if (m.class.isInstance(obj)) {
            long d = ((m) obj).d();
            this.y.c(d);
            this.y.b(d);
        } else if (d.class.isInstance(obj)) {
            long b2 = ((d) obj).b();
            this.y.c(b2);
            this.y.b(b2);
        }
    }

    public final void a(String str) {
        if (str != null) {
            this.y.a(str);
            m.a(this.y.h(), this.m);
        }
    }

    public final Object b() {
        return this.y;
    }

    public final void b(long j2) {
        this.y.b(j2);
        this.y.c(j2);
        this.b.setText(aa.a(aa.l(this.y.b()), 2));
        this.j.setText(aa.a(aa.l(this.y.d()), 2));
    }

    public final boolean c() {
        if (this.y.d() <= 0 || this.y.b() <= 0) {
            m.a(this.n, (Animation) null, 0, (View) null, (int) C0000R.string.txtInvalidMoney);
            return false;
        } else if (!aa.b("TBL_ACCOUNTINFO", this.y.e())) {
            m.a(this.n, (Animation) null, 0, (View) null, (int) C0000R.string.txtInvalidAccountOut);
            return false;
        } else if (!aa.b("TBL_ACCOUNTINFO", this.y.g())) {
            m.a(this.n, (Animation) null, 0, (View) null, (int) C0000R.string.txtInvalidAccountIn);
            return false;
        } else if (this.y.g() == this.y.e()) {
            m.a(this.n, (Animation) null, 0, (View) null, (int) C0000R.string.txtSameAccount);
            return false;
        } else if (!l() || this.y.b() == this.y.d()) {
            this.y.f(false);
            this.y.c();
            return true;
        } else {
            m.a(this.n, (Animation) null, 0, (View) null, (int) C0000R.string.txtDiffMoney);
            return false;
        }
    }

    public final void d() {
        if (this.f != null) {
            if (af.class.isInstance(this.f)) {
                ((af) this.f).a(this.z, this.a);
            } else if (zl.class.isInstance(this.f)) {
                ((zl) this.f).a(this.B, this.w);
            }
        }
    }

    public final ca e() {
        return this.f;
    }

    public final void f() {
        this.y.k(0);
        this.y.a("");
        this.y.b(0);
        this.y.c(0);
        this.b.setText(aa.a(aa.l(this.y.b()), 2));
        this.j.setText(aa.a(aa.l(this.y.d()), 2));
        m.a(this.y.h(), this.m);
    }
}
