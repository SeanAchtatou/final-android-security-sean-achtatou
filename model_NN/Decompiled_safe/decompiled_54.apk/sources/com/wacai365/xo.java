package com.wacai365;

import android.content.Context;
import com.wacai.b;

final class xo extends Thread {
    private /* synthetic */ AccountRegister a;

    xo(AccountRegister accountRegister) {
        this.a = accountRegister;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener, boolean):void
     arg types: [com.wacai365.AccountRegister, ?, int, android.content.DialogInterface$OnClickListener, int]
     candidates:
      com.wacai365.m.a(android.content.Context, android.view.animation.Animation, int, android.view.View, int):android.view.animation.Animation
      com.wacai365.m.a(android.view.animation.Animation, int, android.view.View, int, int):android.view.animation.Animation
      com.wacai365.m.a(android.content.Context, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, boolean, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener, boolean):void */
    public final void run() {
        if (b.m().b() == null || b.m().b().length() <= 0) {
            m.a((Context) this.a, (int) C0000R.string.txtSucceedPrompt, true, this.a.a, true);
            this.a.a();
            return;
        }
        m.a(this.a, (int) C0000R.string.txtAlertTitleInfo, (int) C0000R.string.registAccountPrompt, (int) C0000R.string.txtOK, (int) C0000R.string.txtCancel, this.a.b);
    }
}
