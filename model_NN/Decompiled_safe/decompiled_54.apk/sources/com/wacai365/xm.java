package com.wacai365;

import android.view.View;
import com.wacai.a.a;
import java.util.Calendar;
import java.util.Date;

final class xm implements View.OnClickListener {
    final /* synthetic */ QueryStatBase a;

    xm(QueryStatBase queryStatBase) {
        this.a = queryStatBase;
    }

    public final void onClick(View view) {
        Calendar instance = Calendar.getInstance();
        Date date = new Date(a.b(this.a.c.b));
        instance.setTime(date);
        if (QueryStatBase.a == this.a.C) {
            new ut(this.a, this.a, new mj(this), date, 2).show();
        } else {
            new ut(this.a, this.a, new mk(this), date, 1).show();
        }
    }
}
