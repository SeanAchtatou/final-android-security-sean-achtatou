package com.wacai365;

import android.content.Intent;
import android.view.View;

final class abj implements View.OnClickListener {
    private /* synthetic */ kk a;

    abj(kk kkVar) {
        this.a = kkVar;
    }

    public final void onClick(View view) {
        Intent intent = new Intent(this.a.a, MyShortcuts.class);
        intent.putExtra("LaunchedByApplication", 1);
        this.a.a.startActivityForResult(intent, 0);
    }
}
