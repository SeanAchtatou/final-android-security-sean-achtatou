package com.wacai365;

import android.content.DialogInterface;
import android.view.View;
import android.view.animation.Animation;
import com.wacai.b;

final class yz implements DialogInterface.OnClickListener {
    final /* synthetic */ Login a;

    yz(Login login) {
        this.a = login;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case -3:
                m.a(this.a, (int) C0000R.string.resetPrompt, new xr(this));
                return;
            case -2:
            default:
                return;
            case -1:
                String e = b.m().e();
                String b = b.m().b();
                if ((e == null || e.length() <= 0) && (b == null || b.length() <= 0)) {
                    m.a(this.a, (Animation) null, 0, (View) null, (int) C0000R.string.txtNotRegisterUser);
                    return;
                } else {
                    this.a.showDialog(11);
                    return;
                }
        }
    }
}
