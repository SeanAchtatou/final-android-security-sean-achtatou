package com.wacai365;

import android.os.Parcel;
import android.os.Parcelable;
import com.wacai.a.a;
import com.wacai.b;
import com.wacai.data.aa;
import java.util.Date;

public class QueryInfo implements Parcelable {
    public static final Parcelable.Creator CREATOR = new fb();
    public int a;
    public long b;
    public long c;
    public int d;
    public int e;
    public int f;
    public int g;
    public String h;
    public String i;
    public long j;
    public long k;
    public long l;
    public long m;
    public int n;
    public int o;
    public int p;
    public int q;
    public int r;
    public int s;
    public int t;
    public int[] u;

    public QueryInfo() {
        this.a = 10;
        this.b = 0;
        this.c = 0;
        this.d = (int) b.m().j();
        this.e = -1;
        this.f = -1;
        this.g = -1;
        this.h = "";
        this.i = "";
        this.j = -1;
        this.k = -1;
        this.l = -1;
        this.m = -1;
        this.n = -1;
        this.o = -1;
        this.p = -1;
        this.q = -1;
        this.r = 0;
        this.s = 0;
        this.t = 15;
        this.u = new int[]{15, 1, 2, 4, 8};
    }

    /* synthetic */ QueryInfo(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private QueryInfo(Parcel parcel, byte b2) {
        this.a = 10;
        this.b = 0;
        this.c = 0;
        this.d = (int) b.m().j();
        this.e = -1;
        this.f = -1;
        this.g = -1;
        this.h = "";
        this.i = "";
        this.j = -1;
        this.k = -1;
        this.l = -1;
        this.m = -1;
        this.n = -1;
        this.o = -1;
        this.p = -1;
        this.q = -1;
        this.r = 0;
        this.s = 0;
        this.t = 15;
        this.u = new int[]{15, 1, 2, 4, 8};
        this.a = parcel.readInt();
        this.b = parcel.readLong();
        this.c = parcel.readLong();
        this.d = parcel.readInt();
        this.e = parcel.readInt();
        this.f = parcel.readInt();
        this.g = parcel.readInt();
        this.h = parcel.readString();
        this.i = parcel.readString();
        this.j = parcel.readLong();
        this.k = parcel.readLong();
        this.l = parcel.readLong();
        this.m = parcel.readLong();
        this.s = parcel.readInt();
        this.n = parcel.readInt();
        this.o = parcel.readInt();
        this.t = parcel.readInt();
        this.p = parcel.readInt();
        this.q = parcel.readInt();
        this.r = parcel.readInt();
    }

    public QueryInfo(QueryInfo queryInfo) {
        this.a = 10;
        this.b = 0;
        this.c = 0;
        this.d = (int) b.m().j();
        this.e = -1;
        this.f = -1;
        this.g = -1;
        this.h = "";
        this.i = "";
        this.j = -1;
        this.k = -1;
        this.l = -1;
        this.m = -1;
        this.n = -1;
        this.o = -1;
        this.p = -1;
        this.q = -1;
        this.r = 0;
        this.s = 0;
        this.t = 15;
        this.u = new int[]{15, 1, 2, 4, 8};
        this.a = queryInfo.a;
        this.b = queryInfo.b;
        this.c = queryInfo.c;
        this.d = queryInfo.d;
        this.e = queryInfo.e;
        this.f = queryInfo.f;
        this.g = queryInfo.g;
        this.h = new String(queryInfo.h);
        this.i = new String(queryInfo.i);
        this.j = queryInfo.j;
        this.k = queryInfo.k;
        this.l = queryInfo.l;
        this.m = queryInfo.m;
        this.n = queryInfo.n;
        this.o = queryInfo.o;
        this.t = queryInfo.t;
        this.p = queryInfo.p;
        this.q = queryInfo.q;
        this.r = queryInfo.r;
    }

    public final void a(int i2) {
        this.a = i2;
        Date date = new Date();
        Date date2 = new Date();
        a.a(this.a, date, date2);
        this.b = a.a(date.getTime());
        this.c = a.a(date2.getTime());
    }

    public final void a(StringBuffer stringBuffer, int i2) {
        stringBuffer.append("type=\"");
        stringBuffer.append(i2);
        if (i2 == 6) {
            stringBuffer.append("\">");
            return;
        }
        stringBuffer.append("\"><wac-startdate>");
        stringBuffer.append(a.b(this.b) / 1000);
        stringBuffer.append("</wac-startdate><wac-enddate>");
        stringBuffer.append(((a.b(this.c) / 1000) + 86400) - 1);
        stringBuffer.append("</wac-enddate>");
        if (this.e != -1) {
            stringBuffer.append("<wac-accountuuid>");
            stringBuffer.append(aa.a("TBL_ACCOUNTINFO", "uuid", this.e));
            stringBuffer.append("</wac-accountuuid>");
        }
        if (this.f != -1) {
            stringBuffer.append("<wac-projectuuid>");
            stringBuffer.append(aa.a("TBL_PROJECTINFO", "uuid", this.f));
            stringBuffer.append("</wac-projectuuid>");
        }
        if (this.g != -1) {
            stringBuffer.append("<wac-reimburse>");
            stringBuffer.append(this.g);
            stringBuffer.append("</wac-reimburse>");
        }
        if (this.d != -1) {
            stringBuffer.append("<wac-moneytypeuuid>");
            stringBuffer.append(aa.a("TBL_MONEYTYPE", "uuid", this.d));
            stringBuffer.append("</wac-moneytypeuuid>");
        }
        if (this.h.length() > 0) {
            stringBuffer.append("<wac-memberlist>");
            String[] split = this.h.split(",");
            for (String parseInt : split) {
                int parseInt2 = Integer.parseInt(parseInt);
                if (parseInt2 > 0) {
                    stringBuffer.append("<wac-memberuuid>");
                    stringBuffer.append(aa.a("TBL_MEMBERINFO", "uuid", parseInt2));
                    stringBuffer.append("</wac-memberuuid>");
                }
            }
            stringBuffer.append("</wac-memberlist>");
        }
        int i3 = (i2 == 1 || i2 == 5) ? 1 : 0;
        stringBuffer.append("<wac-sort>");
        stringBuffer.append(i3);
        stringBuffer.append("</wac-sort>");
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.a);
        parcel.writeLong(this.b);
        parcel.writeLong(this.c);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e);
        parcel.writeInt(this.f);
        parcel.writeInt(this.g);
        parcel.writeString(this.h);
        parcel.writeString(this.i);
        parcel.writeLong(this.j);
        parcel.writeLong(this.k);
        parcel.writeLong(this.l);
        parcel.writeLong(this.m);
        parcel.writeInt(this.s);
        parcel.writeInt(this.n);
        parcel.writeInt(this.o);
        parcel.writeInt(this.t);
        parcel.writeInt(this.p);
        parcel.writeInt(this.q);
        parcel.writeInt(this.r);
    }
}
