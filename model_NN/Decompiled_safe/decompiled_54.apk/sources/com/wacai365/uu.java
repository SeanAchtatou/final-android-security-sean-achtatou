package com.wacai365;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.view.View;
import android.view.animation.Animation;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.wacai.data.aa;
import com.wacai.data.q;
import com.wacai.data.s;
import com.wacai.data.x;
import java.util.Date;

public class uu extends wg implements View.OnClickListener {
    private LinearLayout A;
    private LinearLayout B;
    private LinearLayout C;
    private LinearLayout D;
    private LinearLayout E;
    private LinearLayout F;
    private LinearLayout G;
    private LinearLayout H;
    private LinearLayout I;
    private LinearLayout J;
    /* access modifiers changed from: private */
    public int[] K = null;
    private int[] L = null;
    /* access modifiers changed from: private */
    public ProgressDialog M;
    private va N = new bq(this);
    protected int[] a = null;
    protected rk b = null;
    private LinearLayout c;
    /* access modifiers changed from: private */
    public TextView j;
    private TextView k;
    private TextView l;
    private TextView m;
    /* access modifiers changed from: private */
    public TextView r;
    /* access modifiers changed from: private */
    public TextView s;
    private TextView t;
    /* access modifiers changed from: private */
    public TextView u;
    /* access modifiers changed from: private */
    public TextView v;
    /* access modifiers changed from: private */
    public TextView w;
    private TextView x;
    private CheckBox y;
    /* access modifiers changed from: private */
    public q z;

    public uu(long j2) {
        if (j2 > 0) {
            this.z = q.f(j2);
        } else {
            this.z = new q();
            this.z.s().add(new x(this.z));
        }
        this.d = C0000R.layout.schedule_outgo_tab;
    }

    /* access modifiers changed from: private */
    public void a(int i, Context context) {
        Resources resources = context.getResources();
        if (i == 0) {
            this.D.setVisibility(8);
            this.m.setText(resources.getText(C0000R.string.txtPerDay).toString());
        } else if (i == 1) {
            this.D.setVisibility(8);
            this.m.setText(resources.getText(C0000R.string.txtWorkingDay).toString());
        } else if (i == 2) {
            this.D.setVisibility(0);
            this.m.setText(resources.getText(C0000R.string.txtPerMonth).toString());
            this.r.setText("" + (this.z.e() < 1 ? 1 : this.z.e()));
        }
    }

    public final void a(int i, int i2, Intent intent) {
        if (i2 == -1 && intent != null) {
            switch (i) {
                case 3:
                    m.a(intent, this.z);
                    m.a(this.z.s(), this.s);
                    break;
                case 6:
                    long longExtra = intent.getLongExtra("Outgo_Sel_Id", this.z.b());
                    if (longExtra > 0) {
                        this.z.a(longExtra);
                        m.a(longExtra, this.l);
                        break;
                    }
                    break;
                case 18:
                    String stringExtra = intent.getStringExtra("Text_String");
                    if (stringExtra == null) {
                        stringExtra = "";
                    }
                    String trim = stringExtra.trim();
                    this.k.setText(trim);
                    this.z.a(trim);
                    break;
                case 24:
                    long longExtra2 = intent.getLongExtra("Target_ID", 0);
                    this.z.j(longExtra2);
                    m.a("TBL_TRADETARGET", longExtra2, this.x);
                    break;
                case 37:
                    this.z.h(intent.getLongExtra("account_Sel_Id", this.z.p()));
                    m.a(this.o, this.z.p(), this.t);
                    break;
            }
            super.a(i, i2, intent);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(LinearLayout linearLayout) {
        this.g = linearLayout;
        this.j = (TextView) linearLayout.findViewById(C0000R.id.viewMoney);
        this.k = (TextView) linearLayout.findViewById(C0000R.id.viewName);
        this.l = (TextView) linearLayout.findViewById(C0000R.id.viewType);
        this.m = (TextView) linearLayout.findViewById(C0000R.id.viewTimeType);
        this.s = (TextView) linearLayout.findViewById(C0000R.id.viewMember);
        this.t = (TextView) linearLayout.findViewById(C0000R.id.viewAccount);
        this.u = (TextView) linearLayout.findViewById(C0000R.id.viewProject);
        this.v = (TextView) linearLayout.findViewById(C0000R.id.viewStartDate);
        this.w = (TextView) linearLayout.findViewById(C0000R.id.viewEndDate);
        this.r = (TextView) linearLayout.findViewById(C0000R.id.viewOccurDate);
        this.x = (TextView) linearLayout.findViewById(C0000R.id.viewTarget);
        this.y = (CheckBox) linearLayout.findViewById(C0000R.id.cbReimburse);
        this.c = (LinearLayout) linearLayout.findViewById(C0000R.id.moneyItem);
        this.c.setOnClickListener(this);
        this.A = (LinearLayout) linearLayout.findViewById(C0000R.id.nameItem);
        this.A.setOnClickListener(this);
        this.B = (LinearLayout) linearLayout.findViewById(C0000R.id.typeItem);
        this.B.setOnClickListener(this);
        this.C = (LinearLayout) linearLayout.findViewById(C0000R.id.timeItem);
        this.C.setOnClickListener(this);
        this.D = (LinearLayout) linearLayout.findViewById(C0000R.id.occurDateItem);
        this.D.setOnClickListener(this);
        this.E = (LinearLayout) linearLayout.findViewById(C0000R.id.accountItem);
        this.E.setOnClickListener(this);
        this.F = (LinearLayout) linearLayout.findViewById(C0000R.id.memberItem);
        this.F.setOnClickListener(this);
        this.G = (LinearLayout) linearLayout.findViewById(C0000R.id.projectItem);
        this.G.setOnClickListener(this);
        this.H = (LinearLayout) linearLayout.findViewById(C0000R.id.startDateItem);
        this.H.setOnClickListener(this);
        this.I = (LinearLayout) linearLayout.findViewById(C0000R.id.endDateItem);
        this.I.setOnClickListener(this);
        this.J = (LinearLayout) linearLayout.findViewById(C0000R.id.targetItem);
        this.J.setOnClickListener(this);
        this.y.setOnCheckedChangeListener(new py(this));
        if (!(this.j == null || this.z == null)) {
            this.j.setText(aa.a(aa.l(this.z.o()), 2));
        }
        this.k.setText(this.z.a());
        m.a("TBL_ACCOUNTINFO", this.z.p(), this.t);
        m.a(this.z.b(), this.l);
        m.a("TBL_PROJECTINFO", this.z.q(), this.u);
        m.a(this.z.s(), this.s);
        m.c(this.z.e(), this.v);
        m.c(this.z.m(), this.w);
        m.a("TBL_TRADETARGET", this.z.r(), this.x);
        m.c(this.z.g() * 1000, this.v);
        m.c(this.z.m() * 1000, this.w);
        a((int) this.z.d(), this.n);
        this.y.setChecked(this.z.n());
        if (this.M == null) {
            this.M = new ProgressDialog(this.n);
            this.M.setIndeterminate(true);
            this.M.setCancelable(false);
        }
    }

    public final void a(ScrollView scrollView, int i) {
        this.h = scrollView;
        this.i = i;
    }

    public final void a(y yVar, boolean z2) {
        switch (bm.a[yVar.ordinal()]) {
            case 1:
                this.e = z2;
                if (this.e) {
                    this.f = af.a(this.q, this.o, m.a(this.z.o()), false);
                    ((af) this.f).a(this.N, this.c);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void a(Object obj) {
        if (q.class.isInstance(obj)) {
            this.z = (q) obj;
            return;
        }
        if (this.z == null) {
            this.z = new q();
        }
        if (s.class.isInstance(obj)) {
            this.z.g(((s) obj).o());
        }
    }

    public final Object b() {
        return this.z;
    }

    public final void b(long j2) {
        this.z.g(j2);
        this.j.setText(aa.a(aa.l(this.z.o()), 2));
    }

    public final boolean c() {
        if (!oj.a(this.o, this.z)) {
            return false;
        }
        if (this.z.a() == null || this.z.a().equals("")) {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.txtEmptyName);
            return false;
        } else if (!aa.b("TBL_OUTGOSUBTYPEINFO", this.z.b())) {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideOutgoST);
            return false;
        } else if (!aa.b("TBL_ACCOUNTINFO", this.z.p())) {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideAccount);
            return false;
        } else if (!aa.b("TBL_PROJECTINFO", this.z.q())) {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideProject);
            return false;
        } else if (!x.a(this.z.s())) {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideMember);
            return false;
        } else if ((this.z.m() - this.z.g()) / 86400 <= 0) {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.txtDurationError);
            return false;
        } else {
            long g = this.z.d() == 2 ? this.z.g() + 946080000 : this.z.g() + 31536000;
            if (this.z.m() > g) {
                m.a((Animation) null, 0, (View) null, String.format(this.o.getResources().getString(C0000R.string.txtScheduleDateExceedLimit), m.c.format(new Date(g * 1000))));
                return false;
            }
            this.z.a(this.y.isChecked());
            this.z.f(false);
            this.M.setTitle(this.n.getResources().getText(C0000R.string.txtDataSave));
            this.M.setMessage(this.n.getResources().getText(C0000R.string.txtScheduleOutgoCreating));
            this.M.show();
            new qe(this).start();
            return true;
        }
    }

    public final void d() {
        if (this.f != null && af.class.isInstance(this.f)) {
            ((af) this.f).a(this.N, this.c);
        }
    }

    public final ca e() {
        return this.f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.c(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[]
     arg types: [android.app.Activity, int, com.wacai365.pz]
     candidates:
      com.wacai365.m.c(java.lang.String, java.lang.String, java.lang.String):android.database.Cursor
      com.wacai365.m.c(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String, int, int, int]
     candidates:
      com.wacai365.m.a(android.content.Context, int, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[]
      com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.app.Activity, long, boolean):void
     arg types: [android.app.Activity, long, int]
     candidates:
      com.wacai365.m.a(java.lang.String, android.widget.TextView, android.widget.TextView):double
      com.wacai365.m.a(long, long, long):long
      com.wacai365.m.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String
      com.wacai365.m.a(android.app.Activity, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, java.util.ArrayList, long):void
      com.wacai365.m.a(android.content.Context, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, android.widget.TextView):void
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(java.lang.String, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean, boolean):boolean
      com.wacai365.m.a(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.app.Activity, long, boolean):void */
    public void onClick(View view) {
        if (this.f != null) {
            this.f.a(false);
            this.f = null;
        }
        if (view == this.B) {
            m.c(this.o);
        } else if (view == this.E) {
            m.a((Activity) this.n);
        } else if (view == this.G) {
            this.a = m.c((Context) this.o, false, (DialogInterface.OnClickListener) new pz(this));
        } else if (view == this.F) {
            this.b = m.a(this.o, this.z.s(), new pw(this), new px(this));
        } else if (view == this.c) {
            this.f = af.a(this.q, this.o, m.a(this.z.o()), false);
            ((af) this.f).a(this.N, this.c);
        } else if (view == this.A) {
            m.a(this.o, this.z.a(), this.n.getResources().getString(C0000R.string.txtEditName), 20, 18, true);
        } else if (view == this.C) {
            this.K = m.e(this.o, new qn(this));
        } else if (view == this.D) {
            m.f(this.o, new qo(this));
        } else if (view == this.J) {
            m.a(this.o, this.z.r(), false);
        } else if (view == this.H) {
            new ut(this.o, this.n, new qk(this), new Date(this.z.g() * 1000), 2).show();
        } else if (view == this.I) {
            new ut(this.o, this.n, new qm(this), new Date(this.z.m() * 1000), 2).show();
        }
    }
}
