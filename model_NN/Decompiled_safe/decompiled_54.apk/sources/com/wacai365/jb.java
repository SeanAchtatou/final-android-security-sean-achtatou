package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;

final class jb implements DialogInterface.OnClickListener {
    private /* synthetic */ bj a;

    jb(bj bjVar) {
        this.a = bjVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == 0) {
            this.a.o.startActivityForResult(new Intent(this.a.o, InputIncomeMainType.class), 0);
        } else {
            this.a.D.a((long) this.a.F[i]);
            m.a("TBL_INCOMEMAINTYPEINFO", (long) this.a.F[i], this.a.k);
        }
        dialogInterface.dismiss();
    }
}
