package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import com.wacai.a;
import com.wacai.e;

public class SettingProjectMgr extends WacaiActivity {
    /* access modifiers changed from: private */
    public Button a;
    /* access modifiers changed from: private */
    public Button b;
    private ListView c;
    private ListAdapter d;
    private View.OnClickListener e = new te(this);

    /* access modifiers changed from: private */
    public void a(int i) {
        Cursor cursor = (Cursor) this.c.getItemAtPosition(i);
        long j = cursor != null ? cursor.getLong(cursor.getColumnIndexOrThrow("_id")) : 0;
        Intent intent = new Intent(this, InputProject.class);
        intent.putExtra("Record_Id", j);
        startActivityForResult(intent, 0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        switch (menuItem.getItemId()) {
            case C0000R.id.idEdit /*2131493342*/:
                a(adapterContextMenuInfo.position);
                return false;
            default:
                return false;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.setting_item_list);
        this.a = (Button) findViewById(C0000R.id.btnAdd);
        this.a.setOnClickListener(this.e);
        this.b = (Button) findViewById(C0000R.id.btnCancel);
        this.b.setOnClickListener(this.e);
        this.c = (ListView) findViewById(C0000R.id.IOList);
        Cursor rawQuery = e.c().b().rawQuery(a.a("BasicSortStyle", 0) == 0 ? "select id as _id, name as _name, enable as _enable, isdefault as _isdefault from TBL_PROJECTINFO where enable = 1 " + " ORDER BY orderno ASC " : "select id as _id, name as _name, enable as _enable, isdefault as _isdefault from TBL_PROJECTINFO where enable = 1 " + " ORDER BY pinyin ASC ", null);
        startManagingCursor(rawQuery);
        this.d = new SimpleCursorAdapter(this, C0000R.layout.list_item_withoutcheckable, rawQuery, new String[]{"_name"}, new int[]{C0000R.id.listitem1});
        this.c.setAdapter(this.d);
        this.c.setOnItemClickListener(new tf(this));
    }
}
