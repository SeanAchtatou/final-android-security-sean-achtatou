package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.wacai.data.g;
import com.wacai.data.q;
import com.wacai.e;
import java.text.ParseException;
import java.util.Date;

public class MyScheduleTrade extends WacaiActivity implements View.OnClickListener {
    private Button a;
    private Button b;
    /* access modifiers changed from: private */
    public Button c;
    private Button d;
    private Button e;
    private ListView f = null;
    private TextView g = null;
    /* access modifiers changed from: private */
    public String[] h;
    /* access modifiers changed from: private */
    public int i = 1;

    /* access modifiers changed from: private */
    public void b(int i2) {
        String str = "";
        Date date = new Date();
        try {
            date = m.c.parse(m.c.format(date));
        } catch (ParseException e2) {
            e2.printStackTrace();
        }
        long time = date.getTime() / 1000;
        if (i2 == 0) {
            str = String.format("and a.enddate < %d", Long.valueOf(time));
        } else if (i2 != 2) {
            str = String.format("and a.enddate >= %d", Long.valueOf(time));
        }
        Cursor rawQuery = e.c().b().rawQuery(String.format("SELECT 0 as _flag2, a.id as _id, a.name as _name, a.cycle as _cycle, a.occurday as _occurday, a.money as _money, c.flag as _flag  FROM TBL_SCHEDULEOUTGOINFO a, tbl_accountinfo b, tbl_moneytype c where a.isdelete = 0 and a.accountid = b.id and b.moneytype = c.id %s union all SELECT 1 as _flag2, a.id as _id, a.name as _name, a.cycle as _cycle, a.occurday as _occurday, a.money as _money, c.flag as _flag  FROM TBL_SCHEDULEINCOMEINFO a, tbl_accountinfo b, tbl_moneytype c where a.isdelete = 0 and a.accountid = b.id and b.moneytype = c.id %s ", str, str), null);
        startManagingCursor(rawQuery);
        rawQuery.registerDataSetObserver(new fu(rawQuery, this.f, this.g, getResources().getString(C0000R.string.txtScheduleEmptyHint)));
        this.f.setAdapter((ListAdapter) new ar(this, C0000R.layout.list_item_3_withicon, rawQuery, new String[]{"_name", "_cycle", "_occurday"}, new int[]{C0000R.id.listitem1, C0000R.id.listitem2, C0000R.id.listitem3}));
    }

    public final void a(int i2) {
        Cursor cursor = (Cursor) ((ListView) findViewById(C0000R.id.IOList)).getItemAtPosition(i2);
        int i3 = cursor.getInt(cursor.getColumnIndexOrThrow("_flag2"));
        long j = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
        Intent a2 = InputScheduleTrade.a(this, null, j);
        a2.putExtra("LaunchedByApplication", 1);
        if (i3 == 0) {
            a2.putExtra("Extra_Type", 0);
        } else {
            a2.putExtra("Extra_Type", 1);
        }
        a2.putExtra("Extra_Id", j);
        startActivityForResult(a2, 0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 != -1) {
        }
    }

    public void onClick(View view) {
        if (view == this.a) {
            if (this.i > 0) {
                Button button = this.c;
                String[] strArr = this.h;
                int i2 = this.i - 1;
                this.i = i2;
                button.setText(strArr[i2]);
                b(this.i);
                return;
            }
            this.i = this.h.length - 1;
            this.c.setText(this.h[this.i]);
            b(this.i);
        } else if (view == this.b) {
            if (this.i < this.h.length - 1) {
                Button button2 = this.c;
                String[] strArr2 = this.h;
                int i3 = this.i + 1;
                this.i = i3;
                button2.setText(strArr2[i3]);
                b(this.i);
                return;
            }
            this.i = 0;
            this.c.setText(this.h[this.i]);
            b(this.i);
        } else if (view == this.d) {
            Intent a2 = InputScheduleTrade.a(this, null, 0);
            a2.putExtra("LaunchedByApplication", 1);
            startActivityForResult(a2, 1000);
        } else if (view == this.e) {
            finish();
        } else if (view == this.c) {
            m.g(this, new th(this));
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        int i2;
        long j;
        g f2;
        q f3;
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        switch (menuItem.getItemId()) {
            case C0000R.id.idDelete /*2131493338*/:
                ListView listView = (ListView) findViewById(C0000R.id.IOList);
                Cursor cursor = (Cursor) listView.getItemAtPosition(adapterContextMenuInfo.position);
                if (cursor != null) {
                    j = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
                    i2 = cursor.getInt(cursor.getColumnIndexOrThrow("_flag2"));
                } else {
                    i2 = 0;
                    j = 0;
                }
                if (i2 == 0) {
                    if (j > 0 && (f3 = q.f(j)) != null) {
                        f3.f();
                    }
                } else if (i2 == 1 && j > 0 && (f2 = g.f(j)) != null) {
                    f2.f();
                }
                WidgetProvider.a(this);
                cursor.requery();
                listView.invalidateViews();
                break;
            case C0000R.id.idEdit /*2131493342*/:
                ListView listView2 = (ListView) findViewById(C0000R.id.IOList);
                a(adapterContextMenuInfo.position);
                ((Cursor) listView2.getItemAtPosition(adapterContextMenuInfo.position)).requery();
                listView2.invalidateViews();
                break;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.schedule_trade_list);
        this.h = getResources().getStringArray(C0000R.array.SearchType);
        this.a = (Button) findViewById(C0000R.id.btnPrev);
        this.a.setOnClickListener(this);
        this.b = (Button) findViewById(C0000R.id.btnNext);
        this.b.setOnClickListener(this);
        this.d = (Button) findViewById(C0000R.id.btnAdd);
        this.d.setOnClickListener(this);
        this.e = (Button) findViewById(C0000R.id.btnCancel);
        this.e.setOnClickListener(this);
        this.c = (Button) findViewById(C0000R.id.btnCur);
        this.c.setOnClickListener(this);
        this.c.setText(this.h[this.i]);
        ArrayAdapter.createFromResource(this, C0000R.array.SearchType, 17367048).setDropDownViewResource(17367049);
        this.f = (ListView) findViewById(C0000R.id.IOList);
        this.f.setOnItemClickListener(new qd(this));
        this.f.setOnCreateContextMenuListener(new qb(this));
        this.g = (TextView) findViewById(C0000R.id.id_listhint);
        b(this.i);
    }
}
