package com.wacai365;

import android.content.Intent;
import android.view.View;

final class v implements View.OnClickListener {
    private /* synthetic */ SettingBudgetMgr a;

    v(SettingBudgetMgr settingBudgetMgr) {
        this.a = settingBudgetMgr;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.b)) {
            this.a.startActivityForResult(new Intent(this.a, InputBudget.class), 0);
        } else if (view.equals(this.a.c)) {
            this.a.finish();
        }
    }
}
