package com.wacai365;

import android.content.Intent;
import android.view.View;

final class s implements View.OnClickListener {
    private /* synthetic */ ChooseAccountType a;

    s(ChooseAccountType chooseAccountType) {
        this.a = chooseAccountType;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.d)) {
            this.a.finish();
        } else if (view.equals(this.a.c)) {
            ChooseAccountType chooseAccountType = this.a;
            chooseAccountType.startActivityForResult(new Intent(chooseAccountType, InputAccount.class), 18);
        }
    }
}
