package com.wacai365;

import android.content.DialogInterface;
import android.view.View;
import android.view.animation.Animation;
import android.widget.EditText;
import com.wacai.a.f;
import com.wacai.b;

final class zb implements DialogInterface.OnClickListener {
    private /* synthetic */ EditText a;
    private /* synthetic */ EditText b;
    private /* synthetic */ boolean c;
    private /* synthetic */ Login d;

    zb(Login login, EditText editText, EditText editText2, boolean z) {
        this.d = login;
        this.a = editText;
        this.b = editText2;
        this.c = z;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String obj = this.a.getText().toString();
        String obj2 = this.b.getText().toString();
        String b2 = b.m().b();
        String e = b.m().e();
        if (obj != null) {
            String trim = obj.trim();
            if (trim.length() > 0) {
                if (!this.c || m.a(trim)) {
                    if (obj2 != null) {
                        String trim2 = obj2.trim();
                        if (trim2.length() > 0) {
                            if (trim.equalsIgnoreCase(b2) || trim.equalsIgnoreCase(e)) {
                                String a2 = f.a(trim2);
                                if (a2.equals(b.m().c())) {
                                    Login.a();
                                    m.a(this.d, (int) C0000R.string.txtAlertTitleInfo, (int) C0000R.string.txtResetLocalPSWSucceedPrompt, (int) C0000R.string.txtOK, 0, this.d.a);
                                    return;
                                }
                                Login.a(this.d, trim, a2);
                                return;
                            }
                            m.a(this.d, (Animation) null, 0, (View) null, (int) C0000R.string.txtNotSameLoginUser);
                            return;
                        }
                    }
                    m.a(this.d, (Animation) null, 0, (View) null, (int) C0000R.string.txtEmptyPassword);
                    return;
                }
                m.a(this.d, (Animation) null, 0, (View) null, (int) C0000R.string.txtInvalidEmail);
                return;
            }
        }
        m.a(this.d, (Animation) null, 0, (View) null, this.c ? C0000R.string.txtEmptyEmail : C0000R.string.txtEmptyUserName);
    }
}
