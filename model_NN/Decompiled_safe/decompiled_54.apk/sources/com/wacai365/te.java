package com.wacai365;

import android.content.Intent;
import android.view.View;

final class te implements View.OnClickListener {
    private /* synthetic */ SettingProjectMgr a;

    te(SettingProjectMgr settingProjectMgr) {
        this.a = settingProjectMgr;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.a)) {
            this.a.startActivityForResult(new Intent(this.a, InputProject.class), 0);
        } else if (view.equals(this.a.b)) {
            this.a.finish();
        }
    }
}
