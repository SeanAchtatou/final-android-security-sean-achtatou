package com.wacai365;

import android.view.View;
import java.util.Date;

final class yu implements View.OnClickListener {
    final /* synthetic */ ml a;

    yu(ml mlVar) {
        this.a = mlVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String, int, int, int]
     candidates:
      com.wacai365.m.a(android.content.Context, int, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[]
      com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void */
    public final void onClick(View view) {
        if (this.a.f != null) {
            this.a.f.a(false);
            this.a.f = null;
        }
        if (view.equals(this.a.a)) {
            this.a.f = af.a(this.a.q, this.a.o, m.a(this.a.y.b()), false);
            ((af) this.a.f).a(this.a.z, this.a.a);
        } else if (view.equals(this.a.v)) {
            this.a.f = af.a(this.a.q, this.a.o, m.a(this.a.y.d()), false);
            ((af) this.a.f).a(this.a.A, this.a.v);
        } else if (view.equals(this.a.s)) {
            ml.l(this.a);
        } else if (view.equals(this.a.t)) {
            ml.n(this.a);
        } else if (view.equals(this.a.u)) {
            m.a(this.a.o, this.a.y.h(), this.a.n.getResources().getString(C0000R.string.txtEditComment), 100, 16, false);
        } else if (view.equals(this.a.w)) {
            new ut(this.a.o, this.a.n, new ui(this), new Date(this.a.y.a() * 1000), 4).show();
        } else if (view.equals(this.a.r)) {
            VoiceInput.a(this.a.o, 100, this.a.y.h());
        }
    }
}
