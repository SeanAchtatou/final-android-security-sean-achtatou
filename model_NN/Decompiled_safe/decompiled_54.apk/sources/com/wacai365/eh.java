package com.wacai365;

import android.view.animation.Animation;
import android.widget.LinearLayout;

final class eh implements Animation.AnimationListener {
    private /* synthetic */ kk a;

    eh(kk kkVar) {
        this.a = kkVar;
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.D.setVisibility(8);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.a.E.getLayoutParams();
        layoutParams.weight = 1.0f;
        this.a.E.setLayoutParams(layoutParams);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
        this.a.E.setVisibility(0);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.a.E.getLayoutParams();
        layoutParams.weight = 0.0f;
        this.a.E.setLayoutParams(layoutParams);
    }
}
