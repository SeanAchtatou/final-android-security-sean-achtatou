package com.wacai365;

import android.view.View;

final class aaf implements View.OnClickListener {
    private /* synthetic */ InputShortcut a;

    aaf(InputShortcut inputShortcut) {
        this.a = inputShortcut;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.d)) {
            if (InputShortcut.k(this.a)) {
                this.a.finish();
            }
        } else if (view.equals(this.a.e)) {
            this.a.finish();
        } else if (view.equals(this.a.f)) {
            InputShortcut inputShortcut = this.a;
            m.a(inputShortcut, (int) C0000R.string.txtDeleteConfirm, new dx(inputShortcut));
        }
    }
}
