package com.wacai365;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import java.util.Date;

final class fp extends SimpleCursorAdapter {
    private LayoutInflater a;
    private String[] b;
    private int[] c;
    private /* synthetic */ AlertCenter d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fp(AlertCenter alertCenter, Context context, int i, Cursor cursor, String[] strArr, int[] iArr) {
        super(context, i, cursor, strArr, iArr);
        this.d = alertCenter;
        this.b = strArr;
        this.c = iArr;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        String string;
        LinearLayout linearLayout = view == null ? (LinearLayout) this.a.inflate((int) C0000R.layout.alert_center_item, (ViewGroup) null) : (LinearLayout) view;
        boolean z = cursor.getInt(cursor.getColumnIndexOrThrow(this.b[5])) == 1;
        View findViewById = linearLayout.findViewById(C0000R.id.list_item);
        if (z) {
            findViewById.setBackgroundResource(C0000R.drawable.bg_alert_list_item);
        } else {
            findViewById.setBackgroundResource(C0000R.drawable.bg_alert_list_item_new);
        }
        View findViewById2 = linearLayout.findViewById(this.c[0]);
        if (findViewById2 != null) {
            switch (cursor.getInt(cursor.getColumnIndexOrThrow(this.b[1]))) {
                case 1:
                    String string2 = cursor.getString(cursor.getColumnIndexOrThrow(this.b[0]));
                    if (string2 == null) {
                        string2 = "";
                    }
                    string = String.format(this.d.getString(C0000R.string.txtAlertMsg), string2);
                    break;
                case 2:
                    string = cursor.getString(cursor.getColumnIndexOrThrow(this.b[2]));
                    break;
                default:
                    string = null;
                    break;
            }
            setViewText((TextView) findViewById2, string);
            setViewText((TextView) linearLayout.findViewById(this.c[1]), m.f.format(new Date(cursor.getLong(cursor.getColumnIndexOrThrow(this.b[4])) * 1000)));
            linearLayout.findViewById(C0000R.id.imagebutton).setOnClickListener(new fs(this.d, cursor.getPosition()));
            linearLayout.findViewById(C0000R.id.list_item).setOnClickListener(new ul(this.d, cursor.getPosition()));
        }
    }
}
