package com.wacai365;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.wacai.b;

public final class ar extends SimpleCursorAdapter {
    private String[] a;
    private int[] b;

    public ar(Context context, int i, Cursor cursor, String[] strArr, int[] iArr) {
        super(context, i, cursor, strArr, iArr);
        this.b = iArr;
        this.a = strArr;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        Resources resources = context.getResources();
        long j = cursor.getLong(cursor.getColumnIndexOrThrow("_flag2"));
        View findViewById = view.findViewById(this.b[0]);
        if (findViewById != null) {
            String string = cursor.getString(cursor.getColumnIndexOrThrow(this.a[0]));
            if (string == null) {
                string = "";
            }
            setViewText((TextView) findViewById, string);
        }
        View findViewById2 = view.findViewById(this.b[1]);
        if (findViewById2 != null) {
            String str = "";
            int i = (int) cursor.getLong(cursor.getColumnIndexOrThrow(this.a[1]));
            int i2 = (int) cursor.getLong(cursor.getColumnIndexOrThrow(this.a[2]));
            if (j == 0 && i == 0) {
                str = resources.getText(C0000R.string.txtPerDay).toString();
            } else if (j == 0 && i == 1) {
                str = resources.getText(C0000R.string.txtWorkingDay).toString();
            } else if (i == 2 || i == 0) {
                str = String.format("%s%d%s", resources.getText(C0000R.string.txtPerMonth).toString(), Integer.valueOf(i2), resources.getText(C0000R.string.txtDay).toString());
            }
            setViewText((TextView) findViewById2, str);
        }
        View findViewById3 = view.findViewById(this.b[2]);
        if (findViewById3 != null) {
            TextView textView = (TextView) findViewById3;
            if (j == 0) {
                textView.setTextColor(resources.getColor(C0000R.color.outgo_money));
            } else if (j == 1) {
                textView.setTextColor(resources.getColor(C0000R.color.income_money));
            }
            long j2 = cursor.getLong(cursor.getColumnIndexOrThrow("_money"));
            String str2 = "";
            if (b.a) {
                str2 = str2 + cursor.getString(cursor.getColumnIndexOrThrow("_flag"));
            }
            textView.setText(str2 + String.format("%.2f", Double.valueOf(m.a(j2))));
        }
        LinearLayout linearLayout = (LinearLayout) view.findViewById(C0000R.id.id_icon);
        if (linearLayout != null) {
            linearLayout.setBackgroundResource(0 == j ? C0000R.drawable.ic_scheduleoutgo : C0000R.drawable.ic_scheduleincome);
        }
    }
}
