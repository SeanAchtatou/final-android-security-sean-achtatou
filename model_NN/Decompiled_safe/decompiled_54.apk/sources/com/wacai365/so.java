package com.wacai365;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.wacai.a.a;
import java.util.Date;

public final class so extends SimpleCursorAdapter {
    private Resources a;
    private boolean b;

    public so(Context context, int i, Cursor cursor, String[] strArr, int[] iArr, boolean z) {
        super(context, i, cursor, strArr, iArr);
        this.a = context.getResources();
        this.b = z;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        String string;
        int i;
        int i2;
        long j = cursor.getLong(cursor.getColumnIndexOrThrow("_flag"));
        TextView textView = (TextView) view.findViewById(C0000R.id.id_desc2);
        textView.setText("");
        View findViewById = view.findViewById(C0000R.id.id_money);
        if (findViewById != null) {
            long j2 = cursor.getLong(cursor.getColumnIndexOrThrow("_money"));
            String b2 = !this.b ? m.b(j2) : cursor.getString(cursor.getColumnIndexOrThrow("_moneyflag1")) + m.b(j2);
            String str = b2 == null ? "" : b2;
            TextView textView2 = (TextView) findViewById;
            switch ((int) j) {
                case 0:
                    textView2.setTextColor(this.a.getColor(C0000R.color.outgo_money));
                    break;
                case 1:
                    textView2.setTextColor(this.a.getColor(C0000R.color.income_money));
                    break;
                case 2:
                case 3:
                case 4:
                    textView2.setTextColor(this.a.getColor(C0000R.color.transfer_money));
                    break;
            }
            setViewText((TextView) findViewById, str);
        }
        ImageView imageView = (ImageView) view.findViewById(C0000R.id.id_icon);
        View findViewById2 = view.findViewById(C0000R.id.id_desc);
        if (findViewById2 != null) {
            if (cursor.getLong(cursor.getColumnIndex("_scheduleid")) == 0 && ((long) cursor.getInt(cursor.getColumnIndex("_reimburse"))) == 1) {
                ((TextView) findViewById2).setTextColor(this.a.getColor(C0000R.color.outgo_money));
            } else {
                ((TextView) findViewById2).setTextColor(this.a.getColor(C0000R.color.text_color_black));
            }
            if (4 == j) {
                if (cursor.getInt(cursor.getColumnIndexOrThrow("_type")) == 0) {
                    imageView.setImageResource(C0000R.drawable.ic_receipt);
                    i2 = C0000R.string.paybackFromsb;
                } else {
                    imageView.setImageResource(C0000R.drawable.ic_repayment);
                    i2 = C0000R.string.paybackTosb;
                }
                string = this.a.getString(i2, cursor.getString(cursor.getColumnIndex("_transferinname")));
            } else if (3 == j) {
                if (cursor.getInt(cursor.getColumnIndexOrThrow("_type")) == 1) {
                    imageView.setImageResource(C0000R.drawable.ic_loanout);
                    i = C0000R.string.loanOutTosb;
                } else {
                    imageView.setImageResource(C0000R.drawable.ic_loanin);
                    i = C0000R.string.loanInFromsb;
                }
                string = this.a.getString(i, cursor.getString(cursor.getColumnIndex("_transferinname")));
            } else if (2 == j) {
                imageView.setImageResource(C0000R.drawable.ic_transfer);
                string = this.a.getString(C0000R.string.txtTransferString);
            } else {
                int i3 = cursor.getInt(cursor.getColumnIndex("_scheduleid"));
                if (1 == j) {
                    imageView.setImageResource(i3 == 0 ? C0000R.drawable.ic_income : C0000R.drawable.ic_scheduleincome);
                } else {
                    imageView.setImageResource(i3 == 0 ? C0000R.drawable.ic_outgo : C0000R.drawable.ic_scheduleoutgo);
                }
                string = cursor.getString(cursor.getColumnIndexOrThrow("_name"));
                if (string == null) {
                    string = "";
                }
                String string2 = cursor.getString(cursor.getColumnIndexOrThrow("_targetname"));
                if (string2 != null) {
                    textView.setText(this.a.getString(0 == j ? C0000R.string.txtDisplayFormatWithTarget : C0000R.string.txtDisplayFormatWithIncomeTarget) + string2);
                }
            }
            setViewText((TextView) findViewById2, string);
        }
        View findViewById3 = view.findViewById(C0000R.id.id_date);
        if (findViewById3 != null) {
            StringBuffer stringBuffer = new StringBuffer(64);
            long j3 = cursor.getLong(cursor.getColumnIndexOrThrow("_outgodate"));
            Date date = new Date(1000 * j3);
            Date date2 = new Date();
            long time = date2.getTime() / 1000;
            if (date.getYear() == date2.getYear() && date.getMonth() == date2.getMonth() && date.getDate() == date2.getDate()) {
                long j4 = time - j3;
                if (3600 <= j4 || j4 <= -3600) {
                    long j5 = j4 / 3600;
                    if (j5 > 0) {
                        stringBuffer.append(j5);
                        stringBuffer.append(this.a.getString(C0000R.string.txtHourOffsetString));
                    } else {
                        stringBuffer.append(-j5);
                        stringBuffer.append(this.a.getString(C0000R.string.txtHourAfterOffsetString));
                    }
                } else {
                    long j6 = j4 / 60;
                    if (j6 > 0) {
                        stringBuffer.append(j6);
                        stringBuffer.append(this.a.getString(C0000R.string.txtMinOffsetString));
                    } else if (j6 < 0) {
                        stringBuffer.append(-j6);
                        stringBuffer.append(this.a.getString(C0000R.string.txtMinAfterOffsetString));
                    } else {
                        stringBuffer.append(this.a.getString(C0000R.string.txtNowString));
                    }
                }
            } else {
                long a2 = a.a(j3 * 1000);
                long a3 = a.a(date2.getTime());
                if (a3 - a2 == 1) {
                    stringBuffer.append(this.a.getString(C0000R.string.txtYesterdayOffsetString));
                    stringBuffer.append(m.d.format(date));
                } else if (a3 - a2 == 2) {
                    stringBuffer.append(this.a.getString(C0000R.string.txt3DayOffsetString));
                    stringBuffer.append(m.d.format(date));
                } else {
                    stringBuffer.append(m.c.format(date));
                }
            }
            setViewText((TextView) findViewById3, stringBuffer.toString());
        }
    }
}
