package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;

final class ub implements DialogInterface.OnClickListener {
    private /* synthetic */ InputTradeTarget a;

    ub(InputTradeTarget inputTradeTarget) {
        this.a = inputTradeTarget;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (this.a.c) {
            InputTradeTarget.g(this.a);
            return;
        }
        Intent intent = this.a.getIntent();
        if (this.a.e.x() > 0) {
            intent.putExtra("ret_id", this.a.e.x());
        }
        this.a.setResult(-1, intent);
        m.a(this.a, this.a.a);
        this.a.finish();
    }
}
