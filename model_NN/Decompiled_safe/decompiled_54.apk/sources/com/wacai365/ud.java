package com.wacai365;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class ud extends WebViewClient {
    private /* synthetic */ WeiboConnect a;

    ud(WeiboConnect weiboConnect) {
        this.a = weiboConnect;
    }

    public final void onPageFinished(WebView webView, String str) {
        Log.v("test", "onPageFinished url=" + str);
        if (str != null) {
            if (!URLUtil.isDataUrl(str) && (this.a.f == null || !this.a.f.equalsIgnoreCase(str))) {
                try {
                    if (this.a.b.a(str)) {
                        this.a.a.f = this.a.b.b();
                        this.a.a.g = this.a.b.c();
                        try {
                            if (!this.a.d.isChecked() || this.a.a.h == null || !this.a.b.b(this.a.a.h)) {
                                this.a.a.i = false;
                                this.a.a.b();
                                m.a(this.a, (Animation) null, -1, (View) null, (int) C0000R.string.txtSucceedPrompt);
                                this.a.setResult(-1);
                                this.a.finish();
                            } else {
                                this.a.a.i = true;
                                this.a.a.b();
                                m.a(this.a, (Animation) null, -1, (View) null, (int) C0000R.string.txtSucceedPrompt);
                                this.a.setResult(-1);
                                this.a.finish();
                            }
                        } catch (Exception e) {
                        }
                    }
                    return;
                } catch (Exception e2) {
                    WeiboConnect.b(this.a, e2.getMessage());
                    return;
                } finally {
                    this.a.runOnUiThread(new dt(this.a, 8));
                }
            }
        }
        this.a.runOnUiThread(new dt(this.a, 8));
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        this.a.runOnUiThread(new dt(this.a, 0));
        super.onPageStarted(webView, str, bitmap);
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return super.shouldOverrideUrlLoading(webView, str);
    }
}
