package com.wacai365;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.a;
import com.wacai.b;
import com.wacai.data.aa;
import com.wacai.data.h;
import com.wacai.data.r;
import com.wacai.data.x;
import com.wacai.e;
import java.util.Date;

public class ReimburseDialog extends WacaiActivity {
    private TextView a;
    /* access modifiers changed from: private */
    public Button b;
    /* access modifiers changed from: private */
    public Button c;
    /* access modifiers changed from: private */
    public LinearLayout d;
    /* access modifiers changed from: private */
    public LinearLayout e;
    /* access modifiers changed from: private */
    public TextView f;
    /* access modifiers changed from: private */
    public double g;
    /* access modifiers changed from: private */
    public r h;
    private long i;
    private long j;
    private long k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p = "";
    private String q = "";
    private int r;
    /* access modifiers changed from: private */
    public af s;
    private View.OnClickListener t = new aaq(this);
    /* access modifiers changed from: private */
    public va u = new aap(this);

    private String a(long j2) {
        return getString(C0000R.string.txtYearMonthDay, new Object[]{Long.valueOf(j2 / 10000), Long.valueOf((j2 / 100) % 100), Long.valueOf(j2 % 100)});
    }

    static /* synthetic */ void d(ReimburseDialog reimburseDialog) {
        if (reimburseDialog.g <= 0.0d) {
            m.a(reimburseDialog, (Animation) null, 0, (View) null, (int) C0000R.string.txtInvalidMoney);
        } else if (!aa.b("TBL_INCOMEMAINTYPEINFO", reimburseDialog.h.a())) {
            m.a(reimburseDialog, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideIncomeT);
        } else if (!aa.b("TBL_ACCOUNTINFO", reimburseDialog.h.p())) {
            m.a(reimburseDialog, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideAccount);
        } else if (!aa.b("TBL_PROJECTINFO", reimburseDialog.h.q())) {
            m.a(reimburseDialog, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideProject);
        } else if (!x.a(reimburseDialog.h.s())) {
            m.a(reimburseDialog, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideMember);
        } else {
            StringBuffer stringBuffer = new StringBuffer(300);
            stringBuffer.append("UPDATE tbl_outgoinfo SET reimburse = 2, updatestatus = 0 WHERE id in ( " + reimburseDialog.n + " )");
            SQLiteDatabase b2 = e.c().b();
            String str = "";
            Cursor rawQuery = b2.rawQuery("SELECT b.flag as _flag FROM TBL_ACCOUNTINFO a, TBL_MONEYTYPE b WHERE a.moneytype = b.id AND a.moneytype = (SELECT moneytype FROM TBL_ACCOUNTINFO WHERE id = " + reimburseDialog.r + ") Group by b.flag", null);
            if (rawQuery != null && rawQuery.moveToFirst()) {
                reimburseDialog.m = rawQuery.getString(rawQuery.getColumnIndex("_flag"));
            }
            rawQuery.close();
            String m2 = aa.m(aa.a(reimburseDialog.g));
            if (reimburseDialog.i != 0 && reimburseDialog.j != 0 && reimburseDialog.i > reimburseDialog.j) {
                str = reimburseDialog.getString(C0000R.string.txtReimburseComment, new Object[]{reimburseDialog.a(reimburseDialog.j), reimburseDialog.a(reimburseDialog.i), reimburseDialog.p, reimburseDialog.q + reimburseDialog.l, reimburseDialog.m + m2});
            } else if (!(reimburseDialog.i == 0 || reimburseDialog.j == 0 || reimburseDialog.i != reimburseDialog.j)) {
                if (reimburseDialog.o == null || reimburseDialog.o.equals("")) {
                    str = reimburseDialog.getString(C0000R.string.txtReimburseCommentSimpleDate, new Object[]{reimburseDialog.a(reimburseDialog.i), reimburseDialog.p, reimburseDialog.q + reimburseDialog.l, reimburseDialog.m + m2});
                } else {
                    str = reimburseDialog.getString(C0000R.string.txtReimburseCommentSimpleRecord, new Object[]{reimburseDialog.a(reimburseDialog.i), reimburseDialog.o, reimburseDialog.q + reimburseDialog.l, reimburseDialog.m + m2});
                }
            }
            reimburseDialog.h.a(str);
            if (reimburseDialog.n != "") {
                b2.execSQL(stringBuffer.toString());
            }
            reimburseDialog.h.f(false);
            reimburseDialog.h.c();
            reimburseDialog.setResult(-1);
            if (a.a("IsAutoBackup", 0) > 0) {
                abt.b(reimburseDialog);
                return;
            }
            m.a(reimburseDialog, (Animation) null, 0, (View) null, (int) C0000R.string.txtReimburseSucceed);
            reimburseDialog.finish();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.app.Activity, long, android.widget.TextView):void
     arg types: [com.wacai365.ReimburseDialog, long, android.widget.TextView]
     candidates:
      com.wacai365.m.a(java.lang.String, android.widget.TextView, android.widget.TextView):double
      com.wacai365.m.a(long, long, long):long
      com.wacai365.m.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String
      com.wacai365.m.a(android.app.Activity, long, boolean):void
      com.wacai365.m.a(android.app.Activity, java.util.ArrayList, long):void
      com.wacai365.m.a(android.content.Context, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, android.widget.TextView):void
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(java.lang.String, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean, boolean):boolean
      com.wacai365.m.a(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.app.Activity, long, android.widget.TextView):void */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && intent != null) {
            switch (i2) {
                case 37:
                    long longExtra = intent.getLongExtra("account_Sel_Id", this.h.p());
                    this.h.h(longExtra);
                    m.a((Activity) this, this.h.p(), this.a);
                    this.r = (int) longExtra;
                    return;
                default:
                    return;
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.reimburse_dialog);
        this.b = (Button) findViewById(C0000R.id.btnOK);
        this.b.setText((int) C0000R.string.txtSave);
        this.c = (Button) findViewById(C0000R.id.btnCancel);
        this.f = (TextView) findViewById(C0000R.id.tvReimburseMoney);
        this.d = (LinearLayout) findViewById(C0000R.id.reimburseMoneyItem);
        this.e = (LinearLayout) findViewById(C0000R.id.id_popupframe);
        this.a = (TextView) findViewById(C0000R.id.viewReimburseAccount);
        this.m = getIntent().getStringExtra("MONEY_FLAG");
        this.q = this.m;
        this.l = m.b(getIntent().getLongExtra("SUM_MONEY", 0));
        this.i = getIntent().getLongExtra("MAX_DAY", 0);
        this.j = getIntent().getLongExtra("MIN_DAY", 0);
        this.n = getIntent().getStringExtra("ID_FOR_SQL");
        this.o = getIntent().getStringExtra("ITEM_TYPE_NAME");
        this.k = (long) getIntent().getIntExtra("MONEY_TYPE", 0);
        long longExtra = getIntent().getLongExtra("PROJECT_ID", 0);
        String stringExtra = getIntent().getStringExtra("PROJECT_NAME");
        if (stringExtra != null) {
            this.p = stringExtra;
        }
        this.b.setOnClickListener(this.t);
        this.c.setOnClickListener(this.t);
        this.d.setOnClickListener(this.t);
        this.s = af.a(this.e, this, m.a(0L), false);
        this.s.a(this.u, this.d);
        this.h = new r();
        this.h.b(new Date().getTime() / 1000);
        this.h.h(h.c(false));
        this.h.s().add(new x(this.h));
        if (longExtra != 0) {
            this.h.i(longExtra);
        }
        SQLiteDatabase b2 = e.c().b();
        Cursor rawQuery = b2.rawQuery("select id from tbl_incomemaintypeinfo where uuid = 14", null);
        if (rawQuery != null && rawQuery.moveToFirst()) {
            this.h.a(rawQuery.getLong(rawQuery.getColumnIndex("id")));
        }
        rawQuery.close();
        if (!b.a) {
            ((TextView) findViewById(C0000R.id.tvMoneySum)).setText(this.l);
        } else {
            ((TextView) findViewById(C0000R.id.tvMoneySum)).setText(this.q + this.l);
        }
        Cursor rawQuery2 = b2.rawQuery("select id from tbl_accountinfo where enable = 1 and moneytype = " + this.k, null);
        if (rawQuery2 == null || !rawQuery2.moveToFirst()) {
            this.r = (int) h.c(false);
        } else {
            this.r = rawQuery2.getInt(rawQuery2.getColumnIndex("id"));
        }
        rawQuery2.close();
        this.h.h((long) this.r);
        m.a("TBL_ACCOUNTINFO", (long) this.r, this.a);
        ((LinearLayout) findViewById(C0000R.id.reimburseAccountItem)).setOnClickListener(new abx(this));
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || this.s == null) {
            return super.onKeyDown(i2, keyEvent);
        }
        this.s.a(true);
        this.s = null;
        return true;
    }
}
