package com.wacai365;

import android.database.Cursor;
import android.view.View;
import android.widget.AdapterView;

final class xc implements AdapterView.OnItemClickListener {
    private /* synthetic */ ChooseOutgoType a;

    xc(ChooseOutgoType chooseOutgoType) {
        this.a = chooseOutgoType;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Cursor cursor = (Cursor) this.a.e.getItemAtPosition(i);
        if (cursor != null) {
            ChooseOutgoType.a(this.a, cursor.getLong(cursor.getColumnIndexOrThrow("_id")));
        }
    }
}
