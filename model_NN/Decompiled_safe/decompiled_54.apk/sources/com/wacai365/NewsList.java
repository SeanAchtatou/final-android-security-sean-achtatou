package com.wacai365;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.wacai.data.c;

public class NewsList extends WacaiActivity {
    /* access modifiers changed from: private */
    public c a;
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener b = new vg(this);

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.news);
        this.a = c.a(getIntent().getLongExtra("Record_Id", -1));
        if (this.a != null) {
            ((TextView) findViewById(C0000R.id.id_aboutTitle)).setText(this.a.a());
            ((TextView) findViewById(C0000R.id.id_copyRight)).setText(this.a.d());
            ((Button) findViewById(C0000R.id.btnDelete)).setOnClickListener(new vi(this));
            Button button = (Button) findViewById(C0000R.id.btnOpenURL);
            if (1 == this.a.e()) {
                button.setText((int) C0000R.string.txtUpdateNow);
            }
            if (this.a.b().length() == 0) {
                button.setEnabled(false);
                button.setClickable(false);
            }
            button.setOnClickListener(new vh(this));
            ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(new vf(this));
            this.a.a(true);
            this.a.c();
        }
    }
}
