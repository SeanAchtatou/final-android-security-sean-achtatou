package com.wacai365;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.webkit.WebView;
import android.widget.Button;
import com.wacai.e;
import com.wacai.f;
import java.util.Date;

public class SoftIntroduce extends WacaiActivity {
    /* access modifiers changed from: private */
    public Button a;
    /* access modifiers changed from: private */
    public WebView b;
    /* access modifiers changed from: private */
    public View c;
    private String d = (e.a + "/wap/softRecommend.jsp?platform=2&mc=");
    private View.OnClickListener e = new tz(this);

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        e.c().b().execSQL(String.format("delete from %s where enddate < %d", "TBL_FRIENDLYLINK", Long.valueOf(new Date().getTime() / 1000)));
        setContentView((int) C0000R.layout.soft_introduce);
        if (!com.wacai.a.e.a()) {
            m.a(this, (Animation) null, -1, (View) null, (int) C0000R.string.txtNoNetworkPrompt);
            finish();
            return;
        }
        this.a = (Button) findViewById(C0000R.id.btnCancel);
        this.a.setOnClickListener(this.e);
        this.c = findViewById(C0000R.id.hint);
        this.b = (WebView) findViewById(C0000R.id.webView);
        this.b.setWebViewClient(new k(this));
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.loadUrl(this.d + f.e().a());
        this.b.setDownloadListener(new tx(this));
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || !this.b.canGoBack()) {
            return super.onKeyDown(i, keyEvent);
        }
        this.b.goBack();
        return true;
    }
}
