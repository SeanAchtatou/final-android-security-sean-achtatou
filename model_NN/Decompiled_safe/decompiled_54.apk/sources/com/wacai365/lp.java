package com.wacai365;

import android.content.Intent;
import android.view.View;

final class lp implements View.OnClickListener {
    private /* synthetic */ SettingTradeTargetMgr a;

    lp(SettingTradeTargetMgr settingTradeTargetMgr) {
        this.a = settingTradeTargetMgr;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.a)) {
            Intent intent = new Intent(this.a, InputTradeTarget.class);
            intent.putExtra("Is_Payer", this.a.e);
            this.a.startActivityForResult(intent, 0);
        } else if (view.equals(this.a.b)) {
            this.a.finish();
        }
    }
}
