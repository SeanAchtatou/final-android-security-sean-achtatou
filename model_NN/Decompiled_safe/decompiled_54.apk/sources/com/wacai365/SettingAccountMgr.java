package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.wacai.a;
import com.wacai.b.c;
import com.wacai.b.f;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.n;
import com.wacai.b.o;
import com.wacai.data.h;
import com.wacai.e;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

public class SettingAccountMgr extends WacaiActivity {
    DialogInterface.OnClickListener a = new kd(this);
    private QueryInfo b;
    /* access modifiers changed from: private */
    public Button c;
    /* access modifiers changed from: private */
    public Button d;
    /* access modifiers changed from: private */
    public ListView e;
    /* access modifiers changed from: private */
    public qj f;
    private long g = 0;
    /* access modifiers changed from: private */
    public aar h;
    private h i;
    private ArrayList j = new ArrayList();
    private View.OnClickListener k = new ix(this);

    private ArrayList a(Cursor cursor) {
        long j2 = 0;
        if (cursor == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        while (cursor.moveToNext()) {
            ru ruVar = new ru(this);
            ruVar.a = cursor.getLong(cursor.getColumnIndex("_type"));
            ruVar.b = cursor.getString(cursor.getColumnIndex("_typename"));
            ruVar.c = j2;
            ruVar.d = j2 + cursor.getLong(cursor.getColumnIndex("_ct"));
            j2 = ruVar.d + 1;
            arrayList.add(ruVar);
        }
        cursor.close();
        return arrayList;
    }

    static /* synthetic */ void a(SettingAccountMgr settingAccountMgr) {
        long time = new Date().getTime() / 1000;
        ArrayList a2 = settingAccountMgr.a(e.c().b().rawQuery(String.format("select count(*) as _ct, _type, _typename from ( select _id, _name, _hasbalance, _flag, _moneytypeid, _typename, _type, _shortname, (_balance - _osum + _isum - _tosum + _tisum) as _sum from ( select a.id as _id, a.name as _name, a.balance as _balance, a.hasbalance as _hasbalance, a.type as _type, c.name as _typename, b.shortname as _shortname, b.flag as _flag, a.moneytype as _moneytypeid, (select ifnull(sum(t1.money), 0) from TBL_OUTGOINFO t1 where t1.isdelete=0 and t1.outgodate <= %d and  t1.outgodate >= a.balancedate and t1.accountid=a.id) _osum, (select ifnull(sum(t2.money), 0) from TBL_INCOMEINFO t2 where t2.isdelete=0 and t2.incomedate <= %d and  t2.incomedate >= a.balancedate and t2.accountid=a.id) _isum, (select ifnull(sum(t3.transferoutmoney), 0) from TBL_TRANSFERINFO t3 where t3.isdelete=0 and  t3.date <= %d and t3.date >= a.balancedate and t3.transferoutaccountid=a.id) _tosum, (select ifnull(sum(t4.transferinmoney), 0) from TBL_TRANSFERINFO t4 where t4.isdelete=0 and  t4.date <= %d and t4.date >= a.balancedate and t4.transferinaccountid=a.id) _tisum from TBL_ACCOUNTTYPE c, TBL_ACCOUNTINFO a join TBL_MONEYTYPE b on a.moneytype = b.id where a.enable=1 and a.type<>3 and a.type = c.uuid %s) order by _type) group by _type", Long.valueOf(time), Long.valueOf(time), Long.valueOf(time), Long.valueOf(time), a.a("BasicSortStyle", 0) == 0 ? " order by a.orderno ASC " : " order by a.pinyin ASC "), null));
        long time2 = new Date().getTime() / 1000;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(String.format("select _id, _name, _hasbalance, _flag, _moneytypeid, _typename, _type, _shortname, (_balance - _osum + _isum - _tosum + _tisum) as _sum from ( select a.id as _id, a.name as _name, a.balance as _balance, a.hasbalance as _hasbalance, a.type as _type, c.name as _typename, b.shortname as _shortname, b.flag as _flag, a.moneytype as _moneytypeid, (select ifnull(sum(t1.money), 0) from TBL_OUTGOINFO t1 where t1.isdelete=0 and t1.outgodate <= %d and  t1.outgodate >= a.balancedate and t1.accountid=a.id) _osum, (select ifnull(sum(t2.money), 0) from TBL_INCOMEINFO t2 where t2.isdelete=0 and t2.incomedate <= %d and  t2.incomedate >= a.balancedate and t2.accountid=a.id) _isum, (select ifnull(sum(t3.transferoutmoney), 0) from TBL_TRANSFERINFO t3 where t3.isdelete=0 and  t3.date <= %d and t3.date >= a.balancedate and t3.transferoutaccountid=a.id) _tosum, (select ifnull(sum(t4.transferinmoney), 0) from TBL_TRANSFERINFO t4 where t4.isdelete=0 and  t4.date <= %d and t4.date >= a.balancedate and t4.transferinaccountid=a.id) _tisum from TBL_ACCOUNTTYPE c, TBL_ACCOUNTINFO a join TBL_MONEYTYPE b on a.moneytype = b.id where a.enable=1 and a.type<>3 and a.type = c.uuid ", Long.valueOf(time2), Long.valueOf(time2), Long.valueOf(time2), Long.valueOf(time2)));
        if (a.a("BasicSortStyle", 0) == 0) {
            stringBuffer.append(" order by a.orderno ASC) order by _type");
        } else {
            stringBuffer.append(" order by a.pinyin ASC) order by _type");
        }
        Cursor rawQuery = e.c().b().rawQuery(stringBuffer.toString(), null);
        settingAccountMgr.startManagingCursor(rawQuery);
        if (settingAccountMgr.g == Thread.currentThread().getId()) {
            settingAccountMgr.f = new qj(settingAccountMgr, rawQuery, a2, settingAccountMgr);
            settingAccountMgr.runOnUiThread(new kb(settingAccountMgr));
        }
    }

    static /* synthetic */ void a(SettingAccountMgr settingAccountMgr, int i2) {
        aar aar;
        Object itemAtPosition = settingAccountMgr.e.getItemAtPosition(i2);
        if (itemAtPosition.getClass() != ry.class && (aar = (aar) itemAtPosition) != null) {
            long j2 = aar.a;
            if (j2 >= 0) {
                Intent intent = new Intent(settingAccountMgr, InputAccount.class);
                intent.putExtra("Record_Id", j2);
                settingAccountMgr.startActivityForResult(intent, 14);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(com.wacai.b.c, boolean):void
     arg types: [com.wacai.b.c, int]
     candidates:
      com.wacai365.ft.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(com.wacai.b.c, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void d(SettingAccountMgr settingAccountMgr) {
        settingAccountMgr.j.clear();
        ft ftVar = new ft(settingAccountMgr);
        ftVar.e(false);
        ftVar.a(new ke(settingAccountMgr));
        c cVar = new c(ftVar);
        ft.a(cVar, true);
        l lVar = new l();
        StringBuffer stringBuffer = new StringBuffer(100);
        settingAccountMgr.b.a(stringBuffer, 6);
        lVar.a(m.b(stringBuffer.toString()));
        cVar.a((o) lVar);
        f fVar = new f();
        fVar.a(true);
        fVar.a(settingAccountMgr.j);
        cVar.a((o) fVar);
        ftVar.a(cVar);
        ftVar.b(C0000R.string.txtRectificationProgress, C0000R.string.txtPreparing);
        ftVar.a(false, true);
    }

    public final void a() {
        this.e.setAdapter((ListAdapter) new qj(this, null, null, null));
        ka kaVar = new ka(this);
        this.g = kaVar.getId();
        kaVar.start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(com.wacai.b.c, boolean):void
     arg types: [com.wacai.b.c, int]
     candidates:
      com.wacai365.ft.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(com.wacai.b.c, boolean):void */
    public final void b() {
        Cursor cursor;
        int size = this.j.size();
        this.i = h.d(this.h.a);
        try {
            Cursor rawQuery = e.c().b().rawQuery("select * from TBL_ACCOUNTINFO where uuid = '" + this.i.B() + "'", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        this.i.b(rawQuery.getString(rawQuery.getColumnIndexOrThrow("name")));
                        this.i.a(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("type")));
                        this.i.b(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("moneytype")));
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            String j2 = this.i.j();
            for (int i2 = 0; i2 < size; i2++) {
                Hashtable hashtable = (Hashtable) this.j.get(i2);
                if (hashtable != null) {
                    if (j2.equals((String) hashtable.get("TAG_LABLE"))) {
                        String str = (String) hashtable.get("TAG_FIRST");
                        double doubleValue = Double.valueOf(str).doubleValue();
                        if (this.h.b == m.a(doubleValue)) {
                            runOnUiThread(new kg(this));
                            return;
                        }
                        this.i.a(m.a(doubleValue));
                        long time = new Date().getTime();
                        this.i.b(time / 1000);
                        this.i.c();
                        runOnUiThread(new ki(this, str, time));
                        ft ftVar = new ft(this);
                        c cVar = new c(ftVar);
                        ftVar.e(false);
                        l lVar = new l();
                        StringBuffer stringBuffer = new StringBuffer();
                        this.i.a(stringBuffer);
                        lVar.a(m.a(stringBuffer.toString()));
                        cVar.a((o) lVar);
                        cVar.a((o) new n());
                        ft.a(cVar, false);
                        cVar.b();
                        return;
                    } else if (this.e != null && i2 == size - 1) {
                        runOnUiThread(new kf(this));
                    }
                }
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = new QueryInfo();
        this.b.a(4);
        setContentView((int) C0000R.layout.setting_item_list);
        this.c = (Button) findViewById(C0000R.id.btnAdd);
        this.c.setOnClickListener(this.k);
        this.d = (Button) findViewById(C0000R.id.btnCancel);
        this.d.setOnClickListener(this.k);
        this.e = (ListView) findViewById(C0000R.id.IOList);
        this.e.setOnItemClickListener(new jy(this));
        a();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        a();
    }
}
