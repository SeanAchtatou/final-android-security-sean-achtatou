package com.wacai365;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.wacai.e;
import java.util.Date;

public final class tm extends SimpleCursorAdapter {
    private String[] a;
    private int[] b;

    public tm(Context context, int i, Cursor cursor, String[] strArr, int[] iArr) {
        super(context, i, cursor, strArr, iArr);
        this.b = iArr;
        this.a = strArr;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        View findViewById = view.findViewById(this.b[0]);
        if (findViewById != null) {
            String string = cursor.getString(cursor.getColumnIndexOrThrow(this.a[0]));
            if (string == null) {
                string = "";
            }
            String str = "";
            Cursor rawQuery = e.c().b().rawQuery(String.format("select name from TBL_WHITELIST where address = '%s'", string), null);
            if (rawQuery != null) {
                if (rawQuery.getCount() > 0) {
                    rawQuery.moveToNext();
                    str = rawQuery.getString(rawQuery.getColumnIndexOrThrow("name"));
                }
                rawQuery.close();
            }
            if (str != null && str.length() > 0) {
                string = str;
            }
            setViewText((TextView) findViewById, string);
        }
        View findViewById2 = view.findViewById(this.b[1]);
        if (findViewById2 != null) {
            String format = m.f.format(new Date(cursor.getLong(cursor.getColumnIndexOrThrow(this.a[1])) * 1000));
            if (format == null) {
                format = "";
            }
            setViewText((TextView) findViewById2, format);
        }
        View findViewById3 = view.findViewById(this.b[2]);
        if (findViewById3 != null) {
            String string2 = cursor.getString(cursor.getColumnIndexOrThrow(this.a[2]));
            if (string2 == null) {
                string2 = "";
            }
            setViewText((TextView) findViewById3, string2);
        }
    }
}
