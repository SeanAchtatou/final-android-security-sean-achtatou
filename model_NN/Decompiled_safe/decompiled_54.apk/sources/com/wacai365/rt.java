package com.wacai365;

import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;

final class rt implements AdapterView.OnItemClickListener {
    private /* synthetic */ DialogInterface.OnClickListener a;
    private /* synthetic */ og b;

    rt(og ogVar, DialogInterface.OnClickListener onClickListener) {
        this.b = ogVar;
        this.a = onClickListener;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.a != null) {
            this.a.onClick(this.b, i);
        }
    }
}
