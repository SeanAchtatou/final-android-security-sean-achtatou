package com.wacai365;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.widget.RemoteViews;
import java.io.File;

public final class cc {
    static final String h = (m.i + "/WacaiSMSService.apk");
    long a = 0;
    long b = 0;
    Notification c = null;
    Context d = null;
    String e = "";
    boolean f = false;
    pj g = null;
    private long i = 0;
    private PendingIntent j = null;
    private String k = "";
    private Handler l = new wu(this);

    private void a(int i2) {
        Message message = new Message();
        message.what = i2;
        this.l.sendMessage(message);
    }

    public static void a(Activity activity) {
        m.e();
        File file = new File(h);
        if (file.exists()) {
            file.delete();
        } else {
            m.a(activity, (int) C0000R.string.txtSMSServiceUninstall, (int) C0000R.string.txtDownload, (int) C0000R.string.txtCancel, new wt(activity));
        }
    }

    public static void a(Context context, String str, String str2, pj pjVar) {
        cc ccVar = new cc();
        ccVar.e = str;
        ccVar.k = str2;
        ccVar.d = context;
        ccVar.j = PendingIntent.getBroadcast(context, 0, new Intent(), 0);
        ccVar.g = pjVar;
        NotificationManager notificationManager = (NotificationManager) ccVar.d.getSystemService("notification");
        ccVar.c = new Notification(C0000R.drawable.news_notification, ccVar.d.getResources().getString(C0000R.string.txtDownloadingFile), System.currentTimeMillis());
        ccVar.c.setLatestEventInfo(ccVar.d, "", "", ccVar.j);
        ccVar.c.contentView = new RemoteViews(ccVar.d.getPackageName(), (int) C0000R.layout.notification_progress);
        notificationManager.cancel(3);
        notificationManager.notify(3, ccVar.c);
        new ws(ccVar).start();
    }

    public static boolean b(Activity activity) {
        try {
            return activity.getPackageManager().getApplicationInfo("com.WacaiService", 0) != null;
        } catch (Exception e2) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0071 A[SYNTHETIC, Splitter:B:18:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0076 A[SYNTHETIC, Splitter:B:21:0x0076] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00ef A[SYNTHETIC, Splitter:B:55:0x00ef] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00f4 A[SYNTHETIC, Splitter:B:58:0x00f4] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x010f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.io.File a(java.lang.String r14) {
        /*
            r13 = this;
            r10 = 102400(0x19000, double:5.05923E-319)
            r9 = 0
            if (r14 != 0) goto L_0x0008
            r0 = r9
        L_0x0007:
            return r0
        L_0x0008:
            java.lang.String r0 = r13.k
            com.wacai365.abw.a(r0)
            java.io.File r0 = new java.io.File
            java.lang.String r1 = r13.k
            r0.<init>(r1)
            java.lang.String r1 = "://"
            int r1 = r14.indexOf(r1)     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            java.lang.String r2 = "/"
            int r3 = r1 + 3
            int r2 = r14.indexOf(r2, r3)     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            if (r1 <= 0) goto L_0x0112
            if (r2 <= 0) goto L_0x0112
            java.lang.String r1 = r14.substring(r2)     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
        L_0x002a:
            org.apache.http.client.methods.HttpPost r2 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            r2.<init>(r1)     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            java.net.URI r1 = java.net.URI.create(r14)     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            r3 = 60000(0xea60, float:8.4078E-41)
            org.apache.http.HttpResponse r1 = com.wacai.a.e.a(r1, r2, r3)     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            org.apache.http.StatusLine r2 = r1.getStatusLine()     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            int r2 = r2.getStatusCode()     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 == r3) goto L_0x007b
            org.apache.http.HttpException r1 = new org.apache.http.HttpException     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            r3.<init>()     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            java.lang.String r4 = "getResponseCode="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            r1.<init>(r2)     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            throw r1     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
        L_0x005f:
            r1 = move-exception
            r2 = r9
            r3 = r9
        L_0x0062:
            r1.printStackTrace()     // Catch:{ all -> 0x0106 }
            r1 = 0
            r13.f = r1     // Catch:{ all -> 0x0106 }
            r0.delete()     // Catch:{ all -> 0x0106 }
            r0 = 1
            r13.a(r0)     // Catch:{ all -> 0x0106 }
            if (r2 == 0) goto L_0x0074
            r2.close()     // Catch:{ Exception -> 0x00fa }
        L_0x0074:
            if (r3 == 0) goto L_0x010f
            r3.close()     // Catch:{ Exception -> 0x00e6 }
            r0 = r9
            goto L_0x0007
        L_0x007b:
            org.apache.http.HttpEntity r2 = r1.getEntity()     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            java.io.InputStream r2 = r2.getContent()     // Catch:{ Exception -> 0x005f, all -> 0x00ea }
            java.lang.String r3 = "Content-Length"
            org.apache.http.Header r1 = r1.getFirstHeader(r3)     // Catch:{ Exception -> 0x010a, all -> 0x0101 }
            if (r1 == 0) goto L_0x00ce
            java.lang.String r1 = r1.getValue()     // Catch:{ Exception -> 0x010a, all -> 0x0101 }
            long r3 = java.lang.Long.parseLong(r1)     // Catch:{ Exception -> 0x010a, all -> 0x0101 }
        L_0x0093:
            r13.a = r3     // Catch:{ Exception -> 0x010a, all -> 0x0101 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x010a, all -> 0x0101 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x010a, all -> 0x0101 }
            r3 = 0
            r13.a(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
            r3 = 10240(0x2800, float:1.4349E-41)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
        L_0x00a2:
            int r4 = r2.read(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
            if (r4 <= 0) goto L_0x00d2
            long r5 = r13.b     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
            long r7 = (long) r4     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
            long r5 = r5 + r7
            r13.b = r5     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
            long r5 = r13.i     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
            long r7 = (long) r4     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
            long r5 = r5 + r7
            r13.i = r5     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
            r5 = 0
            r1.write(r3, r5, r4)     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
            long r4 = r13.i     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
            int r4 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r4 < 0) goto L_0x00a2
            long r4 = r13.i     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
            long r4 = r4 - r10
            r13.i = r4     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
            r4 = 0
            r13.a(r4)     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
            goto L_0x00a2
        L_0x00c8:
            r3 = move-exception
            r12 = r3
            r3 = r2
            r2 = r1
            r1 = r12
            goto L_0x0062
        L_0x00ce:
            r3 = 99999(0x1869f, double:4.9406E-319)
            goto L_0x0093
        L_0x00d2:
            r3 = 1
            r13.f = r3     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
            r3 = 1
            r13.a(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x0104 }
            r1.close()     // Catch:{ Exception -> 0x00f8 }
        L_0x00dc:
            if (r2 == 0) goto L_0x0007
            r2.close()     // Catch:{ Exception -> 0x00e3 }
            goto L_0x0007
        L_0x00e3:
            r1 = move-exception
            goto L_0x0007
        L_0x00e6:
            r0 = move-exception
            r0 = r9
            goto L_0x0007
        L_0x00ea:
            r0 = move-exception
            r1 = r9
            r2 = r9
        L_0x00ed:
            if (r1 == 0) goto L_0x00f2
            r1.close()     // Catch:{ Exception -> 0x00fd }
        L_0x00f2:
            if (r2 == 0) goto L_0x00f7
            r2.close()     // Catch:{ Exception -> 0x00ff }
        L_0x00f7:
            throw r0
        L_0x00f8:
            r1 = move-exception
            goto L_0x00dc
        L_0x00fa:
            r0 = move-exception
            goto L_0x0074
        L_0x00fd:
            r1 = move-exception
            goto L_0x00f2
        L_0x00ff:
            r1 = move-exception
            goto L_0x00f7
        L_0x0101:
            r0 = move-exception
            r1 = r9
            goto L_0x00ed
        L_0x0104:
            r0 = move-exception
            goto L_0x00ed
        L_0x0106:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x00ed
        L_0x010a:
            r1 = move-exception
            r3 = r2
            r2 = r9
            goto L_0x0062
        L_0x010f:
            r0 = r9
            goto L_0x0007
        L_0x0112:
            r1 = r14
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.cc.a(java.lang.String):java.io.File");
    }
}
