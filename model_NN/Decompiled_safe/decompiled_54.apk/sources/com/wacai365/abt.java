package com.wacai365;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.wacai.a;
import com.wacai.a.e;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.o;
import java.util.Date;

public final class abt {
    private static boolean a;
    /* access modifiers changed from: private */
    public static c b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(com.wacai.b.c, boolean):void
     arg types: [com.wacai.b.c, int]
     candidates:
      com.wacai365.ft.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(com.wacai.b.c, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    public static fy a(Activity activity) {
        ft ftVar = new ft(activity);
        c cVar = new c(ftVar);
        ft.a(cVar, true);
        ftVar.a(cVar);
        ftVar.b(C0000R.string.txtUploadProgress, C0000R.string.txtPreparing);
        ftVar.a(false, true);
        return ftVar;
    }

    static sd a(Activity activity, int i, o oVar, DialogInterface.OnClickListener onClickListener) {
        sd sdVar = new sd();
        a(true);
        if (activity != null) {
            if ((i & 1) != 0 && a.a("PropAcceptCopyright", 0) == 0) {
                sdVar.a(new dp(activity, sdVar));
            }
            if ((i & 2) != 0 && !MyApp.a && b.m().f()) {
                sdVar.a(new dn(activity, sdVar));
            }
            if (oVar != null) {
                sdVar.a(oVar);
            }
            if ((i & 4) != 0) {
                sdVar.a(new de());
            }
            if ((i & 8) != 0) {
                sdVar.a(new df(activity));
            }
            if ((i & 16) != 0) {
                sdVar.a(new dg(activity, sdVar));
            }
            if ((i & 32) != 0) {
                sdVar.a(new dh(activity, sdVar, onClickListener));
            }
            if ((i & 128) != 0) {
                sdVar.a(new di());
            }
            if (!((i & 64) == 0 || i == -1)) {
                sdVar.a(new dj(activity));
            }
        }
        return sdVar;
    }

    static /* synthetic */ void a(Activity activity, sd sdVar, DialogInterface.OnClickListener onClickListener) {
        long j;
        long a2 = a.a("DataBackupIntervalType", 1);
        if (-1 != a2) {
            long time = new Date().getTime() / 1000;
            long a3 = a.a("DataBackupInterval", 0);
            boolean z = a.a("IsAutoBackup", 0) == 1;
            long c = c();
            if (c != 0) {
                if (a3 == 0) {
                    a3 = c;
                }
                switch ((int) a2) {
                    case 0:
                        j = 1;
                        break;
                    case 1:
                        j = 3;
                        break;
                    case 2:
                        j = 7;
                        break;
                    default:
                        j = 30;
                        break;
                }
                long j2 = j * 86400;
                long j3 = time - a3;
                if (time > a3 && time - a3 >= j2 && !z) {
                    sdVar.d();
                    Resources resources = activity.getResources();
                    activity.runOnUiThread(new qv(activity, resources.getText(C0000R.string.txtAlertTitleInfo).toString(), resources.getString(C0000R.string.txtDataBackupExpirationFormat, Long.valueOf(j3 / 86400)), sdVar, onClickListener));
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(com.wacai.b.c, boolean):void
     arg types: [com.wacai.b.c, int]
     candidates:
      com.wacai365.ft.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(com.wacai.b.c, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.AlertCenter.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.wacai365.AlertCenter.a(com.wacai365.AlertCenter, com.wacai365.fy):com.wacai365.fy
      com.wacai365.AlertCenter.a(com.wacai365.AlertCenter, int):void
      com.wacai365.AlertCenter.a(boolean, int):void
      com.wacai365.AlertCenter.a(java.lang.String, boolean):void */
    public static void a(Activity activity, boolean z) {
        if (activity != null && b == null) {
            a(false);
            boolean z2 = a.a("IsAutoBackup", 0) > 0;
            boolean z3 = a.a("AutoBackupCondition", 0) > 0;
            boolean a2 = e.a();
            long b2 = b();
            boolean z4 = b2 > 0;
            boolean c = c(activity);
            long a3 = a.a("DataBackupInterval", 0);
            String string = a3 == 0 ? activity.getString(C0000R.string.AutoBackupFirstTime, new Object[]{Long.valueOf(b2)}) : activity.getString(C0000R.string.autoBackupFail, new Object[]{Long.valueOf(b2), m.a.format(Long.valueOf(a3 * 1000))});
            if (!z2 || !a2 || !z4) {
                if (z2 && !a2 && z4) {
                    AlertCenter.a(string, true);
                    if (z && !a()) {
                        MyApp.a();
                    }
                } else if (z) {
                    MyApp.a();
                }
            } else if (z3 || c) {
                c cVar = new c(new qu(string, z));
                b = cVar;
                ft.a(cVar, false);
                b.b();
            } else {
                AlertCenter.a(string, true);
            }
        }
    }

    private static synchronized void a(boolean z) {
        synchronized (abt.class) {
            a = z;
        }
    }

    static synchronized boolean a() {
        boolean z;
        synchronized (abt.class) {
            z = a;
        }
        return z;
    }

    public static long b() {
        Cursor cursor;
        long j = 0;
        try {
            Cursor rawQuery = com.wacai.e.c().b().rawQuery("SELECT SUM(_COUNT) FROM (SELECT COUNT(UPDATESTATUS) AS _COUNT FROM TBL_OUTGOINFO WHERE UPDATESTATUS = 0 AND SCHEDULEOUTGOID = 0 AND PAYBACKID = 0 UNION ALL SELECT COUNT(UPDATESTATUS) AS _COUNT FROM TBL_INCOMEINFO WHERE UPDATESTATUS = 0 AND SCHEDULEINCOMEID = 0 AND PAYBACKID = 0  UNION ALL SELECT COUNT(UPDATESTATUS) AS _COUNT FROM TBL_TRANSFERINFO WHERE UPDATESTATUS = 0 AND TYPE = 0 UNION ALL SELECT COUNT(UPDATESTATUS) AS _COUNT FROM TBL_LOAN WHERE UPDATESTATUS = 0 UNION ALL SELECT COUNT(UPDATESTATUS) AS _COUNT FROM TBL_PAYBACK WHERE UPDATESTATUS = 0 UNION ALL SELECT COUNT(UPDATESTATUS) AS _COUNT FROM TBL_MYNOTE WHERE UPDATESTATUS = 0 UNION ALL SELECT COUNT(UPDATESTATUS) AS _COUNT FROM TBL_SHORTCUTSINFO WHERE UPDATESTATUS = 0 UNION ALL SELECT COUNT(UPDATESTATUS) AS _COUNT FROM TBL_TRADETARGET WHERE UPDATESTATUS = 0 UNION ALL SELECT COUNT(UPDATESTATUS) AS _COUNT FROM TBL_SCHEDULEOUTGOINFO WHERE UPDATESTATUS = 0 UNION ALL SELECT COUNT(UPDATESTATUS) AS _COUNT FROM TBL_SCHEDULEINCOMEINFO WHERE UPDATESTATUS = 0)", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        j = rawQuery.getLong(0);
                    }
                } catch (Throwable th) {
                    th = th;
                    cursor = rawQuery;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return j;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(com.wacai.b.c, boolean):void
     arg types: [com.wacai.b.c, int]
     candidates:
      com.wacai365.ft.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(com.wacai.b.c, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.AlertCenter.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.wacai365.AlertCenter.a(com.wacai365.AlertCenter, com.wacai365.fy):com.wacai365.fy
      com.wacai365.AlertCenter.a(com.wacai365.AlertCenter, int):void
      com.wacai365.AlertCenter.a(boolean, int):void
      com.wacai365.AlertCenter.a(java.lang.String, boolean):void */
    public static void b(Activity activity) {
        if (activity != null) {
            boolean z = a.a("IsAutoBackup", 0) > 0;
            boolean z2 = a.a("AutoBackupCondition", 0) > 0;
            boolean c = c(activity);
            boolean a2 = e.a();
            long b2 = b();
            boolean z3 = b2 > 0;
            long a3 = a.a("DataBackupInterval", 0);
            String string = a3 == 0 ? activity.getString(C0000R.string.AutoBackupFirstTime, new Object[]{Long.valueOf(b2)}) : activity.getString(C0000R.string.autoBackupFail, new Object[]{Long.valueOf(b2), m.a.format(Long.valueOf(a3 * 1000))});
            if (!z || !a2 || !z3) {
                if (z && !a2 && z3) {
                    AlertCenter.a(string, true);
                }
            } else if (z2 || c) {
                ft ftVar = new ft(activity);
                ftVar.a(true);
                ftVar.a(new qw(string, activity));
                ftVar.b(false);
                ftVar.e(false);
                ftVar.a(new qt(activity, string));
                c cVar = new c(ftVar);
                ft.a(cVar, false);
                ftVar.a(cVar);
                ftVar.b(C0000R.string.txtUploadProgress, C0000R.string.txtPreparing);
                ftVar.a(false, true);
                return;
            } else {
                AlertCenter.a(string, true);
            }
            activity.finish();
        }
    }

    private static long c() {
        Cursor cursor;
        long j = 0;
        try {
            Cursor rawQuery = com.wacai.e.c().b().rawQuery("SELECT MIN(_DATE) FROM (SELECT MIN(OUTGODATE) AS _DATE FROM TBL_OUTGOINFO WHERE UPDATESTATUS = 0 AND SCHEDULEOUTGOID = 0 UNION SELECT MIN(INCOMEDATE) AS _DATE FROM TBL_INCOMEINFO WHERE UPDATESTATUS = 0 AND SCHEDULEINCOMEID = 0 UNION SELECT MIN(DATE) AS _DATE FROM TBL_TRANSFERINFO WHERE UPDATESTATUS = 0 AND TYPE = 0 UNION SELECT MIN(DATE) AS _DATE FROM TBL_LOAN WHERE UPDATESTATUS = 0 UNION SELECT MIN(DATE) AS _DATE FROM TBL_PAYBACK WHERE UPDATESTATUS = 0 UNION SELECT MIN(CREATEDATE) AS _DATE FROM TBL_MYNOTE WHERE UPDATESTATUS = 0 )", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        j = rawQuery.getLong(0);
                    }
                } catch (Throwable th) {
                    th = th;
                    cursor = rawQuery;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return j;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
    }

    public static boolean c(Activity activity) {
        NetworkInfo[] allNetworkInfo = ((ConnectivityManager) activity.getSystemService("connectivity")).getAllNetworkInfo();
        if (allNetworkInfo != null && allNetworkInfo.length > 0) {
            for (NetworkInfo networkInfo : allNetworkInfo) {
                if (networkInfo.isConnected() && networkInfo.getType() == 1) {
                    return true;
                }
            }
        }
        return false;
    }
}
