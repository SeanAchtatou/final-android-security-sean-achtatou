package com.wacai365;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.TextView;

public class AutoAdjustTextView extends TextView {
    private TextPaint a;
    private Resources b = getContext().getResources();
    private float c = this.b.getDimension(C0000R.dimen.TextView_MinSize);
    private float d = getTextSize();
    private float e;
    private float f;

    public AutoAdjustTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    private void a(String str, int i) {
        if (i > 0 && str != null && str.length() > 0 && this.d > this.c) {
            if (this.a == null) {
                this.a = new TextPaint(getPaint());
            }
            int compoundPaddingLeft = (i - getCompoundPaddingLeft()) - getCompoundPaddingRight();
            if (isPaddingOffsetRequired()) {
                compoundPaddingLeft = (int) (((float) compoundPaddingLeft) - ((this.e + this.f) * 2.0f));
            }
            float f2 = this.d;
            while (f2 >= this.c) {
                this.a.setTextSize(f2);
                if (this.a.measureText(str) <= ((float) compoundPaddingLeft)) {
                    break;
                }
                f2 -= 1.0f;
            }
            setTextSize(f2 / this.b.getDisplayMetrics().scaledDensity);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        if (i != i3) {
            a(getText().toString(), i);
        }
    }

    /* access modifiers changed from: protected */
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.onTextChanged(charSequence, i, i2, i3);
        a(charSequence.toString(), getWidth());
    }

    public void setShadowLayer(float f2, float f3, float f4, int i) {
        super.setShadowLayer(f2, f3, f4, i);
        this.e = f2;
        this.f = f3;
    }
}
