package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;

final class gn implements DialogInterface.OnClickListener {
    private /* synthetic */ MyShortcuts a;

    gn(MyShortcuts myShortcuts) {
        this.a = myShortcuts;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        MyShortcuts myShortcuts = this.a;
        switch (i) {
            case -3:
                Intent intent = new Intent(myShortcuts, DataBackupSetting.class);
                intent.putExtra("LaunchedByApplication", 1);
                myShortcuts.startActivityForResult(intent, 0);
                break;
            case -1:
                fy unused = this.a.i = abt.a(myShortcuts);
                break;
        }
        this.a.h.e();
    }
}
