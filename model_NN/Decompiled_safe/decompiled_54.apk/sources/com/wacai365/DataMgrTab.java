package com.wacai365;

import android.content.Intent;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.o;
import com.wacai.b.u;
import java.util.ArrayList;

public class DataMgrTab extends WacaiActivity {
    ListView a;
    private fy b = null;
    private AdapterView.OnItemClickListener c = new lx(this);

    /* access modifiers changed from: private */
    public void a() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new fr(C0000R.string.txtUploadTitle, C0000R.string.txtUploadTitleDetail, "com.wacai365.DataSynchronize"));
        arrayList.add(new fr(C0000R.string.txtDataManager, C0000R.string.txtDataManagerDetail, "com.wacai365.DataSynchronize"));
        arrayList.add(new fr(C0000R.string.syncSetting, C0000R.string.syncSettingDetail, "com.wacai365.DataBackupSetting"));
        arrayList.add(new fr(C0000R.string.txtRegisterNewUser, C0000R.string.txtRegisterNewUserDetail, "com.wacai365.AccountRegister"));
        if (!b.m().p()) {
            arrayList.add(new fr(C0000R.string.txtSetUser, C0000R.string.txtSetUserDetail, "com.wacai365.AccountConfig"));
        }
        arrayList.add(new fr(C0000R.string.txtLocalDataClear, C0000R.string.txtLocalDataClearDetail, "com.wacai365.LocalDataClear"));
        arrayList.add(new fr(C0000R.string.txtHardwareReset, C0000R.string.txtHardwareResetDetail, "com.wacai365.DataManager"));
        arrayList.add(new fr(C0000R.string.weibosettings, C0000R.string.weibosettingsDetails, "com.wacai365.WeiboSettingList"));
        this.a.setAdapter((ListAdapter) new xw(this, (int) C0000R.layout.list_item_line2, arrayList));
        this.a.setOnItemClickListener(this.c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void a(DataMgrTab dataMgrTab) {
        ft ftVar = new ft(dataMgrTab);
        dataMgrTab.b = ftVar;
        c cVar = new c(ftVar);
        cVar.a((o) new u());
        cVar.a((o) new lw(dataMgrTab));
        ftVar.a(cVar);
        ftVar.b(false);
        ftVar.c(false);
        ftVar.a(new lv(dataMgrTab));
        ftVar.b(C0000R.string.txtRestore, C0000R.string.txtRestoring);
        ftVar.a(true, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(com.wacai.b.c, boolean):void
     arg types: [com.wacai.b.c, int]
     candidates:
      com.wacai365.ft.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(com.wacai.b.c, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void b(DataMgrTab dataMgrTab) {
        ft ftVar = new ft(dataMgrTab);
        ftVar.a(true);
        dataMgrTab.b = ftVar;
        c cVar = new c(ftVar);
        ft.a(cVar, true);
        ftVar.a(cVar);
        ftVar.b(C0000R.string.txtUploadProgress, C0000R.string.txtPreparing);
        ftVar.a(false, true);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (this.b != null) {
            this.b.a(i, i2, intent);
        }
        a();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.setting_item_list_withoutbutton);
        this.a = (ListView) findViewById(C0000R.id.IOList);
        a();
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(new ly(this));
    }
}
