package com.wacai365;

import android.app.NotificationManager;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

final class wu extends Handler {
    private /* synthetic */ cc a;

    wu(cc ccVar) {
        this.a = ccVar;
    }

    public final void handleMessage(Message message) {
        if (!Thread.currentThread().isInterrupted()) {
            switch (message.what) {
                case 0:
                    this.a.c.contentView.setProgressBar(C0000R.id.progressbar_download, 100, (int) ((this.a.b * 100) / this.a.a), false);
                    ((NotificationManager) this.a.d.getSystemService("notification")).notify(3, this.a.c);
                    break;
                case 1:
                    ((NotificationManager) this.a.d.getSystemService("notification")).cancel(3);
                    Toast.makeText(this.a.d, this.a.f ? this.a.d.getResources().getString(C0000R.string.txtDownloadingFileFinish) : this.a.d.getResources().getString(C0000R.string.txtDownloadingFileFailed), 1).show();
                    break;
            }
        }
        super.handleMessage(message);
    }
}
