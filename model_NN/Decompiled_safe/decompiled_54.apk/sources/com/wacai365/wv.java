package com.wacai365;

import android.content.Intent;
import android.view.View;

final class wv implements View.OnClickListener {
    private /* synthetic */ ChooseOutgoType a;

    wv(ChooseOutgoType chooseOutgoType) {
        this.a = chooseOutgoType;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.i)) {
            this.a.finish();
        } else if (view.equals(this.a.h)) {
            ChooseOutgoType chooseOutgoType = this.a;
            Intent intent = new Intent(chooseOutgoType, SettingMainTypeMgr.class);
            intent.putExtra("LaunchedByApplication", 1);
            chooseOutgoType.startActivityForResult(intent, 33);
        }
    }
}
