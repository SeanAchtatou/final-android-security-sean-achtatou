package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.f;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.o;
import com.wacai.e;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

public class StatByProperty extends WacaiActivity implements tt {
    private RadioGroup a;
    /* access modifiers changed from: private */
    public Button b;
    /* access modifiers changed from: private */
    public ListView c;
    private QueryInfo d;
    /* access modifiers changed from: private */
    public ArrayList e = new ArrayList();
    private cb f = new cb(this);
    private fy g = null;
    private View.OnClickListener h = new tn(this);
    private RadioGroup.OnCheckedChangeListener i = new to(this);

    private long a() {
        Cursor cursor;
        long j = 0;
        long j2 = b.m().j();
        long time = new Date().getTime() / 1000;
        try {
            Cursor rawQuery = e.c().b().rawQuery(String.format("select sum(_balance - _osum + _isum - _tosum + _tisum) from (select a.balance as _balance,  (select ifnull(sum(t1.money), 0) from TBL_OUTGOINFO t1 where t1.isdelete=0 and t1.outgodate<=%d and  t1.outgodate >= a.balancedate and t1.accountid=a.id) _osum,  (select ifnull(sum(t2.money), 0) from TBL_INCOMEINFO t2 where t2.isdelete=0 and t2.incomedate<=%d and  t2.incomedate >= a.balancedate and t2.accountid=a.id) _isum,  (select ifnull(sum(t3.transferoutmoney), 0) from TBL_TRANSFERINFO t3 where t3.isdelete=0 and  t3.date<=%d and t3.date >= a.balancedate and t3.transferoutaccountid=a.id) _tosum,  (select ifnull(sum(t4.transferinmoney), 0) from TBL_TRANSFERINFO t4 where t4.isdelete=0 and  t4.date<=%d and t4.date >= a.balancedate and t4.transferinaccountid=a.id) _tisum  from TBL_ACCOUNTINFO a where a.enable=1 and a.hasbalance=1 and a.type<>3 and a.moneytype=%d)", Long.valueOf(time), Long.valueOf(time), Long.valueOf(time), Long.valueOf(time), Long.valueOf(j2)), null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        j = rawQuery.getLong(0);
                    }
                } catch (Throwable th) {
                    th = th;
                    cursor = rawQuery;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            a(C0000R.string.txtBalanceSheetBalanceTitle, j);
            return j;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
    }

    private void a(int i2, long j) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("TAG_LABLE", getResources().getString(i2));
        hashtable.put("TAG_FIRST", m.b(j));
        this.e.add(hashtable);
    }

    private long b() {
        Cursor cursor;
        long time = new Date().getTime() / 1000;
        try {
            Cursor rawQuery = e.c().b().rawQuery(String.format("select (_tisum - _tosum) as _sum from (select  (select ifnull(sum(t3.transferoutmoney), 0) from TBL_TRANSFERINFO t3 where t3.isdelete=0 and  t3.type<>0 and t3.transferoutaccountid=a.id and t3.date<=%d and t3.date >= a.balancedate) _tosum,  (select ifnull(sum(t4.transferinmoney), 0) from TBL_TRANSFERINFO t4 where t4.isdelete=0 and  t4.type<>0 and t4.transferinaccountid=a.id and t4.date<=%d and t4.date >= a.balancedate ) _tisum  from TBL_ACCOUNTINFO a where a.enable=1 and a.type=3 and a.moneytype=%d order by a.orderno ASC)", Long.valueOf(time), Long.valueOf(time), Long.valueOf(b.m().j())), null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        long j = 0;
                        long j2 = 0;
                        do {
                            long j3 = rawQuery.getLong(0);
                            if (j3 > 0) {
                                j2 += j3;
                            } else {
                                j += j3;
                            }
                        } while (rawQuery.moveToNext());
                        a(C0000R.string.txtBalanceSheetClaimsTitle, j2);
                        a(C0000R.string.txtBalanceSheetDebtTitel, Math.abs(j));
                        long j4 = j + j2;
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        return j4;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return 0;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* access modifiers changed from: private */
    public void c() {
        this.e.clear();
        a(C0000R.string.txtBalanceSheetSumTitel, a() + b());
        runOnUiThread(this.f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void d(StatByProperty statByProperty) {
        statByProperty.e.clear();
        statByProperty.runOnUiThread(statByProperty.f);
        ft ftVar = new ft(statByProperty);
        ftVar.e(false);
        ftVar.a((tt) statByProperty);
        ftVar.b(C0000R.string.networkProgress, C0000R.string.txtTransformData);
        statByProperty.g = ftVar;
        c cVar = new c(ftVar);
        is.a(cVar);
        is.c(cVar, false);
        l lVar = new l();
        StringBuffer stringBuffer = new StringBuffer(100);
        statByProperty.d.a(stringBuffer, 8);
        lVar.a(m.b(stringBuffer.toString()));
        cVar.a((o) lVar);
        f fVar = new f();
        fVar.a(true);
        fVar.a(statByProperty.e);
        cVar.a((o) fVar);
        ftVar.a(cVar);
        ftVar.a(false, true);
    }

    public final void a(int i2) {
        double d2;
        if (this.c != null) {
            if (this.e != null) {
                ArrayList arrayList = new ArrayList(this.e);
                this.e.clear();
                double d3 = 0.0d;
                int i3 = 0;
                while (true) {
                    int i4 = i3;
                    d2 = d3;
                    if (i4 >= arrayList.size()) {
                        break;
                    }
                    Hashtable hashtable = (Hashtable) arrayList.get(i4);
                    String str = (String) hashtable.get("TAG_LABLE");
                    if (str.equals("asset")) {
                        hashtable.put("TAG_LABLE", getResources().getString(C0000R.string.txtBalanceSheetBalanceTitle));
                        d2 += Double.valueOf((String) hashtable.get("TAG_FIRST")).doubleValue();
                        this.e.add(hashtable);
                    } else if (str.equals("credit")) {
                        hashtable.put("TAG_LABLE", getResources().getString(C0000R.string.txtBalanceSheetClaimsTitle));
                        d2 += Double.valueOf((String) hashtable.get("TAG_FIRST")).doubleValue();
                        this.e.add(hashtable);
                    } else if (str.equals("debt")) {
                        hashtable.put("TAG_LABLE", getResources().getString(C0000R.string.txtBalanceSheetDebtTitel));
                        d2 -= Double.valueOf((String) hashtable.get("TAG_FIRST")).doubleValue();
                        this.e.add(hashtable);
                    }
                    d3 = d2;
                    i3 = i4 + 1;
                }
                Hashtable hashtable2 = new Hashtable();
                hashtable2.put("TAG_LABLE", getResources().getString(C0000R.string.txtBalanceSheetSumTitel));
                hashtable2.put("TAG_FIRST", m.a(d2, 2));
                this.e.add(hashtable2);
            }
            runOnUiThread(this.f);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1) {
            if (i2 == 14) {
                c();
            } else if (this.g != null) {
                this.g.a(i2, i3, intent);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.d = new QueryInfo();
        this.d.a(4);
        setContentView((int) C0000R.layout.stat_table_property);
        this.a = (RadioGroup) findViewById(C0000R.id.rgQueryChoice);
        this.a.setOnCheckedChangeListener(this.i);
        this.b = (Button) findViewById(C0000R.id.btnCancel);
        this.b.setOnClickListener(this.h);
        this.c = (ListView) findViewById(C0000R.id.QueryList);
        this.c.setFocusable(false);
        this.c.setAdapter((ListAdapter) new bz(this, this, C0000R.layout.list_item_2));
        c();
    }
}
