package com.wacai365;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

final class fv implements View.OnClickListener {
    final /* synthetic */ LocalDataClear a;

    fv(LocalDataClear localDataClear) {
        this.a = localDataClear;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener, boolean):void
     arg types: [com.wacai365.LocalDataClear, ?, int, ?[OBJECT, ARRAY], int]
     candidates:
      com.wacai365.m.a(android.content.Context, android.view.animation.Animation, int, android.view.View, int):android.view.animation.Animation
      com.wacai365.m.a(android.view.animation.Animation, int, android.view.View, int, int):android.view.animation.Animation
      com.wacai365.m.a(android.content.Context, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, boolean, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener, boolean):void */
    public final void onClick(View view) {
        if (view.equals(this.a.e)) {
            if ((((this.a.j - this.a.i) * 12) + this.a.l) - this.a.k < 0) {
                m.a((Context) this.a, (int) C0000R.string.timeIlegalClear, true, (DialogInterface.OnClickListener) null, false);
            } else {
                m.a(this.a, (int) C0000R.string.txtClearPrompt, this.a.p);
            }
        } else if (view.equals(this.a.f)) {
            this.a.finish();
        } else if (view.equals(this.a.a)) {
            new ut(this.a, this.a, new zo(this), this.a.m, 1).show();
        } else if (view.equals(this.a.c)) {
            new ut(this.a, this.a, new zm(this), this.a.n, 1).show();
        }
    }
}
