package com.wacai365;

import android.content.DialogInterface;
import com.wacai.data.x;
import java.util.ArrayList;

final class ai implements DialogInterface.OnClickListener {
    private /* synthetic */ wc a;

    ai(wc wcVar) {
        this.a = wcVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (!(this.a.b == null || this.a.b.b == null || this.a.b.b.length <= 0)) {
            int length = this.a.b.b.length;
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < length; i2++) {
                if (this.a.b.b[i2]) {
                    arrayList.add(m.a(this.a.x, this.a.b.a[i2]));
                }
            }
            int size = arrayList.size();
            if (1 == size) {
                ((x) arrayList.get(0)).b(this.a.x.o());
                this.a.x.a(arrayList);
                m.a(this.a.x.s(), this.a.r);
            } else if (1 < size) {
                m.a(this.a.o, arrayList, this.a.x.o());
            } else {
                this.a.x.a(arrayList);
                this.a.r.setText("");
            }
        }
        dialogInterface.dismiss();
    }
}
