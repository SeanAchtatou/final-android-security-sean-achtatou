package com.wacai365;

import android.text.Editable;
import android.text.TextWatcher;

final class kr implements TextWatcher {
    private /* synthetic */ ChooseTarget a;

    kr(ChooseTarget chooseTarget) {
        this.a = chooseTarget;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        ChooseTarget.e(this.a);
    }
}
