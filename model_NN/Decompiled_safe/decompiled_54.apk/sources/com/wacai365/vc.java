package com.wacai365;

import android.view.ContextMenu;
import android.view.View;

final class vc implements View.OnCreateContextMenuListener {
    private /* synthetic */ SettingMainTypeMgr a;

    vc(SettingMainTypeMgr settingMainTypeMgr) {
        this.a = settingMainTypeMgr;
    }

    public final void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.clear();
        this.a.getMenuInflater().inflate(C0000R.menu.setting_list_edit_delete, contextMenu);
    }
}
