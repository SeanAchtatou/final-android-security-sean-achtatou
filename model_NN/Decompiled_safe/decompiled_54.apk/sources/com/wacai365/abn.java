package com.wacai365;

import android.os.AsyncTask;
import android.widget.ExpandableListView;

final class abn extends AsyncTask {
    private /* synthetic */ ChooseOutgoType a;

    abn(ChooseOutgoType chooseOutgoType) {
        this.a = chooseOutgoType;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        ChooseOutgoType.j(this.a);
        return true;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        ChooseOutgoType.k(this.a);
        this.a.c.setVisibility(0);
        super.onPostExecute((Boolean) obj);
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        if (this.a.e != null) {
            this.a.e.setVisibility(8);
        }
        if (this.a.c == null) {
            ExpandableListView unused = this.a.c = (ExpandableListView) this.a.b.findViewById(C0000R.id.elOutgoTypes);
        }
        this.a.c.setVisibility(8);
        super.onPreExecute();
    }
}
