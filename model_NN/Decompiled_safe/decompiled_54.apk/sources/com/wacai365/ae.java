package com.wacai365;

import android.content.DialogInterface;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;
import com.wacai.data.aa;
import com.wacai.e;

final class ae implements DialogInterface.OnClickListener {
    private /* synthetic */ TextView a;
    private /* synthetic */ TextView b;
    private /* synthetic */ WhiteListMgr c;

    ae(WhiteListMgr whiteListMgr, TextView textView, TextView textView2) {
        this.c = whiteListMgr;
        this.a = textView;
        this.b = textView2;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String format;
        if (this.a.length() > 0) {
            String obj = this.b.getText().toString();
            String obj2 = this.a.getText().toString();
            if (-1 == this.c.e) {
                format = String.format("insert into TBL_WHITELIST (name, address) values ('%s', '%s')", aa.e(obj), aa.e(obj2));
            } else {
                format = String.format("update TBL_WHITELIST set name = '%s', address = '%s' where id = %d", aa.e(obj), aa.e(obj2), Integer.valueOf(this.c.e));
            }
            e.c().b().execSQL(format);
            this.c.removeDialog(8);
            this.c.a();
            return;
        }
        m.a(this.c, (Animation) null, 0, (View) null, (int) C0000R.string.txtInputAddressTips);
    }
}
