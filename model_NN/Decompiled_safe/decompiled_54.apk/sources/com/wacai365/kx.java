package com.wacai365;

import android.view.View;
import com.wacai.a.e;
import com.wacai.b.c;

final class kx implements View.OnClickListener {
    final /* synthetic */ DataBackupSetting a;

    kx(DataBackupSetting dataBackupSetting) {
        this.a = dataBackupSetting;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(com.wacai.b.c, boolean):void
     arg types: [com.wacai.b.c, int]
     candidates:
      com.wacai365.ft.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(com.wacai.b.c, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    public final void onClick(View view) {
        if (!this.a.l.isChecked() || ((!this.a.f.isChecked() || !e.a()) && !abt.c(this.a))) {
            DataBackupSetting.h(this.a);
            abt.b(this.a);
            return;
        }
        ft ftVar = new ft(this.a);
        ftVar.a(true);
        ftVar.a(new uo(this));
        ftVar.e(false);
        ftVar.a(new ur(this));
        c cVar = new c(ftVar);
        ft.a(cVar, true);
        ftVar.a(cVar);
        ftVar.b(C0000R.string.txtUploadProgress, C0000R.string.txtPreparing);
        ftVar.a(false, true);
    }
}
