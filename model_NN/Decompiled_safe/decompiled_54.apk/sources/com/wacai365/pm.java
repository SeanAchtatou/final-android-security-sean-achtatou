package com.wacai365;

import android.content.Intent;
import android.view.View;

final class pm implements View.OnClickListener {
    private /* synthetic */ MyNote a;

    pm(MyNote myNote) {
        this.a = myNote;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.a)) {
            Intent intent = new Intent(this.a, InputNote.class);
            intent.putExtra("LaunchedByApplication", 1);
            this.a.startActivityForResult(intent, 0);
        } else if (view.equals(this.a.b)) {
            this.a.finish();
        }
    }
}
