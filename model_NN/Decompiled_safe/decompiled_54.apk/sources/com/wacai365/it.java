package com.wacai365;

import android.content.Intent;
import android.widget.CompoundButton;

final class it implements CompoundButton.OnCheckedChangeListener {
    private /* synthetic */ InputTrade a;

    it(InputTrade inputTrade) {
        this.a = inputTrade;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.InputTrade.a(com.wacai365.InputTrade, boolean):boolean
     arg types: [com.wacai365.InputTrade, int]
     candidates:
      com.wacai365.InputTrade.a(com.wacai365.InputTrade, int):int
      com.wacai365.InputTrade.a(com.wacai365.InputTrade, com.wacai365.fy):com.wacai365.fy
      com.wacai365.InputTrade.a(com.wacai365.InputTrade, boolean):boolean */
    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        boolean z2;
        if (!compoundButton.equals(this.a.v) || !z) {
            if (compoundButton.equals(this.a.c) && z) {
                int unused = this.a.k = 0;
            } else if (compoundButton.equals(this.a.b) && z) {
                int unused2 = this.a.k = 1;
            } else if (compoundButton.equals(this.a.d) && z) {
                int unused3 = this.a.k = 2;
            } else if (compoundButton.equals(this.a.e) && z) {
                int unused4 = this.a.k = 3;
            }
            this.a.a(false);
            return;
        }
        boolean unused5 = this.a.w = true;
        InputTrade inputTrade = this.a;
        ll[] b = aaj.a().b();
        int length = b.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z2 = false;
                break;
            } else if (b[i].a()) {
                z2 = true;
                break;
            } else {
                i++;
            }
        }
        if (!z2) {
            inputTrade.startActivityForResult(new Intent(inputTrade, WeiboSettingList.class), 31);
        }
    }
}
