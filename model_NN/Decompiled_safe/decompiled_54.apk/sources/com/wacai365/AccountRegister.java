package com.wacai365;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.CheckBox;
import android.widget.EditText;
import com.wacai.a;
import com.wacai.a.f;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.g;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.o;

public class AccountRegister extends WacaiActivity implements tt {
    DialogInterface.OnClickListener a = new xp(this);
    DialogInterface.OnClickListener b = new xu(this);
    private EditText c;
    private EditText d;
    private EditText e;
    private EditText f;
    private CheckBox g;
    private Animation h;
    private boolean i = false;
    private View.OnClickListener j = new xq(this);

    /* access modifiers changed from: private */
    public void a() {
        b m = b.m();
        m.a(this.c.getText().toString().trim());
        m.b(f.a(this.d.getText().toString().trim()));
        m.c("");
        m.d("");
        m.l();
        a.b("MultiPhoneAccount", this.g.isChecked() ? 1 : 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void a(AccountRegister accountRegister) {
        String obj = accountRegister.c.getText().toString();
        String obj2 = accountRegister.d.getText().toString();
        String obj3 = accountRegister.e.getText().toString();
        String obj4 = accountRegister.f.getText().toString();
        if (obj4 == null || obj4.trim().length() <= 0) {
            accountRegister.f.setText("");
            accountRegister.h = m.a(accountRegister, accountRegister.h, (int) C0000R.anim.shake, accountRegister.f, (int) C0000R.string.txtEmptyEmail);
        } else if (!m.a(obj4)) {
            accountRegister.h = m.a(accountRegister, accountRegister.h, (int) C0000R.anim.shake, accountRegister.f, (int) C0000R.string.txtInvalidEmail);
        } else if (obj == null || obj.length() <= 0) {
            accountRegister.c.setText("");
            accountRegister.h = m.a(accountRegister, accountRegister.h, (int) C0000R.anim.shake, accountRegister.c, (int) C0000R.string.txtEmptyUserName);
        } else if (!m.b(obj)) {
            accountRegister.h = m.a(accountRegister, accountRegister.h, (int) C0000R.anim.shake, accountRegister.c, (int) C0000R.string.invalidUserName);
        } else if (obj2 == null || obj2.length() <= 0) {
            accountRegister.d.setText("");
            accountRegister.h = m.a(accountRegister, accountRegister.h, (int) C0000R.anim.shake, accountRegister.d, (int) C0000R.string.txtEmptyPassword);
        } else {
            if (!m.c(obj2)) {
                accountRegister.h = m.a(accountRegister, accountRegister.h, (int) C0000R.anim.shake, accountRegister.d, (int) C0000R.string.invalidPassword);
            }
            if (obj2.compareTo(obj3) != 0) {
                accountRegister.h = m.a(accountRegister, accountRegister.h, (int) C0000R.anim.shake, accountRegister.e, (int) C0000R.string.txtConfimPassword);
                return;
            }
            ft ftVar = new ft(accountRegister);
            ftVar.a((tt) accountRegister);
            ftVar.b(accountRegister.i);
            ftVar.c(false);
            ftVar.e(false);
            c cVar = new c(ftVar);
            l lVar = new l();
            if (!b.m().p()) {
                lVar.a(true);
            }
            String trim = accountRegister.c.getText().toString().trim();
            String trim2 = accountRegister.d.getText().toString().trim();
            lVar.a(m.a(trim, trim2, accountRegister.f.getText().toString()));
            lVar.d(accountRegister.getResources().getText(C0000R.string.txtTransformData).toString());
            cVar.a((o) lVar);
            cVar.a((o) new g());
            if (accountRegister.g.isChecked()) {
                l lVar2 = new l();
                lVar2.a(m.a(1L));
                lVar2.b(trim);
                lVar2.c(trim2);
                cVar.a((o) lVar2);
            }
            ftVar.a(cVar);
            ftVar.b(C0000R.string.networkProgress, C0000R.string.txtTransformData);
            ftVar.a(true, false);
        }
    }

    public final void a(int i2) {
        if (!this.i) {
            a();
            setResult(-1, getIntent());
            finish();
            return;
        }
        runOnUiThread(new xo(this));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.account_register);
        this.i = getIntent().getBooleanExtra("Extra_AlertAccount", false);
        this.c = (EditText) findViewById(C0000R.id.etUserName);
        this.c.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        this.d = (EditText) findViewById(C0000R.id.etPassword);
        this.e = (EditText) findViewById(C0000R.id.etPasswordConfirm);
        this.f = (EditText) findViewById(C0000R.id.etEmail);
        String e2 = b.m().e();
        if (e2 != null && e2.length() > 0) {
            this.f.setText(e2);
            this.c.requestFocus();
        }
        this.g = (CheckBox) findViewById(C0000R.id.cbMultiPhoneAccount);
        findViewById(C0000R.id.btnSave).setOnClickListener(this.j);
        findViewById(C0000R.id.btnCancel).setOnClickListener(this.j);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        setResult(0, getIntent());
        finish();
        return true;
    }
}
