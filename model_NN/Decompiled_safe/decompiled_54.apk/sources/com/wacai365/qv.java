package com.wacai365;

import android.app.Activity;
import android.content.DialogInterface;
import com.wacai.b;

final class qv implements Runnable {
    final /* synthetic */ sd a;
    private /* synthetic */ Activity b;
    private /* synthetic */ String c;
    private /* synthetic */ String d;
    private /* synthetic */ DialogInterface.OnClickListener e;

    qv(Activity activity, String str, String str2, sd sdVar, DialogInterface.OnClickListener onClickListener) {
        this.b = activity;
        this.c = str;
        this.d = str2;
        this.a = sdVar;
        this.e = onClickListener;
    }

    public final void run() {
        if (MyApp.a || !b.m().f()) {
            m.a(this.b, this.c, this.d, C0000R.string.txtBackupNow, C0000R.string.txtCancel, C0000R.string.txtBackupSetting, this.e);
        } else {
            m.a(this.b, this.c, this.d, this.b.getString(C0000R.string.txtIKnow), new pl(this));
        }
    }
}
