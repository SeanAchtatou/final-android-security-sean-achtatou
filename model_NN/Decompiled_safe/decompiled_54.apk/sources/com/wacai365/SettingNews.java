package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.wacai.data.c;
import com.wacai.e;

public class SettingNews extends WacaiActivity {
    /* access modifiers changed from: private */
    public ListView a;
    private ListAdapter b;
    private long c = -1;
    /* access modifiers changed from: private */
    public Button d;
    private View.OnClickListener e = new ol(this);

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        Cursor cursor = (Cursor) this.a.getItemAtPosition(((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).position);
        if (cursor != null) {
            this.c = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
        }
        switch (menuItem.getItemId()) {
            case C0000R.id.idDelete /*2131493338*/:
                c.b(this.c);
                cursor.requery();
                this.a.invalidateViews();
                return false;
            default:
                return false;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.news_list);
        m.b(this, 0);
        this.a = (ListView) findViewById(C0000R.id.IOList);
        Cursor rawQuery = e.c().b().rawQuery("select TBL_NEWS.id as _id, TBL_NEWS.title as _title, TBL_NEWS.type as _type, TBL_NEWS.enddate as _enddate, TBL_NEWSTYPE.name as _typename from TBL_NEWS, TBL_NEWSTYPE WHERE TBL_NEWSTYPE.id = TBL_NEWS.type GROUP BY TBL_NEWS.id ORDER BY TBL_NEWS.orderno ASC", null);
        startManagingCursor(rawQuery);
        this.b = new by(this, C0000R.layout.list_item_line2_news, rawQuery, new String[]{"_title", "_type", "_enddate", "_typename"}, new int[]{C0000R.id.listitem1, C0000R.id.listitem2, C0000R.id.listitem3});
        this.a.setAdapter(this.b);
        this.a.setOnItemClickListener(new on(this));
        this.a.setOnCreateContextMenuListener(new om(this));
        this.d = (Button) findViewById(C0000R.id.btnCancel);
        this.d.setOnClickListener(this.e);
    }
}
