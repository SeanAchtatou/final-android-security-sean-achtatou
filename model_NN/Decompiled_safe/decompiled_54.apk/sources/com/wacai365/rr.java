package com.wacai365;

import android.content.res.Resources;
import android.database.Cursor;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.f;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.o;
import com.wacai.data.aa;
import com.wacai.e;
import java.util.ArrayList;
import java.util.Hashtable;

public final class rr extends hg {
    private long[] e = null;
    private double f;

    public rr(StatView statView, ArrayList arrayList) {
        super(statView, arrayList);
    }

    private int a(int i, int i2, double d) {
        int size = this.b.size();
        String format = String.format("%04d/%02d", Integer.valueOf(i2 / 100), Integer.valueOf(i2 % 100));
        int i3 = i;
        while (true) {
            if (i3 >= size) {
                break;
            }
            Hashtable hashtable = (Hashtable) this.b.get(i3);
            if (hashtable != null) {
                int compareTo = ((String) hashtable.get("TAG_LABLE")).compareTo(format);
                if (compareTo == 0) {
                    hashtable.put("TAG_SECOND", m.a(d, 2));
                    break;
                } else if (compareTo > 0) {
                    Hashtable hashtable2 = new Hashtable();
                    hashtable2.put("TAG_LABLE", format);
                    hashtable2.put("TAG_FIRST", "0.0");
                    hashtable2.put("TAG_SECOND", m.a(d, 2));
                    this.b.add(i3, hashtable2);
                    break;
                }
            }
            i3++;
        }
        return i3;
    }

    private void d(QueryInfo queryInfo) {
        double[] dArr = new double[12];
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select ");
        for (int i = 1; i <= 12; i++) {
            if (i != 1) {
                stringBuffer.append(", ");
            }
            stringBuffer.append("sum(a.m" + i + ") as bm" + i);
        }
        stringBuffer.append(" from tbl_budgetinfo a");
        if (queryInfo.h.length() > 0) {
            stringBuffer.append(" where a.memberid in (" + queryInfo.h + ")");
        }
        Cursor rawQuery = e.c().b().rawQuery(stringBuffer.toString(), null);
        if (rawQuery != null) {
            int count = rawQuery.getCount();
            rawQuery.moveToFirst();
            for (int i2 = 0; i2 < count; i2++) {
                for (int i3 = 0; i3 < 12; i3++) {
                    dArr[i3] = m.a(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("bm" + (i3 + 1))));
                }
                rawQuery.moveToNext();
            }
            rawQuery.close();
        }
        this.f = 0.0d;
        int size = this.b.size();
        int i4 = 0;
        int i5 = (int) ((queryInfo.b / 100) % 100);
        while (i4 < size) {
            double d = dArr[i5 - 1];
            ((Hashtable) this.b.get(i4)).put("TAG_FIRST", m.a(d, 2));
            int i6 = i5 < 12 ? i5 + 1 : 1;
            this.f += d;
            i4++;
            i5 = i6;
        }
    }

    public final int a() {
        return 8;
    }

    public final QueryInfo a(QueryInfo queryInfo, int i) {
        QueryInfo queryInfo2 = new QueryInfo(queryInfo);
        queryInfo2.t = 1;
        queryInfo2.b = m.c(this.e[i]);
        queryInfo2.c = m.d(this.e[i]);
        return queryInfo2;
    }

    public final rj a(int i) {
        if (this.d == null) {
            this.d = new zn(this.a, this.b, i);
        }
        return this.d;
    }

    /* access modifiers changed from: protected */
    public final void a(QueryInfo queryInfo) {
        int i;
        if (this.b != null && queryInfo != null) {
            this.b.clear();
            long j = queryInfo.b / 100;
            long j2 = queryInfo.c / 100;
            long max = Math.max(j, j2);
            long min = Math.min(j, j2);
            this.e = new long[((int) ((((max % 100) + ((max / 100) * 12)) - ((min % 100) + ((min / 100) * 12))) + 1))];
            int i2 = (int) (j / 100);
            long j3 = j;
            int i3 = (int) (j % 100);
            int i4 = 0;
            while (j3 <= j2) {
                Hashtable hashtable = new Hashtable();
                hashtable.put("TAG_LABLE", String.format("%04d/%02d", Integer.valueOf(i2), Integer.valueOf(i3)));
                hashtable.put("TAG_SECOND", "0.0");
                hashtable.put("TAG_FIRST", "0.0");
                this.b.add(hashtable);
                this.e[i4] = (long) ((i2 * 100) + i3);
                if (i3 < 12) {
                    i3++;
                    i = i2;
                } else {
                    i = i2 + 1;
                    i3 = 1;
                }
                i4++;
                i2 = i;
                j3 = (long) ((i * 100) + i3);
            }
        }
    }

    public final boolean a(c cVar, QueryInfo queryInfo) {
        if (cVar == null || queryInfo == null) {
            return false;
        }
        this.f = 0.0d;
        a(queryInfo);
        d(queryInfo);
        l lVar = new l();
        StringBuffer stringBuffer = new StringBuffer(100);
        queryInfo.a(stringBuffer, 1);
        lVar.a(m.b(stringBuffer.toString()));
        cVar.a((o) lVar);
        f fVar = new f();
        fVar.a(false);
        fVar.a(1);
        fVar.a(this.b);
        cVar.a((o) fVar);
        return true;
    }

    public final String b(QueryInfo queryInfo) {
        Resources resources = this.a.getResources();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(resources.getText(C0000R.string.txtStringCountBudget));
        String str = "";
        if (b.a) {
            str = aa.a("TBL_MONEYTYPE", "flag", queryInfo.d);
            stringBuffer.append(str);
        }
        stringBuffer.append(m.a(this.f, 2));
        stringBuffer.append(" ");
        stringBuffer.append(resources.getText(C0000R.string.txtStringCountOutgo));
        if (b.a) {
            stringBuffer.append(str);
        }
        stringBuffer.append(m.a(this.c, 2));
        return stringBuffer.toString();
    }

    public final boolean b() {
        return true;
    }

    public final Class c() {
        return QueryStatByMonth.class;
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0126  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean c(com.wacai365.QueryInfo r11) {
        /*
            r10 = this;
            r5 = 0
            r4 = 0
            r3 = -1
            if (r11 != 0) goto L_0x0007
            r0 = r4
        L_0x0006:
            return r0
        L_0x0007:
            r10.a(r11)
            r10.d(r11)
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "select a.ymd/100 as ym, sum(b.sharemoney) as sm from tbl_outgoinfo a, tbl_outgomemberinfo b, tbl_accountinfo d "
            r0.append(r1)
            java.lang.String r1 = "where a.isdelete = 0 and a.ymd >= "
            r0.append(r1)
            long r1 = r11.b
            r0.append(r1)
            java.lang.String r1 = " and a.ymd <= "
            r0.append(r1)
            long r1 = r11.c
            r0.append(r1)
            java.lang.String r1 = " and b.outgoid = a.id and a.accountid = d.id "
            r0.append(r1)
            int r1 = r11.e
            if (r3 == r1) goto L_0x00fc
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = " and a.accountid = "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r11.e
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.append(r1)
        L_0x004c:
            int r1 = r11.f
            if (r3 == r1) goto L_0x0068
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = " and a.projectid = "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r11.f
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.append(r1)
        L_0x0068:
            int r1 = r11.g
            if (r3 == r1) goto L_0x0084
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = " and a.reimburse = "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r11.g
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.append(r1)
        L_0x0084:
            java.lang.String r1 = r11.h
            int r1 = r1.length()
            if (r1 <= 0) goto L_0x00aa
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = " and b.memberid in ("
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r11.h
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ")"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.append(r1)
        L_0x00aa:
            java.lang.String r1 = " group by a.ymd/100 order by a.ymd/100 ASC"
            r0.append(r1)
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ all -> 0x0122 }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ all -> 0x0122 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0122 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ all -> 0x0122 }
            r1 = 0
            r10.c = r1     // Catch:{ all -> 0x012a }
            if (r0 == 0) goto L_0x011a
            boolean r1 = r0.moveToFirst()     // Catch:{ all -> 0x012a }
            if (r1 == 0) goto L_0x011a
            int r1 = r0.getCount()     // Catch:{ all -> 0x012a }
            r2 = r4
            r3 = r4
        L_0x00d2:
            if (r2 >= r1) goto L_0x011a
            java.lang.String r4 = "ym"
            int r4 = r0.getColumnIndexOrThrow(r4)     // Catch:{ all -> 0x012a }
            int r4 = r0.getInt(r4)     // Catch:{ all -> 0x012a }
            java.lang.String r5 = "sm"
            int r5 = r0.getColumnIndexOrThrow(r5)     // Catch:{ all -> 0x012a }
            long r5 = r0.getLong(r5)     // Catch:{ all -> 0x012a }
            double r5 = com.wacai365.m.a(r5)     // Catch:{ all -> 0x012a }
            int r3 = r10.a(r3, r4, r5)     // Catch:{ all -> 0x012a }
            double r7 = r10.c     // Catch:{ all -> 0x012a }
            double r4 = r7 + r5
            r10.c = r4     // Catch:{ all -> 0x012a }
            r0.moveToNext()     // Catch:{ all -> 0x012a }
            int r2 = r2 + 1
            goto L_0x00d2
        L_0x00fc:
            int r1 = r11.d
            if (r3 == r1) goto L_0x004c
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = " and d.moneytype = "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r11.d
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.append(r1)
            goto L_0x004c
        L_0x011a:
            if (r0 == 0) goto L_0x011f
            r0.close()
        L_0x011f:
            r0 = 1
            goto L_0x0006
        L_0x0122:
            r0 = move-exception
            r1 = r5
        L_0x0124:
            if (r1 == 0) goto L_0x0129
            r1.close()
        L_0x0129:
            throw r0
        L_0x012a:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0124
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.rr.c(com.wacai365.QueryInfo):boolean");
    }

    public final int d() {
        return C0000R.string.txtStatBOByMonth;
    }

    public final void e() {
        this.c = 0.0d;
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            this.c += Double.parseDouble((String) ((Hashtable) this.b.get(i)).get("TAG_SECOND"));
        }
    }
}
