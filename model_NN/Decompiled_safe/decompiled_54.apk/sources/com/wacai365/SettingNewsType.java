package com.wacai365;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.wacai.data.i;
import com.wacai.e;

public class SettingNewsType extends WacaiActivity {
    private ListView a;
    private ListAdapter b;
    /* access modifiers changed from: private */
    public Button c;
    /* access modifiers changed from: private */
    public Button d;
    private i[] e;
    private View.OnClickListener f = new zg(this);

    static /* synthetic */ void b(SettingNewsType settingNewsType) {
        for (int i = 0; i < settingNewsType.a.getCount(); i++) {
            Object[] objArr = new Object[2];
            objArr[0] = Integer.valueOf(settingNewsType.a.isItemChecked(i) ? 1 : 0);
            objArr[1] = Long.valueOf(settingNewsType.e[i].x());
            e.c().b().execSQL(String.format("update TBL_NEWSTYPE set enable = %d where id = %d", objArr));
        }
    }

    public void onCreate(Bundle bundle) {
        Cursor cursor;
        String[] strArr;
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.setting_news_type);
        try {
            Cursor rawQuery = e.c().b().rawQuery("select id as _id, name as _name, mandatory as _mandatory, uuid as _uuid, enable as _enable from TBL_NEWSTYPE where mandatory = 0 ORDER BY orderno ASC", null);
            if (rawQuery == null) {
                try {
                    finish();
                    if (rawQuery != null) {
                        rawQuery.close();
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            } else {
                if (rawQuery.moveToFirst()) {
                    int count = rawQuery.getCount();
                    this.e = new i[count];
                    String[] strArr2 = new String[count];
                    for (int i = 0; i < count; i++) {
                        this.e[i] = new i();
                        this.e[i].k(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_id")));
                        this.e[i].a = rawQuery.getString(rawQuery.getColumnIndexOrThrow("_name"));
                        this.e[i].b = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_mandatory"));
                        this.e[i].g(rawQuery.getString(rawQuery.getColumnIndexOrThrow("_uuid")));
                        this.e[i].c = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_enable")) > 0;
                        strArr2[i] = this.e[i].a;
                        rawQuery.moveToNext();
                    }
                    strArr = strArr2;
                } else {
                    strArr = null;
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
                this.a = (ListView) findViewById(C0000R.id.IOList);
                if (strArr != null) {
                    this.b = new ArrayAdapter(this, (int) C0000R.layout.simple_list_item_multiple_choice, strArr);
                    this.a.setAdapter(this.b);
                }
                this.a.setItemsCanFocus(false);
                this.a.setChoiceMode(2);
                this.c = (Button) findViewById(C0000R.id.btnOK);
                this.c.setOnClickListener(this.f);
                this.d = (Button) findViewById(C0000R.id.btnCancel);
                this.d.setOnClickListener(this.f);
                for (int i2 = 0; i2 < this.a.getCount(); i2++) {
                    this.a.setItemChecked(i2, this.e[i2].c);
                }
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }
}
