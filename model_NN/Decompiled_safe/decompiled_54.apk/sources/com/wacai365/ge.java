package com.wacai365;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.animation.Animation;

final class ge implements View.OnClickListener {
    final /* synthetic */ InputAccount a;

    ge(InputAccount inputAccount) {
        this.a = inputAccount;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, boolean, boolean, android.content.DialogInterface$OnClickListener):int[]
     arg types: [com.wacai365.InputAccount, int, int, com.wacai365.aau]
     candidates:
      com.wacai365.m.a(android.view.animation.Animation, int, android.view.View, java.lang.String):android.view.animation.Animation
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, java.util.ArrayList, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, int[], android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.app.ProgressDialog, boolean, boolean, android.content.DialogInterface$OnClickListener):com.wacai365.tp
      com.wacai365.m.a(android.content.Context, long, android.content.DialogInterface$OnClickListener, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, long, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, boolean, boolean, android.content.DialogInterface$OnClickListener):int[] */
    public final void onClick(View view) {
        if (this.a.C.m()) {
            m.a(this.a, (Animation) null, 0, (View) null, (int) C0000R.string.txtDisableAccountTypePrompt);
        } else {
            int[] unused = this.a.q = m.a((Context) this.a, true, false, (DialogInterface.OnClickListener) new aau(this));
        }
    }
}
