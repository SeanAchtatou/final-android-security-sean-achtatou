package com.wacai365;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Backup extends WacaiActivity {
    /* access modifiers changed from: private */
    public View a;
    /* access modifiers changed from: private */
    public Button b;
    /* access modifiers changed from: private */
    public Button c;
    private View.OnClickListener d = new km(this);

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (-1 == i2 && intent != null) {
            switch (i) {
                case 2:
                    setResult(-1, getIntent());
                    finish();
                    return;
                case 13:
                    setResult(-1, getIntent());
                    finish();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.backup);
        this.a = findViewById(C0000R.id.backup_directly);
        this.a.setOnClickListener(this.d);
        this.b = (Button) findViewById(C0000R.id.btn_registerBackup);
        this.b.setOnClickListener(this.d);
        this.c = (Button) findViewById(C0000R.id.btn_configBackup);
        this.c.setOnClickListener(this.d);
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(new kw(this));
    }
}
