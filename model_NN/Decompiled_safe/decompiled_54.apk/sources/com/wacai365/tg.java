package com.wacai365;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

final class tg extends BaseAdapter {
    private Context a;
    private int b;
    private LayoutInflater c = ((LayoutInflater) this.a.getSystemService("layout_inflater"));
    private /* synthetic */ StatBalance d;

    public tg(StatBalance statBalance, Context context, int i) {
        this.d = statBalance;
        this.a = context;
        this.b = i;
    }

    public final int getCount() {
        return this.d.c.length;
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        LinearLayout linearLayout = view == null ? (LinearLayout) this.c.inflate(this.b, (ViewGroup) null) : (LinearLayout) view;
        String str = this.d.c[i];
        String str2 = this.d.d[i];
        ((TextView) linearLayout.findViewById(C0000R.id.listitem1)).setText(str);
        ((TextView) linearLayout.findViewById(C0000R.id.listitem2)).setText(str2);
        return linearLayout;
    }
}
