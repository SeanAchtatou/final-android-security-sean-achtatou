package com.wacai365;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.wacai.a.a;
import com.wacai.data.aa;
import com.wacai.data.b;
import com.wacai.data.d;
import com.wacai.data.h;
import com.wacai.data.m;
import com.wacai.data.s;
import com.wacai.data.u;
import java.util.Date;

public class be extends oj {
    private TextView A;
    /* access modifiers changed from: private */
    public TextView B;
    /* access modifiers changed from: private */
    public TextView C;
    /* access modifiers changed from: private */
    public TextView D;
    private TextView E;
    /* access modifiers changed from: private */
    public TextView F;
    private TextView G;
    private TextView H;
    /* access modifiers changed from: private */
    public TextView I;
    private TextView J;
    /* access modifiers changed from: private */
    public Button K;
    private m L;
    private d M;
    /* access modifiers changed from: private */
    public long N = (new Date().getTime() / 1000);
    /* access modifiers changed from: private */
    public long O = 0;
    private long P = 0;
    /* access modifiers changed from: private */
    public long Q = 0;
    private boolean R = false;
    private int S = 3;
    /* access modifiers changed from: private */
    public int T = 0;
    private int[] U = null;
    private String[] V = null;
    /* access modifiers changed from: private */
    public double W = 0.0d;
    /* access modifiers changed from: private */
    public va X = new du(this);
    private yn Y = new ds(this);
    private View.OnClickListener Z = new dq(this);
    String[] a = null;
    int[] b = null;
    String[] c = null;
    /* access modifiers changed from: private */
    public View j;
    /* access modifiers changed from: private */
    public View k;
    /* access modifiers changed from: private */
    public View l;
    /* access modifiers changed from: private */
    public View m;
    /* access modifiers changed from: private */
    public View r;
    /* access modifiers changed from: private */
    public View s;
    /* access modifiers changed from: private */
    public View t;
    /* access modifiers changed from: private */
    public View u;
    /* access modifiers changed from: private */
    public View v;
    /* access modifiers changed from: private */
    public View w;
    /* access modifiers changed from: private */
    public TextView x;
    /* access modifiers changed from: private */
    public TextView y;
    /* access modifiers changed from: private */
    public TextView z;

    public be(long j2, Intent intent) {
        this.R = j2 > 0;
        this.d = C0000R.layout.input_loan_tab;
        int intExtra = intent.getIntExtra("Extra_Type2", 0);
        this.S = intent.getIntExtra("Extra_Type", 3);
        this.Q = (long) intent.getIntExtra("Extra_DebtID", 0);
        if (!this.R) {
            this.P = h.c(false);
            if (this.Q <= 0) {
                this.Q = h.c(true);
            }
            if (this.S == 3) {
                if (intExtra == 0) {
                    this.T = 0;
                } else {
                    this.T = 1;
                }
            } else if (intExtra == 0) {
                this.T = 3;
            } else {
                this.T = 2;
            }
        } else if (this.S == 3) {
            this.L = m.f(j2);
            this.P = this.L.g();
            this.Q = this.L.h();
        } else {
            this.M = d.f(j2);
            this.P = this.M.g();
            this.Q = this.M.h();
        }
    }

    private static int a(String str, String[] strArr) {
        for (int i = 0; i < strArr.length; i++) {
            if (str.equals(strArr[i])) {
                return i;
            }
        }
        return 0;
    }

    private void a(int i, int i2) {
        char c2 = 3;
        if (i == 3) {
            c2 = i2 == 0 ? (char) 0 : 1;
        } else if (i2 != 0) {
            c2 = 2;
        }
        this.y.setText(this.a[c2]);
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, boolean z2) {
        if (i == -1 || i2 < 0 || i2 > 28) {
            this.E.setText(this.V[0]);
            this.u.setVisibility(0);
            this.v.setVisibility(8);
        } else if (i != 0) {
            if (i2 != 0) {
                this.F.setText(Integer.toString(i2));
            } else {
                this.F.setText((int) C0000R.string.txtMonthEnd);
            }
            this.v.setVisibility(0);
            this.E.setText(this.V[3]);
        } else if (z2 || this.O != 0) {
            this.E.setText(this.V[i2 == 0 ? (char) 1 : 2]);
            this.v.setVisibility(8);
        } else {
            m.a((Animation) null, 0, (View) null, (int) C0000R.string.txtMsgAlertDate, 1);
        }
    }

    /* access modifiers changed from: package-private */
    public static void a(Context context, String[] strArr, DialogInterface.OnClickListener onClickListener) {
        new og(context, strArr, onClickListener).show();
    }

    private boolean a(double d) {
        if (d > 0.0d) {
            return true;
        }
        m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.txtInvalidMoney);
        return false;
    }

    private static double b(String str) {
        if (str == null || str.length() <= 0) {
            return 0.0d;
        }
        return Double.valueOf(str).doubleValue();
    }

    private boolean b(int i) {
        boolean z2;
        double b2 = b(this.x.getText().toString());
        double b3 = b(this.B.getText().toString());
        if (!a(b2)) {
            return false;
        }
        if (b3 > b2) {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.txtMsgAlertProfit);
            z2 = false;
        } else {
            z2 = true;
        }
        if (!z2) {
            return false;
        }
        if (!k()) {
            return false;
        }
        d dVar = (!this.R || this.M == null || 4 != this.S) ? new d() : this.M;
        dVar.b(m.a(b2));
        dVar.c(m.a(b3));
        dVar.a(i);
        dVar.e(this.Q);
        dVar.d(this.P);
        dVar.a(this.N);
        dVar.a(this.D.getText().toString());
        dVar.a(false);
        dVar.f(false);
        dVar.c();
        return true;
    }

    private boolean c(int i) {
        boolean z2;
        b bVar;
        double b2 = b(this.x.getText().toString());
        if (!a(b2)) {
            return false;
        }
        if (this.O == 0 || a.a(this.O * 1000) >= a.a(this.N * 1000)) {
            z2 = true;
        } else {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.txtMsgDateErr);
            z2 = false;
        }
        if (!z2) {
            return false;
        }
        if (!k()) {
            return false;
        }
        m mVar = (!this.R || this.L == null || 3 != this.S) ? new m() : this.L;
        mVar.e(this.Q);
        mVar.d(this.P);
        mVar.a(this.N);
        mVar.c(this.O);
        mVar.a(i);
        mVar.b(m.a(b2));
        mVar.a(this.D.getText().toString());
        mVar.a(false);
        int a2 = a(this.E.getText().toString(), this.V);
        if (a2 != 0) {
            b bVar2 = new b();
            bVar2.b(1);
            if (a2 == 1) {
                bVar2.a(0);
                bVar2.c(0);
                bVar = bVar2;
            } else if (a2 == 2) {
                bVar2.a(0);
                bVar2.c(3);
                bVar = bVar2;
            } else {
                bVar2.a(2);
                String obj = this.F.getText().toString();
                bVar2.c(!obj.equals(this.n.getResources().getString(C0000R.string.txtMonthEnd)) ? a(obj, this.c) + 1 : 0);
                bVar = bVar2;
            }
        } else {
            bVar = null;
        }
        mVar.a(bVar);
        mVar.f(false);
        mVar.c();
        return true;
    }

    private boolean k() {
        if (!aa.b("TBL_ACCOUNTINFO", this.Q)) {
            m.a((Animation) null, 0, (View) null, String.format(this.n.getResources().getString(C0000R.string.txtMsgDebtTitleErr), this.G.getText()));
            return false;
        } else if (!aa.b("TBL_ACCOUNTINFO", this.P)) {
            m.a(this.n, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideAccount);
            return false;
        } else if (aa.a("TBL_ACCOUNTINFO", "moneytype", (int) this.Q).equals(aa.a("TBL_ACCOUNTINFO", "moneytype", (int) this.P))) {
            return true;
        } else {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.txtMsgMoneyTypeErr);
            return false;
        }
    }

    public final long a() {
        return this.N;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.be.a(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      com.wacai365.be.a(android.content.Context, java.lang.String[], android.content.DialogInterface$OnClickListener):void
      com.wacai365.be.a(int, int, android.content.Intent):void
      com.wacai365.yi.a(int, int, android.content.Intent):void
      com.wacai365.ug.a(int, int, android.content.Intent):void
      com.wacai365.be.a(int, int, boolean):void */
    /* access modifiers changed from: protected */
    public final void a(int i) {
        switch (i) {
            case 0:
                this.G.setText(this.n.getResources().getString(C0000R.string.txtCreditor));
                this.H.setText(this.n.getResources().getString(C0000R.string.txtCreditAccount));
                this.r.setVisibility(8);
                this.s.setVisibility(0);
                this.J.setVisibility(0);
                int a2 = a(this.E.getText().toString(), this.V);
                if (a2 == 0) {
                    a(-1, -1, true);
                    return;
                }
                this.u.setVisibility(0);
                if (a2 == this.V.length - 1) {
                    this.v.setVisibility(0);
                    return;
                } else {
                    this.v.setVisibility(8);
                    return;
                }
            case 1:
                this.G.setText(this.n.getResources().getString(C0000R.string.txtDebtor));
                this.H.setText(this.n.getResources().getString(C0000R.string.txtDebitAccount));
                this.s.setVisibility(0);
                this.r.setVisibility(8);
                this.J.setVisibility(0);
                int a3 = a(this.E.getText().toString(), this.V);
                if (a3 == 0) {
                    a(-1, -1, true);
                    return;
                }
                this.u.setVisibility(0);
                if (a3 == this.V.length - 1) {
                    this.v.setVisibility(0);
                    return;
                } else {
                    this.v.setVisibility(8);
                    return;
                }
            case 2:
                this.G.setText(this.n.getResources().getString(C0000R.string.txtCreditor));
                this.H.setText(this.n.getResources().getString(C0000R.string.txtRepaymentAccount));
                this.u.setVisibility(8);
                this.s.setVisibility(8);
                this.r.setVisibility(0);
                this.v.setVisibility(8);
                this.J.setVisibility(8);
                return;
            case 3:
                this.G.setText(this.n.getResources().getString(C0000R.string.txtDebtor));
                this.H.setText(this.n.getResources().getString(C0000R.string.txtMakeCollectionsAccount));
                this.u.setVisibility(8);
                this.s.setVisibility(8);
                this.r.setVisibility(0);
                this.v.setVisibility(8);
                this.J.setVisibility(8);
                return;
            default:
                return;
        }
    }

    public final void a(int i, int i2, Intent intent) {
        super.a(i, i2, intent);
        if (i2 == -1 && intent != null) {
            switch (i) {
                case 16:
                case 34:
                    String string = intent.getExtras().getString("Text_String");
                    m.a(string, this.x, this.D);
                    if (string != null) {
                        a(string);
                        m.a(string, this.D);
                        return;
                    }
                    return;
                case 30:
                    this.Q = intent.getLongExtra("ret_id", this.Q);
                    m.a("TBL_ACCOUNTINFO", this.Q, this.z);
                    return;
                case 37:
                    this.P = intent.getLongExtra("account_Sel_Id", this.P);
                    m.a(this.o, this.P, this.A);
                    return;
                default:
                    return;
            }
        }
    }

    public final void a(long j2) {
        this.N = j2;
        if (this.L != null) {
            this.L.a(this.N);
        } else if (this.M != null) {
            this.M.a(this.N);
        }
        if (this.I != null) {
            m.d(this.N * 1000, this.I);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.be.a(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      com.wacai365.be.a(android.content.Context, java.lang.String[], android.content.DialogInterface$OnClickListener):void
      com.wacai365.be.a(int, int, android.content.Intent):void
      com.wacai365.yi.a(int, int, android.content.Intent):void
      com.wacai365.ug.a(int, int, android.content.Intent):void
      com.wacai365.be.a(int, int, boolean):void */
    /* access modifiers changed from: protected */
    public final void a(LinearLayout linearLayout) {
        this.g = linearLayout;
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.g.getLayoutParams();
        layoutParams.setMargins(0, 0, 0, m.h(this.n, 50));
        this.g.setLayoutParams(layoutParams);
        this.x = (TextView) linearLayout.findViewById(C0000R.id.viewMoney);
        this.j = (LinearLayout) linearLayout.findViewById(C0000R.id.moneyItem);
        this.j.setOnClickListener(this.Z);
        this.I = (TextView) linearLayout.findViewById(C0000R.id.viewDate);
        this.J = (TextView) linearLayout.findViewById(C0000R.id.id_AdvOptionTitle);
        this.y = (TextView) linearLayout.findViewById(C0000R.id.viewLoanType);
        this.z = (TextView) linearLayout.findViewById(C0000R.id.viewLoanDebt);
        this.A = (TextView) linearLayout.findViewById(C0000R.id.viewLoanAccount);
        this.C = (TextView) linearLayout.findViewById(C0000R.id.viewExpectDate);
        this.D = (TextView) linearLayout.findViewById(C0000R.id.viewComment);
        this.E = (TextView) linearLayout.findViewById(C0000R.id.viewAlertType);
        this.F = (TextView) linearLayout.findViewById(C0000R.id.viewAlertDate);
        this.B = (TextView) linearLayout.findViewById(C0000R.id.viewProfit);
        this.G = (TextView) linearLayout.findViewById(C0000R.id.viewUserTitle);
        this.H = (TextView) linearLayout.findViewById(C0000R.id.viewAccountTitle);
        this.k = (LinearLayout) linearLayout.findViewById(C0000R.id.layout_loantype);
        this.k.setOnClickListener(this.Z);
        this.l = (LinearLayout) linearLayout.findViewById(C0000R.id.layout_loandebt);
        this.l.setOnClickListener(this.Z);
        this.m = (LinearLayout) linearLayout.findViewById(C0000R.id.layout_loanaccount);
        this.m.setOnClickListener(this.Z);
        this.r = (LinearLayout) linearLayout.findViewById(C0000R.id.layout_profit);
        this.r.setOnClickListener(this.Z);
        this.s = (LinearLayout) linearLayout.findViewById(C0000R.id.layout_expectdate);
        this.s.setOnClickListener(this.Z);
        this.t = linearLayout.findViewById(C0000R.id.Layout_commentItem);
        this.t.setOnClickListener(this.Z);
        this.u = (LinearLayout) linearLayout.findViewById(C0000R.id.layout_alerttype);
        this.u.setOnClickListener(this.Z);
        this.v = (LinearLayout) linearLayout.findViewById(C0000R.id.layout_alertdate);
        this.v.setOnClickListener(this.Z);
        this.w = linearLayout.findViewById(C0000R.id.dateItem);
        this.w.setOnClickListener(this.Z);
        this.K = (Button) linearLayout.findViewById(C0000R.id.voice_input);
        this.K.setOnClickListener(this.Z);
        this.a = this.n.getResources().getStringArray(C0000R.array.LoanType);
        this.V = this.n.getResources().getStringArray(C0000R.array.AlertType);
        String[] stringArray = this.n.getResources().getStringArray(C0000R.array.OccurDate);
        this.c = new String[(stringArray.length + 1)];
        for (int i = 0; i < stringArray.length; i++) {
            this.c[i] = stringArray[i];
        }
        this.c[stringArray.length] = this.n.getResources().getString(C0000R.string.txtMonthEnd);
        if (this.R) {
            if (this.R && ((this.S != 3 || this.L != null) && (this.S != 4 || this.M != null))) {
                if (this.k != null) {
                    this.k.setClickable(false);
                }
                switch (this.S) {
                    case 3:
                        this.r.setVisibility(8);
                        this.x.setText(aa.a(aa.l(this.L.d()), 2));
                        this.N = this.L.b();
                        m.d(this.N * 1000, this.I);
                        this.O = this.L.e();
                        if (0 != this.O) {
                            m.c(this.O * 1000, this.C);
                        }
                        if (1 == this.L.a()) {
                            this.G.setText(this.n.getResources().getString(C0000R.string.txtDebtor));
                            this.H.setText(this.n.getResources().getString(C0000R.string.txtDebitAccount));
                        }
                        a(3, this.L.a());
                        this.D.setText(this.L.i());
                        b j2 = this.L.j();
                        if (j2 != null) {
                            a(j2.b(), j2.d(), true);
                            break;
                        } else {
                            a(-1, -1, true);
                            break;
                        }
                    case 4:
                        this.s.setVisibility(8);
                        this.u.setVisibility(8);
                        this.v.setVisibility(8);
                        this.J.setVisibility(8);
                        this.x.setText(aa.a(aa.l(this.M.b()), 2));
                        this.N = this.M.a();
                        m.d(this.N * 1000, this.I);
                        a(4, this.M.e());
                        if (this.M.e() == 0) {
                            this.G.setText(this.n.getResources().getString(C0000R.string.txtDebtor));
                            this.H.setText(this.n.getResources().getString(C0000R.string.txtMakeCollectionsAccount));
                        } else {
                            this.G.setText(this.n.getResources().getString(C0000R.string.txtCreditor));
                            this.H.setText(this.n.getResources().getString(C0000R.string.txtRepaymentAccount));
                        }
                        long d = this.M.d();
                        if (d != 0) {
                            this.B.setText(aa.a(aa.l(d), 2));
                        }
                        this.D.setText(this.M.i());
                        break;
                }
            } else {
                Log.e(be.class.getName(), "Init Edit Mode illegal!");
            }
        } else {
            m.d(this.N * 1000, this.I);
            if (this.y != null) {
                this.y.setText(this.a[this.T]);
            }
            this.x.setText(aa.a(this.W, 2));
            a(this.T);
        }
        m.a("TBL_ACCOUNTINFO", this.Q, this.z);
        m.a("TBL_ACCOUNTINFO", this.P, this.A);
    }

    public final void a(ScrollView scrollView, int i) {
        this.h = scrollView;
        this.i = i;
    }

    public final void a(Object obj) {
        if (s.class.isInstance(obj)) {
            this.W = m.a(((s) obj).o());
        } else if (u.class.isInstance(obj)) {
            this.W = m.a(((u) obj).b());
        }
    }

    public final void a(String str) {
        if (str != null) {
            if (3 == this.S && this.L != null) {
                this.L.a(str);
            } else if (4 == this.S && this.L != null) {
                this.M.a(str);
            }
            m.a(str, this.D);
        }
    }

    public final Object b() {
        if (3 == this.S) {
            if (this.L == null) {
                this.L = new m();
            }
            this.L.b(m.a(this.W));
            return this.L;
        }
        if (this.M == null) {
            this.M = new d();
        }
        this.M.b(m.a(this.W));
        return this.M;
    }

    public final void b(long j2) {
        this.x.setText(aa.a(aa.l(j2), 2));
    }

    public final boolean c() {
        String obj = this.y.getText().toString();
        if (obj.equals(this.a[0])) {
            return c(0);
        }
        if (obj.equals(this.a[1])) {
            return c(1);
        }
        if (obj.equals(this.a[2])) {
            return b(1);
        }
        if (obj.equals(this.a[3])) {
            return b(0);
        }
        return true;
    }

    public final void d() {
        if (this.f != null) {
            if (af.class.isInstance(this.f)) {
                ((af) this.f).a(this.X, this.j);
            } else if (zl.class.isInstance(this.f)) {
                ((zl) this.f).a(this.Y, this.w);
            }
        }
    }

    public final ca e() {
        return this.f;
    }

    public final void f() {
        this.W = 0.0d;
        this.x.setText(aa.a(this.W, 2));
        this.D.setText("");
        this.B.setText("");
    }
}
