package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.wacai.b;
import com.wacai.data.aa;
import com.wacai.data.d;
import com.wacai.data.m;
import com.wacai.data.r;
import com.wacai.data.u;
import com.wacai.data.z;
import com.wacai.e;

public class MySearch extends WacaiActivity {
    /* access modifiers changed from: private */
    public Button a;
    /* access modifiers changed from: private */
    public Button b;
    /* access modifiers changed from: private */
    public EditText c;
    /* access modifiers changed from: private */
    public ListView d;
    private View e;
    private TextView f;
    private int g = 1;
    /* access modifiers changed from: private */
    public sd h = new sd();
    /* access modifiers changed from: private */
    public fy i;
    private View.OnClickListener j = new mn(this);

    /* access modifiers changed from: private */
    public void a() {
        runOnUiThread(new mr(this));
        if (this.g == 0) {
            this.h.d();
            new mo(this).start();
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, long j2) {
        long j3;
        Cursor cursor = (Cursor) this.d.getItemAtPosition(i2);
        int i3 = cursor.getInt(cursor.getColumnIndexOrThrow("_flag"));
        if (i3 == 0 || i3 == 1) {
            j3 = cursor.getLong(cursor.getColumnIndexOrThrow("_paybackid"));
            if (j3 > 0) {
                i3 = 4;
                Intent a2 = InputTrade.a(this, "", j3);
                a2.putExtra("LaunchedByApplication", 1);
                a2.putExtra("Extra_Type", i3);
                a2.putExtra("Extra_Type2", cursor.getInt(cursor.getColumnIndex("_targetname")));
                a2.putExtra("Extra_Id", j3);
                startActivityForResult(a2, 19);
            }
        }
        j3 = j2;
        Intent a22 = InputTrade.a(this, "", j3);
        a22.putExtra("LaunchedByApplication", 1);
        a22.putExtra("Extra_Type", i3);
        a22.putExtra("Extra_Type2", cursor.getInt(cursor.getColumnIndex("_targetname")));
        a22.putExtra("Extra_Id", j3);
        startActivityForResult(a22, 19);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.MySearch.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.MySearch.a(com.wacai365.MySearch, com.wacai365.fy):com.wacai365.fy
      com.wacai365.MySearch.a(int, long):void
      com.wacai365.MySearch.a(boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.wacai365.m.a(com.wacai.data.s, int):com.wacai.data.x
      com.wacai365.m.a(double, int):java.lang.String
      com.wacai365.m.a(long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, android.widget.EditText):void
      com.wacai365.m.a(android.content.Context, int):void
      com.wacai365.m.a(android.content.Context, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Intent, com.wacai.data.s):void
      com.wacai365.m.a(android.view.View, int):void
      com.wacai365.m.a(android.widget.Button, android.content.res.Resources):void
      com.wacai365.m.a(java.io.File, android.app.Activity):void
      com.wacai365.m.a(java.lang.String, android.widget.TextView):void
      com.wacai365.m.a(java.util.ArrayList, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean):boolean
      com.wacai365.m.a(android.graphics.Bitmap, android.content.Context):boolean
      com.wacai365.m.a(java.lang.String, int):java.lang.String[]
      com.wacai365.m.a(java.lang.String, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.MySearch.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.wacai365.MySearch.a(com.wacai365.MySearch, com.wacai365.fy):com.wacai365.fy
      com.wacai365.MySearch.a(int, long):void
      com.wacai365.MySearch.a(boolean, boolean):void */
    static /* synthetic */ void a(MySearch mySearch) {
        if (mySearch.c.getText().length() == 0) {
            mySearch.a(true, false);
            return;
        }
        String trim = aa.e(mySearch.c.getText().toString()).trim();
        if (trim.length() > 0) {
            String a2 = m.a(trim, false);
            Cursor rawQuery = e.c().b().rawQuery(String.format("select * from (select 0 as _flag, a.id as _id, a.outgodate as _outgodate, a.money as _money, a.comment as _comment, a.scheduleoutgoid as _scheduleid, c.name as _name, a.targetid as _tid, h.name as _targetname, i.flag as _moneyflag1, a.paybackid as _paybackid, a.reimburse as _reimburse, i.id as _moneytypeid1, a.ymd as _ymd from tbl_outgoinfo a, TBL_MONEYTYPE i join TBL_OUTGOSUBTYPEINFO c on a.subtypeid=c.id join TBL_PROJECTINFO d on d.id = a.projectid join TBL_ACCOUNTINFO e on e.id = a.accountid join TBL_OUTGOMAINTYPEINFO g on g.id = c.id / 10000 LEFT JOIN TBL_TRADETARGET h ON a.targetid = h.id where a.isdelete = 0 and a.scheduleoutgoid = 0 and e.moneytype = i.id and ( (a.comment like '%s' ESCAPE '|' or c.name like '%s' ESCAPE '|' or d.name like '%s' ESCAPE '|' or e.name like '%s' ESCAPE '|' or g.name like '%s' ESCAPE '|' or h.name like '%s' ESCAPE '|') or exists ( select * from tbl_outgomemberinfo b join TBL_MEMBERINFO f on f.id = b.memberid where a.id=b.outgoid and f.name like '%s' ESCAPE '|' ) ) UNION select 1 as _flag, a.id as _id, a.incomedate as _outgodate, a.money as _money, a.comment as _comment, a.scheduleincomeid as _scheduleid, c.name as _name, a.targetid as _tid, h.name as _targetname, i.flag as _moneyflag1, a.paybackid as _paybackid, null as _reimburse, i.id as _moneytypeid1, a.ymd as _ymd from TBL_INCOMEINFO a, TBL_MONEYTYPE i join TBL_INCOMEMAINTYPEINFO c on c.id = a.typeid join TBL_PROJECTINFO d on d.id = a.projectid join TBL_ACCOUNTINFO e on e.id = a.accountid LEFT JOIN TBL_TRADETARGET h ON a.targetid = h.id where a.isdelete = 0 and a.scheduleincomeid = 0 and e.moneytype = i.id and ( (a.comment like '%s' ESCAPE '|' or c.name like '%s' ESCAPE '|' or d.name like '%s' ESCAPE '|' or e.name like '%s' ESCAPE '|' or h.name like '%s' ESCAPE '|') or exists ( select * from TBL_INCOMEMEMBERINFO b join TBL_MEMBERINFO f on f.id = b.memberid where b.incomeid = a.id and f.name like '%s' ESCAPE '|' ) ) UNION select 2 as _flag, a.id as _id, a.date as _outgodate, a.transferoutmoney as _money, a.comment as _comment, 0 as _scheduleid, 0 as _name, 0 as _tid, 0 as _targetname, c.flag as _moneyflag1, 0 as _paybackid, null as _reimburse, c.id as _moneytypeid1, a.ymd as _ymd   from TBL_TRANSFERINFO as a join TBL_ACCOUNTINFO b on b.id=a.transferoutaccountid join TBL_MONEYTYPE c on c.id=b.moneytype join TBL_ACCOUNTINFO d on d.id=a.transferinaccountid join TBL_MONEYTYPE e on e.id=d.moneytype where a.isdelete = 0 and a.type = 0 and (a.comment like '%s' ESCAPE '|' or b.name like '%s' ESCAPE '|' or d.name like '%s' ESCAPE '|') group by a.id UNION select 3 as _flag, a.id as _id, a.date as _outgodate, a.money as _money, a.comment as _comment, 0 as _scheduleid, b.name as _name,  0 as _tid, a.type as _targetname, c.flag as _moneyflag1, 0 as _paybackid, null as _reimburse, c.id as _moneytypeid1, a.ymd as _ymd  from TBL_LOAN as a, TBL_ACCOUNTINFO as b, TBL_MONEYTYPE as c, TBL_ACCOUNTINFO as d where a.isdelete = 0 and b.type = 3 and a.debtid = b.id and b.moneytype = c.id and a.accountid = d.id and (a.comment like '%s' ESCAPE '|' or b.name like '%s' ESCAPE '|' or d.name like '%s' ESCAPE '|' ) group by a.id  UNION select 4 as _flag, a.id as _id, a.date as _outgodate, a.money as _money, a.comment as _comment, 0 as _scheduleid, b.name as _name,  0 as _tid, a.type as _targetname, c.flag as _moneyflag1, 0 as _paybackid , null as _reimburse, c.id as _moneytypeid1, a.ymd as _ymd from TBL_PAYBACK as a, TBL_ACCOUNTINFO as b, TBL_MONEYTYPE as c, TBL_ACCOUNTINFO as d where a.isdelete = 0 and b.type = 3 and a.debtid = b.id and b.moneytype = c.id and a.accountid = d.id and (a.comment like '%s' ESCAPE '|' or b.name like '%s' ESCAPE '|' or d.name like '%s' ESCAPE '|' ) group by a.id ) order by _outgodate DESC ", a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2), null);
            mySearch.startManagingCursor(rawQuery);
            if (mySearch.d == null) {
                mySearch.d = (ListView) mySearch.findViewById(C0000R.id.list);
                mySearch.d.setAdapter((ListAdapter) new hq(mySearch, C0000R.layout.search_list_item, rawQuery, new String[]{"_outgodate"}, new int[]{C0000R.id.id_money}, b.a));
                mySearch.d.setOnItemClickListener(new mp(mySearch));
                mySearch.d.setOnCreateContextMenuListener(new mm(mySearch));
            } else {
                ((SimpleCursorAdapter) mySearch.d.getAdapter()).changeCursor(rawQuery);
            }
            mySearch.a(rawQuery.getCount() == 0, true);
        }
    }

    private void a(boolean z, boolean z2) {
        if (z) {
            if (this.d != null) {
                this.d.setVisibility(8);
            }
            this.e.setVisibility(0);
            if (z2) {
                this.f.setHint((int) C0000R.string.txtSearchResultEmptyHint);
            } else {
                this.f.setHint("");
            }
        } else {
            this.e.setVisibility(8);
            this.d.setVisibility(0);
        }
    }

    static /* synthetic */ void e(MySearch mySearch) {
        mySearch.c = (EditText) mySearch.findViewById(C0000R.id.etSearch);
        mySearch.c.addTextChangedListener(new mv(mySearch));
        ((ImageButton) mySearch.findViewById(C0000R.id.btnClear)).setOnClickListener(new ms(mySearch));
        mySearch.a = (Button) mySearch.findViewById(C0000R.id.btnCount);
        mySearch.a.setOnClickListener(mySearch.j);
        mySearch.b = (Button) mySearch.findViewById(C0000R.id.btnCancel);
        mySearch.b.setOnClickListener(mySearch.j);
        mySearch.f = (TextView) mySearch.findViewById(C0000R.id.id_listhint);
        mySearch.e = mySearch.findViewById(C0000R.id.id_hint);
    }

    public void finish() {
        super.finish();
        if (this.g == 0) {
            abt.a(this, false);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        switch (i2) {
            case 19:
                if (i3 == -1) {
                    ((hq) this.d.getAdapter()).getCursor().requery();
                    this.d.invalidateViews();
                    return;
                }
                return;
            case 27:
                if (i3 == -1) {
                    this.h.e();
                    break;
                } else {
                    this.h.c();
                    finish();
                    break;
                }
        }
        if (this.i != null) {
            this.i.a(i2, i3, intent);
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        switch (menuItem.getItemId()) {
            case C0000R.id.idDelete /*2131493338*/:
                int i2 = adapterContextMenuInfo.position;
                ListView listView = (ListView) findViewById(C0000R.id.list);
                Cursor cursor = (Cursor) listView.getItemAtPosition(i2);
                if (cursor == null) {
                    return true;
                }
                int i3 = cursor.getInt(cursor.getColumnIndexOrThrow("_flag"));
                long j2 = cursor.getLong(cursor.getColumnIndexOrThrow("_scheduleid"));
                long j3 = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
                long j4 = cursor.getLong(cursor.getColumnIndexOrThrow("_paybackid"));
                if (j2 > 0) {
                    m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtDeleteScheduleData);
                    return true;
                } else if (j4 > 0) {
                    m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtDeletePayGenData);
                    return true;
                } else {
                    switch (i3) {
                        case 0:
                            z.e(j3);
                            break;
                        case 1:
                            r.d(j3);
                            break;
                        case 2:
                            u.g(j3);
                            break;
                        case 3:
                            m.g(j3);
                            break;
                        case 4:
                            d.g(j3);
                            break;
                    }
                    cursor.requery();
                    listView.invalidateViews();
                    return true;
                }
            case C0000R.id.idEdit /*2131493342*/:
                a(adapterContextMenuInfo.position, adapterContextMenuInfo.id);
                return true;
            case C0000R.id.idReimburse /*2131493345*/:
                long j5 = adapterContextMenuInfo.id;
                int i4 = adapterContextMenuInfo.position;
                Intent intent = new Intent(this, ReimburseDialog.class);
                Cursor cursor2 = (Cursor) this.d.getItemAtPosition(i4);
                if (cursor2 != null) {
                    long j6 = cursor2.getLong(cursor2.getColumnIndexOrThrow("_money"));
                    String string = cursor2.getString(cursor2.getColumnIndexOrThrow("_moneyflag1"));
                    long j7 = cursor2.getLong(cursor2.getColumnIndexOrThrow("_moneytypeid1"));
                    String string2 = cursor2.getString(cursor2.getColumnIndexOrThrow("_name"));
                    long j8 = cursor2.getLong(cursor2.getColumnIndexOrThrow("_ymd"));
                    intent.putExtra("ITEM_TYPE_NAME", string2);
                    intent.putExtra("SUM_MONEY", j6);
                    intent.putExtra("MONEY_FLAG", string);
                    intent.putExtra("MONEY_TYPE", j7);
                    intent.putExtra("MAX_DAY", j8);
                    intent.putExtra("MIN_DAY", j8);
                }
                intent.putExtra("ID_FOR_SQL", String.valueOf(j5));
                startActivityForResult(intent, 19);
                return true;
            default:
                return true;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.g = getIntent().getIntExtra("LaunchedByApplication", 0);
        if (this.g == 0) {
            this.h = abt.a(this, 106, new mt(this), new mq(this));
            this.h.a();
            return;
        }
        a();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.g == 0) {
            MyApp.a = false;
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        setResult(0);
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }
}
