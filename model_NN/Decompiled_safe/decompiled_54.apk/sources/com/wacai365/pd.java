package com.wacai365;

import android.content.Intent;
import android.view.View;

final class pd implements View.OnClickListener {
    private /* synthetic */ SettingReimburseMgr a;

    pd(SettingReimburseMgr settingReimburseMgr) {
        this.a = settingReimburseMgr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.SettingReimburseMgr.a(com.wacai365.SettingReimburseMgr, boolean):void
     arg types: [com.wacai365.SettingReimburseMgr, int]
     candidates:
      com.wacai365.SettingReimburseMgr.a(com.wacai365.SettingReimburseMgr, long):long
      com.wacai365.SettingReimburseMgr.a(long, int):void
      com.wacai365.SettingReimburseMgr.a(com.wacai365.QueryInfo, java.lang.StringBuffer):void
      com.wacai365.SettingReimburseMgr.a(com.wacai365.SettingReimburseMgr, boolean):void */
    public final void onClick(View view) {
        if (view.equals(this.a.g)) {
            this.a.finish();
        } else if (view.equals(this.a.h)) {
            Intent intent = new Intent(this.a, QueryReimburse.class);
            intent.putExtra("QUERYINFO", this.a.o);
            intent.putExtra("LaunchedByApplication", 1);
            this.a.startActivityForResult(intent, 1);
        } else if (view.equals(this.a.d)) {
            SettingReimburseMgr.g(this.a);
        } else if (view.equals(this.a.i)) {
            SettingReimburseMgr.a(this.a, true);
            this.a.a();
        } else if (view.equals(this.a.j)) {
            SettingReimburseMgr.a(this.a, false);
            this.a.a();
        } else if (view.equals(this.a.e)) {
            Intent intent2 = new Intent(this.a, QueryReimburse.class);
            intent2.putExtra("QUERYINFO", this.a.o);
            intent2.putExtra("LaunchedByApplication", 1);
            this.a.startActivityForResult(intent2, 1);
        }
    }
}
