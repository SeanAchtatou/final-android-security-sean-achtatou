package com.wacai365;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.animation.Animation;
import com.wacai.b.k;
import com.wacai.data.ab;

public final class we implements Runnable {
    public boolean a = true;
    public boolean b = true;
    public k c;
    public int d = C0000R.string.txtSucceedPrompt;
    private Activity e;
    private DialogInterface.OnClickListener f;

    public we(Activity activity, DialogInterface.OnClickListener onClickListener) {
        this.e = activity;
        this.f = onClickListener;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void
     arg types: [android.app.Activity, java.lang.String, int, ?[OBJECT, ARRAY]]
     candidates:
      com.wacai365.m.a(android.view.animation.Animation, int, android.view.View, java.lang.String):android.view.animation.Animation
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, java.util.ArrayList, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, int[], android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.app.ProgressDialog, boolean, boolean, android.content.DialogInterface$OnClickListener):com.wacai365.tp
      com.wacai365.m.a(android.content.Context, long, android.content.DialogInterface$OnClickListener, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, long, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, boolean, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener, boolean):void
     arg types: [android.app.Activity, int, int, android.content.DialogInterface$OnClickListener, int]
     candidates:
      com.wacai365.m.a(android.content.Context, android.view.animation.Animation, int, android.view.View, int):android.view.animation.Animation
      com.wacai365.m.a(android.view.animation.Animation, int, android.view.View, int, int):android.view.animation.Animation
      com.wacai365.m.a(android.content.Context, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, boolean, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener, boolean):void */
    public final void run() {
        switch (this.c.d) {
            case 0:
                if (!this.b) {
                    return;
                }
                m.a((Context) this.e, this.d, true, this.f, true);
                return;
            case 50:
            case 51:
                if (this.a) {
                    m.a((Animation) null, 0, (View) null, this.c.c);
                    m.b(this.e);
                    return;
                }
                m.a((Context) this.e, ab.b("\\n", "\n", this.c.c), false, (DialogInterface.OnClickListener) null);
                return;
            case 10001:
                m.a((Context) this.e, this.d, true, this.f, true);
                return;
            default:
                m.a((Context) this.e, ab.b("\\n", "\n", this.c.c), false, (DialogInterface.OnClickListener) null);
                return;
        }
    }
}
