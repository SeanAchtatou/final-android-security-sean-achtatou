package com.wacai365;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.wacai.e;

public class WhiteListMgr extends WacaiActivity {
    /* access modifiers changed from: private */
    public Button a;
    /* access modifiers changed from: private */
    public Button b;
    private ListView c;
    private ListAdapter d;
    /* access modifiers changed from: private */
    public int e = -1;
    private View.OnClickListener f = new ah(this);

    /* access modifiers changed from: private */
    public void a() {
        Cursor rawQuery = e.c().b().rawQuery("select id as _id, name as _name, address as _address from TBL_WHITELIST", null);
        startManagingCursor(rawQuery);
        this.d = new mu(this, this, C0000R.layout.white_list_item, rawQuery, new String[]{"_name", "_address"}, new int[]{C0000R.id.listitem1, C0000R.id.listitem2});
        this.c.setAdapter(this.d);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        Cursor cursor = (Cursor) this.c.getItemAtPosition(((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).position);
        long j = cursor != null ? cursor.getLong(cursor.getColumnIndexOrThrow("_id")) : -1;
        switch (menuItem.getItemId()) {
            case C0000R.id.idDelete /*2131493338*/:
                e.c().b().execSQL(String.format("delete from TBL_WHITELIST where id = %d", Long.valueOf(j)));
                a();
                break;
            case C0000R.id.idEdit /*2131493342*/:
                this.e = (int) j;
                showDialog(8);
                break;
        }
        return false;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.white_item_list);
        this.a = (Button) findViewById(C0000R.id.btnAdd);
        this.a.setOnClickListener(this.f);
        this.b = (Button) findViewById(C0000R.id.btnCancel);
        this.b.setOnClickListener(this.f);
        this.c = (ListView) findViewById(C0000R.id.IOList);
        a();
        this.c.setOnItemClickListener(new bf(this));
        this.c.setOnCreateContextMenuListener(new bd(this));
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        Cursor rawQuery;
        super.onCreateDialog(i);
        switch (i) {
            case 8:
                View inflate = LayoutInflater.from(this).inflate((int) C0000R.layout.white_member, (ViewGroup) null);
                TextView textView = (TextView) inflate.findViewById(C0000R.id.input_name);
                TextView textView2 = (TextView) inflate.findViewById(C0000R.id.input_address);
                textView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
                textView2.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
                if (!(-1 == this.e || (rawQuery = e.c().b().rawQuery(String.format("select * from TBL_WHITELIST where id = %d", Integer.valueOf(this.e)), null)) == null)) {
                    if (rawQuery.getCount() > 0) {
                        rawQuery.moveToNext();
                        String string = rawQuery.getString(rawQuery.getColumnIndexOrThrow("name"));
                        String string2 = rawQuery.getString(rawQuery.getColumnIndexOrThrow("address"));
                        textView.setText(string);
                        textView2.setText(string2);
                    }
                    rawQuery.close();
                }
                return new AlertDialog.Builder(this).setView(inflate).setTitle((int) C0000R.string.txtWhiteMemberDetail).setPositiveButton((int) C0000R.string.txtOK, new ae(this, textView2, textView)).setNegativeButton((int) C0000R.string.txtCancel, new ac(this)).setOnKeyListener(new aj(this)).create();
            default:
                return null;
        }
    }
}
