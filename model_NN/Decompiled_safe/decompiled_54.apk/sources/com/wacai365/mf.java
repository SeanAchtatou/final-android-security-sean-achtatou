package com.wacai365;

import android.content.Intent;
import android.view.View;

final class mf implements View.OnClickListener {
    private /* synthetic */ AccountConfig a;

    mf(AccountConfig accountConfig) {
        this.a = accountConfig;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.c)) {
            AccountConfig.b(this.a);
        } else if (view.equals(this.a.d)) {
            this.a.finish();
        } else if (view.equals(this.a.e)) {
            Intent intent = new Intent(this.a, AccountRegister.class);
            intent.putExtra("Extra_AlertAccount", this.a.i);
            this.a.startActivityForResult(intent, 13);
        }
    }
}
