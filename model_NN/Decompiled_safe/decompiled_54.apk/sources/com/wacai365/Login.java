package com.wacai365;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.a.f;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.g;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.o;
import com.wacai.b.u;

public class Login extends WacaiActivity implements View.OnClickListener {
    DialogInterface.OnClickListener a = new zc(this);
    private View[] b;
    private TextView c;
    private Button d;
    private Button e;
    private Button f;
    private View g;
    private View h;
    private Button i;
    private Button j;
    private boolean k;
    private fy l = null;
    private boolean m = false;
    private DialogInterface.OnClickListener n = new yz(this);

    static /* synthetic */ void a() {
        b.m().e("");
        b.m().a(false);
        b.m().l();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void a(Login login) {
        ft ftVar = new ft(login);
        ftVar.b(false);
        ftVar.c(false);
        ftVar.a((tt) null);
        ftVar.a(login.a);
        login.l = ftVar;
        c cVar = new c(ftVar);
        cVar.a((o) new u());
        ftVar.a(cVar);
        ftVar.b(C0000R.string.txtRestore, C0000R.string.txtRestoring);
        ftVar.a(true, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void a(Login login, String str, String str2) {
        ft ftVar = new ft(login);
        ftVar.d(false);
        ftVar.a(login.a);
        ftVar.a((int) C0000R.string.txtResetLocalPSWSucceedPrompt);
        ftVar.a(new yy(login, str2));
        login.l = ftVar;
        c cVar = new c(ftVar);
        l lVar = new l();
        lVar.b(str);
        lVar.c(str2);
        lVar.a(m.a());
        cVar.a((o) lVar);
        cVar.a((o) new g());
        ftVar.a(cVar);
        ftVar.b(C0000R.string.networkProgress, C0000R.string.remoteVerifyUser);
        ftVar.a(true, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    static /* synthetic */ void b(Login login) {
        Intent intent = login.getIntent();
        intent.putExtra("Restart_NoPwd", true);
        intent.putExtra("isneedupdate", login.m);
        login.setResult(-1, intent);
        login.finish();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (-1 == i3) {
            this.m = true;
            if (intent != null && this.l != null) {
                this.l.a(i2, i3, intent);
            }
        }
    }

    public void onClick(View view) {
        if (view == this.d) {
            Intent a2 = InputTrade.a(this, "", 0);
            a2.putExtra("LaunchedByApplication", 1);
            startActivityForResult(a2, 0);
        } else if (view == this.e) {
            Intent intent = new Intent(this, MyShortcuts.class);
            intent.putExtra("LaunchedByApplication", 1);
            startActivityForResult(intent, 0);
        } else if (view == this.f) {
            String obj = this.c.getText().toString();
            if (obj == null || obj.length() == 0) {
                m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtEmptyPassword);
            } else if (f.a(obj).equals(b.m().g())) {
                MyApp.a = true;
                getIntent().putExtra("isneedupdate", this.m);
                setResult(-1, getIntent());
                finish();
            } else {
                this.c.setText("");
                m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtMisPassword);
            }
        } else if (view == this.g) {
            String obj2 = this.c.getText().toString();
            if (obj2 != null && obj2.length() > 0) {
                this.c.setText(obj2.substring(0, obj2.length() - 1));
            }
        } else if (view == this.h) {
            this.c.setText("");
        } else if (view == this.i) {
            setResult(0, getIntent());
            finish();
        } else if (view.equals(this.j)) {
            m.a(this, getString(C0000R.string.txtForgetPasswordAlertTitle), getString(C0000R.string.forgetPwdSynReset), C0000R.string.txtResetLocalPSW, C0000R.string.txtCancel, C0000R.string.txtRestore, this.n);
        } else {
            for (int i2 = 0; i2 < this.b.length; i2++) {
                if (view == this.b[i2]) {
                    this.c.append(String.valueOf(i2));
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        this.k = getIntent().getBooleanExtra("login_hideinput", false);
        if (!this.k) {
            setTheme((int) C0000R.style.WaicaiNoTitleTheme);
            this.D = false;
            this.E = false;
        }
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.login);
        this.c = (TextView) findViewById(C0000R.id.etPwd);
        this.d = (Button) findViewById(C0000R.id.record);
        this.d.setOnClickListener(this);
        this.e = (Button) findViewById(C0000R.id.quickRecord);
        this.e.setOnClickListener(this);
        this.f = (Button) findViewById(C0000R.id.btnOK);
        this.f.setOnClickListener(this);
        this.g = findViewById(C0000R.id.del);
        this.g.setOnClickListener(this);
        this.i = (Button) findViewById(C0000R.id.btnCancel);
        this.i.setOnClickListener(this);
        this.h = findViewById(C0000R.id.btnClear);
        this.h.setOnClickListener(this);
        this.j = (Button) findViewById(C0000R.id.btnForgetPwd);
        this.j.setOnClickListener(this);
        if (this.k) {
            setTitle((int) C0000R.string.changePswLoginTitle);
            ((LinearLayout) findViewById(C0000R.id.layout_login_inputio)).setVisibility(8);
            this.j.setText("");
            this.j.setEnabled(false);
        }
        int[] iArr = {C0000R.id.key_0, C0000R.id.key_1, C0000R.id.key_2, C0000R.id.key_3, C0000R.id.key_4, C0000R.id.key_5, C0000R.id.key_6, C0000R.id.key_7, C0000R.id.key_8, C0000R.id.key_9};
        this.b = new View[iArr.length];
        for (int i2 = 0; i2 < this.b.length; i2++) {
            this.b[i2] = findViewById(iArr[i2]);
            this.b[i2].setOnClickListener(this);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        super.onCreateDialog(i2);
        switch (i2) {
            case 11:
                View inflate = LayoutInflater.from(this).inflate((int) C0000R.layout.user_login, (ViewGroup) null);
                boolean p = b.m().p();
                if (p) {
                    ((TextView) inflate.findViewById(C0000R.id.nameTitle)).setText((int) C0000R.string.txtEmail);
                    inflate.findViewById(C0000R.id.tvNote).setVisibility(0);
                }
                EditText editText = (EditText) inflate.findViewById(C0000R.id.input_name);
                EditText editText2 = (EditText) inflate.findViewById(C0000R.id.input_password);
                editText.setText("");
                editText2.setText("");
                return new AlertDialog.Builder(this).setView(inflate).setTitle((int) C0000R.string.txtResetLocalPSW).setPositiveButton((int) C0000R.string.txtResetString, new zb(this, editText, editText2, p)).setNegativeButton((int) C0000R.string.txtCancel, (DialogInterface.OnClickListener) null).create();
            default:
                return null;
        }
    }
}
