package com.wacai365;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

final class qf extends SimpleCursorAdapter {
    private String[] a;
    private int[] b;

    public qf(Context context, int i, Cursor cursor, String[] strArr, int[] iArr) {
        super(context, i, cursor, strArr, iArr);
        this.b = iArr;
        this.a = strArr;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        String string;
        View inflate = view == null ? ((LayoutInflater) MyApp.b.getSystemService("layout_inflater")).inflate((int) C0000R.layout.list_item_line2, (ViewGroup) null) : view;
        if (inflate != null) {
            View findViewById = inflate.findViewById(this.b[0]);
            if (!(findViewById == null || (string = cursor.getString(cursor.getColumnIndexOrThrow(this.a[0]))) == null)) {
                setViewText((TextView) findViewById, string);
            }
            View findViewById2 = inflate.findViewById(this.b[1]);
            if (findViewById2 == null) {
                return;
            }
            if (cursor.getInt(cursor.getColumnIndexOrThrow(this.a[1])) == 1) {
                ((TextView) findViewById2).setText((int) C0000R.string.txtIncomeString);
            } else {
                ((TextView) findViewById2).setText((int) C0000R.string.txtOutgoString);
            }
        }
    }
}
