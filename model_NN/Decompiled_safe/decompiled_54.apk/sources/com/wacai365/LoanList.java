package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.wacai.e;
import java.util.ArrayList;

public class LoanList extends WacaiActivity {
    /* access modifiers changed from: private */
    public ListView a;
    private TextView b;
    /* access modifiers changed from: private */
    public Button c;
    /* access modifiers changed from: private */
    public Button d;
    /* access modifiers changed from: private */
    public Button e;
    private long f = 0;
    /* access modifiers changed from: private */
    public long g;
    private View.OnClickListener h = new na(this);

    private ArrayList a(Cursor cursor) {
        long j = 0;
        if (cursor == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        while (cursor.moveToNext()) {
            long j2 = cursor.getLong(cursor.getColumnIndex("_ct"));
            long j3 = cursor.getLong(cursor.getColumnIndex("_ymd"));
            yb ybVar = new yb(this);
            ybVar.a = j3;
            ybVar.c = j;
            ybVar.d = j + j2;
            long j4 = j3 / 10000;
            StringBuffer stringBuffer = new StringBuffer(64);
            stringBuffer.append(j4);
            stringBuffer.append("年");
            stringBuffer.append((j3 / 100) % 100);
            stringBuffer.append("月");
            ybVar.b = stringBuffer.toString();
            j = ybVar.d + 1;
            arrayList.add(ybVar);
        }
        cursor.close();
        return arrayList;
    }

    private void a() {
        this.a.setAdapter((ListAdapter) new nk(this, null, null, null));
        this.a.setVisibility(8);
        this.b.setText((int) C0000R.string.txtLoading);
        this.b.setVisibility(0);
        nc ncVar = new nc(this);
        this.f = ncVar.getId();
        ncVar.start();
    }

    static /* synthetic */ void a(LoanList loanList) {
        StringBuffer stringBuffer = new StringBuffer(3000);
        stringBuffer.append("select count(*) as _ct, _ymd from ( select a.ymd as _ymd, a.date as _outgodate from TBL_LOAN a, TBL_ACCOUNTINFO b, TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and b.id = a.accountid and c.id = a.debtid and b.moneytype = d.id and c.moneytype = e.id and c.id = " + String.valueOf(loanList.g) + " group by a.id UNION ALL " + "select a.ymd as _ymd, a.date as _outgodate from TBL_PAYBACK a, TBL_ACCOUNTINFO b, " + "TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and " + "b.id = a.accountid and c.id = a.debtid and b.moneytype = d.id and c.moneytype = e.id " + "and c.id = " + String.valueOf(loanList.g) + " group by a.id) group by  _ymd/100 order by _outgodate DESC");
        ArrayList a2 = loanList.a(e.c().b().rawQuery(stringBuffer.toString(), null));
        StringBuffer stringBuffer2 = new StringBuffer(3000);
        stringBuffer2.append("select * from ( select 0 as _typeid, a.ymd as _ymd, 3 as _flag, a.id as _id, a.date as _outgodate, a.money as _money, a.comment as _comment, 0 as _scheduleid, 0 as _paybackid, 0 as _name, a.type as _type, 0 as _maintypename, 0 as _transferoutmoney, 0 as _transferinmoney, b.name as _transferoutname, c.name as _transferinname, a.accountid as _transferoutaccountid, a.debtid as _transferinaccountid, 0 as _transinmoney, d.flag as _moneyflag1, e.flag as _moneyflag2, d.id as _moneytypeid1, e.id as _moneytypeid2 from TBL_LOAN a, TBL_ACCOUNTINFO b,  TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and b.id = a.accountid and c.id = a.debtid and b.moneytype = d.id and c.moneytype = e.id and c.id = " + String.valueOf(loanList.g) + " group by a.id UNION select 0 as _typeid, a.ymd as _ymd, " + "4 as _flag, a.id as _id, a.date as _outgodate, a.money as _money, a.comment as _comment, " + "0 as _scheduleid, 0 as _paybackid, 0 as _name, a.type as _type, 0 as _maintypename, 0 as " + "_transferoutmoney, 0 as _transferinmoney, b.name as _transferoutname, c.name as _transferinname, " + "a.accountid as _transferoutaccountid, a.debtid as _transferinaccountid, 0 as _transinmoney" + ", d.flag as _moneyflag1, e.flag as _moneyflag2, d.id as _moneytypeid1, e.id as _moneytypeid2 " + "from TBL_PAYBACK a, TBL_ACCOUNTINFO b,  TBL_ACCOUNTINFO c, TBL_MONEYTYPE d" + ", TBL_MONEYTYPE e where a.isdelete = 0 and b.id = a.accountid and c.id = a.debtid " + "and b.moneytype = d.id and c.moneytype = e.id and c.id = " + String.valueOf(loanList.g) + " group by a.id) order by _outgodate DESC");
        Cursor rawQuery = e.c().b().rawQuery(stringBuffer2.toString(), null);
        loanList.startManagingCursor(rawQuery);
        if (loanList.f == Thread.currentThread().getId()) {
            loanList.runOnUiThread(new my(loanList, new nk(loanList, rawQuery, a2, loanList)));
        }
    }

    static /* synthetic */ void a(LoanList loanList, int i) {
        wf wfVar;
        int i2;
        long j;
        Object itemAtPosition = loanList.a.getItemAtPosition(i);
        if (itemAtPosition.getClass() != yb.class && (wfVar = (wf) itemAtPosition) != null && wfVar.a > 0) {
            int i3 = wfVar.f;
            long j2 = wfVar.a;
            if ((i3 == 0 || i3 == 1) && wfVar.j > 0) {
                i2 = 4;
                j = wfVar.j;
            } else {
                i2 = i3;
                j = j2;
            }
            Intent a2 = InputTrade.a(loanList, "", j);
            a2.putExtra("LaunchedByApplication", 1);
            a2.putExtra("Extra_Type", i2);
            a2.putExtra("Extra_Id", j);
            a2.putExtra("Extra_Type2", wfVar.g);
            loanList.startActivityForResult(a2, 19);
        }
    }

    static /* synthetic */ void g(LoanList loanList) {
        if (loanList.a.getAdapter().getCount() == 0) {
            loanList.b.setText((int) C0000R.string.txtNoLoanRecord);
            loanList.b.setVisibility(0);
            loanList.a.setVisibility(8);
            return;
        }
        loanList.b.setVisibility(8);
        loanList.a.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (-1 != i2) {
            return;
        }
        if (intent.getBooleanExtra("IS_LOAD_ACCOUNT_DELETE", false)) {
            finish();
        } else {
            a();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.loan_list);
        this.e = (Button) findViewById(C0000R.id.btnCancel);
        this.d = (Button) findViewById(C0000R.id.btnEditAccount);
        this.c = (Button) findViewById(C0000R.id.btnCount);
        this.b = (TextView) findViewById(C0000R.id.id_listhint);
        this.e.setOnClickListener(this.h);
        this.d.setOnClickListener(this.h);
        this.c.setOnClickListener(this.h);
        this.g = getIntent().getLongExtra("LOANACCOUNTID", 0);
        this.a = (ListView) findViewById(C0000R.id.IOList);
        this.a.setOnItemClickListener(new ne(this));
        a();
    }
}
