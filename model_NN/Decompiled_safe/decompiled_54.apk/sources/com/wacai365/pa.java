package com.wacai365;

import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;

final class pa implements View.OnCreateContextMenuListener {
    private /* synthetic */ SettingReimburseMgr a;

    pa(SettingReimburseMgr settingReimburseMgr) {
        this.a = settingReimburseMgr;
    }

    public final void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.clear();
        MenuInflater menuInflater = this.a.getMenuInflater();
        if (this.a.b.getItemAtPosition(((AdapterView.AdapterContextMenuInfo) contextMenuInfo).position).getClass() != ry.class) {
            menuInflater.inflate(C0000R.menu.reimburse_list_context, contextMenu);
        }
    }
}
