package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import com.wacai.e;

public class SettingBudgetMgr extends WacaiActivity {
    int a = -1;
    /* access modifiers changed from: private */
    public Button b;
    /* access modifiers changed from: private */
    public Button c;
    /* access modifiers changed from: private */
    public ListView d;
    private ListAdapter e;
    private DialogInterface.OnClickListener f = new w(this);
    private View.OnClickListener g = new v(this);

    static /* synthetic */ void a(SettingBudgetMgr settingBudgetMgr, int i) {
        Cursor cursor = (Cursor) settingBudgetMgr.d.getItemAtPosition(i);
        long j = cursor != null ? cursor.getLong(cursor.getColumnIndexOrThrow("_id")) : 0;
        Intent intent = new Intent(settingBudgetMgr, InputBudget.class);
        intent.putExtra("Record_Id", j);
        settingBudgetMgr.startActivityForResult(intent, 0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        super.onContextItemSelected(menuItem);
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        switch (menuItem.getItemId()) {
            case C0000R.id.idDelete /*2131493338*/:
                this.a = adapterContextMenuInfo.position;
                m.a(this, (int) C0000R.string.txtDeleteConfirm, this.f);
                return false;
            default:
                return false;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.setting_item_list);
        this.b = (Button) findViewById(C0000R.id.btnAdd);
        this.b.setOnClickListener(this.g);
        this.c = (Button) findViewById(C0000R.id.btnCancel);
        this.c.setOnClickListener(this.g);
        this.d = (ListView) findViewById(C0000R.id.IOList);
        Cursor rawQuery = e.c().b().rawQuery("select a.id as _id, b.name as _name from TBL_BUDGETINFO a, TBL_MEMBERINFO b where a.memberid = b.id GROUP BY a.memberid", null);
        startManagingCursor(rawQuery);
        this.e = new SimpleCursorAdapter(this, C0000R.layout.list_item_withoutcheckable, rawQuery, new String[]{"_name"}, new int[]{C0000R.id.listitem1});
        this.d.setAdapter(this.e);
        this.d.setOnItemClickListener(new u(this));
        this.d.setOnCreateContextMenuListener(new t(this));
    }
}
