package com.wacai365;

import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;

public class MyBalanceStatByMoneyType extends WacaiActivity {
    private QueryInfo a;
    private ListAdapter b;
    private ListView c = null;
    private Button d = null;
    /* access modifiers changed from: private */
    public Button e = null;
    /* access modifiers changed from: private */
    public Button f = null;
    /* access modifiers changed from: private */
    public Button g = null;
    /* access modifiers changed from: private */
    public ArrayList h = null;
    /* access modifiers changed from: private */
    public int i = 0;
    private int[] j = null;
    private Hashtable k = new Hashtable();
    private View.OnClickListener l = new um(this);

    /* access modifiers changed from: private */
    public void a() {
        if (this.h.size() > 0) {
            qa qaVar = (qa) this.h.get(this.i);
            this.g.setText(qaVar.a);
            this.b = new fo(this, this, qaVar);
            this.c.setAdapter(this.b);
            return;
        }
        this.g.setText("");
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x009d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r14, java.lang.String r15) {
        /*
            r13 = this;
            r2 = 0
            com.wacai.e r0 = com.wacai.e.c()     // Catch:{ Exception -> 0x00c8, all -> 0x00c5 }
            android.database.sqlite.SQLiteDatabase r0 = r0.b()     // Catch:{ Exception -> 0x00c8, all -> 0x00c5 }
            r1 = 0
            android.database.Cursor r1 = r0.rawQuery(r15, r1)     // Catch:{ Exception -> 0x00c8, all -> 0x00c5 }
            if (r1 != 0) goto L_0x0016
            if (r1 == 0) goto L_0x0015
            r1.close()
        L_0x0015:
            return
        L_0x0016:
            r1.moveToFirst()     // Catch:{ Exception -> 0x008c, all -> 0x009a }
        L_0x0019:
            java.lang.String r0 = "_totalMoney"
            int r0 = r1.getColumnIndexOrThrow(r0)     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            long r2 = r1.getLong(r0)     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            java.lang.String r0 = "_moneytype"
            int r0 = r1.getColumnIndexOrThrow(r0)     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            long r4 = r1.getLong(r0)     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            java.lang.String r0 = "_orderno"
            int r0 = r1.getColumnIndexOrThrow(r0)     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            long r6 = r1.getLong(r0)     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            java.lang.String r0 = "_name"
            int r0 = r1.getColumnIndexOrThrow(r0)     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            java.lang.String r8 = r1.getString(r0)     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            java.lang.String r0 = "_flag"
            int r0 = r1.getColumnIndexOrThrow(r0)     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            java.lang.String r9 = r1.getString(r0)     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            java.lang.String r0 = "%d"
            r10 = 1
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            r11 = 0
            java.lang.Long r12 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            r10[r11] = r12     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            java.lang.String r10 = java.lang.String.format(r0, r10)     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            java.util.Hashtable r0 = r13.k     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            java.lang.Object r0 = r0.get(r10)     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            com.wacai365.qa r0 = (com.wacai365.qa) r0     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            if (r0 != 0) goto L_0x006a
            com.wacai365.qa r0 = new com.wacai365.qa     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            r0.<init>(r13)     // Catch:{ Exception -> 0x008c, all -> 0x009a }
        L_0x006a:
            r0.d = r4     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            r0.a = r8     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            r0.b = r9     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            r0.c = r6     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            switch(r14) {
                case 0: goto L_0x0086;
                case 1: goto L_0x0094;
                case 2: goto L_0x00a1;
                case 3: goto L_0x00a7;
                case 4: goto L_0x00ad;
                case 5: goto L_0x00b3;
                case 6: goto L_0x00b9;
                case 7: goto L_0x00bf;
                default: goto L_0x0075;
            }     // Catch:{ Exception -> 0x008c, all -> 0x009a }
        L_0x0075:
            java.util.Hashtable r2 = r13.k     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            r2.put(r10, r0)     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            if (r0 != 0) goto L_0x0019
            if (r1 == 0) goto L_0x0015
            r1.close()
            goto L_0x0015
        L_0x0086:
            long r4 = r0.e     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            long r2 = r2 + r4
            r0.e = r2     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            goto L_0x0075
        L_0x008c:
            r0 = move-exception
            r0 = r1
        L_0x008e:
            if (r0 == 0) goto L_0x0015
            r0.close()
            goto L_0x0015
        L_0x0094:
            long r4 = r0.f     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            long r2 = r2 + r4
            r0.f = r2     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            goto L_0x0075
        L_0x009a:
            r0 = move-exception
        L_0x009b:
            if (r1 == 0) goto L_0x00a0
            r1.close()
        L_0x00a0:
            throw r0
        L_0x00a1:
            long r4 = r0.h     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            long r2 = r2 + r4
            r0.h = r2     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            goto L_0x0075
        L_0x00a7:
            long r4 = r0.g     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            long r2 = r2 + r4
            r0.g = r2     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            goto L_0x0075
        L_0x00ad:
            long r4 = r0.i     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            long r2 = r2 + r4
            r0.i = r2     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            goto L_0x0075
        L_0x00b3:
            long r4 = r0.j     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            long r2 = r2 + r4
            r0.j = r2     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            goto L_0x0075
        L_0x00b9:
            long r4 = r0.k     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            long r2 = r2 + r4
            r0.k = r2     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            goto L_0x0075
        L_0x00bf:
            long r4 = r0.l     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            long r2 = r2 + r4
            r0.l = r2     // Catch:{ Exception -> 0x008c, all -> 0x009a }
            goto L_0x0075
        L_0x00c5:
            r0 = move-exception
            r1 = r2
            goto L_0x009b
        L_0x00c8:
            r0 = move-exception
            r0 = r2
            goto L_0x008e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.MyBalanceStatByMoneyType.a(int, java.lang.String):void");
    }

    static /* synthetic */ void a(MyBalanceStatByMoneyType myBalanceStatByMoneyType, boolean z) {
        if (myBalanceStatByMoneyType.h != null) {
            int size = myBalanceStatByMoneyType.h.size();
            if (z) {
                if (myBalanceStatByMoneyType.i == 0) {
                    myBalanceStatByMoneyType.i = size - 1;
                } else {
                    myBalanceStatByMoneyType.i--;
                }
            } else if (myBalanceStatByMoneyType.i < size - 1) {
                myBalanceStatByMoneyType.i++;
            } else {
                myBalanceStatByMoneyType.i = 0;
            }
        }
    }

    private void a(ArrayList arrayList) {
        Enumeration elements = this.k.elements();
        while (elements.hasMoreElements()) {
            qa qaVar = (qa) elements.nextElement();
            if (qaVar != null) {
                arrayList.add(qaVar);
            }
        }
        Collections.sort(arrayList, new fj(this));
    }

    private void a(boolean z) {
        int i2 = 1;
        StringBuffer stringBuffer = new StringBuffer(2000);
        Object[] objArr = new Object[1];
        if (z) {
            i2 = 0;
        }
        objArr[0] = Integer.valueOf(i2);
        stringBuffer.append(String.format("select sum(a.money) as _totalMoney, e.moneytype as _moneytype, h.name as _name, h.flag as _flag, h.orderno as _orderno from TBL_LOAN a, TBL_ACCOUNTINFO e, TBL_MONEYTYPE h where a.accountid = e.id and e.moneytype = h.id and a.id in (select a.id as _id from TBL_LOAN a, TBL_ACCOUNTINFO b,  TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and a.type = %d and b.id = a.accountid and c.id = a.debtid and b.moneytype = d.id and c.moneytype = e.id ", objArr));
        MyBalance.a(this.a, stringBuffer);
        stringBuffer.append(" group by a.id) group by e.moneytype");
        a(z ? 5 : 4, stringBuffer.toString());
    }

    private void b(boolean z) {
        int i2 = 1;
        StringBuffer stringBuffer = new StringBuffer(2000);
        Object[] objArr = new Object[1];
        if (z) {
            i2 = 0;
        }
        objArr[0] = Integer.valueOf(i2);
        stringBuffer.append(String.format("select sum(a.money) as _totalMoney, e.moneytype as _moneytype, h.name as _name, h.flag as _flag, h.orderno as _orderno from TBL_PAYBACK a, TBL_ACCOUNTINFO e, TBL_MONEYTYPE h where a.accountid = e.id and e.moneytype = h.id and a.id in (select a.id as _id from TBL_PAYBACK a, TBL_ACCOUNTINFO b,  TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and a.type = %d and b.id = a.accountid and c.id = a.debtid and b.moneytype = d.id and c.moneytype = e.id ", objArr));
        MyBalance.a(this.a, stringBuffer);
        stringBuffer.append(" group by a.id) group by e.moneytype");
        a(z ? 6 : 7, stringBuffer.toString());
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x04f3  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0276  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x029f  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0306  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x037b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r13) {
        /*
            r12 = this;
            r11 = 15
            r10 = 4
            r9 = -1
            r8 = 1
            r7 = 0
            super.onCreate(r13)
            r0 = 2130903178(0x7f03008a, float:1.7413167E38)
            r12.setContentView(r0)
            android.content.Intent r0 = r12.getIntent()
            java.lang.String r1 = "QUERYINFO"
            android.os.Parcelable r0 = r0.getParcelableExtra(r1)
            com.wacai365.QueryInfo r0 = (com.wacai365.QueryInfo) r0
            r12.a = r0
            r0 = 2131493291(0x7f0c01ab, float:1.8610058E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.ListView r0 = (android.widget.ListView) r0
            r12.c = r0
            r0 = 2131492870(0x7f0c0006, float:1.8609204E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r12.d = r0
            android.widget.Button r0 = r12.d
            com.wacai365.uj r1 = new com.wacai365.uj
            r1.<init>(r12)
            r0.setOnClickListener(r1)
            r0 = 2131492988(0x7f0c007c, float:1.8609443E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r12.e = r0
            android.widget.Button r0 = r12.e
            android.view.View$OnClickListener r1 = r12.l
            r0.setOnClickListener(r1)
            r0 = 2131492986(0x7f0c007a, float:1.860944E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r12.f = r0
            android.widget.Button r0 = r12.f
            android.view.View$OnClickListener r1 = r12.l
            r0.setOnClickListener(r1)
            r0 = 2131493292(0x7f0c01ac, float:1.861006E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r12.g = r0
            android.widget.Button r0 = r12.g
            android.view.View$OnClickListener r1 = r12.l
            r0.setOnClickListener(r1)
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.t
            r0 = r0 & 1
            if (r0 == 0) goto L_0x0098
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r1 = 2000(0x7d0, float:2.803E-42)
            r0.<init>(r1)
            java.lang.String r1 = "select sum(a.money) as _totalMoney, e.moneytype as _moneytype, h.name as _name, h.flag as _flag, h.orderno as _orderno from tbl_outgoinfo a, TBL_ACCOUNTINFO e, TBL_MONEYTYPE h where a.accountid = e.id and e.moneytype = h.id and a.id in (select a.id as _id from tbl_outgoinfo a, tbl_outgomemberinfo b, TBL_OUTGOSUBTYPEINFO c, TBL_PROJECTINFO d, TBL_ACCOUNTINFO e, TBL_MEMBERINFO f, TBL_OUTGOMAINTYPEINFO g, TBL_MONEYTYPE h where a.isdelete = 0 and b.outgoid = a.id and c.id = a.subtypeid and d.id = a.projectid and e.id = a.accountid and f.id = b.memberid and g.id = c.id / 10000 and e.moneytype = h.id "
            r0.append(r1)
            com.wacai365.QueryInfo r1 = r12.a
            com.wacai365.MyBalance.a(r1, r7, r0)
            java.lang.String r1 = " group by a.id ) group by e.moneytype"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r12.a(r7, r0)
        L_0x0098:
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.t
            r0 = r0 & 2
            if (r0 == 0) goto L_0x00bd
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r1 = 2000(0x7d0, float:2.803E-42)
            r0.<init>(r1)
            java.lang.String r1 = "select sum(a.money) as _totalMoney, e.moneytype as _moneytype, h.name as _name, h.flag as _flag, h.orderno as _orderno from TBL_INCOMEINFO a, TBL_ACCOUNTINFO e, TBL_MONEYTYPE h where a.accountid = e.id and e.moneytype = h.id and a.id in (select a.id as _id from TBL_INCOMEINFO a, TBL_INCOMEMEMBERINFO b, TBL_INCOMEMAINTYPEINFO c, TBL_PROJECTINFO d, TBL_ACCOUNTINFO e, TBL_MEMBERINFO f, TBL_MONEYTYPE g where a.isdelete = 0 and b.incomeid = a.id and c.id = a.typeid and d.id = a.projectid and e.id = a.accountid and f.id = b.memberid and e.moneytype = g.id "
            r0.append(r1)
            com.wacai365.QueryInfo r1 = r12.a
            com.wacai365.MyBalance.b(r1, r7, r0)
            java.lang.String r1 = " group by a.id) group by e.moneytype"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r12.a(r8, r0)
        L_0x00bd:
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.t
            r0 = r0 & 4
            if (r0 == 0) goto L_0x03f9
            java.lang.String r0 = "select sum(a.transferoutmoney) as _totalMoney, e.moneytype as _moneytype, h.name as _name, h.flag as _flag, h.orderno as _orderno from TBL_TRANSFERINFO a, TBL_ACCOUNTINFO e, TBL_MONEYTYPE h where a.transferoutaccountid = e.id and e.moneytype = h.id and a.id in (select a.id as _id from TBL_TRANSFERINFO a, TBL_ACCOUNTINFO b,  TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and a.type = 0 and b.id = a.transferoutaccountid and c.id = a.transferinaccountid and b.moneytype = d.id and c.moneytype = e.id "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            com.wacai365.QueryInfo r1 = r12.a
            java.lang.String r2 = ""
            r3 = -1
            long r5 = r1.j
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 == 0) goto L_0x00fd
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.transferoutmoney >= %d"
            java.lang.Object[] r4 = new java.lang.Object[r8]
            long r5 = r1.j
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x00fd:
            r3 = -1
            long r5 = r1.k
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 == 0) goto L_0x0126
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.transferoutmoney <= %d"
            java.lang.Object[] r4 = new java.lang.Object[r8]
            long r5 = r1.k
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x0126:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.ymd >= %d"
            java.lang.Object[] r4 = new java.lang.Object[r8]
            long r5 = r1.b
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.ymd <= %d"
            java.lang.Object[] r4 = new java.lang.Object[r8]
            long r5 = r1.c
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            int r3 = r1.t
            if (r10 != r3) goto L_0x04a5
            int r3 = r1.n
            if (r9 == r3) goto L_0x0191
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.transferoutaccountid = %d"
            java.lang.Object[] r4 = new java.lang.Object[r8]
            int r5 = r1.n
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x0191:
            int r3 = r1.o
            if (r9 == r3) goto L_0x01b6
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.transferinaccountid = %d"
            java.lang.Object[] r4 = new java.lang.Object[r8]
            int r5 = r1.o
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x01b6:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and d.id = %d "
            java.lang.Object[] r4 = new java.lang.Object[r8]
            com.wacai.b r5 = com.wacai.b.m()
            long r5 = r5.j()
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x01dd:
            int r3 = r1.t
            if (r10 == r3) goto L_0x0544
            int r3 = r1.f
            if (r9 == r3) goto L_0x01fe
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.id = 0"
            java.lang.Object[] r4 = new java.lang.Object[r7]
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x01fe:
            java.lang.String r3 = r1.h
            int r3 = r3.length()
            if (r3 <= 0) goto L_0x021f
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.id = 0"
            java.lang.Object[] r4 = new java.lang.Object[r7]
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x021f:
            r3 = -1
            long r5 = r1.l
            int r1 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r1 == 0) goto L_0x0544
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " and a.id = 0"
            java.lang.Object[] r3 = new java.lang.Object[r7]
            java.lang.String r2 = java.lang.String.format(r2, r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
        L_0x0240:
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = " group by a.id) group by e.moneytype"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 2
            r12.a(r1, r0)
            java.lang.String r0 = "select sum(a.transferinmoney) as _totalMoney, e.moneytype as _moneytype, h.name as _name, h.flag as _flag, h.orderno as _orderno from TBL_TRANSFERINFO a, TBL_ACCOUNTINFO e, TBL_MONEYTYPE h where a.transferinaccountid = e.id and e.moneytype = h.id and a.id in (select a.id as _id from TBL_TRANSFERINFO a, TBL_ACCOUNTINFO b,  TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and a.type = 0 and b.id = a.transferoutaccountid and c.id = a.transferinaccountid and b.moneytype = d.id and c.moneytype = e.id "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            com.wacai365.QueryInfo r1 = r12.a
            java.lang.String r2 = ""
            r3 = -1
            long r5 = r1.j
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 == 0) goto L_0x0297
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.transferoutmoney >= %d"
            java.lang.Object[] r4 = new java.lang.Object[r8]
            long r5 = r1.j
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x0297:
            r3 = -1
            long r5 = r1.k
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 == 0) goto L_0x02c0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.transferoutmoney <= %d"
            java.lang.Object[] r4 = new java.lang.Object[r8]
            long r5 = r1.k
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x02c0:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.ymd >= %d"
            java.lang.Object[] r4 = new java.lang.Object[r8]
            long r5 = r1.b
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.ymd <= %d"
            java.lang.Object[] r4 = new java.lang.Object[r8]
            long r5 = r1.c
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            int r3 = r1.t
            if (r10 != r3) goto L_0x04f3
            int r3 = r1.n
            if (r9 == r3) goto L_0x032b
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.transferoutaccountid = %d"
            java.lang.Object[] r4 = new java.lang.Object[r8]
            int r5 = r1.n
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x032b:
            int r3 = r1.o
            if (r9 == r3) goto L_0x0350
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.transferinaccountid = %d"
            java.lang.Object[] r4 = new java.lang.Object[r8]
            int r5 = r1.o
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x0350:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and e.id = %d "
            java.lang.Object[] r4 = new java.lang.Object[r8]
            com.wacai.b r5 = com.wacai.b.m()
            long r5 = r5.j()
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x0377:
            int r3 = r1.t
            if (r10 == r3) goto L_0x0541
            int r3 = r1.f
            if (r9 == r3) goto L_0x0398
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.id = 0"
            java.lang.Object[] r4 = new java.lang.Object[r7]
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x0398:
            java.lang.String r3 = r1.h
            int r3 = r3.length()
            if (r3 <= 0) goto L_0x03b9
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.id = 0"
            java.lang.Object[] r4 = new java.lang.Object[r7]
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x03b9:
            r3 = -1
            long r5 = r1.l
            int r1 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r1 == 0) goto L_0x0541
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " and a.id = 0"
            java.lang.Object[] r3 = new java.lang.Object[r7]
            java.lang.String r2 = java.lang.String.format(r2, r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
        L_0x03da:
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = " group by a.id) group by e.moneytype"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 3
            r12.a(r1, r0)
        L_0x03f9:
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.t
            r0 = r0 & 8
            if (r0 == 0) goto L_0x0495
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.t
            if (r11 == r0) goto L_0x0414
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            r1 = 3
            if (r0 == r1) goto L_0x044b
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            if (r0 == r10) goto L_0x044b
        L_0x0414:
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            if (r0 == r8) goto L_0x042c
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            if (r0 == 0) goto L_0x042c
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.t
            if (r11 == r0) goto L_0x042c
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            if (r0 != r9) goto L_0x042f
        L_0x042c:
            r12.a(r8)
        L_0x042f:
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            r1 = 2
            if (r0 == r1) goto L_0x0448
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            if (r0 == 0) goto L_0x0448
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.t
            if (r11 == r0) goto L_0x0448
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            if (r0 != r9) goto L_0x044b
        L_0x0448:
            r12.a(r7)
        L_0x044b:
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.t
            if (r11 == r0) goto L_0x045e
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            if (r0 == r8) goto L_0x0495
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            r1 = 2
            if (r0 == r1) goto L_0x0495
        L_0x045e:
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            if (r0 == r10) goto L_0x0476
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            if (r0 == 0) goto L_0x0476
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.t
            if (r11 == r0) goto L_0x0476
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            if (r0 != r9) goto L_0x0479
        L_0x0476:
            r12.b(r8)
        L_0x0479:
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            r1 = 3
            if (r0 == r1) goto L_0x0492
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            if (r0 == 0) goto L_0x0492
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.t
            if (r11 == r0) goto L_0x0492
            com.wacai365.QueryInfo r0 = r12.a
            int r0 = r0.p
            if (r0 != r9) goto L_0x0495
        L_0x0492:
            r12.b(r7)
        L_0x0495:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r12.h = r0
            java.util.ArrayList r0 = r12.h
            r12.a(r0)
            r12.a()
            return
        L_0x04a5:
            int r3 = r1.e
            if (r9 == r3) goto L_0x04cc
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.transferoutaccountid = %d "
            java.lang.Object[] r4 = new java.lang.Object[r8]
            int r5 = r1.e
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            goto L_0x01dd
        L_0x04cc:
            int r3 = r1.d
            if (r9 == r3) goto L_0x01dd
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and d.id = %d "
            java.lang.Object[] r4 = new java.lang.Object[r8]
            int r5 = r1.d
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            goto L_0x01dd
        L_0x04f3:
            int r3 = r1.e
            if (r9 == r3) goto L_0x051a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and a.transferinaccountid = %d "
            java.lang.Object[] r4 = new java.lang.Object[r8]
            int r5 = r1.e
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            goto L_0x0377
        L_0x051a:
            int r3 = r1.d
            if (r9 == r3) goto L_0x0377
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " and e.id = %d "
            java.lang.Object[] r4 = new java.lang.Object[r8]
            int r5 = r1.d
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r4[r7] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            goto L_0x0377
        L_0x0541:
            r1 = r2
            goto L_0x03da
        L_0x0544:
            r1 = r2
            goto L_0x0240
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.MyBalanceStatByMoneyType.onCreate(android.os.Bundle):void");
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }
}
