package com.wacai365;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public final class pv extends ArrayAdapter {
    private Context a = null;
    private /* synthetic */ og b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pv(og ogVar, Context context, int i, String[] strArr) {
        super(context, i, strArr);
        this.b = ogVar;
        this.a = context;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? ((LayoutInflater) this.a.getSystemService("layout_inflater")).inflate((int) C0000R.layout.list_item_1_small_pop, (ViewGroup) null) : view;
        String str = (String) getItem(i);
        if (str != null) {
            ((TextView) inflate.findViewById(C0000R.id.listitem1)).setText(str);
        }
        return inflate;
    }
}
