package com.wacai365;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

final class vh implements View.OnClickListener {
    private /* synthetic */ NewsList a;

    vh(NewsList newsList) {
        this.a = newsList;
    }

    public final void onClick(View view) {
        this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.a.a.b())));
    }
}
