package com.wacai365;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.data.e;
import com.wacai.data.h;

public class InputLoanAccount extends InputBasicItem {
    private TextView e = null;
    /* access modifiers changed from: private */
    public TextView f = null;
    private LinearLayout g = null;
    private LinearLayout h = null;
    /* access modifiers changed from: private */
    public int[] i = null;
    /* access modifiers changed from: private */
    public h j;

    private h e() {
        this.j = new h();
        this.j.a(3);
        this.j.a(true);
        return this.j;
    }

    private void f() {
        this.e.setText(this.j.j());
        this.f.setText(e.a((long) this.j.g()));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.e.setText("");
        a(e());
        f();
        m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtContinuePrompt);
    }

    /* access modifiers changed from: protected */
    public final InputFilter[] b() {
        return new InputFilter[]{new InputFilter.LengthFilter(20)};
    }

    /* access modifiers changed from: protected */
    public final void c() {
        super.c();
        this.e = (TextView) findViewById(C0000R.id.tvloanaccountname);
        this.f = (TextView) findViewById(C0000R.id.tvMoneyType);
        this.h = (LinearLayout) findViewById(C0000R.id.layout_name);
        this.g = (LinearLayout) findViewById(C0000R.id.layoutMoneyType);
        this.b = (Button) findViewById(C0000R.id.btnCancel);
        this.a = (Button) findViewById(C0000R.id.btnOK);
        this.g.setOnClickListener(new fd(this));
        this.h.setOnClickListener(new fe(this));
        f();
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        String a = a(this.e);
        return a != null && a.trim().length() > 0;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1) {
            switch (i2) {
                case 18:
                    if (intent != null) {
                        String stringExtra = intent.getStringExtra("Text_String");
                        String trim = stringExtra == null ? "" : stringExtra.trim();
                        this.j.b(trim);
                        this.e.setText(trim);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.input_loanaccount);
        long longExtra = getIntent().getLongExtra("Record_Id", -1);
        if (longExtra > -1) {
            setTitle((int) C0000R.string.txtEditLoanAccount);
            h d = h.d(longExtra);
            this.j = d;
            a(d);
        } else {
            a(e());
        }
        c();
    }
}
