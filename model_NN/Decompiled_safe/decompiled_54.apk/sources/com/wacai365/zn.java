package com.wacai365;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.b;
import java.util.ArrayList;
import java.util.Hashtable;

public final class zn extends rj {
    public zn(Context context, ArrayList arrayList, int i) {
        super(context, arrayList, i);
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        Hashtable hashtable = (Hashtable) this.a.get(i);
        if (hashtable == null) {
            return null;
        }
        LinearLayout linearLayout = view == null ? (LinearLayout) this.b.inflate((int) C0000R.layout.histogram_listitem2, (ViewGroup) null) : (LinearLayout) view;
        ((TextView) linearLayout.findViewById(C0000R.id.listitem1)).setText((CharSequence) hashtable.get("TAG_LABLE"));
        String str = (String) hashtable.get("TAG_FIRST");
        TextView textView = (TextView) linearLayout.findViewById(C0000R.id.listitem2);
        String str2 = "";
        if (b.a) {
            str2 = str2 + this.c;
        }
        textView.setText(str2 + str);
        String str3 = (String) hashtable.get("TAG_SECOND");
        TextView textView2 = (TextView) linearLayout.findViewById(C0000R.id.listitem3);
        String str4 = "";
        if (b.a) {
            str4 = str4 + this.c;
        }
        textView2.setText(str4 + str3);
        TextView textView3 = (TextView) linearLayout.findViewById(C0000R.id.listitem4);
        double parseDouble = Double.parseDouble(str) - Double.parseDouble(str3);
        String str5 = "";
        if (b.a) {
            str5 = str5 + this.c;
        }
        textView3.setText(str5 + m.a(parseDouble, 2));
        return linearLayout;
    }
}
