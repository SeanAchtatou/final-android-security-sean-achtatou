package com.wacai365;

import android.view.View;
import android.webkit.WebView;

final class tz implements View.OnClickListener {
    private /* synthetic */ SoftIntroduce a;

    tz(SoftIntroduce softIntroduce) {
        this.a = softIntroduce;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.a) && this.a.b.canGoBack()) {
            this.a.b.goBack();
        } else if (view.equals(this.a.a)) {
            this.a.finish();
            this.a.b.clearCache(true);
            this.a.b.clearHistory();
            WebView unused = this.a.b = (WebView) null;
        }
    }
}
