package com.wacai365;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.b;

public final class rq extends BaseAdapter {
    private Context a = null;
    private Cursor b = null;
    private LayoutInflater c = null;
    private Resources d = null;
    private abo e = null;
    private /* synthetic */ LoanCollect f;

    public rq(LoanCollect loanCollect, Context context, abo abo) {
        this.f = loanCollect;
        this.a = context;
        this.e = abo;
        this.d = this.a.getResources();
        this.c = (LayoutInflater) this.a.getSystemService("layout_inflater");
    }

    private void a(TextView textView, boolean z) {
        textView.setTextColor(this.d.getColor(z ? C0000R.color.income_money : C0000R.color.outgo_money));
    }

    public final int getCount() {
        return 4;
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.c.inflate((int) C0000R.layout.list_item_withicon, (ViewGroup) null) : view;
        TextView textView = (TextView) inflate.findViewById(C0000R.id.headertitle);
        TextView textView2 = (TextView) inflate.findViewById(C0000R.id.headervalue);
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(C0000R.id.id_icon);
        if (!(textView == null || textView2 == null || linearLayout == null)) {
            String str = b.a ? this.e.a : "";
            switch (i) {
                case 0:
                    textView.setText(this.d.getString(C0000R.string.txtLoanReceivable));
                    a(textView2, false);
                    textView2.setText(str + m.a(m.a(this.e.c), 2));
                    linearLayout.setBackgroundResource(C0000R.drawable.ic_loanout);
                    break;
                case 1:
                    textView.setText(this.d.getString(C0000R.string.txtMakeCollections));
                    a(textView2, true);
                    textView2.setText(str + m.a(m.a(this.e.e), 2));
                    linearLayout.setBackgroundResource(C0000R.drawable.ic_loanin);
                    break;
                case 2:
                    textView.setText(this.d.getString(C0000R.string.txtBorrowPayable));
                    a(textView2, true);
                    textView2.setText(str + m.a(m.a(this.e.d), 2));
                    linearLayout.setBackgroundResource(C0000R.drawable.ic_receipt);
                    break;
                case 3:
                    textView.setText(this.d.getString(C0000R.string.txtRepayment));
                    a(textView2, false);
                    textView2.setText(str + m.a(m.a(this.e.f), 2));
                    linearLayout.setBackgroundResource(C0000R.drawable.ic_repayment);
                    break;
            }
        }
        return inflate;
    }

    public final int getViewTypeCount() {
        return 2;
    }

    public final boolean isEnabled(int i) {
        return false;
    }
}
