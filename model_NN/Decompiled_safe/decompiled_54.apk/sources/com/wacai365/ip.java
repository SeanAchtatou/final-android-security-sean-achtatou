package com.wacai365;

import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;

final class ip implements AdapterView.OnItemClickListener {
    private /* synthetic */ DialogInterface.OnMultiChoiceClickListener a;
    private /* synthetic */ hd b;

    ip(hd hdVar, DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener) {
        this.b = hdVar;
        this.a = onMultiChoiceClickListener;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.b.b[i] = !this.b.b[i];
        if (this.a != null) {
            this.a.onClick(this.b, i, this.b.b[i]);
        }
        this.b.a.invalidateViews();
    }
}
