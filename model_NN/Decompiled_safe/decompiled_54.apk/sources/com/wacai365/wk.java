package com.wacai365;

import android.database.Cursor;
import com.wacai.b.j;
import com.wacai.b.o;
import com.wacai.b.w;
import com.wacai.e;

final class wk extends j {
    wk() {
    }

    public final boolean a() {
        return is.a <= 0;
    }

    public final boolean a(o oVar) {
        Cursor cursor;
        if (oVar == null || !w.class.isInstance(oVar)) {
            return false;
        }
        w wVar = (w) oVar;
        StringBuffer stringBuffer = new StringBuffer(128);
        stringBuffer.append(String.format("select '%s' as tableName, id from %s where ( uuid IS NULL OR uuid = '' ) ", "TBL_TRADETARGET", "TBL_TRADETARGET"));
        try {
            Cursor rawQuery = e.c().b().rawQuery(stringBuffer.toString(), null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        int count = rawQuery.getCount();
                        is.b = new String[count];
                        is.c = new long[count];
                        for (int i = 0; i < count; i++) {
                            String string = rawQuery.getString(rawQuery.getColumnIndexOrThrow("tableName"));
                            long j = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("id"));
                            wVar.a(string, String.valueOf(j));
                            is.b[i] = string;
                            is.c[i] = j;
                            rawQuery.moveToNext();
                        }
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return true;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }
}
