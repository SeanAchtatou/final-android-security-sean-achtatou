package com.wacai365;

import android.content.DialogInterface;
import android.database.Cursor;
import com.wacai.e;

final class w implements DialogInterface.OnClickListener {
    private /* synthetic */ SettingBudgetMgr a;

    w(SettingBudgetMgr settingBudgetMgr) {
        this.a = settingBudgetMgr;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case -1:
                long j = 0;
                Cursor cursor = (Cursor) this.a.d.getItemAtPosition(this.a.a);
                if (cursor != null) {
                    j = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
                }
                e.c().b().execSQL(String.format("DELETE FROM tbl_budgetinfo where id = %d", Long.valueOf(j)));
                cursor.requery();
                return;
            default:
                return;
        }
    }
}
