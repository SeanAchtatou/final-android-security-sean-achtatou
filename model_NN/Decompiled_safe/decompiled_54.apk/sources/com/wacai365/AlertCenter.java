package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.wacai.b;
import com.wacai.e;
import java.util.Date;

public class AlertCenter extends WacaiActivity {
    /* access modifiers changed from: private */
    public Cursor a;
    private ListView b;
    /* access modifiers changed from: private */
    public fp c;
    /* access modifiers changed from: private */
    public fy d = null;
    private View.OnClickListener e = new ot(this);

    private static int a(int i) {
        String str = "select count(*) from TBL_ALERT where isread = 0 and alertstyle = " + i;
        if (i != 2) {
            str = str + " and nextday <= " + (System.currentTimeMillis() / 1000);
        }
        Cursor rawQuery = e.c().b().rawQuery(str, null);
        if (rawQuery == null || !rawQuery.moveToNext()) {
            return 0;
        }
        return rawQuery.getInt(0);
    }

    public static void a() {
        e.c().b().execSQL("DELETE FROM TBL_ALERT WHERE alertstyle = 2");
    }

    static /* synthetic */ void a(AlertCenter alertCenter) {
        if (alertCenter.a == null || alertCenter.a.getCount() <= 0) {
            alertCenter.finish();
        } else {
            m.a(alertCenter, (int) C0000R.string.alertDeleteAllAlert, new ou(alertCenter));
        }
    }

    static /* synthetic */ void a(AlertCenter alertCenter, int i) {
        if (i == -1) {
            return;
        }
        if (MyApp.a || !b.m().f()) {
            Cursor cursor = (Cursor) alertCenter.b.getItemAtPosition(i);
            if (cursor != null) {
                switch (cursor.getInt(cursor.getColumnIndexOrThrow("_style"))) {
                    case 1:
                        alertCenter.startActivity(new Intent(alertCenter, SettingLoanAccountMgr.class));
                        return;
                    case 2:
                        if (alertCenter.a == null || alertCenter.a.getCount() <= 0) {
                            alertCenter.finish();
                            return;
                        } else {
                            m.a(alertCenter, (int) C0000R.string.alertUpload, new pt(alertCenter));
                            return;
                        }
                    default:
                        return;
                }
            }
        } else {
            alertCenter.startActivity(new Intent(alertCenter, Login.class));
        }
    }

    public static void a(String str, boolean z) {
        if (str != null) {
            Cursor rawQuery = e.c().b().rawQuery(String.format("select comment from TBL_ALERT where alertstyle = %d", 2), null);
            if (rawQuery == null || !rawQuery.moveToNext()) {
                if (rawQuery != null) {
                    rawQuery.close();
                }
                com.wacai.data.b bVar = new com.wacai.data.b();
                bVar.b(2);
                bVar.a(System.currentTimeMillis() / 1000);
                bVar.a(false);
                bVar.a(str);
                bVar.c();
                return;
            }
            String string = rawQuery.getString(0);
            rawQuery.close();
            StringBuffer stringBuffer = new StringBuffer("update TBL_ALERT set comment = '%s',");
            if (z && !str.equals(string)) {
                stringBuffer.append("isread = 0,");
            }
            stringBuffer.append("nextday = %d where alertstyle = %d");
            e.c().b().execSQL(String.format(stringBuffer.toString(), str, Long.valueOf(System.currentTimeMillis() / 1000), 2));
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z, int i) {
        Cursor cursor = (Cursor) this.c.getItem(i);
        if (cursor != null) {
            int i2 = cursor.getInt(cursor.getColumnIndexOrThrow("_type"));
            int i3 = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
            String str = cursor.getInt(cursor.getColumnIndexOrThrow("_style")) == 1 ? "TBL_LOAN" : "";
            if (z) {
                com.wacai.data.b.a((long) i3, str);
                return;
            }
            switch (i2) {
                case 0:
                    com.wacai.data.b.a((long) i3, str);
                    return;
                case 1:
                case 2:
                    com.wacai.data.b c2 = com.wacai.data.b.c((long) i3);
                    c2.g();
                    c2.a(false);
                    c2.c();
                    return;
                default:
                    return;
            }
        }
    }

    public static int b() {
        return a(1);
    }

    static /* synthetic */ void c(AlertCenter alertCenter) {
        alertCenter.a.requery();
        alertCenter.a.getCount();
    }

    public static boolean c() {
        return a(2) > 0;
    }

    /* access modifiers changed from: private */
    public static void e() {
        e.c().b().execSQL("update TBL_ALERT set isread = 1 where nextday <=" + (System.currentTimeMillis() / 1000));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1 && this.d != null) {
            this.d.a(i, i2, intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.alert_center);
        ((TextView) findViewById(C0000R.id.btnSave)).setText(getString(C0000R.string.alertClearAllAlert));
        findViewById(C0000R.id.btnSave).setOnClickListener(this.e);
        findViewById(C0000R.id.btnCancel).setOnClickListener(this.e);
        m.b(this, 4);
        this.a = e.c().b().rawQuery(String.format("select * from (select a.id as _id, a.type as _type, a.destid as _destid,  c.name as _name, a.alertstyle as _style, a.comment as _comment, a.nextday as _nextday , a.isread as _isread from  TBL_ALERT a  LEFT JOIN TBL_LOAN b on a.id = b.alertid  LEFT JOIN TBL_ACCOUNTINFO c on b.debtid = c.id  where  (a.alertstyle = %d and a.nextday <= %d and a.id = b.alertid and b.debtid = c.id) or (a.alertstyle = %d)  ) order by _isread asc, _nextday desc", 1, Long.valueOf(new Date().getTime() / 1000), 2), null);
        startManagingCursor(this.a);
        if (this.a != null && this.a.getCount() != 0) {
            this.b = (ListView) findViewById(C0000R.id.list);
            this.c = new fp(this, this, C0000R.layout.alert_center_item, this.a, new String[]{"_name", "_style", "_comment", "_id", "_nextday", "_isread"}, new int[]{C0000R.id.list_item1, C0000R.id.list_item2});
            this.b.setAdapter((ListAdapter) this.c);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            e();
        }
        return super.onKeyDown(i, keyEvent);
    }
}
