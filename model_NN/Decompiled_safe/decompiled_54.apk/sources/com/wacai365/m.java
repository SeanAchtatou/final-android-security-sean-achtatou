package com.wacai365;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.wacai.a;
import com.wacai.a.g;
import com.wacai.b;
import com.wacai.data.aa;
import com.wacai.data.s;
import com.wacai.data.x;
import com.wacai.e;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class m {
    public static final SimpleDateFormat a = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat b = new SimpleDateFormat("yy-MM-dd");
    public static final SimpleDateFormat c = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat d = new SimpleDateFormat("HH:mm");
    public static final SimpleDateFormat e = new SimpleDateFormat("HH:mm:ss");
    public static final SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static final SimpleDateFormat g = new SimpleDateFormat("yyyy-MM");
    public static final SimpleDateFormat h = new SimpleDateFormat("MM-dd");
    public static String i = (Environment.getExternalStorageDirectory().getAbsolutePath() + "/wacai");
    private static SimpleDateFormat j = new SimpleDateFormat("HH:mm:ss yy-MM-dd");
    private static SimpleDateFormat k = new SimpleDateFormat("HH:mm yyyy-MM-dd");

    public static double a(long j2) {
        return aa.l(j2);
    }

    public static double a(String str, TextView textView, TextView textView2) {
        if (str == null || textView == null || textView2 == null) {
            return 0.0d;
        }
        textView2.setText(str);
        String[] a2 = a(str, 0);
        if (a2 == null) {
            return 0.0d;
        }
        double d2 = 0.0d;
        for (String valueOf : a2) {
            d2 += Double.valueOf(valueOf).doubleValue();
        }
        String obj = textView.getText().toString();
        if (obj != null && Double.valueOf(obj).doubleValue() == 0.0d && vk.a(d2)) {
            textView.setText(aa.a(d2, 2));
        }
        return d2;
    }

    public static long a(double d2) {
        return aa.a(d2);
    }

    static long a(long j2, long j3, long j4) {
        long time = new Date().getTime() / 1000;
        long j5 = 0;
        long j6 = 0;
        long j7 = 0;
        Cursor rawQuery = e.c().b().rawQuery(String.format("select sum(money) from TBL_OUTGOINFO where outgodate >= %d and outgodate <= %d and accountid = %d and isdelete = 0", Long.valueOf(j3), Long.valueOf(time), Long.valueOf(j2)), null);
        while (rawQuery.moveToNext()) {
            j5 = rawQuery.getLong(0);
        }
        rawQuery.close();
        Cursor rawQuery2 = e.c().b().rawQuery(String.format("select sum(money) from TBL_INCOMEINFO where incomedate >= %d and incomedate <= %d and accountid = %d and isdelete = 0", Long.valueOf(j3), Long.valueOf(time), Long.valueOf(j2)), null);
        while (rawQuery2.moveToNext()) {
            j6 = rawQuery2.getLong(0);
        }
        rawQuery2.close();
        Cursor rawQuery3 = e.c().b().rawQuery(String.format("select sum(transferoutmoney) from TBL_TRANSFERINFO where date >= %d and date <= %d and transferoutaccountid = %d and isdelete = 0", Long.valueOf(j3), Long.valueOf(time), Long.valueOf(j2)), null);
        while (rawQuery3.moveToNext()) {
            j7 = rawQuery3.getLong(0);
        }
        rawQuery3.close();
        Cursor rawQuery4 = e.c().b().rawQuery(String.format("select sum(transferinmoney) from TBL_TRANSFERINFO where date >= %d and date <= %d and transferinaccountid = %d and isdelete = 0", Long.valueOf(j3), Long.valueOf(time), Long.valueOf(j2)), null);
        long j8 = 0;
        while (rawQuery4.moveToNext()) {
            j8 = rawQuery4.getLong(0);
        }
        rawQuery4.close();
        return (((j4 + j6) - j5) - j7) + j8;
    }

    public static Bitmap a(View view) {
        if (view == null) {
            return null;
        }
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        return view.getDrawingCache();
    }

    public static Animation a(Context context, Animation animation, int i2, View view, int i3) {
        return a(animation, i2, view, i3 != 0 ? context.getResources().getText(i3).toString() : null);
    }

    public static Animation a(Animation animation, int i2, View view, int i3, int i4) {
        Animation animation2;
        Context a2 = e.c().a();
        if (view != null) {
            animation2 = animation == null ? AnimationUtils.loadAnimation(a2, i2) : animation;
            view.startAnimation(animation2);
        } else {
            animation2 = animation;
        }
        Toast.makeText(a2, i3, i4).show();
        return animation2;
    }

    public static Animation a(Animation animation, int i2, View view, String str) {
        Animation animation2;
        Context a2 = e.c().a();
        if (view != null) {
            animation2 = animation == null ? AnimationUtils.loadAnimation(a2, i2) : animation;
            view.startAnimation(animation2);
        } else {
            animation2 = animation;
        }
        if (!(str == null || str.length() == 0)) {
            Toast.makeText(a2, str, 0).show();
        }
        return animation2;
    }

    public static x a(s sVar, int i2) {
        x xVar = new x(sVar);
        xVar.a((long) i2);
        ArrayList s = sVar.s();
        if (s != null) {
            int size = s.size();
            int i3 = 0;
            while (true) {
                if (i3 < size) {
                    x xVar2 = (x) s.get(i3);
                    if (xVar2 != null && xVar2.a() == ((long) i2)) {
                        xVar.b(xVar2.b());
                        break;
                    }
                    i3++;
                } else {
                    break;
                }
            }
        }
        return xVar;
    }

    static rk a(Context context, String str, DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener, DialogInterface.OnClickListener onClickListener) {
        int[] iArr;
        if (str == null || str.length() <= 0) {
            iArr = null;
        } else {
            String[] split = str.split(",");
            if (split == null || split.length <= 0) {
                iArr = null;
            } else {
                int length = split.length;
                int[] iArr2 = new int[length];
                for (int i2 = 0; i2 < length; i2++) {
                    iArr2[i2] = Integer.parseInt(split[i2].trim());
                }
                iArr = iArr2;
            }
        }
        return a(context, iArr, onMultiChoiceClickListener, onClickListener);
    }

    static rk a(Context context, ArrayList arrayList, DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener, DialogInterface.OnClickListener onClickListener) {
        int size = arrayList.size();
        int[] iArr = new int[size];
        for (int i2 = 0; i2 < size; i2++) {
            iArr[i2] = (int) ((x) arrayList.get(i2)).a();
        }
        return a(context, iArr, onMultiChoiceClickListener, onClickListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.wacai365.m.a(java.lang.String, android.widget.TextView, android.widget.TextView):double
      com.wacai365.m.a(long, long, long):long
      com.wacai365.m.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai365.m.a(android.app.Activity, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, long, boolean):void
      com.wacai365.m.a(android.app.Activity, java.util.ArrayList, long):void
      com.wacai365.m.a(android.content.Context, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, android.widget.TextView):void
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(java.lang.String, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean, boolean):boolean
      com.wacai365.m.a(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String */
    private static rk a(Context context, int[] iArr, DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener, DialogInterface.OnClickListener onClickListener) {
        Cursor cursor;
        boolean z;
        try {
            Cursor rawQuery = e.c().b().rawQuery(a("TBL_MEMBERINFO", true, true), null);
            try {
                int count = rawQuery.getCount();
                String[] strArr = new String[count];
                rk rkVar = new rk();
                rkVar.a = new int[count];
                if (rkVar.a == null) {
                    if (rawQuery != null) {
                        rawQuery.close();
                    }
                    return null;
                }
                rkVar.b = new boolean[count];
                if (rkVar.b == null) {
                    if (rawQuery != null) {
                        rawQuery.close();
                    }
                    return null;
                }
                int length = iArr != null ? iArr.length : 0;
                int i2 = 0;
                while (rawQuery.moveToNext()) {
                    strArr[i2] = rawQuery.getString(rawQuery.getColumnIndexOrThrow("name"));
                    rkVar.a[i2] = rawQuery.getInt(rawQuery.getColumnIndexOrThrow("id"));
                    int i3 = 0;
                    while (true) {
                        if (i3 >= length) {
                            z = false;
                            break;
                        } else if (rkVar.a[i2] == iArr[i3]) {
                            z = true;
                            break;
                        } else {
                            i3++;
                        }
                    }
                    rkVar.b[i2] = z;
                    i2++;
                }
                new hd(context, strArr, rkVar.b, onMultiChoiceClickListener, onClickListener).show();
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return rkVar;
            } catch (Throwable th) {
                th = th;
                cursor = rawQuery;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
    }

    public static tp a(ProgressDialog progressDialog, boolean z, boolean z2, DialogInterface.OnClickListener onClickListener) {
        if (progressDialog == null) {
            return null;
        }
        if (z) {
            progressDialog.setIndeterminate(true);
        } else {
            progressDialog.setProgressStyle(1);
        }
        progressDialog.setCancelable(false);
        if (z2) {
            progressDialog.setButton(MyApp.b.getResources().getText(C0000R.string.txtCancel).toString(), onClickListener);
        }
        return new tp(progressDialog);
    }

    public static String a(double d2, int i2) {
        return aa.a(d2, i2);
    }

    static String a(String str, String str2, String str3) {
        if (str2.length() <= 0 && str3.length() <= 0) {
            return str;
        }
        if (str2.length() > 0 && str3.length() > 0) {
            return String.format("%s (%s, %s)", str, str2, str3);
        } else if (str2.length() > 0) {
            return String.format("%s (%s)", str, str2);
        } else if (str3.length() <= 0) {
            return str;
        } else {
            return String.format("%s (%s)", str, str3);
        }
    }

    static String a(String str, boolean z) {
        if (str == null || str.length() <= 0) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer(str.length() * 2);
        stringBuffer.append("%%");
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if (charAt == '|') {
                stringBuffer.append("||");
            } else if (charAt == '%') {
                stringBuffer.append("|%");
            } else if (charAt == '_') {
                stringBuffer.append("|_");
            } else {
                stringBuffer.append(charAt);
            }
            if (z || i2 == str.length() - 1) {
                stringBuffer.append("%%");
            }
        }
        return stringBuffer.toString();
    }

    private static String a(String str, boolean z, boolean z2) {
        if (str == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder("select * from ");
        sb.append(str);
        if (z) {
            sb.append(" where enable = 1 ");
        }
        if (!z2) {
            sb.append(" order by orderno ASC ");
        } else if (a.a("BasicSortStyle", 0) == 0) {
            sb.append(" order by orderno ASC ");
        } else {
            sb.append(" order by pinyin ASC ");
        }
        return sb.toString();
    }

    public static void a() {
        a.b("DataBackupInterval", new Date().getTime() / 1000);
    }

    public static void a(long j2, TextView textView) {
        if (j2 > 0 && textView != null) {
            StringBuffer stringBuffer = new StringBuffer(20);
            stringBuffer.append(aa.a("TBL_OUTGOMAINTYPEINFO", "name", (int) (j2 / 10000)));
            String a2 = aa.a("TBL_OUTGOSUBTYPEINFO", "name", (int) j2);
            if (a2 != null) {
                stringBuffer.append("--");
                stringBuffer.append(a2);
            }
            textView.setText(stringBuffer.toString());
        }
    }

    static void a(Activity activity) {
        activity.startActivityForResult(new Intent(activity, ChooseAccountType.class), 37);
    }

    static void a(Activity activity, long j2, TextView textView) {
        if (0 == j2) {
            activity.startActivityForResult(new Intent(activity, InputAccount.class), 18);
        } else {
            a("TBL_ACCOUNTINFO", j2, textView);
        }
    }

    public static void a(Activity activity, long j2, boolean z) {
        if (activity != null) {
            Intent intent = new Intent(activity, ChooseTarget.class);
            intent.putExtra("Target_ID", j2);
            intent.putExtra("Is_Payer", z);
            activity.startActivityForResult(intent, 24);
        }
    }

    public static void a(Activity activity, EditText editText) {
        if (activity != null && editText != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService("input_method");
            if (inputMethodManager.isActive()) {
                inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
            }
        }
    }

    public static void a(Activity activity, String str, String str2, int i2, int i3, boolean z) {
        if (activity != null) {
            Intent intent = new Intent(activity, InputText.class);
            if (str != null) {
                intent.putExtra("Text_String", str);
            }
            if (i2 > 0) {
                intent.putExtra("Max_LengthAllowed", i2);
            }
            if (str2 != null) {
                intent.putExtra("TITLE_NAME", str2);
            }
            intent.putExtra("EXTRA_ISSINGLELINE", z);
            activity.startActivityForResult(intent, i3);
        }
    }

    public static void a(Activity activity, ArrayList arrayList, long j2) {
        if (activity != null) {
            Intent intent = new Intent(activity, ChooseMember.class);
            if (arrayList != null) {
                int size = arrayList.size();
                Hashtable hashtable = new Hashtable();
                for (int i2 = 0; i2 < size; i2++) {
                    x xVar = (x) arrayList.get(i2);
                    hashtable.put(String.valueOf(xVar.a()), String.valueOf(xVar.b()));
                }
                intent.putExtra("Member_ShareInfo", hashtable);
            }
            intent.putExtra("Member_Total_Money", j2);
            activity.startActivityForResult(intent, 3);
        }
    }

    public static void a(Context context) {
        Intent intent = new Intent("android.intent.action.SEND");
        String string = context.getResources().getString(C0000R.string.txtSendToFriends);
        intent.putExtra("android.intent.extra.TEXT", context.getResources().getString(C0000R.string.txtSendToFriendsSubject));
        intent.setType("text/plain");
        context.startActivity(Intent.createChooser(intent, string));
    }

    public static void a(Context context, int i2) {
        int i3;
        String str;
        long j2;
        String str2;
        String str3;
        PendingIntent pendingIntent;
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        String string = context.getResources().getString(C0000R.string.app_name);
        switch (i2) {
            case 0:
                String string2 = context.getResources().getString(C0000R.string.txtIncomingNewsWithoutTitle);
                pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, SettingNews.class), 0);
                String string3 = context.getResources().getString(C0000R.string.txtIncomingNews);
                str2 = string;
                i3 = C0000R.drawable.news_notification;
                j2 = 2131296443;
                String str4 = string2;
                str = string3;
                str3 = str4;
                break;
            case 1:
                String string4 = context.getResources().getString(C0000R.string.txtIncomingSMSWithoutTitle);
                pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, MySMS.class), 0);
                String string5 = context.getResources().getString(C0000R.string.txtIncomingSMS);
                str2 = string;
                i3 = C0000R.drawable.news_notification;
                j2 = 2131296445;
                String str5 = string4;
                str = string5;
                str3 = str5;
                break;
            case 2:
                String string6 = context.getResources().getString(C0000R.string.txtPSWResetWithoutTitle);
                PendingIntent activity = PendingIntent.getActivity(context, 0, new Intent(context, SettingPasswordMgr.class), 0);
                String string7 = context.getResources().getString(C0000R.string.txtPSWReset);
                i3 = C0000R.drawable.news_notification;
                j2 = 2131296721;
                str3 = string6;
                str2 = string7;
                str = string7;
                pendingIntent = activity;
                break;
            case 3:
            default:
                str = "";
                str2 = string;
                str3 = "";
                pendingIntent = null;
                i3 = C0000R.drawable.news_notification;
                j2 = 0;
                break;
            case 4:
                String string8 = context.getResources().getString(C0000R.string.alertHasNewAlertWithoutTitle);
                PendingIntent activity2 = PendingIntent.getActivity(context, 0, new Intent(context, AlertCenter.class), 0);
                String string9 = context.getResources().getString(C0000R.string.alertHasNewAlert);
                i3 = C0000R.drawable.alert_notification;
                str = string9;
                String str6 = string8;
                j2 = 2131296972;
                str2 = string9;
                str3 = str6;
                pendingIntent = activity2;
                break;
            case 5:
                String string10 = context.getResources().getString(C0000R.string.alertHasNewAlertWithoutTitle);
                PendingIntent activity3 = PendingIntent.getActivity(context, 0, new Intent(context, AlertCenter.class), 0);
                String string11 = context.getResources().getString(C0000R.string.alertHasLoanAlert);
                i3 = C0000R.drawable.alert_notification;
                str = string11;
                String str7 = string10;
                j2 = 2131296972;
                str2 = string11;
                str3 = str7;
                pendingIntent = activity3;
                break;
            case 6:
                String string12 = context.getResources().getString(C0000R.string.alertHasNewAlertWithoutTitle);
                PendingIntent activity4 = PendingIntent.getActivity(context, 0, new Intent(context, AlertCenter.class), 0);
                String string13 = context.getResources().getString(C0000R.string.alertHasAutoUploadAlert);
                i3 = C0000R.drawable.alert_notification;
                str = string13;
                String str8 = string12;
                j2 = 2131296972;
                str2 = string13;
                str3 = str8;
                pendingIntent = activity4;
                break;
        }
        Notification notification = new Notification(i3, str, System.currentTimeMillis());
        notification.setLatestEventInfo(context, str2, str3, pendingIntent);
        notificationManager.cancel((int) j2);
        notificationManager.notify((int) j2, notification);
    }

    public static void a(Context context, int i2, int i3, int i4, int i5, DialogInterface.OnClickListener onClickListener) {
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(i2);
            builder.setIcon(17301659);
            builder.setMessage(i3);
            if (i4 != 0) {
                builder.setPositiveButton(i4, onClickListener);
            }
            if (i5 != 0) {
                builder.setNegativeButton(i5, onClickListener);
            }
            builder.setCancelable(true);
            builder.setOnCancelListener(new bn(onClickListener));
            builder.show();
        }
    }

    public static void a(Context context, int i2, int i3, int i4, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle((int) C0000R.string.txtAlertTitleInfo);
        builder.setMessage(i2);
        builder.setIcon(17301543);
        builder.setCancelable(true);
        if (i4 != 0) {
            builder.setNegativeButton(i4, onClickListener);
        }
        if (i3 != 0) {
            builder.setPositiveButton(i3, onClickListener);
        }
        builder.show();
    }

    public static void a(Context context, int i2, DialogInterface.OnClickListener onClickListener) {
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle((int) C0000R.string.txtAlertTitleInfo);
            builder.setMessage(i2);
            builder.setIcon(17301543);
            builder.setCancelable(true);
            builder.setNegativeButton((int) C0000R.string.txtCancel, onClickListener);
            builder.setPositiveButton((int) C0000R.string.txtOK, onClickListener);
            builder.setOnCancelListener(new bt(onClickListener));
            builder.show();
        }
    }

    public static void a(Context context, int i2, boolean z, DialogInterface.OnClickListener onClickListener, boolean z2) {
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(z ? C0000R.string.txtAlertTitleInfo : C0000R.string.txtAlertTitleError);
            builder.setIcon(z ? 17301659 : 17301543);
            builder.setMessage(i2);
            builder.setPositiveButton((int) C0000R.string.txtOK, onClickListener);
            builder.setCancelable(true);
            if (z2) {
                builder.setOnCancelListener(new br(onClickListener));
            }
            builder.show();
        }
    }

    public static void a(Context context, long j2, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        String str;
        String str2;
        long j3;
        String obj = context.getResources().getText(C0000R.string.txtWarningTitle).toString();
        int i2 = (int) j2;
        if (context == null || !aa.b("TBL_ACCOUNTINFO", (long) i2)) {
            str = "";
        } else {
            Cursor rawQuery = e.c().b().rawQuery("select * from tbl_accountinfo where id = " + i2, null);
            if (rawQuery.moveToNext()) {
                j3 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("warningbalance"));
                str2 = rawQuery.getString(rawQuery.getColumnIndexOrThrow("name"));
            } else {
                str2 = "";
                j3 = 0;
            }
            rawQuery.close();
            str = String.format(context.getText(C0000R.string.txtBalanceWarning).toString(), str2, aa.m(j3));
        }
        if (MyApp.a || !b.m().f()) {
            a(context, obj, str, (int) C0000R.string.txtCloseWarning, (int) C0000R.string.txtOK, onClickListener2);
        } else {
            a(context, obj, context.getResources().getString(C0000R.string.txtBalanceWarningNoLogin), onClickListener);
        }
    }

    public static void a(Context context, long j2, TextView textView) {
        if (textView != null) {
            String[] stringArray = context.getResources().getStringArray(C0000R.array.ReimburseType);
            switch ((int) j2) {
                case 0:
                    textView.setText(stringArray[0]);
                    return;
                case 1:
                    textView.setText(stringArray[1]);
                    return;
                case 2:
                    textView.setText(stringArray[2]);
                    return;
                default:
                    return;
            }
        }
    }

    static void a(Context context, DialogInterface.OnClickListener onClickListener) {
        new og(context, context.getResources().getStringArray(C0000R.array.Times), onClickListener).show();
    }

    public static void a(Context context, String str, DialogInterface.OnClickListener onClickListener) {
        if (1 != a.a("WeiboUploadPic", 1)) {
            onClickListener.onClick(null, -1);
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View inflate = LayoutInflater.from(context).inflate((int) C0000R.layout.network_warning, (ViewGroup) null);
        CheckBox checkBox = (CheckBox) inflate.findViewById(C0000R.id.cbEnableNetworkWarning);
        checkBox.setChecked(1 != a.a("WeiboUploadPic", 1));
        hz hzVar = new hz(checkBox, onClickListener);
        builder.setView(inflate);
        builder.setTitle((int) C0000R.string.txtAlertTitleInfo);
        builder.setMessage(str);
        builder.setIcon(17301543);
        builder.setCancelable(true);
        builder.setNegativeButton((int) C0000R.string.txtCancel, onClickListener);
        builder.setPositiveButton((int) C0000R.string.txtOK, hzVar);
        builder.setOnCancelListener(new hs(onClickListener));
        builder.show();
    }

    public static void a(Context context, String str, String str2, int i2, int i3, int i4, DialogInterface.OnClickListener onClickListener) {
        if (context != null && str != null && str2 != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(str);
            builder.setIcon(17301659);
            builder.setMessage(str2);
            if (i2 != 0) {
                builder.setPositiveButton(i2, onClickListener);
            }
            if (i3 != 0) {
                builder.setNegativeButton(i3, onClickListener);
            }
            if (i4 != 0) {
                builder.setNeutralButton(i4, onClickListener);
            }
            builder.setCancelable(true);
            builder.setOnCancelListener(new bo(onClickListener));
            builder.show();
        }
    }

    public static void a(Context context, String str, String str2, int i2, int i3, DialogInterface.OnClickListener onClickListener) {
        if (context != null && str != null && str2 != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(str);
            builder.setIcon(17301659);
            builder.setMessage(str2);
            if (i2 != 0) {
                builder.setPositiveButton(i2, onClickListener);
            }
            if (i3 != 0) {
                builder.setNegativeButton(i3, onClickListener);
            }
            builder.setCancelable(true);
            builder.setOnCancelListener(new bp(onClickListener));
            builder.show();
        }
    }

    public static void a(Context context, String str, String str2, DialogInterface.OnClickListener onClickListener) {
        if (context != null && str != null && str2 != null) {
            a(context, str, str2, context.getString(C0000R.string.txtOK), onClickListener);
        }
    }

    public static void a(Context context, String str, String str2, String str3, DialogInterface.OnClickListener onClickListener) {
        if (context != null && str != null && str2 != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(str);
            builder.setIcon(17301659);
            builder.setMessage(str2);
            builder.setPositiveButton(str3, onClickListener);
            builder.setCancelable(true);
            builder.setOnCancelListener(new bl(onClickListener));
            builder.show();
        }
    }

    public static void a(Context context, String str, boolean z, DialogInterface.OnClickListener onClickListener) {
        if (context != null && str != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            if (z) {
                builder.setTitle((int) C0000R.string.txtAlertTitleInfo);
                builder.setIcon(17301659);
            } else {
                builder.setTitle((int) C0000R.string.txtAlertTitleError);
                builder.setIcon(17301543);
            }
            builder.setMessage(str);
            builder.setPositiveButton((int) C0000R.string.txtOK, onClickListener);
            builder.setCancelable(true);
            builder.setOnCancelListener(new bv(onClickListener));
            builder.show();
        }
    }

    public static void a(Intent intent, s sVar) {
        if (intent != null && sVar != null && sVar.s() != null) {
            Hashtable hashtable = new Hashtable((HashMap) intent.getSerializableExtra("Member_ShareInfo"));
            if (hashtable.size() > 0) {
                ArrayList arrayList = new ArrayList();
                Enumeration keys = hashtable.keys();
                Enumeration elements = hashtable.elements();
                while (keys.hasMoreElements()) {
                    x xVar = new x(sVar);
                    xVar.a(Long.parseLong((String) keys.nextElement()));
                    xVar.b(Long.parseLong((String) elements.nextElement()));
                    arrayList.add(xVar);
                }
                sVar.a(arrayList);
            }
        }
    }

    public static void a(View view, int i2) {
        int paddingLeft = view.getPaddingLeft();
        int paddingRight = view.getPaddingRight();
        int paddingTop = view.getPaddingTop();
        int paddingBottom = view.getPaddingBottom();
        view.setBackgroundResource(i2);
        view.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
    }

    public static void a(Button button, Resources resources) {
        button.setEnabled(false);
        button.setText("");
        button.setBackgroundDrawable(resources.getDrawable(C0000R.drawable.n_bar_bottom));
    }

    public static void a(File file, Activity activity) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        activity.startActivity(intent);
    }

    public static void a(String str, long j2, TextView textView) {
        String a2;
        if (j2 > 0 && textView != null && str != null && str.length() > 0 && (a2 = aa.a(str, "name", (int) j2)) != null) {
            textView.setText(a2);
        }
    }

    public static void a(String str, TextView textView) {
        if (textView != null && str != null) {
            textView.setText(str);
        }
    }

    public static void a(ArrayList arrayList, TextView textView) {
        if (arrayList != null && arrayList.size() > 0 && textView != null) {
            StringBuffer stringBuffer = new StringBuffer(128);
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < arrayList.size()) {
                    stringBuffer.append(aa.a("TBL_MEMBERINFO", "name", (int) ((x) arrayList.get(i3)).a()));
                    if (i3 != arrayList.size() - 1) {
                        stringBuffer.append(",");
                    }
                    i2 = i3 + 1;
                } else {
                    textView.setText(stringBuffer.toString());
                    return;
                }
            }
        }
    }

    static boolean a(int i2) {
        boolean z;
        Cursor rawQuery = e.c().b().rawQuery("select * from tbl_accountinfo where id = " + i2, null);
        if (rawQuery.moveToNext()) {
            long j2 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("balancedate"));
            long j3 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("balance"));
            long j4 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("hasbalance"));
            long j5 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("haswarning"));
            long j6 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("warningbalance"));
            if (!(0 == j4 || 0 == j5 || a((long) i2, j2, j3) > j6)) {
                z = true;
                rawQuery.close();
                return z;
            }
        }
        z = false;
        rawQuery.close();
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.app.Activity, boolean, boolean):boolean
     arg types: [android.app.Activity, boolean, int]
     candidates:
      com.wacai365.m.a(java.lang.String, android.widget.TextView, android.widget.TextView):double
      com.wacai365.m.a(long, long, long):long
      com.wacai365.m.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String
      com.wacai365.m.a(android.app.Activity, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, long, boolean):void
      com.wacai365.m.a(android.app.Activity, java.util.ArrayList, long):void
      com.wacai365.m.a(android.content.Context, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, android.widget.TextView):void
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(java.lang.String, long, android.widget.TextView):void
      com.wacai365.m.a(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.app.Activity, boolean, boolean):boolean */
    public static boolean a(Activity activity, boolean z) {
        return a(activity, z, false);
    }

    public static boolean a(Activity activity, boolean z, boolean z2) {
        b m = b.m();
        if (m.b() != null && m.b().length() > 0 && m.c() != null && m.c().length() > 0) {
            return true;
        }
        if (z) {
            activity.startActivityForResult(new Intent(activity, Backup.class), 2);
        } else {
            Intent intent = new Intent(activity, AccountConfig.class);
            intent.putExtra("Extra_AlertAccount", z2);
            activity.startActivityForResult(intent, 2);
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a9 A[SYNTHETIC, Splitter:B:32:0x00a9] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.graphics.Bitmap r11, android.content.Context r12) {
        /*
            r10 = 0
            r9 = 0
            r8 = 0
            r7 = 1
            if (r11 == 0) goto L_0x0008
            if (r12 != 0) goto L_0x0010
        L_0x0008:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Helper.saveBitmap met null point"
            r0.<init>(r1)
            throw r0
        L_0x0010:
            android.content.res.Resources r0 = r12.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            java.lang.String r1 = "wacai.com"
            android.graphics.Paint r2 = new android.graphics.Paint
            r2.<init>()
            android.graphics.Rect r3 = new android.graphics.Rect
            r3.<init>()
            r4 = 1096810496(0x41600000, float:14.0)
            float r5 = r0.scaledDensity
            float r4 = r4 * r5
            r2.setTextSize(r4)
            android.content.res.Resources r4 = r12.getResources()
            r5 = 2131165216(0x7f070020, float:1.7944643E38)
            int r4 = r4.getColor(r5)
            r2.setColor(r4)
            r2.setAntiAlias(r7)
            int r4 = r1.length()
            r2.getTextBounds(r1, r10, r4, r3)
            r4 = 1084227584(0x40a00000, float:5.0)
            float r5 = r0.density
            float r4 = r4 * r5
            int r3 = r3.height()
            float r3 = (float) r3
            r5 = 1109393408(0x42200000, float:40.0)
            float r0 = r0.density
            float r0 = r0 * r5
            float r0 = r0 + r3
            if (r11 != 0) goto L_0x0077
            r0 = r8
        L_0x0057:
            boolean r1 = c()     // Catch:{ FileNotFoundException -> 0x009c, all -> 0x00a5 }
            if (r1 == 0) goto L_0x0094
            java.lang.String r1 = "tmp.jpg"
            java.io.File r1 = f(r1)     // Catch:{ FileNotFoundException -> 0x009c, all -> 0x00a5 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x009c, all -> 0x00a5 }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x009c, all -> 0x00a5 }
            r1 = r2
        L_0x0069:
            android.graphics.Bitmap$CompressFormat r2 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ FileNotFoundException -> 0x00b5, all -> 0x00b3 }
            r3 = 60
            r0.compress(r2, r3, r1)     // Catch:{ FileNotFoundException -> 0x00b5, all -> 0x00b3 }
            if (r1 == 0) goto L_0x0075
            r1.close()     // Catch:{ IOException -> 0x00ad }
        L_0x0075:
            r0 = r7
        L_0x0076:
            return r0
        L_0x0077:
            int r3 = r11.getWidth()
            int r5 = r11.getHeight()
            android.graphics.Bitmap$Config r6 = r11.getConfig()
            android.graphics.Bitmap r3 = android.graphics.Bitmap.createBitmap(r3, r5, r6)
            android.graphics.Canvas r5 = new android.graphics.Canvas
            r5.<init>(r3)
            r5.drawBitmap(r11, r9, r9, r8)
            r5.drawText(r1, r4, r0, r2)
            r0 = r3
            goto L_0x0057
        L_0x0094:
            java.lang.String r1 = "tmp.jpg"
            r2 = 1
            java.io.FileOutputStream r1 = r12.openFileOutput(r1, r2)     // Catch:{ FileNotFoundException -> 0x009c, all -> 0x00a5 }
            goto L_0x0069
        L_0x009c:
            r0 = move-exception
            r0 = r8
        L_0x009e:
            if (r0 == 0) goto L_0x00a3
            r0.close()     // Catch:{ IOException -> 0x00af }
        L_0x00a3:
            r0 = r10
            goto L_0x0076
        L_0x00a5:
            r0 = move-exception
            r1 = r8
        L_0x00a7:
            if (r1 == 0) goto L_0x00ac
            r1.close()     // Catch:{ IOException -> 0x00b1 }
        L_0x00ac:
            throw r0
        L_0x00ad:
            r0 = move-exception
            goto L_0x0075
        L_0x00af:
            r0 = move-exception
            goto L_0x00a3
        L_0x00b1:
            r1 = move-exception
            goto L_0x00ac
        L_0x00b3:
            r0 = move-exception
            goto L_0x00a7
        L_0x00b5:
            r0 = move-exception
            r0 = r1
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.m.a(android.graphics.Bitmap, android.content.Context):boolean");
    }

    public static boolean a(String str) {
        if (str == null) {
            return false;
        }
        return Pattern.compile("[\\w\\.\\-]+@([\\w\\-]+\\.)+[\\w\\-]+", 2).matcher(str).matches();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[]
     arg types: [android.content.Context, java.lang.String, boolean, int, android.content.DialogInterface$OnClickListener, int]
     candidates:
      com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void
      com.wacai365.m.a(android.content.Context, int, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[] */
    static int[] a(Context context, int i2, boolean z, DialogInterface.OnClickListener onClickListener) {
        String format = String.format("select * from tbl_outgosubtypeinfo where id >= %d and id < %d and enable = 1 ", Long.valueOf((long) (i2 * 10000)), Long.valueOf((long) ((i2 + 1) * 10000)));
        return a(context, a.a("BasicSortStyle", 0) == 0 ? format + " order by orderno ASC " : format + " order by pinyin ASC ", z, true, onClickListener, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, long, boolean, boolean, android.content.DialogInterface$OnClickListener):int[]
     arg types: [android.content.Context, long, boolean, int, android.content.DialogInterface$OnClickListener]
     candidates:
      com.wacai365.m.a(android.content.Context, android.view.animation.Animation, int, android.view.View, int):android.view.animation.Animation
      com.wacai365.m.a(android.view.animation.Animation, int, android.view.View, int, int):android.view.animation.Animation
      com.wacai365.m.a(android.content.Context, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener, boolean):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, boolean, boolean, android.content.DialogInterface$OnClickListener):int[] */
    static int[] a(Context context, long j2, boolean z, DialogInterface.OnClickListener onClickListener) {
        return a(context, j2, z, true, onClickListener);
    }

    private static int[] a(Context context, long j2, boolean z, boolean z2, DialogInterface.OnClickListener onClickListener) {
        boolean z3 = !MyApp.a && b.m().f();
        StringBuilder sb = new StringBuilder("select a.id as _id, a.name as _name, a.balance as _balance, a.balancedate as _balancedate, a.hasbalance as _hasbalance, b.id as _moneytypeid, b.shortname as _shortname  from tbl_accountinfo a, tbl_moneytype b where ");
        if (z2) {
            sb.append(" a.type <> 3 ");
        } else {
            sb.append(" a.type = 3 ");
        }
        sb.append("and a.enable = 1 and a.moneytype = b.id ");
        if (-1 != j2) {
            sb.append(" and a.moneytype = ");
            sb.append(j2);
            sb.append(" ");
        }
        if (a.a("BasicSortStyle", 0) == 0) {
            sb.append(" group by a.id  order by a.orderno ASC ");
        } else {
            sb.append(" group by a.id  order by a.pinyin ASC ");
        }
        Cursor cursor = null;
        try {
            cursor = e.c().b().rawQuery(sb.toString(), null);
            int count = cursor.getCount() + 1;
            String[] strArr = new String[count];
            int[] iArr = new int[count];
            if (z) {
                strArr[0] = context.getResources().getString(C0000R.string.txtFullString);
            } else {
                strArr[0] = context.getResources().getString(C0000R.string.txtNew);
            }
            iArr[0] = -1;
            int i2 = 1;
            while (cursor.moveToNext()) {
                long j3 = cursor.getLong(cursor.getColumnIndexOrThrow("_balancedate"));
                long j4 = cursor.getLong(cursor.getColumnIndexOrThrow("_balance"));
                long j5 = z3 ? 0 : cursor.getLong(cursor.getColumnIndexOrThrow("_hasbalance"));
                iArr[i2] = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
                String string = cursor.getString(cursor.getColumnIndexOrThrow("_name"));
                String string2 = cursor.getString(cursor.getColumnIndexOrThrow("_shortname"));
                if (0 != j5) {
                    String m = aa.m(a((long) iArr[i2], j3, j4));
                    if (!b.a) {
                        strArr[i2] = a(string, "", m);
                    } else {
                        strArr[i2] = a(string, string2, m);
                    }
                } else if (!b.a) {
                    strArr[i2] = a(string, "", "");
                } else {
                    strArr[i2] = a(string, string2, "");
                }
                i2++;
            }
            new og(context, strArr, onClickListener).show();
            return iArr;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private static int[] a(Context context, String str, boolean z, boolean z2, DialogInterface.OnClickListener onClickListener, boolean z3) {
        Cursor cursor;
        int i2;
        int i3;
        if (context == null || str == null) {
            return null;
        }
        try {
            cursor = e.c().b().rawQuery(str, null);
            if (z || z2) {
                try {
                    i2 = cursor.getCount() + 1;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            } else {
                i2 = cursor.getCount();
            }
            String[] strArr = new String[i2];
            int[] iArr = new int[i2];
            if (z) {
                strArr[0] = context.getResources().getText(C0000R.string.txtFullString).toString();
                iArr[0] = -1;
                i3 = 0 + 1;
            } else if (z2) {
                strArr[0] = context.getResources().getText(C0000R.string.txtNew).toString();
                iArr[0] = -1;
                i3 = 0 + 1;
            } else {
                i3 = 0;
            }
            while (cursor.moveToNext()) {
                if (z3) {
                    String string = cursor.getString(cursor.getColumnIndexOrThrow("uuid"));
                    String[] strArr2 = {"2", "4", "5", "6", "7"};
                    int[] iArr2 = {C0000R.string.accountDepositComment, C0000R.string.accountInvestComment, C0000R.string.accountValuableCardComment, C0000R.string.accountInternetPayComment, C0000R.string.accountVirtualComment};
                    String str2 = null;
                    for (int i4 = 0; i4 < strArr2.length; i4++) {
                        if (string.equals(strArr2[i4])) {
                            str2 = context.getResources().getString(iArr2[i4]);
                        }
                    }
                    if (str2 != null) {
                        strArr[i3] = cursor.getString(cursor.getColumnIndexOrThrow("name")) + str2;
                        iArr[i3] = cursor.getInt(cursor.getColumnIndexOrThrow("id"));
                        i3++;
                    }
                }
                strArr[i3] = cursor.getString(cursor.getColumnIndexOrThrow("name"));
                iArr[i3] = cursor.getInt(cursor.getColumnIndexOrThrow("id"));
                i3++;
            }
            new og(context, strArr, onClickListener).show();
            if (cursor != null) {
                cursor.close();
            }
            return iArr;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[]
     arg types: [android.content.Context, java.lang.String, boolean, int, android.content.DialogInterface$OnClickListener, int]
     candidates:
      com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void
      com.wacai365.m.a(android.content.Context, int, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.wacai365.m.a(java.lang.String, android.widget.TextView, android.widget.TextView):double
      com.wacai365.m.a(long, long, long):long
      com.wacai365.m.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai365.m.a(android.app.Activity, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, long, boolean):void
      com.wacai365.m.a(android.app.Activity, java.util.ArrayList, long):void
      com.wacai365.m.a(android.content.Context, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, android.widget.TextView):void
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(java.lang.String, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean, boolean):boolean
      com.wacai365.m.a(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String */
    static int[] a(Context context, boolean z, DialogInterface.OnClickListener onClickListener) {
        return a(context, a("TBL_INCOMEMAINTYPEINFO", true, true), z, true, onClickListener, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[]
     arg types: [android.content.Context, java.lang.String, boolean, int, android.content.DialogInterface$OnClickListener, int]
     candidates:
      com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void
      com.wacai365.m.a(android.content.Context, int, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[] */
    static int[] a(Context context, boolean z, boolean z2, DialogInterface.OnClickListener onClickListener) {
        StringBuilder sb = new StringBuilder("select * from TBL_ACCOUNTTYPE where ");
        if (z) {
            sb.append(" uuid <> 3 ");
        } else {
            sb.append(" uuid = 3 ");
        }
        sb.append(" order by orderno ASC ");
        return a(context, sb.toString(), z2, false, onClickListener, true);
    }

    static String[] a(String str, int i2) {
        String str2 = "([0-9]+([\\.]?)[0-9]*)";
        switch (i2) {
            case 0:
                str2 = "([0-9]+([\\.]?)[0-9]*)";
                break;
            case 1:
                str2 = "(([0-9]{1,3})((,[0-9]{3})+))((\\.[0-9]{1,2})?)|(([0-9]+)(\\.[0-9]{1,2})?)";
                break;
            case 2:
                str2 = "((([0-9]{1,3})((,[0-9]{3})+))((\\.[0-9]{1,2})?)|(([0-9]+)(\\.[0-9]{1,2})?))元{1}";
                break;
        }
        Matcher matcher = Pattern.compile(str2, 32).matcher(str);
        int i3 = 0;
        while (matcher.find()) {
            i3++;
        }
        if (i3 <= 0) {
            return null;
        }
        String[] strArr = new String[i3];
        matcher.reset();
        int i4 = 0;
        while (matcher.find()) {
            strArr[i4] = matcher.group();
            strArr[i4].trim();
            i4++;
        }
        return strArr;
    }

    public static int b() {
        return Integer.parseInt(Build.VERSION.SDK);
    }

    public static String b(long j2) {
        return aa.m(j2);
    }

    public static String b(String str, String str2, String str3) {
        if (str == null || str.length() == 0) {
            return "";
        }
        Cursor rawQuery = e.c().b().rawQuery(String.format("select %s from %s where id in (%s) order by orderno ", str2, str3, str), null);
        String str4 = "";
        while (rawQuery.moveToNext()) {
            str4 = str4.length() == 0 ? str4 + rawQuery.getString(rawQuery.getColumnIndex(str2)) : str4 + ", " + rawQuery.getString(rawQuery.getColumnIndex(str2));
        }
        rawQuery.close();
        return str4;
    }

    static void b(int i2) {
        e.c().b().execSQL("UPDATE tbl_accountinfo SET haswarning = 0, warningbalance = 0 where id = " + i2);
    }

    public static void b(long j2, TextView textView) {
        if (textView != null) {
            textView.setText(e.format(Long.valueOf(j2)));
        }
    }

    public static void b(Activity activity) {
        Intent intent = new Intent();
        intent.setClass(activity, AccountConfig.class);
        activity.startActivityForResult(intent, 2);
    }

    public static void b(Context context, int i2) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        long j2 = 0;
        switch (i2) {
            case 0:
                j2 = 2131296443;
                break;
            case 1:
                j2 = 2131296445;
                break;
            case 2:
                j2 = 2131296721;
                break;
            case 4:
                j2 = 2131296972;
                break;
        }
        notificationManager.cancel((int) j2);
    }

    public static void b(Context context, int i2, DialogInterface.OnClickListener onClickListener) {
        if (1 != a.a("ShowNetworkWarning", 1)) {
            onClickListener.onClick(null, -1);
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View inflate = LayoutInflater.from(context).inflate((int) C0000R.layout.network_warning, (ViewGroup) null);
        CheckBox checkBox = (CheckBox) inflate.findViewById(C0000R.id.cbEnableNetworkWarning);
        checkBox.setChecked(1 != a.a("ShowNetworkWarning", 1));
        ia iaVar = new ia(checkBox, onClickListener);
        builder.setView(inflate);
        builder.setTitle((int) C0000R.string.txtAlertTitleInfo);
        builder.setMessage(i2);
        builder.setIcon(17301543);
        builder.setCancelable(true);
        builder.setNegativeButton((int) C0000R.string.txtCancel, onClickListener);
        builder.setPositiveButton((int) C0000R.string.txtOK, iaVar);
        builder.setOnCancelListener(new hy(onClickListener));
        builder.show();
    }

    public static boolean b(String str) {
        if (str == null) {
            return false;
        }
        return Pattern.compile("\\w+", 2).matcher(str).matches();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, long, boolean, boolean, android.content.DialogInterface$OnClickListener):int[]
     arg types: [android.content.Context, long, boolean, int, android.content.DialogInterface$OnClickListener]
     candidates:
      com.wacai365.m.a(android.content.Context, android.view.animation.Animation, int, android.view.View, int):android.view.animation.Animation
      com.wacai365.m.a(android.view.animation.Animation, int, android.view.View, int, int):android.view.animation.Animation
      com.wacai365.m.a(android.content.Context, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener, boolean):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, boolean, boolean, android.content.DialogInterface$OnClickListener):int[] */
    static int[] b(Context context, long j2, boolean z, DialogInterface.OnClickListener onClickListener) {
        return a(context, j2, z, false, onClickListener);
    }

    public static int[] b(Context context, DialogInterface.OnClickListener onClickListener) {
        int[] iArr = {-1, 0, 1, 2};
        new og(context, new String[]{context.getResources().getString(C0000R.string.txtFullString), context.getResources().getString(C0000R.string.txtNoneReimburse), context.getResources().getString(C0000R.string.txtReimburseMoney), context.getResources().getString(C0000R.string.txtHaveReimburse)}, onClickListener).show();
        return iArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[]
     arg types: [android.content.Context, java.lang.String, boolean, int, android.content.DialogInterface$OnClickListener, int]
     candidates:
      com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void
      com.wacai365.m.a(android.content.Context, int, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.wacai365.m.a(java.lang.String, android.widget.TextView, android.widget.TextView):double
      com.wacai365.m.a(long, long, long):long
      com.wacai365.m.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai365.m.a(android.app.Activity, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, long, boolean):void
      com.wacai365.m.a(android.app.Activity, java.util.ArrayList, long):void
      com.wacai365.m.a(android.content.Context, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, android.widget.TextView):void
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(java.lang.String, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean, boolean):boolean
      com.wacai365.m.a(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String */
    static int[] b(Context context, boolean z, DialogInterface.OnClickListener onClickListener) {
        return a(context, a("TBL_OUTGOMAINTYPEINFO", true, true), z, true, onClickListener, false);
    }

    public static long c(long j2) {
        Calendar instance = Calendar.getInstance();
        instance.set((int) (j2 / 100), ((int) (j2 % 100)) - 1, 1);
        return com.wacai.a.a.a(instance.getTimeInMillis());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.wacai365.m.a(com.wacai.data.s, int):com.wacai.data.x
      com.wacai365.m.a(double, int):java.lang.String
      com.wacai365.m.a(long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, android.widget.EditText):void
      com.wacai365.m.a(android.content.Context, int):void
      com.wacai365.m.a(android.content.Context, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Intent, com.wacai.data.s):void
      com.wacai365.m.a(android.view.View, int):void
      com.wacai365.m.a(android.widget.Button, android.content.res.Resources):void
      com.wacai365.m.a(java.io.File, android.app.Activity):void
      com.wacai365.m.a(java.lang.String, android.widget.TextView):void
      com.wacai365.m.a(java.util.ArrayList, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean):boolean
      com.wacai365.m.a(android.graphics.Bitmap, android.content.Context):boolean
      com.wacai365.m.a(java.lang.String, int):java.lang.String[]
      com.wacai365.m.a(java.lang.String, boolean):java.lang.String */
    public static Cursor c(String str, String str2, String str3) {
        StringBuffer stringBuffer = new StringBuffer(64);
        stringBuffer.append("SELECT ");
        if (str == null || str.length() <= 0) {
            stringBuffer.append(" id as _id, name as _name, ");
        } else {
            stringBuffer.append(str);
        }
        stringBuffer.append(" pinyin as _pinyin, enable FROM ");
        stringBuffer.append(str3);
        stringBuffer.append(" WHERE enable = 1 ");
        if (str2 != null && str2.length() > 0) {
            String lowerCase = str2.trim().toLowerCase();
            if (lowerCase.length() <= 0) {
                return null;
            }
            String a2 = g.a(lowerCase);
            if (a2.equalsIgnoreCase(lowerCase)) {
                stringBuffer.append(String.format(" AND _pinyin LIKE '%s' ESCAPE '|' ", aa.e(a(a2, true))));
            } else {
                stringBuffer.append(String.format(" AND _name LIKE '%s' ESCAPE '|' ", aa.e(a(lowerCase, false))));
            }
        }
        stringBuffer.append("ORDER BY _pinyin ASC");
        return e.c().b().rawQuery(stringBuffer.toString(), null);
    }

    public static String c(Context context, int i2) {
        return i2 < 0 ? "" : context.getResources().getStringArray(C0000R.array.Times)[i2];
    }

    public static void c(long j2, TextView textView) {
        if (textView != null) {
            textView.setText(c.format(Long.valueOf(j2)));
        }
    }

    public static void c(Activity activity) {
        if (activity != null) {
            activity.startActivityForResult(new Intent(activity, ChooseOutgoType.class), 6);
        }
    }

    public static boolean c() {
        return Environment.getExternalStorageState().equalsIgnoreCase("mounted");
    }

    public static boolean c(String str) {
        if (str == null) {
            return false;
        }
        return Pattern.compile("\\S+", 2).matcher(str).matches();
    }

    public static int[] c(Context context, DialogInterface.OnClickListener onClickListener) {
        int[] iArr = {0, 1};
        new og(context, context.getResources().getStringArray(C0000R.array.BasicSortMode), onClickListener).show();
        return iArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[]
     arg types: [android.content.Context, java.lang.String, boolean, int, android.content.DialogInterface$OnClickListener, int]
     candidates:
      com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void
      com.wacai365.m.a(android.content.Context, int, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.wacai365.m.a(java.lang.String, android.widget.TextView, android.widget.TextView):double
      com.wacai365.m.a(long, long, long):long
      com.wacai365.m.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai365.m.a(android.app.Activity, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, long, boolean):void
      com.wacai365.m.a(android.app.Activity, java.util.ArrayList, long):void
      com.wacai365.m.a(android.content.Context, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, android.widget.TextView):void
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(java.lang.String, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean, boolean):boolean
      com.wacai365.m.a(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String */
    static int[] c(Context context, boolean z, DialogInterface.OnClickListener onClickListener) {
        return a(context, a("TBL_PROJECTINFO", true, true), z, true, onClickListener, false);
    }

    public static long d(long j2) {
        Calendar instance = Calendar.getInstance();
        instance.set((int) (j2 / 100), ((int) (j2 % 100)) - 1, 1);
        instance.add(2, 1);
        instance.add(5, -1);
        return com.wacai.a.a.a(instance.getTimeInMillis());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.wacai365.m.a(com.wacai.data.s, int):com.wacai.data.x
      com.wacai365.m.a(double, int):java.lang.String
      com.wacai365.m.a(long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, android.widget.EditText):void
      com.wacai365.m.a(android.content.Context, int):void
      com.wacai365.m.a(android.content.Context, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Intent, com.wacai.data.s):void
      com.wacai365.m.a(android.view.View, int):void
      com.wacai365.m.a(android.widget.Button, android.content.res.Resources):void
      com.wacai365.m.a(java.io.File, android.app.Activity):void
      com.wacai365.m.a(java.lang.String, android.widget.TextView):void
      com.wacai365.m.a(java.util.ArrayList, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean):boolean
      com.wacai365.m.a(android.graphics.Bitmap, android.content.Context):boolean
      com.wacai365.m.a(java.lang.String, int):java.lang.String[]
      com.wacai365.m.a(java.lang.String, boolean):java.lang.String */
    public static Cursor d(String str) {
        StringBuffer stringBuffer = new StringBuffer(64);
        stringBuffer.append("SELECT a.id as _id, a.name as _name, a.pinyin as _pinyin, a.star as _star FROM TBL_OUTGOSUBTYPEINFO a JOIN TBL_OUTGOMAINTYPEINFO b ON b.enable=1 AND a.id/10000=b.id WHERE a.enable = 1 ");
        if (str != null && str.length() > 0) {
            String lowerCase = str.trim().toLowerCase();
            if (lowerCase.length() <= 0) {
                return null;
            }
            String a2 = g.a(lowerCase);
            if (a2.equalsIgnoreCase(lowerCase)) {
                stringBuffer.append(String.format(" AND _pinyin LIKE '%s' ESCAPE '|' ", aa.e(a(a2, true))));
            } else {
                stringBuffer.append(String.format(" AND _name LIKE '%s' ESCAPE '|' ", aa.e(a(lowerCase, false))));
            }
        }
        stringBuffer.append("ORDER BY _pinyin ASC");
        return e.c().b().rawQuery(stringBuffer.toString(), null);
    }

    public static String d(Context context, int i2) {
        String[] stringArray = context.getResources().getStringArray(C0000R.array.ReimburseSetting);
        switch (i2) {
            case -1:
                return stringArray[0];
            case 0:
                return stringArray[2];
            case 1:
                return stringArray[1];
            case 2:
                return stringArray[3];
            default:
                return "";
        }
    }

    public static void d(long j2, TextView textView) {
        if (textView != null) {
            textView.setText(f.format(Long.valueOf(j2)));
        }
    }

    public static boolean d() {
        String externalStorageState = Environment.getExternalStorageState();
        if (externalStorageState.equalsIgnoreCase("mounted")) {
            return true;
        }
        String[] strArr = {"shared", "unmounted", "unmountable", "bad_removal", "removed", "nofs", "checking", "mounted_ro"};
        int[] iArr = {C0000R.string.Alert_MEDIA_SHARED, C0000R.string.Alert_MEDIA_UNMOUNTED, C0000R.string.Alert_MEDIA_UNMOUNTABLE, C0000R.string.Alert_MEDIA_BAD_REMOVAL, C0000R.string.Alert_MEDIA_REMOVED, C0000R.string.Alert_MEDIA_NOFS, C0000R.string.Alert_MEDIA_CHECKING, C0000R.string.Alert_MEDIA_MOUNTED_READ_ONLY};
        int i2 = 0;
        while (true) {
            if (i2 >= strArr.length) {
                break;
            } else if (externalStorageState.equalsIgnoreCase(strArr[i2])) {
                a(MyApp.b, (Animation) null, 0, (View) null, iArr[i2]);
                break;
            } else {
                i2++;
            }
        }
        return false;
    }

    public static int[] d(Context context, DialogInterface.OnClickListener onClickListener) {
        int[] iArr = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        new og(context, context.getResources().getStringArray(C0000R.array.HomeScreenDisplayType), onClickListener).show();
        return iArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[]
     arg types: [android.content.Context, java.lang.String, boolean, int, android.content.DialogInterface$OnClickListener, int]
     candidates:
      com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void
      com.wacai365.m.a(android.content.Context, int, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.wacai365.m.a(java.lang.String, android.widget.TextView, android.widget.TextView):double
      com.wacai365.m.a(long, long, long):long
      com.wacai365.m.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai365.m.a(android.app.Activity, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, long, boolean):void
      com.wacai365.m.a(android.app.Activity, java.util.ArrayList, long):void
      com.wacai365.m.a(android.content.Context, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, android.widget.TextView):void
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(java.lang.String, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean, boolean):boolean
      com.wacai365.m.a(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String */
    static int[] d(Context context, boolean z, DialogInterface.OnClickListener onClickListener) {
        return a(context, a("TBL_MONEYTYPE", false, false), z, false, onClickListener, false);
    }

    public static int e(String str) {
        if (str == null || str == "") {
            return 0;
        }
        int length = str.length();
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4++) {
            if (str.charAt(i4) < 0 || str.charAt(i4) > 255) {
                i2++;
            } else {
                i3++;
            }
        }
        return ((i3 + 1) / 2) + i2;
    }

    public static String e(Context context, int i2) {
        return context.getResources().getStringArray(C0000R.array.BasicSortMode)[i2];
    }

    public static boolean e() {
        if (!d()) {
            return false;
        }
        File file = new File(i);
        if (!file.exists()) {
            file.mkdirs();
        }
        return true;
    }

    public static int[] e(Context context, DialogInterface.OnClickListener onClickListener) {
        int[] iArr = {0, 1, 2};
        new og(context, context.getResources().getStringArray(C0000R.array.CycleType), onClickListener).show();
        return iArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[]
     arg types: [android.content.Context, java.lang.String, boolean, int, android.content.DialogInterface$OnClickListener, int]
     candidates:
      com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void
      com.wacai365.m.a(android.content.Context, int, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.wacai365.m.a(java.lang.String, android.widget.TextView, android.widget.TextView):double
      com.wacai365.m.a(long, long, long):long
      com.wacai365.m.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai365.m.a(android.app.Activity, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, long, boolean):void
      com.wacai365.m.a(android.app.Activity, java.util.ArrayList, long):void
      com.wacai365.m.a(android.content.Context, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, android.widget.TextView):void
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(java.lang.String, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean, boolean):boolean
      com.wacai365.m.a(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String */
    static int[] e(Context context, boolean z, DialogInterface.OnClickListener onClickListener) {
        return a(context, a("TBL_MEMBERINFO", true, true), z, true, onClickListener, false);
    }

    public static File f(String str) {
        if (!e()) {
            return null;
        }
        File file = new File(i, str);
        if (file.exists()) {
            return file;
        }
        try {
            file.createNewFile();
            return file;
        } catch (IOException e2) {
            return null;
        }
    }

    public static String f(Context context, int i2) {
        switch (i2) {
            case 0:
                return context.getResources().getText(C0000R.string.txtHomeScreenDisplayType_OutgoToday).toString();
            case 1:
                return context.getResources().getText(C0000R.string.txtHomeScreenDisplayType_OutgoWeek).toString();
            case 2:
                return context.getResources().getText(C0000R.string.txtHomeScreenDisplayType_OutgoMonth).toString();
            case 3:
                return context.getResources().getText(C0000R.string.txtHomeScreenDisplayType_OutgoYear).toString();
            case 4:
                return context.getResources().getText(C0000R.string.txtHomeScreenDisplayType_IncomeToday).toString();
            case 5:
                return context.getResources().getText(C0000R.string.txtHomeScreenDisplayType_IncomeWeek).toString();
            case 6:
                return context.getResources().getText(C0000R.string.txtHomeScreenDisplayType_IncomeMonth).toString();
            case 7:
                return context.getResources().getText(C0000R.string.txtHomeScreenDisplayType_IncomeYear).toString();
            case 8:
                return context.getResources().getText(C0000R.string.txtHomeScreenDisplayType_AccountTotal).toString();
            case 9:
                return context.getResources().getText(C0000R.string.txtHomeScreenDisplayType_AccountSpec).toString();
            default:
                return "";
        }
    }

    public static void f(Context context, DialogInterface.OnClickListener onClickListener) {
        new og(context, context.getResources().getStringArray(C0000R.array.OccurDate), onClickListener).show();
    }

    public static String g(Context context, int i2) {
        return context.getResources().getStringArray(C0000R.array.OccurDate)[i2];
    }

    public static void g(Context context, DialogInterface.OnClickListener onClickListener) {
        new og(context, context.getResources().getStringArray(C0000R.array.SearchType), onClickListener).show();
    }

    public static int h(Context context, int i2) {
        return (int) ((context.getResources().getDisplayMetrics().density * ((float) i2)) + 0.5f);
    }
}
