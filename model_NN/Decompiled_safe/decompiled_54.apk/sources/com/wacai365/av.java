package com.wacai365;

import android.view.ContextMenu;
import android.view.View;

final class av implements View.OnCreateContextMenuListener {
    private /* synthetic */ MySMS a;

    av(MySMS mySMS) {
        this.a = mySMS;
    }

    public final void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.clear();
        this.a.getMenuInflater().inflate(C0000R.menu.mysms_list_context, contextMenu);
    }
}
