package com.wacai365;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.wacai365.widget.WheelView;
import com.wacai365.widget.b;
import com.wacai365.widget.h;
import com.wacai365.widget.l;
import java.util.Calendar;
import java.util.Date;

public class PickerDayHourMinute extends LinearLayout implements h {
    private tr a;
    private WheelView b;
    private WheelView c;
    private WheelView d;
    private Context e;
    private Date f;
    private Activity g;
    private int h;
    private int i;
    private int j;

    public PickerDayHourMinute(Context context) {
        this(context, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.wacai365.PickerDayHourMinute, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public PickerDayHourMinute(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.e = context;
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) C0000R.layout.wacai_date_time_picker, (ViewGroup) this, true);
        this.d = (WheelView) findViewById(C0000R.id.id_date);
        this.d.a(this);
        this.c = (WheelView) findViewById(C0000R.id.id_hour);
        this.c.a(this);
        this.b = (WheelView) findViewById(C0000R.id.id_minute);
        this.b.a(this);
        a(new Date());
    }

    public final void a() {
        this.a = null;
        if (this.d != null) {
            this.d.a();
        }
        if (this.c != null) {
            this.c.a();
        }
        if (this.b != null) {
            this.b.a();
        }
    }

    public final void a(Activity activity) {
        this.g = activity;
    }

    public final void a(tr trVar) {
        this.a = trVar;
    }

    public final void a(WheelView wheelView, int i2) {
        if (wheelView.equals(this.d)) {
            this.h = i2;
        } else if (wheelView.equals(this.c)) {
            this.i = i2;
        } else if (wheelView.equals(this.b)) {
            this.j = i2;
        }
        if (this.a != null) {
            Calendar instance = Calendar.getInstance();
            instance.setTime(this.f);
            instance.set(6, this.h + 1);
            instance.set(11, this.i);
            instance.set(12, this.j);
            this.f = instance.getTime();
            this.a.a(this.f);
        }
    }

    public final void a(Date date) {
        this.f = (Date) date.clone();
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        this.c.a(new l(this.e, 0, 23, instance.get(11), null, this.e.getResources().getString(C0000R.string.txtHour)));
        this.b.a(new l(this.e, 0, 59, instance.get(12), null, this.e.getResources().getString(C0000R.string.txtMinute)));
        this.d.a(new b(this.e, C0000R.layout.list_item_datewithdesc, -1, instance.get(1), instance.get(6) - 1));
    }
}
