package com.wacai365;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.SimpleExpandableListAdapter;
import java.util.List;
import java.util.Map;

final class xa extends SimpleExpandableListAdapter {
    private /* synthetic */ ChooseOutgoType a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    xa(ChooseOutgoType chooseOutgoType, Context context, List list, int i, String[] strArr, int[] iArr, List list2, int i2, String[] strArr2, int[] iArr2) {
        super(context, list, i, strArr, iArr, list2, i2, strArr2, iArr2);
        this.a = chooseOutgoType;
    }

    public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        View childView = super.getChildView(i, i2, z, view, viewGroup);
        CheckBox checkBox = (CheckBox) childView.findViewById(C0000R.id.btnStar);
        Map map = (Map) ((List) this.a.k.get(i)).get(i2);
        checkBox.setChecked(Integer.valueOf((String) map.get("star")).intValue() > 0);
        checkBox.setTag(map);
        checkBox.setOnClickListener(new ox(this));
        return childView;
    }
}
