package com.wacai365;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import com.wacai.a.a;
import java.util.Date;

final class ci implements View.OnClickListener {
    final /* synthetic */ MyBalance a;

    ci(MyBalance myBalance) {
        this.a = myBalance;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, long, boolean, android.content.DialogInterface$OnClickListener):int[]
     arg types: [com.wacai365.MyBalance, int, int, com.wacai365.gy]
     candidates:
      com.wacai365.m.a(android.view.animation.Animation, int, android.view.View, java.lang.String):android.view.animation.Animation
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, java.util.ArrayList, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, int[], android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.app.ProgressDialog, boolean, boolean, android.content.DialogInterface$OnClickListener):com.wacai365.tp
      com.wacai365.m.a(android.content.Context, long, android.content.DialogInterface$OnClickListener, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, boolean, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, long, boolean, android.content.DialogInterface$OnClickListener):int[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.MyBalance.a(com.wacai365.MyBalance, boolean):void
     arg types: [com.wacai365.MyBalance, int]
     candidates:
      com.wacai365.MyBalance.a(long, android.database.Cursor):java.lang.String
      com.wacai365.MyBalance.a(com.wacai365.MyBalance, int):void
      com.wacai365.MyBalance.a(com.wacai365.MyBalance, long):void
      com.wacai365.MyBalance.a(com.wacai365.QueryInfo, java.lang.StringBuffer):void
      com.wacai365.MyBalance.a(com.wacai365.MyBalance, boolean):void */
    public final void onClick(View view) {
        if (view.equals(this.a.b)) {
            if (4 == this.a.m.t) {
                m.a(this.a, (Animation) null, 0, (View) null, (int) C0000R.string.txtUseSettingFromQueryInfo);
                return;
            }
            this.a.a = m.a((Context) this.a, -1L, true, (DialogInterface.OnClickListener) new gy(this));
        } else if (view.equals(this.a.e)) {
            MyBalance.a(this.a, true);
            this.a.a();
        } else if (view.equals(this.a.d)) {
            MyBalance.a(this.a, false);
            this.a.a();
        } else if (view.equals(this.a.f)) {
            this.a.finish();
        } else if (view.equals(this.a.g)) {
            Intent intent = new Intent(this.a, MyBalanceQuerySetting.class);
            intent.putExtra("QUERYINFO", this.a.m);
            intent.putExtra("LaunchedByApplication", 1);
            this.a.startActivityForResult(intent, 1);
        } else if (view.equals(this.a.h)) {
            Intent intent2 = new Intent(this.a, MyScheduleTrade.class);
            intent2.putExtra("LaunchedByApplication", 1);
            this.a.startActivityForResult(intent2, 28);
        } else if (view.equals(this.a.c)) {
            new ut(this.a, this.a, new gx(this), new Date(a.b(this.a.m.b)), 1).show();
        } else if (view.equals(this.a.i)) {
            Intent intent3 = new Intent(this.a, MyBalanceStatByMoneyType.class);
            intent3.putExtra("QUERYINFO", this.a.m);
            intent3.putExtra("LaunchedByApplication", 1);
            this.a.startActivity(intent3);
        } else if (view.equals(this.a.j)) {
            Intent a2 = InputTrade.a(this.a, null, 0);
            a2.putExtra("LaunchedByApplication", 1);
            this.a.startActivityForResult(a2, 19);
        }
    }
}
