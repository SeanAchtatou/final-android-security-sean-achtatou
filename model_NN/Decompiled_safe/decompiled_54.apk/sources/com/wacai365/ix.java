package com.wacai365;

import android.content.Intent;
import android.view.View;

final class ix implements View.OnClickListener {
    private /* synthetic */ SettingAccountMgr a;

    ix(SettingAccountMgr settingAccountMgr) {
        this.a = settingAccountMgr;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.c)) {
            this.a.startActivityForResult(new Intent(this.a, InputAccount.class), 0);
        } else if (view.equals(this.a.d)) {
            this.a.finish();
        }
    }
}
