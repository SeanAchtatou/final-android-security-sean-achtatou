package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import com.wacai.a;
import com.wacai.e;

public class SettingMainTypeMgr extends WacaiActivity {
    /* access modifiers changed from: private */
    public Button a;
    /* access modifiers changed from: private */
    public Button b;
    private ListView c;
    private ListAdapter d;
    private View.OnClickListener e = new vd(this);

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        switch (menuItem.getItemId()) {
            case C0000R.id.idEdit /*2131493342*/:
                Cursor cursor = (Cursor) this.c.getItemAtPosition(adapterContextMenuInfo.position);
                long j = cursor != null ? cursor.getLong(cursor.getColumnIndexOrThrow("_id")) : -1;
                Intent intent = new Intent(this, InputMainType.class);
                intent.putExtra("Record_Id", j);
                startActivityForResult(intent, 0);
                break;
            case C0000R.id.idViewSub /*2131493346*/:
                Cursor cursor2 = (Cursor) this.c.getItemAtPosition(adapterContextMenuInfo.position);
                long j2 = cursor2 != null ? cursor2.getLong(cursor2.getColumnIndexOrThrow("_id")) : -1;
                Intent intent2 = new Intent(this, SettingSubTypeMgr.class);
                intent2.putExtra("Record_Id", j2);
                startActivityForResult(intent2, 0);
                break;
        }
        return false;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.setting_item_list);
        this.a = (Button) findViewById(C0000R.id.btnAdd);
        this.a.setOnClickListener(this.e);
        this.b = (Button) findViewById(C0000R.id.btnCancel);
        this.b.setOnClickListener(this.e);
        this.c = (ListView) findViewById(C0000R.id.IOList);
        Cursor rawQuery = e.c().b().rawQuery(a.a("BasicSortStyle", 0) == 0 ? "select id as _id, name as _name from TBL_OUTGOMAINTYPEINFO where enable = 1 " + " ORDER BY orderno ASC " : "select id as _id, name as _name from TBL_OUTGOMAINTYPEINFO where enable = 1 " + " ORDER BY pinyin ASC ", null);
        startManagingCursor(rawQuery);
        this.d = new SimpleCursorAdapter(this, C0000R.layout.list_item_withoutcheckable, rawQuery, new String[]{"_name"}, new int[]{C0000R.id.listitem1});
        this.c.setAdapter(this.d);
        this.c.setOnCreateContextMenuListener(new vc(this));
        this.c.setOnItemClickListener(new vb(this));
    }
}
