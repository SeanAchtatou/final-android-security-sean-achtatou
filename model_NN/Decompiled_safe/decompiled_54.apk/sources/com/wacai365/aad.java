package com.wacai365;

import android.widget.CompoundButton;

final class aad implements CompoundButton.OnCheckedChangeListener {
    private /* synthetic */ InputShortcut a;

    aad(InputShortcut inputShortcut) {
        this.a = inputShortcut;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (compoundButton.equals(this.a.c) && z) {
            this.a.o.a(0);
        } else if (compoundButton.equals(this.a.b) && z) {
            this.a.o.a(1);
        }
        this.a.a();
    }
}
