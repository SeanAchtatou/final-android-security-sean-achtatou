package com.wacai365;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import java.util.Calendar;
import java.util.Date;

public class zl extends ca {
    private ViewGroup a = null;
    private Activity b = null;
    private LinearLayout c = null;
    private PickerDayHourMinute d = null;
    private PickerYearMonthDay e = null;
    private PickerHourMinuteSecond f = null;
    /* access modifiers changed from: private */
    public Date g = null;
    private Date h = null;
    private Date i = null;
    /* access modifiers changed from: private */
    public Button j = null;
    private Button k = null;
    /* access modifiers changed from: private */
    public Button l = null;
    /* access modifiers changed from: private */
    public Button m = null;
    /* access modifiers changed from: private */
    public Object n = null;
    /* access modifiers changed from: private */
    public yn o = null;
    private boolean p = true;
    private boolean q = false;
    /* access modifiers changed from: private */
    public RadioButton r = null;
    /* access modifiers changed from: private */
    public RadioButton s = null;
    private LinearLayout t = null;
    private View.OnClickListener u = new jm(this);
    private CompoundButton.OnCheckedChangeListener v = new jo(this);

    public static zl a(ViewGroup viewGroup, Activity activity, Date date, int i2) {
        zl zlVar = new zl();
        zlVar.b = activity;
        zlVar.a = viewGroup;
        zlVar.p = false;
        zlVar.g = date;
        LayoutInflater layoutInflater = (LayoutInflater) zlVar.b.getSystemService("layout_inflater");
        if (1 == i2 || 2 == i2) {
            boolean z = 1 == i2;
            zlVar.c = (LinearLayout) layoutInflater.inflate(z ? C0000R.layout.choose_datetime_view_yearmonth : C0000R.layout.choose_datetime_view_datefordialog, (ViewGroup) null);
            zlVar.e = (PickerYearMonthDay) zlVar.c.findViewById(C0000R.id.id_datepicker);
            zlVar.e.a(!z);
            zlVar.b(date);
            zlVar.l = (Button) zlVar.c.findViewById(C0000R.id.id_ok);
            zlVar.l.setOnClickListener(zlVar.u);
            zlVar.m = (Button) zlVar.c.findViewById(C0000R.id.id_cancel);
            zlVar.m.setOnClickListener(zlVar.u);
        } else if (3 == i2) {
            zlVar.c = (LinearLayout) layoutInflater.inflate((int) C0000R.layout.choose_datetime_view_time, (ViewGroup) null);
            zlVar.f = (PickerHourMinuteSecond) zlVar.c.findViewById(C0000R.id.id_timepicker);
            zlVar.f.a(zlVar.b);
            zlVar.f.a(date);
            zlVar.f.a(new jq(zlVar));
            zlVar.l = (Button) zlVar.c.findViewById(C0000R.id.id_ok);
            zlVar.l.setOnClickListener(zlVar.u);
            zlVar.m = (Button) zlVar.c.findViewById(C0000R.id.id_cancel);
            zlVar.m.setOnClickListener(zlVar.u);
        } else if (4 == i2) {
            zlVar.c = (LinearLayout) layoutInflater.inflate((int) C0000R.layout.choose_datetime_view_switchable, (ViewGroup) null);
            zlVar.d = (PickerDayHourMinute) zlVar.c.findViewById(C0000R.id.id_datetimepicker);
            zlVar.a(date);
            zlVar.t = (LinearLayout) zlVar.c.findViewById(C0000R.id.id_container);
            zlVar.r = (RadioButton) zlVar.c.findViewById(C0000R.id.btnDateTime);
            zlVar.r.setChecked(true);
            zlVar.r.setOnCheckedChangeListener(zlVar.v);
            zlVar.s = (RadioButton) zlVar.c.findViewById(C0000R.id.btnDate);
            zlVar.s.setOnCheckedChangeListener(zlVar.v);
            zlVar.l = (Button) zlVar.c.findViewById(C0000R.id.id_ok);
            zlVar.l.setOnClickListener(zlVar.u);
            zlVar.m = (Button) zlVar.c.findViewById(C0000R.id.id_cancel);
            zlVar.m.setOnClickListener(zlVar.u);
        }
        zlVar.j = (Button) zlVar.c.findViewById(C0000R.id.id_back);
        if (zlVar.j != null) {
            zlVar.j.setOnClickListener(zlVar.u);
        }
        zlVar.k = (Button) zlVar.c.findViewById(C0000R.id.id_calendar);
        if (zlVar.k != null) {
            zlVar.k.setOnClickListener(zlVar.u);
        }
        zlVar.a();
        return zlVar;
    }

    static /* synthetic */ void a(zl zlVar, boolean z) {
        if (zlVar.t != null) {
            if (z) {
                if (zlVar.e != null) {
                    zlVar.e.a();
                }
                if (zlVar.d == null) {
                    zlVar.d = new PickerDayHourMinute(zlVar.b);
                    zlVar.d.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                }
                zlVar.a(zlVar.g);
                zlVar.t.removeAllViews();
                zlVar.t.addView(zlVar.d);
                return;
            }
            if (zlVar.d != null) {
                zlVar.d.a();
            }
            if (zlVar.e == null) {
                zlVar.e = new PickerYearMonthDay(zlVar.b);
                zlVar.e.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                zlVar.b(zlVar.g);
            }
            zlVar.b(zlVar.g);
            zlVar.t.removeAllViews();
            zlVar.t.addView(zlVar.e);
        }
    }

    private void a(Date date) {
        this.d.a(this.b);
        this.d.a(date);
        this.d.a(new jr(this));
    }

    private void b(Date date) {
        this.e.a(this.b);
        this.e.a(date);
        this.e.a(new js(this));
    }

    static /* synthetic */ void e(zl zlVar) {
        if (zlVar.q) {
            Calendar instance = Calendar.getInstance();
            instance.setTime(zlVar.g);
            if (zlVar.i != null) {
                Calendar instance2 = Calendar.getInstance();
                instance2.setTime(zlVar.i);
                instance.set(11, instance2.get(11));
                instance.set(12, instance2.get(12));
                instance.set(13, instance2.get(13));
            }
            if (zlVar.h != null) {
                Calendar instance3 = Calendar.getInstance();
                instance3.setTime(zlVar.h);
                instance.set(1, instance3.get(1));
                instance.set(2, instance3.get(2));
                instance.set(5, instance3.get(5));
            }
            zlVar.g = instance.getTime();
        }
    }

    public final void a() {
        if (this.q) {
            this.a.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            this.c.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
            this.a.addView(this.c);
            if (this.o != null) {
                this.o.a(this.n);
                return;
            }
            return;
        }
        this.c.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.a.addView(this.c);
        if (this.p) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.b.getApplicationContext(), C0000R.anim.popup_frame);
            loadAnimation.setAnimationListener(new jp(this));
            this.c.setAnimation(loadAnimation);
        } else if (this.o != null) {
            this.o.a(this.n);
        }
    }

    public final void a(yn ynVar, Object obj) {
        this.o = ynVar;
        this.n = obj;
    }

    public final void a(boolean z) {
        if (this.o != null) {
            this.o.a(z ? or.RESULT_CANCEL : or.RESULT_OK, this.n);
        }
    }

    public final int b() {
        if (this.c == null) {
            return 0;
        }
        return this.c.getHeight();
    }
}
