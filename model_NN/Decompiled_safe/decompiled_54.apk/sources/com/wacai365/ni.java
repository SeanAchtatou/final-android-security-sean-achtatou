package com.wacai365;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

final class ni implements AdapterView.OnItemClickListener {
    private /* synthetic */ PersonalSettingMgr a;

    ni(PersonalSettingMgr personalSettingMgr) {
        this.a = personalSettingMgr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void
     arg types: [com.wacai365.PersonalSettingMgr, java.lang.String, int, ?[OBJECT, ARRAY]]
     candidates:
      com.wacai365.m.a(android.view.animation.Animation, int, android.view.View, java.lang.String):android.view.animation.Animation
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, java.util.ArrayList, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, int[], android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.app.ProgressDialog, boolean, boolean, android.content.DialogInterface$OnClickListener):com.wacai365.tp
      com.wacai365.m.a(android.content.Context, long, android.content.DialogInterface$OnClickListener, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, long, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, boolean, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void */
    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent;
        if (4 != i || cc.b(this.a)) {
            switch (i) {
                case 0:
                    intent = new Intent(this.a, MoneyTypeSetting.class);
                    break;
                case 1:
                    intent = new Intent(this.a, LocalSetting.class);
                    break;
                case 2:
                    intent = new Intent(this.a, HomeScreenSetting.class);
                    break;
                case 3:
                    intent = new Intent(this.a, SettingNewsType.class);
                    break;
                case 4:
                    intent = new Intent(this.a, SettingSMSMgr.class);
                    break;
                default:
                    return;
            }
            intent.putExtra("LaunchedByApplication", 1);
            this.a.startActivityForResult(intent, 0);
        } else if (3 < m.b()) {
            cc.a(this.a);
        } else {
            m.a((Context) this.a, this.a.getResources().getString(C0000R.string.unSupportedROM), false, (DialogInterface.OnClickListener) null);
        }
    }
}
