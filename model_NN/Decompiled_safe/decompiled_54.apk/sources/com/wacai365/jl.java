package com.wacai365;

import android.content.DialogInterface;
import android.view.View;
import android.view.animation.Animation;
import com.wacai.a;

final class jl implements DialogInterface.OnClickListener {
    private /* synthetic */ en a;

    jl(en enVar) {
        this.a = enVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.k = this.a.a.i[i];
        this.a.a.f.setText(m.f(this.a.a.a, this.a.a.k));
        this.a.a.m = 0;
        this.a.a.g.setText("");
        dialogInterface.dismiss();
        if (9 == this.a.a.k) {
            m.a(this.a.a.a, (Animation) null, 0, (View) null, (int) C0000R.string.txtStatChooseAccount);
        } else {
            a.b("HomeScreenTitleType2", (long) this.a.a.k);
            this.a.a.e();
        }
        this.a.a.g();
    }
}
