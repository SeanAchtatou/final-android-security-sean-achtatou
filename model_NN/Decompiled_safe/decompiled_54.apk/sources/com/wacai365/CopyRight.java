package com.wacai365;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class CopyRight extends Activity {
    public void onCreate(Bundle bundle) {
        super.setTheme((int) C0000R.style.WaicaiTheme);
        super.onCreate(bundle);
        requestWindowFeature(7);
        setContentView((int) C0000R.layout.copyright);
        getWindow().setFeatureInt(7, C0000R.layout.title_bar);
        TextView textView = (TextView) findViewById(16908310);
        if (textView != null) {
            textView.setText(getTitle());
        }
        ((Button) findViewById(C0000R.id.btnAccept)).setOnClickListener(new r(this));
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(new q(this));
    }
}
