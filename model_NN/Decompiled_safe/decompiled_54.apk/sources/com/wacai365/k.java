package com.wacai365;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class k extends WebViewClient {
    private /* synthetic */ SoftIntroduce a;

    k(SoftIntroduce softIntroduce) {
        this.a = softIntroduce;
    }

    public final void onPageFinished(WebView webView, String str) {
        this.a.runOnUiThread(new ue(this.a, 8));
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        this.a.runOnUiThread(new ue(this.a, 0));
        super.onPageStarted(webView, str, bitmap);
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return super.shouldOverrideUrlLoading(webView, str);
    }
}
