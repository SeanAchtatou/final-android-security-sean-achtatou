package com.wacai365;

import android.content.Intent;
import android.view.View;
import com.wacai.a;

final class kl implements View.OnClickListener {
    private /* synthetic */ SettingSMSMgr a;

    kl(SettingSMSMgr settingSMSMgr) {
        this.a = settingSMSMgr;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.a)) {
            a.b("SMSFilter", this.a.f.isChecked() ? 1 : 0);
            if (this.a.f.isChecked()) {
                a.b("SMSFilterType", this.a.d.isChecked() ? 0 : 1);
            }
            this.a.finish();
        } else if (view.equals(this.a.b)) {
            this.a.finish();
        } else if (view.equals(this.a.c)) {
            Intent intent = new Intent(this.a, WhiteListMgr.class);
            intent.putExtra("LaunchedByApplication", 1);
            this.a.startActivityForResult(intent, 0);
        }
    }
}
