package com.wacai365;

import android.content.Intent;
import android.view.View;

final class hp implements View.OnClickListener {
    private /* synthetic */ StatByBalance a;

    hp(StatByBalance statByBalance) {
        this.a = statByBalance;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.btnCancel /*2131492870*/:
                this.a.finish();
                return;
            case C0000R.id.btnCollect /*2131493293*/:
                this.a.a();
                Intent intent = new Intent(this.a, StatBalance.class);
                intent.putExtra("Name_Array", this.a.e);
                intent.putExtra("Money_Array", this.a.f);
                this.a.startActivityForResult(intent, 0);
                return;
            default:
                return;
        }
    }
}
