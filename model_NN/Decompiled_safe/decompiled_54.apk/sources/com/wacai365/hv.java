package com.wacai365;

import java.util.Date;

final class hv implements Runnable {
    private /* synthetic */ String a;
    private /* synthetic */ long b;
    private /* synthetic */ StatByBalance c;

    hv(StatByBalance statByBalance, String str, long j) {
        this.c = statByBalance;
        this.a = str;
        this.b = j;
    }

    public final void run() {
        m.a(this.c, this.c.getString(C0000R.string.txtAlertTitleInfo), this.c.getString(C0000R.string.rectifySucceedPrompt, new Object[]{this.a, m.c.format(new Date(this.b)), m.e.format(new Date(this.b))}), new yr(this));
    }
}
