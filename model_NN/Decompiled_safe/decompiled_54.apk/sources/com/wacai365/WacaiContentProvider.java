package com.wacai365;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import com.wacai.a;
import com.wacai.data.aa;
import com.wacai.e;

public class WacaiContentProvider extends ContentProvider {
    private static final UriMatcher a;

    static {
        UriMatcher uriMatcher = new UriMatcher(-1);
        a = uriMatcher;
        uriMatcher.addURI("com.wacai.provider.main", "sms", 1);
    }

    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        boolean z;
        MyApp.a(getContext());
        if (a.match(uri) != 1) {
            return null;
        }
        if (!contentValues.containsKey("smsbody") || !contentValues.containsKey("smssender") || !contentValues.containsKey("smsdate")) {
            return null;
        }
        if (!(0 != a.a("SMSFilter", 0))) {
            return null;
        }
        String asString = contentValues.getAsString("smsbody");
        String asString2 = contentValues.getAsString("smssender");
        long longValue = contentValues.getAsLong("smsdate").longValue();
        if (1 == ((int) a.a("SMSFilterType", 0))) {
            Cursor rawQuery = e.c().b().rawQuery(String.format("select * from TBL_WHITELIST where address = '%s'", aa.e(asString2)), null);
            if (rawQuery != null) {
                boolean z2 = rawQuery.getCount() > 0;
                rawQuery.close();
                z = z2;
            } else {
                z = false;
            }
            if (!z) {
                return null;
            }
        }
        if (m.a(asString, 1) == null) {
            return null;
        }
        e.c().b().execSQL(String.format("insert into tbl_sms (content, smsdate, sender) values ('%s', %d, '%s')", aa.e(asString), Long.valueOf(longValue / 1000), aa.e(asString2)));
        m.a(getContext(), 1);
        return null;
    }

    public boolean onCreate() {
        return true;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        MyApp.a(getContext());
        switch (a.match(uri)) {
            case 1:
                return e.c().b().rawQuery(String.format("select propertyvalue as rowvalue from TBL_USERPROFILE where propertyname = '%s'", "SMSFilter"), null);
            default:
                return null;
        }
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        MyApp.a(getContext());
        return 0;
    }
}
