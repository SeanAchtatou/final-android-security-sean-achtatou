package com.wacai365;

import android.content.Intent;
import android.view.View;

final class vd implements View.OnClickListener {
    private /* synthetic */ SettingMainTypeMgr a;

    vd(SettingMainTypeMgr settingMainTypeMgr) {
        this.a = settingMainTypeMgr;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.a)) {
            this.a.startActivityForResult(new Intent(this.a, InputMainType.class), 0);
        } else if (view.equals(this.a.b)) {
            this.a.finish();
        }
    }
}
