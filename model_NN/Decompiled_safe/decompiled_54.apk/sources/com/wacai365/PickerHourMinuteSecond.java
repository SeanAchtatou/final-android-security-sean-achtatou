package com.wacai365;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.wacai365.widget.WheelView;
import com.wacai365.widget.h;
import com.wacai365.widget.l;
import java.util.Calendar;
import java.util.Date;

public class PickerHourMinuteSecond extends LinearLayout implements h {
    private WheelView a = null;
    private WheelView b = null;
    private WheelView c = null;
    private Context d = null;
    private uc e = null;
    private Date f;
    private Activity g;
    private int h;
    private int i;
    private int j;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.wacai365.PickerHourMinuteSecond, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public PickerHourMinuteSecond(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = context;
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) C0000R.layout.wacai_time_picker, (ViewGroup) this, true);
        this.c = (WheelView) findViewById(C0000R.id.id_sec);
        this.c.a(this);
        this.b = (WheelView) findViewById(C0000R.id.id_hour);
        this.b.a(this);
        this.a = (WheelView) findViewById(C0000R.id.id_minute);
        this.a.a(this);
        a(new Date());
    }

    public final void a(Activity activity) {
        this.g = activity;
    }

    public final void a(uc ucVar) {
        this.e = ucVar;
    }

    public final void a(WheelView wheelView, int i2) {
        if (wheelView.equals(this.b)) {
            this.h = i2;
        } else if (wheelView.equals(this.a)) {
            this.i = i2;
        } else if (wheelView.equals(this.c)) {
            this.j = i2;
        }
        if (this.e != null) {
            Calendar instance = Calendar.getInstance();
            instance.setTime(this.f);
            instance.set(11, this.h);
            instance.set(12, this.i);
            instance.set(13, this.j);
            this.f = instance.getTime();
            this.e.a(this.f);
        }
    }

    public final void a(Date date) {
        this.f = (Date) date.clone();
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        this.b.a(new l(this.d, 0, 23, instance.get(11), null, this.d.getResources().getString(C0000R.string.txtHour)));
        this.a.a(new l(this.d, 0, 59, instance.get(12), null, this.d.getResources().getString(C0000R.string.txtMinute)));
        this.c.a(new l(this.d, 0, 59, instance.get(13), null, this.d.getResources().getString(C0000R.string.txtSecond)));
    }
}
