package com.wacai365;

import android.content.DialogInterface;

final class iz implements DialogInterface.OnClickListener {
    private /* synthetic */ boolean a;
    private /* synthetic */ InputTrade b;

    iz(InputTrade inputTrade, boolean z) {
        this.b = inputTrade;
        this.a = z;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case -1:
                if (!this.a) {
                    m.b((int) this.b.a);
                    break;
                }
                break;
        }
        this.b.setResult(-1, this.b.getIntent());
        if (this.b.v.isChecked() || this.b.u.isChecked()) {
            this.b.c();
        } else {
            this.b.finish();
        }
    }
}
