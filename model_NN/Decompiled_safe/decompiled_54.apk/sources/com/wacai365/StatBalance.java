package com.wacai365;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

public class StatBalance extends WacaiActivity {
    private ListView a = null;
    private Button b = null;
    /* access modifiers changed from: private */
    public String[] c = null;
    /* access modifiers changed from: private */
    public String[] d = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.stat_balance);
        Intent intent = getIntent();
        this.c = intent.getStringArrayExtra("Name_Array");
        this.d = intent.getStringArrayExtra("Money_Array");
        this.a = (ListView) findViewById(C0000R.id.QueryList);
        this.b = (Button) findViewById(C0000R.id.btnCancel);
        this.b.setOnClickListener(new pp(this));
        this.a.setAdapter((ListAdapter) new tg(this, this, C0000R.layout.list_item_2));
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        finish();
        return true;
    }
}
