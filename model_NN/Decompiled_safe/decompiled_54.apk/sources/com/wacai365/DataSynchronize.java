package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.a.a;
import com.wacai.b.c;
import java.util.Date;

public class DataSynchronize extends WacaiActivity implements tt {
    /* access modifiers changed from: private */
    public Button a;
    /* access modifiers changed from: private */
    public Button b;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public Date g;
    /* access modifiers changed from: private */
    public Date h;
    private fy i = null;
    private LinearLayout j = null;
    /* access modifiers changed from: private */
    public TextView k = null;
    private LinearLayout l = null;
    /* access modifiers changed from: private */
    public TextView m = null;
    private View.OnClickListener n = new vu(this);
    private DialogInterface.OnClickListener o = new vs(this);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void f(DataSynchronize dataSynchronize) {
        ft ftVar = new ft(dataSynchronize);
        ftVar.a((tt) dataSynchronize);
        ftVar.a(dataSynchronize.o);
        dataSynchronize.i = ftVar;
        c cVar = new c(ftVar);
        int i2 = dataSynchronize.c;
        int i3 = dataSynchronize.e;
        int i4 = dataSynchronize.d;
        int i5 = dataSynchronize.f;
        is.a(cVar, true);
        is.c(cVar, true);
        is.a(cVar, false);
        is.b(cVar, true);
        int i6 = (i5 - i3) + ((i4 - i2) * 12) + 1;
        long j2 = (long) i2;
        long j3 = (long) i3;
        for (int i7 = 0; i7 < i6; i7++) {
            is.b(cVar, j2, j3);
            j3++;
            if (j3 > 12) {
                j2 = 1 + j2;
                j3 = 1;
            }
        }
        ftVar.a(cVar);
        ftVar.b(C0000R.string.txtDownloadProgress, C0000R.string.txtPreparing);
        ftVar.a(false, true);
    }

    public final void a(int i2) {
        WidgetProvider.a(this);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && this.i != null) {
            this.i.a(i2, i3, intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.data_synchronize);
        this.a = (Button) findViewById(C0000R.id.btnOK);
        this.a.setOnClickListener(this.n);
        this.b = (Button) findViewById(C0000R.id.btnCancel);
        this.b.setOnClickListener(this.n);
        this.g = new Date();
        this.h = new Date();
        a.a(4, this.g, this.h);
        long a2 = a.a(this.g.getTime());
        long a3 = a.a(this.h.getTime());
        this.c = (int) (a2 / 10000);
        this.d = (int) (a3 / 10000);
        this.e = (int) ((a2 % 10000) / 100);
        this.f = (int) ((a3 % 10000) / 100);
        this.j = (LinearLayout) findViewById(C0000R.id.layoutDateStart);
        this.j.setOnClickListener(new vw(this));
        this.k = (TextView) findViewById(C0000R.id.tvStartDate);
        this.k.setText(m.g.format(this.g));
        this.l = (LinearLayout) findViewById(C0000R.id.layoutDateEnd);
        this.l.setOnClickListener(new vt(this));
        this.m = (TextView) findViewById(C0000R.id.tvEndDate);
        this.m.setText(m.g.format(this.h));
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            this.a.setEnabled(true);
        }
    }
}
