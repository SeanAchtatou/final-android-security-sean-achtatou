package com.wacai365;

import android.os.Handler;
import android.os.Message;

final class db extends Handler {
    private /* synthetic */ VoiceInput a;

    db(VoiceInput voiceInput) {
        this.a = voiceInput;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 30:
                this.a.c();
                return;
            case 31:
                String str = (String) message.obj;
                if (str != null && str.length() > 0) {
                    this.a.d.getText().insert(this.a.d.getSelectionStart(), str);
                    this.a.d.setSelection(this.a.d.length());
                    return;
                }
                return;
            case 32:
                this.a.c();
                return;
            default:
                return;
        }
    }
}
