package com.wacai365;

final class abc implements Runnable {
    private /* synthetic */ StatView a;

    abc(StatView statView) {
        this.a = statView;
    }

    public final void run() {
        if (this.a.d != null) {
            this.a.d();
            this.a.d.a(this.a.q);
            this.a.d.invalidate();
        }
        if (this.a.f != null) {
            this.a.f.invalidateViews();
        }
        if (this.a.e != null) {
            this.a.e.a(this.a.q);
            this.a.e.invalidate();
        }
        this.a.f();
    }
}
