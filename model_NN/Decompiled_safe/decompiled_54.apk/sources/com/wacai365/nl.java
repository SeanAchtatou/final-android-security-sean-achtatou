package com.wacai365;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

public final class nl extends Dialog implements DialogInterface {
    protected double a = 0.0d;
    private va b = new aay(this);

    public nl(Context context, DialogInterface.OnClickListener onClickListener, double d, boolean z) {
        super(context, C0000R.style.choose_dialog);
        this.a = d;
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(context).inflate((int) C0000R.layout.choose_money_dialog, (ViewGroup) null);
        if (linearLayout != null) {
            linearLayout.removeAllViews();
            af.b(linearLayout, context, this.a, z).a(this.b, onClickListener);
            setContentView(linearLayout);
        }
    }

    public final double a() {
        return this.a;
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.height = -1;
        attributes.width = -1;
        getWindow().setAttributes(attributes);
    }
}
