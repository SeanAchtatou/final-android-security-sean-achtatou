package com.wacai365;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.a.a;
import com.wacai.b;
import com.wacai.data.e;
import com.wacai.data.f;
import com.wacai.data.h;
import java.util.Date;

public class zz extends yi {
    protected rk a = null;
    /* access modifiers changed from: private */
    public TextView b;
    /* access modifiers changed from: private */
    public TextView c;
    /* access modifiers changed from: private */
    public TextView d;
    /* access modifiers changed from: private */
    public TextView e;
    /* access modifiers changed from: private */
    public TextView f;
    /* access modifiers changed from: private */
    public TextView g;
    /* access modifiers changed from: private */
    public TextView h;
    /* access modifiers changed from: private */
    public TextView i;
    /* access modifiers changed from: private */
    public TextView j;
    private LinearLayout k;
    /* access modifiers changed from: private */
    public int[] l = null;
    /* access modifiers changed from: private */
    public int[] m = null;
    /* access modifiers changed from: private */
    public int[] r = null;
    /* access modifiers changed from: private */
    public int s = -1;
    /* access modifiers changed from: private */
    public QueryInfo t;

    public zz(QueryInfo queryInfo) {
        this.t = queryInfo;
    }

    public final void a(Context context, LinearLayout linearLayout, LinearLayout linearLayout2, Activity activity, boolean z) {
        super.a(context, linearLayout, linearLayout2, activity, true);
        LinearLayout linearLayout3 = (LinearLayout) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) C0000R.layout.query_all_setting_list, (ViewGroup) null).findViewById(C0000R.id.mainlayout);
        linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.removeAllViews();
        linearLayout.addView(linearLayout3);
        this.b = (TextView) linearLayout3.findViewById(C0000R.id.viewBeninMoney);
        this.c = (TextView) linearLayout3.findViewById(C0000R.id.viewEndMoney);
        this.d = (TextView) linearLayout3.findViewById(C0000R.id.viewShortcutsDate);
        this.e = (TextView) linearLayout3.findViewById(C0000R.id.viewBeginDate);
        this.f = (TextView) linearLayout3.findViewById(C0000R.id.viewEndDate);
        this.g = (TextView) linearLayout3.findViewById(C0000R.id.viewMember);
        this.h = (TextView) linearLayout3.findViewById(C0000R.id.viewMoneyType);
        this.i = (TextView) linearLayout3.findViewById(C0000R.id.viewAccount);
        this.j = (TextView) linearLayout3.findViewById(C0000R.id.viewProject);
        ((LinearLayout) this.p.findViewById(C0000R.id.beginMoneyItem)).setOnClickListener(new aac(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.endMoneyItem)).setOnClickListener(new aae(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.dateShortcutsItem)).setOnClickListener(new aag(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.memberItem)).setOnClickListener(new aah(this));
        this.k = (LinearLayout) this.p.findViewById(C0000R.id.moneyTypeItem);
        this.k.setOnClickListener(new aai(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.accountItem)).setOnClickListener(new zq(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.projectItem)).setOnClickListener(new zp(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.dateBeginItem)).setOnClickListener(new zs(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.dateEndItem)).setOnClickListener(new zr(this));
        a_();
    }

    public final void a(Object obj) {
        if (QueryInfo.class.isInstance(obj)) {
            this.t = (QueryInfo) obj;
        } else if (this.t == null) {
            this.t = new QueryInfo();
        }
        this.t.l = -1;
        this.t.m = -1;
    }

    public final void a_() {
        this.s = this.t.a;
        if (-1 == this.t.j) {
            this.b.setText("");
        } else {
            this.b.setText(m.a(m.a(this.t.j), 2));
        }
        if (-1 == this.t.k) {
            this.c.setText("");
        } else {
            this.c.setText(m.a(m.a(this.t.k), 2));
        }
        this.d.setText(this.n.getResources().getStringArray(C0000R.array.Times)[this.t.a]);
        this.e.setText(m.c.format(new Date(a.b(this.t.b))));
        this.f.setText(m.c.format(new Date(a.b(this.t.c))));
        if (this.t.i == null || this.t.i.length() <= 0) {
            this.g.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.g.setText(this.t.i);
        }
        if (-1 == this.t.d) {
            this.h.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.h.setText(e.a((long) this.t.d));
        }
        if (-1 == this.t.e) {
            this.i.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.i.setText(h.e((long) this.t.e));
        }
        if (-1 == this.t.f) {
            this.j.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.j.setText(f.b((long) this.t.f));
        }
        if (b.a) {
            this.k.setVisibility(0);
        } else {
            this.k.setVisibility(8);
        }
    }

    public final Object b() {
        return this.t;
    }

    public final boolean c() {
        this.t.t = 15;
        return true;
    }
}
