package com.wacai365;

import android.view.View;

final class gs implements View.OnClickListener {
    private /* synthetic */ ChooseMember a;

    gs(ChooseMember chooseMember) {
        this.a = chooseMember;
    }

    public final void onClick(View view) {
        int size = this.a.e.size();
        long j = this.a.b / ((long) size);
        long j2 = 0;
        for (int i = size - 1; i >= 0; i--) {
            uz uzVar = (uz) this.a.e.get(i);
            if (uzVar != null) {
                if (i != 0) {
                    uzVar.b = j;
                    j2 += j;
                } else {
                    uzVar.b = this.a.b - j2;
                }
            }
        }
        this.a.c.invalidateViews();
    }
}
