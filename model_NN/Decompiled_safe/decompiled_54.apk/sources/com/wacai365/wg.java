package com.wacai365;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public abstract class wg extends yi {
    private LinearLayout a = null;
    protected int d;
    protected boolean e = false;
    protected ca f = null;
    protected LinearLayout g = null;
    protected ScrollView h = null;
    protected int i = 0;

    public final void a(Context context, LinearLayout linearLayout, LinearLayout linearLayout2, Activity activity, boolean z) {
        LinearLayout linearLayout3;
        super.a(context, linearLayout, linearLayout2, activity, z);
        linearLayout.removeAllViews();
        if (this.g == null) {
            linearLayout3 = (LinearLayout) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(this.d, linearLayout).findViewById(C0000R.id.mainlayout);
        } else {
            linearLayout.addView(this.g);
            linearLayout3 = this.g;
        }
        a(linearLayout3);
    }

    /* access modifiers changed from: protected */
    public abstract void a(LinearLayout linearLayout);

    public abstract void a(ScrollView scrollView, int i2);

    public final void a(ca caVar) {
        if (caVar != null) {
            i();
        } else if (this.f != null) {
            i();
            this.f.a(false);
            this.f = null;
        }
        this.f = caVar;
        if (this.f != null) {
            j();
            d();
        }
    }

    public void a(y yVar, boolean z) {
        switch (qy.a[yVar.ordinal()]) {
            case 1:
                this.e = z;
                return;
            default:
                return;
        }
    }

    public abstract void b(long j);

    public abstract void d();

    public abstract ca e();

    /* access modifiers changed from: protected */
    public void g() {
    }

    public final boolean h() {
        if (this.f == null || !this.f.c()) {
            return false;
        }
        this.f.a(false);
        this.f = null;
        return true;
    }

    /* access modifiers changed from: protected */
    public final void i() {
        if (this.a != null) {
            this.g.removeView(this.a);
            this.a = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void j() {
        if (this.f != null && this.a == null) {
            this.a = new LinearLayout(this.o);
            this.a.setLayoutParams(new LinearLayout.LayoutParams(1, this.f.b() - this.i));
            this.g.addView(this.a);
        }
    }
}
