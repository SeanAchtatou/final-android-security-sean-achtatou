package com.wacai365;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.a.a;
import com.wacai.b;
import com.wacai.data.f;
import com.wacai.data.h;
import com.wacai.data.p;
import com.wacai.data.y;
import com.wacai.e;
import java.util.Date;

public class QueryReimburse extends WacaiActivity {
    /* access modifiers changed from: private */
    public static Boolean C;
    /* access modifiers changed from: private */
    public int[] A = null;
    /* access modifiers changed from: private */
    public int B = -1;
    private View.OnClickListener F = new ec(this);
    protected rk a = null;
    /* access modifiers changed from: private */
    public QueryInfo b;
    /* access modifiers changed from: private */
    public Button c;
    /* access modifiers changed from: private */
    public Button d;
    /* access modifiers changed from: private */
    public Button e;
    /* access modifiers changed from: private */
    public Button f;
    /* access modifiers changed from: private */
    public TextView g;
    /* access modifiers changed from: private */
    public TextView h;
    /* access modifiers changed from: private */
    public TextView i;
    /* access modifiers changed from: private */
    public TextView j;
    /* access modifiers changed from: private */
    public TextView k;
    /* access modifiers changed from: private */
    public TextView l;
    /* access modifiers changed from: private */
    public TextView m;
    /* access modifiers changed from: private */
    public TextView n;
    /* access modifiers changed from: private */
    public TextView o;
    /* access modifiers changed from: private */
    public TextView p;
    /* access modifiers changed from: private */
    public TextView q;
    private LinearLayout r;
    /* access modifiers changed from: private */
    public LinearLayout s;
    /* access modifiers changed from: private */
    public LinearLayout t;
    /* access modifiers changed from: private */
    public LinearLayout u;
    /* access modifiers changed from: private */
    public LinearLayout v;
    /* access modifiers changed from: private */
    public int[] w = null;
    /* access modifiers changed from: private */
    public int[] x = null;
    /* access modifiers changed from: private */
    public int[] y = null;
    /* access modifiers changed from: private */
    public int[] z = null;

    public final void a() {
        this.B = this.b.a;
        if (-1 == this.b.j) {
            this.g.setText("");
        } else {
            this.g.setText(m.a(m.a(this.b.j), 2));
        }
        if (-1 == this.b.k) {
            this.h.setText("");
        } else {
            this.h.setText(m.a(m.a(this.b.k), 2));
        }
        this.i.setText(getResources().getStringArray(C0000R.array.Times)[this.b.a]);
        this.j.setText(m.c.format(new Date(a.b(this.b.b))));
        this.k.setText(m.c.format(new Date(a.b(this.b.c))));
        if (-1 == this.b.l) {
            this.l.setText(getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.l.setText(p.b(this.b.l));
        }
        if (-1 == this.b.m) {
            this.m.setText(getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.m.setText(y.c(this.b.m));
        }
        if (this.b.i == null || this.b.i.length() <= 0) {
            this.n.setText(getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.n.setText(this.b.i);
        }
        if (-1 == this.b.d) {
            Cursor rawQuery = e.c().b().rawQuery("SELECT name from TBL_moneytype where uuid = " + b.m().j() + " -1", null);
            if (rawQuery != null && rawQuery.moveToFirst()) {
                this.o.setText(rawQuery.getString(rawQuery.getColumnIndex("name")));
            }
        } else {
            this.o.setText(com.wacai.data.e.a((long) this.b.d));
        }
        if (-1 == this.b.e) {
            this.p.setText(getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.p.setText(h.e((long) this.b.e));
        }
        if (-1 == this.b.f) {
            this.q.setText(getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.q.setText(f.b((long) this.b.f));
        }
        if (b.a) {
            this.r.setVisibility(0);
        } else {
            this.r.setVisibility(8);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.query_reimburse);
        this.b = (QueryInfo) getIntent().getParcelableExtra("QUERYINFO");
        this.c = (Button) findViewById(C0000R.id.btnOK);
        this.d = (Button) findViewById(C0000R.id.btnReset);
        this.e = (Button) findViewById(C0000R.id.btnCancel);
        this.f = (Button) findViewById(C0000R.id.btnMore);
        this.g = (TextView) findViewById(C0000R.id.viewBeninMoney);
        this.h = (TextView) findViewById(C0000R.id.viewEndMoney);
        this.i = (TextView) findViewById(C0000R.id.viewShortcutsDate);
        this.j = (TextView) findViewById(C0000R.id.viewBeginDate);
        this.k = (TextView) findViewById(C0000R.id.viewEndDate);
        this.l = (TextView) findViewById(C0000R.id.viewMainType);
        this.m = (TextView) findViewById(C0000R.id.viewSubType);
        this.n = (TextView) findViewById(C0000R.id.viewMember);
        this.o = (TextView) findViewById(C0000R.id.viewMoneyType);
        this.p = (TextView) findViewById(C0000R.id.viewAccount);
        this.q = (TextView) findViewById(C0000R.id.viewProject);
        this.u = (LinearLayout) findViewById(C0000R.id.beginMoneyItem);
        this.u.setOnClickListener(new dz(this));
        this.v = (LinearLayout) findViewById(C0000R.id.endMoneyItem);
        this.v.setOnClickListener(new ed(this));
        ((LinearLayout) findViewById(C0000R.id.dateShortcutsItem)).setOnClickListener(new eg(this));
        ((LinearLayout) findViewById(C0000R.id.mainTypeItem)).setOnClickListener(new ei(this));
        ((LinearLayout) findViewById(C0000R.id.subTypeItem)).setOnClickListener(new ek(this));
        this.t = (LinearLayout) findViewById(C0000R.id.memberItem);
        this.t.setOnClickListener(new em(this));
        this.r = (LinearLayout) findViewById(C0000R.id.moneyTypeItem);
        this.r.setOnClickListener(new ep(this));
        this.s = (LinearLayout) findViewById(C0000R.id.accountItem);
        this.s.setOnClickListener(new er(this));
        ((LinearLayout) findViewById(C0000R.id.projectItem)).setOnClickListener(new es(this));
        ((LinearLayout) findViewById(C0000R.id.dateBeginItem)).setOnClickListener(new dw(this));
        ((LinearLayout) findViewById(C0000R.id.dateEndItem)).setOnClickListener(new ea(this));
        a();
        Boolean bool = C;
        if (bool != null) {
            if (bool.booleanValue()) {
                this.s.setVisibility(0);
                this.t.setVisibility(0);
                this.u.setVisibility(0);
                this.v.setVisibility(0);
                this.f.setText((int) C0000R.string.txtHideQueryMore);
            } else {
                this.s.setVisibility(8);
                this.t.setVisibility(8);
                this.u.setVisibility(8);
                this.v.setVisibility(8);
                this.f.setText((int) C0000R.string.txtQueryMore);
            }
        }
        this.c.setOnClickListener(this.F);
        this.d.setOnClickListener(this.F);
        this.e.setOnClickListener(this.F);
        this.f.setOnClickListener(this.F);
    }
}
