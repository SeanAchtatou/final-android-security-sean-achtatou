package com.wacai365;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

final class tk extends BaseAdapter {
    private Context a;
    private ll[] b = aaj.a().b();
    private /* synthetic */ WeiboSettingList c;

    public tk(WeiboSettingList weiboSettingList, Context context) {
        this.c = weiboSettingList;
        this.a = context;
    }

    public final int getCount() {
        if (this.b != null) {
            return this.b.length;
        }
        return 0;
    }

    public final Object getItem(int i) {
        if (this.b == null || i >= this.b.length) {
            return null;
        }
        return this.b[i];
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        if (view == null) {
            View inflate = ((LayoutInflater) this.a.getSystemService("layout_inflater")).inflate((int) C0000R.layout.list_item_arrow, (ViewGroup) null);
            View findViewById = inflate.findViewById(C0000R.id.list_arrow_icon);
            if (findViewById != null) {
                findViewById.setBackgroundResource(WeiboSettingList.c[i]);
            }
            view2 = inflate;
        } else {
            view2 = view;
        }
        ll llVar = (ll) getItem(i);
        if (llVar == null) {
            return view2;
        }
        TextView textView = (TextView) view2.findViewById(C0000R.id.list_arrow_text1);
        if (textView != null) {
            textView.setText(llVar.c);
        }
        TextView textView2 = (TextView) view2.findViewById(C0000R.id.list_arrow_text2);
        if (textView2 != null) {
            Resources resources = this.a.getResources();
            if (llVar.a()) {
                textView2.setText(resources.getText(C0000R.string.weiboConnected));
                textView2.setTextColor(resources.getColor(C0000R.color.text_color_weibo_connected));
            } else {
                textView2.setText(resources.getText(C0000R.string.weiboDisconnected));
                textView2.setTextColor(resources.getColor(C0000R.color.text_color_gray));
            }
        }
        return view2;
    }
}
