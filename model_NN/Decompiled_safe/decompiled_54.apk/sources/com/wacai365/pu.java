package com.wacai365;

import android.content.DialogInterface;

final class pu implements DialogInterface.OnClickListener {
    private /* synthetic */ MyNote a;

    pu(MyNote myNote) {
        this.a = myNote;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i < this.a.i.length - 1) {
            String unused = this.a.g = this.a.i[i].replaceAll("Ԫ", "").replaceAll(",", "");
        } else {
            double d = 0.0d;
            for (int i2 = 0; i2 < this.a.i.length - 1; i2++) {
                d += Double.parseDouble(this.a.i[i2].replace("Ԫ", "").replaceAll(",", ""));
            }
            String unused2 = this.a.g = String.format("%.2f", Double.valueOf(d));
        }
        dialogInterface.dismiss();
        this.a.f();
    }
}
