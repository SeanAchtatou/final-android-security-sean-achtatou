package com.wacai365;

import android.util.Log;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

final class gz implements CompoundButton.OnCheckedChangeListener {
    private /* synthetic */ SettingReimburseMgr a;

    gz(SettingReimburseMgr settingReimburseMgr) {
        this.a = settingReimburseMgr;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        ye yeVar = (ye) compoundButton.getTag();
        if (z) {
            if (this.a.n.indexOf(Integer.valueOf((int) yeVar.a)) < 0) {
                this.a.n.add(Integer.valueOf((int) yeVar.a));
                SettingReimburseMgr.a(this.a, yeVar.i);
            }
            if (this.a.k > 0) {
                this.a.d.setClickable(true);
                this.a.d.setEnabled(true);
            }
            this.a.a = (LinearLayout) compoundButton.getParent();
            this.a.a.setBackgroundResource(C0000R.drawable.list_item_checked);
        } else if (!z) {
            this.a.a = (LinearLayout) compoundButton.getParent();
            this.a.a.setBackgroundResource(C0000R.drawable.list_item_main);
            Log.e("index", String.valueOf(this.a.n.indexOf(Integer.valueOf((int) yeVar.a))));
            if (this.a.n.indexOf(Integer.valueOf((int) yeVar.a)) >= 0) {
                this.a.n.remove(this.a.n.indexOf(Integer.valueOf((int) yeVar.a)));
                SettingReimburseMgr.b(this.a, yeVar.i);
            }
            if (this.a.k == 0) {
                this.a.d.setClickable(false);
                this.a.d.setEnabled(false);
            }
        }
        SettingReimburseMgr.l(this.a);
    }
}
