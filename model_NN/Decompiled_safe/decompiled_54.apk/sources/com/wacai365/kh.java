package com.wacai365;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.wacai.e;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class kh extends BaseAdapter {
    String a;
    List b;
    List c;
    xl d;
    ho e;
    private int[] f;
    private Context g;
    private LayoutInflater h;

    kh(Context context, String str, int[] iArr, List list, List list2, xl xlVar, ho hoVar) {
        this.g = context;
        this.a = str;
        this.f = iArr;
        this.b = list;
        this.c = list2;
        this.d = xlVar;
        this.e = hoVar;
        if (this.b == null) {
            this.b = new ArrayList();
        }
        if (this.c == null) {
            this.c = new ArrayList();
        }
        this.h = LayoutInflater.from(this.g);
    }

    public static void a(String str, List list) {
        Cursor cursor;
        if (str != null && list != null) {
            list.clear();
            try {
                Cursor rawQuery = e.c().b().rawQuery(str, null);
                if (rawQuery != null) {
                    try {
                        if (rawQuery.moveToFirst()) {
                            do {
                                HashMap hashMap = new HashMap();
                                list.add(hashMap);
                                hashMap.put("NAME", rawQuery.getString(rawQuery.getColumnIndexOrThrow("name")));
                                hashMap.put("ID", rawQuery.getString(rawQuery.getColumnIndexOrThrow("id")));
                                hashMap.put("star", rawQuery.getString(rawQuery.getColumnIndexOrThrow("star")));
                            } while (rawQuery.moveToNext());
                        }
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        cursor = rawQuery;
                        th = th2;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (Throwable th3) {
                th = th3;
                cursor = null;
            }
        }
    }

    public final boolean areAllItemsEnabled() {
        return false;
    }

    public final int getCount() {
        int size = this.b.size() + this.c.size() + 2;
        return this.b.size() == 0 ? size + 1 : size;
    }

    public final Object getItem(int i) {
        if (i <= 0 || i >= getCount() || i == (getCount() - this.c.size()) - 1) {
            return null;
        }
        if (i == 1 && this.b.size() == 0) {
            return null;
        }
        int size = this.b.size() == 0 ? 1 : this.b.size();
        return i - 1 < size ? this.b.get(i - 1) : this.c.get((i - size) - 2);
    }

    public final long getItemId(int i) {
        Map map = (Map) getItem(i);
        if (map != null) {
            return Long.parseLong((String) map.get("ID"));
        }
        return -1;
    }

    public final int getItemViewType(int i) {
        if (i < 0 || i >= getCount()) {
            return -1;
        }
        if (i == 1 && this.b.size() == 0) {
            return 2;
        }
        return (i == 0 || i == (getCount() - this.c.size()) - 1) ? 0 : 1;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        int itemViewType = getItemViewType(i);
        if (itemViewType < 0 || itemViewType >= getViewTypeCount()) {
            return view;
        }
        if (view == null) {
            int i2 = C0000R.layout.list_item_1_small_withstar;
            if (itemViewType == 0) {
                i2 = C0000R.layout.list_item_groupheader;
            } else if (itemViewType == 2) {
                i2 = C0000R.layout.list_item_1_small_center;
            }
            view2 = this.h.inflate(i2, (ViewGroup) null);
        } else {
            view2 = view;
        }
        TextView textView = (TextView) view2.findViewById(C0000R.id.listitem1);
        if (itemViewType == 1) {
            Map map = (Map) getItem(i);
            textView.setText((CharSequence) map.get("NAME"));
            CheckBox checkBox = (CheckBox) view2.findViewById(C0000R.id.btnStar);
            checkBox.setChecked(((String) map.get("star")).equalsIgnoreCase("1"));
            checkBox.setTag(map.get("ID"));
            checkBox.setOnClickListener(new abp(this));
        } else if (itemViewType == 0) {
            textView.setText(i == 0 ? this.f[0] : this.f[1]);
        } else if (itemViewType == 2) {
            textView.setText(this.g.getResources().getString(C0000R.string.noFavorites));
        }
        return view2;
    }

    public final int getViewTypeCount() {
        return 3;
    }

    public final boolean isEnabled(int i) {
        return getItemViewType(i) == 1;
    }
}
