package com.wacai365;

import android.text.Editable;
import android.text.TextWatcher;

final class b implements TextWatcher {
    private /* synthetic */ WeiboPublish a;

    b(WeiboPublish weiboPublish) {
        this.a = weiboPublish;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.WeiboPublish.a(com.wacai365.WeiboPublish, boolean):boolean
     arg types: [com.wacai365.WeiboPublish, int]
     candidates:
      com.wacai365.WeiboPublish.a(com.wacai365.WeiboPublish, java.lang.String):java.lang.String
      com.wacai365.WeiboPublish.a(com.wacai365.WeiboPublish, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.WeiboPublish.b(com.wacai365.WeiboPublish, boolean):boolean
     arg types: [com.wacai365.WeiboPublish, int]
     candidates:
      com.wacai365.WeiboPublish.b(com.wacai365.WeiboPublish, java.lang.String):java.lang.String
      com.wacai365.WeiboPublish.b(com.wacai365.WeiboPublish, boolean):boolean */
    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        String obj = this.a.c.getText().toString();
        if (this.a.n && !this.a.o) {
            obj = this.a.m;
        } else if (this.a.n && this.a.o) {
            boolean unused = this.a.o = false;
            if (this.a.s) {
                boolean unused2 = this.a.s = false;
                obj = this.a.m + obj.substring(i);
                this.a.c.setText(obj);
                this.a.c.setSelection(obj.length());
            }
            String unused3 = this.a.m = obj;
        }
        int e = 140 - m.e(obj);
        if (e < 0) {
            this.a.e.setText(this.a.getResources().getString(C0000R.string.weiboInputTextExceedCount, Integer.valueOf(-e)));
            this.a.e.setEnabled(false);
            return;
        }
        this.a.e.setText(this.a.getResources().getString(C0000R.string.weiboCharacterCanInput, Integer.valueOf(e)));
        this.a.e.setEnabled(true);
    }
}
