package com.wacai365;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.b;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

final class nk extends BaseAdapter {
    private ArrayList a = null;
    private Cursor b = null;
    private LayoutInflater c = null;
    private Context d = null;
    private /* synthetic */ LoanList e;

    public nk(LoanList loanList, Cursor cursor, ArrayList arrayList, Context context) {
        this.e = loanList;
        this.a = arrayList;
        this.b = cursor;
        this.d = context;
        this.c = this.d == null ? null : (LayoutInflater) this.d.getSystemService("layout_inflater");
    }

    private static String a(long j, Cursor cursor) {
        String str;
        StringBuilder append;
        if (!b.a) {
            return m.b(j);
        }
        if (cursor == null) {
            return "";
        }
        str = "";
        try {
            str = cursor.getString(cursor.getColumnIndexOrThrow("_moneyflag2"));
            return append.toString();
        } catch (Exception e2) {
            return append.toString();
        } finally {
            str + m.b(j);
        }
    }

    public final int getCount() {
        int i = 0;
        if (this.a == null) {
            return 0;
        }
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            yb ybVar = (yb) it.next();
            i = (int) (((long) i) + (ybVar.d - ybVar.c) + 1);
        }
        return i;
    }

    public final Object getItem(int i) {
        String string;
        int i2;
        Iterator it = this.a.iterator();
        long j = 0;
        while (it.hasNext()) {
            yb ybVar = (yb) it.next();
            j++;
            if (((long) i) == ybVar.c) {
                return ybVar;
            }
            if (((long) i) <= ybVar.d) {
                this.b.moveToPosition((int) (((long) i) - j));
                wf wfVar = new wf(this.e);
                wfVar.a = this.b.getLong(this.b.getColumnIndex("_id"));
                wfVar.f = this.b.getInt(this.b.getColumnIndex("_flag"));
                wfVar.g = this.b.getInt(this.b.getColumnIndex("_type"));
                Cursor cursor = this.b;
                int i3 = wfVar.f;
                int i4 = wfVar.g;
                switch (i3) {
                    case 0:
                        string = (cursor.getString(cursor.getColumnIndex("_maintypename")) + "-") + cursor.getString(cursor.getColumnIndex("_name"));
                        break;
                    case 1:
                        string = cursor.getString(cursor.getColumnIndex("_name"));
                        break;
                    case 2:
                        string = this.e.getResources().getString(C0000R.string.txtTransferString).toString();
                        break;
                    case 3:
                        string = this.e.getResources().getString(i4 == 1 ? C0000R.string.loanOutTosb : C0000R.string.loanInFromsb, cursor.getString(cursor.getColumnIndex("_transferinname")));
                        break;
                    case 4:
                        string = this.e.getResources().getString(i4 == 0 ? C0000R.string.paybackFromsb : C0000R.string.paybackTosb, cursor.getString(cursor.getColumnIndex("_transferinname")));
                        break;
                    default:
                        string = "";
                        break;
                }
                wfVar.b = string;
                wfVar.c = a(this.b.getLong(this.b.getColumnIndex("_money")), this.b);
                wfVar.h = this.b.getLong(this.b.getColumnIndex("_outgodate"));
                wfVar.e = m.h.format(new Date(wfVar.h * 1000));
                wfVar.j = this.b.getLong(this.b.getColumnIndex("_paybackid"));
                String string2 = this.b.getString(this.b.getColumnIndexOrThrow("_comment"));
                if (string2 == null || string2.length() <= 0) {
                    string2 = this.d.getResources().getString(C0000R.string.txtNoCommentsString);
                }
                wfVar.d = string2;
                long j2 = (long) wfVar.f;
                long j3 = (long) wfVar.g;
                if (3 != j2) {
                    if (4 == j2) {
                        if (j3 == 0) {
                            i2 = C0000R.drawable.ic_receipt;
                        } else if (j3 == 1) {
                            i2 = C0000R.drawable.ic_repayment;
                        }
                    }
                    i2 = -1;
                } else if (j3 == 1) {
                    i2 = C0000R.drawable.ic_loanout;
                } else {
                    if (j3 == 0) {
                        i2 = C0000R.drawable.ic_loanin;
                    }
                    i2 = -1;
                }
                wfVar.i = i2;
                return wfVar;
            }
        }
        return null;
    }

    public final long getItemId(int i) {
        Object item = getItem(i);
        if (item.getClass() == yb.class) {
            return 0;
        }
        if (item.getClass() == wf.class) {
            return ((wf) item).a;
        }
        return 0;
    }

    public final int getItemViewType(int i) {
        Object item = getItem(i);
        return (item == null || item.getClass() == yb.class) ? 0 : 1;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        Object item = getItem(i);
        int itemViewType = getItemViewType(i);
        View inflate = view == null ? itemViewType == 0 ? this.c.inflate((int) C0000R.layout.balance_list_time_item, (ViewGroup) null) : this.c.inflate((int) C0000R.layout.balance_list_item, (ViewGroup) null) : view;
        if (item == null) {
            return inflate;
        }
        if (itemViewType == 0) {
            TextView textView = (TextView) inflate.findViewById(C0000R.id.datetitle);
            if (textView != null) {
                textView.setText(((yb) item).b);
            }
        } else {
            TextView textView2 = (TextView) inflate.findViewById(C0000R.id.headertitle);
            if (textView2 != null) {
                textView2.setText(((wf) item).b);
            }
            TextView textView3 = (TextView) inflate.findViewById(C0000R.id.headervalue);
            if (textView3 != null) {
                Context context = this.d;
                int i2 = ((wf) item).f;
                if (textView3 != null) {
                    switch (i2) {
                        case 0:
                            textView3.setTextColor(context.getResources().getColor(C0000R.color.outgo_money));
                            break;
                        case 1:
                            textView3.setTextColor(context.getResources().getColor(C0000R.color.income_money));
                            break;
                        case 2:
                        case 3:
                        case 4:
                            textView3.setTextColor(context.getResources().getColor(C0000R.color.transfer_money));
                            break;
                    }
                }
                textView3.setText(((wf) item).c);
            }
            TextView textView4 = (TextView) inflate.findViewById(C0000R.id.id_comments);
            if (textView4 != null) {
                textView4.setText(((wf) item).d);
            }
            TextView textView5 = (TextView) inflate.findViewById(C0000R.id.datetitle);
            if (textView5 != null) {
                textView5.setText(((wf) item).e);
            }
            LinearLayout linearLayout = (LinearLayout) inflate.findViewById(C0000R.id.id_icon);
            if (linearLayout != null) {
                linearLayout.setBackgroundResource(((wf) item).i);
            }
        }
        return inflate;
    }

    public final int getViewTypeCount() {
        return 2;
    }
}
