package com.wacai365;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import com.wacai.b;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

final class gb extends BaseAdapter {
    private ArrayList a = null;
    private Cursor b = null;
    private LayoutInflater c = null;
    private Context d = null;
    private /* synthetic */ SettingReimburseMgr e;

    public gb(SettingReimburseMgr settingReimburseMgr, Cursor cursor, ArrayList arrayList, Context context) {
        this.e = settingReimburseMgr;
        this.a = arrayList;
        this.b = cursor;
        this.d = context;
        this.c = this.d == null ? null : (LayoutInflater) this.d.getSystemService("layout_inflater");
    }

    private Boolean a(long j) {
        if (this.e.n == null) {
            return false;
        }
        for (int i = 0; i < this.e.n.size(); i++) {
            if (j == ((long) ((Integer) this.e.n.get(i)).intValue())) {
                return true;
            }
        }
        return false;
    }

    private static String a(long j, Cursor cursor) {
        String str;
        StringBuilder append;
        if (!b.a) {
            return m.b(j);
        }
        if (cursor == null) {
            return "";
        }
        str = "";
        try {
            str = cursor.getString(cursor.getColumnIndexOrThrow("_moneyflag1"));
            return append.toString();
        } catch (Exception e2) {
            return append.toString();
        } finally {
            str + m.b(j);
        }
    }

    public final boolean areAllItemsEnabled() {
        return false;
    }

    public final int getCount() {
        int i = 0;
        if (this.a == null) {
            return 0;
        }
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            ry ryVar = (ry) it.next();
            i = (int) (((long) i) + (ryVar.d - ryVar.c) + 1);
        }
        return i;
    }

    public final Object getItem(int i) {
        Iterator it = this.a.iterator();
        long j = 0;
        while (it.hasNext()) {
            ry ryVar = (ry) it.next();
            j++;
            if (((long) i) == ryVar.c) {
                return ryVar;
            }
            if (((long) i) <= ryVar.d) {
                this.b.moveToPosition((int) (((long) i) - j));
                ye yeVar = new ye(this.e);
                yeVar.a = this.b.getLong(this.b.getColumnIndex("_id"));
                yeVar.b = (long) this.b.getInt(this.b.getColumnIndex("_ymd"));
                Cursor cursor = this.b;
                yeVar.c = (cursor.getString(cursor.getColumnIndex("_maintypename")) + "-") + cursor.getString(cursor.getColumnIndex("_name"));
                yeVar.i = this.b.getLong(this.b.getColumnIndex("_money"));
                yeVar.d = a(yeVar.i, this.b);
                yeVar.g = this.b.getLong(this.b.getColumnIndex("_outgodate"));
                yeVar.f = m.h.format(new Date(yeVar.g * 1000));
                yeVar.k = this.b.getLong(this.b.getColumnIndex("_moneytypeid1"));
                yeVar.j = this.b.getLong(this.b.getColumnIndex("_ymd"));
                String string = this.b.getString(this.b.getColumnIndexOrThrow("_comment"));
                if (string == null || string.length() <= 0) {
                    string = this.d.getResources().getString(C0000R.string.txtNoCommentsString);
                }
                yeVar.l = this.b.getLong(this.b.getColumnIndex("_projectID"));
                yeVar.e = string;
                yeVar.h = C0000R.drawable.ic_outgo;
                yeVar.m = a(yeVar.a);
                return yeVar;
            }
        }
        return null;
    }

    public final long getItemId(int i) {
        Object item = getItem(i);
        if (item.getClass() == ry.class) {
            return 0;
        }
        if (item.getClass() == ye.class) {
            return ((ye) item).a;
        }
        return 0;
    }

    public final int getItemViewType(int i) {
        Object item = getItem(i);
        return (item == null || item.getClass() == ry.class) ? 0 : 1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00f3  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0117  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.view.View getView(int r10, android.view.View r11, android.view.ViewGroup r12) {
        /*
            r9 = this;
            r4 = 0
            r5 = 2131492899(0x7f0c0023, float:1.8609263E38)
            r8 = 0
            java.lang.Object r0 = r9.getItem(r10)
            int r1 = r9.getItemViewType(r10)
            if (r11 != 0) goto L_0x0122
            if (r1 != 0) goto L_0x001e
            android.view.LayoutInflater r2 = r9.c
            r3 = 2130903051(0x7f03000b, float:1.741291E38)
            android.view.View r2 = r2.inflate(r3, r4)
        L_0x001a:
            if (r0 != 0) goto L_0x0028
            r0 = r2
        L_0x001d:
            return r0
        L_0x001e:
            android.view.LayoutInflater r2 = r9.c
            r3 = 2130903120(0x7f030050, float:1.741305E38)
            android.view.View r2 = r2.inflate(r3, r4)
            goto L_0x001a
        L_0x0028:
            if (r1 != 0) goto L_0x003b
            android.view.View r9 = r2.findViewById(r5)
            android.widget.TextView r9 = (android.widget.TextView) r9
            if (r9 == 0) goto L_0x0039
            com.wacai365.ry r0 = (com.wacai365.ry) r0
            java.lang.String r0 = r0.b
            r9.setText(r0)
        L_0x0039:
            r0 = r2
            goto L_0x001d
        L_0x003b:
            com.wacai365.ye r0 = (com.wacai365.ye) r0
            r1 = 2131492900(0x7f0c0024, float:1.8609265E38)
            android.view.View r1 = r2.findViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            if (r1 == 0) goto L_0x004d
            java.lang.String r3 = r0.c
            r1.setText(r3)
        L_0x004d:
            r1 = 2131492901(0x7f0c0025, float:1.8609267E38)
            android.view.View r1 = r2.findViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            if (r1 == 0) goto L_0x006d
            android.content.Context r3 = r9.d
            android.content.res.Resources r3 = r3.getResources()
            r4 = 2131165222(0x7f070026, float:1.7944655E38)
            int r3 = r3.getColor(r4)
            r1.setTextColor(r3)
            java.lang.String r3 = r0.d
            r1.setText(r3)
        L_0x006d:
            r1 = 2131492902(0x7f0c0026, float:1.860927E38)
            android.view.View r1 = r2.findViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            if (r1 == 0) goto L_0x007d
            java.lang.String r3 = r0.e
            r1.setText(r3)
        L_0x007d:
            android.view.View r1 = r2.findViewById(r5)
            android.widget.TextView r1 = (android.widget.TextView) r1
            if (r1 == 0) goto L_0x008a
            java.lang.String r3 = r0.f
            r1.setText(r3)
        L_0x008a:
            r1 = 2131492898(0x7f0c0022, float:1.860926E38)
            android.view.View r1 = r2.findViewById(r1)
            android.widget.LinearLayout r1 = (android.widget.LinearLayout) r1
            if (r1 == 0) goto L_0x009a
            int r3 = r0.h
            r1.setBackgroundResource(r3)
        L_0x009a:
            com.wacai365.SettingReimburseMgr r3 = r9.e
            r1 = 2131492897(0x7f0c0021, float:1.8609259E38)
            android.view.View r1 = r2.findViewById(r1)
            android.widget.LinearLayout r1 = (android.widget.LinearLayout) r1
            r3.a = r1
            r1 = 2131493132(0x7f0c010c, float:1.8609736E38)
            android.view.View r1 = r2.findViewById(r1)
            android.widget.CheckBox r1 = (android.widget.CheckBox) r1
            if (r1 == 0) goto L_0x0039
            r1.setTag(r0)
            long r3 = r0.a
            com.wacai365.SettingReimburseMgr r0 = r9.e
            java.util.ArrayList r0 = r0.n
            if (r0 == 0) goto L_0x0112
            r5 = r8
        L_0x00c0:
            com.wacai365.SettingReimburseMgr r0 = r9.e
            java.util.ArrayList r0 = r0.n
            int r0 = r0.size()
            if (r5 >= r0) goto L_0x0112
            com.wacai365.SettingReimburseMgr r0 = r9.e
            java.util.ArrayList r0 = r0.n
            java.lang.Object r0 = r0.get(r5)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            long r6 = (long) r0
            int r0 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r0 != 0) goto L_0x010e
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
        L_0x00e6:
            boolean r0 = r0.booleanValue()
            r1.setChecked(r0)
            boolean r0 = r1.isChecked()
            if (r0 == 0) goto L_0x0117
            com.wacai365.SettingReimburseMgr r0 = r9.e
            android.widget.LinearLayout r0 = r0.a
            r3 = 2130837687(0x7f0200b7, float:1.7280335E38)
            r0.setBackgroundResource(r3)
        L_0x00fd:
            com.wacai365.SettingReimburseMgr r0 = r9.e
            com.wacai365.SettingReimburseMgr.l(r0)
            com.wacai365.gz r0 = new com.wacai365.gz
            com.wacai365.SettingReimburseMgr r3 = r9.e
            r0.<init>(r3)
            r1.setOnCheckedChangeListener(r0)
            goto L_0x0039
        L_0x010e:
            int r0 = r5 + 1
            r5 = r0
            goto L_0x00c0
        L_0x0112:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r8)
            goto L_0x00e6
        L_0x0117:
            com.wacai365.SettingReimburseMgr r0 = r9.e
            android.widget.LinearLayout r0 = r0.a
            r3 = 2130837688(0x7f0200b8, float:1.7280337E38)
            r0.setBackgroundResource(r3)
            goto L_0x00fd
        L_0x0122:
            r2 = r11
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.gb.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }

    public final int getViewTypeCount() {
        return 2;
    }

    public final boolean isEnabled(int i) {
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            if (((long) i) == ((ry) it.next()).c) {
                return false;
            }
        }
        return true;
    }
}
