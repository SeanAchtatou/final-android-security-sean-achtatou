package com.wacai365;

import android.content.Intent;
import android.view.View;

final class qg implements View.OnClickListener {
    private /* synthetic */ SettingLoanAccountMgr a;

    qg(SettingLoanAccountMgr settingLoanAccountMgr) {
        this.a = settingLoanAccountMgr;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.a)) {
            this.a.startActivityForResult(new Intent(this.a, InputLoanAccount.class), 0);
        } else if (view.equals(this.a.b)) {
            this.a.finish();
        }
    }
}
