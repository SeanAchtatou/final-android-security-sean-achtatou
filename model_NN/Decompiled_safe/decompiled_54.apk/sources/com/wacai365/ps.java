package com.wacai365;

import android.view.ContextMenu;
import android.view.View;

final class ps implements View.OnCreateContextMenuListener {
    private /* synthetic */ MyNote a;

    ps(MyNote myNote) {
        this.a = myNote;
    }

    public final void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.clear();
        this.a.getMenuInflater().inflate(C0000R.menu.mynote_list_context, contextMenu);
    }
}
