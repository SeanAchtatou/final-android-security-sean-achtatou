package com.wacai365;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai365.widget.Pie;
import java.util.Hashtable;

public final class yc extends BaseAdapter {
    private Context a;
    private int b;
    private LayoutInflater c = ((LayoutInflater) this.a.getSystemService("layout_inflater"));
    private Pie d;

    public yc(Context context, Pie pie, int i) {
        this.a = context;
        this.d = pie;
        this.b = i;
    }

    public final int getCount() {
        return this.d.b.size();
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        Hashtable hashtable = (Hashtable) this.d.b.get(i);
        if (hashtable == null) {
            return null;
        }
        LinearLayout linearLayout = view == null ? (LinearLayout) this.c.inflate(this.b, (ViewGroup) null) : (LinearLayout) view;
        ((ImageView) linearLayout.findViewById(C0000R.id.listitem1)).setBackgroundColor(this.a.getResources().getColor(Pie.a[i % Pie.a.length]));
        ((TextView) linearLayout.findViewById(C0000R.id.listitem2)).setText((CharSequence) hashtable.get("TAG_LABLE"));
        TextView textView = (TextView) linearLayout.findViewById(C0000R.id.listitem3);
        if (this.d.c == 0.0d) {
            textView.setText("0.00%");
        } else {
            textView.setText(m.a((100.0d * Double.parseDouble((String) hashtable.get("TAG_FIRST"))) / this.d.c, 2) + "%");
        }
        return linearLayout;
    }
}
