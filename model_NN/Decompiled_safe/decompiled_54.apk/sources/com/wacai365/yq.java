package com.wacai365;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.a.a;
import com.wacai.data.h;
import java.util.Date;

public class yq extends yi {
    /* access modifiers changed from: private */
    public TextView a;
    /* access modifiers changed from: private */
    public TextView b;
    /* access modifiers changed from: private */
    public TextView c;
    /* access modifiers changed from: private */
    public TextView d;
    /* access modifiers changed from: private */
    public TextView e;
    /* access modifiers changed from: private */
    public TextView f;
    /* access modifiers changed from: private */
    public TextView g;
    /* access modifiers changed from: private */
    public TextView h;
    /* access modifiers changed from: private */
    public TextView i;
    /* access modifiers changed from: private */
    public TextView j;
    /* access modifiers changed from: private */
    public int[] k = null;
    /* access modifiers changed from: private */
    public int[] l = null;
    /* access modifiers changed from: private */
    public String[] m = null;
    private rk r = null;
    /* access modifiers changed from: private */
    public int s = -1;
    /* access modifiers changed from: private */
    public QueryInfo t;

    public yq(QueryInfo queryInfo) {
        this.t = queryInfo;
    }

    public final void a(Context context, LinearLayout linearLayout, LinearLayout linearLayout2, Activity activity, boolean z) {
        super.a(context, linearLayout, linearLayout2, activity, true);
        LinearLayout linearLayout3 = (LinearLayout) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) C0000R.layout.query_loan_setting_list, (ViewGroup) null).findViewById(C0000R.id.mainlayout);
        linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.removeAllViews();
        linearLayout.addView(linearLayout3);
        this.a = (TextView) linearLayout3.findViewById(C0000R.id.viewBeninMoney);
        this.b = (TextView) linearLayout3.findViewById(C0000R.id.viewEndMoney);
        this.c = (TextView) linearLayout3.findViewById(C0000R.id.viewShortcutsDate);
        this.d = (TextView) linearLayout3.findViewById(C0000R.id.viewBeginDate);
        this.e = (TextView) linearLayout3.findViewById(C0000R.id.viewEndDate);
        this.f = (TextView) linearLayout3.findViewById(C0000R.id.viewOnLoanType);
        this.g = (TextView) linearLayout3.findViewById(C0000R.id.viewCreditorAndDebtor);
        this.h = (TextView) linearLayout3.findViewById(C0000R.id.viewLoanAccount);
        this.i = (TextView) linearLayout3.findViewById(C0000R.id.tvCreditorAndDebtor);
        this.j = (TextView) linearLayout3.findViewById(C0000R.id.tvLoanAccount);
        ((LinearLayout) this.p.findViewById(C0000R.id.beginMoneyItem)).setOnClickListener(new ng(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.endMoneyItem)).setOnClickListener(new nf(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.dateShortcutsItem)).setOnClickListener(new mz(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.loanTypeItem)).setOnClickListener(new mx(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.creditorAndDebtorItem)).setOnClickListener(new nd(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.loanAccountItem)).setOnClickListener(new nb(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.dateBeginItem)).setOnClickListener(new ny(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.dateEndItem)).setOnClickListener(new nz(this));
        a_();
    }

    public final void a(Object obj) {
        if (QueryInfo.class.isInstance(obj)) {
            this.t = (QueryInfo) obj;
        } else if (this.t == null) {
            this.t = new QueryInfo();
        }
        this.t.l = -1;
        this.t.m = -1;
    }

    public final void a_() {
        this.s = this.t.a;
        if (-1 == this.t.j) {
            this.a.setText("");
        } else {
            this.a.setText(m.a(m.a(this.t.j), 2));
        }
        if (-1 == this.t.k) {
            this.b.setText("");
        } else {
            this.b.setText(m.a(m.a(this.t.k), 2));
        }
        this.c.setText(this.n.getResources().getStringArray(C0000R.array.Times)[this.t.a]);
        this.d.setText(m.c.format(new Date(a.b(this.t.b))));
        this.e.setText(m.c.format(new Date(a.b(this.t.c))));
        if (-1 == this.t.p) {
            this.f.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.m = this.n.getResources().getStringArray(C0000R.array.SearchLoanType);
            this.f.setText(this.m[this.t.p]);
        }
        if (-1 == this.t.q) {
            this.g.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.g.setText(h.e((long) this.t.q));
        }
        if (-1 == this.t.e) {
            this.h.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.h.setText(h.e((long) this.t.e));
        }
    }

    public final Object b() {
        return this.t;
    }

    public final boolean c() {
        this.t.t = 8;
        return true;
    }
}
