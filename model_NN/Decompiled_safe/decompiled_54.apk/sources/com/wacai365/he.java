package com.wacai365;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

final class he implements AdapterView.OnItemClickListener {
    private /* synthetic */ StatMain a;

    he(StatMain statMain) {
        this.a = statMain;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (view != null) {
            try {
                fr frVar = (fr) this.a.a.getItemAtPosition(i);
                if (frVar != null) {
                    Intent intent = new Intent(this.a, Class.forName(frVar.c));
                    intent.putExtra("LaunchedByApplication", 1);
                    intent.putExtra("Provider_Type", i >= StatMain.b.length ? 0 : StatMain.b[i]);
                    this.a.startActivityForResult(intent, 0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
