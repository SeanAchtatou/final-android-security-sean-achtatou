package com.wacai365;

import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class QueryStatByMonth extends QueryStatBase {
    public QueryStatByMonth() {
        super(b);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.query_stat_bymonth);
        this.c = (QueryInfo) getIntent().getParcelableExtra("QUERYINFO");
        this.d = (QueryInfo) getIntent().getParcelableExtra("SRCQUERYINFO");
        this.t = (LinearLayout) findViewById(C0000R.id.layoutDateStart);
        this.u = (TextView) findViewById(C0000R.id.tvStartDate);
        this.v = (LinearLayout) findViewById(C0000R.id.layoutDateEnd);
        this.w = (TextView) findViewById(C0000R.id.tvEndDate);
        this.k = (LinearLayout) findViewById(C0000R.id.layoutProject);
        this.l = (TextView) findViewById(C0000R.id.tvProject);
        this.g = (LinearLayout) findViewById(C0000R.id.layoutMoneyType);
        this.h = (TextView) findViewById(C0000R.id.tvMoneyType);
        this.i = (LinearLayout) findViewById(C0000R.id.layoutAccount);
        this.j = (TextView) findViewById(C0000R.id.tvAccount);
        this.m = (LinearLayout) findViewById(C0000R.id.layoutReimburse);
        this.n = (TextView) findViewById(C0000R.id.tvReimburse);
        this.x = (LinearLayout) findViewById(C0000R.id.layoutMember);
        this.y = (TextView) findViewById(C0000R.id.tvMember);
        this.z = (Button) findViewById(C0000R.id.btnOK);
        this.A = (Button) findViewById(C0000R.id.btnCancel);
        this.B = (Button) findViewById(C0000R.id.btnReset);
        a();
    }
}
