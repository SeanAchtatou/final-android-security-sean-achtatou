package com.wacai365;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

public class ShortcutsList extends Activity {
    private ListView a;
    private ListAdapter b;
    private int[] c = {C0000R.string.txtWriteTitle, C0000R.string.txtShortcutsTitle, C0000R.string.txtMainProgram, C0000R.string.txtNewNote};

    public void onCreate(Bundle bundle) {
        super.setTheme((int) C0000R.style.WaicaiTheme);
        super.onCreate(bundle);
        this.a = new ListView(this);
        this.a.setDivider(getResources().getDrawable(C0000R.drawable.listview_divider));
        this.a.setDividerHeight(1);
        this.a.setBackgroundDrawable(getResources().getDrawable(C0000R.drawable.bgMain));
        setContentView(this.a);
        String[] strArr = new String[this.c.length];
        for (int i = 0; i < this.c.length; i++) {
            strArr[i] = getResources().getText(this.c[i]).toString();
        }
        this.b = new ArrayAdapter(this, (int) C0000R.layout.list_item_withoutcheckable, (int) C0000R.id.listitem1, strArr);
        this.a.setAdapter(this.b);
        this.a.setOnItemClickListener(new ji(this));
    }
}
