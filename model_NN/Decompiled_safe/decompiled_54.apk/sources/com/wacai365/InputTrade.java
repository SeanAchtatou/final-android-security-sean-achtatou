package com.wacai365;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import com.wacai.a.a;
import com.wacai.data.aa;
import com.wacai.data.r;
import com.wacai.data.z;
import java.text.SimpleDateFormat;
import java.util.Date;

public class InputTrade extends WacaiActivity {
    /* access modifiers changed from: private */
    public int A = 0;
    private yf B;
    private bj C;
    private ml F;
    private be G;
    private CompoundButton.OnCheckedChangeListener H = new it(this);
    private View.OnClickListener I = new iu(this);
    long a = 0;
    /* access modifiers changed from: private */
    public RadioButton b;
    /* access modifiers changed from: private */
    public RadioButton c;
    /* access modifiers changed from: private */
    public RadioButton d;
    /* access modifiers changed from: private */
    public RadioButton e;
    /* access modifiers changed from: private */
    public Button f;
    /* access modifiers changed from: private */
    public Button g;
    /* access modifiers changed from: private */
    public Button h;
    /* access modifiers changed from: private */
    public Button i;
    private long j = -1;
    /* access modifiers changed from: private */
    public int k;
    private String l = "";
    private int m = 0;
    private int n = 0;
    private ScrollView o = null;
    private LinearLayout p = null;
    private LinearLayout q = null;
    /* access modifiers changed from: private */
    public oj r = null;
    private LinearLayout s = null;
    private LinearLayout t = null;
    /* access modifiers changed from: private */
    public CheckBox u = null;
    /* access modifiers changed from: private */
    public CheckBox v = null;
    /* access modifiers changed from: private */
    public boolean w = false;
    /* access modifiers changed from: private */
    public int x = 1;
    /* access modifiers changed from: private */
    public sd y = new sd();
    /* access modifiers changed from: private */
    public fy z;

    static Intent a(Context context, String str, long j2) {
        if (j2 > 0) {
            return new Intent(context, InputTrade.class);
        }
        Intent intent = new Intent(context, InputTrade.class);
        if (str == null || str.length() <= 0) {
            return intent;
        }
        intent.putExtra("Extra_Money", str);
        return intent;
    }

    private void a() {
        if (this.i != null) {
            m.a(this.i, getResources());
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        boolean z3;
        oj ojVar;
        boolean z4;
        RadioButton radioButton;
        boolean z5;
        boolean z6;
        ca caVar;
        boolean z7;
        switch (this.k) {
            case 0:
                if (!yf.class.isInstance(this.r)) {
                    if (this.B == null) {
                        this.B = new yf(this.j);
                        z7 = true;
                    } else {
                        z7 = false;
                    }
                    ojVar = this.B;
                    RadioButton radioButton2 = this.c;
                    b();
                    z4 = z7;
                    radioButton = radioButton2;
                    break;
                } else {
                    return;
                }
            case 1:
                if (!bj.class.isInstance(this.r)) {
                    if (this.C == null) {
                        this.C = new bj(this.j);
                        z6 = true;
                    } else {
                        z6 = false;
                    }
                    ojVar = this.C;
                    RadioButton radioButton3 = this.b;
                    b();
                    z4 = z6;
                    radioButton = radioButton3;
                    break;
                } else {
                    return;
                }
            case 2:
                if (!ml.class.isInstance(this.r)) {
                    if (this.F == null) {
                        this.F = new ml(this.j);
                        z5 = true;
                    } else {
                        z5 = false;
                    }
                    ojVar = this.F;
                    RadioButton radioButton4 = this.d;
                    a();
                    z4 = z5;
                    radioButton = radioButton4;
                    break;
                } else {
                    return;
                }
            case 3:
            case 4:
                if (!be.class.isInstance(this.r)) {
                    if (this.G == null) {
                        Intent intent = getIntent();
                        intent.putExtra("Extra_Type", this.k);
                        intent.putExtra("Extra_Money", this.l);
                        intent.putExtra("Extra_DebtID", this.m);
                        intent.putExtra("Extra_Type2", this.n);
                        this.G = new be(this.j, intent);
                        z3 = true;
                    } else {
                        z3 = false;
                    }
                    ojVar = this.G;
                    RadioButton radioButton5 = this.e;
                    a();
                    z4 = z3;
                    radioButton = radioButton5;
                    break;
                } else {
                    return;
                }
            default:
                return;
        }
        if (z2 && radioButton != null) {
            radioButton.setChecked(true);
        }
        if (ojVar != null) {
            if (this.r != null) {
                ojVar.a(this.r.b());
                ojVar.a(this.r.a());
                caVar = this.r.e();
            } else {
                caVar = null;
            }
            this.r = ojVar;
            this.r.a(this, this.p, this.q, this, false);
            if (z4) {
                this.r.a(this.o, m.h(this, 50));
            }
            this.r.a(caVar);
            if (this.k == 0) {
                this.v.setVisibility(0);
            } else {
                this.v.setVisibility(4);
            }
            Intent intent2 = getIntent();
            if (intent2 != null && intent2.getStringExtra("Text_String") != null) {
                this.r.a(16, -1, intent2);
            }
        }
    }

    private DialogInterface.OnClickListener b(boolean z2) {
        return new iz(this, z2);
    }

    private void b() {
        if (this.i != null) {
            this.i.setText(getResources().getString(C0000R.string.txtSaveAsShortcut));
            this.i.setOnClickListener(this.I);
            this.i.setEnabled(true);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        this.a = 0;
        if (this.k != 0) {
            if (this.u.isChecked()) {
                this.A = -1;
                m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtContinuePrompt);
                d();
                return;
            }
            setResult(-1, getIntent());
            if (this.x != 0) {
                abt.b(this);
            } else {
                finish();
            }
        } else if (this.u.isChecked() && !this.v.isChecked()) {
            this.A = -1;
            m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtContinuePrompt);
            d();
        } else if (!this.u.isChecked() && !this.v.isChecked()) {
            setResult(-1, getIntent());
            if (this.x != 0) {
                abt.b(this);
            } else {
                finish();
            }
        } else if (this.u.isChecked() && this.v.isChecked()) {
            this.A = -1;
            e();
            d();
        } else if (!this.u.isChecked() && this.v.isChecked()) {
            setResult(-1, getIntent());
            e();
            if (this.x != 0) {
                abt.b(this);
            } else {
                finish();
            }
        }
    }

    private void d() {
        this.r.f();
        Intent intent = getIntent();
        intent.putExtra("Text_String", "");
        intent.putExtra("Text_String", "");
    }

    private void e() {
        boolean z2;
        String str;
        if (this.r != null && this.k == 0) {
            ll[] b2 = aaj.a().b();
            int length = b2.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    z2 = false;
                    break;
                } else if (b2[i2].a()) {
                    z2 = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (z2) {
                if (this.r == null || this.k != 0) {
                    str = "";
                } else {
                    z zVar = (z) this.r.b();
                    long b3 = zVar.b() * 1000;
                    String string = a.a(b3) == a.a(new Date(System.currentTimeMillis()).getTime()) ? getResources().getString(C0000R.string.txtToday) : new SimpleDateFormat("MM月dd日").format(new Date(b3));
                    int i3 = C0000R.string.weiboShareOutgoWithMerchant;
                    String a2 = zVar.r() > 0 ? aa.a("TBL_TRADETARGET", "name", (int) zVar.r()) : null;
                    if (a2 == null || a2.length() <= 0) {
                        i3 = C0000R.string.weiboShareOutgoWithType;
                        a2 = aa.a("TBL_OUTGOSUBTYPEINFO", "name", (int) zVar.a());
                    }
                    str = getResources().getString(i3, string, a2, m.b(zVar.o()));
                }
                Intent intent = new Intent(this, WeiboPublish.class);
                intent.putExtra("publish-content-txt", str);
                startActivity(intent);
                m.a(this, (Animation) null, -1, (View) null, (int) C0000R.string.weiboTxtSaveSucceed);
            }
        }
    }

    static /* synthetic */ void h(InputTrade inputTrade) {
        if (inputTrade.r.c()) {
            WidgetProvider.a(inputTrade);
            if (yf.class.isInstance(inputTrade.r)) {
                inputTrade.a = ((z) inputTrade.r.b()).p();
            }
            if (m.a((int) inputTrade.a)) {
                m.a(inputTrade, inputTrade.a, inputTrade.b(true), inputTrade.b(false));
            } else {
                inputTrade.c();
            }
        }
    }

    public void finish() {
        super.finish();
        if (this.x == 0) {
            abt.a(this, false);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 != 31) {
            if (intent != null && i3 == -1) {
                switch (i2) {
                    case 16:
                    case 34:
                        getIntent().putExtra("Text_String", intent.getStringExtra("Text_String"));
                        break;
                }
            }
            if (this.r != null) {
                this.r.a(i2, i3, intent);
            }
            if (this.z != null) {
                this.z.a(i2, i3, intent);
            }
        } else if (i3 != -1 && this.v != null) {
            this.v.setChecked(false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.wg.a(com.wacai365.y, boolean):void
     arg types: [com.wacai365.y, int]
     candidates:
      com.wacai365.oj.a(android.content.Context, com.wacai.data.s):boolean
      com.wacai365.wg.a(android.widget.ScrollView, int):void
      com.wacai365.wg.a(com.wacai365.y, boolean):void */
    public void onCreate(Bundle bundle) {
        long j2;
        super.onCreate(bundle);
        this.x = getIntent().getIntExtra("LaunchedByApplication", 0);
        setContentView((int) C0000R.layout.input_trade);
        Intent intent = getIntent();
        this.k = intent.getIntExtra("Extra_Type", 0);
        this.j = intent.getLongExtra("Extra_Id", -1);
        this.n = intent.getIntExtra("Extra_Type2", 0);
        this.m = intent.getIntExtra("Extra_DebtID", 0);
        this.l = intent.getStringExtra("Extra_Money");
        if (this.j > 0) {
            setTitle((int) C0000R.string.txtEditTradeTitle);
        }
        this.b = (RadioButton) findViewById(C0000R.id.btnIncome);
        this.c = (RadioButton) findViewById(C0000R.id.btnOutgo);
        this.d = (RadioButton) findViewById(C0000R.id.btnTransfer);
        this.e = (RadioButton) findViewById(C0000R.id.btnOnLoan);
        this.o = (ScrollView) findViewById(C0000R.id.baseScrollLayout);
        this.s = (LinearLayout) findViewById(C0000R.id.menubar);
        this.t = (LinearLayout) findViewById(C0000R.id.id_bottombar);
        this.p = (LinearLayout) findViewById(C0000R.id.baselayout);
        this.q = (LinearLayout) findViewById(C0000R.id.id_popupframe);
        this.f = (Button) findViewById(C0000R.id.btnSave);
        this.g = (Button) findViewById(C0000R.id.btnCancel);
        this.u = (CheckBox) findViewById(C0000R.id.id_continue);
        this.v = (CheckBox) findViewById(C0000R.id.id_save_and_share);
        this.v.setOnCheckedChangeListener(this.H);
        if (this.j > 0) {
            this.h = (Button) findViewById(C0000R.id.btnDelete);
            this.h.setOnClickListener(this.I);
        } else {
            this.i = (Button) findViewById(C0000R.id.btnDelete);
            this.i.setText(getResources().getString(C0000R.string.txtSaveAsShortcut));
            this.i.setOnClickListener(this.I);
        }
        a(true);
        this.b.setGravity(17);
        this.c.setGravity(17);
        this.d.setGravity(17);
        this.e.setGravity(17);
        this.b.setOnCheckedChangeListener(this.H);
        this.c.setOnCheckedChangeListener(this.H);
        this.d.setOnCheckedChangeListener(this.H);
        this.e.setOnCheckedChangeListener(this.H);
        this.f.setOnClickListener(this.I);
        this.g.setOnClickListener(this.I);
        String stringExtra = intent.getStringExtra("Extra_Comment");
        if (stringExtra != null) {
            this.r.a(stringExtra);
        }
        long longExtra = intent.getLongExtra("Extra_Date", -1);
        if (longExtra > 0) {
            this.r.a(longExtra);
        }
        String stringExtra2 = intent.getStringExtra("Extra_Money");
        if (stringExtra2 == null || stringExtra2.length() <= 0) {
            j2 = 0;
        } else {
            j2 = aa.a(Double.parseDouble(stringExtra2));
            this.r.b(j2);
        }
        if (this.j > 0) {
            this.b.setEnabled(false);
            this.d.setEnabled(false);
            this.e.setEnabled(false);
            this.c.setEnabled(false);
            if (this.k != 0) {
                this.t.setVisibility(8);
            } else if (z.d(this.j).g() > 0) {
                this.t.setVisibility(8);
            } else {
                this.u.setVisibility(8);
            }
            if (this.j > 0 && this.r != null) {
                Object b2 = this.r.b();
                if ((r.class.isInstance(b2) ? ((r) b2).e() : z.class.isInstance(b2) ? ((z) b2).g() : 0) > 0) {
                    m.a(this.f, getResources());
                    m.a(this.h, getResources());
                    m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtEditScheduleData);
                }
            }
        } else {
            String stringExtra3 = intent.getStringExtra("Text_String");
            if (0 == j2 && stringExtra3 == null) {
                this.r.a(y.PROPERTY_LAUNCHMONEYFIRST, true);
            }
        }
        if (this.x == 0) {
            this.y = abt.a(this, 104, null, new jh(this));
            this.y.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        WidgetProvider.a(this);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (this.r != null && this.r.h()) {
            return true;
        }
        setResult(this.A, getIntent());
        if (this.A != -1 || this.x == 0) {
            finish();
        } else {
            abt.b(this);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.r != null) {
            this.r.g();
        }
        if (this.w) {
            if (this.v != null) {
                this.v.setChecked(false);
                ll[] b2 = aaj.a().b();
                int length = b2.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        break;
                    } else if (b2[i2].a()) {
                        this.v.setChecked(true);
                        break;
                    } else {
                        i2++;
                    }
                }
            }
            this.w = false;
        }
    }
}
