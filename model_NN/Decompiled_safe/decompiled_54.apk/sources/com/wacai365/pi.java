package com.wacai365;

import android.content.Intent;
import android.view.View;

final class pi implements View.OnClickListener {
    private /* synthetic */ SettingMemberMgr a;

    pi(SettingMemberMgr settingMemberMgr) {
        this.a = settingMemberMgr;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.a)) {
            this.a.startActivityForResult(new Intent(this.a, InputMember.class), 0);
        } else if (view.equals(this.a.b)) {
            this.a.finish();
        }
    }
}
