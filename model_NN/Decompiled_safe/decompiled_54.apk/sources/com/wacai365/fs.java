package com.wacai365;

import android.app.AlertDialog;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

final class fs implements View.OnClickListener {
    private int a = -1;
    private /* synthetic */ AlertCenter b;

    public fs(AlertCenter alertCenter, int i) {
        this.b = alertCenter;
        this.a = i;
    }

    public final void onClick(View view) {
        if (this.a != -1) {
            Cursor cursor = (Cursor) this.b.c.getItem(this.a);
            switch (cursor.getInt(cursor.getColumnIndexOrThrow("_type"))) {
                case 1:
                case 2:
                    AlertCenter alertCenter = this.b;
                    int i = this.a;
                    AlertDialog.Builder builder = new AlertDialog.Builder(alertCenter);
                    View inflate = LayoutInflater.from(alertCenter).inflate((int) C0000R.layout.alert_delete_warning, (ViewGroup) null);
                    pr prVar = new pr(alertCenter, (CheckBox) inflate.findViewById(C0000R.id.cbDeleteForever), i);
                    builder.setView(inflate);
                    builder.setTitle((int) C0000R.string.txtAlertTitleInfo);
                    builder.setMessage((int) C0000R.string.alertRemoveAlertForever);
                    builder.setIcon(17301543);
                    builder.setCancelable(true);
                    builder.setNegativeButton((int) C0000R.string.txtCancel, prVar);
                    builder.setPositiveButton((int) C0000R.string.txtOK, prVar);
                    builder.setOnCancelListener(new pn(alertCenter));
                    builder.show();
                    break;
                default:
                    this.b.a(false, this.a);
                    break;
            }
            AlertCenter.c(this.b);
        }
    }
}
