package com.wacai365;

import com.wacai.b.k;
import com.wacai.b.o;
import com.wacai.b.q;
import com.wacai.e;

final class fi implements q {
    private long a;
    private long b;

    public fi(long j, long j2) {
        this.a = j;
        this.b = j2;
    }

    public final boolean a(o oVar, k kVar) {
        if (kVar == null || !kVar.a) {
            return false;
        }
        long j = (this.a * 10000) + (this.b * 100);
        String str = " AND ymd >= " + j + " AND ymd < " + (99 + j);
        e.c().b().execSQL("UPDATE TBL_OUTGOINFO SET updatestatus = 1 where uuid IS NOT NULL AND uuid <> '' AND scheduleoutgoid = 0" + str);
        e.c().b().execSQL("UPDATE TBL_INCOMEINFO SET updatestatus = 1 where uuid IS NOT NULL AND uuid <> '' AND scheduleincomeid = 0" + str);
        e.c().b().execSQL("UPDATE TBL_TRANSFERINFO SET updatestatus = 1 where uuid IS NOT NULL AND uuid <> ''" + str);
        e.c().b().execSQL("UPDATE TBL_MYNOTE SET updatestatus = 1 where uuid IS NOT NULL AND uuid <> ''" + str);
        m.a();
        return true;
    }
}
