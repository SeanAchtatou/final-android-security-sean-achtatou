package com.wacai365;

import android.view.View;
import android.widget.ExpandableListView;
import java.util.List;
import java.util.Map;

final class ir implements ExpandableListView.OnChildClickListener {
    private /* synthetic */ ChooseOutgoType a;

    ir(ChooseOutgoType chooseOutgoType) {
        this.a = chooseOutgoType;
    }

    public final boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        Map map;
        List l = this.a.k;
        if (i < l.size()) {
            List list = (List) l.get(i);
            if (i2 < list.size() && (map = (Map) list.get(i2)) != null) {
                ChooseOutgoType.a(this.a, Long.parseLong((String) map.get("ID")));
                return true;
            }
        }
        return false;
    }
}
