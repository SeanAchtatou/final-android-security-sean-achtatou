package com.wacai365;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.animation.Animation;
import java.util.Date;

final class jc implements View.OnClickListener {
    final /* synthetic */ bj a;

    jc(bj bjVar) {
        this.a = bjVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String, int, int, int]
     candidates:
      com.wacai365.m.a(android.content.Context, int, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[]
      com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.app.Activity, long, boolean):void
     arg types: [android.app.Activity, long, int]
     candidates:
      com.wacai365.m.a(java.lang.String, android.widget.TextView, android.widget.TextView):double
      com.wacai365.m.a(long, long, long):long
      com.wacai365.m.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String
      com.wacai365.m.a(android.app.Activity, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, java.util.ArrayList, long):void
      com.wacai365.m.a(android.content.Context, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, android.widget.TextView):void
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(java.lang.String, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean, boolean):boolean
      com.wacai365.m.a(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.app.Activity, long, boolean):void */
    public final void onClick(View view) {
        if (!this.a.E) {
            m.a(this.a.o, (Animation) null, 0, (View) null, (int) C0000R.string.txtEditScheduleData);
            return;
        }
        if (this.a.f != null) {
            this.a.f.a(false);
            this.a.f = null;
        }
        if (view.equals(this.a.b)) {
            this.a.f = af.a(this.a.q, this.a.o, m.a(this.a.D.o()), false);
            ((af) this.a.f).a(this.a.H, this.a.b);
        } else if (view.equals(this.a.w)) {
            this.a.F = m.a((Context) this.a.o, false, (DialogInterface.OnClickListener) new jb(this.a));
        } else if (view.equals(this.a.v)) {
            m.a(this.a.o);
        } else if (view.equals(this.a.x)) {
            m.a(this.a.o, this.a.D.d(), this.a.n.getResources().getString(C0000R.string.txtEditComment), 100, 16, false);
        } else if (view.equals(this.a.y)) {
            m.a(this.a.o, this.a.D.r(), true);
        } else if (view.equals(this.a.z)) {
            this.a.G = m.c((Context) this.a.o, false, (DialogInterface.OnClickListener) new ja(this.a));
        } else if (view.equals(this.a.A)) {
            this.a.a = m.a(this.a.o, this.a.D.s(), new iy(this.a), new iv(this.a));
        } else if (view.equals(this.a.B)) {
            new ut(this.a.o, this.a.n, new mc(this), new Date(this.a.D.b() * 1000), 4).show();
        } else if (view.equals(this.a.u)) {
            VoiceInput.a(this.a.o, 100, this.a.D.d());
        }
    }
}
