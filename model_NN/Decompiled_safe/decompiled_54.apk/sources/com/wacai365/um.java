package com.wacai365;

import android.view.View;

final class um implements View.OnClickListener {
    final /* synthetic */ MyBalanceStatByMoneyType a;

    um(MyBalanceStatByMoneyType myBalanceStatByMoneyType) {
        this.a = myBalanceStatByMoneyType;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.MyBalanceStatByMoneyType.a(com.wacai365.MyBalanceStatByMoneyType, boolean):void
     arg types: [com.wacai365.MyBalanceStatByMoneyType, int]
     candidates:
      com.wacai365.MyBalanceStatByMoneyType.a(com.wacai365.MyBalanceStatByMoneyType, int):int
      com.wacai365.MyBalanceStatByMoneyType.a(int, java.lang.String):void
      com.wacai365.MyBalanceStatByMoneyType.a(com.wacai365.MyBalanceStatByMoneyType, boolean):void */
    public final void onClick(View view) {
        int size;
        if (view.equals(this.a.e)) {
            MyBalanceStatByMoneyType.a(this.a, false);
            this.a.a();
        } else if (view.equals(this.a.f)) {
            MyBalanceStatByMoneyType.a(this.a, true);
            this.a.a();
        } else if (view.equals(this.a.g) && (size = this.a.h.size()) > 0) {
            String[] strArr = new String[size];
            for (int i = 0; i < size; i++) {
                strArr[i] = new String(((qa) this.a.h.get(i)).a);
            }
            new og(this.a, strArr, new aas(this)).show();
        }
    }
}
