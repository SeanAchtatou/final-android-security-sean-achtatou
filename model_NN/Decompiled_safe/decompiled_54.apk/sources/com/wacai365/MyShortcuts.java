package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.wacai.e;

public class MyShortcuts extends WacaiActivity {
    /* access modifiers changed from: private */
    public long a;
    private ListView b;
    private TextView c;
    private Button d;
    private Button e;
    /* access modifiers changed from: private */
    public int f = 0;
    /* access modifiers changed from: private */
    public int g = 1;
    /* access modifiers changed from: private */
    public sd h = new sd();
    /* access modifiers changed from: private */
    public fy i;

    static /* synthetic */ DialogInterface.OnClickListener a(MyShortcuts myShortcuts, boolean z) {
        return new gp(myShortcuts, z);
    }

    public void finish() {
        super.finish();
        if (this.g == 0) {
            abt.a(this, false);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (this.i != null) {
            this.i.a(i2, i3, intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.g = getIntent().getIntExtra("LaunchedByApplication", 0);
        setContentView((int) C0000R.layout.shortcut_selector);
        this.b = (ListView) findViewById(C0000R.id.listView);
        this.d = (Button) findViewById(C0000R.id.btnDataMana);
        this.e = (Button) findViewById(C0000R.id.btnCancel);
        this.c = (TextView) findViewById(C0000R.id.id_listhint);
        Cursor rawQuery = e.c().b().rawQuery("select a.id as _id, a.name as _name, a.type as _type, a.accountid as _accountid from TBL_SHORTCUTSINFO a where isdelete <> 1", null);
        startManagingCursor(rawQuery);
        rawQuery.registerDataSetObserver(new fu(rawQuery, this.b, this.c, getResources().getString(C0000R.string.txtChooseShortcutsHint)));
        qf qfVar = new qf(this, C0000R.layout.list_item_line2, rawQuery, new String[]{"_name", "_type", "_id"}, new int[]{C0000R.id.listitem1, C0000R.id.listitem2});
        this.b.setAdapter((ListAdapter) qfVar);
        this.b.setOnItemClickListener(new hb(this, qfVar));
        this.d.setOnClickListener(new ha(this));
        this.e.setOnClickListener(new go(this));
        if (this.g == 0) {
            this.h = abt.a(this, 104, null, new gn(this));
            this.h.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }
}
