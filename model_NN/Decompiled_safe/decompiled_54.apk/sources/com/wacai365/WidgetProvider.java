package com.wacai365;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

public class WidgetProvider extends AppWidgetProvider {
    private static RemoteViews a;

    public static void a(Context context) {
        context.sendBroadcast(new Intent("com.wacai365.action.widget.refresh"));
    }

    /* JADX WARNING: Removed duplicated region for block: B:53:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x014a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r12, android.content.Intent r13) {
        /*
            r11 = this;
            r9 = 0
            r8 = 0
            r7 = 1
            r6 = 0
            super.onReceive(r12, r13)
            java.lang.String r0 = "WidgetProvider"
            java.lang.String r1 = "OnReceive"
            android.util.Log.i(r0, r1)
            java.lang.String r0 = r13.getAction()
            if (r12 == 0) goto L_0x0027
            if (r0 == 0) goto L_0x0027
            java.lang.String r1 = "android.appwidget.action.APPWIDGET_DELETED"
            boolean r1 = r1.equals(r0)
            if (r1 != 0) goto L_0x0027
            java.lang.String r1 = "android.appwidget.action.APPWIDGET_DISABLED"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0028
        L_0x0027:
            return
        L_0x0028:
            java.lang.String r0 = "WidgetProvider"
            java.lang.String r1 = "updateRemoteView"
            android.util.Log.i(r0, r1)
            android.widget.RemoteViews r0 = com.wacai365.WidgetProvider.a
            if (r0 != 0) goto L_0x003b
            java.lang.String r0 = "WidgetProvider"
            java.lang.String r1 = "updateRemoteView no remove view created"
            android.util.Log.i(r0, r1)
            goto L_0x0027
        L_0x003b:
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            long r0 = r0.getTime()
            long r0 = com.wacai.a.a.a(r0)
            java.lang.String r2 = "select max(ymd) as _maxymd from (select ymd from tbl_outgoinfo where paybackid = 0 AND ymd <= %d UNION select ymd from tbl_incomeinfo where paybackid = 0 AND ymd <= %d UNION select ymd from TBL_TRANSFERINFO where type = 0 AND ymd <= %d UNION select ymd from TBL_LOAN where ymd <= %d UNION select ymd from TBL_PAYBACK where ymd <= %d)"
            r3 = 5
            java.lang.Object[] r3 = new java.lang.Object[r3]
            java.lang.Long r4 = java.lang.Long.valueOf(r0)
            r3[r6] = r4
            java.lang.Long r4 = java.lang.Long.valueOf(r0)
            r3[r7] = r4
            r4 = 2
            java.lang.Long r5 = java.lang.Long.valueOf(r0)
            r3[r4] = r5
            r4 = 3
            java.lang.Long r5 = java.lang.Long.valueOf(r0)
            r3[r4] = r5
            r4 = 4
            java.lang.Long r5 = java.lang.Long.valueOf(r0)
            r3[r4] = r5
            java.lang.String r2 = java.lang.String.format(r2, r3)
            com.wacai.e r3 = com.wacai.e.c()
            android.content.Context r3 = r3.a()
            if (r3 == 0) goto L_0x00ab
            r3 = r7
        L_0x007d:
            if (r3 != 0) goto L_0x008a
            com.wacai.e r4 = com.wacai.e.c()     // Catch:{ Exception -> 0x0153, all -> 0x014e }
            android.content.Context r5 = r12.getApplicationContext()     // Catch:{ Exception -> 0x0153, all -> 0x014e }
            r4.c(r5)     // Catch:{ Exception -> 0x0153, all -> 0x014e }
        L_0x008a:
            com.wacai.e r4 = com.wacai.e.c()     // Catch:{ Exception -> 0x0153, all -> 0x014e }
            android.database.sqlite.SQLiteDatabase r4 = r4.b()     // Catch:{ Exception -> 0x0153, all -> 0x014e }
            r5 = 0
            android.database.Cursor r2 = r4.rawQuery(r2, r5)     // Catch:{ Exception -> 0x0153, all -> 0x014e }
            if (r2 == 0) goto L_0x009f
            boolean r4 = r2.moveToFirst()     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            if (r4 != 0) goto L_0x00ad
        L_0x009f:
            if (r2 == 0) goto L_0x00a4
            r2.close()
        L_0x00a4:
            if (r3 != 0) goto L_0x0027
            com.wacai.e.e()
            goto L_0x0027
        L_0x00ab:
            r3 = r6
            goto L_0x007d
        L_0x00ad:
            java.lang.String r4 = "_maxymd"
            int r4 = r2.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            long r4 = r2.getLong(r4)     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            android.content.res.Resources r6 = r12.getResources()     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            int r7 = (r9 > r4 ? 1 : (r9 == r4 ? 0 : -1))
            if (r7 != 0) goto L_0x00da
            android.widget.RemoteViews r0 = com.wacai365.WidgetProvider.a     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            r1 = 2131493337(0x7f0c01d9, float:1.8610151E38)
            r4 = 2131296719(0x7f0901cf, float:1.8211363E38)
            java.lang.String r4 = r6.getString(r4)     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            r0.setTextViewText(r1, r4)     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
        L_0x00ce:
            if (r2 == 0) goto L_0x00d3
            r2.close()
        L_0x00d3:
            if (r3 != 0) goto L_0x0027
            com.wacai.e.e()
            goto L_0x0027
        L_0x00da:
            long r7 = r0 - r4
            int r7 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r7 != 0) goto L_0x011a
            android.widget.RemoteViews r0 = com.wacai365.WidgetProvider.a     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            r1 = 2131493337(0x7f0c01d9, float:1.8610151E38)
            r4 = 2131296718(0x7f0901ce, float:1.821136E38)
            java.lang.String r4 = r6.getString(r4)     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            r0.setTextViewText(r1, r4)     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            goto L_0x00ce
        L_0x00f0:
            r0 = move-exception
            r1 = r2
        L_0x00f2:
            java.lang.String r2 = "WidgetProvider"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0151 }
            r4.<init>()     // Catch:{ all -> 0x0151 }
            java.lang.String r5 = "OnReceive query DB expection "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0151 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0151 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0151 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0151 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0151 }
            if (r1 == 0) goto L_0x0113
            r1.close()
        L_0x0113:
            if (r3 != 0) goto L_0x0027
            com.wacai.e.e()
            goto L_0x0027
        L_0x011a:
            long r0 = com.wacai.a.a.b(r0)     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            long r4 = com.wacai.a.a.b(r4)     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            long r0 = r0 - r4
            r4 = 86400000(0x5265c00, double:4.2687272E-316)
            long r0 = r0 / r4
            android.widget.RemoteViews r4 = com.wacai365.WidgetProvider.a     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            r5 = 2131493337(0x7f0c01d9, float:1.8610151E38)
            r7 = 2131296720(0x7f0901d0, float:1.8211365E38)
            r8 = 1
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            r9 = 0
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            r8[r9] = r0     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            java.lang.String r0 = r6.getString(r7, r8)     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            r4.setTextViewText(r5, r0)     // Catch:{ Exception -> 0x00f0, all -> 0x0141 }
            goto L_0x00ce
        L_0x0141:
            r0 = move-exception
            r1 = r2
        L_0x0143:
            if (r1 == 0) goto L_0x0148
            r1.close()
        L_0x0148:
            if (r3 != 0) goto L_0x014d
            com.wacai.e.e()
        L_0x014d:
            throw r0
        L_0x014e:
            r0 = move-exception
            r1 = r8
            goto L_0x0143
        L_0x0151:
            r0 = move-exception
            goto L_0x0143
        L_0x0153:
            r0 = move-exception
            r1 = r8
            goto L_0x00f2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.WidgetProvider.onReceive(android.content.Context, android.content.Intent):void");
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        Log.i("WidgetProvider", "onUpdate");
        for (int updateAppWidget : iArr) {
            a = new RemoteViews(context.getPackageName(), (int) C0000R.layout.widgetmain);
            Intent a2 = InputTrade.a(context, "", 0);
            a2.putExtra("LaunchedByApplication", 0);
            a2.setFlags(268435456);
            a.setOnClickPendingIntent(C0000R.id.button1, PendingIntent.getActivity(context, 0, a2, 0));
            Intent intent = new Intent(context, MyShortcuts.class);
            intent.putExtra("LaunchedByApplication", 0);
            intent.setFlags(268435456);
            a.setOnClickPendingIntent(C0000R.id.button2, PendingIntent.getActivity(context, 0, intent, 0));
            Intent intent2 = new Intent(context, MySearch.class);
            intent2.putExtra("LaunchedByApplication", 0);
            intent2.setFlags(268435456);
            a.setOnClickPendingIntent(C0000R.id.button3, PendingIntent.getActivity(context, 0, intent2, 0));
            Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
            launchIntentForPackage.addFlags(268435456);
            a.setOnClickPendingIntent(C0000R.id.id_main, PendingIntent.getActivity(context, 0, launchIntentForPackage, 0));
            appWidgetManager.updateAppWidget(updateAppWidget, a);
        }
    }
}
