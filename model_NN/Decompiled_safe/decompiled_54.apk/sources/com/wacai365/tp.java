package com.wacai365;

import android.app.ProgressDialog;

public final class tp implements Runnable {
    public String a = "";
    public int b;
    public int c;
    private ProgressDialog d;

    public tp(ProgressDialog progressDialog) {
        this.d = progressDialog;
    }

    public final void run() {
        if (this.d != null) {
            if (this.a != null && this.a.length() > 0) {
                this.d.setMessage(this.a);
            }
            this.d.setMax(this.c);
            this.d.setProgress(this.b);
        }
    }
}
