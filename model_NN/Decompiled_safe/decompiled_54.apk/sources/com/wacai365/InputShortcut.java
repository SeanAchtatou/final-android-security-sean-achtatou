package com.wacai365;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import com.wacai.data.n;

public class InputShortcut extends WacaiActivity {
    protected int[] a = null;
    /* access modifiers changed from: private */
    public RadioButton b;
    /* access modifiers changed from: private */
    public RadioButton c;
    /* access modifiers changed from: private */
    public Button d;
    /* access modifiers changed from: private */
    public Button e;
    /* access modifiers changed from: private */
    public Button f;
    private TextView g;
    /* access modifiers changed from: private */
    public TextView h;
    private TextView i;
    private TextView j;
    /* access modifiers changed from: private */
    public TextView k;
    /* access modifiers changed from: private */
    public TextView l;
    private CheckBox m;
    private TextView n;
    /* access modifiers changed from: private */
    public n o;
    /* access modifiers changed from: private */
    public int[] p = null;
    private int[] q = null;
    /* access modifiers changed from: private */
    public int[] r = null;
    private CompoundButton.OnCheckedChangeListener s = new aad(this);
    private View.OnClickListener t = new aaf(this);

    /* access modifiers changed from: private */
    public void a() {
        if (this.o.b() == 0) {
            findViewById(C0000R.id.reimburseItem).setVisibility(0);
            m.a(this.o.d(), this.h);
            this.n.setText((int) C0000R.string.txtOutgoTarget);
        } else {
            findViewById(C0000R.id.reimburseItem).setVisibility(8);
            m.a("TBL_INCOMEMAINTYPEINFO", this.o.d(), this.h);
            this.n.setText((int) C0000R.string.txtIncomeTarget);
        }
        m.a("TBL_TRADETARGET", this.o.i(), this.j);
    }

    static /* synthetic */ boolean k(InputShortcut inputShortcut) {
        String a2 = inputShortcut.o.a();
        if (a2 == null || a2.length() == 0) {
            m.a(inputShortcut, (Animation) null, 0, (View) null, (int) C0000R.string.txtEmptyName);
            return false;
        }
        inputShortcut.o.f(false);
        inputShortcut.o.c();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.app.Activity, long, android.widget.TextView):void
     arg types: [com.wacai365.InputShortcut, long, android.widget.TextView]
     candidates:
      com.wacai365.m.a(java.lang.String, android.widget.TextView, android.widget.TextView):double
      com.wacai365.m.a(long, long, long):long
      com.wacai365.m.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String
      com.wacai365.m.a(android.app.Activity, long, boolean):void
      com.wacai365.m.a(android.app.Activity, java.util.ArrayList, long):void
      com.wacai365.m.a(android.content.Context, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, android.widget.TextView):void
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(java.lang.String, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean, boolean):boolean
      com.wacai365.m.a(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.app.Activity, long, android.widget.TextView):void */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (-1 == i3 && intent != null) {
            switch (i2) {
                case 6:
                    long longExtra = intent.getLongExtra("Outgo_Sel_Id", this.o.d());
                    if (longExtra > 0) {
                        this.o.a(longExtra);
                        m.a(longExtra, this.h);
                        return;
                    }
                    return;
                case 18:
                    String stringExtra = intent.getStringExtra("Text_String");
                    if (stringExtra == null) {
                        stringExtra = "";
                    }
                    String trim = stringExtra.trim();
                    this.o.a(trim);
                    this.g.setText(trim);
                    return;
                case 24:
                    long longExtra2 = intent.getLongExtra("Target_ID", 0);
                    this.o.e(longExtra2);
                    m.a("TBL_TRADETARGET", longExtra2, this.j);
                    return;
                case 37:
                    this.o.c(intent.getLongExtra("account_Sel_Id", this.o.g()));
                    m.a((Activity) this, this.o.g(), this.i);
                    return;
                default:
                    return;
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.input_shortcut);
        this.b = (RadioButton) findViewById(C0000R.id.btnIncome);
        this.c = (RadioButton) findViewById(C0000R.id.btnOutgo);
        this.d = (Button) findViewById(C0000R.id.btnSave);
        this.e = (Button) findViewById(C0000R.id.btnCancel);
        this.f = (Button) findViewById(C0000R.id.btnDelete);
        this.g = (TextView) findViewById(C0000R.id.viewName);
        this.h = (TextView) findViewById(C0000R.id.viewType);
        this.i = (TextView) findViewById(C0000R.id.viewAccount);
        this.j = (TextView) findViewById(C0000R.id.viewTarget);
        this.n = (TextView) findViewById(C0000R.id.target_name);
        this.k = (TextView) findViewById(C0000R.id.viewProject);
        this.l = (TextView) findViewById(C0000R.id.viewMember);
        this.m = (CheckBox) findViewById(C0000R.id.cbReimburse);
        this.m.setOnCheckedChangeListener(new zt(this));
        long longExtra = getIntent().getLongExtra("Extra_Id", -1);
        if (longExtra > 0) {
            setTitle((int) C0000R.string.txtEditShortcut);
            this.b.setEnabled(false);
            this.c.setEnabled(false);
            this.o = n.g(longExtra);
        } else {
            this.o = new n();
            this.f.setText("");
            this.f.setEnabled(false);
        }
        this.g.setText(this.o.a());
        m.a("TBL_ACCOUNTINFO", this.o.g(), this.i);
        m.a("TBL_PROJECTINFO", this.o.h(), this.k);
        m.a("TBL_MEMBERINFO", this.o.e(), this.l);
        m.a("TBL_TRADETARGET", this.o.i(), this.j);
        if (this.o.b() == 0) {
            m.a(this.o.d(), this.h);
            this.m.setChecked(this.o.j() == 1);
            this.c.setChecked(true);
            this.n.setText((int) C0000R.string.txtOutgoTarget);
        } else {
            m.a("TBL_INCOMEMAINTYPEINFO", this.o.d(), this.h);
            this.b.setChecked(true);
            this.n.setText((int) C0000R.string.txtIncomeTarget);
        }
        a();
        this.b.setOnCheckedChangeListener(this.s);
        this.c.setOnCheckedChangeListener(this.s);
        this.d.setOnClickListener(this.t);
        this.e.setOnClickListener(this.t);
        this.f.setOnClickListener(this.t);
        ((LinearLayout) findViewById(C0000R.id.nameItem)).setOnClickListener(new zv(this));
        ((LinearLayout) findViewById(C0000R.id.accountItem)).setOnClickListener(new zu(this));
        ((LinearLayout) findViewById(C0000R.id.typeItem)).setOnClickListener(new zw(this));
        ((LinearLayout) findViewById(C0000R.id.projectItem)).setOnClickListener(new zx(this));
        ((LinearLayout) findViewById(C0000R.id.memberItem)).setOnClickListener(new zy(this));
        ((LinearLayout) findViewById(C0000R.id.targetItem)).setOnClickListener(new aab(this));
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }
}
