package com.wacai365;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import com.wacai.a.e;
import com.wacai365.a.c;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class WeiboConnect extends WacaiActivity {
    ll a;
    c b;
    /* access modifiers changed from: private */
    public TextView c;
    /* access modifiers changed from: private */
    public CheckBox d;
    /* access modifiers changed from: private */
    public WebView e;
    /* access modifiers changed from: private */
    public String f = null;

    static /* synthetic */ void b(WeiboConnect weiboConnect, String str) {
        if (str != null) {
            try {
                weiboConnect.e.loadData(URLEncoder.encode(str, "utf-8"), "text/html", "utf-8");
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.weibo_connect);
        if (!e.a()) {
            m.a(this, (Animation) null, -1, (View) null, (int) C0000R.string.txtNoNetworkPrompt);
            finish();
            return;
        }
        this.a = aaj.a().a(getIntent().getLongExtra("Extra-webo-type", -1));
        ll llVar = (ll) this.a.clone();
        llVar.f = null;
        llVar.g = null;
        this.b = c.a(llVar);
        if (this.b == null) {
            finish();
        }
        this.c = (TextView) findViewById(C0000R.id.hint);
        this.c.setText(getResources().getString(C0000R.string.weiboPromptLoadingPage, this.a.c));
        this.d = (CheckBox) findViewById(C0000R.id.checkBox);
        this.d.setChecked(true);
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(new fn(this));
        new dr(this).start();
    }
}
