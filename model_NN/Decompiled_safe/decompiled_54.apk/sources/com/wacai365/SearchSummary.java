package com.wacai365;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.wacai.b;
import com.wacai.data.aa;
import com.wacai.e;

public class SearchSummary extends WacaiActivity {
    private ListView a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.wacai365.m.a(com.wacai.data.s, int):com.wacai.data.x
      com.wacai365.m.a(double, int):java.lang.String
      com.wacai365.m.a(long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, android.widget.EditText):void
      com.wacai365.m.a(android.content.Context, int):void
      com.wacai365.m.a(android.content.Context, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Intent, com.wacai.data.s):void
      com.wacai365.m.a(android.view.View, int):void
      com.wacai365.m.a(android.widget.Button, android.content.res.Resources):void
      com.wacai365.m.a(java.io.File, android.app.Activity):void
      com.wacai365.m.a(java.lang.String, android.widget.TextView):void
      com.wacai365.m.a(java.util.ArrayList, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean):boolean
      com.wacai365.m.a(android.graphics.Bitmap, android.content.Context):boolean
      com.wacai365.m.a(java.lang.String, int):java.lang.String[]
      com.wacai365.m.a(java.lang.String, boolean):java.lang.String */
    public void onCreate(Bundle bundle) {
        Cursor cursor;
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.money_summary);
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(new wj(this));
        this.a = (ListView) findViewById(C0000R.id.list);
        String stringExtra = getIntent().getStringExtra("Extra_Constraint");
        if (stringExtra != null) {
            String trim = stringExtra.trim();
            if (trim.length() > 0) {
                String trim2 = trim.trim();
                if (trim2.length() <= 0) {
                    cursor = null;
                } else {
                    String a2 = m.a(aa.e(trim2), false);
                    if (a2 == null || a2.length() <= 0) {
                        cursor = null;
                    } else {
                        cursor = e.c().b().rawQuery(String.format("select m.id as _id, m.name as _name, m.flag as _flag, ifnull((select sum(a.money) from tbl_outgoinfo a join TBL_OUTGOSUBTYPEINFO c on a.subtypeid=c.id join TBL_PROJECTINFO d on d.id = a.projectid join TBL_ACCOUNTINFO e on e.id = a.accountid join TBL_OUTGOMAINTYPEINFO g on g.id = c.id / 10000 LEFT JOIN TBL_TRADETARGET h ON a.targetid = h.id where a.isdelete = 0 and a.scheduleoutgoid = 0 and ( (a.comment like '%s' ESCAPE '|' or c.name like '%s' ESCAPE '|' or d.name like '%s' ESCAPE '|' or e.name like '%s' ESCAPE '|' or g.name like '%s' ESCAPE '|' or h.name like '%s' ESCAPE '|') or exists (select * from tbl_outgomemberinfo b join TBL_MEMBERINFO f on f.id = b.memberid where a.id=b.outgoid and f.name like '%s' ESCAPE '|' )) and e.moneytype = m.id), 0) _osum, ifnull((select sum(a.money) from TBL_INCOMEINFO a join TBL_INCOMEMAINTYPEINFO c on c.id = a.typeid join TBL_PROJECTINFO d on d.id = a.projectid join TBL_ACCOUNTINFO e on e.id = a.accountid LEFT JOIN TBL_TRADETARGET h ON a.targetid = h.id where a.isdelete = 0 and a.scheduleincomeid = 0 and ( (a.comment like '%s' ESCAPE '|' or c.name like '%s' ESCAPE '|' or d.name like '%s' ESCAPE '|' or e.name like '%s' ESCAPE '|' or h.name like '%s' ESCAPE '|') or exists (select * from TBL_INCOMEMEMBERINFO b join TBL_MEMBERINFO f on f.id = b.memberid where b.incomeid = a.id and f.name like '%s' ESCAPE '|' ))  and e.moneytype = m.id), 0) _isum, ifnull((select sum(a.transferoutmoney) from TBL_TRANSFERINFO a join TBL_ACCOUNTINFO b on a.transferoutaccountid = b.id join TBL_ACCOUNTINFO c on a.transferinaccountid = c.id where a.isdelete = 0 and a.type = 0 and b.moneytype = m.id and (a.comment like '%s' ESCAPE '|' or b.name like '%s' ESCAPE '|' or c.name like '%s' ESCAPE '|')), 0) _tosum, ifnull((select sum(a.transferinmoney) from TBL_TRANSFERINFO a join TBL_ACCOUNTINFO b on a.transferoutaccountid = b.id join TBL_ACCOUNTINFO c on a.transferinaccountid = c.id where a.isdelete = 0 and a.type = 0 and c.moneytype = m.id and (a.comment like '%s' ESCAPE '|' or b.name like '%s' ESCAPE '|' or c.name like '%s' ESCAPE '|')), 0) _tisum from TBL_MONEYTYPE m where (m.id = %d or _osum <> 0 or _isum <> 0 or _tosum <> 0 or _tisum <> 0) order by m.id", a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, a2, Long.valueOf(b.m().j())), null);
                    }
                }
                if (cursor != null) {
                    startManagingCursor(cursor);
                    this.a.setAdapter((ListAdapter) new ee(this, this, C0000R.layout.list_item_summary, cursor, new String[]{"_name", "_osum", "_isum", "_tosum", "_tisum"}, new int[]{C0000R.id.itemTitel, C0000R.id.itemOutgo, C0000R.id.itemIncome, C0000R.id.itemTranOut, C0000R.id.itemTranIn}));
                }
            }
        }
    }
}
