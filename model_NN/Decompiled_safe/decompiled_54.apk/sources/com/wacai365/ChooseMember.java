package com.wacai365;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.wacai.e;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;

public class ChooseMember extends WacaiActivity {
    int a;
    long b;
    /* access modifiers changed from: private */
    public ListView c;
    private zi d;
    /* access modifiers changed from: private */
    public ArrayList e = new ArrayList();

    static /* synthetic */ double a(ChooseMember chooseMember, int i) {
        long j;
        long j2 = chooseMember.b;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            j = j2;
            if (i3 >= chooseMember.e.size()) {
                break;
            }
            j2 = i != i3 ? j - ((uz) chooseMember.e.get(i3)).b : j;
            i2 = i3 + 1;
        }
        return m.a(j < 0 ? 0 : j);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.choose_member_list);
        Hashtable hashtable = new Hashtable((HashMap) getIntent().getSerializableExtra("Member_ShareInfo"));
        this.b = getIntent().getLongExtra("Member_Total_Money", 0);
        StringBuffer stringBuffer = new StringBuffer(128);
        Enumeration keys = hashtable.keys();
        stringBuffer.append("select id as _id, name as _name from tbl_memberinfo where ");
        if (keys.hasMoreElements()) {
            stringBuffer.append(" id in (");
            StringBuffer stringBuffer2 = new StringBuffer(32);
            while (keys.hasMoreElements()) {
                if (stringBuffer2.length() > 0) {
                    stringBuffer2.append(",");
                }
                stringBuffer2.append((String) keys.nextElement());
            }
            stringBuffer.append(stringBuffer2);
            stringBuffer.append(") ");
        }
        stringBuffer.append("order by orderno");
        Cursor rawQuery = e.c().b().rawQuery(stringBuffer.toString(), null);
        if (rawQuery != null) {
            while (rawQuery.moveToNext()) {
                uz uzVar = new uz(this);
                uzVar.c = rawQuery.getString(rawQuery.getColumnIndex("_name"));
                uzVar.a = rawQuery.getInt(rawQuery.getColumnIndex("_id"));
                uzVar.b = Long.parseLong((String) hashtable.get(String.format("%d", Integer.valueOf(uzVar.a))));
                this.e.add(uzVar);
            }
            rawQuery.close();
        }
        this.d = new zi(this, this.e, getLayoutInflater());
        this.c = (ListView) findViewById(C0000R.id.MemberList);
        this.c.setAdapter((ListAdapter) this.d);
        this.c.setOnItemClickListener(new gq(this));
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(new gr(this));
        ((Button) findViewById(C0000R.id.btnOK)).setOnClickListener(new gt(this));
        ((Button) findViewById(C0000R.id.btnAASet)).setOnClickListener(new gs(this));
    }
}
