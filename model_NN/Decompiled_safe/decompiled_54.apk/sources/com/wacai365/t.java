package com.wacai365;

import android.view.ContextMenu;
import android.view.View;

final class t implements View.OnCreateContextMenuListener {
    private /* synthetic */ SettingBudgetMgr a;

    t(SettingBudgetMgr settingBudgetMgr) {
        this.a = settingBudgetMgr;
    }

    public final void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.clear();
        this.a.getMenuInflater().inflate(C0000R.menu.delete_list_context, contextMenu);
    }
}
