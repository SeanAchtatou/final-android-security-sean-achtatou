package com.wacai365;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Process;
import android.util.Log;
import com.wacai.e;

public class MyApp extends Application {
    public static boolean a = false;
    public static Context b = null;
    private static boolean c = false;

    public static void a() {
        Log.i("MyApp", "Application exited!");
        c = false;
        e.e();
        Process.killProcess(Process.myPid());
    }

    static void a(Activity activity) {
        Intent launchIntentForPackage = activity.getBaseContext().getPackageManager().getLaunchIntentForPackage(activity.getBaseContext().getPackageName());
        launchIntentForPackage.addFlags(335544320);
        activity.startActivity(launchIntentForPackage);
    }

    public static void a(Context context) {
        if (!c) {
            b = context;
            e c2 = e.c();
            c2.a(new l());
            if (c2.c(b)) {
                Log.i("MyApp", "Application inited!");
                c = true;
                return;
            }
            Log.e("MyApp", "Application init failed!");
        }
    }

    public void onCreate() {
        b = getApplicationContext();
        e.c().a(b);
        Log.i("MyApp", "Application onCreate!");
        a(b);
        super.onCreate();
    }

    public void onTerminate() {
        Log.i("MyApp", "Application terminated!");
        super.onTerminate();
    }
}
