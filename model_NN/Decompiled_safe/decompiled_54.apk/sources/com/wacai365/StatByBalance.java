package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.f;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.n;
import com.wacai.b.o;
import com.wacai.data.h;
import com.wacai.e;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

public class StatByBalance extends WacaiActivity implements tt {
    DialogInterface.OnClickListener a = new hn(this);
    private QueryInfo b;
    /* access modifiers changed from: private */
    public ListView c = null;
    private RadioGroup d;
    /* access modifiers changed from: private */
    public String[] e = null;
    /* access modifiers changed from: private */
    public String[] f = null;
    private h g;
    /* access modifiers changed from: private */
    public Hashtable h;
    /* access modifiers changed from: private */
    public Boolean i = false;
    /* access modifiers changed from: private */
    public ArrayList j = new ArrayList();
    private wb k = new wb(this);
    /* access modifiers changed from: private */
    public fy l = null;
    private RadioGroup.OnCheckedChangeListener m = new hr(this);
    private View.OnClickListener n = new hp(this);

    /* access modifiers changed from: private */
    public void c() {
        setContentView((int) C0000R.layout.stat_table_balance);
        this.d = (RadioGroup) findViewById(C0000R.id.rgQueryChoice);
        this.d.setOnCheckedChangeListener(this.m);
        ((Button) findViewById(C0000R.id.btnCollect)).setOnClickListener(this.n);
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(this.n);
        this.c = (ListView) findViewById(C0000R.id.QueryList);
        this.c.setOnItemClickListener(new hm(this));
        this.c.setAdapter((ListAdapter) new tj(this, this, C0000R.layout.list_item_with_btn));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(com.wacai.b.c, boolean):void
     arg types: [com.wacai.b.c, int]
     candidates:
      com.wacai365.ft.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(com.wacai.b.c, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void c(StatByBalance statByBalance) {
        statByBalance.j.clear();
        ft ftVar = new ft(statByBalance);
        statByBalance.l = ftVar;
        ftVar.e(false);
        ftVar.a(new hu(statByBalance));
        c cVar = new c(ftVar);
        ft.a(cVar, true);
        l lVar = new l();
        StringBuffer stringBuffer = new StringBuffer(100);
        statByBalance.b.a(stringBuffer, 6);
        lVar.a(m.b(stringBuffer.toString()));
        cVar.a((o) lVar);
        f fVar = new f();
        fVar.a(true);
        fVar.a(statByBalance.j);
        cVar.a((o) fVar);
        ftVar.a(cVar);
        ftVar.b(C0000R.string.txtRectificationProgress, C0000R.string.txtPreparing);
        ftVar.a(false, true);
    }

    /* access modifiers changed from: private */
    public void d() {
        Cursor cursor;
        long time = new Date().getTime() / 1000;
        long j2 = b.m().j();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(String.format("select _id, _name, _hasbalance, _flag, _moneytypeid, (_balance - _osum + _isum - _tosum + _tisum) as _sum from (select a.id as _id, a.name as _name, a.balance as _balance,  a.hasbalance as _hasbalance, b.flag as _flag, a.moneytype as _moneytypeid,  (select ifnull(sum(t1.money), 0) from TBL_OUTGOINFO t1 where t1.isdelete=0 and t1.outgodate<=%d and  t1.outgodate >= a.balancedate and t1.accountid=a.id) _osum,  (select ifnull(sum(t2.money), 0) from TBL_INCOMEINFO t2 where t2.isdelete=0 and t2.incomedate<=%d and  t2.incomedate >= a.balancedate and t2.accountid=a.id) _isum,  (select ifnull(sum(t3.transferoutmoney), 0) from TBL_TRANSFERINFO t3 where t3.isdelete=0 and  t3.date<=%d and t3.date >= a.balancedate and t3.transferoutaccountid=a.id) _tosum,  (select ifnull(sum(t4.transferinmoney), 0) from TBL_TRANSFERINFO t4 where t4.isdelete=0 and  t4.date<=%d and t4.date >= a.balancedate and t4.transferinaccountid=a.id) _tisum  from TBL_ACCOUNTINFO a join TBL_MONEYTYPE b on a.moneytype = b.id where a.enable=1 and a.type<>3  order by a.type asc, a.pinyin asc ) order by _hasbalance desc, (CASE WHEN _moneytypeid = %d then %d END) desc, _moneytypeid ASC", Long.valueOf(time), Long.valueOf(time), Long.valueOf(time), Long.valueOf(time), Long.valueOf(j2), Long.valueOf(j2)));
        this.j.clear();
        try {
            Cursor rawQuery = e.c().b().rawQuery(stringBuffer.toString(), null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        do {
                            Hashtable hashtable = new Hashtable();
                            hashtable.put("TAG_LABLE", rawQuery.getString(rawQuery.getColumnIndexOrThrow("_name")));
                            hashtable.put("TAG_FIRST", m.b(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_sum"))));
                            hashtable.put("flag", rawQuery.getString(rawQuery.getColumnIndexOrThrow("_flag")));
                            hashtable.put("id", rawQuery.getString(rawQuery.getColumnIndexOrThrow("_moneytypeid")));
                            hashtable.put("itemid", rawQuery.getString(rawQuery.getColumnIndexOrThrow("_id")));
                            hashtable.put("TAG_INITED", rawQuery.getString(rawQuery.getColumnIndexOrThrow("_hasbalance")));
                            this.j.add(hashtable);
                        } while (rawQuery.moveToNext());
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        runOnUiThread(this.k);
                        return;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
                return;
            }
            return;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void f(StatByBalance statByBalance) {
        statByBalance.j.clear();
        statByBalance.runOnUiThread(statByBalance.k);
        ft ftVar = new ft(statByBalance);
        ftVar.e(false);
        ftVar.a((tt) statByBalance);
        ftVar.b(C0000R.string.networkProgress, C0000R.string.txtTransformData);
        statByBalance.l = ftVar;
        c cVar = new c(ftVar);
        is.c(cVar, false);
        l lVar = new l();
        StringBuffer stringBuffer = new StringBuffer(100);
        statByBalance.b.a(stringBuffer, 6);
        lVar.a(m.b(stringBuffer.toString()));
        cVar.a((o) lVar);
        f fVar = new f();
        fVar.a(true);
        fVar.a(statByBalance.j);
        cVar.a((o) fVar);
        ftVar.a(cVar);
        ftVar.a(false, true);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        int i2;
        String str;
        String str2;
        int i3;
        long j2;
        Hashtable hashtable = new Hashtable();
        int size = this.j.size();
        int i4 = 0;
        int i5 = 0;
        long j3 = 0;
        while (i4 < size) {
            Hashtable hashtable2 = (Hashtable) this.j.get(i4);
            if (hashtable2 != null) {
                String str3 = (String) hashtable2.get("id");
                long parseLong = str3 == null ? 0 : Long.parseLong(str3);
                String str4 = (String) hashtable2.get("TAG_FIRST");
                String str5 = (String) hashtable2.get("TAG_INITED");
                String str6 = (String) hashtable.get(str3);
                if (str5 != null && str5.compareToIgnoreCase("1") == 0 && str4 != null && str4.length() > 0) {
                    i3 = str6 == null ? i5 + 1 : i5;
                    hashtable.put(str3, m.b(m.a(Double.parseDouble(str4) + ((str6 == null || str6.length() <= 0) ? 0.0d : Double.parseDouble(str6)))));
                    j2 = Math.max(j3, parseLong);
                    i4++;
                    i5 = i3;
                    j3 = j2;
                }
            }
            i3 = i5;
            j2 = j3;
            i4++;
            i5 = i3;
            j3 = j2;
        }
        this.e = new String[i5];
        this.f = new String[i5];
        int i6 = (int) (1 + j3);
        int i7 = 0;
        int i8 = 0;
        while (i7 < i6) {
            String str7 = (String) hashtable.get(String.format("%d", Integer.valueOf(i7)));
            if (str7 == null || str7.length() <= 0) {
                i2 = i8;
            } else {
                Cursor rawQuery = e.c().b().rawQuery(String.format("select * from tbl_moneytype where id = %d", Integer.valueOf(i7)), null);
                if (rawQuery != null) {
                    rawQuery.moveToFirst();
                    if (rawQuery.getCount() > 0) {
                        str2 = rawQuery.getString(rawQuery.getColumnIndexOrThrow("name"));
                        str = rawQuery.getString(rawQuery.getColumnIndexOrThrow("flag"));
                    } else {
                        str = "";
                        str2 = "";
                    }
                    rawQuery.close();
                } else {
                    str = "";
                    str2 = "";
                }
                this.e[i8] = str2;
                String[] strArr = this.f;
                if (b.a) {
                    str7 = str + str7;
                }
                strArr[i8] = str7;
                i2 = i8 + 1;
            }
            i7++;
            i8 = i2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x00c1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(int r13) {
        /*
            r12 = this;
            r11 = 0
            r1 = 0
            java.util.ArrayList r0 = r12.j
            int r0 = r0.size()
            long r2 = (long) r0
            r4 = r1
        L_0x000a:
            long r0 = (long) r4
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x00c5
            java.util.ArrayList r0 = r12.j
            java.lang.Object r0 = r0.get(r4)
            java.util.Hashtable r0 = (java.util.Hashtable) r0
            if (r0 == 0) goto L_0x009b
            java.lang.String r1 = "TAG_LABLE"
            java.lang.Object r1 = r0.get(r1)     // Catch:{ Exception -> 0x00a0, all -> 0x00bd }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x00a0, all -> 0x00bd }
            java.lang.String r5 = "select a.id as _aid, b.flag as _flag, b.id as _moneytypeid from tbl_accountinfo a, tbl_moneytype b where a.moneytype = b.id and a.name = '%s'"
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x00a0, all -> 0x00bd }
            r7 = 0
            java.lang.String r1 = com.wacai.data.aa.e(r1)     // Catch:{ Exception -> 0x00a0, all -> 0x00bd }
            r6[r7] = r1     // Catch:{ Exception -> 0x00a0, all -> 0x00bd }
            java.lang.String r1 = java.lang.String.format(r5, r6)     // Catch:{ Exception -> 0x00a0, all -> 0x00bd }
            com.wacai.e r5 = com.wacai.e.c()     // Catch:{ Exception -> 0x00a0, all -> 0x00bd }
            android.database.sqlite.SQLiteDatabase r5 = r5.b()     // Catch:{ Exception -> 0x00a0, all -> 0x00bd }
            r6 = 0
            android.database.Cursor r1 = r5.rawQuery(r1, r6)     // Catch:{ Exception -> 0x00a0, all -> 0x00bd }
            if (r1 == 0) goto L_0x0096
            int r5 = r1.getCount()     // Catch:{ Exception -> 0x00d1 }
            if (r5 <= 0) goto L_0x0096
            r1.moveToFirst()     // Catch:{ Exception -> 0x00d1 }
            java.lang.String r5 = "itemid"
            java.lang.String r6 = "%d"
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x00d1 }
            r8 = 0
            java.lang.String r9 = "_aid"
            int r9 = r1.getColumnIndexOrThrow(r9)     // Catch:{ Exception -> 0x00d1 }
            long r9 = r1.getLong(r9)     // Catch:{ Exception -> 0x00d1 }
            java.lang.Long r9 = java.lang.Long.valueOf(r9)     // Catch:{ Exception -> 0x00d1 }
            r7[r8] = r9     // Catch:{ Exception -> 0x00d1 }
            java.lang.String r6 = java.lang.String.format(r6, r7)     // Catch:{ Exception -> 0x00d1 }
            r0.put(r5, r6)     // Catch:{ Exception -> 0x00d1 }
            java.lang.String r5 = "flag"
            java.lang.String r6 = "_flag"
            int r6 = r1.getColumnIndexOrThrow(r6)     // Catch:{ Exception -> 0x00d1 }
            java.lang.String r6 = r1.getString(r6)     // Catch:{ Exception -> 0x00d1 }
            r0.put(r5, r6)     // Catch:{ Exception -> 0x00d1 }
            java.lang.String r5 = "id"
            java.lang.String r6 = "%d"
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x00d1 }
            r8 = 0
            java.lang.String r9 = "_moneytypeid"
            int r9 = r1.getColumnIndexOrThrow(r9)     // Catch:{ Exception -> 0x00d1 }
            long r9 = r1.getLong(r9)     // Catch:{ Exception -> 0x00d1 }
            java.lang.Long r9 = java.lang.Long.valueOf(r9)     // Catch:{ Exception -> 0x00d1 }
            r7[r8] = r9     // Catch:{ Exception -> 0x00d1 }
            java.lang.String r6 = java.lang.String.format(r6, r7)     // Catch:{ Exception -> 0x00d1 }
            r0.put(r5, r6)     // Catch:{ Exception -> 0x00d1 }
        L_0x0096:
            if (r1 == 0) goto L_0x009b
            r1.close()
        L_0x009b:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x000a
        L_0x00a0:
            r1 = move-exception
            r1 = r11
        L_0x00a2:
            java.lang.String r5 = "itemid"
            java.lang.String r6 = "0"
            r0.put(r5, r6)     // Catch:{ all -> 0x00cf }
            java.lang.String r5 = "flag"
            java.lang.String r6 = ""
            r0.put(r5, r6)     // Catch:{ all -> 0x00cf }
            java.lang.String r5 = "id"
            java.lang.String r6 = "0"
            r0.put(r5, r6)     // Catch:{ all -> 0x00cf }
            if (r1 == 0) goto L_0x009b
            r1.close()
            goto L_0x009b
        L_0x00bd:
            r0 = move-exception
            r1 = r11
        L_0x00bf:
            if (r1 == 0) goto L_0x00c4
            r1.close()
        L_0x00c4:
            throw r0
        L_0x00c5:
            android.widget.ListView r0 = r12.c
            if (r0 == 0) goto L_0x00ce
            com.wacai365.wb r0 = r12.k
            r12.runOnUiThread(r0)
        L_0x00ce:
            return
        L_0x00cf:
            r0 = move-exception
            goto L_0x00bf
        L_0x00d1:
            r5 = move-exception
            goto L_0x00a2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.StatByBalance.a(int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(com.wacai.b.c, boolean):void
     arg types: [com.wacai.b.c, int]
     candidates:
      com.wacai365.ft.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(com.wacai.b.c, boolean):void */
    public final void b() {
        Cursor cursor;
        int size = this.j.size();
        this.g = h.d(Long.parseLong((String) this.h.get("itemid")));
        try {
            Cursor rawQuery = e.c().b().rawQuery("select * from TBL_ACCOUNTINFO where uuid = '" + this.g.B() + "'", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        this.g.b(rawQuery.getString(rawQuery.getColumnIndexOrThrow("name")));
                        this.g.a(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("type")));
                        this.g.b(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("moneytype")));
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            String j2 = this.g.j();
            for (int i2 = 0; i2 < size; i2++) {
                Hashtable hashtable = (Hashtable) this.j.get(i2);
                if (hashtable != null) {
                    if (j2.equals((String) hashtable.get("TAG_LABLE"))) {
                        String str = (String) hashtable.get("TAG_FIRST");
                        double longValue = (double) Long.valueOf(str).longValue();
                        if (Double.parseDouble((String) this.h.get("TAG_FIRST")) == longValue) {
                            runOnUiThread(new hw(this));
                            return;
                        }
                        this.g.a(m.a(longValue));
                        long time = new Date().getTime();
                        this.g.b(time / 1000);
                        this.g.c();
                        runOnUiThread(new hv(this, str, time));
                        ft ftVar = new ft(this);
                        ftVar.a((tt) this);
                        this.l = ftVar;
                        c cVar = new c(ftVar);
                        ftVar.e(false);
                        l lVar = new l();
                        StringBuffer stringBuffer = new StringBuffer();
                        this.g.a(stringBuffer);
                        lVar.a(m.a(stringBuffer.toString()));
                        cVar.a((o) lVar);
                        cVar.a((o) new n());
                        ft.a(cVar, false);
                        cVar.b();
                        d();
                        return;
                    } else if (this.c != null && i2 == size - 1) {
                        runOnUiThread(new ht(this));
                    }
                }
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1) {
            if (i2 == 14) {
                d();
            } else if (this.l != null) {
                this.l.a(i2, i3, intent);
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = new QueryInfo();
        this.b.a(4);
        c();
        d();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.i.booleanValue()) {
            d();
        }
    }
}
