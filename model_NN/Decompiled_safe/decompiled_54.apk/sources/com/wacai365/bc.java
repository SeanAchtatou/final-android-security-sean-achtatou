package com.wacai365;

import android.content.Intent;
import android.view.View;

final class bc implements View.OnClickListener {
    private /* synthetic */ MySMS a;

    bc(MySMS mySMS) {
        this.a = mySMS;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.a)) {
            Intent intent = new Intent(this.a, SettingSMSMgr.class);
            intent.putExtra("LaunchedByApplication", 1);
            this.a.startActivityForResult(intent, 0);
        } else if (view.equals(this.a.b)) {
            this.a.finish();
        }
    }
}
