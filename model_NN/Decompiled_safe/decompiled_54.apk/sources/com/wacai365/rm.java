package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;

final class rm implements DialogInterface.OnClickListener {
    private /* synthetic */ InputNote a;

    rm(InputNote inputNote) {
        this.a = inputNote;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        InputNote inputNote = this.a;
        switch (i) {
            case -3:
                Intent intent = new Intent(inputNote, DataBackupSetting.class);
                intent.putExtra("LaunchedByApplication", 1);
                inputNote.startActivityForResult(intent, 0);
                break;
            case -1:
                fy unused = this.a.g = abt.a(inputNote);
                break;
        }
        this.a.f.e();
    }
}
