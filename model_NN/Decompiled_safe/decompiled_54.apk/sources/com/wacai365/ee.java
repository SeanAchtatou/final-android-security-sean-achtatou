package com.wacai365;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.wacai.b;

public final class ee extends SimpleCursorAdapter {
    private Resources a;
    private /* synthetic */ SearchSummary b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ee(SearchSummary searchSummary, Context context, int i, Cursor cursor, String[] strArr, int[] iArr) {
        super(context, i, cursor, strArr, iArr);
        this.b = searchSummary;
        this.a = context.getResources();
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        String string = b.a ? cursor.getString(cursor.getColumnIndexOrThrow("_flag")) : "";
        ((TextView) view.findViewById(C0000R.id.itemTitel)).setText(cursor.getString(cursor.getColumnIndexOrThrow("_name")));
        ((TextView) view.findViewById(C0000R.id.itemOutgo)).setText(this.a.getString(C0000R.string.txtOutgoCountString, string, m.a(m.a(cursor.getLong(cursor.getColumnIndexOrThrow("_osum"))), 2)));
        ((TextView) view.findViewById(C0000R.id.itemIncome)).setText(this.a.getString(C0000R.string.txtIncomeCountString, string, m.a(m.a(cursor.getLong(cursor.getColumnIndexOrThrow("_isum"))), 2)));
        ((TextView) view.findViewById(C0000R.id.itemTranOut)).setText(this.a.getString(C0000R.string.txtTransOutCountString, string, m.a(m.a(cursor.getLong(cursor.getColumnIndexOrThrow("_tosum"))), 2)));
        ((TextView) view.findViewById(C0000R.id.itemTranIn)).setText(this.a.getString(C0000R.string.txtTransInCountString, string, m.a(m.a(cursor.getLong(cursor.getColumnIndexOrThrow("_tisum"))), 2)));
    }
}
