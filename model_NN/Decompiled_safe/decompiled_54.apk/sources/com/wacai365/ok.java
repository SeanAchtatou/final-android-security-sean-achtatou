package com.wacai365;

import android.content.Intent;
import android.view.View;

final class ok implements View.OnClickListener {
    private /* synthetic */ StatView a;

    ok(StatView statView) {
        this.a = statView;
    }

    public final void onClick(View view) {
        boolean z;
        ll[] b = aaj.a().b();
        int length = b.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (b[i].a()) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            this.a.startActivityForResult(new Intent(this.a, WeiboSettingList.class), 0);
            return;
        }
        this.a.g();
    }
}
