package com.wacai365;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.wacai.a;

public class DataBackupSetting extends WacaiActivity {
    /* access modifiers changed from: private */
    public LinearLayout a;
    /* access modifiers changed from: private */
    public TextView b;
    /* access modifiers changed from: private */
    public TextView c;
    /* access modifiers changed from: private */
    public RadioGroup d;
    private RadioButton e;
    /* access modifiers changed from: private */
    public RadioButton f;
    private RadioButton g;
    private RadioButton h;
    private RadioButton i;
    private RadioButton j;
    private RadioButton k;
    /* access modifiers changed from: private */
    public CheckBox l;
    private Button m;
    private Button n;
    /* access modifiers changed from: private */
    public int[] o = {0, 1, 2, 3, -1};
    /* access modifiers changed from: private */
    public int p = 0;

    static /* synthetic */ void h(DataBackupSetting dataBackupSetting) {
        a.b("IsAutoBackup", dataBackupSetting.l.isChecked() ? 1 : 0);
        a.b("DataBackupIntervalType", (long) dataBackupSetting.p);
        if (dataBackupSetting.f.isChecked()) {
            a.b("AutoBackupCondition", 1);
        } else if (dataBackupSetting.e.isChecked()) {
            a.b("AutoBackupCondition", 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1) {
            this.l.setChecked(true);
            this.d.setVisibility(0);
            this.a.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.data_backup_setting);
        this.p = (int) a.a("DataBackupIntervalType", 1);
        this.a = (LinearLayout) findViewById(C0000R.id.backupTypeLayout);
        this.b = (TextView) findViewById(C0000R.id.backupPrompt);
        this.c = (TextView) findViewById(C0000R.id.backupSettingTitle);
        this.d = (RadioGroup) findViewById(C0000R.id.rgAutoBackupGroup);
        this.e = (RadioButton) findViewById(C0000R.id.rbAutoBackupByWifi);
        this.f = (RadioButton) findViewById(C0000R.id.rbAutoBackupAllTime);
        this.g = (RadioButton) findViewById(C0000R.id.rb1DayTitle);
        this.h = (RadioButton) findViewById(C0000R.id.rb3DayTitle);
        this.i = (RadioButton) findViewById(C0000R.id.rb7DayTitle);
        this.j = (RadioButton) findViewById(C0000R.id.rb30DayTitle);
        this.k = (RadioButton) findViewById(C0000R.id.rbNeverTitle);
        this.l = (CheckBox) findViewById(C0000R.id.cbSyncSetting);
        this.m = (Button) findViewById(C0000R.id.btnCancel);
        this.n = (Button) findViewById(C0000R.id.btnSave);
        RadioButton[] radioButtonArr = {this.g, this.h, this.i, this.j, this.k};
        for (int i2 = 0; i2 < this.o.length; i2++) {
            radioButtonArr[i2].setOnCheckedChangeListener(new kz(this, radioButtonArr));
            if (this.p == this.o[i2]) {
                radioButtonArr[i2].setChecked(true);
            }
        }
        if (a.a("AutoBackupCondition", 0) > 0) {
            this.f.setChecked(true);
        } else {
            this.e.setChecked(true);
        }
        this.l.setOnCheckedChangeListener(new lf(this));
        this.l.setChecked(a.a("IsAutoBackup", 0) > 0);
        if (a.a("IsAutoBackup", 0) > 0) {
            this.d.setVisibility(0);
            this.a.setVisibility(8);
        } else {
            this.d.setVisibility(8);
            this.a.setVisibility(0);
        }
        this.m.setOnClickListener(new ld(this));
        this.n = (Button) findViewById(C0000R.id.btnSave);
        this.n.setOnClickListener(new kx(this));
    }
}
