package com.wacai365;

import android.view.ContextMenu;
import android.view.View;

final class om implements View.OnCreateContextMenuListener {
    private /* synthetic */ SettingNews a;

    om(SettingNews settingNews) {
        this.a = settingNews;
    }

    public final void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.clear();
        this.a.getMenuInflater().inflate(C0000R.menu.delete_list_context, contextMenu);
    }
}
