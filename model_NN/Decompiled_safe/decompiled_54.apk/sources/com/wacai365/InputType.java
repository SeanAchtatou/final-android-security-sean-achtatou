package com.wacai365;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.animation.Animation;
import com.wacai.data.y;

public class InputType extends InputBasicItem {
    private y e;
    private long f;

    /* access modifiers changed from: protected */
    public final void a() {
        y yVar = new y();
        this.e = yVar;
        a(yVar);
        this.e.a(this.f);
        this.c.setText("");
        m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtContinuePrompt);
    }

    /* access modifiers changed from: protected */
    public final InputFilter[] b() {
        return new InputFilter[]{new InputFilter.LengthFilter(20)};
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.input_name);
        long longExtra = getIntent().getLongExtra("Record_Id", -1);
        this.f = getIntent().getLongExtra("Parent_Record_Id", -1);
        if (-1 == this.f) {
            finish();
            return;
        }
        if (longExtra > 0) {
            setTitle((int) C0000R.string.txtEditSubType);
            this.e = y.b(longExtra);
        } else {
            this.e = new y();
        }
        this.e.a(this.f);
        a(this.e);
        c();
    }
}
