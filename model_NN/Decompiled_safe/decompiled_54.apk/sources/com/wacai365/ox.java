package com.wacai365;

import android.view.View;
import android.widget.CheckBox;
import com.wacai.e;
import java.util.Map;

final class ox implements View.OnClickListener {
    private /* synthetic */ xa a;

    ox(xa xaVar) {
        this.a = xaVar;
    }

    public final void onClick(View view) {
        CheckBox checkBox = (CheckBox) view;
        Map map = (Map) view.getTag();
        map.put("star", checkBox.isChecked() ? "1" : "0");
        Object[] objArr = new Object[2];
        objArr[0] = Integer.valueOf(checkBox.isChecked() ? 1 : 0);
        objArr[1] = map.get("ID");
        e.c().b().execSQL(String.format("UPDATE TBL_OUTGOSUBTYPEINFO SET star = %d WHERE id = %s", objArr));
    }
}
