package com.wacai365;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import java.util.Date;

public final class hq extends SimpleCursorAdapter {
    private Resources a;
    private int[] b;
    private boolean c;

    public hq(Context context, int i, Cursor cursor, String[] strArr, int[] iArr, boolean z) {
        super(context, i, cursor, strArr, iArr);
        this.b = iArr;
        this.a = context.getResources();
        this.c = z;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        String string;
        long j = cursor.getLong(cursor.getColumnIndexOrThrow("_flag"));
        View findViewById = view.findViewById(C0000R.id.id_money);
        if (findViewById != null) {
            String format = String.format("%.2f", Double.valueOf(m.a(cursor.getLong(cursor.getColumnIndexOrThrow("_money")))));
            TextView textView = (TextView) findViewById;
            switch ((int) j) {
                case 0:
                    textView.setTextColor(this.a.getColor(C0000R.color.outgo_money));
                    break;
                case 1:
                    textView.setTextColor(this.a.getColor(C0000R.color.income_money));
                    break;
                case 2:
                case 3:
                case 4:
                    textView.setTextColor(this.a.getColor(C0000R.color.transfer_money));
                    break;
            }
            setViewText((TextView) findViewById, format);
        }
        View findViewById2 = view.findViewById(C0000R.id.id_desc);
        if (findViewById2 != null) {
            if (cursor.getLong(cursor.getColumnIndex("_scheduleid")) == 0 && ((long) cursor.getInt(cursor.getColumnIndex("_reimburse"))) == 1) {
                ((TextView) findViewById2).setTextColor(this.a.getColor(C0000R.color.outgo_money));
            } else {
                ((TextView) findViewById2).setTextColor(this.a.getColor(C0000R.color.text_color_black));
            }
            if (2 != j) {
                string = cursor.getString(cursor.getColumnIndexOrThrow("_name"));
                if (string == null) {
                    string = "";
                }
                String string2 = cursor.getString(cursor.getColumnIndexOrThrow("_targetname"));
                switch ((int) j) {
                    case 0:
                    case 1:
                        if (string2 != null) {
                            string = string + this.a.getString(0 == j ? C0000R.string.txtDisplayFormatWithTarget : C0000R.string.txtDisplayFormatWithIncomeTarget) + string2;
                            break;
                        }
                        break;
                    case 3:
                        string = this.a.getString(Integer.valueOf(string2).intValue() == 1 ? C0000R.string.loanOutTosb : C0000R.string.loanInFromsb, string);
                        break;
                    case 4:
                        string = this.a.getString(Integer.valueOf(string2).intValue() == 0 ? C0000R.string.paybackFromsb : C0000R.string.paybackTosb, string);
                        break;
                }
            } else {
                string = this.a.getString(C0000R.string.txtTransferString);
            }
            setViewText((TextView) findViewById2, string);
        }
        View findViewById3 = view.findViewById(C0000R.id.id_date);
        if (findViewById3 != null) {
            String format2 = m.c.format(new Date(cursor.getLong(cursor.getColumnIndexOrThrow("_outgodate")) * 1000));
            ((TextView) findViewById3).setTextColor(this.a.getColor(C0000R.color.home_timetext));
            setViewText((TextView) findViewById3, format2);
        }
        View findViewById4 = view.findViewById(C0000R.id.id_comments);
        if (findViewById4 != null) {
            setViewText((TextView) findViewById4, cursor.getString(cursor.getColumnIndexOrThrow("_comment")));
        }
    }
}
