package com.wacai365;

import android.view.View;
import java.util.Date;

final class dq implements View.OnClickListener {
    final /* synthetic */ be a;

    dq(be beVar) {
        this.a = beVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String, int, int, int]
     candidates:
      com.wacai365.m.a(android.content.Context, int, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[]
      com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void */
    public final void onClick(View view) {
        if (this.a.f != null) {
            this.a.f.a(false);
            this.a.f = null;
        }
        if (view.equals(this.a.k)) {
            be beVar = this.a;
            be.a(beVar.o, beVar.a, new dv(beVar));
        } else if (view.equals(this.a.l)) {
            be beVar2 = this.a;
            beVar2.b = m.b(beVar2.o, -1, false, new dy(beVar2));
        } else if (view.equals(this.a.m)) {
            m.a(this.a.o);
        } else if (view.equals(this.a.s)) {
            new ut(this.a.o, this.a.n, new kj(this), new Date((0 == this.a.O ? this.a.N : this.a.O) * 1000), 2).show();
        } else if (view.equals(this.a.t)) {
            m.a(this.a.o, this.a.D.getText().toString(), this.a.n.getResources().getString(C0000R.string.txtEditComment), 100, 16, false);
        } else if (view.equals(this.a.u)) {
            be.a(this.a.o, this.a.V, new Cdo(this.a));
        } else if (view.equals(this.a.v)) {
            be beVar3 = this.a;
            be.a(beVar3.o, beVar3.c, new eb(beVar3));
        } else if (view.equals(this.a.r)) {
            String obj = this.a.B.getText().toString();
            this.a.f = af.a(this.a.q, this.a.o, (obj == null || obj.length() <= 0) ? 0.0d : Double.valueOf(obj).doubleValue(), true);
            ((af) this.a.f).a(this.a.X, this.a.r);
        } else if (view.equals(this.a.j)) {
            this.a.f = af.a(this.a.q, this.a.o, this.a.x.getText().length() > 0 ? Double.valueOf(this.a.x.getText().toString()).doubleValue() : 0.0d, false);
            ((af) this.a.f).a(this.a.X, this.a.j);
        } else if (view.equals(this.a.w)) {
            new ut(this.a.o, this.a.n, new jv(this), new Date(this.a.N * 1000), 4).show();
        } else if (view.equals(this.a.K)) {
            VoiceInput.a(this.a.o, 100, this.a.D.getText().toString());
        }
    }
}
