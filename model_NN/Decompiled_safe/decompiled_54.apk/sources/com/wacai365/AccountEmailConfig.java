package com.wacai365;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.g;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.o;

public class AccountEmailConfig extends WacaiActivity {
    /* access modifiers changed from: private */
    public EditText a;
    private Button b;
    /* access modifiers changed from: private */
    public ImageButton c;
    private View d;
    private View.OnClickListener e = new oq(this);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void a(AccountEmailConfig accountEmailConfig) {
        String obj = accountEmailConfig.a.getText().toString();
        if (obj == null || obj.trim().length() <= 0) {
            accountEmailConfig.a.setText("");
            m.a(accountEmailConfig, (Animation) null, -1, (View) null, (int) C0000R.string.txtEmptyEmail);
        } else if (!m.a(obj)) {
            m.a(accountEmailConfig, (Animation) null, -1, (View) null, (int) C0000R.string.txtInvalidEmail);
        } else {
            ft ftVar = new ft(accountEmailConfig);
            ftVar.b(false);
            ftVar.c(false);
            c cVar = new c(ftVar);
            l lVar = new l();
            lVar.a(m.a((String) null, (String) null, accountEmailConfig.a.getText().toString()));
            lVar.d(accountEmailConfig.getResources().getText(C0000R.string.txtTransformData).toString());
            cVar.a((o) lVar);
            cVar.a((o) new g());
            ftVar.a(cVar);
            ftVar.a(new op(accountEmailConfig));
            ftVar.b(C0000R.string.networkProgress, C0000R.string.txtTransformData);
            ftVar.a(true, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void
     arg types: [com.wacai365.AccountEmailConfig, java.lang.String, int, ?[OBJECT, ARRAY]]
     candidates:
      com.wacai365.m.a(android.view.animation.Animation, int, android.view.View, java.lang.String):android.view.animation.Animation
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, java.util.ArrayList, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, int[], android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.app.ProgressDialog, boolean, boolean, android.content.DialogInterface$OnClickListener):com.wacai365.tp
      com.wacai365.m.a(android.content.Context, long, android.content.DialogInterface$OnClickListener, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, long, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, boolean, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void */
    static /* synthetic */ void d(AccountEmailConfig accountEmailConfig) {
        b.m().d(accountEmailConfig.a.getText().toString().trim());
        b.m().l();
        m.a((Context) accountEmailConfig, accountEmailConfig.getResources().getString(C0000R.string.accountEmailSuccess), true, (DialogInterface.OnClickListener) null);
        accountEmailConfig.c.setVisibility(0);
        accountEmailConfig.a.setEnabled(false);
        accountEmailConfig.a.setFocusable(false);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (-1 == i2 && intent != null) {
            switch (i) {
                case 13:
                    setResult(-1, getIntent());
                    finish();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.account_email_config);
        this.a = (EditText) findViewById(C0000R.id.etEmail);
        findViewById(C0000R.id.btnCancel).setOnClickListener(this.e);
        this.b = (Button) findViewById(C0000R.id.btnEmailCommit);
        this.b.setOnClickListener(this.e);
        this.c = (ImageButton) findViewById(C0000R.id.ib_edit);
        this.c.setOnClickListener(this.e);
        this.d = findViewById(C0000R.id.account_register);
        this.d.setOnClickListener(this.e);
    }
}
