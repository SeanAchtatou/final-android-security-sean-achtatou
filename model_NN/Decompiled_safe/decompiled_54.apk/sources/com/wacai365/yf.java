package com.wacai365;

import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.wacai.data.aa;
import com.wacai.data.d;
import com.wacai.data.m;
import com.wacai.data.n;
import com.wacai.data.p;
import com.wacai.data.s;
import com.wacai.data.u;
import com.wacai.data.x;
import com.wacai.data.y;
import com.wacai.data.z;
import java.util.Date;

public class yf extends oj {
    private TextView A;
    /* access modifiers changed from: private */
    public View B;
    /* access modifiers changed from: private */
    public View C;
    /* access modifiers changed from: private */
    public View D;
    /* access modifiers changed from: private */
    public View E;
    /* access modifiers changed from: private */
    public View F;
    /* access modifiers changed from: private */
    public View G;
    /* access modifiers changed from: private */
    public View H;
    /* access modifiers changed from: private */
    public View I;
    private String J = "";
    private int[] K = null;
    /* access modifiers changed from: private */
    public String[] L = null;
    /* access modifiers changed from: private */
    public va M = new cz(this);
    private yn N = new cv(this);
    private View.OnClickListener O = new cu(this);
    protected z a;
    /* access modifiers changed from: protected */
    public int[] b = null;
    /* access modifiers changed from: protected */
    public rk c = null;
    protected boolean j = true;
    /* access modifiers changed from: private */
    public LinearLayout k;
    /* access modifiers changed from: private */
    public TextView l;
    private TextView m;
    private TextView r;
    private TextView s;
    private TextView t;
    /* access modifiers changed from: private */
    public TextView u;
    /* access modifiers changed from: private */
    public TextView v;
    /* access modifiers changed from: private */
    public TextView w;
    private CheckBox x;
    /* access modifiers changed from: private */
    public Button y;
    /* access modifiers changed from: private */
    public TextView z;

    public yf(long j2) {
        if (j2 > 0) {
            this.a = z.d(j2);
            this.j = !aa.b("TBL_SCHEDULEOUTGOINFO", this.a.g());
        } else {
            this.a = new z();
            this.a.s().add(new x(this.a));
            Date date = new Date(System.currentTimeMillis());
            int hours = date.getHours();
            float minutes = (((float) date.getMinutes()) / 60.0f) + ((float) hours);
            if (p.a(1L).l()) {
                if (minutes < 5.0f || minutes >= 10.0f) {
                    if (minutes < 10.0f || minutes >= 15.0f) {
                        if (minutes < 15.0f || hours >= 21) {
                            if (y.b(10004).l()) {
                                this.a.a(10004L);
                            }
                        } else if (y.b(10003).l()) {
                            this.a.a(10003L);
                        }
                    } else if (y.b(10002).l()) {
                        this.a.a(10002L);
                    }
                } else if (y.b(10001).l()) {
                    this.a.a(10001L);
                }
            }
            m.a(this.a.a(), this.r);
        }
        this.d = C0000R.layout.input_outgo_tab;
    }

    public final long a() {
        if (this.a == null) {
            return 0;
        }
        return this.a.b();
    }

    public final void a(int i, int i2, Intent intent) {
        super.a(i, i2, intent);
        if (i2 == -1 && intent != null) {
            switch (i) {
                case 3:
                    m.a(intent, this.a);
                    m.a(this.a.s(), this.v);
                    return;
                case 6:
                    long longExtra = intent.getLongExtra("Outgo_Sel_Id", this.a.a());
                    if (longExtra > 0) {
                        this.a.a(longExtra);
                        m.a(longExtra, this.r);
                        return;
                    }
                    return;
                case 16:
                case 34:
                    String stringExtra = intent.getStringExtra("Text_String");
                    double a2 = m.a(stringExtra, this.l, this.s);
                    if (stringExtra != null) {
                        if (this.a.o() == 0 && vk.a(a2)) {
                            this.a.g((long) (a2 * 100.0d));
                        }
                        this.a.a(stringExtra);
                        m.a(stringExtra, this.s);
                        return;
                    }
                    return;
                case 18:
                    this.J = intent.getStringExtra("Text_String");
                    if (this.J == null || this.J.length() <= 0) {
                        m.a(this.n, (Animation) null, 0, (View) null, (int) C0000R.string.txtEmptyShortcutName);
                        return;
                    } else if (this.a.s().size() == 0) {
                        m.a(this.n, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideMember);
                        return;
                    } else {
                        n nVar = new n();
                        nVar.a(this.J);
                        nVar.a(0);
                        nVar.c(this.a.p());
                        nVar.d(this.a.q());
                        nVar.e(this.a.r());
                        if (this.a.s().size() == 1) {
                            nVar.b(((x) this.a.s().get(0)).a());
                        }
                        nVar.a(this.a.a());
                        nVar.f(this.a.d());
                        nVar.f(false);
                        nVar.c();
                        m.a(this.n, (Animation) null, 0, (View) null, (int) C0000R.string.txtSaveAsShortcutSuc);
                        return;
                    }
                case 24:
                    long longExtra2 = intent.getLongExtra("Target_ID", 0);
                    this.a.j(longExtra2);
                    m.a("TBL_TRADETARGET", longExtra2, this.t);
                    return;
                case 37:
                    this.a.h(intent.getLongExtra("account_Sel_Id", 0));
                    m.a(this.o, this.a.p(), this.m);
                    return;
                default:
                    return;
            }
        }
    }

    public final void a(long j2) {
        if (this.a != null) {
            this.a.b(j2);
        }
        if (this.w != null) {
            m.d(1000 * j2, this.w);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(LinearLayout linearLayout) {
        this.g = linearLayout;
        this.k = (LinearLayout) linearLayout.findViewById(C0000R.id.moneyItem);
        this.l = (TextView) linearLayout.findViewById(C0000R.id.viewMoney);
        this.r = (TextView) linearLayout.findViewById(C0000R.id.viewType);
        this.m = (TextView) linearLayout.findViewById(C0000R.id.viewAccount);
        this.s = (TextView) linearLayout.findViewById(C0000R.id.viewComment);
        this.t = (TextView) linearLayout.findViewById(C0000R.id.viewTarget);
        this.u = (TextView) linearLayout.findViewById(C0000R.id.viewProject);
        this.v = (TextView) linearLayout.findViewById(C0000R.id.viewMember);
        this.w = (TextView) linearLayout.findViewById(C0000R.id.viewDate);
        this.A = (TextView) linearLayout.findViewById(C0000R.id.titleOfReimburse);
        this.z = (TextView) linearLayout.findViewById(C0000R.id.viewReimburse);
        this.x = (CheckBox) linearLayout.findViewById(C0000R.id.cbReimburse);
        this.x.setOnCheckedChangeListener(new da(this));
        this.k.setOnClickListener(this.O);
        this.l.setText(aa.a(aa.l(this.a.o()), 2));
        m.a("TBL_ACCOUNTINFO", this.a.p(), this.m);
        m.a(this.a.a(), this.r);
        m.a(this.a.e(), this.s);
        m.a("TBL_TRADETARGET", this.a.r(), this.t);
        m.a("TBL_PROJECTINFO", this.a.q(), this.u);
        m.a(this.a.s(), this.v);
        m.d(this.a.b() * 1000, this.w);
        this.x.setChecked(this.a.d() == 1);
        this.B = linearLayout.findViewById(C0000R.id.accountItem);
        this.B.setOnClickListener(this.O);
        this.C = linearLayout.findViewById(C0000R.id.typeItem);
        this.C.setOnClickListener(this.O);
        this.D = linearLayout.findViewById(C0000R.id.commentItem);
        this.D.setOnClickListener(this.O);
        this.E = linearLayout.findViewById(C0000R.id.targetItem);
        this.E.setOnClickListener(this.O);
        this.F = linearLayout.findViewById(C0000R.id.projectItem);
        this.F.setOnClickListener(this.O);
        this.G = linearLayout.findViewById(C0000R.id.memberItem);
        this.G.setOnClickListener(this.O);
        this.H = linearLayout.findViewById(C0000R.id.dateItem);
        this.H.setOnClickListener(this.O);
        this.I = (LinearLayout) linearLayout.findViewById(C0000R.id.reimburseItem);
        this.I.setOnClickListener(this.O);
        if (this.a.x() > 0) {
            this.x.setVisibility(8);
            this.z.setVisibility(0);
            this.I.setClickable(true);
            this.I.setFocusable(true);
            this.A.setText((int) C0000R.string.txtReimburseState);
            m.a(this.n, this.a.d(), this.z);
        }
        this.y = (Button) linearLayout.findViewById(C0000R.id.voice_input);
        this.y.setOnClickListener(this.O);
    }

    public final void a(ScrollView scrollView, int i) {
        this.h = scrollView;
        this.i = i;
    }

    public final void a(y yVar, boolean z2) {
        switch (cq.a[yVar.ordinal()]) {
            case 1:
                this.e = z2;
                if (this.e) {
                    this.f = af.a(this.q, this.o, m.a(this.a.o()), false);
                    ((af) this.f).a(this.M, this.k);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void a(Object obj) {
        if (z.class.isInstance(obj)) {
            this.a = (z) obj;
            this.j = !aa.b("TBL_SCHEDULEOUTGOINFO", this.a.g());
        } else {
            if (this.a == null) {
                this.a = new z();
            }
            if (s.class.isInstance(obj)) {
                this.a.g(((s) obj).o());
            } else if (u.class.isInstance(obj)) {
                this.a.g(((u) obj).b());
            } else if (m.class.isInstance(obj)) {
                this.a.g(((m) obj).d());
            } else if (d.class.isInstance(obj)) {
                this.a.g(((d) obj).b());
            }
        }
        if (this.a != null && this.a.s().size() <= 0) {
            this.a.s().add(new x(this.a));
        }
    }

    public final void a(String str) {
        if (str != null) {
            this.a.a(str);
            m.a(this.a.e(), this.s);
        }
    }

    public final Object b() {
        return this.a;
    }

    public final void b(long j2) {
        this.a.g(j2);
        this.l.setText(aa.a(aa.l(this.a.o()), 2));
    }

    public final boolean c() {
        if (!this.j) {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.txtEditScheduleData);
            return false;
        } else if (!a(this.o, this.a)) {
            return false;
        } else {
            if (!aa.b("TBL_OUTGOSUBTYPEINFO", this.a.a())) {
                m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideOutgoST);
                return false;
            } else if (!aa.b("TBL_ACCOUNTINFO", this.a.p())) {
                m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideAccount);
                return false;
            } else if (!aa.b("TBL_PROJECTINFO", this.a.q())) {
                m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideProject);
                return false;
            } else if (!x.a(this.a.s())) {
                m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideMember);
                return false;
            } else {
                this.a.f(false);
                this.a.c();
                return true;
            }
        }
    }

    public final void d() {
        if (this.f != null) {
            if (af.class.isInstance(this.f)) {
                ((af) this.f).a(this.M, this.k);
            } else if (zl.class.isInstance(this.f)) {
                ((zl) this.f).a(this.N, this.H);
            }
        }
    }

    public final ca e() {
        return this.f;
    }

    public final void f() {
        this.a.k(0);
        this.a.a("");
        this.a.g(0);
        this.l.setText(aa.a(aa.l(this.a.o()), 2));
        m.a(this.a.e(), this.s);
    }

    /* access modifiers changed from: protected */
    public final void g() {
        if (this.a != null) {
            if (this.r != null) {
                m.a(this.a.a(), this.r);
            }
            if (this.t != null) {
                m.a("TBL_TRADETARGET", this.a.r(), this.t);
            }
        }
    }
}
