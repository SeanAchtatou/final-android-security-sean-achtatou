package com.wacai365;

import android.view.View;

final class zg implements View.OnClickListener {
    private /* synthetic */ SettingNewsType a;

    zg(SettingNewsType settingNewsType) {
        this.a = settingNewsType;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.c)) {
            SettingNewsType.b(this.a);
            this.a.finish();
        } else if (view.equals(this.a.d)) {
            this.a.finish();
        }
    }
}
