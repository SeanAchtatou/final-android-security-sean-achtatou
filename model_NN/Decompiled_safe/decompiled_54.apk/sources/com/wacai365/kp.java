package com.wacai365;

import android.content.Intent;
import android.view.View;

final class kp implements View.OnClickListener {
    private /* synthetic */ ChooseTarget a;

    kp(ChooseTarget chooseTarget) {
        this.a = chooseTarget;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.btnCancel /*2131492870*/:
                this.a.finish();
                return;
            case C0000R.id.btnAdd /*2131492886*/:
                ChooseTarget.b(this.a);
                return;
            case C0000R.id.btnTargetMgr /*2131492956*/:
                Intent intent = new Intent(this.a, SettingTradeTargetMgr.class);
                intent.putExtra("LaunchedByApplication", 1);
                intent.putExtra("Is_Payer", this.a.h);
                this.a.startActivityForResult(intent, 32);
                return;
            default:
                return;
        }
    }
}
