package com.wacai365;

import android.view.View;

final class vu implements View.OnClickListener {
    private /* synthetic */ DataSynchronize a;

    vu(DataSynchronize dataSynchronize) {
        this.a = dataSynchronize;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.a)) {
            this.a.a.setEnabled(false);
            DataSynchronize.f(this.a);
        } else if (view.equals(this.a.b)) {
            this.a.finish();
        }
    }
}
