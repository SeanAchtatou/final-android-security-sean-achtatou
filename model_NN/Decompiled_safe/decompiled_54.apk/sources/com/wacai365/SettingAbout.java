package com.wacai365;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.h;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.o;
import com.wacai.data.VersionItem;
import com.wacai.f;
import java.util.ArrayList;

public class SettingAbout extends WacaiActivity implements tt {
    /* access modifiers changed from: private */
    public ArrayList a = new ArrayList();
    private fy b = null;

    public static void a(Activity activity, ArrayList arrayList) {
        VersionItem versionItem = (VersionItem) arrayList.get(0);
        Resources resources = activity.getResources();
        m.a(activity, resources.getString(C0000R.string.txtVersionTitle), resources.getString(C0000R.string.txtVersionDes, versionItem.c, versionItem.b), (int) C0000R.string.txtUpdateNow, (int) C0000R.string.txtCancel, new us(activity, versionItem));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void a(SettingAbout settingAbout) {
        settingAbout.a.clear();
        ft ftVar = new ft(settingAbout);
        ftVar.e(false);
        ftVar.a((tt) settingAbout);
        ftVar.b(C0000R.string.networkProgress, C0000R.string.txtTransformData);
        settingAbout.b = ftVar;
        c cVar = new c(ftVar);
        l lVar = new l();
        lVar.a(true);
        lVar.d(settingAbout.getResources().getText(C0000R.string.txtTransformData).toString());
        lVar.a(m.b());
        cVar.a((o) lVar);
        h hVar = new h();
        hVar.a(settingAbout.a);
        cVar.a((o) hVar);
        ftVar.a(cVar);
        ftVar.b(false);
        ftVar.c(false);
        ftVar.a(false, true);
    }

    public final void a(int i) {
        runOnUiThread(new up(this));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1 && this.b != null) {
            this.b.a(i, i2, intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.about);
        String string = getResources().getString(C0000R.string.txtAboutTitle);
        b.m();
        StringBuilder sb = new StringBuilder(b.a());
        String b2 = f.e().b();
        if (b2.length() > 0) {
            sb.append(".");
            sb.append(b2);
        }
        ((TextView) findViewById(C0000R.id.id_aboutTitle)).setText(String.format(string, sb.toString()));
        ((TextView) findViewById(C0000R.id.id_copyRight)).setText(getResources().getString(C0000R.string.txtCopyRight));
        ((Button) findViewById(C0000R.id.btnUpdate)).setOnClickListener(new un(this));
        ((Button) findViewById(C0000R.id.btnFeedback)).setOnClickListener(new uk(this));
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(new uq(this));
    }
}
