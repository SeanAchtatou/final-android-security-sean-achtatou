package com.wacai365;

import android.content.DialogInterface;
import android.database.Cursor;
import android.view.View;
import android.view.animation.Animation;
import com.wacai.a;
import com.wacai.data.aa;
import com.wacai.data.n;
import com.wacai.data.s;

final class oh implements DialogInterface.OnClickListener {
    private /* synthetic */ int a;
    private /* synthetic */ hb b;

    oh(hb hbVar, int i) {
        this.b = hbVar;
        this.a = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.MyShortcuts.a(com.wacai365.MyShortcuts, boolean):android.content.DialogInterface$OnClickListener
     arg types: [com.wacai365.MyShortcuts, int]
     candidates:
      com.wacai365.MyShortcuts.a(com.wacai365.MyShortcuts, int):int
      com.wacai365.MyShortcuts.a(com.wacai365.MyShortcuts, long):long
      com.wacai365.MyShortcuts.a(com.wacai365.MyShortcuts, com.wacai365.fy):com.wacai365.fy
      com.wacai365.MyShortcuts.a(com.wacai365.MyShortcuts, boolean):android.content.DialogInterface$OnClickListener */
    public final void onClick(DialogInterface dialogInterface, int i) {
        if (-1 == i) {
            double a2 = ((nl) dialogInterface).a();
            if (a2 <= 0.0d) {
                m.a(this.b.b, (Animation) null, 0, (View) null, (int) C0000R.string.txtInvalidMoney);
                return;
            }
            s h = n.h(this.b.b.a);
            h.g(aa.a(a2));
            h.f(false);
            h.c();
            WidgetProvider.a(this.b.b);
            this.b.b.setResult(-1, this.b.b.getIntent());
            Cursor cursor = (Cursor) this.b.a.getItem(this.a);
            int unused = this.b.b.f = cursor.getInt(cursor.getColumnIndexOrThrow("_accountid"));
            if (m.a(this.b.b.f)) {
                m.a(this.b.b, (long) this.b.b.f, MyShortcuts.a(this.b.b, true), MyShortcuts.a(this.b.b, false));
            } else if (a.a("IsAutoBackup", 0) <= 0 || this.b.b.g == 0) {
                m.a(this.b.b, (Animation) null, 0, (View) null, (int) C0000R.string.txtSaveSuccess);
                this.b.b.finish();
            } else {
                abt.b(this.b.b);
            }
        }
    }
}
