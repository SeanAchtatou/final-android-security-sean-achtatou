package com.wacai365;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

final class d implements AdapterView.OnItemClickListener {
    private /* synthetic */ WeiboSettingList a;

    d(WeiboSettingList weiboSettingList) {
        this.a = weiboSettingList;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        ll[] b;
        if (view != null && (b = aaj.a().b()) != null && i >= 0 && i < b.length) {
            ll llVar = b[i];
            if (llVar.a()) {
                Intent intent = new Intent(this.a, WeiboHome.class);
                intent.putExtra("Extra-webo-type", llVar.b);
                this.a.startActivity(intent);
                return;
            }
            Intent intent2 = new Intent(this.a, WeiboConnect.class);
            intent2.putExtra("Extra-webo-type", llVar.b);
            this.a.startActivityForResult(intent2, 31);
        }
    }
}
