package com.wacai365;

import android.app.Activity;
import com.wacai.a;
import com.wacai.a.e;
import com.wacai.b.c;
import com.wacai.b.h;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.o;
import java.util.ArrayList;
import java.util.Date;

final class df extends o {
    private /* synthetic */ Activity d;

    df(Activity activity) {
        this.d = activity;
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        Activity activity = this.d;
        if (e.a()) {
            long time = new Date().getTime();
            if (a.a("PropNextUpdateTime", 0) <= time) {
                ArrayList arrayList = new ArrayList();
                c cVar = new c(new qr(time, arrayList, activity));
                l lVar = new l();
                lVar.a(true);
                lVar.a(m.b());
                cVar.a((o) lVar);
                h hVar = new h();
                hVar.a(arrayList);
                cVar.a((o) hVar);
                cVar.b();
            }
        }
        return true;
    }

    public final void a_(Object obj) {
    }
}
