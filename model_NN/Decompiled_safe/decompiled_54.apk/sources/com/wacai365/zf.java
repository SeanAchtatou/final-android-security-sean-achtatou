package com.wacai365;

import android.text.Spanned;
import android.text.method.NumberKeyListener;

final class zf extends NumberKeyListener {
    private /* synthetic */ NumberPicker a;

    /* synthetic */ zf(NumberPicker numberPicker) {
        this(numberPicker, (byte) 0);
    }

    private zf(NumberPicker numberPicker, byte b) {
        this.a = numberPicker;
    }

    public final CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        CharSequence filter = super.filter(charSequence, i, i2, spanned, i3, i4);
        if (filter == null) {
            filter = charSequence.subSequence(i, i2);
        }
        String str = String.valueOf(spanned.subSequence(0, i3)) + ((Object) filter) + ((Object) spanned.subSequence(i4, spanned.length()));
        return "".equals(str) ? str : this.a.a(str) > this.a.a ? "" : filter;
    }

    /* access modifiers changed from: protected */
    public final char[] getAcceptedChars() {
        return NumberPicker.o;
    }

    public final int getInputType() {
        return 2;
    }
}
