package com.wacai365;

import android.content.DialogInterface;
import com.wacai.data.x;
import java.util.ArrayList;

final class iv implements DialogInterface.OnClickListener {
    private /* synthetic */ bj a;

    iv(bj bjVar) {
        this.a = bjVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (!(this.a.a == null || this.a.a.b == null || this.a.a.b.length <= 0)) {
            int length = this.a.a.b.length;
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < length; i2++) {
                if (this.a.a.b[i2]) {
                    arrayList.add(m.a(this.a.D, this.a.a.a[i2]));
                }
            }
            int size = arrayList.size();
            if (1 == size) {
                ((x) arrayList.get(0)).b(this.a.D.o());
                this.a.D.a(arrayList);
                m.a(this.a.D.s(), this.a.s);
            } else if (1 < size) {
                m.a(this.a.o, arrayList, this.a.D.o());
            } else {
                this.a.D.a(arrayList);
                this.a.s.setText("");
            }
        }
        dialogInterface.dismiss();
    }
}
