package com.wacai365;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.wacai.a;
import com.wacai.b;
import com.wacai.data.aa;
import com.wacai.data.d;
import com.wacai.data.h;
import com.wacai.data.m;
import com.wacai.data.r;
import com.wacai.data.u;
import com.wacai.data.z;
import com.wacai.e;
import java.util.Date;

public final class kk extends ug {
    private static rn[] H = {new rn(2, C0000R.string.txtHomeScreenDisplayType_OutgoMonth, true, 4), new rn(0, C0000R.string.txtHomeScreenDisplayType_OutgoToday, true, 0), new rn(1, C0000R.string.txtHomeScreenDisplayType_OutgoWeek, true, 2), new rn(3, C0000R.string.txtHomeScreenDisplayType_OutgoYear, true, 8), new rn(6, C0000R.string.txtHomeScreenDisplayType_IncomeMonth, false, 4), new rn(4, C0000R.string.txtHomeScreenDisplayType_IncomeToday, false, 0), new rn(5, C0000R.string.txtHomeScreenDisplayType_IncomeWeek, false, 2), new rn(7, C0000R.string.txtHomeScreenDisplayType_IncomeYear, false, 8)};
    private QueryInfo A = null;
    private Animation B;
    private CheckBox C;
    /* access modifiers changed from: private */
    public View D;
    /* access modifiers changed from: private */
    public View E;
    private View F;
    /* access modifiers changed from: private */
    public fy G = null;
    private LinearLayout I = null;
    private LinearLayout J = null;
    private LinearLayout K = null;
    private LinearLayout L = null;
    /* access modifiers changed from: private */
    public int M = 16;
    Activity a = null;
    ListView b = null;
    pb c = new pb(this);
    TextView d = null;
    TextView e = null;
    TextView f = null;
    TextView g = null;
    int[] h = null;
    int[] i = null;
    int j = 0;
    int k = 0;
    int l = 0;
    int m = 0;
    private Context r = null;
    private LinearLayout s = null;
    private LinearLayout t = null;
    private TextView u = null;
    private TextView v = null;
    private TextView w = null;
    private TextView x;
    private TextView y;
    private QueryInfo z = null;

    private static long a(int i2) {
        StringBuffer stringBuffer = new StringBuffer(256);
        stringBuffer.append("select a.id as _id, a.name as _name, a.enable as _enable, a.isdefault as _isdefault, a.balance as _balance, a.balancedate as _balancedate, a.hasbalance as _hasbalance, b.flag as _flag, b.id as _moneytypeid from TBL_ACCOUNTINFO a, TBL_MONEYTYPE b where a.enable = 1 and a.type <> 3 and a.moneytype = b.id ");
        if (-1 != i2) {
            stringBuffer.append(" and a.id = " + i2);
        } else {
            stringBuffer.append(" and a.moneytype = " + b.m().j());
        }
        stringBuffer.append(" ORDER BY a.orderno ASC");
        Cursor rawQuery = e.c().b().rawQuery(stringBuffer.toString(), null);
        long time = new Date().getTime() / 1000;
        int count = rawQuery.getCount();
        if (rawQuery == null || count <= 0) {
            return 0;
        }
        rawQuery.moveToFirst();
        long j2 = 0;
        long j3 = 0;
        for (int i3 = 0; i3 < count; i3++) {
            long j4 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_id"));
            long j5 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_balance"));
            long j6 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_balancedate"));
            if (0 != rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_hasbalance"))) {
                stringBuffer.delete(0, stringBuffer.length());
                stringBuffer.append("select sum(_total) from (");
                stringBuffer.append("select sum(money) as _total from TBL_INCOMEINFO where incomedate >= " + j6 + " and incomedate <= " + time + " and accountid = " + j4 + " and isdelete = 0");
                stringBuffer.append(" UNION ALL ");
                stringBuffer.append("select -sum(money) from TBL_OUTGOINFO where outgodate >= " + j6 + " and outgodate <= " + time + " and accountid = " + j4 + " and isdelete = 0");
                stringBuffer.append(" UNION ALL ");
                stringBuffer.append("select -sum(transferoutmoney) from TBL_TRANSFERINFO where date >= " + j6 + " and date <= " + time + " and transferoutaccountid = " + j4 + " and isdelete = 0");
                stringBuffer.append(" UNION ALL ");
                stringBuffer.append("select sum(transferinmoney) from TBL_TRANSFERINFO where date >= " + j6 + " and date <= " + time + " and transferinaccountid = " + j4 + " and isdelete = 0");
                stringBuffer.append(")");
                Cursor rawQuery2 = e.c().b().rawQuery(stringBuffer.toString(), null);
                while (rawQuery2.moveToNext()) {
                    j3 = rawQuery2.getLong(0);
                }
                rawQuery2.close();
                j2 += j5 + j3;
            }
            rawQuery.moveToNext();
        }
        rawQuery.close();
        return j2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long a(long r6, long r8, boolean r10) {
        /*
            r5 = 0
            r0 = 0
            if (r10 == 0) goto L_0x0082
            boolean r2 = com.wacai.b.a
            if (r2 != 0) goto L_0x004c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "SELECT sum(a.money) from TBL_OUTGOINFO a where a.outgodate >= "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r3 = " and a.outgodate <= "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r3 = " and a.isdelete = 0"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x002c:
            com.wacai.e r3 = com.wacai.e.c()
            android.database.sqlite.SQLiteDatabase r3 = r3.b()
            r4 = 0
            android.database.Cursor r2 = r3.rawQuery(r2, r4)     // Catch:{ Exception -> 0x00e1, all -> 0x00f0 }
            if (r2 == 0) goto L_0x0046
            boolean r3 = r2.moveToFirst()     // Catch:{ Exception -> 0x00fb, all -> 0x00f8 }
            if (r3 == 0) goto L_0x0046
            r0 = 0
            long r0 = r2.getLong(r0)     // Catch:{ Exception -> 0x00fb, all -> 0x00f8 }
        L_0x0046:
            if (r2 == 0) goto L_0x004b
            r2.close()
        L_0x004b:
            return r0
        L_0x004c:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "SELECT sum(a.money) from TBL_OUTGOINFO a, TBL_ACCOUNTINFO b where a.outgodate >= "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r3 = " and a.outgodate <= "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r3 = " and "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "a.isdelete = 0 and a.accountid = b.id and b.moneytype = "
            java.lang.StringBuilder r2 = r2.append(r3)
            com.wacai.b r3 = com.wacai.b.m()
            long r3 = r3.j()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            goto L_0x002c
        L_0x0082:
            boolean r2 = com.wacai.b.a
            if (r2 != 0) goto L_0x00aa
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "SELECT sum(a.money) from TBL_INCOMEINFO a where a.incomedate >= "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r3 = " and a.incomedate <= "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r3 = " and a.isdelete = 0"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            goto L_0x002c
        L_0x00aa:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "SELECT sum(a.money) from TBL_INCOMEINFO a, TBL_ACCOUNTINFO b where a.incomedate >= "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r3 = " and a.incomedate <= "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r3 = " and "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "a.isdelete = 0 and a.accountid = b.id and b.moneytype = "
            java.lang.StringBuilder r2 = r2.append(r3)
            com.wacai.b r3 = com.wacai.b.m()
            long r3 = r3.j()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            goto L_0x002c
        L_0x00e1:
            r0 = move-exception
            r0 = r5
        L_0x00e3:
            r1 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            if (r0 == 0) goto L_0x00fe
            r0.close()
            r0 = r1
            goto L_0x004b
        L_0x00f0:
            r0 = move-exception
            r1 = r5
        L_0x00f2:
            if (r1 == 0) goto L_0x00f7
            r1.close()
        L_0x00f7:
            throw r0
        L_0x00f8:
            r0 = move-exception
            r1 = r2
            goto L_0x00f2
        L_0x00fb:
            r0 = move-exception
            r0 = r2
            goto L_0x00e3
        L_0x00fe:
            r0 = r1
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.kk.a(long, long, boolean):long");
    }

    /* access modifiers changed from: private */
    public void a(int i2, long j2) {
        long j3;
        long j4;
        Cursor cursor = (Cursor) this.b.getItemAtPosition(i2);
        if (cursor != null) {
            j3 = cursor.getLong(cursor.getColumnIndexOrThrow("_flag"));
            j4 = j2 <= 0 ? cursor.getLong(cursor.getColumnIndexOrThrow("_id")) : j2;
        } else {
            j3 = 0;
            j4 = j2;
        }
        if (j3 == 0 || j3 == 1) {
            long j5 = cursor.getLong(cursor.getColumnIndexOrThrow("_paybackid"));
            if (j5 > 0) {
                j3 = 4;
                j4 = j5;
            }
        }
        if (0 < j4) {
            Intent a2 = InputTrade.a(this.a, "", j4);
            a2.putExtra("LaunchedByApplication", 1);
            a2.putExtra("Extra_Type", (int) j3);
            a2.putExtra("Extra_Type2", cursor.getInt(cursor.getColumnIndex("_type")));
            a2.putExtra("Extra_Id", j4);
            this.a.startActivityForResult(a2, 19);
        }
    }

    static /* synthetic */ void a(kk kkVar, boolean z2) {
        if (kkVar.B == null) {
            kkVar.B = new ef(kkVar, kkVar.D.getHeight());
            kkVar.B.setDuration(600);
            kkVar.B.setInterpolator(new AccelerateInterpolator());
            kkVar.B.setAnimationListener(new eh(kkVar));
        }
        if (z2) {
            kkVar.j = (int) a.a("HomeScreenTitleType1", 1);
            kkVar.k = (int) a.a("HomeScreenTitleType2", 2);
            kkVar.l = (int) a.a("HomeScreenAccountID1", aa.f("TBL_ACCOUNTINFO"));
            kkVar.m = (int) a.a("HomeScreenAccountID2", aa.f("TBL_ACCOUNTINFO"));
            kkVar.I = (LinearLayout) kkVar.a.findViewById(C0000R.id.layoutItem1);
            kkVar.I.setOnClickListener(new ej(kkVar));
            kkVar.d = (TextView) kkVar.a.findViewById(C0000R.id.btnItem1);
            kkVar.d.setText(m.f(kkVar.a, kkVar.j));
            kkVar.J = (LinearLayout) kkVar.a.findViewById(C0000R.id.layoutItem2);
            kkVar.J.setOnClickListener(new el(kkVar));
            kkVar.e = (TextView) kkVar.a.findViewById(C0000R.id.btnItem2);
            m.a("TBL_ACCOUNTINFO", (long) kkVar.l, kkVar.e);
            kkVar.K = (LinearLayout) kkVar.a.findViewById(C0000R.id.layoutItem3);
            kkVar.K.setOnClickListener(new en(kkVar));
            kkVar.f = (TextView) kkVar.a.findViewById(C0000R.id.btnItem3);
            kkVar.f.setText(m.f(kkVar.a, kkVar.k));
            kkVar.L = (LinearLayout) kkVar.a.findViewById(C0000R.id.layoutItem4);
            kkVar.L.setOnClickListener(new eq(kkVar));
            kkVar.g = (TextView) kkVar.a.findViewById(C0000R.id.btnItem4);
            m.a("TBL_ACCOUNTINFO", (long) kkVar.m, kkVar.g);
            kkVar.g();
            kkVar.E.startAnimation(kkVar.B);
            return;
        }
        kkVar.E.setVisibility(8);
        kkVar.D.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void c() {
        Date date = new Date();
        Date date2 = new Date();
        com.wacai.a.a.a(10, date, date2);
        long time = date.getTime() / 1000;
        long time2 = date2.getTime() / 1000;
        StringBuffer stringBuffer = new StringBuffer(512);
        stringBuffer.append("select * from (select 0 as _flag, a.ymd as _ymd, a.id as _id, a.outgodate as _outgodate, a.money as _money, a.comment as _comment, a.scheduleoutgoid as _scheduleid, a.paybackid as _paybackid, 0 as _type, c.name as _name, 0 as _transferoutmoney, 0 as _transferinmoney, 0 as _transferoutname, 0 as _transferinname, 0 as _transferoutaccountid, 0 as _transferinaccountid, 0 as _transinmoney, h.flag as _moneyflag1, 0 as _moneyflag2, h.id as _moneytypeid1, 0 as _moneytypeid2, i.name as _targetname , a.reimburse as _reimburse, a.projectid as _projectID from tbl_outgoinfo a, TBL_OUTGOSUBTYPEINFO c, TBL_PROJECTINFO d, TBL_ACCOUNTINFO e, TBL_MEMBERINFO f, TBL_OUTGOMAINTYPEINFO g, TBL_MONEYTYPE h LEFT JOIN TBL_TRADETARGET i ON a.targetid = i.id LEFT JOIN TBL_OUTGOMEMBERINFO b ON b.memberid = f.id and b.outgoid = a.id where a.isdelete = 0 and c.id = a.subtypeid and d.id = a.projectid and e.id = a.accountid and g.id = c.id / 10000 and e.moneytype = h.id ");
        stringBuffer.append(" and a.outgodate >= " + time);
        stringBuffer.append(" and a.outgodate <= " + time2);
        stringBuffer.append(" group by a.id UNION ALL select 1 as _flag, a.ymd as _ymd, a.id as _id, a.incomedate as _outgodate, a.money as _money, a.comment as _comment, a.scheduleincomeid as _scheduleid, a.paybackid as _paybackid, 0 as _type, c.name as _name, 0 as _transferoutmoney, 0 as _transferinmoney, 0 as _transferoutname, 0 as _transferinname, 0 as _transferoutaccountid, 0 as _transferinaccountid, 0 as _transinmoney, g.flag as _moneyflag1, 0 as _moneyflag2, g.id as _moneytypeid1, 0 as _moneytypeid2, i.name as _targetname, null as _reimburse, a.projectid as _projectID from TBL_INCOMEINFO a, TBL_INCOMEMAINTYPEINFO c, TBL_PROJECTINFO d, TBL_ACCOUNTINFO e, TBL_MEMBERINFO f, TBL_MONEYTYPE g LEFT JOIN TBL_TRADETARGET i ON a.targetid = i.id LEFT JOIN TBL_INCOMEMEMBERINFO b ON b.incomeid = a.id and b.memberid = f.id where a.isdelete = 0 and c.id = a.typeid and d.id = a.projectid and e.id = a.accountid and e.moneytype = g.id ");
        stringBuffer.append(" and a.incomedate >= " + time);
        stringBuffer.append(" and a.incomedate <= " + time2);
        stringBuffer.append(" group by a.id UNION ALL select 2 as _flag, a.ymd as _ymd, a.id as _id, a.date as _outgodate, a.transferoutmoney as _money, a.comment as _comment, 0 as _scheduleid, 0 as _paybackid, 0 as _type, 0 as _name, a.transferoutmoney as _transferoutmoney, a.transferinmoney as _transferinmoney, b.name as _transferoutname, c.name as _transferinname, a.transferoutaccountid as _transferoutaccountid, a.transferinaccountid as _transferinaccountid, a.transferinmoney as _transinmoney, d.flag as _moneyflag1, e.flag as _moneyflag2, d.id as _moneytypeid1, e.id as _moneytypeid2, NULL as _targetname , null as _reimburse, 0 as _projectID from TBL_TRANSFERINFO a, TBL_ACCOUNTINFO b,  TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and b.id = a.transferoutaccountid and c.id = a.transferinaccountid and b.moneytype = d.id and c.moneytype = e.id and a.type = 0 ");
        stringBuffer.append(" and a.date >= " + time);
        stringBuffer.append(" and a.date <= " + time2);
        stringBuffer.append(" group by a.id UNION ALL select 3 as _flag, a.ymd as _ymd, a.id as _id, a.date as _outgodate, a.money as _money, a.comment as _comment, 0 as _scheduleid, 0 as _paybackid, a.type as _type, 0 as _name, 0 as _transferoutmoney, 0 as _transferinmoney, b.name as _transferoutname, c.name as _transferinname, a.accountid as _transferoutaccountid, a.debtid as _transferinaccountid, 0 as _transinmoney, d.flag as _moneyflag1, e.flag as _moneyflag2, d.id as _moneytypeid1, e.id as _moneytypeid2, 0 as _targetname, null as _reimburse, 0 as _projectID from TBL_LOAN a, TBL_ACCOUNTINFO b, TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and b.id = a.accountid and c.id = a.debtid and d.id = b.moneytype and e.id = c.moneytype ");
        stringBuffer.append(" and a.date >= " + time);
        stringBuffer.append(" and a.date <= " + time2);
        stringBuffer.append(" group by a.id UNION ALL select 4 as _flag, a.ymd as _ymd, a.id as _id, a.date as _outgodate, a.money as _money, a.comment as _comment, 0 as _scheduleid, 0 as _paybackid, a.type as _type, 0 as _name, 0 as _transferoutmoney, 0 as _transferinmoney, b.name as _transferoutname, c.name as _transferinname, a.accountid as _transferoutaccountid, a.debtid as _transferinaccountid, 0 as _transinmoney, d.flag as _moneyflag1, e.flag as _moneyflag2, d.id as _moneytypeid1, e.id as _moneytypeid2, 0 as _targetname, null as _reimburse, 0 as _projectID from TBL_PAYBACK a, TBL_ACCOUNTINFO b, TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and b.id = a.accountid and c.id = a.debtid and d.id = b.moneytype and e.id = c.moneytype ");
        stringBuffer.append(" and a.date >= " + time);
        stringBuffer.append(" and a.date <= " + time2);
        stringBuffer.append(" group by a.id) order by _outgodate DESC");
        Cursor rawQuery = e.c().b().rawQuery(stringBuffer.toString(), null);
        this.a.startManagingCursor(rawQuery);
        this.b.setAdapter((ListAdapter) new so(this.r, C0000R.layout.home_list_item, rawQuery, new String[]{"_money"}, new int[]{C0000R.id.id_money}, b.a));
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.b.getAdapter().getCount() == 0) {
            this.u.setVisibility(0);
            this.b.setVisibility(8);
            return;
        }
        this.u.setVisibility(8);
        this.b.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void e() {
        boolean z2;
        int i2;
        int i3;
        boolean z3;
        Date date = new Date();
        Date date2 = new Date();
        int a2 = (int) a.a("HomeScreenTitleType1", 1);
        int a3 = (int) a.a("HomeScreenTitleType2", 2);
        String a4 = b.a ? aa.a("TBL_MONEYTYPE", "flag", (int) b.m().j()) : "";
        if (8 == a2) {
            this.v.setText((int) C0000R.string.txtHeaderStatTitleBalance);
            this.x.setText(a4 + m.b(a(-1)));
            i3 = 0;
            z3 = true;
        } else if (9 == a2) {
            int a5 = (int) a.a("HomeScreenAccountID1", aa.f("TBL_ACCOUNTINFO"));
            this.v.setText(aa.a("TBL_ACCOUNTINFO", "name", a5));
            this.x.setText(h.c(a5) + m.b(a(a5)));
            i3 = 0;
            z3 = true;
        } else {
            int length = H.length;
            int i4 = 0;
            while (true) {
                if (i4 >= length) {
                    z2 = true;
                    i2 = 0;
                    break;
                } else if (H[i4].a == a2) {
                    com.wacai.a.a.a(H[i4].d, date, date2);
                    i2 = H[i4].b;
                    z2 = H[i4].c;
                    break;
                } else {
                    i4++;
                }
            }
            this.z = new QueryInfo();
            this.z.b = com.wacai.a.a.a(date.getTime());
            this.z.c = com.wacai.a.a.a(date2.getTime());
            long a6 = a(date.getTime() / 1000, date2.getTime() / 1000, z2);
            this.v.setText(i2);
            this.x.setText(a4 + m.b(a6));
            i3 = i2;
            z3 = z2;
        }
        if (8 == a3) {
            this.w.setText((int) C0000R.string.txtHeaderStatTitleBalance);
            this.y.setText(a4 + m.b(a(-1)));
        } else if (9 == a3) {
            int a7 = (int) a.a("HomeScreenAccountID2", aa.f("TBL_ACCOUNTINFO"));
            this.w.setText(aa.a("TBL_ACCOUNTINFO", "name", a7));
            this.y.setText(h.c(a7) + m.b(a(a7)));
        } else {
            Date date3 = new Date();
            Date date4 = new Date();
            int length2 = H.length;
            int i5 = 0;
            while (true) {
                if (i5 >= length2) {
                    break;
                } else if (H[i5].a == a3) {
                    com.wacai.a.a.a(H[i5].d, date3, date4);
                    i3 = H[i5].b;
                    z3 = H[i5].c;
                    break;
                } else {
                    i5++;
                }
            }
            this.A = new QueryInfo();
            this.A.b = com.wacai.a.a.a(date3.getTime());
            this.A.c = com.wacai.a.a.a(date4.getTime());
            long a8 = a(date3.getTime() / 1000, date4.getTime() / 1000, z3);
            this.w.setText(i3);
            this.y.setText(a4 + m.b(a8));
        }
    }

    private void f() {
        if (this.F != null) {
            if (com.wacai.data.b.a()) {
                this.F.setVisibility(0);
            } else {
                this.F.setVisibility(4);
            }
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        if (9 == this.j) {
            this.J.setVisibility(0);
        } else {
            this.J.setVisibility(8);
        }
        if (9 == this.k) {
            this.L.setVisibility(0);
        } else {
            this.L.setVisibility(8);
        }
    }

    public final void a(int i2, int i3, Intent intent) {
        c();
        e();
        d();
        if (this.G != null) {
            this.G.a(i2, i3, intent);
        }
        if (i3 == -1 && intent != null && i2 == 34) {
            Intent a2 = InputTrade.a(this.a, "", 0);
            a2.putExtra("LaunchedByApplication", 1);
            a2.putExtra("Text_String", intent.getStringExtra("Text_String"));
            this.a.startActivityForResult(a2, 0);
        }
        if (i3 != -1 || intent == null || i2 != 37) {
            return;
        }
        if (16 == this.M) {
            this.l = (int) intent.getLongExtra("account_Sel_Id", (long) this.l);
            if (h.d((long) this.l).d()) {
                m.a("TBL_ACCOUNTINFO", (long) this.l, this.e);
                a.b("HomeScreenTitleType1", (long) this.j);
                a.b("HomeScreenAccountID1", (long) this.l);
                e();
                return;
            }
            m.a(this.a, (Animation) null, 0, (View) null, (int) C0000R.string.txtChoosedBalanceNotSet);
        } else if (32 == this.M) {
            this.m = (int) intent.getLongExtra("account_Sel_Id", (long) this.m);
            if (h.d((long) this.m).d()) {
                m.a("TBL_ACCOUNTINFO", (long) this.m, this.g);
                a.b("HomeScreenTitleType2", (long) this.k);
                a.b("HomeScreenAccountID2", (long) this.m);
                e();
                return;
            }
            m.a(this.a, (Animation) null, 0, (View) null, (int) C0000R.string.txtChoosedBalanceNotSet);
        }
    }

    public final void a(Context context, LinearLayout linearLayout, LinearLayout linearLayout2, Activity activity, boolean z2) {
        this.r = context;
        this.a = activity;
        this.s = linearLayout;
        ViewGroup viewGroup = (ViewGroup) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) C0000R.layout.homescreen, (ViewGroup) null).findViewById(C0000R.id.homescreenlayout);
        viewGroup.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.removeAllViews();
        linearLayout.addView(viewGroup);
        this.D = viewGroup.findViewById(C0000R.id.segMain);
        this.E = viewGroup.findViewById(C0000R.id.segSetting);
        ((Button) viewGroup.findViewById(C0000R.id.btnInput)).setOnClickListener(new abd(this));
        ((Button) viewGroup.findViewById(C0000R.id.btnShortcuts)).setOnClickListener(new abj(this));
        ((ImageButton) viewGroup.findViewById(C0000R.id.btnVoiceAccount)).setOnClickListener(new abi(this));
        ((ImageButton) viewGroup.findViewById(C0000R.id.btnSearch)).setOnClickListener(new abl(this));
        this.F = viewGroup.findViewById(C0000R.id.btnNews);
        abk abk = new abk(this);
        this.F.setOnClickListener(abk);
        f();
        viewGroup.findViewById(C0000R.id.btnAlertCenter).setOnClickListener(abk);
        this.C = (CheckBox) viewGroup.findViewById(C0000R.id.cbHomeSetting);
        this.C.setOnCheckedChangeListener(new abf(this));
        this.b = (ListView) viewGroup.findViewById(C0000R.id.IOList);
        this.b.setOnItemClickListener(new abe(this));
        this.b.setOnCreateContextMenuListener(new abh(this));
        this.u = (TextView) viewGroup.findViewById(C0000R.id.id_listhint);
        this.x = (TextView) this.s.findViewById(C0000R.id.tv_stat_one);
        this.y = (TextView) this.s.findViewById(C0000R.id.tv_stat_two);
        Typeface createFromAsset = Typeface.createFromAsset(this.a.getAssets(), "font/wacaittf.ttf");
        this.x.setTypeface(createFromAsset);
        this.y.setTypeface(createFromAsset);
        this.v = (TextView) this.s.findViewById(C0000R.id.tv_stat_title_one);
        this.w = (TextView) this.s.findViewById(C0000R.id.tv_stat_title_two);
        ((ImageView) this.s.findViewById(C0000R.id.ivSynch)).setOnClickListener(new abg(this));
        super.a(context, linearLayout, linearLayout2, activity, z2);
    }

    public final boolean a(MenuItem menuItem) {
        long j2;
        long j3;
        long j4;
        long j5;
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        switch (menuItem.getItemId()) {
            case C0000R.id.idDelete /*2131493338*/:
                Cursor cursor = (Cursor) this.b.getItemAtPosition(adapterContextMenuInfo.position);
                if (cursor != null) {
                    long j6 = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
                    long j7 = cursor.getLong(cursor.getColumnIndexOrThrow("_flag"));
                    long j8 = cursor.getLong(cursor.getColumnIndexOrThrow("_scheduleid"));
                    j2 = j6;
                    j3 = cursor.getLong(cursor.getColumnIndexOrThrow("_paybackid"));
                    long j9 = j7;
                    j5 = j8;
                    j4 = j9;
                } else {
                    j2 = 0;
                    j3 = 0;
                    j4 = 0;
                    j5 = 0;
                }
                if (j5 <= 0) {
                    if (j3 <= 0) {
                        switch ((int) j4) {
                            case 0:
                                z.e(j2);
                                break;
                            case 1:
                                r.d(j2);
                                break;
                            case 2:
                                u.g(j2);
                                break;
                            case 3:
                                m.g(j2);
                                break;
                            case 4:
                                d.g(j2);
                                break;
                        }
                        WidgetProvider.a(this.a);
                        cursor.requery();
                        this.b.invalidateViews();
                        e();
                        d();
                        b();
                        break;
                    } else {
                        m.a(this.a, (Animation) null, 0, (View) null, (int) C0000R.string.txtDeletePayGenData);
                        return false;
                    }
                } else {
                    m.a(this.a, (Animation) null, 0, (View) null, (int) C0000R.string.txtDeleteScheduleData);
                    return false;
                }
            case C0000R.id.idEdit /*2131493342*/:
                a(adapterContextMenuInfo.position, -1);
                break;
            case C0000R.id.idReimburse /*2131493345*/:
                long j10 = adapterContextMenuInfo.id;
                int i2 = adapterContextMenuInfo.position;
                Intent intent = new Intent(this.a, ReimburseDialog.class);
                Cursor cursor2 = (Cursor) this.b.getItemAtPosition(i2);
                if (cursor2 != null) {
                    long j11 = cursor2.getLong(cursor2.getColumnIndexOrThrow("_money"));
                    String string = cursor2.getString(cursor2.getColumnIndexOrThrow("_moneyflag1"));
                    long j12 = cursor2.getLong(cursor2.getColumnIndexOrThrow("_moneytypeid1"));
                    String string2 = cursor2.getString(cursor2.getColumnIndexOrThrow("_name"));
                    long j13 = cursor2.getLong(cursor2.getColumnIndex("_projectID"));
                    long j14 = cursor2.getLong(cursor2.getColumnIndexOrThrow("_ymd"));
                    intent.putExtra("PROJECT_ID", j13);
                    intent.putExtra("ITEM_TYPE_NAME", string2);
                    intent.putExtra("SUM_MONEY", j11);
                    intent.putExtra("MONEY_FLAG", string);
                    intent.putExtra("MONEY_TYPE", (int) j12);
                    intent.putExtra("MAX_DAY", j14);
                    intent.putExtra("MIN_DAY", j14);
                }
                cursor2.close();
                intent.putExtra("ID_FOR_SQL", String.valueOf(j10));
                this.a.startActivityForResult(intent, 19);
                break;
        }
        return false;
    }

    public final void a_() {
        this.a.runOnUiThread(this.c);
    }

    public final void b() {
        if (a.a("IsAutoBackup", 0) > 0 && abt.b() == 0) {
            AlertCenter.a();
        }
        f();
    }
}
