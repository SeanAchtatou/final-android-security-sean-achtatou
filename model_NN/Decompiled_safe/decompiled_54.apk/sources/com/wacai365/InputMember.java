package com.wacai365;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.animation.Animation;
import com.wacai.data.l;

public class InputMember extends InputBasicItem {
    private l e;

    /* access modifiers changed from: protected */
    public final void a() {
        l lVar = new l();
        this.e = lVar;
        a(lVar);
        this.c.setText("");
        m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtContinuePrompt);
    }

    /* access modifiers changed from: protected */
    public final InputFilter[] b() {
        return new InputFilter[]{new InputFilter.LengthFilter(20)};
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.input_name);
        long longExtra = getIntent().getLongExtra("Record_Id", -1);
        if (longExtra > 0) {
            setTitle((int) C0000R.string.txtEditMember);
            this.e = l.a(longExtra);
        } else {
            this.e = new l();
        }
        a(this.e);
        c();
    }
}
