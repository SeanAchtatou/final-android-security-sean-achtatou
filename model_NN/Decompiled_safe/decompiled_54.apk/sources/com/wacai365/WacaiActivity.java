package com.wacai365;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

public class WacaiActivity extends Activity {
    protected boolean D = true;
    protected boolean E = true;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        if (this.D) {
            super.setTheme((int) C0000R.style.WaicaiTheme);
        }
        super.onCreate(bundle);
        if (this.E) {
            requestWindowFeature(7);
        }
        MyApp.a((Context) this);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.E) {
            getWindow().setFeatureInt(7, C0000R.layout.title_bar);
            TextView textView = (TextView) findViewById(16908310);
            if (textView != null) {
                textView.setText(getTitle());
            }
        }
    }
}
