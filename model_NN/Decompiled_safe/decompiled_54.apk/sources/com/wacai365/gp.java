package com.wacai365;

import android.content.DialogInterface;
import android.view.View;
import android.view.animation.Animation;
import com.wacai.a;

final class gp implements DialogInterface.OnClickListener {
    private /* synthetic */ boolean a;
    private /* synthetic */ MyShortcuts b;

    gp(MyShortcuts myShortcuts, boolean z) {
        this.b = myShortcuts;
        this.a = z;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case -1:
                if (!this.a) {
                    m.b(this.b.f);
                    break;
                }
                break;
        }
        if (a.a("IsAutoBackup", 0) > 0) {
            abt.b(this.b);
            return;
        }
        m.a(this.b, (Animation) null, 0, (View) null, (int) C0000R.string.txtSaveSuccess);
        this.b.finish();
    }
}
