package com.wacai365;

import android.content.Intent;
import android.view.View;

final class ec implements View.OnClickListener {
    private /* synthetic */ QueryReimburse a;

    ec(QueryReimburse queryReimburse) {
        this.a = queryReimburse;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.c)) {
            Intent intent = this.a.getIntent();
            intent.putExtra("QUERYINFO", this.a.b);
            this.a.setResult(-1, intent);
            this.a.finish();
        } else if (view.equals(this.a.e)) {
            this.a.setResult(0);
            this.a.finish();
        } else if (view.equals(this.a.d)) {
            QueryInfo unused = this.a.b = new QueryInfo();
            this.a.b.a(8);
            this.a.a();
        } else if (!view.equals(this.a.f)) {
        } else {
            if (this.a.s.getVisibility() == 8 && this.a.t.getVisibility() == 8 && this.a.u.getVisibility() == 8 && this.a.v.getVisibility() == 8) {
                this.a.s.setVisibility(0);
                this.a.t.setVisibility(0);
                this.a.u.setVisibility(0);
                this.a.v.setVisibility(0);
                this.a.f.setText((int) C0000R.string.txtHideQueryMore);
                Boolean unused2 = QueryReimburse.C = (Boolean) true;
                return;
            }
            this.a.s.setVisibility(8);
            this.a.t.setVisibility(8);
            this.a.u.setVisibility(8);
            this.a.v.setVisibility(8);
            this.a.f.setText((int) C0000R.string.txtQueryMore);
            Boolean unused3 = QueryReimburse.C = (Boolean) false;
        }
    }
}
