package com.wacai365;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import java.util.Date;

public final class in extends SimpleCursorAdapter {
    private String[] a;
    private int[] b;

    public in(Context context, int i, Cursor cursor, String[] strArr, int[] iArr) {
        super(context, i, cursor, strArr, iArr);
        this.b = iArr;
        this.a = strArr;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        View findViewById = view.findViewById(this.b[0]);
        if (findViewById != null) {
            String format = m.f.format(new Date(cursor.getLong(cursor.getColumnIndexOrThrow(this.a[0])) * 1000));
            if (format == null) {
                format = "";
            }
            setViewText((TextView) findViewById, format);
        }
        View findViewById2 = view.findViewById(this.b[1]);
        if (findViewById2 != null) {
            String string = cursor.getString(cursor.getColumnIndexOrThrow(this.a[1]));
            if (string == null) {
                string = "";
            }
            setViewText((TextView) findViewById2, string);
        }
    }
}
