package com.wacai365;

import android.content.Context;
import android.content.DialogInterface;

final class mi implements DialogInterface.OnClickListener {
    private /* synthetic */ Context a;

    mi(Context context) {
        this.a = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void
     arg types: [android.content.Context, java.lang.String, int, ?[OBJECT, ARRAY]]
     candidates:
      com.wacai365.m.a(android.view.animation.Animation, int, android.view.View, java.lang.String):android.view.animation.Animation
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, java.util.ArrayList, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, int[], android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.app.ProgressDialog, boolean, boolean, android.content.DialogInterface$OnClickListener):com.wacai365.tp
      com.wacai365.m.a(android.content.Context, long, android.content.DialogInterface$OnClickListener, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, long, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, boolean, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void */
    public final void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        if (i == -1) {
            try {
                if (m.d()) {
                    if (abw.c(abw.a)) {
                        abw.d(this.a);
                        return;
                    }
                    m.a(this.a, String.format(this.a.getString(C0000R.string.Alert_BACKUPFILE_NOT_EXIST), abw.a), false, (DialogInterface.OnClickListener) null);
                }
            } catch (Exception e) {
            }
        }
    }
}
