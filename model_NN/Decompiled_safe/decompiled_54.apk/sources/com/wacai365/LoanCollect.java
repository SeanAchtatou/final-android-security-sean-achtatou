package com.wacai365;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;

public class LoanCollect extends WacaiActivity {
    private Button a;
    private ListView b;
    private ListAdapter c;
    private long d;
    private ArrayList e = null;
    private int f = 0;
    private Hashtable g = new Hashtable();

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0087  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r12, java.lang.String r13) {
        /*
            r11 = this;
            r2 = 0
            com.wacai.e r0 = com.wacai.e.c()     // Catch:{ Exception -> 0x009a, all -> 0x0097 }
            android.database.sqlite.SQLiteDatabase r0 = r0.b()     // Catch:{ Exception -> 0x009a, all -> 0x0097 }
            r1 = 0
            android.database.Cursor r1 = r0.rawQuery(r13, r1)     // Catch:{ Exception -> 0x009a, all -> 0x0097 }
            if (r1 != 0) goto L_0x0016
            if (r1 == 0) goto L_0x0015
            r1.close()
        L_0x0015:
            return
        L_0x0016:
            r1.moveToFirst()     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
        L_0x0019:
            java.lang.String r0 = "_totalMoney"
            int r0 = r1.getColumnIndexOrThrow(r0)     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            long r2 = r1.getLong(r0)     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            java.lang.String r0 = "_orderno"
            int r0 = r1.getColumnIndexOrThrow(r0)     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            long r4 = r1.getLong(r0)     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            java.lang.String r0 = "_flag"
            int r0 = r1.getColumnIndexOrThrow(r0)     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            java.lang.String r6 = r1.getString(r0)     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            java.lang.String r0 = "%d"
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            r8 = 0
            r9 = 0
            java.lang.Long r9 = java.lang.Long.valueOf(r9)     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            java.lang.String r7 = java.lang.String.format(r0, r7)     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            java.util.Hashtable r0 = r11.g     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            java.lang.Object r0 = r0.get(r7)     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            com.wacai365.abo r0 = (com.wacai365.abo) r0     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            if (r0 != 0) goto L_0x0058
            com.wacai365.abo r0 = new com.wacai365.abo     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            r0.<init>(r11)     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
        L_0x0058:
            r0.b = r4     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            r0.a = r6     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            switch(r12) {
                case 4: goto L_0x0070;
                case 5: goto L_0x007e;
                case 6: goto L_0x008b;
                case 7: goto L_0x0091;
                default: goto L_0x005f;
            }     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
        L_0x005f:
            java.util.Hashtable r2 = r11.g     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            r2.put(r7, r0)     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            if (r0 != 0) goto L_0x0019
            if (r1 == 0) goto L_0x0015
            r1.close()
            goto L_0x0015
        L_0x0070:
            long r4 = r0.c     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            long r2 = r2 + r4
            r0.c = r2     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            goto L_0x005f
        L_0x0076:
            r0 = move-exception
            r0 = r1
        L_0x0078:
            if (r0 == 0) goto L_0x0015
            r0.close()
            goto L_0x0015
        L_0x007e:
            long r4 = r0.d     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            long r2 = r2 + r4
            r0.d = r2     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            goto L_0x005f
        L_0x0084:
            r0 = move-exception
        L_0x0085:
            if (r1 == 0) goto L_0x008a
            r1.close()
        L_0x008a:
            throw r0
        L_0x008b:
            long r4 = r0.e     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            long r2 = r2 + r4
            r0.e = r2     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            goto L_0x005f
        L_0x0091:
            long r4 = r0.f     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            long r2 = r2 + r4
            r0.f = r2     // Catch:{ Exception -> 0x0076, all -> 0x0084 }
            goto L_0x005f
        L_0x0097:
            r0 = move-exception
            r1 = r2
            goto L_0x0085
        L_0x009a:
            r0 = move-exception
            r0 = r2
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.LoanCollect.a(int, java.lang.String):void");
    }

    private void a(ArrayList arrayList) {
        Enumeration elements = this.g.elements();
        while (elements.hasMoreElements()) {
            abo abo = (abo) elements.nextElement();
            if (abo != null) {
                arrayList.add(abo);
            }
        }
        Collections.sort(arrayList, new vn(this));
    }

    private void a(boolean z) {
        StringBuffer stringBuffer = new StringBuffer(2000);
        String str = "select sum(a.money) as _totalMoney, e.moneytype as _moneytype, h.name as _name, h.flag as _flag, h.orderno as _orderno from TBL_LOAN a, TBL_ACCOUNTINFO e, TBL_MONEYTYPE h where a.debtid = e.id and e.moneytype = h.id and a.id in (select a.id as _id from TBL_LOAN a, TBL_ACCOUNTINFO b,  TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and a.type = %d and b.id = a.accountid and c.id = a.debtid and b.moneytype = d.id and c.moneytype = e.id and c.id = " + String.valueOf(this.d);
        Object[] objArr = new Object[1];
        objArr[0] = Integer.valueOf(z ? 0 : 1);
        stringBuffer.append(String.format(str, objArr));
        stringBuffer.append(" group by a.id) group by e.moneytype");
        a(z ? 5 : 4, stringBuffer.toString());
    }

    private void b(boolean z) {
        StringBuffer stringBuffer = new StringBuffer(2000);
        String str = "select sum(a.money) as _totalMoney, e.moneytype as _moneytype, h.name as _name, h.flag as _flag, h.orderno as _orderno from TBL_PAYBACK a, TBL_ACCOUNTINFO e, TBL_MONEYTYPE h where a.debtid = e.id and e.moneytype = h.id and a.id in (select a.id as _id from TBL_PAYBACK a, TBL_ACCOUNTINFO b,  TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and a.type = %d and b.id = a.accountid and c.id = a.debtid and b.moneytype = d.id and c.moneytype = e.id and c.id = " + String.valueOf(this.d);
        Object[] objArr = new Object[1];
        objArr[0] = Integer.valueOf(z ? 0 : 1);
        stringBuffer.append(String.format(str, objArr));
        stringBuffer.append(" group by a.id) group by e.moneytype");
        a(z ? 6 : 7, stringBuffer.toString());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.loan_collect);
        this.b = (ListView) findViewById(C0000R.id.list);
        this.d = getIntent().getLongExtra("LOANACCOUNTID", 0);
        this.a = (Button) findViewById(C0000R.id.btnCancel);
        this.a.setOnClickListener(new st(this));
        a(true);
        a(false);
        b(true);
        b(false);
        this.e = new ArrayList();
        a(this.e);
        if (this.e.size() > 0) {
            this.c = new rq(this, this, (abo) this.e.get(this.f));
            this.b.setAdapter(this.c);
        }
    }
}
