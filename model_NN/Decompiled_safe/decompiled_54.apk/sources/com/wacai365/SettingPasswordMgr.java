package com.wacai365;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.wacai.a.f;
import com.wacai.b;
import com.wacai.e;

public class SettingPasswordMgr extends WacaiActivity implements CompoundButton.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public Button a;
    /* access modifiers changed from: private */
    public Button b;
    private LinearLayout c;
    private LinearLayout d;
    private LinearLayout e;
    private EditText f = null;
    private EditText g = null;
    private CheckBox h = null;
    private String i = null;
    private Animation j;
    private boolean k = true;
    private View.OnClickListener l = new id(this);

    private void a(String str, boolean z) {
        b.m().a(z);
        if (z) {
            b.m().e(str);
        }
        b.m().l();
        m.b(e.c().a(), 2);
        finish();
    }

    private void a(boolean z) {
        this.c.setVisibility(z ? 0 : 8);
        this.d.setVisibility(z ? 0 : 8);
        this.e.setVisibility(z ? 0 : 8);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    static /* synthetic */ void b(SettingPasswordMgr settingPasswordMgr) {
        boolean isChecked = settingPasswordMgr.h.isChecked();
        String obj = settingPasswordMgr.f.getText().toString();
        String obj2 = settingPasswordMgr.g.getText().toString();
        if (isChecked && obj.length() <= 0) {
            settingPasswordMgr.j = m.a(settingPasswordMgr, settingPasswordMgr.j, (int) C0000R.anim.shake, settingPasswordMgr.f, (int) C0000R.string.txtEmptyPassword);
        } else if (!isChecked || obj.equals(obj2)) {
            settingPasswordMgr.i = f.a(obj);
            if (b.m().g().length() > 0) {
                Intent intent = new Intent(settingPasswordMgr, Login.class);
                intent.putExtra("login_hideinput", true);
                settingPasswordMgr.startActivityForResult(intent, 27);
                return;
            }
            settingPasswordMgr.a(settingPasswordMgr.i, isChecked);
        } else {
            settingPasswordMgr.j = m.a(settingPasswordMgr, settingPasswordMgr.j, (int) C0000R.anim.shake, settingPasswordMgr.g, (int) C0000R.string.txtConfimPassword);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    static /* synthetic */ void d(SettingPasswordMgr settingPasswordMgr) {
        Intent intent = new Intent(settingPasswordMgr, AccountRegister.class);
        intent.putExtra("Extra_AlertAccount", true);
        settingPasswordMgr.startActivityForResult(intent, 13);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 27:
                if (i3 == -1) {
                    a(this.i, this.h.isChecked());
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        a(z);
        if (this.k) {
            this.k = false;
        } else if (z) {
            boolean z2 = b.m().b() != null && b.m().b().length() > 0;
            if (b.m().p()) {
                m.a(this, getString(C0000R.string.txtSetLocalPSWTitleInfo), getString(C0000R.string.txtSetPSWWarning), (int) C0000R.string.txtRegisterUserName, (int) C0000R.string.txtIKnow, new ie(this));
            } else if (!z2) {
                m.a(this, getString(C0000R.string.txtSetLocalPSWTitleInfo), getString(C0000R.string.txtSetPSWWarning), C0000R.string.txtSetUserName, C0000R.string.txtIKnow, C0000R.string.txtRegisterUserName, new ib(this));
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.setting_local_password);
        this.a = (Button) findViewById(C0000R.id.btnOK);
        this.a.setOnClickListener(this.l);
        this.b = (Button) findViewById(C0000R.id.btnCancel);
        this.b.setOnClickListener(this.l);
        this.c = (LinearLayout) findViewById(C0000R.id.warningItem);
        this.d = (LinearLayout) findViewById(C0000R.id.passwordItem);
        this.e = (LinearLayout) findViewById(C0000R.id.passwordItem2);
        this.f = (EditText) findViewById(C0000R.id.etPassword);
        this.g = (EditText) findViewById(C0000R.id.etPasswordConfirm);
        this.h = (CheckBox) findViewById(C0000R.id.cbNeedLocalPassword);
        this.h.setOnCheckedChangeListener(this);
        boolean f2 = b.m().f();
        this.k = f2;
        this.h.setChecked(f2);
        this.f.setFilters(new InputFilter[]{new InputFilter.LengthFilter(16)});
        this.g.setFilters(new InputFilter[]{new InputFilter.LengthFilter(16)});
        a(f2);
    }
}
