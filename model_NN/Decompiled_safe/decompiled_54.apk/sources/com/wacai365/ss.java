package com.wacai365;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.Animation;

final class ss extends Handler {
    private /* synthetic */ WeiboHome a;

    ss(WeiboHome weiboHome) {
        this.a = weiboHome;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 300:
                m.a(this.a, (Animation) null, -1, (View) null, (int) C0000R.string.txtSucceedPrompt);
                this.a.d.dismiss();
                return;
            case 301:
                this.a.c.setChecked(!this.a.c.isChecked());
                m.a((Animation) null, 0, (View) null, (String) message.obj);
                this.a.d.dismiss();
                return;
            default:
                return;
        }
    }
}
