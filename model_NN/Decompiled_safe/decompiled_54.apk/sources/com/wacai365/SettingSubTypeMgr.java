package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import com.wacai.a;
import com.wacai.e;

public class SettingSubTypeMgr extends WacaiActivity {
    long a = -1;
    /* access modifiers changed from: private */
    public Button b;
    /* access modifiers changed from: private */
    public Button c;
    private ListView d;
    private SimpleCursorAdapter e;
    private View.OnClickListener f = new oy(this);

    /* access modifiers changed from: private */
    public void a(int i) {
        Cursor cursor = (Cursor) this.d.getItemAtPosition(i);
        long j = cursor != null ? cursor.getLong(cursor.getColumnIndexOrThrow("_id")) : 0;
        Intent intent = new Intent(this, InputType.class);
        intent.putExtra("Record_Id", j);
        intent.putExtra("Parent_Record_Id", this.a);
        startActivityForResult(intent, 0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        switch (menuItem.getItemId()) {
            case C0000R.id.idEdit /*2131493342*/:
                a(adapterContextMenuInfo.position);
                return false;
            default:
                return false;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.setting_item_list);
        this.b = (Button) findViewById(C0000R.id.btnAdd);
        this.b.setOnClickListener(this.f);
        this.c = (Button) findViewById(C0000R.id.btnCancel);
        this.c.setOnClickListener(this.f);
        this.d = (ListView) findViewById(C0000R.id.IOList);
        this.a = getIntent().getLongExtra("Record_Id", -1);
        String format = String.format("select id as _id, name as _name, star as _star, isdefault as _isdefault from TBL_OUTGOSUBTYPEINFO WHERE id >= %d and id < %d and enable = 1 ", Long.valueOf(this.a * 10000), Long.valueOf((this.a + 1) * 10000));
        Cursor rawQuery = e.c().b().rawQuery(a.a("BasicSortStyle", 0) == 0 ? format + " ORDER BY orderno ASC " : format + " ORDER BY pinyin ASC ", null);
        startManagingCursor(rawQuery);
        this.e = new yk(this, C0000R.layout.list_item_withstar, rawQuery, new String[]{"_name", "_star"}, new int[]{C0000R.id.listitem1, C0000R.id.btnStar}, "TBL_OUTGOSUBTYPEINFO");
        this.d.setAdapter((ListAdapter) this.e);
        this.d.setOnItemClickListener(new ow(this));
    }
}
