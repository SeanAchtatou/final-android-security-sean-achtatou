package com.wacai365;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import com.iflytek.f;
import java.util.Date;

public class Wacai365 extends Activity {
    /* access modifiers changed from: private */
    public ug a = null;
    private LinearLayout b = null;
    /* access modifiers changed from: private */
    public fy c = null;
    private boolean d = false;
    private long e = 0;
    /* access modifiers changed from: private */
    public sd f = new sd();
    private View.OnClickListener g = new as(this);

    static /* synthetic */ void a(Wacai365 wacai365) {
        wacai365.setContentView((int) C0000R.layout.mainscreen);
        wacai365.b = (LinearLayout) wacai365.findViewById(C0000R.id.baselayout);
        wacai365.a = new kk();
        wacai365.a.a(wacai365, wacai365.b, null, wacai365, true);
        ((Button) wacai365.findViewById(C0000R.id.button1)).setOnClickListener(wacai365.g);
        ((Button) wacai365.findViewById(C0000R.id.button2)).setOnClickListener(wacai365.g);
        ((Button) wacai365.findViewById(C0000R.id.button3)).setOnClickListener(wacai365.g);
        ((Button) wacai365.findViewById(C0000R.id.button4)).setOnClickListener(wacai365.g);
        ((Button) wacai365.findViewById(C0000R.id.button5)).setOnClickListener(wacai365.g);
        wacai365.f.d();
        new aq(wacai365).start();
    }

    private void a(String str, int i, int i2) {
        if (str != null && str.length() > 0) {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.setClassName(this, str);
            Intent intent2 = new Intent();
            intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
            intent2.putExtra("android.intent.extra.shortcut.NAME", getString(i));
            intent2.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this, i2));
            setResult(-1, intent2);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        switch (i) {
            case 4:
                if (i2 != -1) {
                    this.f.c();
                    finish();
                    break;
                } else {
                    this.f.e();
                    break;
                }
            case 5:
                if (-1 == i2) {
                    switch (intent.getIntExtra("SHORTCUTS_TYPE", 0)) {
                        case 0:
                            a(InputTrade.class.getName(), C0000R.string.txtWriteTitle, C0000R.drawable.outgoinput_shortcuts);
                            break;
                        case 1:
                            a(MyShortcuts.class.getName(), C0000R.string.txtShortcutsTitle, C0000R.drawable.scinput_shortcuts);
                            break;
                        case 2:
                        default:
                            a(getClass().getName(), C0000R.string.app_name, C0000R.drawable.icon);
                            break;
                        case 3:
                            a(InputNote.class.getName(), C0000R.string.txtNewNote, C0000R.drawable.noteinput_shortcuts);
                            break;
                    }
                }
                finish();
                break;
            case 27:
                if (i2 == -1) {
                    this.d = intent.getBooleanExtra("Restart_NoPwd", false);
                    if (!this.d) {
                        if (intent.getBooleanExtra("isneedupdate", false) && this.a != null) {
                            this.a.a(i, i2, intent);
                        }
                        this.f.e();
                        break;
                    } else {
                        this.f.c();
                        MyApp.a((Activity) this);
                        break;
                    }
                } else {
                    this.f.c();
                    finish();
                    break;
                }
            default:
                if (this.a != null) {
                    this.a.a(i, i2, intent);
                    break;
                } else {
                    return;
                }
        }
        if (this.c != null) {
            this.c.a(i, i2, intent);
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        if (this.a == null) {
            return false;
        }
        return this.a.a(menuItem);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        String action = getIntent().getAction();
        if (action != null && action.equals("android.intent.action.CREATE_SHORTCUT")) {
            startActivityForResult(new Intent(this, ShortcutsList.class), 5);
            return;
        }
        requestWindowFeature(1);
        this.f = abt.a(this, -1, new aw(this), new ax(this));
        this.f.a();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.i("Wacai365", "onDestroy");
        super.onDestroy();
        f a2 = f.a();
        if (a2 != null) {
            a2.e();
        }
        if (!this.d) {
            abt.a(this, true);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        long time = new Date().getTime();
        if (0 == this.e || 2000 < time - this.e) {
            this.e = time;
            m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtExitPromt);
        } else {
            finish();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.a != null && (this.a instanceof kk)) {
            ((kk) this.a).b();
        }
    }
}
