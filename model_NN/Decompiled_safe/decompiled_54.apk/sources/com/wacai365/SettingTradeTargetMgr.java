package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.wacai.a;
import com.wacai.e;

public class SettingTradeTargetMgr extends WacaiActivity {
    /* access modifiers changed from: private */
    public Button a;
    /* access modifiers changed from: private */
    public Button b;
    private ListView c;
    private ListAdapter d;
    /* access modifiers changed from: private */
    public boolean e;
    private View.OnClickListener f = new lp(this);

    /* access modifiers changed from: private */
    public void a(int i) {
        Cursor cursor = (Cursor) this.c.getItemAtPosition(i);
        long j = cursor != null ? cursor.getLong(cursor.getColumnIndexOrThrow("_id")) : 0;
        Intent intent = new Intent(this, InputTradeTarget.class);
        intent.putExtra("Record_Id", j);
        intent.putExtra("Is_Payer", this.e);
        startActivityForResult(intent, 0);
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        switch (menuItem.getItemId()) {
            case C0000R.id.idEdit /*2131493342*/:
                a(adapterContextMenuInfo.position);
                return false;
            default:
                return false;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.setting_item_list);
        this.e = getIntent().getBooleanExtra("Is_Payer", false);
        if (this.e) {
            setTitle((int) C0000R.string.payTargetMgr);
        }
        this.a = (Button) findViewById(C0000R.id.btnAdd);
        this.a.setOnClickListener(this.f);
        this.b = (Button) findViewById(C0000R.id.btnCancel);
        this.b.setOnClickListener(this.f);
        this.c = (ListView) findViewById(C0000R.id.IOList);
        Cursor rawQuery = e.c().b().rawQuery(a.a("BasicSortStyle", 0) == 0 ? "select id as _id, name as _name, star as _star from TBL_TRADETARGET where enable = 1 " + " ORDER BY orderno ASC " : "select id as _id, name as _name, star as _star from TBL_TRADETARGET where enable = 1 " + " ORDER BY pinyin ASC ", null);
        startManagingCursor(rawQuery);
        this.d = new yk(this, C0000R.layout.list_item_withstar, rawQuery, new String[]{"_name", "_star"}, new int[]{C0000R.id.listitem1, C0000R.id.btnStar}, "TBL_TRADETARGET");
        this.c.setAdapter(this.d);
        this.c.setOnItemClickListener(new lo(this));
    }
}
