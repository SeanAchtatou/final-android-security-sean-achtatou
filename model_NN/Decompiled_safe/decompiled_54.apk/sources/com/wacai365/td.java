package com.wacai365;

import android.content.DialogInterface;
import com.wacai.a.a;
import java.util.Date;

final class td implements DialogInterface.OnClickListener {
    private /* synthetic */ mz a;

    td(mz mzVar) {
        this.a = mzVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.c.setText(this.a.a.n.getResources().getStringArray(C0000R.array.Times)[i]);
        Date date = new Date();
        Date date2 = new Date();
        a.a(i, date, date2);
        this.a.a.t.b = a.a(date.getTime());
        this.a.a.t.c = a.a(date2.getTime());
        String format = m.c.format(date);
        String format2 = m.c.format(date2);
        this.a.a.d.setText(format);
        this.a.a.e.setText(format2);
        int unused = this.a.a.s = i;
        dialogInterface.dismiss();
    }
}
