package com.wacai365;

import android.content.DialogInterface;
import android.widget.CheckBox;

final class pr implements DialogInterface.OnClickListener {
    private /* synthetic */ CheckBox a;
    private /* synthetic */ int b;
    private /* synthetic */ AlertCenter c;

    pr(AlertCenter alertCenter, CheckBox checkBox, int i) {
        this.c = alertCenter;
        this.a = checkBox;
        this.b = i;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case -1:
                this.c.a(this.a.isChecked(), this.b);
                AlertCenter.c(this.c);
                return;
            default:
                dialogInterface.dismiss();
                return;
        }
    }
}
