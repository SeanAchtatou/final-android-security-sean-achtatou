package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.wacai.data.n;
import com.wacai.e;

public class ShortcutsMgr extends WacaiActivity {
    /* access modifiers changed from: private */
    public Button a;
    /* access modifiers changed from: private */
    public Button b;
    private ListView c;
    private TextView d;
    private ListAdapter e;
    private View.OnClickListener f = new fm(this);

    /* access modifiers changed from: private */
    public void a(int i) {
        Cursor cursor = (Cursor) this.c.getItemAtPosition(i);
        long j = cursor != null ? cursor.getLong(cursor.getColumnIndexOrThrow("_id")) : -1;
        if (j >= 0) {
            Intent intent = new Intent(this, InputShortcut.class);
            intent.putExtra("Extra_Id", j);
            startActivityForResult(intent, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        switch (menuItem.getItemId()) {
            case C0000R.id.idDelete /*2131493338*/:
                long j = 0;
                Cursor cursor = (Cursor) this.c.getItemAtPosition(adapterContextMenuInfo.position);
                if (cursor != null) {
                    j = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
                }
                n.g(j).f();
                cursor.requery();
                this.c.invalidateViews();
                return false;
            case C0000R.id.idEdit /*2131493342*/:
                a(adapterContextMenuInfo.position);
                return false;
            default:
                return false;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.setting_item_list);
        this.a = (Button) findViewById(C0000R.id.btnAdd);
        this.a.setOnClickListener(this.f);
        this.b = (Button) findViewById(C0000R.id.btnCancel);
        this.b.setOnClickListener(this.f);
        this.d = (TextView) findViewById(C0000R.id.id_listhint);
        this.c = (ListView) findViewById(C0000R.id.IOList);
        Cursor rawQuery = e.c().b().rawQuery("select a.id as _id, a.name as _name, a.type as _type from TBL_SHORTCUTSINFO a where isdelete <> 1", null);
        startManagingCursor(rawQuery);
        rawQuery.registerDataSetObserver(new fu(rawQuery, this.c, this.d, getResources().getString(C0000R.string.txtNoShortcutsHint)));
        this.e = new ce(this, this, C0000R.layout.list_item_withoutcheckable, rawQuery, new String[]{"_name", "_type", "_id"}, new int[]{C0000R.id.listitem1});
        this.c.setAdapter(this.e);
        this.c.setOnItemClickListener(new fl(this));
        this.c.setOnCreateContextMenuListener(new fk(this));
    }
}
