package com.wacai365;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.DownloadListener;

final class tx implements DownloadListener {
    private /* synthetic */ SoftIntroduce a;

    tx(SoftIntroduce softIntroduce) {
        this.a = softIntroduce;
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        try {
            this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (ActivityNotFoundException e) {
            Log.w("YourLogTag", "Couldn't find activity to view mimetype: " + str4);
        }
    }
}
