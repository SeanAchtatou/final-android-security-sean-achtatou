package com.wacai365;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import com.wacai.data.s;
import com.wacai.data.x;

public abstract class oj extends wg {
    static boolean a(Context context, s sVar) {
        if (context == null || sVar == null) {
            return false;
        }
        if (sVar.o() <= 0) {
            m.a(context, (Animation) null, 0, (View) null, (int) C0000R.string.txtInvalidMoney);
            return false;
        } else if (sVar.s().size() <= 0) {
            m.a(context, (Animation) null, 0, (View) null, (int) C0000R.string.noneMemberNotAllowed);
            return false;
        } else {
            if (sVar.s().size() > 1) {
                long o = sVar.o();
                for (int i = 1; i < sVar.s().size(); i++) {
                    o -= ((x) sVar.s().get(i)).b();
                }
                if (o != ((x) sVar.s().get(0)).b()) {
                    m.a(context, (Animation) null, 0, (View) null, (int) C0000R.string.txtMoneyExceedTotal);
                    return false;
                }
            }
            return true;
        }
    }

    public abstract long a();

    public abstract void a(long j);

    public abstract void a(String str);
}
