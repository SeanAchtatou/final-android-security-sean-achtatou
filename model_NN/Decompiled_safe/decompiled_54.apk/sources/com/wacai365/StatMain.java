package com.wacai365;

import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

public class StatMain extends WacaiActivity {
    static final int[] b = {1, 2, 3, 4, 5, 6, 7};
    private static fr[] c = {new fr(C0000R.string.txtStatOBType, C0000R.string.txtStatOBTypeDetail, "com.wacai365.StatView"), new fr(C0000R.string.txtStatIncomeTypes, C0000R.string.txtStatITypesDetail, "com.wacai365.StatView"), new fr(C0000R.string.txtStatByMonth, C0000R.string.txtStatByMonthDetail, "com.wacai365.StatView"), new fr(C0000R.string.txtStatIOByMonth, C0000R.string.txtStatIOByMonthDetail, "com.wacai365.StatView"), new fr(C0000R.string.txtStatBOByMonth, C0000R.string.txtStatBOByMonthDetail, "com.wacai365.StatView"), new fr(C0000R.string.txtStatByMember, C0000R.string.txtStatByMemberDetail, "com.wacai365.StatView"), new fr(C0000R.string.txtStatByProject, C0000R.string.txtStatByProjectDetail, "com.wacai365.StatView"), new fr(C0000R.string.txtStatByBalance, C0000R.string.txtStatByBalanceDetail, "com.wacai365.StatByBalance"), new fr(C0000R.string.txtStatByProperty, C0000R.string.txtStatByBalanceSheetDetail, "com.wacai365.StatByProperty")};
    ListView a;
    private AdapterView.OnItemClickListener d = new he(this);

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.setting_item_list_withoutbutton);
        this.a = (ListView) findViewById(C0000R.id.IOList);
        this.a.setAdapter((ListAdapter) new xw(this, (int) C0000R.layout.list_item_line2, c));
        this.a.setOnItemClickListener(this.d);
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(new hf(this));
    }
}
