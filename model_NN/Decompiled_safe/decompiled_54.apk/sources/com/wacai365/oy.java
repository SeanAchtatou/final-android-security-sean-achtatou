package com.wacai365;

import android.content.Intent;
import android.view.View;

final class oy implements View.OnClickListener {
    private /* synthetic */ SettingSubTypeMgr a;

    oy(SettingSubTypeMgr settingSubTypeMgr) {
        this.a = settingSubTypeMgr;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.b)) {
            Intent intent = new Intent(this.a, InputType.class);
            intent.putExtra("Parent_Record_Id", this.a.a);
            this.a.startActivityForResult(intent, 0);
        } else if (view.equals(this.a.c)) {
            this.a.finish();
        }
    }
}
