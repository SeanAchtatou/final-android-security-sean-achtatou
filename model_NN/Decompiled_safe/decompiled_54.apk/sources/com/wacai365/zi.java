package com.wacai365;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;

public final class zi extends BaseAdapter {
    private ArrayList a = null;
    private LayoutInflater b = null;
    private /* synthetic */ ChooseMember c;

    public zi(ChooseMember chooseMember, ArrayList arrayList, LayoutInflater layoutInflater) {
        this.c = chooseMember;
        this.a = arrayList;
        this.b = layoutInflater;
    }

    public final int getCount() {
        if (this.a != null) {
            return this.a.size();
        }
        return 0;
    }

    public final Object getItem(int i) {
        if (this.a == null) {
            return null;
        }
        if (i >= this.a.size()) {
            return null;
        }
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view != null ? view : this.b.inflate((int) C0000R.layout.choose_member_list_item, viewGroup, false);
        uz uzVar = (uz) this.a.get(i);
        if (uzVar == null) {
            return inflate;
        }
        View findViewById = inflate.findViewById(C0000R.id.name);
        if (findViewById != null) {
            String str = uzVar.c;
            if (str == null) {
                str = "";
            }
            ((TextView) findViewById).setText(str);
        }
        View findViewById2 = inflate.findViewById(C0000R.id.money);
        if (findViewById2 != null) {
            ((TextView) findViewById2).setText(String.format("%.2f", Double.valueOf(m.a(uzVar.b))));
        }
        return inflate;
    }
}
