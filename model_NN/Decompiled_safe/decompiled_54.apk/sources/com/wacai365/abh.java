package com.wacai365;

import android.database.Cursor;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;

final class abh implements View.OnCreateContextMenuListener {
    private /* synthetic */ kk a;

    abh(kk kkVar) {
        this.a = kkVar;
    }

    public final void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        long j;
        long j2;
        contextMenu.clear();
        MenuInflater menuInflater = this.a.a.getMenuInflater();
        Cursor cursor = (Cursor) this.a.b.getItemAtPosition(((AdapterView.AdapterContextMenuInfo) contextMenuInfo).position);
        if (cursor != null) {
            j2 = cursor.getLong(cursor.getColumnIndex("_reimburse"));
            j = cursor.getLong(cursor.getColumnIndex("_scheduleid"));
        } else {
            j = 0;
            j2 = 0;
        }
        if (j2 == 1 && j == 0) {
            menuInflater.inflate(C0000R.menu.reimburse_list_context, contextMenu);
        } else {
            menuInflater.inflate(C0000R.menu.white_list_context, contextMenu);
        }
    }
}
