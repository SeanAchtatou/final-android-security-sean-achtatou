package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.SimpleCursorAdapter;
import com.wacai.a;
import com.wacai.e;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ChooseOutgoType extends WacaiActivity {
    private ListView a;
    /* access modifiers changed from: private */
    public LinearLayout b;
    /* access modifiers changed from: private */
    public ExpandableListView c;
    /* access modifiers changed from: private */
    public EditText d;
    /* access modifiers changed from: private */
    public ListView e;
    private LinearLayout f;
    private RadioGroup g;
    /* access modifiers changed from: private */
    public Button h;
    /* access modifiers changed from: private */
    public Button i;
    private List j = new ArrayList();
    /* access modifiers changed from: private */
    public List k = new ArrayList();
    private int l = 0;
    private View.OnClickListener m = new wv(this);
    private RadioGroup.OnCheckedChangeListener n = new ww(this);

    /* access modifiers changed from: private */
    public void a() {
        this.l = 0;
        if (this.a == null) {
            this.a = new ListView(this);
            this.a.setDivider(new ColorDrawable(getResources().getColor(C0000R.color.listDivider)));
            this.a.setDividerHeight(1);
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        a(arrayList, arrayList2);
        this.a.setAdapter((ListAdapter) new kh(this, "TBL_OUTGOSUBTYPEINFO", new int[]{C0000R.string.myFavorites, C0000R.string.myFrequences}, arrayList, arrayList2, new xf(this), new xg(this)));
        this.a.setOnItemClickListener(new xd(this));
        this.a.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.f.removeAllViews();
        this.f.addView(this.a);
    }

    static void a(ListView listView) {
        if (listView != null) {
            CursorAdapter cursorAdapter = (CursorAdapter) listView.getAdapter();
            if (!(cursorAdapter == null || cursorAdapter.getCursor() == null)) {
                cursorAdapter.getCursor().requery();
            }
            listView.invalidateViews();
        }
    }

    static /* synthetic */ void a(ChooseOutgoType chooseOutgoType, long j2) {
        if (j2 > 0) {
            chooseOutgoType.getIntent().putExtra("Outgo_Sel_Id", j2);
            chooseOutgoType.setResult(-1, chooseOutgoType.getIntent());
            chooseOutgoType.finish();
        }
    }

    static /* synthetic */ void a(ChooseOutgoType chooseOutgoType, long j2, boolean z) {
        if (j2 > 0 && chooseOutgoType.k != null) {
            for (List it : chooseOutgoType.k) {
                Iterator it2 = it.iterator();
                while (true) {
                    if (it2.hasNext()) {
                        Map map = (Map) it2.next();
                        if (j2 == Long.parseLong((String) map.get("ID"))) {
                            map.put("star", z ? "1" : "0");
                            return;
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(List list, List list2) {
        if (list != null && list2 != null) {
            kh.a("select a.id, a.name, a.star from TBL_OUTGOSUBTYPEINFO a, TBL_OUTGOMAINTYPEINFO b where a.enable = 1 and b.enable=1 and a.id/10000=b.id and a.star = 1 order by a.pinyin ASC", list);
            kh.a("select a.id, a.name, a.star from TBL_OUTGOSUBTYPEINFO a, TBL_OUTGOMAINTYPEINFO b where a.enable = 1 and b.enable=1 and a.id/10000=b.id and a.star = 0 and a.refcount > 0 order by a.refcount DESC, a.pinyin ASC limit 0, 10", list2);
            this.a.invalidateViews();
        }
    }

    static /* synthetic */ void d(ChooseOutgoType chooseOutgoType) {
        if (chooseOutgoType.b == null) {
            chooseOutgoType.l = 1;
            chooseOutgoType.b = (LinearLayout) ((LayoutInflater) chooseOutgoType.getSystemService("layout_inflater")).inflate((int) C0000R.layout.outgo_type_extendlist, (ViewGroup) null);
            new abn(chooseOutgoType).execute(0);
            chooseOutgoType.d = (EditText) chooseOutgoType.b.findViewById(C0000R.id.etSearch);
            chooseOutgoType.d.addTextChangedListener(new xe(chooseOutgoType));
            ((ImageButton) chooseOutgoType.b.findViewById(C0000R.id.btnClear)).setOnClickListener(new xb(chooseOutgoType));
        } else {
            chooseOutgoType.l = chooseOutgoType.d != null && chooseOutgoType.d.getText() != null && chooseOutgoType.d.getText().length() > 0 ? 2 : 1;
            if (chooseOutgoType.l == 1 && chooseOutgoType.c != null) {
                chooseOutgoType.c.invalidateViews();
            }
            if (chooseOutgoType.l == 2 && chooseOutgoType.e != null) {
                a(chooseOutgoType.e);
            }
        }
        chooseOutgoType.b.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        chooseOutgoType.f.removeAllViews();
        chooseOutgoType.f.addView(chooseOutgoType.b);
    }

    static /* synthetic */ void e(ChooseOutgoType chooseOutgoType) {
        if (chooseOutgoType.d.getText().length() == 0) {
            chooseOutgoType.l = 1;
            new abn(chooseOutgoType).execute(0);
            return;
        }
        chooseOutgoType.l = 2;
        chooseOutgoType.c.setVisibility(8);
        Cursor d2 = m.d(chooseOutgoType.d.getText().toString());
        if (d2 != null) {
            chooseOutgoType.startManagingCursor(d2);
        }
        if (chooseOutgoType.e == null) {
            chooseOutgoType.e = new ListView(chooseOutgoType);
            chooseOutgoType.e.setDivider(new ColorDrawable(chooseOutgoType.getResources().getColor(C0000R.color.listDivider)));
            chooseOutgoType.e.setDividerHeight(1);
            chooseOutgoType.e.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
            chooseOutgoType.b.addView(chooseOutgoType.e);
            ChooseOutgoType chooseOutgoType2 = chooseOutgoType;
            chooseOutgoType.e.setAdapter((ListAdapter) new yk(chooseOutgoType2, C0000R.layout.list_item_withstar, d2, new String[]{"_name", "_star"}, new int[]{C0000R.id.listitem1, C0000R.id.btnStar}, "TBL_OUTGOSUBTYPEINFO"));
            chooseOutgoType.e.setOnItemClickListener(new xc(chooseOutgoType));
            return;
        }
        ((SimpleCursorAdapter) chooseOutgoType.e.getAdapter()).changeCursor(d2);
        chooseOutgoType.e.setVisibility(0);
    }

    static /* synthetic */ void j(ChooseOutgoType chooseOutgoType) {
        Cursor cursor;
        ArrayList arrayList;
        Cursor cursor2;
        chooseOutgoType.j.clear();
        chooseOutgoType.k.clear();
        String str = a.a("BasicSortStyle", 0) == 0 ? " order by orderno ASC " : " order by pinyin ASC ";
        try {
            cursor = e.c().b().rawQuery("select * from TBL_OUTGOMAINTYPEINFO where enable = 1 group by id " + str, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        HashMap hashMap = new HashMap();
                        chooseOutgoType.j.add(hashMap);
                        hashMap.put("NAME", cursor.getString(cursor.getColumnIndexOrThrow("name")));
                        int i2 = cursor.getInt(cursor.getColumnIndexOrThrow("id"));
                        String str2 = String.format("select * from tbl_outgosubtypeinfo where id >= %d and id < %d and enable = 1", Long.valueOf((long) (i2 * 10000)), Long.valueOf((long) ((i2 + 1) * 10000))) + str;
                        arrayList = new ArrayList();
                        try {
                            cursor2 = e.c().b().rawQuery(str2, null);
                            if (cursor2 != null) {
                                try {
                                    if (cursor2.moveToFirst()) {
                                        for (int i3 = 0; i3 < cursor2.getCount(); i3++) {
                                            HashMap hashMap2 = new HashMap();
                                            arrayList.add(hashMap2);
                                            hashMap2.put("NAME", cursor2.getString(cursor2.getColumnIndexOrThrow("name")));
                                            hashMap2.put("ID", cursor2.getString(cursor2.getColumnIndexOrThrow("id")));
                                            hashMap2.put("star", cursor2.getString(cursor2.getColumnIndexOrThrow("star")));
                                            cursor2.moveToNext();
                                        }
                                        chooseOutgoType.k.add(arrayList);
                                        if (cursor2 != null) {
                                            cursor2.close();
                                        }
                                    }
                                } catch (Throwable th) {
                                    th = th;
                                }
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            cursor2 = null;
                        }
                        try {
                            chooseOutgoType.k.add(arrayList);
                            if (cursor2 != null) {
                                cursor2.close();
                            }
                        } catch (Throwable th3) {
                            th = th3;
                        }
                    } while (cursor.moveToNext());
                    if (cursor != null) {
                        cursor.close();
                        return;
                    }
                    return;
                }
            }
            if (cursor != null) {
                cursor.close();
                return;
            }
            return;
            chooseOutgoType.k.add(arrayList);
            if (cursor2 != null) {
                cursor2.close();
            }
            throw th;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        } catch (Throwable th4) {
            th = th4;
            cursor = null;
        }
    }

    static /* synthetic */ void k(ChooseOutgoType chooseOutgoType) {
        chooseOutgoType.c.setAdapter(new xa(chooseOutgoType, chooseOutgoType, chooseOutgoType.j, C0000R.layout.simple_expandable_list_item_1, new String[]{"NAME"}, new int[]{C0000R.id.listitem1}, chooseOutgoType.k, C0000R.layout.simple_expandable_list_item_1_withstar, new String[]{"NAME"}, new int[]{C0000R.id.listitem1}));
        chooseOutgoType.c.setOnChildClickListener(new ir(chooseOutgoType));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 != 33) {
            return;
        }
        if (this.l == 0) {
            a();
        } else if (this.l == 1 && this.c != null) {
            new abn(this).execute(0);
        } else if (this.l == 2 && this.e != null) {
            ((CursorAdapter) this.e.getAdapter()).getCursor().requery();
            this.e.invalidateViews();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.outgo_type_list);
        this.f = (LinearLayout) findViewById(C0000R.id.baselayout);
        this.g = (RadioGroup) findViewById(C0000R.id.rgHeader);
        this.g.setOnCheckedChangeListener(this.n);
        this.i = (Button) findViewById(C0000R.id.btnCancel);
        this.i.setOnClickListener(this.m);
        this.h = (Button) findViewById(C0000R.id.btnTypeMgr);
        this.h.setOnClickListener(this.m);
        a();
    }
}
