package com.wacai365;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public final class tl extends ArrayAdapter {
    private Context a = null;
    private /* synthetic */ hd b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public tl(hd hdVar, Context context, int i, String[] strArr) {
        super(context, i, strArr);
        this.b = hdVar;
        this.a = context;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? ((LayoutInflater) this.a.getSystemService("layout_inflater")).inflate((int) C0000R.layout.list_item_1_small_withcheckbox, (ViewGroup) null) : view;
        String str = (String) getItem(i);
        if (str != null) {
            ((TextView) inflate.findViewById(C0000R.id.listitem1)).setText(str);
            ((CheckBox) inflate.findViewById(C0000R.id.checkitem)).setChecked(this.b.b[i]);
        }
        return inflate;
    }
}
