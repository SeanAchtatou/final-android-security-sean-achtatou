package com.wacai365;

import android.webkit.WebSettings;
import android.webkit.WebView;

final class dr extends Thread {
    final /* synthetic */ WeiboConnect a;

    dr(WeiboConnect weiboConnect) {
        this.a = weiboConnect;
    }

    public final void run() {
        WebView unused = this.a.e = (WebView) this.a.findViewById(C0000R.id.webView);
        WebSettings settings = this.a.e.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setSaveFormData(true);
        settings.setSavePassword(false);
        settings.setLightTouchEnabled(true);
        try {
            String unused2 = this.a.f = this.a.b.a();
            this.a.runOnUiThread(new qp(this));
        } catch (Exception e) {
            WeiboConnect.b(this.a, e.getMessage());
        }
    }
}
