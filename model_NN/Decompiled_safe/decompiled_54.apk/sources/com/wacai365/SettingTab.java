package com.wacai365;

import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

public class SettingTab extends WacaiActivity {
    private static fr[] b = {new fr(C0000R.string.txtSettingAccountMgr, C0000R.string.txtSettingAccountMgrDetail, "com.wacai365.SettingAccountMgr"), new fr(C0000R.string.loanAccountManage, C0000R.string.loanAccountManageDetail, "com.wacai365.SettingLoanAccountMgr"), new fr(C0000R.string.txtSettingMemberMgr, C0000R.string.txtSettingMemberMgrDetail, "com.wacai365.SettingMemberMgr"), new fr(C0000R.string.txtSettingProjectMgr, C0000R.string.txtSettingProjectMgrDetail, "com.wacai365.SettingProjectMgr"), new fr(C0000R.string.txtSettingMainTypeMgr, C0000R.string.txtSettingMainTypeMgrDetail, "com.wacai365.SettingMainTypeMgr"), new fr(C0000R.string.txtSettingIncomeMainTypeMgr, C0000R.string.txtSettingIncomeMainTypeMgrDetail, "com.wacai365.SettingIncomeMainTypeMgr"), new fr(C0000R.string.txtSettingReimburseMgr, C0000R.string.txtSettingReimburseMgrDetail, "com.wacai365.SettingReimburseMgr"), new fr(C0000R.string.tradeTargetMgr, C0000R.string.tradeTargetMgrDetail, "com.wacai365.SettingTradeTargetMgr"), new fr(C0000R.string.txtShortcutsMgr, C0000R.string.txtShortcutsMgrDetail, "com.wacai365.ShortcutsMgr"), new fr(C0000R.string.txtSettingBudgetMgr, C0000R.string.txtSettingBudgetMgrDetail, "com.wacai365.SettingBudgetMgr"), new fr(C0000R.string.txtPersonalSettingMgr, C0000R.string.txtPersonalSettingMgrDetail, "com.wacai365.PersonalSettingMgr"), new fr(C0000R.string.txtSettingPasswordMgr, C0000R.string.txtSettingPasswordMgrDetail, "com.wacai365.SettingPasswordMgr")};
    ListView a;
    private AdapterView.OnItemClickListener c = new hj(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.setting_item_list_withoutbutton);
        this.a = (ListView) findViewById(C0000R.id.IOList);
        this.a.setAdapter((ListAdapter) new xw(this, (int) C0000R.layout.list_item_line2, b));
        this.a.setOnItemClickListener(this.c);
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(new hi(this));
    }
}
