package com.wacai365;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

public class WeiboSettingList extends WacaiActivity {
    /* access modifiers changed from: private */
    public static final int[] c = {C0000R.drawable.sina_logo, C0000R.drawable.tencent_logo};
    private ListView a;
    private AdapterView.OnItemClickListener b = new d(this);

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 31 && i2 == -1) {
            setResult(-1);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.weibo_setting_list);
        this.a = (ListView) findViewById(C0000R.id.listView);
        this.a.setOnItemClickListener(this.b);
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(new a(this));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.a.setAdapter((ListAdapter) new tk(this, this));
        this.a.invalidate();
        super.onResume();
    }
}
