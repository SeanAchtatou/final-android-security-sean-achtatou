package com.wacai365;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public final class ce extends SimpleCursorAdapter {
    private String[] a;
    private int[] b;
    private /* synthetic */ ShortcutsMgr c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ce(ShortcutsMgr shortcutsMgr, Context context, int i, Cursor cursor, String[] strArr, int[] iArr) {
        super(context, i, cursor, strArr, iArr);
        this.c = shortcutsMgr;
        this.b = iArr;
        this.a = strArr;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        View findViewById = view.findViewById(this.b[0]);
        if (findViewById != null) {
            String string = cursor.getString(cursor.getColumnIndexOrThrow(this.a[0]));
            if (string == null) {
                string = "";
            }
            setViewText((TextView) findViewById, string);
        }
    }
}
