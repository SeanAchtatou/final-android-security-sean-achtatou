package com.wacai365;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.animation.Animation;
import com.wacai.data.f;

public class InputProject extends InputBasicItem {
    private f e;

    /* access modifiers changed from: protected */
    public final void a() {
        f fVar = new f();
        this.e = fVar;
        a(fVar);
        this.c.setText("");
        m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtContinuePrompt);
    }

    /* access modifiers changed from: protected */
    public final InputFilter[] b() {
        return new InputFilter[]{new InputFilter.LengthFilter(20)};
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.input_name);
        long longExtra = getIntent().getLongExtra("Record_Id", -1);
        if (longExtra > 0) {
            setTitle((int) C0000R.string.txtEditProject);
            this.e = f.a(longExtra);
        } else {
            this.e = new f();
        }
        a(this.e);
        c();
    }
}
