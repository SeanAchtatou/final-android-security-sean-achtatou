package com.wacai365;

import android.widget.RadioGroup;

final class hr implements RadioGroup.OnCheckedChangeListener {
    private /* synthetic */ StatByBalance a;

    hr(StatByBalance statByBalance) {
        this.a = statByBalance;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.StatByBalance.a(com.wacai365.StatByBalance, java.lang.Boolean):java.lang.Boolean
     arg types: [com.wacai365.StatByBalance, int]
     candidates:
      com.wacai365.StatByBalance.a(com.wacai365.StatByBalance, com.wacai365.fy):com.wacai365.fy
      com.wacai365.StatByBalance.a(com.wacai365.StatByBalance, java.util.Hashtable):java.util.Hashtable
      com.wacai365.StatByBalance.a(com.wacai365.StatByBalance, java.lang.Boolean):java.lang.Boolean */
    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case C0000R.id.btnQuery /*2131492984*/:
                Boolean unused = this.a.i = (Boolean) false;
                this.a.d();
                return;
            case C0000R.id.btnRemoteQuery /*2131492985*/:
                Boolean unused2 = this.a.i = (Boolean) true;
                StatByBalance.f(this.a);
                return;
            default:
                return;
        }
    }
}
