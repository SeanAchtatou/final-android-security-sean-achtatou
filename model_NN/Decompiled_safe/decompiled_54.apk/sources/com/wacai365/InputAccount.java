package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.animation.Animation;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.b.c;
import com.wacai.b.f;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.o;
import com.wacai.data.aa;
import com.wacai.data.e;
import com.wacai.data.h;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

public class InputAccount extends InputBasicItem {
    private LinearLayout A = null;
    private LinearLayout B = null;
    /* access modifiers changed from: private */
    public h C;
    /* access modifiers changed from: private */
    public fy F = null;
    private ArrayList G = new ArrayList();
    private QueryInfo H;
    DialogInterface.OnClickListener e = new ii(this);
    private View f = null;
    private CheckBox g = null;
    /* access modifiers changed from: private */
    public CheckBox h = null;
    /* access modifiers changed from: private */
    public TextView i = null;
    /* access modifiers changed from: private */
    public TextView j = null;
    /* access modifiers changed from: private */
    public TextView k = null;
    /* access modifiers changed from: private */
    public TextView l = null;
    /* access modifiers changed from: private */
    public TextView m = null;
    /* access modifiers changed from: private */
    public TextView n = null;
    /* access modifiers changed from: private */
    public TextView o = null;
    /* access modifiers changed from: private */
    public TextView p = null;
    /* access modifiers changed from: private */
    public int[] q = null;
    /* access modifiers changed from: private */
    public int[] r = null;
    private LinearLayout s = null;
    private LinearLayout t = null;
    private LinearLayout u = null;
    private LinearLayout v = null;
    private LinearLayout w = null;
    private LinearLayout x = null;
    private LinearLayout y = null;
    private LinearLayout z = null;

    /* access modifiers changed from: private */
    public void b(int i2) {
        if (7 == i2) {
            this.p.setText(e.a(14L));
            if (this.C != null) {
                this.C.b(14);
            }
            m.a(this, (Animation) null, -1, (View) null, (int) C0000R.string.accountVirtualPrompt);
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.C.d()) {
            m.a(this.v, (int) C0000R.drawable.bg_item_editline);
            this.w.setVisibility(0);
            this.x.setVisibility(0);
            this.y.setVisibility(0);
            this.z.setVisibility(0);
            this.A.setVisibility(0);
            if (this.C.i()) {
                this.B.setVisibility(0);
                m.a(this.A, (int) C0000R.drawable.bg_item_editline);
                return;
            }
            m.a(this.A, (int) C0000R.drawable.bg_item_editline);
            this.B.setVisibility(4);
            return;
        }
        m.a(this.v, (int) C0000R.drawable.bg_item_editline);
        this.w.setVisibility(4);
        this.x.setVisibility(4);
        this.y.setVisibility(4);
        this.z.setVisibility(4);
        this.A.setVisibility(4);
        this.B.setVisibility(4);
    }

    private void h() {
        String str;
        String str2;
        this.n.setText(this.C.j());
        this.o.setText(aa.a("TBL_ACCOUNTTYPE", "name", String.valueOf(this.C.e())));
        this.p.setText(e.a((long) this.C.g()));
        if (this.C.d()) {
            String b = m.b(m.a(this.C.x(), this.C.b(), this.C.a()));
            str = b;
            str2 = m.b(this.C.a());
        } else {
            this.C.b(new Date().getTime() / 1000);
            str = "0.00";
            str2 = "0.00";
        }
        this.k.setText(str2);
        this.j.setText(str);
        m.c(this.C.b() * 1000, this.l);
        m.b(this.C.b() * 1000, this.m);
        String str3 = "0.00";
        if (this.C.i()) {
            str3 = m.b(this.C.h());
        }
        this.i.setText(str3);
        this.g.setChecked(this.C.d());
        this.h.setChecked(this.C.i());
        g();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(com.wacai.b.c, boolean):void
     arg types: [com.wacai.b.c, int]
     candidates:
      com.wacai365.ft.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(com.wacai.b.c, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void l(InputAccount inputAccount) {
        inputAccount.G.clear();
        ft ftVar = new ft(inputAccount);
        inputAccount.F = ftVar;
        ftVar.a(new ij(inputAccount));
        c cVar = new c(ftVar);
        ft.a(cVar, true);
        l lVar = new l();
        StringBuffer stringBuffer = new StringBuffer(100);
        inputAccount.H.a(stringBuffer, 6);
        lVar.a(m.b(stringBuffer.toString()));
        cVar.a((o) lVar);
        f fVar = new f();
        fVar.a(true);
        fVar.a(inputAccount.G);
        cVar.a((o) fVar);
        ftVar.a(cVar);
        ftVar.b(C0000R.string.txtRectificationProgress, C0000R.string.txtPreparing);
        ftVar.a(false, true);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.n.setText("");
        h hVar = new h();
        this.C = hVar;
        a(hVar);
        h();
        m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtContinuePrompt);
    }

    /* access modifiers changed from: protected */
    public final InputFilter[] b() {
        return new InputFilter[]{new InputFilter.LengthFilter(20)};
    }

    /* access modifiers changed from: protected */
    public final void c() {
        super.c();
        this.n = (TextView) findViewById(C0000R.id.id_name);
        this.o = (TextView) findViewById(C0000R.id.tvAccountType);
        this.p = (TextView) findViewById(C0000R.id.tvMoneyType);
        this.g = (CheckBox) findViewById(C0000R.id.cbSetBalance);
        this.h = (CheckBox) findViewById(C0000R.id.cbSetWarningBalance);
        this.i = (TextView) findViewById(C0000R.id.id_warningbalance);
        this.s = (LinearLayout) findViewById(C0000R.id.layout_name);
        this.v = (LinearLayout) findViewById(C0000R.id.enableBalance);
        this.w = (LinearLayout) findViewById(C0000R.id.layout_nowbalance);
        this.x = (LinearLayout) findViewById(C0000R.id.layout_primebalance);
        this.y = (LinearLayout) findViewById(C0000R.id.layout_balancetime);
        this.z = (LinearLayout) findViewById(C0000R.id.layout_balancedate);
        this.A = (LinearLayout) findViewById(C0000R.id.enableWarningBalance);
        this.B = (LinearLayout) findViewById(C0000R.id.layout_warningbalance);
        this.t = (LinearLayout) findViewById(C0000R.id.layoutAccountType);
        this.u = (LinearLayout) findViewById(C0000R.id.layoutMoneyType);
        this.j = (TextView) findViewById(C0000R.id.id_nowbalance);
        this.k = (TextView) findViewById(C0000R.id.id_primebalance);
        this.l = (TextView) findViewById(C0000R.id.id_balancedate);
        this.m = (TextView) findViewById(C0000R.id.id_balancetime);
        this.f = findViewById(C0000R.id.btnRectificationBalance);
        if (0 > getIntent().getLongExtra("Record_Id", -1)) {
            this.f.setVisibility(8);
        }
        this.H = new QueryInfo();
        this.H.a(4);
        this.t.setOnClickListener(new ge(this));
        this.u.setOnClickListener(new gf(this));
        this.s.setOnClickListener(new gg(this));
        this.g.setOnCheckedChangeListener(new gh(this));
        this.h.setOnCheckedChangeListener(new gi(this));
        this.x.setOnClickListener(new gj(this));
        this.B.setOnClickListener(new gk(this));
        this.z.setOnClickListener(new gl(this));
        this.y.setOnClickListener(new gc(this));
        this.f.setOnClickListener(new ih(this));
        h();
        b(this.C.e());
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        String a = a(this.n);
        if (a == null || a.trim().length() <= 0) {
            return false;
        }
        if (!this.C.d()) {
            this.C.a(0L);
            this.C.b(false);
            this.k.setText("0.00");
        }
        if (!this.C.i()) {
            this.C.c(0L);
            this.i.setText("0.00");
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void e() {
        this.j.setText(m.b(m.a(this.C.x(), this.C.b(), this.C.a())));
    }

    public final void f() {
        Cursor cursor;
        int size = this.G.size();
        try {
            Cursor rawQuery = com.wacai.e.c().b().rawQuery("select * from TBL_ACCOUNTINFO where uuid = '" + this.C.B() + "'", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        this.C.b(rawQuery.getString(rawQuery.getColumnIndexOrThrow("name")));
                        this.C.a(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("type")));
                        this.C.b(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("moneytype")));
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            runOnUiThread(new Cif(this));
            if (rawQuery != null) {
                rawQuery.close();
            }
            String j2 = this.C.j();
            int i2 = 0;
            while (i2 < size) {
                Hashtable hashtable = (Hashtable) this.G.get(i2);
                if (hashtable == null || !j2.equals((String) hashtable.get("TAG_LABLE"))) {
                    i2++;
                } else {
                    String str = (String) hashtable.get("TAG_FIRST");
                    double doubleValue = Double.valueOf(str).doubleValue();
                    if (Double.valueOf(this.j.getText().toString()).doubleValue() != doubleValue) {
                        runOnUiThread(new ik(this, str, doubleValue));
                        return;
                    }
                    return;
                }
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (this.F != null) {
            if (i3 == -1) {
                this.F.a(i2, i3, intent);
                this.F = null;
                return;
            }
            this.F = null;
        }
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1) {
            switch (i2) {
                case 18:
                    if (intent != null) {
                        String stringExtra = intent.getStringExtra("Text_String");
                        String trim = stringExtra == null ? "" : stringExtra.trim();
                        this.C.b(trim);
                        this.n.setText(trim);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.input_account);
        long longExtra = getIntent().getLongExtra("Record_Id", -1);
        if (longExtra > 0) {
            setTitle((int) C0000R.string.txtEditAccount);
            this.C = h.d(longExtra);
        } else {
            this.C = new h();
        }
        a(this.C);
        c();
    }
}
