package com.wacai365;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.a.a;
import com.wacai.b;
import com.wacai.data.e;
import com.wacai.data.f;
import com.wacai.data.h;
import com.wacai.data.p;
import com.wacai.data.y;
import java.util.Date;

public class qc extends yi {
    protected rk a = null;
    /* access modifiers changed from: private */
    public TextView b;
    /* access modifiers changed from: private */
    public TextView c;
    /* access modifiers changed from: private */
    public TextView d;
    /* access modifiers changed from: private */
    public TextView e;
    /* access modifiers changed from: private */
    public TextView f;
    /* access modifiers changed from: private */
    public TextView g;
    /* access modifiers changed from: private */
    public TextView h;
    /* access modifiers changed from: private */
    public TextView i;
    /* access modifiers changed from: private */
    public TextView j;
    /* access modifiers changed from: private */
    public TextView k;
    /* access modifiers changed from: private */
    public TextView l;
    /* access modifiers changed from: private */
    public TextView m;
    private LinearLayout r;
    /* access modifiers changed from: private */
    public int[] s = null;
    /* access modifiers changed from: private */
    public int[] t = null;
    /* access modifiers changed from: private */
    public int[] u = null;
    /* access modifiers changed from: private */
    public int[] v = null;
    /* access modifiers changed from: private */
    public int[] w = null;
    /* access modifiers changed from: private */
    public int[] x = null;
    /* access modifiers changed from: private */
    public int y = -1;
    /* access modifiers changed from: private */
    public QueryInfo z;

    public qc(QueryInfo queryInfo) {
        this.z = queryInfo;
    }

    public final void a(Context context, LinearLayout linearLayout, LinearLayout linearLayout2, Activity activity, boolean z2) {
        super.a(context, linearLayout, linearLayout2, activity, true);
        LinearLayout linearLayout3 = (LinearLayout) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) C0000R.layout.query_outgo_setting_list, (ViewGroup) null).findViewById(C0000R.id.mainlayout);
        linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.removeAllViews();
        linearLayout.addView(linearLayout3);
        this.b = (TextView) linearLayout3.findViewById(C0000R.id.viewBeninMoney);
        this.c = (TextView) linearLayout3.findViewById(C0000R.id.viewEndMoney);
        this.d = (TextView) linearLayout3.findViewById(C0000R.id.viewShortcutsDate);
        this.e = (TextView) linearLayout3.findViewById(C0000R.id.viewBeginDate);
        this.f = (TextView) linearLayout3.findViewById(C0000R.id.viewEndDate);
        this.g = (TextView) linearLayout3.findViewById(C0000R.id.viewMainType);
        this.h = (TextView) linearLayout3.findViewById(C0000R.id.viewSubType);
        this.i = (TextView) linearLayout3.findViewById(C0000R.id.viewMember);
        this.j = (TextView) linearLayout3.findViewById(C0000R.id.viewMoneyType);
        this.k = (TextView) linearLayout3.findViewById(C0000R.id.viewAccount);
        this.l = (TextView) linearLayout3.findViewById(C0000R.id.viewProject);
        this.m = (TextView) linearLayout3.findViewById(C0000R.id.viewReimburse);
        ((LinearLayout) this.p.findViewById(C0000R.id.beginMoneyItem)).setOnClickListener(new rh(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.endMoneyItem)).setOnClickListener(new ri(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.dateShortcutsItem)).setOnClickListener(new rf(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.mainTypeItem)).setOnClickListener(new rg(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.subTypeItem)).setOnClickListener(new rd(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.memberItem)).setOnClickListener(new re(this));
        this.r = (LinearLayout) this.p.findViewById(C0000R.id.moneyTypeItem);
        this.r.setOnClickListener(new rb(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.accountItem)).setOnClickListener(new rc(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.projectItem)).setOnClickListener(new rl(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.reimburseItem)).setOnClickListener(new pf(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.dateBeginItem)).setOnClickListener(new pg(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.dateEndItem)).setOnClickListener(new ph(this));
        a_();
    }

    public final void a(Object obj) {
        if (QueryInfo.class.isInstance(obj)) {
            this.z = (QueryInfo) obj;
        } else if (this.z == null) {
            this.z = new QueryInfo();
        }
        this.z.l = -1;
        this.z.m = -1;
    }

    public final void a_() {
        this.y = this.z.a;
        if (-1 == this.z.j) {
            this.b.setText("");
        } else {
            this.b.setText(m.a(m.a(this.z.j), 2));
        }
        if (-1 == this.z.k) {
            this.c.setText("");
        } else {
            this.c.setText(m.a(m.a(this.z.k), 2));
        }
        this.d.setText(this.n.getResources().getStringArray(C0000R.array.Times)[this.z.a]);
        this.e.setText(m.c.format(new Date(a.b(this.z.b))));
        this.f.setText(m.c.format(new Date(a.b(this.z.c))));
        if (-1 == this.z.l) {
            this.g.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.g.setText(p.b(this.z.l));
        }
        if (-1 == this.z.m) {
            this.h.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.h.setText(y.c(this.z.m));
        }
        if (this.z.i == null || this.z.i.length() <= 0) {
            this.i.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.i.setText(this.z.i);
        }
        if (-1 == this.z.d) {
            this.j.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.j.setText(e.a((long) this.z.d));
        }
        if (-1 == this.z.e) {
            this.k.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.k.setText(h.e((long) this.z.e));
        }
        if (-1 == this.z.f) {
            this.l.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.l.setText(f.b((long) this.z.f));
        }
        this.m.setText(m.d(this.n, this.z.g));
        if (b.a) {
            this.r.setVisibility(0);
        } else {
            this.r.setVisibility(8);
        }
    }

    public final Object b() {
        return this.z;
    }

    public final boolean c() {
        this.z.t = 1;
        return true;
    }
}
