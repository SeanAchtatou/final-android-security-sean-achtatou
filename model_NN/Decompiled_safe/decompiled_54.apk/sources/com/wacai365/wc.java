package com.wacai365;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.wacai.data.aa;
import com.wacai.data.g;
import com.wacai.data.s;
import com.wacai.data.x;
import java.util.Date;

public class wc extends wg implements View.OnClickListener {
    private LinearLayout A;
    private LinearLayout B;
    private LinearLayout C;
    private LinearLayout D;
    private LinearLayout E;
    private LinearLayout F;
    private LinearLayout G;
    /* access modifiers changed from: private */
    public int[] H = null;
    private int[] I = null;
    /* access modifiers changed from: private */
    public ProgressDialog J;
    private va K = new ao(this);
    protected int[] a = null;
    protected rk b = null;
    private LinearLayout c;
    /* access modifiers changed from: private */
    public TextView j;
    private TextView k;
    /* access modifiers changed from: private */
    public TextView l;
    /* access modifiers changed from: private */
    public TextView m;
    /* access modifiers changed from: private */
    public TextView r;
    private TextView s;
    /* access modifiers changed from: private */
    public TextView t;
    /* access modifiers changed from: private */
    public TextView u;
    /* access modifiers changed from: private */
    public TextView v;
    private TextView w;
    /* access modifiers changed from: private */
    public g x;
    private LinearLayout y;
    private LinearLayout z;

    public wc(long j2) {
        if (j2 > 0) {
            this.x = g.f(j2);
        } else {
            this.x = new g();
            this.x.s().add(new x(this.x));
        }
        this.d = C0000R.layout.schedule_income_tab;
    }

    public final void a(int i, int i2, Intent intent) {
        if (i2 == -1 && intent != null) {
            switch (i) {
                case 3:
                    m.a(intent, this.x);
                    m.a(this.x.s(), this.r);
                    break;
                case 18:
                    String stringExtra = intent.getStringExtra("Text_String");
                    if (stringExtra == null) {
                        stringExtra = "";
                    }
                    String trim = stringExtra.trim();
                    this.k.setText(trim);
                    this.x.a(trim);
                    break;
                case 24:
                    long longExtra = intent.getLongExtra("Target_ID", 0);
                    this.x.j(longExtra);
                    m.a("TBL_TRADETARGET", longExtra, this.w);
                    break;
                case 37:
                    this.x.h(intent.getLongExtra("account_Sel_Id", this.x.p()));
                    m.a(this.o, this.x.p(), this.s);
                    break;
            }
            super.a(i, i2, intent);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(LinearLayout linearLayout) {
        this.g = linearLayout;
        this.j = (TextView) linearLayout.findViewById(C0000R.id.viewMoney);
        this.k = (TextView) linearLayout.findViewById(C0000R.id.viewName);
        this.l = (TextView) linearLayout.findViewById(C0000R.id.viewType);
        this.r = (TextView) linearLayout.findViewById(C0000R.id.viewMember);
        this.s = (TextView) linearLayout.findViewById(C0000R.id.viewAccount);
        this.t = (TextView) linearLayout.findViewById(C0000R.id.viewProject);
        this.u = (TextView) linearLayout.findViewById(C0000R.id.viewStartDate);
        this.v = (TextView) linearLayout.findViewById(C0000R.id.viewEndDate);
        this.m = (TextView) linearLayout.findViewById(C0000R.id.viewOccurDate);
        this.w = (TextView) linearLayout.findViewById(C0000R.id.viewTarget);
        this.c = (LinearLayout) linearLayout.findViewById(C0000R.id.moneyItem);
        this.c.setOnClickListener(this);
        this.y = (LinearLayout) linearLayout.findViewById(C0000R.id.nameItem);
        this.y.setOnClickListener(this);
        this.z = (LinearLayout) linearLayout.findViewById(C0000R.id.typeItem);
        this.z.setOnClickListener(this);
        this.A = (LinearLayout) linearLayout.findViewById(C0000R.id.occurDateItem);
        this.A.setOnClickListener(this);
        this.B = (LinearLayout) linearLayout.findViewById(C0000R.id.accountItem);
        this.B.setOnClickListener(this);
        this.C = (LinearLayout) linearLayout.findViewById(C0000R.id.memberItem);
        this.C.setOnClickListener(this);
        this.D = (LinearLayout) linearLayout.findViewById(C0000R.id.projectItem);
        this.D.setOnClickListener(this);
        this.E = (LinearLayout) linearLayout.findViewById(C0000R.id.startDateItem);
        this.E.setOnClickListener(this);
        this.F = (LinearLayout) linearLayout.findViewById(C0000R.id.endDateItem);
        this.F.setOnClickListener(this);
        this.G = (LinearLayout) linearLayout.findViewById(C0000R.id.targetItem);
        this.G.setOnClickListener(this);
        this.j.setText(aa.a(aa.l(this.x.o()), 2));
        this.k.setText(this.x.a());
        m.a("TBL_ACCOUNTINFO", this.x.p(), this.s);
        m.a("TBL_INCOMEMAINTYPEINFO", this.x.b(), this.l);
        m.a("TBL_PROJECTINFO", this.x.q(), this.t);
        m.a(this.x.s(), this.r);
        m.a("TBL_TRADETARGET", this.x.r(), this.w);
        m.c(this.x.e() * 1000, this.u);
        m.c(this.x.g() * 1000, this.v);
        this.m.setText("" + (this.x.d() < 1 ? 1 : this.x.d()));
        if (this.J == null) {
            this.J = new ProgressDialog(this.n);
            this.J.setIndeterminate(true);
            this.J.setCancelable(false);
        }
    }

    public final void a(ScrollView scrollView, int i) {
        this.h = scrollView;
        this.i = i;
    }

    public final void a(Object obj) {
        if (g.class.isInstance(obj)) {
            this.x = (g) obj;
            return;
        }
        if (this.x == null) {
            this.x = new g();
        }
        this.x.g(((s) obj).o());
    }

    public final Object b() {
        return this.x;
    }

    public final void b(long j2) {
        this.x.g(j2);
        this.j.setText(aa.a(aa.l(this.x.o()), 2));
    }

    public final boolean c() {
        if (!oj.a(this.o, this.x)) {
            return false;
        }
        if (this.x.a() == null || this.x.a().equals("")) {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.txtEmptyName);
            return false;
        } else if (!aa.b("TBL_INCOMEMAINTYPEINFO", this.x.b())) {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideIncomeT);
            return false;
        } else if (!aa.b("TBL_ACCOUNTINFO", this.x.p())) {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideAccount);
            return false;
        } else if (!aa.b("TBL_PROJECTINFO", this.x.q())) {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideProject);
            return false;
        } else if (!x.a(this.x.s())) {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideMember);
            return false;
        } else if ((this.x.g() - this.x.e()) / 86400 <= 0) {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.txtDurationError);
            return false;
        } else {
            long e = this.x.e() + 946080000;
            if (this.x.g() > e) {
                String format = m.c.format(new Date(e * 1000));
                m.a((Animation) null, 0, (View) null, String.format(this.o.getResources().getString(C0000R.string.txtScheduleDateExceedLimit), format));
                return false;
            }
            this.x.b(0);
            this.x.f(false);
            this.J.setTitle(this.n.getResources().getText(C0000R.string.txtDataSave));
            this.J.setMessage(this.n.getResources().getText(C0000R.string.txtScheduleIncomeCreating));
            this.J.show();
            new z(this).start();
            return true;
        }
    }

    public final void d() {
        if (this.f != null && af.class.isInstance(this.f)) {
            ((af) this.f).a(this.K, this.c);
        }
    }

    public final ca e() {
        return this.f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[]
     arg types: [android.app.Activity, int, com.wacai365.ab]
     candidates:
      com.wacai365.m.a(java.lang.String, android.widget.TextView, android.widget.TextView):double
      com.wacai365.m.a(long, long, long):long
      com.wacai365.m.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String
      com.wacai365.m.a(android.app.Activity, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, long, boolean):void
      com.wacai365.m.a(android.app.Activity, java.util.ArrayList, long):void
      com.wacai365.m.a(android.content.Context, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, android.widget.TextView):void
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(java.lang.String, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean, boolean):boolean
      com.wacai365.m.a(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.c(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[]
     arg types: [android.app.Activity, int, com.wacai365.ad]
     candidates:
      com.wacai365.m.c(java.lang.String, java.lang.String, java.lang.String):android.database.Cursor
      com.wacai365.m.c(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String, int, int, int]
     candidates:
      com.wacai365.m.a(android.content.Context, int, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[]
      com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.app.Activity, long, boolean):void
     arg types: [android.app.Activity, long, int]
     candidates:
      com.wacai365.m.a(java.lang.String, android.widget.TextView, android.widget.TextView):double
      com.wacai365.m.a(long, long, long):long
      com.wacai365.m.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.wacai365.m.a(java.lang.String, boolean, boolean):java.lang.String
      com.wacai365.m.a(android.app.Activity, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, java.util.ArrayList, long):void
      com.wacai365.m.a(android.content.Context, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, long, android.widget.TextView):void
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(java.lang.String, long, android.widget.TextView):void
      com.wacai365.m.a(android.app.Activity, boolean, boolean):boolean
      com.wacai365.m.a(android.content.Context, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.app.Activity, long, boolean):void */
    public void onClick(View view) {
        if (this.f != null) {
            this.f.a(false);
            this.f = null;
        }
        if (view == this.z) {
            this.H = m.a((Context) this.o, false, (DialogInterface.OnClickListener) new ab(this));
        } else if (view == this.B) {
            m.a((Activity) this.n);
        } else if (view == this.D) {
            this.a = m.c((Context) this.o, false, (DialogInterface.OnClickListener) new ad(this));
        } else if (view == this.C) {
            this.b = m.a(this.o, this.x.s(), new ag(this), new ai(this));
        } else if (view == this.c) {
            this.f = af.a(this.q, this.o, m.a(this.x.o()), false);
            ((af) this.f).a(this.K, this.c);
        } else if (view == this.y) {
            m.a(this.o, this.x.a(), this.n.getResources().getString(C0000R.string.txtEditName), 20, 18, true);
        } else if (view == this.A) {
            m.f(this.o, new ak(this));
        } else if (view == this.E) {
            new ut(this.o, this.n, new al(this), new Date(this.x.e() * 1000), 2).show();
        } else if (view == this.F) {
            new ut(this.o, this.n, new am(this), new Date(this.x.g() * 1000), 2).show();
        } else if (view == this.G) {
            m.a(this.o, this.x.r(), true);
        }
    }
}
