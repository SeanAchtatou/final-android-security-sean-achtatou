package com.wacai365;

import android.text.InputFilter;
import android.text.Spanned;

final class wd implements InputFilter {
    private /* synthetic */ NumberPicker a;

    /* synthetic */ wd(NumberPicker numberPicker) {
        this(numberPicker, (byte) 0);
    }

    private wd(NumberPicker numberPicker, byte b) {
        this.a = numberPicker;
    }

    public final CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        if (this.a.h == null) {
            return this.a.g.filter(charSequence, i, i2, spanned, i3, i4);
        }
        String valueOf = String.valueOf(charSequence.subSequence(i, i2));
        String lowerCase = String.valueOf(String.valueOf(spanned.subSequence(0, i3)) + ((Object) valueOf) + ((Object) spanned.subSequence(i4, spanned.length()))).toLowerCase();
        for (String lowerCase2 : this.a.h) {
            if (lowerCase2.toLowerCase().startsWith(lowerCase)) {
                return valueOf;
            }
        }
        return "";
    }
}
