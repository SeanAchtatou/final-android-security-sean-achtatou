package com.wacai365;

import android.content.DialogInterface;

final class bo implements DialogInterface.OnCancelListener {
    private /* synthetic */ DialogInterface.OnClickListener a;

    bo(DialogInterface.OnClickListener onClickListener) {
        this.a = onClickListener;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        if (this.a != null) {
            this.a.onClick(dialogInterface, -2);
        }
    }
}
