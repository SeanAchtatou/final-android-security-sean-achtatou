package com.wacai365;

import android.view.View;
import android.widget.AdapterView;

final class ji implements AdapterView.OnItemClickListener {
    private /* synthetic */ ShortcutsList a;

    ji(ShortcutsList shortcutsList) {
        this.a = shortcutsList;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.a.getIntent().putExtra("SHORTCUTS_TYPE", i);
        this.a.setResult(-1, this.a.getIntent());
        this.a.finish();
    }
}
