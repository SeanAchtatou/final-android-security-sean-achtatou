package com.wacai365;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.wacai.b;
import java.util.ArrayList;
import java.util.Iterator;

final class qj extends BaseAdapter {
    final /* synthetic */ SettingAccountMgr a;
    private ArrayList b = null;
    private Cursor c = null;
    private LayoutInflater d = null;
    private Context e = null;

    public qj(SettingAccountMgr settingAccountMgr, Cursor cursor, ArrayList arrayList, Context context) {
        this.a = settingAccountMgr;
        this.b = arrayList;
        this.c = cursor;
        this.e = context;
        this.d = this.e == null ? null : (LayoutInflater) this.e.getSystemService("layout_inflater");
    }

    public final boolean areAllItemsEnabled() {
        return false;
    }

    public final int getCount() {
        int i = 0;
        if (this.b == null) {
            return 0;
        }
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            ru ruVar = (ru) it.next();
            i = (int) (((long) i) + (ruVar.d - ruVar.c) + 1);
        }
        return i;
    }

    public final Object getItem(int i) {
        Iterator it = this.b.iterator();
        long j = 0;
        while (it.hasNext()) {
            ru ruVar = (ru) it.next();
            j++;
            if (((long) i) == ruVar.c) {
                return ruVar;
            }
            if (((long) i) <= ruVar.d) {
                this.c.moveToPosition((int) (((long) i) - j));
                aar aar = new aar(this.a);
                aar.a = this.c.getLong(this.c.getColumnIndex("_id"));
                aar.c = this.c.getString(this.c.getColumnIndex("_name"));
                if (b.a) {
                    aar.d = this.c.getString(this.c.getColumnIndex("_shortname"));
                }
                aar.b = this.c.getLong(this.c.getColumnIndex("_sum"));
                aar.e = this.c.getInt(this.c.getColumnIndex("_hasbalance"));
                return aar;
            }
        }
        return null;
    }

    public final long getItemId(int i) {
        Object item = getItem(i);
        if (item.getClass() == ru.class) {
            return 0;
        }
        if (item.getClass() == aar.class) {
            return ((aar) item).a;
        }
        return 0;
    }

    public final int getItemViewType(int i) {
        Object item = getItem(i);
        return (item == null || item.getClass() == ru.class) ? 0 : 1;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        Object item = getItem(i);
        int itemViewType = getItemViewType(i);
        View inflate = view == null ? itemViewType == 0 ? this.d.inflate((int) C0000R.layout.balance_list_time_item, (ViewGroup) null) : this.d.inflate((int) C0000R.layout.account_list_item, (ViewGroup) null) : view;
        if (item == null) {
            return inflate;
        }
        if (itemViewType == 0) {
            TextView textView = (TextView) inflate.findViewById(C0000R.id.datetitle);
            if (textView != null) {
                textView.setText(((ru) item).b);
            }
        } else {
            aar aar = (aar) item;
            TextView textView2 = (TextView) inflate.findViewById(C0000R.id.listitem1);
            Button button = (Button) inflate.findViewById(C0000R.id.btnRectificationBalance);
            button.setTag(item);
            if (textView2 != null) {
                textView2.setText(aar.c);
            }
            TextView textView3 = (TextView) inflate.findViewById(C0000R.id.listitem2);
            if (textView3 != null) {
                textView3.setText(aar.d);
            }
            TextView textView4 = (TextView) inflate.findViewById(C0000R.id.listitem3);
            if (textView4 != null) {
                Resources resources = this.a.getResources();
                if (aar.e != 0) {
                    textView4.setText(resources.getString(C0000R.string.txtBalanceTitle) + " " + String.valueOf(m.b(aar.b)));
                    textView4.setTextColor(resources.getColor(C0000R.color.transfer_money));
                    button.setEnabled(true);
                } else {
                    textView4.setText(resources.getString(C0000R.string.txtBalanceNotSet));
                    textView4.setTextColor(resources.getColor(C0000R.color.item_detail_color));
                    button.setEnabled(false);
                }
            }
            button.setOnClickListener(new gv(this));
        }
        return inflate;
    }

    public final int getViewTypeCount() {
        return 2;
    }

    public final boolean isEnabled(int i) {
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            if (((long) i) == ((ru) it.next()).c) {
                return false;
            }
        }
        return true;
    }
}
