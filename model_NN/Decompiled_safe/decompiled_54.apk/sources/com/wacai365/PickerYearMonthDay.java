package com.wacai365;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.wacai.a.a;
import com.wacai365.widget.WheelView;
import com.wacai365.widget.h;
import com.wacai365.widget.l;
import com.wacai365.widget.m;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class PickerYearMonthDay extends LinearLayout implements h {
    private WheelView a;
    private WheelView b;
    private WheelView c;
    private Context d;
    private yo e;
    private Date f;
    private Activity g;
    private m h;
    private int i;
    private int j;
    private int k;
    private GregorianCalendar l;

    public PickerYearMonthDay(Context context) {
        this(context, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.wacai365.PickerYearMonthDay, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public PickerYearMonthDay(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.d = context;
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) C0000R.layout.wacai_date_picker, (ViewGroup) this, true);
        this.a = (WheelView) findViewById(C0000R.id.id_day);
        this.a.a(this);
        this.b = (WheelView) findViewById(C0000R.id.id_month);
        this.b.a(this);
        this.c = (WheelView) findViewById(C0000R.id.id_year);
        this.c.a(this);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, ts.c);
        a(obtainStyledAttributes.getBoolean(0, true));
        obtainStyledAttributes.recycle();
        a(new Date());
    }

    private void b() {
        m mVar = (m) this.a.b();
        if (mVar != null) {
            int i2 = this.k + 1970;
            int a2 = a.a(i2, this.j + 1);
            this.i = Math.min(this.i, a2 - 1);
            mVar.b(a2);
            mVar.d(a.b(i2, this.j + 1, 1));
            this.a.a(true);
        }
    }

    public final void a() {
        this.e = null;
        if (this.c != null) {
            this.c.a();
        }
        if (this.b != null) {
            this.b.a();
        }
        if (this.a != null) {
            this.a.a();
        }
    }

    public final void a(Activity activity) {
        this.g = activity;
    }

    public final void a(WheelView wheelView, int i2) {
        if (wheelView.equals(this.c)) {
            this.k = i2;
            b();
        } else if (wheelView.equals(this.b)) {
            this.j = i2;
            b();
        } else if (wheelView.equals(this.a)) {
            this.i = i2;
            this.h.c(this.i);
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(this.f);
        instance.set(1, this.k + 1970);
        instance.set(2, this.j);
        instance.set(5, this.i + 1);
        this.f = instance.getTime();
        if (this.e != null) {
            this.e.a(this.f);
        }
    }

    public final void a(yo yoVar) {
        this.e = yoVar;
    }

    public final void a(Date date) {
        this.f = (Date) date.clone();
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        int i2 = instance.get(1);
        int i3 = instance.get(2);
        int i4 = instance.get(5);
        if (this.l == null) {
            this.l = new GregorianCalendar();
        }
        this.c.a(new l(this.d, 1970, 2089, i2, "%04d", this.d.getString(C0000R.string.txtYear)));
        this.b.a(new l(this.d, 1, 12, i3 + 1, null, this.d.getString(C0000R.string.txtMonth)));
        WheelView wheelView = this.a;
        m mVar = new m(this.d, C0000R.layout.list_item_datewithdesc, -1, a.a(i2, i3 + 1), a.b(i2, i3 + 1, 1), i4 - 1);
        this.h = mVar;
        wheelView.a(mVar);
    }

    public final void a(boolean z) {
        if (z) {
            this.a.setVisibility(0);
        } else {
            this.a.setVisibility(8);
        }
    }
}
