package com.wacai365;

import java.util.Date;

final class ki implements Runnable {
    final /* synthetic */ SettingAccountMgr a;
    private /* synthetic */ String b;
    private /* synthetic */ long c;

    ki(SettingAccountMgr settingAccountMgr, String str, long j) {
        this.a = settingAccountMgr;
        this.b = str;
        this.c = j;
    }

    public final void run() {
        m.a(this.a, this.a.getString(C0000R.string.txtAlertTitleInfo), this.a.getString(C0000R.string.rectifySucceedPrompt, new Object[]{this.b, m.c.format(new Date(this.c)), m.e.format(new Date(this.c))}), new cf(this));
    }
}
