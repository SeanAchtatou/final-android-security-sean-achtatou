package com.wacai365;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Environment;
import com.wacai.data.VersionItem;

final class us implements DialogInterface.OnClickListener {
    final /* synthetic */ Activity a;
    private /* synthetic */ VersionItem b;

    us(Activity activity, VersionItem versionItem) {
        this.a = activity;
        this.b = versionItem;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        if (i == -1) {
            cc.a(this.a, this.b.d, Environment.getExternalStorageDirectory() + "/" + "Wacai" + this.b.c + ".apk", new jj(this));
        }
    }
}
