package com.wacai365;

import android.content.DialogInterface;

final class bl implements DialogInterface.OnCancelListener {
    private /* synthetic */ DialogInterface.OnClickListener a;

    bl(DialogInterface.OnClickListener onClickListener) {
        this.a = onClickListener;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        if (this.a != null) {
            this.a.onClick(dialogInterface, -1);
        }
    }
}
