package com.wacai365;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.b;
import java.util.Hashtable;

final class tj extends BaseAdapter {
    final /* synthetic */ StatByBalance a;
    private Context b;
    private int c;
    private LayoutInflater d = ((LayoutInflater) this.b.getSystemService("layout_inflater"));

    public tj(StatByBalance statByBalance, Context context, int i) {
        this.a = statByBalance;
        this.b = context;
        this.c = i;
    }

    public final int getCount() {
        return this.a.j.size();
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        String string;
        Hashtable hashtable = (Hashtable) this.a.j.get(i);
        if (hashtable == null) {
            return null;
        }
        LinearLayout linearLayout = view == null ? (LinearLayout) this.d.inflate(this.c, (ViewGroup) null) : (LinearLayout) view;
        ((TextView) linearLayout.findViewById(C0000R.id.listitem1)).setText((CharSequence) hashtable.get("TAG_LABLE"));
        Button button = (Button) linearLayout.findViewById(C0000R.id.btnRectificationBalance);
        button.setTag(hashtable);
        TextView textView = (TextView) linearLayout.findViewById(C0000R.id.listitem2);
        Resources resources = this.a.getResources();
        if (((String) hashtable.get("TAG_INITED")).compareToIgnoreCase("1") == 0) {
            string = (b.a ? "" + ((String) hashtable.get("flag")) : "") + ((String) hashtable.get("TAG_FIRST"));
            textView.setTextColor(resources.getColor(C0000R.color.transfer_money));
            if (!this.a.i.booleanValue()) {
                button.setVisibility(0);
            } else {
                button.setVisibility(8);
            }
        } else {
            string = resources.getString(C0000R.string.txtBalanceNotSet);
            textView.setTextColor(resources.getColor(C0000R.color.item_detail_color));
            button.setVisibility(8);
        }
        textView.setText(string);
        button.setOnClickListener(new io(this));
        return linearLayout;
    }
}
