package com.wacai365;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.a.e;
import com.wacai365.a.c;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class WeiboPublish extends WacaiActivity implements DialogInterface.OnClickListener, View.OnClickListener, View.OnLongClickListener {
    public static final String a = (m.i + "/tmp.jpg");
    /* access modifiers changed from: private */
    public int b = 100;
    /* access modifiers changed from: private */
    public EditText c;
    private ImageView d;
    /* access modifiers changed from: private */
    public TextView e;
    private ImageButton f;
    private ImageView g;
    private ImageView h;
    private View i;
    /* access modifiers changed from: private */
    public ArrayList j = new ArrayList();
    private boolean k;
    /* access modifiers changed from: private */
    public boolean l;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public boolean n = true;
    /* access modifiers changed from: private */
    public boolean o = false;
    /* access modifiers changed from: private */
    public ProgressDialog p;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public Handler r = new c(this);
    /* access modifiers changed from: private */
    public boolean s = true;
    private TextWatcher t = new b(this);

    private static Bitmap a(Context context, InputStream inputStream, boolean z) {
        try {
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            BitmapFactory.Options options = new BitmapFactory.Options();
            if (z) {
                options.inSampleSize = 4;
            } else {
                options.inSampleSize = 1;
            }
            options.outHeight = (int) (displayMetrics.density * 150.0f);
            return BitmapFactory.decodeStream(inputStream, null, options);
        } catch (OutOfMemoryError e2) {
            m.a(context, (Animation) null, 0, (View) null, (int) C0000R.string.weiboOutOfMemory);
            return null;
        }
    }

    private synchronized boolean a() {
        boolean z;
        boolean z2;
        if (!e.a()) {
            m.a(this, (Animation) null, -1, (View) null, (int) C0000R.string.txtNoNetworkPrompt);
            z = false;
        } else {
            this.q = this.n ? this.m : this.c.getText().toString();
            if (this.q == null || this.q.length() <= 0 || this.q.trim().length() <= 0) {
                m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.weiboErrorEmpty);
                z = false;
            } else if (m.e(this.q) > 140) {
                m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.weiboTextCountExceedLimitation);
                z = false;
            } else {
                NetworkInfo[] allNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getAllNetworkInfo();
                if (allNetworkInfo != null && allNetworkInfo.length > 0) {
                    int length = allNetworkInfo.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            break;
                        }
                        NetworkInfo networkInfo = allNetworkInfo[i2];
                        if (networkInfo.isConnected() && networkInfo.getType() == 1) {
                            z2 = true;
                            break;
                        }
                        i2++;
                    }
                }
                z2 = false;
                if (!z2) {
                    File file = new File(a);
                    float length2 = (float) file.length();
                    if (file.exists() && length2 > 0.0f && this.k) {
                        if (length2 >= 5242880.0f) {
                            m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.weiboFileSizeTooLarge);
                            z = false;
                        } else {
                            StringBuilder sb = new StringBuilder();
                            float f2 = length2 / 1024.0f;
                            if (f2 < 1024.0f) {
                                sb.append(((double) ((float) ((int) (f2 * 100.0f)))) / 100.0d);
                                sb.append("KB");
                            } else {
                                sb.append(((double) ((float) ((int) ((f2 / 1024.0f) * 100.0f)))) / 100.0d);
                                sb.append("M");
                            }
                            m.a(this, getResources().getString(C0000R.string.weiboPicSizeLarge, sb.toString()), this);
                            z = false;
                        }
                    }
                }
                z = true;
            }
        }
        return z;
    }

    private void b() {
        if (this.p == null) {
            this.p = new ProgressDialog(this);
            this.p.setProgressStyle(0);
            this.p.setMessage(getResources().getString(C0000R.string.txtTransformData));
        }
        this.p.show();
        new Thread((ThreadGroup) null, new bh(this)).start();
    }

    private void c() {
        File f2;
        deleteFile("tmp.jpg");
        if (m.c() && (f2 = m.f("tmp.jpg")) != null) {
            f2.delete();
        }
    }

    private void d() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        View findViewById = findViewById(C0000R.id.weibo_image_share);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) findViewById.getLayoutParams();
        layoutParams.topMargin = (int) (displayMetrics.density * 15.0f);
        findViewById.setLayoutParams(layoutParams);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0132, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0133, code lost:
        r9 = r1;
        r1 = r2;
        r2 = r0;
        r0 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x0144, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x0145, code lost:
        r9 = r1;
        r1 = r2;
        r2 = r0;
        r0 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r2.close();
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006b, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0088, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x008a, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        r2.close();
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0093, code lost:
        r1 = r0;
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0113 A[SYNTHETIC, Splitter:B:102:0x0113] */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0132 A[ExcHandler: all (r1v29 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:15:0x004a] */
    /* JADX WARNING: Removed duplicated region for block: B:142:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:143:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:145:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0092 A[ExcHandler: FileNotFoundException (e java.io.FileNotFoundException), Splitter:B:15:0x004a] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0097 A[SYNTHETIC, Splitter:B:52:0x0097] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x009c A[SYNTHETIC, Splitter:B:55:0x009c] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00ac A[SYNTHETIC, Splitter:B:63:0x00ac] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00b1 A[SYNTHETIC, Splitter:B:66:0x00b1] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00be A[SYNTHETIC, Splitter:B:72:0x00be] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x00c3 A[SYNTHETIC, Splitter:B:75:0x00c3] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0107 A[SYNTHETIC, Splitter:B:96:0x0107] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onActivityResult(int r11, int r12, android.content.Intent r13) {
        /*
            r10 = this;
            r8 = -1
            r7 = 0
            r6 = 0
            if (r12 == r8) goto L_0x0006
        L_0x0005:
            return
        L_0x0006:
            switch(r11) {
                case 35: goto L_0x000a;
                case 36: goto L_0x00c7;
                default: goto L_0x0009;
            }
        L_0x0009:
            goto L_0x0005
        L_0x000a:
            if (r13 == 0) goto L_0x0005
            android.net.Uri r0 = r13.getData()
            android.content.ContentResolver r1 = r10.getContentResolver()
            java.io.InputStream r2 = r1.openInputStream(r0)     // Catch:{ FileNotFoundException -> 0x014b, IOException -> 0x00a4, all -> 0x00b9 }
            r3 = 1
            android.graphics.Bitmap r3 = a(r10, r2, r3)     // Catch:{ FileNotFoundException -> 0x0150, IOException -> 0x013a, all -> 0x012a }
            android.widget.ImageButton r4 = r10.f     // Catch:{ FileNotFoundException -> 0x0150, IOException -> 0x013a, all -> 0x012a }
            r5 = 0
            r4.setVisibility(r5)     // Catch:{ FileNotFoundException -> 0x0150, IOException -> 0x013a, all -> 0x012a }
            r4 = 2131493330(0x7f0c01d2, float:1.8610137E38)
            android.view.View r4 = r10.findViewById(r4)     // Catch:{ FileNotFoundException -> 0x0150, IOException -> 0x013a, all -> 0x012a }
            r5 = 0
            r4.setVisibility(r5)     // Catch:{ FileNotFoundException -> 0x0150, IOException -> 0x013a, all -> 0x012a }
            android.widget.ImageView r4 = r10.d     // Catch:{ FileNotFoundException -> 0x0150, IOException -> 0x013a, all -> 0x012a }
            r4.setImageBitmap(r3)     // Catch:{ FileNotFoundException -> 0x0150, IOException -> 0x013a, all -> 0x012a }
            r2.close()     // Catch:{ FileNotFoundException -> 0x0150, IOException -> 0x013a, all -> 0x012a }
            r3 = 1
            r10.k = r3     // Catch:{ FileNotFoundException -> 0x0150, IOException -> 0x013a, all -> 0x012a }
            java.io.InputStream r0 = r1.openInputStream(r0)     // Catch:{ FileNotFoundException -> 0x0150, IOException -> 0x013a, all -> 0x012a }
            java.lang.String r1 = "tmp.jpg"
            java.io.File r1 = com.wacai365.m.f(r1)     // Catch:{ FileNotFoundException -> 0x0155, IOException -> 0x013e, all -> 0x012d }
            if (r1 == 0) goto L_0x015f
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0155, IOException -> 0x013e, all -> 0x012d }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0155, IOException -> 0x013e, all -> 0x012d }
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ FileNotFoundException -> 0x0092, IOException -> 0x0144, all -> 0x0132 }
            r1.<init>(r0)     // Catch:{ FileNotFoundException -> 0x0092, IOException -> 0x0144, all -> 0x0132 }
            r3 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x0064, all -> 0x008a }
            r4 = r6
        L_0x0054:
            if (r4 == r8) goto L_0x0079
            r4 = 0
            r5 = 1024(0x400, float:1.435E-42)
            int r4 = r1.read(r3, r4, r5)     // Catch:{ IOException -> 0x0064, all -> 0x008a }
            if (r4 <= 0) goto L_0x0054
            r5 = 0
            r2.write(r3, r5, r4)     // Catch:{ IOException -> 0x0064, all -> 0x008a }
            goto L_0x0054
        L_0x0064:
            r3 = move-exception
            r2.close()     // Catch:{ IOException -> 0x0087, FileNotFoundException -> 0x0092, all -> 0x0132 }
            r1.close()     // Catch:{ IOException -> 0x0087, FileNotFoundException -> 0x0092, all -> 0x0132 }
            r1 = r2
        L_0x006c:
            if (r0 == 0) goto L_0x0071
            r0.close()     // Catch:{ IOException -> 0x0117 }
        L_0x0071:
            if (r1 == 0) goto L_0x0005
            r1.close()     // Catch:{ IOException -> 0x0077 }
            goto L_0x0005
        L_0x0077:
            r0 = move-exception
            goto L_0x0005
        L_0x0079:
            r2.flush()     // Catch:{ IOException -> 0x0064, all -> 0x008a }
            r2.close()     // Catch:{ IOException -> 0x0084, FileNotFoundException -> 0x0092, all -> 0x0132 }
            r1.close()     // Catch:{ IOException -> 0x0084, FileNotFoundException -> 0x0092, all -> 0x0132 }
            r1 = r2
            goto L_0x006c
        L_0x0084:
            r1 = move-exception
            r1 = r2
            goto L_0x006c
        L_0x0087:
            r1 = move-exception
            r1 = r2
            goto L_0x006c
        L_0x008a:
            r3 = move-exception
            r2.close()     // Catch:{ IOException -> 0x015a, FileNotFoundException -> 0x0092, all -> 0x0132 }
            r1.close()     // Catch:{ IOException -> 0x015a, FileNotFoundException -> 0x0092, all -> 0x0132 }
        L_0x0091:
            throw r3     // Catch:{ FileNotFoundException -> 0x0092, IOException -> 0x0144, all -> 0x0132 }
        L_0x0092:
            r1 = move-exception
            r1 = r0
            r0 = r2
        L_0x0095:
            if (r1 == 0) goto L_0x009a
            r1.close()     // Catch:{ IOException -> 0x011a }
        L_0x009a:
            if (r0 == 0) goto L_0x0005
            r0.close()     // Catch:{ IOException -> 0x00a1 }
            goto L_0x0005
        L_0x00a1:
            r0 = move-exception
            goto L_0x0005
        L_0x00a4:
            r0 = move-exception
            r1 = r7
            r2 = r7
        L_0x00a7:
            r0.printStackTrace()     // Catch:{ all -> 0x0138 }
            if (r2 == 0) goto L_0x00af
            r2.close()     // Catch:{ IOException -> 0x011d }
        L_0x00af:
            if (r1 == 0) goto L_0x0005
            r1.close()     // Catch:{ IOException -> 0x00b6 }
            goto L_0x0005
        L_0x00b6:
            r0 = move-exception
            goto L_0x0005
        L_0x00b9:
            r0 = move-exception
            r1 = r7
            r2 = r7
        L_0x00bc:
            if (r2 == 0) goto L_0x00c1
            r2.close()     // Catch:{ IOException -> 0x011f }
        L_0x00c1:
            if (r1 == 0) goto L_0x00c6
            r1.close()     // Catch:{ IOException -> 0x0121 }
        L_0x00c6:
            throw r0
        L_0x00c7:
            java.lang.String r0 = "tmp.jpg"
            java.io.File r0 = com.wacai365.m.f(r0)     // Catch:{ FileNotFoundException -> 0x0103, all -> 0x010f }
            if (r0 == 0) goto L_0x015d
            boolean r1 = r0.exists()     // Catch:{ FileNotFoundException -> 0x0103, all -> 0x010f }
            if (r1 == 0) goto L_0x015d
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0103, all -> 0x010f }
            r1.<init>(r0)     // Catch:{ FileNotFoundException -> 0x0103, all -> 0x010f }
            r0 = 1
            android.graphics.Bitmap r0 = a(r10, r1, r0)     // Catch:{ FileNotFoundException -> 0x0127, all -> 0x0125 }
            android.widget.ImageButton r2 = r10.f     // Catch:{ FileNotFoundException -> 0x0127, all -> 0x0125 }
            r3 = 0
            r2.setVisibility(r3)     // Catch:{ FileNotFoundException -> 0x0127, all -> 0x0125 }
            r2 = 2131493330(0x7f0c01d2, float:1.8610137E38)
            android.view.View r2 = r10.findViewById(r2)     // Catch:{ FileNotFoundException -> 0x0127, all -> 0x0125 }
            r3 = 0
            r2.setVisibility(r3)     // Catch:{ FileNotFoundException -> 0x0127, all -> 0x0125 }
            android.widget.ImageView r2 = r10.d     // Catch:{ FileNotFoundException -> 0x0127, all -> 0x0125 }
            r2.setImageBitmap(r0)     // Catch:{ FileNotFoundException -> 0x0127, all -> 0x0125 }
            r0 = 1
            r10.k = r0     // Catch:{ FileNotFoundException -> 0x0127, all -> 0x0125 }
            r0 = r1
        L_0x00f9:
            if (r0 == 0) goto L_0x0005
            r0.close()     // Catch:{ IOException -> 0x0100 }
            goto L_0x0005
        L_0x0100:
            r0 = move-exception
            goto L_0x0005
        L_0x0103:
            r0 = move-exception
            r0 = r7
        L_0x0105:
            if (r0 == 0) goto L_0x0005
            r0.close()     // Catch:{ IOException -> 0x010c }
            goto L_0x0005
        L_0x010c:
            r0 = move-exception
            goto L_0x0005
        L_0x010f:
            r0 = move-exception
            r1 = r7
        L_0x0111:
            if (r1 == 0) goto L_0x0116
            r1.close()     // Catch:{ IOException -> 0x0123 }
        L_0x0116:
            throw r0
        L_0x0117:
            r0 = move-exception
            goto L_0x0071
        L_0x011a:
            r1 = move-exception
            goto L_0x009a
        L_0x011d:
            r0 = move-exception
            goto L_0x00af
        L_0x011f:
            r2 = move-exception
            goto L_0x00c1
        L_0x0121:
            r1 = move-exception
            goto L_0x00c6
        L_0x0123:
            r1 = move-exception
            goto L_0x0116
        L_0x0125:
            r0 = move-exception
            goto L_0x0111
        L_0x0127:
            r0 = move-exception
            r0 = r1
            goto L_0x0105
        L_0x012a:
            r0 = move-exception
            r1 = r7
            goto L_0x00bc
        L_0x012d:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r7
            goto L_0x00bc
        L_0x0132:
            r1 = move-exception
            r9 = r1
            r1 = r2
            r2 = r0
            r0 = r9
            goto L_0x00bc
        L_0x0138:
            r0 = move-exception
            goto L_0x00bc
        L_0x013a:
            r0 = move-exception
            r1 = r7
            goto L_0x00a7
        L_0x013e:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r7
            goto L_0x00a7
        L_0x0144:
            r1 = move-exception
            r9 = r1
            r1 = r2
            r2 = r0
            r0 = r9
            goto L_0x00a7
        L_0x014b:
            r0 = move-exception
            r0 = r7
            r1 = r7
            goto L_0x0095
        L_0x0150:
            r0 = move-exception
            r0 = r7
            r1 = r2
            goto L_0x0095
        L_0x0155:
            r1 = move-exception
            r1 = r0
            r0 = r7
            goto L_0x0095
        L_0x015a:
            r1 = move-exception
            goto L_0x0091
        L_0x015d:
            r0 = r7
            goto L_0x00f9
        L_0x015f:
            r1 = r7
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.WeiboPublish.onActivityResult(int, int, android.content.Intent):void");
    }

    public void onClick(DialogInterface dialogInterface, int i2) {
        switch (i2) {
            case -1:
                b();
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        File f2;
        switch (view.getId()) {
            case C0000R.id.btnCancel /*2131492870*/:
                finish();
                return;
            case C0000R.id.btnOK /*2131492903*/:
                if (a()) {
                    b();
                    return;
                }
                return;
            case C0000R.id.suggestion_or_bugs /*2131493326*/:
                if (this.n && !this.o) {
                    this.n = false;
                    this.c.setText(this.m);
                    if (this.m != null) {
                        this.c.setSelection(0, this.m.length());
                        return;
                    }
                    return;
                }
                return;
            case C0000R.id.btn_gallery /*2131493328*/:
                if (m.d()) {
                    Intent intent = new Intent("android.intent.action.GET_CONTENT");
                    intent.setType("image/*");
                    intent.addFlags(67108864);
                    startActivityForResult(intent, 35);
                    return;
                }
                return;
            case C0000R.id.btn_camera /*2131493329*/:
                if (m.d() && (f2 = m.f("tmp.jpg")) != null) {
                    Intent intent2 = new Intent("android.media.action.IMAGE_CAPTURE");
                    intent2.putExtra("output", Uri.fromFile(f2));
                    intent2.addFlags(67108864);
                    startActivityForResult(intent2, 36);
                    return;
                }
                return;
            case C0000R.id.remove_image /*2131493332*/:
                if (this.d != null) {
                    this.d.setImageBitmap(null);
                }
                this.f.setVisibility(4);
                findViewById(C0000R.id.weibo_image_share).setVisibility(4);
                this.l = true;
                c();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        int i2;
        super.onCreate(bundle);
        Intent intent = getIntent();
        this.b = intent.getExtras().getInt("view-type", 100);
        long longExtra = getIntent().getLongExtra("Extra-webo-type", -1);
        if (longExtra > 0) {
            this.j.add(c.a(aaj.a().a(longExtra)));
            if (this.j == null) {
                finish();
            }
        } else {
            for (ll llVar : aaj.a().b()) {
                if (llVar.a()) {
                    this.j.add(c.a(llVar));
                }
            }
        }
        setContentView((int) C0000R.layout.weibo_publish);
        this.c = (EditText) findViewById(C0000R.id.suggestion_or_bugs);
        this.c.addTextChangedListener(this.t);
        this.c.setOnClickListener(this);
        this.c.setOnLongClickListener(this);
        this.d = (ImageView) findViewById(C0000R.id.image_content);
        this.e = (TextView) findViewById(C0000R.id.input_text_count_prompt);
        this.f = (ImageButton) findViewById(C0000R.id.remove_image);
        this.f.setOnClickListener(this);
        findViewById(C0000R.id.btnOK).setOnClickListener(this);
        findViewById(C0000R.id.btnCancel).setOnClickListener(this);
        this.g = (ImageView) findViewById(C0000R.id.btn_gallery);
        this.g.setOnClickListener(this);
        this.h = (ImageView) findViewById(C0000R.id.btn_camera);
        this.h.setOnClickListener(this);
        this.i = findViewById(C0000R.id.image_pick);
        if (longExtra > 0) {
            this.i.setVisibility(8);
            d();
        }
        View findViewById = findViewById(C0000R.id.weibo_image_share);
        switch (this.b) {
            case 100:
                findViewById.setVisibility(0);
                String string = getString(C0000R.string.weiboShareToFriendsContent);
                this.c.setText(string);
                this.m = string;
                i2 = C0000R.string.weiboPublish;
                break;
            case 101:
                String a2 = ((c) this.j.get(0)).a(this);
                this.c.setText(a2 + getString(C0000R.string.weiboSuggestionsHint));
                this.m = a2;
                this.i.setVisibility(8);
                d();
                i2 = C0000R.string.weiboPublish;
                break;
            default:
                i2 = 0;
                break;
        }
        this.e.setText(getResources().getString(C0000R.string.weiboCharacterCanInput, Integer.valueOf(140 - m.e(this.m))));
        setTitle(i2);
        if (intent.hasExtra("publish-content-txt")) {
            this.m = intent.getExtras().getString("publish-content-txt");
            this.c.setText(this.m + getResources().getString(C0000R.string.weiboShareHint));
        }
        this.c.setSelection(this.c.getText().toString().length());
        if (this.b == 100) {
            FileInputStream fileInputStream = null;
            try {
                fileInputStream = m.c() ? new FileInputStream(new File(a)) : openFileInput("tmp.jpg");
            } catch (FileNotFoundException e2) {
                this.d.setImageResource(C0000R.drawable.share_logo);
            }
            if (fileInputStream != null) {
                this.d.setImageBitmap(a(this, fileInputStream, false));
                this.f.setVisibility(4);
                this.i.setVisibility(8);
                d();
                try {
                    fileInputStream.close();
                } catch (IOException e3) {
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        c();
        if (this.j != null) {
            this.j.clear();
            this.j = null;
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        switch (i2) {
            case 4:
                if (this.p != null && this.p.isShowing()) {
                    this.r.removeMessages(200);
                    this.r.removeMessages(201);
                    this.p.dismiss();
                    return true;
                }
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onLongClick(View view) {
        switch (view.getId()) {
            case C0000R.id.suggestion_or_bugs /*2131493326*/:
                this.o = true;
                return false;
            default:
                return false;
        }
    }
}
