package com.wacai365;

import android.database.Cursor;
import android.view.View;
import android.widget.AdapterView;

final class kv implements AdapterView.OnItemClickListener {
    private /* synthetic */ ChooseTarget a;

    kv(ChooseTarget chooseTarget) {
        this.a = chooseTarget;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Cursor cursor = (Cursor) this.a.e.getItemAtPosition(i);
        if (cursor != null) {
            this.a.a(cursor.getLong(cursor.getColumnIndexOrThrow("_id")));
        }
    }
}
