package com.wacai365;

import android.content.DialogInterface;
import com.wacai.data.x;
import java.util.ArrayList;

final class cr implements DialogInterface.OnClickListener {
    private /* synthetic */ yf a;

    cr(yf yfVar) {
        this.a = yfVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (!(this.a.c == null || this.a.c.b == null || this.a.c.b.length <= 0)) {
            int length = this.a.c.b.length;
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < length; i2++) {
                if (this.a.c.b[i2]) {
                    arrayList.add(m.a(this.a.a, this.a.c.a[i2]));
                }
            }
            int size = arrayList.size();
            if (1 == size) {
                ((x) arrayList.get(0)).b(this.a.a.o());
                this.a.a.a(arrayList);
                m.a(this.a.a.s(), this.a.v);
            } else if (1 < size) {
                m.a(this.a.o, arrayList, this.a.a.o());
            } else {
                this.a.a.a(arrayList);
                this.a.v.setText("");
            }
        }
        dialogInterface.dismiss();
    }
}
