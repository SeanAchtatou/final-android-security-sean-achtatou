package com.wacai365;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import java.util.Calendar;
import java.util.Date;

public final class ut extends Dialog implements DialogInterface {
    protected Date a;
    private int b = 2;
    private yn c = new hc(this);

    public ut(Activity activity, Context context, DialogInterface.OnClickListener onClickListener, Date date, int i) {
        super(context, C0000R.style.choose_dialog);
        this.a = date;
        this.b = i;
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(context).inflate((int) C0000R.layout.choose_money_dialog, (ViewGroup) null);
        if (linearLayout != null) {
            linearLayout.removeAllViews();
            zl.a(linearLayout, activity, date, i).a(this.c, onClickListener);
            setContentView(linearLayout);
        }
    }

    public final Date a() {
        if (1 != this.b) {
            return this.a;
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(this.a);
        instance.set(5, 1);
        return instance.getTime();
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }
}
