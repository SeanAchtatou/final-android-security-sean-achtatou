package com.wacai365;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.wacai.e;

final class yk extends SimpleCursorAdapter {
    private String[] a;
    private int[] b;
    private String c;
    private View.OnClickListener d = new et(this);

    public yk(Context context, int i, Cursor cursor, String[] strArr, int[] iArr, String str) {
        super(context, i, cursor, strArr, iArr);
        this.b = iArr;
        this.a = strArr;
        this.c = str;
    }

    static /* synthetic */ void a(yk ykVar, String str, boolean z) {
        e.c().b().execSQL(new StringBuffer("UPDATE ").append(ykVar.c).append(" SET star = ").append(z ? 1 : 0).append(" WHERE id = ").append(str).toString());
        ykVar.getCursor().requery();
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        View findViewById = view.findViewById(this.b[0]);
        if (findViewById != null) {
            String string = cursor.getString(cursor.getColumnIndexOrThrow(this.a[0]));
            if (string == null) {
                string = "";
            }
            setViewText((TextView) findViewById, string);
        }
        View findViewById2 = view.findViewById(this.b[1]);
        if (CheckBox.class.isInstance(findViewById2)) {
            CheckBox checkBox = (CheckBox) findViewById2;
            checkBox.setChecked(cursor.getLong(cursor.getColumnIndexOrThrow(this.a[1])) != 0);
            checkBox.setTag(cursor.getString(cursor.getColumnIndexOrThrow("_id")));
            checkBox.setOnClickListener(this.d);
        }
    }
}
