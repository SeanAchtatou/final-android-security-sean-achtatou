package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.SimpleCursorAdapter;
import com.wacai.a;
import com.wacai.e;
import java.util.ArrayList;
import java.util.List;

public class ChooseTarget extends WacaiActivity {
    private ListView a;
    private LinearLayout b;
    private ListView c;
    /* access modifiers changed from: private */
    public EditText d;
    /* access modifiers changed from: private */
    public ListView e;
    private LinearLayout f;
    private RadioGroup g;
    /* access modifiers changed from: private */
    public boolean h;
    private View.OnClickListener i = new kp(this);
    private RadioGroup.OnCheckedChangeListener j = new kn(this);

    /* access modifiers changed from: private */
    public void a() {
        if (this.a == null) {
            this.a = new ListView(this);
            this.a.setDivider(new ColorDrawable(getResources().getColor(C0000R.color.listDivider)));
            this.a.setDividerHeight(1);
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        a(arrayList, arrayList2);
        this.a.setAdapter((ListAdapter) new kh(this, "TBL_TRADETARGET", new int[]{C0000R.string.myFavorites, C0000R.string.myFrequences}, arrayList, arrayList2, new ko(this), new ks(this)));
        this.a.setOnItemClickListener(new kt(this));
        this.a.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.f.removeAllViews();
        this.f.addView(this.a);
    }

    /* access modifiers changed from: private */
    public void a(long j2) {
        if (j2 > 0) {
            getIntent().putExtra("Target_ID", j2);
            setResult(-1, getIntent());
            finish();
        }
    }

    /* access modifiers changed from: private */
    public void a(List list, List list2) {
        if (list != null && list2 != null) {
            kh.a("select a.id, a.name, a.star from TBL_TRADETARGET a where a.enable = 1 and a.star = 1 order by a.pinyin ASC", list);
            kh.a("select a.id, a.name, a.star from TBL_TRADETARGET a where a.enable = 1 and a.star = 0 and a.refcount > 0 order by a.refcount DESC, a.pinyin ASC limit 0, 10", list2);
            this.a.invalidateViews();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void b(com.wacai365.ChooseTarget r6) {
        /*
            r2 = 2131296395(0x7f09008b, float:1.8210705E38)
            r5 = 2131296285(0x7f09001d, float:1.8210482E38)
            r4 = -1
            r3 = 0
            android.widget.EditText r0 = r6.d     // Catch:{ all -> 0x006a }
            android.text.Editable r0 = r0.getText()     // Catch:{ all -> 0x006a }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x006a }
            if (r0 == 0) goto L_0x001e
            java.lang.String r0 = r0.trim()     // Catch:{ all -> 0x006a }
            int r1 = r0.length()     // Catch:{ all -> 0x006a }
            if (r1 > 0) goto L_0x0029
        L_0x001e:
            android.widget.EditText r0 = r6.d     // Catch:{ all -> 0x0072 }
            java.lang.String r1 = ""
            r0.setText(r1)     // Catch:{ all -> 0x0072 }
            com.wacai365.m.a(r6, r3, r4, r3, r2)
        L_0x0028:
            return
        L_0x0029:
            r1 = 20
            int r2 = r0.length()     // Catch:{ all -> 0x006a }
            if (r1 >= r2) goto L_0x0038
            r0 = 2131296396(0x7f09008c, float:1.8210707E38)
            com.wacai365.m.a(r6, r3, r4, r3, r0)
            goto L_0x0028
        L_0x0038:
            com.wacai.data.j r1 = new com.wacai.data.j     // Catch:{ all -> 0x006a }
            r1.<init>()     // Catch:{ all -> 0x006a }
            boolean r2 = r1.c(r0)     // Catch:{ all -> 0x006a }
            if (r2 == 0) goto L_0x004a
            r0 = 2131296397(0x7f09008d, float:1.821071E38)
            com.wacai365.m.a(r6, r3, r4, r3, r0)
            goto L_0x0028
        L_0x004a:
            com.wacai.data.j r2 = com.wacai.data.j.a(r0)     // Catch:{ all -> 0x006a }
            if (r2 != 0) goto L_0x0078
            r1.b(r0)     // Catch:{ all -> 0x006a }
            r0 = r1
        L_0x0054:
            r1 = 0
            r0.f(r1)     // Catch:{ all -> 0x006a }
            r1 = 1
            r0.d(r1)     // Catch:{ all -> 0x006a }
            r0.c()     // Catch:{ all -> 0x006a }
            long r0 = r0.x()     // Catch:{ all -> 0x0075 }
            r6.a(r0)     // Catch:{ all -> 0x0075 }
            com.wacai365.m.a(r6, r3, r4, r3, r5)
            goto L_0x0028
        L_0x006a:
            r0 = move-exception
            r1 = r4
        L_0x006c:
            if (r1 == r4) goto L_0x0071
            com.wacai365.m.a(r6, r3, r4, r3, r1)
        L_0x0071:
            throw r0
        L_0x0072:
            r0 = move-exception
            r1 = r2
            goto L_0x006c
        L_0x0075:
            r0 = move-exception
            r1 = r5
            goto L_0x006c
        L_0x0078:
            r0 = r2
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.ChooseTarget.b(com.wacai365.ChooseTarget):void");
    }

    static /* synthetic */ void d(ChooseTarget chooseTarget) {
        if (chooseTarget.b == null) {
            chooseTarget.f.removeAllViews();
            chooseTarget.b = (LinearLayout) ((LayoutInflater) chooseTarget.getSystemService("layout_inflater")).inflate((int) C0000R.layout.choose_target_all, (ViewGroup) null);
            chooseTarget.c = (ListView) chooseTarget.b.findViewById(C0000R.id.lvAll);
            Cursor rawQuery = e.c().b().rawQuery(a.a("BasicSortStyle", 0) == 0 ? "select id as _id, name as _name, star as _star from TBL_TRADETARGET where enable = 1 " + " ORDER BY orderno ASC " : "select id as _id, name as _name, star as _star from TBL_TRADETARGET where enable = 1 " + " ORDER BY pinyin ASC ", null);
            chooseTarget.startManagingCursor(rawQuery);
            chooseTarget.c.setAdapter((ListAdapter) new yk(chooseTarget, C0000R.layout.list_item_withstar, rawQuery, new String[]{"_name", "_star"}, new int[]{C0000R.id.listitem1, C0000R.id.btnStar}, "TBL_TRADETARGET"));
            chooseTarget.c.setOnItemClickListener(new kq(chooseTarget));
            chooseTarget.b.findViewById(C0000R.id.btnAdd).setOnClickListener(chooseTarget.i);
            chooseTarget.d = (EditText) chooseTarget.b.findViewById(C0000R.id.etSearch);
            chooseTarget.d.addTextChangedListener(new kr(chooseTarget));
            ((ImageButton) chooseTarget.b.findViewById(C0000R.id.btnClear)).setOnClickListener(new ku(chooseTarget));
        } else {
            ChooseOutgoType.a(chooseTarget.c);
            ChooseOutgoType.a(chooseTarget.e);
        }
        chooseTarget.b.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        chooseTarget.f.removeAllViews();
        chooseTarget.f.addView(chooseTarget.b);
    }

    static /* synthetic */ void e(ChooseTarget chooseTarget) {
        if (chooseTarget.d.getText().length() == 0) {
            if (chooseTarget.e != null) {
                chooseTarget.e.setVisibility(8);
            }
            ((CursorAdapter) chooseTarget.c.getAdapter()).getCursor().requery();
            chooseTarget.c.setVisibility(0);
            return;
        }
        chooseTarget.c.setVisibility(8);
        Cursor c2 = m.c(" id as _id, name as _name, star as _star, ", chooseTarget.d.getText().toString(), "TBL_TRADETARGET");
        if (c2 != null) {
            chooseTarget.startManagingCursor(c2);
        }
        if (chooseTarget.e == null) {
            chooseTarget.e = new ListView(chooseTarget);
            chooseTarget.e.setDivider(new ColorDrawable(chooseTarget.getResources().getColor(C0000R.color.listDivider)));
            chooseTarget.e.setDividerHeight(1);
            chooseTarget.e.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
            chooseTarget.b.addView(chooseTarget.e);
            ChooseTarget chooseTarget2 = chooseTarget;
            chooseTarget.e.setAdapter((ListAdapter) new yk(chooseTarget2, C0000R.layout.list_item_withstar, c2, new String[]{"_name", "_star"}, new int[]{C0000R.id.listitem1, C0000R.id.btnStar}, "TBL_TRADETARGET"));
            chooseTarget.e.setOnItemClickListener(new kv(chooseTarget));
            return;
        }
        ((SimpleCursorAdapter) chooseTarget.e.getAdapter()).changeCursor(c2);
        chooseTarget.e.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        View childAt;
        super.onActivityResult(i2, i3, intent);
        if (i2 == 32 && (childAt = this.f.getChildAt(0)) != null && childAt.equals(this.a)) {
            a();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.choose_target);
        this.h = getIntent().getBooleanExtra("Is_Payer", false);
        this.f = (LinearLayout) findViewById(C0000R.id.baselayout);
        this.g = (RadioGroup) findViewById(C0000R.id.rgHeader);
        this.g.setOnCheckedChangeListener(this.j);
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(this.i);
        Button button = (Button) findViewById(C0000R.id.btnTargetMgr);
        button.setOnClickListener(this.i);
        if (this.h) {
            setTitle((int) C0000R.string.choosePayTarget);
            button.setText((int) C0000R.string.payTargetMgr);
        }
        a();
    }
}
