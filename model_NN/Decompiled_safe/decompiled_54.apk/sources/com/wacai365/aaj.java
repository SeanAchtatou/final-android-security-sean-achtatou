package com.wacai365;

public final class aaj {
    private static aaj a;
    private ll[] b;

    private aaj() {
        d();
    }

    public static synchronized aaj a() {
        aaj aaj;
        synchronized (aaj.class) {
            if (a == null) {
                a = new aaj();
            }
            aaj = a;
        }
        return aaj;
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ef  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean d() {
        /*
            r10 = this;
            r3 = 0
            r8 = 1
            r7 = 0
            com.wacai.e r0 = com.wacai.e.c()     // Catch:{ Exception -> 0x00c5, all -> 0x00eb }
            android.database.sqlite.SQLiteDatabase r0 = r0.b()     // Catch:{ Exception -> 0x00c5, all -> 0x00eb }
            java.lang.String r1 = "SELECT * from TBL_WEIBOINFO"
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ Exception -> 0x00c5, all -> 0x00eb }
            if (r0 == 0) goto L_0x001a
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            if (r1 != 0) goto L_0x0021
        L_0x001a:
            if (r0 == 0) goto L_0x001f
            r0.close()
        L_0x001f:
            r0 = r7
        L_0x0020:
            return r0
        L_0x0021:
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            com.wacai365.ll[] r1 = new com.wacai365.ll[r1]     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            r10.b = r1     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            r1 = r7
        L_0x002a:
            int r2 = r0.getCount()     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            if (r1 >= r2) goto L_0x00bd
            com.wacai365.ll r2 = new com.wacai365.ll     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            r2.<init>(r10)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = "id"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            long r3 = r0.getLong(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            r2.a = r3     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = "type"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            long r3 = r0.getLong(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            r2.b = r3     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = "name"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            r2.c = r3     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = "apk"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = com.wacai.a.h.a(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            r2.d = r3     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = "aps"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = com.wacai.a.h.a(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            r2.e = r3     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = "tk"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            r2.f = r3     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = "tks"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            r2.g = r3     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = "wac"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            r2.h = r3     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            java.lang.String r3 = "isrwc"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            long r3 = r0.getLong(r3)     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            r5 = 0
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x00bb
            r3 = r8
        L_0x00ae:
            r2.i = r3     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            com.wacai365.ll[] r3 = r10.b     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            r3[r1] = r2     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            r0.moveToNext()     // Catch:{ Exception -> 0x00fa, all -> 0x00f3 }
            int r1 = r1 + 1
            goto L_0x002a
        L_0x00bb:
            r3 = r7
            goto L_0x00ae
        L_0x00bd:
            if (r0 == 0) goto L_0x00c2
            r0.close()
        L_0x00c2:
            r0 = r8
            goto L_0x0020
        L_0x00c5:
            r0 = move-exception
            r1 = r3
        L_0x00c7:
            java.lang.String r2 = "WeiboCenter"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f8 }
            r3.<init>()     // Catch:{ all -> 0x00f8 }
            java.lang.String r4 = "init exception:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00f8 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00f8 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00f8 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f8 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00f8 }
            if (r1 == 0) goto L_0x00e8
            r1.close()
        L_0x00e8:
            r0 = r7
            goto L_0x0020
        L_0x00eb:
            r0 = move-exception
            r1 = r3
        L_0x00ed:
            if (r1 == 0) goto L_0x00f2
            r1.close()
        L_0x00f2:
            throw r0
        L_0x00f3:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00ed
        L_0x00f8:
            r0 = move-exception
            goto L_0x00ed
        L_0x00fa:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00c7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.aaj.d():boolean");
    }

    public final ll a(long j) {
        if (j <= 0 || (this.b == null && !d())) {
            return null;
        }
        for (ll llVar : this.b) {
            if (llVar.b == j) {
                return llVar;
            }
        }
        return null;
    }

    public final ll[] b() {
        if (this.b == null) {
            d();
        }
        return this.b;
    }

    public final void c() {
        this.b = null;
        d();
    }
}
