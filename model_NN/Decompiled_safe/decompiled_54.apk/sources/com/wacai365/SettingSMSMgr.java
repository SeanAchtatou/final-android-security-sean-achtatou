package com.wacai365;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import com.wacai.a;

public class SettingSMSMgr extends WacaiActivity implements CompoundButton.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public Button a;
    /* access modifiers changed from: private */
    public Button b;
    /* access modifiers changed from: private */
    public Button c;
    /* access modifiers changed from: private */
    public RadioButton d = null;
    private RadioButton e = null;
    /* access modifiers changed from: private */
    public CheckBox f = null;
    private boolean g = false;
    private int h = 0;
    private ViewGroup i;
    private ViewGroup j;
    private View.OnClickListener k = new kl(this);

    private void a(boolean z) {
        this.j.setVisibility(z ? 0 : 8);
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        a(z);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.setting_sms);
        this.i = (ViewGroup) findViewById(C0000R.id.checkSMSItem);
        this.j = (ViewGroup) findViewById(C0000R.id.radioSMSItem);
        this.a = (Button) findViewById(C0000R.id.btnOK);
        this.a.setOnClickListener(this.k);
        this.b = (Button) findViewById(C0000R.id.btnCancel);
        this.b.setOnClickListener(this.k);
        this.c = (Button) findViewById(C0000R.id.btnWhiteList);
        this.c.setOnClickListener(this.k);
        this.d = (RadioButton) findViewById(C0000R.id.rbAll);
        this.d.setOnClickListener(this.k);
        this.e = (RadioButton) findViewById(C0000R.id.rbWhiteList);
        this.e.setOnClickListener(this.k);
        this.f = (CheckBox) findViewById(C0000R.id.cbNeedSMS);
        this.f.setOnCheckedChangeListener(this);
        this.h = (int) a.a("SMSFilterType", 0);
        switch (this.h) {
            case 0:
                this.d.setChecked(true);
                break;
            case 1:
                this.e.setChecked(true);
                break;
        }
        this.g = 0 != a.a("SMSFilter", 0);
        this.f.setChecked(this.g);
        a(this.g);
    }
}
