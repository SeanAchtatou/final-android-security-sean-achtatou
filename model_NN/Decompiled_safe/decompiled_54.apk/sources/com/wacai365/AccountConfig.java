package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import com.wacai.a;
import com.wacai.a.f;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.g;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.o;

public class AccountConfig extends WacaiActivity {
    /* access modifiers changed from: private */
    public EditText a;
    /* access modifiers changed from: private */
    public EditText b;
    /* access modifiers changed from: private */
    public Button c;
    /* access modifiers changed from: private */
    public Button d;
    /* access modifiers changed from: private */
    public View e;
    private CheckBox f;
    private long g;
    private Animation h;
    /* access modifiers changed from: private */
    public boolean i;
    private View.OnClickListener j = new mf(this);
    private DialogInterface.OnClickListener k = new me(this);

    static /* synthetic */ void a(AccountConfig accountConfig, String str, String str2) {
        if (!accountConfig.i || b.m().b() == null || b.m().b().length() <= 0 || str.equalsIgnoreCase(b.m().b())) {
            accountConfig.a(str, str2);
            return;
        }
        m.a(accountConfig, (int) C0000R.string.txtAlertTitleInfo, (int) C0000R.string.changeAccountPrompt, (int) C0000R.string.txtOK, (int) C0000R.string.txtCancel, accountConfig.k);
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2) {
        if (str != null && str2 != null) {
            b.m().a(str.trim());
            b.m().b(f.a(str2.trim()));
            b.m().c("");
            b.m().l();
            long j2 = this.f.isChecked() ? 1 : 0;
            if ((this.g & 1) != j2) {
                a.b("MultiPhoneAccount", j2 | 2);
            }
            setResult(-1, getIntent());
            finish();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void b(AccountConfig accountConfig) {
        c cVar;
        ft ftVar;
        String obj = accountConfig.a.getText().toString();
        String obj2 = accountConfig.b.getText().toString();
        if (obj == null || obj.trim().length() <= 0) {
            accountConfig.a.setText("");
            accountConfig.h = m.a(accountConfig, accountConfig.h, (int) C0000R.anim.shake, accountConfig.a, (int) C0000R.string.txtEmptyUserName);
        } else if (obj2 == null || obj2.trim().length() <= 0) {
            accountConfig.b.setText("");
            accountConfig.h = m.a(accountConfig, accountConfig.h, (int) C0000R.anim.shake, accountConfig.b, (int) C0000R.string.txtEmptyPassword);
        } else {
            if (!obj.equalsIgnoreCase(b.m().b()) || !f.a(obj2.trim()).equalsIgnoreCase(b.m().c())) {
                ft ftVar2 = new ft(accountConfig);
                ftVar2.d(false);
                ftVar2.b(accountConfig.i);
                ftVar2.c(false);
                ftVar2.e(false);
                ftVar2.a((int) C0000R.string.txtResetLocalPSWSucceedPrompt);
                ftVar2.a(new md(accountConfig, obj, obj2));
                c cVar2 = new c(ftVar2);
                l lVar = new l();
                lVar.b(obj);
                lVar.c(obj2);
                lVar.a(m.a());
                cVar2.a((o) lVar);
                cVar2.a((o) new g());
                if (accountConfig.f.isChecked()) {
                    l lVar2 = new l();
                    lVar2.a(m.a(1L));
                    lVar2.b(obj);
                    lVar2.c(obj2);
                    cVar2.a((o) lVar2);
                }
                cVar = cVar2;
                ftVar = ftVar2;
            } else {
                long j2 = accountConfig.f.isChecked() ? 1 : 0;
                if ((accountConfig.g & 1) != j2) {
                    a.b("MultiPhoneAccount", j2 | 2);
                }
                long j3 = accountConfig.f.isChecked() ? 1 : 0;
                if (accountConfig.g != j3) {
                    l lVar3 = new l();
                    lVar3.a(m.a(j3));
                    lVar3.b(obj);
                    lVar3.c(obj2);
                    lVar3.a(new mg(accountConfig));
                    ft ftVar3 = new ft(accountConfig);
                    ftVar3.d(false);
                    ftVar3.b(true);
                    ftVar3.c(false);
                    ftVar3.e(true);
                    ftVar3.a((int) C0000R.string.txtResetLocalPSWSucceedPrompt);
                    c cVar3 = new c(ftVar3);
                    cVar3.a((o) lVar3);
                    cVar3.a((o) new g());
                    c cVar4 = cVar3;
                    ftVar = ftVar3;
                    cVar = cVar4;
                } else {
                    accountConfig.setResult(-1, accountConfig.getIntent());
                    accountConfig.finish();
                    return;
                }
            }
            ftVar.a(cVar);
            ftVar.b(C0000R.string.networkProgress, C0000R.string.remoteVerifyUser);
            ftVar.a(true, false);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (-1 == i3 && intent != null) {
            switch (i2) {
                case 13:
                    setResult(-1, getIntent());
                    finish();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.i = getIntent().getBooleanExtra("Extra_AlertAccount", false);
        setContentView((int) C0000R.layout.account_config);
        this.a = (EditText) findViewById(C0000R.id.etUserName);
        this.a.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        this.b = (EditText) findViewById(C0000R.id.etPassword);
        if (b.m().b() != null) {
            this.a.setText(b.m().b());
        }
        this.f = (CheckBox) findViewById(C0000R.id.cbMultiPhoneAccount);
        this.g = a.a("MultiPhoneAccount", 0);
        this.f.setChecked((this.g & 1) > 0);
        this.c = (Button) findViewById(C0000R.id.btnSave);
        if (this.c != null) {
            this.c.setOnClickListener(this.j);
        }
        this.d = (Button) findViewById(C0000R.id.btnCancel);
        this.d.setOnClickListener(this.j);
        this.e = findViewById(C0000R.id.account_register);
        this.e.setOnClickListener(this.j);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }
}
