package com.wacai365;

import android.view.View;
import android.widget.CheckBox;
import com.wacai.e;

final class abp implements View.OnClickListener {
    private /* synthetic */ kh a;

    abp(kh khVar) {
        this.a = khVar;
    }

    public final void onClick(View view) {
        CheckBox checkBox = (CheckBox) view;
        int parseInt = Integer.parseInt((String) view.getTag());
        Object[] objArr = new Object[3];
        objArr[0] = this.a.a;
        objArr[1] = Integer.valueOf(checkBox.isChecked() ? 1 : 0);
        objArr[2] = Integer.valueOf(parseInt);
        e.c().b().execSQL(String.format("UPDATE %s SET star = %d WHERE id = %d", objArr));
        if (this.a.d != null) {
            this.a.d.a(parseInt, checkBox.isChecked());
        }
        if (this.a.e != null) {
            this.a.e.a(this.a.b, this.a.c);
        }
    }
}
