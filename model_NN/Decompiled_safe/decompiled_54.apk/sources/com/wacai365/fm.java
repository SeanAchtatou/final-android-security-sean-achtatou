package com.wacai365;

import android.content.Intent;
import android.view.View;

final class fm implements View.OnClickListener {
    private /* synthetic */ ShortcutsMgr a;

    fm(ShortcutsMgr shortcutsMgr) {
        this.a = shortcutsMgr;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.a)) {
            this.a.startActivityForResult(new Intent(this.a, InputShortcut.class), 0);
        } else if (view.equals(this.a.b)) {
            this.a.finish();
        }
    }
}
