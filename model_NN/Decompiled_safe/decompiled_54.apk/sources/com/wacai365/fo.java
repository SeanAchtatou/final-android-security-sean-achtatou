package com.wacai365;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.b;

public final class fo extends BaseAdapter {
    private Context a = null;
    private Cursor b = null;
    private LayoutInflater c = null;
    private Resources d = null;
    private qa e = null;
    private /* synthetic */ MyBalanceStatByMoneyType f;

    public fo(MyBalanceStatByMoneyType myBalanceStatByMoneyType, Context context, qa qaVar) {
        this.f = myBalanceStatByMoneyType;
        this.a = context;
        this.e = qaVar;
        this.d = this.a.getResources();
        this.c = (LayoutInflater) this.a.getSystemService("layout_inflater");
    }

    private void a(TextView textView, boolean z) {
        textView.setTextColor(this.d.getColor(z ? C0000R.color.income_money : C0000R.color.outgo_money));
    }

    private static boolean a(int i) {
        switch (i) {
            case 0:
            case 3:
            case 6:
                return true;
            default:
                return false;
        }
    }

    public final boolean areAllItemsEnabled() {
        return false;
    }

    public final int getCount() {
        return 11;
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final int getItemViewType(int i) {
        return a(i) ? 0 : 1;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        boolean a2 = a(i);
        if (view == null) {
            view2 = this.c.inflate(a2 ? C0000R.layout.balance_list_time_item : C0000R.layout.list_item_withicon, (ViewGroup) null);
        } else {
            view2 = view;
        }
        if (!a2) {
            TextView textView = (TextView) view2.findViewById(C0000R.id.headertitle);
            TextView textView2 = (TextView) view2.findViewById(C0000R.id.headervalue);
            LinearLayout linearLayout = (LinearLayout) view2.findViewById(C0000R.id.id_icon);
            if (!(textView == null || textView2 == null || linearLayout == null)) {
                String str = b.a ? this.e.b : "";
                switch (i) {
                    case 1:
                        textView.setText(this.d.getString(C0000R.string.txtOutgoString));
                        a(textView2, false);
                        textView2.setText(str + m.a(m.a(this.e.e), 2));
                        linearLayout.setBackgroundResource(C0000R.drawable.ic_outgo);
                        break;
                    case 2:
                        textView.setText(this.d.getString(C0000R.string.txtIncomeString));
                        a(textView2, true);
                        textView2.setText(str + m.a(m.a(this.e.f), 2));
                        linearLayout.setBackgroundResource(C0000R.drawable.ic_income);
                        break;
                    case 4:
                        textView.setText(this.d.getString(C0000R.string.txtTransferOut));
                        a(textView2, false);
                        textView2.setText(str + m.a(m.a(this.e.h), 2));
                        linearLayout.setBackgroundResource(C0000R.drawable.ic_transout);
                        break;
                    case 5:
                        textView.setText(this.d.getString(C0000R.string.txtTransferIn));
                        a(textView2, true);
                        textView2.setText(str + m.a(m.a(this.e.g), 2));
                        linearLayout.setBackgroundResource(C0000R.drawable.ic_transin);
                        break;
                    case 7:
                        textView.setText(this.d.getString(C0000R.string.txtLoanReceivable));
                        a(textView2, false);
                        textView2.setText(str + m.a(m.a(this.e.i), 2));
                        linearLayout.setBackgroundResource(C0000R.drawable.ic_loanout);
                        break;
                    case 8:
                        textView.setText(this.d.getString(C0000R.string.txtMakeCollections));
                        a(textView2, true);
                        textView2.setText(str + m.a(m.a(this.e.k), 2));
                        linearLayout.setBackgroundResource(C0000R.drawable.ic_receipt);
                        break;
                    case 9:
                        textView.setText(this.d.getString(C0000R.string.txtBorrowPayable));
                        a(textView2, true);
                        textView2.setText(str + m.a(m.a(this.e.j), 2));
                        linearLayout.setBackgroundResource(C0000R.drawable.ic_loanin);
                        break;
                    case 10:
                        textView.setText(this.d.getString(C0000R.string.txtRepayment));
                        a(textView2, false);
                        textView2.setText(str + m.a(m.a(this.e.l), 2));
                        linearLayout.setBackgroundResource(C0000R.drawable.ic_repayment);
                        break;
                }
            }
        } else {
            TextView textView3 = (TextView) view2.findViewById(C0000R.id.datetitle);
            if (textView3 != null) {
                switch (i) {
                    case 0:
                        textView3.setText(this.d.getString(C0000R.string.txtIOCountTitle));
                        break;
                    case 3:
                        textView3.setText(this.d.getString(C0000R.string.txtTRCountTitle));
                        break;
                    case 6:
                        textView3.setText(this.d.getString(C0000R.string.txtLOANCountTitle));
                        break;
                }
            }
        }
        return view2;
    }

    public final int getViewTypeCount() {
        return 2;
    }

    public final boolean isEnabled(int i) {
        return false;
    }
}
