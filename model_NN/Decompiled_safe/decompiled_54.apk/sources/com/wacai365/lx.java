package com.wacai365;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.wacai.b;

final class lx implements AdapterView.OnItemClickListener {
    final /* synthetic */ DataMgrTab a;

    lx(DataMgrTab dataMgrTab) {
        this.a = dataMgrTab;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void
     arg types: [com.wacai365.DataMgrTab, java.lang.String, int, ?[OBJECT, ARRAY]]
     candidates:
      com.wacai365.m.a(android.view.animation.Animation, int, android.view.View, java.lang.String):android.view.animation.Animation
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, java.util.ArrayList, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, int[], android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.app.ProgressDialog, boolean, boolean, android.content.DialogInterface$OnClickListener):com.wacai365.tp
      com.wacai365.m.a(android.content.Context, long, android.content.DialogInterface$OnClickListener, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, long, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, boolean, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        try {
            fr frVar = (fr) this.a.a.getItemAtPosition(i);
            if (frVar != null) {
                if (C0000R.string.txtHardwareReset == frVar.a) {
                    String b = b.m().b();
                    m.a(this.a, (b == null || b.length() <= 0) ? C0000R.string.resetAnonyPrompt : C0000R.string.resetRegPrompt, new eo(this));
                } else if (C0000R.string.txtUploadTitle == frVar.a) {
                    DataMgrTab.b(this.a);
                } else if (C0000R.string.txtDatabaseToSD == frVar.a) {
                    try {
                        abw.a(this.a);
                    } catch (Exception e) {
                        m.a((Context) this.a, this.a.getString(C0000R.string.txtExceptionOper) + e.getMessage(), false, (DialogInterface.OnClickListener) null);
                    }
                } else if (C0000R.string.txtLocalDataSync == frVar.a) {
                    try {
                        abw.b(this.a);
                    } catch (Exception e2) {
                        m.a((Context) this.a, this.a.getString(C0000R.string.txtExceptionOper) + e2.getMessage(), false, (DialogInterface.OnClickListener) null);
                    }
                } else {
                    Intent intent = new Intent(this.a, Class.forName(frVar.c));
                    if (frVar.c.equals("com.wacai365.AccountConfig") || frVar.c.equals("com.wacai365.AccountRegister")) {
                        intent.putExtra("Extra_AlertAccount", true);
                        intent.putExtra("Extra_Anonymous", false);
                    }
                    intent.putExtra("LaunchedByApplication", 1);
                    this.a.startActivityForResult(intent, 0);
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }
}
