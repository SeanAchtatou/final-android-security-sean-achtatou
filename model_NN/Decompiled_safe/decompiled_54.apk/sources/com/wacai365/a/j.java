package com.wacai365.a;

import android.util.Log;
import com.wacai.e;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;

public final class j {
    private Random a = new Random();

    private static String a() {
        long currentTimeMillis;
        StringBuilder sb = new StringBuilder(50);
        sb.append(e.a);
        sb.append("/hibrid/now.jsp");
        HttpPost httpPost = new HttpPost(sb.toString());
        httpPost.getParams().setParameter("http.socket.timeout", new Integer(60000));
        try {
            HttpResponse a2 = com.wacai.a.e.a(URI.create(e.a), httpPost, 60000);
            currentTimeMillis = a2.getStatusLine().getStatusCode() == 200 ? Long.valueOf(EntityUtils.toString(a2.getEntity(), "UTF-8").trim()).longValue() : System.currentTimeMillis();
        } catch (Exception e) {
            currentTimeMillis = System.currentTimeMillis();
        }
        return String.valueOf(currentTimeMillis / 1000);
    }

    private static String a(String str) {
        String str2 = null;
        try {
            str2 = URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        StringBuffer stringBuffer = new StringBuffer(str2.length());
        int i = 0;
        while (i < str2.length()) {
            char charAt = str2.charAt(i);
            if (charAt == '*') {
                stringBuffer.append("%2A");
            } else if (charAt == '+') {
                stringBuffer.append("%20");
            } else if (charAt == '%' && i + 1 < str2.length() && str2.charAt(i + 1) == '7' && str2.charAt(i + 2) == 'E') {
                stringBuffer.append('~');
                i += 2;
            } else {
                stringBuffer.append(charAt);
            }
            i++;
        }
        return stringBuffer.toString();
    }

    private static String a(URL url, String str, String str2, String str3, List list, StringBuffer stringBuffer, StringBuffer stringBuffer2) {
        byte[] bArr;
        Collections.sort(list);
        stringBuffer.append(url.getProtocol());
        stringBuffer.append("://");
        stringBuffer.append(url.getHost());
        if ((url.getProtocol().equals("http") || url.getProtocol().equals("https")) && url.getPort() != -1) {
            stringBuffer.append(":");
            stringBuffer.append(url.getPort());
        }
        stringBuffer.append(url.getPath());
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            h hVar = (h) it.next();
            arrayList.add(new h(hVar.a, a(hVar.b)));
        }
        stringBuffer2.append(a(arrayList));
        StringBuffer stringBuffer3 = new StringBuffer();
        stringBuffer3.append(str3.toUpperCase());
        stringBuffer3.append("&");
        stringBuffer3.append(a(stringBuffer.toString()));
        stringBuffer3.append("&");
        stringBuffer3.append(a(stringBuffer2.toString()));
        String stringBuffer4 = stringBuffer3.toString();
        try {
            Mac instance = Mac.getInstance("HmacSHA1");
            instance.init(new SecretKeySpec((a(str) + "&" + ((str2 == null || str2.equals("")) ? "" : a(str2))).getBytes("US-ASCII"), "HmacSHA1"));
            bArr = instance.doFinal(stringBuffer4.getBytes("US-ASCII"));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            bArr = null;
        } catch (InvalidKeyException e2) {
            e2.printStackTrace();
            bArr = null;
        } catch (IllegalStateException e3) {
            e3.printStackTrace();
            bArr = null;
        } catch (UnsupportedEncodingException e4) {
            e4.printStackTrace();
            bArr = null;
        }
        return f.a(bArr);
    }

    private static String a(List list) {
        StringBuffer stringBuffer = new StringBuffer();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            h hVar = (h) list.get(i);
            stringBuffer.append(hVar.a);
            stringBuffer.append("=");
            stringBuffer.append(hVar.b);
            if (i < size - 1) {
                stringBuffer.append("&");
            }
        }
        return stringBuffer.toString();
    }

    public final String a(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, List list, StringBuffer stringBuffer) {
        URL url;
        String a2 = a(list);
        try {
            url = new URL((a2 == null || a2.equals("")) ? str : str + "?" + a2);
        } catch (MalformedURLException e) {
            Log.e("wacai", "URL parse error:" + e.getLocalizedMessage());
            url = null;
        }
        String valueOf = String.valueOf(this.a.nextInt(9876599) + 123400);
        String a3 = a();
        list.add(new h("oauth_version", "1.0"));
        list.add(new h("oauth_nonce", valueOf));
        list.add(new h("oauth_timestamp", a3));
        list.add(new h("oauth_signature_method", "HMAC-SHA1"));
        list.add(new h("oauth_consumer_key", str3));
        if (str5 != null && !str5.equals("")) {
            list.add(new h("oauth_token", str5));
        }
        if (str7 != null && !str7.equals("")) {
            list.add(new h("oauth_verifier", str7));
        }
        if (str8 != null && !str8.equals("")) {
            list.add(new h("oauth_callback", str8));
        }
        StringBuffer stringBuffer2 = new StringBuffer();
        String a4 = a(url, str4, str6, str2, list, stringBuffer2, stringBuffer);
        stringBuffer.append("&oauth_signature=");
        stringBuffer.append(a(a4));
        return stringBuffer2.toString();
    }
}
