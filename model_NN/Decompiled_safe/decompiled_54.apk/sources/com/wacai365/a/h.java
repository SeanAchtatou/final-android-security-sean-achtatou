package com.wacai365.a;

public final class h implements Comparable {
    public String a;
    public String b;

    public h(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
        h hVar = (h) obj;
        int compareTo = this.a.compareTo(hVar.a);
        return compareTo == 0 ? this.b.compareTo(hVar.b) : compareTo;
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof h)) {
            return false;
        }
        h hVar = (h) obj;
        return this.a.equals(hVar.a) && this.b.equals(hVar.b);
    }
}
