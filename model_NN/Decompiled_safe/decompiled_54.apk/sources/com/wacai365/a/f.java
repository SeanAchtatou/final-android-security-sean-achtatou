package com.wacai365.a;

public final class f {
    private static final char a = ((char) Integer.parseInt("00000011", 2));
    private static final char b = ((char) Integer.parseInt("00001111", 2));
    private static final char c = ((char) Integer.parseInt("00111111", 2));
    private static final char d = ((char) Integer.parseInt("11111100", 2));
    private static final char e = ((char) Integer.parseInt("11110000", 2));
    private static final char f = ((char) Integer.parseInt("11000000", 2));
    private static final char[] g = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

    public static String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer(((int) (((double) bArr.length) * 1.34d)) + 3);
        char c2 = 0;
        int i = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            i %= 8;
            while (i < 8) {
                switch (i) {
                    case 0:
                        c2 = (char) (((char) (bArr[i2] & d)) >>> 2);
                        break;
                    case 2:
                        c2 = (char) (bArr[i2] & c);
                        break;
                    case 4:
                        c2 = (char) (((char) (bArr[i2] & b)) << 2);
                        if (i2 + 1 >= bArr.length) {
                            break;
                        } else {
                            c2 = (char) (c2 | ((bArr[i2 + 1] & f) >>> 6));
                            break;
                        }
                    case 6:
                        c2 = (char) (((char) (bArr[i2] & a)) << 4);
                        if (i2 + 1 >= bArr.length) {
                            break;
                        } else {
                            c2 = (char) (c2 | ((bArr[i2 + 1] & e) >>> 4));
                            break;
                        }
                }
                stringBuffer.append(g[c2]);
                i += 6;
            }
        }
        if (stringBuffer.length() % 4 != 0) {
            for (int length = 4 - (stringBuffer.length() % 4); length > 0; length--) {
                stringBuffer.append("=");
            }
        }
        return stringBuffer.toString();
    }
}
