package com.wacai365.a;

import android.content.Context;
import android.content.res.Resources;
import com.wacai.a.e;
import com.wacai365.C0000R;
import com.wacai365.MyApp;
import com.wacai365.ll;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.util.EntityUtils;

public abstract class c {
    protected boolean a = false;
    protected boolean b = false;
    protected g c = new g();

    public c(String str, String str2, String str3, String str4) {
        this.c.a = str;
        this.c.b = str2;
        if (str3 != null && str3.length() > 0 && str4 != null && str4.length() > 0) {
            this.c.c = str3;
            this.c.d = str4;
            this.c.f = null;
            this.a = true;
            this.b = true;
        }
    }

    public static c a(ll llVar) {
        if (llVar == null) {
            return null;
        }
        if (llVar.b < 0) {
            return null;
        }
        switch ((int) llVar.b) {
            case 1:
                return new a(llVar.d, llVar.e, llVar.f, llVar.g);
            case 2:
                return new d(llVar.d, llVar.e, llVar.f, llVar.g);
            default:
                return null;
        }
    }

    private static String a(h[] hVarArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < hVarArr.length; i++) {
            if (i != 0) {
                stringBuffer.append("&");
            }
            try {
                stringBuffer.append(URLEncoder.encode(hVarArr[i].a, "UTF-8")).append("=").append(URLEncoder.encode(hVarArr[i].b, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
            }
        }
        return stringBuffer.toString();
    }

    protected static String a(String[] strArr, String str) {
        for (String str2 : strArr) {
            if (str2.startsWith(str + '=')) {
                return str2.split("=")[1].trim();
            }
        }
        return null;
    }

    private void b(int i, String str) {
        if (200 == i && str != null && (!this.a || !this.b)) {
            String[] split = str.split("&");
            this.c.d = a(split, "oauth_token_secret");
            this.c.c = a(split, "oauth_token");
        }
        if (!a(i, str)) {
            String str2 = null;
            Resources resources = MyApp.b.getResources();
            switch (i) {
                case -1:
                    str2 = resources.getString(C0000R.string.networkTimeout);
                    break;
                case 400:
                    str2 = resources.getString(C0000R.string.weiboErrorBadRequest);
                    break;
                case 401:
                    str2 = resources.getString(C0000R.string.weiboErrorNotAuth);
                    break;
                case 403:
                    str2 = resources.getString(C0000R.string.weiboErrorForbidden);
                    break;
                case 404:
                    str2 = resources.getString(C0000R.string.weiboErrorNotFound);
                    break;
                case 406:
                    str2 = resources.getString(C0000R.string.weiboErrorNotAcceptable);
                    break;
                case 500:
                    str2 = resources.getString(C0000R.string.weiboErrorServerInternal);
                    break;
                case 502:
                    str2 = resources.getString(C0000R.string.weiboErrorBadGateway);
                    break;
                case 503:
                    str2 = resources.getString(C0000R.string.weiboErrorBusy);
                    break;
            }
            if (str2 != null) {
                throw new Exception(str2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final b a(String str, String str2, boolean z) {
        HttpGet httpGet;
        URI uri = new URI(str);
        String str3 = uri.getScheme() + "://" + uri.getHost();
        b bVar = new b(this);
        if (z) {
            httpGet = new HttpGet(str);
            httpGet.setHeader("Authorization", str2);
        } else {
            httpGet = new HttpGet(uri.getPath() + '?' + str2);
        }
        HttpResponse a2 = e.a(URI.create(str3), httpGet, 60000);
        bVar.a = a2.getStatusLine().getStatusCode();
        bVar.b = EntityUtils.toString(a2.getEntity(), "UTF-8");
        b(bVar.a, bVar.b);
        return bVar;
    }

    /* access modifiers changed from: protected */
    public final b a(String str, String str2, boolean z, h[] hVarArr, InputStream inputStream) {
        HttpPost httpPost;
        if (str2 == null || str2.equals("")) {
            return null;
        }
        URI uri = new URI(str);
        String str3 = uri.getScheme() + "://" + uri.getHost();
        b bVar = new b(this);
        if (z) {
            HttpPost httpPost2 = new HttpPost(uri.getPath());
            httpPost2.setHeader("Authorization", str2);
            httpPost = httpPost2;
        } else {
            httpPost = new HttpPost(uri.getPath() + '?' + str2);
        }
        httpPost.setHeader("Charset", "UTF-8");
        httpPost.setHeader("Connection", "Keep-Alive");
        if (inputStream == null) {
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
            httpPost.setEntity(new ByteArrayEntity((hVarArr != null ? a(hVarArr) : null).getBytes("UTF-8")));
        } else {
            httpPost.setEntity(new i(inputStream, hVarArr));
        }
        HttpResponse a2 = e.a(URI.create(str3), httpPost, 60000);
        bVar.a = a2.getStatusLine().getStatusCode();
        bVar.b = EntityUtils.toString(a2.getEntity(), "UTF-8");
        b(bVar.a, bVar.b);
        return bVar;
    }

    public abstract String a();

    public abstract String a(Context context);

    public abstract void a(String str, InputStream inputStream);

    /* access modifiers changed from: protected */
    public abstract boolean a(int i, String str);

    public abstract boolean a(String str);

    public final String b() {
        if (this.b) {
            return this.c.c;
        }
        return null;
    }

    public abstract String b(Context context);

    public abstract boolean b(String str);

    public final String c() {
        if (this.b) {
            return this.c.d;
        }
        return null;
    }

    public abstract boolean c(String str);
}
