package com.wacai365.a;

import android.content.Context;
import com.wacai.a.e;
import com.wacai365.C0000R;
import com.wacai365.MyApp;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;

public final class a extends c {
    private static Random e = new Random();
    private String d;

    public a(String str, String str2, String str3, String str4) {
        super(str, str2, str3, str4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.a.a.a(java.util.List, java.lang.String, boolean):java.lang.String
     arg types: [java.util.ArrayList, java.lang.String, int]
     candidates:
      com.wacai365.a.a.a(java.lang.String, java.lang.String, com.wacai365.a.h[]):java.lang.String
      com.wacai365.a.c.a(java.lang.String, java.lang.String, boolean):com.wacai365.a.b
      com.wacai365.a.a.a(java.util.List, java.lang.String, boolean):java.lang.String */
    private String a(String str, String str2, h[] hVarArr) {
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        String valueOf = String.valueOf(((long) e.nextInt()) + currentTimeMillis);
        String valueOf2 = String.valueOf(currentTimeMillis);
        h[] hVarArr2 = hVarArr == null ? new h[0] : hVarArr;
        ArrayList arrayList = new ArrayList(5);
        arrayList.add(new h("oauth_consumer_key", this.c.a));
        arrayList.add(new h("oauth_signature_method", "HMAC-SHA1"));
        arrayList.add(new h("oauth_timestamp", valueOf2));
        arrayList.add(new h("oauth_nonce", valueOf));
        arrayList.add(new h("oauth_version", "1.0"));
        if (this.c.c != null) {
            arrayList.add(new h("oauth_token", this.c.c));
        }
        ArrayList arrayList2 = new ArrayList(arrayList.size() + hVarArr2.length);
        arrayList2.addAll(arrayList);
        ArrayList arrayList3 = new ArrayList(hVarArr2.length);
        arrayList3.addAll(Arrays.asList(hVarArr2));
        arrayList2.addAll(arrayList3);
        a(str2, arrayList2);
        StringBuffer append = new StringBuffer(str).append("&");
        int indexOf = str2.indexOf("?");
        String substring = -1 != indexOf ? str2.substring(0, indexOf) : str2;
        int indexOf2 = substring.indexOf("/", 8);
        String lowerCase = substring.substring(0, indexOf2).toLowerCase();
        int indexOf3 = lowerCase.indexOf(":", 8);
        if (-1 != indexOf3) {
            if (lowerCase.startsWith("http://") && lowerCase.endsWith(":80")) {
                lowerCase = lowerCase.substring(0, indexOf3);
            } else if (lowerCase.startsWith("https://") && lowerCase.endsWith(":443")) {
                lowerCase = lowerCase.substring(0, indexOf3);
            }
        }
        StringBuffer append2 = append.append(e(lowerCase + substring.substring(indexOf2))).append("&");
        Collections.sort(arrayList2);
        append2.append(e(a((List) arrayList2, "&", false)));
        arrayList.add(new h("oauth_signature", d(append2.toString())));
        return "OAuth " + a((List) arrayList, ",", true);
    }

    private static String a(List list, String str, boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            h hVar = (h) it.next();
            if (stringBuffer.length() != 0) {
                if (z) {
                    stringBuffer.append("\"");
                }
                stringBuffer.append(str);
            }
            stringBuffer.append(e(hVar.a)).append("=");
            if (z) {
                stringBuffer.append("\"");
            }
            stringBuffer.append(e(hVar.b));
        }
        if (stringBuffer.length() != 0 && z) {
            stringBuffer.append("\"");
        }
        return stringBuffer.toString();
    }

    private static void a(String str, List list) {
        int indexOf = str.indexOf("?");
        if (-1 != indexOf) {
            try {
                for (String split : str.substring(indexOf + 1).split("&")) {
                    String[] split2 = split.split("=");
                    if (split2.length == 2) {
                        list.add(new h(URLDecoder.decode(split2[0], "UTF-8"), URLDecoder.decode(split2[1], "UTF-8")));
                    } else {
                        list.add(new h(URLDecoder.decode(split2[0], "UTF-8"), ""));
                    }
                }
            } catch (UnsupportedEncodingException e2) {
            }
        }
    }

    private boolean a(boolean z, String str) {
        if (!this.b || str == null || str.equals("")) {
            return false;
        }
        StringBuffer stringBuffer = new StringBuffer(z ? "http://api.t.sina.com.cn/friendships/create/" : "http://api.t.sina.com.cn/friendships/destroy/");
        stringBuffer.append(str).append(".json");
        String stringBuffer2 = stringBuffer.toString();
        h[] hVarArr = new h[0];
        h[] a = a(hVarArr, hVarArr.length + 1);
        a[hVarArr.length] = new h("source", this.c.a);
        a(stringBuffer2, a("POST", stringBuffer2, a), true, a, null);
        return true;
    }

    private static h[] a(h[] hVarArr, int i) {
        h[] hVarArr2 = new h[i];
        int length = hVarArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            hVarArr2[i2] = hVarArr[i2];
        }
        return hVarArr2;
    }

    private String d(String str) {
        byte[] bArr = null;
        try {
            Mac instance = Mac.getInstance("HMAC-SHA1");
            instance.init(this.c.c == null ? new SecretKeySpec((e(this.c.b) + "&").getBytes(), "HMAC-SHA1") : new SecretKeySpec((e(this.c.b) + "&" + e(this.c.d)).getBytes(), "HMAC-SHA1"));
            bArr = instance.doFinal(str.getBytes());
        } catch (InvalidKeyException e2) {
            e2.printStackTrace();
        } catch (NoSuchAlgorithmException e3) {
        }
        return f.a(bArr);
    }

    private static String e(String str) {
        String str2 = null;
        try {
            str2 = URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
        }
        StringBuffer stringBuffer = new StringBuffer(str2.length());
        int i = 0;
        while (i < str2.length()) {
            char charAt = str2.charAt(i);
            if (charAt == '*') {
                stringBuffer.append("%2A");
            } else if (charAt == '+') {
                stringBuffer.append("%20");
            } else if (charAt == '%' && i + 1 < str2.length() && str2.charAt(i + 1) == '7' && str2.charAt(i + 2) == 'E') {
                stringBuffer.append('~');
                i += 2;
            } else {
                stringBuffer.append(charAt);
            }
            i++;
        }
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.a.c.a(java.lang.String, java.lang.String, boolean):com.wacai365.a.b
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.wacai365.a.a.a(java.lang.String, java.lang.String, com.wacai365.a.h[]):java.lang.String
      com.wacai365.a.a.a(java.util.List, java.lang.String, boolean):java.lang.String
      com.wacai365.a.c.a(java.lang.String, java.lang.String, boolean):com.wacai365.a.b */
    public final String a() {
        if (this.d != null) {
            return this.d;
        }
        if (!this.a) {
            a("http://api.t.sina.com.cn/oauth/request_token", a("GET", "http://api.t.sina.com.cn/oauth/request_token", (h[]) null), true);
            this.a = true;
        }
        this.d = "http://api.t.sina.com.cn/oauth/authorize?oauth_token=" + this.c.c;
        return this.d;
    }

    public final String a(Context context) {
        return context.getResources().getString(C0000R.string.weiboWacaiNickname);
    }

    public final void a(String str, InputStream inputStream) {
        if (!this.b || this.c.c == null || this.c.d == null) {
            throw new Exception(MyApp.b.getResources().getString(C0000R.string.weiboErrorNotConnect));
        } else if (str == null && inputStream == null) {
            throw new Exception(MyApp.b.getResources().getString(C0000R.string.txtParamError));
        } else if (inputStream == null) {
            h[] hVarArr = {new h("status", str)};
            a("http://api.t.sina.com.cn/statuses/update.json", a("POST", "http://api.t.sina.com.cn/statuses/update.json", hVarArr), true, hVarArr, null);
        } else {
            h[] hVarArr2 = {new h("status", str), new h("source", this.c.a)};
            a("http://api.t.sina.com.cn/statuses/upload.json", a("POST", "http://api.t.sina.com.cn/statuses/upload.json", hVarArr2), true, hVarArr2, inputStream);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00db A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(int r9, java.lang.String r10) {
        /*
            r8 = this;
            r7 = 1
            r6 = 0
            if (r10 == 0) goto L_0x00d9
            android.content.Context r0 = com.wacai365.MyApp.b
            android.content.res.Resources r0 = r0.getResources()
            java.lang.String r1 = ","
            java.lang.String[] r1 = r10.split(r1)
            r2 = r6
        L_0x0011:
            int r3 = r1.length
            if (r2 >= r3) goto L_0x00d9
            r3 = r1[r2]
            java.lang.String r4 = "\"error\""
            boolean r3 = r3.startsWith(r4)
            if (r3 == 0) goto L_0x00d5
            java.io.PrintStream r3 = java.lang.System.out
            r4 = r1[r2]
            r3.println(r4)
            r3 = r1[r2]
            java.lang.String r4 = "}"
            java.lang.String r5 = ""
            java.lang.String r3 = r3.replace(r4, r5)
            r1[r2] = r3
            r3 = r1[r2]
            java.lang.String r4 = "\""
            java.lang.String r5 = ""
            java.lang.String r3 = r3.replace(r4, r5)
            r1[r2] = r3
            r3 = r1[r2]
            java.lang.String r4 = "{"
            java.lang.String r5 = ""
            java.lang.String r3 = r3.replace(r4, r5)
            r1[r2] = r3
            r3 = r1[r2]
            java.lang.String r4 = "\n"
            java.lang.String r5 = ""
            java.lang.String r3 = r3.replace(r4, r5)
            r1[r2] = r3
            r3 = r1[r2]
            java.lang.String r4 = "\r"
            java.lang.String r5 = ""
            java.lang.String r3 = r3.replace(r4, r5)
            r1[r2] = r3
            java.io.PrintStream r3 = java.lang.System.out
            r4 = r1[r2]
            r3.println(r4)
            r1 = r1[r2]
            java.lang.String r2 = ":"
            java.lang.String[] r1 = r1.split(r2)
            r2 = r6
        L_0x0071:
            int r3 = r1.length
            if (r2 >= r3) goto L_0x00d0
            r3 = r1[r2]
            java.lang.String r4 = "40025"
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x0088
            r3 = r1[r2]
            java.lang.String r4 = "40028"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0097
        L_0x0088:
            r1 = 2131296897(0x7f090281, float:1.8211724E38)
            java.lang.String r0 = r0.getString(r1)
        L_0x008f:
            if (r0 == 0) goto L_0x00db
            java.lang.Exception r1 = new java.lang.Exception
            r1.<init>(r0)
            throw r1
        L_0x0097:
            r3 = r1[r2]
            java.lang.String r4 = "40013"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00a9
            r1 = 2131296912(0x7f090290, float:1.8211754E38)
            java.lang.String r0 = r0.getString(r1)
            goto L_0x008f
        L_0x00a9:
            r3 = r1[r2]
            java.lang.String r4 = "40012"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00bb
            r1 = 2131296898(0x7f090282, float:1.8211726E38)
            java.lang.String r0 = r0.getString(r1)
            goto L_0x008f
        L_0x00bb:
            r3 = r1[r2]
            java.lang.String r4 = "40045"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00cd
            r1 = 2131296921(0x7f090299, float:1.8211772E38)
            java.lang.String r0 = r0.getString(r1)
            goto L_0x008f
        L_0x00cd:
            int r2 = r2 + 1
            goto L_0x0071
        L_0x00d0:
            int r0 = r1.length
            int r0 = r0 - r7
            r0 = r1[r0]
            goto L_0x008f
        L_0x00d5:
            int r2 = r2 + 1
            goto L_0x0011
        L_0x00d9:
            r0 = 0
            goto L_0x008f
        L_0x00db:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.a.a.a(int, java.lang.String):boolean");
    }

    public final boolean a(String str) {
        String str2;
        if (!this.b && this.a && this.c.c != null && this.c.d != null) {
            String str3 = this.d;
            if (this.b || str3 == null) {
                str2 = null;
            } else {
                HttpGet httpGet = new HttpGet(str3);
                URI uri = new URI(str3);
                HttpResponse a = e.a(URI.create(uri.getScheme() + "://" + uri.getHost()), httpGet, 60000);
                if (a.getStatusLine().getStatusCode() == 200) {
                    Matcher matcher = Pattern.compile(".*授权码.*[0-9]{6}").matcher(EntityUtils.toString(a.getEntity(), "UTF-8"));
                    if (matcher.find()) {
                        Matcher matcher2 = Pattern.compile("[0-9]{6}").matcher(matcher.group());
                        if (matcher2.find()) {
                            str2 = matcher2.group();
                        }
                    }
                }
                str2 = null;
            }
            if (str2 != null) {
                h[] hVarArr = {new h("oauth_verifier", str2), new h("source", this.c.a)};
                a("http://api.t.sina.com.cn/oauth/access_token", a("POST", "http://api.t.sina.com.cn/oauth/access_token", hVarArr), true, hVarArr, null);
                this.b = true;
                return true;
            }
        }
        return false;
    }

    public final String b(Context context) {
        return context.getResources().getString(C0000R.string.weiboWacaiNickname);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.a.a.a(boolean, java.lang.String):boolean
     arg types: [int, java.lang.String]
     candidates:
      com.wacai365.a.a.a(java.lang.String, java.util.List):void
      com.wacai365.a.a.a(com.wacai365.a.h[], int):com.wacai365.a.h[]
      com.wacai365.a.a.a(java.lang.String, java.io.InputStream):void
      com.wacai365.a.a.a(int, java.lang.String):boolean
      com.wacai365.a.c.a(java.lang.String[], java.lang.String):java.lang.String
      com.wacai365.a.c.a(java.lang.String, java.io.InputStream):void
      com.wacai365.a.c.a(int, java.lang.String):boolean
      com.wacai365.a.a.a(boolean, java.lang.String):boolean */
    public final boolean b(String str) {
        String str2;
        h[] hVarArr = {new h("target_id", str), new h("source", this.c.a)};
        String str3 = a("http://api.t.sina.com.cn/friendships/show.json", a("POST", "http://api.t.sina.com.cn/friendships/show.json", hVarArr), true, hVarArr, null).b;
        String a = a(str3.split("&"), "following");
        if (a == null) {
            String[] split = str3.split(",");
            int i = 0;
            while (true) {
                if (i >= split.length) {
                    break;
                }
                if (split[i].contains("\"")) {
                    split[i] = split[i].replaceAll("\"", "");
                }
                if (split[i].contains("{")) {
                    split[i] = split[i].replace("{", "");
                }
                if (split[i].contains("}")) {
                    split[i] = split[i].replace("}", "");
                }
                if (split[i].startsWith("following")) {
                    String[] split2 = split[i].split(":");
                    if (split2 != null && split2.length == 2) {
                        str2 = split2[1];
                    }
                } else {
                    i++;
                }
            }
        }
        str2 = a;
        if (!(str2 != null && str2.equals("true"))) {
            return a(true, str);
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.a.a.a(boolean, java.lang.String):boolean
     arg types: [int, java.lang.String]
     candidates:
      com.wacai365.a.a.a(java.lang.String, java.util.List):void
      com.wacai365.a.a.a(com.wacai365.a.h[], int):com.wacai365.a.h[]
      com.wacai365.a.a.a(java.lang.String, java.io.InputStream):void
      com.wacai365.a.a.a(int, java.lang.String):boolean
      com.wacai365.a.c.a(java.lang.String[], java.lang.String):java.lang.String
      com.wacai365.a.c.a(java.lang.String, java.io.InputStream):void
      com.wacai365.a.c.a(int, java.lang.String):boolean
      com.wacai365.a.a.a(boolean, java.lang.String):boolean */
    public final boolean c(String str) {
        return a(false, str);
    }
}
