package com.wacai365.a;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import org.apache.http.Header;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.message.BasicHeader;

public final class i extends AbstractHttpEntity {
    private static final byte[] a = "\r\n----------------et567z--\r\n".getBytes();
    private final int b = 2048;
    private InputStream c;
    private long d;
    private String e;
    private String f;

    public i(InputStream inputStream, h[] hVarArr) {
        int i;
        int i2;
        if (inputStream == null) {
            throw new IllegalArgumentException("stream cannot be null");
        }
        this.c = inputStream;
        try {
            i = inputStream.available();
        } catch (IOException e2) {
            i = 0;
        }
        this.d += (long) i;
        if (hVarArr != null) {
            StringBuilder sb = new StringBuilder();
            for (h hVar : hVarArr) {
                sb.append("--");
                sb.append("--------------et567z");
                sb.append("\r\n");
                sb.append("Content-Disposition: form-data; name=\"" + hVar.a + "\"\r\n\r\n");
                sb.append(hVar.b);
                sb.append("\r\n");
            }
            this.e = sb.toString();
            try {
                i2 = this.e.getBytes("UTF-8").length;
            } catch (UnsupportedEncodingException e3) {
                i2 = 0;
            }
            this.d += (long) i2;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("--");
        sb2.append("--------------et567z");
        sb2.append("\r\n");
        sb2.append("Content-Disposition: form-data;name=\"pic\";filename=\"WaCai\"\r\n");
        sb2.append("Content-Type: image/*\r\n\r\n");
        this.f = sb2.toString();
        this.d += (long) sb2.length();
        this.d += (long) a.length;
    }

    public final InputStream getContent() {
        return null;
    }

    public final long getContentLength() {
        return this.d;
    }

    public final Header getContentType() {
        return new BasicHeader("Content-Type", "Multipart/form-data; boundary=--------------et567z");
    }

    public final boolean isRepeatable() {
        return false;
    }

    public final boolean isStreaming() {
        return false;
    }

    public final void writeTo(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        if (this.e != null) {
            outputStream.write(this.e.getBytes());
        }
        outputStream.write(this.f.getBytes());
        byte[] bArr = new byte[2048];
        int i = 0;
        while (i != -1) {
            i = this.c.read(bArr, 0, 2048);
            if (i > 0) {
                outputStream.write(bArr, 0, i);
            }
        }
        outputStream.write(a);
        outputStream.flush();
    }
}
