package com.wacai365.a;

import android.content.Context;
import android.content.res.Resources;
import com.wacai.a.e;
import com.wacai365.C0000R;
import com.wacai365.MyApp;
import java.io.InputStream;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public final class d extends c {
    public d(String str, String str2, String str3, String str4) {
        super(str, str2, str3, str4);
        this.c.f = "null";
    }

    private String a(String str, h[] hVarArr, String str2) {
        ArrayList arrayList = new ArrayList();
        if (hVarArr != null) {
            for (h add : hVarArr) {
                arrayList.add(add);
            }
        }
        g gVar = this.c;
        j jVar = new j();
        StringBuffer stringBuffer = new StringBuffer();
        String a = jVar.a(str, str2, gVar.a, gVar.b, gVar.c, gVar.d, gVar.e, gVar.f, arrayList, stringBuffer);
        String stringBuffer2 = stringBuffer.toString();
        if (a == null || stringBuffer2 == null) {
            return null;
        }
        return stringBuffer2;
    }

    private static h[] d(String str) {
        return new h[]{new h("format", "json"), new h("content", str), new h("clientip", e.b())};
    }

    private e e(String str) {
        if (str == null) {
            return null;
        }
        e eVar = new e(this);
        try {
            JSONObject jSONObject = new JSONObject(new JSONTokener(str));
            eVar.a = jSONObject.getInt("ret");
            eVar.c = jSONObject.getInt("errcode");
            if (Integer.MIN_VALUE == eVar.c) {
                eVar.c = jSONObject.getInt("Errcode");
            }
            eVar.b = jSONObject.getString("msg");
            if (eVar.b != null) {
                return eVar;
            }
            eVar.b = jSONObject.getString("Msg");
            return eVar;
        } catch (JSONException e) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.a.c.a(java.lang.String, java.lang.String, boolean):com.wacai365.a.b
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.wacai365.a.d.a(java.lang.String, com.wacai365.a.h[], java.lang.String):java.lang.String
      com.wacai365.a.c.a(java.lang.String, java.lang.String, boolean):com.wacai365.a.b */
    public final String a() {
        if (!this.a) {
            a("https://open.t.qq.com/cgi-bin/request_token", a("https://open.t.qq.com/cgi-bin/request_token", (h[]) null, "GET"), false);
            this.a = true;
        }
        return "https://open.t.qq.com/cgi-bin/authorize?oauth_token=" + this.c.c;
    }

    public final String a(Context context) {
        return context.getResources().getString(C0000R.string.weiboWacaiNickname);
    }

    public final void a(String str, InputStream inputStream) {
        if (!this.b || this.c.c == null || this.c.d == null) {
            throw new Exception(MyApp.b.getResources().getString(C0000R.string.weiboErrorNotConnect));
        } else if (str == null && inputStream == null) {
            throw new Exception(MyApp.b.getResources().getString(C0000R.string.txtParamError));
        } else if (inputStream == null) {
            h[] d = d(str);
            a("http://open.t.qq.com/api/t/add", a("http://open.t.qq.com/api/t/add", d, "POST"), false, d, null);
        } else {
            h[] d2 = d(str);
            a("http://open.t.qq.com/api/t/add_pic", a("http://open.t.qq.com/api/t/add_pic", d2, "POST"), false, d2, inputStream);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(int i, String str) {
        int i2;
        if (200 != i) {
            return false;
        }
        e e = e(str);
        if (e != null) {
            Resources resources = MyApp.b.getResources();
            switch (e.c) {
                case 0:
                    i2 = -1;
                    break;
                case 1:
                case 2:
                case 3:
                case 6:
                case 7:
                case 10:
                case 11:
                default:
                    throw new Exception(e.b);
                case 4:
                    i2 = C0000R.string.weiboTencentErrorContentForbidden;
                    break;
                case 5:
                    i2 = C0000R.string.weiboTencentErrorAccess;
                    break;
                case 8:
                    i2 = -1;
                    break;
                case 9:
                    i2 = C0000R.string.weiboTencentErrorContentForbidden;
                    break;
                case 12:
                    i2 = C0000R.string.weiboTencentErrorChecking;
                    break;
                case 13:
                    i2 = C0000R.string.weiboErrorResend;
                    break;
            }
            if (-1 != i2) {
                throw new Exception(resources.getString(i2));
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.a.c.a(java.lang.String, java.lang.String, boolean):com.wacai365.a.b
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.wacai365.a.d.a(java.lang.String, com.wacai365.a.h[], java.lang.String):java.lang.String
      com.wacai365.a.c.a(java.lang.String, java.lang.String, boolean):com.wacai365.a.b */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.lang.String r10) {
        /*
            r9 = this;
            r8 = 0
            r7 = 1
            r6 = 0
            boolean r0 = r9.b
            if (r0 != 0) goto L_0x0071
            boolean r0 = r9.a
            if (r0 == 0) goto L_0x0071
            com.wacai365.a.g r0 = r9.c
            java.lang.String r0 = r0.c
            if (r0 == 0) goto L_0x0071
            com.wacai365.a.g r0 = r9.c
            java.lang.String r0 = r0.d
            if (r0 == 0) goto L_0x0071
            if (r10 == 0) goto L_0x006f
            java.lang.String r0 = "&"
            java.lang.String[] r0 = r10.split(r0)
            if (r0 == 0) goto L_0x006f
            int r1 = r0.length
            r2 = r6
        L_0x0023:
            if (r2 >= r1) goto L_0x006f
            r3 = r0[r2]
            java.lang.String r4 = "="
            java.lang.String[] r3 = r3.split(r4)
            if (r3 == 0) goto L_0x006c
            r4 = r3[r6]
            java.lang.String r5 = "v"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x006c
            java.lang.String r4 = "[0-9]{6}"
            java.util.regex.Pattern r4 = java.util.regex.Pattern.compile(r4)
            r5 = r3[r7]
            java.util.regex.Matcher r4 = r4.matcher(r5)
            boolean r4 = r4.find()
            if (r4 == 0) goto L_0x006c
            r0 = r3[r7]
        L_0x004d:
            if (r0 == 0) goto L_0x0071
            com.wacai365.a.g r1 = r9.c
            r1.e = r0
            com.wacai365.a.g r0 = r9.c
            r0.f = r8
            java.lang.String r0 = "https://open.t.qq.com/cgi-bin/access_token"
            java.lang.String r1 = "GET"
            java.lang.String r0 = r9.a(r0, r8, r1)
            java.lang.String r1 = "https://open.t.qq.com/cgi-bin/access_token"
            r9.a(r1, r0, r6)
            r9.b = r7
            com.wacai365.a.g r0 = r9.c
            r0.f = r8
            r0 = r7
        L_0x006b:
            return r0
        L_0x006c:
            int r2 = r2 + 1
            goto L_0x0023
        L_0x006f:
            r0 = r8
            goto L_0x004d
        L_0x0071:
            r0 = r6
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.a.d.a(java.lang.String):boolean");
    }

    public final String b(Context context) {
        return "@xiaowamm ";
    }

    public final boolean b(String str) {
        h[] hVarArr = {new h("format", "json"), new h("name", str), new h("clientip", e.b())};
        a("http://open.t.qq.com/api/friends/add", a("http://open.t.qq.com/api/friends/add", hVarArr, "POST"), false, hVarArr, null);
        return true;
    }

    public final boolean c(String str) {
        h[] hVarArr = {new h("format", "json"), new h("name", str)};
        a("http://open.t.qq.com/api/friends/del", a("http://open.t.qq.com/api/friends/del", hVarArr, "POST"), false, hVarArr, null);
        return true;
    }
}
