package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.g;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.o;

public class SettingFeedback extends WacaiActivity implements View.OnClickListener {
    private EditText a = null;
    private String b = null;
    private Animation c;
    private fy d = null;
    private yg e;
    private DialogInterface.OnClickListener f = new uh(this);

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1) {
            if (this.d != null) {
                this.d.a(i, i2, intent);
            }
            if (intent != null && i2 == -1 && 34 == i && this.a != null) {
                String stringExtra = intent.getStringExtra("Text_String");
                this.a.setText(stringExtra);
                this.a.setSelection(stringExtra.length());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.btnCancel /*2131492870*/:
                finish();
                return;
            case C0000R.id.btnOK /*2131492903*/:
                if (this.e.a()) {
                    this.b = this.a.getEditableText().toString();
                    if (this.b == null || this.b.length() == 0) {
                        this.c = m.a(this, this.c, (int) C0000R.anim.shake, this.a, (int) C0000R.string.txtEmptyFeedback);
                        return;
                    }
                    ft ftVar = new ft(this);
                    boolean p = b.m().p();
                    String e2 = b.m().e();
                    ftVar.a((!p || (e2 != null && e2.length() > 0)) ? C0000R.string.feedbackSuccessPrompt : C0000R.string.feedbackSuccessPromptAnonymous);
                    ftVar.a(this.f);
                    ftVar.b(C0000R.string.networkProgress, C0000R.string.txtTransformData);
                    this.d = ftVar;
                    c cVar = new c(ftVar);
                    l lVar = new l();
                    lVar.a(m.c(this.b));
                    cVar.a((o) lVar);
                    cVar.a((o) new g());
                    ftVar.a(cVar);
                    ftVar.a(false, true);
                    return;
                }
                return;
            case C0000R.id.btnVoice /*2131492907*/:
                VoiceInput.a(this, 500, this.a.getText().toString());
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.input_feedback);
        this.a = (EditText) findViewById(C0000R.id.etFeedback);
        this.e = new yg(this, (TextView) findViewById(C0000R.id.text_count_prompt), 500);
        this.a.addTextChangedListener(this.e);
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(this);
        ((Button) findViewById(C0000R.id.btnOK)).setOnClickListener(this);
        ((Button) findViewById(C0000R.id.btnVoice)).setOnClickListener(this);
    }
}
