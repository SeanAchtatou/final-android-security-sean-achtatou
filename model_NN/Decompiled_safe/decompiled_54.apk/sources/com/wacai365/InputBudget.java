package com.wacai365;

import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.e;

public class InputBudget extends WacaiActivity {
    private LinearLayout A = null;
    private LinearLayout B = null;
    private LinearLayout C = null;
    private LinearLayout F = null;
    private long G = -1;
    /* access modifiers changed from: private */
    public long H = -1;
    private boolean I = false;
    /* access modifiers changed from: private */
    public int[] J = null;
    private Animation K;
    /* access modifiers changed from: private */
    public int L = 0;
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener M = new tb(this);
    private Button a = null;
    private Button b = null;
    private Button c = null;
    /* access modifiers changed from: private */
    public TextView d = null;
    /* access modifiers changed from: private */
    public TextView e = null;
    /* access modifiers changed from: private */
    public TextView f = null;
    /* access modifiers changed from: private */
    public TextView g = null;
    /* access modifiers changed from: private */
    public TextView h = null;
    /* access modifiers changed from: private */
    public TextView i = null;
    /* access modifiers changed from: private */
    public TextView j = null;
    /* access modifiers changed from: private */
    public TextView k = null;
    /* access modifiers changed from: private */
    public TextView l = null;
    /* access modifiers changed from: private */
    public TextView m = null;
    /* access modifiers changed from: private */
    public TextView n = null;
    /* access modifiers changed from: private */
    public TextView o = null;
    /* access modifiers changed from: private */
    public TextView p = null;
    /* access modifiers changed from: private */
    public CheckBox q = null;
    private LinearLayout r = null;
    private LinearLayout s = null;
    private LinearLayout t = null;
    private LinearLayout u = null;
    private LinearLayout v = null;
    private LinearLayout w = null;
    private LinearLayout x = null;
    private LinearLayout y = null;
    private LinearLayout z = null;

    static /* synthetic */ void a(InputBudget inputBudget, String str) {
        inputBudget.e.setText(str);
        inputBudget.f.setText(str);
        inputBudget.g.setText(str);
        inputBudget.h.setText(str);
        inputBudget.i.setText(str);
        inputBudget.j.setText(str);
        inputBudget.k.setText(str);
        inputBudget.l.setText(str);
        inputBudget.m.setText(str);
        inputBudget.n.setText(str);
        inputBudget.o.setText(str);
        inputBudget.p.setText(str);
    }

    static /* synthetic */ void s(InputBudget inputBudget) {
        inputBudget.d.setText("");
        inputBudget.G = -1;
        inputBudget.H = -1;
        inputBudget.I = false;
        inputBudget.e.setText("");
        inputBudget.f.setText("");
        inputBudget.g.setText("");
        inputBudget.h.setText("");
        inputBudget.i.setText("");
        inputBudget.j.setText("");
        inputBudget.k.setText("");
        inputBudget.l.setText("");
        inputBudget.m.setText("");
        inputBudget.n.setText("");
        inputBudget.o.setText("");
        inputBudget.p.setText("");
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        if (-1 == this.H) {
            this.K = m.a(this, this.K, C0000R.anim.shake, (View) null, C0000R.string.txtMustChooseMember);
            return false;
        }
        String obj = this.e.getText().toString();
        String obj2 = this.f.getText().toString();
        String obj3 = this.g.getText().toString();
        String obj4 = this.h.getText().toString();
        String obj5 = this.i.getText().toString();
        String obj6 = this.j.getText().toString();
        String obj7 = this.k.getText().toString();
        String obj8 = this.l.getText().toString();
        String obj9 = this.m.getText().toString();
        String obj10 = this.n.getText().toString();
        String obj11 = this.o.getText().toString();
        String obj12 = this.p.getText().toString();
        long a2 = obj.length() == 0 ? 0 : m.a(Double.parseDouble(obj));
        long a3 = obj2.length() == 0 ? 0 : m.a(Double.parseDouble(obj2));
        long a4 = obj3.length() == 0 ? 0 : m.a(Double.parseDouble(obj3));
        long a5 = obj4.length() == 0 ? 0 : m.a(Double.parseDouble(obj4));
        long a6 = obj5.length() == 0 ? 0 : m.a(Double.parseDouble(obj5));
        long a7 = obj6.length() == 0 ? 0 : m.a(Double.parseDouble(obj6));
        long a8 = obj7.length() == 0 ? 0 : m.a(Double.parseDouble(obj7));
        long a9 = obj8.length() == 0 ? 0 : m.a(Double.parseDouble(obj8));
        long a10 = obj9.length() == 0 ? 0 : m.a(Double.parseDouble(obj9));
        long a11 = obj10.length() == 0 ? 0 : m.a(Double.parseDouble(obj10));
        long a12 = obj11.length() == 0 ? 0 : m.a(Double.parseDouble(obj11));
        long a13 = obj12.length() == 0 ? 0 : m.a(Double.parseDouble(obj12));
        if (this.I) {
            e.c().b().execSQL(String.format("DELETE FROM TBL_BUDGETINFO WHERE id = %d", Long.valueOf(this.G)));
        }
        e.c().b().execSQL(String.format("INSERT INTO TBL_BUDGETINFO (memberid, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12 ) VALUES (%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d)", Long.valueOf(this.H), Long.valueOf(a2), Long.valueOf(a3), Long.valueOf(a4), Long.valueOf(a5), Long.valueOf(a6), Long.valueOf(a7), Long.valueOf(a8), Long.valueOf(a9), Long.valueOf(a10), Long.valueOf(a11), Long.valueOf(a12), Long.valueOf(a13)));
        return true;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(C0000R.layout.input_budget);
        this.G = getIntent().getLongExtra("Record_Id", -1);
        this.I = this.G > 0;
        this.e = (TextView) findViewById(C0000R.id.etMonth1);
        this.f = (TextView) findViewById(C0000R.id.etMonth2);
        this.g = (TextView) findViewById(C0000R.id.etMonth3);
        this.h = (TextView) findViewById(C0000R.id.etMonth4);
        this.i = (TextView) findViewById(C0000R.id.etMonth5);
        this.j = (TextView) findViewById(C0000R.id.etMonth6);
        this.k = (TextView) findViewById(C0000R.id.etMonth7);
        this.l = (TextView) findViewById(C0000R.id.etMonth8);
        this.m = (TextView) findViewById(C0000R.id.etMonth9);
        this.n = (TextView) findViewById(C0000R.id.etMonth10);
        this.o = (TextView) findViewById(C0000R.id.etMonth11);
        this.p = (TextView) findViewById(C0000R.id.etMonth12);
        this.s = (LinearLayout) findViewById(C0000R.id.layoutMonth1);
        this.s.setOnClickListener(new tc(this));
        this.t = (LinearLayout) findViewById(C0000R.id.layoutMonth2);
        this.t.setOnClickListener(new sy(this));
        this.u = (LinearLayout) findViewById(C0000R.id.layoutMonth3);
        this.u.setOnClickListener(new ta(this));
        this.v = (LinearLayout) findViewById(C0000R.id.layoutMonth4);
        this.v.setOnClickListener(new sr(this));
        this.w = (LinearLayout) findViewById(C0000R.id.layoutMonth5);
        this.w.setOnClickListener(new sp(this));
        this.x = (LinearLayout) findViewById(C0000R.id.layoutMonth6);
        this.x.setOnClickListener(new sn(this));
        this.y = (LinearLayout) findViewById(C0000R.id.layoutMonth7);
        this.y.setOnClickListener(new sm(this));
        this.z = (LinearLayout) findViewById(C0000R.id.layoutMonth8);
        this.z.setOnClickListener(new sx(this));
        this.A = (LinearLayout) findViewById(C0000R.id.layoutMonth9);
        this.A.setOnClickListener(new la(this));
        this.B = (LinearLayout) findViewById(C0000R.id.layoutMonth10);
        this.B.setOnClickListener(new ky(this));
        this.C = (LinearLayout) findViewById(C0000R.id.layoutMonth11);
        this.C.setOnClickListener(new le(this));
        this.F = (LinearLayout) findViewById(C0000R.id.layoutMonth12);
        this.F.setOnClickListener(new lc(this));
        this.d = (TextView) findViewById(C0000R.id.btManaMember);
        this.r = (LinearLayout) findViewById(C0000R.id.layoutManaMember);
        this.r.setOnClickListener(new lh(this));
        this.q = (CheckBox) findViewById(C0000R.id.cbUseSameSetting);
        this.c = (Button) findViewById(C0000R.id.btnCancel);
        this.b = (Button) findViewById(C0000R.id.btnContinue);
        this.a = (Button) findViewById(C0000R.id.btnOK);
        this.c.setOnClickListener(new lg(this));
        this.b.setOnClickListener(new lk(this));
        this.a.setOnClickListener(new li(this));
        if (this.I) {
            setTitle(C0000R.string.txtEditBudget);
            Cursor rawQuery = e.c().b().rawQuery(String.format("select b.name as _name, b.id as _id, a.m1 as m1, a.m2 as m2, a.m3 as m3, a.m4 as m4, a.m5 as m5, a.m6 as m6, a.m7 as m7, a.m8 as m8, a.m9 as m9, a.m10 as m10, a.m11 as m11, a.m12 as m12 from TBL_BUDGETINFO a, TBL_MEMBERINFO b where a.memberid = b.id and a.id = %d GROUP BY a.id", Long.valueOf(this.G)), null);
            if ((rawQuery == null ? 0 : rawQuery.getCount()) > 0) {
                rawQuery.moveToNext();
                long j2 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("m1"));
                long j3 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("m2"));
                long j4 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("m3"));
                long j5 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("m4"));
                long j6 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("m5"));
                long j7 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("m6"));
                long j8 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("m7"));
                long j9 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("m8"));
                long j10 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("m9"));
                long j11 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("m10"));
                long j12 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("m11"));
                long j13 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("m12"));
                this.H = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_id"));
                this.d.setText(rawQuery.getString(rawQuery.getColumnIndexOrThrow("_name")));
                this.e.setText(m.a(m.a(j2), 2));
                this.f.setText(m.a(m.a(j3), 2));
                this.g.setText(m.a(m.a(j4), 2));
                this.h.setText(m.a(m.a(j5), 2));
                this.i.setText(m.a(m.a(j6), 2));
                this.j.setText(m.a(m.a(j7), 2));
                this.k.setText(m.a(m.a(j8), 2));
                this.l.setText(m.a(m.a(j9), 2));
                this.m.setText(m.a(m.a(j10), 2));
                this.n.setText(m.a(m.a(j11), 2));
                this.o.setText(m.a(m.a(j12), 2));
                this.p.setText(m.a(m.a(j13), 2));
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
        }
    }
}
