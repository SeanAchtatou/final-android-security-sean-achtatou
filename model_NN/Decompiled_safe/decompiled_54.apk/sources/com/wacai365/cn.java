package com.wacai365;

import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;

final class cn implements View.OnCreateContextMenuListener {
    private /* synthetic */ MyBalance a;

    cn(MyBalance myBalance) {
        this.a = myBalance;
    }

    public final void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        xs xsVar;
        contextMenu.clear();
        MenuInflater menuInflater = this.a.getMenuInflater();
        Object itemAtPosition = this.a.k.getItemAtPosition(((AdapterView.AdapterContextMenuInfo) contextMenuInfo).position);
        if (itemAtPosition.getClass() != gd.class && (xsVar = (xs) itemAtPosition) != null && xsVar.a > 0) {
            if (xsVar.j == 1 && xsVar.f == 0) {
                menuInflater.inflate(C0000R.menu.reimburse_list_context, contextMenu);
            } else {
                menuInflater.inflate(C0000R.menu.white_list_context, contextMenu);
            }
        }
    }
}
