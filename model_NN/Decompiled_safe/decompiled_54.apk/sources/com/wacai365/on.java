package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.view.View;
import android.widget.AdapterView;

final class on implements AdapterView.OnItemClickListener {
    private /* synthetic */ SettingNews a;

    on(SettingNews settingNews) {
        this.a = settingNews;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Cursor cursor = (Cursor) this.a.a.getItemAtPosition(i);
        long j2 = cursor != null ? cursor.getLong(cursor.getColumnIndexOrThrow("_id")) : 0;
        Intent intent = new Intent(this.a, NewsList.class);
        intent.putExtra("Record_Id", j2);
        this.a.startActivityForResult(intent, 0);
    }
}
