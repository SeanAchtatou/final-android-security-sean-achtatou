package com.wacai365;

import android.view.MotionEvent;
import android.view.View;

final class ct implements View.OnTouchListener {
    private boolean a = false;
    private /* synthetic */ VoiceInput b;

    ct(VoiceInput voiceInput) {
        this.b = voiceInput;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.a = VoiceInput.c(this.b);
                return true;
            case 1:
                if (!this.a || 2 != this.b.y) {
                    return true;
                }
                this.a = false;
                VoiceInput.e(this.b);
                return true;
            case 2:
            default:
                return true;
            case 3:
                if (!this.a || 2 != this.b.y) {
                    return true;
                }
                this.a = false;
                VoiceInput.e(this.b);
                return true;
        }
    }
}
