package com.wacai365;

import android.content.DialogInterface;
import com.wacai.a.a;
import java.util.Date;

final class os implements DialogInterface.OnClickListener {
    private /* synthetic */ sj a;

    os(sj sjVar) {
        this.a = sjVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.c.setText(this.a.a.n.getResources().getStringArray(C0000R.array.Times)[i]);
        Date date = new Date();
        Date date2 = new Date();
        a.a(i, date, date2);
        this.a.a.k.b = a.a(date.getTime());
        this.a.a.k.c = a.a(date2.getTime());
        String format = m.c.format(date);
        String format2 = m.c.format(date2);
        this.a.a.d.setText(format);
        this.a.a.e.setText(format2);
        int unused = this.a.a.j = i;
        dialogInterface.dismiss();
    }
}
