package com.wacai365;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.wacai.data.v;
import com.wacai.e;

public class MyNote extends WacaiActivity {
    /* access modifiers changed from: private */
    public Button a;
    /* access modifiers changed from: private */
    public Button b;
    private ListView c;
    /* access modifiers changed from: private */
    public long d = -1;
    /* access modifiers changed from: private */
    public long e = -1;
    private long f = 0;
    /* access modifiers changed from: private */
    public String g = "";
    private String h = "";
    /* access modifiers changed from: private */
    public String[] i = null;
    private int j = 0;
    private View.OnClickListener k = new pm(this);

    /* access modifiers changed from: private */
    public void f() {
        if (this.g.length() > 0 && !vk.a(Double.parseDouble(this.g))) {
            if (this.h == null || this.h.length() == 0) {
                this.h = this.g;
            }
            this.g = "";
        }
        Intent a2 = InputTrade.a(this, this.g, 0);
        a2.putExtra("LaunchedByApplication", 1);
        a2.putExtra("Extra_Type", this.j);
        a2.putExtra("Extra_Comment", this.h);
        a2.putExtra("Extra_Date", this.f);
        a2.putExtra("Extra_Money", this.g);
        startActivityForResult(a2, 10);
    }

    private void g() {
        Cursor cursor = (Cursor) this.c.getItemAtPosition((int) this.e);
        this.h = cursor.getString(cursor.getColumnIndexOrThrow("_comment"));
        this.f = cursor.getLong(cursor.getColumnIndexOrThrow("_createdate"));
        this.g = "";
        if (this.h != null && this.h.length() != 0) {
            String[] a2 = m.a(this.h, 0);
            if (a2 != null) {
                if (1 == a2.length) {
                    this.g = a2[0];
                } else {
                    int length = a2.length;
                    String[] strArr = new String[(length + 1)];
                    for (int i2 = 0; i2 < length; i2++) {
                        strArr[i2] = a2[i2];
                    }
                    strArr[length] = getText(C0000R.string.txtMoneyTotal).toString();
                    this.i = strArr;
                    new AlertDialog.Builder(this).setSingleChoiceItems(this.i, -1, new pu(this)).show();
                    return;
                }
            }
            f();
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        v b2 = v.b(this.d);
        if (b2 != null) {
            b2.f();
            Cursor cursor = (Cursor) this.c.getItemAtPosition(0);
            if (cursor != null) {
                cursor.requery();
                this.c.invalidateViews();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        Intent intent = new Intent(this, InputNote.class);
        intent.putExtra("LaunchedByApplication", 1);
        intent.putExtra("Record_Id", this.d);
        startActivityForResult(intent, 0);
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.j = 0;
        g();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.j = 1;
        g();
    }

    /* access modifiers changed from: protected */
    public final void e() {
        this.j = 2;
        g();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 != -1) {
            return;
        }
        if (10 == i2 || 11 == i2 || 12 == i2) {
            a();
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        this.e = (long) ((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).position;
        Cursor cursor = (Cursor) this.c.getItemAtPosition((int) this.e);
        if (cursor != null) {
            this.d = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
        }
        switch (menuItem.getItemId()) {
            case C0000R.id.idDelete /*2131493338*/:
                a();
                return false;
            case C0000R.id.idSaveAsOutgo /*2131493339*/:
                c();
                return false;
            case C0000R.id.idSaveAsIncome /*2131493340*/:
                d();
                return false;
            case C0000R.id.idSaveAsTransfer /*2131493341*/:
                e();
                return false;
            case C0000R.id.idEdit /*2131493342*/:
                b();
                return false;
            default:
                return false;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.mynote);
        this.a = (Button) findViewById(C0000R.id.btnAdd);
        this.a.setOnClickListener(this.k);
        this.b = (Button) findViewById(C0000R.id.btnCancel);
        this.b.setOnClickListener(this.k);
        this.c = (ListView) findViewById(C0000R.id.IOList);
        this.c.setOnItemClickListener(new pk(this));
        this.c.setOnCreateContextMenuListener(new ps(this));
        Cursor rawQuery = e.c().b().rawQuery(String.format("select id as _id, createdate as _createdate, comment as _comment from TBL_MYNOTE where isdelete = 0 order by createdate DESC", new Object[0]), null);
        startManagingCursor(rawQuery);
        this.c.setAdapter((ListAdapter) new in(this, C0000R.layout.list_item_line2_note, rawQuery, new String[]{"_createdate", "_comment"}, new int[]{C0000R.id.listitem1, C0000R.id.listitem2}));
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 6:
                return new AlertDialog.Builder(this).setItems((int) C0000R.array.NoteTransferType, new po(this)).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }
}
