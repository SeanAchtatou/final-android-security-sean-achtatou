package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;

final class mq implements DialogInterface.OnClickListener {
    private /* synthetic */ MySearch a;

    mq(MySearch mySearch) {
        this.a = mySearch;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        MySearch mySearch = this.a;
        switch (i) {
            case -3:
                Intent intent = new Intent(mySearch, DataBackupSetting.class);
                intent.putExtra("LaunchedByApplication", 1);
                mySearch.startActivityForResult(intent, 0);
                break;
            case -1:
                fy unused = this.a.i = abt.a(mySearch);
                break;
        }
        this.a.h.e();
    }
}
