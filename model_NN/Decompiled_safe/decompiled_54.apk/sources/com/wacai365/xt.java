package com.wacai365;

import android.content.Intent;
import android.view.View;

final class xt implements View.OnClickListener {
    private /* synthetic */ MyBalanceQuerySetting a;

    xt(MyBalanceQuerySetting myBalanceQuerySetting) {
        this.a = myBalanceQuerySetting;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.MyBalanceQuerySetting.a(com.wacai365.MyBalanceQuerySetting, boolean):void
     arg types: [com.wacai365.MyBalanceQuerySetting, int]
     candidates:
      com.wacai365.MyBalanceQuerySetting.a(com.wacai365.MyBalanceQuerySetting, com.wacai365.QueryInfo):com.wacai365.QueryInfo
      com.wacai365.MyBalanceQuerySetting.a(com.wacai365.MyBalanceQuerySetting, boolean):void */
    public final void onClick(View view) {
        if (view.equals(this.a.d)) {
            if (this.a.k.c()) {
                Intent intent = this.a.getIntent();
                intent.putExtra("QUERYINFO", this.a.a);
                this.a.setResult(-1, intent);
                this.a.finish();
            }
        } else if (view.equals(this.a.f)) {
            this.a.setResult(0);
            this.a.finish();
        } else if (view.equals(this.a.e)) {
            if (this.a.k != null) {
                QueryInfo unused = this.a.a = new QueryInfo();
                this.a.a.a(4);
                if (!((this.a.a.t & 8) == 0 || this.a.a.t == 15)) {
                    this.a.a.a(8);
                }
                this.a.a.d = -1;
                this.a.k.a(this.a.a);
                this.a.k.a_();
            }
        } else if (view.equals(this.a.h)) {
            MyBalanceQuerySetting.a(this.a, true);
        } else if (view.equals(this.a.g)) {
            MyBalanceQuerySetting.a(this.a, false);
        }
    }
}
