package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.g;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.o;
import com.wacai.data.aa;
import com.wacai.data.e;

public class MoneyTypeSetting extends WacaiActivity implements tt {
    protected int[] a = null;
    private Button b = null;
    private Button c = null;
    /* access modifiers changed from: private */
    public TextView d = null;
    private LinearLayout e = null;
    /* access modifiers changed from: private */
    public long f = -1;
    private fy g = null;
    private DialogInterface.OnClickListener h = new bx(this);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void a(MoneyTypeSetting moneyTypeSetting) {
        ft ftVar = new ft(moneyTypeSetting);
        ftVar.a((tt) moneyTypeSetting);
        ftVar.a(moneyTypeSetting.h);
        ftVar.b(C0000R.string.networkProgress, C0000R.string.txtTransformData);
        moneyTypeSetting.g = ftVar;
        c cVar = new c(ftVar);
        l lVar = new l();
        lVar.a(m.d(aa.a("TBL_MONEYTYPE", "uuid", (int) moneyTypeSetting.f)));
        lVar.d(moneyTypeSetting.getResources().getText(C0000R.string.txtTransformData).toString());
        cVar.a((o) lVar);
        cVar.a((o) new g());
        ftVar.a(cVar);
        ftVar.a(false, true);
    }

    public final void a(int i) {
        b.m().c(this.f);
        b.m().l();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1 && this.g != null) {
            this.g.a(i, i2, intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.local_money_setting);
        this.f = b.m().j();
        this.c = (Button) findViewById(C0000R.id.btnCancel);
        this.b = (Button) findViewById(C0000R.id.btnSave);
        this.c.setOnClickListener(new bs(this));
        this.b.setOnClickListener(new bw(this));
        this.e = (LinearLayout) findViewById(C0000R.id.layoutMoneyType);
        this.e.setOnClickListener(new bu(this));
        String a2 = e.a(this.f);
        this.d = (TextView) findViewById(C0000R.id.txtMoneyType);
        this.d.setText(a2);
    }
}
