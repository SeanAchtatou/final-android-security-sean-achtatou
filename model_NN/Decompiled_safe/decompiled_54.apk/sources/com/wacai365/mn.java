package com.wacai365;

import android.content.Intent;
import android.view.View;

final class mn implements View.OnClickListener {
    private /* synthetic */ MySearch a;

    mn(MySearch mySearch) {
        this.a = mySearch;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.a)) {
            Intent intent = new Intent(this.a, SearchSummary.class);
            intent.putExtra("Extra_Constraint", this.a.c.getText().toString());
            this.a.startActivityForResult(intent, -1);
        } else if (view.equals(this.a.b)) {
            this.a.setResult(0);
            this.a.finish();
        }
    }
}
