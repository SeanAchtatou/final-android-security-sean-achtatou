package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;

final class cx implements DialogInterface.OnClickListener {
    private /* synthetic */ yf a;

    cx(yf yfVar) {
        this.a = yfVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == 0) {
            this.a.o.startActivityForResult(new Intent(this.a.o, InputProject.class), 0);
        } else {
            this.a.a.i((long) this.a.b[i]);
            m.a("TBL_PROJECTINFO", (long) this.a.b[i], this.a.u);
        }
        dialogInterface.dismiss();
    }
}
