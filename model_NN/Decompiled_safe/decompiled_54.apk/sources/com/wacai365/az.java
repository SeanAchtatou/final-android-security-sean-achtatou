package com.wacai365;

import android.content.DialogInterface;

final class az implements DialogInterface.OnClickListener {
    private /* synthetic */ MySMS a;

    az(MySMS mySMS) {
        this.a = mySMS;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case 0:
                this.a.b();
                return;
            case 1:
                this.a.c();
                return;
            case 2:
                this.a.d();
                return;
            case 3:
                this.a.showDialog(8);
                return;
            case 4:
                this.a.a();
                return;
            case 5:
                MySMS mySMS = this.a;
                m.a(mySMS, (int) C0000R.string.txtDeleteAllConfirm, mySMS);
                return;
            default:
                return;
        }
    }
}
