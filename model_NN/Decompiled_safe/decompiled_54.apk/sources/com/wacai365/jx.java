package com.wacai365;

import android.content.DialogInterface;
import com.wacai.a.a;
import java.util.Date;

final class jx implements DialogInterface.OnClickListener {
    private /* synthetic */ nv a;

    jx(nv nvVar) {
        this.a = nvVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.d.setText(this.a.a.n.getResources().getStringArray(C0000R.array.Times)[i]);
        Date date = new Date();
        Date date2 = new Date();
        a.a(i, date, date2);
        this.a.a.v.b = a.a(date.getTime());
        this.a.a.v.c = a.a(date2.getTime());
        String format = m.c.format(date);
        String format2 = m.c.format(date2);
        this.a.a.e.setText(format);
        this.a.a.f.setText(format2);
        int unused = this.a.a.u = i;
        dialogInterface.dismiss();
    }
}
