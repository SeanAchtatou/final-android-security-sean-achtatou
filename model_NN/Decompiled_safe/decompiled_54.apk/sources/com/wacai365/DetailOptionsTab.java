package com.wacai365;

import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

public class DetailOptionsTab extends WacaiActivity {
    private static fr[] b = {new fr(C0000R.string.txtMyIOT, C0000R.string.txtMyIOTDetail, "com.wacai365.MyBalance"), new fr(C0000R.string.myScheduleIO, C0000R.string.myScheduleIODetail, "com.wacai365.MyScheduleTrade"), new fr(C0000R.string.txtMyStat, C0000R.string.txtMyStatDetail, "com.wacai365.StatMain"), new fr(C0000R.string.txtMyNote, C0000R.string.txtMyNoteDetail, "com.wacai365.MyNote"), new fr(C0000R.string.txtShortcutsMgr, C0000R.string.txtShortcutsMgrDetail, "com.wacai365.ShortcutsMgr")};
    ListView a;
    private AdapterView.OnItemClickListener c = new aax(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.a = new ListView(this);
        setContentView(this.a);
        this.a.setAdapter((ListAdapter) new xw(this, (int) C0000R.layout.list_item_line2, b));
        this.a.setOnItemClickListener(this.c);
    }
}
