package com.wacai365;

import com.wacai.e;

public final class ll {
    public long a;
    public long b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public boolean i;
    private /* synthetic */ aaj j;

    public ll(aaj aaj) {
        this.j = aaj;
    }

    public final boolean a() {
        return this.f != null && this.f.length() > 0 && this.g != null && this.g.length() > 0;
    }

    public final void b() {
        StringBuilder sb = new StringBuilder(96);
        sb.append("UPDATE TBL_WEIBOINFO SET ");
        if (this.f != null) {
            sb.append("tk = '");
            sb.append(this.f);
            sb.append("', ");
        }
        if (this.g != null) {
            sb.append("tks = '");
            sb.append(this.g);
            sb.append("', ");
        }
        sb.append("isrwc = ");
        sb.append(this.i ? 1 : 0);
        sb.append(" WHERE id = ");
        sb.append(this.a);
        e.c().b().execSQL(sb.toString());
    }

    public final Object clone() {
        ll llVar = new ll(this.j);
        llVar.d = this.d;
        llVar.e = this.e;
        llVar.a = this.a;
        llVar.i = this.i;
        llVar.c = this.c;
        llVar.f = this.f;
        llVar.g = this.g;
        llVar.b = this.b;
        llVar.h = this.h;
        return llVar;
    }
}
