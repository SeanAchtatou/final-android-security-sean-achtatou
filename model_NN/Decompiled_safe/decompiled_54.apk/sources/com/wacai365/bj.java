package com.wacai365;

import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.wacai.data.aa;
import com.wacai.data.d;
import com.wacai.data.m;
import com.wacai.data.n;
import com.wacai.data.r;
import com.wacai.data.s;
import com.wacai.data.u;
import com.wacai.data.x;

public class bj extends oj {
    /* access modifiers changed from: private */
    public View A;
    /* access modifiers changed from: private */
    public View B;
    private String C = "";
    /* access modifiers changed from: private */
    public r D;
    /* access modifiers changed from: private */
    public boolean E = true;
    /* access modifiers changed from: private */
    public int[] F = null;
    /* access modifiers changed from: private */
    public int[] G = null;
    /* access modifiers changed from: private */
    public va H = new kc(this);
    private yn I = new jd(this);
    private View.OnClickListener J = new jc(this);
    /* access modifiers changed from: protected */
    public rk a = null;
    /* access modifiers changed from: private */
    public LinearLayout b;
    /* access modifiers changed from: private */
    public TextView c;
    private TextView j;
    /* access modifiers changed from: private */
    public TextView k;
    private TextView l;
    private TextView m;
    /* access modifiers changed from: private */
    public TextView r;
    /* access modifiers changed from: private */
    public TextView s;
    /* access modifiers changed from: private */
    public TextView t;
    /* access modifiers changed from: private */
    public Button u;
    /* access modifiers changed from: private */
    public View v;
    /* access modifiers changed from: private */
    public View w;
    /* access modifiers changed from: private */
    public View x;
    /* access modifiers changed from: private */
    public View y;
    /* access modifiers changed from: private */
    public View z;

    public bj(long j2) {
        if (j2 > 0) {
            this.D = r.c(j2);
            this.E = !aa.b("TBL_SCHEDULEINCOMEINFO", this.D.e());
        } else {
            this.D = new r();
            this.D.s().add(new x(this.D));
        }
        this.d = C0000R.layout.input_income_tab;
    }

    public final long a() {
        if (this.D == null) {
            return 0;
        }
        return this.D.b();
    }

    public final void a(int i, int i2, Intent intent) {
        super.a(i, i2, intent);
        if (i2 == -1 && intent != null) {
            switch (i) {
                case 3:
                    m.a(intent, this.D);
                    m.a(this.D.s(), this.s);
                    return;
                case 16:
                case 34:
                    String string = intent.getExtras().getString("Text_String");
                    double a2 = m.a(string, this.c, this.l);
                    if (string != null) {
                        if (this.D.o() == 0 && vk.a(a2)) {
                            this.D.g((long) (a2 * 100.0d));
                        }
                        this.D.a(string);
                        m.a(string, this.l);
                        return;
                    }
                    return;
                case 18:
                    this.C = intent.getStringExtra("Text_String");
                    if (this.C == null || this.C.length() <= 0) {
                        m.a(this.n, (Animation) null, 0, (View) null, (int) C0000R.string.txtEmptyShortcutName);
                        return;
                    } else if (this.D.s().size() == 0) {
                        m.a(this.n, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideMember);
                        return;
                    } else {
                        n nVar = new n();
                        nVar.a(this.C);
                        nVar.a(1);
                        nVar.c(this.D.p());
                        nVar.d(this.D.q());
                        nVar.e(this.D.r());
                        if (this.D.s().size() == 1) {
                            nVar.b(((x) this.D.s().get(0)).a());
                        }
                        nVar.a(this.D.a());
                        nVar.f(false);
                        nVar.c();
                        m.a(this.n, (Animation) null, 0, (View) null, (int) C0000R.string.txtSaveAsShortcutSuc);
                        return;
                    }
                case 24:
                    long longExtra = intent.getLongExtra("Target_ID", 0);
                    this.D.j(longExtra);
                    m.a("TBL_TRADETARGET", longExtra, this.m);
                    return;
                case 37:
                    this.D.h(intent.getLongExtra("account_Sel_Id", this.D.p()));
                    m.a(this.o, this.D.p(), this.j);
                    return;
                default:
                    return;
            }
        }
    }

    public final void a(long j2) {
        if (this.D != null) {
            this.D.b(j2);
        }
        if (this.t != null) {
            m.d(1000 * j2, this.t);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(LinearLayout linearLayout) {
        this.g = linearLayout;
        this.b = (LinearLayout) linearLayout.findViewById(C0000R.id.moneyItem);
        this.c = (TextView) linearLayout.findViewById(C0000R.id.viewMoney);
        this.k = (TextView) linearLayout.findViewById(C0000R.id.viewType);
        this.j = (TextView) linearLayout.findViewById(C0000R.id.viewAccount);
        this.l = (TextView) linearLayout.findViewById(C0000R.id.viewComment);
        this.m = (TextView) linearLayout.findViewById(C0000R.id.viewTarget);
        this.r = (TextView) linearLayout.findViewById(C0000R.id.viewProject);
        this.s = (TextView) linearLayout.findViewById(C0000R.id.viewMember);
        this.t = (TextView) linearLayout.findViewById(C0000R.id.viewDate);
        this.c.setText(aa.a(aa.l(this.D.o()), 2));
        m.a("TBL_ACCOUNTINFO", this.D.p(), this.j);
        m.a("TBL_INCOMEMAINTYPEINFO", this.D.a(), this.k);
        m.a(this.D.d(), this.l);
        m.a("TBL_TRADETARGET", this.D.r(), this.m);
        m.a("TBL_PROJECTINFO", this.D.q(), this.r);
        m.a(this.D.s(), this.s);
        m.d(this.D.b() * 1000, this.t);
        this.b.setOnClickListener(this.J);
        this.v = linearLayout.findViewById(C0000R.id.accountItem);
        this.v.setOnClickListener(this.J);
        this.w = linearLayout.findViewById(C0000R.id.typeItem);
        this.w.setOnClickListener(this.J);
        this.x = linearLayout.findViewById(C0000R.id.commentItem);
        this.x.setOnClickListener(this.J);
        this.y = linearLayout.findViewById(C0000R.id.targetItem);
        this.y.setOnClickListener(this.J);
        this.z = linearLayout.findViewById(C0000R.id.projectItem);
        this.z.setOnClickListener(this.J);
        this.A = linearLayout.findViewById(C0000R.id.memberItem);
        this.A.setOnClickListener(this.J);
        this.B = linearLayout.findViewById(C0000R.id.dateItem);
        this.B.setOnClickListener(this.J);
        this.u = (Button) linearLayout.findViewById(C0000R.id.voice_input);
        this.u.setOnClickListener(this.J);
    }

    public final void a(ScrollView scrollView, int i) {
        this.h = scrollView;
        this.i = i;
    }

    public final void a(Object obj) {
        if (r.class.isInstance(obj)) {
            this.D = (r) obj;
            this.E = !aa.b("TBL_SCHEDULEINCOMEINFO", this.D.e());
        } else {
            if (this.D == null) {
                this.D = new r();
            }
            if (s.class.isInstance(obj)) {
                this.D.g(((s) obj).o());
            } else if (u.class.isInstance(obj)) {
                this.D.g(((u) obj).b());
            } else if (m.class.isInstance(obj)) {
                this.D.g(((m) obj).d());
            } else if (d.class.isInstance(obj)) {
                this.D.g(((d) obj).b());
            }
        }
        if (this.D != null && this.D.s().size() <= 0) {
            this.D.s().add(new x(this.D));
        }
    }

    public final void a(String str) {
        if (str != null && this.D != null) {
            this.D.a(str);
            m.a(this.D.d(), this.l);
        }
    }

    public final Object b() {
        return this.D;
    }

    public final void b(long j2) {
        this.D.g(j2);
        this.c.setText(aa.a(aa.l(this.D.o()), 2));
    }

    public final boolean c() {
        if (!this.E) {
            m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.txtEditScheduleData);
            return false;
        } else if (!a(this.o, this.D)) {
            return false;
        } else {
            if (!aa.b("TBL_INCOMEMAINTYPEINFO", this.D.a())) {
                m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideIncomeT);
                return false;
            } else if (!aa.b("TBL_ACCOUNTINFO", this.D.p())) {
                m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideAccount);
                return false;
            } else if (!aa.b("TBL_PROJECTINFO", this.D.q())) {
                m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideProject);
                return false;
            } else if (!x.a(this.D.s())) {
                m.a(this.o, (Animation) null, 0, (View) null, (int) C0000R.string.InvalideMember);
                return false;
            } else {
                this.D.f(false);
                this.D.c();
                return true;
            }
        }
    }

    public final void d() {
        if (this.f != null) {
            if (af.class.isInstance(this.f)) {
                ((af) this.f).a(this.H, this.b);
            } else if (zl.class.isInstance(this.f)) {
                ((zl) this.f).a(this.I, this.B);
            }
        }
    }

    public final ca e() {
        return this.f;
    }

    public final void f() {
        this.D.k(0);
        this.D.a("");
        this.D.g(0);
        this.c.setText(aa.a(aa.l(this.D.o()), 2));
        m.a(this.D.d(), this.l);
    }

    /* access modifiers changed from: protected */
    public final void g() {
        if (this.D != null) {
            if (this.k != null) {
                m.a("TBL_INCOMEMAINTYPEINFO", this.D.a(), this.k);
            }
            if (this.m != null) {
                m.a("TBL_TRADETARGET", this.D.r(), this.m);
            }
        }
    }
}
