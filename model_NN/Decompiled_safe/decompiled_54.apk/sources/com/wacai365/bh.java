package com.wacai365;

import android.os.Message;
import com.wacai365.a.c;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

final class bh implements Runnable {
    private /* synthetic */ WeiboPublish a;

    bh(WeiboPublish weiboPublish) {
        this.a = weiboPublish;
    }

    public final void run() {
        Exception e;
        boolean z;
        Exception exc = null;
        Iterator it = this.a.j.iterator();
        boolean z2 = false;
        InputStream inputStream = null;
        while (it.hasNext()) {
            c cVar = (c) it.next();
            if (this.a.b != 101 && !this.a.l) {
                try {
                    inputStream = m.c() ? new FileInputStream(new File(WeiboPublish.a)) : this.a.openFileInput("tmp.jpg");
                } catch (FileNotFoundException e2) {
                    try {
                        inputStream = this.a.getAssets().open("share_logo.png");
                    } catch (Exception e3) {
                    }
                }
            }
            try {
                if (this.a.q.contains(cVar.a(this.a))) {
                    String unused = this.a.q = this.a.q.replace(cVar.a(this.a), cVar.b(this.a));
                }
                cVar.a(this.a.q, inputStream);
                Exception exc2 = exc;
                z = true;
                e = exc2;
            } catch (Exception e4) {
                e = e4;
                z = z2;
            } catch (OutOfMemoryError e5) {
                e = new Exception(e5.getMessage());
                z = z2;
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e6) {
                    z2 = z;
                    exc = e;
                }
            }
            z2 = z;
            exc = e;
        }
        if (this.a.p != null && !this.a.p.isShowing()) {
            return;
        }
        if (z2) {
            this.a.r.sendEmptyMessage(200);
        } else if (exc != null) {
            Message obtain = Message.obtain();
            obtain.what = 201;
            obtain.obj = exc.getMessage();
            this.a.r.sendMessage(obtain);
        }
    }
}
