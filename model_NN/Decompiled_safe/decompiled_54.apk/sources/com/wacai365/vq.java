package com.wacai365;

import android.content.DialogInterface;

final class vq implements DialogInterface.OnClickListener {
    private /* synthetic */ aah a;

    vq(aah aah) {
        this.a = aah;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.t.h = "";
        this.a.a.t.i = "";
        if (!(this.a.a.a == null || this.a.a.a.b == null || this.a.a.a.b.length <= 0)) {
            int length = this.a.a.a.b.length;
            for (int i2 = 0; i2 < length; i2++) {
                if (this.a.a.a.b[i2]) {
                    if (this.a.a.t.h.length() == 0) {
                        StringBuilder sb = new StringBuilder();
                        QueryInfo a2 = this.a.a.t;
                        a2.h = sb.append(a2.h).append(String.format("%d", Integer.valueOf(this.a.a.a.a[i2]))).toString();
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        QueryInfo a3 = this.a.a.t;
                        a3.h = sb2.append(a3.h).append(",").append(String.format("%d", Integer.valueOf(this.a.a.a.a[i2]))).toString();
                    }
                }
            }
        }
        this.a.a.t.i = m.b(this.a.a.t.h, "name", "TBL_MEMBERINFO");
        this.a.a.g.setText(this.a.a.t.h.length() <= 0 ? this.a.a.o.getResources().getText(C0000R.string.txtFullString).toString() : this.a.a.t.i);
        dialogInterface.dismiss();
    }
}
