package com.wacai365;

import android.content.Intent;
import android.view.View;

final class as implements View.OnClickListener {
    private /* synthetic */ Wacai365 a;

    as(Wacai365 wacai365) {
        this.a = wacai365;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.button1 /*2131493192*/:
                Intent intent = new Intent(this.a, MyBalance.class);
                intent.putExtra("LaunchedByApplication", 1);
                this.a.startActivityForResult(intent, 0);
                return;
            case C0000R.id.button2 /*2131493193*/:
                this.a.startActivityForResult(new Intent(this.a, StatMain.class), 0);
                return;
            case C0000R.id.button3 /*2131493194*/:
                this.a.startActivityForResult(new Intent(this.a, DataMgrTab.class), 0);
                return;
            case C0000R.id.button4 /*2131493195*/:
                this.a.startActivityForResult(new Intent(this.a, SettingTab.class), 0);
                return;
            case C0000R.id.button5 /*2131493196*/:
                this.a.startActivityForResult(new Intent(this.a, MoreOptionsTab.class), 0);
                return;
            default:
                return;
        }
    }
}
