package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.wacai.a.a;
import com.wacai.b;
import com.wacai.data.aa;
import com.wacai.data.d;
import com.wacai.data.m;
import com.wacai.data.r;
import com.wacai.data.u;
import com.wacai.data.z;
import com.wacai.e;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MyBalance extends WacaiActivity {
    int[] a = null;
    /* access modifiers changed from: private */
    public Button b;
    /* access modifiers changed from: private */
    public TextView c;
    /* access modifiers changed from: private */
    public Button d;
    /* access modifiers changed from: private */
    public Button e;
    /* access modifiers changed from: private */
    public Button f;
    /* access modifiers changed from: private */
    public Button g;
    /* access modifiers changed from: private */
    public Button h;
    /* access modifiers changed from: private */
    public Button i;
    /* access modifiers changed from: private */
    public Button j;
    /* access modifiers changed from: private */
    public ListView k;
    /* access modifiers changed from: private */
    public TextView l;
    /* access modifiers changed from: private */
    public QueryInfo m = null;
    private int n = 0;
    private String o = "0";
    private String p = "0";
    private String q = "0";
    private String r = "0";
    private long s = 0;
    private ArrayList t = null;
    private int[] u = {C0000R.string.txtSunday, C0000R.string.txtMonday, C0000R.string.txtTuesday, C0000R.string.txtWednesday, C0000R.string.txtThursday, C0000R.string.txtFriday, C0000R.string.txtSaturday};
    private View.OnClickListener v = new ci(this);

    private ArrayList a(Cursor cursor) {
        long j2 = 0;
        if (cursor == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        while (cursor.moveToNext()) {
            long j3 = cursor.getLong(cursor.getColumnIndex("_ct"));
            long j4 = cursor.getLong(cursor.getColumnIndex("_ymd"));
            gd gdVar = new gd(this);
            gdVar.a = j4;
            gdVar.c = j2;
            gdVar.d = j2 + j3;
            Calendar instance = Calendar.getInstance();
            instance.setTime(new Date(a.b(j4)));
            String obj = getResources().getText(this.u[instance.get(7) - 1]).toString();
            StringBuffer stringBuffer = new StringBuffer(64);
            stringBuffer.append(j4 / 10000);
            stringBuffer.append("-");
            stringBuffer.append((j4 / 100) % 100);
            stringBuffer.append("-");
            stringBuffer.append(j4 % 100);
            stringBuffer.append(" ");
            stringBuffer.append(obj);
            gdVar.b = stringBuffer.toString();
            if (!((this.m.t & 8) == 0 || this.m.t == 15)) {
                long j5 = j4 / 10000;
                StringBuffer stringBuffer2 = new StringBuffer(64);
                stringBuffer2.append(j5);
                stringBuffer2.append("年");
                stringBuffer2.append((j4 / 100) % 100);
                stringBuffer2.append("月");
                gdVar.b = stringBuffer2.toString();
            }
            j2 = gdVar.d + 1;
            arrayList.add(gdVar);
        }
        cursor.close();
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        xs xsVar;
        int i3;
        long j2;
        Object itemAtPosition = this.k.getItemAtPosition(i2);
        if (itemAtPosition.getClass() != gd.class && (xsVar = (xs) itemAtPosition) != null && xsVar.a > 0) {
            int i4 = xsVar.g;
            long j3 = xsVar.a;
            if ((i4 == 0 || i4 == 1) && xsVar.m > 0) {
                i3 = 4;
                j2 = xsVar.m;
            } else {
                i3 = i4;
                j2 = j3;
            }
            Intent a2 = InputTrade.a(this, "", j2);
            a2.putExtra("LaunchedByApplication", 1);
            a2.putExtra("Extra_Type", i3);
            a2.putExtra("Extra_Id", j2);
            a2.putExtra("Extra_Type2", xsVar.h);
            startActivityForResult(a2, 19);
        }
    }

    /* access modifiers changed from: private */
    public void a(long j2) {
        this.n = 0;
        int length = this.a.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (this.a[i2] == ((int) j2)) {
                this.n = i2;
                return;
            }
        }
    }

    static /* synthetic */ void a(MyBalance myBalance, boolean z) {
        long j2;
        long j3;
        long j4;
        long j5;
        long j6;
        long j7 = myBalance.m.b / 10000;
        long j8 = (myBalance.m.b / 100) % 100;
        long j9 = myBalance.m.c / 10000;
        long j10 = (myBalance.m.c / 100) % 100;
        if ((myBalance.m.t & 8) == 0 || myBalance.m.t == 15) {
            if (z) {
                if (j8 > 1) {
                    j6 = j8 - 1;
                    j5 = j7;
                } else {
                    j5 = j7 - 1;
                    j6 = 12;
                }
                myBalance.m.c = a.a(a.a((int) j7, (int) j8, 1).b() - 1000);
                myBalance.m.b = (10000 * j5) + (100 * j6) + 1;
            } else {
                if (j10 < 12) {
                    j3 = 1 + j10;
                    j2 = j9;
                } else {
                    j2 = 1 + j9;
                    j3 = 1;
                }
                myBalance.m.b = (10000 * j2) + (100 * j3) + 1;
                if (j3 < 12) {
                    j4 = j3 + 1;
                } else {
                    j2 = 1 + j2;
                    j4 = 1;
                }
                myBalance.m.c = a.a(a.b(((j4 * 100) + (j2 * 10000)) + 1) - 1000);
            }
        } else if (z) {
            myBalance.m.c = a.a(a.a((int) j7, 1, 1).b() - 1000);
            myBalance.m.b = (10000 * (j7 - 1)) + 100 + 1;
        } else {
            long j11 = 1 + j9;
            myBalance.m.b = (10000 * j11) + 100 + 1;
            myBalance.m.c = a.a(a.b((((j11 + 1) * 10000) + 100) + 1) - 1000);
        }
        myBalance.b();
    }

    public static void a(QueryInfo queryInfo, StringBuffer stringBuffer) {
        if (-1 != queryInfo.j) {
            stringBuffer.append(" and a.money >= ");
            stringBuffer.append(queryInfo.j);
        }
        if (-1 != queryInfo.k) {
            stringBuffer.append(" and a.money <= ");
            stringBuffer.append(queryInfo.k);
        }
        stringBuffer.append(" and a.ymd >= ");
        stringBuffer.append(queryInfo.b);
        stringBuffer.append(" and a.ymd <= ");
        stringBuffer.append(queryInfo.c);
        if (queryInfo.p == 1) {
            stringBuffer.append(" and a.type = 0 ");
        } else if (queryInfo.p == 2) {
            stringBuffer.append(" and a.type = 1 ");
        }
        if (-1 != queryInfo.q) {
            stringBuffer.append(" and a.debtid = ");
            stringBuffer.append(queryInfo.q);
        }
        if (-1 != queryInfo.e) {
            stringBuffer.append(" and a.accountid = ");
            stringBuffer.append(queryInfo.e);
        }
        if (8 != queryInfo.t) {
            if (-1 != queryInfo.f) {
                stringBuffer.append(" and a.id = 0");
            }
            if (queryInfo.h.length() > 0) {
                stringBuffer.append(" and a.id = 0");
            }
            if (-1 != queryInfo.d) {
                stringBuffer.append(" and e.id = " + queryInfo.d);
            }
        }
    }

    public static void a(QueryInfo queryInfo, boolean z, StringBuffer stringBuffer) {
        if (-1 != queryInfo.j) {
            stringBuffer.append(" and a.money >= ");
            stringBuffer.append(queryInfo.j);
        }
        if (-1 != queryInfo.k) {
            stringBuffer.append(" and a.money <= ");
            stringBuffer.append(queryInfo.k);
        }
        stringBuffer.append(" and a.ymd >= ");
        stringBuffer.append(queryInfo.b);
        stringBuffer.append(" and a.ymd <= ");
        stringBuffer.append(queryInfo.c);
        if (-1 != queryInfo.e) {
            stringBuffer.append(" and a.accountid = ");
            stringBuffer.append(queryInfo.e);
        } else if (-1 != queryInfo.d) {
            stringBuffer.append(" and h.id = ");
            stringBuffer.append(queryInfo.d);
        } else if (z) {
            stringBuffer.append(" and h.id = ");
            stringBuffer.append(b.m().j());
        }
        if (-1 != queryInfo.f) {
            stringBuffer.append(" and a.projectid = ");
            stringBuffer.append(queryInfo.f);
        }
        if (-1 != queryInfo.g) {
            stringBuffer.append(" and a.reimburse = ");
            stringBuffer.append(queryInfo.g);
        }
        if (queryInfo.h.length() > 0) {
            stringBuffer.append(" and b.memberid in (");
            stringBuffer.append(queryInfo.h);
            stringBuffer.append(")");
        }
        if (-1 != queryInfo.m) {
            stringBuffer.append(" and a.subtypeid = ");
            stringBuffer.append(queryInfo.m);
        }
        if (-1 != queryInfo.l) {
            stringBuffer.append(" and g.id = ");
            stringBuffer.append(queryInfo.l);
        }
    }

    private void a(StringBuffer stringBuffer) {
        if (-1 != this.m.j) {
            stringBuffer.append(" and a.transferoutmoney >= ");
            stringBuffer.append(this.m.j);
        }
        if (-1 != this.m.k) {
            stringBuffer.append(" and a.transferoutmoney <= ");
            stringBuffer.append(this.m.k);
        }
        stringBuffer.append(" and a.ymd >= ");
        stringBuffer.append(this.m.b);
        stringBuffer.append(" and a.ymd <= ");
        stringBuffer.append(this.m.c);
        if (4 == this.m.t) {
            if (-1 != this.m.n) {
                stringBuffer.append(" and a.transferoutaccountid = ");
                stringBuffer.append(this.m.n);
            }
            if (-1 != this.m.o) {
                stringBuffer.append(" and a.transferinaccountid = ");
                stringBuffer.append(this.m.o);
            }
        } else if (-1 != this.m.e) {
            stringBuffer.append(" and ( a.transferoutaccountid = ");
            stringBuffer.append(this.m.e);
            stringBuffer.append(" OR a.transferinaccountid = ");
            stringBuffer.append(this.m.e);
            stringBuffer.append(" )");
        } else if (-1 != this.m.d) {
            stringBuffer.append(" and ( d.id = ");
            stringBuffer.append(this.m.d);
            stringBuffer.append(" OR e.id = ");
            stringBuffer.append(this.m.d);
            stringBuffer.append(" )");
        }
        if (4 != this.m.t) {
            if (-1 != this.m.f) {
                stringBuffer.append(" and a.id = 0");
            }
            if (this.m.h.length() > 0) {
                stringBuffer.append(" and a.id = 0");
            }
            if (-1 != this.m.l) {
                stringBuffer.append(" and a.id = 0");
            }
        }
    }

    /* access modifiers changed from: private */
    public static String b(long j2, Cursor cursor) {
        String str;
        StringBuilder append;
        if (!b.a) {
            return m.b(j2);
        }
        if (cursor == null) {
            return "";
        }
        str = "";
        try {
            str = cursor.getString(cursor.getColumnIndexOrThrow("_moneyflag1"));
            return append.toString();
        } catch (Exception e2) {
            return append.toString();
        } finally {
            str + m.b(j2);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        String format = m.b.format(new Date(a.b(this.m.b)));
        String format2 = m.b.format(new Date(a.b(this.m.c)));
        this.c.setText(String.format(getResources().getString(C0000R.string.txtMyBalanceTimeFormatString), format, format2));
    }

    static /* synthetic */ void b(MyBalance myBalance) {
        StringBuffer stringBuffer = new StringBuffer(3000);
        stringBuffer.append("select count(*) as _ct, _ymd from ( ");
        if ((myBalance.m.t & 1) != 0) {
            stringBuffer.append(" select a.ymd as _ymd, a.outgodate as _outgodate from tbl_outgoinfo a , TBL_OUTGOSUBTYPEINFO c, TBL_PROJECTINFO d, TBL_ACCOUNTINFO e, TBL_MEMBERINFO f, TBL_OUTGOMAINTYPEINFO g, TBL_MONEYTYPE h LEFT JOIN tbl_outgomemberinfo b  ON b.outgoid = a.id and f.id = b.memberid where a.isdelete = 0 and c.id = a.subtypeid and d.id = a.projectid and e.id = a.accountid and g.id = c.id / 10000 and e.moneytype = h.id ");
            a(myBalance.m, false, stringBuffer);
        }
        if (!((myBalance.m.t & 1) == 0 || ((myBalance.m.t & 2) == 0 && (myBalance.m.t & 4) == 0 && (myBalance.m.t & 8) == 0))) {
            stringBuffer.append(" group by a.id UNION ALL ");
        }
        if ((myBalance.m.t & 2) != 0) {
            stringBuffer.append(" select a.ymd as _ymd, a.incomedate as _outgodate from TBL_INCOMEINFO a , TBL_INCOMEMAINTYPEINFO c, TBL_PROJECTINFO d, TBL_ACCOUNTINFO e, TBL_MEMBERINFO f, TBL_MONEYTYPE g LEFT JOIN TBL_INCOMEMEMBERINFO b ON b.incomeid = a.id and f.id = b.memberid  where a.isdelete = 0 and c.id = a.typeid and d.id = a.projectid and e.id = a.accountid and e.moneytype = g.id ");
            b(myBalance.m, false, stringBuffer);
        }
        if (!((myBalance.m.t & 2) == 0 || ((myBalance.m.t & 4) == 0 && (myBalance.m.t & 8) == 0))) {
            stringBuffer.append(" group by a.id UNION ALL ");
        }
        if ((myBalance.m.t & 4) != 0) {
            stringBuffer.append(" select a.ymd as _ymd, a.date as _outgodate from TBL_TRANSFERINFO a, TBL_ACCOUNTINFO b,  TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and b.id = a.transferoutaccountid and c.id = a.transferinaccountid and b.moneytype = d.id and c.moneytype = e.id and a.type = 0 ");
            myBalance.a(stringBuffer);
        }
        if (!((myBalance.m.t & 4) == 0 || (myBalance.m.t & 8) == 0)) {
            stringBuffer.append(" group by a.id UNION ALL ");
        }
        if ((myBalance.m.t & 8) != 0 && (15 == myBalance.m.t || !(myBalance.m.p == 3 || myBalance.m.p == 4))) {
            stringBuffer.append(" select a.ymd as _ymd, a.date as _outgodate from TBL_LOAN a, TBL_ACCOUNTINFO b, TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and b.id = a.accountid and c.id = a.debtid and b.moneytype = d.id and c.moneytype = e.id ");
            a(myBalance.m, stringBuffer);
        }
        if ((myBalance.m.t & 8) != 0 && (15 == myBalance.m.t || myBalance.m.p == 0 || myBalance.m.p == -1)) {
            stringBuffer.append(" group by a.id UNION ALL ");
        }
        if ((myBalance.m.t & 8) != 0 && (15 == myBalance.m.t || !(myBalance.m.p == 1 || myBalance.m.p == 2))) {
            stringBuffer.append(" select a.ymd as _ymd, a.date as _outgodate from TBL_PAYBACK a, TBL_ACCOUNTINFO b, TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and b.id = a.accountid and c.id = a.debtid and b.moneytype = d.id and c.moneytype = e.id ");
            b(myBalance.m, stringBuffer);
        }
        stringBuffer.append(" group by a.id) group by ");
        if ((myBalance.m.t & 8) == 0 || 15 == myBalance.m.t) {
            stringBuffer.append(" _ymd ");
        } else {
            stringBuffer.append(" _ymd/100 ");
        }
        stringBuffer.append(" order by _outgodate DESC");
        ArrayList a2 = myBalance.a(e.c().b().rawQuery(stringBuffer.toString(), null));
        StringBuffer stringBuffer2 = new StringBuffer(3000);
        stringBuffer2.append("select * from ( ");
        if ((myBalance.m.t & 1) != 0) {
            stringBuffer2.append(" select g.id as _typeid, a.ymd as _ymd, 0 as _flag, a.id as _id, a.outgodate as _outgodate, a.money as _money, a.comment as _comment, a.scheduleoutgoid as _scheduleid, a.paybackid as _paybackid, c.name as _name, 0 as _type, g.name as _maintypename, 0 as _transferoutmoney, 0 as _transferinmoney, 0 as _transferoutname, 0 as _transferinname, 0 as _transferoutaccountid, 0 as _transferinaccountid, 0 as _transinmoney, h.flag as _moneyflag1, 0 as _moneyflag2, h.id as _moneytypeid1, 0 as _moneytypeid2, a.reimburse as _reimburse, a.projectid as _projectID from tbl_outgoinfo a , TBL_OUTGOSUBTYPEINFO c, TBL_PROJECTINFO d, TBL_ACCOUNTINFO e, TBL_MEMBERINFO f, TBL_OUTGOMAINTYPEINFO g, TBL_MONEYTYPE h LEFT JOIN tbl_outgomemberinfo b ON b.outgoid = a.id and f.id = b.memberid where a.isdelete = 0 and c.id = a.subtypeid and d.id = a.projectid and e.id = a.accountid and g.id = c.id / 10000 and e.moneytype = h.id ");
            a(myBalance.m, false, stringBuffer2);
        }
        if (!((myBalance.m.t & 1) == 0 || ((myBalance.m.t & 2) == 0 && (myBalance.m.t & 4) == 0 && (myBalance.m.t & 8) == 0))) {
            stringBuffer2.append(" group by a.id UNION ");
        }
        if ((myBalance.m.t & 2) != 0) {
            stringBuffer2.append(" select c.id as _typeid, a.ymd as _ymd, 1 as _flag, a.id as _id, a.incomedate as _outgodate, a.money as _money, a.comment as _comment, a.scheduleincomeid as _scheduleid, a.paybackid as _paybackid, c.name as _name, 0 as _type, 0 as _maintypename, 0 as _transferoutmoney, 0 as _transferinmoney, 0 as _transferoutname, 0 as _transferinname, 0 as _transferoutaccountid, 0 as _transferinaccountid, 0 as _transinmoney, g.flag as _moneyflag1, 0 as _moneyflag2, g.id as _moneytypeid1, 0 as _moneytypeid2, null as _reimburse, a.projectid as _projectID  from TBL_INCOMEINFO a , TBL_INCOMEMAINTYPEINFO c, TBL_PROJECTINFO d, TBL_ACCOUNTINFO e, TBL_MEMBERINFO f, TBL_MONEYTYPE g LEFT JOIN TBL_INCOMEMEMBERINFO b ON b.incomeid = a.id and f.id = b.memberid where a.isdelete = 0 and c.id = a.typeid and d.id = a.projectid and e.id = a.accountid and e.moneytype = g.id ");
            b(myBalance.m, false, stringBuffer2);
        }
        if (!((myBalance.m.t & 2) == 0 || ((myBalance.m.t & 4) == 0 && (myBalance.m.t & 8) == 0))) {
            stringBuffer2.append(" group by a.id UNION ");
        }
        if ((myBalance.m.t & 4) != 0) {
            stringBuffer2.append(" select 0 as _typeid, a.ymd as _ymd, 2 as _flag, a.id as _id, a.date as _outgodate, a.transferoutmoney as _money, a.comment as _comment, 0 as _scheduleid, 0 as _paybackid, 0 as _name, 0 as _type, 0 as _maintypename, a.transferoutmoney as _transferoutmoney, a.transferinmoney as _transferinmoney, b.name as _transferoutname, c.name as _transferinname, a.transferoutaccountid as _transferoutaccountid, a.transferinaccountid as _transferinaccountid, a.transferinmoney as _transinmoney, d.flag as _moneyflag1, e.flag as _moneyflag2, d.id as _moneytypeid1, e.id as _moneytypeid2, null as _reimburse, 0 as _projectID  from TBL_TRANSFERINFO a, TBL_ACCOUNTINFO b,  TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and b.id = a.transferoutaccountid and c.id = a.transferinaccountid and b.moneytype = d.id and c.moneytype = e.id and a.type = 0 ");
            myBalance.a(stringBuffer2);
        }
        if (!((myBalance.m.t & 4) == 0 || (myBalance.m.t & 8) == 0)) {
            stringBuffer2.append(" group by a.id UNION ");
        }
        if ((myBalance.m.t & 8) != 0 && (15 == myBalance.m.t || !(myBalance.m.p == 3 || myBalance.m.p == 4))) {
            stringBuffer2.append(" select 0 as _typeid, a.ymd as _ymd, 3 as _flag, a.id as _id, a.date as _outgodate, a.money as _money, a.comment as _comment, 0 as _scheduleid, 0 as _paybackid, 0 as _name, a.type as _type, 0 as _maintypename, 0 as _transferoutmoney, 0 as _transferinmoney, b.name as _transferoutname, c.name as _transferinname, a.accountid as _transferoutaccountid, a.debtid as _transferinaccountid, 0 as _transinmoney, d.flag as _moneyflag1, e.flag as _moneyflag2, d.id as _moneytypeid1, e.id as _moneytypeid2, null as _reimburse, 0 as _projectID from TBL_LOAN a, TBL_ACCOUNTINFO b,  TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and b.id = a.accountid and c.id = a.debtid and b.moneytype = d.id and c.moneytype = e.id ");
            a(myBalance.m, stringBuffer2);
        }
        if ((myBalance.m.t & 8) != 0 && (15 == myBalance.m.t || myBalance.m.p == 0 || myBalance.m.p == -1)) {
            stringBuffer2.append(" group by a.id UNION ");
        }
        if ((myBalance.m.t & 8) != 0 && (15 == myBalance.m.t || !(myBalance.m.p == 1 || myBalance.m.p == 2))) {
            stringBuffer2.append(" select 0 as _typeid, a.ymd as _ymd, 4 as _flag, a.id as _id, a.date as _outgodate, a.money as _money, a.comment as _comment, 0 as _scheduleid, 0 as _paybackid, 0 as _name, a.type as _type, 0 as _maintypename, 0 as _transferoutmoney, 0 as _transferinmoney, b.name as _transferoutname, c.name as _transferinname, a.accountid as _transferoutaccountid, a.debtid as _transferinaccountid, 0 as _transinmoney, d.flag as _moneyflag1, e.flag as _moneyflag2, d.id as _moneytypeid1, e.id as _moneytypeid2, null as _reimburse, 0 as _projectID from TBL_PAYBACK a, TBL_ACCOUNTINFO b,  TBL_ACCOUNTINFO c, TBL_MONEYTYPE d, TBL_MONEYTYPE e where a.isdelete = 0 and b.id = a.accountid and c.id = a.debtid and b.moneytype = d.id and c.moneytype = e.id ");
            b(myBalance.m, stringBuffer2);
        }
        stringBuffer2.append(" group by a.id) order by _outgodate DESC");
        Cursor rawQuery = e.c().b().rawQuery(stringBuffer2.toString(), null);
        myBalance.startManagingCursor(rawQuery);
        if (myBalance.s == Thread.currentThread().getId()) {
            myBalance.runOnUiThread(new ch(myBalance, new ig(myBalance, rawQuery, a2, myBalance)));
        }
    }

    private static void b(QueryInfo queryInfo, StringBuffer stringBuffer) {
        if (-1 != queryInfo.j) {
            stringBuffer.append(" and a.money >= ");
            stringBuffer.append(queryInfo.j);
        }
        if (-1 != queryInfo.k) {
            stringBuffer.append(" and a.money <= ");
            stringBuffer.append(queryInfo.k);
        }
        stringBuffer.append(" and a.ymd >= ");
        stringBuffer.append(queryInfo.b);
        stringBuffer.append(" and a.ymd <= ");
        stringBuffer.append(queryInfo.c);
        if (queryInfo.p == 3) {
            stringBuffer.append(" and a.type = 1 ");
        } else if (queryInfo.p == 4) {
            stringBuffer.append(" and a.type = 0 ");
        }
        if (-1 != queryInfo.q) {
            stringBuffer.append(" and a.debtid = ");
            stringBuffer.append(queryInfo.q);
        }
        if (-1 != queryInfo.e) {
            stringBuffer.append(" and a.accountid = ");
            stringBuffer.append(queryInfo.e);
        }
        if (8 != queryInfo.t) {
            if (-1 != queryInfo.f) {
                stringBuffer.append(" and a.id = 0");
            }
            if (queryInfo.h.length() > 0) {
                stringBuffer.append(" and a.id = 0");
            }
            if (-1 != queryInfo.d) {
                stringBuffer.append(" and e.id = " + queryInfo.d);
            }
        }
    }

    public static void b(QueryInfo queryInfo, boolean z, StringBuffer stringBuffer) {
        if (-1 != queryInfo.j) {
            stringBuffer.append(" and a.money >= ");
            stringBuffer.append(queryInfo.j);
        }
        if (-1 != queryInfo.k) {
            stringBuffer.append(" and a.money <= ");
            stringBuffer.append(queryInfo.k);
        }
        stringBuffer.append(" and a.ymd >= ");
        stringBuffer.append(queryInfo.b);
        stringBuffer.append(" and a.ymd <= ");
        stringBuffer.append(queryInfo.c);
        if (-1 != queryInfo.e) {
            stringBuffer.append(" and a.accountid = ");
            stringBuffer.append(queryInfo.e);
        } else if (-1 != queryInfo.d) {
            stringBuffer.append(" and g.id = ");
            stringBuffer.append(queryInfo.d);
        } else if (z) {
            stringBuffer.append(" and g.id = ");
            stringBuffer.append(b.m().j());
        }
        if (-1 != queryInfo.f) {
            stringBuffer.append(" and a.projectid = ");
            stringBuffer.append(queryInfo.f);
        }
        if (queryInfo.h.length() > 0) {
            stringBuffer.append(" and b.memberid in (");
            stringBuffer.append(queryInfo.h);
            stringBuffer.append(")");
        }
        if (-1 != queryInfo.l) {
            stringBuffer.append(" and a.typeid = ");
            stringBuffer.append(queryInfo.l);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x00a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c() {
        /*
            r9 = this;
            r7 = 0
            r6 = 1
            r4 = 0
            r5 = -1
            r9.n = r4
            java.lang.String r0 = "select id as _id from tbl_accountinfo where enable = 1 "
            com.wacai365.QueryInfo r1 = r9.m
            int r1 = r1.d
            if (r5 == r1) goto L_0x0031
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = " and moneytype = %d "
            java.lang.Object[] r2 = new java.lang.Object[r6]
            com.wacai365.QueryInfo r3 = r9.m
            int r3 = r3.d
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2[r4] = r3
            java.lang.String r1 = java.lang.String.format(r1, r2)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
        L_0x0031:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = " order by orderno ASC"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.wacai.e r1 = com.wacai.e.c()     // Catch:{ Exception -> 0x0096, all -> 0x009e }
            android.database.sqlite.SQLiteDatabase r1 = r1.b()     // Catch:{ Exception -> 0x0096, all -> 0x009e }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ Exception -> 0x0096, all -> 0x009e }
            if (r0 != 0) goto L_0x0059
            if (r0 == 0) goto L_0x0058
            r0.close()
        L_0x0058:
            return
        L_0x0059:
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            int r1 = r1 + 1
            int[] r1 = new int[r1]     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            r9.a = r1     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            int[] r1 = r9.a     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            r2 = 0
            r3 = -1
            r1[r2] = r3     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            r1 = r6
        L_0x006a:
            boolean r2 = r0.moveToNext()     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            if (r2 == 0) goto L_0x0090
            java.lang.String r2 = "_id"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            com.wacai365.QueryInfo r3 = r9.m     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            int r3 = r3.e     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            if (r3 == r5) goto L_0x0088
            com.wacai365.QueryInfo r3 = r9.m     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            int r3 = r3.e     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            if (r2 != r3) goto L_0x0088
            r9.n = r1     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
        L_0x0088:
            int[] r3 = r9.a     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            int r4 = r1 + 1
            r3[r1] = r2     // Catch:{ Exception -> 0x00ab, all -> 0x00a6 }
            r1 = r4
            goto L_0x006a
        L_0x0090:
            if (r0 == 0) goto L_0x0058
            r0.close()
            goto L_0x0058
        L_0x0096:
            r0 = move-exception
            r0 = r7
        L_0x0098:
            if (r0 == 0) goto L_0x0058
            r0.close()
            goto L_0x0058
        L_0x009e:
            r0 = move-exception
            r1 = r7
        L_0x00a0:
            if (r1 == 0) goto L_0x00a5
            r1.close()
        L_0x00a5:
            throw r0
        L_0x00a6:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00a0
        L_0x00ab:
            r1 = move-exception
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.MyBalance.c():void");
    }

    /* access modifiers changed from: private */
    public void d() {
        if (4 == this.m.t) {
            this.b.setText(getResources().getString(C0000R.string.txtUserDefine));
        } else if (this.a == null || this.a[this.n] == -1) {
            this.m.e = -1;
            this.b.setText(getResources().getText(C0000R.string.txtAllAccountString));
        } else {
            this.m.e = this.a[this.n];
            this.b.setText(aa.a("tbl_accountinfo", "name", this.a[this.n]));
        }
    }

    public final void a() {
        this.k.setAdapter((ListAdapter) new ig(this, null, null, null));
        this.k.setVisibility(8);
        this.l.setVisibility(0);
        cm cmVar = new cm(this);
        this.s = cmVar.getId();
        cmVar.start();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (-1 == i3) {
            switch (i2) {
                case 1:
                    if (intent != null) {
                        this.m = (QueryInfo) intent.getParcelableExtra("QUERYINFO");
                        a((long) this.m.e);
                        d();
                        b();
                        a();
                        return;
                    }
                    return;
                case 19:
                    a();
                    return;
            }
        }
        if (28 == i2) {
            a();
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        long j2;
        long j3;
        long j4;
        long j5;
        boolean z;
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        switch (menuItem.getItemId()) {
            case C0000R.id.idDelete /*2131493338*/:
                xs xsVar = (xs) this.k.getItemAtPosition(adapterContextMenuInfo.position);
                if (xsVar != null) {
                    j2 = xsVar.a;
                    j3 = xsVar.m;
                    long j6 = (long) xsVar.g;
                    j5 = (long) xsVar.f;
                    j4 = j6;
                } else {
                    j2 = -1;
                    j3 = 0;
                    j4 = 0;
                    j5 = 0;
                }
                if (j5 > 0) {
                    m.a(this, (Animation) null, 0, (View) null, C0000R.string.txtDeleteScheduleData);
                    z = false;
                } else if (j3 > 0) {
                    m.a(this, (Animation) null, 0, (View) null, C0000R.string.txtDeletePayGenData);
                    z = false;
                } else {
                    switch ((int) j4) {
                        case 0:
                            z.e(j2);
                            break;
                        case 1:
                            r.d(j2);
                            break;
                        case 2:
                            u.g(j2);
                            break;
                        case 3:
                            m.g(j2);
                            break;
                        case 4:
                            d.g(j2);
                            break;
                    }
                    WidgetProvider.a(this);
                    z = true;
                }
                if (!z) {
                    return false;
                }
                a();
                return false;
            case C0000R.id.idEdit /*2131493342*/:
                a(adapterContextMenuInfo.position);
                return false;
            case C0000R.id.idReimburse /*2131493345*/:
                long j7 = adapterContextMenuInfo.id;
                int i2 = adapterContextMenuInfo.position;
                Intent intent = new Intent(this, ReimburseDialog.class);
                xs xsVar2 = (xs) this.k.getItemAtPosition(i2);
                intent.putExtra("ITEM_TYPE_NAME", xsVar2.t);
                intent.putExtra("SUM_MONEY", xsVar2.o);
                intent.putExtra("MONEY_FLAG", xsVar2.s);
                intent.putExtra("MONEY_TYPE", (int) xsVar2.r);
                intent.putExtra("MAX_DAY", xsVar2.p);
                intent.putExtra("MIN_DAY", xsVar2.q);
                intent.putExtra("PROJECT_ID", xsVar2.n);
                intent.putExtra("ID_FOR_SQL", String.valueOf(j7));
                startActivityForResult(intent, 19);
                return false;
            default:
                return false;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.my_balance_list);
        this.b = (Button) findViewById(C0000R.id.btnAccount);
        this.b.setOnClickListener(this.v);
        this.c = (TextView) findViewById(C0000R.id.btnDate);
        this.c.setOnClickListener(this.v);
        this.d = (Button) findViewById(C0000R.id.btnDateNext);
        this.d.setOnClickListener(this.v);
        this.e = (Button) findViewById(C0000R.id.btnDatePrev);
        this.e.setOnClickListener(this.v);
        this.f = (Button) findViewById(C0000R.id.btnCancel);
        this.f.setOnClickListener(this.v);
        this.g = (Button) findViewById(C0000R.id.btnQuery);
        this.g.setOnClickListener(this.v);
        this.h = (Button) findViewById(C0000R.id.btnStats);
        this.h.setOnClickListener(this.v);
        this.i = (Button) findViewById(C0000R.id.btnCount);
        this.i.setOnClickListener(this.v);
        this.j = (Button) findViewById(C0000R.id.btnAdd);
        this.j.setOnClickListener(this.v);
        if (bundle != null) {
            this.m = (QueryInfo) bundle.getParcelable("QUERYINFO");
        } else {
            this.m = (QueryInfo) getIntent().getParcelableExtra("QUERYINFO");
        }
        if (this.m == null) {
            this.m = new QueryInfo();
            this.m.a(4);
            this.m.d = -1;
            Date date = new Date();
            this.m.c = a.a(date.getTime());
        }
        c();
        d();
        b();
        this.l = (TextView) findViewById(C0000R.id.id_listhint);
        this.k = (ListView) findViewById(C0000R.id.IOList);
        this.k.setOnItemClickListener(new cj(this));
        this.k.setOnCreateContextMenuListener(new cn(this));
        a();
    }
}
