package com.wacai365;

import android.content.res.Resources;
import android.database.Cursor;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.f;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.o;
import com.wacai.data.aa;
import com.wacai.e;
import java.util.ArrayList;
import java.util.Hashtable;

public final class jk extends hg {
    private long[] e = null;

    public jk(StatView statView, ArrayList arrayList) {
        super(statView, arrayList);
    }

    public final QueryInfo a(QueryInfo queryInfo, int i) {
        QueryInfo queryInfo2 = new QueryInfo(queryInfo);
        queryInfo2.t = 1;
        queryInfo2.l = this.e[i];
        return queryInfo2;
    }

    public final boolean a(c cVar, QueryInfo queryInfo) {
        if (cVar == null || queryInfo == null) {
            return false;
        }
        a(queryInfo);
        is.c(cVar, false);
        l lVar = new l();
        StringBuffer stringBuffer = new StringBuffer(100);
        queryInfo.a(stringBuffer, 0);
        lVar.a(m.b(stringBuffer.toString()));
        cVar.a((o) lVar);
        f fVar = new f();
        fVar.a(true);
        fVar.a("TBL_OUTGOMAINTYPEINFO");
        fVar.a(this.b);
        cVar.a((o) fVar);
        return true;
    }

    public final String b(QueryInfo queryInfo) {
        Resources resources = this.a.getResources();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(resources.getText(C0000R.string.txtStringCountOutgo).toString());
        if (b.a) {
            stringBuffer.append(aa.a("TBL_MONEYTYPE", "flag", queryInfo.d));
        }
        stringBuffer.append(m.a(this.c, 2));
        return stringBuffer.toString();
    }

    public final Class c() {
        return QueryStatByTypes.class;
    }

    public final boolean c(QueryInfo queryInfo) {
        Cursor cursor;
        if (queryInfo == null) {
            return false;
        }
        a(queryInfo);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("where a.isdelete = 0 and a.ymd >= ");
        stringBuffer.append(queryInfo.b);
        stringBuffer.append(" and a.ymd <= ");
        stringBuffer.append(queryInfo.c);
        stringBuffer.append(" and b.outgoid = a.id and a.accountid = d.id ");
        if (-1 != queryInfo.e) {
            stringBuffer.append(" and a.accountid = " + queryInfo.e);
        } else if (-1 != queryInfo.d) {
            stringBuffer.append(" and d.moneytype = " + queryInfo.d);
        }
        if (-1 != queryInfo.f) {
            stringBuffer.append(" and a.projectid = " + queryInfo.f);
        }
        if (-1 != queryInfo.g) {
            stringBuffer.append(" and a.reimburse = " + queryInfo.g);
        }
        if (queryInfo.h.length() > 0) {
            stringBuffer.append(" and b.memberid in (" + queryInfo.h + ")");
        }
        stringBuffer.append(" group by a.subtypeid/10000 order by sm DESC");
        stringBuffer.insert(0, "select c.name as nm, qtable.sm as sm, c.id as maintypeid from (select a.subtypeid/10000 as mt, sum(b.sharemoney) as sm from tbl_outgoinfo a, tbl_outgomemberinfo b, tbl_accountinfo d ");
        stringBuffer.append(" ) qtable, tbl_outgomaintypeinfo c where c.id = qtable.mt");
        try {
            Cursor rawQuery = e.c().b().rawQuery(stringBuffer.toString(), null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        int count = rawQuery.getCount();
                        this.e = new long[count];
                        for (int i = 0; i < count; i++) {
                            String string = rawQuery.getString(rawQuery.getColumnIndexOrThrow("nm"));
                            long j = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("sm"));
                            long j2 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("maintypeid"));
                            double a = m.a(j);
                            this.c += a;
                            Hashtable hashtable = new Hashtable();
                            hashtable.put("TAG_LABLE", string);
                            hashtable.put("TAG_FIRST", m.a(a, 2));
                            this.e[i] = j2;
                            this.b.add(hashtable);
                            rawQuery.moveToNext();
                        }
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return true;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    public final int d() {
        return C0000R.string.txtStatOBType;
    }

    public final void e() {
        this.c = 0.0d;
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            this.c += Double.parseDouble((String) ((Hashtable) this.b.get(i)).get("TAG_FIRST"));
        }
    }
}
