package com.wacai365;

import android.widget.CompoundButton;

final class lf implements CompoundButton.OnCheckedChangeListener {
    private /* synthetic */ DataBackupSetting a;

    lf(DataBackupSetting dataBackupSetting) {
        this.a = dataBackupSetting;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (z) {
            DataBackupSetting dataBackupSetting = this.a;
            if (new ft(dataBackupSetting).a(dataBackupSetting).booleanValue()) {
                compoundButton.setChecked(true);
                this.a.d.setVisibility(0);
                this.a.a.setVisibility(8);
                this.a.b.setText((int) C0000R.string.txtAutoBackupOnHint);
                this.a.c.setText((int) C0000R.string.txtNetCondition);
                return;
            }
        }
        compoundButton.setChecked(false);
        this.a.d.setVisibility(8);
        this.a.a.setVisibility(0);
        this.a.b.setText((int) C0000R.string.txtAutoBackupOffHint);
        this.a.c.setText((int) C0000R.string.txtHandBackup);
    }
}
