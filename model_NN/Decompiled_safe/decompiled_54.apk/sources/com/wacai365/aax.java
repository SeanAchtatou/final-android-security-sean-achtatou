package com.wacai365;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

final class aax implements AdapterView.OnItemClickListener {
    private /* synthetic */ DetailOptionsTab a;

    aax(DetailOptionsTab detailOptionsTab) {
        this.a = detailOptionsTab;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (view != null) {
            try {
                fr frVar = (fr) this.a.a.getItemAtPosition(i);
                if (frVar != null) {
                    Intent intent = new Intent(this.a, Class.forName(frVar.c));
                    intent.putExtra("LaunchedByApplication", 1);
                    this.a.startActivityForResult(intent, 0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
