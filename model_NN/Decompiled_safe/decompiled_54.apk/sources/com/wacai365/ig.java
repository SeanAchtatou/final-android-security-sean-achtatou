package com.wacai365;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

final class ig extends BaseAdapter {
    private ArrayList a = null;
    private Cursor b = null;
    private LayoutInflater c = null;
    private Context d = null;
    private /* synthetic */ MyBalance e;

    public ig(MyBalance myBalance, Cursor cursor, ArrayList arrayList, Context context) {
        this.e = myBalance;
        this.a = arrayList;
        this.b = cursor;
        this.d = context;
        this.c = this.d == null ? null : (LayoutInflater) this.d.getSystemService("layout_inflater");
    }

    public final boolean areAllItemsEnabled() {
        return false;
    }

    public final int getCount() {
        int i = 0;
        if (this.a == null) {
            return 0;
        }
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            gd gdVar = (gd) it.next();
            i = (int) (((long) i) + (gdVar.d - gdVar.c) + 1);
        }
        return i;
    }

    public final Object getItem(int i) {
        String string;
        int i2;
        Iterator it = this.a.iterator();
        long j = 0;
        while (it.hasNext()) {
            gd gdVar = (gd) it.next();
            j++;
            if (((long) i) == gdVar.c) {
                return gdVar;
            }
            if (((long) i) <= gdVar.d) {
                this.b.moveToPosition((int) (((long) i) - j));
                xs xsVar = new xs(this.e);
                xsVar.a = this.b.getLong(this.b.getColumnIndex("_id"));
                xsVar.g = this.b.getInt(this.b.getColumnIndex("_flag"));
                xsVar.h = this.b.getInt(this.b.getColumnIndex("_type"));
                MyBalance myBalance = this.e;
                Cursor cursor = this.b;
                int i3 = xsVar.g;
                int i4 = xsVar.h;
                switch (i3) {
                    case 0:
                        string = (cursor.getString(cursor.getColumnIndex("_maintypename")) + "-") + cursor.getString(cursor.getColumnIndex("_name"));
                        break;
                    case 1:
                        string = cursor.getString(cursor.getColumnIndex("_name"));
                        break;
                    case 2:
                        string = myBalance.getResources().getString(C0000R.string.txtTransferString).toString();
                        break;
                    case 3:
                        string = myBalance.getResources().getString(i4 == 1 ? C0000R.string.loanOutTosb : C0000R.string.loanInFromsb, cursor.getString(cursor.getColumnIndex("_transferinname")));
                        break;
                    case 4:
                        string = myBalance.getResources().getString(i4 == 0 ? C0000R.string.paybackFromsb : C0000R.string.paybackTosb, cursor.getString(cursor.getColumnIndex("_transferinname")));
                        break;
                    default:
                        string = "";
                        break;
                }
                xsVar.b = string;
                xsVar.j = this.b.getLong(this.b.getColumnIndex("_reimburse"));
                xsVar.o = this.b.getLong(this.b.getColumnIndex("_money"));
                xsVar.c = MyBalance.b(xsVar.o, this.b);
                xsVar.k = this.b.getLong(this.b.getColumnIndex("_outgodate"));
                xsVar.e = m.d.format(new Date(xsVar.k * 1000));
                if (!((this.e.m.t & 8) == 0 || this.e.m.t == 15)) {
                    xsVar.e = m.h.format(new Date(xsVar.k * 1000));
                }
                xsVar.n = this.b.getLong(this.b.getColumnIndex("_projectID"));
                xsVar.f = this.b.getInt(this.b.getColumnIndex("_scheduleid"));
                xsVar.i = this.b.getInt(this.b.getColumnIndex("_typeid"));
                xsVar.m = this.b.getLong(this.b.getColumnIndex("_paybackid"));
                xsVar.s = this.b.getString(this.b.getColumnIndex("_moneyflag1"));
                xsVar.r = this.b.getLong(this.b.getColumnIndex("_moneytypeid1"));
                xsVar.t = this.b.getString(this.b.getColumnIndex("_name"));
                long j2 = this.b.getLong(this.b.getColumnIndex("_ymd"));
                xsVar.q = j2;
                xsVar.p = j2;
                if (xsVar.f != 0) {
                    xsVar.d = this.d.getResources().getString(C0000R.string.txtScheduleItem);
                } else {
                    String string2 = this.b.getString(this.b.getColumnIndexOrThrow("_comment"));
                    if (string2 == null || string2.length() <= 0) {
                        string2 = this.d.getResources().getString(C0000R.string.txtNoCommentsString);
                    }
                    xsVar.d = string2;
                }
                long j3 = (long) xsVar.g;
                long j4 = (long) xsVar.h;
                if (0 == j3) {
                    i2 = C0000R.drawable.ic_outgo;
                } else if (1 == j3) {
                    i2 = C0000R.drawable.ic_income;
                } else if (2 == j3) {
                    i2 = C0000R.drawable.ic_transfer;
                } else {
                    if (3 == j3) {
                        if (j4 == 1) {
                            i2 = C0000R.drawable.ic_loanout;
                        } else if (j4 == 0) {
                            i2 = C0000R.drawable.ic_loanin;
                        }
                    } else if (4 == j3) {
                        if (j4 == 0) {
                            i2 = C0000R.drawable.ic_receipt;
                        } else if (j4 == 1) {
                            i2 = C0000R.drawable.ic_repayment;
                        }
                    }
                    i2 = -1;
                }
                xsVar.l = i2;
                return xsVar;
            }
        }
        return null;
    }

    public final long getItemId(int i) {
        Object item = getItem(i);
        if (item.getClass() == gd.class) {
            return 0;
        }
        if (item.getClass() == xs.class) {
            return ((xs) item).a;
        }
        return 0;
    }

    public final int getItemViewType(int i) {
        Object item = getItem(i);
        return (item == null || item.getClass() == gd.class) ? 0 : 1;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        Object item = getItem(i);
        int itemViewType = getItemViewType(i);
        View inflate = view == null ? itemViewType == 0 ? this.c.inflate((int) C0000R.layout.balance_list_time_item, (ViewGroup) null) : this.c.inflate((int) C0000R.layout.balance_list_item, (ViewGroup) null) : view;
        if (item == null) {
            return inflate;
        }
        if (itemViewType == 0) {
            TextView textView = (TextView) inflate.findViewById(C0000R.id.datetitle);
            if (textView != null) {
                textView.setText(((gd) item).b);
            }
        } else {
            TextView textView2 = (TextView) inflate.findViewById(C0000R.id.headertitle);
            if (textView2 != null) {
                Context context = this.d;
                int i2 = (int) ((xs) item).j;
                long j = (long) ((xs) item).f;
                if (textView2 != null) {
                    if (((long) i2) == 1 && j == 0) {
                        textView2.setTextColor(context.getResources().getColor(C0000R.color.outgo_money));
                    } else {
                        textView2.setTextColor(context.getResources().getColor(C0000R.color.text_color_black));
                    }
                }
                textView2.setText(((xs) item).b);
            }
            TextView textView3 = (TextView) inflate.findViewById(C0000R.id.headervalue);
            if (textView3 != null) {
                Context context2 = this.d;
                int i3 = ((xs) item).g;
                if (textView3 != null) {
                    switch (i3) {
                        case 0:
                            textView3.setTextColor(context2.getResources().getColor(C0000R.color.outgo_money));
                            break;
                        case 1:
                            textView3.setTextColor(context2.getResources().getColor(C0000R.color.income_money));
                            break;
                        case 2:
                        case 3:
                        case 4:
                            textView3.setTextColor(context2.getResources().getColor(C0000R.color.transfer_money));
                            break;
                    }
                }
                textView3.setText(((xs) item).c);
            }
            TextView textView4 = (TextView) inflate.findViewById(C0000R.id.id_comments);
            if (textView4 != null) {
                textView4.setText(((xs) item).d);
            }
            TextView textView5 = (TextView) inflate.findViewById(C0000R.id.datetitle);
            if (textView5 != null) {
                textView5.setText(((xs) item).e);
            }
            LinearLayout linearLayout = (LinearLayout) inflate.findViewById(C0000R.id.id_icon);
            if (linearLayout != null) {
                linearLayout.setBackgroundResource(((xs) item).l);
            }
        }
        return inflate;
    }

    public final int getViewTypeCount() {
        return 2;
    }

    public final boolean isEnabled(int i) {
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            if (((long) i) == ((gd) it.next()).c) {
                return false;
            }
        }
        return true;
    }
}
