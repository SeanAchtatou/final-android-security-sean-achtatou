package com.wacai365;

import android.widget.CompoundButton;

final class gh implements CompoundButton.OnCheckedChangeListener {
    private /* synthetic */ InputAccount a;

    gh(InputAccount inputAccount) {
        this.a = inputAccount;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        this.a.C.a(z);
        if (!z) {
            this.a.C.b(false);
            this.a.h.setChecked(false);
        }
        this.a.g();
    }
}
