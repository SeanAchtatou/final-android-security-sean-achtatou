package com.wacai365;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class af extends ca implements View.OnClickListener {
    private Button A = null;
    private Button B = null;
    private TextView C = null;
    private vj D = vj.MODE_SIMPLE;
    private boolean E = true;
    private int F = 0;
    private double G = 0.0d;
    private boolean H = false;
    private boolean I = false;
    /* access modifiers changed from: private */
    public Object J = null;
    /* access modifiers changed from: private */
    public va K = null;
    private ViewGroup a = null;
    private Context b = null;
    private LinearLayout c = null;
    private String d = "";
    private TextView e;
    private TextView f;
    private TextView g;
    private TextView h;
    private TextView i;
    private TextView j;
    private TextView k;
    private TextView l;
    private TextView m;
    private TextView n;
    private TextView o;
    private TextView p;
    private TextView q;
    private TextView r;
    private TextView s;
    private TextView t;
    private TextView u;
    private TextView v;
    private TextView w;
    private Button x = null;
    private Button y = null;
    private Button z = null;

    public static af a(ViewGroup viewGroup, Context context, double d2, boolean z2) {
        af afVar = new af();
        afVar.H = z2;
        afVar.b = context;
        afVar.a = viewGroup;
        afVar.a(d2, vj.MODE_SIMPLE);
        afVar.a();
        return afVar;
    }

    private void a(double d2) {
        this.d = String.valueOf(d2);
        f();
    }

    private void a(double d2, vj vjVar) {
        this.G = d2;
        if (this.G - ((double) ((long) this.G)) == 0.0d) {
            this.d = Long.toString((long) this.G);
        } else {
            this.d = Double.toString(this.G);
        }
        if (vj.MODE_SIMPLE == vjVar) {
            this.c = (LinearLayout) ((LayoutInflater) this.b.getSystemService("layout_inflater")).inflate((int) C0000R.layout.choose_money_view, (ViewGroup) null);
            b(false);
            this.y = (Button) this.c.findViewById(C0000R.id.btnCancel);
            this.y.setOnClickListener(this);
            this.z = (Button) this.c.findViewById(C0000R.id.btnCal);
            this.z.setOnClickListener(this);
            this.x = (Button) this.c.findViewById(C0000R.id.btnOK);
            this.x.setOnClickListener(this);
        } else if (vj.MODE_FULLSCREEN == vjVar) {
            d();
            this.C.setText(this.d);
        }
    }

    private void a(int i2) {
        if (this.E || this.d == null || this.d.length() <= 0) {
            this.F = i2;
            return;
        }
        double parseDouble = Double.parseDouble(this.d);
        if (this.F == 0) {
            this.G = parseDouble;
            if (i2 != 0) {
                this.F = i2;
                this.E = true;
                return;
            }
            return;
        }
        boolean b2 = b(parseDouble);
        if (this.F != 0) {
            if (b2) {
                this.F = i2;
            }
            this.E = true;
            c(false);
            a(this.G);
        }
    }

    private void a(String str) {
        if (this.F == 0) {
            this.G = 0.0d;
        }
        if (this.E) {
            this.d = "";
            this.E = false;
        }
        String str2 = new String(this.d);
        if (str.equals(".")) {
            if (this.d != null && this.d.length() > 0 && this.d.indexOf(".") < 0) {
                this.d += str;
            } else if (this.d.indexOf(".") < 0) {
                this.d = "0.";
            } else {
                return;
            }
        } else if (this.d == null || this.d.length() <= 0) {
            if (!str.equals("-")) {
                this.d = str;
            }
        } else if (str.equals("-")) {
            double parseDouble = Double.parseDouble(this.d);
            if (parseDouble > 0.0d) {
                this.d = "-" + this.d;
            } else if (parseDouble < 0.0d) {
                this.d = this.d.substring(1);
            }
        } else {
            this.d += str;
        }
        if (this.d.length() > 0) {
            double parseDouble2 = Double.parseDouble(this.d);
            if (parseDouble2 > vk.a || parseDouble2 < vk.b) {
                this.d = str2;
                return;
            } else if (this.D == vj.MODE_SIMPLE) {
                this.G = parseDouble2;
            }
        }
        g();
        c(false);
        f();
    }

    public static af b(ViewGroup viewGroup, Context context, double d2, boolean z2) {
        af afVar = new af();
        afVar.H = z2;
        afVar.b = context;
        afVar.a = viewGroup;
        afVar.I = true;
        afVar.a(d2, vj.MODE_FULLSCREEN);
        afVar.a();
        return afVar;
    }

    private void b(boolean z2) {
        this.e = (TextView) this.c.findViewById(C0000R.id.btnDot);
        this.e.setOnClickListener(this);
        this.g = (TextView) this.c.findViewById(C0000R.id.btn0);
        this.g.setOnClickListener(this);
        this.h = (TextView) this.c.findViewById(C0000R.id.btn1);
        this.h.setOnClickListener(this);
        this.i = (TextView) this.c.findViewById(C0000R.id.btn2);
        this.i.setOnClickListener(this);
        this.j = (TextView) this.c.findViewById(C0000R.id.btn3);
        this.j.setOnClickListener(this);
        this.k = (TextView) this.c.findViewById(C0000R.id.btn4);
        this.k.setOnClickListener(this);
        this.l = (TextView) this.c.findViewById(C0000R.id.btn5);
        this.l.setOnClickListener(this);
        this.m = (TextView) this.c.findViewById(C0000R.id.btn6);
        this.m.setOnClickListener(this);
        this.n = (TextView) this.c.findViewById(C0000R.id.btn7);
        this.n.setOnClickListener(this);
        this.o = (TextView) this.c.findViewById(C0000R.id.btn8);
        this.o.setOnClickListener(this);
        this.p = (TextView) this.c.findViewById(C0000R.id.btn9);
        this.p.setOnClickListener(this);
        this.r = (TextView) this.c.findViewById(C0000R.id.btnDel);
        this.r.setOnClickListener(this);
        this.r.setOnLongClickListener(new nm(this));
        if (z2) {
            this.f = (TextView) this.c.findViewById(C0000R.id.btnClear);
            this.f.setOnClickListener(this);
            this.q = (TextView) this.c.findViewById(C0000R.id.btnPM);
            this.q.setOnClickListener(this);
            this.s = (TextView) this.c.findViewById(C0000R.id.btnPlus);
            this.s.setOnClickListener(this);
            this.t = (TextView) this.c.findViewById(C0000R.id.btnMinus);
            this.t.setOnClickListener(this);
            this.u = (TextView) this.c.findViewById(C0000R.id.btnMult);
            this.u.setOnClickListener(this);
            this.v = (TextView) this.c.findViewById(C0000R.id.btnDivi);
            this.v.setOnClickListener(this);
            this.w = (TextView) this.c.findViewById(C0000R.id.btnEqual);
            this.w.setOnClickListener(this);
            this.C = (TextView) this.c.findViewById(C0000R.id.id_display);
        }
        DisplayMetrics displayMetrics = MyApp.b.getResources().getDisplayMetrics();
        if (displayMetrics.heightPixels <= 320 || displayMetrics.widthPixels <= 320) {
            this.e.setTextSize(28.0f);
            this.g.setTextSize(28.0f);
            this.h.setTextSize(28.0f);
            this.i.setTextSize(28.0f);
            this.j.setTextSize(28.0f);
            this.k.setTextSize(28.0f);
            this.l.setTextSize(28.0f);
            this.m.setTextSize(28.0f);
            this.n.setTextSize(28.0f);
            this.o.setTextSize(28.0f);
            this.p.setTextSize(28.0f);
            this.r.setTextSize(28.0f);
            if (z2) {
                this.f.setTextSize(28.0f);
                this.q.setTextSize(28.0f);
                this.s.setTextSize(28.0f);
                this.t.setTextSize(28.0f);
                this.u.setTextSize(28.0f);
                this.v.setTextSize(28.0f);
                this.w.setTextSize(28.0f);
            }
        }
    }

    private boolean b(double d2) {
        switch (this.F) {
            case 1:
                this.G += d2;
                break;
            case 2:
                this.G -= d2;
                break;
            case 3:
                this.G *= d2;
                break;
            case 4:
                if (d2 != 0.0d) {
                    this.G /= d2;
                    break;
                } else {
                    m.a(this.b, (Animation) null, -1, (View) null, (int) C0000R.string.divideZeroPrompt);
                    return false;
                }
            default:
                this.F = 0;
                break;
        }
        if (this.G > vk.a) {
            this.G = vk.a;
            m.a(this.b, (Animation) null, -1, (View) null, (int) C0000R.string.txtMoneyDecimalsLimit);
        } else if (this.G < vk.b) {
            this.G = vk.b;
            m.a(this.b, (Animation) null, -1, (View) null, (int) C0000R.string.txtMoneyDecimalsLimit);
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void c(boolean z2) {
        if ((vj.MODE_SIMPLE == this.D || z2) && this.K != null) {
            this.K.a(this.G, this.J);
        }
    }

    private void d() {
        this.D = vj.MODE_FULLSCREEN;
        this.a.removeAllViews();
        this.c = (LinearLayout) ((LayoutInflater) this.b.getSystemService("layout_inflater")).inflate(this.I ? C0000R.layout.choose_money_view_fullfordialog : C0000R.layout.choose_money_view_full, (ViewGroup) null);
        b(true);
        this.A = (Button) this.c.findViewById(C0000R.id.id_ok);
        this.A.setOnClickListener(this);
        this.B = (Button) this.c.findViewById(C0000R.id.id_cancel);
        this.B.setOnClickListener(this);
    }

    private void d(boolean z2) {
        if (this.E) {
            this.F = 0;
            return;
        }
        if (this.d == null || this.d.length() <= 0) {
            this.d = "0";
        }
        double parseDouble = Double.parseDouble(this.d);
        if (this.F != 0) {
            if (b(parseDouble)) {
                this.F = 0;
            }
            this.E = true;
            c(false);
            a(this.G);
        } else if (z2) {
            this.G = parseDouble;
            this.E = true;
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        this.G = 0.0d;
        this.d = "0";
        this.F = 0;
        this.E = false;
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.C != null) {
            this.C.setText(this.d);
        }
    }

    private boolean g() {
        if (this.d == null || this.d.length() == 0) {
            this.d = "0";
        }
        int indexOf = this.d.indexOf(".");
        if (this.d.length() > 1 && this.d.substring(0, 1).equals("0") && (indexOf < 0 || indexOf - 1 != 0)) {
            this.d = this.d.substring(1);
        }
        if (this.D == vj.MODE_SIMPLE && indexOf > 0 && this.d.length() - indexOf > 2) {
            this.d = this.d.substring(0, indexOf + 3);
            this.G = Double.parseDouble(this.d);
        }
        if (this.d.length() > 16) {
            int indexOf2 = this.d.indexOf(".");
            if (indexOf2 < 0 || indexOf2 > 16) {
                m.a(this.b, (Animation) null, (int) C0000R.anim.shake, (View) null, (int) C0000R.string.txtMoneyDecimalsLimit);
                e();
                return false;
            }
            while (this.d.length() > 16) {
                this.d = this.d.substring(0, this.d.length() - 1);
            }
            if (this.d.length() - 1 == this.d.indexOf(".")) {
                this.d = this.d.substring(0, this.d.length() - 1);
            }
            this.G = Double.parseDouble(this.d);
        }
        return true;
    }

    public final void a() {
        super.a();
        if (vj.MODE_SIMPLE == this.D) {
            this.c.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            this.a.addView(this.c);
            Animation loadAnimation = AnimationUtils.loadAnimation(this.b, C0000R.anim.popup_frame);
            loadAnimation.setAnimationListener(new nh(this));
            ((LinearLayout) this.c.findViewById(C0000R.id.id_calframe)).setAnimation(loadAnimation);
        } else if (vj.MODE_FULLSCREEN == this.D) {
            this.a.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            this.c.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
            this.a.addView(this.c);
        }
    }

    public final void a(va vaVar, Object obj) {
        this.K = vaVar;
        this.J = obj;
    }

    public final void a(boolean z2) {
        super.a(z2);
        if (!this.I) {
            this.a.removeAllViews();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams.addRule(12, -1);
            this.a.setLayoutParams(layoutParams);
        }
        if (this.K != null) {
            this.K.a(z2 ? or.RESULT_CANCEL : or.RESULT_OK, this.J);
        }
    }

    public final int b() {
        if (this.c == null) {
            return 0;
        }
        return this.c.getHeight();
    }

    public void onClick(View view) {
        if (view.equals(this.g)) {
            a("0");
        } else if (view.equals(this.f)) {
            e();
            c(false);
            f();
        } else if (view.equals(this.h)) {
            a("1");
        } else if (view.equals(this.i)) {
            a("2");
        } else if (view.equals(this.j)) {
            a("3");
        } else if (view.equals(this.k)) {
            a("4");
        } else if (view.equals(this.l)) {
            a("5");
        } else if (view.equals(this.m)) {
            a("6");
        } else if (view.equals(this.n)) {
            a("7");
        } else if (view.equals(this.o)) {
            a("8");
        } else if (view.equals(this.p)) {
            a("9");
        } else if (view.equals(this.e)) {
            a(".");
        } else if (view.equals(this.r)) {
            if (this.d != null && this.d.length() > 0) {
                if (this.E) {
                    e();
                } else {
                    int i2 = this.d.charAt(this.d.length() - 1) == '.' ? 2 : 1;
                    if (this.d.length() == i2) {
                        this.d = "0";
                    } else {
                        this.d = this.d.substring(0, this.d.length() - i2);
                    }
                    if (this.D == vj.MODE_SIMPLE) {
                        this.G = Double.parseDouble(this.d);
                    }
                }
                g();
                c(false);
                f();
            }
        } else if (view.equals(this.q)) {
            if (this.H) {
                a("-");
            } else {
                m.a(this.b, (Animation) null, 0, (View) null, (int) C0000R.string.txtCanNotSupportMinus);
            }
        } else if (view.equals(this.s)) {
            a(1);
        } else if (view.equals(this.t)) {
            a(2);
        } else if (view.equals(this.u)) {
            a(3);
        } else if (view.equals(this.v)) {
            a(4);
        } else if (view.equals(this.w)) {
            d(false);
        } else if (view.equals(this.y) || view.equals(this.x)) {
            a(true);
        } else if (view.equals(this.z)) {
            d();
            if (this.C != null) {
                this.C.setText(this.d);
            }
            a();
        } else if (view.equals(this.A)) {
            d(true);
            if ((vj.MODE_SIMPLE == this.D ? Double.parseDouble(this.d) : this.G) >= 0.0d || this.H) {
                c(true);
                a(false);
                return;
            }
            m.a(this.b, (Animation) null, 0, (View) null, (int) C0000R.string.txtCanNotSupportMinus);
        } else if (view.equals(this.B)) {
            a(true);
        }
    }
}
