package com.wacai365;

import android.content.DialogInterface;

/* renamed from: com.wacai365.do  reason: invalid class name */
final class Cdo implements DialogInterface.OnClickListener {
    private /* synthetic */ be a;

    Cdo(be beVar) {
        this.a = beVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == 0) {
            this.a.a(-1, -1, false);
        } else if (i == 1) {
            this.a.a(0, 0, false);
        } else if (i == 2) {
            this.a.a(0, 3, false);
        } else {
            this.a.a(2, 1, false);
        }
        dialogInterface.dismiss();
    }
}
