package com.wacai365;

import android.content.res.Resources;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.f;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.o;
import com.wacai.data.aa;
import java.util.ArrayList;
import java.util.Hashtable;

public final class fq extends hg {
    private long[] e = null;
    private double f;

    public fq(StatView statView, ArrayList arrayList) {
        super(statView, arrayList);
    }

    private int a(int i, int i2, double d) {
        int size = this.b.size();
        String format = String.format("%04d/%02d", Integer.valueOf(i2 / 100), Integer.valueOf(i2 % 100));
        int i3 = i;
        while (true) {
            if (i3 >= size) {
                break;
            }
            Hashtable hashtable = (Hashtable) this.b.get(i3);
            if (hashtable != null) {
                int compareTo = ((String) hashtable.get("TAG_LABLE")).compareTo(format);
                if (compareTo != 0) {
                    if (compareTo > 0) {
                        break;
                    }
                } else {
                    hashtable.put("TAG_FIRST", m.a(d, 2));
                    break;
                }
            }
            i3++;
        }
        return i3;
    }

    private int b(int i, int i2, double d) {
        int size = this.b.size();
        String format = String.format("%04d/%02d", Integer.valueOf(i2 / 100), Integer.valueOf(i2 % 100));
        int i3 = i;
        while (true) {
            if (i3 >= size) {
                break;
            }
            Hashtable hashtable = (Hashtable) this.b.get(i3);
            if (hashtable != null) {
                int compareTo = ((String) hashtable.get("TAG_LABLE")).compareTo(format);
                if (compareTo != 0) {
                    if (compareTo > 0) {
                        break;
                    }
                } else {
                    hashtable.put("TAG_SECOND", m.a(d, 2));
                    break;
                }
            }
            i3++;
        }
        return i3;
    }

    public final int a() {
        return 8;
    }

    public final QueryInfo a(QueryInfo queryInfo, int i) {
        QueryInfo queryInfo2 = new QueryInfo(queryInfo);
        queryInfo2.t = 3;
        queryInfo2.b = m.c(this.e[i]);
        queryInfo2.c = m.d(this.e[i]);
        return queryInfo2;
    }

    public final rj a(int i) {
        if (this.d == null) {
            this.d = new zn(this.a, this.b, i);
        }
        return this.d;
    }

    /* access modifiers changed from: protected */
    public final void a(QueryInfo queryInfo) {
        int i;
        if (this.b != null) {
            this.f = 0.0d;
            this.b.clear();
            long j = queryInfo.b / 100;
            long j2 = queryInfo.c / 100;
            long max = Math.max(j, j2);
            long min = Math.min(j, j2);
            this.e = new long[((int) ((((max % 100) + ((max / 100) * 12)) - ((min % 100) + ((min / 100) * 12))) + 1))];
            int i2 = (int) (j / 100);
            long j3 = j;
            int i3 = (int) (j % 100);
            int i4 = 0;
            while (j3 <= j2) {
                Hashtable hashtable = new Hashtable();
                hashtable.put("TAG_LABLE", String.format("%04d/%02d", Integer.valueOf(i2), Integer.valueOf(i3)));
                hashtable.put("TAG_FIRST", "0.0");
                hashtable.put("TAG_SECOND", "0.0");
                this.b.add(hashtable);
                this.e[i4] = (long) ((i2 * 100) + i3);
                if (i3 < 12) {
                    i3++;
                    i = i2;
                } else {
                    i = i2 + 1;
                    i3 = 1;
                }
                i4++;
                i2 = i;
                j3 = (long) ((i * 100) + i3);
            }
        }
    }

    public final boolean a(c cVar, QueryInfo queryInfo) {
        if (cVar == null || queryInfo == null) {
            return false;
        }
        a(queryInfo);
        l lVar = new l();
        StringBuffer stringBuffer = new StringBuffer(100);
        queryInfo.a(stringBuffer, 5);
        lVar.a(m.b(stringBuffer.toString()));
        cVar.a((o) lVar);
        f fVar = new f();
        fVar.a(true);
        fVar.a(1);
        fVar.a(this.b);
        cVar.a((o) fVar);
        l lVar2 = new l();
        StringBuffer stringBuffer2 = new StringBuffer(100);
        queryInfo.a(stringBuffer2, 1);
        lVar2.a(m.b(stringBuffer2.toString()));
        cVar.a((o) lVar2);
        f fVar2 = new f();
        fVar2.a(false);
        fVar2.a(1);
        fVar2.a(this.b);
        cVar.a((o) fVar2);
        return true;
    }

    public final String b(QueryInfo queryInfo) {
        Resources resources = this.a.getResources();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(resources.getText(C0000R.string.txtStringCountIncome));
        String str = "";
        if (b.a) {
            str = aa.a("TBL_MONEYTYPE", "flag", queryInfo.d);
            stringBuffer.append(str);
        }
        stringBuffer.append(m.a(this.c, 2));
        stringBuffer.append(" ");
        stringBuffer.append(resources.getText(C0000R.string.txtStringCountOutgo));
        if (b.a) {
            stringBuffer.append(str);
        }
        stringBuffer.append(m.a(this.f, 2));
        return stringBuffer.toString();
    }

    public final boolean b() {
        return true;
    }

    public final Class c() {
        return QueryStatByMonth.class;
    }

    /* JADX WARNING: Removed duplicated region for block: B:51:0x01a2  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01a7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean c(com.wacai365.QueryInfo r15) {
        /*
            r14 = this;
            r12 = 0
            r4 = -1
            r11 = 0
            if (r15 != 0) goto L_0x0007
            r0 = r11
        L_0x0006:
            return r0
        L_0x0007:
            r14.a(r15)
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r2 = "select a.ymd/100 as ym, sum(b.sharemoney) as sm from tbl_incomeinfo a, tbl_incomememberinfo b, tbl_accountinfo d "
            r0.append(r2)
            java.lang.String r2 = "select a.ymd/100 as ym, sum(b.sharemoney) as sm from tbl_outgoinfo a, tbl_outgomemberinfo b, tbl_accountinfo d "
            r1.append(r2)
            java.lang.String r2 = " where a.isdelete = 0 and a.ymd >= "
            r0.append(r2)
            long r2 = r15.b
            r0.append(r2)
            java.lang.String r2 = " and a.ymd <= "
            r0.append(r2)
            long r2 = r15.c
            r0.append(r2)
            java.lang.String r2 = " and b.incomeid = a.id and a.accountid = d.id "
            r0.append(r2)
            java.lang.String r2 = " where a.isdelete = 0 and a.ymd >= "
            r1.append(r2)
            long r2 = r15.b
            r1.append(r2)
            java.lang.String r2 = " and a.ymd <= "
            r1.append(r2)
            long r2 = r15.c
            r1.append(r2)
            java.lang.String r2 = " and b.outgoid = a.id and a.accountid = d.id "
            r1.append(r2)
            int r2 = r15.e
            if (r4 == r2) goto L_0x012a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = " and a.accountid = "
            java.lang.StringBuilder r2 = r2.append(r3)
            int r3 = r15.e
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.append(r2)
            r1.append(r2)
        L_0x006f:
            int r2 = r15.f
            if (r4 == r2) goto L_0x008e
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = " and a.projectid = "
            java.lang.StringBuilder r2 = r2.append(r3)
            int r3 = r15.f
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.append(r2)
            r1.append(r2)
        L_0x008e:
            int r2 = r15.g
            if (r4 == r2) goto L_0x00aa
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = " and a.reimburse = "
            java.lang.StringBuilder r2 = r2.append(r3)
            int r3 = r15.g
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.append(r2)
        L_0x00aa:
            java.lang.String r2 = r15.h
            int r2 = r2.length()
            if (r2 <= 0) goto L_0x00d3
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = " and b.memberid in ("
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r15.h
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ")"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.append(r2)
            r1.append(r2)
        L_0x00d3:
            java.lang.String r2 = " group by a.ymd/100 order by a.ymd/100 ASC"
            r0.append(r2)
            java.lang.String r2 = " group by a.ymd/100 order by a.ymd/100 ASC"
            r1.append(r2)
            com.wacai.e r2 = com.wacai.e.c()
            android.database.sqlite.SQLiteDatabase r2 = r2.b()
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x019d }
            r3 = 0
            android.database.Cursor r1 = r2.rawQuery(r1, r3)     // Catch:{ all -> 0x019d }
            r3 = 0
            r14.f = r3     // Catch:{ all -> 0x01ab }
            if (r1 == 0) goto L_0x014b
            boolean r3 = r1.moveToFirst()     // Catch:{ all -> 0x01ab }
            if (r3 == 0) goto L_0x014b
            int r3 = r1.getCount()     // Catch:{ all -> 0x01ab }
            r4 = r11
            r5 = r11
        L_0x0100:
            if (r4 >= r3) goto L_0x014b
            java.lang.String r6 = "ym"
            int r6 = r1.getColumnIndexOrThrow(r6)     // Catch:{ all -> 0x01ab }
            int r6 = r1.getInt(r6)     // Catch:{ all -> 0x01ab }
            java.lang.String r7 = "sm"
            int r7 = r1.getColumnIndexOrThrow(r7)     // Catch:{ all -> 0x01ab }
            long r7 = r1.getLong(r7)     // Catch:{ all -> 0x01ab }
            double r7 = com.wacai365.m.a(r7)     // Catch:{ all -> 0x01ab }
            int r5 = r14.b(r5, r6, r7)     // Catch:{ all -> 0x01ab }
            double r9 = r14.f     // Catch:{ all -> 0x01ab }
            double r6 = r9 + r7
            r14.f = r6     // Catch:{ all -> 0x01ab }
            r1.moveToNext()     // Catch:{ all -> 0x01ab }
            int r4 = r4 + 1
            goto L_0x0100
        L_0x012a:
            int r2 = r15.d
            if (r4 == r2) goto L_0x006f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = " and d.moneytype = "
            java.lang.StringBuilder r2 = r2.append(r3)
            int r3 = r15.d
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.append(r2)
            r1.append(r2)
            goto L_0x006f
        L_0x014b:
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x01ab }
            r3 = 0
            android.database.Cursor r0 = r2.rawQuery(r0, r3)     // Catch:{ all -> 0x01ab }
            r2 = 0
            r14.c = r2     // Catch:{ all -> 0x01af }
            if (r0 == 0) goto L_0x0190
            boolean r2 = r0.moveToFirst()     // Catch:{ all -> 0x01af }
            if (r2 == 0) goto L_0x0190
            int r2 = r0.getCount()     // Catch:{ all -> 0x01af }
            r3 = r11
            r4 = r11
        L_0x0166:
            if (r3 >= r2) goto L_0x0190
            java.lang.String r5 = "ym"
            int r5 = r0.getColumnIndexOrThrow(r5)     // Catch:{ all -> 0x01af }
            int r5 = r0.getInt(r5)     // Catch:{ all -> 0x01af }
            java.lang.String r6 = "sm"
            int r6 = r0.getColumnIndexOrThrow(r6)     // Catch:{ all -> 0x01af }
            long r6 = r0.getLong(r6)     // Catch:{ all -> 0x01af }
            double r6 = com.wacai365.m.a(r6)     // Catch:{ all -> 0x01af }
            int r4 = r14.a(r4, r5, r6)     // Catch:{ all -> 0x01af }
            double r8 = r14.c     // Catch:{ all -> 0x01af }
            double r5 = r8 + r6
            r14.c = r5     // Catch:{ all -> 0x01af }
            r0.moveToNext()     // Catch:{ all -> 0x01af }
            int r3 = r3 + 1
            goto L_0x0166
        L_0x0190:
            if (r1 == 0) goto L_0x0195
            r1.close()
        L_0x0195:
            if (r0 == 0) goto L_0x019a
            r0.close()
        L_0x019a:
            r0 = 1
            goto L_0x0006
        L_0x019d:
            r0 = move-exception
            r1 = r12
            r2 = r12
        L_0x01a0:
            if (r2 == 0) goto L_0x01a5
            r2.close()
        L_0x01a5:
            if (r1 == 0) goto L_0x01aa
            r1.close()
        L_0x01aa:
            throw r0
        L_0x01ab:
            r0 = move-exception
            r2 = r1
            r1 = r12
            goto L_0x01a0
        L_0x01af:
            r2 = move-exception
            r13 = r2
            r2 = r1
            r1 = r0
            r0 = r13
            goto L_0x01a0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.fq.c(com.wacai365.QueryInfo):boolean");
    }

    public final int d() {
        return C0000R.string.txtStatIOByMonth;
    }

    public final void e() {
        this.c = 0.0d;
        this.f = 0.0d;
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            this.c += Double.parseDouble((String) ((Hashtable) this.b.get(i)).get("TAG_FIRST"));
            this.f += Double.parseDouble((String) ((Hashtable) this.b.get(i)).get("TAG_SECOND"));
        }
    }
}
