package com.wacai365;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.wacai.b;

public final class sz extends SimpleCursorAdapter {
    private LayoutInflater a;
    private String[] b;
    private int[] c;
    private /* synthetic */ SettingLoanAccountMgr d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sz(SettingLoanAccountMgr settingLoanAccountMgr, Context context, int i, Cursor cursor, String[] strArr, int[] iArr) {
        super(context, i, cursor, strArr, iArr);
        this.d = settingLoanAccountMgr;
        this.c = iArr;
        this.b = strArr;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        String string;
        LinearLayout linearLayout = view == null ? (LinearLayout) this.a.inflate((int) C0000R.layout.loan_list_item, (ViewGroup) null) : (LinearLayout) view;
        View findViewById = linearLayout.findViewById(this.c[0]);
        if (findViewById != null) {
            String string2 = cursor.getString(cursor.getColumnIndexOrThrow(this.b[0]));
            if (string2 == null) {
                string2 = "";
            }
            setViewText((TextView) findViewById, string2);
        }
        View findViewById2 = linearLayout.findViewById(this.c[1]);
        String str = null;
        if (findViewById2 != null) {
            if (b.a) {
                String string3 = cursor.getString(cursor.getColumnIndexOrThrow(this.b[1]));
                if (string3 == null) {
                    string3 = "";
                }
                String string4 = cursor.getString(cursor.getColumnIndexOrThrow(this.b[3]));
                setViewText((TextView) findViewById2, string3);
                str = string4;
            } else {
                setViewText((TextView) findViewById2, "");
                str = "";
            }
        }
        View findViewById3 = linearLayout.findViewById(this.c[2]);
        long j = cursor.getLong(cursor.getColumnIndexOrThrow(this.b[2]));
        Button unused = this.d.g = (Button) linearLayout.findViewById(C0000R.id.listButton);
        this.d.g.setTag(Integer.valueOf(cursor.getPosition()));
        this.d.g.setOnClickListener(new abb(this.d));
        if (j == 0) {
            string = this.d.getString(C0000R.string.txtLoanMsg3);
            this.d.g.setText((int) C0000R.string.txtAdd);
            this.d.g.setBackgroundResource(C0000R.drawable.btn_new);
        } else {
            this.d.g.setBackgroundResource(C0000R.drawable.btn_edit);
            this.d.g.setText(j > 0 ? C0000R.string.txtMakeCollections : C0000R.string.txtRepayment);
            string = this.d.getString(j > 0 ? C0000R.string.txtLoanMsg1 : C0000R.string.txtLoanMsg2, new Object[]{str, m.a(m.a(Math.abs(j)), 2)});
        }
        if (findViewById3 != null) {
            setViewText((TextView) findViewById3, string);
        }
    }
}
