package com.wacai365;

import android.view.View;
import com.wacai.a.a;
import java.util.Calendar;
import java.util.Date;

final class xk implements View.OnClickListener {
    final /* synthetic */ QueryStatBase a;

    xk(QueryStatBase queryStatBase) {
        this.a = queryStatBase;
    }

    public final void onClick(View view) {
        Calendar instance = Calendar.getInstance();
        Date date = new Date(a.b(this.a.c.c));
        instance.setTime(date);
        if (QueryStatBase.a == this.a.C) {
            new ut(this.a, this.a, new abu(this), date, 2).show();
        } else {
            new ut(this.a, this.a, new abv(this), date, 1).show();
        }
    }
}
