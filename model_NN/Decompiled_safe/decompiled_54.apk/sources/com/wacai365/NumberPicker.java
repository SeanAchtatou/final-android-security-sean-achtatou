package com.wacai365;

import android.content.Context;
import android.os.Handler;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NumberPicker extends LinearLayout implements View.OnClickListener, View.OnFocusChangeListener, View.OnLongClickListener {
    private static nu c = new vl();
    /* access modifiers changed from: private */
    public static final char[] o = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    protected int a;
    protected int b;
    /* access modifiers changed from: private */
    public final Handler d;
    private final Runnable e;
    private final EditText f;
    /* access modifiers changed from: private */
    public final InputFilter g;
    /* access modifiers changed from: private */
    public String[] h;
    private int i;
    private int j;
    private nu k;
    /* access modifiers changed from: private */
    public long l;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public boolean n;
    private NumberPickerButton p;
    private NumberPickerButton q;

    public NumberPicker(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.wacai365.NumberPicker, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public NumberPicker(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        this.e = new vm(this);
        this.l = 300;
        setOrientation(1);
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) C0000R.layout.number_picker, (ViewGroup) this, true);
        this.d = new Handler();
        wd wdVar = new wd(this);
        this.g = new zf(this);
        this.p = (NumberPickerButton) findViewById(C0000R.id.increment);
        this.p.setOnClickListener(this);
        this.p.setOnLongClickListener(this);
        this.p.a(this);
        this.q = (NumberPickerButton) findViewById(C0000R.id.decrement);
        this.q.setOnClickListener(this);
        this.q.setOnLongClickListener(this);
        this.q.a(this);
        this.f = (EditText) findViewById(C0000R.id.timepicker_input);
        this.f.setOnFocusChangeListener(this);
        this.f.setFilters(new InputFilter[]{wdVar});
        this.f.setRawInputType(2);
        if (!isEnabled()) {
            setEnabled(false);
        }
    }

    /* access modifiers changed from: private */
    public int a(String str) {
        if (this.h == null) {
            return Integer.parseInt(str);
        }
        String str2 = str;
        for (int i2 = 0; i2 < this.h.length; i2++) {
            str2 = str2.toLowerCase();
            if (this.h[i2].toLowerCase().startsWith(str2)) {
                return i2 + this.i;
            }
        }
        try {
            return Integer.parseInt(str2);
        } catch (NumberFormatException e2) {
            return this.i;
        }
    }

    private void a(View view) {
        String valueOf = String.valueOf(((TextView) view).getText());
        if ("".equals(valueOf)) {
            d();
            return;
        }
        int a2 = a(valueOf.toString());
        if (a2 >= this.i && a2 <= this.a && this.b != a2) {
            this.j = this.b;
            this.b = a2;
        }
        d();
    }

    private void d() {
        if (this.h == null) {
            EditText editText = this.f;
            int i2 = this.b;
            editText.setText(this.k != null ? this.k.a(i2) : String.valueOf(i2));
        } else {
            this.f.setText(this.h[this.b - this.i]);
        }
        this.f.setSelection(this.f.getText().length());
    }

    public final void a() {
        this.m = false;
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        int i3 = i2 > this.a ? this.i : i2 < this.i ? this.a : i2;
        this.j = this.b;
        this.b = i3;
        d();
    }

    public final void b() {
        this.n = false;
    }

    public void onClick(View view) {
        a(this.f);
        if (!this.f.hasFocus()) {
            this.f.requestFocus();
        }
        if (C0000R.id.increment == view.getId()) {
            a(this.b + 1);
        } else if (C0000R.id.decrement == view.getId()) {
            a(this.b - 1);
        }
    }

    public void onFocusChange(View view, boolean z) {
        if (!z) {
            a(view);
        }
    }

    public boolean onLongClick(View view) {
        this.f.clearFocus();
        if (C0000R.id.increment == view.getId()) {
            this.m = true;
            this.d.post(this.e);
        } else if (C0000R.id.decrement == view.getId()) {
            this.n = true;
            this.d.post(this.e);
        }
        return true;
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.p.setEnabled(z);
        this.q.setEnabled(z);
        this.f.setEnabled(z);
    }
}
