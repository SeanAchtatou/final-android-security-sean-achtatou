package com.wacai365;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.a;
import com.wacai.data.aa;
import com.wacai.data.h;

public class HomeScreenSetting extends WacaiActivity {
    TextView a = null;
    TextView b = null;
    TextView c = null;
    TextView d = null;
    Button e = null;
    Button f = null;
    int[] g = null;
    int[] h = null;
    int i = 0;
    int j = 0;
    int k = 0;
    int l = 0;
    private LinearLayout m = null;
    private LinearLayout n = null;
    private LinearLayout o = null;
    private LinearLayout p = null;
    /* access modifiers changed from: private */
    public int q = 16;
    private View.OnClickListener r = new aao(this);

    /* access modifiers changed from: package-private */
    public final void a() {
        if (9 == this.i) {
            this.n.setVisibility(0);
        } else {
            this.n.setVisibility(8);
        }
        if (9 == this.j) {
            this.p.setVisibility(0);
        } else {
            this.p.setVisibility(8);
        }
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && intent != null) {
            switch (i2) {
                case 37:
                    if (16 == this.q) {
                        this.k = (int) intent.getLongExtra("account_Sel_Id", (long) this.k);
                        if (h.d((long) this.k).d()) {
                            m.a("TBL_ACCOUNTINFO", (long) this.k, this.b);
                            return;
                        } else {
                            m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtChoosedBalanceNotSet);
                            return;
                        }
                    } else if (32 == this.q) {
                        this.l = (int) intent.getLongExtra("account_Sel_Id", (long) this.l);
                        if (h.d((long) this.l).d()) {
                            m.a("TBL_ACCOUNTINFO", (long) this.l, this.d);
                            return;
                        } else {
                            m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtChoosedBalanceNotSet);
                            return;
                        }
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.homescreen_setting);
        this.i = (int) a.a("HomeScreenTitleType1", 1);
        this.j = (int) a.a("HomeScreenTitleType2", 2);
        this.k = (int) a.a("HomeScreenAccountID1", aa.f("TBL_ACCOUNTINFO"));
        this.l = (int) a.a("HomeScreenAccountID2", aa.f("TBL_ACCOUNTINFO"));
        this.m = (LinearLayout) findViewById(C0000R.id.layoutItem1);
        this.m.setOnClickListener(new aal(this));
        this.a = (TextView) findViewById(C0000R.id.btnItem1);
        this.a.setText(m.f(this, this.i));
        this.n = (LinearLayout) findViewById(C0000R.id.layoutItem2);
        this.n.setOnClickListener(new aak(this));
        this.b = (TextView) findViewById(C0000R.id.btnItem2);
        m.a("TBL_ACCOUNTINFO", (long) this.k, this.b);
        this.o = (LinearLayout) findViewById(C0000R.id.layoutItem3);
        this.o.setOnClickListener(new aan(this));
        this.c = (TextView) findViewById(C0000R.id.btnItem3);
        this.c.setText(m.f(this, this.j));
        this.p = (LinearLayout) findViewById(C0000R.id.layoutItem4);
        this.p.setOnClickListener(new aam(this));
        this.d = (TextView) findViewById(C0000R.id.btnItem4);
        m.a("TBL_ACCOUNTINFO", (long) this.l, this.d);
        this.e = (Button) findViewById(C0000R.id.btnSave);
        this.e.setOnClickListener(this.r);
        this.f = (Button) findViewById(C0000R.id.btnCancel);
        this.f.setOnClickListener(this.r);
        a();
    }
}
