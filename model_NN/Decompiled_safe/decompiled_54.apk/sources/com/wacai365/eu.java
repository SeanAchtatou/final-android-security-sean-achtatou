package com.wacai365;

import android.content.DialogInterface;

final class eu implements DialogInterface.OnClickListener {
    private /* synthetic */ mx a;

    eu(mx mxVar) {
        this.a = mxVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.t.p = i;
        this.a.a.f.setText(this.a.a.m[i]);
        switch (i) {
            case 0:
                this.a.a.i.setText((int) C0000R.string.txtCreditorAndDebtor);
                this.a.a.j.setText((int) C0000R.string.txtAccount);
                break;
            case 1:
                this.a.a.i.setText((int) C0000R.string.txtCreditor);
                this.a.a.j.setText((int) C0000R.string.txtCreditAccount);
                break;
            case 2:
                this.a.a.i.setText((int) C0000R.string.txtDebtor);
                this.a.a.j.setText((int) C0000R.string.txtDebitAccount);
                break;
            case 3:
                this.a.a.i.setText((int) C0000R.string.txtCreditor);
                this.a.a.j.setText((int) C0000R.string.txtRepaymentAccount);
                break;
            case 4:
                this.a.a.i.setText((int) C0000R.string.txtDebtor);
                this.a.a.j.setText((int) C0000R.string.txtMakeCollectionsAccount);
                break;
        }
        dialogInterface.dismiss();
    }
}
