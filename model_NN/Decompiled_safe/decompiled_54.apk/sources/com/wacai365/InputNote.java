package com.wacai365;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.wacai.data.v;
import com.wacai.e;
import java.util.Date;

public class InputNote extends WacaiActivity implements View.OnClickListener {
    private EditText a = null;
    private String b = null;
    private Animation c;
    private v d;
    private int e = 1;
    /* access modifiers changed from: private */
    public sd f = new sd();
    /* access modifiers changed from: private */
    public fy g;
    private yg h;

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (this.g != null) {
            this.g.a(i, i2, intent);
        }
        if (intent != null && i2 == -1 && 34 == i && this.a != null) {
            String stringExtra = intent.getStringExtra("Text_String");
            this.a.setText(stringExtra);
            this.a.setSelection(stringExtra.length());
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.btnCancel /*2131492870*/:
                finish();
                return;
            case C0000R.id.btnOK /*2131492903*/:
                if (this.h.a()) {
                    this.b = this.a.getEditableText().toString();
                    if (this.b == null || this.b.length() == 0) {
                        this.c = m.a(this, this.c, (int) C0000R.anim.shake, this.a, (int) C0000R.string.txtEmptyContent);
                        return;
                    }
                    this.d.a(this.b);
                    this.d.f(false);
                    if (this.d.a() == 0) {
                        this.d.a(new Date().getTime() / 1000);
                    }
                    this.d.c();
                    finish();
                    if (this.e == 0) {
                        abt.a(this, false);
                        return;
                    }
                    return;
                }
                return;
            case C0000R.id.btnVoice /*2131492907*/:
                VoiceInput.a(this, 100, this.a.getText().toString());
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.input_note);
        if (e.c().b() == null) {
            finish();
            return;
        }
        this.e = getIntent().getIntExtra("LaunchedByApplication", 0);
        long longExtra = getIntent().getLongExtra("Record_Id", -1);
        if (longExtra > 0) {
            setTitle((int) C0000R.string.txtEditNote);
            this.d = v.b(longExtra);
        } else {
            this.d = new v();
        }
        this.a = (EditText) findViewById(C0000R.id.etNote);
        this.h = new yg(this, (TextView) findViewById(C0000R.id.text_count_prompt), 100);
        this.a.addTextChangedListener(this.h);
        this.a.setText(this.d.b());
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(this);
        ((Button) findViewById(C0000R.id.btnOK)).setOnClickListener(this);
        ((Button) findViewById(C0000R.id.btnVoice)).setOnClickListener(this);
        if (this.e == 0) {
            this.f = abt.a(this, 104, null, new rm(this));
            this.f.b();
        }
    }
}
