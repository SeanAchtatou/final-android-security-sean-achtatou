package com.wacai365;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.wacai.e;

public class MySMS extends WacaiActivity implements DialogInterface.OnClickListener {
    /* access modifiers changed from: private */
    public Button a;
    /* access modifiers changed from: private */
    public Button b;
    /* access modifiers changed from: private */
    public ListView c;
    /* access modifiers changed from: private */
    public long d = -1;
    /* access modifiers changed from: private */
    public long e = -1;
    private long f = 0;
    /* access modifiers changed from: private */
    public String g = "";
    private String h = "";
    /* access modifiers changed from: private */
    public String[] i = null;
    private int j = 0;
    private View.OnClickListener k = new bc(this);

    /* access modifiers changed from: private */
    public void e() {
        if (this.g.length() > 0 && !vk.a(Double.parseDouble(this.g))) {
            if (this.h == null || this.h.length() == 0) {
                this.h = this.g;
            }
            this.g = "";
        }
        Intent a2 = InputTrade.a(this, this.g, 0);
        a2.putExtra("LaunchedByApplication", 1);
        a2.putExtra("Extra_Type", this.j);
        a2.putExtra("Extra_Comment", this.h);
        a2.putExtra("Extra_Date", this.f);
        a2.putExtra("Extra_Money", this.g);
        startActivityForResult(a2, 10);
    }

    private void f() {
        Cursor cursor = (Cursor) this.c.getItemAtPosition((int) this.e);
        this.h = cursor.getString(cursor.getColumnIndexOrThrow("_content"));
        this.f = cursor.getLong(cursor.getColumnIndexOrThrow("_smsdate"));
        if (this.h != null && this.h.length() != 0) {
            this.i = m.a(this.h, 2);
            if (this.i == null) {
                this.i = m.a(this.h, 1);
            }
            if (this.i == null) {
                return;
            }
            if (1 == this.i.length) {
                this.g = this.i[0].replaceAll("元", "").replace(",", "");
                e();
                return;
            }
            int length = this.i.length;
            String[] strArr = new String[(length + 1)];
            for (int i2 = 0; i2 < length; i2++) {
                strArr[i2] = this.i[i2];
            }
            strArr[length] = getText(C0000R.string.txtMoneyTotal).toString();
            this.i = strArr;
            new AlertDialog.Builder(this).setSingleChoiceItems(this.i, -1, new bb(this)).show();
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        e.c().b().execSQL(String.format("delete from TBL_SMS where id = %d", Long.valueOf(this.d)));
        ((Cursor) this.c.getItemAtPosition((int) this.e)).requery();
        this.c.invalidateViews();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.j = 0;
        f();
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.j = 1;
        f();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.j = 2;
        f();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 != -1) {
            return;
        }
        if (10 == i2 || 11 == i2 || 12 == i2) {
            e.c().b().execSQL(String.format("DELETE from tbl_sms where id = %d", Long.valueOf(this.d)));
        }
    }

    public void onClick(DialogInterface dialogInterface, int i2) {
        switch (i2) {
            case -1:
                e.c().b().execSQL(String.format("delete from TBL_SMS", new Object[0]));
                ((Cursor) this.c.getItemAtPosition((int) this.e)).requery();
                this.c.invalidateViews();
                return;
            default:
                return;
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        this.e = (long) ((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).position;
        Cursor cursor = (Cursor) this.c.getItemAtPosition((int) this.e);
        if (cursor != null) {
            this.d = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
        }
        switch (menuItem.getItemId()) {
            case C0000R.id.idDelete /*2131493338*/:
                a();
                return false;
            case C0000R.id.idSaveAsOutgo /*2131493339*/:
                b();
                return false;
            case C0000R.id.idSaveAsIncome /*2131493340*/:
                c();
                return false;
            case C0000R.id.idSaveAsTransfer /*2131493341*/:
                d();
                return false;
            case C0000R.id.idEdit /*2131493342*/:
            default:
                return false;
            case C0000R.id.idSaveAsWhiteList /*2131493343*/:
                showDialog(8);
                return false;
            case C0000R.id.idDeleteAll /*2131493344*/:
                m.a(this, (int) C0000R.string.txtDeleteAllConfirm, this);
                return false;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.mysms);
        m.b(this, 1);
        this.a = (Button) findViewById(C0000R.id.btnSetting);
        this.a.setOnClickListener(this.k);
        this.b = (Button) findViewById(C0000R.id.btnCancel);
        this.b.setOnClickListener(this.k);
        this.c = (ListView) findViewById(C0000R.id.IOList);
        this.c.setOnItemClickListener(new at(this));
        this.c.setOnCreateContextMenuListener(new av(this));
        Cursor rawQuery = e.c().b().rawQuery(String.format("select id as _id, smsdate as _smsdate, sender as _sender, content as _content from TBL_SMS order by smsdate DESC", new Object[0]), null);
        startManagingCursor(rawQuery);
        this.c.setAdapter((ListAdapter) new tm(this, C0000R.layout.list_item_line2_sms, rawQuery, new String[]{"_sender", "_smsdate", "_content"}, new int[]{C0000R.id.listitem1, C0000R.id.listitem2, C0000R.id.listitem3}));
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 6:
                return new AlertDialog.Builder(this).setItems((int) C0000R.array.SMSTransferType, new az(this)).create();
            case 7:
            default:
                return null;
            case 8:
                View inflate = LayoutInflater.from(this).inflate((int) C0000R.layout.white_member, (ViewGroup) null);
                TextView textView = (TextView) inflate.findViewById(C0000R.id.input_name);
                TextView textView2 = (TextView) inflate.findViewById(C0000R.id.input_address);
                textView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
                textView2.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
                Cursor cursor = (Cursor) this.c.getItemAtPosition((int) this.e);
                if (cursor != null) {
                    textView2.setText(cursor.getString(cursor.getColumnIndexOrThrow("_sender")));
                }
                return new AlertDialog.Builder(this).setView(inflate).setTitle((int) C0000R.string.txtWhiteMemberDetail).setPositiveButton((int) C0000R.string.txtOK, new an(this, textView2, textView)).setNegativeButton((int) C0000R.string.txtCancel, new ap(this)).setOnKeyListener(new ba(this)).create();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }
}
