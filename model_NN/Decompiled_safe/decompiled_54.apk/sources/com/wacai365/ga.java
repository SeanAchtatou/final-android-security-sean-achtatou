package com.wacai365;

import android.content.Intent;
import android.view.View;

final class ga implements View.OnClickListener {
    private /* synthetic */ SettingIncomeMainTypeMgr a;

    ga(SettingIncomeMainTypeMgr settingIncomeMainTypeMgr) {
        this.a = settingIncomeMainTypeMgr;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.a)) {
            this.a.startActivityForResult(new Intent(this.a, InputIncomeMainType.class), 0);
        } else if (view.equals(this.a.b)) {
            this.a.finish();
        }
    }
}
