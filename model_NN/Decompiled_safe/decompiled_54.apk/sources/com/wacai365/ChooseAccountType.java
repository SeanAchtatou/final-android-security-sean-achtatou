package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.wacai.a;
import com.wacai.b;
import com.wacai.e;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ChooseAccountType extends WacaiActivity {
    private ListView a;
    private LinearLayout b;
    /* access modifiers changed from: private */
    public Button c;
    /* access modifiers changed from: private */
    public Button d;
    private View.OnClickListener e = new s(this);

    private void a() {
        if (this.a == null) {
            this.a = new ListView(this);
            this.a.setDivider(new ColorDrawable(getResources().getColor(C0000R.color.listDivider)));
            this.a.setDividerHeight(1);
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        a(arrayList, arrayList2);
        this.a.setAdapter((ListAdapter) new kh(this, "TBL_ACCOUNTINFO", new int[]{C0000R.string.myFavorites, C0000R.string.all}, arrayList, arrayList2, new o(this), new p(this)));
        this.a.setOnItemClickListener(new n(this, arrayList, arrayList2));
        this.a.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.b.removeAllViews();
        this.b.addView(this.a);
    }

    /* access modifiers changed from: private */
    public void a(long j) {
        if (j > 0) {
            getIntent().putExtra("account_Sel_Id", j);
            setResult(-1, getIntent());
            finish();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ChooseAccountType.a(java.util.List, boolean):void
     arg types: [java.util.List, int]
     candidates:
      com.wacai365.ChooseAccountType.a(com.wacai365.ChooseAccountType, long):void
      com.wacai365.ChooseAccountType.a(java.util.List, java.util.List):void
      com.wacai365.ChooseAccountType.a(java.util.List, boolean):void */
    /* access modifiers changed from: private */
    public void a(List list, List list2) {
        if (list != null && list2 != null) {
            a(list, true);
            a(list2, false);
            this.a.invalidateViews();
        }
    }

    private static void a(List list, boolean z) {
        Cursor cursor;
        String a2;
        if (list != null) {
            list.clear();
            boolean z2 = !MyApp.a && b.m().f();
            StringBuilder sb = new StringBuilder("select a.id as _id, a.name as _name, a.star as _star, a.balance as _balance, a.balancedate as _balancedate, a.hasbalance as _hasbalance, b.id as _moneytypeid, b.shortname as _shortname  from tbl_accountinfo a, tbl_moneytype b where ");
            sb.append("a.type <> 3 and ");
            sb.append("a.enable = 1 and ");
            if (z) {
                sb.append("a.star = 1 and ");
            } else {
                sb.append("a.star = 0 and ");
            }
            sb.append("a.moneytype = b.id ");
            if (a.a("BasicSortStyle", 0) == 0) {
                sb.append("group by a.id  order by a.orderno ASC ");
            } else {
                sb.append("group by a.id  order by a.pinyin ASC ");
            }
            try {
                Cursor rawQuery = e.c().b().rawQuery(sb.toString(), null);
                if (rawQuery != null) {
                    try {
                        if (rawQuery.moveToFirst()) {
                            do {
                                String string = rawQuery.getString(rawQuery.getColumnIndexOrThrow("_name"));
                                String string2 = rawQuery.getString(rawQuery.getColumnIndexOrThrow("_shortname"));
                                long j = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_balance"));
                                long j2 = z2 ? 0 : rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_hasbalance"));
                                long j3 = rawQuery.getLong(rawQuery.getColumnIndexOrThrow("_balancedate"));
                                int i = rawQuery.getInt(rawQuery.getColumnIndexOrThrow("_id"));
                                HashMap hashMap = new HashMap();
                                if (0 != j2) {
                                    String b2 = m.b(m.a((long) i, j3, j));
                                    a2 = !b.a ? m.a(string, "", b2) : m.a(string, string2, b2);
                                } else {
                                    a2 = !b.a ? m.a(string, "", "") : m.a(string, string2, "");
                                }
                                hashMap.put("NAME", a2);
                                hashMap.put("ID", rawQuery.getString(rawQuery.getColumnIndexOrThrow("_id")));
                                hashMap.put("star", rawQuery.getString(rawQuery.getColumnIndexOrThrow("_star")));
                                list.add(hashMap);
                            } while (rawQuery.moveToNext());
                        }
                    } catch (Throwable th) {
                        th = th;
                        cursor = rawQuery;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (Throwable th2) {
                th = th2;
                cursor = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (-1 == i2 && intent != null) {
            switch (i) {
                case 18:
                    a(intent.getLongExtra("ret_id", 0));
                    return;
                default:
                    return;
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.account_type_list);
        this.b = (LinearLayout) findViewById(C0000R.id.baselayout);
        this.d = (Button) findViewById(C0000R.id.btnCancel);
        this.d.setOnClickListener(this.e);
        this.c = (Button) findViewById(C0000R.id.btnAdd);
        this.c.setOnClickListener(this.e);
        a();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        a();
    }
}
