package com.wacai365;

import android.content.DialogInterface;

final class bt implements DialogInterface.OnCancelListener {
    private /* synthetic */ DialogInterface.OnClickListener a;

    bt(DialogInterface.OnClickListener onClickListener) {
        this.a = onClickListener;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        if (this.a != null) {
            this.a.onClick(dialogInterface, -2);
        }
    }
}
