package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;

final class jh implements DialogInterface.OnClickListener {
    private /* synthetic */ InputTrade a;

    jh(InputTrade inputTrade) {
        this.a = inputTrade;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case -3:
                Intent intent = new Intent(this.a, DataBackupSetting.class);
                intent.putExtra("LaunchedByApplication", 1);
                this.a.startActivityForResult(intent, 0);
                break;
            case -1:
                fy unused = this.a.z = abt.a(this.a);
                break;
        }
        this.a.y.e();
    }
}
