package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;

final class rw implements DialogInterface.OnClickListener {
    private /* synthetic */ InputBasicItem a;

    rw(InputBasicItem inputBasicItem) {
        this.a = inputBasicItem;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (this.a.d) {
            this.a.a();
            return;
        }
        Intent intent = this.a.getIntent();
        if (this.a.g.x() > 0) {
            intent.putExtra("ret_id", this.a.g.x());
        }
        this.a.setResult(-1, intent);
        this.a.finish();
    }
}
