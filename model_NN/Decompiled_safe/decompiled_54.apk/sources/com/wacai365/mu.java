package com.wacai365;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public final class mu extends SimpleCursorAdapter {
    private String[] a;
    private int[] b;
    private /* synthetic */ WhiteListMgr c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mu(WhiteListMgr whiteListMgr, Context context, int i, Cursor cursor, String[] strArr, int[] iArr) {
        super(context, i, cursor, strArr, iArr);
        this.c = whiteListMgr;
        this.b = iArr;
        this.a = strArr;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        View findViewById = view.findViewById(this.b[0]);
        if (findViewById != null) {
            String string = cursor.getString(cursor.getColumnIndexOrThrow(this.a[0]));
            if (string == null) {
                string = "";
            }
            setViewText((TextView) findViewById, string);
        }
        View findViewById2 = view.findViewById(this.b[1]);
        if (findViewById2 != null) {
            String string2 = cursor.getString(cursor.getColumnIndexOrThrow(this.a[1]));
            if (string2 == null) {
                string2 = "";
            }
            setViewText((TextView) findViewById2, string2);
        }
    }
}
