package com.wacai365;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.a.a;
import com.wacai.data.h;
import java.util.Date;

public class ra extends yi {
    /* access modifiers changed from: private */
    public TextView a;
    /* access modifiers changed from: private */
    public TextView b;
    /* access modifiers changed from: private */
    public TextView c;
    /* access modifiers changed from: private */
    public TextView d;
    /* access modifiers changed from: private */
    public TextView e;
    /* access modifiers changed from: private */
    public TextView f;
    /* access modifiers changed from: private */
    public TextView g;
    /* access modifiers changed from: private */
    public int[] h = null;
    /* access modifiers changed from: private */
    public int[] i = null;
    /* access modifiers changed from: private */
    public int j = -1;
    /* access modifiers changed from: private */
    public QueryInfo k;

    public ra(QueryInfo queryInfo) {
        this.k = queryInfo;
    }

    public final void a(Context context, LinearLayout linearLayout, LinearLayout linearLayout2, Activity activity, boolean z) {
        super.a(context, linearLayout, linearLayout2, activity, true);
        LinearLayout linearLayout3 = (LinearLayout) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) C0000R.layout.query_transfer_setting_list, (ViewGroup) null).findViewById(C0000R.id.mainlayout);
        linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.removeAllViews();
        linearLayout.addView(linearLayout3);
        this.a = (TextView) linearLayout.findViewById(C0000R.id.viewBeninMoney);
        this.b = (TextView) linearLayout.findViewById(C0000R.id.viewEndMoney);
        this.c = (TextView) linearLayout.findViewById(C0000R.id.viewShortcutsDate);
        this.d = (TextView) linearLayout.findViewById(C0000R.id.viewBeginDate);
        this.e = (TextView) linearLayout.findViewById(C0000R.id.viewEndDate);
        this.f = (TextView) linearLayout.findViewById(C0000R.id.viewTransferOutAccount);
        this.g = (TextView) linearLayout.findViewById(C0000R.id.viewTransferInAccount);
        ((LinearLayout) this.p.findViewById(C0000R.id.beginMoneyItem)).setOnClickListener(new si(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.endMoneyItem)).setOnClickListener(new sk(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.dateShortcutsItem)).setOnClickListener(new sj(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.transferOutItem)).setOnClickListener(new sf(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.transferInItem)).setOnClickListener(new se(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.dateBeginItem)).setOnClickListener(new sh(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.dateEndItem)).setOnClickListener(new sg(this));
        a_();
    }

    public final void a(Object obj) {
        if (QueryInfo.class.isInstance(obj)) {
            this.k = (QueryInfo) obj;
        } else if (this.k == null) {
            this.k = new QueryInfo();
        }
    }

    public final void a_() {
        this.j = this.k.a;
        if (-1 == this.k.j) {
            this.a.setText("");
        } else {
            this.a.setText(m.a(m.a(this.k.j), 2));
        }
        if (-1 == this.k.k) {
            this.b.setText("");
        } else {
            this.b.setText(m.a(m.a(this.k.k), 2));
        }
        this.c.setText(this.n.getResources().getStringArray(C0000R.array.Times)[this.k.a]);
        this.d.setText(m.c.format(new Date(a.b(this.k.b))));
        this.e.setText(m.c.format(new Date(a.b(this.k.c))));
        if (-1 == this.k.n) {
            this.f.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.f.setText(h.e((long) this.k.n));
        }
        if (-1 == this.k.o) {
            this.g.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.g.setText(h.e((long) this.k.o));
        }
    }

    public final Object b() {
        return this.k;
    }

    public final boolean c() {
        this.k.t = 4;
        return true;
    }
}
