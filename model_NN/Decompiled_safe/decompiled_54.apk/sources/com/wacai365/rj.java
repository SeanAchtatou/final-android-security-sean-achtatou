package com.wacai365;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.b;
import com.wacai.data.aa;
import java.util.ArrayList;
import java.util.Hashtable;

public class rj extends BaseAdapter {
    protected ArrayList a;
    protected LayoutInflater b;
    protected String c = "";
    private Context d;
    private int e = 0;

    public rj(Context context, ArrayList arrayList, int i) {
        this.d = context;
        this.a = arrayList;
        a(i);
        if (this.d != null) {
            this.b = (LayoutInflater) this.d.getSystemService("layout_inflater");
        }
    }

    public final void a(int i) {
        if (i > 0 && this.e != i) {
            this.e = i;
            this.c = aa.a("TBL_MONEYTYPE", "flag", this.e);
        }
    }

    public int getCount() {
        if (this.a != null) {
            return this.a.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        Hashtable hashtable = (Hashtable) this.a.get(i);
        if (hashtable == null) {
            return null;
        }
        LinearLayout linearLayout = view == null ? (LinearLayout) this.b.inflate((int) C0000R.layout.histogram_listitem, (ViewGroup) null) : (LinearLayout) view;
        ((TextView) linearLayout.findViewById(C0000R.id.listitem1)).setText((CharSequence) hashtable.get("TAG_LABLE"));
        TextView textView = (TextView) linearLayout.findViewById(C0000R.id.listitem2);
        String str = "";
        if (b.a) {
            str = str + this.c;
        }
        textView.setText(str + ((String) hashtable.get("TAG_FIRST")));
        return linearLayout;
    }
}
