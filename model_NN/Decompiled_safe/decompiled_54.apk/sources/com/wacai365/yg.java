package com.wacai365;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

public final class yg implements TextWatcher {
    private TextView a;
    private Context b;
    private int c;
    private int d;

    yg(Context context, TextView textView, int i) {
        this.b = context;
        this.a = textView;
        this.c = i;
        this.a.setText(this.b.getResources().getString(C0000R.string.weiboCharacterCanInput, Integer.valueOf(i)));
    }

    public final boolean a() {
        if (this.c - this.d >= 0) {
            return true;
        }
        m.a(this.b, (Animation) null, 0, (View) null, (int) C0000R.string.txtCountOutBound);
        return false;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.d = charSequence.length();
        int i4 = this.c - this.d;
        if (i4 < 0) {
            this.a.setText(this.b.getResources().getString(C0000R.string.weiboInputTextExceedCount, Integer.valueOf(-i4)));
            this.a.setEnabled(false);
            return;
        }
        this.a.setText(this.b.getResources().getString(C0000R.string.weiboCharacterCanInput, Integer.valueOf(i4)));
        this.a.setEnabled(true);
    }
}
