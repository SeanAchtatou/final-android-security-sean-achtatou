package com.wacai365;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.Animation;

final class c extends Handler {
    private /* synthetic */ WeiboPublish a;

    c(WeiboPublish weiboPublish) {
        this.a = weiboPublish;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 200:
                m.a(this.a, (Animation) null, 0, (View) null, (int) C0000R.string.weiboPromptShareSuccess);
                this.a.p.dismiss();
                this.a.finish();
                return;
            case 201:
                m.a((Animation) null, 0, (View) null, (String) message.obj);
                this.a.p.dismiss();
                return;
            default:
                return;
        }
    }
}
