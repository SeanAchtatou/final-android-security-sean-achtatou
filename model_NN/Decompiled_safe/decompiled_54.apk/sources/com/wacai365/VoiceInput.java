package com.wacai365;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.iflytek.f;
import com.iflytek.k;
import com.wacai.a;
import com.wacai.a.e;
import com.wacai.b.l;
import com.wacai.b.m;

public class VoiceInput extends WacaiActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public static long b;
    private View.OnTouchListener A = new ct(this);
    private k B = new cs(this);
    private boolean a;
    private f c = null;
    /* access modifiers changed from: private */
    public EditText d;
    private LinearLayout e;
    private TextView f;
    private Button g;
    private Button h;
    private ImageView i;
    private ImageView j;
    private ImageView k;
    private FrameLayout l;
    private AnimationDrawable m;
    private uw n;
    private long o;
    private int p;
    private yg q;
    private final int r = 30;
    private final int s = 31;
    private final int t = 32;
    private final int u = 1;
    private final int v = 2;
    private final int w = 3;
    private final int x = 4;
    /* access modifiers changed from: private */
    public int y = 1;
    /* access modifiers changed from: private */
    public Handler z = new db(this);

    static /* synthetic */ long a() {
        long j2 = b;
        b = 1 + j2;
        return j2;
    }

    public static void a(Activity activity, int i2, String str) {
        long a2 = a.a("VoiceAsrUseCount", 0);
        b = a2;
        if (a2 > 0) {
            c(activity, i2, str);
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage((int) C0000R.string.voiceUseGuide);
        builder.setIcon(17301543);
        builder.setCancelable(true);
        builder.setOnCancelListener(new dd());
        builder.setPositiveButton((int) C0000R.string.voiceOk, new dc(activity, i2, str));
        builder.show();
    }

    private void b(int i2) {
        this.y = i2;
        switch (i2) {
            case 1:
                this.j.setVisibility(8);
                this.e.setVisibility(4);
                this.l.setVisibility(8);
                String obj = this.d.getText().toString();
                if (obj == null || obj.length() <= 0) {
                    this.f.setVisibility(0);
                    return;
                } else {
                    this.f.setVisibility(8);
                    return;
                }
            case 2:
                this.g.setPressed(true);
                this.j.setVisibility(0);
                this.f.setVisibility(8);
                this.e.setVisibility(0);
                this.d.setVisibility(0);
                return;
            case 3:
                this.g.setPressed(false);
                this.g.setEnabled(false);
                this.i.setEnabled(false);
                this.l.setVisibility(0);
                this.e.setVisibility(4);
                this.j.setVisibility(8);
                this.m.start();
                return;
            case 4:
                this.g.setPressed(false);
                this.g.setEnabled(true);
                this.i.setEnabled(true);
                this.m.stop();
                this.l.setVisibility(8);
                this.e.setVisibility(4);
                this.j.setVisibility(8);
                this.f.setVisibility(8);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        this.a = true;
        b(4);
        if (this.c != null) {
            this.c.c();
        }
    }

    /* access modifiers changed from: private */
    public static void c(Activity activity, int i2, String str) {
        if (!e.a()) {
            m.a(activity, (Animation) null, 0, (View) null, (int) C0000R.string.txtNoNetworkPrompt);
            return;
        }
        Intent intent = new Intent(activity, VoiceInput.class);
        intent.addFlags(67108864);
        intent.putExtra("max-text-count", i2);
        intent.putExtra("received-text", str);
        activity.startActivityForResult(intent, 34);
    }

    static /* synthetic */ boolean c(VoiceInput voiceInput) {
        if (voiceInput.n == null) {
            return false;
        }
        AsyncTask.Status status = voiceInput.n.getStatus();
        if (AsyncTask.Status.PENDING == status || AsyncTask.Status.RUNNING == status) {
            voiceInput.n.a();
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - voiceInput.o < 500) {
            return false;
        }
        voiceInput.o = currentTimeMillis;
        voiceInput.b(2);
        if (voiceInput.c == null) {
            voiceInput.c = f.a();
        }
        return voiceInput.c.a("sms", voiceInput.B);
    }

    static /* synthetic */ void e(VoiceInput voiceInput) {
        if (voiceInput.c != null) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - voiceInput.o < 500) {
                voiceInput.c.c();
                voiceInput.b(4);
                return;
            }
            voiceInput.o = currentTimeMillis;
            voiceInput.b(3);
            voiceInput.c.d();
        }
    }

    public final void a(int i2) {
        int childCount = this.e.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            if (i3 <= i2 - 1) {
                this.e.getChildAt(i3).setPressed(true);
            } else {
                this.e.getChildAt(i3).setPressed(false);
            }
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.btn_voice_ok /*2131492908*/:
                if (this.q.a() && this.d != null && this.d.getText().toString() != null) {
                    String obj = this.d.getText().toString();
                    if (m.e(obj) > this.p) {
                        m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtCountOutBound);
                        return;
                    }
                    if (this.a && e.a()) {
                        l lVar = new l();
                        lVar.a(m.b(1));
                        lVar.m();
                    }
                    Intent intent = getIntent();
                    intent.putExtra("Text_String", obj);
                    setResult(-1, intent);
                    finish();
                    return;
                }
                return;
            case C0000R.id.btn_voice_cancel /*2131492910*/:
                finish();
                return;
            case C0000R.id.voiceText /*2131493308*/:
                b(4);
                return;
            default:
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.voice_input);
        Intent intent = getIntent();
        this.p = intent.getIntExtra("max-text-count", 100);
        String stringExtra = intent.getStringExtra("received-text");
        this.d = (EditText) findViewById(C0000R.id.voiceText);
        this.d.setOnClickListener(this);
        this.q = new yg(this, (TextView) findViewById(C0000R.id.text_count_prompt), this.p);
        this.d.addTextChangedListener(this.q);
        if (stringExtra != null) {
            this.d.setText(stringExtra);
            this.d.setSelection(stringExtra.length());
        }
        this.e = (LinearLayout) findViewById(C0000R.id.volume);
        this.f = (TextView) findViewById(C0000R.id.voiceTextWarning);
        this.j = (ImageView) findViewById(C0000R.id.voice_mic_rec);
        this.h = (Button) findViewById(C0000R.id.btn_voice_cancel);
        this.h.setOnClickListener(this);
        this.i = (ImageView) findViewById(C0000R.id.btn_voice_ok);
        this.i.setOnClickListener(this);
        this.g = (Button) findViewById(C0000R.id.btn_voice_press_speak);
        this.g.setOnTouchListener(this.A);
        this.g.setLongClickable(false);
        this.l = (FrameLayout) findViewById(C0000R.id.voice_waiting);
        this.k = (ImageView) findViewById(C0000R.id.voice_recognize_anim);
        this.k.setBackgroundResource(C0000R.drawable.loading);
        this.m = (AnimationDrawable) this.k.getBackground();
        b(1);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        c();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.g != null && !this.g.isEnabled()) {
            b(4);
        }
        this.n = new uw(this, 0, C0000R.string.voiceInitLib);
        this.n.execute(null, null);
    }
}
