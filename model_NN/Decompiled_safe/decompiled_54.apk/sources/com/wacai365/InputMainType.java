package com.wacai365;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.animation.Animation;
import com.wacai.data.p;

public class InputMainType extends InputBasicItem {
    private p e;

    /* access modifiers changed from: protected */
    public final void a() {
        p pVar = new p();
        this.e = pVar;
        a(pVar);
        this.c.setText("");
        m.a(this, (Animation) null, 0, (View) null, (int) C0000R.string.txtContinuePrompt);
    }

    /* access modifiers changed from: protected */
    public final InputFilter[] b() {
        return new InputFilter[]{new InputFilter.LengthFilter(20)};
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.input_name);
        long longExtra = getIntent().getLongExtra("Record_Id", -1);
        if (longExtra > 0) {
            setTitle((int) C0000R.string.txtEditMainType);
            this.e = p.a(longExtra);
        } else {
            this.e = new p();
        }
        a(this.e);
        c();
    }
}
