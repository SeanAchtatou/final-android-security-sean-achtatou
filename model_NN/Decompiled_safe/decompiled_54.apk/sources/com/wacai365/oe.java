package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.view.View;
import android.view.animation.Animation;
import com.wacai.data.aa;
import com.wacai.e;

final class oe implements DialogInterface.OnClickListener {
    private /* synthetic */ lh a;

    oe(lh lhVar) {
        this.a = lhVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == 0) {
            this.a.a.startActivityForResult(new Intent(this.a.a, InputMember.class), 0);
        } else {
            long j = (long) this.a.a.J[i];
            Cursor rawQuery = e.c().b().rawQuery(String.format("select a.id as _id from TBL_BUDGETINFO a, TBL_MEMBERINFO b where a.memberid = b.id and b.id = %d group by b.id", Long.valueOf(j)), null);
            boolean z = (rawQuery == null ? 0 : rawQuery.getCount()) > 0;
            if (rawQuery != null) {
                rawQuery.close();
            }
            if (!z || j == this.a.a.H) {
                long unused = this.a.a.H = j;
                this.a.a.d.setText(aa.a("TBL_MEMBERINFO", "name", (int) this.a.a.H));
            } else {
                m.a(this.a.a, (Animation) null, 0, (View) null, (int) C0000R.string.txtWarningSameMember);
            }
        }
        dialogInterface.dismiss();
    }
}
