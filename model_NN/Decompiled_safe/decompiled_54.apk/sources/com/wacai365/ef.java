package com.wacai365;

import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

final class ef extends Animation {
    private /* synthetic */ int a;
    private /* synthetic */ kk b;

    ef(kk kkVar, int i) {
        this.b = kkVar;
        this.a = i;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.b.E.getLayoutParams();
        layoutParams.height = (int) (((float) this.a) * f);
        this.b.E.setLayoutParams(layoutParams);
    }
}
