package com.wacai365;

import android.content.DialogInterface;

final class ay implements DialogInterface.OnClickListener {
    private /* synthetic */ em a;

    ay(em emVar) {
        this.a = emVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.b.h = "";
        this.a.a.b.i = "";
        if (!(this.a.a.a == null || this.a.a.a.b == null || this.a.a.a.b.length <= 0)) {
            int length = this.a.a.a.b.length;
            for (int i2 = 0; i2 < length; i2++) {
                if (this.a.a.a.b[i2]) {
                    if (this.a.a.b.h.length() == 0) {
                        StringBuilder sb = new StringBuilder();
                        QueryInfo a2 = this.a.a.b;
                        a2.h = sb.append(a2.h).append(String.format("%d", Integer.valueOf(this.a.a.a.a[i2]))).toString();
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        QueryInfo a3 = this.a.a.b;
                        a3.h = sb2.append(a3.h).append(",").append(String.format("%d", Integer.valueOf(this.a.a.a.a[i2]))).toString();
                    }
                }
            }
        }
        this.a.a.b.i = m.b(this.a.a.b.h, "name", "TBL_MEMBERINFO");
        this.a.a.n.setText(this.a.a.b.h.length() <= 0 ? this.a.a.getResources().getText(C0000R.string.txtFullString).toString() : this.a.a.b.i);
        dialogInterface.dismiss();
    }
}
