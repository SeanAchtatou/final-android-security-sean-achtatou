package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.wacai.a.a;
import com.wacai.b;
import com.wacai.data.z;
import com.wacai.e;
import java.util.ArrayList;
import java.util.Date;

public class SettingReimburseMgr extends WacaiActivity {
    LinearLayout a;
    /* access modifiers changed from: private */
    public ListView b;
    private TextView c;
    /* access modifiers changed from: private */
    public Button d;
    /* access modifiers changed from: private */
    public TextView e;
    private TextView f;
    /* access modifiers changed from: private */
    public Button g;
    /* access modifiers changed from: private */
    public Button h;
    /* access modifiers changed from: private */
    public Button i;
    /* access modifiers changed from: private */
    public Button j;
    /* access modifiers changed from: private */
    public long k;
    /* access modifiers changed from: private */
    public gb l;
    private String m;
    /* access modifiers changed from: private */
    public ArrayList n = new ArrayList();
    /* access modifiers changed from: private */
    public QueryInfo o = null;
    private long p = 0;
    private View.OnClickListener q = new pd(this);

    static /* synthetic */ long a(SettingReimburseMgr settingReimburseMgr, long j2) {
        long j3 = settingReimburseMgr.k + j2;
        settingReimburseMgr.k = j3;
        return j3;
    }

    private ArrayList a(Cursor cursor) {
        long j2 = 0;
        if (cursor == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        while (cursor.moveToNext()) {
            long j3 = cursor.getLong(cursor.getColumnIndex("_ct"));
            long j4 = cursor.getLong(cursor.getColumnIndex("_ymd"));
            ry ryVar = new ry(this);
            ryVar.a = j4;
            ryVar.c = j2;
            ryVar.d = j2 + j3;
            long j5 = j4 / 10000;
            StringBuffer stringBuffer = new StringBuffer(64);
            stringBuffer.append(j5);
            stringBuffer.append(getResources().getString(C0000R.string.txtYear));
            stringBuffer.append((j4 / 100) % 100);
            stringBuffer.append(getResources().getString(C0000R.string.txtMonth));
            ryVar.b = stringBuffer.toString();
            j2 = ryVar.d + 1;
            arrayList.add(ryVar);
        }
        cursor.close();
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void a(long j2, int i2) {
        Intent intent = new Intent(this, ReimburseDialog.class);
        ye yeVar = (ye) this.b.getItemAtPosition(i2);
        int indexOf = yeVar.c.indexOf(45);
        if (indexOf < 0 || indexOf >= yeVar.c.length() - 1) {
            intent.putExtra("ITEM_TYPE_NAME", yeVar.c);
        } else {
            intent.putExtra("ITEM_TYPE_NAME", yeVar.c.substring(indexOf + 1));
        }
        intent.putExtra("SUM_MONEY", yeVar.i);
        intent.putExtra("MONEY_FLAG", this.m);
        intent.putExtra("MONEY_TYPE", (int) yeVar.k);
        intent.putExtra("MAX_DAY", yeVar.j);
        intent.putExtra("MIN_DAY", yeVar.j);
        intent.putExtra("ID_FOR_SQL", String.valueOf(j2));
        intent.putExtra("PROJECT_ID", yeVar.l);
        startActivityForResult(intent, 19);
    }

    private static void a(QueryInfo queryInfo, StringBuffer stringBuffer) {
        if (-1 != queryInfo.j) {
            stringBuffer.append(" and a.money >= ");
            stringBuffer.append(queryInfo.j);
        }
        if (-1 != queryInfo.k) {
            stringBuffer.append(" and a.money <= ");
            stringBuffer.append(queryInfo.k);
        }
        stringBuffer.append(" and a.ymd >= ");
        stringBuffer.append(queryInfo.b);
        stringBuffer.append(" and a.ymd <= ");
        stringBuffer.append(queryInfo.c);
        if (-1 != queryInfo.e) {
            stringBuffer.append(" and a.accountid = ");
            stringBuffer.append(queryInfo.e);
        } else {
            stringBuffer.append(" and h.id = ");
            stringBuffer.append(queryInfo.d);
        }
        if (-1 != queryInfo.f) {
            stringBuffer.append(" and a.projectid = ");
            stringBuffer.append(queryInfo.f);
        }
        if (queryInfo.h.length() > 0) {
            stringBuffer.append(" and b.memberid in (");
            stringBuffer.append(queryInfo.h);
            stringBuffer.append(")");
        }
        if (-1 != queryInfo.m) {
            stringBuffer.append(" and a.subtypeid = ");
            stringBuffer.append(queryInfo.m);
        }
        if (-1 != queryInfo.l) {
            stringBuffer.append(" and g.id = ");
            stringBuffer.append(queryInfo.l);
        }
    }

    static /* synthetic */ void a(SettingReimburseMgr settingReimburseMgr, boolean z) {
        long j2 = settingReimburseMgr.o.b / 10000;
        long j3 = settingReimburseMgr.o.c / 10000;
        if (z) {
            settingReimburseMgr.o.c = a.a(a.a((int) j2, 1, 1).b() - 1000);
            settingReimburseMgr.o.b = ((j2 - 1) * 10000) + 100 + 1;
        } else {
            long j4 = j3 + 1;
            settingReimburseMgr.o.b = (j4 * 10000) + 100 + 1;
            settingReimburseMgr.o.c = a.a(a.b((((j4 + 1) * 10000) + 100) + 1) - 1000);
        }
        settingReimburseMgr.b();
    }

    static /* synthetic */ long b(SettingReimburseMgr settingReimburseMgr, long j2) {
        long j3 = settingReimburseMgr.k - j2;
        settingReimburseMgr.k = j3;
        return j3;
    }

    private void b() {
        String format = m.b.format(new Date(a.b(this.o.b)));
        String format2 = m.b.format(new Date(a.b(this.o.c)));
        this.e.setText(String.format(getResources().getString(C0000R.string.txtMyBalanceTimeFormatString), format, format2));
    }

    private void b(Cursor cursor) {
        this.k = 0;
        this.n.removeAll(this.n);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                this.k += cursor.getLong(cursor.getColumnIndex("_money"));
                this.n.add(Integer.valueOf((int) cursor.getLong(cursor.getColumnIndex("_id"))));
            } while (cursor.moveToNext());
        }
    }

    static /* synthetic */ void b(SettingReimburseMgr settingReimburseMgr) {
        StringBuffer stringBuffer = new StringBuffer(3000);
        stringBuffer.append("select count(*) as _ct, _ymd from ( ");
        stringBuffer.append(" select a.ymd as _ymd, a.outgodate as _outgodate from tbl_outgoinfo a , TBL_OUTGOSUBTYPEINFO c, TBL_PROJECTINFO d, TBL_ACCOUNTINFO e, TBL_MEMBERINFO f, TBL_OUTGOMAINTYPEINFO g, TBL_MONEYTYPE h LEFT JOIN tbl_outgomemberinfo b  ON b.outgoid = a.id and f.id = b.memberid where a.isdelete = 0 and c.id = a.subtypeid and d.id = a.projectid and e.id = a.accountid and g.id = c.id / 10000 and e.moneytype = h.id and a.reimburse = 1 and a.scheduleoutgoid = 0 ");
        a(settingReimburseMgr.o, stringBuffer);
        stringBuffer.append(" group by a.id) group by _ymd/100 order by _outgodate DESC");
        ArrayList a2 = settingReimburseMgr.a(e.c().b().rawQuery(stringBuffer.toString(), null));
        StringBuffer stringBuffer2 = new StringBuffer(3000);
        stringBuffer2.append("select * from ( ");
        stringBuffer2.append("select g.id as _typeid, 0 as _flag, a.ymd as _ymd, a.id as _id, a.outgodate as _outgodate, a.money as _money, a.comment as _comment, a.scheduleoutgoid as _scheduleid, a.paybackid as _paybackid, c.name as _name, 0 as _type, g.name as _maintypename, h.flag as _moneyflag1, h.id as _moneytypeid1, a.reimburse as _reimburse, d.name as _project, a.projectid as _projectID from tbl_outgoinfo a, TBL_OUTGOSUBTYPEINFO c, TBL_PROJECTINFO d, TBL_ACCOUNTINFO e, TBL_MEMBERINFO f, TBL_OUTGOMAINTYPEINFO g, TBL_MONEYTYPE h LEFT JOIN tbl_outgomemberinfo b ON b.outgoid = a.id and f.id = b.memberid where a.isdelete = 0 and c.id = a.subtypeid and d.id = a.projectid and e.id = a.accountid and g.id = c.id / 10000 and e.moneytype = h.id and a.reimburse = 1 and a.scheduleoutgoid = 0 ");
        a(settingReimburseMgr.o, stringBuffer2);
        stringBuffer2.append(" group by a.id) order by _outgodate DESC");
        Cursor rawQuery = e.c().b().rawQuery(stringBuffer2.toString(), null);
        settingReimburseMgr.startManagingCursor(rawQuery);
        if (settingReimburseMgr.p == Thread.currentThread().getId()) {
            settingReimburseMgr.b(rawQuery);
            settingReimburseMgr.l = new gb(settingReimburseMgr, rawQuery, a2, settingReimburseMgr);
            settingReimburseMgr.runOnUiThread(new pe(settingReimburseMgr));
        }
    }

    static /* synthetic */ void g(SettingReimburseMgr settingReimburseMgr) {
        long j2;
        long j3;
        long j4;
        Boolean bool;
        String str;
        String str2;
        Cursor cursor;
        Intent intent = new Intent(settingReimburseMgr, ReimburseDialog.class);
        SQLiteDatabase b2 = e.c().b();
        if (settingReimburseMgr.n != null) {
            String str3 = "";
            for (int i2 = 0; i2 < settingReimburseMgr.n.size(); i2++) {
                str3 = str3 + ((Integer) settingReimburseMgr.n.get(i2));
                if (i2 < settingReimburseMgr.n.size() - 1) {
                    str3 = str3 + ", ";
                }
            }
            Cursor rawQuery = b2.rawQuery("SELECT MAX(ymd) as _maxymd, MIN(ymd) as _minymd  FROM tbl_outgoinfo where id in( " + str3 + ")", null);
            if (rawQuery == null || !rawQuery.moveToFirst()) {
                j2 = 0;
                j3 = 0;
            } else {
                j2 = rawQuery.getLong(rawQuery.getColumnIndex("_maxymd"));
                j3 = rawQuery.getLong(rawQuery.getColumnIndex("_minymd"));
            }
            rawQuery.close();
            Cursor rawQuery2 = b2.rawQuery("SELECT b.id as _projectID, b.name as _projectname FROM tbl_outgoinfo a, tbl_projectinfo b where a.projectid = b.id and a.id in(" + str3 + ") group by _projectname ", null);
            if (rawQuery2 == null || !rawQuery2.moveToFirst()) {
                j4 = 0;
                bool = 1;
                str = "";
            } else {
                bool = rawQuery2.getCount() <= 1;
                str = rawQuery2.getString(rawQuery2.getColumnIndex("_projectname"));
                j4 = rawQuery2.getLong(rawQuery2.getColumnIndex("_projectID"));
            }
            rawQuery2.close();
            if (settingReimburseMgr.n.size() == 1) {
                Cursor rawQuery3 = b2.rawQuery("SELECT b.name as _typename FROM tbl_outgoinfo a, tbl_outgosubtypeinfo b where a.subtypeid = b.id and a.id = " + settingReimburseMgr.n.get(0), null);
                if (rawQuery3 == null || !rawQuery3.moveToFirst()) {
                    Cursor cursor2 = rawQuery3;
                    str2 = "";
                    cursor = cursor2;
                } else {
                    Cursor cursor3 = rawQuery3;
                    str2 = rawQuery3.getString(rawQuery3.getColumnIndex("_typename"));
                    cursor = cursor3;
                }
            } else {
                str2 = "";
                cursor = rawQuery2;
            }
            cursor.close();
            if (bool.booleanValue()) {
                intent.putExtra("PROJECT_NAME", str);
                intent.putExtra("PROJECT_ID", j4);
            }
            intent.putExtra("ID_FOR_SQL", str3);
            intent.putExtra("ITEM_TYPE_NAME", str2);
        } else {
            j2 = 0;
            j3 = 0;
        }
        intent.putExtra("MONEY_FLAG", settingReimburseMgr.m);
        intent.putExtra("MONEY_TYPE", settingReimburseMgr.o.d);
        intent.putExtra("MAX_DAY", j2);
        intent.putExtra("MIN_DAY", j3);
        intent.putExtra("SUM_MONEY", settingReimburseMgr.k);
        settingReimburseMgr.startActivityForResult(intent, 19);
    }

    static /* synthetic */ void l(SettingReimburseMgr settingReimburseMgr) {
        Cursor rawQuery = e.c().b().rawQuery("SELECT flag from TBL_moneytype where id = " + settingReimburseMgr.o.d, null);
        if (rawQuery != null && rawQuery.moveToFirst()) {
            settingReimburseMgr.m = rawQuery.getString(rawQuery.getColumnIndex("flag"));
        }
        if (!b.a) {
            settingReimburseMgr.f.setText(m.b(settingReimburseMgr.k));
        } else {
            settingReimburseMgr.f.setText(settingReimburseMgr.m + m.b(settingReimburseMgr.k));
        }
        rawQuery.close();
    }

    static /* synthetic */ void m(SettingReimburseMgr settingReimburseMgr) {
        if (settingReimburseMgr.b.getAdapter().getCount() == 0) {
            settingReimburseMgr.c.setText((int) C0000R.string.txtNoContentRecord);
            settingReimburseMgr.d.setClickable(false);
            settingReimburseMgr.d.setEnabled(false);
            settingReimburseMgr.c.setVisibility(0);
            settingReimburseMgr.b.setVisibility(8);
            return;
        }
        settingReimburseMgr.d.setClickable(true);
        settingReimburseMgr.d.setEnabled(true);
        settingReimburseMgr.c.setVisibility(8);
        settingReimburseMgr.b.setVisibility(0);
    }

    public final void a() {
        this.b.setAdapter((ListAdapter) new gb(this, null, null, null));
        this.b.setVisibility(8);
        this.c.setText((int) C0000R.string.txtLoading);
        this.c.setVisibility(0);
        pc pcVar = new pc(this);
        this.p = pcVar.getId();
        pcVar.start();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (-1 == i3) {
            switch (i2) {
                case 1:
                    if (intent != null) {
                        this.o = (QueryInfo) intent.getParcelableExtra("QUERYINFO");
                        b();
                        a();
                        return;
                    }
                    return;
                case 19:
                    a();
                    return;
            }
        }
        if (28 == i2) {
            a();
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        ye yeVar;
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        switch (menuItem.getItemId()) {
            case C0000R.id.idDelete /*2131493338*/:
                ye yeVar2 = (ye) this.b.getItemAtPosition(adapterContextMenuInfo.position);
                if (yeVar2 != null) {
                    z.e(yeVar2.a);
                }
                WidgetProvider.a(this);
                a();
                return false;
            case C0000R.id.idEdit /*2131493342*/:
                Object itemAtPosition = this.b.getItemAtPosition(adapterContextMenuInfo.position);
                if (itemAtPosition.getClass() == ry.class || (yeVar = (ye) itemAtPosition) == null || yeVar.a <= 0) {
                    return false;
                }
                long j2 = yeVar.a;
                Intent a2 = InputTrade.a(this, "", j2);
                a2.putExtra("LaunchedByApplication", 1);
                a2.putExtra("Extra_Id", j2);
                startActivityForResult(a2, 19);
                return false;
            case C0000R.id.idReimburse /*2131493345*/:
                a(adapterContextMenuInfo.id, adapterContextMenuInfo.position);
                return false;
            default:
                return false;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.setting_reimburse_mgr);
        if (bundle != null) {
            this.o = (QueryInfo) bundle.getParcelable("QUERYINFO");
        } else {
            this.o = (QueryInfo) getIntent().getParcelableExtra("QUERYINFO");
        }
        if (this.o == null) {
            this.o = new QueryInfo();
            this.o.a(8);
            this.o.d = (int) b.m().j();
        }
        this.g = (Button) findViewById(C0000R.id.btnCancel);
        this.c = (TextView) findViewById(C0000R.id.id_listhint);
        this.f = (TextView) findViewById(C0000R.id.tvReimburseMoney);
        this.h = (Button) findViewById(C0000R.id.btnQuery);
        this.i = (Button) findViewById(C0000R.id.btnDatePrev);
        this.j = (Button) findViewById(C0000R.id.btnDateNext);
        this.e = (TextView) findViewById(C0000R.id.btnDate);
        this.d = (Button) findViewById(C0000R.id.btnOK);
        this.d.setText((int) C0000R.string.txtReimburseTitle);
        this.g.setOnClickListener(this.q);
        this.h.setOnClickListener(this.q);
        this.e.setOnClickListener(this.q);
        this.i.setOnClickListener(this.q);
        this.j.setOnClickListener(this.q);
        this.d.setOnClickListener(this.q);
        this.b = (ListView) findViewById(C0000R.id.IOList);
        this.b.setOnItemClickListener(new oz(this));
        this.b.setOnCreateContextMenuListener(new pa(this));
        b();
        a();
    }
}
