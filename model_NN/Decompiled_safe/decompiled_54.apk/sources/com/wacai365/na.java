package com.wacai365;

import android.content.Intent;
import android.view.View;

final class na implements View.OnClickListener {
    private /* synthetic */ LoanList a;

    na(LoanList loanList) {
        this.a = loanList;
    }

    public final void onClick(View view) {
        if (view.equals(this.a.e)) {
            this.a.finish();
        } else if (view.equals(this.a.d)) {
            Intent intent = new Intent(this.a, InputLoanAccount.class);
            intent.putExtra("Record_Id", this.a.g);
            this.a.startActivityForResult(intent, 0);
        } else if (view.equals(this.a.c)) {
            Intent intent2 = new Intent(this.a, LoanCollect.class);
            intent2.putExtra("LOANACCOUNTID", this.a.g);
            this.a.startActivity(intent2);
        }
    }
}
