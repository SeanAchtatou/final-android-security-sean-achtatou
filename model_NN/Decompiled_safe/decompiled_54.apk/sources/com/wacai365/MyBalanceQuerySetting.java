package com.wacai365;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.wacai.a.a;
import java.util.Date;

public class MyBalanceQuerySetting extends WacaiActivity {
    /* access modifiers changed from: private */
    public QueryInfo a;
    private Button b = null;
    /* access modifiers changed from: private */
    public String[] c = null;
    /* access modifiers changed from: private */
    public Button d;
    /* access modifiers changed from: private */
    public Button e;
    /* access modifiers changed from: private */
    public Button f;
    /* access modifiers changed from: private */
    public Button g;
    /* access modifiers changed from: private */
    public Button h;
    private int i = 1;
    private LinearLayout j = null;
    /* access modifiers changed from: private */
    public yi k = null;
    private View.OnClickListener l = new xt(this);

    /* access modifiers changed from: private */
    public void a() {
        yi yiVar;
        Date date = new Date();
        Date date2 = new Date();
        a.a(8, date, date2);
        if (this.a.c == a.a(date2.getTime()) && this.a.b == a.a(date.getTime())) {
            this.a.a(4);
        }
        if (15 == this.a.t) {
            if (!zz.class.isInstance(this.k)) {
                yiVar = new zz(this.a);
                this.b.setText(this.c[0]);
            } else {
                return;
            }
        } else if ((this.a.t & 1) > 0) {
            if (!qc.class.isInstance(this.k)) {
                yiVar = new qc(this.a);
                this.b.setText(this.c[1]);
            } else {
                return;
            }
        } else if ((this.a.t & 2) > 0) {
            if (!jw.class.isInstance(this.k)) {
                yiVar = new jw(this.a);
                this.b.setText(this.c[2]);
            } else {
                return;
            }
        } else if ((this.a.t & 4) > 0) {
            if (!ra.class.isInstance(this.k)) {
                yiVar = new ra(this.a);
                this.b.setText(this.c[3]);
            } else {
                return;
            }
        } else if ((this.a.t & 8) <= 0) {
            yiVar = null;
        } else if (!yq.class.isInstance(this.k)) {
            a.a(4, date, date2);
            if ((this.a.c == a.a(new Date().getTime()) && this.a.b == a.a(date.getTime())) || (this.a.c == a.a(date2.getTime()) && this.a.b == a.a(date.getTime()))) {
                this.a.a(8);
            }
            yiVar = new yq(this.a);
            this.b.setText(this.c[4]);
        } else {
            return;
        }
        if (yiVar != null) {
            if (this.k != null) {
                yiVar.a(this.k.b());
            }
            this.k = yiVar;
            this.k.a(this, this.j, null, this, false);
        }
    }

    static /* synthetic */ void a(MyBalanceQuerySetting myBalanceQuerySetting, boolean z) {
        if (z) {
            if (myBalanceQuerySetting.a.r > 0) {
                myBalanceQuerySetting.a.r--;
            } else {
                myBalanceQuerySetting.a.r = 4;
            }
        } else if (myBalanceQuerySetting.a.r < 4) {
            myBalanceQuerySetting.a.r++;
        } else {
            myBalanceQuerySetting.a.r = 0;
        }
        myBalanceQuerySetting.a.t = myBalanceQuerySetting.a.u[myBalanceQuerySetting.a.r];
        myBalanceQuerySetting.a();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (-1 == i3 && intent != null) {
            this.k.a(i2, i3, intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.i = getIntent().getIntExtra("LaunchedByApplication", 0);
        MyApp.a((Context) this);
        setContentView((int) C0000R.layout.my_balance_query_setting);
        this.a = (QueryInfo) getIntent().getParcelableExtra("QUERYINFO");
        this.a.a = 4;
        this.b = (Button) findViewById(C0000R.id.btnMyBalance);
        this.c = getResources().getStringArray(C0000R.array.MyBalanceType);
        this.b.setOnClickListener(new xv(this));
        this.j = (LinearLayout) findViewById(C0000R.id.baselayout);
        this.d = (Button) findViewById(C0000R.id.btnOK);
        this.e = (Button) findViewById(C0000R.id.btnReset);
        this.f = (Button) findViewById(C0000R.id.btnCancel);
        this.g = (Button) findViewById(C0000R.id.btnTypeNext);
        this.h = (Button) findViewById(C0000R.id.btnTypePrev);
        a();
        this.d.setOnClickListener(this.l);
        this.e.setOnClickListener(this.l);
        this.f.setOnClickListener(this.l);
        this.g.setOnClickListener(this.l);
        this.h.setOnClickListener(this.l);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        setResult(0);
        finish();
        return true;
    }
}
