package com.wacai365;

import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.a.a;
import com.wacai.b;
import com.wacai.data.e;
import com.wacai.data.f;
import com.wacai.data.h;
import java.util.Date;

public class QueryStatBase extends WacaiActivity {
    static int a = 0;
    static int b = 1;
    protected Button A = null;
    protected Button B = null;
    protected int C = a;
    /* access modifiers changed from: private */
    public int F = -1;
    private View.OnClickListener G = new wx(this);
    protected QueryInfo c;
    protected QueryInfo d;
    protected LinearLayout e = null;
    protected TextView f = null;
    protected LinearLayout g = null;
    protected TextView h = null;
    protected LinearLayout i = null;
    protected TextView j = null;
    protected LinearLayout k = null;
    protected TextView l = null;
    protected LinearLayout m = null;
    protected TextView n = null;
    protected int[] o = null;
    protected int[] p = null;
    protected int[] q = null;
    protected int[] r = null;
    protected rk s = null;
    protected LinearLayout t = null;
    protected TextView u = null;
    protected LinearLayout v = null;
    protected TextView w = null;
    protected LinearLayout x = null;
    protected TextView y = null;
    protected Button z = null;

    public QueryStatBase() {
    }

    public QueryStatBase(int i2) {
        this.C = i2;
    }

    static /* synthetic */ void a(QueryStatBase queryStatBase) {
        if (queryStatBase.c.b > queryStatBase.c.c) {
            m.a(queryStatBase, (Animation) null, 0, (View) null, (int) C0000R.string.txtDurationError);
            return;
        }
        queryStatBase.getIntent().putExtra("QUERYINFO", queryStatBase.c);
        queryStatBase.setResult(-1, queryStatBase.getIntent());
        queryStatBase.finish();
    }

    public final void a() {
        if (this.e != null) {
            this.F = this.c.a;
            this.e.setOnClickListener(new xn(this));
        }
        if (this.f != null) {
            this.f.setText(m.c(this, this.c.a));
        }
        if (this.t != null) {
            this.t.setOnClickListener(new xm(this));
        }
        if (this.u != null) {
            Date date = new Date(a.b(this.c.b));
            this.u.setText(a == this.C ? m.c.format(date) : m.g.format(date));
        }
        if (this.v != null) {
            this.v.setOnClickListener(new xk(this));
        }
        if (this.w != null) {
            Date date2 = new Date(a.b(this.c.c));
            this.w.setText(a == this.C ? m.c.format(date2) : m.g.format(date2));
        }
        if (this.k != null) {
            this.k.setOnClickListener(new xj(this));
        }
        if (this.l != null) {
            this.l.setText(-1 == this.c.f ? getResources().getText(C0000R.string.txtFullString).toString() : f.b((long) this.c.f));
        }
        if (this.g != null) {
            this.g.setOnClickListener(new xi(this));
        }
        if (this.h != null) {
            this.h.setText(-1 == this.c.d ? getResources().getText(C0000R.string.txtFullString).toString() : e.a((long) this.c.d));
        }
        if (this.i != null) {
            this.i.setOnClickListener(new xh(this));
        }
        if (this.j != null) {
            this.j.setText(-1 == this.c.e ? getResources().getText(C0000R.string.txtFullString).toString() : h.e((long) this.c.e));
        }
        if (this.m != null) {
            this.m.setOnClickListener(new wy(this));
        }
        if (this.n != null) {
            this.n.setText(m.d(this, this.c.g));
        }
        if (this.x != null) {
            this.x.setOnClickListener(new wz(this));
        }
        if (this.y != null) {
            this.y.setText(this.c.h.length() <= 0 ? getResources().getText(C0000R.string.txtFullString).toString() : this.c.i);
        }
        if (this.z != null) {
            this.z.setOnClickListener(this.G);
        }
        if (this.A != null) {
            this.A.setOnClickListener(this.G);
        }
        if (this.B != null) {
            this.B.setOnClickListener(this.G);
        }
        if (this.g != null) {
            this.g.setVisibility(b.a ? 0 : 8);
        }
    }
}
