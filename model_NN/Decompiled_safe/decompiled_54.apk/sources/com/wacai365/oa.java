package com.wacai365;

import android.view.View;
import android.view.animation.Animation;
import android.widget.AdapterView;

final class oa implements AdapterView.OnItemClickListener {
    private /* synthetic */ StatView a;

    oa(StatView statView) {
        this.a = statView;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.a.b.s != 0) {
            m.a(this.a, (Animation) null, 0, (View) null, (int) C0000R.string.txtRemoteListPrompt);
        } else {
            StatView.b(this.a, i);
        }
    }
}
