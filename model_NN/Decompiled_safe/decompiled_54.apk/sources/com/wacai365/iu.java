package com.wacai365;

import android.app.Activity;
import android.view.View;
import android.view.animation.Animation;

final class iu implements View.OnClickListener {
    private /* synthetic */ InputTrade a;

    iu(InputTrade inputTrade) {
        this.a = inputTrade;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void
     arg types: [com.wacai365.InputTrade, java.lang.String, java.lang.String, int, int, int]
     candidates:
      com.wacai365.m.a(android.content.Context, int, int, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, int, int, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, boolean, android.content.DialogInterface$OnClickListener, boolean):int[]
      com.wacai365.m.a(android.app.Activity, java.lang.String, java.lang.String, int, int, boolean):void */
    public final void onClick(View view) {
        if (view.equals(this.a.f)) {
            InputTrade.h(this.a);
        } else if (view.equals(this.a.g)) {
            this.a.setResult(this.a.A, this.a.getIntent());
            if (this.a.A != -1 || this.a.x == 0) {
                this.a.finish();
            } else {
                abt.b(this.a);
            }
        } else if (view.equals(this.a.h)) {
            InputTrade inputTrade = this.a;
            m.a(inputTrade, (int) C0000R.string.txtDeleteConfirm, new iw(inputTrade));
        } else if (view.equals(this.a.i)) {
            InputTrade inputTrade2 = this.a;
            m.a(inputTrade2, (Animation) null, 0, (View) null, (int) C0000R.string.txtInputNameForShortcuts);
            m.a((Activity) inputTrade2, "", inputTrade2.getResources().getString(C0000R.string.txtEditName), 20, 18, true);
        }
    }
}
