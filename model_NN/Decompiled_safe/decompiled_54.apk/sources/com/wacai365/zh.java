package com.wacai365;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

final class zh implements AdapterView.OnItemClickListener {
    private /* synthetic */ MoreOptionsTab a;

    zh(MoreOptionsTab moreOptionsTab) {
        this.a = moreOptionsTab;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void
     arg types: [com.wacai365.MoreOptionsTab, java.lang.String, int, ?[OBJECT, ARRAY]]
     candidates:
      com.wacai365.m.a(android.view.animation.Animation, int, android.view.View, java.lang.String):android.view.animation.Animation
      com.wacai365.m.a(android.content.Context, java.lang.String, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, java.util.ArrayList, android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.content.Context, int[], android.content.DialogInterface$OnMultiChoiceClickListener, android.content.DialogInterface$OnClickListener):com.wacai365.rk
      com.wacai365.m.a(android.app.ProgressDialog, boolean, boolean, android.content.DialogInterface$OnClickListener):com.wacai365.tp
      com.wacai365.m.a(android.content.Context, long, android.content.DialogInterface$OnClickListener, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      com.wacai365.m.a(android.content.Context, int, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, long, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, boolean, boolean, android.content.DialogInterface$OnClickListener):int[]
      com.wacai365.m.a(android.content.Context, java.lang.String, boolean, android.content.DialogInterface$OnClickListener):void */
    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (view != null) {
            try {
                fr frVar = (fr) this.a.a.getItemAtPosition(i);
                if (frVar == null) {
                    return;
                }
                if (frVar.a == C0000R.string.txtSendToFriends) {
                    m.a((Context) this.a);
                } else if (frVar.a != C0000R.string.txtMySMS || cc.b(this.a)) {
                    Intent intent = new Intent(this.a, Class.forName(frVar.c));
                    intent.putExtra("LaunchedByApplication", 1);
                    this.a.startActivityForResult(intent, 0);
                } else if (3 < m.b()) {
                    cc.a(this.a);
                } else {
                    m.a((Context) this.a, this.a.getResources().getString(C0000R.string.unSupportedROM), false, (DialogInterface.OnClickListener) null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
