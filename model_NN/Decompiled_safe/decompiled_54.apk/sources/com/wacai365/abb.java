package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.view.View;

final class abb implements View.OnClickListener {
    private /* synthetic */ SettingLoanAccountMgr a;

    abb(SettingLoanAccountMgr settingLoanAccountMgr) {
        this.a = settingLoanAccountMgr;
    }

    public final void onClick(View view) {
        long abs;
        Cursor cursor = (Cursor) this.a.d.getItem(((Integer) view.getTag()).intValue());
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_debtmoney");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("_id");
        long j = cursor.getLong(columnIndexOrThrow);
        int i = cursor.getInt(columnIndexOrThrow2);
        String.valueOf(j);
        if (j == 0) {
            int unused = this.a.e = 3;
            abs = j;
        } else {
            int unused2 = this.a.e = 4;
            if (j > 0) {
                int unused3 = this.a.f = 0;
                abs = j;
            } else {
                int unused4 = this.a.f = 1;
                abs = Math.abs(j);
            }
        }
        String a2 = m.a(m.a(abs), 2);
        Intent a3 = InputTrade.a(this.a, a2, -1);
        a3.putExtra("Extra_Type", this.a.e);
        if (abs != 0) {
            a3.putExtra("Extra_Type2", this.a.f);
        }
        a3.putExtra("Extra_Money", a2);
        a3.putExtra("Extra_DebtID", i);
        a3.putExtra("LaunchedByApplication", 1);
        this.a.startActivityForResult(a3, 0);
    }
}
