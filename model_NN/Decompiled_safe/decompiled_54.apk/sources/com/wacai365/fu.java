package com.wacai365;

import android.database.Cursor;
import android.database.DataSetObserver;
import android.widget.ListView;
import android.widget.TextView;

public final class fu extends DataSetObserver {
    private Cursor a = null;
    private ListView b = null;
    private TextView c = null;
    private String d = null;

    fu(Cursor cursor, ListView listView, TextView textView, String str) {
        this.a = cursor;
        this.b = listView;
        this.c = textView;
        this.d = str;
        a();
    }

    private void a() {
        if (this.a.getCount() == 0) {
            this.b.setVisibility(8);
            this.c.setHint(this.d);
            this.c.setVisibility(0);
            return;
        }
        this.b.setVisibility(0);
        this.c.setVisibility(8);
    }

    public final void onChanged() {
        a();
    }
}
