package com.wacai365;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.wacai.a.a;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai365.widget.Histogram;
import com.wacai365.widget.Pie;
import java.util.ArrayList;
import java.util.Date;

public class StatView extends WacaiActivity implements tt {
    private int a;
    /* access modifiers changed from: private */
    public QueryInfo b;
    private QueryInfo c;
    /* access modifiers changed from: private */
    public Histogram d = null;
    /* access modifiers changed from: private */
    public Pie e = null;
    /* access modifiers changed from: private */
    public ListView f = null;
    private RadioGroup g;
    /* access modifiers changed from: private */
    public Button h = null;
    /* access modifiers changed from: private */
    public Button i = null;
    /* access modifiers changed from: private */
    public Button j = null;
    private CheckBox k = null;
    /* access modifiers changed from: private */
    public Button l = null;
    /* access modifiers changed from: private */
    public Button m = null;
    private TextView n = null;
    /* access modifiers changed from: private */
    public int o = 0;
    private fy p = null;
    /* access modifiers changed from: private */
    public ArrayList q = new ArrayList();
    private abc r = new abc(this);
    private hg s;
    private View.OnClickListener t = new ob(this);
    private AdapterView.OnItemClickListener u = new oa(this);

    /* access modifiers changed from: private */
    public void a() {
        if (this.o == 0 || this.o == 2) {
            setRequestedOrientation(this.o == 0 ? 1 : 0);
            setContentView(this.o == 0 ? C0000R.layout.histogram_list : C0000R.layout.histogram);
            this.d = (Histogram) findViewById(C0000R.id.histogramView);
            if (this.d != null) {
                this.d.a(this.q);
                if (this.d != null) {
                    this.d.a(this.s.b());
                }
            }
            this.f = (ListView) findViewById(C0000R.id.histogramList);
            if (this.f != null) {
                this.f.setAdapter((ListAdapter) this.s.a(this.b.d));
                this.f.setOnItemClickListener(this.u);
            }
        } else if (this.o == 1) {
            setContentView((int) C0000R.layout.pie_list);
            this.n = (TextView) findViewById(C0000R.id.send_to_sina_microblog);
            this.n.setOnClickListener(new ok(this));
            this.e = (Pie) findViewById(C0000R.id.pieView);
            if (this.e != null) {
                this.e.a(this.q);
            }
            this.f = (ListView) findViewById(C0000R.id.pieList);
            if (this.f != null) {
                this.f.setAdapter((ListAdapter) new yc(this, this.e, C0000R.layout.pie_list_item));
                this.f.setOnItemClickListener(this.u);
            }
        }
        this.g = (RadioGroup) findViewById(C0000R.id.rgQueryChoice);
        if (this.g != null) {
            this.g.check(this.b.s != 0 ? C0000R.id.btnRemoteQuery : C0000R.id.btnQuery);
            this.g.setOnCheckedChangeListener(new oi(this));
        }
        this.h = (Button) findViewById(C0000R.id.btnPrev);
        if (this.h != null) {
            this.h.setOnClickListener(this.t);
        }
        this.i = (Button) findViewById(C0000R.id.btnNext);
        if (this.i != null) {
            this.i.setOnClickListener(this.t);
        }
        this.j = (Button) findViewById(C0000R.id.btnCancel);
        if (this.j != null) {
            this.j.setOnClickListener(this.t);
        }
        this.l = (Button) findViewById(C0000R.id.btnMoreCondition);
        if (this.l != null) {
            this.l.setOnClickListener(this.t);
        }
        this.k = (CheckBox) findViewById(C0000R.id.cbHisPie);
        if (this.k != null) {
            if (this.s.b()) {
                this.k.setVisibility(8);
            } else {
                this.k.setVisibility(0);
                this.k.setChecked(this.o == 1);
                this.k.setOnCheckedChangeListener(new od(this));
            }
        }
        d();
        f();
    }

    static /* synthetic */ void a(StatView statView, boolean z) {
        if (statView.s != null) {
            if (statView.s.a() == 8) {
                QueryInfo queryInfo = statView.b;
                if (queryInfo != null) {
                    if (z) {
                        queryInfo.b += 10000;
                        queryInfo.c += 10000;
                    } else {
                        queryInfo.b -= 10000;
                        queryInfo.c -= 10000;
                    }
                }
            } else {
                QueryInfo queryInfo2 = statView.b;
                if (queryInfo2 != null) {
                    if (z) {
                        a c2 = a.c(queryInfo2.c);
                        c2.c();
                        c2.e = 1;
                        queryInfo2.b = c2.a();
                        c2.c();
                        c2.f = 0;
                        c2.g = 0;
                        c2.h = 0;
                        queryInfo2.c = a.a(c2.b() - 1000);
                    } else {
                        a c3 = a.c(queryInfo2.b);
                        c3.e = 1;
                        c3.f = 0;
                        c3.g = 0;
                        c3.h = 0;
                        queryInfo2.c = a.a(c3.b() - 1000);
                        c3.d();
                        queryInfo2.b = c3.a();
                    }
                }
            }
            statView.c();
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        runOnUiThread(new oc(this));
    }

    static /* synthetic */ void b(StatView statView, int i2) {
        Intent intent = new Intent(statView, MyBalance.class);
        intent.putExtra("QUERYINFO", statView.s.a(statView.b, i2));
        statView.startActivityForResult(intent, 29);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    /* access modifiers changed from: private */
    public void c() {
        if (this.b.s > 0) {
            ft ftVar = new ft(this);
            ftVar.e(false);
            ftVar.a((tt) this);
            ftVar.b(C0000R.string.txtQueryProgress, C0000R.string.txtTransformData);
            this.p = ftVar;
            c cVar = new c(ftVar);
            this.s.a(cVar, this.b);
            ftVar.a(cVar);
            ftVar.a(true, true);
        } else {
            this.s.c(this.b);
            runOnUiThread(this.r);
        }
        runOnUiThread(this.r);
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.o == 0) {
            this.m = (Button) findViewById(C0000R.id.btnFullScreen);
            if (this.m == null) {
                return;
            }
            if (this.d == null || !this.d.a()) {
                this.m.setVisibility(8);
                return;
            }
            this.m.setVisibility(0);
            this.m.setOnClickListener(this.t);
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.o == 2) {
            this.o = 0;
            b();
            return;
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void f() {
        TextView textView = (TextView) findViewById(C0000R.id.summaryTitle);
        if (textView != null) {
            textView.setText(this.s.b(this.b));
        }
        TextView textView2 = (TextView) findViewById(C0000R.id.tvCurDisplay);
        if (textView2 != null) {
            String format = m.b.format(new Date(a.b(this.b.b)));
            String format2 = m.b.format(new Date(a.b(this.b.c)));
            textView2.setText(getResources().getString(C0000R.string.txtMyBalanceTimeFormatString, format, format2));
        }
        if (this.e != null) {
            boolean a2 = this.e.a();
            if (this.o == 1 && this.n != null) {
                this.n.setEnabled(a2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        View findViewById = findViewById(C0000R.id.clip_root);
        findViewById(C0000R.id.sum_bar).setVisibility(8);
        Bitmap a2 = m.a(findViewById);
        findViewById(C0000R.id.sum_bar).setVisibility(0);
        m.a(a2, getApplicationContext());
        Intent intent = new Intent(this, WeiboPublish.class);
        intent.putExtra("publish-content-txt", getResources().getString(C0000R.string.weiboSharePie));
        startActivity(intent);
    }

    static /* synthetic */ void j(StatView statView) {
        Intent intent = new Intent(statView, statView.s.c());
        intent.putExtra("QUERYINFO", statView.b);
        statView.c.a(statView.s.a());
        intent.putExtra("SRCQUERYINFO", statView.c);
        statView.startActivityForResult(intent, 1);
    }

    public final void a(int i2) {
        this.s.e();
        runOnUiThread(this.r);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 29) {
            c();
        } else if (i3 == -1) {
            if (i2 == 1) {
                this.b = (QueryInfo) intent.getParcelableExtra("QUERYINFO");
                this.s.a(0).a(this.b.d);
                c();
            } else if (this.p != null) {
                this.p.a(i2, i3, intent);
            }
            if (i2 == 31 && i3 == -1) {
                g();
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.a = getIntent().getIntExtra("Provider_Type", -1);
        if (this.a <= 0) {
            finish();
            return;
        }
        ArrayList arrayList = this.q;
        int i2 = this.a;
        this.s = i2 == 1 ? new jk(this, arrayList) : i2 == 2 ? new abr(this, arrayList) : i2 == 3 ? new uv(this, arrayList) : i2 == 6 ? new ma(this, arrayList) : i2 == 7 ? new sa(this, arrayList) : i2 == 4 ? new fq(this, arrayList) : i2 == 5 ? new rr(this, arrayList) : null;
        setTitle(this.s.d());
        this.b = new QueryInfo();
        this.b.a(this.s.a());
        this.b.d = (int) b.m().j();
        this.c = new QueryInfo(this.b);
        a();
        c();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        e();
        return true;
    }
}
