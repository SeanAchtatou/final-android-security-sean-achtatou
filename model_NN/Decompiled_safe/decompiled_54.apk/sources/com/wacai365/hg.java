package com.wacai365;

import com.wacai.b.c;
import java.util.ArrayList;

public abstract class hg {
    protected StatView a;
    protected ArrayList b;
    protected double c;
    protected rj d;
    private double e;

    protected hg(StatView statView, ArrayList arrayList) {
        this.a = statView;
        this.b = arrayList;
    }

    public int a() {
        return 4;
    }

    public abstract QueryInfo a(QueryInfo queryInfo, int i);

    public rj a(int i) {
        if (this.d == null) {
            this.d = new rj(this.a, this.b, i);
        }
        return this.d;
    }

    /* access modifiers changed from: protected */
    public void a(QueryInfo queryInfo) {
        this.e = 0.0d;
        this.c = 0.0d;
        this.b.clear();
    }

    public abstract boolean a(c cVar, QueryInfo queryInfo);

    public abstract String b(QueryInfo queryInfo);

    public boolean b() {
        return false;
    }

    public abstract Class c();

    public abstract boolean c(QueryInfo queryInfo);

    public abstract int d();

    public abstract void e();
}
