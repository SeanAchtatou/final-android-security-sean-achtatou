package com.wacai365;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import com.iflytek.f;

public final class uw extends AsyncTask {
    private ProgressDialog a;
    private Activity b;
    private int c;
    private int d;

    uw(Activity activity, int i, int i2) {
        this.b = activity;
        this.c = i;
        this.d = i2;
    }

    public final void a() {
        if (this.a != null) {
            this.a.show();
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return Boolean.valueOf(f.a().b());
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        this.a.dismiss();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        f a2;
        this.a.dismiss();
        if (!((Boolean) obj).booleanValue() && (a2 = f.a()) != null) {
            a2.e();
        }
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        this.a = new ProgressDialog(this.b);
        this.a.setProgressStyle(0);
        if (this.c > 0) {
            this.a.setTitle(this.c);
        }
        if (this.d > 0) {
            this.a.setMessage(this.b.getResources().getString(this.d));
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onProgressUpdate(Object[] objArr) {
    }
}
