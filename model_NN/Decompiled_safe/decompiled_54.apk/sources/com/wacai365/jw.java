package com.wacai365;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wacai.a.a;
import com.wacai.b;
import com.wacai.data.e;
import com.wacai.data.f;
import com.wacai.data.h;
import com.wacai.data.w;
import java.util.Date;

public class jw extends yi {
    protected rk a = null;
    /* access modifiers changed from: private */
    public TextView b;
    /* access modifiers changed from: private */
    public TextView c;
    /* access modifiers changed from: private */
    public TextView d;
    /* access modifiers changed from: private */
    public TextView e;
    /* access modifiers changed from: private */
    public TextView f;
    /* access modifiers changed from: private */
    public TextView g;
    /* access modifiers changed from: private */
    public TextView h;
    /* access modifiers changed from: private */
    public TextView i;
    /* access modifiers changed from: private */
    public TextView j;
    /* access modifiers changed from: private */
    public TextView k;
    private LinearLayout l;
    /* access modifiers changed from: private */
    public int[] m = null;
    /* access modifiers changed from: private */
    public int[] r = null;
    /* access modifiers changed from: private */
    public int[] s = null;
    /* access modifiers changed from: private */
    public int[] t = null;
    /* access modifiers changed from: private */
    public int u = -1;
    /* access modifiers changed from: private */
    public QueryInfo v;

    public jw(QueryInfo queryInfo) {
        this.v = queryInfo;
    }

    public final void a(Context context, LinearLayout linearLayout, LinearLayout linearLayout2, Activity activity, boolean z) {
        super.a(context, linearLayout, linearLayout2, activity, true);
        LinearLayout linearLayout3 = (LinearLayout) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) C0000R.layout.query_income_setting_list, (ViewGroup) null).findViewById(C0000R.id.mainlayout);
        linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.removeAllViews();
        linearLayout.addView(linearLayout3);
        this.b = (TextView) linearLayout.findViewById(C0000R.id.viewBeninMoney);
        this.c = (TextView) linearLayout.findViewById(C0000R.id.viewEndMoney);
        this.d = (TextView) linearLayout.findViewById(C0000R.id.viewShortcutsDate);
        this.e = (TextView) linearLayout.findViewById(C0000R.id.viewBeginDate);
        this.f = (TextView) linearLayout.findViewById(C0000R.id.viewEndDate);
        this.g = (TextView) linearLayout.findViewById(C0000R.id.viewType);
        this.h = (TextView) linearLayout.findViewById(C0000R.id.viewMember);
        this.i = (TextView) linearLayout.findViewById(C0000R.id.viewMoneyType);
        this.j = (TextView) linearLayout.findViewById(C0000R.id.viewAccount);
        this.k = (TextView) linearLayout.findViewById(C0000R.id.viewProject);
        ((LinearLayout) this.p.findViewById(C0000R.id.beginMoneyItem)).setOnClickListener(new nx(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.endMoneyItem)).setOnClickListener(new nw(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.dateShortcutsItem)).setOnClickListener(new nv(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.typeItem)).setOnClickListener(new nt(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.memberItem)).setOnClickListener(new nr(this));
        this.l = (LinearLayout) this.p.findViewById(C0000R.id.moneyTypeItem);
        this.l.setOnClickListener(new nq(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.accountItem)).setOnClickListener(new np(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.projectItem)).setOnClickListener(new no(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.dateBeginItem)).setOnClickListener(new nn(this));
        ((LinearLayout) this.p.findViewById(C0000R.id.dateEndItem)).setOnClickListener(new yl(this));
        a_();
    }

    public final void a(Object obj) {
        if (QueryInfo.class.isInstance(obj)) {
            this.v = (QueryInfo) obj;
        } else if (this.v == null) {
            this.v = new QueryInfo();
        }
        this.v.l = -1;
        this.v.m = -1;
    }

    public final void a_() {
        this.u = this.v.a;
        if (-1 == this.v.j) {
            this.b.setText("");
        } else {
            this.b.setText(m.a(m.a(this.v.j), 2));
        }
        if (-1 == this.v.k) {
            this.c.setText("");
        } else {
            this.c.setText(m.a(m.a(this.v.k), 2));
        }
        this.d.setText(this.n.getResources().getStringArray(C0000R.array.Times)[this.v.a]);
        this.e.setText(m.c.format(new Date(a.b(this.v.b))));
        this.f.setText(m.c.format(new Date(a.b(this.v.c))));
        if (-1 == this.v.l) {
            this.g.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.g.setText(w.b(this.v.l));
        }
        if (this.v.i == null || this.v.i.length() <= 0) {
            this.h.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.h.setText(this.v.i);
        }
        if (-1 == this.v.d) {
            this.i.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.i.setText(e.a((long) this.v.d));
        }
        if (-1 == this.v.e) {
            this.j.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.j.setText(h.e((long) this.v.e));
        }
        if (-1 == this.v.f) {
            this.k.setText(this.n.getResources().getText(C0000R.string.txtFullString).toString());
        } else {
            this.k.setText(f.b((long) this.v.f));
        }
        if (b.a) {
            this.l.setVisibility(0);
        } else {
            this.l.setVisibility(8);
        }
    }

    public final Object b() {
        return this.v;
    }

    public final boolean c() {
        this.v.t = 2;
        return true;
    }
}
