package com.wacai365;

import android.content.DialogInterface;
import android.view.View;
import android.view.animation.Animation;
import com.wacai.a;

final class jz implements DialogInterface.OnClickListener {
    private /* synthetic */ ej a;

    jz(ej ejVar) {
        this.a = ejVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.j = this.a.a.h[i];
        this.a.a.d.setText(m.f(this.a.a.a, this.a.a.j));
        this.a.a.l = 0;
        this.a.a.e.setText("");
        dialogInterface.dismiss();
        if (9 == this.a.a.j) {
            m.a(this.a.a.a, (Animation) null, 0, (View) null, (int) C0000R.string.txtStatChooseAccount);
        } else {
            a.b("HomeScreenTitleType1", (long) this.a.a.j);
            this.a.a.e();
        }
        this.a.a.g();
    }
}
