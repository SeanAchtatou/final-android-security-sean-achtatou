package com.wacai365;

import android.content.res.Resources;
import android.database.Cursor;
import com.wacai.b;
import com.wacai.b.c;
import com.wacai.b.f;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.o;
import com.wacai.data.aa;
import com.wacai.e;
import java.util.ArrayList;
import java.util.Hashtable;

public final class uv extends hg {
    private long[] e = null;

    public uv(StatView statView, ArrayList arrayList) {
        super(statView, arrayList);
    }

    private int a(int i, int i2, double d) {
        int size = this.b.size();
        String format = String.format("%04d/%02d", Integer.valueOf(i2 / 100), Integer.valueOf(i2 % 100));
        int i3 = i;
        while (true) {
            if (i3 >= size) {
                break;
            }
            Hashtable hashtable = (Hashtable) this.b.get(i3);
            if (hashtable != null) {
                int compareTo = ((String) hashtable.get("TAG_LABLE")).compareTo(format);
                if (compareTo != 0) {
                    if (compareTo > 0) {
                        break;
                    }
                } else {
                    hashtable.put("TAG_FIRST", m.a(d, 2));
                    break;
                }
            }
            i3++;
        }
        return i3;
    }

    public final int a() {
        return 8;
    }

    public final QueryInfo a(QueryInfo queryInfo, int i) {
        QueryInfo queryInfo2 = new QueryInfo(queryInfo);
        queryInfo2.t = 1;
        queryInfo2.b = m.c(this.e[i]);
        queryInfo2.c = m.d(this.e[i]);
        return queryInfo2;
    }

    /* access modifiers changed from: protected */
    public final void a(QueryInfo queryInfo) {
        int i;
        if (this.b != null) {
            this.c = 0.0d;
            this.b.clear();
            long j = queryInfo.b / 100;
            long j2 = queryInfo.c / 100;
            long max = Math.max(j, j2);
            long min = Math.min(j, j2);
            this.e = new long[((int) ((((max % 100) + ((max / 100) * 12)) - ((min % 100) + ((min / 100) * 12))) + 1))];
            int i2 = (int) (j / 100);
            long j3 = j;
            int i3 = (int) (j % 100);
            int i4 = 0;
            while (j3 <= j2) {
                Hashtable hashtable = new Hashtable();
                hashtable.put("TAG_LABLE", String.format("%04d/%02d", Integer.valueOf(i2), Integer.valueOf(i3)));
                hashtable.put("TAG_FIRST", "0.0");
                this.b.add(hashtable);
                this.e[i4] = (long) ((i2 * 100) + i3);
                if (i3 < 12) {
                    i3++;
                    i = i2;
                } else {
                    i = i2 + 1;
                    i3 = 1;
                }
                i4++;
                i2 = i;
                j3 = (long) ((i * 100) + i3);
            }
        }
    }

    public final boolean a(c cVar, QueryInfo queryInfo) {
        if (cVar == null || queryInfo == null) {
            return false;
        }
        a(queryInfo);
        l lVar = new l();
        StringBuffer stringBuffer = new StringBuffer(100);
        queryInfo.a(stringBuffer, 1);
        lVar.a(m.b(stringBuffer.toString()));
        cVar.a((o) lVar);
        f fVar = new f();
        fVar.a(true);
        fVar.a(1);
        fVar.a(this.b);
        cVar.a((o) fVar);
        return true;
    }

    public final String b(QueryInfo queryInfo) {
        Resources resources = this.a.getResources();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(resources.getText(C0000R.string.txtStringCountOutgo).toString());
        if (b.a) {
            stringBuffer.append(aa.a("TBL_MONEYTYPE", "flag", queryInfo.d));
        }
        stringBuffer.append(m.a(this.c, 2));
        return stringBuffer.toString();
    }

    public final Class c() {
        return QueryStatByMonth.class;
    }

    public final boolean c(QueryInfo queryInfo) {
        if (queryInfo == null) {
            return false;
        }
        a(queryInfo);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select a.ymd/100 as ym, sum(b.sharemoney) as sm from tbl_outgoinfo a, tbl_outgomemberinfo b, tbl_accountinfo d ");
        stringBuffer.append("where a.isdelete = 0 and a.ymd >= ");
        stringBuffer.append(queryInfo.b);
        stringBuffer.append(" and a.ymd <= ");
        stringBuffer.append(queryInfo.c);
        stringBuffer.append(" and b.outgoid = a.id and a.accountid = d.id ");
        if (-1 != queryInfo.e) {
            stringBuffer.append(" and a.accountid = " + queryInfo.e);
        } else if (-1 != queryInfo.d) {
            stringBuffer.append(" and d.moneytype = " + queryInfo.d);
        }
        if (-1 != queryInfo.f) {
            stringBuffer.append(" and a.projectid = " + queryInfo.f);
        }
        if (-1 != queryInfo.g) {
            stringBuffer.append(" and a.reimburse = " + queryInfo.g);
        }
        if (queryInfo.h.length() > 0) {
            stringBuffer.append(" and b.memberid in (" + queryInfo.h + ")");
        }
        stringBuffer.append(" group by a.ymd/100 order by a.ymd/100 ASC");
        Cursor rawQuery = e.c().b().rawQuery(stringBuffer.toString(), null);
        this.c = 0.0d;
        int count = rawQuery.getCount();
        if (rawQuery != null && count > 0) {
            rawQuery.moveToFirst();
            int i = 0;
            for (int i2 = 0; i2 < count; i2++) {
                int i3 = rawQuery.getInt(rawQuery.getColumnIndexOrThrow("ym"));
                double a = m.a(rawQuery.getLong(rawQuery.getColumnIndexOrThrow("sm")));
                i = a(i, i3, a);
                this.c += a;
                rawQuery.moveToNext();
            }
            rawQuery.close();
        }
        return true;
    }

    public final int d() {
        return C0000R.string.txtStatByMonth;
    }

    public final void e() {
        this.c = 0.0d;
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            this.c += Double.parseDouble((String) ((Hashtable) this.b.get(i)).get("TAG_FIRST"));
        }
    }
}
