package com.wacai365.widget;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.wacai.a.a;
import com.wacai365.C0000R;
import java.util.GregorianCalendar;

public final class b extends e {
    public static final int[] a = {C0000R.string.txtMondaySimple, C0000R.string.txtTuesdaySimple, C0000R.string.txtWednesdaySimple, C0000R.string.txtThursdaySimple, C0000R.string.txtFridaySimple, C0000R.string.txtSaturdaySimple, C0000R.string.txtSundaySimple};
    private int c;
    private int d;
    private Context e;
    private boolean f;
    private int g = a.b(this.c, 1, 1);

    public b(Context context, int i, int i2, int i3, int i4) {
        super(context, i, i2);
        this.e = context;
        this.c = i3;
        this.d = i4;
        this.f = new GregorianCalendar().isLeapYear(i3);
    }

    public final int a() {
        return this.f ? 366 : 365;
    }

    /* access modifiers changed from: protected */
    public final CharSequence a(int i) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(View view, int i) {
        if (view != null) {
            int[] iArr = this.f ? a.b : a.a;
            int[] iArr2 = new int[3];
            int i2 = 0;
            int i3 = i;
            while (true) {
                if (i2 >= iArr.length) {
                    break;
                } else if (i3 < iArr[i2]) {
                    iArr2[0] = i2 + 1;
                    iArr2[1] = i3 + 1;
                    break;
                } else {
                    i3 -= iArr[i2];
                    i2++;
                }
            }
            iArr2[2] = (this.g + i) % 7;
            StringBuilder sb = new StringBuilder();
            sb.append(iArr2[0]);
            sb.append('-');
            sb.append(iArr2[1]);
            ((TextView) view.findViewById(C0000R.id.listitem1)).setText(sb);
            ((TextView) view.findViewById(C0000R.id.listitem2)).setText(this.e.getResources().getString(a[iArr2[2]]));
        }
    }

    public final int b() {
        return this.d;
    }
}
