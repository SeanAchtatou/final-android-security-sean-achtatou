package com.wacai365.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.wacai365.C0000R;
import com.wacai365.m;
import com.wacai365.ts;
import java.util.ArrayList;
import java.util.Hashtable;

public class Pie extends View {
    public static final int[] a = {C0000R.color.pie_part_1, C0000R.color.pie_part_2, C0000R.color.pie_part_3, C0000R.color.pie_part_4, C0000R.color.pie_part_5, C0000R.color.pie_part_6, C0000R.color.pie_part_7, C0000R.color.pie_part_8, C0000R.color.pie_part_9, C0000R.color.pie_part_10, C0000R.color.pie_part_11, C0000R.color.pie_part_12};
    public ArrayList b;
    public double c;
    private Context d;
    private Paint e;
    private Paint f;
    private float g = 12.0f;
    private int h;
    private int i;
    private int j;
    private RectF k;
    private int l;
    private int m;

    public Pie(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = context;
        TypedArray obtainStyledAttributes = this.d.obtainStyledAttributes(attributeSet, ts.b);
        this.g = obtainStyledAttributes.getDimension(0, 12.0f);
        obtainStyledAttributes.recycle();
        this.e = new Paint();
        this.e.setAntiAlias(true);
        this.e.setDither(true);
        this.e.setStrokeWidth(0.0f);
        this.e.setStyle(Paint.Style.FILL);
        this.e.setStrokeJoin(Paint.Join.ROUND);
        this.e.setStrokeCap(Paint.Cap.ROUND);
        this.f = new Paint();
        this.f.setAntiAlias(true);
        this.f.setTextSize(this.g);
        this.f.setTextAlign(Paint.Align.CENTER);
        this.f.setStyle(Paint.Style.FILL);
        this.f.setColor(-1);
        Rect rect = new Rect();
        this.f.getTextBounds("55.55%", 0, 5, rect);
        this.l = rect.right - rect.left;
        this.m = rect.bottom - rect.top;
    }

    public final void a(ArrayList arrayList) {
        this.c = 0.0d;
        this.b = arrayList;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.c += Double.parseDouble((String) ((Hashtable) this.b.get(i2)).get("TAG_FIRST"));
            }
        }
    }

    public final boolean a() {
        return this.b != null && this.b.size() > 0 && this.c > 0.0d;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        double d2;
        PointF pointF;
        PointF pointF2;
        double d3;
        double d4;
        super.onDraw(canvas);
        int width = getWidth();
        int height = getHeight();
        int min = Math.min(width, height) - 20;
        int i2 = (width - min) >> 1;
        int i3 = (height - min) >> 1;
        this.k = new RectF((float) i2, (float) i3, (float) (i2 + min), (float) (i3 + min));
        this.j = min >> 1;
        this.h = i2 + this.j;
        this.i = this.j + i3;
        if (!a()) {
            Bitmap decodeResource = BitmapFactory.decodeResource(this.d.getResources(), C0000R.drawable.empty_chart);
            int width2 = (getWidth() - decodeResource.getWidth()) >> 1;
            int height2 = (getHeight() - decodeResource.getHeight()) >> 1;
            canvas.drawBitmap(decodeResource, (Rect) null, new RectF((float) width2, (float) height2, (float) (width2 + decodeResource.getWidth()), (float) (height2 + decodeResource.getHeight())), this.e);
            return;
        }
        int size = this.b.size();
        int i4 = 0;
        double d5 = 0.0d;
        while (i4 < size) {
            Hashtable hashtable = (Hashtable) this.b.get(i4);
            if (hashtable != null) {
                this.e.setColor(this.d.getResources().getColor(a[i4 % a.length]));
                double parseDouble = i4 != size - 1 ? this.c != 0.0d ? (Double.parseDouble((String) hashtable.get("TAG_FIRST")) / this.c) * 360.0d : 0.0d : 360.0d - d5;
                canvas.drawArc(this.k, (float) d5, (float) parseDouble, true, this.e);
                if (size == 1 || parseDouble == 360.0d) {
                    pointF = new PointF((float) this.h, (float) this.i);
                } else {
                    double d6 = d5 + parseDouble;
                    double d7 = d6 - d5;
                    int i5 = this.j >> 1;
                    if (d7 < 10.0d || this.j <= (Math.max(this.l, this.m) << 1)) {
                        pointF2 = null;
                    } else {
                        double d8 = (d6 + d5) * 0.5d;
                        int i6 = d7 < 30.0d ? (i5 >> 1) * 3 : i5;
                        if (d8 <= 90.0d) {
                            double d9 = (d8 * 3.1415926d) / 180.0d;
                            double cos = Math.cos(d9) * ((double) i6);
                            d3 = Math.sin(d9) * ((double) i6);
                            d4 = cos;
                        } else if (d8 <= 180.0d) {
                            double d10 = ((180.0d - d8) * 3.1415926d) / 180.0d;
                            double d11 = (-Math.cos(d10)) * ((double) i6);
                            d3 = Math.sin(d10) * ((double) i6);
                            d4 = d11;
                        } else if (d8 <= 270.0d) {
                            double d12 = ((270.0d - d8) * 3.1415926d) / 180.0d;
                            double d13 = (-Math.sin(d12)) * ((double) i6);
                            d3 = (-Math.cos(d12)) * ((double) i6);
                            d4 = d13;
                        } else {
                            double d14 = ((360.0d - d8) * 3.1415926d) / 180.0d;
                            double cos2 = Math.cos(d14) * ((double) i6);
                            d3 = (-Math.sin(d14)) * ((double) i6);
                            d4 = cos2;
                        }
                        pointF2 = new PointF(((float) d4) + ((float) this.h), ((float) d3) + ((float) this.i));
                    }
                    pointF = pointF2;
                }
                if (pointF != null) {
                    String str = this.c != 0.0d ? m.a((100.0d * Double.parseDouble((String) hashtable.get("TAG_FIRST"))) / this.c, 2) + "%" : "0.00%";
                    canvas.drawText(str, 0, str.length(), pointF.x, pointF.y, this.f);
                }
                d2 = d5 + parseDouble;
            } else {
                d2 = d5;
            }
            i4++;
            d5 = d2;
        }
    }
}
