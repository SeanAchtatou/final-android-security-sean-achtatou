package com.wacai365.widget;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.wacai365.C0000R;

public final class m extends e {
    private int a;
    private int c;
    private int d;

    public m(Context context, int i, int i2, int i3, int i4, int i5) {
        super(context, i, i2);
        this.a = i3;
        this.d = i4;
        this.c = i5;
    }

    public final int a() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public final CharSequence a(int i) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(View view, int i) {
        if (view != null) {
            int i2 = (this.d + i) % 7;
            StringBuilder sb = new StringBuilder();
            sb.append(i + 1);
            sb.append(this.b.getResources().getString(C0000R.string.txtDay));
            ((TextView) view.findViewById(C0000R.id.listitem1)).setText(sb);
            ((TextView) view.findViewById(C0000R.id.listitem2)).setText(this.b.getResources().getText(b.a[i2]).toString());
        }
    }

    public final int b() {
        return this.c;
    }

    public final void b(int i) {
        this.a = i;
        this.c = Math.min(this.c, this.a - 1);
    }

    public final void c(int i) {
        this.c = Math.min(i, this.a - 1);
    }

    public final void d(int i) {
        this.d = i;
    }
}
