package com.wacai365.widget;

public final class a {
    private int a;
    private int b;

    public a(int i, int i2) {
        this.a = i;
        this.b = i2;
    }

    public final int a() {
        return this.a;
    }

    public final boolean a(int i) {
        return i >= this.a && i <= b();
    }

    public final int b() {
        return (this.a + this.b) - 1;
    }

    public final int c() {
        return this.b;
    }
}
