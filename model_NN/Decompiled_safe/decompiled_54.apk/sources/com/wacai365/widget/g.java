package com.wacai365.widget;

import android.os.Handler;
import android.os.Message;

final class g extends Handler {
    private /* synthetic */ c a;

    g(c cVar) {
        this.a = cVar;
    }

    public final void handleMessage(Message message) {
        this.a.d.computeScrollOffset();
        int currY = this.a.d.getCurrY();
        int a2 = this.a.e - currY;
        int unused = this.a.e = currY;
        if (a2 != 0) {
            this.a.a.a(a2);
        }
        if (Math.abs(currY - this.a.d.getFinalY()) <= 0) {
            this.a.d.getFinalY();
            this.a.d.forceFinished(true);
        }
        if (!this.a.d.isFinished()) {
            this.a.k.sendEmptyMessage(message.what);
        } else if (message.what == 0) {
            this.a.d();
        } else {
            this.a.b();
        }
    }
}
