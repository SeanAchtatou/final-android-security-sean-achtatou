package com.wacai365.widget;

import android.view.View;
import android.widget.LinearLayout;
import java.util.LinkedList;
import java.util.List;

public final class j {
    private List a;
    private List b;
    private WheelView c;

    public j(WheelView wheelView) {
        this.c = wheelView;
    }

    private static View a(List list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        View view = (View) list.get(0);
        list.remove(0);
        return view;
    }

    private static List a(View view, List list) {
        List linkedList = list == null ? new LinkedList() : list;
        linkedList.add(view);
        return linkedList;
    }

    public final int a(LinearLayout linearLayout, int i, a aVar) {
        int i2 = 0;
        int i3 = i;
        int i4 = i;
        while (i2 < linearLayout.getChildCount()) {
            if (!aVar.a(i3)) {
                View childAt = linearLayout.getChildAt(i2);
                int a2 = this.c.b().a();
                if ((i3 < 0 || i3 >= a2) && !this.c.e()) {
                    this.b = a(childAt, this.b);
                } else {
                    this.a = a(childAt, this.a);
                }
                linearLayout.removeViewAt(i2);
                if (i2 == 0) {
                    i4++;
                }
            } else {
                i2++;
            }
            i3++;
        }
        return i4;
    }

    public final View a() {
        return a(this.a);
    }

    public final View b() {
        return a(this.b);
    }

    public final void c() {
        if (this.a != null) {
            this.a.clear();
        }
        if (this.b != null) {
            this.b.clear();
        }
    }
}
