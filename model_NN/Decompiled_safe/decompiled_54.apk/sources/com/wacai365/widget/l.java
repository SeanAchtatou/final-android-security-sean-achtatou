package com.wacai365.widget;

import android.content.Context;
import android.widget.TextView;

public final class l extends e {
    private int a;
    private int c;
    private int d;
    private String e;
    private String f;

    public l(Context context, int i, int i2, int i3, String str, String str2) {
        super(context);
        this.a = i;
        this.c = i2;
        this.e = str;
        this.f = str2;
        this.d = i3;
    }

    public final int a() {
        return (this.c - this.a) + 1;
    }

    public final CharSequence a(int i) {
        if (i < 0 || i >= a()) {
            return null;
        }
        int i2 = this.a + i;
        if (this.e == null) {
            return Integer.toString(i2) + this.f;
        }
        return String.format(this.e, Integer.valueOf(i2)) + this.f;
    }

    /* access modifiers changed from: protected */
    public final void a(TextView textView) {
        if (textView != null) {
            textView.setTextAppearance(this.b, 2131361801);
            textView.setGravity(17);
        }
    }

    public final int b() {
        return this.d - this.a;
    }
}
