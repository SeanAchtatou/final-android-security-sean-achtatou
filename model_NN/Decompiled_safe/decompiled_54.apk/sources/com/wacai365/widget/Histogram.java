package com.wacai365.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.wacai365.C0000R;
import com.wacai365.ts;
import java.util.ArrayList;
import java.util.Hashtable;

public class Histogram extends View implements GestureDetector.OnGestureListener, View.OnTouchListener {
    private ArrayList a;
    private boolean b = false;
    private Context c;
    private Paint d;
    private Paint e;
    private float f = 12.0f;
    private int g = -16777216;
    private int h;
    private int i;
    private int j = 0;
    private int k = 0;
    private int l = 0;
    private int m = 0;
    private int n = 5;
    private float o = 15.0f;
    private float p = 10.0f;
    private double q = 0.0d;
    private int r;
    private double s = 100.0d;
    private int t = 0;
    private GestureDetector u;
    private double v = 25.0d;
    private double w;
    private boolean x;
    private double y;

    public Histogram(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = context;
        a(attributeSet);
    }

    private static int a(String str, Paint paint) {
        if (str == null || str.length() <= 0 || paint == null) {
            return 0;
        }
        Rect rect = new Rect();
        paint.getTextBounds(str, 0, str.length(), rect);
        return rect.right - rect.left;
    }

    private void a(Canvas canvas) {
        double height = (double) getHeight();
        double width = (double) getWidth();
        int min = (int) Math.min((height / 8.0d) / 10.0d, 5.0d);
        int i2 = min <= 0 ? 1 : min;
        double d2 = height / ((double) (i2 * 8));
        this.d.setColor(285212672);
        int i3 = 1;
        while (true) {
            int i4 = i3;
            double d3 = ((double) i4) * d2;
            if (d3 >= width) {
                break;
            }
            canvas.drawLine((float) d3, 0.0f, (float) d3, (float) height, this.d);
            i3 = i4 + 1;
        }
        this.e.setTextAlign(Paint.Align.RIGHT);
        this.e.setTypeface(Typeface.createFromAsset(this.c.getAssets(), "font/wacaittf.ttf"));
        int i5 = 1;
        while (true) {
            int i6 = i5;
            double d4 = ((double) i6) * d2;
            if (i6 % i2 == 0) {
                this.d.setColor(855638016);
                boolean z = ((long) (this.s * 100.0d)) - (((long) this.s) * 100) > 0;
                double d5 = this.s * ((double) ((7 - (i6 / i2)) + 1));
                canvas.drawText((this.s > 10000.0d || !z) ? String.format("%d", Long.valueOf((long) d5)) : String.format("%.2f", Double.valueOf(d5)), (float) (this.n + this.m), (float) d4, this.e);
            } else {
                this.d.setColor(285212672);
            }
            if (d4 < height) {
                canvas.drawLine(0.0f, (float) d4, (float) width, (float) d4, this.d);
                i5 = i6 + 1;
            } else {
                this.e.setTextAlign(Paint.Align.CENTER);
                this.e.setTypeface(null);
                return;
            }
        }
    }

    private void a(Canvas canvas, float f2, float f3, float f4, float f5) {
        if (f5 - f3 > 0.0f) {
            if (f5 - f3 < 1.0f) {
                this.d.setAntiAlias(false);
                this.d.setDither(false);
                this.d.setStrokeWidth(0.0f);
                this.d.setStyle(Paint.Style.STROKE);
                canvas.drawLine(f2, f5 - 1.0f, f4, f5 - 1.0f, this.d);
                this.d.setAntiAlias(true);
                this.d.setDither(true);
                this.d.setStrokeWidth(0.0f);
                this.d.setStyle(Paint.Style.FILL);
                return;
            }
            canvas.drawRect(f2, f3, f4, f5, this.d);
        }
    }

    private synchronized void a(AttributeSet attributeSet) {
        this.t = 0;
        Resources resources = this.c.getResources();
        this.g = resources.getColor(C0000R.color.histogram_text);
        this.h = resources.getColor(C0000R.color.histogram_up);
        this.i = resources.getColor(C0000R.color.histogram_down);
        this.o = resources.getDimension(C0000R.dimen.Histogram_width);
        this.p = resources.getDimension(C0000R.dimen.Histogram_interval);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = this.c.obtainStyledAttributes(attributeSet, ts.a);
            this.b = obtainStyledAttributes.getBoolean(1, false);
            this.f = obtainStyledAttributes.getDimension(0, this.f);
            this.h = obtainStyledAttributes.getColor(4, this.h);
            this.i = obtainStyledAttributes.getColor(5, this.i);
            this.o = obtainStyledAttributes.getDimension(2, this.o);
            this.p = obtainStyledAttributes.getDimension(3, this.p);
            obtainStyledAttributes.recycle();
        }
        this.d = new Paint();
        this.d.setAntiAlias(true);
        this.d.setDither(true);
        this.d.setStrokeWidth(0.0f);
        this.d.setStyle(Paint.Style.FILL);
        this.d.setStrokeJoin(Paint.Join.ROUND);
        this.d.setStrokeCap(Paint.Cap.ROUND);
        this.e = new Paint();
        this.e.setAntiAlias(true);
        this.e.setTextSize(this.f);
        this.e.setTextAlign(Paint.Align.CENTER);
        this.e.setStyle(Paint.Style.FILL);
        this.e.setColor(this.g);
        Rect rect = new Rect();
        this.e.getTextBounds("国9G", 0, 3, rect);
        this.l = (rect.bottom - rect.top) + 4;
        this.u = new GestureDetector(this.c, this);
        this.v = (double) (this.o + this.p);
        setFocusable(true);
        setOnTouchListener(this);
        setLongClickable(true);
    }

    private boolean a(int i2) {
        if (this.t <= 0) {
            return false;
        }
        this.t = Math.max(0, this.t - i2);
        invalidate();
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x01a5  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x01cf A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(android.graphics.Canvas r25) {
        /*
            r24 = this;
            r0 = r24
            java.util.ArrayList r0 = r0.a
            r5 = r0
            if (r5 == 0) goto L_0x0012
            r0 = r24
            java.util.ArrayList r0 = r0.a
            r5 = r0
            int r5 = r5.size()
            if (r5 > 0) goto L_0x0013
        L_0x0012:
            return
        L_0x0013:
            r0 = r24
            int r0 = r0.n
            r5 = r0
            r0 = r24
            int r0 = r0.m
            r6 = r0
            int r5 = r5 + r6
            float r5 = (float) r5
            r0 = r24
            int r0 = r0.r
            r6 = r0
            r0 = r24
            java.util.ArrayList r0 = r0.a
            r7 = r0
            int r7 = r7.size()
            int r6 = java.lang.Math.min(r6, r7)
            r0 = r24
            int r0 = r0.j
            r7 = r0
            r0 = r24
            int r0 = r0.m
            r8 = r0
            int r7 = r7 - r8
            float r7 = (float) r7
            float r8 = (float) r6
            r0 = r24
            float r0 = r0.o
            r9 = r0
            float r8 = r8 * r9
            float r7 = r7 - r8
            r8 = 1
            int r6 = r6 - r8
            float r6 = (float) r6
            r0 = r24
            float r0 = r0.p
            r8 = r0
            float r6 = r6 * r8
            float r6 = r7 - r6
            r7 = 1073741824(0x40000000, float:2.0)
            float r6 = r6 / r7
            float r5 = r5 + r6
            r0 = r24
            boolean r0 = r0.b
            r6 = r0
            if (r6 == 0) goto L_0x01d7
            r0 = r24
            float r0 = r0.o
            r6 = r0
            r7 = 1073741824(0x40000000, float:2.0)
            float r6 = r6 / r7
            r15 = r6
        L_0x0064:
            r0 = r24
            int r0 = r0.k
            r6 = r0
            r0 = r24
            int r0 = r0.l
            r7 = r0
            int r6 = r6 - r7
            r0 = r6
            double r0 = (double) r0
            r16 = r0
            r0 = r24
            double r0 = r0.s
            r6 = r0
            r8 = 4619567317775286272(0x401c000000000000, double:7.0)
            double r18 = r6 * r8
            r6 = 0
            r20 = r6
            r6 = r5
        L_0x0080:
            r0 = r24
            int r0 = r0.r
            r5 = r0
            r0 = r20
            r1 = r5
            if (r0 >= r1) goto L_0x0012
            r0 = r24
            int r0 = r0.t
            r5 = r0
            int r11 = r5 + r20
            r0 = r24
            java.util.ArrayList r0 = r0.a
            r5 = r0
            int r5 = r5.size()
            if (r11 >= r5) goto L_0x0012
            r0 = r24
            java.util.ArrayList r0 = r0.a
            r5 = r0
            java.lang.Object r5 = r5.get(r11)
            java.util.Hashtable r5 = (java.util.Hashtable) r5
            java.lang.String r7 = "TAG_LABLE"
            java.lang.Object r5 = r5.get(r7)
            r0 = r5
            java.lang.String r0 = (java.lang.String) r0
            r14 = r0
            if (r20 != 0) goto L_0x01e1
            if (r14 == 0) goto L_0x01e1
            r0 = r24
            android.graphics.Paint r0 = r0.e
            r5 = r0
            int r5 = a(r14, r5)
            float r7 = (float) r5
            r0 = r24
            float r0 = r0.o
            r8 = r0
            int r7 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
            if (r7 <= 0) goto L_0x01e1
            float r5 = (float) r5
            r0 = r24
            float r0 = r0.o
            r7 = r0
            float r5 = r5 - r7
            r7 = 1073741824(0x40000000, float:2.0)
            float r5 = r5 / r7
            float r5 = r5 + r6
            r21 = r5
        L_0x00d5:
            r0 = r24
            float r0 = r0.o
            r5 = r0
            r0 = r24
            float r0 = r0.p
            r6 = r0
            float r5 = r5 + r6
            r0 = r20
            float r0 = (float) r0
            r6 = r0
            float r5 = r5 * r6
            float r7 = r21 + r5
            r0 = r24
            java.util.ArrayList r0 = r0.a
            r5 = r0
            java.lang.Object r5 = r5.get(r11)
            java.util.Hashtable r5 = (java.util.Hashtable) r5
            java.lang.String r6 = "TAG_FIRST"
            java.lang.Object r5 = r5.get(r6)
            java.lang.String r5 = (java.lang.String) r5
            double r5 = java.lang.Double.parseDouble(r5)
            r12 = 0
            double r5 = r5 * r16
            double r22 = r5 / r18
            r0 = r24
            android.graphics.Paint r0 = r0.d
            r5 = r0
            r0 = r24
            android.content.Context r0 = r0.c
            r6 = r0
            android.content.res.Resources r6 = r6.getResources()
            r8 = 2131165196(0x7f07000c, float:1.7944602E38)
            int r6 = r6.getColor(r8)
            r5.setColor(r6)
            double r5 = r16 - r22
            r0 = r24
            int r0 = r0.l
            r8 = r0
            double r8 = (double) r8
            double r5 = r5 + r8
            float r8 = (float) r5
            float r9 = r7 + r15
            r0 = r16
            float r0 = (float) r0
            r5 = r0
            r0 = r24
            int r0 = r0.l
            r6 = r0
            float r6 = (float) r6
            float r10 = r5 + r6
            r5 = r24
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            r0 = r24
            boolean r0 = r0.b
            r5 = r0
            if (r5 == 0) goto L_0x01df
            r0 = r24
            java.util.ArrayList r0 = r0.a
            r5 = r0
            java.lang.Object r5 = r5.get(r11)
            java.util.Hashtable r5 = (java.util.Hashtable) r5
            java.lang.String r6 = "TAG_SECOND"
            java.lang.Object r5 = r5.get(r6)
            java.lang.String r5 = (java.lang.String) r5
            if (r5 == 0) goto L_0x01df
            double r5 = java.lang.Double.parseDouble(r5)
            r0 = r24
            double r0 = r0.q
            r8 = r0
            r10 = 0
            int r8 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r8 == 0) goto L_0x016a
            double r5 = r5 * r16
            double r5 = r5 / r18
        L_0x016a:
            r0 = r24
            android.graphics.Paint r0 = r0.d
            r8 = r0
            r0 = r24
            android.content.Context r0 = r0.c
            r9 = r0
            android.content.res.Resources r9 = r9.getResources()
            r10 = 2131165197(0x7f07000d, float:1.7944604E38)
            int r9 = r9.getColor(r10)
            r8.setColor(r9)
            float r10 = r7 + r15
            double r8 = r16 - r5
            r0 = r24
            int r0 = r0.l
            r11 = r0
            double r11 = (double) r11
            double r8 = r8 + r11
            float r11 = (float) r8
            float r12 = r10 + r15
            r0 = r16
            float r0 = (float) r0
            r8 = r0
            r0 = r24
            int r0 = r0.l
            r9 = r0
            float r9 = (float) r9
            float r13 = r8 + r9
            r8 = r24
            r9 = r25
            r8.a(r9, r10, r11, r12, r13)
        L_0x01a3:
            if (r14 == 0) goto L_0x01cf
            r0 = r22
            r2 = r5
            double r5 = java.lang.Math.max(r0, r2)
            float r5 = (float) r5
            r0 = r24
            float r0 = r0.o
            r6 = r0
            r8 = 1056964608(0x3f000000, float:0.5)
            float r6 = r6 * r8
            float r6 = r6 + r7
            r0 = r24
            int r0 = r0.k
            r7 = r0
            float r7 = (float) r7
            float r5 = r7 - r5
            r7 = 1073741824(0x40000000, float:2.0)
            float r5 = r5 - r7
            r0 = r24
            android.graphics.Paint r0 = r0.e
            r7 = r0
            r0 = r25
            r1 = r14
            r2 = r6
            r3 = r5
            r4 = r7
            r0.drawText(r1, r2, r3, r4)
        L_0x01cf:
            int r5 = r20 + 1
            r20 = r5
            r6 = r21
            goto L_0x0080
        L_0x01d7:
            r0 = r24
            float r0 = r0.o
            r6 = r0
            r15 = r6
            goto L_0x0064
        L_0x01df:
            r5 = r12
            goto L_0x01a3
        L_0x01e1:
            r21 = r6
            goto L_0x00d5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.widget.Histogram.b(android.graphics.Canvas):void");
    }

    private boolean b(int i2) {
        if (this.a == null || this.t + this.r >= this.a.size()) {
            return false;
        }
        this.t = Math.min(this.a.size() - this.r, this.t + i2);
        invalidate();
        return true;
    }

    public final void a(ArrayList arrayList) {
        this.a = arrayList;
        int size = this.a.size();
        double d2 = 0.0d;
        for (int i2 = 0; i2 < size; i2++) {
            d2 = Math.max(d2, Double.parseDouble((String) ((Hashtable) this.a.get(i2)).get("TAG_FIRST")));
            String str = (String) ((Hashtable) this.a.get(i2)).get("TAG_SECOND");
            if (str != null) {
                d2 = Math.max(d2, Double.parseDouble(str));
            }
        }
        this.q = d2;
    }

    public final void a(boolean z) {
        this.b = z;
    }

    public final boolean a() {
        return this.a != null && this.a.size() > 0 && this.q > 0.0d;
    }

    public boolean onDown(MotionEvent motionEvent) {
        this.w = (double) motionEvent.getX();
        this.y = 0.0d;
        return false;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        double d2;
        double d3;
        super.onDraw(canvas);
        if (this.q != 0.0d && this.a != null && this.a.size() > 0) {
            double d4 = this.q;
            if (d4 < 1.0d) {
                d2 = ((1.0d - d4) / 2.0d) + d4;
                d3 = 0.0d - ((1.0d - d4) / 2.0d);
            } else {
                d2 = d4;
                d3 = 0.0d;
            }
            double d5 = d2 - d3;
            double pow = Math.pow(10.0d, (double) (((int) Math.log10(d5)) - 2));
            double[] dArr = {0.0d, 1.0d, 2.0d, 2.5d, 5.0d};
            int i2 = 0;
            loop0:
            while (true) {
                if (i2 >= 1000) {
                    break;
                }
                for (double d6 : dArr) {
                    double d7 = (((double) i2) + d6) * pow;
                    if (d5 <= 6.0d * d7) {
                        this.s = d7;
                        break loop0;
                    }
                }
                i2 += 10;
            }
        }
        this.j = getWidth() - (this.n << 1);
        this.k = getHeight();
        this.l = getHeight() / 8;
        boolean z = ((long) (this.s * 100.0d)) - (((long) this.s) * 100) > 0;
        double d8 = this.s * 7.0d;
        String format = (this.s > 10000.0d || !z) ? String.format("%d", Long.valueOf((long) d8)) : String.format("%.2f", Double.valueOf(d8));
        this.e.setTypeface(Typeface.createFromAsset(this.c.getAssets(), "font/wacaittf.ttf"));
        this.m = a(format, this.e);
        this.e.setTypeface(null);
        this.r = (int) (((double) (this.j - this.m)) / this.v);
        if (((double) (this.j - this.m)) - (((double) this.r) * this.v) > ((double) this.o)) {
            this.r++;
        }
        if (!a()) {
            Bitmap decodeResource = BitmapFactory.decodeResource(this.c.getResources(), C0000R.drawable.empty_chart);
            int width = (getWidth() - decodeResource.getWidth()) >> 1;
            int height = (getHeight() - decodeResource.getHeight()) >> 1;
            canvas.drawBitmap(decodeResource, (Rect) null, new RectF((float) width, (float) height, (float) (width + decodeResource.getWidth()), (float) (height + decodeResource.getHeight())), this.d);
            return;
        }
        a(canvas);
        b(canvas);
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        return i2 == 21 ? a(1) : i2 == 22 ? b(1) : super.onKeyDown(i2, keyEvent);
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        if (motionEvent2.getAction() != 2) {
            this.y = 0.0d;
            return false;
        }
        boolean z = ((double) motionEvent2.getX()) - this.w > 0.0d;
        if (this.x == z) {
            this.y += (double) f2;
        } else {
            this.x = z;
            this.y = (double) f2;
        }
        this.w = (double) motionEvent2.getX();
        int abs = (int) (Math.abs(this.y) / this.v);
        if (abs <= 0) {
            return false;
        }
        if (this.x) {
            if (!a(abs)) {
                this.y = 0.0d;
            }
        } else if (!b(abs)) {
            this.y = 0.0d;
        }
        this.y %= this.v;
        return true;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        return this.u.onTouchEvent(motionEvent);
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        return action == 4 ? a(1) : action == 8 ? b(1) : super.onTrackballEvent(motionEvent);
    }
}
