package com.wacai365.widget;

final class d implements i {
    private /* synthetic */ WheelView a;

    d(WheelView wheelView) {
        this.a = wheelView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.widget.WheelView.a(com.wacai365.widget.WheelView, boolean):boolean
     arg types: [com.wacai365.widget.WheelView, int]
     candidates:
      com.wacai365.widget.WheelView.a(int, int):int
      com.wacai365.widget.WheelView.a(int, boolean):void
      com.wacai365.widget.WheelView.a(com.wacai365.widget.WheelView, int):void
      com.wacai365.widget.WheelView.a(com.wacai365.widget.WheelView, boolean):boolean */
    public final void a() {
        boolean unused = this.a.k = true;
        this.a.c();
    }

    public final void a(int i) {
        WheelView.a(this.a, i);
        int height = this.a.getHeight();
        if (this.a.l > height) {
            int unused = this.a.l = height;
            this.a.j.a();
        } else if (this.a.l < (-height)) {
            int unused2 = this.a.l = -height;
            this.a.j.a();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.widget.WheelView.a(com.wacai365.widget.WheelView, boolean):boolean
     arg types: [com.wacai365.widget.WheelView, int]
     candidates:
      com.wacai365.widget.WheelView.a(int, int):int
      com.wacai365.widget.WheelView.a(int, boolean):void
      com.wacai365.widget.WheelView.a(com.wacai365.widget.WheelView, int):void
      com.wacai365.widget.WheelView.a(com.wacai365.widget.WheelView, boolean):boolean */
    public final void b() {
        if (this.a.k) {
            boolean unused = this.a.k = false;
            this.a.d();
        }
        int unused2 = this.a.l = 0;
        this.a.invalidate();
    }

    public final void c() {
        if (Math.abs(this.a.l) > 1) {
            this.a.j.a(this.a.l, 0);
        }
    }
}
