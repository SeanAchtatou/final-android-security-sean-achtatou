package com.wacai365.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.wacai365.ts;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class WheelView extends View {
    private static final int[] a = {-15658735, 11184810, 11184810};
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private Drawable g;
    private GradientDrawable h;
    private GradientDrawable i;
    /* access modifiers changed from: private */
    public c j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public int l;
    private boolean m;
    private LinearLayout n;
    private k o;
    private j p;
    private List q;
    private List r;
    private i s;

    public WheelView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public WheelView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.c = 0;
        this.f = 0;
        this.m = false;
        this.p = new j(this);
        this.q = new LinkedList();
        this.r = new LinkedList();
        this.s = new d(this);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, ts.d);
        this.d = obtainStyledAttributes.getInteger(0, 5);
        this.g = obtainStyledAttributes.getDrawable(1);
        this.e = obtainStyledAttributes.getInteger(2, 10);
        this.m = obtainStyledAttributes.getBoolean(3, false);
        obtainStyledAttributes.recycle();
        this.j = new c(getContext(), this.s);
    }

    private int a(int i2, int i3) {
        int i4;
        boolean z;
        if (this.h == null) {
            this.h = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, a);
        }
        if (this.i == null) {
            this.i = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, a);
        }
        this.n.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        this.n.measure(View.MeasureSpec.makeMeasureSpec(i2, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
        int measuredWidth = this.n.getMeasuredWidth();
        if (i3 == 1073741824) {
            z = true;
            i4 = i2;
        } else {
            int max = Math.max((this.e * 2) + measuredWidth, getSuggestedMinimumWidth());
            if (i3 != Integer.MIN_VALUE || i2 >= max) {
                i4 = max;
                z = false;
            } else {
                z = true;
                i4 = i2;
            }
        }
        if (z) {
            measuredWidth = i2 - (this.e * 2);
        }
        this.n.measure(View.MeasureSpec.makeMeasureSpec(measuredWidth, 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
        return i4;
    }

    private void a(int i2) {
        if (!this.k) {
            for (h a2 : this.q) {
                a2.a(this, i2);
            }
        }
    }

    private void a(int i2, boolean z) {
        int i3;
        int i4;
        if (this.o != null && this.o.a() != 0) {
            int a2 = this.o.a();
            if (i2 >= 0 && i2 < a2) {
                i3 = i2;
            } else if (this.m) {
                int i5 = i2;
                while (i5 < 0) {
                    i5 += a2;
                }
                i3 = i5 % a2;
            } else {
                return;
            }
            if (i3 == this.c) {
                return;
            }
            if (z) {
                int i6 = i3 - this.c;
                if (!this.m || (i4 = (a2 + Math.min(i3, this.c)) - Math.max(i3, this.c)) >= Math.abs(i6)) {
                    i4 = i6;
                } else if (i6 >= 0) {
                    i4 = -i4;
                }
                this.j.a((i4 * f()) - this.l, 0);
                return;
            }
            this.l = 0;
            this.c = i3;
            a(this.c);
            invalidate();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.widget.WheelView.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.widget.WheelView.a(int, int):int
      com.wacai365.widget.WheelView.a(com.wacai365.widget.WheelView, int):void
      com.wacai365.widget.WheelView.a(com.wacai365.widget.WheelView, boolean):boolean
      com.wacai365.widget.WheelView.a(int, boolean):void */
    static /* synthetic */ void a(WheelView wheelView, int i2) {
        int i3;
        int i4;
        int i5;
        wheelView.l += i2;
        int f2 = wheelView.f();
        int i6 = wheelView.l / f2;
        int i7 = wheelView.c - i6;
        int a2 = wheelView.o.a();
        int i8 = wheelView.l % f2;
        if (Math.abs(i8) <= f2 / 2) {
            i8 = 0;
        }
        if (wheelView.m && a2 > 0) {
            if (i8 > 0) {
                int i9 = i7 - 1;
                i3 = i6 + 1;
                i5 = i9;
            } else if (i8 < 0) {
                int i10 = i7 + 1;
                i3 = i6 - 1;
                i5 = i10;
            } else {
                int i11 = i7;
                i3 = i6;
                i5 = i11;
            }
            while (i5 < 0) {
                i5 += a2;
            }
            i4 = i5 % a2;
        } else if (i7 < 0) {
            i3 = wheelView.c;
            i4 = 0;
        } else if (i7 >= a2) {
            i3 = (wheelView.c - a2) + 1;
            i4 = a2 - 1;
        } else if (i7 > 0 && i8 > 0) {
            int i12 = i7 - 1;
            i3 = i6 + 1;
            i4 = i12;
        } else if (i7 >= a2 - 1 || i8 >= 0) {
            int i13 = i7;
            i3 = i6;
            i4 = i13;
        } else {
            int i14 = i7 + 1;
            i3 = i6 - 1;
            i4 = i14;
        }
        int i15 = wheelView.l;
        if (i4 != wheelView.c) {
            wheelView.a(i4, false);
        } else {
            wheelView.invalidate();
        }
        wheelView.l = i15 - (f2 * i3);
        if (wheelView.l > wheelView.getHeight()) {
            wheelView.l = (wheelView.l % wheelView.getHeight()) + wheelView.getHeight();
        }
    }

    private void b(int i2, int i3) {
        this.n.layout(0, 0, i2 - (this.e * 2), i3);
    }

    private boolean b(int i2) {
        return this.o != null && this.o.a() > 0 && (this.m || (i2 >= 0 && i2 < this.o.a()));
    }

    private boolean b(int i2, boolean z) {
        View view;
        int i3 = 0;
        if (f() <= 0) {
            return false;
        }
        if (this.o == null || this.o.a() == 0) {
            view = null;
        } else {
            int a2 = this.o.a();
            if (!b(i2)) {
                view = this.o.a(this.p.b());
            } else {
                int i4 = i2;
                while (i4 < 0) {
                    i4 += a2;
                }
                view = this.o.a(i4 % a2, this.p.a());
            }
        }
        if (view == null) {
            return false;
        }
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, f());
        LinearLayout linearLayout = this.n;
        if (!z) {
            i3 = -1;
        }
        linearLayout.addView(view, i3, layoutParams);
        return true;
    }

    private int f() {
        return this.f != 0 ? this.f : getHeight() / this.d;
    }

    private void g() {
        if (this.n == null) {
            this.n = new LinearLayout(getContext());
            this.n.setOrientation(1);
        }
    }

    public final void a() {
        if (this.j != null) {
            this.j.a();
        }
    }

    public final void a(h hVar) {
        this.q.add(hVar);
    }

    public final void a(k kVar) {
        this.o = kVar;
        a(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.widget.WheelView.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.widget.WheelView.a(int, int):int
      com.wacai365.widget.WheelView.a(com.wacai365.widget.WheelView, int):void
      com.wacai365.widget.WheelView.a(com.wacai365.widget.WheelView, boolean):boolean
      com.wacai365.widget.WheelView.a(int, boolean):void */
    public final void a(boolean z) {
        if (z) {
            this.p.c();
            if (this.n != null) {
                this.n.removeAllViews();
            }
            a(this.o.b(), false);
        } else if (this.n != null) {
            this.p.a(this.n, this.b, new a(0, 0));
        }
        this.l = 0;
        invalidate();
    }

    public final k b() {
        return this.o;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        Iterator it = this.r.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        Iterator it = this.r.iterator();
        while (it.hasNext()) {
            it.next();
        }
        a(this.c);
    }

    public final boolean e() {
        return this.m;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.widget.WheelView.b(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.wacai365.widget.WheelView.b(com.wacai365.widget.WheelView, int):int
      com.wacai365.widget.WheelView.b(int, int):void
      com.wacai365.widget.WheelView.b(int, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        a aVar;
        boolean z;
        super.onDraw(canvas);
        if (this.o != null && this.o.a() != 0) {
            if (f() == 0) {
                aVar = null;
            } else {
                int i2 = this.c;
                int i3 = 1;
                while (f() * i3 < getHeight()) {
                    i2--;
                    i3 += 2;
                }
                if (this.l != 0) {
                    if (this.l > 0) {
                        i2--;
                    }
                    i3++;
                    i2 -= this.l / f();
                }
                aVar = new a(i2, i3);
            }
            if (this.n != null) {
                int a2 = this.p.a(this.n, this.b, aVar);
                boolean z2 = this.b != a2;
                this.b = a2;
                z = z2;
            } else {
                g();
                z = true;
            }
            if (!z) {
                z = (this.b == aVar.a() && this.n.getChildCount() == aVar.c()) ? false : true;
            }
            if (this.b <= aVar.a() || this.b > aVar.b()) {
                this.b = aVar.a();
            } else {
                int i4 = this.b - 1;
                while (i4 >= aVar.a() && b(i4, true)) {
                    this.b = i4;
                    i4--;
                }
            }
            int i5 = this.b;
            for (int childCount = this.n.getChildCount(); childCount < aVar.c(); childCount++) {
                if (!b(this.b + childCount, false) && this.n.getChildCount() == 0) {
                    i5++;
                }
            }
            this.b = i5;
            if (z) {
                a(getWidth(), 1073741824);
                b(getWidth(), getHeight());
            }
            canvas.save();
            canvas.translate((float) this.e, (float) ((-(((this.c - this.b) * f()) + ((f() - getHeight()) / 2))) + this.l));
            this.n.draw(canvas);
            canvas.restore();
            if (this.g != null) {
                int height = getHeight() / 2;
                int f2 = (int) (((double) (f() / 2)) * 1.2d);
                this.g.setBounds(0, height - f2, getWidth(), height + f2);
                this.g.draw(canvas);
            }
            int f3 = (int) (1.5d * ((double) f()));
            this.h.setBounds(0, 0, getWidth(), f3);
            this.h.draw(canvas);
            this.i.setBounds(0, getHeight() - f3, getWidth(), getHeight());
            this.i.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        b(i4 - i2, i5 - i3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.widget.WheelView.b(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.wacai365.widget.WheelView.b(com.wacai365.widget.WheelView, int):int
      com.wacai365.widget.WheelView.b(int, int):void
      com.wacai365.widget.WheelView.b(int, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int min;
        int mode = View.MeasureSpec.getMode(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i2);
        int size2 = View.MeasureSpec.getSize(i3);
        if (this.n != null) {
            this.p.a(this.n, this.b, new a(0, 0));
        } else {
            g();
        }
        int i4 = this.d / 2;
        for (int i5 = this.c + i4; i5 >= this.c - i4; i5--) {
            if (b(i5, true)) {
                this.b = i5;
            }
        }
        int a2 = a(size, mode);
        if (mode2 == 1073741824) {
            min = size2;
        } else {
            this.f = f();
            int max = Math.max((this.f * this.d) - ((this.f * 10) / 50), getSuggestedMinimumHeight());
            min = mode2 == Integer.MIN_VALUE ? Math.min(max, size2) : max;
        }
        setMeasuredDimension(a2, min);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.widget.WheelView.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.widget.WheelView.a(int, int):int
      com.wacai365.widget.WheelView.a(com.wacai365.widget.WheelView, int):void
      com.wacai365.widget.WheelView.a(com.wacai365.widget.WheelView, boolean):boolean
      com.wacai365.widget.WheelView.a(int, boolean):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.o == null) {
            return true;
        }
        switch (motionEvent.getAction()) {
            case 1:
                if (!this.k) {
                    int y = ((int) motionEvent.getY()) - (getHeight() / 2);
                    int f2 = (y > 0 ? y + (f() / 2) : y - (f() / 2)) / f();
                    if (f2 != 0 && b(this.c + f2)) {
                        a(f2 + this.c, true);
                        return true;
                    }
                }
                break;
            case 2:
                if (getParent() != null) {
                    getParent().requestDisallowInterceptTouchEvent(true);
                    break;
                }
                break;
        }
        return this.j.a(motionEvent);
    }
}
