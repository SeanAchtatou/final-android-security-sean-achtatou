package com.wacai365.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public abstract class e implements k {
    private int a;
    protected Context b;
    private int c;
    private LayoutInflater d;
    private int e;
    private int f;
    private int g;

    protected e(Context context) {
        this(context, -1, -1);
    }

    protected e(Context context, int i, int i2) {
        this.a = -61424;
        this.c = 24;
        this.e = i;
        this.g = i2;
        this.b = context;
        this.d = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private View b(int i) {
        switch (i) {
            case -1:
                return new TextView(this.b);
            case 0:
                return null;
            default:
                return this.d.inflate(i, (ViewGroup) null, false);
        }
    }

    private static TextView b(View view, int i) {
        if (i == 0) {
            try {
                if (view instanceof TextView) {
                    return (TextView) view;
                }
            } catch (ClassCastException e2) {
                throw new IllegalStateException("AbstractWheelAdapter requires the resource ID to be a TextView", e2);
            }
        }
        if (i != 0) {
            return (TextView) view.findViewById(i);
        }
        return null;
    }

    public final View a(int i, View view) {
        TextView b2;
        if (i < 0 || i >= a()) {
            return null;
        }
        int i2 = this.e;
        int i3 = this.f;
        View b3 = view != null ? view : b(i2);
        if (i2 > 0) {
            a(b3, i);
        } else {
            CharSequence a2 = a(i);
            if (!(b3 == null || !(b3 instanceof TextView) || (b2 = b(b3, i3)) == null)) {
                if (a2 != null) {
                    b2.setText(a2);
                }
                if (i2 == -1) {
                    a(b2);
                }
            }
        }
        return b3;
    }

    public final View a(View view) {
        int i = this.g;
        View b2 = view != null ? view : b(i);
        if (i == -1 && (b2 instanceof TextView)) {
            a((TextView) b2);
        }
        if (i > 0) {
            a(b2, -1);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public abstract CharSequence a(int i);

    /* access modifiers changed from: protected */
    public void a(View view, int i) {
    }

    /* access modifiers changed from: protected */
    public void a(TextView textView) {
        textView.setTextColor(this.a);
        textView.setGravity(17);
        textView.setTextSize((float) this.c);
        textView.setLines(1);
        textView.setTypeface(Typeface.SANS_SERIF, 1);
    }

    public int b() {
        return 0;
    }
}
