package com.wacai365.widget;

import android.content.Context;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.Scroller;

public final class c {
    /* access modifiers changed from: private */
    public i a;
    private Context b;
    private GestureDetector c;
    /* access modifiers changed from: private */
    public Scroller d;
    /* access modifiers changed from: private */
    public int e;
    private float f;
    private boolean g;
    private GestureDetector.SimpleOnGestureListener h = new f(this);
    private final int i = 0;
    private final int j = 1;
    /* access modifiers changed from: private */
    public Handler k = new g(this);

    public c(Context context, i iVar) {
        this.c = new GestureDetector(context, this.h);
        this.c.setIsLongpressEnabled(false);
        this.d = new Scroller(context);
        this.a = iVar;
        this.b = context;
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        c();
        this.k.sendEmptyMessage(i2);
    }

    private void c() {
        this.k.removeMessages(0);
        this.k.removeMessages(1);
    }

    /* access modifiers changed from: private */
    public void d() {
        this.a.c();
        a(1);
    }

    private void e() {
        if (!this.g) {
            this.g = true;
            this.a.a();
        }
    }

    public final void a() {
        this.d.forceFinished(true);
    }

    public final void a(int i2, int i3) {
        this.d.forceFinished(true);
        this.e = 0;
        this.d.startScroll(0, 0, 0, i2, i3 != 0 ? i3 : 400);
        a(0);
        e();
    }

    public final boolean a(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.f = motionEvent.getY();
                this.d.forceFinished(true);
                c();
                break;
            case 2:
                int y = (int) (motionEvent.getY() - this.f);
                if (y != 0) {
                    e();
                    this.a.a(y);
                    this.f = motionEvent.getY();
                    break;
                }
                break;
        }
        if (!this.c.onTouchEvent(motionEvent) && motionEvent.getAction() == 1) {
            d();
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (this.g) {
            this.a.b();
            this.g = false;
        }
    }
}
