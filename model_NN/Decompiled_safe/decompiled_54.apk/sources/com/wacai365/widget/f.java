package com.wacai365.widget;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class f extends GestureDetector.SimpleOnGestureListener {
    private /* synthetic */ c a;

    f(c cVar) {
        this.a = cVar;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        int unused = this.a.e = 0;
        this.a.d.fling(0, this.a.e, 0, (int) (-f2), 0, 0, -2147483647, Integer.MAX_VALUE);
        this.a.a(0);
        return true;
    }

    public final boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return true;
    }
}
