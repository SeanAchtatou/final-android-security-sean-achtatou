package com.wacai365;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class InputText extends WacaiActivity implements View.OnClickListener {
    private EditText a = null;
    private int b;
    private yg c;

    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == -1 && intent != null && intent != null && i2 == -1 && 34 == i && this.a != null) {
            String stringExtra = intent.getStringExtra("Text_String");
            this.a.setText(stringExtra);
            this.a.setSelection(stringExtra.length());
        }
    }

    public void onClick(View view) {
        String obj;
        switch (view.getId()) {
            case C0000R.id.btnCancel /*2131492870*/:
                setResult(0, getIntent());
                finish();
                return;
            case C0000R.id.btnOK /*2131492903*/:
                if (this.c.a() && (obj = this.a.getEditableText().toString()) != null) {
                    getIntent().putExtra("Text_String", obj);
                    setResult(-1, getIntent());
                    finish();
                    return;
                }
                return;
            case C0000R.id.btnVoice /*2131492907*/:
                VoiceInput.a(this, this.b, this.a.getText().toString());
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.input_text);
        Intent intent = getIntent();
        this.a = (EditText) findViewById(C0000R.id.etText);
        this.b = intent.getIntExtra("Max_LengthAllowed", 100);
        this.c = new yg(this, (TextView) findViewById(C0000R.id.text_count_prompt), this.b);
        this.a.addTextChangedListener(this.c);
        String stringExtra = intent.getStringExtra("Text_String");
        if (stringExtra != null) {
            this.a.setText(stringExtra);
            this.a.setSelection(stringExtra.length());
        }
        this.a.setSingleLine(intent.getBooleanExtra("EXTRA_ISSINGLELINE", false));
        String stringExtra2 = getIntent().getStringExtra("TITLE_NAME");
        if (stringExtra2 != null) {
            setTitle(stringExtra2);
        }
        findViewById(C0000R.id.btnCancel).setOnClickListener(this);
        findViewById(C0000R.id.btnOK).setOnClickListener(this);
        findViewById(C0000R.id.btnVoice).setOnClickListener(this);
    }
}
