package com.wacai365;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.wacai.a;
import com.wacai.e;
import java.util.Date;

public class SettingLoanAccountMgr extends WacaiActivity {
    /* access modifiers changed from: private */
    public Button a;
    /* access modifiers changed from: private */
    public Button b;
    /* access modifiers changed from: private */
    public ListView c;
    /* access modifiers changed from: private */
    public CursorAdapter d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public Button g;
    private View.OnClickListener h = new qg(this);

    /* access modifiers changed from: private */
    public Cursor a() {
        long time = new Date().getTime() / 1000;
        String format = String.format("select (ifnull(t4.num,0) - ifnull(t3.num,0)) as _debtmoney, a.id as _id, a.name as _name, a.enable as _enable, b.shortname as _shortname,b.flag as _flag from TBL_ACCOUNTINFO a left join TBL_MONEYTYPE b on a.moneytype = b.id left join (select ifnull(sum(transferoutmoney), 0) as num, transferoutaccountid, date from TBL_TRANSFERINFO where isdelete=0 and type<>0 and date <= %d group by transferoutaccountid) t3 on t3.transferoutaccountid = a.id and t3.date >= 0 left join (select ifnull(sum(transferinmoney), 0) as num, transferinaccountid, date from TBL_TRANSFERINFO where isdelete=0 and type<>0 and date <= %d group by transferinaccountid) t4 on t4.transferinaccountid = a.id and t4.date >= 0 where a.type = 3 and a.enable = 1 ", Long.valueOf(time), Long.valueOf(time));
        Cursor rawQuery = e.c().b().rawQuery(a.a("BasicSortStyle", 0) == 0 ? format + " order by a.orderno ASC " : format + " order by a.pinyin ASC ", null);
        startManagingCursor(rawQuery);
        return rawQuery;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1) {
            runOnUiThread(new qi(this));
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        switch (menuItem.getItemId()) {
            case C0000R.id.idEdit /*2131493342*/:
                Cursor cursor = (Cursor) this.c.getItemAtPosition(adapterContextMenuInfo.position);
                long j = cursor != null ? cursor.getLong(cursor.getColumnIndexOrThrow("_id")) : -1;
                if (j >= 0) {
                    Intent intent = new Intent(this, InputLoanAccount.class);
                    intent.putExtra("Record_Id", j);
                    startActivityForResult(intent, 0);
                    break;
                }
                break;
        }
        return false;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.loan_item_list);
        this.a = (Button) findViewById(C0000R.id.btnAdd);
        this.a.setOnClickListener(this.h);
        this.b = (Button) findViewById(C0000R.id.btnCancel);
        this.b.setOnClickListener(this.h);
        this.c = (ListView) findViewById(C0000R.id.IOList);
        this.d = new sz(this, this, C0000R.layout.loan_list_item, a(), new String[]{"_name", "_shortname", "_debtmoney", "_flag", "_id"}, new int[]{C0000R.id.listitem1, C0000R.id.listitem2, C0000R.id.listitem3});
        this.c.setAdapter((ListAdapter) this.d);
        this.c.setOnItemClickListener(new qh(this));
    }
}
