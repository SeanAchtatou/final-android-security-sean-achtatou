package com.wacai365;

import android.os.Message;
import com.wacai.a.e;

final class sw implements Runnable {
    private /* synthetic */ boolean a;
    private /* synthetic */ WeiboHome b;

    sw(WeiboHome weiboHome, boolean z) {
        this.b = weiboHome;
        this.a = z;
    }

    public final void run() {
        try {
            if (this.a) {
                this.b.b.b(this.b.a.h);
            } else {
                this.b.b.c(this.b.a.h);
            }
            this.b.a.i = this.a;
            this.b.a.b();
            this.b.e.sendEmptyMessage(300);
        } catch (Exception e) {
            Message obtain = Message.obtain();
            obtain.what = 301;
            if (!e.a()) {
                obtain.obj = this.b.getResources().getString(C0000R.string.txtNoNetworkPrompt);
                this.b.e.sendMessage(obtain);
                return;
            }
            obtain.obj = e.getMessage();
        }
    }
}
