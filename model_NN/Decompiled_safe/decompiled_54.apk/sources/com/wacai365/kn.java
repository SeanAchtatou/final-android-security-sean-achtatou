package com.wacai365;

import android.widget.RadioGroup;

final class kn implements RadioGroup.OnCheckedChangeListener {
    private /* synthetic */ ChooseTarget a;

    kn(ChooseTarget chooseTarget) {
        this.a = chooseTarget;
    }

    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (i == C0000R.id.btnGeneral) {
            this.a.a();
        } else if (i == C0000R.id.btnAll) {
            ChooseTarget.d(this.a);
        }
    }
}
