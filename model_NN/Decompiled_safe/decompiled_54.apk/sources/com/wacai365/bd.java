package com.wacai365;

import android.view.ContextMenu;
import android.view.View;

final class bd implements View.OnCreateContextMenuListener {
    private /* synthetic */ WhiteListMgr a;

    bd(WhiteListMgr whiteListMgr) {
        this.a = whiteListMgr;
    }

    public final void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.clear();
        this.a.getMenuInflater().inflate(C0000R.menu.white_list_context, contextMenu);
    }
}
