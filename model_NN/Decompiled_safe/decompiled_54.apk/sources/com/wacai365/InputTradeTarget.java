package com.wacai365;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import com.wacai.b.c;
import com.wacai.b.k;
import com.wacai.b.l;
import com.wacai.b.m;
import com.wacai.b.n;
import com.wacai.b.o;
import com.wacai.b.r;
import com.wacai.b.t;
import com.wacai.data.j;

public class InputTradeTarget extends WacaiActivity implements tt {
    /* access modifiers changed from: private */
    public EditText a = null;
    private Animation b;
    /* access modifiers changed from: private */
    public boolean c = false;
    private fy d = null;
    /* access modifiers changed from: private */
    public j e;
    private View.OnClickListener f = new ty(this);
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener g = new ua(this);
    private DialogInterface.OnClickListener h = new ub(this);

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0057  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(android.widget.TextView r10) {
        /*
            r9 = this;
            r7 = 2131296395(0x7f09008b, float:1.8210705E38)
            r4 = -1
            r6 = 2130968579(0x7f040003, float:1.7545816E38)
            r5 = 0
            if (r10 != 0) goto L_0x000c
            r1 = r5
        L_0x000b:
            return r1
        L_0x000c:
            java.lang.Class<android.widget.EditText> r1 = android.widget.EditText.class
            boolean r1 = r1.isInstance(r10)     // Catch:{ all -> 0x0052 }
            if (r1 == 0) goto L_0x006c
            r0 = r10
            android.widget.EditText r0 = (android.widget.EditText) r0     // Catch:{ all -> 0x0052 }
            r1 = r0
        L_0x0018:
            java.lang.CharSequence r2 = r10.getText()     // Catch:{ all -> 0x0060 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0060 }
            if (r2 == 0) goto L_0x002c
            java.lang.String r2 = r2.trim()     // Catch:{ all -> 0x0060 }
            int r3 = r2.length()     // Catch:{ all -> 0x0060 }
            if (r3 > 0) goto L_0x003b
        L_0x002c:
            java.lang.String r2 = ""
            r10.setText(r2)     // Catch:{ all -> 0x0066 }
            android.view.animation.Animation r2 = r9.b
            android.view.animation.Animation r1 = com.wacai365.m.a(r9, r2, r6, r1, r7)
            r9.b = r1
            r1 = r5
            goto L_0x000b
        L_0x003b:
            r3 = 20
            int r4 = r2.length()     // Catch:{ all -> 0x0060 }
            if (r3 >= r4) goto L_0x0050
            android.view.animation.Animation r2 = r9.b
            r3 = 2131296396(0x7f09008c, float:1.8210707E38)
            android.view.animation.Animation r1 = com.wacai365.m.a(r9, r2, r6, r1, r3)
            r9.b = r1
            r1 = r5
            goto L_0x000b
        L_0x0050:
            r1 = r2
            goto L_0x000b
        L_0x0052:
            r1 = move-exception
            r2 = r5
            r3 = r4
        L_0x0055:
            if (r3 == r4) goto L_0x005f
            android.view.animation.Animation r4 = r9.b
            android.view.animation.Animation r2 = com.wacai365.m.a(r9, r4, r6, r2, r3)
            r9.b = r2
        L_0x005f:
            throw r1
        L_0x0060:
            r2 = move-exception
            r3 = r4
            r8 = r1
            r1 = r2
            r2 = r8
            goto L_0x0055
        L_0x0066:
            r2 = move-exception
            r3 = r7
            r8 = r1
            r1 = r2
            r2 = r8
            goto L_0x0055
        L_0x006c:
            r1 = r5
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wacai365.InputTradeTarget.a(android.widget.TextView):java.lang.String");
    }

    private void a(k kVar) {
        we weVar = new we(this, kVar.a ? this.h : null);
        weVar.c = kVar;
        runOnUiThread(weVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wacai365.ft.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wacai365.ft.a(com.wacai.b.c, boolean):void
      com.wacai365.ft.a(int, int):void
      com.wacai.b.e.a(int, int):void
      com.wacai365.ft.a(boolean, boolean):void */
    static /* synthetic */ void d(InputTradeTarget inputTradeTarget) {
        String a2 = inputTradeTarget.a(inputTradeTarget.a);
        if (a2 != null) {
            inputTradeTarget.e.b(a2);
            if (inputTradeTarget.e.B() == null || inputTradeTarget.e.B().length() <= 0) {
                j a3 = j.a(inputTradeTarget.e.j());
                if (a3 == null || a3.x() == inputTradeTarget.e.x()) {
                    a3 = inputTradeTarget.e;
                } else if (a3.l()) {
                    inputTradeTarget.b = m.a(inputTradeTarget, inputTradeTarget.b, (int) C0000R.anim.shake, inputTradeTarget.a, (int) C0000R.string.txtNameExist);
                    return;
                } else if (inputTradeTarget.e.a()) {
                    inputTradeTarget.b = m.a(inputTradeTarget, inputTradeTarget.b, (int) C0000R.anim.shake, inputTradeTarget.a, (int) C0000R.string.sameNameDeletedExist);
                    return;
                } else {
                    a3.d(true);
                    inputTradeTarget.e.f();
                }
                a3.f(false);
                a3.d(true);
                a3.c();
                inputTradeTarget.a(new k());
                return;
            }
            ft ftVar = new ft(inputTradeTarget);
            ftVar.a((tt) inputTradeTarget);
            inputTradeTarget.d = ftVar;
            c cVar = new c(ftVar);
            if (inputTradeTarget.e.B().length() > 0) {
                l lVar = new l();
                StringBuffer stringBuffer = new StringBuffer();
                inputTradeTarget.e.a(stringBuffer);
                lVar.a(m.a(stringBuffer.toString()));
                cVar.a((o) lVar);
                cVar.a((o) new n());
            } else {
                l lVar2 = new l();
                lVar2.a(m.a(1));
                cVar.a((o) lVar2);
                cVar.a((o) new r());
                t tVar = new t();
                tVar.a(inputTradeTarget.e);
                cVar.a((o) tVar);
                cVar.a((o) new l());
                cVar.a((o) new n());
            }
            ftVar.a(cVar);
            ftVar.a(inputTradeTarget.h);
            ftVar.b(C0000R.string.networkProgress, C0000R.string.txtTransformData);
            ftVar.a(false, true);
        }
    }

    static /* synthetic */ void e(InputTradeTarget inputTradeTarget) {
        inputTradeTarget.e.f();
        inputTradeTarget.a(new k());
    }

    static /* synthetic */ void g(InputTradeTarget inputTradeTarget) {
        inputTradeTarget.e = new j();
        inputTradeTarget.a.setText("");
        m.a(inputTradeTarget, (Animation) null, 0, (View) null, (int) C0000R.string.txtContinuePrompt);
    }

    public final void a(int i) {
        if (i != 10001) {
            this.e.c();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1 && this.d != null) {
            this.d.a(i, i2, intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.input_name);
        boolean booleanExtra = getIntent().getBooleanExtra("Is_Payer", false);
        long longExtra = getIntent().getLongExtra("Record_Id", -1);
        if (longExtra > 0) {
            setTitle(booleanExtra ? C0000R.string.editPayTarget : C0000R.string.editTradeTarget);
            this.e = j.a(longExtra);
        } else {
            setTitle(booleanExtra ? C0000R.string.addPayTarget : C0000R.string.addTradeTarget);
            this.e = new j();
        }
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(this.f);
        Button button = (Button) findViewById(C0000R.id.btnContinue);
        button.setOnClickListener(this.f);
        if (this.e.x() > 0) {
            button.setText((int) C0000R.string.txtMenuDelete);
        }
        ((Button) findViewById(C0000R.id.btnOK)).setOnClickListener(this.f);
        this.a = (EditText) findViewById(C0000R.id.etName);
        if (this.a != null) {
            this.a.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
            this.a.setText(this.e.j());
        }
    }
}
