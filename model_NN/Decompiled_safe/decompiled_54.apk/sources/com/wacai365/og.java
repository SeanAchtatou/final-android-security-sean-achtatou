package com.wacai365;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public final class og extends Dialog implements DialogInterface {
    private ListView a = null;

    public og(Context context, String[] strArr, DialogInterface.OnClickListener onClickListener) {
        super(context, C0000R.style.choose_dialog);
        View inflate = LayoutInflater.from(context).inflate((int) C0000R.layout.choose_item_list, (ViewGroup) null);
        if (inflate != null) {
            this.a = (ListView) inflate.findViewById(C0000R.id.itemList);
            if (this.a != null) {
                this.a.setAdapter((ListAdapter) new pv(this, context, C0000R.layout.list_item_1_small, strArr));
                this.a.setOnItemClickListener(new rt(this, onClickListener));
            }
            setContentView(inflate);
        }
    }
}
