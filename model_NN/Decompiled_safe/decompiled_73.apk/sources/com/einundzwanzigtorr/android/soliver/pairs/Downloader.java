package com.einundzwanzigtorr.android.soliver.pairs;

import android.content.Context;
import android.os.AsyncTask;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Downloader {
    public static final int CONNECTION_TIMEOUT_MILLIS = 5000;
    private static DownloadListener mListener;
    private static ArrayList<Download> mQueue;
    private static boolean mShouldAbort;

    public interface DownloadListener {
        void onDownloadError(int i, Exception exc, Download download);

        void onDownloadFinished(int i, ArrayList<Download> arrayList, Long l);

        void onDownloadProgress(int i, Integer... numArr);

        void onDownloadStarted(int i, int i2);
    }

    public static class Download {
        public static final int STORAGE_CACHE = 2;
        public static final int STORAGE_EXTERNAL = 1;
        public static final int STORAGE_INTERNAL = 0;
        private Context mContext;
        private String mDestination;
        private int mStorage;
        /* access modifiers changed from: private */
        public URL mUrl;

        public Download(Context context, String src, String destination, int storage) {
            try {
                this.mContext = context;
                this.mUrl = new URL(src);
                this.mDestination = destination;
                this.mStorage = storage;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        public File getDestinationFile() {
            switch (this.mStorage) {
                case 0:
                    return new File(this.mContext.getFilesDir(), this.mDestination);
                case 1:
                default:
                    return null;
            }
        }

        public File getOutputFile() {
            File f = getDestinationFile();
            if (f.getParentFile().exists()) {
                return f;
            }
            if (f.getParentFile().mkdirs()) {
                return f;
            }
            return null;
        }
    }

    private static class BackgroundTask extends AsyncTask<Object, Integer, Long> {
        protected ArrayList<Download> mDownloads;
        protected int mTag;

        private BackgroundTask() {
        }

        /* synthetic */ BackgroundTask(BackgroundTask backgroundTask) {
            this();
        }

        public void setTag(int tag) {
            this.mTag = tag;
        }

        /* access modifiers changed from: protected */
        public Long doInBackground(Object... downloads) {
            this.mDownloads = new ArrayList<>();
            int count = downloads.length;
            long totalSize = 0;
            for (int i = 0; i < count; i++) {
                Download d = (Download) downloads[i];
                long bytesDownloaded = Downloader.download(this.mTag, d);
                if (bytesDownloaded > 0) {
                    this.mDownloads.add(d);
                }
                totalSize += bytesDownloaded;
                publishProgress(Integer.valueOf(i + 1));
            }
            return Long.valueOf(totalSize);
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... progress) {
            Downloader.notifyProgressUpdate(this.mTag, progress);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Long result) {
            Downloader.onDownloadsFinished(this.mTag, this.mDownloads, result);
        }
    }

    /* JADX WARN: Type inference failed for: r11v4, types: [java.net.URLConnection] */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long download(int r13, com.einundzwanzigtorr.android.soliver.pairs.Downloader.Download r14) {
        /*
            boolean r11 = com.einundzwanzigtorr.android.soliver.pairs.Downloader.mShouldAbort
            if (r11 == 0) goto L_0x0007
            r11 = 0
        L_0x0006:
            return r11
        L_0x0007:
            r2 = 0
            r4 = 0
            java.net.URL r10 = r14.mUrl     // Catch:{ Exception -> 0x001c }
            java.io.File r5 = r14.getOutputFile()     // Catch:{ Exception -> 0x001c }
            if (r5 != 0) goto L_0x002b
            java.lang.Exception r11 = new java.lang.Exception     // Catch:{ Exception -> 0x001c }
            java.lang.String r12 = "Cannot create directories"
            r11.<init>(r12)     // Catch:{ Exception -> 0x001c }
            throw r11     // Catch:{ Exception -> 0x001c }
        L_0x001c:
            r11 = move-exception
            r6 = r11
            notifyError(r13, r6, r14)     // Catch:{ all -> 0x0063 }
            r6.printStackTrace()     // Catch:{ all -> 0x0063 }
            if (r4 == 0) goto L_0x0029
            r4.disconnect()
        L_0x0029:
            r11 = r2
            goto L_0x0006
        L_0x002b:
            java.net.URLConnection r11 = r10.openConnection()     // Catch:{ Exception -> 0x001c }
            r0 = r11
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x001c }
            r4 = r0
            r11 = 5000(0x1388, float:7.006E-42)
            r4.setConnectTimeout(r11)     // Catch:{ Exception -> 0x001c }
            java.io.InputStream r7 = r4.getInputStream()     // Catch:{ Exception -> 0x001c }
            java.io.FileOutputStream r9 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x001c }
            r9.<init>(r5)     // Catch:{ Exception -> 0x001c }
            r11 = 8192(0x2000, float:1.14794E-41)
            byte[] r1 = new byte[r11]     // Catch:{ Exception -> 0x001c }
        L_0x0045:
            int r8 = r7.read(r1)     // Catch:{ Exception -> 0x001c }
            if (r8 > 0) goto L_0x0057
            r9.close()     // Catch:{ Exception -> 0x001c }
            r7.close()     // Catch:{ Exception -> 0x001c }
            if (r4 == 0) goto L_0x0029
            r4.disconnect()
            goto L_0x0029
        L_0x0057:
            boolean r11 = com.einundzwanzigtorr.android.soliver.pairs.Downloader.mShouldAbort     // Catch:{ Exception -> 0x001c }
            if (r11 == 0) goto L_0x006a
            java.lang.Exception r11 = new java.lang.Exception     // Catch:{ Exception -> 0x001c }
            java.lang.String r12 = "Download aborted"
            r11.<init>(r12)     // Catch:{ Exception -> 0x001c }
            throw r11     // Catch:{ Exception -> 0x001c }
        L_0x0063:
            r11 = move-exception
            if (r4 == 0) goto L_0x0069
            r4.disconnect()
        L_0x0069:
            throw r11
        L_0x006a:
            r11 = 0
            r9.write(r1, r11, r8)     // Catch:{ Exception -> 0x001c }
            long r11 = (long) r8
            long r2 = r2 + r11
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: com.einundzwanzigtorr.android.soliver.pairs.Downloader.download(int, com.einundzwanzigtorr.android.soliver.pairs.Downloader$Download):long");
    }

    public static void setListener(DownloadListener listener) {
        mListener = listener;
    }

    public static void start(int tag) {
        mShouldAbort = false;
        BackgroundTask task = new BackgroundTask(null);
        task.setTag(tag);
        task.execute(mQueue.toArray());
        if (mListener != null) {
            mListener.onDownloadStarted(tag, mQueue.size());
        }
    }

    public static void abort() {
        mShouldAbort = true;
    }

    public static void add(Download download) {
        if (mQueue == null) {
            mQueue = new ArrayList<>();
        }
        mQueue.add(download);
    }

    public static void cleanup() {
        mQueue = null;
        mShouldAbort = false;
    }

    /* access modifiers changed from: private */
    public static void notifyProgressUpdate(int tag, Integer... progress) {
        if (mListener != null) {
            mListener.onDownloadProgress(tag, progress);
        }
    }

    private static void notifyError(int tag, Exception error, Download brokenDownload) {
        if (mListener != null) {
            mListener.onDownloadError(tag, error, brokenDownload);
        }
    }

    /* access modifiers changed from: private */
    public static void onDownloadsFinished(int tag, ArrayList<Download> downloads, Long result) {
        cleanup();
        if (mListener != null) {
            mListener.onDownloadFinished(tag, downloads, result);
        }
    }
}
