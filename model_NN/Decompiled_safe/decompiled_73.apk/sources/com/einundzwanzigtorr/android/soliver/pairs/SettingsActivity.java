package com.einundzwanzigtorr.android.soliver.pairs;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.view.View;

public class SettingsActivity extends BasePreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings);
        addPreferencesFromResource(R.xml.settings);
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AppTracker.page("/settings/");
    }

    public void back(View v) {
        super.onBackPressed();
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        setResult(-1);
        Preference p = findPreference(key);
        if (key.equals("enableGoogleAnalytics")) {
            AppTracker.event(AppTracker.CATEGORY_SETTINGS, "enableGoogleAnalytics", ((CheckBoxPreference) p).isChecked() ? "Yes" : "No", -1);
        }
    }
}
