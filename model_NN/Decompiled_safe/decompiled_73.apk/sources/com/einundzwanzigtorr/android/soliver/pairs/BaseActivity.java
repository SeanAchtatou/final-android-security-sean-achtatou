package com.einundzwanzigtorr.android.soliver.pairs;

import android.app.Activity;
import android.os.Bundle;

public class BaseActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppTracker.start(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        AppTracker.stop();
        super.onDestroy();
    }
}
