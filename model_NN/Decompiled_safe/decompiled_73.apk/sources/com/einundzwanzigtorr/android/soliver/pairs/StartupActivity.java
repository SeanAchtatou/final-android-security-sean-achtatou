package com.einundzwanzigtorr.android.soliver.pairs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.einundzwanzigtorr.android.soliver.pairs.Card;
import com.einundzwanzigtorr.android.soliver.pairs.CardSet;
import com.einundzwanzigtorr.android.soliver.pairs.Downloader;
import com.longevitysoft.android.xml.plist.PListXMLHandler;
import com.longevitysoft.android.xml.plist.PListXMLParser;
import com.longevitysoft.android.xml.plist.domain.Dict;
import com.longevitysoft.android.xml.plist.domain.PList;
import com.longevitysoft.android.xml.plist.domain.PListObject;
import com.longevitysoft.android.xml.plist.domain.String;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class StartupActivity extends BaseActivity implements View.OnClickListener, Animation.AnimationListener, Downloader.DownloadListener, Card.OnFlipCardListener {
    private static final int[][] CardPositionsInDPLandscape = {new int[]{-10, 60, -2, 1}, new int[]{320, 30, 1, 1}, new int[]{270, 120, 3, 1}, new int[]{460, -10, -4, 1}, new int[]{430, 100, -2, 1}, new int[]{20, 110, 1, 1}, new int[]{120, 70, -2, 1}, new int[]{390, 220, -2, 1}, new int[]{150, 5, 3, 1}, new int[]{-10, 250, 5, 1}, new int[]{310, 170, 2, 1}, new int[]{60, 199, 2, 1}, new int[]{400, 20, -2, 1}, new int[]{150, 190, -1, 1}};
    private static final int[][] CardPositionsInDPPortrait;
    private static final int DIALOG_CHECK = 1;
    private static final int DIALOG_DOWNLOAD = 2;
    private Button mButtonOnePlayer;
    private Button mButtonStartGame;
    private Button mButtonTwoPlayers;
    private ArrayList<Card> mCards;
    private View mDarkLayer;
    private TextView mLabelNameOfPlayerTwo;
    private EditText mNameOfPlayerOne;
    private EditText mNameOfPlayerTwo;
    private int mNumPlayers = 1;
    /* access modifiers changed from: private */
    public SharedPreferences mPrefs;
    private ProgressDialog mProgressDialog;
    private ViewGroup mRibbon;
    private boolean mRibbonIsUp;

    static {
        int[] iArr = new int[4];
        iArr[0] = -30;
        iArr[1] = 80;
        iArr[2] = -5;
        int[] iArr2 = new int[4];
        iArr2[0] = 220;
        iArr2[1] = 65;
        iArr2[2] = 4;
        int[] iArr3 = new int[4];
        iArr3[0] = -50;
        iArr3[1] = 240;
        iArr3[2] = -5;
        int[] iArr4 = new int[4];
        iArr4[0] = 260;
        iArr4[1] = 250;
        iArr4[2] = -1;
        int[] iArr5 = new int[4];
        iArr5[0] = -30;
        iArr5[1] = 410;
        iArr5[2] = 4;
        int[] iArr6 = new int[4];
        iArr6[0] = 140;
        iArr6[1] = 390;
        iArr6[2] = 4;
        int[] iArr7 = new int[4];
        iArr7[0] = 200;
        iArr7[1] = 450;
        iArr7[2] = -2;
        CardPositionsInDPPortrait = new int[][]{new int[]{40, 55, 3, 1}, iArr, new int[]{246, 120, 5, 1}, iArr2, new int[]{166, 80, -2, 1}, new int[]{-25, 280, 4, 1}, iArr3, new int[]{270, 320, 4, 1}, iArr4, new int[]{20, 345, -2, 1}, iArr5, new int[]{60, 480, 2, 1}, new int[]{180, 365, -3, 1}, iArr6, iArr7};
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        setContentView((int) R.layout.startup);
        this.mNameOfPlayerOne = (EditText) findViewById(R.id.EditText_NameOfPlayerOne);
        this.mLabelNameOfPlayerTwo = (TextView) findViewById(R.id.Label_NameOfPlayerTwo);
        this.mNameOfPlayerTwo = (EditText) findViewById(R.id.EditText_NameOfPlayerTwo);
        this.mButtonStartGame = (Button) findViewById(R.id.Button_StartGame);
        this.mButtonStartGame.setOnClickListener(this);
        this.mButtonOnePlayer = (Button) findViewById(R.id.Button_OnePlayer);
        this.mButtonOnePlayer.setOnClickListener(this);
        this.mButtonTwoPlayers = (Button) findViewById(R.id.Button_TwoPlayers);
        this.mButtonTwoPlayers.setOnClickListener(this);
        this.mDarkLayer = findViewById(R.id.DarkLayer);
        this.mRibbon = (ViewGroup) findViewById(R.id.Ribbon);
        this.mRibbonIsUp = true;
        if (!this.mPrefs.contains("gaNoticeShown")) {
            showGANotice();
        } else {
            startApp();
        }
    }

    /* access modifiers changed from: protected */
    public void startApp() {
        this.mDarkLayer.postDelayed(new Runnable() {
            public void run() {
                StartupActivity.this.flyInCards();
            }
        }, 200);
        this.mDarkLayer.postDelayed(new Runnable() {
            public void run() {
                StartupActivity.this.checkForUpdates();
            }
        }, 3000);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mPrefs.contains("nameOfPlayerOne")) {
            this.mNameOfPlayerOne.setText(this.mPrefs.getString("nameOfPlayerOne", "Player 1"));
        }
        if (this.mPrefs.contains("nameOfPlayerTwo")) {
            this.mNameOfPlayerTwo.setText(this.mPrefs.getString("nameOfPlayerTwo", "Player 2"));
        }
        AppTracker.page("/");
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        startActivityForResult(new Intent(this, SettingsActivity.class), SettingsActivity.class.hashCode());
        return true;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1 && requestCode == SettingsActivity.class.hashCode()) {
            boolean enabled = this.mPrefs.getBoolean("enableGoogleAnalytics", true);
            AppTracker.setTrackingEnabled(enabled);
            if (enabled) {
                AppTracker.start(this);
            } else {
                AppTracker.stop();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void showGANotice() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.GANotice).setTitle((int) R.string.GANoticeTitle).setCancelable(false).setPositiveButton((int) R.string.Close, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                StartupActivity.this.mPrefs.edit().putBoolean("gaNoticeShown", true).commit();
                dialog.dismiss();
                StartupActivity.this.startApp();
            }
        });
        builder.show();
    }

    /* access modifiers changed from: protected */
    public void flyInCards() {
        int w;
        int[][] cardPositionsInDP;
        FrameLayout chrome = (FrameLayout) findViewById(R.id.chrome);
        if (this.mCards != null) {
            Iterator<Card> it = this.mCards.iterator();
            while (it.hasNext()) {
                chrome.removeView(it.next());
            }
            this.mCards = null;
        }
        this.mCards = CardSet.getAvailableSets(this).get(0).getCards();
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float densityFactor = metrics.density;
        if (getResources().getConfiguration().orientation == 2) {
            w = chrome.getHeight() / 3;
            cardPositionsInDP = CardPositionsInDPLandscape;
        } else {
            w = chrome.getWidth() / 3;
            cardPositionsInDP = CardPositionsInDPPortrait;
        }
        int idx = 0;
        Iterator<Card> it2 = this.mCards.iterator();
        while (it2.hasNext()) {
            Card card = it2.next();
            if (idx != cardPositionsInDP.length) {
                int x = (int) (((float) cardPositionsInDP[idx][0]) * densityFactor);
                int r = cardPositionsInDP[idx][2];
                int front = cardPositionsInDP[idx][3];
                FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(w, w);
                lp.gravity = 51;
                lp.leftMargin = x;
                lp.topMargin = (int) (((float) cardPositionsInDP[idx][1]) * densityFactor);
                if (front == 0) {
                    card.showBack();
                } else {
                    card.showFront();
                }
                card.setRotation(r);
                card.setOnFlipCardListener(this);
                chrome.addView(card, 0, lp);
                startFlyAnimationOnCard(card);
                idx++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                this.mProgressDialog = new ProgressDialog(this);
                this.mProgressDialog.setProgressStyle(0);
                this.mProgressDialog.setMessage(getString(R.string.CheckingForUpdate));
                return this.mProgressDialog;
            case 2:
                this.mProgressDialog = new ProgressDialog(this);
                this.mProgressDialog.setProgressStyle(1);
                this.mProgressDialog.setMessage(getString(R.string.DownloadingCards));
                return this.mProgressDialog;
            default:
                return null;
        }
    }

    private void startGame() {
        App.getGameEngine().reset();
        String nameOfPlayerOne = this.mNameOfPlayerOne.getText().toString();
        if (TextUtils.isEmpty(nameOfPlayerOne)) {
            Toast.makeText(this, (int) R.string.PleaseEnterName, 0).show();
            this.mNameOfPlayerOne.requestFocus();
            return;
        }
        Player playerOne = new Player();
        playerOne.setName(nameOfPlayerOne);
        App.getGameEngine().addPlayer(playerOne);
        this.mPrefs.edit().putString("nameOfPlayerOne", nameOfPlayerOne).commit();
        if (this.mNumPlayers == 2) {
            String nameOfPlayerTwo = this.mNameOfPlayerTwo.getText().toString();
            if (TextUtils.isEmpty(nameOfPlayerTwo)) {
                Toast.makeText(this, (int) R.string.PleaseEnterBothNames, 0).show();
                this.mNameOfPlayerTwo.requestFocus();
                return;
            }
            Player playerTwo = new Player();
            playerTwo.setName(nameOfPlayerTwo);
            App.getGameEngine().addPlayer(playerTwo);
            this.mPrefs.edit().putString("nameOfPlayerTwo", nameOfPlayerTwo).commit();
        }
        hideRibbon();
        this.mRibbon.postDelayed(new Runnable() {
            public void run() {
                StartupActivity.this.startActivity(new Intent(StartupActivity.this, GameActivity.class));
            }
        }, 1000);
    }

    private void showRibbon() {
        if (this.mRibbonIsUp) {
            this.mNameOfPlayerOne.setVisibility(0);
            if (this.mNumPlayers == 2) {
                this.mLabelNameOfPlayerTwo.setVisibility(0);
                this.mNameOfPlayerTwo.setVisibility(0);
                this.mNameOfPlayerTwo.setNextFocusDownId(this.mButtonStartGame.getId());
                this.mNameOfPlayerOne.setNextFocusDownId(this.mNameOfPlayerTwo.getId());
            } else {
                this.mLabelNameOfPlayerTwo.setVisibility(8);
                this.mNameOfPlayerTwo.setVisibility(8);
                this.mNameOfPlayerOne.setNextFocusDownId(this.mButtonStartGame.getId());
            }
            this.mDarkLayer.bringToFront();
            this.mDarkLayer.startAnimation(getDarkLayerAnimation(true));
            this.mRibbon.bringToFront();
            this.mRibbon.startAnimation(getRibbonAnimation(true));
        }
    }

    private void hideRibbon() {
        if (!this.mRibbonIsUp) {
            this.mDarkLayer.startAnimation(getDarkLayerAnimation(false));
            this.mRibbon.startAnimation(getRibbonAnimation(false));
        }
    }

    private Animation getRibbonAnimation(boolean fallDown) {
        Animation a = AnimationUtils.loadAnimation(this, fallDown ? R.anim.ribbon_fall_down : R.anim.ribbon_wind_up);
        a.setAnimationListener(this);
        if (fallDown) {
            a.setStartOffset(500);
        }
        return a;
    }

    private Animation getDarkLayerAnimation(boolean darken) {
        Animation a;
        if (darken) {
            a = new AlphaAnimation(0.0f, 0.5f);
        } else {
            a = new AlphaAnimation(0.5f, 0.0f);
        }
        a.setFillEnabled(true);
        a.setFillAfter(true);
        a.setDuration(300);
        if (!darken) {
            a.setStartOffset(500);
        }
        return a;
    }

    public void onClick(View v) {
        if (v == this.mButtonOnePlayer) {
            this.mNumPlayers = 1;
            showRibbon();
        } else if (v == this.mButtonTwoPlayers) {
            this.mNumPlayers = 2;
            showRibbon();
        } else if (v == this.mButtonStartGame) {
            startGame();
        }
    }

    public void onAnimationEnd(Animation animation) {
        if (animation == this.mRibbon.getAnimation()) {
            this.mRibbonIsUp = !this.mRibbonIsUp;
            if (this.mRibbonIsUp) {
                this.mButtonStartGame.setVisibility(4);
                this.mNameOfPlayerOne.setVisibility(4);
                this.mNameOfPlayerTwo.setVisibility(4);
                ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mNameOfPlayerOne.getWindowToken(), 0);
                return;
            }
            this.mNameOfPlayerOne.requestFocus();
            if (this.mNameOfPlayerOne.getText().toString().length() == 0) {
                ((InputMethodManager) getSystemService("input_method")).showSoftInput(this.mNameOfPlayerOne, 0);
            }
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
        if (animation == this.mRibbon.getAnimation()) {
            this.mButtonStartGame.setVisibility(0);
        }
    }

    public void onBackPressed() {
        if (!this.mRibbonIsUp) {
            hideRibbon();
        } else {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: private */
    public void checkForUpdates() {
        if (this.mPrefs.getLong("checkForUpdatesAfter", 0) > System.currentTimeMillis()) {
            Log.d("StartupActivity", "Update time not reached, not looking for an update...");
            return;
        }
        this.mPrefs.edit().putLong("checkForUpdatesAfter", System.currentTimeMillis() + 604800000).commit();
        Downloader.setListener(this);
        Downloader.add(new Downloader.Download(this, String.valueOf(getString(R.string.cardCollectionBaseURL)) + "/" + getString(R.string.cardCollectionPlistFileName), "cardset.plist", 0));
        Downloader.start(0);
    }

    private void downloadSet(CardSet.Definition definition, File plistFile) {
        Downloader.setListener(this);
        String baseUrl = getString(R.string.cardCollectionBaseURL);
        String dstDir = String.valueOf(CardSet.getStorageDir(definition)) + CardSet.INCOMPLETE_SUFFIX;
        File absDstDir = new File(getFilesDir(), dstDir);
        absDstDir.mkdirs();
        plistFile.renameTo(new File(absDstDir, plistFile.getName()));
        Downloader.add(new Downloader.Download(this, String.valueOf(baseUrl) + "/" + definition.backside, String.valueOf(dstDir) + "/" + new File(definition.backside).getName(), 0));
        Iterator<String> it = definition.cardFiles.iterator();
        while (it.hasNext()) {
            String s = it.next();
            Downloader.add(new Downloader.Download(this, String.valueOf(baseUrl) + "/" + s, String.valueOf(dstDir) + "/" + new File(s).getName(), 0));
        }
        Downloader.start(1);
    }

    private CardSet.Definition parsePlist(File plistFile) {
        CardSet.Definition definition = new CardSet.Definition();
        PListXMLParser parser = new PListXMLParser();
        try {
            FileInputStream is = new FileInputStream(plistFile);
            PListXMLHandler handler = new PListXMLHandler();
            parser.setHandler(handler);
            parser.parse(is);
            PList plist = handler.getPlist();
            definition.cardFiles = new ArrayList<>();
            Dict root = (Dict) plist.getRootElement();
            Iterator<PListObject> it = root.getConfigurationArray("playCards").iterator();
            while (it.hasNext()) {
                definition.cardFiles.add(((String) it.next()).getValue());
            }
            definition.title = root.getConfiguration("campaignTitle").getValue();
            definition.backside = root.getConfiguration("cardBackImage").getValue();
            definition.version = root.getConfiguration("versionInfo").getValue();
            is.close();
            return definition;
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return null;
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void startFlyAnimationOnCard(Card card) {
        int fromX;
        int fromY;
        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) card.getLayoutParams();
        Random random = new Random();
        int pw = this.mDarkLayer.getWidth();
        int ph = this.mDarkLayer.getHeight();
        if (lp.leftMargin < pw / 2) {
            fromX = (lp.width * -1) - (lp.width * 2);
        } else {
            fromX = lp.leftMargin;
        }
        if (lp.topMargin < ph / 2) {
            fromY = (lp.height * -1) - (lp.height * 2);
        } else {
            fromY = lp.topMargin;
        }
        int delay = random.nextInt(500);
        Animation a = new TranslateAnimation((float) fromX, 0.0f, (float) fromY, 0.0f);
        a.initialize(lp.width, lp.height, pw, ph);
        a.setInterpolator(new DecelerateInterpolator());
        a.setDuration((long) 500);
        a.setStartTime(-1);
        a.setStartOffset((long) delay);
        a.setFillAfter(true);
        a.setFillEnabled(true);
        a.setZAdjustment(1);
        card.startAnimation(a);
    }

    public void onDownloadError(int tag, Exception error, Downloader.Download brokenDownload) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(StartupActivity.this, (int) R.string.NetworkIsDown, 1).show();
            }
        });
    }

    public void onDownloadProgress(int tag, Integer... progress) {
        switch (tag) {
            case 0:
            default:
                return;
            case 1:
                this.mProgressDialog.setProgress(progress[0].intValue());
                return;
        }
    }

    public void onDownloadStarted(int tag, int numDownloads) {
        switch (tag) {
            case 0:
                showDialog(1);
                return;
            case 1:
                showDialog(2);
                this.mProgressDialog.setMax(numDownloads);
                return;
            default:
                return;
        }
    }

    public void onDownloadFinished(int tag, ArrayList<Downloader.Download> finishedDownloads, Long result) {
        this.mProgressDialog.dismiss();
        if (finishedDownloads.size() != 0) {
            switch (tag) {
                case 0:
                    File f = finishedDownloads.get(0).getDestinationFile();
                    CardSet.Definition definition = parsePlist(f);
                    if (!CardSet.setExists(this, definition)) {
                        downloadSet(definition, f);
                        return;
                    }
                    return;
                case 1:
                    File dir = finishedDownloads.get(0).getDestinationFile().getParentFile();
                    if (dir.isDirectory() && dir.getName().endsWith(CardSet.INCOMPLETE_SUFFIX)) {
                        File newDir = new File(dir.getAbsolutePath().replace(CardSet.INCOMPLETE_SUFFIX, ""));
                        if (newDir.exists()) {
                            Log.d("StartupActivity", "New target dir exists, cleaning...");
                            File[] oldFiles = newDir.listFiles();
                            for (File delete : oldFiles) {
                                delete.delete();
                            }
                            newDir.delete();
                        }
                        dir.renameTo(newDir);
                        flyInCards();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public boolean onFlipCard(Card card, boolean willShowFront) {
        card.bringToFront();
        return true;
    }

    public void onCardFlipped(Card card, boolean showsFront) {
    }
}
