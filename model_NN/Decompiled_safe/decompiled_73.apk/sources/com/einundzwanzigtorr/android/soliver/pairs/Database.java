package com.einundzwanzigtorr.android.soliver.pairs;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Database extends SQLiteOpenHelper {
    public static final String[] COLUMNS_SCORES = {"_id", "name", "time", "cdate"};
    public static final String DB_NAME = "sOliverPairs.db";
    public static final String TABLE_SCORES = "scores";
    private static final String TAG = "sOliverPairs/DB";
    private static final int VERSION = 1;
    private static final int VERSION_INITIAL = 1;

    public Database(Context context) {
        super(context, DB_NAME, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE scores (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, time INTEGER NOT NULL)");
        onUpgrade(db, 1, 1);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            Log.i(TAG, "Upgrading Database from " + String.valueOf(oldVersion) + " to " + String.valueOf(newVersion));
        } catch (SQLException e) {
            Log.e(toString(), "Could not upgrade database: " + e.getMessage());
        }
    }

    public void clear() {
        clear(getWritableDatabase());
    }

    public boolean clear(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            db.execSQL("DELETE FROM scores");
            db.setTransactionSuccessful();
            return true;
        } finally {
            db.endTransaction();
        }
    }

    public void insertScore(String playerName, int gameTime) throws SQLException {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("name", playerName);
        cv.put("time", Integer.valueOf(gameTime));
        db.insertOrThrow(TABLE_SCORES, null, cv);
    }
}
