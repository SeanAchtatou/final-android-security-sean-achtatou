package com.einundzwanzigtorr.android.soliver.pairs;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class HighScoresActivity extends BaseListActivity {

    private static class ViewHolder {
        public TextView name;
        public TextView pos;
        public TextView time;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }

    private class ScoreAdapter extends SimpleCursorAdapter {
        private static final int LAYOUT = 2130903042;
        private Cursor mCursor = getCursor();
        private LayoutInflater mInflater;

        public ScoreAdapter(Context context, Cursor c, String[] from, int[] to) {
            super(context, R.layout.highscores_listitem, c, from, to);
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.highscores_listitem, parent, false);
                holder = new ViewHolder(null);
                holder.pos = (TextView) convertView.findViewById(R.id.Text_Pos);
                holder.name = (TextView) convertView.findViewById(R.id.Text_Name);
                holder.time = (TextView) convertView.findViewById(R.id.Text_Time);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            this.mCursor.moveToPosition(position);
            holder.pos.setText(String.format("%d.", Integer.valueOf(position + 1)));
            holder.name.setText(this.mCursor.getString(this.mCursor.getColumnIndexOrThrow("name")));
            holder.time.setText(convertTime(this.mCursor.getInt(this.mCursor.getColumnIndexOrThrow("time"))));
            return convertView;
        }

        public boolean isEnabled(int position) {
            return false;
        }

        private String convertTime(int seconds) {
            return String.format("%02d:%02d", Integer.valueOf(seconds / 60), Integer.valueOf(seconds % 60));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.highscores);
        getListView().setAdapter((ListAdapter) new ScoreAdapter(this, App.getDatabase().getReadableDatabase().query(Database.TABLE_SCORES, new String[]{"_id", "name", "time"}, null, null, null, null, "time ASC"), new String[]{"name", "time"}, new int[]{R.id.Text_Name, R.id.Text_Time}));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AppTracker.page("/highscores/");
    }

    public void back(View v) {
        super.onBackPressed();
    }
}
