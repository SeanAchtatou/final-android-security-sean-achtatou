package com.einundzwanzigtorr.android.soliver.pairs;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundEngine {
    private AudioManager mAudioManager = ((AudioManager) this.mContext.getSystemService("audio"));
    private Context mContext;
    private int mMaxStreams;
    private int[] mPlayMap;
    private int[] mSoundMap;
    private SoundPool mSoundPool;
    private float mVolume;

    public SoundEngine(Context context, int maxStreams) {
        this.mContext = context;
        this.mMaxStreams = maxStreams;
        this.mSoundPool = new SoundPool(this.mMaxStreams, 3, 0);
        this.mSoundMap = new int[this.mMaxStreams];
        this.mPlayMap = new int[this.mMaxStreams];
    }

    public boolean loadSound(int soundId, int resId) {
        if (soundId > this.mMaxStreams - 1) {
            return false;
        }
        this.mSoundMap[soundId] = this.mSoundPool.load(this.mContext, resId, 1);
        return true;
    }

    private void calculateVolume() {
        this.mVolume = ((float) this.mAudioManager.getStreamVolume(3)) / ((float) this.mAudioManager.getStreamMaxVolume(3));
    }

    public void play(int sound) {
        repeat(sound, 0);
    }

    public void repeat(int sound, int timesRepeat) {
        calculateVolume();
        this.mPlayMap[sound] = this.mSoundPool.play(this.mSoundMap[sound], this.mVolume, this.mVolume, 1, timesRepeat, 1.0f);
    }

    public void stop(int sound) {
        if (this.mPlayMap[sound] != 0) {
            this.mSoundPool.stop(this.mPlayMap[sound]);
            this.mPlayMap[sound] = 0;
        }
    }

    public void stopAll() {
        for (int i = 0; i < this.mMaxStreams; i++) {
            stop(i);
        }
    }

    public void release() {
        this.mSoundPool.release();
        this.mSoundPool = null;
    }
}
