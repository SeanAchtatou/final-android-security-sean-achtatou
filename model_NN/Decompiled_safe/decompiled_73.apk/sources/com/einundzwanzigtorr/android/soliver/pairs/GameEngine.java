package com.einundzwanzigtorr.android.soliver.pairs;

import android.content.Context;
import android.database.SQLException;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Chronometer;
import com.einundzwanzigtorr.android.soliver.pairs.Card;
import com.einundzwanzigtorr.android.soliver.pairs.Grid;
import java.util.ArrayList;
import java.util.Iterator;

public class GameEngine implements Grid.OnGridIsReadyListener, Card.OnFlipCardListener {
    private static final int SHUFFLE_TIMES = 3;
    private static final int SOUND_GAME_SOLVED = 1;
    private static final int SOUND_PAIR_SOLVED = 0;
    /* access modifiers changed from: private */
    public static GameEngine mInstance;
    /* access modifiers changed from: private */
    public Card mCard1;
    /* access modifiers changed from: private */
    public Card mCard2;
    private Context mContext;
    private Player mCurrentPlayer;
    private long mElapsedTime;
    private boolean mGameIsRunning;
    private boolean mGameIsSolved;
    private GameListener mGameListener;
    private Chronometer mGameTimer;
    private Grid mGrid;
    private boolean mGridIsShuffled;
    private int mNumColumns;
    private int mNumRows;
    private int mPairsLeft;
    private ArrayList<Player> mPlayers;
    /* access modifiers changed from: private */
    public SoundEngine mSoundEngine;

    public interface GameListener {
        void onGameSolved(Player player, int i);

        void onGameStarted(Player player);

        void onPlayerScored(Player player);

        void onPlayerSwitched(Player player, Player player2);

        void onPlayerWins(Player player);

        void onTie(Score score);
    }

    private GameEngine() {
        reset();
    }

    public static GameEngine getInstance() {
        if (mInstance == null) {
            mInstance = new GameEngine();
        }
        return mInstance;
    }

    public void setGameListener(GameListener gameListener) {
        this.mGameListener = gameListener;
    }

    public void setGameTimer(Chronometer gameTimer) {
        this.mGameTimer = gameTimer;
    }

    public void reset() {
        this.mPlayers = new ArrayList<>();
        this.mGrid = null;
        this.mNumRows = 0;
        this.mNumColumns = 0;
        this.mPairsLeft = 0;
        this.mElapsedTime = 0;
        this.mCard1 = null;
        this.mCard2 = null;
        this.mGridIsShuffled = false;
        this.mGameIsRunning = false;
        this.mGameIsSolved = false;
    }

    public void setGrid(Grid grid) {
        this.mGrid = grid;
    }

    public Grid getGrid() {
        return this.mGrid;
    }

    public void addPlayer(Player player) {
        this.mPlayers.add(player);
        player.setNumber(this.mPlayers.size());
    }

    public ArrayList<Player> getPlayers() {
        return this.mPlayers;
    }

    public int getNumPlayers() {
        return this.mPlayers.size();
    }

    public void setupGrid(Context context, int numRows, int numColumns) {
        this.mContext = context;
        this.mSoundEngine = new SoundEngine(this.mContext, 2);
        this.mSoundEngine.loadSound(0, R.raw.pair_solved);
        this.mSoundEngine.loadSound(1, R.raw.game_solved);
        this.mNumRows = numRows;
        this.mNumColumns = numColumns;
        this.mPairsLeft = (numRows * numColumns) / 2;
        this.mGrid.setup(CardSet.getAvailableSets(this.mContext).get(0), this.mNumRows, this.mNumColumns);
        this.mGrid.setOnFlipCardListener(this);
        this.mGrid.flyInCards();
    }

    public void start() {
        switchPlayers();
        if (this.mGameListener != null) {
            this.mGameListener.onGameStarted(this.mCurrentPlayer);
        }
        this.mElapsedTime = 0;
        this.mGameIsSolved = false;
        resumeTimer();
        this.mGameIsRunning = true;
    }

    public void resume() {
        if (!this.mGameIsSolved) {
            resumeTimer();
            this.mGameIsRunning = true;
        }
    }

    public void pause() {
        pauseTimer();
        this.mGameIsRunning = false;
    }

    public void stop() {
        pauseTimer();
        this.mGameIsRunning = false;
        this.mCurrentPlayer = null;
    }

    public void abort() {
        stop();
        reset();
    }

    public void restart() {
        this.mPairsLeft = (this.mNumRows * this.mNumColumns) / 2;
        this.mElapsedTime = 0;
        this.mGridIsShuffled = false;
        this.mGameIsRunning = false;
        this.mCurrentPlayer = null;
        this.mCard1 = null;
        this.mCard2 = null;
        if (this.mGameTimer != null) {
            this.mGameTimer.stop();
            this.mGameTimer.setBase(SystemClock.elapsedRealtime());
        }
        Iterator<Player> it = this.mPlayers.iterator();
        while (it.hasNext()) {
            it.next().getScore().setPoints(0);
        }
        this.mGrid.resetCards();
    }

    public void shutdown() {
        this.mSoundEngine.release();
    }

    private void pauseTimer() {
        if (this.mGameTimer != null && this.mGameIsRunning) {
            this.mElapsedTime = SystemClock.elapsedRealtime() - this.mGameTimer.getBase();
            this.mGameTimer.stop();
        }
    }

    private void resumeTimer() {
        if (this.mGameTimer != null && !this.mGameIsRunning && !this.mGameIsSolved) {
            this.mGameTimer.setBase(SystemClock.elapsedRealtime() - this.mElapsedTime);
            this.mGameTimer.start();
        }
    }

    /* access modifiers changed from: private */
    public void switchPlayers() {
        if (this.mPlayers != null && this.mPlayers.size() != 0) {
            Player prevPlayer = null;
            if (this.mCurrentPlayer == null) {
                this.mCurrentPlayer = this.mPlayers.get(0);
            } else {
                int i = 0;
                int len = this.mPlayers.size();
                while (true) {
                    if (i >= len) {
                        break;
                    } else if (this.mPlayers.get(i) == this.mCurrentPlayer) {
                        prevPlayer = this.mCurrentPlayer;
                        if (i + 1 < len) {
                            this.mCurrentPlayer = this.mPlayers.get(i + 1);
                        } else {
                            this.mCurrentPlayer = this.mPlayers.get(0);
                        }
                    } else {
                        i++;
                    }
                }
            }
            if (this.mGameListener != null) {
                this.mGameListener.onPlayerSwitched(this.mCurrentPlayer, prevPlayer);
            }
        }
    }

    public boolean isRunning() {
        return this.mGameIsRunning && !this.mGameIsSolved;
    }

    public void shuffle() {
        if (!this.mGridIsShuffled) {
            this.mGrid.setOnGridIsReadyListener(this);
            this.mGrid.shuffle(3);
        }
    }

    private void saveScore() {
        if (getNumPlayers() == 1) {
            try {
                App.getDatabase().insertScore(this.mCurrentPlayer.getName(), (int) (this.mElapsedTime / 1000));
            } catch (SQLException e) {
                Log.e("GameEnginge", "Could not save score");
            }
        }
    }

    private void onPlayerFoundMatch() {
        this.mCurrentPlayer.getScore().inc();
        if (this.mGameListener != null) {
            this.mGameListener.onPlayerScored(this.mCurrentPlayer);
        }
        this.mPairsLeft--;
        if (this.mPairsLeft == 0) {
            onGameSolved();
        } else {
            this.mSoundEngine.play(0);
        }
    }

    private void onGameSolved() {
        this.mGameIsSolved = true;
        pauseTimer();
        saveScore();
        this.mGrid.postDelayed(new Runnable() {
            public void run() {
                GameEngine.this.mSoundEngine.play(1);
            }
        }, 500);
        if (this.mGameListener != null) {
            Player winner = this.mCurrentPlayer;
            if (getNumPlayers() > 1) {
                boolean isTie = true;
                Iterator<Player> it = this.mPlayers.iterator();
                while (it.hasNext()) {
                    Player p = it.next();
                    if (p.getScore().getPoints() > winner.getScore().getPoints()) {
                        winner = p;
                        isTie = false;
                    } else if (p.getScore().getPoints() < winner.getScore().getPoints()) {
                        isTie = false;
                    }
                }
                if (isTie) {
                    this.mGameListener.onTie(winner.getScore());
                } else {
                    this.mGameListener.onPlayerWins(winner);
                }
            } else {
                this.mGameListener.onGameSolved(winner, (int) (this.mElapsedTime / 1000));
            }
        }
        stop();
    }

    public void onGridIsReady(Grid grid) {
    }

    public boolean onFlipCard(Card card, boolean willShowFront) {
        if (!this.mGameIsRunning) {
            return false;
        }
        if (!this.mGridIsShuffled) {
            return false;
        }
        if (!this.mGrid.isReady()) {
            return false;
        }
        if (this.mCard1 != null && this.mCard2 != null) {
            return false;
        }
        if (card == this.mCard1 || card == this.mCard2) {
            return false;
        }
        if (this.mCard1 == null) {
            this.mCard1 = card;
            return true;
        } else if (this.mCard2 != null) {
            return false;
        } else {
            this.mCard2 = card;
            return true;
        }
    }

    public void onCardFlipped(Card card, boolean showsFront) {
        if (this.mCard1 != null && this.mCard2 != null && showsFront) {
            if (this.mCard1.matches(this.mCard2)) {
                this.mCard1.freeze();
                this.mCard1 = null;
                this.mCard2.freeze();
                this.mCard2 = null;
                onPlayerFoundMatch();
                return;
            }
            this.mGrid.postDelayed(new Runnable() {
                public void run() {
                    if (GameEngine.this.mCard1 != null && GameEngine.this.mCard2 != null) {
                        GameEngine.this.mCard1.forceFlip();
                        GameEngine.this.mCard2.forceFlip();
                        GameEngine.this.mCard1 = null;
                        GameEngine.this.mCard2 = null;
                        GameEngine.mInstance.switchPlayers();
                    }
                }
            }, 800);
        }
    }

    public void onGridIsShuffled(Grid grid) {
        this.mGridIsShuffled = true;
        start();
    }
}
