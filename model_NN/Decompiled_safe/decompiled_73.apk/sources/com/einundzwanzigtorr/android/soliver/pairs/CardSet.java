package com.einundzwanzigtorr.android.soliver.pairs;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class CardSet implements Comparable<CardSet> {
    public static final String CARDS_DIR = "cards";
    private static final String CARD_BACKSIDE_PREFIX = "backside";
    private static final String CARD_PREFIX = "img";
    public static final String INCOMPLETE_SUFFIX = ".incomplete";
    private ArrayList<Card> mCards;
    private Context mContext;
    private File mDirectory;
    private boolean mIsFallback = false;
    private String mTitle;

    public static class Definition {
        public String backside;
        public ArrayList<String> cardFiles;
        public String title;
        public String version;
    }

    public CardSet(Context context, File directory) {
        this.mContext = context;
        this.mIsFallback = false;
        this.mDirectory = directory;
        clear();
    }

    public CardSet(Context context) {
        this.mContext = context;
        this.mIsFallback = true;
        this.mDirectory = null;
        clear();
    }

    public void clear() {
        this.mCards = new ArrayList<>();
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getTitle() {
        return this.mTitle;
    }

    public int getNumCards() {
        return this.mCards.size();
    }

    public ArrayList<Card> getCards() {
        if (this.mCards.isEmpty()) {
            load();
        }
        return this.mCards;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Card getCard(int index) {
        if (this.mCards.isEmpty()) {
            load();
        }
        if (index < this.mCards.size()) {
            return this.mCards.get(index);
        }
        return null;
    }

    public void load() {
        if (this.mIsFallback) {
            loadFallbackSet();
        } else {
            loadDownloadedSet();
        }
    }

    public static boolean setExists(Context context, Definition definition) {
        File storageDir = new File(context.getFilesDir(), getStorageDir(definition));
        if (!new File(storageDir, new File(definition.backside).getName()).exists()) {
            return false;
        }
        Iterator<String> it = definition.cardFiles.iterator();
        while (it.hasNext()) {
            if (!new File(storageDir, new File(it.next()).getName()).exists()) {
                return false;
            }
        }
        return true;
    }

    public static String getStorageDir(Definition definition) {
        return CARDS_DIR + File.separatorChar + definition.title.replaceAll("[^a-zA-Z0-9]", "");
    }

    public static ArrayList<CardSet> getAvailableSets(final Context context) {
        final ArrayList<CardSet> sets = new ArrayList<>();
        new File(context.getFilesDir(), CARDS_DIR).listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                if (!pathname.isDirectory() || pathname.getName().endsWith(CardSet.INCOMPLETE_SUFFIX)) {
                    return false;
                }
                sets.add(new CardSet(context, pathname));
                return false;
            }
        });
        CardSet fallbackSet = new CardSet(context);
        fallbackSet.setTitle(context.getString(R.string.DefaultCardSetTitle));
        sets.add(fallbackSet);
        Collections.sort(sets);
        Collections.reverse(sets);
        return sets;
    }

    /* access modifiers changed from: protected */
    public void loadFallbackSet() {
        AssetManager assetManager = this.mContext.getAssets();
        InputStream backsideInputStream = null;
        try {
            String[] files = assetManager.list(CARDS_DIR);
            int len = files.length;
            for (int i = 0; i < len; i++) {
                String path = CARDS_DIR + File.separator + files[i];
                if (files[i].startsWith(CARD_BACKSIDE_PREFIX)) {
                    backsideInputStream = assetManager.open(path);
                } else if (files[i].startsWith(CARD_PREFIX)) {
                    InputStream frontInputStream = assetManager.open(path);
                    Bitmap b = getBitmapFromStream(frontInputStream);
                    frontInputStream.close();
                    Card card1 = new Card(this.mContext);
                    card1.setSideFromBitmap(b, 0);
                    this.mCards.add(card1);
                    Card card2 = new Card(this.mContext);
                    card2.setSideFromBitmap(b, 0);
                    this.mCards.add(card2);
                    card1.setCounterPart(card2);
                    card2.setCounterPart(card1);
                }
            }
            Bitmap b2 = getBitmapFromStream(backsideInputStream);
            backsideInputStream.close();
            Iterator<Card> it = this.mCards.iterator();
            while (it.hasNext()) {
                Card card = it.next();
                card.setSideFromBitmap(b2, 1);
                card.showFront();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void loadDownloadedSet() {
        IOException e;
        NullPointerException e2;
        FileInputStream fileInputStream;
        try {
            String[] files = this.mDirectory.list();
            int i = 0;
            int len = files.length;
            FileInputStream fileInputStream2 = null;
            while (i < len) {
                File f = new File(this.mDirectory, files[i]);
                String name = f.getName();
                if (name.startsWith(CARD_BACKSIDE_PREFIX)) {
                    fileInputStream = new FileInputStream(f);
                } else {
                    if (name.startsWith(CARD_PREFIX)) {
                        InputStream frontInputStream = new FileInputStream(f);
                        Bitmap b = getBitmapFromStream(frontInputStream);
                        frontInputStream.close();
                        Card card1 = new Card(this.mContext);
                        card1.setSideFromBitmap(b, 0);
                        this.mCards.add(card1);
                        Card card2 = new Card(this.mContext);
                        card2.setSideFromBitmap(b, 0);
                        this.mCards.add(card2);
                        card1.setCounterPart(card2);
                        card2.setCounterPart(card1);
                    }
                    fileInputStream = fileInputStream2;
                }
                i++;
                fileInputStream2 = fileInputStream;
            }
            try {
                Bitmap b2 = getBitmapFromStream(fileInputStream2);
                fileInputStream2.close();
                Iterator<Card> it = this.mCards.iterator();
                while (it.hasNext()) {
                    Card card = it.next();
                    card.setSideFromBitmap(b2, 1);
                    card.showFront();
                }
            } catch (IOException e3) {
                e = e3;
                e.printStackTrace();
            } catch (NullPointerException e4) {
                e2 = e4;
                e2.printStackTrace();
            }
        } catch (IOException e5) {
            e = e5;
            e.printStackTrace();
        } catch (NullPointerException e6) {
            e2 = e6;
            e2.printStackTrace();
        }
    }

    public int indexOf(Card card) {
        return this.mCards.indexOf(card);
    }

    /* access modifiers changed from: protected */
    public Bitmap getBitmapFromStream(InputStream is) {
        return BitmapFactory.decodeStream(is);
    }

    public int compareTo(CardSet another) {
        if (this.mDirectory == null) {
            return -1;
        }
        return Long.valueOf(this.mDirectory.lastModified()).compareTo(Long.valueOf(another.mDirectory.lastModified()));
    }
}
