package com.einundzwanzigtorr.android.soliver.pairs;

import android.database.Cursor;

public class Player {
    private String mName = "";
    private int mNumber = 0;
    private Score mScore = new Score();

    public void setNumber(int number) {
        this.mNumber = number;
    }

    public int getNumber() {
        return this.mNumber;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name.trim();
    }

    public Score getScore() {
        return this.mScore;
    }

    public boolean isHighscore(int gameTimeInSeconds) {
        boolean isHighscore;
        Cursor cursor = App.getDatabase().getReadableDatabase().query(Database.TABLE_SCORES, new String[]{"_id", "name", "time"}, "name = ? AND time < ?", new String[]{this.mName, String.valueOf(gameTimeInSeconds)}, null, null, null);
        if (cursor.moveToFirst()) {
            isHighscore = false;
        } else {
            isHighscore = true;
        }
        cursor.close();
        return isHighscore;
    }
}
