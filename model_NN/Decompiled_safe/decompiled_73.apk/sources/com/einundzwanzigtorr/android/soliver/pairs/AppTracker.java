package com.einundzwanzigtorr.android.soliver.pairs;

import android.content.Context;
import android.util.Log;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class AppTracker {
    public static final String CATEGORY_GAME = "Game";
    public static final String CATEGORY_SETTINGS = "Settings";
    protected static GoogleAnalyticsTracker GA_TRACKER = null;
    public static final String NAME_GAME_RESTARTED = "gameRestarted";
    public static final String NAME_NUM_PLAYERS = "numPlayers";
    public static final int SCOPE_PAGE = 3;
    public static final int SCOPE_SESSION = 2;
    public static final int SCOPE_VISITOR = 1;
    protected static boolean TRACKING_ENABLED = false;

    public static void start(Context context) {
        if (TRACKING_ENABLED) {
            stop();
            if (GA_TRACKER == null) {
                GA_TRACKER = GoogleAnalyticsTracker.getInstance();
            }
            try {
                GA_TRACKER.start(context.getString(R.string.GAWebPropertyId), context);
                Log.d("AppTracker", "Tracking started");
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    public static void page(String url) {
        if (TRACKING_ENABLED && GA_TRACKER != null) {
            GA_TRACKER.trackPageView(url);
            Log.d("AppTracker", "Tracking page: " + url);
        }
    }

    public static void event(String category, String action, String label, int value) {
        if (TRACKING_ENABLED && GA_TRACKER != null) {
            GA_TRACKER.trackEvent(category, action, label, value);
            Log.d("AppTracker", String.format("Tracking event: %s/%s/%s/%d", category, action, label, Integer.valueOf(value)));
        }
    }

    public static void event(String category, String action) {
        event(category, action, "", -1);
    }

    public static void set(int slot, String name, String value, int scope) {
        if (TRACKING_ENABLED && GA_TRACKER != null) {
            GA_TRACKER.setCustomVar(slot, name, value, scope);
            Log.d("AppTracker", String.format("Setting custom var: %d/%s/%s/%d", Integer.valueOf(slot), name, value, Integer.valueOf(scope)));
        }
    }

    public static void dispatch() {
        if (TRACKING_ENABLED) {
            try {
                GA_TRACKER.dispatch();
                Log.d("AppTracker", String.format("Dispatching data.", new Object[0]));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    public static void stop() {
        if (GA_TRACKER != null) {
            dispatch();
            GA_TRACKER.stop();
            Log.d("AppTracker", String.format("Tracking stopped.", new Object[0]));
        }
    }

    public static void setTrackingEnabled(boolean enabled) {
        TRACKING_ENABLED = enabled;
        Log.d("AppTracker", "Tracking is " + (enabled ? "enabled" : "disabled"));
    }
}
