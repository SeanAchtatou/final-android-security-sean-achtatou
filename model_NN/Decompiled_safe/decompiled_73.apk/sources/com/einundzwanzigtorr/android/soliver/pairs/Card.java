package com.einundzwanzigtorr.android.soliver.pairs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.NinePatchDrawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.einundzwanzigtorr.android.soliver.pairs.Grid;

public class Card extends ImageView implements Animation.AnimationListener {
    public static final int SIDE_BACK = 1;
    public static final int SIDE_FRONT = 0;
    private Bitmap mBackBitmap;
    private NinePatchDrawable mBackgroundDrawable;
    private Rect mBackgroundPadding;
    private Context mContext;
    private Card mCounterpart;
    private AnimationSet mFlipHideAnim;
    private AnimationSet mFlipShowAnim;
    private Bitmap mFrontBitmap;
    private boolean mIsFrozen = false;
    private OnFlipCardListener mOnFlipCardListener;
    private int mPivotX;
    private int mPivotY;
    private int mRotation = 0;
    private boolean mShowsFront = false;

    public interface OnFlipCardListener {
        void onCardFlipped(Card card, boolean z);

        boolean onFlipCard(Card card, boolean z);
    }

    public Card(Context context) {
        super(context);
        this.mContext = context;
        this.mBackgroundDrawable = (NinePatchDrawable) this.mContext.getResources().getDrawable(R.drawable.dropshadow);
        this.mBackgroundDrawable.getPaint().setFlags(3);
        this.mBackgroundPadding = new Rect(0, 0, 0, 0);
        this.mBackgroundDrawable.getPadding(this.mBackgroundPadding);
        setPadding(this.mBackgroundPadding.left, this.mBackgroundPadding.top, this.mBackgroundPadding.right, this.mBackgroundPadding.bottom);
        setScaleType(ImageView.ScaleType.CENTER_CROP);
        setDrawingCacheEnabled(true);
        createFlipAnim();
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Card.this.flip();
            }
        });
    }

    public void setCounterPart(Card counterpart) {
        this.mCounterpart = counterpart;
    }

    public Card getCounterPart() {
        return this.mCounterpart;
    }

    public void setOnFlipCardListener(OnFlipCardListener onFlipCardListener) {
        this.mOnFlipCardListener = onFlipCardListener;
    }

    public void setSideFromBitmap(Bitmap b, int side) {
        if (side == 0) {
            this.mFrontBitmap = b;
        } else if (side == 1) {
            this.mBackBitmap = b;
        }
    }

    public void setPosition(int x, int y) {
        Grid.LayoutParams lp = (Grid.LayoutParams) getLayoutParams();
        lp.x = x;
        lp.y = y;
        setLayoutParams(lp);
        invalidate();
    }

    public void setRotation(int rotation) {
        this.mRotation = rotation;
    }

    public boolean matches(Card card) {
        return this.mFrontBitmap.equals(card.mFrontBitmap);
    }

    public void freeze() {
        this.mIsFrozen = true;
    }

    public void unfreeze() {
        this.mIsFrozen = false;
    }

    public void flip() {
        if (!this.mIsFrozen) {
            if (this.mOnFlipCardListener != null) {
                if (!this.mOnFlipCardListener.onFlipCard(this, !this.mShowsFront)) {
                    return;
                }
            }
            forceFlip();
        }
    }

    public void forceFlip() {
        startAnimation(this.mFlipHideAnim);
    }

    public void showFront() {
        this.mShowsFront = false;
        toggleImage();
    }

    public void showBack() {
        this.mShowsFront = true;
        toggleImage();
    }

    /* access modifiers changed from: protected */
    public void toggleImage() {
        setImageBitmap(this.mShowsFront ? this.mBackBitmap : this.mFrontBitmap);
        this.mShowsFront = !this.mShowsFront;
    }

    private void createFlipAnim() {
        this.mFlipHideAnim = (AnimationSet) AnimationUtils.loadAnimation(this.mContext, R.anim.shrink_to_middle);
        this.mFlipHideAnim.setAnimationListener(this);
        this.mFlipShowAnim = (AnimationSet) AnimationUtils.loadAnimation(this.mContext, R.anim.grow_from_middle);
        this.mFlipShowAnim.setAnimationListener(this);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.mPivotX == 0) {
            int w = getMeasuredWidth();
            int h = getMeasuredHeight();
            this.mPivotX = w / 2;
            this.mPivotY = h / 2;
            this.mBackgroundDrawable.setBounds(0, 0, w, h);
            ((BitmapDrawable) getDrawable()).getPaint().setFlags(3);
        }
        canvas.save();
        if (this.mRotation != 0) {
            canvas.rotate((float) this.mRotation, (float) this.mPivotX, (float) this.mPivotY);
        }
        this.mBackgroundDrawable.draw(canvas);
        super.onDraw(canvas);
        canvas.restore();
    }

    public void onAnimationEnd(Animation animation) {
        if (animation == this.mFlipHideAnim) {
            toggleImage();
            startAnimation(this.mFlipShowAnim);
        } else if (this.mOnFlipCardListener != null) {
            this.mOnFlipCardListener.onCardFlipped(this, this.mShowsFront);
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
