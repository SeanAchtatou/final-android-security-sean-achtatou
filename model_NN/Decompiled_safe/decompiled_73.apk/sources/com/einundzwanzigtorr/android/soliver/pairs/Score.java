package com.einundzwanzigtorr.android.soliver.pairs;

public class Score {
    private static final int DEFAULT_INCREASE_VALUE = 1;
    private int mPoints = 0;

    public int getPoints() {
        return this.mPoints;
    }

    public void setPoints(int points) {
        this.mPoints = points;
    }

    public void inc(int amount) {
        this.mPoints += amount;
    }

    public void inc() {
        inc(1);
    }
}
