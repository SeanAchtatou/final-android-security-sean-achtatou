package com.einundzwanzigtorr.android.soliver.pairs;

import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;
import com.einundzwanzigtorr.android.soliver.pairs.GameEngine;
import com.einundzwanzigtorr.android.soliver.pairs.Grid;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import java.util.ArrayList;
import java.util.Iterator;

public class GameActivity extends BaseActivity implements SensorEventListener, View.OnClickListener, Animation.AnimationListener, Grid.OnGridIsReadyListener, GameEngine.GameListener {
    private static final String EXPIRES = "expires_in";
    private static final String[] FB_PERMISSIONS = {"publish_stream"};
    private static final String KEY = "facebook-credentials";
    private static final String TOKEN = "access_token";
    private float mAccel;
    private float mAccelCurrent;
    private float mAccelLast;
    private ImageView mBackButton;
    private View mDarkLayer;
    private Facebook mFacebook;
    private Chronometer mGameTimer;
    private ImageView mPauseButton;
    private TextView mPlayerName;
    private TextView mPlayerOneScore;
    private TextView mPlayerTwoScore;
    private ViewGroup mRibbon;
    private Button mRibbonButton;
    private Button mRibbonButton2;
    private boolean mRibbonIsUp;
    private SensorManager mSensorManager;
    private String mShareMessage;
    private boolean mShouldShowRestartButtons;
    private boolean mShouldShuffle;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.game);
        this.mFacebook = new Facebook(getString(R.string.fbAppId));
        this.mPlayerName = (TextView) findViewById(R.id.Text_PlayerName);
        this.mPlayerOneScore = (TextView) findViewById(R.id.Text_PlayerOneScore);
        this.mPlayerTwoScore = (TextView) findViewById(R.id.Text_PlayerTwoScore);
        this.mGameTimer = (Chronometer) findViewById(R.id.GameTimer);
        setVolumeControlStream(3);
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.mAccel = 0.0f;
        this.mAccelCurrent = 9.80665f;
        this.mAccelLast = 9.80665f;
        this.mBackButton = (ImageView) findViewById(R.id.Button_Back);
        this.mBackButton.setOnClickListener(this);
        this.mPauseButton = (ImageView) findViewById(R.id.Button_Pause);
        this.mPauseButton.setOnClickListener(this);
        this.mDarkLayer = findViewById(R.id.DarkLayer);
        this.mRibbon = (ViewGroup) findViewById(R.id.Ribbon);
        this.mRibbonIsUp = true;
        this.mRibbonButton = (Button) this.mRibbon.findViewById(R.id.RibbonButton);
        this.mRibbonButton2 = (Button) this.mRibbon.findViewById(R.id.RibbonButton2);
        this.mShouldShuffle = true;
        this.mShouldShowRestartButtons = false;
        ((Button) this.mRibbon.findViewById(R.id.FacebookButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GameActivity.this.shareOnFacebook();
            }
        });
        Grid grid = (Grid) findViewById(R.id.grid);
        grid.setOnGridIsReadyListener(this);
        GameEngine game = App.getGameEngine();
        game.setGrid(grid);
        Iterator<Player> it = game.getPlayers().iterator();
        while (it.hasNext()) {
            onPlayerScored(it.next());
        }
        setup();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AppTracker.set(1, AppTracker.NAME_NUM_PLAYERS, String.valueOf(App.getGameEngine().getNumPlayers()), 3);
        AppTracker.set(2, AppTracker.NAME_GAME_RESTARTED, "No", 3);
        AppTracker.page("/game/");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        stopShakeSensor();
        pauseGame();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (isFinishing()) {
            App.getGameEngine().abort();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.mFacebook.authorizeCallback(requestCode, resultCode, data);
    }

    private void toggleFacebookButton(boolean show) {
        ((Button) this.mRibbon.findViewById(R.id.FacebookButton)).setVisibility(show ? 0 : 8);
    }

    public boolean saveFBCredentials() {
        SharedPreferences.Editor editor = getApplicationContext().getSharedPreferences(KEY, 0).edit();
        editor.putString("access_token", this.mFacebook.getAccessToken());
        editor.putLong("expires_in", this.mFacebook.getAccessExpires());
        return editor.commit();
    }

    public boolean restoreFBCredentials() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(KEY, 0);
        this.mFacebook.setAccessToken(sharedPreferences.getString("access_token", null));
        this.mFacebook.setAccessExpires(sharedPreferences.getLong("expires_in", 0));
        return this.mFacebook.isSessionValid();
    }

    /* access modifiers changed from: private */
    public void shareOnFacebook() {
        if (!restoreFBCredentials()) {
            this.mFacebook.authorize(this, FB_PERMISSIONS, new Facebook.DialogListener() {
                public void onFacebookError(FacebookError e) {
                    Log.d("GameActivity", e.getMessage());
                    AppTracker.event(AppTracker.CATEGORY_GAME, "facebookError");
                }

                public void onError(DialogError e) {
                    Log.d("GameActivity", e.getMessage());
                }

                public void onComplete(Bundle values) {
                    GameActivity.this.saveFBCredentials();
                    GameActivity.this.shareOnFacebook();
                }

                public void onCancel() {
                }
            });
            return;
        }
        AppTracker.set(1, AppTracker.NAME_NUM_PLAYERS, String.valueOf(App.getGameEngine().getNumPlayers()), 3);
        AppTracker.page("/facebook/");
        Bundle parameters = new Bundle();
        parameters.putString("description", this.mShareMessage);
        parameters.putString("link", getString(R.string.fbShareUrl));
        parameters.putString("name", getString(R.string.app_name));
        parameters.putString("picture", getString(R.string.fbShareImage));
        this.mFacebook.dialog(this, "feed", parameters, new Facebook.DialogListener() {
            public void onComplete(Bundle values) {
            }

            public void onFacebookError(FacebookError error) {
                Log.d("GameActivity", error.getMessage());
                AppTracker.event(AppTracker.CATEGORY_GAME, "facebookError");
            }

            public void onError(DialogError e) {
                Log.d("GameActivity", e.getMessage());
            }

            public void onCancel() {
            }
        });
    }

    private void setup() {
        GameEngine game = App.getGameEngine();
        if (game.getNumPlayers() == 1) {
            game.setGameTimer(this.mGameTimer);
            this.mPlayerOneScore.setVisibility(8);
            this.mPlayerTwoScore.setVisibility(8);
            this.mGameTimer.setVisibility(0);
        } else {
            this.mGameTimer.setVisibility(8);
            this.mPlayerOneScore.setVisibility(0);
            this.mPlayerTwoScore.setVisibility(0);
        }
        int numRows = Integer.valueOf(getString(R.string.numRows)).intValue();
        int numCols = Integer.valueOf(getString(R.string.numCols)).intValue();
        if (getResources().getConfiguration().orientation == 2) {
            numRows = Integer.valueOf(getString(R.string.numCols)).intValue();
            numCols = Integer.valueOf(getString(R.string.numRows)).intValue();
        }
        game.setGameListener(this);
        if (!game.isRunning()) {
            game.setupGrid(this, numRows, numCols);
        }
    }

    private void pauseGame() {
        final GameEngine game = App.getGameEngine();
        if (game.isRunning()) {
            game.pause();
            showRibbon((int) R.string.GameIsPaused, (int) R.string.Resume, new View.OnClickListener() {
                public void onClick(View v) {
                    GameActivity.this.hideRibbon();
                    game.resume();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void abortGame() {
        GameEngine game = App.getGameEngine();
        int numPlayers = game.getNumPlayers();
        game.abort();
        if (numPlayers > 1) {
            AppTracker.event(AppTracker.CATEGORY_GAME, "mulitplayerAbort");
        } else {
            AppTracker.event(AppTracker.CATEGORY_GAME, "singleplayerAbort");
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void restartGame() {
        Animation in;
        hideRibbon();
        GameEngine game = App.getGameEngine();
        game.getGrid().setOnGridIsReadyListener(this);
        game.restart();
        Iterator<Player> it = game.getPlayers().iterator();
        while (it.hasNext()) {
            onPlayerScored(it.next());
        }
        AppTracker.set(1, AppTracker.NAME_NUM_PLAYERS, String.valueOf(game.getNumPlayers()), 3);
        AppTracker.set(2, AppTracker.NAME_GAME_RESTARTED, "Yes", 3);
        AppTracker.page("/game/");
        if (getResources().getConfiguration().orientation == 2) {
            in = AnimationUtils.loadAnimation(this, R.anim.move_in);
        } else {
            in = AnimationUtils.loadAnimation(this, R.anim.appear);
        }
        this.mPlayerOneScore.startAnimation(in);
        this.mPlayerTwoScore.startAnimation(in);
    }

    /* access modifiers changed from: private */
    public void showHighScores() {
        startActivity(new Intent(this, HighScoresActivity.class));
    }

    private void showRibbon(int stringResId, int buttonTextResId, View.OnClickListener onButtonClick) {
        showRibbon(getString(stringResId), buttonTextResId, onButtonClick);
    }

    private void showRibbon(String text, int buttonTextResId, View.OnClickListener onButtonClick) {
        if (this.mRibbonIsUp) {
            ((TextView) this.mRibbon.findViewById(R.id.RibbonText)).setText(text);
            if (onButtonClick != null) {
                this.mRibbonButton.setVisibility(0);
                this.mRibbonButton.setText(buttonTextResId);
                this.mRibbonButton.setOnClickListener(onButtonClick);
            }
            this.mDarkLayer.startAnimation(getDarkLayerAnimation(true));
            this.mDarkLayer.setVisibility(0);
            this.mRibbon.startAnimation(getRibbonAnimation(true));
            this.mRibbon.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void hideRibbon() {
        if (!this.mRibbonIsUp) {
            this.mDarkLayer.startAnimation(getDarkLayerAnimation(false));
            this.mRibbon.startAnimation(getRibbonAnimation(false));
        }
    }

    private Animation getRibbonAnimation(boolean fallDown) {
        Animation a = AnimationUtils.loadAnimation(this, fallDown ? R.anim.ribbon_fall_down : R.anim.ribbon_wind_up);
        a.setAnimationListener(this);
        if (fallDown) {
            a.setStartOffset(500);
        }
        return a;
    }

    private Animation getDarkLayerAnimation(boolean darken) {
        Animation a;
        if (darken) {
            a = new AlphaAnimation(0.0f, 0.5f);
        } else {
            a = new AlphaAnimation(0.5f, 0.0f);
        }
        a.setFillEnabled(true);
        a.setFillAfter(true);
        a.setDuration(300);
        if (!darken) {
            a.setStartOffset(500);
        }
        return a;
    }

    private void startShakeSensor() {
        this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(1), 3);
    }

    private void stopShakeSensor() {
        this.mSensorManager.unregisterListener(this);
    }

    private String convertTime(int seconds) {
        return String.format("%02d:%02d", Integer.valueOf(seconds / 60), Integer.valueOf(seconds % 60));
    }

    /* access modifiers changed from: private */
    public void onDeviceIsShaked() {
        stopShakeSensor();
        this.mShouldShuffle = true;
        hideRibbon();
    }

    public void onBackPressed() {
        if (App.getGameEngine().isRunning() && this.mRibbonIsUp) {
            showRibbon((int) R.string.QuitGameConfirmationText, (int) R.string.Quit, new View.OnClickListener() {
                public void onClick(View v) {
                    GameActivity.this.abortGame();
                }
            });
        } else if (!this.mRibbonIsUp) {
            hideRibbon();
        } else {
            super.onBackPressed();
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];
        this.mAccelLast = this.mAccelCurrent;
        this.mAccelCurrent = (float) Math.sqrt((double) ((x * x) + (y * y) + (z * z)));
        this.mAccel = (this.mAccel * 0.9f) + (this.mAccelCurrent - this.mAccelLast);
        if (Math.abs(this.mAccel) > 2.0f) {
            onDeviceIsShaked();
        }
    }

    public void onClick(View v) {
        if (v == this.mBackButton) {
            onBackPressed();
        } else if (v == this.mPauseButton) {
            pauseGame();
        }
    }

    public void onAnimationEnd(Animation animation) {
        if (animation == this.mRibbon.getAnimation()) {
            this.mRibbonIsUp = !this.mRibbonIsUp;
            if (this.mRibbonIsUp) {
                this.mRibbonButton.setVisibility(4);
                this.mRibbonButton2.setVisibility(4);
                toggleFacebookButton(false);
                if (this.mShouldShowRestartButtons) {
                    this.mRibbonButton2.setVisibility(0);
                    this.mRibbonButton2.setText((int) R.string.BackToMainMenu);
                    this.mRibbonButton2.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            GameActivity.this.abortGame();
                        }
                    });
                    showRibbon((int) R.string.RestartOrExit, (int) R.string.PlayAgain, new View.OnClickListener() {
                        public void onClick(View v) {
                            GameActivity.this.restartGame();
                        }
                    });
                    this.mShouldShowRestartButtons = false;
                }
            }
            GameEngine game = App.getGameEngine();
            if (this.mShouldShuffle) {
                if (this.mRibbonIsUp) {
                    game.shuffle();
                    this.mShouldShuffle = false;
                    return;
                }
                startShakeSensor();
            } else if (this.mRibbonIsUp) {
                game.resume();
            } else {
                game.pause();
            }
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onGridIsReady(Grid grid) {
        this.mShouldShuffle = true;
        showRibbon((int) R.string.ShakeToShuffle, (int) R.string.Shuffle, new View.OnClickListener() {
            public void onClick(View v) {
                GameActivity.this.onDeviceIsShaked();
            }
        });
    }

    public void onGridIsShuffled(Grid grid) {
    }

    public void onPlayerSwitched(Player nextPlayer, Player previousPlayer) {
        Animation in;
        Animation out;
        this.mPlayerName.setText(nextPlayer.getName());
        if (getResources().getConfiguration().orientation == 2) {
            in = AnimationUtils.loadAnimation(this, R.anim.move_in);
            out = AnimationUtils.loadAnimation(this, R.anim.move_out);
        } else {
            in = AnimationUtils.loadAnimation(this, R.anim.appear);
            out = AnimationUtils.loadAnimation(this, R.anim.dissolve);
        }
        if (nextPlayer.getNumber() == 1) {
            if (previousPlayer != null) {
                this.mPlayerOneScore.startAnimation(in);
            }
            this.mPlayerTwoScore.startAnimation(out);
        } else if (nextPlayer.getNumber() == 2) {
            this.mPlayerOneScore.startAnimation(out);
            if (previousPlayer != null) {
                this.mPlayerTwoScore.startAnimation(in);
            }
        }
    }

    public void onGameStarted(Player currentPlayer) {
    }

    public void onPlayerScored(Player player) {
        String text = String.format("%s: %d", player.getName(), Integer.valueOf(player.getScore().getPoints()));
        if (player.getNumber() == 1) {
            this.mPlayerOneScore.setText(text);
        } else if (player.getNumber() == 2) {
            this.mPlayerTwoScore.setText(text);
        }
    }

    public void onPlayerWins(Player winner) {
        Animation in;
        this.mGameTimer.setVisibility(8);
        this.mPlayerOneScore.setVisibility(0);
        this.mPlayerTwoScore.setVisibility(0);
        if (getResources().getConfiguration().orientation == 2) {
            in = AnimationUtils.loadAnimation(this, R.anim.move_in);
        } else {
            in = AnimationUtils.loadAnimation(this, R.anim.appear);
        }
        this.mPlayerOneScore.startAnimation(in);
        this.mPlayerTwoScore.startAnimation(in);
        Player looser = null;
        Iterator<Player> it = App.getGameEngine().getPlayers().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Player p = it.next();
            if (p != winner) {
                looser = p;
                break;
            }
        }
        this.mShareMessage = getString(R.string.FacebookMultiPlayerWinMessage, new Object[]{winner.getName(), looser.getName()});
        toggleFacebookButton(true);
        showRibbon(getString(R.string.PlayerHasWon, new Object[]{winner.getName(), Integer.valueOf(winner.getScore().getPoints())}), 0, (View.OnClickListener) null);
        this.mShouldShowRestartButtons = true;
        AppTracker.event(AppTracker.CATEGORY_GAME, "mulitplayerWon");
    }

    public void onTie(Score score) {
        ArrayList<Player> players = App.getGameEngine().getPlayers();
        this.mShareMessage = getString(R.string.FacebookMultiPlayerDrawMessage, new Object[]{players.get(0).getName(), players.get(1).getName()});
        toggleFacebookButton(true);
        showRibbon((int) R.string.GameTie, 0, (View.OnClickListener) null);
        this.mShouldShowRestartButtons = true;
        AppTracker.event(AppTracker.CATEGORY_GAME, "mulitplayerTie");
    }

    public void onGameSolved(Player player, int gameTimeInSeconds) {
        String text;
        toggleFacebookButton(true);
        String time = convertTime(gameTimeInSeconds);
        this.mGameTimer.setText(time);
        if (player.isHighscore(gameTimeInSeconds)) {
            this.mShareMessage = getString(R.string.FacebookSinglePlayerWinNewHighscoreMessage, new Object[]{player.getName(), time});
            text = getString(R.string.SinglePlayerSolvedHighScore, new Object[]{time});
        } else {
            this.mShareMessage = getString(R.string.FacebookSinglePlayerWinMessage, new Object[]{player.getName(), time});
            text = getString(R.string.SinglePlayerSolved, new Object[]{time});
        }
        showRibbon(text, (int) R.string.ShowHighScore, new View.OnClickListener() {
            public void onClick(View v) {
                GameActivity.this.showHighScores();
            }
        });
        this.mShouldShowRestartButtons = true;
        AppTracker.event(AppTracker.CATEGORY_GAME, "singleplayerWon");
    }
}
