package com.einundzwanzigtorr.android.soliver.pairs;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class BasePreferenceActivity extends PreferenceActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppTracker.start(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        AppTracker.stop();
        super.onDestroy();
    }
}
