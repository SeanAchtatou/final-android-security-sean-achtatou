package com.einundzwanzigtorr.android.soliver.pairs;

import android.app.ListActivity;
import android.os.Bundle;

public class BaseListActivity extends ListActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppTracker.start(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        AppTracker.stop();
        super.onDestroy();
    }
}
