package com.einundzwanzigtorr.android.soliver.pairs;

import android.app.Application;
import android.preference.PreferenceManager;

public class App extends Application {
    private static Database mDatabase;
    private static GameEngine mGameEngine;

    public void onCreate() {
        super.onCreate();
        mGameEngine = GameEngine.getInstance();
        mDatabase = new Database(getApplicationContext());
        AppTracker.setTrackingEnabled(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean("enableGoogleAnalytics", true));
    }

    public static GameEngine getGameEngine() {
        return mGameEngine;
    }

    public static Database getDatabase() {
        return mDatabase;
    }
}
