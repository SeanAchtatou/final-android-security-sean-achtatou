package com.einundzwanzigtorr.android.soliver.pairs;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import com.einundzwanzigtorr.android.soliver.pairs.Card;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

public class Grid extends ViewGroup {
    protected int mCardSize;
    protected int mCardSpacing;
    protected Card[][] mCards;
    protected Context mContext;
    protected boolean mGridIsReady;
    protected Card mLastShuffledCard;
    protected int mNumColumns;
    protected int mNumRows;
    protected Card.OnFlipCardListener mOnFlipCardListener;
    protected OnGridIsReadyListener mOnGridIsReadyListener;
    protected CardSet mSet;
    protected int mShuffleTimes;

    public interface OnGridIsReadyListener {
        void onGridIsReady(Grid grid);

        void onGridIsShuffled(Grid grid);
    }

    public Grid(Context context) {
        super(context);
        init(context);
    }

    public Grid(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public Grid(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public boolean isReady() {
        return this.mGridIsReady;
    }

    public void setOnGridIsReadyListener(OnGridIsReadyListener onGridIsReadyListener) {
        this.mOnGridIsReadyListener = onGridIsReadyListener;
    }

    public void setOnFlipCardListener(Card.OnFlipCardListener onFlipCardListener) {
        this.mOnFlipCardListener = onFlipCardListener;
        if (this.mCards != null) {
            for (int rowIdx = 0; rowIdx < this.mNumRows; rowIdx++) {
                for (int colIdx = 0; colIdx < this.mNumColumns; colIdx++) {
                    this.mCards[rowIdx][colIdx].setOnFlipCardListener(this.mOnFlipCardListener);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void init(Context context) {
        this.mContext = context;
    }

    public void setup(CardSet set, int rows, int columns) {
        this.mNumRows = rows;
        this.mNumColumns = columns;
        this.mCardSize = 0;
        this.mCardSpacing = 0;
        this.mSet = set;
        this.mGridIsReady = false;
        initCards();
    }

    public void resetGridLayout(int numRows, int numCols) {
        layoutCards(getMeasuredWidth(), getMeasuredHeight());
        invalidate();
    }

    public void flyInCards() {
        this.mGridIsReady = false;
        post(new Runnable() {
            public void run() {
                for (int rowIdx = 0; rowIdx < Grid.this.mNumRows; rowIdx++) {
                    for (int colIdx = 0; colIdx < Grid.this.mNumColumns; colIdx++) {
                        Grid.this.startFlyAnimationOnCard(Grid.this.mCards[rowIdx][colIdx]);
                    }
                }
                Grid.this.postDelayed(new Runnable() {
                    public void run() {
                        Grid.this.animatedFlip();
                    }
                }, 1000);
                Grid grid = Grid.this;
                final Grid grid2 = this;
                grid.postDelayed(new Runnable() {
                    public void run() {
                        Grid.this.mGridIsReady = true;
                        if (Grid.this.mOnGridIsReadyListener != null) {
                            Grid.this.mOnGridIsReadyListener.onGridIsReady(grid2);
                        }
                    }
                }, (long) ((((Grid.this.mNumColumns * Grid.this.mNumRows) * 200) / 2) + 1500));
            }
        });
    }

    /* access modifiers changed from: protected */
    public void animatedFlip() {
        int numCards = this.mNumRows * this.mNumColumns;
        for (int i = 0; i < numCards / 2; i++) {
            final Card card1 = getCardByIndex(i);
            final Card card2 = getCardByIndex((numCards - 1) - i);
            card1.postDelayed(new Runnable() {
                public void run() {
                    card1.forceFlip();
                    card2.forceFlip();
                }
            }, (long) (i * 200));
        }
    }

    /* access modifiers changed from: protected */
    public void shuffle(int times) {
        this.mShuffleTimes = times;
        doShuffle();
    }

    /* access modifiers changed from: protected */
    public void doShuffle() {
        if (this.mGridIsReady) {
            this.mGridIsReady = false;
            int numCards = this.mNumRows * this.mNumColumns;
            ArrayList<Integer> switchBuffer = new ArrayList<>(numCards);
            for (int i = 0; i < numCards; i++) {
                switchBuffer.add(Integer.valueOf(i));
            }
            Random randomizer = new Random();
            int i2 = 0;
            while (true) {
                try {
                    int bufferIndex = randomizer.nextInt(switchBuffer.size());
                    Card card1 = getCardByIndex(((Integer) switchBuffer.get(bufferIndex)).intValue());
                    switchBuffer.remove(bufferIndex);
                    int bufferIndex2 = randomizer.nextInt(switchBuffer.size());
                    Card card2 = getCardByIndex(switchBuffer.get(bufferIndex2).intValue());
                    switchBuffer.remove(bufferIndex2);
                    i2++;
                    switchCards(card1, card2, i2 * 100);
                } catch (IllegalArgumentException e) {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void onCardShuffled(Card card) {
        if (this.mLastShuffledCard == card) {
            this.mGridIsReady = true;
            int i = this.mShuffleTimes - 1;
            this.mShuffleTimes = i;
            if (i > 0) {
                postDelayed(new Runnable() {
                    public void run() {
                        Grid.this.doShuffle();
                    }
                }, 200);
            } else if (this.mOnGridIsReadyListener != null) {
                this.mOnGridIsReadyListener.onGridIsShuffled(this);
            }
        }
    }

    private Card getCardByIndex(int index) {
        int row = index / this.mNumColumns;
        return this.mCards[row][index % this.mNumColumns];
    }

    private void switchCards(Card card1, Card card2, int delay) {
        int card1X = card1.getLeft();
        int card1Y = card1.getTop();
        int card2X = card2.getLeft();
        int card2Y = card2.getTop();
        int dx = card2X - card1X;
        int dy = card2Y - card1Y;
        TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, (float) dx, 0, 0.0f, 0, (float) dy);
        translateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        translateAnimation.setDuration((long) 200);
        translateAnimation.setStartOffset((long) delay);
        final Card card = card1;
        final int i = card2X;
        final int i2 = card2Y;
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                Grid.this.mLastShuffledCard = card;
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                card.setPosition(i, i2);
                card.clearAnimation();
                Grid.this.onCardShuffled(card);
            }
        });
        TranslateAnimation translateAnimation2 = new TranslateAnimation(0, 0.0f, 0, (float) (dx * -1), 0, 0.0f, 0, (float) (dy * -1));
        translateAnimation2.setInterpolator(new AccelerateDecelerateInterpolator());
        translateAnimation2.setDuration((long) 200);
        translateAnimation2.setStartOffset((long) delay);
        final Card card3 = card2;
        final int i3 = card1X;
        final int i4 = card1Y;
        translateAnimation2.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                Grid.this.mLastShuffledCard = card3;
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                card3.setPosition(i3, i4);
                card3.clearAnimation();
                Grid.this.onCardShuffled(card3);
            }
        });
        card1.bringToFront();
        card1.startAnimation(translateAnimation);
        card2.bringToFront();
        card2.startAnimation(translateAnimation2);
    }

    public int getNumRows() {
        return this.mNumRows;
    }

    public int getNumColumns() {
        return this.mNumColumns;
    }

    /* access modifiers changed from: protected */
    public void initCards() {
        ArrayList<Integer> usedCards = new ArrayList<>();
        this.mCards = (Card[][]) Array.newInstance(Card.class, this.mNumRows, this.mNumColumns);
        Random randomizer = new Random();
        this.mSet.load();
        int numCards = this.mSet.getNumCards();
        for (int rowIdx = 0; rowIdx < this.mNumRows; rowIdx++) {
            Card[] row = new Card[this.mNumColumns];
            int colIdx = 0;
            while (colIdx < this.mNumColumns) {
                int index = randomizer.nextInt(numCards);
                while (usedCards.contains(Integer.valueOf(index))) {
                    index = randomizer.nextInt(numCards);
                }
                Card card = this.mSet.getCard(index);
                row[colIdx] = card;
                addView(card);
                usedCards.add(Integer.valueOf(index));
                Card counterPart = card.getCounterPart();
                int colIdx2 = colIdx + 1;
                row[colIdx2] = counterPart;
                addView(counterPart);
                usedCards.add(Integer.valueOf(this.mSet.indexOf(counterPart)));
                colIdx = colIdx2 + 1;
            }
            this.mCards[rowIdx] = row;
        }
    }

    /* access modifiers changed from: protected */
    public void resetCards() {
        for (int rowIdx = 0; rowIdx < this.mNumRows; rowIdx++) {
            for (int colIdx = 0; colIdx < this.mNumColumns; colIdx++) {
                this.mCards[rowIdx][colIdx].unfreeze();
            }
        }
        postDelayed(new Runnable() {
            public void run() {
                Grid.this.animatedFlip();
            }
        }, 1000);
        postDelayed(new Runnable() {
            public void run() {
                Grid.this.mGridIsReady = true;
                if (Grid.this.mOnGridIsReadyListener != null) {
                    Grid.this.mOnGridIsReadyListener.onGridIsReady(this);
                }
            }
        }, (long) ((((this.mNumColumns * this.mNumRows) * 200) / 2) + 1500));
    }

    /* access modifiers changed from: protected */
    public int[] layoutCards(int availWidth, int availHeight) {
        int maxSize;
        int numSections;
        if (availWidth < availHeight) {
            maxSize = availWidth;
            numSections = this.mNumColumns;
        } else {
            maxSize = availHeight;
            numSections = this.mNumRows;
        }
        this.mCardSize = (int) Math.round((((double) maxSize) * 0.95d) / ((double) numSections));
        this.mCardSpacing = (int) Math.round(((double) (maxSize - (this.mCardSize * numSections))) / ((double) (numSections + 1)));
        Random randomizer = new Random();
        for (int rowIdx = 0; rowIdx < this.mNumRows; rowIdx++) {
            for (int colIdx = 0; colIdx < this.mNumColumns; colIdx++) {
                LayoutParams layoutParams = new LayoutParams(this.mCardSize, this.mCardSize, getXPos(colIdx), getYPos(rowIdx));
                Card card = this.mCards[rowIdx][colIdx];
                card.setLayoutParams(layoutParams);
                card.setRotation(randomizer.nextInt(5) * (randomizer.nextBoolean() ? -1 : 1));
            }
        }
        return new int[]{(this.mNumColumns * (this.mCardSize + this.mCardSpacing)) + this.mCardSpacing, (this.mNumRows * (this.mCardSize + this.mCardSpacing)) + this.mCardSpacing};
    }

    /* access modifiers changed from: protected */
    public int getXPos(int column) {
        return (this.mCardSize * column) + (this.mCardSpacing * column) + this.mCardSpacing;
    }

    /* access modifiers changed from: protected */
    public int getYPos(int row) {
        return (this.mCardSize * row) + (this.mCardSpacing * row) + this.mCardSpacing;
    }

    /* access modifiers changed from: protected */
    public void startFlyAnimationOnCard(Card card) {
        int fromX;
        int fromY;
        LayoutParams lp = (LayoutParams) card.getLayoutParams();
        Random random = new Random();
        if (lp.x < getMeasuredWidth() / 2) {
            fromX = (lp.width * -1) - (lp.width * 2);
        } else {
            fromX = lp.x;
        }
        if (lp.y < getMeasuredHeight() / 2) {
            fromY = (lp.height * -1) - (lp.height * 2);
        } else {
            fromY = lp.y;
        }
        int delay = random.nextInt(500);
        Animation a = new TranslateAnimation((float) fromX, 0.0f, (float) fromY, 0.0f);
        a.initialize(lp.width, lp.height, getMeasuredWidth(), getMeasuredHeight());
        a.setInterpolator(new DecelerateInterpolator());
        a.setDuration((long) 500);
        a.setStartTime(-1);
        a.setStartOffset((long) delay);
        a.setFillAfter(true);
        a.setFillEnabled(true);
        a.setZAdjustment(1);
        card.startAnimation(a);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != 8) {
                LayoutParams lp = (LayoutParams) child.getLayoutParams();
                int childLeft = lp.x;
                int childTop = lp.y;
                child.layout(childLeft, childTop, childLeft + child.getMeasuredWidth(), childTop + child.getMeasuredHeight());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width;
        int height;
        if (this.mCardSize > 0) {
            width = getMeasuredWidth();
            height = getMeasuredHeight();
        } else {
            int allowedWidth = View.MeasureSpec.getSize(widthMeasureSpec);
            int allowedHeight = View.MeasureSpec.getSize(heightMeasureSpec);
            int[] usedSpace = layoutCards(allowedWidth, allowedHeight);
            measureChildren(View.MeasureSpec.makeMeasureSpec(usedSpace[0], 1073741824), View.MeasureSpec.makeMeasureSpec(usedSpace[1], 1073741824));
            width = usedSpace[0];
            height = Math.max(usedSpace[1], allowedHeight);
        }
        setMeasuredDimension(width, height);
    }

    public static class LayoutParams extends ViewGroup.LayoutParams {
        public int x;
        public int y;

        public LayoutParams(int width, int height, int x2, int y2) {
            super(width, height);
            this.x = x2;
            this.y = y2;
        }

        public LayoutParams(ViewGroup.LayoutParams source) {
            super(source);
        }
    }
}
