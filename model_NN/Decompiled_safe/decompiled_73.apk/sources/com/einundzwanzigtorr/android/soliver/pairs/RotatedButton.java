package com.einundzwanzigtorr.android.soliver.pairs;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.Button;

public class RotatedButton extends Button {
    public RotatedButton(Context context) {
        super(context);
    }

    public RotatedButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RotatedButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        canvas.save();
        canvas.rotate(30.0f);
        super.onDraw(canvas);
        canvas.restore();
    }
}
