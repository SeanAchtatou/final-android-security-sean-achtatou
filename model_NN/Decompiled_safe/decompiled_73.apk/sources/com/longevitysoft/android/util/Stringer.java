package com.longevitysoft.android.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class Stringer {
    private StringBuilder builder;

    public Stringer() {
        this.builder = new StringBuilder();
    }

    public Stringer(String val) {
        this.builder = new StringBuilder(val);
    }

    public StringBuilder newBuilder() {
        this.builder.setLength(0);
        return this.builder;
    }

    public StringBuilder getBuilder() {
        return this.builder;
    }

    public static Stringer convert(InputStream is) throws IOException {
        Stringer ret = new Stringer();
        if (is != null) {
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                while (true) {
                    int n = reader.read(buffer);
                    if (n == -1) {
                        break;
                    }
                    ret.getBuilder().append(buffer, 0, n);
                }
            } finally {
                is.close();
            }
        }
        return ret;
    }
}
