package com.longevitysoft.android.xml.plist.domain;

public class False extends PListObject implements IPListSimpleObject<Boolean> {
    private static final long serialVersionUID = -8533886020773567552L;

    public False() {
        setType(PListObjectType.FALSE);
    }

    public Boolean getValue() {
        return new Boolean(false);
    }

    public void setValue(Boolean val) {
    }

    public void setValue(String val) {
    }
}
