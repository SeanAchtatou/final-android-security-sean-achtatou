package com.longevitysoft.android.xml.plist;

import com.longevitysoft.android.util.Stringer;
import java.io.IOException;
import java.io.InputStream;

public class PListXMLParser extends BaseXMLParser {
    public static final String TAG = "PListXMLParser";

    public void parse(String xml) throws IllegalStateException {
        PListXMLHandler pListHandler = (PListXMLHandler) getHandler();
        if (pListHandler == null) {
            throw new IllegalStateException("handler is null, must set a document handler before calling parse");
        } else if (xml == null) {
            pListHandler.setPlist(null);
        } else {
            initParser();
            super.parse(xml);
        }
    }

    public void parse(InputStream is) throws IllegalStateException, IOException {
        PListXMLHandler pListHandler = (PListXMLHandler) getHandler();
        if (pListHandler == null) {
            throw new IllegalStateException("handler is null, must set a document handler before calling parse");
        } else if (is == null) {
            pListHandler.setPlist(null);
        } else {
            try {
                Stringer xml = Stringer.convert(is);
                initParser();
                super.parse(xml.getBuilder().toString());
            } catch (IOException e) {
                throw new IOException("error reading from input string - is it encoded as UTF-8?");
            }
        }
    }
}
