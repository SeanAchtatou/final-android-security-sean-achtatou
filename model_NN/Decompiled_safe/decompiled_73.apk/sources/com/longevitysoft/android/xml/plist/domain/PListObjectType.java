package com.longevitysoft.android.xml.plist.domain;

public enum PListObjectType {
    ARRAY(0),
    DATA(1),
    DATE(2),
    DICT(3),
    REAL(4),
    INTEGER(5),
    STRING(6),
    TRUE(7),
    FALSE(8);
    
    private int type;

    private PListObjectType(int type2) {
        this.type = type2;
    }

    public int getType() {
        return this.type;
    }
}
