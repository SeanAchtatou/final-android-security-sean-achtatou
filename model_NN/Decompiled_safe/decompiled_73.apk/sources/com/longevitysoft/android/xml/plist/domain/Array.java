package com.longevitysoft.android.xml.plist.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Array extends PListObject implements List<PListObject> {
    private static final long serialVersionUID = -2673110114913406413L;
    private ArrayList<PListObject> data;

    public Array() {
        setType(PListObjectType.ARRAY);
        this.data = new ArrayList<>();
    }

    public Array(Collection<? extends PListObject> collection) {
        setType(PListObjectType.ARRAY);
        this.data = new ArrayList<>(collection);
    }

    public Array(int capacity) {
        setType(PListObjectType.ARRAY);
        this.data = new ArrayList<>(capacity);
    }

    public void add(int arg0, PListObject arg1) {
        this.data.add(arg0, arg1);
    }

    public boolean add(PListObject arg0) {
        return this.data.add(arg0);
    }

    public boolean addAll(Collection<? extends PListObject> arg0) {
        return this.data.addAll(arg0);
    }

    public boolean addAll(int arg0, Collection<? extends PListObject> arg1) {
        return this.data.addAll(arg0, arg1);
    }

    public boolean isEmpty() {
        return this.data.isEmpty();
    }

    public int lastIndexOf(Object arg0) {
        return this.data.indexOf(arg0);
    }

    public ListIterator<PListObject> listIterator() {
        return this.data.listIterator();
    }

    public ListIterator<PListObject> listIterator(int arg0) {
        return this.data.listIterator(arg0);
    }

    public PListObject remove(int arg0) {
        return this.data.remove(arg0);
    }

    public boolean remove(Object arg0) {
        return this.data.remove(arg0);
    }

    public boolean removeAll(Collection<?> arg0) {
        return this.data.remove(arg0);
    }

    public boolean retainAll(Collection<?> arg0) {
        return this.data.retainAll(arg0);
    }

    public PListObject set(int arg0, PListObject arg1) {
        return this.data.set(arg0, arg1);
    }

    public List<PListObject> subList(int arg0, int arg1) {
        return this.data.subList(arg0, arg1);
    }

    public Object[] toArray() {
        return this.data.toArray();
    }

    public Object[] toArray(Object[] array) {
        return this.data.toArray(array);
    }

    public void clear() {
        this.data.clear();
    }

    public Object clone() {
        return this.data.clone();
    }

    public boolean contains(Object obj) {
        return this.data.contains(obj);
    }

    public boolean containsAll(Collection arg0) {
        return this.data.contains(arg0);
    }

    public boolean equals(Object that) {
        return this.data.equals(that);
    }

    public PListObject get(int index) {
        return this.data.get(index);
    }

    public int indexOf(Object object) {
        return this.data.indexOf(object);
    }

    public Iterator<PListObject> iterator() {
        return this.data.iterator();
    }

    public int size() {
        return this.data.size();
    }
}
