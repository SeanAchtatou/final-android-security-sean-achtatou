package com.longevitysoft.android.xml.plist.domain;

import com.longevitysoft.android.util.Stringer;
import net.sf.migbase64.Base64;

public class Data extends PListObject implements IPListSimpleObject<String> {
    private static final long serialVersionUID = -3101592260075687323L;
    protected Stringer dataStringer = new Stringer();
    protected byte[] rawData;

    public Data() {
        setType(PListObjectType.DATA);
    }

    public String getValue() {
        return getValue(true);
    }

    public String getValue(boolean decode) {
        this.dataStringer.newBuilder();
        if (decode) {
            return this.dataStringer.getBuilder().append(new String(Base64.decodeFast(this.rawData))).toString();
        }
        return this.dataStringer.getBuilder().append(this.rawData).toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.longevitysoft.android.xml.plist.domain.Data.setValue(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.longevitysoft.android.xml.plist.domain.Data.setValue(byte[], boolean):void
      com.longevitysoft.android.xml.plist.domain.Data.setValue(java.lang.String, boolean):void */
    public void setValue(String val) {
        setValue(val, true);
    }

    public void setValue(String val, boolean encoded) {
        if (!encoded) {
            this.rawData = Base64.encodeToByte(val.getBytes(), false);
        } else {
            this.rawData = val.getBytes();
        }
    }

    public void setValue(byte[] val, boolean encoded) {
        if (!encoded) {
            this.rawData = Base64.encodeToByte(val, false);
        } else {
            this.rawData = val;
        }
    }
}
