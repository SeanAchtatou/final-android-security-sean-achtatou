package com.longevitysoft.android.xml.plist.domain;

import java.io.Serializable;

public class PListObject implements Cloneable, Serializable {
    private static final long serialVersionUID = -5258056855425643835L;
    private PListObjectType type;

    public PListObjectType getType() {
        return this.type;
    }

    public void setType(PListObjectType type2) {
        this.type = type2;
    }
}
