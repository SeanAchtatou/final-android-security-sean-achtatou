package com.longevitysoft.android.xml.plist.domain;

import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class Dict extends PListObject {
    public static final String DOT = ".";
    private static final long serialVersionUID = -556589348083152733L;
    protected Map<String, PListObject> configMap = new TreeMap();

    public Dict() {
        setType(PListObjectType.DICT);
    }

    public void putConfig(String key, PListObject value) {
        this.configMap.put(key, value);
    }

    public Map<String, PListObject> getConfigMap() {
        return this.configMap;
    }

    public void setConfigMap(Map<String, PListObject> configMap2) {
        this.configMap = configMap2;
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    public <E extends PListObject> E getConfigurationObject(String key) {
        StringTokenizer st = new StringTokenizer(key, DOT);
        if (st.hasMoreTokens()) {
            Map<String, PListObject> dict = this.configMap;
            while (st.hasMoreTokens()) {
                Object obj = dict.get(st.nextToken());
                if (!(obj instanceof TreeMap)) {
                    return (PListObject) obj;
                }
                dict = (Map) obj;
            }
        }
        return (PListObject) this.configMap.get(key);
    }

    public String getConfiguration(String key) {
        return (String) getConfigurationObject(key);
    }

    public String getConfigurationWithDefault(String key, String defaultValue) {
        String value = getConfiguration(key);
        if (value == null) {
            return defaultValue;
        }
        return value;
    }

    public Integer getConfigurationInteger(String key) {
        return (Integer) getConfigurationObject(key);
    }

    public Integer getConfigurationIntegerWithDefault(String key, Integer defaultValue) {
        Integer value = getConfigurationInteger(key);
        if (value == null) {
            return defaultValue;
        }
        return value;
    }

    public Array getConfigurationArray(String key) {
        return (Array) getConfigurationObject(key);
    }

    public String toString() {
        StringBuilder retVal = new StringBuilder();
        for (String key : this.configMap.keySet()) {
            retVal.append("key=").append(key).append(this.configMap.get(key).toString());
        }
        return retVal.toString();
    }
}
