package com.longevitysoft.android.xml.plist.domain;

public class Date extends PListObject implements IPListSimpleObject<java.util.Date> {
    private static final long serialVersionUID = 3846688440069431376L;
    protected java.util.Date date;

    public Date() {
        setType(PListObjectType.DATE);
    }

    public java.util.Date getValue() {
        return this.date;
    }

    public void setValue(java.util.Date val) {
        this.date = val;
    }

    public void setValue(String val) {
        this.date = new java.util.Date(java.util.Date.parse(val.trim()));
    }
}
