package com.longevitysoft.android.xml.plist.domain;

public class Real extends PListObject implements IPListSimpleObject<Float> {
    private static final long serialVersionUID = -4204214862534504729L;
    protected Float real;

    public Real() {
        setType(PListObjectType.REAL);
    }

    public Float getValue() {
        return this.real;
    }

    public void setValue(Float val) {
        this.real = val;
    }

    public void setValue(String val) {
        this.real = new Float(Float.parseFloat(val.trim()));
    }
}
