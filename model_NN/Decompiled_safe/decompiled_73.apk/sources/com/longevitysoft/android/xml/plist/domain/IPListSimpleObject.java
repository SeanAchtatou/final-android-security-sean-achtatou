package com.longevitysoft.android.xml.plist.domain;

public interface IPListSimpleObject<E> {
    E getValue();

    void setValue(Object obj);

    void setValue(String str);
}
