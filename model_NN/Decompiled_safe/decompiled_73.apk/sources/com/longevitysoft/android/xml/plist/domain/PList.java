package com.longevitysoft.android.xml.plist.domain;

import android.util.Log;
import com.longevitysoft.android.util.Stringer;
import com.longevitysoft.android.xml.plist.Constants;
import java.util.Stack;

public class PList {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$longevitysoft$android$xml$plist$domain$PListObjectType = null;
    public static final String TAG = "PList";
    private PListObject root;
    private Stack<PListObject> stack = new Stack<>();
    private boolean stackCtxInArray = false;
    private boolean stackCtxInDict = false;
    private int stackCtxNestedDepth = 0;
    private Stringer stringer = new Stringer();

    static /* synthetic */ int[] $SWITCH_TABLE$com$longevitysoft$android$xml$plist$domain$PListObjectType() {
        int[] iArr = $SWITCH_TABLE$com$longevitysoft$android$xml$plist$domain$PListObjectType;
        if (iArr == null) {
            iArr = new int[PListObjectType.values().length];
            try {
                iArr[PListObjectType.ARRAY.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[PListObjectType.DATA.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[PListObjectType.DATE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[PListObjectType.DICT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[PListObjectType.FALSE.ordinal()] = 9;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[PListObjectType.INTEGER.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[PListObjectType.REAL.ordinal()] = 5;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[PListObjectType.STRING.ordinal()] = 7;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[PListObjectType.TRUE.ordinal()] = 8;
            } catch (NoSuchFieldError e9) {
            }
            $SWITCH_TABLE$com$longevitysoft$android$xml$plist$domain$PListObjectType = iArr;
        }
        return iArr;
    }

    public PListObject getRootElement() {
        return this.root;
    }

    public void setRootElement(PListObject root2) {
        this.root = root2;
    }

    private void attachPListObjToParent(PListObject obj, String key) {
        if (this.stackCtxInArray) {
            attachPListObjToArrayParent(this.stack, obj);
        } else if (this.stackCtxInDict) {
            attachPListObjToDictParent(obj, key);
        } else if (this.stackCtxNestedDepth == 0) {
            setRootElement(obj);
        }
    }

    private void attachPListObjToDictParent(PListObject obj, String key) {
        Log.v(this.stringer.newBuilder().append(TAG).append("#attachPListObjToDictParent").toString(), this.stringer.newBuilder().append("key|obj-type|obj: ").append(key).append(Constants.PIPE).append(obj.getType()).append(Constants.PIPE).append(obj.toString()).append(Constants.PIPE).toString());
        Dict parent = (Dict) this.stack.pop();
        parent.putConfig(key, obj);
        this.stack.push(parent);
    }

    private void attachPListObjToArrayParent(Stack<PListObject> stack2, PListObject obj) {
        Log.v(this.stringer.newBuilder().append(TAG).append("#attachPListObjToArrayParent").toString(), this.stringer.newBuilder().append("obj-type|obj: ").append(Constants.PIPE).append(obj.getType()).append(Constants.PIPE).append(obj.toString()).append(Constants.PIPE).toString());
        Array parent = (Array) stack2.pop();
        parent.add(obj);
        stack2.push(parent);
    }

    public void stackObject(PListObject obj, String key) throws Exception {
        if (key == null && this.stackCtxInDict) {
            throw new Exception("PList objects with Dict parents require a key.");
        } else if (this.stackCtxNestedDepth <= 0 || this.stackCtxInDict || this.stackCtxInArray) {
            switch ($SWITCH_TABLE$com$longevitysoft$android$xml$plist$domain$PListObjectType()[obj.getType().ordinal()]) {
                case 1:
                    attachPListObjToParent(obj, key);
                    this.stack.push(obj);
                    this.stackCtxInArray = true;
                    this.stackCtxInDict = false;
                    this.stackCtxNestedDepth++;
                    return;
                case 2:
                case 3:
                default:
                    attachPListObjToParent(obj, key);
                    return;
                case 4:
                    attachPListObjToParent(obj, key);
                    this.stack.push(obj);
                    this.stackCtxInArray = false;
                    this.stackCtxInDict = true;
                    this.stackCtxNestedDepth++;
                    return;
            }
        } else {
            throw new Exception("PList elements that are not at the root should have an Array or Dict parent.");
        }
    }

    public PListObject popStack() {
        if (this.stack.isEmpty()) {
            return null;
        }
        PListObject ret = this.stack.pop();
        this.stackCtxNestedDepth--;
        if (!this.stack.isEmpty()) {
            switch ($SWITCH_TABLE$com$longevitysoft$android$xml$plist$domain$PListObjectType()[this.stack.lastElement().getType().ordinal()]) {
                case 1:
                    this.stackCtxInArray = true;
                    this.stackCtxInDict = false;
                    break;
                case 4:
                    this.stackCtxInArray = false;
                    this.stackCtxInDict = true;
                    break;
            }
        } else {
            this.stackCtxInArray = false;
            this.stackCtxInDict = false;
        }
        return ret;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.longevitysoft.android.xml.plist.domain.Data.setValue(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.longevitysoft.android.xml.plist.domain.Data.setValue(byte[], boolean):void
      com.longevitysoft.android.xml.plist.domain.Data.setValue(java.lang.String, boolean):void */
    public PListObject buildObject(String tag, String value) throws Exception {
        if (tag == null) {
            throw new Exception("Cannot add a child with a null tag to a PList.");
        } else if (tag.equalsIgnoreCase(Constants.TAG_INTEGER)) {
            Integer integer = new Integer();
            integer.setValue(value);
            return integer;
        } else if (tag.equalsIgnoreCase(Constants.TAG_STRING)) {
            String string = new String();
            string.setValue(value);
            return string;
        } else if (tag.equalsIgnoreCase(Constants.TAG_REAL)) {
            Real real = new Real();
            real.setValue(value);
            return real;
        } else if (tag.equalsIgnoreCase(Constants.TAG_DATE)) {
            Date date = new Date();
            date.setValue(value);
            return date;
        } else if (tag.equalsIgnoreCase(Constants.TAG_BOOL_FALSE)) {
            return new False();
        } else {
            if (tag.equalsIgnoreCase(Constants.TAG_BOOL_TRUE)) {
                return new True();
            }
            if (tag.equalsIgnoreCase(Constants.TAG_DATA)) {
                Data data = new Data();
                data.setValue(value.trim(), true);
                return data;
            } else if (tag.equalsIgnoreCase(Constants.TAG_DICT)) {
                return new Dict();
            } else {
                if (tag.equalsIgnoreCase(Constants.TAG_PLIST_ARRAY)) {
                    return new Array();
                }
                return null;
            }
        }
    }

    public String toString() {
        if (this.root == null) {
            return null;
        }
        return this.root.toString();
    }
}
