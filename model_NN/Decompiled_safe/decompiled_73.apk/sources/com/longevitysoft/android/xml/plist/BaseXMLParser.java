package com.longevitysoft.android.xml.plist;

import android.util.Log;
import com.longevitysoft.android.util.Stringer;
import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public abstract class BaseXMLParser {
    public static final String TAG = "BaseXMLParser";
    private DefaultHandler handler;
    protected SAXParser sp;
    protected SAXParserFactory spf;
    protected Stringer stringer = new Stringer();

    public DefaultHandler getHandler() {
        return this.handler;
    }

    public void setHandler(DefaultHandler handler2) {
        this.handler = handler2;
    }

    public void initParser() {
        if (this.spf == null) {
            this.spf = SAXParserFactory.newInstance();
        }
        try {
            this.sp = this.spf.newSAXParser();
        } catch (ParserConfigurationException e) {
            Log.e(this.stringer.newBuilder().append(TAG).append("#parse").toString(), "ParserConfigurationException");
            e.printStackTrace();
        } catch (SAXException e2) {
            Log.e(this.stringer.newBuilder().append(TAG).append("#parse").toString(), "SAXException");
            e2.printStackTrace();
        }
    }

    public void parse(String xml) throws IllegalStateException {
        try {
            this.sp.parse(new InputSource(new StringReader(xml)), getHandler());
        } catch (SAXException e) {
            Log.e(this.stringer.newBuilder().append(TAG).append("#parse").toString(), "SAXException");
            e.printStackTrace();
        } catch (IOException e2) {
            Log.e(this.stringer.newBuilder().append(TAG).append("#parse").toString(), "IOException");
            e2.printStackTrace();
        }
        Log.v(this.stringer.newBuilder().append(TAG).append("#parse").toString(), "done parsing xml");
    }
}
