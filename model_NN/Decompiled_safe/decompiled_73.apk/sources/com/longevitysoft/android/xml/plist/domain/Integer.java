package com.longevitysoft.android.xml.plist.domain;

public class Integer extends PListObject implements IPListSimpleObject<Integer> {
    private static final long serialVersionUID = -5952071046933925529L;
    protected Integer intgr;

    public Integer() {
        setType(PListObjectType.INTEGER);
    }

    public Integer getValue() {
        return this.intgr;
    }

    public void setValue(Integer val) {
        this.intgr = val;
    }

    public void setValue(String val) {
        this.intgr = new Integer(Integer.parseInt(val.trim()));
    }
}
