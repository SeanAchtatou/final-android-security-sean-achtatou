package com.longevitysoft.android.xml.plist;

import android.util.Log;
import com.longevitysoft.android.util.Stringer;
import com.longevitysoft.android.xml.plist.domain.PList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

public class PListXMLHandler extends DefaultHandler2 {
    public static final String TAG = "PListXMLHandler";
    protected String key;
    private PList pList;
    private PListParserListener parseListener;
    private Stringer stringer = new Stringer();
    private Stringer tempVal;

    public interface PListParserListener {
        void onPListParseDone(PList pList, ParseMode parseMode);
    }

    public enum ParseMode {
        START_TAG,
        END_TAG
    }

    public PList getPlist() {
        return this.pList;
    }

    public void setPlist(PList plist) {
        this.pList = plist;
    }

    public PListParserListener getParseListener() {
        return this.parseListener;
    }

    public void setParseListener(PListParserListener parseListener2) {
        this.parseListener = parseListener2;
    }

    public Stringer getTempVal() {
        return this.tempVal;
    }

    public void setTempVal(Stringer tempVal2) {
        this.tempVal = tempVal2;
    }

    public void startDocument() throws SAXException {
        super.startDocument();
        this.tempVal = new Stringer();
        this.pList = null;
        this.key = null;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        Log.v(this.stringer.newBuilder().append(TAG).append("#startElement").toString(), this.stringer.newBuilder().append("Start Element lname|uri|attr.length :").append(localName).append(Constants.PIPE).append(uri).append(Constants.PIPE).append(attributes.getLength()).toString());
        this.tempVal.newBuilder();
        if (localName.equalsIgnoreCase(Constants.TAG_PLIST)) {
            if (this.pList != null) {
                throw new SAXException("there should only be one PList element in PList XML");
            }
            this.pList = new PList();
        } else if (this.pList == null) {
            throw new SAXException("invalid PList - please see http://www.apple.com/DTDs/PropertyList-1.0.dtd");
        } else if (localName.equalsIgnoreCase(Constants.TAG_DICT) || localName.equalsIgnoreCase(Constants.TAG_PLIST_ARRAY)) {
            try {
                this.pList.stackObject(this.pList.buildObject(localName, this.tempVal.getBuilder().toString()), this.key);
            } catch (Exception e) {
                throw new SAXException(e);
            }
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        Log.v(this.stringer.newBuilder().append(TAG).append("#characters").toString(), this.stringer.newBuilder().append(ch).append(Constants.PIPE).append(start).append(Constants.PIPE).append(length).append(Constants.PIPE).toString());
        this.tempVal.getBuilder().append(new String(ch, start, length));
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        Log.v(this.stringer.newBuilder().append(TAG).append("#endElement").toString(), this.stringer.newBuilder().append("localName|qName|uri|tempVal: ").append(localName).append(Constants.PIPE).append(qName).append(Constants.PIPE).append(uri).append(Constants.PIPE).append(this.tempVal.getBuilder().toString()).toString());
        if (localName.equalsIgnoreCase(Constants.TAG_KEY)) {
            this.key = this.tempVal.getBuilder().toString().trim();
        } else if (localName.equalsIgnoreCase(Constants.TAG_DICT) || localName.equalsIgnoreCase(Constants.TAG_PLIST_ARRAY)) {
            this.pList.popStack();
        } else if (!localName.equalsIgnoreCase(Constants.TAG_PLIST)) {
            try {
                this.pList.stackObject(this.pList.buildObject(localName, this.tempVal.getBuilder().toString()), this.key);
                this.key = null;
            } catch (Exception e) {
                throw new SAXException(e);
            }
        } else if (localName.equalsIgnoreCase(Constants.TAG_PLIST) && this.parseListener != null) {
            this.parseListener.onPListParseDone(this.pList, ParseMode.END_TAG);
        }
        this.tempVal.newBuilder();
    }
}
