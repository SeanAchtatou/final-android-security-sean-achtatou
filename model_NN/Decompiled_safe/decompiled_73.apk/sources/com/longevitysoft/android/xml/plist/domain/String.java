package com.longevitysoft.android.xml.plist.domain;

import com.longevitysoft.android.util.Stringer;

public class String extends PListObject implements IPListSimpleObject<String> {
    private static final long serialVersionUID = -8134261357175236382L;
    protected Stringer str = new Stringer();

    public String() {
        setType(PListObjectType.STRING);
    }

    public String getValue() {
        return this.str.getBuilder().toString();
    }

    public void setValue(String val) {
        this.str.newBuilder().append(val);
    }
}
