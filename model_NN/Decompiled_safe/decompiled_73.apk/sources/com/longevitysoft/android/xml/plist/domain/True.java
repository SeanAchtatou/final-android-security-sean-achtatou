package com.longevitysoft.android.xml.plist.domain;

public class True extends PListObject implements IPListSimpleObject<Boolean> {
    private static final long serialVersionUID = -3560354198720649001L;

    public True() {
        setType(PListObjectType.TRUE);
    }

    public Boolean getValue() {
        return new Boolean(true);
    }

    public void setValue(Boolean val) {
    }

    public void setValue(String val) {
    }
}
