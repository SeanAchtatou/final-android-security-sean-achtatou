package com.facebook.android;

public final class R {

    public static final class anim {
        public static final int appear = 2130968576;
        public static final int dissolve = 2130968577;
        public static final int grow_from_middle = 2130968578;
        public static final int move_in = 2130968579;
        public static final int move_out = 2130968580;
        public static final int ribbon_fall_down = 2130968581;
        public static final int ribbon_wind_up = 2130968582;
        public static final int shrink_to_middle = 2130968583;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131230721;
        public static final int buttonTextColor = 2131230723;
        public static final int transparent = 2131230722;
        public static final int white = 2131230720;
    }

    public static final class drawable {
        public static final int backbutton = 2130837504;
        public static final int backbutton_normal = 2130837505;
        public static final int backbutton_pressed = 2130837506;
        public static final int background_plain = 2130837507;
        public static final int background_titlebar = 2130837508;
        public static final int dropshadow = 2130837509;
        public static final int facebook_icon = 2130837510;
        public static final int fb_leatherbutton = 2130837511;
        public static final int fb_leatherbutton_normal = 2130837512;
        public static final int fb_leatherbutton_pressed = 2130837513;
        public static final int icon = 2130837514;
        public static final int leatherbutton = 2130837515;
        public static final int leatherbutton_normal = 2130837516;
        public static final int leatherbutton_pressed = 2130837517;
        public static final int pausebutton = 2130837518;
        public static final int pausebutton_normal = 2130837519;
        public static final int pausebutton_pressed = 2130837520;
        public static final int ribbon = 2130837521;
        public static final int soliver_logo = 2130837522;
    }

    public static final class id {
        public static final int Button_Back = 2131361792;
        public static final int Button_OnePlayer = 2131361812;
        public static final int Button_Pause = 2131361794;
        public static final int Button_StartGame = 2131361818;
        public static final int Button_TwoPlayers = 2131361813;
        public static final int Controls = 2131361806;
        public static final int DarkLayer = 2131361799;
        public static final int EditText_NameOfPlayerOne = 2131361815;
        public static final int EditText_NameOfPlayerTwo = 2131361817;
        public static final int FacebookButton = 2131361803;
        public static final int GameTimer = 2131361795;
        public static final int Label_NameOfPlayerOne = 2131361814;
        public static final int Label_NameOfPlayerTwo = 2131361816;
        public static final int Ribbon = 2131361800;
        public static final int RibbonButton = 2131361802;
        public static final int RibbonButton2 = 2131361804;
        public static final int RibbonText = 2131361801;
        public static final int Text_Name = 2131361809;
        public static final int Text_PlayerName = 2131361793;
        public static final int Text_PlayerOneScore = 2131361796;
        public static final int Text_PlayerTwoScore = 2131361797;
        public static final int Text_Pos = 2131361808;
        public static final int Text_Time = 2131361810;
        public static final int Text_Title = 2131361807;
        public static final int chrome = 2131361811;
        public static final int grid = 2131361798;
        public static final int logo = 2131361805;
    }

    public static final class layout {
        public static final int game = 2130903040;
        public static final int highscores = 2130903041;
        public static final int highscores_listitem = 2130903042;
        public static final int main = 2130903043;
        public static final int settings = 2130903044;
        public static final int startup = 2130903045;
    }

    public static final class raw {
        public static final int game_solved = 2131099648;
        public static final int pair_solved = 2131099649;
    }

    public static final class string {
        public static final int AppTitle = 2131165194;
        public static final int BackToMainMenu = 2131165204;
        public static final int Cancel = 2131165213;
        public static final int CheckingForUpdate = 2131165195;
        public static final int Close = 2131165208;
        public static final int DefaultCardSetTitle = 2131165217;
        public static final int DownloadingCards = 2131165197;
        public static final int FacebookMultiPlayerDrawMessage = 2131165228;
        public static final int FacebookMultiPlayerWinMessage = 2131165227;
        public static final int FacebookSinglePlayerWinMessage = 2131165225;
        public static final int FacebookSinglePlayerWinNewHighscoreMessage = 2131165226;
        public static final int GANotice = 2131165230;
        public static final int GANoticeTitle = 2131165229;
        public static final int GAWebPropertyId = 2131165191;
        public static final int GameIsPaused = 2131165214;
        public static final int GameTie = 2131165223;
        public static final int HighScores = 2131165224;
        public static final int NameOfPlayerOne = 2131165209;
        public static final int NameOfPlayerTwo = 2131165210;
        public static final int NetworkIsDown = 2131165196;
        public static final int OnePlayer = 2131165198;
        public static final int PlayAgain = 2131165203;
        public static final int PlayerHasWon = 2131165220;
        public static final int PleaseEnterBothNames = 2131165212;
        public static final int PleaseEnterName = 2131165211;
        public static final int Quit = 2131165207;
        public static final int QuitGame = 2131165215;
        public static final int QuitGameConfirmationText = 2131165216;
        public static final int RestartOrExit = 2131165218;
        public static final int Resume = 2131165205;
        public static final int Settings = 2131165231;
        public static final int ShakeToShuffle = 2131165219;
        public static final int Share = 2131165201;
        public static final int ShowHighScore = 2131165202;
        public static final int Shuffle = 2131165206;
        public static final int SinglePlayerSolved = 2131165221;
        public static final int SinglePlayerSolvedHighScore = 2131165222;
        public static final int StartGame = 2131165200;
        public static final int TwoPlayers = 2131165199;
        public static final int UseGoogleAnalytics = 2131165232;
        public static final int UseGoogleAnalyticsDesc = 2131165233;
        public static final int app_name = 2131165185;
        public static final int cardCollectionBaseURL = 2131165187;
        public static final int cardCollectionPlistFileName = 2131165188;
        public static final int fbAppId = 2131165186;
        public static final int fbShareImage = 2131165190;
        public static final int fbShareUrl = 2131165189;
        public static final int hello = 2131165184;
        public static final int numCols = 2131165193;
        public static final int numRows = 2131165192;
    }

    public static final class style {
        public static final int Chrome = 2131296257;
        public static final int Chrome_Plain = 2131296258;
        public static final int Chrome_Title = 2131296259;
        public static final int ControlButton = 2131296267;
        public static final int FacebookLeatherButton = 2131296261;
        public static final int GameTimer = 2131296262;
        public static final int HighScoresListItem = 2131296270;
        public static final int LeatherButton = 2131296260;
        public static final int PlayerScore = 2131296263;
        public static final int RibbonEditText = 2131296266;
        public static final int RibbonLabel = 2131296265;
        public static final int RibbonText = 2131296264;
        public static final int Theme = 2131296256;
        public static final int TitleBarButton = 2131296268;
        public static final int TitleBarText = 2131296269;
    }

    public static final class xml {
        public static final int settings = 2131034112;
    }
}
