【《高铁时代》使用帮助 】

一、快速入门：
1、主界面
	包括“火车时刻表与票价查询”、“火车票余票查询”、“火车正晚点查询”、“火车票代售点查询”、“更多...”等功能条目，点击对应条目即可进入相应功能。
2、火车火车时刻表与票价查询：
	按“发到站查询”、“车次查询”、“车站查询”类型进行火车时刻表与票价查询。点击对应按钮即可使用对应功能。
	“发到站查询”：查询发站-到站的直达列车信息，如果没有直达列车，将给出中转方案。
	“车次查询”：按车次查询列车信息。
	“车站查询”：查询车站途经的所有列车信息。
	每种查询都有“我的收藏”和“热点线路（车次、车站）”的列表，点击对应列表记录即可快速选择对应的发站、到站（车次、车站），减少录入工作量。
	输入发站、到站（车次、车站）信息后，点击“查询”即可实现查询功能。
	查询后显示结果的列表信息。按“返回”按钮或者手机的返回键退出结果界面回到查询条件输入界面。
3、火车票余票查询
	查询火车余票信息，输入日期和发站与到站（或者车次）后，按查询按钮，将弹出“校验码”录入对话框，按照提示的校验码录入校验码后（校验码录入不区分大小写！）按“确认”即可查询火车余票信息。
	如果按“确认”查询出现“效验码已过期”，“效验码输入错误”，“0次列车有余票”等信息，请按“返回”按钮或者手机的返回键回到查询条件输入界面重新进入查询。
4、火车正晚点查询
  查询某一个车次的正晚点信息。
  本查询提供未来3小时内列车正晚点信息！
  选择“类型”，输入“车站”、“车次”后（“车次”可通过旁边的辅助查询按钮查询得到），按查询即可查询出本车次列车的正晚点信息。（在本页面显示）
5、火车票代售点查询
  查询各个城市的火车票代售点地址等信息。
  特别提醒：一些大城市（如北京上海等）代售点较多，查询花费的流量较大，建议在wifi联网的情况下进行查询。
6、更多...
	我的收藏维护：查看和删除收藏信息。
	意见反馈：对软件向开发商提出意见和建议。
	版本更新：进行版本更新。如果在您使用过程中软件出现问题，请您试试更新最新版本。
	帮助：软件操作快速入门
	关于：软件说明和开发商介绍，开发商其他精品应用介绍
  
	
★★★ 致谢 ★★★
  使用过程中有任何问题和建议欢迎您通过“意见反馈”功能提供给我们，我们将努力完善和改进软件。
  非常感谢您的使用，我们将努力把“高铁时代”打造成您的出行好帮手！
