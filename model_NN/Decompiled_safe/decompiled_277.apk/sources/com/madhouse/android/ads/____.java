package com.madhouse.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URISyntaxException;

final class ____ extends View {
    private String $;
    private Rect $$;
    private Bitmap $$$;
    private Paint $$$$;
    private Matrix $$$$$;
    int _;
    int __;
    __ ___;
    private byte[] ____;
    private String _____;
    private Context a;

    protected ____(__ __2, Context context) {
        this(__2, context, null, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.madhouse.android.ads.eeeee._(java.lang.String, boolean):byte[]
     arg types: [java.lang.String, int]
     candidates:
      com.madhouse.android.ads.eeeee._(java.lang.String, int):java.lang.String
      com.madhouse.android.ads.eeeee._(java.lang.String, boolean):byte[] */
    private ____(__ __2, Context context, AttributeSet attributeSet, int i) {
        super(context, null, 0);
        this.___ = __2;
        this.a = context;
        int __3 = f.__(__2.____);
        int __4 = f.__(__2._____);
        if (__3 <= 0 || __4 <= 0) {
            new DisplayMetrics();
            DisplayMetrics displayMetrics = context.getApplicationContext().getResources().getDisplayMetrics();
            int i2 = displayMetrics.widthPixels;
            int i3 = displayMetrics.heightPixels;
            if (i2 <= i3) {
                this._ = i2;
                this.__ = (i3 * 15) / 100;
            } else {
                this._ = i3;
                this.__ = (i2 * 15) / 100;
            }
        } else {
            this._ = __3;
            this.__ = __4;
        }
        this.$$ = new Rect(0, 0, this._, this.__);
        if (__2 != null) {
            setFocusable(true);
            setClickable(true);
            String str = __2.$;
            String str2 = __2.___;
            if (str != null && str.length() > 0 && str2 != null && str2.length() == 8) {
                this.____ = null;
                if (__2.$$$$) {
                    this.____ = e._(this.a, str2);
                    if (this.____ == null) {
                        this.____ = eeeee._(str, false);
                        e._(this.a, str2, this.____);
                    }
                } else {
                    this.____ = eeeee._(str, false);
                }
                if (this.____ != null) {
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.____);
                    if (this.$$$ != null && !this.$$$.isRecycled()) {
                        this.$$$.recycle();
                        this.$$$ = null;
                    }
                    this.$$$ = BitmapFactory.decodeStream(byteArrayInputStream);
                    int width = this.$$$.getWidth();
                    int height = this.$$$.getHeight();
                    float f = ((float) this._) / ((float) width);
                    float f2 = ((float) this.__) / ((float) height);
                    this.$$$$$ = new Matrix();
                    this.$$$$$.postScale(f, f2);
                    this.$$$$ = new Paint();
                    try {
                        byteArrayInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (__2.$$$ != null) {
                    this._____ = eeeee._(__2.$$$);
                } else if (__2.$$ != null) {
                    this.$ = __2.$$;
                }
            } else {
                return;
            }
        }
        invalidate();
    }

    private void __() {
        if (this._____ != null) {
            try {
                f.___(this.a, this._____);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        } else if (this.$ != null) {
            try {
                f.__(this.a, this.$);
            } catch (URISyntaxException e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void _() {
        if (!this.$$$.isRecycled()) {
            this.$$$.recycle();
            this.$$$ = null;
        }
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            super.setPressed(true);
        } else if (action == 2) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int left = getLeft();
            int top = getTop();
            int right = getRight();
            int bottom = getBottom();
            if (x < ((float) left) || x > ((float) right) || y < ((float) top) || y > ((float) bottom)) {
                super.setPressed(false);
            } else {
                super.setPressed(true);
            }
        } else if (action == 1) {
            if (isPressed()) {
                __();
            }
            super.setPressed(false);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public final boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            super.setPressed(true);
        } else if (motionEvent.getAction() == 1) {
            if (hasFocus()) {
                __();
            }
            super.setPressed(false);
        }
        return super.dispatchTrackballEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        try {
            super.onDraw(canvas);
            if (this.$$$ != null && this.$$$$$ != null && this.$$$$ != null) {
                canvas.drawBitmap(this.$$$, this.$$$$$, this.$$$$);
                if (!isPressed() && hasFocus()) {
                    Rect rect = this.$$;
                    Paint paint = new Paint();
                    paint.setAntiAlias(true);
                    paint.setColor(-1147097);
                    paint.setStyle(Paint.Style.STROKE);
                    paint.setStrokeWidth(5.0f);
                    paint.setPathEffect(new CornerPathEffect(5.0f));
                    Path path = new Path();
                    path.addRoundRect(new RectF(rect), 5.0f, 5.0f, Path.Direction.CW);
                    canvas.drawPath(path, paint);
                }
            }
        } catch (Exception e) {
            if (this.$$$ != null && !this.$$$.isRecycled()) {
                this.$$$.recycle();
            }
        }
    }

    public final boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 66 || i == 23) {
            super.setPressed(true);
        }
        return super.onKeyDown(i, keyEvent);
    }

    public final boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i == 66 || i == 23) {
            __();
        }
        super.setPressed(false);
        return super.onKeyUp(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        setMeasuredDimension(this._, this.__);
    }

    public final void setPressed(boolean z) {
        super.setPressed(z);
    }
}
