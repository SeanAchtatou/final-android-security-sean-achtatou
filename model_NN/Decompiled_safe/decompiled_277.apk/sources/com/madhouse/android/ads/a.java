package com.madhouse.android.ads;

import android.view.View;

final class a implements View.OnClickListener {
    private /* synthetic */ C$$$$$ _;

    a(C$$$$$ $$$$$) {
        this._ = $$$$$;
    }

    public final void onClick(View view) {
        if (this._.___.___.canGoBack()) {
            this._.___.___.goBack();
            this._._();
            if (!this._.___.___.canGoBack() && this._.___.___.canGoForward()) {
                this._.__.requestFocus();
            }
        }
    }
}
