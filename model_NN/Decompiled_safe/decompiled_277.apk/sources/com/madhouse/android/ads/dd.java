package com.madhouse.android.ads;

import android.content.Context;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.animation.AlphaAnimation;
import android.widget.LinearLayout;

final class dd extends LinearLayout {
    final dddd _;
    final dddd __;
    Handler ___ = new ddd(this);

    public dd(_____ _____, Context context) {
        super(context);
        setClickable(true);
        setFocusable(true);
        setOrientation(0);
        setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this._ = new dddd(this, context, this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.setMargins(3, 3, 3, 3);
        this._.setBackgroundDrawable(context.getResources().getDrawable(17301512));
        addView(this._, layoutParams);
        this.__ = new dddd(this, context, this);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.setMargins(3, 3, 3, 3);
        this.__.setBackgroundDrawable(context.getResources().getDrawable(17301511));
        addView(this.__, layoutParams2);
        setBackgroundColor(-2013265920);
        setBackgroundDrawable(context.getResources().getDrawable(17301603));
    }

    public final void _() {
        _(4, 1.0f, 0.0f);
        this.___.removeMessages(8533591);
    }

    /* access modifiers changed from: package-private */
    public void _(int i, float f, float f2) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f, f2);
        alphaAnimation.setDuration(500);
        startAnimation(alphaAnimation);
        setVisibility(i);
    }

    public final boolean hasFocus() {
        return this._.hasFocus() || this.__.hasFocus();
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        return true;
    }
}
