package com.madhouse.android.ads;

import I.I;
import java.util.ListResourceBundle;

public class Resource_zh_TW extends ListResourceBundle {
    private Object[][] _ = {new Object[]{I.I(1), "關閉"}, new Object[]{I.I(7), "上一頁"}, new Object[]{I.I(16), "下一頁"}, new Object[]{I.I(21), "刷新"}};

    /* access modifiers changed from: protected */
    public final Object[][] getContents() {
        return this._;
    }
}
