package com.madhouse.android.ads;

import I.I;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;

final class _ {
    private _() {
    }

    protected static final l _(Context context) {
        l lVar = new l();
        int __ = __(context);
        if (__ == 0 || __ == 2) {
            lVar._ = __;
            return lVar;
        }
        try {
            Cursor query = context.getContentResolver().query(Uri.parse(I.I(1591)), null, null, null, null);
            query.moveToFirst();
            if (query.getColumnIndex(I.I(1630)) != -1 || query.getColumnIndex(I.I(1636)) != -1 || query.getColumnIndex(I.I(1642)) != -1 || query.getColumnIndex(I.I(1648)) != -1) {
                if (query != null) {
                    query.close();
                }
                lVar._ = __;
                return lVar;
            } else if (query.getInt(query.getColumnIndex(I.I(1655))) > 0) {
                lVar._ = __;
                lVar.__ = query.getString(query.getColumnIndex(I.I(1655)));
                lVar.___ = query.getString(query.getColumnIndex(I.I(1661)));
                if (query != null) {
                    query.close();
                }
                return lVar;
            } else if (query.getInt(query.getColumnIndex(I.I(1666))) > 0) {
                lVar._ = __;
                lVar.__ = query.getString(query.getColumnIndex(I.I(1666)));
                lVar.___ = query.getString(query.getColumnIndex(I.I(1675)));
                if (query != null) {
                    query.close();
                }
                return lVar;
            } else {
                if (query != null) {
                    query.close();
                }
                lVar._ = __;
                return lVar;
            }
        } catch (Exception e) {
            lVar._ = __;
            return lVar;
        }
    }

    private static int __(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(I.I(1683));
            if (connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected()) {
                if (connectivityManager.getActiveNetworkInfo().getType() == 0) {
                    return 1;
                }
                if (connectivityManager.getActiveNetworkInfo().getType() == 1) {
                    return 2;
                }
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }
}
