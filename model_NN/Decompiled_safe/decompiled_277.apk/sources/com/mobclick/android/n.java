package com.mobclick.android;

import android.content.Context;
import java.text.SimpleDateFormat;
import java.util.Date;

public class n {
    private static String[] A = {"年龄", "18岁以下", "18-24岁", "25-30岁", "31-35岁", "36-40岁", "41-50岁", "51-59岁", "60岁及以上"};
    private static String[] B = {"Age", "<18", "18~24", "25~30", "31~35", "36~40", "41~50", "51~59", ">=60"};
    private static String[] C = {"性别", "男", "女"};
    private static String[] D = {"Gender", "Male", "Female"};
    private static String E = "应用程序有新版本更新";
    private static String F = "New version found";
    private static String G = "最新版本: ";
    private static String H = "Latest version: ";

    /* renamed from: I  reason: collision with root package name */
    private static String f0I = "（提示：非WIFI环境）";
    private static String J = "(Warning: Not WIFI Condition)";
    private static String K = "立即更新";
    private static String L = "Update now";
    private static String M = "应用更新";
    private static String N = "App updating";
    private static String O = "正在更新应用程序...";
    private static String P = "Updating application...";
    private static String Q = "以后再说";
    private static String R = "Not now";
    public static String a = "last_send_time";
    public static final Object b = new Object();
    public static final String c = "MobclickAgent";
    public static final String d = "http://www.umeng.com/app_logs";
    public static final String e = "http://www.umeng.com/api/check_app_update";
    public static final String f = "http://www.umeng.com/check_config_update";
    public static final String g = "http://www.umeng.co/app_logs";
    public static final String h = "http://www.umeng.co/api/check_app_update";
    public static final String i = "http://www.umeng.co/check_config_update";
    public static boolean j = true;
    public static String k = "未联网";
    public static String l = "Please make sure you are connected to internet, update failed";
    public static final String m = "正在更新中....";
    public static final String n = "Is updating....";
    public static final String o = "最新版本已下载，是否安装？";
    public static final String p = "The lastest version has been downloaded, install now ?";
    private static String q = "用户反馈";
    private static String r = "Feedback";
    private static String s = "欢迎您提出宝贵的意见和建议，您留下的每个字都將用来改善我们的服务";
    private static String t = "Any comments and suggestions are welcome, we believe every word you write will benefit us";
    private static String u = "请输入您的反馈意见（字数500以内）";
    private static String v = "Input your suggestions here";
    private static String w = "提交反馈";
    private static String x = "Submit suggestions";
    private static String y = "请正确选择年龄和性别再提交";
    private static String z = "Please fill in a correct age and gender before submitting";

    public static String A() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static int a(Date date, Date date2) {
        Date date3;
        Date date4;
        if (date.after(date2)) {
            date3 = date;
            date4 = date2;
        } else {
            date3 = date2;
            date4 = date;
        }
        return (int) ((date3.getTime() - date4.getTime()) / 86400000);
    }

    public static String a() {
        return q;
    }

    public static String a(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? q : r;
    }

    public static Date a(String str) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(str);
        } catch (Exception e2) {
            return null;
        }
    }

    public static String b() {
        return r;
    }

    public static String b(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? s : t;
    }

    public static String c() {
        return s;
    }

    public static String c(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? u : v;
    }

    public static String d() {
        return t;
    }

    public static String d(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? w : x;
    }

    public static String e() {
        return u;
    }

    public static String e(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? y : z;
    }

    public static String f() {
        return v;
    }

    public static String[] f(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? A : B;
    }

    public static String g() {
        return w;
    }

    public static String[] g(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? C : D;
    }

    public static String h() {
        return x;
    }

    public static String h(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? E : F;
    }

    public static String i() {
        return y;
    }

    public static String i(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? G : H;
    }

    public static String j() {
        return z;
    }

    public static String j(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? f0I : J;
    }

    public static String k(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? K : L;
    }

    public static String[] k() {
        return A;
    }

    public static String l(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? M : N;
    }

    public static String[] l() {
        return B;
    }

    public static String m(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? O : P;
    }

    public static String[] m() {
        return C;
    }

    public static String n(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? Q : R;
    }

    public static String[] n() {
        return D;
    }

    public static String o() {
        return E;
    }

    public static String o(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? m : n;
    }

    public static String p() {
        return F;
    }

    public static String p(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? o : p;
    }

    public static String q() {
        return G;
    }

    public static String r() {
        return H;
    }

    public static String s() {
        return K;
    }

    public static String t() {
        return L;
    }

    public static String u() {
        return M;
    }

    public static String v() {
        return N;
    }

    public static String w() {
        return O;
    }

    public static String x() {
        return P;
    }

    public static String y() {
        return Q;
    }

    public static String z() {
        return R;
    }
}
