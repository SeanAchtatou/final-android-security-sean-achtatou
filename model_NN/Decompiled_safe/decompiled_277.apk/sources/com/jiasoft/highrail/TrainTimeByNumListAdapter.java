package com.jiasoft.highrail;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.highrail.pub.TicketInfo;
import com.jiasoft.highrail.pub.UpdateData;
import java.util.List;

public class TrainTimeByNumListAdapter extends BaseAdapter {
    private ParentActivity mContext;
    public TicketInfo maintraininfo = new TicketInfo();
    public List<TicketInfo> traintimeList;
    private UpdateData updateData = new UpdateData(this.mContext, null);

    public TrainTimeByNumListAdapter(ParentActivity c) {
        this.mContext = c;
    }

    public void getDataList(String trainnum, String departDate) {
        this.traintimeList = this.updateData.findTrainTimeByNum(this.maintraininfo, trainnum, departDate);
    }

    public int getCount() {
        return this.traintimeList.size();
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.mContext).inflate((int) R.layout.adaptertraintimebynum, (ViewGroup) null);
            holder = new ViewHolder();
            holder.timeinfo = (TextView) convertView.findViewById(R.id.timeinfo);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        TicketInfo ticketinfo = this.traintimeList.get(position);
        holder.timeinfo.setText(String.valueOf(ticketinfo.getA1()) + " \t" + ticketinfo.getDeparture_station() + " \t[" + ticketinfo.getArrival_time() + "-" + ticketinfo.getDeparture_time() + "] \t" + ticketinfo.getRun_time() + "/" + ticketinfo.getRun_distance());
        return convertView;
    }

    static class ViewHolder {
        TextView timeinfo;

        ViewHolder() {
        }
    }

    public UpdateData getUpdateData() {
        return this.updateData;
    }

    public void setUpdateData(UpdateData updateData2) {
        this.updateData = updateData2;
    }
}
