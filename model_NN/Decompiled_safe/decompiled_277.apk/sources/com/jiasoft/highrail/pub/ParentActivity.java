package com.jiasoft.highrail.pub;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.guohead.sdk.GuoheAdLayout;
import com.jiasoft.highrail.R;
import com.jiasoft.pub.DatabaseAdapter;
import com.jiasoft.pub.DbInterface;
import com.jiasoft.pub.PC_SYS_CONFIG;
import com.jiasoft.pub.SrvProxy;

public class ParentActivity extends Activity implements DbInterface {
    public static final String DB_NAME = "highrailtime.db";
    public static final int DB_VERSION = 3;
    public static final String[][] dbUpdate = {new String[]{"1", "DROP TABLE PC_SYS_CONFIG"}, new String[]{"1", "DROP TABLE MY_HOT"}, new String[]{"1", "DROP TABLE CITY"}, new String[]{"1", "create table PC_SYS_CONFIG  (    CODE    VARCHAR(30) not null,    NAME    VARCHAR(80),    CUR_VAL VARCHAR(80),    MIN_VAL VARCHAR(80),    MAX_VAL VARCHAR(80),    REMARK  VARCHAR(80))"}, new String[]{"1", "create table MY_HOT ( SEQ DECIMAL(10) not null,HOT_TYPE VARCHAR(30),DEPARTURE VARCHAR(30),ARRIVAL VARCHAR(30),READY1 VARCHAR(80),READY2 VARCHAR(80),ADD_TIME DATE,constraint PK_E_BOOKCOMMENT primary key (SEQ))"}, new String[]{"2", "create table CITY  (SEQ DECIMAL(10) not null,SHENG VARCHAR(30),CITY VARCHAR(30),CITY_TYPE VARCHAR(2),SHENG_NAME  VARCHAR(80),CITY_NAME  VARCHAR(80),REMARK  VARCHAR(80),constraint PK_CITY primary key (SEQ))"}, new String[]{"3", "create table STATION  (SEQ DECIMAL(10) not null,PY VARCHAR(30),STATION VARCHAR(30),constraint PK_CITY primary key (SEQ))"}, new String[]{"0", ""}};
    public DatabaseAdapter dbAdapter;
    public MyApplication myApp;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.myApp = (MyApplication) getApplication();
        this.dbAdapter = new DatabaseAdapter(this, DB_NAME, 3, dbUpdate);
        this.dbAdapter.open();
    }

    public void setAdShow() {
        boolean canAd = false;
        try {
            if (MyApplication.isIfAdVerse()) {
                canAd = true;
                if ("1".equalsIgnoreCase(PC_SYS_CONFIG.getConfValue(this, "IF_WIFISHOWAD", "0")) && !SrvProxy.isWifiConnected(this)) {
                    canAd = false;
                }
            }
            if (canAd) {
                ((LinearLayout) findViewById(R.id.adLayout)).addView(new GuoheAdLayout(this), new RelativeLayout.LayoutParams(-1, -2));
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.dbAdapter.close();
        super.onDestroy();
    }

    public void setTitle(int stitle) {
        try {
            ((TextView) findViewById(R.id.title)).setText(stitle);
        } catch (Exception e) {
        }
        try {
            ((Button) findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ParentActivity.this.finish();
                }
            });
        } catch (Exception e2) {
        }
    }

    public DatabaseAdapter getDbAdapter() {
        return this.dbAdapter;
    }
}
