package com.jiasoft.highrail.pub;

import android.content.ContentValues;
import android.database.Cursor;
import com.jiasoft.pub.DbInterface;
import com.jiasoft.pub.wwpublic;

public class MY_HOT {
    private String ADD_TIME = "";
    private String ARRIVAL = "";
    private String DEPARTURE = "";
    private String HOT_TYPE = "";
    private String READY1 = "";
    private String READY2 = "";
    private String SEQ = "";
    private DbInterface mContext;
    private boolean selected = false;

    public MY_HOT(DbInterface mContext2) {
        this.mContext = mContext2;
    }

    public MY_HOT(DbInterface mContext2, Cursor cur) {
        this.mContext = mContext2;
        getFromCur(cur);
    }

    public MY_HOT(DbInterface mContext2, String sWhere) {
        this.mContext = mContext2;
        Cursor cur = mContext2.getDbAdapter().rawQuery("select * from MY_HOT where " + sWhere);
        if (cur != null && cur.moveToFirst()) {
            getFromCur(cur);
        }
        cur.close();
    }

    private void getFromCur(Cursor cur) {
        this.SEQ = cur.getString(cur.getColumnIndex("SEQ"));
        this.HOT_TYPE = cur.getString(cur.getColumnIndex("HOT_TYPE"));
        this.DEPARTURE = cur.getString(cur.getColumnIndex("DEPARTURE"));
        this.ARRIVAL = cur.getString(cur.getColumnIndex("ARRIVAL"));
        this.READY1 = cur.getString(cur.getColumnIndex("READY1"));
        this.READY2 = cur.getString(cur.getColumnIndex("READY2"));
        this.ADD_TIME = cur.getString(cur.getColumnIndex("ADD_TIME"));
        if (wwpublic.ss(this.ADD_TIME).equalsIgnoreCase(" ")) {
            this.ADD_TIME = "";
        }
    }

    public void insert() {
        this.mContext.getDbAdapter().insert("MY_HOT", saveCv());
    }

    public void update(String sWhere) {
        this.mContext.getDbAdapter().update("MY_HOT", saveCv(), sWhere);
    }

    public void delete() {
        this.mContext.getDbAdapter().delete("MY_HOT", "SEQ=" + this.SEQ);
    }

    public ContentValues saveCv() {
        ContentValues cv = new ContentValues();
        cv.put("SEQ", this.SEQ);
        cv.put("HOT_TYPE", this.HOT_TYPE);
        cv.put("DEPARTURE", this.DEPARTURE);
        cv.put("ARRIVAL", this.ARRIVAL);
        cv.put("READY1", this.READY1);
        cv.put("READY2", this.READY2);
        cv.put("ADD_TIME", this.ADD_TIME);
        return cv;
    }

    public DbInterface getMContext() {
        return this.mContext;
    }

    public void setMContext(DbInterface context) {
        this.mContext = context;
    }

    public String getSEQ() {
        return this.SEQ;
    }

    public void setSEQ(String seq) {
        this.SEQ = seq;
    }

    public String getADD_TIME() {
        return this.ADD_TIME;
    }

    public void setADD_TIME(String add_time) {
        this.ADD_TIME = add_time;
    }

    public boolean isSelected() {
        return this.selected;
    }

    public void setSelected(boolean selected2) {
        this.selected = selected2;
    }

    public String getHOT_TYPE() {
        return this.HOT_TYPE;
    }

    public void setHOT_TYPE(String hot_type) {
        this.HOT_TYPE = hot_type;
    }

    public String getDEPARTURE() {
        return this.DEPARTURE;
    }

    public void setDEPARTURE(String departure) {
        this.DEPARTURE = departure;
    }

    public String getARRIVAL() {
        return this.ARRIVAL;
    }

    public void setARRIVAL(String arrival) {
        this.ARRIVAL = arrival;
    }

    public String getREADY1() {
        return this.READY1;
    }

    public void setREADY1(String ready1) {
        this.READY1 = ready1;
    }

    public String getREADY2() {
        return this.READY2;
    }

    public void setREADY2(String ready2) {
        this.READY2 = ready2;
    }
}
