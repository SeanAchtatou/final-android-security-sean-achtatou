package com.jiasoft.highrail.pub;

public class TicketInfo {
    private String a1;
    private String a2;
    private String a3;
    private String a4;
    private String a5;
    private String arrival_station;
    private String arrival_time;
    private String departure_station;
    private String departure_time;
    private String end_station;
    private String first_station;
    private String price_info;
    private String remain_info;
    private String run_distance;
    private String run_time;
    private String train_number;
    private String train_number_url;
    private String train_type;

    public String getTrain_number_url() {
        return this.train_number_url;
    }

    public void setTrain_number_url(String train_number_url2) {
        this.train_number_url = train_number_url2;
    }

    public String getA1() {
        return this.a1;
    }

    public void setA1(String a12) {
        this.a1 = a12;
    }

    public String getA2() {
        return this.a2;
    }

    public void setA2(String a22) {
        this.a2 = a22;
    }

    public String getA3() {
        return this.a3;
    }

    public void setA3(String a32) {
        this.a3 = a32;
    }

    public String getA4() {
        return this.a4;
    }

    public void setA4(String a42) {
        this.a4 = a42;
    }

    public String getA5() {
        return this.a5;
    }

    public void setA5(String a52) {
        this.a5 = a52;
    }

    public String getTrain_number() {
        return this.train_number;
    }

    public void setTrain_number(String train_number2) {
        this.train_number = train_number2;
    }

    public String getTrain_type() {
        return this.train_type;
    }

    public void setTrain_type(String train_type2) {
        this.train_type = train_type2;
    }

    public String getFirst_station() {
        return this.first_station;
    }

    public void setFirst_station(String first_station2) {
        this.first_station = first_station2;
    }

    public String getEnd_station() {
        return this.end_station;
    }

    public void setEnd_station(String end_station2) {
        this.end_station = end_station2;
    }

    public String getDeparture_station() {
        return this.departure_station;
    }

    public void setDeparture_station(String departure_station2) {
        this.departure_station = departure_station2;
    }

    public String getArrival_station() {
        return this.arrival_station;
    }

    public void setArrival_station(String arrival_station2) {
        this.arrival_station = arrival_station2;
    }

    public String getDeparture_time() {
        return this.departure_time;
    }

    public void setDeparture_time(String departure_time2) {
        this.departure_time = departure_time2;
    }

    public String getArrival_time() {
        return this.arrival_time;
    }

    public void setArrival_time(String arrival_time2) {
        this.arrival_time = arrival_time2;
    }

    public String getRun_time() {
        return this.run_time;
    }

    public void setRun_time(String run_time2) {
        this.run_time = run_time2;
    }

    public String getRun_distance() {
        return this.run_distance;
    }

    public void setRun_distance(String run_distance2) {
        this.run_distance = run_distance2;
    }

    public String getPrice_info() {
        return this.price_info;
    }

    public void setPrice_info(String price_info2) {
        this.price_info = price_info2;
    }

    public String getRemain_info() {
        return this.remain_info;
    }

    public void setRemain_info(String remain_info2) {
        this.remain_info = remain_info2;
    }
}
