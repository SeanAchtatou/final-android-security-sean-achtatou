package com.jiasoft.highrail.pub;

import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.jiasoft.highrail.R;
import java.util.ArrayList;
import java.util.List;

public class StationListAdapter extends BaseAdapter implements Filterable {
    public List<STATION> filterList = new ArrayList();
    /* access modifiers changed from: private */
    public ParentActivity mContext;
    private Filter mFilter;

    public StationListAdapter(ParentActivity c) {
        this.mContext = c;
        getDataList();
    }

    public void getDataList() {
        if (this.mContext.myApp.stationList == null) {
            this.mContext.myApp.stationList = new ArrayList();
            Cursor cur = this.mContext.dbAdapter.fetch("STATION");
            if (cur == null || !cur.moveToFirst()) {
                cur.close();
            }
            do {
                this.mContext.myApp.stationList.add(new STATION(this.mContext, cur));
            } while (cur.moveToNext());
            cur.close();
        }
    }

    public int getCount() {
        return this.filterList.size();
    }

    public Object getItem(int position) {
        return this.filterList.get(position).getSTATION();
    }

    public long getItemId(int position) {
        return (long) position;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.mContext).inflate((int) R.layout.adapterstation, (ViewGroup) null);
            holder = new ViewHolder();
            holder.timeinfo = (TextView) convertView.findViewById(R.id.timeinfo);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        try {
            holder.timeinfo.setText(this.filterList.get(position).getSTATION());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    static class ViewHolder {
        TextView timeinfo;

        ViewHolder() {
        }
    }

    public Filter getFilter() {
        if (this.mFilter == null) {
            this.mFilter = new CityArrayFilter(this, null);
        }
        return this.mFilter;
    }

    private class CityArrayFilter extends Filter {
        private CityArrayFilter() {
        }

        /* synthetic */ CityArrayFilter(StationListAdapter stationListAdapter, CityArrayFilter cityArrayFilter) {
            this();
        }

        /* access modifiers changed from: protected */
        public Filter.FilterResults performFiltering(CharSequence prefix) {
            Filter.FilterResults results = new Filter.FilterResults();
            Log.i("FilterResults==", "FilterResults1=" + ((Object) prefix));
            if (prefix == null || prefix.length() == 0) {
                ArrayList<STATION> list = new ArrayList<>(StationListAdapter.this.mContext.myApp.stationList);
                results.values = list;
                results.count = list.size();
            } else {
                Log.i("FilterResults==", "FilterResults2=" + ((Object) prefix));
                String prefixString = prefix.toString().toUpperCase();
                ArrayList<STATION> newValues = new ArrayList<>();
                int iLen = 2;
                try {
                    iLen = prefixString.getBytes("GBK").length;
                } catch (Exception e) {
                }
                Log.i("FilterResults==", "iLen=" + iLen);
                if (iLen == 2) {
                    for (int i = 0; i < StationListAdapter.this.mContext.myApp.stationList.size(); i++) {
                        STATION station = StationListAdapter.this.mContext.myApp.stationList.get(i);
                        if (station.getPY().equalsIgnoreCase(prefixString)) {
                            newValues.add(station);
                        }
                    }
                }
                results.values = newValues;
                results.count = newValues.size();
            }
            return results;
        }

        /* access modifiers changed from: protected */
        public void publishResults(CharSequence constraint, Filter.FilterResults results) {
            StationListAdapter.this.filterList = (List) results.values;
            if (results.count > 0) {
                StationListAdapter.this.notifyDataSetChanged();
            } else {
                StationListAdapter.this.notifyDataSetInvalidated();
            }
        }
    }
}
