package com.jiasoft.highrail.pub;

import android.app.Application;
import java.util.List;

public class MyApplication extends Application {
    private static final boolean ifAdVerse = true;
    private String HOTEL_SEL = "";
    private String serverip = "http://59.56.176.81:7098";
    public List<STATION> stationList;

    public static boolean isIfAdVerse() {
        return ifAdVerse;
    }

    public String getServerip() {
        return this.serverip;
    }

    public void setServerip(String serverip2) {
        this.serverip = serverip2;
    }

    public String getHOTEL_SEL() {
        return this.HOTEL_SEL;
    }

    public void setHOTEL_SEL(String hotel_sel) {
        this.HOTEL_SEL = hotel_sel;
    }
}
