package com.jiasoft.highrail.pub;

import android.database.Cursor;
import com.jiasoft.pub.DbInterface;

public class STATION {
    private String PY = "";
    private String STATION = "";
    private DbInterface mContext;

    public STATION(DbInterface mContext2) {
        this.mContext = mContext2;
    }

    public STATION(DbInterface mContext2, Cursor cur) {
        this.mContext = mContext2;
        getFromCur(cur);
    }

    public STATION(DbInterface mContext2, String sWhere) {
        this.mContext = mContext2;
        Cursor cur = mContext2.getDbAdapter().rawQuery("select * from STATION where " + sWhere);
        if (cur != null && cur.moveToFirst()) {
            getFromCur(cur);
        }
        cur.close();
    }

    private void getFromCur(Cursor cur) {
        this.PY = cur.getString(cur.getColumnIndex("PY"));
        this.STATION = cur.getString(cur.getColumnIndex("STATION"));
    }

    public DbInterface getMContext() {
        return this.mContext;
    }

    public void setMContext(DbInterface context) {
        this.mContext = context;
    }

    public String getPY() {
        return this.PY;
    }

    public void setPY(String py) {
        this.PY = py;
    }

    public String getSTATION() {
        return this.STATION;
    }

    public void setSTATION(String station) {
        this.STATION = station;
    }
}
