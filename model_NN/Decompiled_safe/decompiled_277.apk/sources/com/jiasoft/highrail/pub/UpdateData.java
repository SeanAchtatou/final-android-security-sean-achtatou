package com.jiasoft.highrail.pub;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.os.Handler;
import android.util.Log;
import com.jiasoft.highrail.R;
import com.jiasoft.pub.AndPub;
import com.jiasoft.pub.date;
import com.jiasoft.pub.procString;
import com.jiasoft.pub.wwpublic;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class UpdateData {
    private static final String TRAIN_REMAIN_TICKET = "http://dynamic.12306.cn/TrainQuery/iframeLeftTicketByStation.jsp";
    private static final String TRAIN_TIME_NUM = "http://www.huoche.com.cn/chaxun/resultc.php?txtcheci=";
    private static final String TRAIN_TIME_STATION = "http://www.huoche.com.cn/chaxun/resultz.php?txtchezhan=";
    private static final String TRAIN_TIME_URL = "http://www.huoche.com.cn/shike/";
    private ParentActivity context;
    private boolean ifTransit = false;
    Handler mHandler;
    ProgressDialog m_pDialog;
    private String sErrMsg = "";

    public UpdateData(ParentActivity context2, Handler mHandler2) {
        this.context = context2;
        this.mHandler = mHandler2;
    }

    public static void saveToHot(ParentActivity context2, String hottype, String s1, String s2) {
        String sWhere;
        try {
            if ("".equalsIgnoreCase(s2)) {
                sWhere = "HOT_TYPE='" + hottype + "' and DEPARTURE='" + s1 + "'";
            } else {
                sWhere = "HOT_TYPE='" + hottype + "' and DEPARTURE='" + s1 + "' and ARRIVAL='" + s2 + "'";
            }
            MY_HOT myhot = new MY_HOT(context2, sWhere);
            if ("".equalsIgnoreCase(myhot.getSEQ())) {
                String seq = context2.dbAdapter.queryOneReturn("select max(SEQ)+1 from MY_HOT");
                if (wwpublic.ss(seq).equalsIgnoreCase(" ")) {
                    seq = "1";
                }
                myhot.setSEQ(seq);
                myhot.setHOT_TYPE(hottype);
                myhot.setDEPARTURE(s1);
                myhot.setARRIVAL(s2);
                myhot.setADD_TIME(date.GetLocalTime());
                myhot.insert();
                return;
            }
            myhot.setADD_TIME(date.GetLocalTime());
            myhot.update("SEQ=" + myhot.getSEQ());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean initCity() {
        if (!wwpublic.ss(this.context.dbAdapter.queryOneReturn("select 1 from CITY where CITY_TYPE in ('1','2')")).equalsIgnoreCase(" ")) {
            return false;
        }
        String sheng = "";
        String shengName = "";
        int iSeq = 0;
        try {
            this.context.dbAdapter.beginTransaction();
            this.context.dbAdapter.delete("CITY", "CITY_TYPE in ('1','2')");
            InputStream is = this.context.getAssets().open("city.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(is, "GBK");
            BufferedReader bufReader = new BufferedReader(inputStreamReader);
            while (true) {
                String line = bufReader.readLine();
                if (line == null) {
                    break;
                }
                procString proc = new procString(line);
                boolean ifFirst = true;
                while (true) {
                    String s1 = proc.getStrByDevNext();
                    String s2 = proc.getStrByDevNext();
                    String s3 = proc.getStrByDevNext();
                    if (!wwpublic.ss(s1).equalsIgnoreCase(" ") && !wwpublic.ss(s2).equalsIgnoreCase(" ")) {
                        iSeq++;
                        if (ifFirst) {
                            ifFirst = false;
                            sheng = s1;
                            shengName = s2;
                            ContentValues cv = new ContentValues();
                            cv.put("SEQ", Integer.valueOf(iSeq));
                            cv.put("SHENG", sheng);
                            cv.put("CITY", "");
                            cv.put("CITY_TYPE", "1");
                            cv.put("SHENG_NAME", shengName);
                            cv.put("CITY_NAME", "");
                            cv.put("REMARK", s3);
                            this.context.dbAdapter.insert("CITY", cv);
                        } else {
                            ContentValues cv2 = new ContentValues();
                            cv2.put("SEQ", Integer.valueOf(iSeq));
                            cv2.put("SHENG", sheng);
                            cv2.put("CITY", s1);
                            cv2.put("CITY_TYPE", "2");
                            cv2.put("SHENG_NAME", shengName);
                            cv2.put("CITY_NAME", s2);
                            cv2.put("REMARK", s3);
                            this.context.dbAdapter.insert("CITY", cv2);
                        }
                    }
                }
            }
            this.context.dbAdapter.commit();
            bufReader.close();
            inputStreamReader.close();
            is.close();
        } catch (Exception e) {
            this.context.dbAdapter.rollback();
        }
        return true;
    }

    public void initStation() {
        if (wwpublic.ss(this.context.dbAdapter.queryOneReturn("select 1 from STATION")).equalsIgnoreCase(" ")) {
            int iSeq = 0;
            try {
                this.context.dbAdapter.beginTransaction();
                this.context.dbAdapter.delete("STATION", "1=1");
                InputStream is = this.context.getAssets().open("station.txt");
                InputStreamReader inputReader = new InputStreamReader(is, "GBK");
                BufferedReader bufReader = new BufferedReader(inputReader);
                while (true) {
                    String line = bufReader.readLine();
                    if (line == null) {
                        this.context.dbAdapter.commit();
                        bufReader.close();
                        inputReader.close();
                        is.close();
                        return;
                    }
                    String[] sproc = line.split(";");
                    for (int i = 1; i < sproc.length; i++) {
                        if (!wwpublic.ss(sproc[i]).equalsIgnoreCase(" ")) {
                            iSeq++;
                            ContentValues cv = new ContentValues();
                            cv.put("SEQ", Integer.valueOf(iSeq));
                            cv.put("PY", sproc[0]);
                            cv.put("STATION", sproc[i]);
                            this.context.dbAdapter.insert("STATION", cv);
                        }
                    }
                }
            } catch (Exception e) {
                this.context.dbAdapter.rollback();
            }
        }
    }

    public void initFlightCity() {
        if (wwpublic.ss(this.context.dbAdapter.queryOneReturn("select 1 from CITY where CITY_TYPE in ('3')")).equalsIgnoreCase(" ")) {
            int iSeq = 10000;
            try {
                this.context.dbAdapter.beginTransaction();
                this.context.dbAdapter.delete("CITY", "CITY_TYPE in ('3')");
                procString proc = new procString(AndPub.readAssetTextFile(this.context, "flightcity.txt").toString());
                while (true) {
                    String s1 = proc.getStrByDevNext();
                    String s2 = proc.getStrByDevNext();
                    String s3 = proc.getStrByDevNext();
                    String s4 = proc.getStrByDevNext();
                    if (wwpublic.ss(s1).equalsIgnoreCase(" ") || wwpublic.ss(s4).equalsIgnoreCase(" ")) {
                        this.context.dbAdapter.commit();
                    } else {
                        iSeq++;
                        ContentValues cv = new ContentValues();
                        cv.put("SEQ", Integer.valueOf(iSeq));
                        cv.put("SHENG", s4);
                        cv.put("CITY", s2);
                        cv.put("CITY_TYPE", "3");
                        int iOrder = 0;
                        try {
                            iOrder = Integer.parseInt(s3);
                        } catch (Exception e) {
                        }
                        cv.put("SHENG_NAME", new StringBuilder().append(iOrder).toString());
                        cv.put("CITY_NAME", s1);
                        this.context.dbAdapter.insert("CITY", cv);
                    }
                }
                this.context.dbAdapter.commit();
            } catch (Exception e2) {
                this.context.dbAdapter.rollback();
            }
        }
    }

    public List<TicketInfo> findTrainTime(String departure, String arrival, String departDate) {
        List<TicketInfo> timeList = new ArrayList<>();
        this.ifTransit = false;
        try {
            String sUrl = TRAIN_TIME_URL + URLEncoder.encode(departure, "UTF-8") + "-" + URLEncoder.encode(arrival, "UTF-8") + "/";
            Log.i("sUrl===", sUrl);
            URLConnection conn = new URL(sUrl).openConnection();
            conn.connect();
            StringBuffer resultBuf = new StringBuffer();
            InputStreamReader inputStreamReader = new InputStreamReader(conn.getInputStream(), "UTF-8");
            BufferedReader bufReader = new BufferedReader(inputStreamReader);
            while (true) {
                String line = bufReader.readLine();
                if (line == null) {
                    break;
                }
                resultBuf.append(line);
            }
            bufReader.close();
            inputStreamReader.close();
            String result = resultBuf.toString();
            if (result.indexOf("没有可以直接到达的车次") >= 0) {
                this.ifTransit = true;
            }
            int iPos = result.indexOf("<tbody>");
            if (iPos > 0) {
                String result2 = result.substring(iPos, result.indexOf("</tbody>"));
                while (true) {
                    if (this.ifTransit) {
                        if (result2.indexOf("<tr onmouseover=\"\">") < 0) {
                            break;
                        }
                        TicketInfo ticketInfo = new TicketInfo();
                        String result3 = result2.substring("<td>".length() + result2.indexOf("<td>"));
                        ticketInfo.setA1(result3.substring(0, result3.indexOf("</td>")));
                        String result4 = result3.substring("<td>".length() + result3.indexOf("<td>"));
                        ticketInfo.setDeparture_station(result4.substring(0, result4.indexOf("</td>")));
                        String result5 = result4.substring("\">".length() + result4.indexOf("\">"));
                        ticketInfo.setA2(result5.substring(0, result5.indexOf("</a>")));
                        String result6 = result5.substring("<td>".length() + result5.indexOf("<td>"));
                        ticketInfo.setArrival_station(result6.substring(0, result6.indexOf("</td>")));
                        String result7 = result6.substring("<td>".length() + result6.indexOf("<td>"));
                        ticketInfo.setRun_distance(result7.substring(0, result7.indexOf("</td>")));
                        String result8 = result7.substring("<td>".length() + result7.indexOf("<td>"));
                        ticketInfo.setRun_time(result8.substring(0, result8.indexOf("</td>")));
                        result2 = result8.substring("<a href=\"".length() + result8.indexOf("<a href=\""));
                        ticketInfo.setA3(result2.substring(0, result2.indexOf("\">")));
                        timeList.add(ticketInfo);
                    } else {
                        int iPos2 = result2.indexOf("<tr train=\"");
                        if (iPos2 < 0) {
                            break;
                        }
                        TicketInfo ticketInfo2 = new TicketInfo();
                        String result9 = result2.substring("<tr train=\"".length() + iPos2);
                        ticketInfo2.setTrain_number(result9.substring(0, result9.indexOf("\"")));
                        String result10 = result9.substring("<br />".length() + result9.indexOf("<br />"));
                        ticketInfo2.setTrain_type(result10.substring(0, result10.indexOf("</td>")));
                        String result11 = result10.substring("\" />".length() + result10.indexOf("\" />"));
                        ticketInfo2.setDeparture_station(result11.substring(0, result11.indexOf("</a>")));
                        String result12 = result11.substring("\" />".length() + result11.indexOf("\" />"));
                        ticketInfo2.setArrival_station(result12.substring(0, result12.indexOf("</a>")));
                        String result13 = result12.substring("\">".length() + result12.indexOf("\">"));
                        ticketInfo2.setDeparture_time(result13.substring(0, result13.indexOf("</span>")));
                        String result14 = result13.substring("\">".length() + result13.indexOf("\">"));
                        ticketInfo2.setArrival_time(result14.substring(0, result14.indexOf("</span>")));
                        String result15 = result14.substring("<span>".length() + result14.indexOf("<span>"));
                        ticketInfo2.setRun_time(result15.substring(0, result15.indexOf("<span>")));
                        String result16 = result15.substring("br />".length() + result15.indexOf("br />"));
                        ticketInfo2.setRun_distance(result16.substring(0, result16.indexOf("</td>")));
                        result2 = result16.substring("<td>".length() + result16.indexOf("<td>"));
                        String sTmp = result2.substring(0, result2.indexOf("</td>")).replaceAll("\t", "").replaceAll("<br />", "/").replaceAll(" ", "");
                        if (sTmp.charAt(sTmp.length() - 1) == '/') {
                            sTmp = sTmp.substring(0, sTmp.length() - 1);
                        }
                        ticketInfo2.setPrice_info(sTmp);
                        timeList.add(ticketInfo2);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return timeList;
    }

    public List<TicketInfo> findTrainTimeByNum(TicketInfo maintraininfo, String trainnum, String departDate) {
        String result2;
        String result22;
        ArrayList arrayList = new ArrayList();
        try {
            String sUrl = TRAIN_TIME_NUM + trainnum;
            Log.i("sUrl===", sUrl);
            URLConnection conn = new URL(sUrl).openConnection();
            conn.connect();
            StringBuffer resultBuf = new StringBuffer();
            InputStreamReader inputStreamReader = new InputStreamReader(conn.getInputStream(), "UTF-8");
            BufferedReader bufReader = new BufferedReader(inputStreamReader);
            while (true) {
                String line = bufReader.readLine();
                if (line == null) {
                    break;
                }
                resultBuf.append(line);
            }
            bufReader.close();
            inputStreamReader.close();
            String result = resultBuf.toString();
            int iPos = result.indexOf("T2_checiInfo");
            if (iPos > 0) {
                int iPos2 = result.indexOf("T2_timelist_table");
                String result1 = result.substring(iPos, iPos2);
                String result3 = result.substring(iPos2);
                String result4 = result3.substring(0, result3.indexOf("</table>"));
                String result5 = result4.substring(result4.indexOf("onmouseover="));
                maintraininfo.setTrain_number(trainnum);
                String result12 = result1.substring("<td>".length() + result1.indexOf("<td>"));
                maintraininfo.setRun_distance(result12.substring(0, result12.indexOf("</td>")));
                String result13 = result12.substring("<td>".length() + result12.indexOf("<td>"));
                maintraininfo.setRun_time(result13.substring(0, result13.indexOf("</td>")));
                String result14 = result13.substring("_blank\">".length() + result13.indexOf("_blank\">"));
                maintraininfo.setDeparture_station(result14.substring(0, result14.indexOf("</a></td>")));
                String result15 = result14.substring("_blank\">".length() + result14.indexOf("_blank\">"));
                maintraininfo.setArrival_station(result15.substring(0, result15.indexOf("</a></td>")));
                String result16 = result15.substring("<td>".length() + result15.indexOf("<td>"));
                maintraininfo.setDeparture_time(result16.substring(0, result16.indexOf("</td>")));
                String result17 = result16.substring("<td>".length() + result16.indexOf("<td>"));
                maintraininfo.setArrival_time(result17.substring(0, result17.indexOf("</td>")));
                String result18 = result17.substring("<td>".length() + result17.indexOf("<td>"));
                maintraininfo.setTrain_type(result18.substring(0, result18.indexOf("</td>")));
                String result19 = result18.substring("T2_checi_fares\">".length() + result18.indexOf("T2_checi_fares\">"));
                String sTmp = result19.substring(0, result19.indexOf("<a href")).replaceAll("\t", "").replaceAll("<br />", "/").replaceAll(" ", "").replaceAll("全程", "").replaceAll("\n", "").replaceAll("\r", "");
                if (sTmp.charAt(sTmp.length() - 1) == '/') {
                    sTmp = sTmp.substring(0, sTmp.length() - 1);
                }
                maintraininfo.setPrice_info(sTmp);
                while (true) {
                    int iPos3 = result5.indexOf("onmouseover=");
                    if (iPos3 < 0) {
                        break;
                    }
                    int iPos22 = result5.indexOf("</tr>");
                    String result23 = result5.substring(iPos3, iPos22);
                    result5 = result5.substring("</tr>".length() + iPos22);
                    TicketInfo ticketInfo = new TicketInfo();
                    String result24 = result23.substring("<td>".length() + result23.indexOf("<td>"));
                    ticketInfo.setA1(result24.substring(0, result24.indexOf("</td>")));
                    String result25 = result24.substring("_blank\">".length() + result24.indexOf("_blank\">"));
                    ticketInfo.setDeparture_station(result25.substring(0, result25.indexOf("</a>")));
                    int iPos4 = result25.indexOf("<span class=\"sf\">");
                    if (iPos4 > 0) {
                        result2 = result25.substring("<span class=\"sf\">".length() + iPos4);
                        ticketInfo.setArrival_time(result2.substring(0, result2.indexOf("</span>")));
                    } else {
                        result2 = result25.substring("<td>".length() + result25.indexOf("<td>"));
                        ticketInfo.setArrival_time(result2.substring(0, result2.indexOf("</td>")));
                    }
                    int iPos5 = result2.indexOf("<span class=\"sf\">");
                    if (iPos5 > 0) {
                        result22 = result2.substring("<span class=\"sf\">".length() + iPos5);
                        ticketInfo.setDeparture_time(result22.substring(0, result22.indexOf("</span>")));
                    } else {
                        result22 = result2.substring("<td>".length() + result2.indexOf("<td>"));
                        ticketInfo.setDeparture_time(result22.substring(0, result22.indexOf("</td>")));
                    }
                    String result26 = result22.substring("<td>".length() + result22.indexOf("<td>"));
                    ticketInfo.setRun_time(result26.substring(0, result26.indexOf("</td>")));
                    String result27 = result26.substring("<td>".length() + result26.indexOf("<td>"));
                    ticketInfo.setRun_distance(result27.substring(0, result27.indexOf("</td>")));
                    arrayList.add(ticketInfo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public List<TicketInfo> findTrainTimeByStation(String departure, String departDate) {
        String result2;
        ArrayList arrayList = new ArrayList();
        this.ifTransit = false;
        try {
            String sUrl = TRAIN_TIME_STATION + URLEncoder.encode(departure, "UTF-8");
            Log.i("sUrl===", sUrl);
            URLConnection conn = new URL(sUrl).openConnection();
            conn.connect();
            StringBuffer resultBuf = new StringBuffer();
            InputStreamReader inputStreamReader = new InputStreamReader(conn.getInputStream(), "UTF-8");
            BufferedReader bufReader = new BufferedReader(inputStreamReader);
            while (true) {
                String line = bufReader.readLine();
                if (line == null) {
                    break;
                }
                resultBuf.append(line);
            }
            bufReader.close();
            inputStreamReader.close();
            String result = resultBuf.toString();
            int iPos = result.indexOf("<tbody>");
            if (iPos > 0) {
                String result3 = result.substring(iPos, result.indexOf("</tbody>"));
                while (true) {
                    int iPos2 = result3.indexOf("onmouseover=");
                    if (iPos2 < 0) {
                        break;
                    }
                    int iPos22 = result3.indexOf("</tr>");
                    String result22 = result3.substring(iPos2, iPos22);
                    result3 = result3.substring("</tr>".length() + iPos22);
                    TicketInfo ticketInfo = new TicketInfo();
                    String result23 = result22.substring("<strong>".length() + result22.indexOf("<strong>"));
                    ticketInfo.setTrain_number(result23.substring(0, result23.indexOf("</strong>")));
                    String result24 = result23.substring("\">".length() + result23.indexOf("\">"));
                    ticketInfo.setDeparture_station(result24.substring(0, result24.indexOf("</a>")));
                    String result25 = result24.substring("\">".length() + result24.indexOf("\">"));
                    ticketInfo.setArrival_station(result25.substring(0, result25.indexOf("</a>")));
                    String result26 = result25.substring("<td>".length() + result25.indexOf("<td>"));
                    ticketInfo.setTrain_type(result26.substring(0, result26.indexOf("</td>")));
                    int iPos3 = result26.indexOf("sf start\">");
                    if (iPos3 > 0) {
                        result2 = result26.substring("sf start\">".length() + iPos3);
                        ticketInfo.setArrival_time(result2.substring(0, result2.indexOf("</span>")));
                    } else {
                        int iPos4 = result26.indexOf("middle\">");
                        if (iPos4 > 0) {
                            result2 = result26.substring("middle\">".length() + iPos4);
                            ticketInfo.setArrival_time(result2.substring(0, result2.indexOf("</span>")));
                        } else {
                            result2 = result26.substring("<td>".length() + result26.indexOf("<td>"));
                            ticketInfo.setArrival_time(result2.substring(0, result2.indexOf("</td>")));
                        }
                    }
                    int iPos5 = result2.indexOf("sf end\">");
                    if (iPos5 > 0) {
                        String result27 = result2.substring("sf end\">".length() + iPos5);
                        ticketInfo.setDeparture_time(result27.substring(0, result27.indexOf("</span>")));
                    } else {
                        int iPos6 = result2.indexOf("middle\">");
                        if (iPos6 > 0) {
                            String result28 = result2.substring("middle\">".length() + iPos6);
                            ticketInfo.setDeparture_time(result28.substring(0, result28.indexOf("</span>")));
                        } else {
                            String result29 = result2.substring("<td>".length() + result2.indexOf("<td>"));
                            ticketInfo.setDeparture_time(result29.substring(0, result29.indexOf("</td>")));
                        }
                    }
                    arrayList.add(ticketInfo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public List<TicketInfo> findTrainRemainTicket(String departure, String arrival, String trainnum, String departDate, String passCode, String JSESSIONID, String CiVlkRggf2, String ictStr) {
        ArrayList arrayList = new ArrayList();
        this.ifTransit = false;
        try {
            Log.i("sUrl===", TRAIN_REMAIN_TICKET);
            HttpURLConnection urlConn = (HttpURLConnection) new URL(TRAIN_REMAIN_TICKET).openConnection();
            urlConn.setDoOutput(true);
            urlConn.setDoInput(true);
            urlConn.setRequestMethod("POST");
            urlConn.setUseCaches(false);
            urlConn.setInstanceFollowRedirects(false);
            urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConn.setRequestProperty("Host", "dynamic.12306.cn");
            urlConn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 5.1; rv:5.0) Gecko/20100101 Firefox/5.0");
            urlConn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            urlConn.setRequestProperty("Accept-Language", "zh-cn,zh;q=0.5");
            urlConn.setRequestProperty("Accept-Encoding", "gzip, deflate");
            urlConn.setRequestProperty("Accept-Charset", "GB2312,utf-8;q=0.7,*;q=0.7");
            urlConn.setRequestProperty("Connection", " keep-alive");
            urlConn.setRequestProperty("Referer", "http://dynamic.12306.cn/TrainQuery/leftTicketByStation.jsp");
            urlConn.setRequestProperty("Cookie", "JSESSIONID=" + JSESSIONID + "; CiVlkRggf2=" + CiVlkRggf2);
            urlConn.connect();
            OutputStream out = urlConn.getOutputStream();
            String sPost = String.valueOf(ictStr) + "&fdl=&lx=00&nyear3=" + departDate.substring(0, 4) + "&nyear3_new_value=true&nmonth3=" + departDate.substring(5, 7) + "&nmonth3_new_value=false" + "&nday3=" + departDate.substring(8, 10) + "&nday3_new_value=false&" + "&startStation_ticketLeft=" + chgcode(departure, "liusheng") + "&startStation_ticketLeft_new_value=false" + "&arriveStation_ticketLeft=" + chgcode(arrival, "liusheng") + "&arriveStation_ticketLeft_new_value=false" + "&trainCode=" + trainnum + "&trainCode_new_value=true" + "&rFlag=1&name_ckball=value_ckball&tFlagDC=DC&tFlagZ=Z&tFlagT=T&tFlagK=K&tFlagPK=PK&tFlagPKE=PKE&tFlagLK=LK" + "&randCode=" + passCode;
            Log.i("sPost===", sPost);
            out.write(sPost.getBytes());
            out.flush();
            out.close();
            new StringBuffer();
            InputStreamReader inputStreamReader = new InputStreamReader(urlConn.getInputStream(), "UTF-8");
            BufferedReader bufReader = new BufferedReader(inputStreamReader);
            while (true) {
                String line = bufReader.readLine();
                if (line == null) {
                    bufReader.close();
                    inputStreamReader.close();
                    break;
                }
                int iPos = line.indexOf("alert('");
                if (iPos >= 0) {
                    Log.i("sErrMsg===", line);
                    this.sErrMsg = line.substring("alert('".length() + iPos, line.indexOf("');", iPos));
                    break;
                } else if (line.indexOf("列车余票信息暂无。") >= 0) {
                    Log.i("sErrMsg===", line);
                    this.sErrMsg = this.context.getString(R.string.hint_remain_result);
                    break;
                } else if (line.indexOf("parent.mygrid.addRow(") >= 0) {
                    TicketInfo ticketInfo = new TicketInfo();
                    int iPos2 = line.indexOf("\"");
                    String[] sSpite = line.substring(iPos2, line.indexOf("\"", iPos2 + 1)).split(",");
                    ticketInfo.setA1(sSpite[0]);
                    ticketInfo.setTrain_number(sSpite[1].substring(0, sSpite[1].indexOf(40)));
                    ticketInfo.setFirst_station(sSpite[1].substring(sSpite[1].indexOf(40) + 1, sSpite[1].indexOf(45)));
                    ticketInfo.setEnd_station(sSpite[1].substring(sSpite[1].indexOf(62) + 1, sSpite[1].indexOf(41)));
                    ticketInfo.setDeparture_station(sSpite[2].substring(0, sSpite[2].indexOf(94)));
                    ticketInfo.setArrival_station(sSpite[3].substring(0, sSpite[3].indexOf(94)));
                    ticketInfo.setDeparture_time(sSpite[4]);
                    ticketInfo.setArrival_time(sSpite[5]);
                    ticketInfo.setRun_time(sSpite[6]);
                    String sTmp2 = "";
                    if (!"--".equalsIgnoreCase(sSpite[7])) {
                        sTmp2 = String.valueOf(sTmp2) + "商务座: " + sSpite[7] + "/";
                    }
                    if (!"--".equalsIgnoreCase(sSpite[8])) {
                        sTmp2 = String.valueOf(sTmp2) + "观光座: " + sSpite[8] + "/";
                    }
                    if (!"--".equalsIgnoreCase(sSpite[9])) {
                        sTmp2 = String.valueOf(sTmp2) + "一等包座: " + sSpite[9] + "/";
                    }
                    if (!"--".equalsIgnoreCase(sSpite[10])) {
                        sTmp2 = String.valueOf(sTmp2) + "特等座: " + sSpite[10] + "/";
                    }
                    if (!"--".equalsIgnoreCase(sSpite[11])) {
                        sTmp2 = String.valueOf(sTmp2) + "一等座: " + sSpite[11] + "/";
                    }
                    if (!"--".equalsIgnoreCase(sSpite[12])) {
                        sTmp2 = String.valueOf(sTmp2) + "二等座: " + sSpite[12] + "/";
                    }
                    if (!"--".equalsIgnoreCase(sSpite[13])) {
                        sTmp2 = String.valueOf(sTmp2) + "高级软卧: " + sSpite[13] + "/";
                    }
                    if (!"--".equalsIgnoreCase(sSpite[14])) {
                        sTmp2 = String.valueOf(sTmp2) + "软卧: " + sSpite[14] + "/";
                    }
                    if (!"--".equalsIgnoreCase(sSpite[15])) {
                        sTmp2 = String.valueOf(sTmp2) + "硬卧: " + sSpite[15] + "/";
                    }
                    if (!"--".equalsIgnoreCase(sSpite[16])) {
                        sTmp2 = String.valueOf(sTmp2) + "软座: " + sSpite[16] + "/";
                    }
                    if (!"--".equalsIgnoreCase(sSpite[17])) {
                        sTmp2 = String.valueOf(sTmp2) + "硬座: " + sSpite[17] + "/";
                    }
                    if (!"--".equalsIgnoreCase(sSpite[18])) {
                        sTmp2 = String.valueOf(sTmp2) + "无座: " + sSpite[18] + "/";
                    }
                    ticketInfo.setRemain_info(sTmp2.substring(0, sTmp2.length() - 1));
                    ticketInfo.setTrain_type(sSpite[19]);
                    arrayList.add(ticketInfo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public boolean isIfTransit() {
        return this.ifTransit;
    }

    public void setIfTransit(boolean ifTransit2) {
        this.ifTransit = ifTransit2;
    }

    /* JADX INFO: Multiple debug info for r12v4 java.lang.String: [D('ss' java.lang.String), D('enc_str' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r13v17 char: [D('ii' int), D('pwd' java.lang.String)] */
    public static String chgcode(String str, String pwd) {
        String prand;
        String prand2 = "";
        byte[] bb = pwd.getBytes();
        int i = 0;
        while (true) {
            prand = prand2;
            if (i >= bb.length) {
                break;
            }
            prand2 = String.valueOf(prand) + ((int) bb[i]);
            i++;
        }
        int sPos = (int) Math.floor((double) (prand.length() / 5));
        int mult = Integer.parseInt(new StringBuilder().append(prand.charAt(sPos)).append(prand.charAt(sPos * 2)).append(prand.charAt(sPos * 3)).append(prand.charAt(sPos * 4)).append(prand.charAt(sPos * 5)).toString());
        int incr = (int) Math.ceil((double) (pwd.length() / 2));
        int modu = ((int) Math.pow(2.0d, 31.0d)) - 1;
        int salt = (int) (Math.round(Math.random() * 1.0E9d) % 100000000);
        String prand3 = new StringBuilder(String.valueOf((((String.valueOf(prand) + salt).length() * mult) + incr) % modu)).toString();
        String enc_str = "";
        for (int i2 = 0; i2 < str.length(); i2++) {
            enc_str = String.valueOf(enc_str) + Integer.toHexString(Integer.parseInt(new StringBuilder().append(str.charAt(i2) ^ ((int) Math.floor((Double.parseDouble(prand3) / ((double) modu)) * 255.0d))).toString()));
            prand3 = new StringBuilder().append(((Integer.parseInt(prand3) * mult) + incr) % modu).toString();
        }
        String ss = Integer.toHexString(salt);
        while (ss.length() < 8) {
            ss = "0" + ss;
        }
        return String.valueOf(enc_str) + ss;
    }

    public String getSErrMsg() {
        return this.sErrMsg;
    }

    public void setSErrMsg(String errMsg) {
        this.sErrMsg = errMsg;
    }

    public List<TicketInfo> findSale(String sheng, String city, String shengname, String cityname) {
        ArrayList arrayList = new ArrayList();
        try {
            Log.i("sUrl===", "http://dynamic.12306.cn/TrainQuery/iframeSellTicketStation.jsp");
            HttpURLConnection urlConn = (HttpURLConnection) new URL("http://dynamic.12306.cn/TrainQuery/iframeSellTicketStation.jsp").openConnection();
            urlConn.setDoOutput(true);
            urlConn.setDoInput(true);
            urlConn.setRequestMethod("POST");
            urlConn.setUseCaches(false);
            urlConn.setInstanceFollowRedirects(false);
            urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConn.setRequestProperty("Host", "dynamic.12306.cn");
            urlConn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 5.1; rv:5.0) Gecko/20100101 Firefox/5.0");
            urlConn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            urlConn.setRequestProperty("Accept-Language", "zh-cn,zh;q=0.5");
            urlConn.setRequestProperty("Accept-Encoding", "gzip, deflate");
            urlConn.setRequestProperty("Accept-Charset", "GB2312,utf-8;q=0.7,*;q=0.7");
            urlConn.setRequestProperty("Connection", " keep-alive");
            urlConn.setRequestProperty("Referer", "http://dynamic.12306.cn/TrainQuery/sellTicketStation.jsp");
            urlConn.connect();
            OutputStream out = urlConn.getOutputStream();
            String content = "province=" + URLEncoder.encode(shengname, "UTF-8") + "&province_new_value=false&city=" + URLEncoder.encode(cityname, "UTF-8") + "&city_new_value=false&country=%3D%3D%E8%AF%B7%E9%80%89%E6%8B%A9%3D%3D&country_new_value=true";
            Log.i("content===", content);
            out.write(content.getBytes());
            out.flush();
            out.close();
            StringBuffer resultBuf = new StringBuffer();
            InputStreamReader inputStreamReader = new InputStreamReader(urlConn.getInputStream(), "UTF-8");
            BufferedReader bufReader = new BufferedReader(inputStreamReader);
            while (true) {
                String line = bufReader.readLine();
                if (line == null) {
                    break;
                }
                resultBuf.append(line);
            }
            bufReader.close();
            inputStreamReader.close();
            String result = resultBuf.toString();
            int iPos = result.indexOf("<rows>");
            if (iPos > 0) {
                String result2 = result.substring("<rows>".length() + iPos, result.indexOf("</rows>"));
                while (true) {
                    int iPos2 = result2.indexOf("<row id =");
                    if (iPos2 < 0) {
                        break;
                    }
                    int iPos22 = result2.indexOf("</row>");
                    String result22 = result2.substring(iPos2, iPos22);
                    result2 = result2.substring("</row>".length() + iPos22);
                    TicketInfo ticketInfo = new TicketInfo();
                    int iPos3 = result22.indexOf("</cell><cell>");
                    ticketInfo.setA1(result22.substring("</cell><cell>".length() + iPos3, result22.indexOf("</cell>", iPos3 + 1)));
                    int iPos4 = result22.indexOf("CDATA[ ");
                    int iPos23 = result22.indexOf("^http", iPos4 + 1);
                    ticketInfo.setA2(result22.substring("CDATA[ ".length() + iPos4, iPos23));
                    int iPos5 = result22.indexOf("</cell><cell>", iPos23);
                    ticketInfo.setA3(result22.substring("</cell><cell>".length() + iPos5, result22.indexOf("</cell>", iPos5 + 1)));
                    arrayList.add(ticketInfo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public String findTime(int type, String querytype, String station, String trainnum) {
        String sUrl;
        if (type == 1) {
            try {
                sUrl = "http://dynamic.12306.cn/map_zwdcx/from2.jsp?cz=" + URLEncoder.encode(station, "UTF-8") + "&cc=" + trainnum + "&cxlx=" + querytype + "&rq=" + date.GetLocalTime().substring(0, 10) + "&czEn=" + URLEncoder.encode(station, "UTF-8").replaceAll("%", "-");
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        } else {
            sUrl = "http://dynamic.12306.cn/map_zwdcx/cx.jsp?cz=" + URLEncoder.encode(station, "UTF-8") + "&cc=" + trainnum + "&cxlx=" + querytype + "&rq=" + date.GetLocalTime().substring(0, 10) + "&czEn=" + URLEncoder.encode(station, "UTF-8").replaceAll("%", "-");
        }
        Log.i("sUrl===", sUrl);
        URLConnection conn = new URL(sUrl).openConnection();
        conn.connect();
        StringBuffer resultBuf = new StringBuffer();
        InputStreamReader inputReader = new InputStreamReader(conn.getInputStream(), "GBK");
        BufferedReader bufReader = new BufferedReader(inputReader);
        while (true) {
            String line = bufReader.readLine();
            if (line == null) {
                bufReader.close();
                inputReader.close();
                String result = resultBuf.toString();
                Log.i("result===", result);
                return result;
            }
            resultBuf.append(line);
        }
    }
}
