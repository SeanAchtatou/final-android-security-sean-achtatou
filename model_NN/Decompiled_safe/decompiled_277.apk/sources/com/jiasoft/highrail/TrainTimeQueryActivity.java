package com.jiasoft.highrail;

import android.app.DatePickerDialog;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.highrail.pub.StationListAdapter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TrainTimeQueryActivity extends ParentActivity {
    AutoCompleteTextView arrivestation;
    Calendar cal;
    AutoCompleteTextView departstation;
    SimpleDateFormat df;
    LinearLayout layout1;
    LinearLayout layout2;
    LinearLayout layout3;
    LinearLayout layoutHot1;
    LinearLayout layoutHot2;
    LinearLayout layoutHot3;
    /* access modifiers changed from: private */
    public DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            TrainTimeQueryActivity.this.cal.set(1, arg1);
            TrainTimeQueryActivity.this.cal.set(2, arg2);
            TrainTimeQueryActivity.this.cal.set(5, arg3);
            TrainTimeQueryActivity.this.updateDate();
        }
    };
    Button query1;
    Button query2;
    Button query3;
    int queryType = 1;
    AutoCompleteTextView station;
    StationListAdapter stationlist;
    EditText traindate;
    EditText trainnum;

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0458  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r13) {
        /*
            r12 = this;
            r11 = -2
            r9 = 5
            r8 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            java.lang.String r10 = "DEPARTURE"
            super.onCreate(r13)
            r6 = 2130903059(0x7f030013, float:1.7412925E38)
            r12.setContentView(r6)
            r6 = 2130968593(0x7f040011, float:1.7545844E38)
            r12.setTitle(r6)
            r6 = 2131230801(0x7f080051, float:1.8077665E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.LinearLayout r6 = (android.widget.LinearLayout) r6
            r12.layout1 = r6
            r6 = 2131230802(0x7f080052, float:1.8077667E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.LinearLayout r6 = (android.widget.LinearLayout) r6
            r12.layout2 = r6
            r6 = 2131230814(0x7f08005e, float:1.8077691E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.LinearLayout r6 = (android.widget.LinearLayout) r6
            r12.layout3 = r6
            com.jiasoft.highrail.pub.StationListAdapter r6 = new com.jiasoft.highrail.pub.StationListAdapter
            r6.<init>(r12)
            r12.stationlist = r6
            java.util.Calendar r6 = java.util.Calendar.getInstance()
            r12.cal = r6
            r6 = 2131230776(0x7f080038, float:1.8077614E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.EditText r6 = (android.widget.EditText) r6
            r12.traindate = r6
            r6 = 2131230777(0x7f080039, float:1.8077616E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.AutoCompleteTextView r6 = (android.widget.AutoCompleteTextView) r6
            r12.departstation = r6
            r6 = 2131230778(0x7f08003a, float:1.8077618E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.AutoCompleteTextView r6 = (android.widget.AutoCompleteTextView) r6
            r12.arrivestation = r6
            r6 = 2131230771(0x7f080033, float:1.8077604E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.AutoCompleteTextView r6 = (android.widget.AutoCompleteTextView) r6
            r12.station = r6
            android.widget.AutoCompleteTextView r6 = r12.departstation
            com.jiasoft.highrail.pub.StationListAdapter r7 = r12.stationlist
            r6.setAdapter(r7)
            android.widget.AutoCompleteTextView r6 = r12.arrivestation
            com.jiasoft.highrail.pub.StationListAdapter r7 = r12.stationlist
            r6.setAdapter(r7)
            android.widget.AutoCompleteTextView r6 = r12.station
            com.jiasoft.highrail.pub.StationListAdapter r7 = r12.stationlist
            r6.setAdapter(r7)
            r6 = 2131230772(0x7f080034, float:1.8077606E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.EditText r6 = (android.widget.EditText) r6
            r12.trainnum = r6
            r12.updateDate()
            android.widget.EditText r6 = r12.traindate
            com.jiasoft.highrail.TrainTimeQueryActivity$2 r7 = new com.jiasoft.highrail.TrainTimeQueryActivity$2
            r7.<init>()
            r6.setOnClickListener(r7)
            r6 = 2131230798(0x7f08004e, float:1.8077659E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.Button r6 = (android.widget.Button) r6
            r12.query1 = r6
            android.widget.Button r6 = r12.query1
            com.jiasoft.highrail.TrainTimeQueryActivity$3 r7 = new com.jiasoft.highrail.TrainTimeQueryActivity$3
            r7.<init>()
            r6.setOnClickListener(r7)
            r6 = 2131230799(0x7f08004f, float:1.807766E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.Button r6 = (android.widget.Button) r6
            r12.query2 = r6
            android.widget.Button r6 = r12.query2
            com.jiasoft.highrail.TrainTimeQueryActivity$4 r7 = new com.jiasoft.highrail.TrainTimeQueryActivity$4
            r7.<init>()
            r6.setOnClickListener(r7)
            r6 = 2131230800(0x7f080050, float:1.8077663E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.Button r6 = (android.widget.Button) r6
            r12.query3 = r6
            android.widget.Button r6 = r12.query3
            com.jiasoft.highrail.TrainTimeQueryActivity$5 r7 = new com.jiasoft.highrail.TrainTimeQueryActivity$5
            r7.<init>()
            r6.setOnClickListener(r7)
            r6 = 2131230769(0x7f080031, float:1.80776E38)
            android.view.View r0 = r12.findViewById(r6)
            android.widget.Button r0 = (android.widget.Button) r0
            com.jiasoft.highrail.TrainTimeQueryActivity$6 r6 = new com.jiasoft.highrail.TrainTimeQueryActivity$6
            r6.<init>()
            r0.setOnClickListener(r6)
            r6 = 2131230779(0x7f08003b, float:1.807762E38)
            android.view.View r0 = r12.findViewById(r6)
            android.widget.Button r0 = (android.widget.Button) r0
            com.jiasoft.highrail.TrainTimeQueryActivity$7 r6 = new com.jiasoft.highrail.TrainTimeQueryActivity$7
            r6.<init>()
            r0.setOnClickListener(r6)
            com.jiasoft.highrail.TrainTimeQueryActivity$8 r2 = new com.jiasoft.highrail.TrainTimeQueryActivity$8
            r2.<init>()
            r6 = 2131230780(0x7f08003c, float:1.8077622E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230781(0x7f08003d, float:1.8077624E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230782(0x7f08003e, float:1.8077626E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230783(0x7f08003f, float:1.8077629E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230784(0x7f080040, float:1.807763E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230785(0x7f080041, float:1.8077633E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230786(0x7f080042, float:1.8077635E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230787(0x7f080043, float:1.8077637E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230788(0x7f080044, float:1.8077639E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230789(0x7f080045, float:1.807764E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230790(0x7f080046, float:1.8077643E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230791(0x7f080047, float:1.8077645E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230792(0x7f080048, float:1.8077647E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230793(0x7f080049, float:1.8077649E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230794(0x7f08004a, float:1.807765E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230795(0x7f08004b, float:1.8077653E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230796(0x7f08004c, float:1.8077655E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230797(0x7f08004d, float:1.8077657E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230804(0x7f080054, float:1.8077671E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230805(0x7f080055, float:1.8077673E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230806(0x7f080056, float:1.8077675E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230807(0x7f080057, float:1.8077677E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230808(0x7f080058, float:1.807768E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230809(0x7f080059, float:1.8077681E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230810(0x7f08005a, float:1.8077683E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230811(0x7f08005b, float:1.8077685E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230812(0x7f08005c, float:1.8077687E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230813(0x7f08005d, float:1.807769E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230742(0x7f080016, float:1.8077545E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230743(0x7f080017, float:1.8077547E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230744(0x7f080018, float:1.807755E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230745(0x7f080019, float:1.8077551E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230746(0x7f08001a, float:1.8077553E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230747(0x7f08001b, float:1.8077556E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230748(0x7f08001c, float:1.8077558E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230749(0x7f08001d, float:1.807756E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230750(0x7f08001e, float:1.8077562E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230751(0x7f08001f, float:1.8077564E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230752(0x7f080020, float:1.8077566E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230753(0x7f080021, float:1.8077568E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230754(0x7f080022, float:1.807757E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230755(0x7f080023, float:1.8077572E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230756(0x7f080024, float:1.8077574E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230757(0x7f080025, float:1.8077576E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230758(0x7f080026, float:1.8077578E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230759(0x7f080027, float:1.807758E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230760(0x7f080028, float:1.8077582E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230761(0x7f080029, float:1.8077584E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230762(0x7f08002a, float:1.8077586E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230763(0x7f08002b, float:1.8077588E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230764(0x7f08002c, float:1.807759E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230765(0x7f08002d, float:1.8077592E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230766(0x7f08002e, float:1.8077594E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230767(0x7f08002f, float:1.8077596E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            r6 = 2131230768(0x7f080030, float:1.8077598E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            r6.setOnClickListener(r2)
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams
            r3.<init>(r11, r11)
            r6 = 10
            r3.setMargins(r6, r9, r9, r9)
            r6 = 2131230741(0x7f080015, float:1.8077543E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.LinearLayout r6 = (android.widget.LinearLayout) r6
            r12.layoutHot1 = r6
            com.jiasoft.pub.DatabaseAdapter r6 = r12.dbAdapter
            java.lang.String r7 = "select * from MY_HOT where HOT_TYPE='1' order by ADD_TIME desc"
            android.database.Cursor r1 = r6.rawQuery(r7)
            if (r1 == 0) goto L_0x03ff
            boolean r6 = r1.moveToFirst()
            if (r6 == 0) goto L_0x03ff
        L_0x03bb:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "DEPARTURE"
            int r7 = r1.getColumnIndex(r10)
            java.lang.String r7 = r1.getString(r7)
            java.lang.String r7 = java.lang.String.valueOf(r7)
            r6.<init>(r7)
            java.lang.String r7 = "-"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = "ARRIVAL"
            int r7 = r1.getColumnIndex(r7)
            java.lang.String r7 = r1.getString(r7)
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r4 = r6.toString()
            android.widget.TextView r5 = new android.widget.TextView
            r5.<init>(r12)
            r5.setTextColor(r8)
            r5.setOnClickListener(r2)
            r5.setText(r4)
            android.widget.LinearLayout r6 = r12.layoutHot1
            r6.addView(r5, r3)
            boolean r6 = r1.moveToNext()
            if (r6 != 0) goto L_0x03bb
        L_0x03ff:
            r1.close()
            r6 = 2131230803(0x7f080053, float:1.807767E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.LinearLayout r6 = (android.widget.LinearLayout) r6
            r12.layoutHot2 = r6
            com.jiasoft.pub.DatabaseAdapter r6 = r12.dbAdapter
            java.lang.String r7 = "select * from MY_HOT where HOT_TYPE='2' order by ADD_TIME desc"
            android.database.Cursor r1 = r6.rawQuery(r7)
            if (r1 == 0) goto L_0x0440
            boolean r6 = r1.moveToFirst()
            if (r6 == 0) goto L_0x0440
        L_0x041d:
            java.lang.String r6 = "DEPARTURE"
            int r6 = r1.getColumnIndex(r10)
            java.lang.String r4 = r1.getString(r6)
            android.widget.TextView r5 = new android.widget.TextView
            r5.<init>(r12)
            r5.setTextColor(r8)
            r5.setOnClickListener(r2)
            r5.setText(r4)
            android.widget.LinearLayout r6 = r12.layoutHot2
            r6.addView(r5, r3)
            boolean r6 = r1.moveToNext()
            if (r6 != 0) goto L_0x041d
        L_0x0440:
            r1.close()
            r6 = 2131230815(0x7f08005f, float:1.8077693E38)
            android.view.View r6 = r12.findViewById(r6)
            android.widget.LinearLayout r6 = (android.widget.LinearLayout) r6
            r12.layoutHot3 = r6
            com.jiasoft.pub.DatabaseAdapter r6 = r12.dbAdapter
            java.lang.String r7 = "select * from MY_HOT where HOT_TYPE='3' order by ADD_TIME desc"
            android.database.Cursor r1 = r6.rawQuery(r7)
            if (r1 == 0) goto L_0x0481
            boolean r6 = r1.moveToFirst()
            if (r6 == 0) goto L_0x0481
        L_0x045e:
            java.lang.String r6 = "DEPARTURE"
            int r6 = r1.getColumnIndex(r10)
            java.lang.String r4 = r1.getString(r6)
            android.widget.TextView r5 = new android.widget.TextView
            r5.<init>(r12)
            r5.setTextColor(r8)
            r5.setOnClickListener(r2)
            r5.setText(r4)
            android.widget.LinearLayout r6 = r12.layoutHot3
            r6.addView(r5, r3)
            boolean r6 = r1.moveToNext()
            if (r6 != 0) goto L_0x045e
        L_0x0481:
            r1.close()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jiasoft.highrail.TrainTimeQueryActivity.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: private */
    public void setValue(String ss) {
        try {
            if (this.queryType == 1) {
                this.departstation.setText(ss.substring(0, ss.indexOf("-")));
                this.arrivestation.setText(ss.substring(ss.indexOf("-") + 1));
            } else if (this.queryType == 2) {
                this.trainnum.setText(ss.substring(0, ss.indexOf(" ")));
            } else {
                this.station.setText(ss);
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public void setLayout() {
        if (this.queryType == 1) {
            this.layout1.setVisibility(0);
            this.layout2.setVisibility(8);
            this.layout3.setVisibility(8);
            this.query1.setBackgroundResource(R.drawable.button14);
            this.query2.setBackgroundColor(0);
            this.query3.setBackgroundColor(0);
        } else if (this.queryType == 2) {
            this.layout1.setVisibility(8);
            this.layout2.setVisibility(0);
            this.layout3.setVisibility(8);
            this.query1.setBackgroundColor(0);
            this.query3.setBackgroundColor(0);
            this.query2.setBackgroundResource(R.drawable.button14);
        } else {
            this.layout1.setVisibility(8);
            this.layout2.setVisibility(8);
            this.layout3.setVisibility(0);
            this.query1.setBackgroundColor(0);
            this.query2.setBackgroundColor(0);
            this.query3.setBackgroundResource(R.drawable.button14);
        }
    }

    /* access modifiers changed from: private */
    public void updateDate() {
        this.df = new SimpleDateFormat("yyyy-MM-dd");
        this.traindate.setText(this.df.format(this.cal.getTime()));
    }
}
