package com.jiasoft.highrail;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.pub.Android;
import com.jiasoft.pub.ImageProc;
import com.jiasoft.pub.SrvProxy;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class TrainTicketActivity extends ParentActivity {
    String CiVlkRggf2 = "";
    String JSESSIONID = "";
    String T0egrhEC8V = "";
    String arrivestation = "";
    String departstation = "";
    ListView gridview;
    String ictStr = "";
    TrainTicketListAdapter listadapter;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case -1:
                    TextView tv = (TextView) TrainTicketActivity.this.findViewById(R.id.trainhint);
                    TrainTicketActivity.this.gridview.setAdapter((ListAdapter) TrainTicketActivity.this.listadapter);
                    tv.setText(String.valueOf(TrainTicketActivity.this.traindate) + " " + TrainTicketActivity.this.departstation + "-" + TrainTicketActivity.this.arrivestation + "共有" + TrainTicketActivity.this.listadapter.getCount() + "次列车有余票");
                    if (TrainTicketActivity.this.listadapter.getCount() <= 0) {
                        Android.EMsgDlg(TrainTicketActivity.this, TrainTicketActivity.this.listadapter.getUpdateData().getSErrMsg());
                        return;
                    } else {
                        Toast.makeText(TrainTicketActivity.this, tv.getText().toString(), 0).show();
                        return;
                    }
                default:
                    return;
            }
        }
    };
    String passCode = "";
    String traindate = "";
    String trainnum = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.maintrainticket);
        setTitle((int) R.string.tv_train_ticketremaining);
        try {
            Bundle myBundle = getIntent().getExtras();
            this.traindate = myBundle.getString("traindate");
            this.departstation = myBundle.getString("departstation");
            this.arrivestation = myBundle.getString("arrivestation");
            this.trainnum = myBundle.getString("trainnum");
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.gridview = (ListView) findViewById(R.id.gridview);
        this.listadapter = new TrainTicketListAdapter(this);
        showInput();
    }

    private static class TrustAnyTrustManager implements X509TrustManager {
        private TrustAnyTrustManager() {
        }

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }

    private static class TrustAnyHostnameVerifier implements HostnameVerifier {
        private TrustAnyHostnameVerifier() {
        }

        public boolean verify(String arg0, SSLSession arg1) {
            return true;
        }
    }

    private void getPassCode() {
        int ii;
        String sresult;
        try {
            HttpGet httpRequest = new HttpGet("http://dynamic.12306.cn/TrainQuery/leftTicketByStation.jsp");
            httpRequest.addHeader("", "");
            httpRequest.addHeader("Host", "dynamic.12306.cn");
            httpRequest.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 5.1; rv:5.0) Gecko/20100101 Firefox/5.0");
            httpRequest.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            httpRequest.addHeader("Accept-Language", "zh-cn,zh;q=0.5");
            httpRequest.addHeader("Accept-Encoding", "gzip, deflate");
            httpRequest.addHeader("Accept-Charset", "GB2312,utf-8;q=0.7,*;q=0.7");
            httpRequest.addHeader("Connection", " keep-alive");
            HttpResponse httpResponse = new DefaultHttpClient().execute(httpRequest);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                Header[] header = httpResponse.getHeaders("Set-Cookie");
                for (int i = 0; i < header.length; i++) {
                    String sCookie = header[i].getValue();
                    int iPos1 = sCookie.indexOf("CiVlkRggf2=");
                    if (iPos1 >= 0) {
                        this.CiVlkRggf2 = sCookie.substring("CiVlkRggf2=".length() + iPos1, sCookie.indexOf(";", iPos1));
                    }
                    int iPos12 = sCookie.indexOf("JSESSIONID=");
                    if (iPos12 >= 0) {
                        this.JSESSIONID = sCookie.substring("JSESSIONID=".length() + iPos12, sCookie.indexOf(";", iPos12));
                    }
                }
                Log.i("sCookie GET====", "CiVlkRggf2=" + this.CiVlkRggf2 + ":JSESSIONID=" + this.JSESSIONID);
                this.ictStr = "";
                InputStream instream = httpResponse.getEntity().getContent();
                Header contentEncoding = httpResponse.getFirstHeader("Content-Encoding");
                if (contentEncoding == null || !contentEncoding.getValue().equalsIgnoreCase("gzip")) {
                    sresult = SrvProxy.readData(instream, "UTF-8");
                } else {
                    sresult = SrvProxy.readDataForZgip(instream, "UTF-8");
                }
                try {
                    int iPos13 = sresult.indexOf("ict");
                    this.ictStr = sresult.substring(iPos13, iPos13 + 4);
                    int iPos2 = sresult.indexOf("value=\"", iPos13);
                    int iPos3 = sresult.indexOf("\"/>", iPos2);
                    if (iPos3 > iPos2) {
                        this.ictStr = String.valueOf(this.ictStr) + "=" + sresult.substring("value=\"".length() + iPos2, iPos3);
                    }
                } catch (Exception e) {
                }
                Log.i("ictStr=1==", "ictStr:::" + this.ictStr);
            } else {
                Log.i("strResult===", "请求错误");
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if ("".equalsIgnoreCase(this.ictStr)) {
            this.ictStr = "ictM=6569";
        }
        try {
            String httpUrl = "http://dynamic.12306.cn/TrainQuery/passCodeAction.do?rand=rrand?" + Math.random();
            Log.i("httpUrl====", httpUrl);
            HttpURLConnection conn = (HttpURLConnection) new URL(httpUrl).openConnection();
            conn.setRequestProperty("Host", "dynamic.12306.cn");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 5.1; rv:5.0) Gecko/20100101 Firefox/5.0");
            conn.setRequestProperty("Accept", "image/png,image/*;q=0.8,*/*;q=0.5");
            conn.setRequestProperty("Accept-Language", "zh-cn,zh;q=0.5");
            conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
            conn.setRequestProperty("Accept-Charset", "GB2312,utf-8;q=0.7,*;q=0.7");
            conn.setRequestProperty("Connection", " keep-alive");
            conn.setRequestProperty("Referer", "http://dynamic.12306.cn/TrainQuery/leftTicketByStation.jsp");
            conn.setRequestProperty("Cookie", "JSESSIONID=" + this.JSESSIONID + "; CiVlkRggf2=" + this.CiVlkRggf2);
            conn.setUseCaches(false);
            conn.setInstanceFollowRedirects(false);
            conn.connect();
            InputStream is = conn.getInputStream();
            File file = new File("/sdcard/jiasoft/highrailtime/passcode.jia");
            File file2 = new File(file.getParent());
            if (!file2.exists()) {
                file2.mkdirs();
            }
            if (file.exists()) {
                file.delete();
            }
            FileOutputStream fileOutputStream = new FileOutputStream("/sdcard/jiasoft/highrailtime/passcode.jia");
            byte[] contentByte = new byte[1024];
            do {
                ii = is.read(contentByte);
                if (ii > 0) {
                    fileOutputStream.write(contentByte, 0, ii);
                    continue;
                }
            } while (ii > 0);
            fileOutputStream.close();
            is.close();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private void showInput() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.hint_checkcode_please);
        builder.setIcon((int) R.drawable.dlgquest);
        final View passcodeview = LayoutInflater.from(this).inflate((int) R.layout.dialogpasscode, (ViewGroup) null);
        ImageView image1 = (ImageView) passcodeview.findViewById(R.id.image1);
        getPassCode();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int widthPixels = dm.widthPixels;
        if (widthPixels > dm.heightPixels) {
            widthPixels = dm.heightPixels;
        }
        Log.i("widthPixels===", new StringBuilder(String.valueOf(widthPixels)).toString());
        image1.setImageBitmap(ImageProc.getResizePicture(BitmapFactory.decodeFile("/sdcard/jiasoft/highrailtime/passcode.jia"), ((float) widthPixels) / 240.0f));
        builder.setView(passcodeview);
        builder.setPositiveButton((int) R.string.btn_sure, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                TrainTicketActivity.this.passCode = ((EditText) passcodeview.findViewById(R.id.passcode)).getText().toString().trim();
                TrainTicketActivity.this.getList();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton((int) R.string.btn_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void getList() {
        ((TextView) findViewById(R.id.trainhint)).setText((int) R.string.hint_querying);
        new Thread() {
            public void run() {
                try {
                    TrainTicketActivity.this.listadapter.getDataList(TrainTicketActivity.this.departstation, TrainTicketActivity.this.arrivestation, TrainTicketActivity.this.trainnum, TrainTicketActivity.this.traindate, TrainTicketActivity.this.passCode, TrainTicketActivity.this.JSESSIONID, TrainTicketActivity.this.CiVlkRggf2, TrainTicketActivity.this.ictStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                SrvProxy.sendMsg(TrainTicketActivity.this.mHandler, -1);
            }
        }.start();
    }
}
