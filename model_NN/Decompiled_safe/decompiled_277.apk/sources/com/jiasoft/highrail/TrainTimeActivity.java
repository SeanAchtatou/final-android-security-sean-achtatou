package com.jiasoft.highrail;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.pub.Android;
import com.jiasoft.pub.SrvProxy;

public class TrainTimeActivity extends ParentActivity {
    String arrivestation = "";
    String departstation = "";
    ListView gridview;
    TrainTimeListAdapter listadapter;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case -1:
                    TextView tv = (TextView) TrainTimeActivity.this.findViewById(R.id.trainhint);
                    if (TrainTimeActivity.this.listadapter.getUpdateData().isIfTransit()) {
                        TrainTimeActivity.this.gridview.setAdapter((ListAdapter) TrainTimeActivity.this.listadapter);
                        tv.setText("没有直达列车,为您推荐以下中转方案,请参考");
                        Toast.makeText(TrainTimeActivity.this, "没有直达列车,为您推荐了以下中转方案,请参考", 0).show();
                        return;
                    }
                    TrainTimeActivity.this.gridview.setAdapter((ListAdapter) TrainTimeActivity.this.listadapter);
                    tv.setText(String.valueOf(TrainTimeActivity.this.departstation) + "-" + TrainTimeActivity.this.arrivestation + "共有" + TrainTimeActivity.this.listadapter.getCount() + "次直达列车");
                    if (TrainTimeActivity.this.listadapter.getCount() <= 0) {
                        Android.EMsgDlg(TrainTimeActivity.this, "无数据，请检查网络状态或者发到站名称是否正确！");
                        return;
                    } else {
                        Toast.makeText(TrainTimeActivity.this, tv.getText().toString(), 0).show();
                        return;
                    }
                default:
                    return;
            }
        }
    };
    String traindate = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.maintraintime);
        setTitle((int) R.string.tv_train_schedules);
        try {
            Bundle myBundle = getIntent().getExtras();
            this.traindate = myBundle.getString("traindate");
            this.departstation = myBundle.getString("departstation");
            this.arrivestation = myBundle.getString("arrivestation");
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.gridview = (ListView) findViewById(R.id.gridview);
        this.listadapter = new TrainTimeListAdapter(this);
        getList();
    }

    private void getList() {
        ((TextView) findViewById(R.id.trainhint)).setText((int) R.string.hint_querying);
        new Thread() {
            public void run() {
                try {
                    TrainTimeActivity.this.listadapter.getDataList(TrainTimeActivity.this.departstation, TrainTimeActivity.this.arrivestation, TrainTimeActivity.this.traindate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                SrvProxy.sendMsg(TrainTimeActivity.this.mHandler, -1);
            }
        }.start();
    }
}
