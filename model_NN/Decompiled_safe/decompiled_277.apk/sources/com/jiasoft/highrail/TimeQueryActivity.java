package com.jiasoft.highrail;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.highrail.pub.StationListAdapter;
import com.jiasoft.highrail.pub.UpdateData;
import com.jiasoft.pub.Android;
import com.jiasoft.pub.CodeRadio;
import com.jiasoft.pub.SrvProxy;
import com.jiasoft.pub.date;

public class TimeQueryActivity extends ParentActivity {
    LinearLayout layoutHot1;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case -1:
                    TimeQueryActivity.this.queryresult.setText(TimeQueryActivity.this.sResult);
                    Toast.makeText(TimeQueryActivity.this, "查询完成", 0).show();
                    return;
                default:
                    return;
            }
        }
    };
    TextView queryresult;
    CodeRadio querytype;
    String sResult = "";
    AutoCompleteTextView station;
    StationListAdapter stationlist;
    EditText trainnum;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.maintimequery);
        setTitle((int) R.string.tv_train_dynamic);
        this.stationlist = new StationListAdapter(this);
        this.querytype = (CodeRadio) findViewById(R.id.querytype);
        this.trainnum = (EditText) findViewById(R.id.trainnum);
        this.station = (AutoCompleteTextView) findViewById(R.id.station);
        this.station.setAdapter(this.stationlist);
        this.queryresult = (TextView) findViewById(R.id.queryresult);
        ((Button) findViewById(R.id.querytime)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String code = TimeQueryActivity.this.querytype.getCode();
                String sStation = TimeQueryActivity.this.station.getText().toString().trim();
                String sTrainnum = TimeQueryActivity.this.trainnum.getText().toString().trim();
                if ("".equalsIgnoreCase(sStation)) {
                    Android.EMsgDlg(TimeQueryActivity.this, TimeQueryActivity.this.getString(R.string.hint_station_please));
                    TimeQueryActivity.this.station.requestFocus();
                } else if ("".equalsIgnoreCase(sTrainnum)) {
                    Android.EMsgDlg(TimeQueryActivity.this, TimeQueryActivity.this.getString(R.string.hint_train_num_please));
                    TimeQueryActivity.this.trainnum.requestFocus();
                } else {
                    if (!"".equalsIgnoreCase(sStation) && !"".equalsIgnoreCase(sTrainnum)) {
                        UpdateData.saveToHot(TimeQueryActivity.this, "5", sTrainnum, "");
                    }
                    TimeQueryActivity.this.queryresult.setText((int) R.string.hint_querying);
                    new Thread() {
                        public void run() {
                            try {
                                UpdateData updatedata = new UpdateData(TimeQueryActivity.this, null);
                                String s1 = updatedata.findTime(1, TimeQueryActivity.this.querytype.getCode(), TimeQueryActivity.this.station.getText().toString().trim(), TimeQueryActivity.this.trainnum.getText().toString().trim());
                                if (s1.indexOf("请重新输入") >= 0) {
                                    TimeQueryActivity.this.sResult = "未能查到车站、车次信息，请重新输入！";
                                    TimeQueryActivity.this.station.requestFocus();
                                } else {
                                    TimeQueryActivity.this.sResult = updatedata.findTime(2, TimeQueryActivity.this.querytype.getCode(), TimeQueryActivity.this.station.getText().toString().trim(), TimeQueryActivity.this.trainnum.getText().toString().trim());
                                    TimeQueryActivity.this.sResult = String.valueOf(s1) + "," + TimeQueryActivity.this.sResult;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            SrvProxy.sendMsg(TimeQueryActivity.this.mHandler, -1);
                        }
                    }.start();
                }
            }
        });
        ((Button) findViewById(R.id.querytrainnum)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String sStation = TimeQueryActivity.this.station.getText().toString().trim();
                if ("".equalsIgnoreCase(sStation)) {
                    Android.EMsgDlg(TimeQueryActivity.this, TimeQueryActivity.this.getString(R.string.hint_station_please));
                    TimeQueryActivity.this.station.requestFocus();
                    return;
                }
                Intent intent = new Intent();
                Bundle myBundelForName = new Bundle();
                myBundelForName.putString("traindate", date.GetLocalTime().substring(0, 10));
                myBundelForName.putString("station", sStation);
                myBundelForName.putString("type", "1");
                intent.putExtras(myBundelForName);
                intent.setClass(TimeQueryActivity.this, TrainTimeByStationActivity.class);
                TimeQueryActivity.this.startActivityForResult(intent, 0);
            }
        });
        View.OnClickListener hotListen = new View.OnClickListener() {
            public void onClick(View v) {
                TimeQueryActivity.this.setValue(((TextView) v).getText().toString().trim());
            }
        };
        ((TextView) findViewById(R.id.hot200)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot201)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot202)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot203)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot204)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot205)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot206)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot207)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot208)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot209)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot210)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot211)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot212)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot213)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot214)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot215)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot216)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot217)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot218)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot219)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot220)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot221)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot222)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot223)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot224)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot225)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot226)).setOnClickListener(hotListen);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(-2, -2);
        param.setMargins(10, 5, 5, 5);
        this.layoutHot1 = (LinearLayout) findViewById(R.id.layoutHot1);
        Cursor cur = this.dbAdapter.rawQuery("select * from MY_HOT where HOT_TYPE='5' order by ADD_TIME desc");
        if (cur == null || !cur.moveToFirst()) {
            cur.close();
        }
        do {
            TextView tv = new TextView(this);
            tv.setTextColor(-16777216);
            tv.setOnClickListener(hotListen);
            tv.setText(String.valueOf(cur.getString(cur.getColumnIndex("DEPARTURE"))) + "  " + cur.getString(cur.getColumnIndex("ARRIVAL")));
            this.layoutHot1.addView(tv, param);
        } while (cur.moveToNext());
        cur.close();
    }

    /* access modifiers changed from: private */
    public void setValue(String ss) {
        int iPos = ss.indexOf("  ");
        if (iPos >= 0) {
            this.station.setText(ss.substring(0, iPos));
            this.trainnum.setText(ss.substring(iPos + 2));
            return;
        }
        this.station.setText(ss);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            this.trainnum.setText(data.getExtras().getString("trainnum"));
        }
    }
}
