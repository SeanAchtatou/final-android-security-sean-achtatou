package com.jiasoft.highrail;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.guohead.sdk.GuoheAdManager;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.pub.AndPub;
import com.jiasoft.pub.Android;
import com.jiasoft.pub.AppUpdate;
import com.jiasoft.pub.SrvProxy;
import com.mobclick.android.MobclickAgent;
import com.mobclick.android.UmengFeedbackListener;

public class MoreActivity extends ParentActivity implements UmengFeedbackListener {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mainmore);
        setTitle((int) R.string.tv_more);
        setAdShow();
        MobclickAgent.setFeedbackListener(this);
        MobclickAgent.onResume(this);
        ((LinearLayout) findViewById(R.id.myhot)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MoreActivity.this, MyhotActivity.class);
                MoreActivity.this.startActivity(intent);
            }
        });
        ((LinearLayout) findViewById(R.id.help)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Android.IMsgDlg(MoreActivity.this, MoreActivity.this.getString(R.string.tv_help), AndPub.readAssetTextFile(MoreActivity.this, "help.txt").toString());
            }
        });
        ((LinearLayout) findViewById(R.id.about)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Android.IMsgDlg(MoreActivity.this, MoreActivity.this.getString(R.string.tv_about), AndPub.readAssetTextFile(MoreActivity.this, "about.txt").toString());
            }
        });
        ((LinearLayout) findViewById(R.id.suggest)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SrvProxy.isNetworkConnected(MoreActivity.this)) {
                    MobclickAgent.openFeedbackActivity(MoreActivity.this);
                } else {
                    Toast.makeText(MoreActivity.this, "未联网，无法执行意见反馈", 0).show();
                }
            }
        });
        ((LinearLayout) findViewById(R.id.download)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AppUpdate appupdate = new AppUpdate(MoreActivity.this, null);
                int iCheck = appupdate.checkAppUpdate();
                if (iCheck == 0) {
                    Toast.makeText(MoreActivity.this, "当前应用已是最新版本,谢谢您的使用!", 0).show();
                } else if (iCheck == 1) {
                    Android.QMsgDlg(MoreActivity.this, "应用更新确认", "发现新版本:\n" + appupdate.updateinfo + "\n\n是否确认更新？", appupdate.appUpdateSure, null);
                } else if (iCheck == 2) {
                    Toast.makeText(MoreActivity.this, "检查更新失败,请检查您的网络情况!", 0).show();
                }
            }
        });
    }

    public void onFeedbackReturned(int arg0) {
        if (arg0 == 0) {
            Toast.makeText(this, "反馈发送成功,非常感谢您的意见!", 0).show();
        } else {
            Toast.makeText(this, "反馈发送失败,请检查您的网络情况!", 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        MobclickAgent.onPause(this);
        GuoheAdManager.finish(this);
        super.onDestroy();
    }
}
