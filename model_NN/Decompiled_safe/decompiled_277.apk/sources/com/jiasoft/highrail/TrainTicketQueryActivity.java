package com.jiasoft.highrail;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.highrail.pub.StationListAdapter;
import com.jiasoft.highrail.pub.UpdateData;
import com.jiasoft.pub.Android;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TrainTicketQueryActivity extends ParentActivity {
    AutoCompleteTextView arrivestation;
    Calendar cal;
    AutoCompleteTextView departstation;
    SimpleDateFormat df;
    LinearLayout layoutHot1;
    /* access modifiers changed from: private */
    public DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            TrainTicketQueryActivity.this.cal.set(1, arg1);
            TrainTicketQueryActivity.this.cal.set(2, arg2);
            TrainTicketQueryActivity.this.cal.set(5, arg3);
            TrainTicketQueryActivity.this.updateDate();
        }
    };
    StationListAdapter stationlist;
    EditText traindate;
    EditText trainnum;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.maintrainticketquery);
        setTitle((int) R.string.tv_train_ticketremaining);
        this.stationlist = new StationListAdapter(this);
        this.cal = Calendar.getInstance();
        this.traindate = (EditText) findViewById(R.id.traindate);
        this.departstation = (AutoCompleteTextView) findViewById(R.id.departstation);
        this.arrivestation = (AutoCompleteTextView) findViewById(R.id.arrivestation);
        this.departstation.setAdapter(this.stationlist);
        this.arrivestation.setAdapter(this.stationlist);
        this.trainnum = (EditText) findViewById(R.id.trainnum);
        updateDate();
        this.traindate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new DatePickerDialog(TrainTicketQueryActivity.this, TrainTicketQueryActivity.this.listener, TrainTicketQueryActivity.this.cal.get(1), TrainTicketQueryActivity.this.cal.get(2), TrainTicketQueryActivity.this.cal.get(5)).show();
            }
        });
        ((Button) findViewById(R.id.querytime)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String sDate = TrainTicketQueryActivity.this.traindate.getText().toString().trim();
                String sDepartstation = TrainTicketQueryActivity.this.departstation.getText().toString().trim();
                String sArrivestation = TrainTicketQueryActivity.this.arrivestation.getText().toString().trim();
                String sTrainnum = TrainTicketQueryActivity.this.trainnum.getText().toString().trim();
                if (!"".equalsIgnoreCase(sTrainnum) || (!"".equalsIgnoreCase(sDepartstation) && !"".equalsIgnoreCase(sArrivestation))) {
                    if (!"".equalsIgnoreCase(sDepartstation) && !"".equalsIgnoreCase(sArrivestation)) {
                        UpdateData.saveToHot(TrainTicketQueryActivity.this, "1", sDepartstation, sArrivestation);
                    }
                    Intent intent = new Intent();
                    Bundle myBundelForName = new Bundle();
                    myBundelForName.putString("traindate", sDate);
                    myBundelForName.putString("departstation", sDepartstation);
                    myBundelForName.putString("arrivestation", sArrivestation);
                    myBundelForName.putString("trainnum", sTrainnum);
                    intent.putExtras(myBundelForName);
                    intent.setClass(TrainTicketQueryActivity.this, TrainTicketActivity.class);
                    TrainTicketQueryActivity.this.startActivity(intent);
                    return;
                }
                Android.EMsgDlg(TrainTicketQueryActivity.this, TrainTicketQueryActivity.this.getString(R.string.hint_d_and_a_please));
                TrainTicketQueryActivity.this.trainnum.requestFocus();
            }
        });
        ((Button) findViewById(R.id.change)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String ss = TrainTicketQueryActivity.this.departstation.getText().toString().trim();
                TrainTicketQueryActivity.this.departstation.setText(TrainTicketQueryActivity.this.arrivestation.getText().toString().trim());
                TrainTicketQueryActivity.this.arrivestation.setText(ss);
            }
        });
        View.OnClickListener hotListen = new View.OnClickListener() {
            public void onClick(View v) {
                TrainTicketQueryActivity.this.setValue(((TextView) v).getText().toString().trim());
            }
        };
        ((TextView) findViewById(R.id.hot1)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot2)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot3)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot4)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot5)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot6)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot7)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot8)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot9)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot10)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot11)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot12)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot13)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot14)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot15)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot16)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot17)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot18)).setOnClickListener(hotListen);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(-2, -2);
        param.setMargins(10, 5, 5, 5);
        this.layoutHot1 = (LinearLayout) findViewById(R.id.layoutHot1);
        Cursor cur = this.dbAdapter.rawQuery("select * from MY_HOT where HOT_TYPE='1' order by ADD_TIME desc");
        if (cur == null || !cur.moveToFirst()) {
            cur.close();
        }
        do {
            TextView tv = new TextView(this);
            tv.setTextColor(-16777216);
            tv.setOnClickListener(hotListen);
            tv.setText(String.valueOf(cur.getString(cur.getColumnIndex("DEPARTURE"))) + "-" + cur.getString(cur.getColumnIndex("ARRIVAL")));
            this.layoutHot1.addView(tv, param);
        } while (cur.moveToNext());
        cur.close();
    }

    /* access modifiers changed from: private */
    public void setValue(String ss) {
        try {
            this.departstation.setText(ss.substring(0, ss.indexOf("-")));
            this.arrivestation.setText(ss.substring(ss.indexOf("-") + 1));
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public void updateDate() {
        this.df = new SimpleDateFormat("yyyy-MM-dd");
        this.traindate.setText(this.df.format(this.cal.getTime()));
    }
}
