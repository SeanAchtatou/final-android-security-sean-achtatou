package com.jiasoft.highrail;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.highrail.pub.TicketInfo;
import com.jiasoft.highrail.pub.UpdateData;
import java.util.List;

public class SaleListAdapter extends BaseAdapter {
    private ParentActivity mContext;
    public List<TicketInfo> traintimeList;
    private UpdateData updateData = new UpdateData(this.mContext, null);

    public SaleListAdapter(ParentActivity c) {
        this.mContext = c;
    }

    public void getDataList(String sheng, String city, String shengname, String cityname) {
        this.traintimeList = this.updateData.findSale(sheng, city, shengname, cityname);
    }

    public int getCount() {
        return this.traintimeList.size();
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.mContext).inflate((int) R.layout.adaptersale, (ViewGroup) null);
            holder = new ViewHolder();
            holder.traininfo = (TextView) convertView.findViewById(R.id.traininfo);
            holder.timeinfo = (TextView) convertView.findViewById(R.id.timeinfo);
            holder.ticketinfo = (TextView) convertView.findViewById(R.id.ticketinfo);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        TicketInfo ticketinfo = this.traintimeList.get(position);
        holder.traininfo.setText(ticketinfo.getA1());
        holder.timeinfo.setText(ticketinfo.getA2());
        holder.ticketinfo.setText(ticketinfo.getA3());
        return convertView;
    }

    static class ViewHolder {
        TextView ticketinfo;
        TextView timeinfo;
        TextView traininfo;

        ViewHolder() {
        }
    }

    public UpdateData getUpdateData() {
        return this.updateData;
    }

    public void setUpdateData(UpdateData updateData2) {
        this.updateData = updateData2;
    }
}
