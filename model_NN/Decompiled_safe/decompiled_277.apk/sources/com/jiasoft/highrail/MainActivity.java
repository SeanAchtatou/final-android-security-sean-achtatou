package com.jiasoft.highrail;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.pub.Android;
import com.jiasoft.pub.AppUpdate;
import com.jiasoft.pub.PC_SYS_CONFIG;
import com.jiasoft.pub.SrvProxy;
import com.mobclick.android.MobclickAgent;

public class MainActivity extends ParentActivity {
    AppUpdate appupdate;
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == -1) {
                return;
            }
            if (msg.what == -2) {
                try {
                    if (MainActivity.this.appupdate.newvercode > Integer.parseInt(PC_SYS_CONFIG.getConfValue(MainActivity.this, "CURR_APP_VERCODE", new StringBuilder(String.valueOf(MainActivity.this.getPackageManager().getPackageInfo(MainActivity.this.getPackageName(), 0).versionCode)).toString()))) {
                        Android.QMsgDlg(MainActivity.this, MainActivity.this.getString(R.string.hint_soft_update), "发现新版本:\n" + MainActivity.this.appupdate.updateinfo + "\n按[确认]执行更新\n按[取消]取消更新(本版本不再提示更新)", MainActivity.this.appupdate.appUpdateSure, MainActivity.this.appupdate.appUpdateCancel);
                    }
                } catch (Exception e) {
                }
            } else if (msg.what == -3) {
                Toast.makeText(MainActivity.this, "应用更新失败", 0).show();
            }
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        MobclickAgent.onError(this);
        MobclickAgent.onResume(this);
        ((LinearLayout) findViewById(R.id.tickettime)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, TrainTimeQueryActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });
        ((LinearLayout) findViewById(R.id.remaintichet)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, TrainTicketQueryActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });
        ((LinearLayout) findViewById(R.id.saleticketpos)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, SaleQueryActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });
        ((LinearLayout) findViewById(R.id.traintime)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, TimeQueryActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });
        ((LinearLayout) findViewById(R.id.more)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, MoreActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });
        this.appupdate = new AppUpdate(this, this.mHandler);
        if (SrvProxy.isWifiConnected(this) && this.appupdate.checkAppUpdate() == 1) {
            SrvProxy.sendMsg(this.mHandler, -2);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.hint_title_exit_confirm);
        builder.setIcon((int) R.drawable.dlgquest);
        builder.setMessage((int) R.string.hint_exit_soft);
        builder.setPositiveButton((int) R.string.btn_sure, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                MainActivity.this.finish();
            }
        });
        builder.setNegativeButton((int) R.string.btn_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        builder.create().show();
        return false;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        MobclickAgent.onPause(this);
        super.onDestroy();
    }
}
