package com.jiasoft.highrail;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.highrail.pub.TicketInfo;
import com.jiasoft.highrail.pub.UpdateData;
import com.jiasoft.pub.date;
import java.util.List;

public class TrainTimeListAdapter extends BaseAdapter {
    /* access modifiers changed from: private */
    public ParentActivity mContext;
    public List<TicketInfo> traintimeList;
    private UpdateData updateData = new UpdateData(this.mContext, null);

    public TrainTimeListAdapter(ParentActivity c) {
        this.mContext = c;
    }

    public void getDataList(String departure, String arrival, String departDate) {
        this.traintimeList = this.updateData.findTrainTime(departure, arrival, departDate);
    }

    public int getCount() {
        return this.traintimeList.size();
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.mContext).inflate((int) R.layout.adaptertraintime, (ViewGroup) null);
            holder = new ViewHolder();
            holder.traininfo = (TextView) convertView.findViewById(R.id.traininfo);
            holder.timeinfo = (TextView) convertView.findViewById(R.id.timeinfo);
            holder.ticketinfo = (TextView) convertView.findViewById(R.id.ticketinfo);
            holder.query = (Button) convertView.findViewById(R.id.query);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final TicketInfo ticketinfo = this.traintimeList.get(position);
        if (!this.updateData.isIfTransit()) {
            holder.traininfo.setText(String.valueOf(ticketinfo.getTrain_number()) + " [" + ticketinfo.getTrain_type() + "]");
            holder.timeinfo.setText(String.valueOf(ticketinfo.getDeparture_station()) + "-" + ticketinfo.getArrival_station() + " [" + ticketinfo.getDeparture_time() + "-" + ticketinfo.getArrival_time() + "] " + ticketinfo.getRun_time() + "/" + ticketinfo.getRun_distance());
            holder.ticketinfo.setText(ticketinfo.getPrice_info());
            holder.query.setText((int) R.string.btn_traindetail);
            holder.query.setVisibility(0);
            holder.query.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent();
                    Bundle myBundelForName = new Bundle();
                    myBundelForName.putString("traindate", date.GetLocalTime().substring(0, 10));
                    myBundelForName.putString("trainnum", ticketinfo.getTrain_number());
                    intent.putExtras(myBundelForName);
                    intent.setClass(TrainTimeListAdapter.this.mContext, TrainTimeByNumActivity.class);
                    TrainTimeListAdapter.this.mContext.startActivity(intent);
                }
            });
        } else {
            holder.traininfo.setText(ticketinfo.getA1());
            holder.timeinfo.setText(String.valueOf(ticketinfo.getDeparture_station()) + "-" + ticketinfo.getA2() + "-" + ticketinfo.getArrival_station());
            holder.ticketinfo.setText(String.valueOf(ticketinfo.getRun_time()) + "/" + ticketinfo.getRun_distance());
            holder.query.setVisibility(8);
        }
        return convertView;
    }

    static class ViewHolder {
        Button query;
        TextView ticketinfo;
        TextView timeinfo;
        TextView traininfo;

        ViewHolder() {
        }
    }

    public UpdateData getUpdateData() {
        return this.updateData;
    }

    public void setUpdateData(UpdateData updateData2) {
        this.updateData = updateData2;
    }
}
