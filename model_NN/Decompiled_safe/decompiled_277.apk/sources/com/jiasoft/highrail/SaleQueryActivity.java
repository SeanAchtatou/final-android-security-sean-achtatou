package com.jiasoft.highrail;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.highrail.pub.UpdateData;
import com.jiasoft.pub.CodeSpinner;

public class SaleQueryActivity extends ParentActivity {
    CodeSpinner city;
    LinearLayout layoutHot1;
    String sCityCode = "";
    CodeSpinner sheng;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mainsalequery);
        setTitle((int) R.string.tv_train_sale);
        this.sheng = (CodeSpinner) findViewById(R.id.sheng);
        this.city = (CodeSpinner) findViewById(R.id.city);
        ((Button) findViewById(R.id.querytime)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String sSheng = SaleQueryActivity.this.sheng.getCode();
                String sCity = SaleQueryActivity.this.city.getCode();
                String sShengName = SaleQueryActivity.this.sheng.getName();
                String sCityName = SaleQueryActivity.this.city.getName();
                if (!"".equalsIgnoreCase(sShengName) && !"".equalsIgnoreCase(sCityName)) {
                    UpdateData.saveToHot(SaleQueryActivity.this, "3", sCityName, "");
                }
                Intent intent = new Intent();
                Bundle myBundelForName = new Bundle();
                myBundelForName.putString("sheng", sSheng);
                myBundelForName.putString("city", sCity);
                myBundelForName.putString("shengname", sShengName);
                myBundelForName.putString("cityname", sCityName);
                intent.putExtras(myBundelForName);
                intent.setClass(SaleQueryActivity.this, SaleActivity.class);
                SaleQueryActivity.this.startActivity(intent);
            }
        });
        View.OnClickListener hotListen = new View.OnClickListener() {
            public void onClick(View v) {
                SaleQueryActivity.this.setValue(((TextView) v).getText().toString());
            }
        };
        ((TextView) findViewById(R.id.hot200)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot201)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot202)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot203)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot204)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot205)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot206)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot207)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot208)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot209)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot210)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot211)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot212)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot213)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot214)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot215)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot216)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot217)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot218)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot219)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot220)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot221)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot222)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot223)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot224)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot225)).setOnClickListener(hotListen);
        ((TextView) findViewById(R.id.hot226)).setOnClickListener(hotListen);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(-2, -2);
        param.setMargins(10, 5, 5, 5);
        this.layoutHot1 = (LinearLayout) findViewById(R.id.layoutHot1);
        Cursor cur = this.dbAdapter.rawQuery("select * from MY_HOT where HOT_TYPE='4' order by ADD_TIME desc");
        if (cur == null || !cur.moveToFirst()) {
            cur.close();
            this.sheng.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                    SaleQueryActivity.this.setCityList();
                }

                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        }
        do {
            String ss = cur.getString(cur.getColumnIndex("DEPARTURE"));
            TextView tv = new TextView(this);
            tv.setTextColor(-16777216);
            tv.setOnClickListener(hotListen);
            tv.setText(ss);
            this.layoutHot1.addView(tv, param);
        } while (cur.moveToNext());
        cur.close();
        this.sheng.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                SaleQueryActivity.this.setCityList();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    /* access modifiers changed from: private */
    public void setCityList() {
        this.city.setListSQL("select CITY,CITY_NAME from CITY WHERE CITY_TYPE='2' AND SHENG='" + this.sheng.getCode() + "' ORDER BY SEQ");
        if (!"".equalsIgnoreCase(this.sCityCode)) {
            this.city.setCode(this.sCityCode);
        }
        this.sCityCode = "";
    }

    /* access modifiers changed from: private */
    public void setValue(String ss) {
        try {
            Cursor cur = this.dbAdapter.fetch("CITY", "SHENG_NAME='" + ss + "' or CITY_NAME='" + ss + "'");
            if (cur != null && cur.moveToFirst()) {
                this.sCityCode = cur.getString(cur.getColumnIndex("CITY"));
                this.sheng.setCode(cur.getString(cur.getColumnIndex("SHENG")));
            }
            cur.close();
        } catch (Exception e) {
        }
    }
}
