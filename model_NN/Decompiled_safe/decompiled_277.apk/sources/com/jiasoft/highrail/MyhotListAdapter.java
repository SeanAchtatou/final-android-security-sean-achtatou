package com.jiasoft.highrail;

import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.jiasoft.highrail.pub.MY_HOT;
import com.jiasoft.highrail.pub.ParentActivity;
import java.util.ArrayList;
import java.util.List;

public class MyhotListAdapter extends BaseAdapter {
    private ParentActivity mContext;
    public List<MY_HOT> myhotList = new ArrayList();

    public MyhotListAdapter(ParentActivity c) {
        this.mContext = c;
    }

    public void getDataList() {
        this.myhotList.clear();
        Cursor cur = this.mContext.dbAdapter.rawQuery("select * from MY_HOT order by HOT_TYPE,SEQ");
        if (cur == null || !cur.moveToFirst()) {
            cur.close();
        }
        do {
            this.myhotList.add(new MY_HOT(this.mContext, cur));
        } while (cur.moveToNext());
        cur.close();
    }

    public int getCount() {
        return this.myhotList.size();
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.mContext).inflate((int) R.layout.adaptertraintimebynum, (ViewGroup) null);
            holder = new ViewHolder();
            holder.timeinfo = (TextView) convertView.findViewById(R.id.timeinfo);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        MY_HOT myhot = this.myhotList.get(position);
        String stype = "";
        if (myhot.getHOT_TYPE().charAt(0) == '1') {
            stype = "发到站\t\t" + myhot.getDEPARTURE() + "-" + myhot.getARRIVAL();
        } else if (myhot.getHOT_TYPE().charAt(0) == '2') {
            stype = "车次\t\t" + myhot.getDEPARTURE();
        } else if (myhot.getHOT_TYPE().charAt(0) == '3') {
            stype = "车站\t\t" + myhot.getDEPARTURE();
        } else if (myhot.getHOT_TYPE().charAt(0) == '4') {
            stype = "代售点\t\t" + myhot.getDEPARTURE();
        } else if (myhot.getHOT_TYPE().charAt(0) == '5') {
            stype = "正晚点\t\t" + myhot.getDEPARTURE() + "-" + myhot.getARRIVAL();
        }
        holder.timeinfo.setText(stype);
        return convertView;
    }

    static class ViewHolder {
        TextView timeinfo;

        ViewHolder() {
        }
    }
}
