package com.jiasoft.highrail;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.pub.Android;
import com.jiasoft.pub.SrvProxy;

public class TrainTimeByNumActivity extends ParentActivity {
    ListView gridview;
    TrainTimeByNumListAdapter listadapter;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case -1:
                    if (TrainTimeByNumActivity.this.listadapter.getCount() <= 0) {
                        ((TextView) TrainTimeByNumActivity.this.findViewById(R.id.traininfo)).setText("无数据，请检查网络状态或者车次是否正确！");
                        Android.EMsgDlg(TrainTimeByNumActivity.this, "无数据，请检查网络状态或者车次是否正确！");
                        return;
                    }
                    ((TextView) TrainTimeByNumActivity.this.findViewById(R.id.traininfo)).setText(String.valueOf(TrainTimeByNumActivity.this.listadapter.maintraininfo.getTrain_number()) + "  " + TrainTimeByNumActivity.this.listadapter.maintraininfo.getDeparture_station() + "-" + TrainTimeByNumActivity.this.listadapter.maintraininfo.getArrival_station() + " [" + TrainTimeByNumActivity.this.listadapter.maintraininfo.getTrain_type() + "]");
                    ((TextView) TrainTimeByNumActivity.this.findViewById(R.id.timeinfo)).setText(String.valueOf(TrainTimeByNumActivity.this.listadapter.maintraininfo.getDeparture_time()) + "-" + TrainTimeByNumActivity.this.listadapter.maintraininfo.getArrival_time() + "  " + TrainTimeByNumActivity.this.listadapter.maintraininfo.getRun_time() + "/" + TrainTimeByNumActivity.this.listadapter.maintraininfo.getRun_distance());
                    ((TextView) TrainTimeByNumActivity.this.findViewById(R.id.ticketinfo)).setText(TrainTimeByNumActivity.this.listadapter.maintraininfo.getPrice_info());
                    TrainTimeByNumActivity.this.gridview.setAdapter((ListAdapter) TrainTimeByNumActivity.this.listadapter);
                    return;
                default:
                    return;
            }
        }
    };
    String traindate = "";
    String trainnum = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.maintraintimebynum);
        setTitle((int) R.string.tv_train_schedules);
        try {
            Bundle myBundle = getIntent().getExtras();
            this.traindate = myBundle.getString("traindate");
            this.trainnum = myBundle.getString("trainnum");
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.gridview = (ListView) findViewById(R.id.gridview);
        this.listadapter = new TrainTimeByNumListAdapter(this);
        getList();
    }

    private void getList() {
        ((TextView) findViewById(R.id.traininfo)).setText((int) R.string.hint_querying);
        new Thread() {
            public void run() {
                try {
                    TrainTimeByNumActivity.this.listadapter.getDataList(TrainTimeByNumActivity.this.trainnum, TrainTimeByNumActivity.this.traindate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                SrvProxy.sendMsg(TrainTimeByNumActivity.this.mHandler, -1);
            }
        }.start();
    }
}
