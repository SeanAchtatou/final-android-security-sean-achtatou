package com.jiasoft.highrail;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.highrail.pub.UpdateData;
import com.jiasoft.pub.SrvProxy;
import com.mobclick.android.MobclickAgent;

public class WelcomeActivity extends ParentActivity {
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            Intent intent = new Intent();
            intent.setClass(WelcomeActivity.this, MainActivity.class);
            WelcomeActivity.this.startActivity(intent);
            WelcomeActivity.this.finish();
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.welcome);
        MobclickAgent.onResume(this);
        MobclickAgent.updateOnlineConfig(this);
        new Thread() {
            public void run() {
                try {
                    UpdateData updatedata = new UpdateData(WelcomeActivity.this, null);
                    boolean bret = updatedata.initCity();
                    updatedata.initStation();
                    updatedata.initFlightCity();
                    if (!bret) {
                        Thread.sleep(1000);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                SrvProxy.sendMsg(WelcomeActivity.this.mHandler, -1);
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        MobclickAgent.onPause(this);
        super.onDestroy();
    }
}
