package com.jiasoft.highrail;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.pub.Android;
import com.jiasoft.pub.SrvProxy;
import com.jiasoft.pub.wwpublic;

public class TrainTimeByStationActivity extends ParentActivity {
    ListView gridview;
    TrainTimeByStationListAdapter listadapter;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case -1:
                    TextView tv = (TextView) TrainTimeByStationActivity.this.findViewById(R.id.trainhint);
                    TrainTimeByStationActivity.this.gridview.setAdapter((ListAdapter) TrainTimeByStationActivity.this.listadapter);
                    tv.setText(String.valueOf(TrainTimeByStationActivity.this.station) + "站共有" + TrainTimeByStationActivity.this.listadapter.getCount() + "次途经列车");
                    if (TrainTimeByStationActivity.this.listadapter.getCount() <= 0) {
                        Android.EMsgDlg(TrainTimeByStationActivity.this, "无数据，请检查网络状态或者车站名称是否正确！");
                        return;
                    } else {
                        Toast.makeText(TrainTimeByStationActivity.this, tv.getText().toString(), 0).show();
                        return;
                    }
                default:
                    return;
            }
        }
    };
    String queryType = "0";
    String station = "";
    String traindate = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.maintraintimebystation);
        setTitle((int) R.string.tv_train_schedules);
        try {
            Bundle myBundle = getIntent().getExtras();
            this.traindate = myBundle.getString("traindate");
            this.station = myBundle.getString("station");
            this.queryType = myBundle.getString("type");
            if (wwpublic.ss(this.queryType).equalsIgnoreCase(" ")) {
                this.queryType = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.gridview = (ListView) findViewById(R.id.gridview);
        this.listadapter = new TrainTimeByStationListAdapter(this);
        this.gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if ("1".equalsIgnoreCase(TrainTimeByStationActivity.this.queryType)) {
                    Intent backIntent = new Intent();
                    Bundle myBundelForName = new Bundle();
                    myBundelForName.putString("trainnum", TrainTimeByStationActivity.this.listadapter.traintimeList.get(arg2).getTrain_number());
                    backIntent.putExtras(myBundelForName);
                    TrainTimeByStationActivity.this.setResult(-1, backIntent);
                    TrainTimeByStationActivity.this.finish();
                }
            }
        });
        getList();
    }

    private void getList() {
        ((TextView) findViewById(R.id.trainhint)).setText((int) R.string.hint_querying);
        new Thread() {
            public void run() {
                try {
                    TrainTimeByStationActivity.this.listadapter.getDataList(TrainTimeByStationActivity.this.station, TrainTimeByStationActivity.this.traindate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                SrvProxy.sendMsg(TrainTimeByStationActivity.this.mHandler, -1);
            }
        }.start();
    }
}
