package com.jiasoft.highrail;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.highrail.pub.TicketInfo;
import com.jiasoft.highrail.pub.UpdateData;
import java.util.List;

public class TrainTimeByStationListAdapter extends BaseAdapter {
    private ParentActivity mContext;
    public List<TicketInfo> traintimeList;
    private UpdateData updateData = new UpdateData(this.mContext, null);

    public TrainTimeByStationListAdapter(ParentActivity c) {
        this.mContext = c;
    }

    public void getDataList(String station, String departDate) {
        this.traintimeList = this.updateData.findTrainTimeByStation(station, departDate);
    }

    public int getCount() {
        return this.traintimeList.size();
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.mContext).inflate((int) R.layout.adaptertraintimebystation, (ViewGroup) null);
            holder = new ViewHolder();
            holder.traininfo = (TextView) convertView.findViewById(R.id.traininfo);
            holder.timeinfo = (TextView) convertView.findViewById(R.id.timeinfo);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        TicketInfo ticketinfo = this.traintimeList.get(position);
        holder.traininfo.setText(String.valueOf(ticketinfo.getTrain_number()) + " \t[" + ticketinfo.getTrain_type() + "]");
        holder.timeinfo.setText(String.valueOf(ticketinfo.getDeparture_station()) + "-" + ticketinfo.getArrival_station() + " \t[" + ticketinfo.getArrival_time() + "-" + ticketinfo.getDeparture_time() + "]");
        return convertView;
    }

    static class ViewHolder {
        TextView timeinfo;
        TextView traininfo;

        ViewHolder() {
        }
    }

    public UpdateData getUpdateData() {
        return this.updateData;
    }

    public void setUpdateData(UpdateData updateData2) {
        this.updateData = updateData2;
    }
}
