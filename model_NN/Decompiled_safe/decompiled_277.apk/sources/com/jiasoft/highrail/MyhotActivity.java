package com.jiasoft.highrail;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.pub.Android;
import com.jiasoft.pub.IQMsgDlgCallback;

public class MyhotActivity extends ParentActivity implements IQMsgDlgCallback {
    int curRecord = -1;
    ListView gridview;
    MyhotListAdapter listadapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mainmyhot);
        setTitle((int) R.string.tv_history);
        this.gridview = (ListView) findViewById(R.id.gridview);
        this.listadapter = new MyhotListAdapter(this);
        this.listadapter.getDataList();
        this.gridview.setAdapter((ListAdapter) this.listadapter);
        this.gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                MyhotActivity.this.curRecord = arg2;
                Android.QMsgDlg(MyhotActivity.this, "我的收藏删除提示", "是否确认删除收藏记录", MyhotActivity.this, null);
            }
        });
    }

    public void onSureClick() {
        this.listadapter.myhotList.get(this.curRecord).delete();
        this.listadapter.getDataList();
        this.gridview.setAdapter((ListAdapter) this.listadapter);
    }
}
