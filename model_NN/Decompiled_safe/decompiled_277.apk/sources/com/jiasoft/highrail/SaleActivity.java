package com.jiasoft.highrail;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.pub.Android;
import com.jiasoft.pub.SrvProxy;

public class SaleActivity extends ParentActivity {
    String city = "";
    String cityname = "";
    ListView gridview;
    SaleListAdapter listadapter;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case -1:
                    TextView tv = (TextView) SaleActivity.this.findViewById(R.id.trainhint);
                    SaleActivity.this.gridview.setAdapter((ListAdapter) SaleActivity.this.listadapter);
                    tv.setText(String.valueOf(SaleActivity.this.cityname) + "共有" + SaleActivity.this.listadapter.getCount() + "次途经列车");
                    if (SaleActivity.this.listadapter.getCount() <= 0) {
                        Android.EMsgDlg(SaleActivity.this, "无数据，请检查网络状态或者城市名称是否正确！");
                        return;
                    } else {
                        Toast.makeText(SaleActivity.this, tv.getText().toString(), 0).show();
                        return;
                    }
                default:
                    return;
            }
        }
    };
    String sheng = "";
    String shengname = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.maintraintimebystation);
        setTitle((int) R.string.tv_train_sale);
        try {
            Bundle myBundle = getIntent().getExtras();
            this.sheng = myBundle.getString("sheng");
            this.city = myBundle.getString("city");
            this.shengname = myBundle.getString("shengname");
            this.cityname = myBundle.getString("cityname");
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.gridview = (ListView) findViewById(R.id.gridview);
        this.listadapter = new SaleListAdapter(this);
        getList();
    }

    private void getList() {
        ((TextView) findViewById(R.id.trainhint)).setText((int) R.string.hint_querying);
        new Thread() {
            public void run() {
                try {
                    SaleActivity.this.listadapter.getDataList(SaleActivity.this.sheng, SaleActivity.this.city, SaleActivity.this.shengname, SaleActivity.this.cityname);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                SrvProxy.sendMsg(SaleActivity.this.mHandler, -1);
            }
        }.start();
    }
}
