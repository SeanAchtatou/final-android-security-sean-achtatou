package com.jiasoft.highrail;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.jiasoft.highrail.pub.ParentActivity;
import com.jiasoft.highrail.pub.TicketInfo;
import com.jiasoft.highrail.pub.UpdateData;
import com.jiasoft.pub.date;
import java.util.List;

public class TrainTicketListAdapter extends BaseAdapter {
    /* access modifiers changed from: private */
    public ParentActivity mContext;
    public List<TicketInfo> traintimeList;
    private UpdateData updateData = new UpdateData(this.mContext, null);

    public TrainTicketListAdapter(ParentActivity c) {
        this.mContext = c;
    }

    public void getDataList(String departure, String arrival, String trainnum, String departDate, String passCode, String JSESSIONID, String CiVlkRggf2, String ictStr) {
        this.traintimeList = this.updateData.findTrainRemainTicket(departure, arrival, trainnum, departDate, passCode, JSESSIONID, CiVlkRggf2, ictStr);
    }

    public int getCount() {
        return this.traintimeList.size();
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.mContext).inflate((int) R.layout.adaptertrainticket, (ViewGroup) null);
            holder = new ViewHolder();
            holder.traininfo = (TextView) convertView.findViewById(R.id.traininfo);
            holder.timeinfo = (TextView) convertView.findViewById(R.id.timeinfo);
            holder.ticketinfo = (TextView) convertView.findViewById(R.id.ticketinfo);
            holder.query = (Button) convertView.findViewById(R.id.query);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final TicketInfo ticketinfo = this.traintimeList.get(position);
        holder.traininfo.setText(String.valueOf(ticketinfo.getTrain_number()) + "(" + ticketinfo.getFirst_station() + "-" + ticketinfo.getEnd_station() + ") [" + ticketinfo.getTrain_type() + "]");
        holder.timeinfo.setText(String.valueOf(ticketinfo.getDeparture_station()) + "-" + ticketinfo.getArrival_station() + " [" + ticketinfo.getDeparture_time() + "-" + ticketinfo.getArrival_time() + "] 历时:" + ticketinfo.getRun_time());
        holder.ticketinfo.setText(ticketinfo.getRemain_info());
        holder.query.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                Bundle myBundelForName = new Bundle();
                myBundelForName.putString("traindate", date.GetLocalTime().substring(0, 10));
                myBundelForName.putString("trainnum", ticketinfo.getTrain_number());
                intent.putExtras(myBundelForName);
                intent.setClass(TrainTicketListAdapter.this.mContext, TrainTimeByNumActivity.class);
                TrainTicketListAdapter.this.mContext.startActivity(intent);
            }
        });
        return convertView;
    }

    static class ViewHolder {
        Button query;
        TextView ticketinfo;
        TextView timeinfo;
        TextView traininfo;

        ViewHolder() {
        }
    }

    public UpdateData getUpdateData() {
        return this.updateData;
    }

    public void setUpdateData(UpdateData updateData2) {
        this.updateData = updateData2;
    }
}
