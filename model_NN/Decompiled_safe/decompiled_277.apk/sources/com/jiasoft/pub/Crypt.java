package com.jiasoft.pub;

import java.security.MessageDigest;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: encryption */
class Crypt {
    private static String Algorithm = "DES";

    Crypt() {
    }

    public static byte[] getKey() throws Exception {
        return KeyGenerator.getInstance(Algorithm).generateKey().getEncoded();
    }

    public static byte[] encode(byte[] input, byte[] key) throws Exception {
        SecretKey deskey = new SecretKeySpec(key, Algorithm);
        Cipher c1 = Cipher.getInstance(Algorithm);
        c1.init(1, deskey);
        return c1.doFinal(input);
    }

    public static byte[] decode(byte[] input, byte[] key) throws Exception {
        SecretKey deskey = new SecretKeySpec(key, Algorithm);
        Cipher c1 = Cipher.getInstance(Algorithm);
        c1.init(2, deskey);
        return c1.doFinal(input);
    }

    public static byte[] md5(byte[] input) throws Exception {
        MessageDigest alg = MessageDigest.getInstance("MD5");
        alg.update(input);
        return alg.digest();
    }

    public static String byte2hex(byte[] b) {
        String hs = "";
        for (byte b2 : b) {
            String sTmp = Integer.toHexString(b2 & 255);
            if (sTmp.length() == 1) {
                hs = String.valueOf(hs) + "0" + sTmp;
            } else {
                hs = String.valueOf(hs) + sTmp;
            }
        }
        return hs.toUpperCase();
    }
}
