package com.jiasoft.pub;

import java.util.ArrayList;
import java.util.List;

public class procString {
    private int iFieldCount = 0;
    private int iRecordCount = 0;
    private int ilen = 0;
    private int pos = 0;
    public String strMember = null;

    public procString(String input) {
        this.strMember = input;
        this.pos = 0;
        if (input == null) {
            this.ilen = 0;
        } else {
            this.ilen = this.strMember.length() - 1;
        }
    }

    public void goFirst() {
        this.pos = 0;
    }

    public String getStrByDevNext() {
        StringBuffer strRlt = new StringBuffer();
        if (this.ilen == 0) {
            return null;
        }
        if (this.pos > this.ilen) {
            this.pos = this.ilen;
            return null;
        }
        int i = this.pos;
        char lastone = this.strMember.charAt(i);
        while (i <= this.ilen) {
            char a = this.strMember.charAt(i);
            i++;
            if (lastone != '\\') {
                if (a == '|') {
                    break;
                }
                if (a != '\\') {
                    strRlt.append(a);
                }
                lastone = a;
            } else {
                strRlt.append(a);
                lastone = ' ';
            }
        }
        this.pos = i;
        return strRlt.toString();
    }

    public String getStrByIdx(int idx) {
        int ibak = this.pos;
        int j = 1;
        goFirst();
        String ab = getStrByDevNext();
        while (ab != null && j != idx) {
            j++;
            ab = getStrByDevNext();
        }
        this.pos = ibak;
        return ab;
    }

    public static String encodeStr(String src) {
        if (src == null) {
            return null;
        }
        String dest = "";
        for (int i = 0; i < src.length(); i++) {
            char a = src.charAt(i);
            if (a == '\\' || a == '|') {
                dest = String.valueOf(dest) + '\\';
            }
            dest = String.valueOf(dest) + a;
        }
        return dest;
    }

    public boolean checkSuc() {
        if (getStrByIdx(1).equalsIgnoreCase("0")) {
            return true;
        }
        return false;
    }

    public String getErrMsg() {
        return getStrByIdx(2);
    }

    public int getFieldCount() {
        if (this.iFieldCount == 0) {
            this.iFieldCount = Integer.parseInt(getStrByIdx(8));
        }
        return this.iFieldCount;
    }

    public int getRecordCount() {
        if (this.iRecordCount == 0) {
            this.iRecordCount = Integer.parseInt(getStrByIdx(5));
        }
        return this.iRecordCount;
    }

    public List getRecord(int i) {
        getFieldCount();
        getRecordCount();
        if (i > this.iRecordCount) {
            return null;
        }
        List retList = new ArrayList();
        if (i == 1) {
            int ii = (this.iFieldCount * 3) + 8;
            for (int j = 1; j <= ii; j++) {
                String ss = getStrByDevNext();
            }
        }
        for (int iOrder = 0; iOrder < this.iFieldCount; iOrder++) {
            retList.add(getStrByDevNext());
        }
        return retList;
    }

    public static void main(String[] arg) {
        try {
            String sRet = SrvProxy.doSelect("http://localhost:8080", "235000000|8888|0|0|0|0|  SELECT GCXMDM,         GCXMMC,         CJSPM_DM,         NVL(ZYSJSYJ, 100) ZYSJSYJ,         LDJE,         SKKCJE,         b.KPJE,         YSBJE,         A.SKSSSWJG_DM,         A.JDXZ_DM  FROM JA_XMDJXX a,        (SELECT KJXM,        SUM(KPJE) KPJE,        SUM(DECODE((SELECT COUNT(*)FROM JA_BDCFPYSBQR WHERE FPXH = b.JDFPXH AND YXBZ = 'Y'),0,0, KPJE)) YSBJE        FROM FP_DEAL_INFO b        WHERE NSRDZDAH = ?1        AND KPRQ >= add_months(TO_DATE(?2, 'YYYYMMDD'), -1)        AND KPRQ < TO_DATE(?3, 'YYYYMMDD')        AND FP_JDFP_DM IN ('0202', '0212')        and FPZT <> '1'        GROUP BY KJXM) b WHERE a.GCXMDM = b.KJXM |3|S|100000000002324621|S|20101101|S|20101101|");
            System.out.println(sRet);
            procString proc = new procString(sRet);
            int iRecCount = proc.getRecordCount();
            for (int i = 1; i <= iRecCount; i++) {
                List record = proc.getRecord(i);
                System.out.println((String) record.get(0));
                System.out.println((String) record.get(1));
                System.out.println((String) record.get(2));
                System.out.println((String) record.get(3));
                System.out.println((String) record.get(4));
                System.out.println((String) record.get(5));
                System.out.println((String) record.get(6));
                System.out.println((String) record.get(7));
                System.out.println((String) record.get(8));
                System.out.println((String) record.get(9));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
