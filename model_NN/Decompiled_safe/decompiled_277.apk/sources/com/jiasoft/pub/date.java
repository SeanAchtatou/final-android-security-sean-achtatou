package com.jiasoft.pub;

import com.google.ads.AdActivity;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class date {
    public static String ToStandDate(String d) {
        String ss = d;
        if (wwpublic.ss(ss).equalsIgnoreCase(" ") || ss.length() < 8) {
            return ss;
        }
        if (ss.charAt(6) == '-') {
            ss = String.valueOf(ss.substring(0, 5)) + "0" + ss.substring(5);
        }
        if (ss.length() == 9) {
            return String.valueOf(ss.substring(0, 8)) + "0" + ss.substring(8);
        }
        return ss;
    }

    public static String ConvStandDate(String d) {
        if (d != null) {
            try {
                if (d.length() >= 8) {
                    if (d.length() == 8) {
                        return d;
                    }
                    return d.length() >= 10 ? String.valueOf(d.substring(0, 4)) + d.substring(5, 7) + d.substring(8, 10) : d;
                }
            } catch (Exception e) {
                return d;
            }
        }
        return " ";
    }

    public static String ConvLongDate(String d) {
        if (d == null || d.length() < 8) {
            return " ";
        }
        return String.valueOf(d.substring(0, 4)) + "年" + d.substring(4, 6) + "月" + d.substring(6, 8) + "日";
    }

    public static String ConvBigDate(String d) {
        if (d == null || d.length() < 8) {
            return " ";
        }
        return String.valueOf(wwpublic.toBigString(d.substring(0, 4))) + "年" + wwpublic.numtoBigString(d.substring(4, 6)) + "月" + wwpublic.numtoBigString(d.substring(6, 8)) + "日";
    }

    public static String ConvDateTime(Date d) {
        if (d != null) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
        }
        return "";
    }

    public static String ConvDateTime(Timestamp d) {
        if (d != null) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
        }
        return "";
    }

    public static String ConvDate(String sYear, String sMon, String sDay) {
        String sRet;
        String sRet2;
        if (sYear == null || sYear == null || sYear == null) {
            return "19900101";
        }
        String sRet3 = sYear;
        if (sMon.length() == 1) {
            sRet = String.valueOf(sRet3) + "0" + sMon;
        } else {
            sRet = String.valueOf(sRet3) + sMon;
        }
        if (sDay.length() == 1) {
            sRet2 = String.valueOf(sRet) + "0" + sDay;
        } else {
            sRet2 = String.valueOf(sRet) + sDay;
        }
        return sRet2;
    }

    public static String TranDate(String year, String mon, String day) {
        String sret;
        String sret2;
        String sret3 = year;
        if (year == null || mon == null || day == null || year.equals("") || mon.equals("") || day.equals("")) {
            return null;
        }
        if (mon.length() == 1) {
            sret = String.valueOf(sret3) + '0' + mon;
        } else {
            sret = String.valueOf(sret3) + mon;
        }
        if (day.length() == 1) {
            sret2 = String.valueOf(sret) + '0' + day;
        } else {
            sret2 = String.valueOf(sret) + day;
        }
        return sret2;
    }

    public static String ConvDate(Date d) {
        if (d != null) {
            return new SimpleDateFormat("yyyy-MM-dd").format(d);
        }
        return "";
    }

    public static String ConvDate(Timestamp d) {
        if (d != null) {
            return new SimpleDateFormat("yyyy-MM-dd").format(d);
        }
        return "";
    }

    public static java.sql.Date prepareDateStr(String datestr) {
        java.sql.Date rlt;
        if (datestr == null || datestr.equals("")) {
            return null;
        }
        String temp = datestr;
        try {
            rlt = new java.sql.Date(Integer.parseInt(temp.substring(0, 4)) - 1900, Integer.parseInt(temp.substring(4, 6)) - 1, Integer.parseInt(temp.substring(6, 8)));
        } catch (Exception e) {
            rlt = null;
        }
        return rlt;
    }

    public static String GetLocalTime() {
        return ConvDateTime(new Date());
    }

    public static java.sql.Date GetSysDate() {
        return new java.sql.Date(new Date().getTime());
    }

    public static Timestamp GetSysTimestamp() {
        return new Timestamp(new Date().getTime());
    }

    public static Date GetSysTime() {
        return new Date();
    }

    public static String trunc_date(String sDate, String sDateFormat) {
        try {
            String sTemp = sDate.substring(0, 8);
            String sFormatTemp = sDateFormat.substring(0, 1);
            if (sFormatTemp.equalsIgnoreCase("y")) {
                sTemp = String.valueOf(sDate.substring(0, 4)) + "0101";
            } else if (sFormatTemp.equalsIgnoreCase(AdActivity.TYPE_PARAM)) {
                sTemp = String.valueOf(sDate.substring(0, 6)) + "01";
            } else if (sFormatTemp.equalsIgnoreCase("q")) {
                String sTemp2 = sDate.substring(4, 6);
                if (sTemp2.equalsIgnoreCase("01") || sTemp2.equalsIgnoreCase("02") || sTemp2.equalsIgnoreCase("03")) {
                    sTemp = String.valueOf(sDate.substring(0, 4)) + "0101";
                } else if (sTemp2.equalsIgnoreCase("04") || sTemp2.equalsIgnoreCase("05") || sTemp2.equalsIgnoreCase("06")) {
                    sTemp = String.valueOf(sDate.substring(0, 4)) + "0401";
                } else if (sTemp2.equalsIgnoreCase("07") || sTemp2.equalsIgnoreCase("08") || sTemp2.equalsIgnoreCase("09")) {
                    sTemp = String.valueOf(sDate.substring(0, 4)) + "0701";
                } else {
                    sTemp = String.valueOf(sDate.substring(0, 4)) + "1001";
                }
            }
            return sTemp;
        } catch (Exception e) {
            return null;
        }
    }

    public static String last_day(String sDate) {
        try {
            String sTemp = add_days(trunc_date(add_months(sDate, 1), "mm"), -1);
            if (sDate.length() > 8) {
                return String.valueOf(sTemp) + sDate.substring(8, sDate.length());
            }
            return sTemp;
        } catch (Exception e) {
            return null;
        }
    }

    public static String add_days(String sDate, int dayCount) {
        SimpleDateFormat sd;
        try {
            if (sDate.length() > 8) {
                sd = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            } else {
                sd = new SimpleDateFormat("yyyyMMdd");
            }
            Date dTemp = sd.parse(sDate);
            dTemp.setDate(dTemp.getDate() + dayCount);
            return sd.format(dTemp);
        } catch (Exception e) {
            return null;
        }
    }

    public static String add_months(String sDate, int monCount) {
        SimpleDateFormat sd;
        try {
            if (sDate.length() > 8) {
                sd = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            } else {
                sd = new SimpleDateFormat("yyyyMMdd");
            }
            Date dTemp = sd.parse(sDate);
            dTemp.setMonth(dTemp.getMonth() + monCount);
            return sd.format(dTemp);
        } catch (Exception e) {
            return null;
        }
    }
}
