package com.jiasoft.pub;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import com.jiasoft.highrail.R;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;

public class Android {
    public static boolean canDecode(InputStream input, Charset charset) throws IOException {
        CoderResult coderResult;
        ReadableByteChannel channel = Channels.newChannel(input);
        CharsetDecoder decoder = charset.newDecoder();
        ByteBuffer byteBuffer = ByteBuffer.allocate(2048);
        CharBuffer charBuffer = CharBuffer.allocate(1024);
        boolean endOfInput = false;
        while (!endOfInput) {
            int n = channel.read(byteBuffer);
            byteBuffer.flip();
            if (n == -1) {
                endOfInput = true;
            } else {
                endOfInput = false;
            }
            CoderResult coderResult2 = decoder.decode(byteBuffer, charBuffer, endOfInput);
            charBuffer.clear();
            if (coderResult2 == CoderResult.OVERFLOW) {
                while (coderResult2 == CoderResult.OVERFLOW) {
                    coderResult2 = decoder.decode(byteBuffer, charBuffer, endOfInput);
                    charBuffer.clear();
                }
            }
            if (coderResult2.isError()) {
                return false;
            }
            byteBuffer.compact();
        }
        while (true) {
            coderResult = decoder.flush(charBuffer);
            if (coderResult != CoderResult.OVERFLOW) {
                break;
            }
            charBuffer.clear();
        }
        if (coderResult.isError()) {
            return false;
        }
        return true;
    }

    public static void InputDlg(Context context, IInputDlgCallback click) {
        InputDlg(context, context.getString(R.string.hint_input_please), "", click);
    }

    public static void InputDlg(Context context, String hint, IInputDlgCallback click) {
        InputDlg(context, hint, "", click);
    }

    public static void InputDlg(Context context, String hint, String value, final IInputDlgCallback click) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(hint);
        builder.setIcon((int) R.drawable.dlgquest);
        final EditText searchEdit = new EditText(context);
        searchEdit.setText(value);
        builder.setView(searchEdit);
        builder.setPositiveButton((int) R.string.btn_sure, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                IInputDlgCallback.this.onSureClick(searchEdit.getText().toString());
                dialog.dismiss();
            }
        });
        builder.setNegativeButton((int) R.string.btn_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public static void QMsgDlg(Context context, String Msg, IQMsgDlgCallback click) {
        QMsgDlg(context, context.getString(R.string.hint_sure_confirm), Msg, click, null);
    }

    public static void QMsgDlg(Context context, String Hint, String Msg, final IQMsgDlgCallback click, final IQMsgDlgCallback click2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(Hint);
        builder.setIcon((int) R.drawable.dlgquest);
        builder.setMessage(Msg);
        builder.setPositiveButton((int) R.string.btn_sure, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                IQMsgDlgCallback.this.onSureClick();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton((int) R.string.btn_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (IQMsgDlgCallback.this != null) {
                    IQMsgDlgCallback.this.onSureClick();
                }
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public static void IMsgDlg(Context context, String Msg) {
        IMsgDlg(context, context.getString(R.string.hint_hint_info), Msg);
    }

    public static void IMsgDlg(Context context, String Hint, String Msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(Hint);
        builder.setIcon((int) R.drawable.dlginfor);
        builder.setMessage(Msg);
        builder.setPositiveButton((int) R.string.btn_sure, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public static void WMsgDlg(Context context, String Msg) {
        IMsgDlg(context, context.getString(R.string.hint_warn_info), Msg);
    }

    public static void WMsgDlg(Context context, String Hint, String Msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(Hint);
        builder.setIcon((int) R.drawable.dlgwarm);
        builder.setMessage(Msg);
        builder.setPositiveButton((int) R.string.btn_sure, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public static void EMsgDlg(Context context, String Msg) {
        IMsgDlg(context, context.getString(R.string.hint_error_info), Msg);
    }

    public static void EMsgDlg(Context context, String Hint, String Msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(Hint);
        builder.setIcon((int) R.drawable.dlgerr);
        builder.setMessage(Msg);
        builder.setPositiveButton((int) R.string.btn_sure, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public static ProgressDialog runningDlg(Context context) {
        return runningDlg(context, context.getString(R.string.hint_running));
    }

    public static ProgressDialog runningDlg(Context context, String msg) {
        ProgressDialog m_pDialog = new ProgressDialog(context);
        m_pDialog.setProgressStyle(0);
        m_pDialog.setTitle((int) R.string.hint_title_hint);
        m_pDialog.setMessage(msg);
        m_pDialog.setIcon((int) R.drawable.coffee);
        m_pDialog.setIndeterminate(false);
        m_pDialog.setCancelable(true);
        m_pDialog.show();
        return m_pDialog;
    }
}
