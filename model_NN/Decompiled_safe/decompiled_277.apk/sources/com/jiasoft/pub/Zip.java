package com.jiasoft.pub;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Zip {
    private static int bufSize;
    private byte[] buf;
    private int readedBytes;
    private ZipEntry zipEntry;
    private ZipInputStream zipIn;
    private ZipOutputStream zipOut;

    public Zip() {
        this(512);
    }

    public Zip(int bufSize2) {
        bufSize = bufSize2;
        this.buf = new byte[bufSize];
    }

    public void doZip(String zipDirectory) {
        File zipDir = new File(zipDirectory);
        try {
            this.zipOut = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(String.valueOf(zipDir.getName()) + ".zip")));
            handleDir(zipDir, this.zipOut);
            this.zipOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleDir(File dir, ZipOutputStream zipOut2) throws IOException {
        File[] files = dir.listFiles();
        if (files.length == 0) {
            this.zipOut.putNextEntry(new ZipEntry(String.valueOf(dir.toString()) + "/"));
            this.zipOut.closeEntry();
            return;
        }
        for (File fileName : files) {
            if (fileName.isDirectory()) {
                handleDir(fileName, this.zipOut);
            } else {
                FileInputStream fileIn = new FileInputStream(fileName);
                this.zipOut.putNextEntry(new ZipEntry(fileName.toString()));
                while (true) {
                    int read = fileIn.read(this.buf);
                    this.readedBytes = read;
                    if (read <= 0) {
                        break;
                    }
                    this.zipOut.write(this.buf, 0, this.readedBytes);
                }
                this.zipOut.closeEntry();
            }
        }
    }

    public void unZip(String unZipfileName, String outPath) {
        try {
            this.zipIn = new ZipInputStream(new BufferedInputStream(new FileInputStream(unZipfileName)));
            while (true) {
                ZipEntry nextEntry = this.zipIn.getNextEntry();
                this.zipEntry = nextEntry;
                if (nextEntry != null) {
                    File file = new File(String.valueOf(outPath) + "/" + this.zipEntry.getName());
                    if (this.zipEntry.isDirectory()) {
                        file.mkdirs();
                    } else {
                        File parent = file.getParentFile();
                        if (!parent.exists()) {
                            parent.mkdirs();
                        }
                        FileOutputStream fileOut = new FileOutputStream(file);
                        while (true) {
                            int read = this.zipIn.read(this.buf);
                            this.readedBytes = read;
                            if (read <= 0) {
                                break;
                            }
                            fileOut.write(this.buf, 0, this.readedBytes);
                        }
                        fileOut.close();
                    }
                    this.zipIn.closeEntry();
                } else {
                    return;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setBufSize(int bufSize2) {
        bufSize = bufSize2;
    }

    public static void main(String[] args) throws Exception {
        new Zip().unZip("D:/susj/Android/Listen/delphi/RJ_0302.rar", "");
    }
}
