package com.jiasoft.pub;

import java.security.MessageDigest;

public class encryption {
    private String Key;

    public encryption() {
        this.Key = "12345678";
    }

    public encryption(String Key2) {
        if (Key2 == null) {
            this.Key = "12345678";
        } else {
            this.Key = Key2;
        }
    }

    public String cryp(String text) {
        try {
            return new String(Crypt.encode(text.getBytes(), this.Key.getBytes()), "ISO8859_1");
        } catch (Exception e) {
            System.out.println("In encryption.cryp()：" + e);
            return null;
        }
    }

    public String uncryp(String mtext) {
        try {
            return new String(Crypt.decode(mtext.getBytes("ISO8859_1"), this.Key.getBytes()));
        } catch (Exception e) {
            System.out.println("In encryption.uncryp()：" + e);
            return null;
        }
    }

    public static String md5(String text) {
        try {
            return byte2hex(md5(text.getBytes()));
        } catch (Exception e) {
            System.out.println("In encryption.md5()：" + e);
            return null;
        }
    }

    public static byte[] md5(byte[] input) {
        try {
            MessageDigest alg = MessageDigest.getInstance("MD5");
            alg.update(input);
            return alg.digest();
        } catch (Exception e) {
            System.out.println("In encryption.md5()：" + e);
            return null;
        }
    }

    public static String byte2hex(byte[] b) {
        String hs = "";
        for (byte b2 : b) {
            String sTmp = Integer.toHexString(b2 & 255);
            if (sTmp.length() == 1) {
                hs = String.valueOf(hs) + "0" + sTmp;
            } else {
                hs = String.valueOf(hs) + sTmp;
            }
        }
        return hs.toUpperCase();
    }
}
