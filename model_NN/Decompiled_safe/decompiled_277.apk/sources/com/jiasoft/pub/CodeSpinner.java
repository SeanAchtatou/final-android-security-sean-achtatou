package com.jiasoft.pub;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.jiasoft.highrail.R;
import java.util.ArrayList;
import java.util.List;

public class CodeSpinner extends Spinner implements CodeInterface {
    private List codeList = new ArrayList();
    private List nameList = new ArrayList();

    public CodeSpinner(Context context) {
        super(context);
    }

    public CodeSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CodeSpinner);
        setListSQL(a.getString(0));
        try {
            setCode(a.getString(1));
        } catch (Exception e) {
        }
        a.recycle();
    }

    public void setListSQL(String sSql) {
        if (!"".equalsIgnoreCase(sSql)) {
            try {
                this.codeList.clear();
                this.nameList.clear();
                setAdapter((SpinnerAdapter) null);
                if (sSql.indexOf("TEXTLIST:") == 0) {
                    procString procData = new procString(sSql.substring(9));
                    while (true) {
                        String code = procData.getStrByDevNext();
                        String name = procData.getStrByDevNext();
                        if (code == null || name == null) {
                            break;
                        }
                        this.codeList.add(code);
                        this.nameList.add(name);
                    }
                } else {
                    Cursor cur = ((DbInterface) getContext()).getDbAdapter().rawQuery(sSql);
                    if (cur == null || !cur.moveToFirst()) {
                        cur.close();
                    } else {
                        do {
                            this.codeList.add(cur.getString(0));
                            this.nameList.add(cur.getString(1));
                        } while (cur.moveToNext());
                        cur.close();
                    }
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), 17367048, this.nameList);
                adapter.setDropDownViewResource(17367049);
                setAdapter((SpinnerAdapter) adapter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setCode(String code) {
        int iPos = wwpublic.getStringListPos(this.codeList, code);
        if (iPos >= 0) {
            setSelection(iPos);
        }
    }

    public String getCode() {
        try {
            return (String) this.codeList.get(getSelectedItemPosition());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getName() {
        try {
            return (String) this.nameList.get(getSelectedItemPosition());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
