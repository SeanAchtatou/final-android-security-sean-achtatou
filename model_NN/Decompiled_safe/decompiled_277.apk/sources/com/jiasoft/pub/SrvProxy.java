package com.jiasoft.pub;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;

public class SrvProxy {
    private static final String DBANKBACKUP = "http://59.56.176.81:7098/dat/FileDownload?filename=";

    public static boolean testServer(String sHttp) {
        return true;
    }

    public static boolean isNetworkConnected(Context context) {
        NetworkInfo network = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (network != null) {
            return network.isAvailable();
        }
        return false;
    }

    public static boolean isWifiConnected(Context context) {
        return ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1).isConnectedOrConnecting();
    }

    public static String doSelect(String sHttp, String input) {
        return doHttpSrv("1", sHttp, "23500000000|8888|0|0|0|0|" + procString.encodeStr(input) + "|0|||");
    }

    public static String doSelectByParam(String sHttp, String input) {
        return doHttpSrv("1", sHttp, input);
    }

    public static String doUpdate(String sHttp, String input) {
        return doHttpSrv("2", sHttp, input);
    }

    public static String queryOneReturn(String sHttp, String sSql) {
        procString proc = new procString(doSelect(sHttp, sSql));
        int recordCount = proc.getRecordCount();
        return (String) proc.getRecord(1).get(0);
    }

    public static String doHttpSrv(String sqltype, String sHttp, String input) {
        String sRet = "";
        try {
            URL url = new URL(String.valueOf(sHttp) + "/dat/queryany?sqltype=" + sqltype);
            if (url == null) {
                return "-1|错误|";
            }
            try {
                HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
                urlConn.setDoOutput(true);
                urlConn.setDoInput(true);
                urlConn.setRequestMethod("POST");
                urlConn.setUseCaches(false);
                urlConn.setInstanceFollowRedirects(true);
                urlConn.setRequestProperty("Content-Type", "multipart/form-data");
                OutputStream out = urlConn.getOutputStream();
                out.write(input.getBytes());
                out.flush();
                out.close();
                BufferedReader reader = new BufferedReader(new InputStreamReader(urlConn.getInputStream(), "GBK"));
                while (true) {
                    String inputLine = reader.readLine();
                    if (inputLine == null) {
                        break;
                    }
                    sRet = String.valueOf(sRet) + inputLine + "\n";
                }
                reader.close();
                if ("".equalsIgnoreCase(sRet)) {
                    sRet = "-1|错误|";
                }
                urlConn.disconnect();
                return sRet;
            } catch (Exception e) {
                e.printStackTrace();
                return "-1|错误|";
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            return "-1|错误|";
        }
    }

    public static void sendMsg(Handler mHandler, int flag) {
        if (mHandler != null) {
            Message msg = new Message();
            msg.what = flag;
            mHandler.sendMessage(msg);
        }
    }

    public static boolean download(String sHttp, String input, String output) {
        return download(sHttp, input, output, null);
    }

    /* JADX INFO: Multiple debug info for r7v12 java.net.URLConnection: [D('sHttp' java.lang.String), D('conn' java.net.URLConnection)] */
    /* JADX INFO: Multiple debug info for r1v8 int: [D('iPos' int), D('iPos2' int)] */
    public static boolean download(String sHttp, String input, String output, Handler mHandler) {
        boolean bRet;
        String sFileUrl;
        String ss;
        int iPos2;
        try {
            URLConnection conn = new URL(sHttp).openConnection();
            conn.connect();
            String slink = "http://www.dbank.com/download/" + input + "?";
            InputStreamReader inputReader = new InputStreamReader(conn.getInputStream(), "GBK");
            BufferedReader bufReader = new BufferedReader(inputReader);
            while (true) {
                String line = bufReader.readLine();
                if (line != null) {
                    int iPos = line.indexOf(slink);
                    if (iPos >= 0 && (iPos2 = (ss = line.substring(iPos)).indexOf(34)) >= 0) {
                        sFileUrl = ss.substring(0, iPos2);
                        break;
                    }
                } else {
                    sFileUrl = "";
                    break;
                }
            }
            bufReader.close();
            inputReader.close();
            sendMsg(mHandler, 10);
            if (!"".equalsIgnoreCase(sFileUrl)) {
                getURLSrc(sFileUrl, output, mHandler, 10, 90);
                bRet = true;
            } else {
                bRet = false;
            }
        } catch (Exception e) {
            Exception e2 = e;
            bRet = false;
            e2.printStackTrace();
        }
        if (bRet) {
            File file = new File(output);
            if (!file.exists()) {
                bRet = false;
            } else if (file.length() <= 0) {
                bRet = false;
            }
        }
        if (bRet) {
            return bRet;
        }
        Log.i("DBANKBACKUP==", DBANKBACKUP + input);
        getURLSrc(DBANKBACKUP + input, output, mHandler, 10, 90);
        return true;
    }

    public static void getURLSrc(String urlStr, String outputFile) {
        getURLSrc(urlStr, outputFile, null, 0, 100);
    }

    public static void getURLSrc(String urlStr, String outputFile, Handler mHandler) {
        getURLSrc(urlStr, outputFile, mHandler, 0, 100);
    }

    /* JADX INFO: Multiple debug info for r8v3 java.net.URLConnection: [D('conn' java.net.URLConnection), D('urlStr' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v4 int: [D('conn' java.net.URLConnection), D('fileSize' int)] */
    /* JADX INFO: Multiple debug info for r9v4 java.lang.String: [D('sFileName' java.lang.String), D('outputFile' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v9 byte[]: [D('f1' java.io.File), D('contentByte' byte[])] */
    /* JADX INFO: Multiple debug info for r0v1 int: [D('ii' int), D('downLoadFileSize' int)] */
    public static void getURLSrc(String urlStr, String outputFile, Handler mHandler, int startValue, int maxValue) {
        int fileSize;
        Exception e;
        int downLoadFileSize;
        int fileSize2 = 0;
        try {
            URLConnection conn = new URL(urlStr).openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            int fileSize3 = conn.getContentLength();
            if (fileSize3 <= 0) {
                fileSize3 = 100000;
                try {
                    Log.e("fileSize==", "fileSize error!");
                } catch (Exception e2) {
                    e = e2;
                    fileSize = 100000;
                    e.printStackTrace();
                    return;
                }
            }
            fileSize2 = fileSize3;
            String outputFile2 = outputFile;
            File f1 = new File(new File(outputFile2).getParent());
            if (!f1.exists()) {
                f1.mkdirs();
            }
            FileOutputStream fos = new FileOutputStream(outputFile2);
            byte[] contentByte = new byte[1024];
            int downLoadFileSize2 = 0;
            do {
                try {
                    downLoadFileSize = is.read(contentByte);
                    if (downLoadFileSize > 0) {
                        fos.write(contentByte, 0, downLoadFileSize);
                        downLoadFileSize2 += downLoadFileSize;
                        if (fileSize2 >= 1) {
                            sendMsg(mHandler, new Double((double) (((maxValue - startValue) * downLoadFileSize2) / fileSize2)).intValue() + startValue);
                            continue;
                        } else {
                            continue;
                        }
                    }
                } catch (Exception e3) {
                    fileSize = fileSize2;
                    e = e3;
                    e.printStackTrace();
                    return;
                }
            } while (downLoadFileSize > 0);
            sendMsg(mHandler, maxValue);
            fos.close();
            is.close();
        } catch (Exception e4) {
            e = e4;
            fileSize = fileSize2;
        }
    }

    public static String readData(InputStream inSream, String charsetName) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        while (true) {
            int len = inSream.read(buffer);
            if (len == -1) {
                byte[] data = outStream.toByteArray();
                outStream.close();
                inSream.close();
                return new String(data, charsetName);
            }
            outStream.write(buffer, 0, len);
        }
    }

    public static String readDataForZgip(InputStream inStream, String charsetName) throws Exception {
        GZIPInputStream gzipStream = new GZIPInputStream(inStream);
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        while (true) {
            try {
                int len = gzipStream.read(buffer);
                if (len == -1) {
                    break;
                }
                outStream.write(buffer, 0, len);
            } catch (Exception e) {
            }
        }
        byte[] data = outStream.toByteArray();
        outStream.close();
        gzipStream.close();
        inStream.close();
        return new String(data, charsetName);
    }
}
