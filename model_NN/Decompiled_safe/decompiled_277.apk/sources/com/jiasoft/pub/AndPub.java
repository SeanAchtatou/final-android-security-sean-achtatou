package com.jiasoft.pub;

import android.app.Activity;
import android.content.Context;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

public class AndPub {
    public static String getStar(int iCount) {
        String sRet = "";
        for (int i = 0; i < iCount; i++) {
            sRet = String.valueOf(sRet) + "★";
        }
        return sRet;
    }

    public static boolean ifSDCardFile(String fileName) {
        try {
            if (fileName.length() < 8 || !fileName.substring(0, 8).equalsIgnoreCase("/sdcard/")) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean ifExistAssetFile(Context context, String fileName) {
        try {
            context.getAssets().open(fileName).close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static StringBuffer readAssetTextFile(Context context, String fileName) {
        StringBuffer sRet = new StringBuffer();
        try {
            InputStream is = context.getAssets().open(fileName);
            InputStreamReader inputReader = new InputStreamReader(is, "GBK");
            BufferedReader bufReader = new BufferedReader(inputReader);
            while (true) {
                String line = bufReader.readLine();
                if (line == null) {
                    break;
                }
                sRet.append(line);
                sRet.append("\n");
            }
            bufReader.close();
            inputReader.close();
            is.close();
        } catch (Exception e) {
        }
        return sRet;
    }

    public static String get_charset(File file) {
        int read;
        String charset = "GBK";
        byte[] first3Bytes = new byte[3];
        boolean checked = false;
        try {
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
            if (bis.read(first3Bytes, 0, 3) == -1) {
                return charset;
            }
            if (first3Bytes[0] == -1 && first3Bytes[1] == -2) {
                charset = "UTF-16LE";
                checked = true;
            } else if (first3Bytes[0] == -2 && first3Bytes[1] == -1) {
                charset = "UTF-16BE";
                checked = true;
            } else if (first3Bytes[0] == -17 && first3Bytes[1] == -69 && first3Bytes[2] == -65) {
                charset = "UTF-8";
                checked = true;
            }
            bis.close();
            if (!checked) {
                bis = new BufferedInputStream(new FileInputStream(file));
                int loc = 0;
                while (true) {
                    int read2 = bis.read();
                    if (read2 != -1) {
                        loc++;
                        if (read2 < 240 && (128 > read2 || read2 > 191)) {
                            if (192 <= read2 && read2 <= 223) {
                                int read3 = bis.read();
                                if (128 > read3 || read3 > 191) {
                                    break;
                                }
                            } else if (224 <= read2 && read2 <= 239) {
                                int read4 = bis.read();
                                if (128 <= read4 && read4 <= 191 && 128 <= (read = bis.read()) && read <= 191) {
                                    charset = "UTF-8";
                                }
                            }
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            bis.close();
            return charset;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void converseCode(String encode, String srcFile, String desFile) {
        try {
            File f1 = new File(new File(desFile).getParent());
            if (!f1.exists()) {
                f1.mkdirs();
            }
            InputStream is = new FileInputStream(srcFile);
            InputStreamReader inputReader = new InputStreamReader(is, encode);
            BufferedReader bufReader = new BufferedReader(inputReader);
            FileOutputStream out = new FileOutputStream(desFile);
            while (true) {
                String line = bufReader.readLine();
                if (line == null) {
                    bufReader.close();
                    inputReader.close();
                    is.close();
                    out.flush();
                    out.close();
                    return;
                }
                out.write(line.getBytes("GBK"));
                out.write(10);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static StringBuffer readSdcardTextFile(String fileName) {
        StringBuffer sRet = new StringBuffer();
        try {
            InputStream is = new FileInputStream(fileName);
            InputStreamReader inputReader = new InputStreamReader(is, "GBK");
            BufferedReader bufReader = new BufferedReader(inputReader);
            while (true) {
                String line = bufReader.readLine();
                if (line == null) {
                    break;
                }
                sRet.append(line);
                sRet.append("\n");
            }
            bufReader.close();
            inputReader.close();
            is.close();
        } catch (Exception e) {
        }
        return sRet;
    }

    public static String getProperty(Activity activity, String name) {
        return activity.getPreferences(0).getString(name, "");
    }
}
