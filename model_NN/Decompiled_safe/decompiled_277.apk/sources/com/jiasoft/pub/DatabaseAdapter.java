package com.jiasoft.pub;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseAdapter {
    private static final String TAG = "DataBaseAdapter";
    /* access modifiers changed from: private */
    public String dbName = "";
    /* access modifiers changed from: private */
    public String[][] dbUpdate;
    /* access modifiers changed from: private */
    public int dbVersion = 1;
    private Context mContext = null;
    private DatabaseHelper mDatabaseHelper = null;
    private SQLiteDatabase mSQLiteDatabase = null;

    public DatabaseAdapter(Context context) {
        this.mContext = context;
    }

    public DatabaseAdapter(Context context, String dbName2, int dbVer, String[][] dbUpdate2) {
        this.mContext = context;
        this.dbName = dbName2;
        this.dbUpdate = dbUpdate2;
        this.dbVersion = dbVer;
    }

    private class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DatabaseAdapter.this.dbName, (SQLiteDatabase.CursorFactory) null, DatabaseAdapter.this.dbVersion);
        }

        public void onCreate(SQLiteDatabase db) {
            onUpgrade(db, 0, DatabaseAdapter.this.dbVersion);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            for (int i = 0; i < DatabaseAdapter.this.dbUpdate.length; i++) {
                try {
                    int ver = Integer.parseInt(DatabaseAdapter.this.dbUpdate[i][0]);
                    if (ver > oldVersion && ver <= newVersion) {
                        db.execSQL(DatabaseAdapter.this.dbUpdate[i][1]);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void rebuildDb() {
        this.mDatabaseHelper.onUpgrade(this.mSQLiteDatabase, 0, this.dbVersion);
    }

    public void open() throws SQLException {
        this.mDatabaseHelper = new DatabaseHelper(this.mContext);
        this.mSQLiteDatabase = this.mDatabaseHelper.getWritableDatabase();
    }

    public void beginTransaction() {
        this.mSQLiteDatabase.beginTransaction();
    }

    public void commit() {
        this.mSQLiteDatabase.setTransactionSuccessful();
        this.mSQLiteDatabase.endTransaction();
    }

    public void rollback() {
        this.mSQLiteDatabase.endTransaction();
    }

    public void close() {
        this.mDatabaseHelper.close();
    }

    public void execSQL(String sql) {
        this.mSQLiteDatabase.execSQL(sql);
    }

    public long insert(String tableName, ContentValues cv) {
        return this.mSQLiteDatabase.insert(tableName, null, cv);
    }

    public boolean update(String tableName, ContentValues cv, String sWhere) {
        return this.mSQLiteDatabase.update(tableName, cv, sWhere, null) > 0;
    }

    public boolean delete(String tableName, String sWhere) {
        return this.mSQLiteDatabase.delete(tableName, sWhere, null) > 0;
    }

    public Cursor fetch(String tableName, String sWhere) throws SQLException {
        Cursor mCursor = this.mSQLiteDatabase.rawQuery("select * from " + tableName + " where " + sWhere, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor fetch(String tableName) throws SQLException {
        Cursor mCursor = this.mSQLiteDatabase.rawQuery("select * from " + tableName, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor rawQuery(String sql) {
        Cursor mCursor = this.mSQLiteDatabase.rawQuery(sql, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public String queryOneReturn(String sSql) {
        String sRet = "";
        if (!this.mSQLiteDatabase.isOpen()) {
            return sRet;
        }
        try {
            Cursor cursor = this.mSQLiteDatabase.rawQuery(sSql, null);
            if (cursor.moveToNext()) {
                sRet = cursor.getString(0);
            }
            cursor.close();
        } catch (Exception e) {
        }
        return sRet;
    }

    public boolean tabbleIsExist(String tableName) {
        boolean result = false;
        if (tableName == null) {
            return false;
        }
        if (!this.mSQLiteDatabase.isOpen()) {
            return false;
        }
        try {
            Cursor cursor = this.mSQLiteDatabase.rawQuery("select count(*) as c from sqlite_master where type ='table' and name ='" + tableName.trim() + "' ", null);
            if (cursor.moveToNext() && cursor.getInt(0) > 0) {
                result = true;
            }
            cursor.close();
        } catch (Exception e) {
        }
        return result;
    }

    public String getCodeName(String codeType, String code) {
        return queryOneReturn("select mean from pc_code where code_type='" + codeType + "' and code='" + code + "'");
    }
}
