package com.jiasoft.pub;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.util.AttributeSet;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.jiasoft.highrail.R;
import java.util.ArrayList;
import java.util.List;

public class CodeRadio extends RadioGroup implements CodeInterface {
    private List codeList = new ArrayList();
    private List nameList = new ArrayList();

    public CodeRadio(Context context) {
        super(context);
    }

    public CodeRadio(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CodeSpinner);
        String sSql = a.getString(0);
        String code = a.getString(1);
        setListSQL(sSql);
        setCode(code);
        a.recycle();
    }

    public void setListSQL(String sSql) {
        if (!"".equalsIgnoreCase(sSql)) {
            try {
                this.codeList.clear();
                this.nameList.clear();
                removeAllViews();
                int iOrder = 0;
                if (sSql.indexOf("TEXTLIST:") == 0) {
                    procString procData = new procString(sSql.substring(9));
                    while (true) {
                        String code = procData.getStrByDevNext();
                        String name = procData.getStrByDevNext();
                        if (code != null && name != null) {
                            this.codeList.add(code);
                            this.nameList.add(name);
                            RadioButton rb = new RadioButton(getContext());
                            rb.setText(name);
                            rb.setTextColor(-16777216);
                            rb.setId(iOrder);
                            addView(rb);
                            iOrder++;
                        } else {
                            return;
                        }
                    }
                } else {
                    Cursor cur = ((DbInterface) getContext()).getDbAdapter().rawQuery(sSql);
                    if (cur == null || !cur.moveToFirst()) {
                        cur.close();
                    }
                    do {
                        this.codeList.add(cur.getString(0));
                        this.nameList.add(cur.getString(1));
                        RadioButton rb2 = new RadioButton(getContext());
                        rb2.setText(cur.getString(1));
                        rb2.setId(iOrder);
                        addView(rb2);
                        iOrder++;
                    } while (cur.moveToNext());
                    cur.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setCode(String code) {
        int iPos = wwpublic.getStringListPos(this.codeList, code);
        if (iPos >= 0) {
            check(iPos);
        }
    }

    public String getCode() {
        try {
            return (String) this.codeList.get(getCheckedRadioButtonId());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getName() {
        try {
            return (String) this.nameList.get(getCheckedRadioButtonId());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
