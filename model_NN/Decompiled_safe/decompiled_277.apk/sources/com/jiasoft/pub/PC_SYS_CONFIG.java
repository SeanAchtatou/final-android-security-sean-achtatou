package com.jiasoft.pub;

import android.content.ContentValues;
import android.database.Cursor;

public class PC_SYS_CONFIG {
    private String CODE = "";
    private String CUR_VAL = "";
    private String MAX_VAL = "";
    private String MIN_VAL = "";
    private String NAME = "";
    private String REMARK = "";
    private DbInterface mContext;

    public PC_SYS_CONFIG(DbInterface mContext2) {
        this.mContext = mContext2;
    }

    public PC_SYS_CONFIG(DbInterface mContext2, String sWhere) {
        this.mContext = mContext2;
        Cursor cur = mContext2.getDbAdapter().rawQuery("select * from PC_SYS_CONFIG where " + sWhere);
        if (cur != null && cur.moveToFirst()) {
            getFromCur(cur);
        }
        cur.close();
    }

    public PC_SYS_CONFIG(DbInterface mContext2, Cursor cur) {
        this.mContext = mContext2;
        getFromCur(cur);
    }

    public static String getConfValue(DbInterface mContext2, String confCode) {
        return getConfValue(mContext2, confCode, "");
    }

    public static String getConfValue(DbInterface mContext2, String confCode, String defValue) {
        PC_SYS_CONFIG config = new PC_SYS_CONFIG(mContext2, "CODE='" + confCode + "'");
        if (!"".equalsIgnoreCase(config.getCODE())) {
            return config.getCUR_VAL();
        }
        config.setCODE(confCode);
        config.setCUR_VAL(defValue);
        config.insert();
        return defValue;
    }

    public static void setConfValue(DbInterface mContext2, String confCode, String value) {
        PC_SYS_CONFIG config = new PC_SYS_CONFIG(mContext2, "CODE='" + confCode + "'");
        config.setCUR_VAL(value);
        if ("".equalsIgnoreCase(config.getCODE())) {
            config.setCODE(confCode);
            config.insert();
            return;
        }
        config.update("CODE='" + confCode + "'");
    }

    private void getFromCur(Cursor cur) {
        this.CODE = cur.getString(cur.getColumnIndex("CODE"));
        this.NAME = cur.getString(cur.getColumnIndex("NAME"));
        this.CUR_VAL = cur.getString(cur.getColumnIndex("CUR_VAL"));
        this.MIN_VAL = cur.getString(cur.getColumnIndex("MIN_VAL"));
        this.MAX_VAL = cur.getString(cur.getColumnIndex("MAX_VAL"));
        this.REMARK = cur.getString(cur.getColumnIndex("REMARK"));
    }

    public void insert() {
        this.mContext.getDbAdapter().insert("PC_SYS_CONFIG", saveCv());
    }

    public void update(String sWhere) {
        this.mContext.getDbAdapter().update("PC_SYS_CONFIG", saveCv(), sWhere);
    }

    public ContentValues saveCv() {
        ContentValues cv = new ContentValues();
        cv.put("CODE", this.CODE);
        cv.put("NAME", this.NAME);
        cv.put("CUR_VAL", this.CUR_VAL);
        cv.put("MIN_VAL", this.MIN_VAL);
        cv.put("MAX_VAL", this.MAX_VAL);
        cv.put("REMARK", this.REMARK);
        return cv;
    }

    public String getCODE() {
        return this.CODE;
    }

    public void setCODE(String code) {
        this.CODE = code;
    }

    public String getNAME() {
        return this.NAME;
    }

    public void setNAME(String name) {
        this.NAME = name;
    }

    public String getCUR_VAL() {
        return this.CUR_VAL;
    }

    public void setCUR_VAL(String cur_val) {
        this.CUR_VAL = cur_val;
    }

    public String getMIN_VAL() {
        return this.MIN_VAL;
    }

    public void setMIN_VAL(String min_val) {
        this.MIN_VAL = min_val;
    }

    public String getMAX_VAL() {
        return this.MAX_VAL;
    }

    public void setMAX_VAL(String max_val) {
        this.MAX_VAL = max_val;
    }

    public String getREMARK() {
        return this.REMARK;
    }

    public void setREMARK(String remark) {
        this.REMARK = remark;
    }
}
