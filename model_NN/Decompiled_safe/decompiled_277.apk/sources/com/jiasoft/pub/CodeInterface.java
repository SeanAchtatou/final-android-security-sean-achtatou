package com.jiasoft.pub;

public interface CodeInterface {
    String getCode();

    String getName();

    void setCode(String str);

    void setListSQL(String str);
}
