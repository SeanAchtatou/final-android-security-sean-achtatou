package com.jiasoft.pub;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import java.io.File;
import java.io.FileOutputStream;

public class ImageProc {
    public static Bitmap genPicAndString(Bitmap icon, String ss) {
        try {
            Bitmap contactIcon = Bitmap.createBitmap(icon.getWidth(), icon.getHeight() + 25, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(contactIcon);
            Paint iconPaint = new Paint();
            iconPaint.setDither(true);
            iconPaint.setFilterBitmap(true);
            canvas.drawBitmap(icon, new Rect(0, 0, icon.getWidth(), icon.getHeight()), new Rect(0, 0, icon.getWidth(), icon.getHeight()), iconPaint);
            Paint countPaint = new Paint(257);
            countPaint.setColor(-1);
            countPaint.setTextSize(22.0f);
            countPaint.setTypeface(Typeface.DEFAULT_BOLD);
            canvas.drawText(ss, 20.0f, (float) (icon.getHeight() + 22), countPaint);
            return contactIcon;
        } catch (Exception e) {
            e.printStackTrace();
            return icon;
        }
    }

    public static Bitmap genSignPic(Bitmap icon, String ss) {
        try {
            Canvas canvas = new Canvas(icon);
            Paint countPaint = new Paint(257);
            countPaint.setColor(-65536);
            countPaint.setTextSize(30.0f);
            countPaint.setTypeface(Typeface.DEFAULT_BOLD);
            canvas.drawText(ss, (float) (icon.getWidth() - 30), (float) (icon.getHeight() - 5), countPaint);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return icon;
    }

    public static Bitmap ResizeAndSave(String srcFile, String desFile, int changedImageWidth, int changedImageHeight) {
        try {
            File file = new File(desFile);
            if (file.exists()) {
                return BitmapFactory.decodeFile(desFile);
            }
            Bitmap bit = BitmapFactory.decodeFile(srcFile);
            if (bit == null) {
                return null;
            }
            Bitmap bit2 = getResizePicture(bit, changedImageWidth, changedImageHeight);
            try {
                file.createNewFile();
                FileOutputStream fOut = new FileOutputStream(file);
                bit2.compress(Bitmap.CompressFormat.JPEG, 50, fOut);
                fOut.flush();
                fOut.close();
                return bit2;
            } catch (Exception e) {
                return bit2;
            }
        } catch (Exception e2) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap getResizePicture(Bitmap originalPic, float bo) {
        try {
            int originalImageWidth = originalPic.getWidth();
            int originalImageHeight = originalPic.getHeight();
            Matrix matrix = new Matrix();
            matrix.postScale(bo, bo);
            return Bitmap.createBitmap(originalPic, 0, 0, originalImageWidth, originalImageHeight, matrix, true);
        } catch (Exception e) {
            e.printStackTrace();
            return originalPic;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap getResizePicture(Bitmap originalPic, int changedImageWidth, int changedImageHeight) {
        try {
            int originalImageWidth = originalPic.getWidth();
            int originalImageHeight = originalPic.getHeight();
            Matrix matrix = new Matrix();
            matrix.postScale(((float) changedImageWidth) / ((float) originalImageWidth), ((float) changedImageHeight) / ((float) originalImageHeight));
            return Bitmap.createBitmap(originalPic, 0, 0, originalImageWidth, originalImageHeight, matrix, true);
        } catch (Exception e) {
            e.printStackTrace();
            return originalPic;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap getResizePictureByHeight(Bitmap originalPic, int changedImageHeight) {
        try {
            int originalImageWidth = originalPic.getWidth();
            int originalImageHeight = originalPic.getHeight();
            float bo = (float) (changedImageHeight / originalImageHeight);
            Matrix matrix = new Matrix();
            matrix.postScale(bo, bo);
            return Bitmap.createBitmap(originalPic, 0, 0, originalImageWidth, originalImageHeight, matrix, true);
        } catch (Exception e) {
            e.printStackTrace();
            return originalPic;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap getResizePictureByWidth(Bitmap originalPic, int changedImageWidth) {
        try {
            int originalImageWidth = originalPic.getWidth();
            int originalImageHeight = originalPic.getHeight();
            float bo = ((float) changedImageWidth) / ((float) originalImageWidth);
            Matrix matrix = new Matrix();
            matrix.postScale(bo, bo);
            return Bitmap.createBitmap(originalPic, 0, 0, originalImageWidth, originalImageHeight, matrix, true);
        } catch (Exception e) {
            e.printStackTrace();
            return originalPic;
        }
    }
}
