package com.jiasoft.pub;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.List;

public class wwpublic {
    public static int getStringListPos(List list, String ss) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).toString().equalsIgnoreCase(ss)) {
                return i;
            }
        }
        return -1;
    }

    public static String getCHSStr(String str) {
        if (str != null) {
            try {
                if (!str.equals("")) {
                    return new String(str.getBytes("ISO8859-1"));
                }
            } catch (Exception e) {
                return str;
            }
        }
        return str;
    }

    public static void writelog(String str) {
        System.out.println(str);
    }

    public static String ss(String str) {
        if (str == null || str.equals("")) {
            return " ";
        }
        return str;
    }

    public static String GetRetString(String sInput, String sSep, int iCount) {
        try {
            String stmp = String.valueOf(sInput) + sSep;
            int iIndexEnd = 0;
            for (int i = 0; i < iCount; i++) {
                stmp = stmp.substring(sSep.length() + iIndexEnd);
                iIndexEnd = stmp.indexOf(sSep);
            }
            return stmp.substring(0, iIndexEnd);
        } catch (Exception e) {
            return "";
        }
    }

    public static String toBigChar(char c) {
        String ss = new StringBuilder().append(c).toString();
        if (c == '0') {
            return "○";
        }
        if (c == '1') {
            return "一";
        }
        if (c == '2') {
            return "二";
        }
        if (c == '3') {
            return "三";
        }
        if (c == '4') {
            return "四";
        }
        if (c == '5') {
            return "五";
        }
        if (c == '6') {
            return "六";
        }
        if (c == '7') {
            return "七";
        }
        if (c == '8') {
            return "八";
        }
        if (c == '9') {
            return "九";
        }
        return ss;
    }

    public static String numtoBigString(String s) {
        String ss;
        if (s == null) {
            return "";
        }
        if (s.length() == 1) {
            ss = toBigChar(s.charAt(0));
        } else if (s.charAt(1) == '0') {
            ss = "十";
            if (s.charAt(0) != '1') {
                ss = String.valueOf(toBigChar(s.charAt(0))) + ss;
            }
        } else {
            ss = toBigChar(s.charAt(1));
            if (s.charAt(0) == '1') {
                ss = "十" + ss;
            } else if (s.charAt(0) != '0') {
                ss = String.valueOf(toBigChar(s.charAt(0))) + "十" + ss;
            }
        }
        return ss;
    }

    public static String toBigString(String s) {
        if (s == null) {
            return "";
        }
        String ss = "";
        for (int i = 0; i < s.length(); i++) {
            ss = String.valueOf(ss) + toBigChar(s.charAt(i));
        }
        return ss;
    }

    public static String lpad(String str, int iLength, char c) {
        try {
            String stmp = ss(str);
            for (int i = 0; i < iLength; i++) {
                stmp = String.valueOf(stmp) + c;
            }
            return stmp.substring(0, iLength);
        } catch (Exception e) {
            return str;
        }
    }

    public static double sswr(double dd) {
        return Double.valueOf((double) new Double(0.5d + (dd * 100.0d)).longValue()).doubleValue() / 100.0d;
    }

    public static String MoneyDisplay(String sInput, int money_type) {
        String stmp;
        String stmp2;
        String sRet;
        if (money_type == -4) {
            try {
                stmp = new StringBuilder().append(new Double(sswr(Double.parseDouble(sInput) / 10000.0d)).longValue()).toString();
            } catch (Exception e) {
                return "&nbsp;";
            }
        } else if (money_type == -3) {
            stmp = new StringBuilder().append(new Double(sswr(Double.parseDouble(sInput) / 1000.0d)).longValue()).toString();
        } else if (money_type == 0) {
            stmp = new StringBuilder().append(new Double(sswr(Double.parseDouble(sInput))).longValue()).toString();
        } else {
            stmp = sInput;
        }
        if (stmp.charAt(0) == '.') {
            stmp = "0" + stmp;
        }
        if (stmp.charAt(0) == '-' && stmp.length() > 1 && stmp.charAt(1) == '.') {
            stmp2 = "-0" + stmp.substring(1);
        } else {
            stmp2 = stmp;
        }
        int iPos = stmp2.indexOf(".");
        String sRet2 = "";
        if (iPos == -1) {
            iPos = stmp2.length();
        }
        int iPos2 = iPos - 1;
        for (int i = iPos2; i >= 0; i--) {
            if ((iPos2 - i) % 3 == 0 && i != iPos2) {
                sRet2 = "," + sRet2;
            }
            sRet2 = String.valueOf(stmp2.charAt(i)) + sRet2;
        }
        if (sRet2.charAt(0) == 45 && sRet2.length() > 1 && sRet2.charAt(1) == ',') {
            sRet = String.valueOf(sRet2.charAt(0)) + sRet2.substring(2);
        } else {
            sRet = sRet2;
        }
        if (money_type != 2) {
            return sRet;
        }
        if (iPos2 == stmp2.length() - 1) {
            return String.valueOf(sRet) + ".00";
        }
        if (iPos2 == stmp2.length() - 2) {
            return String.valueOf(sRet) + ".00";
        }
        if (iPos2 == stmp2.length() - 3) {
            return String.valueOf(sRet) + stmp2.substring(iPos2 + 1) + "0";
        }
        if (iPos2 == stmp2.length() - 4) {
            return String.valueOf(sRet) + stmp2.substring(iPos2 + 1);
        }
        return sRet;
    }

    public static String returnToTag(String sStr) {
        if (sStr == null || sStr.equals("")) {
            return sStr;
        }
        StringBuffer sTmp = new StringBuffer();
        for (int i = 0; i <= sStr.length() - 1; i++) {
            if (sStr.charAt(i) == 10) {
                sTmp.append(";nreturn;");
            }
            if (sStr.charAt(i) != 13) {
                sTmp.append(sStr.substring(i, i + 1));
            }
        }
        return sTmp.toString();
    }

    public static String returnToBr(String sStr) {
        if (sStr == null || sStr.equals("")) {
            return sStr;
        }
        StringBuffer sTmp = new StringBuffer();
        for (int i = 0; i <= sStr.length() - 1; i++) {
            if (sStr.charAt(i) == 10) {
                sTmp.append("<br>");
            }
            if (sStr.charAt(i) != 13) {
                sTmp.append(sStr.substring(i, i + 1));
            }
        }
        return sTmp.toString();
    }

    public static String returnToSpace(String sStr) {
        if (sStr == null || sStr.equals("")) {
            return sStr;
        }
        StringBuffer sTmp = new StringBuffer();
        for (int i = 0; i <= sStr.length() - 1; i++) {
            if (sStr.charAt(i) == 10 || sStr.charAt(i) == 13) {
                sTmp.append(" ");
            } else {
                sTmp.append(sStr.substring(i, i + 1));
            }
        }
        return sTmp.toString();
    }

    public static String returnToBrAndSpace(String sStr) {
        if (sStr == null || sStr.equals("")) {
            return "&nbsp;";
        }
        StringBuffer sTmp = new StringBuffer();
        boolean ifchange = true;
        for (int i = 0; i <= sStr.length() - 1; i++) {
            if (sStr.charAt(i) == 10) {
                sTmp.append("<br>");
                ifchange = true;
            } else if (sStr.charAt(i) != 13) {
                if (sStr.charAt(i) != ' ' || !ifchange) {
                    sTmp.append(sStr.substring(i, i + 1));
                    ifchange = false;
                } else {
                    sTmp.append("&nbsp;");
                }
            }
        }
        return sTmp.toString();
    }

    public static String SpaceToNbsp(String sStr) {
        if (sStr == null || sStr.equals("")) {
            return sStr;
        }
        StringBuffer sTmp = new StringBuffer();
        boolean ifchange = true;
        for (int i = 0; i <= sStr.length() - 1; i++) {
            if (sStr.charAt(i) != ' ' || !ifchange) {
                sTmp.append(sStr.substring(i, i + 1));
                ifchange = false;
            } else {
                sTmp.append("&nbsp;");
            }
        }
        return sTmp.toString();
    }

    public static String ConvWebStr(String sStr) {
        return ss(sStr).replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("'", "&apos;").replaceAll("\"", "&quot;");
    }

    public StringBuffer writeTable(ResultSet rs) {
        StringBuffer rlt = new StringBuffer();
        try {
            rlt.append("<table border='0' cellpadding=\"0\" cellspacing=\"1\" bgcolor=\"#0089B6\" width=\"100%\">");
            rlt.append("<tr bgcolor=\"#8EDEED\" height=\"10\">");
            ResultSetMetaData md = rs.getMetaData();
            int iTitleCount = md.getColumnCount();
            for (int j = 1; j <= iTitleCount; j++) {
                rlt.append("<th><font style='font-size:10pt;font-family:宋体' color=\"blue\">" + md.getColumnName(j) + "</font></th>");
            }
            rlt.append("</tr>");
            int i = 0;
            while (rs.next()) {
                i++;
                if (i % 2 == 0) {
                    rlt.append("<tr bgcolor=\"#E3FDFF\" height=\"10\">");
                } else {
                    rlt.append("<tr bgcolor=\"#F1F1F1\" height=\"10\">");
                }
                for (int j2 = 1; j2 <= iTitleCount; j2++) {
                    if (rs.getString(j2) == null) {
                        rlt.append("<td>&nbsp;</td>");
                    } else {
                        rlt.append("<td nowrap>" + ss(rs.getString(j2)) + "</td>");
                    }
                }
                rlt.append("</tr>");
            }
            rlt.append("</table>");
            return rlt;
        } catch (Exception e) {
            Exception exc = e;
            return null;
        }
    }

    public static String changeToChinese(double dou) {
        return changeToChinese(String.valueOf(dou));
    }

    public static String changeToChinese(String sStr) {
        String chinese;
        String decimalP;
        if (sStr.equals("") || sStr == null) {
            return "";
        }
        if (sStr.indexOf(".") > 0) {
            String integerPart = sStr.substring(0, sStr.indexOf("."));
            String decimalPart = sStr.substring(sStr.indexOf(".") + 1, sStr.length());
            String decimalP2 = "";
            if (decimalPart.length() == 2) {
                if (decimalPart.equals("00")) {
                    decimalP2 = "";
                } else {
                    if (decimalPart.charAt(0) == '0') {
                        decimalP = "";
                    } else {
                        decimalP = String.valueOf(changeToBigChinese(decimalPart.charAt(0))) + "角";
                    }
                    if (decimalPart.charAt(1) == '0') {
                        decimalP2 = String.valueOf(decimalP) + "整";
                    } else {
                        decimalP2 = String.valueOf(decimalP) + changeToBigChinese(decimalPart.charAt(1)) + "分";
                    }
                }
            }
            if (decimalPart.length() == 1) {
                if (decimalPart.equals("0")) {
                    decimalP2 = "整";
                } else {
                    decimalP2 = String.valueOf(changeToBigChinese(decimalPart.charAt(0))) + "角整";
                }
            }
            if (!integerPart.equals("0")) {
                chinese = String.valueOf(getChinese(integerPart, 0)) + "元" + decimalP2;
            } else if (decimalPart.length() == 1 && decimalPart.equals("0")) {
                chinese = "";
            } else if ("".length() != 2 || !"".equals("00")) {
                chinese = decimalP2;
            } else {
                chinese = "";
            }
        } else {
            chinese = String.valueOf(getChinese(sStr, 0)) + "元整";
        }
        return chinese;
    }

    protected static String changeToBigChinese(char ch) {
        String ss = new StringBuilder().append(ch).toString();
        if (ch == 0) {
            return "零";
        }
        if (ch == '1') {
            return "壹";
        }
        if (ch == '2') {
            return "贰";
        }
        if (ch == '3') {
            return "叁";
        }
        if (ch == '4') {
            return "肆";
        }
        if (ch == '5') {
            return "伍";
        }
        if (ch == '6') {
            return "陆";
        }
        if (ch == '7') {
            return "柒";
        }
        if (ch == '8') {
            return "捌";
        }
        if (ch == '9') {
            return "玖";
        }
        return ss;
    }

    protected static String getChinese(String number, int depth) {
        if (depth < 0) {
            depth = 0;
        }
        String chinese = "";
        String src = new StringBuilder(String.valueOf(number)).toString();
        if (src.charAt(src.length() - 1) == 'l' || src.charAt(src.length() - 1) == 'L') {
            src = src.substring(0, src.length() - 1);
        }
        if (src.length() > 4) {
            chinese = String.valueOf(getChinese(src.substring(0, src.length() - 4), depth + 1)) + getChinese(src.substring(src.length() - 4, src.length()), depth - 1);
        } else {
            char prv = 0;
            for (int i = 0; i < src.length(); i++) {
                switch (src.charAt(i)) {
                    case '0':
                        if (prv != '0') {
                            chinese = String.valueOf(chinese) + "零";
                            break;
                        }
                        break;
                    case '1':
                        chinese = String.valueOf(chinese) + "壹";
                        break;
                    case '2':
                        chinese = String.valueOf(chinese) + "贰";
                        break;
                    case '3':
                        chinese = String.valueOf(chinese) + "叁";
                        break;
                    case '4':
                        chinese = String.valueOf(chinese) + "肆";
                        break;
                    case '5':
                        chinese = String.valueOf(chinese) + "伍";
                        break;
                    case '6':
                        chinese = String.valueOf(chinese) + "陆";
                        break;
                    case '7':
                        chinese = String.valueOf(chinese) + "柒";
                        break;
                    case '8':
                        chinese = String.valueOf(chinese) + "捌";
                        break;
                    case '9':
                        chinese = String.valueOf(chinese) + "玖";
                        break;
                }
                prv = src.charAt(i);
                switch ((src.length() - 1) - i) {
                    case 1:
                        if (prv == '0') {
                            break;
                        } else {
                            chinese = String.valueOf(chinese) + "拾";
                            break;
                        }
                    case 2:
                        if (prv == '0') {
                            break;
                        } else {
                            chinese = String.valueOf(chinese) + "佰";
                            break;
                        }
                    case 3:
                        if (prv == '0') {
                            break;
                        } else {
                            chinese = String.valueOf(chinese) + "仟";
                            break;
                        }
                }
            }
        }
        while (chinese.length() > 0 && chinese.lastIndexOf("零") == chinese.length() - 1) {
            chinese = chinese.substring(0, chinese.length() - 1);
        }
        if (depth == 1) {
            if (chinese.indexOf("億") == chinese.length() - 1) {
                chinese = new StringBuilder(String.valueOf(chinese)).toString();
            } else {
                chinese = String.valueOf(chinese) + "萬";
            }
        }
        if (depth == 2) {
            return String.valueOf(chinese) + "億";
        }
        return chinese;
    }
}
