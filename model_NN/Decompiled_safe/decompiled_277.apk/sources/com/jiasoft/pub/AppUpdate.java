package com.jiasoft.pub;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;
import com.mobclick.android.MobclickAgent;
import java.io.File;

public class AppUpdate {
    private final String APP_UPDATE_HTTP = "http://dl.dbank.com/c0h30b3ph3";
    public IQMsgDlgCallback appUpdateCancel = new IQMsgDlgCallback() {
        public void onSureClick() {
            try {
                PC_SYS_CONFIG.setConfValue((DbInterface) AppUpdate.this.context, "CURR_APP_VERCODE", new StringBuilder(String.valueOf(AppUpdate.this.newvercode)).toString());
            } catch (Exception e) {
            }
            Toast.makeText(AppUpdate.this.context, "本版本不再提示更新，如果要更新版本，请使用‘菜单-检查更新’功能", 1).show();
        }
    };
    public IQMsgDlgCallback appUpdateSure = new IQMsgDlgCallback() {
        public void onSureClick() {
            Toast.makeText(AppUpdate.this.context, "正在执行更新...", 1).show();
            new Thread() {
                public void run() {
                    try {
                        String sAppFile = "/sdcard/jiasoft/" + AppUpdate.this.context.getPackageName() + ".apk";
                        SrvProxy.getURLSrc(AppUpdate.this.appUpdateUrl, sAppFile);
                        File file = new File(sAppFile);
                        if (!file.exists() || file.length() <= 0) {
                            SrvProxy.sendMsg(AppUpdate.this.mHandler, -3);
                            return;
                        }
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
                        AppUpdate.this.context.startActivity(intent);
                    } catch (Exception e) {
                        SrvProxy.sendMsg(AppUpdate.this.mHandler, -3);
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    };
    public String appUpdateUrl = "";
    /* access modifiers changed from: private */
    public Context context;
    Handler mHandler;
    ProgressDialog m_pDialog;
    public int newvercode = 0;
    public String newvername = "";
    public String updateinfo = "";

    public AppUpdate(Context context2, Handler mHandler2) {
        this.context = context2;
        this.mHandler = mHandler2;
    }

    public int checkAppUpdate() {
        try {
            Log.i("getPackageName===", this.context.getPackageName());
            int curvercode = this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0).versionCode;
            this.newvercode = curvercode;
            String stmp = MobclickAgent.getConfigParams(this.context, "version");
            Log.i("version===", stmp);
            this.newvercode = Integer.parseInt(stmp);
            if (this.newvercode <= curvercode) {
                return 0;
            }
            this.appUpdateUrl = MobclickAgent.getConfigParams(this.context, "downaddr");
            this.updateinfo = MobclickAgent.getConfigParams(this.context, "verinfo");
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 2;
        }
    }
}
