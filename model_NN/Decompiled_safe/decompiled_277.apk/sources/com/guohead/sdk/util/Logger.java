package com.guohead.sdk.util;

import android.os.Handler;
import android.util.Log;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Logger {
    public static final int ADD_LOG = 73;
    public static final int ADD_LOG_STAT = 137;
    static int cacheLen;
    static String curAd;
    static ArrayList<String> logCache = new ArrayList<>();
    static boolean logFlag = false;
    static boolean logForTest = true;
    static Handler mHandler;
    static boolean mLogD = true;
    static boolean mLogE = true;
    static boolean mLogI = true;
    static boolean mLogV = true;
    static boolean mLogW = true;
    static SimpleDateFormat sdf = new SimpleDateFormat("", Locale.SIMPLIFIED_CHINESE);
    static boolean statFlag;
    static ArrayList<String> statusCache = new ArrayList<>();
    static int statusLen;
    static TextView tvLog;

    public static void setLogFlag(boolean logE, boolean logI, boolean logD, boolean logV, boolean logW) {
        mLogE = logE;
        mLogI = logI;
        mLogD = logD;
        mLogV = logV;
        mLogW = logW;
    }

    public static void out(String content) {
        if (logForTest) {
            Log.d(GuoheAdUtil.GUOHEAD, "     " + content);
            addLog(content);
        }
    }

    public static void e(String content) {
        if (mLogE) {
            Log.e(GuoheAdUtil.GUOHEAD, "     " + content);
        }
        addLog(content);
    }

    public static void i(String content) {
        if (mLogI) {
            Log.i(GuoheAdUtil.GUOHEAD, "     " + content);
        }
        addLog(content);
    }

    public static void d(String content) {
        if (mLogD) {
            Log.d(GuoheAdUtil.GUOHEAD, "     " + content);
        }
        addLog(content);
    }

    public static void v(String content) {
        if (mLogV) {
            Log.v(GuoheAdUtil.GUOHEAD, "     " + content);
        }
        addLog(content);
    }

    public static void w(String content) {
        if (mLogW) {
            Log.w(GuoheAdUtil.GUOHEAD, "     " + content);
        }
        addLog(content);
    }

    private static void addLog(String content) {
        if (logFlag) {
            logCache.remove(0);
        } else {
            cacheLen = logCache.size();
            if (cacheLen > 89) {
                logFlag = true;
            }
        }
        logCache.add(content);
        if (mHandler != null) {
            mHandler.sendEmptyMessage(73);
        }
    }

    public static String getLog() {
        return convertToString(logCache);
    }

    private static String convertToString(ArrayList<String> aryList) {
        int len = aryList.size();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < len; i++) {
            sb.append("\n===============================\n");
            sb.append(aryList.get(i));
        }
        return sb.toString();
    }

    public static void setHandler(Handler handler) {
        mHandler = handler;
    }

    public static void addStatus(String stat) {
        if (statFlag) {
            statusCache.remove(0);
        } else {
            cacheLen = statusCache.size();
            if (cacheLen > 149) {
                logFlag = true;
            }
        }
        sdf.applyPattern("HH:mm:ss");
        statusCache.add(new StringBuffer(sdf.format(new Date())).append("\n").append(stat).toString());
        if (mHandler != null) {
            mHandler.sendEmptyMessage(ADD_LOG_STAT);
        }
    }

    public static String getStatus() {
        return convertToString(statusCache);
    }
}
