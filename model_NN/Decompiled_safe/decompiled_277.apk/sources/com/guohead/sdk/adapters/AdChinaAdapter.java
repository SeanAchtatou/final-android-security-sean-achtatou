package com.guohead.sdk.adapters;

import android.graphics.Color;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import com.adchina.android.ads.AdEngine;
import com.adchina.android.ads.AdListener;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.views.AdView;
import com.adchina.android.ads.views.FullScreenAdView;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.Logger;

public class AdChinaAdapter extends GuoheAdAdapter implements AdListener {
    private AdView adView;

    public AdChinaAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.adchina.android.ads.views.AdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        Display display = this.guoheAdLayout.activity.getWindowManager().getDefaultDisplay();
        int screenWidth = display.getWidth();
        AdManager.setResolution(screenWidth + "x" + display.getHeight());
        AdManager.setAdspaceId(this.ration.key);
        boolean debug = true;
        try {
            debug = Boolean.parseBoolean(this.ration.key2);
        } catch (Exception e) {
        }
        AdEngine engine = AdEngine.initAdEngine(this.guoheAdLayout.activity);
        engine.setAdListener(this);
        AdManager.setDebugMode(debug);
        AdManager.setLogMode(false);
        this.adView = new AdView(this.guoheAdLayout.activity);
        Extra extra = this.guoheAdLayout.extra;
        this.adView.setBackgroundColor(Color.argb(extra.bgAlpha, extra.bgRed, extra.bgGreen, extra.bgBlue));
        engine.addBannerAdView(this.adView);
        engine.startBannerAd();
        this.guoheAdLayout.addView((View) this.adView, new ViewGroup.LayoutParams(-2, -2));
    }

    public boolean OnRecvSms(AdView arg0, String arg1) {
        return false;
    }

    public void onFailedToReceiveAd(final AdView adView2) {
        Logger.d("==========> onFailedToReceiveAd");
        notifyOnFail();
        AdEngine.getAdEngine().setAdListener((AdListener) null);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                AdChinaAdapter.this.guoheAdLayout.removeView(adView2);
                AdChinaAdapter.this.guoheAdLayout.rollover();
            }
        });
        Logger.addStatus("adchina receive ad fail----");
    }

    public void onFailedToRefreshAd(AdView arg0) {
        Logger.d("==========> onFailedToRefreshAd");
        Logger.addStatus("adchina failed refresh ad----");
    }

    public void onReceiveAd(final AdView adView2) {
        AdEngine.getAdEngine().setAdListener((AdListener) null);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                AdChinaAdapter.this.guoheAdLayout.removeView(adView2);
                adView2.setVisibility(0);
                AdChinaAdapter.this.guoheAdLayout.guoheAdManager.resetRollover();
                AdChinaAdapter.this.guoheAdLayout.nextView = adView2;
                AdChinaAdapter.this.guoheAdLayout.handler.post(AdChinaAdapter.this.guoheAdLayout.viewRunnable);
                AdChinaAdapter.this.guoheAdLayout.rotateThreadedDelayed();
            }
        });
        Logger.addStatus("adchina receive ad suc++++");
    }

    public void onRefreshAd(AdView adView2) {
        Logger.d("========> onRefreshAd");
        Logger.addStatus("adchina receive ad suc++++");
    }

    public void finish() {
        Logger.i(getClass().getSimpleName() + "==> finish ");
    }

    public void onFailedToPlayVideoAd() {
    }

    public void onFailedToReceiveVideoAd() {
    }

    public void onPlayVideoAd() {
    }

    public void onFailedToReceiveFullScreenAd(FullScreenAdView arg0) {
    }

    public void onReceiveFullScreenAd(FullScreenAdView arg0) {
    }

    public void refreshAd() {
        this.guoheAdLayout.handler.post(this.guoheAdLayout.adRunnable);
        Logger.addStatus("adchina refresh");
    }
}
