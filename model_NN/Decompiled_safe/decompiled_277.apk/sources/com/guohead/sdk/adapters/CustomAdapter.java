package com.guohead.sdk.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.guohead.sdk.GuoheAdActivity;
import com.guohead.sdk.GuoheAdHttpClient;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.GuoheAdManager;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;
import com.guohead.sdk.util.Logger;
import com.madhouse.android.ads.AdView;
import java.io.InputStream;

public class CustomAdapter extends GuoheAdAdapter {
    /* access modifiers changed from: private */
    public Runnable displayCustomRunnable;
    public GuoheAdManager guoheAdManager;
    /* access modifiers changed from: private */
    public GuoheAdHttpClient httpClient = new GuoheAdHttpClient();
    /* access modifiers changed from: private */
    public boolean isClicked = false;

    public CustomAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void handle() {
        this.displayCustomRunnable = new Runnable() {
            public void run() {
                CustomAdapter.this.displayCustom();
            }
        };
        new Thread() {
            public void run() {
                CustomAdapter.this.guoheAdLayout.custom = CustomAdapter.this.guoheAdLayout.guoheAdManager.getCustom(CustomAdapter.this.ration);
                if (CustomAdapter.this.guoheAdLayout.custom == null) {
                    CustomAdapter.this.guoheAdLayout.rotateThreadedNow();
                    Logger.e("Custom Object is null");
                    return;
                }
                CustomAdapter.this.guoheAdLayout.handler.post(CustomAdapter.this.displayCustomRunnable);
            }
        }.start();
    }

    public void displayCustom() {
        switch (this.guoheAdLayout.custom.type) {
            case 1:
                Logger.d("Serving custom type: banner");
                RelativeLayout relativeLayout = new RelativeLayout(this.guoheAdLayout.activity);
                if (this.guoheAdLayout.custom.image != null) {
                    ImageView imageView = new ImageView(this.guoheAdLayout.activity);
                    imageView.setImageDrawable(this.guoheAdLayout.custom.image);
                    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    relativeLayout.addView(imageView, new RelativeLayout.LayoutParams((int) AdView.AD_MEASURE_320, 48));
                    this.guoheAdLayout.pushSubView(relativeLayout);
                    break;
                } else {
                    this.guoheAdLayout.rotateThreadedNow();
                    return;
                }
            case 2:
                Logger.d("Serving custom type: icon");
                RelativeLayout relativeLayout2 = new RelativeLayout(this.guoheAdLayout.activity);
                if (this.guoheAdLayout.custom.image != null) {
                    relativeLayout2.setLayoutParams(new FrameLayout.LayoutParams((int) AdView.AD_MEASURE_320, 48));
                    ImageView imageView2 = new ImageView(this.guoheAdLayout.activity);
                    int backgroundColor = Color.rgb(this.guoheAdLayout.extra.bgRed, this.guoheAdLayout.extra.bgGreen, this.guoheAdLayout.extra.bgBlue);
                    imageView2.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, backgroundColor, backgroundColor, backgroundColor}));
                    relativeLayout2.addView(imageView2, new RelativeLayout.LayoutParams((int) AdView.AD_MEASURE_320, 48));
                    ImageView imageView3 = new ImageView(this.guoheAdLayout.activity);
                    imageView3.setImageDrawable(this.guoheAdLayout.custom.image);
                    imageView3.setId(10);
                    imageView3.setPadding(4, 0, 7, 0);
                    imageView3.setScaleType(ImageView.ScaleType.CENTER);
                    relativeLayout2.addView(imageView3, new RelativeLayout.LayoutParams(-2, -2));
                    TextView textView = new TextView(this.guoheAdLayout.activity);
                    textView.setMaxLines(2);
                    textView.setPadding(0, 0, 4, 0);
                    textView.setText(this.guoheAdLayout.custom.description.replace("\\n", "\n"));
                    textView.setTypeface(Typeface.DEFAULT_BOLD, 1);
                    textView.setTextColor(Color.rgb(this.guoheAdLayout.extra.fgRed, this.guoheAdLayout.extra.fgGreen, this.guoheAdLayout.extra.fgBlue));
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) AdView.AD_MEASURE_320, 48);
                    layoutParams.addRule(1, imageView3.getId());
                    layoutParams.addRule(15);
                    layoutParams.setMargins(0, 0, 5, 0);
                    textView.setGravity(16);
                    relativeLayout2.addView(textView, layoutParams);
                    this.guoheAdLayout.pushSubView(relativeLayout2);
                    break;
                } else {
                    this.guoheAdLayout.rotateThreadedNow();
                    return;
                }
            case 3:
                Logger.d("Serving custom type: banner");
                RelativeLayout relativeLayout3 = new RelativeLayout(this.guoheAdLayout.activity);
                if (this.guoheAdLayout.custom.image != null) {
                    ImageView imageView4 = new ImageView(this.guoheAdLayout.activity);
                    imageView4.setImageDrawable(this.guoheAdLayout.custom.image);
                    imageView4.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams((int) AdView.AD_MEASURE_320, 48);
                    Intent intent = new Intent();
                    intent.setClass(this.guoheAdLayout.activity, GuoheAdActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("link", this.guoheAdLayout.custom.link);
                    intent.putExtras(bundle);
                    final Context context = this.guoheAdLayout.activity;
                    final Intent intent2 = intent;
                    imageView4.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            context.startActivity(intent2);
                        }
                    });
                    relativeLayout3.addView(imageView4, layoutParams2);
                    this.guoheAdLayout.pushSubView(relativeLayout3);
                    break;
                } else {
                    this.guoheAdLayout.rotateThreadedNow();
                    return;
                }
            case 4:
                Logger.d("Serving custom type: icon");
                RelativeLayout relativeLayout4 = new RelativeLayout(this.guoheAdLayout.activity);
                if (this.guoheAdLayout.custom.image != null) {
                    relativeLayout4.setLayoutParams(new FrameLayout.LayoutParams((int) AdView.AD_MEASURE_320, 48));
                    ImageView imageView5 = new ImageView(this.guoheAdLayout.activity);
                    imageView5.setBackgroundColor(Color.rgb(this.guoheAdLayout.extra.bgRed, this.guoheAdLayout.extra.bgGreen, this.guoheAdLayout.extra.bgBlue));
                    relativeLayout4.addView(imageView5, new RelativeLayout.LayoutParams((int) AdView.AD_MEASURE_320, 48));
                    ImageView imageView6 = new ImageView(this.guoheAdLayout.activity);
                    imageView6.setImageDrawable(this.guoheAdLayout.custom.image);
                    imageView6.setId(10);
                    imageView6.setPadding(4, 0, 7, 0);
                    imageView6.setScaleType(ImageView.ScaleType.CENTER);
                    relativeLayout4.addView(imageView6, new RelativeLayout.LayoutParams(-2, -2));
                    TextView textView2 = new TextView(this.guoheAdLayout.activity);
                    textView2.setMaxLines(2);
                    textView2.setPadding(0, 0, 4, 0);
                    textView2.setText(this.guoheAdLayout.custom.description.replace("\\n", "\n"));
                    textView2.setTypeface(Typeface.DEFAULT_BOLD, 1);
                    textView2.setTextColor(Color.rgb(this.guoheAdLayout.extra.fgRed, this.guoheAdLayout.extra.fgGreen, this.guoheAdLayout.extra.fgBlue));
                    RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, 48);
                    layoutParams3.addRule(1, imageView6.getId());
                    layoutParams3.addRule(15);
                    layoutParams3.setMargins(0, 0, 5, 0);
                    textView2.setGravity(17);
                    relativeLayout4.addView(textView2, layoutParams3);
                    Intent mIntent = new Intent();
                    mIntent.setClass(this.guoheAdLayout.activity, GuoheAdActivity.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("link", this.guoheAdLayout.custom.link);
                    mIntent.putExtras(mBundle);
                    final Context context2 = this.guoheAdLayout.activity;
                    final Intent intent3 = mIntent;
                    relativeLayout4.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            context2.startActivity(intent3);
                        }
                    });
                    this.guoheAdLayout.pushSubView(relativeLayout4);
                    break;
                } else {
                    this.guoheAdLayout.rotateThreadedNow();
                    return;
                }
            case 5:
                Logger.d("Serving custom type: choice");
                RelativeLayout relativeLayout5 = new RelativeLayout(this.guoheAdLayout.activity);
                relativeLayout5.setLayoutParams(new RelativeLayout.LayoutParams((int) AdView.AD_MEASURE_320, 50));
                ImageView imageView7 = new ImageView(this.guoheAdLayout.activity);
                imageView7.setBackgroundColor(Color.rgb(this.guoheAdLayout.custom.bgRed, this.guoheAdLayout.custom.bgGreen, this.guoheAdLayout.custom.bgBlue));
                relativeLayout5.addView(imageView7, new RelativeLayout.LayoutParams((int) AdView.AD_MEASURE_320, 48));
                InputStream yesDrawableStream = getClass().getResourceAsStream("/com/guohead/sdk/assets/yes.png");
                InputStream yesClickedDrawableStream = getClass().getResourceAsStream("/com/guohead/sdk/assets/yes_clicked.png");
                InputStream noDrawableStream = getClass().getResourceAsStream("/com/guohead/sdk/assets/no.png");
                InputStream noClickedDrawableStream = getClass().getResourceAsStream("/com/guohead/sdk/assets/no_clicked.png");
                BitmapDrawable bitmapDrawable = new BitmapDrawable(yesDrawableStream);
                Drawable yesClickedDrawable = new BitmapDrawable(yesClickedDrawableStream);
                BitmapDrawable bitmapDrawable2 = new BitmapDrawable(noDrawableStream);
                Drawable noClickedDrawable = new BitmapDrawable(noClickedDrawableStream);
                ImageView imageView8 = new ImageView(this.guoheAdLayout.activity);
                imageView8.setImageDrawable(bitmapDrawable);
                imageView8.setId(10);
                imageView8.setPadding(4, 0, 8, 0);
                imageView8.setScaleType(ImageView.ScaleType.CENTER);
                relativeLayout5.addView(imageView8, new RelativeLayout.LayoutParams(50, 50));
                final ImageView imageView9 = imageView8;
                final Drawable drawable = yesClickedDrawable;
                imageView8.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (!CustomAdapter.this.isClicked) {
                            CustomAdapter.this.makeChoice(true);
                            boolean unused = CustomAdapter.this.isClicked = true;
                            imageView9.setImageDrawable(drawable);
                        }
                    }
                });
                ImageView imageView10 = new ImageView(this.guoheAdLayout.activity);
                imageView10.setImageDrawable(bitmapDrawable2);
                imageView10.setId(12);
                imageView10.setPadding(0, 0, 4, 0);
                imageView10.setScaleType(ImageView.ScaleType.CENTER);
                RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams4.addRule(15);
                layoutParams4.addRule(11);
                relativeLayout5.addView(imageView10, layoutParams4);
                final ImageView imageView11 = imageView10;
                final Drawable drawable2 = noClickedDrawable;
                imageView10.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (!CustomAdapter.this.isClicked) {
                            CustomAdapter.this.makeChoice(false);
                            boolean unused = CustomAdapter.this.isClicked = true;
                            imageView11.setImageDrawable(drawable2);
                        }
                    }
                });
                TextView textView3 = new TextView(this.guoheAdLayout.activity);
                textView3.setMaxLines(2);
                textView3.setId(11);
                textView3.setText(this.guoheAdLayout.custom.description.replace("\\n", "\n"));
                textView3.setTypeface(Typeface.DEFAULT_BOLD, 1);
                textView3.setTextColor(Color.rgb(this.guoheAdLayout.custom.fgRed, this.guoheAdLayout.custom.fgGreen, this.guoheAdLayout.custom.fgBlue));
                RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams5.addRule(1, imageView8.getId());
                layoutParams5.addRule(0, imageView10.getId());
                layoutParams5.addRule(14);
                layoutParams5.addRule(15);
                layoutParams5.addRule(13);
                textView3.setGravity(17);
                relativeLayout5.addView(textView3, layoutParams5);
                this.guoheAdLayout.pushSubView(relativeLayout5);
                break;
            default:
                Logger.w("Unknown custom type!");
                this.guoheAdLayout.rotateThreadedNow();
                return;
        }
        Logger.addStatus("custom receive ad suc++++(not sure,need test)");
        this.guoheAdLayout.rotateThreadedDelayed();
    }

    public void makeChoice(final Boolean isYes) {
        new Thread(GuoheAdUtil.GUOHEAD) {
            public void run() {
                if (isYes.booleanValue()) {
                    CustomAdapter.this.httpClient.httpGet(CustomAdapter.this.ration, 6, false);
                    Logger.e("You said yes !");
                    return;
                }
                CustomAdapter.this.httpClient.httpGet(CustomAdapter.this.ration, 7, false);
                Logger.e("You said no !");
            }
        }.start();
    }

    public void finish() {
        Logger.addStatus("custom finish");
    }

    public void refreshAd() {
        this.guoheAdLayout.handler.post(this.guoheAdLayout.viewRunnable);
        Logger.addStatus("custom refresh");
    }
}
