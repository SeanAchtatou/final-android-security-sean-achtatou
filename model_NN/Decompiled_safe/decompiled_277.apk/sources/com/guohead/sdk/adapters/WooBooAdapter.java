package com.guohead.sdk.adapters;

import android.graphics.Color;
import android.widget.RelativeLayout;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.Logger;
import com.wooboo.adlib_android.AdListener;
import com.wooboo.adlib_android.WoobooAdView;

public class WooBooAdapter extends GuoheAdAdapter implements AdListener {
    WoobooAdView ad;

    public WooBooAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void handle() {
        Extra extra = this.guoheAdLayout.extra;
        this.ad = new WoobooAdView(this.guoheAdLayout.activity, this.ration.key, Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue), Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue), Boolean.parseBoolean(this.ration.key2), 600);
        this.ad.setListener(this);
        this.ad.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        this.guoheAdLayout.addView(this.ad);
    }

    public void onFailedToReceiveAd(final WoobooAdView adView) {
        Logger.d("==========> WoobooAdView failure");
        adView.setListener((AdListener) null);
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                WooBooAdapter.this.guoheAdLayout.removeView(adView);
                WooBooAdapter.this.guoheAdLayout.rollover();
            }
        });
        Logger.addStatus("wooboo receive ad failed----");
    }

    public void onNewAd() {
    }

    public void onReceiveAd(final WoobooAdView adView) {
        Logger.d("========> WoobooAdView success");
        adView.setListener((AdListener) null);
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                WooBooAdapter.this.guoheAdLayout.removeView(adView);
                adView.setVisibility(0);
                WooBooAdapter.this.guoheAdLayout.guoheAdManager.resetRollover();
                WooBooAdapter.this.guoheAdLayout.nextView = adView;
                WooBooAdapter.this.guoheAdLayout.handler.post(WooBooAdapter.this.guoheAdLayout.viewRunnable);
                WooBooAdapter.this.guoheAdLayout.rotateThreadedDelayed();
            }
        });
        Logger.addStatus("wooboo receive ad suc++++");
    }

    public void finish() {
        Logger.i(getClass().getSimpleName() + "==> finish ");
        Logger.addStatus("wooboo finish");
    }

    public void refreshAd() {
        this.guoheAdLayout.handler.post(this.guoheAdLayout.adRunnable);
        Logger.addStatus("wooboo refresh ad suc++++");
    }
}
