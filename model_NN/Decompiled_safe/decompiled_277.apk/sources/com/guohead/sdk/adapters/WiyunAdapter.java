package com.guohead.sdk.adapters;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.Logger;
import com.wiyun.ad.AdView;

public class WiyunAdapter extends GuoheAdAdapter implements AdView.AdListener {
    /* access modifiers changed from: private */
    public AdView adView;

    public WiyunAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.wiyun.ad.AdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        this.adView = new AdView(this.guoheAdLayout.activity);
        try {
            this.adView.setResId(this.ration.key);
            this.adView.setListener(this);
            this.adView.setTestAdType(2);
            this.adView.setTestMode(false);
            Extra extra = this.guoheAdLayout.extra;
            int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
            int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
            this.adView.setBackgroundColor(bgColor);
            this.adView.setTextColor(fgColor);
            this.guoheAdLayout.addView((View) this.adView, new ViewGroup.LayoutParams(-2, -2));
            this.adView.requestAd();
        } catch (IllegalArgumentException e) {
            this.guoheAdLayout.rollover();
        }
    }

    public void onAdClicked() {
    }

    public void onAdLoadFailed() {
        Logger.d("==========> onFailedToReceiveAd");
        notifyOnFail();
        this.adView.setListener((AdView.AdListener) null);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                WiyunAdapter.this.guoheAdLayout.removeView(WiyunAdapter.this.adView);
                WiyunAdapter.this.guoheAdLayout.rollover();
            }
        });
        Logger.addStatus("wiyun receive ad failed----");
    }

    public void onAdLoaded() {
        Logger.d("========> Wiyun success");
        this.adView.setListener((AdView.AdListener) null);
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                WiyunAdapter.this.guoheAdLayout.removeView(WiyunAdapter.this.adView);
                WiyunAdapter.this.adView.setVisibility(0);
                WiyunAdapter.this.guoheAdLayout.guoheAdManager.resetRollover();
                WiyunAdapter.this.guoheAdLayout.nextView = WiyunAdapter.this.adView;
                WiyunAdapter.this.guoheAdLayout.handler.post(WiyunAdapter.this.guoheAdLayout.viewRunnable);
                WiyunAdapter.this.guoheAdLayout.rotateThreadedDelayed();
            }
        });
        Logger.addStatus("wiyun receive ad suc++++");
    }

    public void onExitButtonClicked() {
    }

    public void finish() {
        Logger.i(getClass().getSimpleName() + "==> finish ");
        Logger.addStatus("wiyun finish()");
    }

    public void refreshAd() {
        this.guoheAdLayout.handler.post(this.guoheAdLayout.adRunnable);
        Logger.addStatus("wiyun refresh");
    }
}
