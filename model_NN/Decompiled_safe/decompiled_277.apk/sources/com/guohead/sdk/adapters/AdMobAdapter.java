package com.guohead.sdk.adapters;

import android.view.ViewGroup;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.Logger;
import java.util.HashSet;
import java.util.Set;

public class AdMobAdapter extends GuoheAdAdapter implements AdListener {
    private AdRequest adRequest;
    private AdView adView;
    private Set<String> keywords;

    public AdMobAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void handle() {
        try {
            if (this.ration.key2 != null) {
                this.keywords = new HashSet();
                String[] words = this.ration.key2.split(",");
                for (String add : words) {
                    this.keywords.add(add);
                }
            }
            this.adRequest = new AdRequest();
            this.adRequest.setKeywords(this.keywords);
            this.adView = new AdView(this.guoheAdLayout.activity, AdSize.BANNER, this.ration.key);
            this.adView.setAdListener(this);
            this.adView.loadAd(this.adRequest);
            this.guoheAdLayout.addView(this.adView, new ViewGroup.LayoutParams(-2, -2));
        } catch (IllegalArgumentException e) {
            this.guoheAdLayout.rollover();
        }
    }

    public void onFailedToReceiveRefreshedAd(AdView adView2) {
    }

    public void onReceiveRefreshedAd(AdView adView2) {
    }

    public void finish() {
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
        Logger.d("=======> AdMob failure: +  errorCode + ");
        AdView adView2 = (AdView) arg0;
        adView2.setAdListener(null);
        this.guoheAdLayout.removeView(adView2);
        this.guoheAdLayout.rolloverThreaded();
        Logger.addStatus("admob receive ad failed----");
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad arg0) {
        Logger.d("========> AdMob success");
        AdView adView2 = (AdView) arg0;
        adView2.setAdListener(null);
        this.guoheAdLayout.removeView(adView2);
        adView2.setVisibility(0);
        this.guoheAdLayout.guoheAdManager.resetRollover();
        this.guoheAdLayout.nextView = adView2;
        this.guoheAdLayout.handler.post(this.guoheAdLayout.viewRunnable);
        this.guoheAdLayout.rotateThreadedDelayed();
        Logger.addStatus("admob receive ad suc++++");
    }

    public void refreshAd() {
        this.guoheAdLayout.handler.post(this.guoheAdLayout.adRunnable);
        Logger.addStatus("admob refresh++++");
    }
}
