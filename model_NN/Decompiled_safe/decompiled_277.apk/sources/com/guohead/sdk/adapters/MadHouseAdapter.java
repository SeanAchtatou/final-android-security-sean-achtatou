package com.guohead.sdk.adapters;

import android.graphics.Color;
import android.view.ViewGroup;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.Logger;
import com.madhouse.android.ads.AdListener;
import com.madhouse.android.ads.AdManager;
import com.madhouse.android.ads.AdView;

public class MadHouseAdapter extends GuoheAdAdapter implements AdListener {
    /* access modifiers changed from: private */
    public AdView adView;

    public MadHouseAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    public void handle() {
        Extra extra = this.guoheAdLayout.extra;
        int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
        AdManager.setApplicationId(this.guoheAdLayout.getContext(), this.ration.key);
        this.adView = new AdView(this.guoheAdLayout.activity, null, 0, this.ration.key2, 900, Boolean.parseBoolean(this.ration.key3));
        this.adView.setBackgroundColor(bgColor);
        this.adView.setListener(this);
        this.guoheAdLayout.addView(this.adView, new ViewGroup.LayoutParams(-2, -2));
    }

    public void onAdStatus(int status) {
        Logger.d("====> onAdStatus: " + status);
        this.adView.setListener(null);
        if (status != 200) {
            notifyOnFail();
            clearAdStateListener();
            this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
                public void run() {
                    MadHouseAdapter.this.guoheAdLayout.removeView(MadHouseAdapter.this.adView);
                    MadHouseAdapter.this.guoheAdLayout.rollover();
                }
            });
            Logger.addStatus("MadHouse receive ad failed----");
            return;
        }
        Logger.addStatus("MadHouse receive ad suc++++");
        clearAdStateListener();
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                Logger.i("-----> run thread");
                MadHouseAdapter.this.guoheAdLayout.removeView(MadHouseAdapter.this.adView);
                MadHouseAdapter.this.adView.setVisibility(0);
                MadHouseAdapter.this.guoheAdLayout.guoheAdManager.resetRollover();
                MadHouseAdapter.this.guoheAdLayout.nextView = MadHouseAdapter.this.adView;
                MadHouseAdapter.this.guoheAdLayout.handler.post(MadHouseAdapter.this.guoheAdLayout.viewRunnable);
                MadHouseAdapter.this.guoheAdLayout.rotateThreadedDelayed();
            }
        });
    }

    public void finish() {
        Logger.addStatus("MadHouse finish");
    }

    public void onAdEvent(AdView adview, int arg1) {
    }

    public void refreshAd() {
        this.guoheAdLayout.handler.post(this.guoheAdLayout.adRunnable);
    }
}
