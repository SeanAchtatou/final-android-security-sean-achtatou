package com.guohead.sdk.adapters;

import android.view.View;
import android.view.ViewGroup;
import com.energysource.szj.embeded.AdListener;
import com.energysource.szj.embeded.AdManager;
import com.energysource.szj.embeded.AdView;
import com.guohead.sdk.GuoheAdLayout;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.Logger;

public class AdTouchAdapter extends GuoheAdAdapter implements AdListener {
    /* access modifiers changed from: private */
    public AdView adView;

    public AdTouchAdapter(GuoheAdLayout guoheAdLayout, Ration ration) {
        super(guoheAdLayout, ration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.energysource.szj.embeded.AdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.guohead.sdk.GuoheAdLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        try {
            AdManager.initAd(this.guoheAdLayout.activity, "");
            this.adView = new AdView(this.guoheAdLayout.activity, 0);
            AdManager.openPermissionJudge();
            AdManager.addAd(102, 1000, 81, 0, 0);
            AdManager.setAdListener(this);
            AdManager.openAllAdView();
            this.guoheAdLayout.addView((View) this.adView, new ViewGroup.LayoutParams(-1, -2));
        } catch (IllegalArgumentException e) {
            this.guoheAdLayout.rollover();
        }
    }

    public void finish() {
        Logger.i(getClass().getSimpleName() + "==> finish ");
        AdManager.destoryAd();
        Logger.addStatus("adtouch finish");
    }

    public void failedReceiveAd(AdView arg0) {
        Logger.d("==========> onFailedToReceiveAd");
        notifyOnFail();
        AdManager.setAdListener((AdListener) null);
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                AdTouchAdapter.this.guoheAdLayout.removeView(AdTouchAdapter.this.adView);
                AdTouchAdapter.this.guoheAdLayout.rollover();
            }
        });
        Logger.addStatus("adtouch receive ad failed----");
    }

    public void receiveAd(final AdView arg0) {
        Logger.d("========> AdTouch success");
        AdManager.setAdListener((AdListener) null);
        this.guoheAdLayout.activity.runOnUiThread(new Runnable() {
            public void run() {
                AdTouchAdapter.this.guoheAdLayout.removeView(arg0);
                arg0.setVisibility(0);
                AdTouchAdapter.this.guoheAdLayout.guoheAdManager.resetRollover();
                AdTouchAdapter.this.guoheAdLayout.nextView = arg0;
                AdTouchAdapter.this.guoheAdLayout.handler.post(AdTouchAdapter.this.guoheAdLayout.viewRunnable);
            }
        });
        Logger.addStatus("adtouch receive ad suc++++");
    }

    public void refreshAd() {
        this.guoheAdLayout.handler.post(this.guoheAdLayout.adRunnable);
        Logger.addStatus("adtouch refresh");
    }
}
