package com.guohead.sdk;

import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;
import com.guohead.sdk.util.Logger;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class GuoheAdHttpClient {
    public static final String urlClick = "http://mob.guohead.com/exclick.php?appid=%s&nid=%s&type=%d&uuid=%s&country_code=%s&client=2&model=%s&sdk=%s&network=%s&ghver=%s";
    public static final String urlConfig = "http://mob.guohead.com/getInfo.php?appid=%s&uuid=%s&client=2&model=%s&sdk=%s&network=%s&ghver=%s";
    public static final String urlCustom = "http://mob.guohead.com/custom.php?appid=%s&nid=%s&uuid=%s&country_code=%s&client=2&model=%s&sdk=%s&network=%s&ghver=%s";
    public static final String urlCustomChoice = "http://mob.guohead.com/exclick.php?appid=%s&nid=%s&type=%d&uuid=%s&country_code=%s&client=2&model=%s&sdk=%s&network=%s&ghver=%s&answer=%d";
    public static final String urlFail = "http://mob.guohead.com/exfail.php?appid=%s&nid=%s&type=%d&uuid=%s&country_code=%s&client=2&model=%s&sdk=%s&network=%s&ghver=%s";
    public static final String urlImpression = "http://mob.guohead.com/exmet.php?appid=%s&nid=%s&type=%d&uuid=%s&country_code=%s&client=2&model=%s&sdk=%s&network=%s&ghver=%s";
    public File file;

    public String httpGet(Ration ration, int urlType, Boolean needResponse) {
        HttpClient httpClient = new DefaultHttpClient();
        String url = "Unknow URL";
        switch (urlType) {
            case 1:
                url = String.format(urlConfig, GuoheAdUtil.appKey, GuoheAdUtil.deviceIDHash, URLEncoder.encode(GuoheAdUtil.model), URLEncoder.encode(GuoheAdUtil.SDK), GuoheAdUtil.network, GuoheAdUtil.ghVersion, GuoheAdUtil.locationString);
                break;
            case 2:
                url = String.format(urlImpression, ration.appid, ration.nid, Integer.valueOf(ration.type), GuoheAdUtil.deviceIDHash, GuoheAdUtil.locale, URLEncoder.encode(GuoheAdUtil.model), URLEncoder.encode(GuoheAdUtil.SDK), GuoheAdUtil.network, GuoheAdUtil.ghVersion);
                break;
            case 3:
                url = String.format(urlClick, GuoheAdUtil.appKey, ration.nid, Integer.valueOf(ration.type), GuoheAdUtil.deviceIDHash, GuoheAdUtil.locale, URLEncoder.encode(GuoheAdUtil.model), URLEncoder.encode(GuoheAdUtil.SDK), GuoheAdUtil.network, GuoheAdUtil.ghVersion);
                break;
            case 4:
                url = String.format(urlFail, ration.appid, ration.nid, Integer.valueOf(ration.type), GuoheAdUtil.deviceIDHash, GuoheAdUtil.locale, URLEncoder.encode(GuoheAdUtil.model), URLEncoder.encode(GuoheAdUtil.SDK), GuoheAdUtil.network, GuoheAdUtil.ghVersion);
                break;
            case 5:
                url = String.format(urlCustom, ration.appid, ration.nid, GuoheAdUtil.deviceIDHash, GuoheAdUtil.locale, URLEncoder.encode(GuoheAdUtil.model), URLEncoder.encode(GuoheAdUtil.SDK), GuoheAdUtil.network, GuoheAdUtil.ghVersion);
                break;
            case 6:
                url = String.format(urlCustomChoice, ration.appid, ration.nid, Integer.valueOf(ration.type), GuoheAdUtil.deviceIDHash, GuoheAdUtil.locale, URLEncoder.encode(GuoheAdUtil.model), URLEncoder.encode(GuoheAdUtil.SDK), GuoheAdUtil.network, GuoheAdUtil.ghVersion, 1);
                break;
            case 7:
                url = String.format(urlCustomChoice, ration.appid, ration.nid, Integer.valueOf(ration.type), GuoheAdUtil.deviceIDHash, GuoheAdUtil.locale, URLEncoder.encode(GuoheAdUtil.model), URLEncoder.encode(GuoheAdUtil.SDK), GuoheAdUtil.network, GuoheAdUtil.ghVersion, 0);
                break;
        }
        Logger.d(url);
        HttpGet httpGet = new HttpGet(url);
        StringBuilder sb = new StringBuilder();
        if (needResponse.booleanValue()) {
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                Logger.d(httpResponse.getStatusLine().toString());
                HttpEntity entity = httpResponse.getEntity();
                if (entity != null) {
                    InputStream inputStream = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream), 8192);
                    while (true) {
                        try {
                            String line = reader.readLine();
                            if (line != null) {
                                sb.append(line + "\n");
                            } else {
                                try {
                                    inputStream.close();
                                } catch (IOException e) {
                                    Logger.e("Caught IOException in convertStreamToString()" + e);
                                    return null;
                                }
                            }
                        } catch (IOException e2) {
                            Logger.e("Caught IOException in convertStreamToString()" + e2);
                            try {
                                inputStream.close();
                                return null;
                            } catch (IOException e3) {
                                Logger.e("Caught IOException in convertStreamToString()" + e3);
                                return null;
                            }
                        } catch (Throwable th) {
                            try {
                                inputStream.close();
                                throw th;
                            } catch (IOException e4) {
                                Logger.e("Caught IOException in convertStreamToString()" + e4);
                                return null;
                            }
                        }
                    }
                }
            } catch (Exception e5) {
                Logger.e(e5.toString());
            }
        } else {
            try {
                httpClient.execute(httpGet);
            } catch (ClientProtocolException e6) {
                Logger.e("Caught ClientProtocolException in countClickThreaded()" + e6);
            } catch (IOException e7) {
                Logger.e("Caught IOException in countClickThreaded()" + e7);
            }
        }
        return sb.toString();
    }

    public boolean DownloadFile(String fileName) {
        try {
            InputStream is = new DefaultHttpClient().execute(new HttpGet("http://s.domob.cn/sdk/domob_android_sdk.zip")).getEntity().getContent();
            if (is != null) {
                this.file = new File("/sdcard/.GuoheAd/" + fileName + ".guohead");
                FileOutputStream fos = new FileOutputStream(this.file);
                byte[] buf = new byte[1024];
                while (true) {
                    int ch = is.read(buf);
                    if (ch == -1) {
                        break;
                    }
                    fos.write(buf, 0, ch);
                }
                is.close();
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
