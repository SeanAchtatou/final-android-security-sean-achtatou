package com.guohead.sdk;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import com.guohead.sdk.util.Logger;

public class LocationProvider {
    public String locationString = ",";

    public LocationProvider(Context context) {
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            Criteria criteria = new Criteria();
            criteria.setAccuracy(1);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(true);
            criteria.setPowerRequirement(1);
            updateWithNewLocation(locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, true)));
        } catch (Exception e) {
            Logger.e(e.toString());
        }
    }

    private void updateWithNewLocation(Location location) {
        if (location != null) {
            double lat = location.getLatitude();
            this.locationString = lat + "," + location.getLongitude();
            return;
        }
        this.locationString = ",";
    }

    public String getLocation() {
        return this.locationString;
    }
}
