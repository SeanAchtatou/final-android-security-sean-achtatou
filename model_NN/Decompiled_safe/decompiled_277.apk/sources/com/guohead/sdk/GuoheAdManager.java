package com.guohead.sdk;

import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import com.guohead.sdk.adapters.GuoheAdAdapter;
import com.guohead.sdk.obj.Custom;
import com.guohead.sdk.obj.Extra;
import com.guohead.sdk.obj.Ration;
import com.guohead.sdk.util.GuoheAdUtil;
import com.guohead.sdk.util.Logger;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GuoheAdManager {
    public static String keyGuoheAd;
    private static Context mContext;
    public static int totalAdNum;
    private Context context;
    public SharedPreferences data;
    private Extra extra;
    private GuoheAdHttpClient httpClient = new GuoheAdHttpClient();
    public String localeString;
    private List<Ration> rationsList;
    Iterator<Ration> rollovers;
    private int totalWeight = 0;

    public GuoheAdManager(Context context2, String key) {
        Logger.i("Creating guoheAdManager...");
        Logger.addStatus("Creating GuoheAdManager");
        mContext = context2;
        keyGuoheAd = key;
        GuoheAdUtil.init(mContext);
        this.data = mContext.getSharedPreferences("GHData", 0);
        HttpfetchConfig();
        while (this.extra == null) {
            fetchConfig();
            if (this.extra == null) {
                try {
                    Logger.d("Sleeping for 30 seconds");
                    Thread.sleep(30000);
                } catch (InterruptedException e) {
                    Logger.e("Thread unable to sleep" + e);
                }
            }
        }
        Logger.i("Finished creating guoheAdManager");
        Logger.addStatus("Created GuoheAdManager=>OK");
    }

    public static void init(String key) {
        GuoheAdUtil.setAppKey(key);
    }

    public Extra getExtra() {
        if (this.totalWeight > 0) {
            return this.extra;
        }
        Logger.i("Sum of ration weights is 0 - no ads to be shown");
        return null;
    }

    public Ration getRation() {
        Logger.i("getRation()");
        int r = new Random().nextInt(this.totalWeight) + 1;
        int s = 0;
        Iterator<Ration> it = this.rationsList.iterator();
        Ration ration = null;
        while (it.hasNext()) {
            ration = it.next();
            s += ration.weight;
            if (s >= r) {
                break;
            }
        }
        return ration;
    }

    public Ration getRollover() {
        if (this.rollovers == null) {
            return null;
        }
        Ration ration = null;
        if (this.rollovers.hasNext()) {
            ration = this.rollovers.next();
        }
        return ration;
    }

    public void resetRollover() {
        this.rollovers = this.rationsList.iterator();
    }

    public Custom getCustom(Ration ration) {
        return parseCustomJsonString(this.httpClient.httpGet(ration, 5, true));
    }

    public void fetchConfig() {
        String ConfigJson = "{}";
        try {
            this.data = mContext.getSharedPreferences("GHData", 0);
            if (this.data != null) {
                ConfigJson = this.data.getString("GHConfig", "{}");
            }
            if (ConfigJson != null && ConfigJson.length() > 2) {
                parseConfigurationString(ConfigJson);
                Logger.d("Readed jsonString: " + ConfigJson);
            }
            HttpfetchConfig();
        } catch (Exception e) {
            Logger.e("Caught Exception in fetchConfig()" + e);
        }
    }

    public void HttpfetchConfig() {
        String jsonString = this.httpClient.httpGet(null, 1, true);
        Logger.d("Received jsonString: " + jsonString);
        parseConfigurationString(jsonString);
        this.data.edit().putString("GHConfig", jsonString).commit();
    }

    private void parseConfigurationString(String jsonString) {
        try {
            JSONObject json = new JSONObject(jsonString);
            parseExtraJson(json.getJSONObject("extra"));
            parseRationsJson(json.getJSONArray("rations"));
        } catch (JSONException e) {
            Logger.e("Unable to parse response from JSON. This may or may not be fatal." + e);
            this.extra = new Extra();
        } catch (NullPointerException e2) {
            Logger.e("Unable to parse response from JSON. This may or may not be fatal." + e2);
            this.extra = new Extra();
        }
    }

    private void parseExtraJson(JSONObject json) {
        Extra extra2 = new Extra();
        try {
            extra2.cycleTime = json.getInt("cycle_time");
            extra2.locationOn = json.getInt("location_on");
            extra2.transition = json.getInt("transition");
            JSONObject backgroundColor = json.getJSONObject("background_color_rgb");
            extra2.bgRed = backgroundColor.getInt("red");
            extra2.bgGreen = backgroundColor.getInt("green");
            extra2.bgBlue = backgroundColor.getInt("blue");
            extra2.bgAlpha = backgroundColor.getInt("alpha") * 255;
            JSONObject textColor = json.getJSONObject("text_color_rgb");
            extra2.fgRed = textColor.getInt("red");
            extra2.fgGreen = textColor.getInt("green");
            extra2.fgBlue = textColor.getInt("blue");
            extra2.fgAlpha = textColor.getInt("alpha") * 255;
        } catch (JSONException e) {
            Logger.e("Exception in parsing config.extra JSON. This may or may not be fatal." + e);
        }
        this.extra = extra2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private void parseRationsJson(JSONArray json) {
        List<Ration> rationsList2 = new ArrayList<>();
        this.totalWeight = 0;
        int i = 0;
        while (i < json.length()) {
            try {
                JSONObject jsonRation = json.getJSONObject(i);
                if (jsonRation != null) {
                    Ration ration = new Ration();
                    ration.appid = jsonRation.getString("appid");
                    ration.nid = jsonRation.getString("nid");
                    ration.type = jsonRation.getInt("type");
                    ration.name = jsonRation.getString("nname");
                    ration.weight = jsonRation.getInt("weight");
                    ration.priority = jsonRation.getInt("priority");
                    switch (ration.type) {
                        case 1:
                            String src = jsonRation.getString("key");
                            if (src.contains("|;|")) {
                                String[] arr = src.split("\\|;\\|");
                                ration.key = arr[0];
                                if (arr.length > 1) {
                                    ration.key2 = arr[1];
                                }
                            } else {
                                ration.key = jsonRation.getString("key");
                            }
                            this.totalWeight += ration.weight;
                            rationsList2.add(ration);
                            totalAdNum = rationsList2.size();
                            break;
                        case 6:
                            String[] arr2 = jsonRation.getString("key").split("\\|;\\|");
                            if (arr2.length == 2) {
                                ration.key = arr2[0];
                                ration.key2 = arr2[1];
                            } else {
                                ration.key = arr2[0];
                            }
                            this.totalWeight += ration.weight;
                            rationsList2.add(ration);
                            totalAdNum = rationsList2.size();
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_CUSTOM /*9*/:
                        case GuoheAdUtil.NETWORK_TYPE_ADTOUCH /*91*/:
                            this.totalWeight += ration.weight;
                            rationsList2.add(ration);
                            totalAdNum = rationsList2.size();
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_VPON /*90*/:
                            ration.key = jsonRation.getString("key").split("\\|;\\|")[0];
                            this.totalWeight += ration.weight;
                            rationsList2.add(ration);
                            totalAdNum = rationsList2.size();
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_DOMOB /*92*/:
                            String[] arr3 = jsonRation.getString("key").split("\\|;\\|");
                            if (arr3.length == 3) {
                                ration.key = arr3[0];
                                ration.key2 = arr3[1];
                                ration.key3 = arr3[2];
                            } else {
                                ration.key = arr3[0];
                                ration.key2 = arr3[1];
                            }
                            this.totalWeight += ration.weight;
                            rationsList2.add(ration);
                            totalAdNum = rationsList2.size();
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_WIYUN /*93*/:
                            String src2 = jsonRation.getString("key");
                            if (src2.contains("|;|")) {
                                String[] arr4 = src2.split("\\|;\\|");
                                ration.key = arr4[0];
                                ration.key2 = arr4[1];
                            } else {
                                ration.key = jsonRation.getString("key");
                            }
                            this.totalWeight += ration.weight;
                            rationsList2.add(ration);
                            totalAdNum = rationsList2.size();
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_MADHOUSE /*94*/:
                            String[] arr5 = jsonRation.getString("key").split("\\|;\\|");
                            ration.key = arr5[0];
                            ration.key2 = arr5[1];
                            ration.key3 = arr5[2];
                            this.totalWeight += ration.weight;
                            rationsList2.add(ration);
                            totalAdNum = rationsList2.size();
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_YOUMI /*95*/:
                            String[] arr6 = jsonRation.getString("key").split("\\|;\\|");
                            ration.key = arr6[0];
                            ration.key2 = arr6[1];
                            this.totalWeight += ration.weight;
                            rationsList2.add(ration);
                            totalAdNum = rationsList2.size();
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_WOOBOO /*96*/:
                            String[] arr7 = jsonRation.getString("key").split("\\|;\\|");
                            ration.key = arr7[0];
                            ration.key2 = arr7[1];
                            this.totalWeight += ration.weight;
                            rationsList2.add(ration);
                            totalAdNum = rationsList2.size();
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_ADCHINA /*97*/:
                            String[] arr8 = jsonRation.getString("key").split("\\|;\\|");
                            ration.key = arr8[0];
                            ration.key2 = arr8[1];
                            this.totalWeight += ration.weight;
                            rationsList2.add(ration);
                            totalAdNum = rationsList2.size();
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_CASEE /*98*/:
                            String src3 = jsonRation.getString("key");
                            if (src3.contains("|;|")) {
                                String[] arr9 = src3.split("\\|;\\|");
                                ration.key = arr9[0];
                                if (arr9.length > 1) {
                                    ration.key2 = arr9[1];
                                }
                            } else {
                                ration.key = jsonRation.getString("key");
                            }
                            this.totalWeight += ration.weight;
                            rationsList2.add(ration);
                            totalAdNum = rationsList2.size();
                            break;
                        case GuoheAdUtil.NETWORK_TYPE_ADWO /*100*/:
                            String[] arr10 = jsonRation.getString("key").split("\\|;\\|");
                            ration.key = arr10[0];
                            ration.key2 = arr10[1];
                            this.totalWeight += ration.weight;
                            rationsList2.add(ration);
                            totalAdNum = rationsList2.size();
                            break;
                        default:
                            Logger.w("Don't know how to fetch key for unexpected ration type: " + ration.type);
                            break;
                    }
                }
                i++;
            } catch (JSONException e) {
                Logger.e("JSONException in parsing config.rations JSON. This may or may not be fatal." + e);
            }
        }
        Collections.sort(rationsList2);
        this.rationsList = rationsList2;
        this.rollovers = this.rationsList.iterator();
    }

    private Custom parseCustomJsonString(String jsonString) {
        Logger.d("Received custom jsonString: " + jsonString);
        Custom custom = new Custom();
        if (jsonString != "{}") {
            try {
                JSONObject json = new JSONObject(jsonString);
                custom.type = json.getInt("ad_type");
                if (custom.type == 1 || custom.type == 3) {
                    custom.description = json.getString("ad_text");
                } else {
                    JSONObject backgroundColor = json.getJSONObject("bgcolor");
                    custom.bgRed = backgroundColor.getInt("red");
                    custom.bgGreen = backgroundColor.getInt("green");
                    custom.bgBlue = backgroundColor.getInt("blue");
                    custom.bgAlpha = backgroundColor.getInt("alpha") * 255;
                    JSONObject textColor = json.getJSONObject("textcolor");
                    custom.fgRed = textColor.getInt("red");
                    custom.fgGreen = textColor.getInt("green");
                    custom.fgBlue = textColor.getInt("blue");
                    custom.fgAlpha = textColor.getInt("alpha") * 255;
                    custom.description = json.getString("ad_text");
                }
                if (custom.type != 5) {
                    custom.link = json.getString("redirect_url");
                    custom.imageLink = json.getString("img_url");
                    custom.image = fetchImage(custom.imageLink);
                }
            } catch (JSONException e) {
                Logger.e("Caught JSONException in parseCustomJsonString()" + e);
                return null;
            }
        } else {
            custom = null;
        }
        return custom;
    }

    private Drawable fetchImage(String urlString) {
        try {
            return Drawable.createFromStream((InputStream) new URL(urlString).getContent(), "src");
        } catch (Exception e) {
            Logger.e("Unable to fetchImage(): " + e);
            return null;
        }
    }

    public Location getLocation() {
        if (this.context == null) {
            return null;
        }
        Location location = null;
        if (this.context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            location = ((LocationManager) this.context.getSystemService("location")).getLastKnownLocation("gps");
        } else if (this.context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            location = ((LocationManager) this.context.getSystemService("location")).getLastKnownLocation("network");
        }
        return location;
    }

    public static void finish(Context context2) {
        try {
            GuoheAdAdapter.finish(context2);
            System.exit(0);
        } catch (Exception e) {
            Logger.v("Finish Exception: " + e.toString());
        }
    }

    public static void setGender(String gender) {
        GuoheAdUtil.Gender = gender;
    }

    public static void refreshAd(Context context2) {
        try {
            GuoheAdAdapter.refreshAd(context2);
        } catch (Exception e) {
            Logger.v("Finish Exception: " + e.toString());
        }
    }

    public static String getRunningServicesInfo(Context context2) {
        StringBuffer serviceInfo = new StringBuffer();
        for (ActivityManager.RunningServiceInfo si : ((ActivityManager) context2.getSystemService("activity")).getRunningServices(100)) {
            serviceInfo.append("pid: ").append(si.pid);
            serviceInfo.append("\nprocess: " + si.process);
            serviceInfo.append("\nservice: ").append(si.service);
            serviceInfo.append("\ncrashCount: ").append(si.crashCount);
            serviceInfo.append("\nclientCount: ").append(si.clientCount);
            serviceInfo.append(";");
        }
        return serviceInfo.toString();
    }

    public ArrayList<PInfo> getInstalledApps(boolean getSysPackages) {
        ArrayList<PInfo> res = new ArrayList<>();
        List<PackageInfo> packs = this.context.getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < packs.size(); i++) {
            PackageInfo p = packs.get(i);
            if (getSysPackages) {
                PInfo newInfo = new PInfo();
                String unused = newInfo.appname = p.applicationInfo.loadLabel(this.context.getPackageManager()).toString();
                String unused2 = newInfo.pname = p.packageName;
                res.add(newInfo);
                newInfo.infoPrint();
            }
        }
        return res;
    }

    class PInfo {
        /* access modifiers changed from: private */
        public String appname = "";
        /* access modifiers changed from: private */
        public String pname = "";

        PInfo() {
        }

        /* access modifiers changed from: private */
        public void infoPrint() {
            Logger.v(this.appname + "/" + this.pname);
        }
    }
}
