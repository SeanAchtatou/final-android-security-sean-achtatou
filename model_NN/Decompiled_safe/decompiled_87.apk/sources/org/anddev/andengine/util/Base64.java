package org.anddev.andengine.util;

import java.io.UnsupportedEncodingException;

public class Base64 {
    static final /* synthetic */ boolean $assertionsDisabled = (!Base64.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    public static final int CRLF = 4;
    public static final int DEFAULT = 0;
    public static final int NO_CLOSE = 16;
    public static final int NO_PADDING = 1;
    public static final int NO_WRAP = 2;
    public static final int URL_SAFE = 8;

    static abstract class Coder {
        public int op;
        public byte[] output;

        public abstract int maxOutputSize(int i);

        public abstract boolean process(byte[] bArr, int i, int i2, boolean z);

        Coder() {
        }
    }

    public static byte[] decode(String str, int flags) {
        return decode(str.getBytes(), flags);
    }

    public static byte[] decode(byte[] input, int flags) {
        return decode(input, 0, input.length, flags);
    }

    public static byte[] decode(byte[] input, int offset, int len, int flags) {
        Decoder decoder = new Decoder(flags, new byte[((len * 3) / 4)]);
        if (!decoder.process(input, offset, len, true)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (decoder.op == decoder.output.length) {
            return decoder.output;
        } else {
            byte[] temp = new byte[decoder.op];
            System.arraycopy(decoder.output, 0, temp, 0, decoder.op);
            return temp;
        }
    }

    static class Decoder extends Coder {
        private static final int[] DECODE;
        private static final int[] DECODE_WEBSAFE;
        private static final int EQUALS = -2;
        private static final int SKIP = -1;
        private final int[] alphabet;
        private int state;
        private int value;

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: int[]} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: int[]} */
        /* JADX WARNING: Multi-variable type inference failed */
        static {
            /*
                r7 = 4
                r6 = 3
                r5 = 2
                r4 = 1
                r3 = -1
                r0 = 256(0x100, float:3.59E-43)
                int[] r0 = new int[r0]
                r1 = 0
                r0[r1] = r3
                r0[r4] = r3
                r0[r5] = r3
                r0[r6] = r3
                r0[r7] = r3
                r1 = 5
                r0[r1] = r3
                r1 = 6
                r0[r1] = r3
                r1 = 7
                r0[r1] = r3
                r1 = 8
                r0[r1] = r3
                r1 = 9
                r0[r1] = r3
                r1 = 10
                r0[r1] = r3
                r1 = 11
                r0[r1] = r3
                r1 = 12
                r0[r1] = r3
                r1 = 13
                r0[r1] = r3
                r1 = 14
                r0[r1] = r3
                r1 = 15
                r0[r1] = r3
                r1 = 16
                r0[r1] = r3
                r1 = 17
                r0[r1] = r3
                r1 = 18
                r0[r1] = r3
                r1 = 19
                r0[r1] = r3
                r1 = 20
                r0[r1] = r3
                r1 = 21
                r0[r1] = r3
                r1 = 22
                r0[r1] = r3
                r1 = 23
                r0[r1] = r3
                r1 = 24
                r0[r1] = r3
                r1 = 25
                r0[r1] = r3
                r1 = 26
                r0[r1] = r3
                r1 = 27
                r0[r1] = r3
                r1 = 28
                r0[r1] = r3
                r1 = 29
                r0[r1] = r3
                r1 = 30
                r0[r1] = r3
                r1 = 31
                r0[r1] = r3
                r1 = 32
                r0[r1] = r3
                r1 = 33
                r0[r1] = r3
                r1 = 34
                r0[r1] = r3
                r1 = 35
                r0[r1] = r3
                r1 = 36
                r0[r1] = r3
                r1 = 37
                r0[r1] = r3
                r1 = 38
                r0[r1] = r3
                r1 = 39
                r0[r1] = r3
                r1 = 40
                r0[r1] = r3
                r1 = 41
                r0[r1] = r3
                r1 = 42
                r0[r1] = r3
                r1 = 43
                r2 = 62
                r0[r1] = r2
                r1 = 44
                r0[r1] = r3
                r1 = 45
                r0[r1] = r3
                r1 = 46
                r0[r1] = r3
                r1 = 47
                r2 = 63
                r0[r1] = r2
                r1 = 48
                r2 = 52
                r0[r1] = r2
                r1 = 49
                r2 = 53
                r0[r1] = r2
                r1 = 50
                r2 = 54
                r0[r1] = r2
                r1 = 51
                r2 = 55
                r0[r1] = r2
                r1 = 52
                r2 = 56
                r0[r1] = r2
                r1 = 53
                r2 = 57
                r0[r1] = r2
                r1 = 54
                r2 = 58
                r0[r1] = r2
                r1 = 55
                r2 = 59
                r0[r1] = r2
                r1 = 56
                r2 = 60
                r0[r1] = r2
                r1 = 57
                r2 = 61
                r0[r1] = r2
                r1 = 58
                r0[r1] = r3
                r1 = 59
                r0[r1] = r3
                r1 = 60
                r0[r1] = r3
                r1 = 61
                r2 = -2
                r0[r1] = r2
                r1 = 62
                r0[r1] = r3
                r1 = 63
                r0[r1] = r3
                r1 = 64
                r0[r1] = r3
                r1 = 66
                r0[r1] = r4
                r1 = 67
                r0[r1] = r5
                r1 = 68
                r0[r1] = r6
                r1 = 69
                r0[r1] = r7
                r1 = 70
                r2 = 5
                r0[r1] = r2
                r1 = 71
                r2 = 6
                r0[r1] = r2
                r1 = 72
                r2 = 7
                r0[r1] = r2
                r1 = 73
                r2 = 8
                r0[r1] = r2
                r1 = 74
                r2 = 9
                r0[r1] = r2
                r1 = 75
                r2 = 10
                r0[r1] = r2
                r1 = 76
                r2 = 11
                r0[r1] = r2
                r1 = 77
                r2 = 12
                r0[r1] = r2
                r1 = 78
                r2 = 13
                r0[r1] = r2
                r1 = 79
                r2 = 14
                r0[r1] = r2
                r1 = 80
                r2 = 15
                r0[r1] = r2
                r1 = 81
                r2 = 16
                r0[r1] = r2
                r1 = 82
                r2 = 17
                r0[r1] = r2
                r1 = 83
                r2 = 18
                r0[r1] = r2
                r1 = 84
                r2 = 19
                r0[r1] = r2
                r1 = 85
                r2 = 20
                r0[r1] = r2
                r1 = 86
                r2 = 21
                r0[r1] = r2
                r1 = 87
                r2 = 22
                r0[r1] = r2
                r1 = 88
                r2 = 23
                r0[r1] = r2
                r1 = 89
                r2 = 24
                r0[r1] = r2
                r1 = 90
                r2 = 25
                r0[r1] = r2
                r1 = 91
                r0[r1] = r3
                r1 = 92
                r0[r1] = r3
                r1 = 93
                r0[r1] = r3
                r1 = 94
                r0[r1] = r3
                r1 = 95
                r0[r1] = r3
                r1 = 96
                r0[r1] = r3
                r1 = 97
                r2 = 26
                r0[r1] = r2
                r1 = 98
                r2 = 27
                r0[r1] = r2
                r1 = 99
                r2 = 28
                r0[r1] = r2
                r1 = 100
                r2 = 29
                r0[r1] = r2
                r1 = 101(0x65, float:1.42E-43)
                r2 = 30
                r0[r1] = r2
                r1 = 102(0x66, float:1.43E-43)
                r2 = 31
                r0[r1] = r2
                r1 = 103(0x67, float:1.44E-43)
                r2 = 32
                r0[r1] = r2
                r1 = 104(0x68, float:1.46E-43)
                r2 = 33
                r0[r1] = r2
                r1 = 105(0x69, float:1.47E-43)
                r2 = 34
                r0[r1] = r2
                r1 = 106(0x6a, float:1.49E-43)
                r2 = 35
                r0[r1] = r2
                r1 = 107(0x6b, float:1.5E-43)
                r2 = 36
                r0[r1] = r2
                r1 = 108(0x6c, float:1.51E-43)
                r2 = 37
                r0[r1] = r2
                r1 = 109(0x6d, float:1.53E-43)
                r2 = 38
                r0[r1] = r2
                r1 = 110(0x6e, float:1.54E-43)
                r2 = 39
                r0[r1] = r2
                r1 = 111(0x6f, float:1.56E-43)
                r2 = 40
                r0[r1] = r2
                r1 = 112(0x70, float:1.57E-43)
                r2 = 41
                r0[r1] = r2
                r1 = 113(0x71, float:1.58E-43)
                r2 = 42
                r0[r1] = r2
                r1 = 114(0x72, float:1.6E-43)
                r2 = 43
                r0[r1] = r2
                r1 = 115(0x73, float:1.61E-43)
                r2 = 44
                r0[r1] = r2
                r1 = 116(0x74, float:1.63E-43)
                r2 = 45
                r0[r1] = r2
                r1 = 117(0x75, float:1.64E-43)
                r2 = 46
                r0[r1] = r2
                r1 = 118(0x76, float:1.65E-43)
                r2 = 47
                r0[r1] = r2
                r1 = 119(0x77, float:1.67E-43)
                r2 = 48
                r0[r1] = r2
                r1 = 120(0x78, float:1.68E-43)
                r2 = 49
                r0[r1] = r2
                r1 = 121(0x79, float:1.7E-43)
                r2 = 50
                r0[r1] = r2
                r1 = 122(0x7a, float:1.71E-43)
                r2 = 51
                r0[r1] = r2
                r1 = 123(0x7b, float:1.72E-43)
                r0[r1] = r3
                r1 = 124(0x7c, float:1.74E-43)
                r0[r1] = r3
                r1 = 125(0x7d, float:1.75E-43)
                r0[r1] = r3
                r1 = 126(0x7e, float:1.77E-43)
                r0[r1] = r3
                r1 = 127(0x7f, float:1.78E-43)
                r0[r1] = r3
                r1 = 128(0x80, float:1.794E-43)
                r0[r1] = r3
                r1 = 129(0x81, float:1.81E-43)
                r0[r1] = r3
                r1 = 130(0x82, float:1.82E-43)
                r0[r1] = r3
                r1 = 131(0x83, float:1.84E-43)
                r0[r1] = r3
                r1 = 132(0x84, float:1.85E-43)
                r0[r1] = r3
                r1 = 133(0x85, float:1.86E-43)
                r0[r1] = r3
                r1 = 134(0x86, float:1.88E-43)
                r0[r1] = r3
                r1 = 135(0x87, float:1.89E-43)
                r0[r1] = r3
                r1 = 136(0x88, float:1.9E-43)
                r0[r1] = r3
                r1 = 137(0x89, float:1.92E-43)
                r0[r1] = r3
                r1 = 138(0x8a, float:1.93E-43)
                r0[r1] = r3
                r1 = 139(0x8b, float:1.95E-43)
                r0[r1] = r3
                r1 = 140(0x8c, float:1.96E-43)
                r0[r1] = r3
                r1 = 141(0x8d, float:1.98E-43)
                r0[r1] = r3
                r1 = 142(0x8e, float:1.99E-43)
                r0[r1] = r3
                r1 = 143(0x8f, float:2.0E-43)
                r0[r1] = r3
                r1 = 144(0x90, float:2.02E-43)
                r0[r1] = r3
                r1 = 145(0x91, float:2.03E-43)
                r0[r1] = r3
                r1 = 146(0x92, float:2.05E-43)
                r0[r1] = r3
                r1 = 147(0x93, float:2.06E-43)
                r0[r1] = r3
                r1 = 148(0x94, float:2.07E-43)
                r0[r1] = r3
                r1 = 149(0x95, float:2.09E-43)
                r0[r1] = r3
                r1 = 150(0x96, float:2.1E-43)
                r0[r1] = r3
                r1 = 151(0x97, float:2.12E-43)
                r0[r1] = r3
                r1 = 152(0x98, float:2.13E-43)
                r0[r1] = r3
                r1 = 153(0x99, float:2.14E-43)
                r0[r1] = r3
                r1 = 154(0x9a, float:2.16E-43)
                r0[r1] = r3
                r1 = 155(0x9b, float:2.17E-43)
                r0[r1] = r3
                r1 = 156(0x9c, float:2.19E-43)
                r0[r1] = r3
                r1 = 157(0x9d, float:2.2E-43)
                r0[r1] = r3
                r1 = 158(0x9e, float:2.21E-43)
                r0[r1] = r3
                r1 = 159(0x9f, float:2.23E-43)
                r0[r1] = r3
                r1 = 160(0xa0, float:2.24E-43)
                r0[r1] = r3
                r1 = 161(0xa1, float:2.26E-43)
                r0[r1] = r3
                r1 = 162(0xa2, float:2.27E-43)
                r0[r1] = r3
                r1 = 163(0xa3, float:2.28E-43)
                r0[r1] = r3
                r1 = 164(0xa4, float:2.3E-43)
                r0[r1] = r3
                r1 = 165(0xa5, float:2.31E-43)
                r0[r1] = r3
                r1 = 166(0xa6, float:2.33E-43)
                r0[r1] = r3
                r1 = 167(0xa7, float:2.34E-43)
                r0[r1] = r3
                r1 = 168(0xa8, float:2.35E-43)
                r0[r1] = r3
                r1 = 169(0xa9, float:2.37E-43)
                r0[r1] = r3
                r1 = 170(0xaa, float:2.38E-43)
                r0[r1] = r3
                r1 = 171(0xab, float:2.4E-43)
                r0[r1] = r3
                r1 = 172(0xac, float:2.41E-43)
                r0[r1] = r3
                r1 = 173(0xad, float:2.42E-43)
                r0[r1] = r3
                r1 = 174(0xae, float:2.44E-43)
                r0[r1] = r3
                r1 = 175(0xaf, float:2.45E-43)
                r0[r1] = r3
                r1 = 176(0xb0, float:2.47E-43)
                r0[r1] = r3
                r1 = 177(0xb1, float:2.48E-43)
                r0[r1] = r3
                r1 = 178(0xb2, float:2.5E-43)
                r0[r1] = r3
                r1 = 179(0xb3, float:2.51E-43)
                r0[r1] = r3
                r1 = 180(0xb4, float:2.52E-43)
                r0[r1] = r3
                r1 = 181(0xb5, float:2.54E-43)
                r0[r1] = r3
                r1 = 182(0xb6, float:2.55E-43)
                r0[r1] = r3
                r1 = 183(0xb7, float:2.56E-43)
                r0[r1] = r3
                r1 = 184(0xb8, float:2.58E-43)
                r0[r1] = r3
                r1 = 185(0xb9, float:2.59E-43)
                r0[r1] = r3
                r1 = 186(0xba, float:2.6E-43)
                r0[r1] = r3
                r1 = 187(0xbb, float:2.62E-43)
                r0[r1] = r3
                r1 = 188(0xbc, float:2.63E-43)
                r0[r1] = r3
                r1 = 189(0xbd, float:2.65E-43)
                r0[r1] = r3
                r1 = 190(0xbe, float:2.66E-43)
                r0[r1] = r3
                r1 = 191(0xbf, float:2.68E-43)
                r0[r1] = r3
                r1 = 192(0xc0, float:2.69E-43)
                r0[r1] = r3
                r1 = 193(0xc1, float:2.7E-43)
                r0[r1] = r3
                r1 = 194(0xc2, float:2.72E-43)
                r0[r1] = r3
                r1 = 195(0xc3, float:2.73E-43)
                r0[r1] = r3
                r1 = 196(0xc4, float:2.75E-43)
                r0[r1] = r3
                r1 = 197(0xc5, float:2.76E-43)
                r0[r1] = r3
                r1 = 198(0xc6, float:2.77E-43)
                r0[r1] = r3
                r1 = 199(0xc7, float:2.79E-43)
                r0[r1] = r3
                r1 = 200(0xc8, float:2.8E-43)
                r0[r1] = r3
                r1 = 201(0xc9, float:2.82E-43)
                r0[r1] = r3
                r1 = 202(0xca, float:2.83E-43)
                r0[r1] = r3
                r1 = 203(0xcb, float:2.84E-43)
                r0[r1] = r3
                r1 = 204(0xcc, float:2.86E-43)
                r0[r1] = r3
                r1 = 205(0xcd, float:2.87E-43)
                r0[r1] = r3
                r1 = 206(0xce, float:2.89E-43)
                r0[r1] = r3
                r1 = 207(0xcf, float:2.9E-43)
                r0[r1] = r3
                r1 = 208(0xd0, float:2.91E-43)
                r0[r1] = r3
                r1 = 209(0xd1, float:2.93E-43)
                r0[r1] = r3
                r1 = 210(0xd2, float:2.94E-43)
                r0[r1] = r3
                r1 = 211(0xd3, float:2.96E-43)
                r0[r1] = r3
                r1 = 212(0xd4, float:2.97E-43)
                r0[r1] = r3
                r1 = 213(0xd5, float:2.98E-43)
                r0[r1] = r3
                r1 = 214(0xd6, float:3.0E-43)
                r0[r1] = r3
                r1 = 215(0xd7, float:3.01E-43)
                r0[r1] = r3
                r1 = 216(0xd8, float:3.03E-43)
                r0[r1] = r3
                r1 = 217(0xd9, float:3.04E-43)
                r0[r1] = r3
                r1 = 218(0xda, float:3.05E-43)
                r0[r1] = r3
                r1 = 219(0xdb, float:3.07E-43)
                r0[r1] = r3
                r1 = 220(0xdc, float:3.08E-43)
                r0[r1] = r3
                r1 = 221(0xdd, float:3.1E-43)
                r0[r1] = r3
                r1 = 222(0xde, float:3.11E-43)
                r0[r1] = r3
                r1 = 223(0xdf, float:3.12E-43)
                r0[r1] = r3
                r1 = 224(0xe0, float:3.14E-43)
                r0[r1] = r3
                r1 = 225(0xe1, float:3.15E-43)
                r0[r1] = r3
                r1 = 226(0xe2, float:3.17E-43)
                r0[r1] = r3
                r1 = 227(0xe3, float:3.18E-43)
                r0[r1] = r3
                r1 = 228(0xe4, float:3.2E-43)
                r0[r1] = r3
                r1 = 229(0xe5, float:3.21E-43)
                r0[r1] = r3
                r1 = 230(0xe6, float:3.22E-43)
                r0[r1] = r3
                r1 = 231(0xe7, float:3.24E-43)
                r0[r1] = r3
                r1 = 232(0xe8, float:3.25E-43)
                r0[r1] = r3
                r1 = 233(0xe9, float:3.27E-43)
                r0[r1] = r3
                r1 = 234(0xea, float:3.28E-43)
                r0[r1] = r3
                r1 = 235(0xeb, float:3.3E-43)
                r0[r1] = r3
                r1 = 236(0xec, float:3.31E-43)
                r0[r1] = r3
                r1 = 237(0xed, float:3.32E-43)
                r0[r1] = r3
                r1 = 238(0xee, float:3.34E-43)
                r0[r1] = r3
                r1 = 239(0xef, float:3.35E-43)
                r0[r1] = r3
                r1 = 240(0xf0, float:3.36E-43)
                r0[r1] = r3
                r1 = 241(0xf1, float:3.38E-43)
                r0[r1] = r3
                r1 = 242(0xf2, float:3.39E-43)
                r0[r1] = r3
                r1 = 243(0xf3, float:3.4E-43)
                r0[r1] = r3
                r1 = 244(0xf4, float:3.42E-43)
                r0[r1] = r3
                r1 = 245(0xf5, float:3.43E-43)
                r0[r1] = r3
                r1 = 246(0xf6, float:3.45E-43)
                r0[r1] = r3
                r1 = 247(0xf7, float:3.46E-43)
                r0[r1] = r3
                r1 = 248(0xf8, float:3.48E-43)
                r0[r1] = r3
                r1 = 249(0xf9, float:3.49E-43)
                r0[r1] = r3
                r1 = 250(0xfa, float:3.5E-43)
                r0[r1] = r3
                r1 = 251(0xfb, float:3.52E-43)
                r0[r1] = r3
                r1 = 252(0xfc, float:3.53E-43)
                r0[r1] = r3
                r1 = 253(0xfd, float:3.55E-43)
                r0[r1] = r3
                r1 = 254(0xfe, float:3.56E-43)
                r0[r1] = r3
                r1 = 255(0xff, float:3.57E-43)
                r0[r1] = r3
                org.anddev.andengine.util.Base64.Decoder.DECODE = r0
                r0 = 256(0x100, float:3.59E-43)
                int[] r0 = new int[r0]
                r1 = 0
                r0[r1] = r3
                r0[r4] = r3
                r0[r5] = r3
                r0[r6] = r3
                r0[r7] = r3
                r1 = 5
                r0[r1] = r3
                r1 = 6
                r0[r1] = r3
                r1 = 7
                r0[r1] = r3
                r1 = 8
                r0[r1] = r3
                r1 = 9
                r0[r1] = r3
                r1 = 10
                r0[r1] = r3
                r1 = 11
                r0[r1] = r3
                r1 = 12
                r0[r1] = r3
                r1 = 13
                r0[r1] = r3
                r1 = 14
                r0[r1] = r3
                r1 = 15
                r0[r1] = r3
                r1 = 16
                r0[r1] = r3
                r1 = 17
                r0[r1] = r3
                r1 = 18
                r0[r1] = r3
                r1 = 19
                r0[r1] = r3
                r1 = 20
                r0[r1] = r3
                r1 = 21
                r0[r1] = r3
                r1 = 22
                r0[r1] = r3
                r1 = 23
                r0[r1] = r3
                r1 = 24
                r0[r1] = r3
                r1 = 25
                r0[r1] = r3
                r1 = 26
                r0[r1] = r3
                r1 = 27
                r0[r1] = r3
                r1 = 28
                r0[r1] = r3
                r1 = 29
                r0[r1] = r3
                r1 = 30
                r0[r1] = r3
                r1 = 31
                r0[r1] = r3
                r1 = 32
                r0[r1] = r3
                r1 = 33
                r0[r1] = r3
                r1 = 34
                r0[r1] = r3
                r1 = 35
                r0[r1] = r3
                r1 = 36
                r0[r1] = r3
                r1 = 37
                r0[r1] = r3
                r1 = 38
                r0[r1] = r3
                r1 = 39
                r0[r1] = r3
                r1 = 40
                r0[r1] = r3
                r1 = 41
                r0[r1] = r3
                r1 = 42
                r0[r1] = r3
                r1 = 43
                r0[r1] = r3
                r1 = 44
                r0[r1] = r3
                r1 = 45
                r2 = 62
                r0[r1] = r2
                r1 = 46
                r0[r1] = r3
                r1 = 47
                r0[r1] = r3
                r1 = 48
                r2 = 52
                r0[r1] = r2
                r1 = 49
                r2 = 53
                r0[r1] = r2
                r1 = 50
                r2 = 54
                r0[r1] = r2
                r1 = 51
                r2 = 55
                r0[r1] = r2
                r1 = 52
                r2 = 56
                r0[r1] = r2
                r1 = 53
                r2 = 57
                r0[r1] = r2
                r1 = 54
                r2 = 58
                r0[r1] = r2
                r1 = 55
                r2 = 59
                r0[r1] = r2
                r1 = 56
                r2 = 60
                r0[r1] = r2
                r1 = 57
                r2 = 61
                r0[r1] = r2
                r1 = 58
                r0[r1] = r3
                r1 = 59
                r0[r1] = r3
                r1 = 60
                r0[r1] = r3
                r1 = 61
                r2 = -2
                r0[r1] = r2
                r1 = 62
                r0[r1] = r3
                r1 = 63
                r0[r1] = r3
                r1 = 64
                r0[r1] = r3
                r1 = 66
                r0[r1] = r4
                r1 = 67
                r0[r1] = r5
                r1 = 68
                r0[r1] = r6
                r1 = 69
                r0[r1] = r7
                r1 = 70
                r2 = 5
                r0[r1] = r2
                r1 = 71
                r2 = 6
                r0[r1] = r2
                r1 = 72
                r2 = 7
                r0[r1] = r2
                r1 = 73
                r2 = 8
                r0[r1] = r2
                r1 = 74
                r2 = 9
                r0[r1] = r2
                r1 = 75
                r2 = 10
                r0[r1] = r2
                r1 = 76
                r2 = 11
                r0[r1] = r2
                r1 = 77
                r2 = 12
                r0[r1] = r2
                r1 = 78
                r2 = 13
                r0[r1] = r2
                r1 = 79
                r2 = 14
                r0[r1] = r2
                r1 = 80
                r2 = 15
                r0[r1] = r2
                r1 = 81
                r2 = 16
                r0[r1] = r2
                r1 = 82
                r2 = 17
                r0[r1] = r2
                r1 = 83
                r2 = 18
                r0[r1] = r2
                r1 = 84
                r2 = 19
                r0[r1] = r2
                r1 = 85
                r2 = 20
                r0[r1] = r2
                r1 = 86
                r2 = 21
                r0[r1] = r2
                r1 = 87
                r2 = 22
                r0[r1] = r2
                r1 = 88
                r2 = 23
                r0[r1] = r2
                r1 = 89
                r2 = 24
                r0[r1] = r2
                r1 = 90
                r2 = 25
                r0[r1] = r2
                r1 = 91
                r0[r1] = r3
                r1 = 92
                r0[r1] = r3
                r1 = 93
                r0[r1] = r3
                r1 = 94
                r0[r1] = r3
                r1 = 95
                r2 = 63
                r0[r1] = r2
                r1 = 96
                r0[r1] = r3
                r1 = 97
                r2 = 26
                r0[r1] = r2
                r1 = 98
                r2 = 27
                r0[r1] = r2
                r1 = 99
                r2 = 28
                r0[r1] = r2
                r1 = 100
                r2 = 29
                r0[r1] = r2
                r1 = 101(0x65, float:1.42E-43)
                r2 = 30
                r0[r1] = r2
                r1 = 102(0x66, float:1.43E-43)
                r2 = 31
                r0[r1] = r2
                r1 = 103(0x67, float:1.44E-43)
                r2 = 32
                r0[r1] = r2
                r1 = 104(0x68, float:1.46E-43)
                r2 = 33
                r0[r1] = r2
                r1 = 105(0x69, float:1.47E-43)
                r2 = 34
                r0[r1] = r2
                r1 = 106(0x6a, float:1.49E-43)
                r2 = 35
                r0[r1] = r2
                r1 = 107(0x6b, float:1.5E-43)
                r2 = 36
                r0[r1] = r2
                r1 = 108(0x6c, float:1.51E-43)
                r2 = 37
                r0[r1] = r2
                r1 = 109(0x6d, float:1.53E-43)
                r2 = 38
                r0[r1] = r2
                r1 = 110(0x6e, float:1.54E-43)
                r2 = 39
                r0[r1] = r2
                r1 = 111(0x6f, float:1.56E-43)
                r2 = 40
                r0[r1] = r2
                r1 = 112(0x70, float:1.57E-43)
                r2 = 41
                r0[r1] = r2
                r1 = 113(0x71, float:1.58E-43)
                r2 = 42
                r0[r1] = r2
                r1 = 114(0x72, float:1.6E-43)
                r2 = 43
                r0[r1] = r2
                r1 = 115(0x73, float:1.61E-43)
                r2 = 44
                r0[r1] = r2
                r1 = 116(0x74, float:1.63E-43)
                r2 = 45
                r0[r1] = r2
                r1 = 117(0x75, float:1.64E-43)
                r2 = 46
                r0[r1] = r2
                r1 = 118(0x76, float:1.65E-43)
                r2 = 47
                r0[r1] = r2
                r1 = 119(0x77, float:1.67E-43)
                r2 = 48
                r0[r1] = r2
                r1 = 120(0x78, float:1.68E-43)
                r2 = 49
                r0[r1] = r2
                r1 = 121(0x79, float:1.7E-43)
                r2 = 50
                r0[r1] = r2
                r1 = 122(0x7a, float:1.71E-43)
                r2 = 51
                r0[r1] = r2
                r1 = 123(0x7b, float:1.72E-43)
                r0[r1] = r3
                r1 = 124(0x7c, float:1.74E-43)
                r0[r1] = r3
                r1 = 125(0x7d, float:1.75E-43)
                r0[r1] = r3
                r1 = 126(0x7e, float:1.77E-43)
                r0[r1] = r3
                r1 = 127(0x7f, float:1.78E-43)
                r0[r1] = r3
                r1 = 128(0x80, float:1.794E-43)
                r0[r1] = r3
                r1 = 129(0x81, float:1.81E-43)
                r0[r1] = r3
                r1 = 130(0x82, float:1.82E-43)
                r0[r1] = r3
                r1 = 131(0x83, float:1.84E-43)
                r0[r1] = r3
                r1 = 132(0x84, float:1.85E-43)
                r0[r1] = r3
                r1 = 133(0x85, float:1.86E-43)
                r0[r1] = r3
                r1 = 134(0x86, float:1.88E-43)
                r0[r1] = r3
                r1 = 135(0x87, float:1.89E-43)
                r0[r1] = r3
                r1 = 136(0x88, float:1.9E-43)
                r0[r1] = r3
                r1 = 137(0x89, float:1.92E-43)
                r0[r1] = r3
                r1 = 138(0x8a, float:1.93E-43)
                r0[r1] = r3
                r1 = 139(0x8b, float:1.95E-43)
                r0[r1] = r3
                r1 = 140(0x8c, float:1.96E-43)
                r0[r1] = r3
                r1 = 141(0x8d, float:1.98E-43)
                r0[r1] = r3
                r1 = 142(0x8e, float:1.99E-43)
                r0[r1] = r3
                r1 = 143(0x8f, float:2.0E-43)
                r0[r1] = r3
                r1 = 144(0x90, float:2.02E-43)
                r0[r1] = r3
                r1 = 145(0x91, float:2.03E-43)
                r0[r1] = r3
                r1 = 146(0x92, float:2.05E-43)
                r0[r1] = r3
                r1 = 147(0x93, float:2.06E-43)
                r0[r1] = r3
                r1 = 148(0x94, float:2.07E-43)
                r0[r1] = r3
                r1 = 149(0x95, float:2.09E-43)
                r0[r1] = r3
                r1 = 150(0x96, float:2.1E-43)
                r0[r1] = r3
                r1 = 151(0x97, float:2.12E-43)
                r0[r1] = r3
                r1 = 152(0x98, float:2.13E-43)
                r0[r1] = r3
                r1 = 153(0x99, float:2.14E-43)
                r0[r1] = r3
                r1 = 154(0x9a, float:2.16E-43)
                r0[r1] = r3
                r1 = 155(0x9b, float:2.17E-43)
                r0[r1] = r3
                r1 = 156(0x9c, float:2.19E-43)
                r0[r1] = r3
                r1 = 157(0x9d, float:2.2E-43)
                r0[r1] = r3
                r1 = 158(0x9e, float:2.21E-43)
                r0[r1] = r3
                r1 = 159(0x9f, float:2.23E-43)
                r0[r1] = r3
                r1 = 160(0xa0, float:2.24E-43)
                r0[r1] = r3
                r1 = 161(0xa1, float:2.26E-43)
                r0[r1] = r3
                r1 = 162(0xa2, float:2.27E-43)
                r0[r1] = r3
                r1 = 163(0xa3, float:2.28E-43)
                r0[r1] = r3
                r1 = 164(0xa4, float:2.3E-43)
                r0[r1] = r3
                r1 = 165(0xa5, float:2.31E-43)
                r0[r1] = r3
                r1 = 166(0xa6, float:2.33E-43)
                r0[r1] = r3
                r1 = 167(0xa7, float:2.34E-43)
                r0[r1] = r3
                r1 = 168(0xa8, float:2.35E-43)
                r0[r1] = r3
                r1 = 169(0xa9, float:2.37E-43)
                r0[r1] = r3
                r1 = 170(0xaa, float:2.38E-43)
                r0[r1] = r3
                r1 = 171(0xab, float:2.4E-43)
                r0[r1] = r3
                r1 = 172(0xac, float:2.41E-43)
                r0[r1] = r3
                r1 = 173(0xad, float:2.42E-43)
                r0[r1] = r3
                r1 = 174(0xae, float:2.44E-43)
                r0[r1] = r3
                r1 = 175(0xaf, float:2.45E-43)
                r0[r1] = r3
                r1 = 176(0xb0, float:2.47E-43)
                r0[r1] = r3
                r1 = 177(0xb1, float:2.48E-43)
                r0[r1] = r3
                r1 = 178(0xb2, float:2.5E-43)
                r0[r1] = r3
                r1 = 179(0xb3, float:2.51E-43)
                r0[r1] = r3
                r1 = 180(0xb4, float:2.52E-43)
                r0[r1] = r3
                r1 = 181(0xb5, float:2.54E-43)
                r0[r1] = r3
                r1 = 182(0xb6, float:2.55E-43)
                r0[r1] = r3
                r1 = 183(0xb7, float:2.56E-43)
                r0[r1] = r3
                r1 = 184(0xb8, float:2.58E-43)
                r0[r1] = r3
                r1 = 185(0xb9, float:2.59E-43)
                r0[r1] = r3
                r1 = 186(0xba, float:2.6E-43)
                r0[r1] = r3
                r1 = 187(0xbb, float:2.62E-43)
                r0[r1] = r3
                r1 = 188(0xbc, float:2.63E-43)
                r0[r1] = r3
                r1 = 189(0xbd, float:2.65E-43)
                r0[r1] = r3
                r1 = 190(0xbe, float:2.66E-43)
                r0[r1] = r3
                r1 = 191(0xbf, float:2.68E-43)
                r0[r1] = r3
                r1 = 192(0xc0, float:2.69E-43)
                r0[r1] = r3
                r1 = 193(0xc1, float:2.7E-43)
                r0[r1] = r3
                r1 = 194(0xc2, float:2.72E-43)
                r0[r1] = r3
                r1 = 195(0xc3, float:2.73E-43)
                r0[r1] = r3
                r1 = 196(0xc4, float:2.75E-43)
                r0[r1] = r3
                r1 = 197(0xc5, float:2.76E-43)
                r0[r1] = r3
                r1 = 198(0xc6, float:2.77E-43)
                r0[r1] = r3
                r1 = 199(0xc7, float:2.79E-43)
                r0[r1] = r3
                r1 = 200(0xc8, float:2.8E-43)
                r0[r1] = r3
                r1 = 201(0xc9, float:2.82E-43)
                r0[r1] = r3
                r1 = 202(0xca, float:2.83E-43)
                r0[r1] = r3
                r1 = 203(0xcb, float:2.84E-43)
                r0[r1] = r3
                r1 = 204(0xcc, float:2.86E-43)
                r0[r1] = r3
                r1 = 205(0xcd, float:2.87E-43)
                r0[r1] = r3
                r1 = 206(0xce, float:2.89E-43)
                r0[r1] = r3
                r1 = 207(0xcf, float:2.9E-43)
                r0[r1] = r3
                r1 = 208(0xd0, float:2.91E-43)
                r0[r1] = r3
                r1 = 209(0xd1, float:2.93E-43)
                r0[r1] = r3
                r1 = 210(0xd2, float:2.94E-43)
                r0[r1] = r3
                r1 = 211(0xd3, float:2.96E-43)
                r0[r1] = r3
                r1 = 212(0xd4, float:2.97E-43)
                r0[r1] = r3
                r1 = 213(0xd5, float:2.98E-43)
                r0[r1] = r3
                r1 = 214(0xd6, float:3.0E-43)
                r0[r1] = r3
                r1 = 215(0xd7, float:3.01E-43)
                r0[r1] = r3
                r1 = 216(0xd8, float:3.03E-43)
                r0[r1] = r3
                r1 = 217(0xd9, float:3.04E-43)
                r0[r1] = r3
                r1 = 218(0xda, float:3.05E-43)
                r0[r1] = r3
                r1 = 219(0xdb, float:3.07E-43)
                r0[r1] = r3
                r1 = 220(0xdc, float:3.08E-43)
                r0[r1] = r3
                r1 = 221(0xdd, float:3.1E-43)
                r0[r1] = r3
                r1 = 222(0xde, float:3.11E-43)
                r0[r1] = r3
                r1 = 223(0xdf, float:3.12E-43)
                r0[r1] = r3
                r1 = 224(0xe0, float:3.14E-43)
                r0[r1] = r3
                r1 = 225(0xe1, float:3.15E-43)
                r0[r1] = r3
                r1 = 226(0xe2, float:3.17E-43)
                r0[r1] = r3
                r1 = 227(0xe3, float:3.18E-43)
                r0[r1] = r3
                r1 = 228(0xe4, float:3.2E-43)
                r0[r1] = r3
                r1 = 229(0xe5, float:3.21E-43)
                r0[r1] = r3
                r1 = 230(0xe6, float:3.22E-43)
                r0[r1] = r3
                r1 = 231(0xe7, float:3.24E-43)
                r0[r1] = r3
                r1 = 232(0xe8, float:3.25E-43)
                r0[r1] = r3
                r1 = 233(0xe9, float:3.27E-43)
                r0[r1] = r3
                r1 = 234(0xea, float:3.28E-43)
                r0[r1] = r3
                r1 = 235(0xeb, float:3.3E-43)
                r0[r1] = r3
                r1 = 236(0xec, float:3.31E-43)
                r0[r1] = r3
                r1 = 237(0xed, float:3.32E-43)
                r0[r1] = r3
                r1 = 238(0xee, float:3.34E-43)
                r0[r1] = r3
                r1 = 239(0xef, float:3.35E-43)
                r0[r1] = r3
                r1 = 240(0xf0, float:3.36E-43)
                r0[r1] = r3
                r1 = 241(0xf1, float:3.38E-43)
                r0[r1] = r3
                r1 = 242(0xf2, float:3.39E-43)
                r0[r1] = r3
                r1 = 243(0xf3, float:3.4E-43)
                r0[r1] = r3
                r1 = 244(0xf4, float:3.42E-43)
                r0[r1] = r3
                r1 = 245(0xf5, float:3.43E-43)
                r0[r1] = r3
                r1 = 246(0xf6, float:3.45E-43)
                r0[r1] = r3
                r1 = 247(0xf7, float:3.46E-43)
                r0[r1] = r3
                r1 = 248(0xf8, float:3.48E-43)
                r0[r1] = r3
                r1 = 249(0xf9, float:3.49E-43)
                r0[r1] = r3
                r1 = 250(0xfa, float:3.5E-43)
                r0[r1] = r3
                r1 = 251(0xfb, float:3.52E-43)
                r0[r1] = r3
                r1 = 252(0xfc, float:3.53E-43)
                r0[r1] = r3
                r1 = 253(0xfd, float:3.55E-43)
                r0[r1] = r3
                r1 = 254(0xfe, float:3.56E-43)
                r0[r1] = r3
                r1 = 255(0xff, float:3.57E-43)
                r0[r1] = r3
                org.anddev.andengine.util.Base64.Decoder.DECODE_WEBSAFE = r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.util.Base64.Decoder.<clinit>():void");
        }

        public Decoder(int flags, byte[] output) {
            this.output = output;
            this.alphabet = (flags & 8) == 0 ? DECODE : DECODE_WEBSAFE;
            this.state = 0;
            this.value = 0;
        }

        public int maxOutputSize(int len) {
            return ((len * 3) / 4) + 10;
        }

        public boolean process(byte[] input, int offset, int len, boolean finish) {
            int op;
            int op2;
            if (this.state == 6) {
                return Base64.$assertionsDisabled;
            }
            int p = offset;
            int len2 = len + offset;
            int state2 = this.state;
            int value2 = this.value;
            int op3 = 0;
            byte[] output = this.output;
            int[] alphabet2 = this.alphabet;
            while (true) {
                if (p >= len2) {
                    op = op3;
                } else {
                    if (state2 == 0) {
                        while (p + 4 <= len2 && (value2 = (alphabet2[input[p] & 255] << 18) | (alphabet2[input[p + 1] & 255] << 12) | (alphabet2[input[p + 2] & 255] << 6) | alphabet2[input[p + 3] & 255]) >= 0) {
                            output[op3 + 2] = (byte) value2;
                            output[op3 + 1] = (byte) (value2 >> 8);
                            output[op3] = (byte) (value2 >> 16);
                            op3 += 3;
                            p += 4;
                        }
                        if (p >= len2) {
                            op = op3;
                        }
                    }
                    int p2 = p + 1;
                    int d = alphabet2[input[p] & 255];
                    switch (state2) {
                        case 0:
                            if (d >= 0) {
                                value2 = d;
                                state2++;
                                p = p2;
                                continue;
                            } else if (d != -1) {
                                this.state = 6;
                                return Base64.$assertionsDisabled;
                            }
                            break;
                        case 1:
                            if (d >= 0) {
                                value2 = (value2 << 6) | d;
                                state2++;
                                p = p2;
                                continue;
                            } else if (d != -1) {
                                this.state = 6;
                                return Base64.$assertionsDisabled;
                            }
                            break;
                        case 2:
                            if (d >= 0) {
                                value2 = (value2 << 6) | d;
                                state2++;
                                p = p2;
                                continue;
                            } else if (d == EQUALS) {
                                output[op3] = (byte) (value2 >> 4);
                                state2 = 4;
                                op3++;
                                p = p2;
                            } else if (d != -1) {
                                this.state = 6;
                                return Base64.$assertionsDisabled;
                            }
                            break;
                        case 3:
                            if (d >= 0) {
                                value2 = (value2 << 6) | d;
                                output[op3 + 2] = (byte) value2;
                                output[op3 + 1] = (byte) (value2 >> 8);
                                output[op3] = (byte) (value2 >> 16);
                                op3 += 3;
                                state2 = 0;
                                p = p2;
                                continue;
                            } else if (d == EQUALS) {
                                output[op3 + 1] = (byte) (value2 >> 2);
                                output[op3] = (byte) (value2 >> 10);
                                op3 += 2;
                                state2 = 5;
                                p = p2;
                            } else if (d != -1) {
                                this.state = 6;
                                return Base64.$assertionsDisabled;
                            }
                            break;
                        case 4:
                            if (d == EQUALS) {
                                state2++;
                                p = p2;
                                continue;
                            } else if (d != -1) {
                                this.state = 6;
                                return Base64.$assertionsDisabled;
                            }
                            break;
                        case 5:
                            if (d != -1) {
                                this.state = 6;
                                return Base64.$assertionsDisabled;
                            }
                            break;
                    }
                    p = p2;
                }
            }
            if (!finish) {
                this.state = state2;
                this.value = value2;
                this.op = op;
                return true;
            }
            switch (state2) {
                case 0:
                    op2 = op;
                    break;
                case 1:
                    this.state = 6;
                    return Base64.$assertionsDisabled;
                case 2:
                    op2 = op + 1;
                    output[op] = (byte) (value2 >> 4);
                    break;
                case 3:
                    int op4 = op + 1;
                    output[op] = (byte) (value2 >> 10);
                    output[op4] = (byte) (value2 >> 2);
                    op2 = op4 + 1;
                    break;
                case 4:
                    this.state = 6;
                    return Base64.$assertionsDisabled;
                default:
                    op2 = op;
                    break;
            }
            this.state = state2;
            this.op = op2;
            return true;
        }
    }

    public static String encodeToString(byte[] input, int flags) {
        try {
            return new String(encode(input, flags), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public static String encodeToString(byte[] input, int offset, int len, int flags) {
        try {
            return new String(encode(input, offset, len, flags), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] encode(byte[] input, int flags) {
        return encode(input, 0, input.length, flags);
    }

    public static byte[] encode(byte[] input, int offset, int len, int flags) {
        int i;
        Encoder encoder = new Encoder(flags, null);
        int output_len = (len / 3) * 4;
        if (!encoder.do_padding) {
            switch (len % 3) {
                case 1:
                    output_len += 2;
                    break;
                case 2:
                    output_len += 3;
                    break;
            }
        } else if (len % 3 > 0) {
            output_len += 4;
        }
        if (encoder.do_newline && len > 0) {
            int i2 = ((len - 1) / 57) + 1;
            if (encoder.do_cr) {
                i = 2;
            } else {
                i = 1;
            }
            output_len += i2 * i;
        }
        encoder.output = new byte[output_len];
        encoder.process(input, offset, len, true);
        if ($assertionsDisabled || encoder.op == output_len) {
            return encoder.output;
        }
        throw new AssertionError();
    }

    static class Encoder extends Coder {
        static final /* synthetic */ boolean $assertionsDisabled;
        private static final byte[] ENCODE = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
        private static final byte[] ENCODE_WEBSAFE = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
        public static final int LINE_GROUPS = 19;
        private final byte[] alphabet;
        private int count;
        public final boolean do_cr;
        public final boolean do_newline;
        public final boolean do_padding;
        private final byte[] tail;
        int tailLen;

        static {
            boolean z;
            if (!Base64.class.desiredAssertionStatus()) {
                z = true;
            } else {
                z = $assertionsDisabled;
            }
            $assertionsDisabled = z;
        }

        public Encoder(int flags, byte[] output) {
            boolean z;
            boolean z2;
            this.output = output;
            this.do_padding = (flags & 1) == 0;
            if ((flags & 2) == 0) {
                z = true;
            } else {
                z = false;
            }
            this.do_newline = z;
            if ((flags & 4) != 0) {
                z2 = true;
            } else {
                z2 = false;
            }
            this.do_cr = z2;
            this.alphabet = (flags & 8) == 0 ? ENCODE : ENCODE_WEBSAFE;
            this.tail = new byte[2];
            this.tailLen = 0;
            this.count = this.do_newline ? 19 : -1;
        }

        public int maxOutputSize(int len) {
            return ((len * 8) / 5) + 10;
        }

        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
            */
        /* JADX WARNING: Removed duplicated region for block: B:13:0x005b  */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x010f  */
        /* JADX WARNING: Removed duplicated region for block: B:81:0x0216  */
        /* JADX WARNING: Removed duplicated region for block: B:93:0x0059 A[SYNTHETIC] */
        public boolean process(byte[] r16, int r17, int r18, boolean r19) {
            /*
                r15 = this;
                byte[] r2 = r15.alphabet
                byte[] r6 = r15.output
                r4 = 0
                int r3 = r15.count
                r7 = r17
                int r18 = r18 + r17
                r11 = -1
                int r12 = r15.tailLen
                switch(r12) {
                    case 0: goto L_0x0011;
                    case 1: goto L_0x00c1;
                    case 2: goto L_0x00e7;
                    default: goto L_0x0011;
                }
            L_0x0011:
                r12 = -1
                if (r11 == r12) goto L_0x0253
                int r5 = r4 + 1
                int r12 = r11 >> 18
                r12 = r12 & 63
                byte r12 = r2[r12]
                r6[r4] = r12
                int r4 = r5 + 1
                int r12 = r11 >> 12
                r12 = r12 & 63
                byte r12 = r2[r12]
                r6[r5] = r12
                int r5 = r4 + 1
                int r12 = r11 >> 6
                r12 = r12 & 63
                byte r12 = r2[r12]
                r6[r4] = r12
                int r4 = r5 + 1
                r12 = r11 & 63
                byte r12 = r2[r12]
                r6[r5] = r12
                int r3 = r3 + -1
                if (r3 != 0) goto L_0x0253
                boolean r12 = r15.do_cr
                if (r12 == 0) goto L_0x0049
                int r5 = r4 + 1
                r12 = 13
                r6[r4] = r12
                r4 = r5
            L_0x0049:
                int r5 = r4 + 1
                r12 = 10
                r6[r4] = r12
                r3 = 19
                r8 = r7
            L_0x0052:
                int r12 = r8 + 3
                r0 = r12
                r1 = r18
                if (r0 <= r1) goto L_0x010f
                if (r19 == 0) goto L_0x0216
                int r12 = r15.tailLen
                int r12 = r8 - r12
                r13 = 1
                int r13 = r18 - r13
                if (r12 != r13) goto L_0x016e
                r9 = 0
                int r12 = r15.tailLen
                if (r12 <= 0) goto L_0x0168
                byte[] r12 = r15.tail
                int r10 = r9 + 1
                byte r12 = r12[r9]
                r9 = r10
                r7 = r8
            L_0x0071:
                r12 = r12 & 255(0xff, float:3.57E-43)
                int r11 = r12 << 4
                int r12 = r15.tailLen
                int r12 = r12 - r9
                r15.tailLen = r12
                int r4 = r5 + 1
                int r12 = r11 >> 6
                r12 = r12 & 63
                byte r12 = r2[r12]
                r6[r5] = r12
                int r5 = r4 + 1
                r12 = r11 & 63
                byte r12 = r2[r12]
                r6[r4] = r12
                boolean r12 = r15.do_padding
                if (r12 == 0) goto L_0x009c
                int r4 = r5 + 1
                r12 = 61
                r6[r5] = r12
                int r5 = r4 + 1
                r12 = 61
                r6[r4] = r12
            L_0x009c:
                r4 = r5
                boolean r12 = r15.do_newline
                if (r12 == 0) goto L_0x00b3
                boolean r12 = r15.do_cr
                if (r12 == 0) goto L_0x00ac
                int r5 = r4 + 1
                r12 = 13
                r6[r4] = r12
                r4 = r5
            L_0x00ac:
                int r5 = r4 + 1
                r12 = 10
                r6[r4] = r12
                r4 = r5
            L_0x00b3:
                boolean r12 = org.anddev.andengine.util.Base64.Encoder.$assertionsDisabled
                if (r12 != 0) goto L_0x0207
                int r12 = r15.tailLen
                if (r12 == 0) goto L_0x0207
                java.lang.AssertionError r12 = new java.lang.AssertionError
                r12.<init>()
                throw r12
            L_0x00c1:
                int r12 = r7 + 2
                r0 = r12
                r1 = r18
                if (r0 > r1) goto L_0x0011
                byte[] r12 = r15.tail
                r13 = 0
                byte r12 = r12[r13]
                r12 = r12 & 255(0xff, float:3.57E-43)
                int r12 = r12 << 16
                int r8 = r7 + 1
                byte r13 = r16[r7]
                r13 = r13 & 255(0xff, float:3.57E-43)
                int r13 = r13 << 8
                r12 = r12 | r13
                int r7 = r8 + 1
                byte r13 = r16[r8]
                r13 = r13 & 255(0xff, float:3.57E-43)
                r11 = r12 | r13
                r12 = 0
                r15.tailLen = r12
                goto L_0x0011
            L_0x00e7:
                int r12 = r7 + 1
                r0 = r12
                r1 = r18
                if (r0 > r1) goto L_0x0011
                byte[] r12 = r15.tail
                r13 = 0
                byte r12 = r12[r13]
                r12 = r12 & 255(0xff, float:3.57E-43)
                int r12 = r12 << 16
                byte[] r13 = r15.tail
                r14 = 1
                byte r13 = r13[r14]
                r13 = r13 & 255(0xff, float:3.57E-43)
                int r13 = r13 << 8
                r12 = r12 | r13
                int r8 = r7 + 1
                byte r13 = r16[r7]
                r13 = r13 & 255(0xff, float:3.57E-43)
                r11 = r12 | r13
                r12 = 0
                r15.tailLen = r12
                r7 = r8
                goto L_0x0011
            L_0x010f:
                byte r12 = r16[r8]
                r12 = r12 & 255(0xff, float:3.57E-43)
                int r12 = r12 << 16
                int r13 = r8 + 1
                byte r13 = r16[r13]
                r13 = r13 & 255(0xff, float:3.57E-43)
                int r13 = r13 << 8
                r12 = r12 | r13
                int r13 = r8 + 2
                byte r13 = r16[r13]
                r13 = r13 & 255(0xff, float:3.57E-43)
                r11 = r12 | r13
                int r12 = r11 >> 18
                r12 = r12 & 63
                byte r12 = r2[r12]
                r6[r5] = r12
                int r12 = r5 + 1
                int r13 = r11 >> 12
                r13 = r13 & 63
                byte r13 = r2[r13]
                r6[r12] = r13
                int r12 = r5 + 2
                int r13 = r11 >> 6
                r13 = r13 & 63
                byte r13 = r2[r13]
                r6[r12] = r13
                int r12 = r5 + 3
                r13 = r11 & 63
                byte r13 = r2[r13]
                r6[r12] = r13
                int r7 = r8 + 3
                int r4 = r5 + 4
                int r3 = r3 + -1
                if (r3 != 0) goto L_0x0253
                boolean r12 = r15.do_cr
                if (r12 == 0) goto L_0x015d
                int r5 = r4 + 1
                r12 = 13
                r6[r4] = r12
                r4 = r5
            L_0x015d:
                int r5 = r4 + 1
                r12 = 10
                r6[r4] = r12
                r3 = 19
                r8 = r7
                goto L_0x0052
            L_0x0168:
                int r7 = r8 + 1
                byte r12 = r16[r8]
                goto L_0x0071
            L_0x016e:
                int r12 = r15.tailLen
                int r12 = r8 - r12
                r13 = 2
                int r13 = r18 - r13
                if (r12 != r13) goto L_0x01e9
                r9 = 0
                int r12 = r15.tailLen
                r13 = 1
                if (r12 <= r13) goto L_0x01de
                byte[] r12 = r15.tail
                int r10 = r9 + 1
                byte r12 = r12[r9]
                r9 = r10
                r7 = r8
            L_0x0185:
                r12 = r12 & 255(0xff, float:3.57E-43)
                int r12 = r12 << 10
                int r13 = r15.tailLen
                if (r13 <= 0) goto L_0x01e3
                byte[] r13 = r15.tail
                int r10 = r9 + 1
                byte r13 = r13[r9]
                r9 = r10
            L_0x0194:
                r13 = r13 & 255(0xff, float:3.57E-43)
                int r13 = r13 << 2
                r11 = r12 | r13
                int r12 = r15.tailLen
                int r12 = r12 - r9
                r15.tailLen = r12
                int r4 = r5 + 1
                int r12 = r11 >> 12
                r12 = r12 & 63
                byte r12 = r2[r12]
                r6[r5] = r12
                int r5 = r4 + 1
                int r12 = r11 >> 6
                r12 = r12 & 63
                byte r12 = r2[r12]
                r6[r4] = r12
                int r4 = r5 + 1
                r12 = r11 & 63
                byte r12 = r2[r12]
                r6[r5] = r12
                boolean r12 = r15.do_padding
                if (r12 == 0) goto L_0x01c6
                int r5 = r4 + 1
                r12 = 61
                r6[r4] = r12
                r4 = r5
            L_0x01c6:
                boolean r12 = r15.do_newline
                if (r12 == 0) goto L_0x00b3
                boolean r12 = r15.do_cr
                if (r12 == 0) goto L_0x01d5
                int r5 = r4 + 1
                r12 = 13
                r6[r4] = r12
                r4 = r5
            L_0x01d5:
                int r5 = r4 + 1
                r12 = 10
                r6[r4] = r12
                r4 = r5
                goto L_0x00b3
            L_0x01de:
                int r7 = r8 + 1
                byte r12 = r16[r8]
                goto L_0x0185
            L_0x01e3:
                int r8 = r7 + 1
                byte r13 = r16[r7]
                r7 = r8
                goto L_0x0194
            L_0x01e9:
                boolean r12 = r15.do_newline
                if (r12 == 0) goto L_0x0203
                if (r5 <= 0) goto L_0x0203
                r12 = 19
                if (r3 == r12) goto L_0x0203
                boolean r12 = r15.do_cr
                if (r12 == 0) goto L_0x0251
                int r4 = r5 + 1
                r12 = 13
                r6[r5] = r12
            L_0x01fd:
                int r5 = r4 + 1
                r12 = 10
                r6[r4] = r12
            L_0x0203:
                r7 = r8
                r4 = r5
                goto L_0x00b3
            L_0x0207:
                boolean r12 = org.anddev.andengine.util.Base64.Encoder.$assertionsDisabled
                if (r12 != 0) goto L_0x0229
                r0 = r7
                r1 = r18
                if (r0 == r1) goto L_0x0229
                java.lang.AssertionError r12 = new java.lang.AssertionError
                r12.<init>()
                throw r12
            L_0x0216:
                r12 = 1
                int r12 = r18 - r12
                if (r8 != r12) goto L_0x022f
                byte[] r12 = r15.tail
                int r13 = r15.tailLen
                int r14 = r13 + 1
                r15.tailLen = r14
                byte r14 = r16[r8]
                r12[r13] = r14
                r7 = r8
                r4 = r5
            L_0x0229:
                r15.op = r4
                r15.count = r3
                r12 = 1
                return r12
            L_0x022f:
                r12 = 2
                int r12 = r18 - r12
                if (r8 != r12) goto L_0x024e
                byte[] r12 = r15.tail
                int r13 = r15.tailLen
                int r14 = r13 + 1
                r15.tailLen = r14
                byte r14 = r16[r8]
                r12[r13] = r14
                byte[] r12 = r15.tail
                int r13 = r15.tailLen
                int r14 = r13 + 1
                r15.tailLen = r14
                int r14 = r8 + 1
                byte r14 = r16[r14]
                r12[r13] = r14
            L_0x024e:
                r7 = r8
                r4 = r5
                goto L_0x0229
            L_0x0251:
                r4 = r5
                goto L_0x01fd
            L_0x0253:
                r8 = r7
                r5 = r4
                goto L_0x0052
            */
            throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.util.Base64.Encoder.process(byte[], int, int, boolean):boolean");
        }
    }

    private Base64() {
    }
}
