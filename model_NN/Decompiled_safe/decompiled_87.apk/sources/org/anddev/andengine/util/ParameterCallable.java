package org.anddev.andengine.util;

public interface ParameterCallable<T> {
    void call(Object obj);
}
