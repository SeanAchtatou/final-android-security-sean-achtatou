package org.anddev.andengine.util.modifier.ease;

public class EaseCubicInOut implements IEaseFunction {
    private static EaseCubicInOut INSTANCE;

    private EaseCubicInOut() {
    }

    public static EaseCubicInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseCubicInOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = pSecondsElapsed / (pDuration * 0.5f);
        if (pSecondsElapsed2 < 1.0f) {
            return (pMaxValue * 0.5f * pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2) + pMinValue;
        }
        float pSecondsElapsed3 = pSecondsElapsed2 - 2.0f;
        return (pMaxValue * 0.5f * ((pSecondsElapsed3 * pSecondsElapsed3 * pSecondsElapsed3) + 2.0f)) + pMinValue;
    }
}
