package org.anddev.andengine.opengl.texture.source.decorator;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import org.anddev.andengine.opengl.texture.source.ITextureSource;

public abstract class BaseTextureSourceDecorator implements ITextureSource {
    protected Paint mPaint;
    protected final ITextureSource mTextureSource;
    protected TextureSourceDecoratorOptions mTextureSourceDecoratorOptions;

    public abstract BaseTextureSourceDecorator clone();

    /* access modifiers changed from: protected */
    public abstract void onDecorateBitmap(Canvas canvas);

    public BaseTextureSourceDecorator(ITextureSource pTextureSource) {
        this(pTextureSource, new TextureSourceDecoratorOptions());
    }

    public BaseTextureSourceDecorator(ITextureSource pTextureSource, TextureSourceDecoratorOptions pTextureSourceDecoratorOptions) {
        TextureSourceDecoratorOptions textureSourceDecoratorOptions;
        this.mPaint = new Paint();
        this.mTextureSource = pTextureSource;
        if (pTextureSourceDecoratorOptions == null) {
            textureSourceDecoratorOptions = new TextureSourceDecoratorOptions();
        } else {
            textureSourceDecoratorOptions = pTextureSourceDecoratorOptions;
        }
        this.mTextureSourceDecoratorOptions = textureSourceDecoratorOptions;
        this.mPaint.setAntiAlias(this.mTextureSourceDecoratorOptions.getAntiAliasing());
    }

    public Paint getPaint() {
        return this.mPaint;
    }

    public void setPaint(Paint pPaint) {
        this.mPaint = pPaint;
    }

    public TextureSourceDecoratorOptions getTextureSourceDecoratorOptions() {
        return this.mTextureSourceDecoratorOptions;
    }

    public void setTextureSourceDecoratorOptions(TextureSourceDecoratorOptions pTextureSourceDecoratorOptions) {
        this.mTextureSourceDecoratorOptions = pTextureSourceDecoratorOptions;
    }

    public int getWidth() {
        return this.mTextureSource.getWidth();
    }

    public int getHeight() {
        return this.mTextureSource.getHeight();
    }

    public Bitmap onLoadBitmap() {
        Bitmap bitmap = ensureLoadedBitmapIsMutable(this.mTextureSource.onLoadBitmap());
        onDecorateBitmap(new Canvas(bitmap));
        return bitmap;
    }

    private Bitmap ensureLoadedBitmapIsMutable(Bitmap pBitmap) {
        if (pBitmap.isMutable()) {
            return pBitmap;
        }
        Bitmap mutableBitmap = pBitmap.copy(pBitmap.getConfig(), true);
        pBitmap.recycle();
        return mutableBitmap;
    }

    public static class TextureSourceDecoratorOptions {
        public static final TextureSourceDecoratorOptions DEFAULT = new TextureSourceDecoratorOptions();
        private boolean mAntiAliasing;
        private float mInsetBottom = 0.0f;
        private float mInsetLeft = 0.0f;
        private float mInsetRight = 0.0f;
        private float mInsetTop = 0.0f;

        /* access modifiers changed from: protected */
        public TextureSourceDecoratorOptions clone() {
            TextureSourceDecoratorOptions textureSourceDecoratorOptions = new TextureSourceDecoratorOptions();
            textureSourceDecoratorOptions.setInsets(this.mInsetLeft, this.mInsetTop, this.mInsetRight, this.mInsetBottom);
            textureSourceDecoratorOptions.setAntiAliasing(this.mAntiAliasing);
            return textureSourceDecoratorOptions;
        }

        public boolean getAntiAliasing() {
            return this.mAntiAliasing;
        }

        public float getInsetLeft() {
            return this.mInsetLeft;
        }

        public float getInsetRight() {
            return this.mInsetRight;
        }

        public float getInsetTop() {
            return this.mInsetTop;
        }

        public float getInsetBottom() {
            return this.mInsetBottom;
        }

        public TextureSourceDecoratorOptions setAntiAliasing(boolean pAntiAliasing) {
            this.mAntiAliasing = pAntiAliasing;
            return this;
        }

        public TextureSourceDecoratorOptions setInsetLeft(float pInsetLeft) {
            this.mInsetLeft = pInsetLeft;
            return this;
        }

        public TextureSourceDecoratorOptions setInsetRight(float pInsetRight) {
            this.mInsetRight = pInsetRight;
            return this;
        }

        public TextureSourceDecoratorOptions setInsetTop(float pInsetTop) {
            this.mInsetTop = pInsetTop;
            return this;
        }

        public TextureSourceDecoratorOptions setInsetBottom(float pInsetBottom) {
            this.mInsetBottom = pInsetBottom;
            return this;
        }

        public TextureSourceDecoratorOptions setInsets(float pInsets) {
            return setInsets(pInsets, pInsets, pInsets, pInsets);
        }

        public TextureSourceDecoratorOptions setInsets(float pInsetLeft, float pInsetTop, float pInsetRight, float pInsetBottom) {
            this.mInsetLeft = pInsetLeft;
            this.mInsetTop = pInsetTop;
            this.mInsetRight = pInsetRight;
            this.mInsetBottom = pInsetBottom;
            return this;
        }
    }
}
