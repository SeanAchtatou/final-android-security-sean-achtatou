package org.anddev.andengine.opengl.util;

import android.graphics.Bitmap;
import android.os.Build;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.engine.options.RenderOptions;
import org.anddev.andengine.util.Debug;

public class GLHelper {
    public static final int BYTES_PER_FLOAT = 4;
    public static final int BYTES_PER_PIXEL_RGBA = 4;
    public static boolean EXTENSIONS_DRAWTEXTURE = false;
    public static boolean EXTENSIONS_VERTEXBUFFEROBJECTS = false;
    private static final int[] HARDWAREBUFFERID_CONTAINER = new int[1];
    private static final int[] HARDWARETEXTUREID_CONTAINER = new int[1];
    private static final boolean IS_LITTLE_ENDIAN;
    private static float sAlpha = -1.0f;
    private static float sBlue = -1.0f;
    private static int sCurrentDestinationBlendMode = -1;
    private static int sCurrentHardwareBufferID = -1;
    private static int sCurrentHardwareTextureID = -1;
    private static int sCurrentMatrix = -1;
    private static int sCurrentSourceBlendMode = -1;
    private static FastFloatBuffer sCurrentTextureFloatBuffer = null;
    private static FastFloatBuffer sCurrentVertexFloatBuffer = null;
    private static boolean sEnableBlend = false;
    private static boolean sEnableCulling = false;
    private static boolean sEnableDepthTest = true;
    private static boolean sEnableDither = true;
    private static boolean sEnableLightning = true;
    private static boolean sEnableMultisample = true;
    private static boolean sEnableScissorTest = false;
    private static boolean sEnableTexCoordArray = false;
    private static boolean sEnableTextures = false;
    private static boolean sEnableVertexArray = false;
    private static float sGreen = -1.0f;
    private static float sLineWidth = 1.0f;
    private static float sRed = -1.0f;

    static {
        boolean z;
        if (ByteOrder.nativeOrder() == ByteOrder.LITTLE_ENDIAN) {
            z = true;
        } else {
            z = false;
        }
        IS_LITTLE_ENDIAN = z;
    }

    public static void reset(GL10 pGL) {
        sCurrentHardwareBufferID = -1;
        sCurrentHardwareTextureID = -1;
        sCurrentMatrix = -1;
        sCurrentSourceBlendMode = -1;
        sCurrentDestinationBlendMode = -1;
        sCurrentTextureFloatBuffer = null;
        sCurrentVertexFloatBuffer = null;
        enableDither(pGL);
        enableLightning(pGL);
        enableDepthTest(pGL);
        enableMultisample(pGL);
        disableBlend(pGL);
        disableCulling(pGL);
        disableTextures(pGL);
        disableTexCoordArray(pGL);
        disableVertexArray(pGL);
        sLineWidth = 1.0f;
        sRed = -1.0f;
        sGreen = -1.0f;
        sBlue = -1.0f;
        sAlpha = -1.0f;
        EXTENSIONS_VERTEXBUFFEROBJECTS = false;
        EXTENSIONS_DRAWTEXTURE = false;
    }

    public static void enableExtensions(GL10 pGL, RenderOptions pRenderOptions) {
        String version = pGL.glGetString(7938);
        String renderer = pGL.glGetString(7937);
        String extensions = pGL.glGetString(7939);
        Debug.d("RENDERER: " + renderer);
        Debug.d("VERSION: " + version);
        Debug.d("EXTENSIONS: " + extensions);
        boolean isOpenGL10 = version.contains("1.0");
        boolean isSoftwareRenderer = renderer.contains("PixelFlinger");
        boolean isVBOCapable = extensions.contains("_vertex_buffer_object");
        boolean isDrawTextureCapable = extensions.contains("draw_texture");
        EXTENSIONS_VERTEXBUFFEROBJECTS = !pRenderOptions.isDisableExtensionVertexBufferObjects() && !isSoftwareRenderer && (isVBOCapable || !isOpenGL10);
        EXTENSIONS_DRAWTEXTURE = isDrawTextureCapable;
        hackBrokenDevices();
        Debug.d("EXTENSIONS_VERXTEXBUFFEROBJECTS = " + EXTENSIONS_VERTEXBUFFEROBJECTS);
        Debug.d("EXTENSIONS_DRAWTEXTURE = " + EXTENSIONS_DRAWTEXTURE);
    }

    private static void hackBrokenDevices() {
        if (Build.PRODUCT.contains("morrison")) {
            EXTENSIONS_VERTEXBUFFEROBJECTS = false;
        }
    }

    public static void setColor(GL10 pGL, float pRed, float pGreen, float pBlue, float pAlpha) {
        if (pAlpha != sAlpha || pRed != sRed || pGreen != sGreen || pBlue != sBlue) {
            sAlpha = pAlpha;
            sRed = pRed;
            sGreen = pGreen;
            sBlue = pBlue;
            pGL.glColor4f(pRed, pGreen, pBlue, pAlpha);
        }
    }

    public static void enableVertexArray(GL10 pGL) {
        if (!sEnableVertexArray) {
            sEnableVertexArray = true;
            pGL.glEnableClientState(32884);
        }
    }

    public static void disableVertexArray(GL10 pGL) {
        if (sEnableVertexArray) {
            sEnableVertexArray = false;
            pGL.glDisableClientState(32884);
        }
    }

    public static void enableTexCoordArray(GL10 pGL) {
        if (!sEnableTexCoordArray) {
            sEnableTexCoordArray = true;
            pGL.glEnableClientState(32888);
        }
    }

    public static void disableTexCoordArray(GL10 pGL) {
        if (sEnableTexCoordArray) {
            sEnableTexCoordArray = false;
            pGL.glDisableClientState(32888);
        }
    }

    public static void enableScissorTest(GL10 pGL) {
        if (!sEnableScissorTest) {
            sEnableScissorTest = true;
            pGL.glEnable(3089);
        }
    }

    public static void disableScissorTest(GL10 pGL) {
        if (sEnableScissorTest) {
            sEnableScissorTest = false;
            pGL.glDisable(3089);
        }
    }

    public static void enableBlend(GL10 pGL) {
        if (!sEnableBlend) {
            sEnableBlend = true;
            pGL.glEnable(3042);
        }
    }

    public static void disableBlend(GL10 pGL) {
        if (sEnableBlend) {
            sEnableBlend = false;
            pGL.glDisable(3042);
        }
    }

    public static void enableCulling(GL10 pGL) {
        if (!sEnableCulling) {
            sEnableCulling = true;
            pGL.glEnable(2884);
        }
    }

    public static void disableCulling(GL10 pGL) {
        if (sEnableCulling) {
            sEnableCulling = false;
            pGL.glDisable(2884);
        }
    }

    public static void enableTextures(GL10 pGL) {
        if (!sEnableTextures) {
            sEnableTextures = true;
            pGL.glEnable(3553);
        }
    }

    public static void disableTextures(GL10 pGL) {
        if (sEnableTextures) {
            sEnableTextures = false;
            pGL.glDisable(3553);
        }
    }

    public static void enableLightning(GL10 pGL) {
        if (!sEnableLightning) {
            sEnableLightning = true;
            pGL.glEnable(2896);
        }
    }

    public static void disableLightning(GL10 pGL) {
        if (sEnableLightning) {
            sEnableLightning = false;
            pGL.glDisable(2896);
        }
    }

    public static void enableDither(GL10 pGL) {
        if (!sEnableDither) {
            sEnableDither = true;
            pGL.glEnable(3024);
        }
    }

    public static void disableDither(GL10 pGL) {
        if (sEnableDither) {
            sEnableDither = false;
            pGL.glDisable(3024);
        }
    }

    public static void enableDepthTest(GL10 pGL) {
        if (!sEnableDepthTest) {
            sEnableDepthTest = true;
            pGL.glEnable(2929);
        }
    }

    public static void disableDepthTest(GL10 pGL) {
        if (sEnableDepthTest) {
            sEnableDepthTest = false;
            pGL.glDisable(2929);
        }
    }

    public static void enableMultisample(GL10 pGL) {
        if (!sEnableMultisample) {
            sEnableMultisample = true;
            pGL.glEnable(32925);
        }
    }

    public static void disableMultisample(GL10 pGL) {
        if (sEnableMultisample) {
            sEnableMultisample = false;
            pGL.glDisable(32925);
        }
    }

    public static void bindBuffer(GL11 pGL11, int pHardwareBufferID) {
        if (sCurrentHardwareBufferID != pHardwareBufferID) {
            sCurrentHardwareBufferID = pHardwareBufferID;
            pGL11.glBindBuffer(34962, pHardwareBufferID);
        }
    }

    public static void deleteBuffer(GL11 pGL11, int pHardwareBufferID) {
        HARDWAREBUFFERID_CONTAINER[0] = pHardwareBufferID;
        pGL11.glDeleteBuffers(1, HARDWAREBUFFERID_CONTAINER, 0);
    }

    public static void bindTexture(GL10 pGL, int pHardwareTextureID) {
        if (sCurrentHardwareTextureID != pHardwareTextureID) {
            sCurrentHardwareTextureID = pHardwareTextureID;
            pGL.glBindTexture(3553, pHardwareTextureID);
        }
    }

    public static void deleteTexture(GL10 pGL, int pHardwareTextureID) {
        HARDWARETEXTUREID_CONTAINER[0] = pHardwareTextureID;
        pGL.glDeleteTextures(1, HARDWARETEXTUREID_CONTAINER, 0);
    }

    public static void texCoordPointer(GL10 pGL, FastFloatBuffer pTextureFloatBuffer) {
        if (sCurrentTextureFloatBuffer != pTextureFloatBuffer) {
            sCurrentTextureFloatBuffer = pTextureFloatBuffer;
            pGL.glTexCoordPointer(2, 5126, 0, pTextureFloatBuffer.mByteBuffer);
        }
    }

    public static void texCoordZeroPointer(GL11 pGL11) {
        pGL11.glTexCoordPointer(2, 5126, 0, 0);
    }

    public static void vertexPointer(GL10 pGL, FastFloatBuffer pVertexFloatBuffer) {
        if (sCurrentVertexFloatBuffer != pVertexFloatBuffer) {
            sCurrentVertexFloatBuffer = pVertexFloatBuffer;
            pGL.glVertexPointer(2, 5126, 0, pVertexFloatBuffer.mByteBuffer);
        }
    }

    public static void vertexZeroPointer(GL11 pGL11) {
        pGL11.glVertexPointer(2, 5126, 0, 0);
    }

    public static void blendFunction(GL10 pGL, int pSourceBlendMode, int pDestinationBlendMode) {
        if (sCurrentSourceBlendMode != pSourceBlendMode || sCurrentDestinationBlendMode != pDestinationBlendMode) {
            sCurrentSourceBlendMode = pSourceBlendMode;
            sCurrentDestinationBlendMode = pDestinationBlendMode;
            pGL.glBlendFunc(pSourceBlendMode, pDestinationBlendMode);
        }
    }

    public static void lineWidth(GL10 pGL, float pLineWidth) {
        if (sLineWidth != pLineWidth) {
            sLineWidth = pLineWidth;
            pGL.glLineWidth(pLineWidth);
        }
    }

    public static void switchToModelViewMatrix(GL10 pGL) {
        if (sCurrentMatrix != 5888) {
            sCurrentMatrix = 5888;
            pGL.glMatrixMode(5888);
        }
    }

    public static void switchToProjectionMatrix(GL10 pGL) {
        if (sCurrentMatrix != 5889) {
            sCurrentMatrix = 5889;
            pGL.glMatrixMode(5889);
        }
    }

    public static void setProjectionIdentityMatrix(GL10 pGL) {
        switchToProjectionMatrix(pGL);
        pGL.glLoadIdentity();
    }

    public static void setModelViewIdentityMatrix(GL10 pGL) {
        switchToModelViewMatrix(pGL);
        pGL.glLoadIdentity();
    }

    public static void setShadeModelFlat(GL10 pGL) {
        pGL.glShadeModel(7424);
    }

    public static void setPerspectiveCorrectionHintFastest(GL10 pGL) {
        pGL.glHint(3152, 4353);
    }

    public static void bufferData(GL11 pGL11, ByteBuffer pByteBuffer, int pUsage) {
        pGL11.glBufferData(34962, pByteBuffer.capacity(), pByteBuffer, pUsage);
    }

    /* JADX INFO: Multiple debug info for r11v1 int[]: [D('target' int), D('pixels' int[])] */
    public static void glTexSubImage2D(GL10 pGL, int target, int level, int xoffset, int yoffset, Bitmap bitmap, int format, int type) {
        GL10 gl10 = pGL;
        int i = xoffset;
        int i2 = yoffset;
        gl10.glTexSubImage2D(3553, 0, i, i2, bitmap.getWidth(), bitmap.getHeight(), 6408, 5121, convertARGBtoRGBABuffer(getPixels(bitmap)));
    }

    private static Buffer convertARGBtoRGBABuffer(int[] pPixels) {
        if (IS_LITTLE_ENDIAN) {
            for (int i = pPixels.length - 1; i >= 0; i--) {
                int pixel = pPixels[i];
                pPixels[i] = ((pixel >> 24) << 24) | ((pixel & 255) << 16) | (((pixel >> 8) & 255) << 8) | ((pixel >> 16) & 255);
            }
        } else {
            for (int i2 = pPixels.length - 1; i2 >= 0; i2--) {
                int pixel2 = pPixels[i2];
                pPixels[i2] = (((pixel2 >> 16) & 255) << 24) | (((pixel2 >> 8) & 255) << 16) | ((pixel2 & 255) << 8) | (pixel2 >> 24);
            }
        }
        return IntBuffer.wrap(pPixels);
    }

    public static int[] getPixels(Bitmap pBitmap) {
        int w = pBitmap.getWidth();
        int h = pBitmap.getHeight();
        int[] pixels = new int[(w * h)];
        pBitmap.getPixels(pixels, 0, w, 0, 0, w, h);
        return pixels;
    }
}
