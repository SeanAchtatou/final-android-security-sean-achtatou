package org.anddev.andengine.opengl.font;

import android.content.Context;
import android.graphics.Typeface;
import org.anddev.andengine.opengl.texture.Texture;

public class FontFactory {
    private static String sAssetBasePath = "";

    public static void setAssetBasePath(String pAssetBasePath) {
        if (pAssetBasePath.endsWith("/") || pAssetBasePath.length() == 0) {
            sAssetBasePath = pAssetBasePath;
            return;
        }
        throw new IllegalStateException("pAssetBasePath must end with '/' or be lenght zero.");
    }

    public static Font create(Texture pTexture, Typeface pTypeface, float pSize, boolean pAntiAlias, int pColor) {
        return new Font(pTexture, pTypeface, pSize, pAntiAlias, pColor);
    }

    public static StrokeFont createStroke(Texture pTexture, Typeface pTypeface, float pSize, boolean pAntiAlias, int pColor, int pStrokeWidth, int pStrokeColor) {
        return new StrokeFont(pTexture, pTypeface, pSize, pAntiAlias, pColor, (float) pStrokeWidth, pStrokeColor);
    }

    public static StrokeFont createStroke(Texture pTexture, Typeface pTypeface, float pSize, boolean pAntiAlias, int pColor, int pStrokeWidth, int pStrokeColor, boolean pStrokeOnly) {
        return new StrokeFont(pTexture, pTypeface, pSize, pAntiAlias, pColor, (float) pStrokeWidth, pStrokeColor, pStrokeOnly);
    }

    public static Font createFromAsset(Texture pTexture, Context pContext, String pAssetPath, float pSize, boolean pAntiAlias, int pColor) {
        return new Font(pTexture, Typeface.createFromAsset(pContext.getAssets(), String.valueOf(sAssetBasePath) + pAssetPath), pSize, pAntiAlias, pColor);
    }

    public static StrokeFont createStrokeFromAsset(Texture pTexture, Context pContext, String pAssetPath, float pSize, boolean pAntiAlias, int pColor, int pStrokeWidth, int pStrokeColor) {
        return new StrokeFont(pTexture, Typeface.createFromAsset(pContext.getAssets(), String.valueOf(sAssetBasePath) + pAssetPath), pSize, pAntiAlias, pColor, (float) pStrokeWidth, pStrokeColor);
    }

    public static StrokeFont createStrokeFromAsset(Texture pTexture, Context pContext, String pAssetPath, float pSize, boolean pAntiAlias, int pColor, int pStrokeWidth, int pStrokeColor, boolean pStrokeOnly) {
        return new StrokeFont(pTexture, Typeface.createFromAsset(pContext.getAssets(), String.valueOf(sAssetBasePath) + pAssetPath), pSize, pAntiAlias, pColor, (float) pStrokeWidth, pStrokeColor, pStrokeOnly);
    }
}
