package org.anddev.andengine.opengl.texture.source.decorator;

import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import org.anddev.andengine.opengl.texture.source.decorator.BaseTextureSourceDecorator;
import org.anddev.andengine.opengl.texture.source.decorator.shape.ITextureSourceDecoratorShape;

public class LinearGradientFillTextureSourceDecorator extends BaseShapeTextureSourceDecorator {
    protected final int mFromColor;
    protected final LinearGradientDirection mLinearGradientDirection;
    protected final int mToColor;

    public LinearGradientFillTextureSourceDecorator(ITextureSource pTextureSource, ITextureSourceDecoratorShape pTextureSourceDecoratorShape, int pFromColor, int pToColor, LinearGradientDirection pLinearGradientDirection) {
        this(pTextureSource, pTextureSourceDecoratorShape, pFromColor, pToColor, pLinearGradientDirection, null);
    }

    public LinearGradientFillTextureSourceDecorator(ITextureSource pTextureSource, ITextureSourceDecoratorShape pTextureSourceDecoratorShape, int pFromColor, int pToColor, LinearGradientDirection pLinearGradientDirection, BaseTextureSourceDecorator.TextureSourceDecoratorOptions pTextureSourceDecoratorOptions) {
        super(pTextureSource, pTextureSourceDecoratorShape, pTextureSourceDecoratorOptions);
        this.mFromColor = pFromColor;
        this.mToColor = pToColor;
        this.mLinearGradientDirection = pLinearGradientDirection;
        this.mPaint.setStyle(Paint.Style.FILL);
        int width = pTextureSource.getWidth();
        int height = pTextureSource.getHeight();
        this.mPaint.setShader(new LinearGradient((float) (pLinearGradientDirection.getFromX() * width), (float) (pLinearGradientDirection.getFromY() * height), (float) (pLinearGradientDirection.getToX() * width), (float) (pLinearGradientDirection.getToY() * height), pFromColor, pToColor, Shader.TileMode.CLAMP));
    }

    public LinearGradientFillTextureSourceDecorator clone() {
        return new LinearGradientFillTextureSourceDecorator(this.mTextureSource, this.mTextureSourceDecoratorShape, this.mFromColor, this.mToColor, this.mLinearGradientDirection, this.mTextureSourceDecoratorOptions);
    }

    public enum LinearGradientDirection {
        LEFT_TO_RIGHT(0, 0, 1, 0),
        RIGHT_TO_LEFT(1, 0, 0, 0),
        BOTTOM_TO_TOP(0, 0, 0, 1),
        TOP_TO_BOTTOM(0, 1, 0, 0),
        TOPLEFT_TO_BOTTOMRIGHT(0, 0, 1, 1),
        BOTTOMRIGHT_TO_TOPLEFT(1, 1, 0, 0),
        TOPRIGHT_TO_BOTTOMLEFT(1, 0, 0, 1),
        BOTTOMLEFT_TO_TOPRIGHT(0, 1, 1, 0);
        
        private final int mFromX;
        private final int mFromY;
        private final int mToX;
        private final int mToY;

        private LinearGradientDirection(int pFromX, int pFromY, int pToX, int pToY) {
            this.mFromX = pFromX;
            this.mFromY = pFromY;
            this.mToX = pToX;
            this.mToY = pToY;
        }

        /* access modifiers changed from: package-private */
        public final int getFromX() {
            return this.mFromX;
        }

        /* access modifiers changed from: package-private */
        public final int getFromY() {
            return this.mFromY;
        }

        /* access modifiers changed from: package-private */
        public final int getToX() {
            return this.mToX;
        }

        /* access modifiers changed from: package-private */
        public final int getToY() {
            return this.mToY;
        }
    }
}
