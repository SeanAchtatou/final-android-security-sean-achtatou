package org.anddev.andengine.opengl.texture;

public class TextureOptions {
    public static final TextureOptions BILINEAR = new TextureOptions(9729, 9729, 33071, 33071, 8448, false);
    public static final TextureOptions BILINEAR_PREMULTIPLYALPHA = new TextureOptions(9729, 9729, 33071, 33071, 8448, true);
    public static final TextureOptions DEFAULT = NEAREST_PREMULTIPLYALPHA;
    public static final TextureOptions NEAREST = new TextureOptions(9728, 9728, 33071, 33071, 8448, false);
    public static final TextureOptions NEAREST_PREMULTIPLYALPHA = new TextureOptions(9728, 9728, 33071, 33071, 8448, true);
    public static final TextureOptions REPEATING = new TextureOptions(9728, 9728, 10497, 10497, 8448, false);
    public static final TextureOptions REPEATING_BILINEAR = new TextureOptions(9729, 9729, 10497, 10497, 8448, false);
    public static final TextureOptions REPEATING_BILINEAR_PREMULTIPLYALPHA = new TextureOptions(9729, 9729, 10497, 10497, 8448, true);
    public static final TextureOptions REPEATING_PREMULTIPLYALPHA = new TextureOptions(9728, 9728, 10497, 10497, 8448, true);
    public final int mMagFilter;
    public final int mMinFilter;
    public final boolean mPreMultipyAlpha;
    public final int mTextureEnvironment;
    public final float mWrapS;
    public final float mWrapT;

    public TextureOptions(int pMinFilter, int pMagFilter, int pWrapT, int pWrapS, int pTextureEnvironment, boolean pPreMultiplyAlpha) {
        this.mMinFilter = pMinFilter;
        this.mMagFilter = pMagFilter;
        this.mWrapT = (float) pWrapT;
        this.mWrapS = (float) pWrapS;
        this.mTextureEnvironment = pTextureEnvironment;
        this.mPreMultipyAlpha = pPreMultiplyAlpha;
    }
}
