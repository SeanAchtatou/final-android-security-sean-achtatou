package org.anddev.andengine.opengl.texture.source;

import android.graphics.Bitmap;

public class EmptyBitmapTextureSource implements ITextureSource {
    private final int mHeight;
    private final int mWidth;

    public EmptyBitmapTextureSource(int pWidth, int pHeight) {
        this.mWidth = pWidth;
        this.mHeight = pHeight;
    }

    public EmptyBitmapTextureSource clone() {
        return new EmptyBitmapTextureSource(this.mWidth, this.mHeight);
    }

    public int getHeight() {
        return this.mHeight;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public Bitmap onLoadBitmap() {
        return Bitmap.createBitmap(this.mWidth, this.mHeight, Bitmap.Config.ARGB_8888);
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + "(" + this.mWidth + " x " + this.mHeight + ")";
    }
}
