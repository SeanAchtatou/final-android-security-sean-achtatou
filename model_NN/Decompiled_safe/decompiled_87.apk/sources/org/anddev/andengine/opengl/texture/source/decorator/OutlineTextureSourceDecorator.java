package org.anddev.andengine.opengl.texture.source.decorator;

import android.graphics.Paint;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import org.anddev.andengine.opengl.texture.source.decorator.BaseTextureSourceDecorator;
import org.anddev.andengine.opengl.texture.source.decorator.shape.ITextureSourceDecoratorShape;

public class OutlineTextureSourceDecorator extends BaseShapeTextureSourceDecorator {
    protected final int mOutlineColor;

    public OutlineTextureSourceDecorator(ITextureSource pTextureSource, ITextureSourceDecoratorShape pTextureSourceDecoratorShape, int pOutlineColor) {
        this(pTextureSource, pTextureSourceDecoratorShape, pOutlineColor, null);
    }

    public OutlineTextureSourceDecorator(ITextureSource pTextureSource, ITextureSourceDecoratorShape pTextureSourceDecoratorShape, int pOutlineColor, BaseTextureSourceDecorator.TextureSourceDecoratorOptions pTextureSourceDecoratorOptions) {
        super(pTextureSource, pTextureSourceDecoratorShape, pTextureSourceDecoratorOptions);
        this.mOutlineColor = pOutlineColor;
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mPaint.setColor(pOutlineColor);
    }

    public OutlineTextureSourceDecorator clone() {
        return new OutlineTextureSourceDecorator(this.mTextureSource, this.mTextureSourceDecoratorShape, this.mOutlineColor, this.mTextureSourceDecoratorOptions);
    }
}
