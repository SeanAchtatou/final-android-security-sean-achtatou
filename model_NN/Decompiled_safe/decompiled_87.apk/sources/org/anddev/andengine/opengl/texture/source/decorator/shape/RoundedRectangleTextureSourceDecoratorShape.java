package org.anddev.andengine.opengl.texture.source.decorator.shape;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import org.anddev.andengine.opengl.texture.source.decorator.BaseTextureSourceDecorator;

public class RoundedRectangleTextureSourceDecoratorShape implements ITextureSourceDecoratorShape {
    private static final float CORNER_RADIUS_DEFAULT = 1.0f;
    private static RoundedRectangleTextureSourceDecoratorShape sDefaultInstance;
    private final float mCornerRadiusX;
    private final float mCornerRadiusY;
    private final RectF mRectF;

    public RoundedRectangleTextureSourceDecoratorShape() {
        this(1.0f, 1.0f);
    }

    public RoundedRectangleTextureSourceDecoratorShape(float pCornerRadiusX, float pCornerRadiusY) {
        this.mRectF = new RectF();
        this.mCornerRadiusX = pCornerRadiusX;
        this.mCornerRadiusY = pCornerRadiusY;
    }

    public static RoundedRectangleTextureSourceDecoratorShape getDefaultInstance() {
        if (sDefaultInstance == null) {
            sDefaultInstance = new RoundedRectangleTextureSourceDecoratorShape();
        }
        return sDefaultInstance;
    }

    public void onDecorateBitmap(Canvas pCanvas, Paint pPaint, BaseTextureSourceDecorator.TextureSourceDecoratorOptions pDecoratorOptions) {
        this.mRectF.set(pDecoratorOptions.getInsetLeft(), pDecoratorOptions.getInsetTop(), ((float) (pCanvas.getWidth() - 1)) - pDecoratorOptions.getInsetRight(), ((float) (pCanvas.getHeight() - 1)) - pDecoratorOptions.getInsetBottom());
        pCanvas.drawRoundRect(this.mRectF, this.mCornerRadiusX, this.mCornerRadiusY, pPaint);
    }
}
