package org.anddev.andengine.opengl.texture;

import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import org.anddev.andengine.util.MathUtils;

public class TextureFactory {
    public static Texture createForTextureSourceSize(TextureRegion pTextureRegion) {
        return createForTextureRegionSize(pTextureRegion, TextureOptions.DEFAULT);
    }

    public static Texture createForTextureRegionSize(TextureRegion pTextureRegion, TextureOptions pTextureOptions) {
        return new Texture(MathUtils.nextPowerOfTwo(pTextureRegion.getWidth()), MathUtils.nextPowerOfTwo(pTextureRegion.getHeight()), pTextureOptions);
    }

    public static Texture createForTextureSourceSize(ITextureSource pTextureSource) {
        return createForTextureSourceSize(pTextureSource, TextureOptions.DEFAULT);
    }

    public static Texture createForTextureSourceSize(ITextureSource pTextureSource, TextureOptions pTextureOptions) {
        return new Texture(MathUtils.nextPowerOfTwo(pTextureSource.getWidth()), MathUtils.nextPowerOfTwo(pTextureSource.getHeight()), pTextureOptions);
    }
}
