package org.anddev.andengine.opengl.texture.region;

import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.region.buffer.BaseTextureRegionBuffer;
import org.anddev.andengine.opengl.texture.region.buffer.TextureRegionBuffer;

public class TextureRegion extends BaseTextureRegion {
    public TextureRegion(Texture pTexture, int pTexturePositionX, int pTexturePositionY, int pWidth, int pHeight) {
        super(pTexture, pTexturePositionX, pTexturePositionY, pWidth, pHeight);
    }

    public TextureRegionBuffer getTextureBuffer() {
        return (TextureRegionBuffer) this.mTextureRegionBuffer;
    }

    public TextureRegion clone() {
        return new TextureRegion(this.mTexture, this.mTexturePositionX, this.mTexturePositionY, this.mWidth, this.mHeight);
    }

    /* access modifiers changed from: protected */
    public BaseTextureRegionBuffer onCreateTextureRegionBuffer() {
        return new TextureRegionBuffer(this, 35044);
    }
}
