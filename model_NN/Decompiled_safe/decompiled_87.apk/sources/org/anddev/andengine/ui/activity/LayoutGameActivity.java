package org.anddev.andengine.ui.activity;

import org.anddev.andengine.opengl.view.RenderSurfaceView;

public abstract class LayoutGameActivity extends BaseGameActivity {
    /* access modifiers changed from: protected */
    public abstract int getLayoutID();

    /* access modifiers changed from: protected */
    public abstract int getRenderSurfaceViewID();

    /* access modifiers changed from: protected */
    public void onSetContentView() {
        super.setContentView(getLayoutID());
        this.mRenderSurfaceView = (RenderSurfaceView) findViewById(getRenderSurfaceViewID());
        this.mRenderSurfaceView.setEGLConfigChooser(false);
        this.mRenderSurfaceView.setRenderer(this.mEngine);
    }
}
