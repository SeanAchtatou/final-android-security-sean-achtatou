package org.anddev.andengine.engine.camera.hud.controls;

import android.util.FloatMath;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.camera.hud.controls.AnalogOnScreenControl;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class RadiusBoundAnalogOnScreenControl extends AnalogOnScreenControl {
    private final float mMaxDistance = 0.5f;
    private final float mMinDistance = 0.3f;

    public RadiusBoundAnalogOnScreenControl(int pX, int pY, Camera pCamera, TextureRegion pControlBaseTextureRegion, TextureRegion pControlKnobTextureRegion, float pTimeBetweenUpdates, AnalogOnScreenControl.IAnalogOnScreenControlListener pAnalogOnScreenControlListener) {
        super(pX, pY, pCamera, pControlBaseTextureRegion, pControlKnobTextureRegion, pTimeBetweenUpdates, pAnalogOnScreenControlListener);
    }

    public RadiusBoundAnalogOnScreenControl(int pX, int pY, Camera pCamera, TextureRegion pControlBaseTextureRegion, TextureRegion pControlKnobTextureRegion, float pTimeBetweenUpdates, long pOnControlClickMaximumMilliseconds, AnalogOnScreenControl.IAnalogOnScreenControlListener pAnalogOnScreenControlListener) {
        super(pX, pY, pCamera, pControlBaseTextureRegion, pControlKnobTextureRegion, pTimeBetweenUpdates, pOnControlClickMaximumMilliseconds, pAnalogOnScreenControlListener);
    }

    public float getMinDistance() {
        return 0.3f;
    }

    public float getMaxDistance() {
        return 0.5f;
    }

    /* access modifiers changed from: protected */
    public void onUpdateControlKnob(float pRelativeX, float pRelativeY) {
        int i;
        int i2;
        int i3;
        int i4;
        if (!(pRelativeX == 0.0f && pRelativeY == 0.0f)) {
            float hypotenuse = FloatMath.sqrt((pRelativeX * pRelativeX) + (pRelativeY * pRelativeY));
            float rad = (float) Math.atan2((double) pRelativeY, (double) pRelativeX);
            if (hypotenuse > 0.5f) {
                float cos = FloatMath.cos(rad) * 0.5f;
                if (pRelativeX < 0.0f) {
                    i3 = -1;
                } else {
                    i3 = 1;
                }
                pRelativeX = cos * ((float) i3);
                float sin = FloatMath.sin(rad) * 0.5f;
                if (pRelativeY < 0.0f) {
                    i4 = -1;
                } else {
                    i4 = 1;
                }
                pRelativeY = sin * ((float) i4);
            } else if (hypotenuse < 0.3f) {
                float cos2 = FloatMath.cos(rad) * 0.3f;
                if (pRelativeX < 0.0f) {
                    i = -1;
                } else {
                    i = 1;
                }
                pRelativeX = cos2 * ((float) i);
                float sin2 = FloatMath.sin(rad) * 0.3f;
                if (pRelativeY < 0.0f) {
                    i2 = -1;
                } else {
                    i2 = 1;
                }
                pRelativeY = sin2 * ((float) i2);
            }
        }
        super.onUpdateControlKnob(pRelativeX, pRelativeY);
    }
}
