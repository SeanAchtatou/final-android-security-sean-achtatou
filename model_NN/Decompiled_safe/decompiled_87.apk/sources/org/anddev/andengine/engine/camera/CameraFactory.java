package org.anddev.andengine.engine.camera;

import android.app.Activity;
import android.util.DisplayMetrics;

public class CameraFactory {
    public static Camera createPixelPerfectCamera(Activity pActivity, float pCenterX, float pCenterY) {
        DisplayMetrics displayMetrics = getDisplayMetrics(pActivity);
        float width = (float) displayMetrics.widthPixels;
        float height = (float) displayMetrics.heightPixels;
        return new Camera(pCenterX - (width * 0.5f), pCenterY - (0.5f * height), width, height);
    }

    private static DisplayMetrics getDisplayMetrics(Activity pActivity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        pActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }
}
