package org.anddev.andengine.entity.scene.background.modifier;

import org.anddev.andengine.entity.scene.background.IBackground;
import org.anddev.andengine.entity.scene.background.modifier.IBackgroundModifier;
import org.anddev.andengine.util.modifier.SequenceModifier;

public class SequenceBackgroundModifier extends SequenceModifier<IBackground> implements IBackgroundModifier {

    public interface ISubSequenceBackgroundModifierListener extends SequenceModifier.ISubSequenceModifierListener<IBackground> {
    }

    public SequenceBackgroundModifier(IBackgroundModifier... pBackgroundModifiers) throws IllegalArgumentException {
        super(pBackgroundModifiers);
    }

    public SequenceBackgroundModifier(IBackgroundModifier.IBackgroundModifierListener pBackgroundModifierListener, IBackgroundModifier... pBackgroundModifiers) throws IllegalArgumentException {
        super(pBackgroundModifierListener, pBackgroundModifiers);
    }

    public SequenceBackgroundModifier(IBackgroundModifier.IBackgroundModifierListener pBackgroundModifierListener, ISubSequenceBackgroundModifierListener pSubSequenceBackgroundModifierListener, IBackgroundModifier... pBackgroundModifiers) throws IllegalArgumentException {
        super(pBackgroundModifierListener, pSubSequenceBackgroundModifierListener, pBackgroundModifiers);
    }

    protected SequenceBackgroundModifier(SequenceBackgroundModifier pSequenceBackgroundModifier) {
        super(pSequenceBackgroundModifier);
    }

    public SequenceBackgroundModifier clone() {
        return new SequenceBackgroundModifier(this);
    }
}
