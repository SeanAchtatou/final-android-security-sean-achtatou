package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.util.IMatcher;
import org.anddev.andengine.util.modifier.IModifier;

public interface IEntityModifier extends IModifier<IEntity> {

    public interface IEntityModifierListener extends IModifier.IModifierListener<IEntity> {
    }

    public interface IEntityModifierMatcher extends IMatcher<IModifier<IEntity>> {
    }
}
