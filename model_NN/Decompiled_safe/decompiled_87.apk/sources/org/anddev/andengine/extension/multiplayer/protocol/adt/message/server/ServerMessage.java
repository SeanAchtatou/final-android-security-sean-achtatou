package org.anddev.andengine.extension.multiplayer.protocol.adt.message.server;

import org.anddev.andengine.extension.multiplayer.protocol.adt.message.Message;

public abstract class ServerMessage extends Message implements IServerMessage {
}
