package org.anddev.andengine.extension.multiplayer.protocol.client.connector;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.IClientMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.connection.ConnectionCloseClientMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.connection.ConnectionEstablishClientMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.IServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.client.IServerMessageHandler;
import org.anddev.andengine.extension.multiplayer.protocol.client.IServerMessageReader;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connection;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connector;
import org.anddev.andengine.util.Debug;

public class ServerConnector<C extends Connection> extends Connector<C> {
    private final IServerMessageReader<C> mServerMessageReader;

    public interface IServerConnectorListener<T extends Connection> extends Connector.IConnectorListener<ServerConnector<T>> {
        void onConnected(ServerConnector<T> serverConnector);

        void onDisconnected(ServerConnector<T> serverConnector);
    }

    public ServerConnector(C pConnection, IServerConnectorListener<C> pServerConnectorListener) throws IOException {
        this(pConnection, new IServerMessageReader.ServerMessageReader.DefaultServerMessageReader(), pServerConnectorListener);
    }

    public ServerConnector(C pConnection, IServerMessageReader<C> pServerMessageReader, IServerConnectorListener<C> pServerConnectorListener) throws IOException {
        super(pConnection);
        this.mServerMessageReader = pServerMessageReader;
        setServerConnectorListener(pServerConnectorListener);
        sendClientMessage(new ConnectionEstablishClientMessage(1));
    }

    public IServerMessageReader<C> getServerMessageReader() {
        return this.mServerMessageReader;
    }

    public IServerConnectorListener<C> getConnectorListener() {
        return (IServerConnectorListener) super.getConnectorListener();
    }

    public void setServerConnectorListener(IServerConnectorListener<C> pServerConnectorListener) {
        super.setConnectorListener(pServerConnectorListener);
    }

    public void onConnected(Connection pConnection) {
        if (hasConnectorListener()) {
            getConnectorListener().onConnected(this);
        }
    }

    public void onDisconnected(Connection pConnection) {
        if (hasConnectorListener()) {
            getConnectorListener().onDisconnected(this);
        }
        try {
            sendClientMessage(new ConnectionCloseClientMessage());
        } catch (Throwable th) {
            Debug.e(th);
        }
    }

    public void read(DataInputStream pDataInputStream) throws IOException {
        IServerMessage serverMessage = this.mServerMessageReader.readMessage(pDataInputStream);
        this.mServerMessageReader.handleMessage(this, serverMessage);
        this.mServerMessageReader.recycleMessage(serverMessage);
    }

    public void registerServerMessage(short pFlag, Class<? extends IServerMessage> pServerMessageClass) {
        this.mServerMessageReader.registerMessage(pFlag, pServerMessageClass);
    }

    public void registerServerMessage(short pFlag, Class<? extends IServerMessage> pServerMessageClass, IServerMessageHandler<C> pServerMessageHandler) {
        this.mServerMessageReader.registerMessage(pFlag, pServerMessageClass, pServerMessageHandler);
    }

    public void registerServerMessageHandler(short pFlag, IServerMessageHandler<C> pServerMessageHandler) {
        this.mServerMessageReader.registerMessageHandler(pFlag, pServerMessageHandler);
    }

    public void sendClientMessage(IClientMessage pClientMessage) throws IOException {
        DataOutputStream dataOutputStream = this.mConnection.getDataOutputStream();
        pClientMessage.transmit(dataOutputStream);
        dataOutputStream.flush();
    }
}
