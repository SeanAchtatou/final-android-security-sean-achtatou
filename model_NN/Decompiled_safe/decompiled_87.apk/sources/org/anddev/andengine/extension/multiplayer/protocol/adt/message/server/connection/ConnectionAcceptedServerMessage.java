package org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.connection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.ServerMessage;

public class ConnectionAcceptedServerMessage extends ServerMessage {
    public short getFlag() {
        return Short.MIN_VALUE;
    }

    public void onReadTransmissionData(DataInputStream pDataInputStream) throws IOException {
    }

    /* access modifiers changed from: protected */
    public void onWriteTransmissionData(DataOutputStream pDataOutputStream) throws IOException {
    }

    /* access modifiers changed from: protected */
    public void onAppendTransmissionDataForToString(StringBuilder pStringBuilder) {
    }
}
