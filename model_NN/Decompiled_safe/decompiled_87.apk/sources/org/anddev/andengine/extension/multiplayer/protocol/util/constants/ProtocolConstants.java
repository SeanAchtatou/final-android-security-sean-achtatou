package org.anddev.andengine.extension.multiplayer.protocol.util.constants;

public interface ProtocolConstants {
    public static final int PROTOCOL_VERSION = 1;
}
