package org.anddev.andengine.extension.multiplayer.protocol.adt.message.server;

import org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage;
import org.anddev.andengine.extension.multiplayer.protocol.util.constants.ServerMessageFlags;

public interface IServerMessage extends IMessage, ServerMessageFlags {
}
