package org.anddev.andengine.extension.multiplayer.protocol.server;

import java.io.IOException;
import java.util.ArrayList;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.IServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connection;
import org.anddev.andengine.extension.multiplayer.protocol.util.constants.ProtocolConstants;
import org.anddev.andengine.util.Debug;

public abstract class Server<C extends Connection, CC extends ClientConnector<C>> extends Thread implements ProtocolConstants {
    protected final ClientConnector.IClientConnectorListener<C> mClientConnectorListener;
    protected final ArrayList<CC> mClientConnectors = new ArrayList<>();
    private boolean mClosed = true;
    private boolean mRunning = false;
    protected IServerListener<? extends Server<C, CC>> mServerListener;

    public interface IServerListener<S extends Server<?, ?>> {
        void onException(Server server, Throwable th);

        void onStarted(Server server);

        void onTerminated(Server server);
    }

    /* access modifiers changed from: protected */
    public abstract CC acceptClientConnector() throws IOException;

    /* access modifiers changed from: protected */
    public abstract void onException(Throwable th);

    /* access modifiers changed from: protected */
    public abstract void onStart() throws IOException;

    /* access modifiers changed from: protected */
    public abstract void onTerminate();

    public Server(ClientConnector.IClientConnectorListener<C> pClientConnectorListener, IServerListener<? extends Server<C, CC>> pServerListener) {
        this.mServerListener = pServerListener;
        this.mClientConnectorListener = pClientConnectorListener;
        initName();
    }

    private void initName() {
        setName(getClass().getName());
    }

    public boolean isRunning() {
        return this.mRunning;
    }

    public boolean isTerminated() {
        return this.mClosed;
    }

    public IServerListener<? extends Server<C, CC>> getServerListener() {
        return this.mServerListener;
    }

    /* access modifiers changed from: protected */
    public void setServerListener(IServerListener<? extends Server<C, CC>> pServerListener) {
        this.mServerListener = pServerListener;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: CC
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void run() {
        /*
            r4 = this;
            r2 = 1
            r4.mRunning = r2
            r2 = 0
            r4.mClosed = r2
            java.lang.Thread r2 = java.lang.Thread.currentThread()     // Catch:{ Throwable -> 0x0037 }
            r3 = 5
            r2.setPriority(r3)     // Catch:{ Throwable -> 0x0037 }
            r4.onStart()     // Catch:{ Throwable -> 0x0037 }
        L_0x0011:
            boolean r2 = java.lang.Thread.interrupted()     // Catch:{ Throwable -> 0x0037 }
            if (r2 == 0) goto L_0x001b
            r4.close()
        L_0x001a:
            return
        L_0x001b:
            org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector r0 = r4.acceptClientConnector()     // Catch:{ Throwable -> 0x0031 }
            org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector$IClientConnectorListener<C> r2 = r4.mClientConnectorListener     // Catch:{ Throwable -> 0x0031 }
            r0.setClientConnectorListener(r2)     // Catch:{ Throwable -> 0x0031 }
            java.util.ArrayList<CC> r2 = r4.mClientConnectors     // Catch:{ Throwable -> 0x0031 }
            r2.add(r0)     // Catch:{ Throwable -> 0x0031 }
            org.anddev.andengine.extension.multiplayer.protocol.shared.Connection r2 = r0.getConnection()     // Catch:{ Throwable -> 0x0031 }
            r2.start()     // Catch:{ Throwable -> 0x0031 }
            goto L_0x0011
        L_0x0031:
            r2 = move-exception
            r1 = r2
            r4.onException(r1)     // Catch:{ Throwable -> 0x0037 }
            goto L_0x0011
        L_0x0037:
            r2 = move-exception
            r1 = r2
            r4.onException(r1)     // Catch:{ all -> 0x0040 }
            r4.close()
            goto L_0x001a
        L_0x0040:
            r2 = move-exception
            r4.close()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.extension.multiplayer.protocol.server.Server.run():void");
    }

    public void interrupt() {
        close();
        super.interrupt();
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        interrupt();
        super.finalize();
    }

    public void close() {
        if (!this.mClosed) {
            this.mClosed = true;
            this.mRunning = false;
            try {
                ArrayList<CC> clientConnectors = this.mClientConnectors;
                for (int i = 0; i < clientConnectors.size(); i++) {
                    ((ClientConnector) clientConnectors.get(i)).getConnection().interrupt();
                }
                clientConnectors.clear();
            } catch (Exception e) {
                onException(e);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e2) {
                Debug.e(e2);
            }
            onTerminate();
        }
    }

    public void sendBroadcastServerMessage(IServerMessage pServerMessage) throws IOException {
        if (this.mRunning && !this.mClosed) {
            ArrayList<CC> clientConnectors = this.mClientConnectors;
            for (int i = 0; i < clientConnectors.size(); i++) {
                try {
                    ((ClientConnector) clientConnectors.get(i)).sendServerMessage(pServerMessage);
                } catch (IOException e) {
                    onException(e);
                }
            }
        }
    }
}
