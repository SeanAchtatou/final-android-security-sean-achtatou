package org.anddev.andengine.extension.multiplayer.protocol.util;

import android.content.Context;
import android.net.wifi.WifiManager;
import java.util.regex.Pattern;

public class IPUtils {
    private static final Pattern IP_PATTERN = Pattern.compile(REGEXP_IP);
    public static final String LOCALHOST_IP = "127.0.0.1";
    private static final String REGEXP_255 = "(25[0-5]|2[0-4]\\d|[01]?\\d\\d?)";
    private static final String REGEXP_IP = "(25[0-5]|2[0-4]\\d|[01]?\\d\\d?)\\.(25[0-5]|2[0-4]\\d|[01]?\\d\\d?)\\.(25[0-5]|2[0-4]\\d|[01]?\\d\\d?)\\.(25[0-5]|2[0-4]\\d|[01]?\\d\\d?)";

    public static String getIPAddress(Context pContext) {
        return ipAddressToString(((WifiManager) pContext.getSystemService("wifi")).getConnectionInfo().getIpAddress());
    }

    public static String ipAddressToString(int pIPAddress) {
        StringBuilder sb = new StringBuilder();
        int pIPAddress2 = pIPAddress >>> 8;
        int pIPAddress3 = pIPAddress2 >>> 8;
        sb.append(pIPAddress & 255).append('.').append(pIPAddress2 & 255).append('.').append(pIPAddress3 & 255).append('.').append((pIPAddress3 >>> 8) & 255);
        return sb.toString();
    }

    public static boolean isValidIP(String pIPAddress) {
        return IP_PATTERN.matcher(pIPAddress).matches();
    }
}
