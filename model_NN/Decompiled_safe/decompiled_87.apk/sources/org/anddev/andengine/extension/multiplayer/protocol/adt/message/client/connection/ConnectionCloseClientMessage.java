package org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.connection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.ClientMessage;

public class ConnectionCloseClientMessage extends ClientMessage {
    public short getFlag() {
        return -32767;
    }

    public void onReadTransmissionData(DataInputStream pDataInputStream) throws IOException {
    }

    public void onWriteTransmissionData(DataOutputStream pDataOutputStream) throws IOException {
    }

    /* access modifiers changed from: protected */
    public void onAppendTransmissionDataForToString(StringBuilder pStringBuilder) {
    }
}
