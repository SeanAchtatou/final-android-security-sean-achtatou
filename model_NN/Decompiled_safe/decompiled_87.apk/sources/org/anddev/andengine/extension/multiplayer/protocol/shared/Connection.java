package org.anddev.andengine.extension.multiplayer.protocol.shared;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.SocketException;
import org.anddev.andengine.util.Debug;

public abstract class Connection extends Thread {
    protected boolean mClosed = false;
    protected IConnectionListener mConnectionListener;
    protected final DataInputStream mDataInputStream;
    protected final DataOutputStream mDataOutputStream;

    public interface IConnectionListener {
        void onConnected(Connection connection);

        void onDisconnected(Connection connection);

        void read(DataInputStream dataInputStream) throws IOException;
    }

    /* access modifiers changed from: protected */
    public abstract void onClosed();

    public Connection(DataInputStream pDataInputStream, DataOutputStream pDataOutputStream) throws IOException {
        this.mDataInputStream = pDataInputStream;
        this.mDataOutputStream = pDataOutputStream;
    }

    public DataOutputStream getDataOutputStream() {
        return this.mDataOutputStream;
    }

    public DataInputStream getDataInputStream() {
        return this.mDataInputStream;
    }

    public boolean hasConnectionListener() {
        return this.mConnectionListener != null;
    }

    public IConnectionListener getConnectionListener() {
        return this.mConnectionListener;
    }

    public void setConnectionListener(IConnectionListener pConnectionListener) {
        this.mConnectionListener = pConnectionListener;
    }

    public void run() {
        if (this.mConnectionListener != null) {
            this.mConnectionListener.onConnected(this);
        }
        Thread.currentThread().setPriority(10);
        while (!isInterrupted()) {
            try {
                this.mConnectionListener.read(this.mDataInputStream);
            } catch (SocketException e) {
                interrupt();
            } catch (EOFException e2) {
                interrupt();
            } catch (Throwable th) {
                try {
                    Debug.e(th);
                    return;
                } finally {
                    close();
                }
            }
        }
        close();
    }

    public void interrupt() {
        close();
        super.interrupt();
    }

    public void close() {
        if (!this.mClosed) {
            this.mClosed = true;
            if (this.mConnectionListener != null) {
                this.mConnectionListener.onDisconnected(this);
            }
            onClosed();
        }
    }
}
