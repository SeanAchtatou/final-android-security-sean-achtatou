package com.mobclix.android.sdk;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Entity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public class MobclixContactsSdk5 extends MobclixContacts {
    private static String TAG = "MobclixContactsSdk5";
    private final String ACCOUNT_TYPE = "com.google";
    private String accountName;

    public Intent getPickContactIntent() {
        return new Intent("android.intent.action.PICK", ContactsContract.Contacts.CONTENT_URI);
    }

    /* Debug info: failed to restart local var, previous not found, register: 25 */
    public Intent getAddContactIntent(JSONObject contact) {
        String number;
        Uri uri;
        String country;
        String postalCode;
        String state;
        String city;
        String neighborhood;
        String poBox;
        String street;
        String primaryNumber = null;
        try {
            HashMap<String, String> c = parseJSONContact(contact);
            JSONArray addresses = contact.getJSONArray("addresses");
            JSONArray phoneNumbers = contact.getJSONArray("phoneNumbers");
            int j = 0;
            while (true) {
                if (j < phoneNumbers.length()) {
                    JSONObject phoneNumber = phoneNumbers.getJSONObject(j);
                    if (phoneNumber.has("number") && (number = phoneNumber.getString("number")) != null && !number.equals("")) {
                        primaryNumber = number;
                        break;
                    }
                    j++;
                } else {
                    break;
                }
            }
            if (primaryNumber != null) {
                uri = Uri.fromParts("tel", primaryNumber, null);
            } else if (c.get("email") == null) {
                return null;
            } else {
                uri = Uri.fromParts("mailto", c.get("email"), null);
            }
            Intent intent = new Intent("com.android.contacts.action.SHOW_OR_CREATE_CONTACT", uri);
            intent.putExtra("name", c.get("displayName"));
            intent.putExtra("notes", c.get("note"));
            intent.putExtra("company", c.get("organization"));
            intent.putExtra("job_title", c.get("jobTitle"));
            intent.putExtra("email", c.get("email"));
            intent.putExtra("email_type", 1);
            intent.putExtra("im_handle", c.get("im"));
            intent.putExtra("im_protocol", 5);
            if (addresses.length() > 0) {
                JSONObject address = addresses.getJSONObject(0);
                StringBuilder postalAddress = new StringBuilder();
                int type = 1;
                if (address.has("label") && address.getString("label") != null && address.getString("label").equalsIgnoreCase("work")) {
                    type = 2;
                }
                if (address.has("street") && (street = address.getString("street")) != null) {
                    postalAddress.append(street);
                }
                if (address.has("poBox") && (poBox = address.getString("poBox")) != null && !poBox.equals("")) {
                    if (postalAddress.length() != 0) {
                        postalAddress.append(", ");
                    }
                    postalAddress.append(poBox);
                }
                if (address.has("neighborhood") && (neighborhood = address.getString("neighborhood")) != null && !neighborhood.equals("")) {
                    if (postalAddress.length() != 0) {
                        postalAddress.append(", ");
                    }
                    postalAddress.append(neighborhood);
                }
                if (address.has("city") && (city = address.getString("city")) != null && !city.equals("")) {
                    if (postalAddress.length() != 0) {
                        postalAddress.append(", ");
                    }
                    postalAddress.append(city);
                }
                if (address.has("state") && (state = address.getString("state")) != null && !state.equals("")) {
                    if (postalAddress.length() != 0) {
                        postalAddress.append(", ");
                    }
                    postalAddress.append(state);
                }
                if (address.has("postalCode") && (postalCode = address.getString("postalCode")) != null && !postalCode.equals("")) {
                    if (postalAddress.length() != 0) {
                        postalAddress.append(", ");
                    }
                    postalAddress.append(postalCode);
                }
                if (address.has("country") && (country = address.getString("country")) != null && !country.equals("")) {
                    if (postalAddress.length() != 0) {
                        postalAddress.append(", ");
                    }
                    postalAddress.append(country);
                }
                if (postalAddress.length() > 0) {
                    intent.putExtra("postal", postalAddress.toString());
                    intent.putExtra("postal_type", type);
                }
            }
            for (int j2 = 0; j2 < phoneNumbers.length(); j2++) {
                JSONObject phoneNumber2 = phoneNumbers.getJSONObject(j2);
                int type2 = 1;
                String number2 = null;
                if (phoneNumber2.has("label") && phoneNumber2.getString("label") != null && phoneNumber2.getString("label").equalsIgnoreCase("work")) {
                    type2 = 3;
                }
                if (phoneNumber2.has("number")) {
                    number2 = phoneNumber2.getString("number");
                    if (number2.equals("")) {
                        number2 = null;
                    }
                }
                if (j2 != 0) {
                    if (j2 != 1) {
                        if (j2 != 2) {
                            break;
                        }
                        intent.putExtra("tertiary_phone", number2);
                        intent.putExtra("tertiary_phone_type", type2);
                    } else {
                        intent.putExtra("secondary_phone", number2);
                        intent.putExtra("secondary_phone_type", type2);
                    }
                } else {
                    intent.putExtra("phone", number2);
                    intent.putExtra("phone_type", type2);
                }
            }
            return intent;
        } catch (Exception e) {
            Exception exc = e;
            return null;
        }
    }

    private static Cursor setupContactCursor(ContentResolver resolver, Uri lookupUri) {
        if (lookupUri == null) {
            return null;
        }
        List<String> segments = lookupUri.getPathSegments();
        if (segments.size() != 4) {
            return null;
        }
        long uriContactId = Long.parseLong(segments.get(3));
        String uriLookupKey = Uri.encode(segments.get(2));
        Cursor cursor = resolver.query(Uri.withAppendedPath(ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, uriContactId), TMXConstants.TAG_DATA), null, null, null, null);
        if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        } else if (cursor.getString(cursor.getColumnIndex("lookup")).equals(uriLookupKey)) {
            return cursor;
        } else {
            cursor.close();
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:259:0x06c4, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:260:0x06c5, code lost:
        r18.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:304:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00ca, code lost:
        if (r39.equals("") == false) goto L_0x0676;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x02c7, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x02c8, code lost:
        r18.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x02cb, code lost:
        throw r3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x02c7 A[ExcHandler: all (r3v8 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r18 
      PHI: (r18v1 'cursor' android.database.Cursor) = (r18v0 'cursor' android.database.Cursor), (r18v5 'cursor' android.database.Cursor), (r18v5 'cursor' android.database.Cursor), (r18v5 'cursor' android.database.Cursor), (r18v5 'cursor' android.database.Cursor), (r18v5 'cursor' android.database.Cursor), (r18v5 'cursor' android.database.Cursor), (r18v5 'cursor' android.database.Cursor), (r18v5 'cursor' android.database.Cursor), (r18v5 'cursor' android.database.Cursor), (r18v5 'cursor' android.database.Cursor), (r18v5 'cursor' android.database.Cursor), (r18v5 'cursor' android.database.Cursor), (r18v5 'cursor' android.database.Cursor), (r18v5 'cursor' android.database.Cursor), (r18v5 'cursor' android.database.Cursor) binds: [B:7:0x0040, B:41:0x0100, B:15:0x008f, B:20:0x00a5, B:23:0x00ae, B:238:0x0644, B:247:0x067f, B:243:0x065f, B:244:?, B:28:0x00bb, B:24:?, B:21:?, B:46:0x010c, B:47:?, B:48:0x0112, B:97:0x02d1] A[DONT_GENERATE, DONT_INLINE], Splitter:B:7:0x0040] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.json.JSONObject loadContact(android.content.ContentResolver r67, android.net.Uri r68) {
        /*
            r66 = this;
            r40 = 0
            java.lang.String r12 = r68.getAuthority()
            java.lang.String r3 = "com.android.contacts"
            boolean r3 = r3.equals(r12)
            if (r3 == 0) goto L_0x00d3
            r40 = r68
        L_0x0010:
            org.json.JSONObject r16 = new org.json.JSONObject
            r16.<init>()
            org.json.JSONArray r11 = new org.json.JSONArray
            r11.<init>()
            org.json.JSONArray r51 = new org.json.JSONArray
            r51.<init>()
            java.lang.String r3 = "addresses"
            r0 = r16
            r1 = r3
            r2 = r11
            r0.put(r1, r2)     // Catch:{ Exception -> 0x06d3 }
            java.lang.String r3 = "phoneNumbers"
            r0 = r16
            r1 = r3
            r2 = r51
            r0.put(r1, r2)     // Catch:{ Exception -> 0x06d3 }
        L_0x0032:
            java.lang.String r34 = ""
            java.lang.String r35 = ""
            r18 = 0
            r23 = 0
            r32 = 0
            r0 = r67
            r1 = r40
            android.database.Cursor r18 = setupContactCursor(r0, r1)     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            if (r18 != 0) goto L_0x0056
            r0 = r67
            r1 = r40
            android.net.Uri r40 = android.provider.ContactsContract.Contacts.getLookupUri(r0, r1)     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r0 = r67
            r1 = r40
            android.database.Cursor r18 = setupContactCursor(r0, r1)     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
        L_0x0056:
            long r14 = android.content.ContentUris.parseId(r40)     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r18.close()     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            android.net.Uri r4 = android.provider.ContactsContract.RawContactsEntity.CONTENT_URI     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r5 = 0
            java.lang.String r6 = "contact_id=?"
            r3 = 1
            java.lang.String[] r7 = new java.lang.String[r3]     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r3 = 0
            java.lang.String r8 = java.lang.String.valueOf(r14)     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r7[r3] = r8     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r8 = 0
            r3 = r67
            android.database.Cursor r18 = r3.query(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            java.util.ArrayList r45 = new java.util.ArrayList     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            int r3 = r18.getCount()     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r0 = r45
            r1 = r3
            r0.<init>(r1)     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            java.lang.String r3 = android.os.Build.VERSION.SDK     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            int r59 = java.lang.Integer.parseInt(r3)     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            com.mobclix.android.sdk.MobclixContactsEntityIterator r37 = newEntityIterator(r18)     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
        L_0x0089:
            boolean r3 = r37.hasNext()     // Catch:{ all -> 0x00ff }
            if (r3 != 0) goto L_0x00f1
            r37.close()     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            java.util.Iterator r3 = r45.iterator()     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
        L_0x0096:
            boolean r4 = r3.hasNext()     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            if (r4 != 0) goto L_0x010c
            r31 = 0
            r39 = 0
            java.lang.String r3 = "firstName"
            r0 = r16
            r1 = r3
            java.lang.String r31 = r0.getString(r1)     // Catch:{ Exception -> 0x06d0, all -> 0x02c7 }
        L_0x00a9:
            java.lang.String r3 = "lastName"
            r0 = r16
            r1 = r3
            java.lang.String r39 = r0.getString(r1)     // Catch:{ Exception -> 0x06cd, all -> 0x02c7 }
        L_0x00b2:
            if (r31 == 0) goto L_0x00cc
            if (r39 == 0) goto L_0x00cc
            java.lang.String r3 = ""
            r0 = r31
            r1 = r3
            boolean r3 = r0.equals(r1)     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            if (r3 != 0) goto L_0x00cc
            java.lang.String r3 = ""
            r0 = r39
            r1 = r3
            boolean r3 = r0.equals(r1)     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            if (r3 == 0) goto L_0x0676
        L_0x00cc:
            if (r23 != 0) goto L_0x063f
            r18.close()
            r3 = 0
        L_0x00d2:
            return r3
        L_0x00d3:
            java.lang.String r3 = "contacts"
            boolean r3 = r3.equals(r12)
            if (r3 == 0) goto L_0x0010
            long r56 = android.content.ContentUris.parseId(r68)
            android.net.Uri r3 = android.provider.ContactsContract.RawContacts.CONTENT_URI
            r0 = r3
            r1 = r56
            android.net.Uri r3 = android.content.ContentUris.withAppendedId(r0, r1)
            r0 = r67
            r1 = r3
            android.net.Uri r40 = android.provider.ContactsContract.RawContacts.getContactLookupUri(r0, r1)
            goto L_0x0010
        L_0x00f1:
            java.lang.Object r29 = r37.next()     // Catch:{ all -> 0x00ff }
            android.content.Entity r29 = (android.content.Entity) r29     // Catch:{ all -> 0x00ff }
            r0 = r45
            r1 = r29
            r0.add(r1)     // Catch:{ all -> 0x00ff }
            goto L_0x0089
        L_0x00ff:
            r3 = move-exception
            r37.close()     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            throw r3     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
        L_0x0104:
            r3 = move-exception
            r25 = r3
            r18.close()
            r3 = 0
            goto L_0x00d2
        L_0x010c:
            java.lang.Object r29 = r3.next()     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            android.content.Entity r29 = (android.content.Entity) r29     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            android.content.ContentValues r28 = r29.getEntityValues()     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            java.lang.String r4 = "account_type"
            r0 = r28
            r1 = r4
            java.lang.String r9 = r0.getAsString(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            java.lang.String r4 = "_id"
            r0 = r28
            r1 = r4
            java.lang.Long r4 = r0.getAsLong(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            long r56 = r4.longValue()     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            java.lang.String r4 = "ACCOUNT TYPE: "
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            java.lang.String r6 = java.lang.String.valueOf(r9)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            java.lang.String r6 = ": "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r5
            r1 = r56
            java.lang.StringBuilder r5 = r0.append(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            android.util.Log.v(r4, r5)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            java.util.ArrayList r4 = r29.getSubValues()     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
        L_0x0153:
            boolean r5 = r4.hasNext()     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x0096
            java.lang.Object r62 = r4.next()     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            android.content.Entity$NamedContentValues r62 = (android.content.Entity.NamedContentValues) r62     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r30 = r0
            java.lang.String r5 = "raw_contact_id"
            java.lang.Long r6 = java.lang.Long.valueOf(r56)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r30
            r1 = r5
            r2 = r6
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            java.lang.String r5 = "_id"
            r0 = r30
            r1 = r5
            java.lang.Long r5 = r0.getAsLong(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            long r20 = r5.longValue()     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            java.lang.String r5 = "mimetype"
            r0 = r30
            r1 = r5
            java.lang.String r42 = r0.getAsString(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r42 == 0) goto L_0x0153
            java.lang.String r5 = "vnd.android.cursor.item/phone_v2"
            r0 = r5
            r1 = r42
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x01ef
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "PHONE"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data1"
            java.lang.String r48 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            java.lang.String r64 = "home"
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data2"
            java.lang.Integer r5 = r5.getAsInteger(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            int r5 = r5.intValue()     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r6 = 3
            if (r5 != r6) goto L_0x01bd
            java.lang.String r64 = "work"
        L_0x01bd:
            if (r48 == 0) goto L_0x0153
            java.lang.String r5 = ""
            r0 = r48
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0153
            org.json.JSONObject r50 = new org.json.JSONObject     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r50.<init>()     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            java.lang.String r5 = "number"
            r0 = r50
            r1 = r5
            r2 = r48
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            java.lang.String r5 = "label"
            r0 = r50
            r1 = r5
            r2 = r64
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r51
            r1 = r50
            r0.put(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            goto L_0x0153
        L_0x01ec:
            r4 = move-exception
            goto L_0x0096
        L_0x01ef:
            java.lang.String r5 = "vnd.android.cursor.item/name"
            r0 = r5
            r1 = r42
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x02cc
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "NAME"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data1"
            java.lang.String r19 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r19 == 0) goto L_0x021b
            java.lang.String r5 = ""
            r0 = r19
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x021b
            r23 = r19
        L_0x021b:
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data2"
            java.lang.String r31 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data3"
            java.lang.String r39 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data5"
            java.lang.String r41 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data4"
            java.lang.String r55 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data6"
            java.lang.String r63 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r31 == 0) goto L_0x0269
            java.lang.String r5 = ""
            r0 = r31
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0269
            java.lang.String r5 = "firstName"
            r0 = r16
            r1 = r5
            r2 = r31
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
        L_0x0269:
            if (r39 == 0) goto L_0x0280
            java.lang.String r5 = ""
            r0 = r39
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0280
            java.lang.String r5 = "lastName"
            r0 = r16
            r1 = r5
            r2 = r39
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
        L_0x0280:
            if (r41 == 0) goto L_0x0297
            java.lang.String r5 = ""
            r0 = r41
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0297
            java.lang.String r5 = "middleName"
            r0 = r16
            r1 = r5
            r2 = r41
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
        L_0x0297:
            if (r55 == 0) goto L_0x02ae
            java.lang.String r5 = ""
            r0 = r55
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x02ae
            java.lang.String r5 = "prefix"
            r0 = r16
            r1 = r5
            r2 = r55
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
        L_0x02ae:
            if (r63 == 0) goto L_0x0153
            java.lang.String r5 = ""
            r0 = r63
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0153
            java.lang.String r5 = "suffix"
            r0 = r16
            r1 = r5
            r2 = r63
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            goto L_0x0153
        L_0x02c7:
            r3 = move-exception
            r18.close()
            throw r3
        L_0x02cc:
            java.lang.String r5 = "vnd.android.cursor.item/nickname"
            r0 = r5
            r1 = r42
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x0302
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "NICKNAME"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data1"
            java.lang.String r46 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r46 == 0) goto L_0x0153
            java.lang.String r5 = ""
            r0 = r46
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0153
            java.lang.String r5 = "nickname"
            r0 = r16
            r1 = r5
            r2 = r46
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            goto L_0x0153
        L_0x0302:
            java.lang.String r5 = "vnd.android.cursor.item/organization"
            r0 = r5
            r1 = r42
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x037c
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "ORGANIZATION"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data1"
            java.lang.String r49 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data4"
            java.lang.String r38 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data5"
            java.lang.String r22 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r49 == 0) goto L_0x034c
            java.lang.String r5 = ""
            r0 = r49
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x034c
            java.lang.String r5 = "organization"
            r0 = r16
            r1 = r5
            r2 = r49
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
        L_0x034c:
            if (r38 == 0) goto L_0x0363
            java.lang.String r5 = ""
            r0 = r38
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0363
            java.lang.String r5 = "jobTitle"
            r0 = r16
            r1 = r5
            r2 = r38
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
        L_0x0363:
            if (r22 == 0) goto L_0x0153
            java.lang.String r5 = ""
            r0 = r22
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0153
            java.lang.String r5 = "department"
            r0 = r16
            r1 = r5
            r2 = r22
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            goto L_0x0153
        L_0x037c:
            java.lang.String r5 = "vnd.android.cursor.item/email_v2"
            r0 = r5
            r1 = r42
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x03b2
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "EMAIL"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data1"
            java.lang.String r27 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r27 == 0) goto L_0x0153
            java.lang.String r5 = ""
            r0 = r27
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0153
            java.lang.String r5 = "email"
            r0 = r16
            r1 = r5
            r2 = r27
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            goto L_0x0153
        L_0x03b2:
            java.lang.String r5 = "vnd.android.cursor.item/note"
            r0 = r5
            r1 = r42
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x03e8
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "NOTE"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data1"
            java.lang.String r47 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r47 == 0) goto L_0x0153
            java.lang.String r5 = ""
            r0 = r47
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0153
            java.lang.String r5 = "note"
            r0 = r16
            r1 = r5
            r2 = r47
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            goto L_0x0153
        L_0x03e8:
            java.lang.String r5 = "vnd.android.cursor.item/postal-address_v2"
            r0 = r5
            r1 = r42
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x05a5
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "POSTAL"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data1"
            java.lang.String r24 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r24 == 0) goto L_0x0412
            java.lang.String r5 = ""
            r0 = r24
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x0414
        L_0x0412:
            r24 = 0
        L_0x0414:
            java.lang.String r64 = "home"
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data2"
            java.lang.Integer r5 = r5.getAsInteger(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            int r5 = r5.intValue()     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r6 = 2
            if (r5 != r6) goto L_0x042a
            java.lang.String r64 = "work"
        L_0x042a:
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data4"
            java.lang.String r61 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r61 == 0) goto L_0x0442
            java.lang.String r5 = ""
            r0 = r61
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x0444
        L_0x0442:
            r61 = 0
        L_0x0444:
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data5"
            java.lang.String r53 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r53 == 0) goto L_0x045c
            java.lang.String r5 = ""
            r0 = r53
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x045e
        L_0x045c:
            r53 = 0
        L_0x045e:
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data6"
            java.lang.String r44 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r44 == 0) goto L_0x0476
            java.lang.String r5 = ""
            r0 = r44
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x0478
        L_0x0476:
            r44 = 0
        L_0x0478:
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data7"
            java.lang.String r13 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r13 == 0) goto L_0x048d
            java.lang.String r5 = ""
            boolean r5 = r13.equals(r5)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x048e
        L_0x048d:
            r13 = 0
        L_0x048e:
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data8"
            java.lang.String r60 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r60 == 0) goto L_0x04a6
            java.lang.String r5 = ""
            r0 = r60
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x04a8
        L_0x04a6:
            r60 = 0
        L_0x04a8:
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data9"
            java.lang.String r54 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r54 == 0) goto L_0x04c0
            java.lang.String r5 = ""
            r0 = r54
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x04c2
        L_0x04c0:
            r54 = 0
        L_0x04c2:
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data10"
            java.lang.String r17 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r17 == 0) goto L_0x04da
            java.lang.String r5 = ""
            r0 = r17
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x04dc
        L_0x04da:
            r17 = 0
        L_0x04dc:
            if (r61 != 0) goto L_0x04ff
            if (r53 != 0) goto L_0x04ff
            if (r44 != 0) goto L_0x04ff
            if (r13 != 0) goto L_0x04ff
            if (r60 != 0) goto L_0x04ff
            if (r54 != 0) goto L_0x04ff
            if (r17 != 0) goto L_0x04ff
            if (r24 == 0) goto L_0x0153
            org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r10.<init>()     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            java.lang.String r5 = "street"
            r0 = r10
            r1 = r5
            r2 = r24
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r11.put(r10)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            goto L_0x0153
        L_0x04ff:
            org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r10.<init>()     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r61 == 0) goto L_0x051a
            java.lang.String r5 = ""
            r0 = r61
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x051a
            java.lang.String r5 = "street"
            r0 = r10
            r1 = r5
            r2 = r61
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
        L_0x051a:
            if (r53 == 0) goto L_0x0530
            java.lang.String r5 = ""
            r0 = r53
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0530
            java.lang.String r5 = "poBox"
            r0 = r10
            r1 = r5
            r2 = r53
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
        L_0x0530:
            if (r44 == 0) goto L_0x0546
            java.lang.String r5 = ""
            r0 = r44
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0546
            java.lang.String r5 = "neighborhood"
            r0 = r10
            r1 = r5
            r2 = r44
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
        L_0x0546:
            if (r13 == 0) goto L_0x0555
            java.lang.String r5 = ""
            boolean r5 = r13.equals(r5)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0555
            java.lang.String r5 = "city"
            r10.put(r5, r13)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
        L_0x0555:
            if (r60 == 0) goto L_0x056b
            java.lang.String r5 = ""
            r0 = r60
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x056b
            java.lang.String r5 = "state"
            r0 = r10
            r1 = r5
            r2 = r60
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
        L_0x056b:
            if (r54 == 0) goto L_0x0581
            java.lang.String r5 = ""
            r0 = r54
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0581
            java.lang.String r5 = "postalCode"
            r0 = r10
            r1 = r5
            r2 = r54
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
        L_0x0581:
            if (r17 == 0) goto L_0x0597
            java.lang.String r5 = ""
            r0 = r17
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0597
            java.lang.String r5 = "country"
            r0 = r10
            r1 = r5
            r2 = r17
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
        L_0x0597:
            java.lang.String r5 = "label"
            r0 = r10
            r1 = r5
            r2 = r64
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r11.put(r10)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            goto L_0x0153
        L_0x05a5:
            java.lang.String r5 = "vnd.android.cursor.item/im"
            r0 = r5
            r1 = r42
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x05db
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "IM"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data1"
            java.lang.String r36 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r36 == 0) goto L_0x0153
            java.lang.String r5 = ""
            r0 = r36
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0153
            java.lang.String r5 = "IM"
            r0 = r16
            r1 = r5
            r2 = r36
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            goto L_0x0153
        L_0x05db:
            java.lang.String r5 = "vnd.android.cursor.item/website"
            r0 = r5
            r1 = r42
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x0611
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "WEBSITE"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data1"
            java.lang.String r65 = r5.getAsString(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r65 == 0) goto L_0x0153
            java.lang.String r5 = ""
            r0 = r65
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 != 0) goto L_0x0153
            java.lang.String r5 = "website"
            r0 = r16
            r1 = r5
            r2 = r65
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            goto L_0x0153
        L_0x0611:
            java.lang.String r5 = "vnd.android.cursor.item/photo"
            r0 = r5
            r1 = r42
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r5 == 0) goto L_0x0153
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "PHOTO"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r62
            android.content.ContentValues r0 = r0.values     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r5 = r0
            java.lang.String r6 = "data15"
            byte[] r52 = r5.getAsByteArray(r6)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            if (r52 == 0) goto L_0x0153
            java.lang.String r5 = "image"
            java.lang.String r6 = com.mobclix.android.sdk.Base64.encodeBytes(r52)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            r0 = r16
            r1 = r5
            r2 = r6
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x02c7 }
            goto L_0x0153
        L_0x063f:
            java.lang.String r3 = " "
            r0 = r23
            r1 = r3
            java.lang.String[] r43 = r0.split(r1)     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r31 = 0
            r41 = 0
            r39 = 0
            r0 = r43
            int r0 = r0.length     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r3 = r0
            r4 = 1
            if (r3 != r4) goto L_0x067d
            r3 = 0
            r31 = r43[r3]     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
        L_0x0658:
            java.lang.String r3 = "firstName"
            r0 = r16
            r1 = r3
            r2 = r31
            r0.put(r1, r2)     // Catch:{ Exception -> 0x06c4, all -> 0x02c7 }
            java.lang.String r3 = "middleName"
            r0 = r16
            r1 = r3
            r2 = r41
            r0.put(r1, r2)     // Catch:{ Exception -> 0x06c4, all -> 0x02c7 }
            java.lang.String r3 = "lastName"
            r0 = r16
            r1 = r3
            r2 = r39
            r0.put(r1, r2)     // Catch:{ Exception -> 0x06c4, all -> 0x02c7 }
        L_0x0676:
            r18.close()
            r3 = r16
            goto L_0x00d2
        L_0x067d:
            r0 = r43
            int r0 = r0.length     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r3 = r0
            r4 = 2
            if (r3 != r4) goto L_0x068b
            r3 = 0
            r31 = r43[r3]     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r3 = 1
            r39 = r43[r3]     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            goto L_0x0658
        L_0x068b:
            r0 = r43
            int r0 = r0.length     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r3 = r0
            r4 = 3
            if (r3 < r4) goto L_0x0658
            r3 = 0
            r31 = r43[r3]     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r3 = 1
            r41 = r43[r3]     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            java.lang.StringBuilder r58 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r3 = 2
            r3 = r43[r3]     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r0 = r58
            r1 = r3
            r0.<init>(r1)     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r33 = 3
        L_0x06a5:
            r0 = r43
            int r0 = r0.length     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r3 = r0
            r0 = r33
            r1 = r3
            if (r0 < r1) goto L_0x06b3
            java.lang.String r39 = r58.toString()     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            goto L_0x0658
        L_0x06b3:
            java.lang.String r3 = " "
            r0 = r58
            r1 = r3
            java.lang.StringBuilder r3 = r0.append(r1)     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r4 = r43[r33]     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            r3.append(r4)     // Catch:{ Exception -> 0x0104, all -> 0x02c7 }
            int r33 = r33 + 1
            goto L_0x06a5
        L_0x06c4:
            r3 = move-exception
            r26 = r3
            r18.close()
            r3 = 0
            goto L_0x00d2
        L_0x06cd:
            r3 = move-exception
            goto L_0x00b2
        L_0x06d0:
            r3 = move-exception
            goto L_0x00a9
        L_0x06d3:
            r3 = move-exception
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixContactsSdk5.loadContact(android.content.ContentResolver, android.net.Uri):org.json.JSONObject");
    }

    public void addContact(JSONObject contact, Activity activity) throws Exception {
        this.accountName = AccountManager.get(activity).getAccountsByType("com.google")[0].name;
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        int rawContactInsertIndex = ops.size();
        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI).withValue("account_type", "com.google").withValue("account_name", this.accountName).build());
        Cursor tempCur = activity.managedQuery(ContactsContract.Data.CONTENT_URI, new String[]{"data1", "group_sourceid"}, "mimetype='vnd.android.cursor.item/group_membership'", null, null);
        if (tempCur.moveToFirst()) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", rawContactInsertIndex).withValue("mimetype", "vnd.android.cursor.item/group_membership").withValue("group_sourceid", Integer.valueOf(tempCur.getInt(1))).build());
        }
        HashMap<String, String> c = parseJSONContact(contact);
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", rawContactInsertIndex).withValue("mimetype", "vnd.android.cursor.item/name").withValue("data2", c.get("firstName")).withValue("data5", c.get("middleName")).withValue("data3", c.get("lastName")).withValue("data4", c.get("prefix")).withValue("data6", c.get("suffix")).withValue("data1", c.get("displayName")).build());
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", rawContactInsertIndex).withValue("mimetype", "vnd.android.cursor.item/nickname").withValue("data1", c.get("nickname")).withValue("data2", 1).build());
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", rawContactInsertIndex).withValue("mimetype", "vnd.android.cursor.item/organization").withValue("data1", c.get("organization")).withValue("data4", c.get("jobTitle")).withValue("data5", c.get("department")).build());
        if (c.get("email") != null) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", rawContactInsertIndex).withValue("mimetype", "vnd.android.cursor.item/email_v2").withValue("data1", c.get("email")).withValue("data2", 3).build());
        }
        if (c.get("note") != null) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", rawContactInsertIndex).withValue("mimetype", "vnd.android.cursor.item/note").withValue("data1", c.get("note")).build());
        }
        if (c.get("website") != null) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", rawContactInsertIndex).withValue("mimetype", "vnd.android.cursor.item/website").withValue("data1", c.get("website")).withValue("data2", 1).build());
        }
        if (c.get("birthday") != null) {
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'hh:mmZ").parse(c.get("birthday"));
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", rawContactInsertIndex).withValue("mimetype", "vnd.android.cursor.item/contact_event").withValue("data1", String.valueOf(date.getMonth()) + "/" + date.getDate() + "/" + date.getYear()).withValue("data2", 3).build());
        }
        if (c.get("im") != null) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", rawContactInsertIndex).withValue("mimetype", "vnd.android.cursor.item/im").withValue("data1", c.get("im")).withValue("data2", 1).withValue("data5", 5).build());
        }
        if (c.get(TMXConstants.TAG_IMAGE) != null) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", rawContactInsertIndex).withValue("mimetype", "vnd.android.cursor.item/photo").withValue("data15", Base64.decode(c.get(TMXConstants.TAG_IMAGE))).build());
        }
        JSONArray addresses = contact.getJSONArray("addresses");
        for (int i = 0; i < addresses.length(); i++) {
            JSONObject address = addresses.getJSONObject(i);
            int type = 1;
            String street = null;
            String city = null;
            String state = null;
            String postalCode = null;
            String country = null;
            String neighborhood = null;
            String poBox = null;
            if (address.has("label") && address.getString("label").equalsIgnoreCase("work")) {
                type = 2;
            }
            if (address.has("street") && (street = address.getString("street")) == null) {
                street = "";
            }
            if (address.has("city") && (city = address.getString("city")) == null) {
                city = "";
            }
            if (address.has("state") && (state = address.getString("state")) == null) {
                state = "";
            }
            if (address.has("postalCode") && (postalCode = address.getString("postalCode")) == null) {
                postalCode = "";
            }
            if (address.has("country") && (country = address.getString("country")) == null) {
                country = "";
            }
            if (address.has("neighborhood") && (neighborhood = address.getString("neighborhood")) == null) {
                neighborhood = "";
            }
            if (address.has("poBox") && (poBox = address.getString("poBox")) == null) {
                poBox = "";
            }
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", rawContactInsertIndex).withValue("mimetype", "vnd.android.cursor.item/postal-address_v2").withValue("data2", Integer.valueOf(type)).withValue("data4", street).withValue("data7", city).withValue("data8", state).withValue("data9", postalCode).withValue("data10", country).withValue("data6", neighborhood).withValue("data5", poBox).build());
        }
        JSONArray numbers = contact.getJSONArray("phoneNumbers");
        for (int i2 = 0; i2 < numbers.length(); i2++) {
            JSONObject phoneNumber = numbers.getJSONObject(i2);
            int type2 = 1;
            String number = null;
            if (phoneNumber.has("label") && phoneNumber.getString("label").equalsIgnoreCase("work")) {
                type2 = 3;
            }
            if (phoneNumber.has("number") && (number = phoneNumber.getString("number")) == null) {
                number = "";
            }
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", rawContactInsertIndex).withValue("mimetype", "vnd.android.cursor.item/phone_v2").withValue("data2", Integer.valueOf(type2)).withValue("data1", number).build());
        }
        activity.getContentResolver().applyBatch("com.android.contacts", ops);
    }

    /* access modifiers changed from: package-private */
    public HashMap<String, String> parseJSONContact(JSONObject contact) throws Exception {
        HashMap<String, String> c = new HashMap<>();
        String firstName = null;
        String lastName = null;
        String middleName = null;
        String prefix = null;
        String suffix = null;
        String nickname = null;
        StringBuilder dName = new StringBuilder();
        if (contact.has("firstName")) {
            try {
                firstName = contact.getString("firstName");
            } catch (Exception e) {
            }
            if (firstName.equals("")) {
                firstName = null;
            }
            if (firstName != null) {
                dName.append(firstName);
            }
        }
        if (contact.has("middleName")) {
            try {
                middleName = contact.getString("middleName");
            } catch (Exception e2) {
            }
            if (middleName.equals("")) {
                middleName = null;
            }
            if (middleName != null) {
                if (dName.length() != 0) {
                    dName.append(" ");
                }
                dName.append(middleName);
            }
        }
        if (contact.has("lastName")) {
            try {
                lastName = contact.getString("lastName");
            } catch (Exception e3) {
            }
            if (lastName.equals("")) {
                lastName = null;
            }
            if (lastName != null) {
                if (dName.length() != 0) {
                    dName.append(" ");
                }
                dName.append(lastName);
            }
        }
        String displayName = dName.toString();
        if (contact.has("prefix")) {
            try {
                prefix = contact.getString("prefix");
            } catch (Exception e4) {
            }
            if (prefix.equals("")) {
                prefix = null;
            }
        }
        if (contact.has("suffix")) {
            try {
                suffix = contact.getString("suffix");
            } catch (Exception e5) {
            }
            if (suffix.equals("")) {
                suffix = null;
            }
        }
        if (contact.has("nickname")) {
            try {
                nickname = contact.getString("nickname");
            } catch (Exception e6) {
            }
            if (nickname.equals("")) {
                nickname = null;
            }
        }
        if (displayName == null || displayName.equals("")) {
            throw new Exception("Adding contact failed: No name provided.");
        }
        c.put("firstName", firstName);
        c.put("lastName", lastName);
        c.put("middleName", middleName);
        c.put("prefix", prefix);
        c.put("suffix", suffix);
        c.put("displayName", displayName);
        c.put("nickname", nickname);
        String organization = null;
        String jobTitle = null;
        String department = null;
        if (contact.has("organization")) {
            try {
                organization = contact.getString("organization");
            } catch (Exception e7) {
            }
            if (organization.equals("")) {
                organization = null;
            }
        }
        if (contact.has("jobTitle")) {
            try {
                jobTitle = contact.getString("jobTitle");
            } catch (Exception e8) {
            }
            if (jobTitle.equals("")) {
                jobTitle = null;
            }
        }
        if (contact.has("department")) {
            try {
                department = contact.getString("department");
            } catch (Exception e9) {
            }
            if (department.equals("")) {
                department = null;
            }
        }
        c.put("organization", organization);
        c.put("jobTitle", jobTitle);
        c.put("department", department);
        String email = null;
        String im = null;
        String website = null;
        String note = null;
        String birthday = null;
        String photo = null;
        if (contact.has("email")) {
            try {
                email = contact.getString("email");
            } catch (Exception e10) {
            }
            if (email.equals("")) {
                email = null;
            }
        }
        if (contact.has("IM")) {
            try {
                im = contact.getString("IM");
            } catch (Exception e11) {
            }
            if (im.equals("")) {
                im = null;
            }
        }
        if (contact.has("website")) {
            try {
                website = contact.getString("website");
            } catch (Exception e12) {
            }
            if (website.equals("")) {
                website = null;
            }
        }
        if (contact.has("note")) {
            try {
                note = contact.getString("note");
            } catch (Exception e13) {
            }
            if (note.equals("")) {
                note = null;
            }
        }
        if (contact.has("birthday")) {
            try {
                birthday = contact.getString("birthday");
            } catch (Exception e14) {
            }
            if (birthday.equals("")) {
                birthday = null;
            }
        }
        if (contact.has("photo")) {
            try {
                photo = contact.getString(TMXConstants.TAG_IMAGE);
            } catch (Exception e15) {
            }
            if (photo.equals("")) {
                photo = null;
            }
        }
        c.put("email", email);
        c.put("im", im);
        c.put("website", website);
        c.put("note", note);
        c.put("birthday", birthday);
        c.put(TMXConstants.TAG_IMAGE, photo);
        return c;
    }

    public static MobclixContactsEntityIterator newEntityIterator(Cursor cursor) {
        return new EntityIteratorImpl(cursor);
    }

    private static class EntityIteratorImpl extends MobclixContactsCursorEntityIterator {
        private static final String[] DATA_KEYS = {"data1", "data2", "data3", "data4", "data5", "data6", "data7", "data8", "data9", "data10", "data11", "data12", "data13", "data14", "data15", "data_sync1", "data_sync2", "data_sync3", "data_sync4"};

        public EntityIteratorImpl(Cursor cursor) {
            super(cursor);
        }

        public Entity getEntityAndIncrementCursor(Cursor cursor) throws RemoteException {
            int columnRawContactId = cursor.getColumnIndexOrThrow("_id");
            long rawContactId = cursor.getLong(columnRawContactId);
            ContentValues cv = new ContentValues();
            MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, cv, "account_name");
            MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, cv, "account_type");
            MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, cv, "_id");
            MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, cv, "dirty");
            MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, cv, "version");
            MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, cv, "sourceid");
            MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, cv, "sync1");
            MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, cv, "sync2");
            MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, cv, "sync3");
            MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, cv, "sync4");
            MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, cv, "deleted");
            MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, cv, "contact_id");
            MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, cv, "starred");
            MobclixContactsSdk5.cursorIntToContentValuesIfPresent(cursor, cv, "is_restricted");
            Entity contact = new Entity(cv);
            while (rawContactId == cursor.getLong(columnRawContactId)) {
                ContentValues cv2 = new ContentValues();
                cv2.put("_id", Long.valueOf(cursor.getLong(cursor.getColumnIndexOrThrow("data_id"))));
                MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, cv2, "res_package");
                MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, cv2, "mimetype");
                MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, cv2, "is_primary");
                MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, cv2, "is_super_primary");
                MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, cv2, "data_version");
                MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, cv2, "group_sourceid");
                MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, cv2, "data_version");
                for (String key : DATA_KEYS) {
                    int columnIndex = cursor.getColumnIndexOrThrow(key);
                    if (!cursor.isNull(columnIndex)) {
                        try {
                            cv2.put(key, cursor.getString(columnIndex));
                        } catch (SQLiteException e) {
                            cv2.put(key, cursor.getBlob(columnIndex));
                        }
                    }
                }
                contact.addSubValue(ContactsContract.Data.CONTENT_URI, cv2);
                if (!cursor.moveToNext()) {
                    break;
                }
            }
            return contact;
        }
    }

    public static void cursorStringToContentValuesIfPresent(Cursor cursor, ContentValues values, String column) {
        int index = cursor.getColumnIndexOrThrow(column);
        if (!cursor.isNull(index)) {
            values.put(column, cursor.getString(index));
        }
    }

    public static void cursorLongToContentValuesIfPresent(Cursor cursor, ContentValues values, String column) {
        int index = cursor.getColumnIndexOrThrow(column);
        if (!cursor.isNull(index)) {
            values.put(column, Long.valueOf(cursor.getLong(index)));
        }
    }

    public static void cursorIntToContentValuesIfPresent(Cursor cursor, ContentValues values, String column) {
        int index = cursor.getColumnIndexOrThrow(column);
        if (!cursor.isNull(index)) {
            values.put(column, Integer.valueOf(cursor.getInt(index)));
        }
    }
}
