package com.mobclix.android.sdk;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

class MobclixLocation {
    boolean gps_enabled = false;
    LocationManager lm;
    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            try {
                if (MobclixLocation.this.timer1 != null) {
                    MobclixLocation.this.timer1.cancel();
                    MobclixLocation.this.timer1.purge();
                    MobclixLocation.this.timer1 = null;
                }
                MobclixLocation.this.locationResult.gotLocation(location);
            } catch (Exception e) {
            }
            try {
                MobclixLocation.this.lm.removeUpdates(this);
                MobclixLocation.this.lm.removeUpdates(MobclixLocation.this.locationListenerNetwork);
            } catch (Exception e2) {
            }
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            try {
                if (MobclixLocation.this.timer1 != null) {
                    MobclixLocation.this.timer1.cancel();
                    MobclixLocation.this.timer1.purge();
                    MobclixLocation.this.timer1 = null;
                }
                MobclixLocation.this.locationResult.gotLocation(location);
            } catch (Exception e) {
            }
            try {
                MobclixLocation.this.lm.removeUpdates(this);
                MobclixLocation.this.lm.removeUpdates(MobclixLocation.this.locationListenerGps);
            } catch (Exception e2) {
            }
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    LocationResult locationResult;
    boolean network_enabled = false;
    Timer timer1;

    public static abstract class LocationResult {
        public abstract void gotLocation(Location location);
    }

    MobclixLocation() {
    }

    public synchronized boolean getLocation(Context context, LocationResult result) {
        boolean z;
        if (this.timer1 != null) {
            z = true;
        } else {
            try {
                this.locationResult = result;
                if (this.lm == null) {
                    this.lm = (LocationManager) context.getSystemService("location");
                }
                if (this.lm == null) {
                    z = false;
                } else {
                    if (isProviderSupported("gps")) {
                        this.gps_enabled = this.lm.isProviderEnabled("gps");
                    } else {
                        this.gps_enabled = false;
                    }
                    if (isProviderSupported("network")) {
                        this.network_enabled = this.lm.isProviderEnabled("network");
                    } else {
                        this.network_enabled = false;
                    }
                    if (this.gps_enabled || this.network_enabled) {
                        new Thread(new GetLastLocation()).run();
                        z = true;
                    } else {
                        z = false;
                    }
                }
            } catch (Exception e) {
            }
        }
        return z;
    }

    public void stopLocation() {
        try {
            if (this.timer1 != null) {
                this.timer1.cancel();
                this.timer1.purge();
                this.timer1 = null;
            }
            if (this.lm != null) {
                if (this.locationListenerGps != null) {
                    this.lm.removeUpdates(this.locationListenerGps);
                }
                if (this.locationListenerNetwork != null) {
                    this.lm.removeUpdates(this.locationListenerNetwork);
                }
            }
        } catch (Exception e) {
        }
    }

    class GetLastLocation extends TimerTask {
        GetLastLocation() {
        }

        public void run() {
            Location net_loc = null;
            Location gps_loc = null;
            try {
                if (MobclixLocation.this.gps_enabled) {
                    gps_loc = MobclixLocation.this.lm.getLastKnownLocation("gps");
                }
                if (MobclixLocation.this.network_enabled) {
                    net_loc = MobclixLocation.this.lm.getLastKnownLocation("network");
                }
                if (gps_loc == null || net_loc == null) {
                    if (gps_loc != null) {
                        MobclixLocation.this.locationResult.gotLocation(gps_loc);
                    } else if (net_loc != null) {
                        MobclixLocation.this.locationResult.gotLocation(net_loc);
                    } else {
                        MobclixLocation.this.locationResult.gotLocation(null);
                    }
                } else if (gps_loc.getTime() > net_loc.getTime()) {
                    MobclixLocation.this.locationResult.gotLocation(gps_loc);
                } else {
                    MobclixLocation.this.locationResult.gotLocation(net_loc);
                }
            } catch (Exception e) {
            }
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public boolean isProviderSupported(String in_Provider) {
        try {
            List<String> lv_List = this.lm.getAllProviders();
            int lv_N = 0;
            while (lv_N < lv_List.size()) {
                try {
                    if (in_Provider.equals(lv_List.get(lv_N))) {
                        return true;
                    }
                    lv_N++;
                } catch (Exception e) {
                    return false;
                }
            }
            return false;
        } catch (Throwable th) {
            Throwable th2 = th;
            return false;
        }
    }
}
