package com.mobclix.android.sdk;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Contacts;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public class MobclixContactsSdk3_4 extends MobclixContacts {
    public Intent getPickContactIntent() {
        return new Intent("android.intent.action.PICK", Contacts.People.CONTENT_URI);
    }

    public Intent getAddContactIntent(JSONObject contact) {
        return null;
    }

    /* JADX INFO: finally extract failed */
    public JSONObject loadContact(ContentResolver contentResolver, Uri contactUri) {
        JSONObject contactInfo = new JSONObject();
        Cursor cursor = contentResolver.query(contactUri, null, null, null, null);
        try {
            if (cursor.moveToFirst()) {
                String[] names = cursor.getString(cursor.getColumnIndexOrThrow("name")).split(" ");
                String firstName = null;
                String middleName = null;
                String lastName = null;
                if (names.length == 1) {
                    firstName = names[0];
                } else if (names.length == 2) {
                    firstName = names[0];
                    lastName = names[1];
                } else if (names.length >= 3) {
                    firstName = names[0];
                    middleName = names[1];
                    StringBuilder sb = new StringBuilder(names[2]);
                    for (int i = 3; i < names.length; i++) {
                        sb.append(" ").append(names[i]);
                    }
                    lastName = sb.toString();
                }
                String id = cursor.getString(cursor.getColumnIndex("_id"));
                contactInfo.put("firstName", firstName);
                contactInfo.put("middleName", middleName);
                contactInfo.put("lastName", lastName);
                contactInfo.put("note", cursor.getString(cursor.getColumnIndexOrThrow("notes")));
                cursor.close();
                try {
                    cursor = contentResolver.query(Uri.withAppendedPath(contactUri, "organizations"), null, null, null, "isprimary DESC");
                    if (cursor.moveToFirst()) {
                        contactInfo.put("organization", cursor.getString(cursor.getColumnIndexOrThrow("company")));
                        contactInfo.put("jobTitle", cursor.getString(cursor.getColumnIndexOrThrow("title")));
                    }
                } catch (Exception e) {
                } finally {
                    cursor.close();
                }
                try {
                    cursor = contentResolver.query(Contacts.ContactMethods.CONTENT_EMAIL_URI, null, "person = ?", new String[]{id}, null);
                    if (cursor.moveToFirst()) {
                        contactInfo.put("email", cursor.getString(cursor.getColumnIndexOrThrow(TMXConstants.TAG_DATA)));
                    }
                } catch (Exception e2) {
                } finally {
                    cursor.close();
                }
                try {
                    ContentResolver contentResolver2 = contentResolver;
                    cursor = contentResolver2.query(Contacts.ContactMethods.CONTENT_URI, null, "person = ? AND kind = ?", new String[]{id, "vnd.android.cursor.item/jabber-im"}, null);
                    if (cursor.moveToFirst()) {
                        contactInfo.put("IM", cursor.getString(cursor.getColumnIndex(TMXConstants.TAG_DATA)));
                    }
                } catch (Exception e3) {
                } finally {
                    cursor.close();
                }
                try {
                    cursor = contentResolver.query(Uri.withAppendedPath(contactUri, "phones"), null, null, null, "isprimary DESC");
                    JSONArray phoneNumbers = new JSONArray();
                    while (cursor.moveToNext()) {
                        JSONObject phoneNumber = new JSONObject();
                        phoneNumber.put("number", cursor.getString(cursor.getColumnIndex("number")));
                        String type = "home";
                        if (cursor.getInt(cursor.getColumnIndex(TMXConstants.TAG_OBJECT_ATTRIBUTE_TYPE)) == 3) {
                            type = "work";
                        }
                        phoneNumber.put("label", type);
                        phoneNumbers.put(phoneNumber);
                    }
                    contactInfo.put("phoneNumbers", phoneNumbers);
                } catch (Exception e4) {
                } finally {
                    cursor.close();
                }
                try {
                    ContentResolver contentResolver3 = contentResolver;
                    cursor = contentResolver3.query(Contacts.ContactMethods.CONTENT_URI, null, "person = ? AND kind = ?", new String[]{id, Integer.toString(2)}, null);
                    JSONArray addresses = new JSONArray();
                    while (cursor.moveToNext()) {
                        JSONObject address = new JSONObject();
                        address.put("street", cursor.getString(cursor.getColumnIndex(TMXConstants.TAG_DATA)));
                        String type2 = "home";
                        if (cursor.getInt(cursor.getColumnIndex(TMXConstants.TAG_OBJECT_ATTRIBUTE_TYPE)) == 2) {
                            type2 = "work";
                        }
                        address.put("label", type2);
                        address.put("city", (Object) null);
                        address.put("state", (Object) null);
                        address.put("postalCode", (Object) null);
                        address.put("country", (Object) null);
                        address.put("countryCode", (Object) null);
                        address.put("neighborhood", (Object) null);
                        address.put("poBox", (Object) null);
                        addresses.put(address);
                    }
                    contactInfo.put("addresses", addresses);
                    contactInfo.put("prefix", (Object) null);
                    contactInfo.put("suffix", (Object) null);
                    contactInfo.put("nickname", (Object) null);
                    contactInfo.put("department", (Object) null);
                    contactInfo.put("birthday", (Object) null);
                    contactInfo.put("website", (Object) null);
                    contactInfo.put(TMXConstants.TAG_IMAGE, (Object) null);
                } catch (Exception e5) {
                } finally {
                    cursor.close();
                }
                return contactInfo;
            }
            cursor.close();
            return null;
        } catch (Exception e6) {
            cursor.close();
            return null;
        } catch (Throwable th) {
            cursor.close();
            throw th;
        }
    }

    public void addContact(JSONObject contact, Activity activity) throws Exception {
        String country;
        String postalCode;
        String state;
        String city;
        String poBox;
        String street;
        String lastName;
        String middleName;
        String firstName;
        StringBuilder dName = new StringBuilder();
        if (contact.has("firstName") && (firstName = contact.getString("firstName")) != null) {
            dName.append(firstName);
        }
        if (contact.has("middleName") && (middleName = contact.getString("middleName")) != null && !middleName.equals("")) {
            if (dName.length() != 0) {
                dName.append(" ");
            }
            dName.append(middleName);
        }
        if (contact.has("lastName") && (lastName = contact.getString("lastName")) != null && !lastName.equals("")) {
            if (dName.length() != 0) {
                dName.append(" ");
            }
            dName.append(lastName);
        }
        String displayName = dName.toString();
        if (contact.has("prefix") && contact.getString("prefix") == null) {
        }
        if (contact.has("suffix") && contact.getString("suffix") == null) {
        }
        if (contact.has("nickname") && contact.getString("nickname") == null) {
        }
        String note = null;
        if (contact.has("note") && (note = contact.getString("note")) == null) {
            note = "";
        }
        if (displayName == null || displayName.equals("")) {
            throw new Exception("Adding contact failed: No name provided.");
        }
        ContentValues c = new ContentValues();
        c.put("name", displayName);
        if (!note.equals("")) {
            c.put("notes", note);
        }
        Uri newPersonUri = Contacts.People.createPersonInMyContactsGroup(activity.getContentResolver(), c);
        c.clear();
        if (newPersonUri == null) {
            throw new Exception("Error adding contact.");
        }
        String organization = null;
        String jobTitle = null;
        if (contact.has("organization") && (organization = contact.getString("organization")) == null) {
            organization = "";
        }
        if (contact.has("jobTitle") && (jobTitle = contact.getString("jobTitle")) == null) {
            jobTitle = "";
        }
        if (contact.has("department") && contact.getString("department") == null) {
        }
        if (!organization.equals("")) {
            Uri orgUri = Uri.withAppendedPath(newPersonUri, "organizations");
            c.put("company", organization);
            if (!jobTitle.equals("")) {
                c.put("title", jobTitle);
            }
            c.put(TMXConstants.TAG_OBJECT_ATTRIBUTE_TYPE, (Integer) 1);
            if (activity.getContentResolver().insert(orgUri, c) == null) {
                throw new Exception("Failed to insert organisation");
            }
            c.clear();
        }
        String email = null;
        if (contact.has("email") && (email = contact.getString("email")) == null) {
            email = "";
        }
        if (!email.equals("")) {
            Uri emailUri = Uri.withAppendedPath(newPersonUri, "contact_methods");
            c.put("kind", (Integer) 1);
            c.put(TMXConstants.TAG_OBJECT_ATTRIBUTE_TYPE, (Integer) 1);
            c.put(TMXConstants.TAG_DATA, email);
            if (activity.getContentResolver().insert(emailUri, c) == null) {
                throw new Exception("Failed to insert email");
            }
            c.clear();
        }
        String im = null;
        if (contact.has("im") && (im = contact.getString("im")) == null) {
            im = "";
        }
        if (!im.equals("")) {
            Uri imUri = Uri.withAppendedPath(newPersonUri, "contact_methods");
            c.put("kind", (Integer) 3);
            c.put(TMXConstants.TAG_DATA, im);
            c.put("aux_data", Contacts.ContactMethods.encodePredefinedImProtocol(5));
            c.put(TMXConstants.TAG_OBJECT_ATTRIBUTE_TYPE, (Integer) 1);
            c.put(TMXConstants.TAG_DATA, im);
            if (activity.getContentResolver().insert(imUri, c) == null) {
                throw new Exception("Failed to insert IM");
            }
            c.clear();
        }
        JSONArray addresses = contact.getJSONArray("addresses");
        for (int i = 0; i < addresses.length(); i++) {
            JSONObject address = addresses.getJSONObject(i);
            int type = 1;
            if (address.has("label") && address.getString("label").equalsIgnoreCase("work")) {
                type = 2;
            }
            StringBuilder postalAddress = new StringBuilder();
            if (address.has("street") && (street = address.getString("street")) != null) {
                postalAddress.append(street);
            }
            if (address.has("poBox") && (poBox = address.getString("poBox")) != null && !poBox.equals("")) {
                if (postalAddress.length() != 0) {
                    postalAddress.append(", ");
                }
                postalAddress.append(poBox);
            }
            if (address.has("city") && (city = address.getString("city")) != null && !city.equals("")) {
                if (postalAddress.length() != 0) {
                    postalAddress.append(", ");
                }
                postalAddress.append(city);
            }
            if (address.has("state") && (state = address.getString("state")) != null && !state.equals("")) {
                if (postalAddress.length() != 0) {
                    postalAddress.append(", ");
                }
                postalAddress.append(state);
            }
            if (address.has("postalCode") && (postalCode = address.getString("postalCode")) != null && !postalCode.equals("")) {
                if (postalAddress.length() != 0) {
                    postalAddress.append(", ");
                }
                postalAddress.append(postalCode);
            }
            if (address.has("country") && (country = address.getString("country")) != null && !country.equals("")) {
                if (postalAddress.length() != 0) {
                    postalAddress.append(", ");
                }
                postalAddress.append(country);
            }
            if (postalAddress.length() > 0) {
                Uri addressUri = Uri.withAppendedPath(newPersonUri, "contact_methods");
                c.put("kind", (Integer) 2);
                c.put(TMXConstants.TAG_OBJECT_ATTRIBUTE_TYPE, Integer.valueOf(type));
                c.put(TMXConstants.TAG_DATA, postalAddress.toString());
                if (activity.getContentResolver().insert(addressUri, c) == null) {
                    throw new Exception("Failed to insert address");
                }
                c.clear();
            }
        }
        JSONArray numbers = contact.getJSONArray("phoneNumbers");
        for (int i2 = 0; i2 < numbers.length(); i2++) {
            JSONObject phoneNumber = numbers.getJSONObject(i2);
            int type2 = 1;
            String number = null;
            if (phoneNumber.has("label") && phoneNumber.getString("label").equalsIgnoreCase("work")) {
                type2 = 3;
            }
            if (phoneNumber.has("number") && (number = phoneNumber.getString("number")) == null) {
                number = "";
            }
            if (!number.equals("")) {
                Uri mobileUri = Uri.withAppendedPath(newPersonUri, "phones");
                c.put("number", number);
                c.put(TMXConstants.TAG_OBJECT_ATTRIBUTE_TYPE, Integer.valueOf(type2));
                if (activity.getContentResolver().insert(mobileUri, c) == null) {
                    throw new Exception("Failed to insert mobile phone number");
                }
                c.clear();
            }
        }
    }
}
