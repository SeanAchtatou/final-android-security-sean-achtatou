package com.mobclix.android.sdk;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import com.mobclix.android.sdk.Mobclix;
import java.io.FileOutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: MobclixConfig */
class FetchRemoteConfig extends AsyncTask<String, Integer, JSONArray> {
    private static String TAG = "MobclixConfig";
    private static boolean running = false;
    Mobclix c = Mobclix.getInstance();
    private MobclixInstrumentation instrumentation = MobclixInstrumentation.getInstance();
    private String url;

    FetchRemoteConfig() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:116:0x06f5 A[LOOP:1: B:20:0x0142->B:116:0x06f5, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x0825  */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x0156 A[EDGE_INSN: B:179:0x0156->B:22:0x0156 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.json.JSONArray doInBackground(java.lang.String... r41) {
        /*
            r40 = this;
            boolean r35 = com.mobclix.android.sdk.FetchRemoteConfig.running
            if (r35 == 0) goto L_0x0007
            r35 = 0
        L_0x0006:
            return r35
        L_0x0007:
            r35 = 1
            com.mobclix.android.sdk.FetchRemoteConfig.running = r35
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            if (r35 != 0) goto L_0x001d
            com.mobclix.android.sdk.MobclixInstrumentation r35 = com.mobclix.android.sdk.MobclixInstrumentation.getInstance()
            r0 = r35
            r1 = r40
            r1.instrumentation = r0
        L_0x001d:
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            java.lang.String r36 = com.mobclix.android.sdk.MobclixInstrumentation.STARTUP
            java.lang.String r23 = r35.startGroup(r36)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            java.lang.String r36 = "config"
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r35 = r0
            android.content.Context r35 = r35.getContext()
            android.webkit.CookieSyncManager.createInstance(r35)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            java.lang.String r36 = "update_session"
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r35 = r0
            r35.updateSession()
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)
            r6 = 0
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            java.lang.String r36 = "update"
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            java.lang.String r36 = "parse_cache"
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            java.lang.String r36 = "load_misc_settings"
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)
            java.lang.String r35 = "deviceId"
            boolean r35 = com.mobclix.android.sdk.Mobclix.hasPref(r35)     // Catch:{ Exception -> 0x02a9 }
            if (r35 == 0) goto L_0x0288
            java.lang.String r35 = "deviceId"
            java.lang.String r26 = com.mobclix.android.sdk.Mobclix.getPref(r35)     // Catch:{ Exception -> 0x02a9 }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x02a9 }
            r35 = r0
            r0 = r35
            java.lang.String r0 = r0.deviceId     // Catch:{ Exception -> 0x02a9 }
            r35 = r0
            r0 = r26
            r1 = r35
            boolean r35 = r0.equals(r1)     // Catch:{ Exception -> 0x02a9 }
            if (r35 != 0) goto L_0x00d8
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x02a9 }
            r35 = r0
            r0 = r26
            r1 = r35
            r1.previousDeviceId = r0     // Catch:{ Exception -> 0x02a9 }
        L_0x00d8:
            java.lang.String r35 = "passiveSessionTimeout"
            boolean r35 = com.mobclix.android.sdk.Mobclix.hasPref(r35)
            if (r35 == 0) goto L_0x00f6
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r35 = r0
            java.lang.String r36 = "passiveSessionTimeout"
            java.lang.String r36 = com.mobclix.android.sdk.Mobclix.getPref(r36)
            int r36 = java.lang.Integer.parseInt(r36)
            r0 = r36
            r1 = r35
            r1.passiveSessionTimeout = r0
        L_0x00f6:
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            java.lang.String r36 = "load_adunits"
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)
            java.lang.String[] r35 = com.mobclix.android.sdk.Mobclix.MC_AD_SIZES
            r0 = r35
            int r0 = r0.length
            r36 = r0
            r37 = 0
        L_0x011f:
            r0 = r37
            r1 = r36
            if (r0 < r1) goto L_0x02ac
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)
            r7 = 1
        L_0x0142:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r35 = r0
            r0 = r35
            int r0 = r0.remoteConfigSet
            r35 = r0
            r36 = 1
            r0 = r35
            r1 = r36
            if (r0 != r1) goto L_0x0321
        L_0x0156:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r35 = r0
            r0 = r35
            int r0 = r0.remoteConfigSet
            r35 = r0
            r36 = 1
            r0 = r35
            r1 = r36
            if (r0 == r1) goto L_0x0248
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            java.lang.String r36 = "parse_cache"
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            java.lang.String r36 = "load_urls"
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)
            java.lang.String r35 = "ConfigServer"
            boolean r35 = com.mobclix.android.sdk.Mobclix.hasPref(r35)
            if (r35 == 0) goto L_0x01a8
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r35 = r0
            java.lang.String r36 = "ConfigServer"
            java.lang.String r36 = com.mobclix.android.sdk.Mobclix.getPref(r36)
            r0 = r36
            r1 = r35
            r1.configServer = r0
        L_0x01a8:
            java.lang.String r35 = "AdServer"
            boolean r35 = com.mobclix.android.sdk.Mobclix.hasPref(r35)
            if (r35 == 0) goto L_0x01c2
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r35 = r0
            java.lang.String r36 = "AdServer"
            java.lang.String r36 = com.mobclix.android.sdk.Mobclix.getPref(r36)
            r0 = r36
            r1 = r35
            r1.adServer = r0
        L_0x01c2:
            java.lang.String r35 = "AnalyticsServer"
            boolean r35 = com.mobclix.android.sdk.Mobclix.hasPref(r35)
            if (r35 == 0) goto L_0x01dc
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r35 = r0
            java.lang.String r36 = "AnalyticsServer"
            java.lang.String r36 = com.mobclix.android.sdk.Mobclix.getPref(r36)
            r0 = r36
            r1 = r35
            r1.analyticsServer = r0
        L_0x01dc:
            java.lang.String r35 = "VcServer"
            boolean r35 = com.mobclix.android.sdk.Mobclix.hasPref(r35)
            if (r35 == 0) goto L_0x01f6
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r35 = r0
            java.lang.String r36 = "VcServer"
            java.lang.String r36 = com.mobclix.android.sdk.Mobclix.getPref(r36)
            r0 = r36
            r1 = r35
            r1.vcServer = r0
        L_0x01f6:
            java.lang.String r35 = "FeedbackServer"
            boolean r35 = com.mobclix.android.sdk.Mobclix.hasPref(r35)
            if (r35 == 0) goto L_0x0210
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r35 = r0
            java.lang.String r36 = "FeedbackServer"
            java.lang.String r36 = com.mobclix.android.sdk.Mobclix.getPref(r36)
            r0 = r36
            r1 = r35
            r1.feedbackServer = r0
        L_0x0210:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r35 = r0
            r36 = 1
            r0 = r36
            r1 = r35
            r1.remoteConfigSet = r0
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r35 = r0
            r36 = 1
            r0 = r36
            r1 = r35
            r1.isOfflineSession = r0
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)
        L_0x0248:
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            java.lang.String r36 = com.mobclix.android.sdk.MobclixInstrumentation.STARTUP
            r35.finishGroup(r36)
            com.mobclix.android.sdk.Mobclix.sync()
            r35 = 0
            com.mobclix.android.sdk.FetchRemoteConfig.running = r35
            r35 = r6
            goto L_0x0006
        L_0x0288:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x02a9 }
            r35 = r0
            r36 = 1
            r0 = r36
            r1 = r35
            r1.isNewUser = r0     // Catch:{ Exception -> 0x02a9 }
            java.lang.String r35 = "deviceId"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x02a9 }
            r36 = r0
            r0 = r36
            java.lang.String r0 = r0.deviceId     // Catch:{ Exception -> 0x02a9 }
            r36 = r0
            com.mobclix.android.sdk.Mobclix.addPref(r35, r36)     // Catch:{ Exception -> 0x02a9 }
            goto L_0x00d8
        L_0x02a9:
            r35 = move-exception
            goto L_0x00d8
        L_0x02ac:
            r30 = r35[r37]
            java.util.HashMap<java.lang.String, com.mobclix.android.sdk.MobclixAdUnitSettings> r38 = com.mobclix.android.sdk.Mobclix.adUnitSettings
            com.mobclix.android.sdk.MobclixAdUnitSettings r39 = new com.mobclix.android.sdk.MobclixAdUnitSettings
            r39.<init>()
            r0 = r38
            r1 = r30
            r2 = r39
            r0.put(r1, r2)
            boolean r38 = com.mobclix.android.sdk.Mobclix.hasPref(r30)
            if (r38 == 0) goto L_0x02d3
            java.lang.String r10 = com.mobclix.android.sdk.Mobclix.getPref(r30)
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Exception -> 0x031a }
            r4.<init>(r10)     // Catch:{ Exception -> 0x031a }
            r0 = r40
            r1 = r4
            r0.processAdUnitConfig(r1)     // Catch:{ Exception -> 0x031a }
        L_0x02d3:
            java.lang.StringBuilder r38 = new java.lang.StringBuilder
            java.lang.String r39 = java.lang.String.valueOf(r30)
            r38.<init>(r39)
            java.lang.String r39 = "CustomAdUrl"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r38 = r38.toString()
            boolean r38 = com.mobclix.android.sdk.Mobclix.hasPref(r38)
            if (r38 == 0) goto L_0x0316
            java.util.HashMap<java.lang.String, com.mobclix.android.sdk.MobclixAdUnitSettings> r38 = com.mobclix.android.sdk.Mobclix.adUnitSettings
            r0 = r38
            r1 = r30
            java.lang.Object r41 = r0.get(r1)
            com.mobclix.android.sdk.MobclixAdUnitSettings r41 = (com.mobclix.android.sdk.MobclixAdUnitSettings) r41
            java.lang.StringBuilder r38 = new java.lang.StringBuilder
            java.lang.String r39 = java.lang.String.valueOf(r30)
            r38.<init>(r39)
            java.lang.String r39 = "CustomAdUrl"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r38 = r38.toString()
            java.lang.String r38 = com.mobclix.android.sdk.Mobclix.getPref(r38)
            r0 = r41
            r1 = r38
            r0.setCustomAdUrl(r1)
        L_0x0316:
            int r37 = r37 + 1
            goto L_0x011f
        L_0x031a:
            r38 = move-exception
            r17 = r38
            com.mobclix.android.sdk.Mobclix.removePref(r30)
            goto L_0x02d3
        L_0x0321:
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            java.lang.StringBuilder r36 = new java.lang.StringBuilder
            java.lang.String r37 = "attempt_"
            r36.<init>(r37)
            r0 = r36
            r1 = r7
            java.lang.StringBuilder r36 = r0.append(r1)
            java.lang.String r36 = r36.toString()
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            java.lang.String r36 = "build_request"
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)
            r35 = 1
            r0 = r7
            r1 = r35
            if (r0 != r1) goto L_0x06f9
            r35 = 1
        L_0x035e:
            r0 = r40
            r1 = r35
            java.lang.String r35 = r0.getConfigUrl(r1)
            r0 = r35
            r1 = r40
            r1.url = r0
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            java.lang.String r36 = "send_request"
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)
            java.lang.String r28 = ""
            r11 = 0
            r9 = 0
            com.mobclix.android.sdk.Mobclix$MobclixHttpClient r19 = new com.mobclix.android.sdk.Mobclix$MobclixHttpClient     // Catch:{ Exception -> 0x084c, all -> 0x081e }
            r0 = r40
            java.lang.String r0 = r0.url     // Catch:{ Exception -> 0x084c, all -> 0x081e }
            r35 = r0
            r0 = r19
            r1 = r35
            r0.<init>(r1)     // Catch:{ Exception -> 0x084c, all -> 0x081e }
            org.apache.http.HttpResponse r21 = r19.execute()     // Catch:{ Exception -> 0x084c, all -> 0x081e }
            org.apache.http.HttpEntity r20 = r21.getEntity()     // Catch:{ Exception -> 0x084c, all -> 0x081e }
            org.apache.http.StatusLine r35 = r21.getStatusLine()     // Catch:{ Exception -> 0x084c, all -> 0x081e }
            int r29 = r35.getStatusCode()     // Catch:{ Exception -> 0x084c, all -> 0x081e }
            r35 = 200(0xc8, float:2.8E-43)
            r0 = r29
            r1 = r35
            if (r0 != r1) goto L_0x07fd
            java.io.BufferedReader r8 = new java.io.BufferedReader     // Catch:{ Exception -> 0x084c, all -> 0x081e }
            java.io.InputStreamReader r35 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x084c, all -> 0x081e }
            java.io.InputStream r36 = r20.getContent()     // Catch:{ Exception -> 0x084c, all -> 0x081e }
            r35.<init>(r36)     // Catch:{ Exception -> 0x084c, all -> 0x081e }
            r36 = 8000(0x1f40, float:1.121E-41)
            r0 = r8
            r1 = r35
            r2 = r36
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x084c, all -> 0x081e }
            java.lang.String r31 = r8.readLine()     // Catch:{ Exception -> 0x07e2 }
        L_0x03d0:
            if (r31 != 0) goto L_0x06fd
            r20.consumeContent()     // Catch:{ Exception -> 0x07e2 }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x07e2 }
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)     // Catch:{ Exception -> 0x07e2 }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x07e2 }
            r35 = r0
            java.lang.String r36 = "handle_response"
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)     // Catch:{ Exception -> 0x07e2 }
            java.lang.String r35 = ""
            r0 = r28
            r1 = r35
            boolean r35 = r0.equals(r1)     // Catch:{ Exception -> 0x07e2 }
            if (r35 != 0) goto L_0x06ca
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            java.lang.String r36 = "decode_json"
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)     // Catch:{ Exception -> 0x07cf }
            org.json.JSONObject r12 = new org.json.JSONObject     // Catch:{ Exception -> 0x07cf }
            r0 = r12
            r1 = r28
            r0.<init>(r1)     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            java.lang.String r36 = "save_json"
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            java.lang.String r36 = "raw_config_json"
            java.lang.String r37 = com.mobclix.android.sdk.MobclixInstrumentation.STARTUP     // Catch:{ Exception -> 0x07cf }
            r0 = r35
            r1 = r28
            r2 = r36
            r3 = r37
            r0.addInfo(r1, r2, r3)     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            java.lang.String r36 = "decoded_config_json"
            java.lang.String r37 = com.mobclix.android.sdk.MobclixInstrumentation.STARTUP     // Catch:{ Exception -> 0x07cf }
            r0 = r35
            r1 = r12
            r2 = r36
            r3 = r37
            r0.addInfo(r1, r2, r3)     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            java.lang.String r36 = "load_config"
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)     // Catch:{ Exception -> 0x07cf }
            java.lang.String r35 = "urls"
            r0 = r12
            r1 = r35
            org.json.JSONObject r32 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            java.lang.String r36 = "config"
            r0 = r32
            r1 = r36
            java.lang.String r36 = r0.getString(r1)     // Catch:{ Exception -> 0x07cf }
            r0 = r36
            r1 = r35
            r1.configServer = r0     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            java.lang.String r36 = "ads"
            r0 = r32
            r1 = r36
            java.lang.String r36 = r0.getString(r1)     // Catch:{ Exception -> 0x07cf }
            r0 = r36
            r1 = r35
            r1.adServer = r0     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            java.lang.String r36 = "analytics"
            r0 = r32
            r1 = r36
            java.lang.String r36 = r0.getString(r1)     // Catch:{ Exception -> 0x07cf }
            r0 = r36
            r1 = r35
            r1.analyticsServer = r0     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            java.lang.String r36 = "vc"
            r0 = r32
            r1 = r36
            java.lang.String r36 = r0.getString(r1)     // Catch:{ Exception -> 0x07cf }
            r0 = r36
            r1 = r35
            r1.vcServer = r0     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            java.lang.String r36 = "feedback"
            r0 = r32
            r1 = r36
            java.lang.String r36 = r0.getString(r1)     // Catch:{ Exception -> 0x07cf }
            r0 = r36
            r1 = r35
            r1.feedbackServer = r0     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            java.lang.String r36 = "debug"
            r0 = r32
            r1 = r36
            java.lang.String r36 = r0.getString(r1)     // Catch:{ Exception -> 0x07cf }
            r0 = r36
            r1 = r35
            r1.debugServer = r0     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x085a }
            r35 = r0
            java.lang.String r36 = "passive_session_timeout"
            r0 = r12
            r1 = r36
            int r36 = r0.getInt(r1)     // Catch:{ Exception -> 0x085a }
            r0 = r36
            int r0 = r0 * 1000
            r36 = r0
            r0 = r36
            r1 = r35
            r1.passiveSessionTimeout = r0     // Catch:{ Exception -> 0x085a }
        L_0x052c:
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            java.lang.String r36 = "set_default_values"
            r0 = r35
            r1 = r23
            r2 = r36
            java.lang.String r23 = r0.benchmarkStart(r1, r2)     // Catch:{ Exception -> 0x07cf }
            java.util.HashMap r27 = new java.util.HashMap     // Catch:{ Exception -> 0x07cf }
            r27.<init>()     // Catch:{ Exception -> 0x07cf }
            java.lang.String r35 = "ConfigServer"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r36 = r0
            r0 = r36
            java.lang.String r0 = r0.configServer     // Catch:{ Exception -> 0x07cf }
            r36 = r0
            r0 = r27
            r1 = r35
            r2 = r36
            r0.put(r1, r2)     // Catch:{ Exception -> 0x07cf }
            java.lang.String r35 = "AdServer"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r36 = r0
            r0 = r36
            java.lang.String r0 = r0.adServer     // Catch:{ Exception -> 0x07cf }
            r36 = r0
            r0 = r27
            r1 = r35
            r2 = r36
            r0.put(r1, r2)     // Catch:{ Exception -> 0x07cf }
            java.lang.String r35 = "AnalyticsServer"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r36 = r0
            r0 = r36
            java.lang.String r0 = r0.analyticsServer     // Catch:{ Exception -> 0x07cf }
            r36 = r0
            r0 = r27
            r1 = r35
            r2 = r36
            r0.put(r1, r2)     // Catch:{ Exception -> 0x07cf }
            java.lang.String r35 = "VcServer"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r36 = r0
            r0 = r36
            java.lang.String r0 = r0.vcServer     // Catch:{ Exception -> 0x07cf }
            r36 = r0
            r0 = r27
            r1 = r35
            r2 = r36
            r0.put(r1, r2)     // Catch:{ Exception -> 0x07cf }
            java.lang.String r35 = "FeedbackServer"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r36 = r0
            r0 = r36
            java.lang.String r0 = r0.feedbackServer     // Catch:{ Exception -> 0x07cf }
            r36 = r0
            r0 = r27
            r1 = r35
            r2 = r36
            r0.put(r1, r2)     // Catch:{ Exception -> 0x07cf }
            java.lang.String r35 = "passiveSessionTimeout"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r36 = r0
            r0 = r36
            int r0 = r0.passiveSessionTimeout     // Catch:{ Exception -> 0x07cf }
            r36 = r0
            java.lang.String r36 = java.lang.Integer.toString(r36)     // Catch:{ Exception -> 0x07cf }
            r0 = r27
            r1 = r35
            r2 = r36
            r0.put(r1, r2)     // Catch:{ Exception -> 0x07cf }
            java.lang.String r35 = "ad_units"
            r0 = r12
            r1 = r35
            org.json.JSONArray r5 = r0.getJSONArray(r1)     // Catch:{ Exception -> 0x07cf }
            r22 = 0
        L_0x05dc:
            int r35 = r5.length()     // Catch:{ Exception -> 0x0857 }
            r0 = r22
            r1 = r35
            if (r0 < r1) goto L_0x0718
        L_0x05e6:
            java.lang.String r35 = "debug_config"
            r0 = r12
            r1 = r35
            org.json.JSONObject r14 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x07b1 }
            java.util.ArrayList r16 = new java.util.ArrayList     // Catch:{ Exception -> 0x07b1 }
            r16.<init>()     // Catch:{ Exception -> 0x07b1 }
            java.lang.String[] r35 = com.mobclix.android.sdk.MobclixInstrumentation.MC_DEBUG_CATS     // Catch:{ Exception -> 0x07b1 }
            r0 = r35
            int r0 = r0.length     // Catch:{ Exception -> 0x07b1 }
            r36 = r0
            r37 = 0
        L_0x05fd:
            r0 = r37
            r1 = r36
            if (r0 < r1) goto L_0x073f
            com.mobclix.android.sdk.Mobclix.removePref(r16)     // Catch:{ Exception -> 0x07b1 }
            java.util.Iterator r13 = r14.keys()     // Catch:{ Exception -> 0x07b1 }
            java.util.HashMap r15 = new java.util.HashMap     // Catch:{ Exception -> 0x07b1 }
            r15.<init>()     // Catch:{ Exception -> 0x07b1 }
        L_0x060f:
            boolean r35 = r13.hasNext()     // Catch:{ Exception -> 0x07b1 }
            if (r35 != 0) goto L_0x075f
            com.mobclix.android.sdk.Mobclix.addPref(r15)     // Catch:{ Exception -> 0x07b1 }
        L_0x0618:
            java.lang.String r35 = "app_alerts"
            r0 = r12
            r1 = r35
            org.json.JSONArray r6 = r0.getJSONArray(r1)     // Catch:{ Exception -> 0x0854 }
        L_0x0621:
            java.lang.String r35 = "native_urls"
            r0 = r12
            r1 = r35
            org.json.JSONArray r25 = r0.getJSONArray(r1)     // Catch:{ Exception -> 0x0851 }
            r22 = 0
        L_0x062c:
            int r35 = r25.length()     // Catch:{ Exception -> 0x0851 }
            r0 = r22
            r1 = r35
            if (r0 < r1) goto L_0x07b4
        L_0x0636:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            r0 = r35
            java.lang.String r0 = r0.previousDeviceId     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            if (r35 == 0) goto L_0x065b
            java.lang.String r35 = "deviceId"
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r36 = r0
            r0 = r36
            java.lang.String r0 = r0.deviceId     // Catch:{ Exception -> 0x07cf }
            r36 = r0
            r0 = r27
            r1 = r35
            r2 = r36
            r0.put(r1, r2)     // Catch:{ Exception -> 0x07cf }
        L_0x065b:
            java.lang.String r35 = "offlineSessions"
            java.lang.String r36 = "0"
            r0 = r27
            r1 = r35
            r2 = r36
            r0.put(r1, r2)     // Catch:{ Exception -> 0x07cf }
            java.lang.String r35 = "totalSessionTime"
            java.lang.String r36 = "0"
            r0 = r27
            r1 = r35
            r2 = r36
            r0.put(r1, r2)     // Catch:{ Exception -> 0x07cf }
            java.lang.String r35 = "totalIdleTime"
            java.lang.String r36 = "0"
            r0 = r27
            r1 = r35
            r2 = r36
            r0.put(r1, r2)     // Catch:{ Exception -> 0x07cf }
            com.mobclix.android.sdk.Mobclix.addPref(r27)     // Catch:{ Exception -> 0x07cf }
            java.lang.String r35 = "MCReferralData"
            boolean r35 = com.mobclix.android.sdk.Mobclix.hasPref(r35)     // Catch:{ Exception -> 0x07cf }
            if (r35 == 0) goto L_0x0692
            java.lang.String r35 = "MCReferralData"
            com.mobclix.android.sdk.Mobclix.removePref(r35)     // Catch:{ Exception -> 0x07cf }
        L_0x0692:
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            r36 = 0
            r0 = r36
            r1 = r35
            r1.isOfflineSession = r0     // Catch:{ Exception -> 0x07cf }
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07cf }
            r35 = r0
            r36 = 1
            r0 = r36
            r1 = r35
            r1.remoteConfigSet = r0     // Catch:{ Exception -> 0x07cf }
        L_0x06ca:
            r8.close()     // Catch:{ Exception -> 0x0839 }
        L_0x06cd:
            if (r11 == 0) goto L_0x06d2
            r11.disconnect()
        L_0x06d2:
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)
            r0 = r40
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r35 = r0
            r0 = r35
            r1 = r23
            java.lang.String r23 = r0.benchmarkFinishPath(r1)
            r35 = 1
            r0 = r7
            r1 = r35
            if (r0 > r1) goto L_0x0156
            int r7 = r7 + 1
            goto L_0x0142
        L_0x06f9:
            r35 = 0
            goto L_0x035e
        L_0x06fd:
            java.lang.StringBuilder r35 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07e2 }
            java.lang.String r36 = java.lang.String.valueOf(r28)     // Catch:{ Exception -> 0x07e2 }
            r35.<init>(r36)     // Catch:{ Exception -> 0x07e2 }
            r0 = r35
            r1 = r31
            java.lang.StringBuilder r35 = r0.append(r1)     // Catch:{ Exception -> 0x07e2 }
            java.lang.String r28 = r35.toString()     // Catch:{ Exception -> 0x07e2 }
            java.lang.String r31 = r8.readLine()     // Catch:{ Exception -> 0x07e2 }
            goto L_0x03d0
        L_0x0718:
            r0 = r5
            r1 = r22
            org.json.JSONObject r4 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0857 }
            java.lang.String r35 = "size"
            r0 = r4
            r1 = r35
            java.lang.String r30 = r0.getString(r1)     // Catch:{ Exception -> 0x0857 }
            r0 = r40
            r1 = r4
            r0.processAdUnitConfig(r1)     // Catch:{ Exception -> 0x0857 }
            java.lang.String r35 = r4.toString()     // Catch:{ Exception -> 0x0857 }
            r0 = r27
            r1 = r30
            r2 = r35
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0857 }
            int r22 = r22 + 1
            goto L_0x05dc
        L_0x073f:
            r33 = r35[r37]     // Catch:{ Exception -> 0x07b1 }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07b1 }
            java.lang.String r39 = "debug_"
            r38.<init>(r39)     // Catch:{ Exception -> 0x07b1 }
            r0 = r38
            r1 = r33
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ Exception -> 0x07b1 }
            java.lang.String r38 = r38.toString()     // Catch:{ Exception -> 0x07b1 }
            r0 = r16
            r1 = r38
            r0.add(r1)     // Catch:{ Exception -> 0x07b1 }
            int r37 = r37 + 1
            goto L_0x05fd
        L_0x075f:
            java.lang.Object r24 = r13.next()     // Catch:{ Exception -> 0x0794 }
            java.lang.String r24 = (java.lang.String) r24     // Catch:{ Exception -> 0x0794 }
            r0 = r14
            r1 = r24
            java.lang.String r34 = r0.getString(r1)     // Catch:{ Exception -> 0x0794 }
            java.util.HashMap<java.lang.String, java.lang.String> r35 = com.mobclix.android.sdk.Mobclix.debugConfig     // Catch:{ Exception -> 0x0794 }
            r0 = r35
            r1 = r24
            r2 = r34
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0794 }
            java.lang.StringBuilder r35 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0794 }
            java.lang.String r36 = "debug_"
            r35.<init>(r36)     // Catch:{ Exception -> 0x0794 }
            r0 = r35
            r1 = r24
            java.lang.StringBuilder r35 = r0.append(r1)     // Catch:{ Exception -> 0x0794 }
            java.lang.String r35 = r35.toString()     // Catch:{ Exception -> 0x0794 }
            r0 = r15
            r1 = r35
            r2 = r34
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0794 }
            goto L_0x060f
        L_0x0794:
            r35 = move-exception
            r18 = r35
            java.lang.String r35 = com.mobclix.android.sdk.FetchRemoteConfig.TAG     // Catch:{ Exception -> 0x07b1 }
            java.lang.StringBuilder r36 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07b1 }
            java.lang.String r37 = "ERROR: "
            r36.<init>(r37)     // Catch:{ Exception -> 0x07b1 }
            java.lang.String r37 = r18.toString()     // Catch:{ Exception -> 0x07b1 }
            java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ Exception -> 0x07b1 }
            java.lang.String r36 = r36.toString()     // Catch:{ Exception -> 0x07b1 }
            android.util.Log.v(r35, r36)     // Catch:{ Exception -> 0x07b1 }
            goto L_0x060f
        L_0x07b1:
            r35 = move-exception
            goto L_0x0618
        L_0x07b4:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x0851 }
            r35 = r0
            r0 = r35
            java.util.List<java.lang.String> r0 = r0.nativeUrls     // Catch:{ Exception -> 0x0851 }
            r35 = r0
            r0 = r25
            r1 = r22
            java.lang.String r36 = r0.getString(r1)     // Catch:{ Exception -> 0x0851 }
            r35.add(r36)     // Catch:{ Exception -> 0x0851 }
            int r22 = r22 + 1
            goto L_0x062c
        L_0x07cf:
            r35 = move-exception
            r17 = r35
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x07e2 }
            r35 = r0
            r36 = -1
            r0 = r36
            r1 = r35
            r1.remoteConfigSet = r0     // Catch:{ Exception -> 0x07e2 }
            goto L_0x06ca
        L_0x07e2:
            r35 = move-exception
            r17 = r35
        L_0x07e5:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ all -> 0x084a }
            r35 = r0
            r36 = -1
            r0 = r36
            r1 = r35
            r1.remoteConfigSet = r0     // Catch:{ all -> 0x084a }
            r8.close()     // Catch:{ Exception -> 0x080e }
        L_0x07f6:
            if (r11 == 0) goto L_0x06d2
            r11.disconnect()
            goto L_0x06d2
        L_0x07fd:
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c     // Catch:{ Exception -> 0x084c, all -> 0x081e }
            r35 = r0
            r36 = -1
            r0 = r36
            r1 = r35
            r1.remoteConfigSet = r0     // Catch:{ Exception -> 0x084c, all -> 0x081e }
            r8 = r9
            goto L_0x06ca
        L_0x080e:
            r17 = move-exception
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r35 = r0
            r36 = -1
            r0 = r36
            r1 = r35
            r1.remoteConfigSet = r0
            goto L_0x07f6
        L_0x081e:
            r35 = move-exception
            r8 = r9
        L_0x0820:
            r8.close()     // Catch:{ Exception -> 0x0829 }
        L_0x0823:
            if (r11 == 0) goto L_0x0828
            r11.disconnect()
        L_0x0828:
            throw r35
        L_0x0829:
            r17 = move-exception
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r36 = r0
            r37 = -1
            r0 = r37
            r1 = r36
            r1.remoteConfigSet = r0
            goto L_0x0823
        L_0x0839:
            r17 = move-exception
            r0 = r40
            com.mobclix.android.sdk.Mobclix r0 = r0.c
            r35 = r0
            r36 = -1
            r0 = r36
            r1 = r35
            r1.remoteConfigSet = r0
            goto L_0x06cd
        L_0x084a:
            r35 = move-exception
            goto L_0x0820
        L_0x084c:
            r35 = move-exception
            r17 = r35
            r8 = r9
            goto L_0x07e5
        L_0x0851:
            r35 = move-exception
            goto L_0x0636
        L_0x0854:
            r35 = move-exception
            goto L_0x0621
        L_0x0857:
            r35 = move-exception
            goto L_0x05e6
        L_0x085a:
            r35 = move-exception
            goto L_0x052c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.FetchRemoteConfig.doInBackground(java.lang.String[]):org.json.JSONArray");
    }

    /* access modifiers changed from: package-private */
    public void processAdUnitConfig(JSONObject a) throws JSONException {
        String s = a.getString("size");
        MobclixAdUnitSettings settings = Mobclix.adUnitSettings.get(s);
        settings.setEnabled(a.getBoolean("enabled"));
        if (a.getLong("refresh") == -1) {
            settings.setRefreshTime(-1);
        } else {
            settings.setRefreshTime(a.getLong("refresh") * 1000);
        }
        settings.setAutoplay(a.getBoolean("autoplay"));
        if (a.getLong("autoplay_interval") == -1) {
            settings.setAutoplayInterval(-1);
        } else {
            settings.setAutoplayInterval(a.getLong("autoplay_interval") * 1000);
        }
        settings.setRichMediaRequireUser(a.getBoolean("rm_require_user"));
        try {
            JSONObject openAllocs = a.getJSONObject("open_allocs");
            String[] strArr = Mobclix.MC_OPEN_ALLOCATIONS;
            int length = strArr.length;
            for (int i = 0; i < length; i++) {
                String oType = strArr[i];
                JSONObject o = openAllocs.getJSONObject(oType);
                if (o != null) {
                    Iterator mKeys = o.keys();
                    HashMap<String, String> keys = new HashMap<>();
                    while (mKeys.hasNext()) {
                        try {
                            String key = mKeys.next();
                            String value = o.getString(key);
                            if (value != null && !value.equals("")) {
                                keys.put(key, value);
                            }
                        } catch (Exception e) {
                        }
                    }
                    settings.openAllocationSettings.put(oType, keys);
                } else {
                    settings.openAllocationSettings.put(oType, null);
                }
            }
        } catch (Exception e2) {
        }
        try {
            String c2 = a.getString("customAdUrl");
            if (c2.equals(settings.getCustomAdUrl())) {
                settings.setCustomAdUrl("");
                return;
            }
            Mobclix.removePref(String.valueOf(s) + "CustomAdUrl");
            settings.setCustomAdUrl(c2);
        } catch (Exception e3) {
            settings.setCustomAdUrl("");
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(JSONArray appAlerts) {
        if (appAlerts != null) {
            for (int i = 0; i < appAlerts.length(); i++) {
                try {
                    JSONObject a = appAlerts.getJSONObject(i);
                    String id = a.getString(TMXConstants.TAG_TILE_ATTRIBUTE_ID);
                    if (id != null && !id.equals("")) {
                        int timesShown = 0;
                        long lastTimeShown = 0;
                        String appAlertPref = Mobclix.getPref("MCAppAlert" + id);
                        if (!appAlertPref.equals("")) {
                            String[] appAlertPrefs = appAlertPref.split(",");
                            try {
                                timesShown = Integer.parseInt(appAlertPrefs[0]);
                            } catch (Exception e) {
                            }
                            try {
                                lastTimeShown = Long.parseLong(appAlertPrefs[1]);
                            } catch (Exception e2) {
                            }
                        }
                        String title = a.getString("title");
                        if (title != null && !title.equals("") && !title.equals("null")) {
                            String message = null;
                            try {
                                message = a.getString("message");
                                if (message == null || message.equals("") || message.equals("null")) {
                                    message = null;
                                }
                            } catch (Exception e3) {
                            }
                            int maxDisplays = 0;
                            try {
                                maxDisplays = a.getInt("max_displays");
                            } catch (Exception e4) {
                            }
                            if (maxDisplays == 0 || timesShown < maxDisplays) {
                                long displayInterval = 0;
                                try {
                                    displayInterval = ((long) a.getInt("display_interval")) * 1000;
                                } catch (Exception e5) {
                                }
                                if (displayInterval != 0) {
                                    if (lastTimeShown + displayInterval >= System.currentTimeMillis()) {
                                    }
                                }
                                JSONArray versions = a.getJSONArray("target_versions");
                                boolean targeted = false;
                                for (int j = 0; j < versions.length(); j++) {
                                    try {
                                        String v = versions.getString(j).split("\\*")[0];
                                        if (this.c.getApplicationVersion().substring(0, v.length()).equals(v)) {
                                            targeted = true;
                                        }
                                    } catch (Exception e6) {
                                    }
                                }
                                if (targeted) {
                                    String actionButton = null;
                                    try {
                                        actionButton = a.getString("action_button");
                                        if (actionButton == null || actionButton.equals("") || actionButton.equals("null")) {
                                            actionButton = null;
                                        }
                                    } catch (Exception e7) {
                                    }
                                    String actionUrl = null;
                                    try {
                                        actionUrl = a.getString("action_url");
                                        if (actionUrl == null || actionUrl.equals("") || actionUrl.equals("null")) {
                                            actionUrl = null;
                                        }
                                    } catch (Exception e8) {
                                    }
                                    if (actionUrl == null || actionButton != null) {
                                        String dismissButton = null;
                                        try {
                                            dismissButton = a.getString("dismiss_button");
                                            if (dismissButton == null || dismissButton.equals("") || dismissButton.equals("null")) {
                                                dismissButton = null;
                                            }
                                        } catch (Exception e9) {
                                        }
                                        if (actionUrl != null || dismissButton != null) {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(this.c.getContext());
                                            builder.setTitle(title);
                                            builder.setCancelable(false);
                                            if (message != null) {
                                                builder.setMessage(message);
                                            }
                                            if (actionUrl != null) {
                                                builder.setPositiveButton(actionButton, new Mobclix.ObjectOnClickListener(actionUrl) {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        FetchRemoteConfig.this.c.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse((String) this.obj1)));
                                                    }
                                                });
                                            }
                                            if (dismissButton != null) {
                                                builder.setNegativeButton(dismissButton, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });
                                            }
                                            builder.create().show();
                                            Mobclix.addPref("MCAppAlert" + id, String.valueOf(Integer.toString(timesShown + 1)) + "," + Long.toString(System.currentTimeMillis()));
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e10) {
                }
            }
        }
    }

    private String getConfigUrl(boolean usePref) {
        String configServer = this.c.configServer;
        StringBuffer data = new StringBuffer();
        try {
            if (Mobclix.hasPref("ConfigServer") && usePref) {
                configServer = Mobclix.getPref("ConfigServer");
            }
            data.append(configServer);
            data.append("?p=android");
            data.append("&rt=").append(URLEncoder.encode(this.c.getRuntimePlatform(), "UTF-8"));
            data.append("&rtv=").append(URLEncoder.encode(this.c.getRuntimePlatformVersion(), "UTF-8"));
            data.append("&a=").append(URLEncoder.encode(this.c.getApplicationId(), "UTF-8"));
            data.append("&m=").append(URLEncoder.encode(this.c.getMobclixVersion()));
            data.append("&v=").append(URLEncoder.encode(this.c.getApplicationVersion(), "UTF-8"));
            data.append("&d=").append(URLEncoder.encode(this.c.getDeviceId(), "UTF-8"));
            data.append("&dm=").append(URLEncoder.encode(this.c.getDeviceModel(), "UTF-8"));
            data.append("&dv=").append(URLEncoder.encode(this.c.getAndroidVersion(), "UTF-8"));
            data.append("&hwdm=").append(URLEncoder.encode(this.c.getDeviceHardwareModel(), "UTF-8"));
            data.append("&g=").append(URLEncoder.encode(this.c.getConnectionType(), "UTF-8"));
            if (!this.c.getGPS().equals("null")) {
                data.append("&ll=").append(URLEncoder.encode(this.c.getGPS(), "UTF-8"));
            }
            if (Mobclix.hasPref("offlineSessions")) {
                try {
                    data.append("&off=").append(Mobclix.getPref("offlineSessions"));
                } catch (Exception e) {
                }
            }
            if (Mobclix.hasPref("totalSessionTime")) {
                try {
                    data.append("&st=").append(Mobclix.getPref("totalSessionTime"));
                } catch (Exception e2) {
                }
            }
            data.append("&it=0");
            if (this.c.previousDeviceId != null) {
                data.append("&pd=").append(URLEncoder.encode(this.c.previousDeviceId, "UTF-8"));
            }
            data.append("&mcc=").append(URLEncoder.encode(this.c.getMcc(), "UTF-8"));
            data.append("&mnc=").append(URLEncoder.encode(this.c.getMnc(), "UTF-8"));
            if (this.c.isNewUser) {
                data.append("&new=true");
            }
            try {
                if (Mobclix.hasPref("MCReferralData")) {
                    String referral = Mobclix.getPref("MCReferralData");
                    if (!referral.equals("")) {
                        data.append("&r=").append(referral);
                    }
                }
            } catch (Exception e3) {
            }
            return data.toString();
        } catch (Exception e4) {
            return "";
        }
    }

    private void downloadCustomImages() {
        for (String s : Mobclix.MC_AD_SIZES) {
            MobclixAdUnitSettings settings = Mobclix.adUnitSettings.get(s);
            String customAdUrl = settings.getCustomAdUrl();
            if (!customAdUrl.equals("")) {
                try {
                    DefaultHttpClient httpClient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet(customAdUrl);
                    httpGet.setHeader("Cookie", Mobclix.getCookieStringFromCookieManager(customAdUrl));
                    HttpEntity httpEntity = httpClient.execute(httpGet).getEntity();
                    Mobclix.syncCookiesToCookieManager(httpClient.getCookieStore(), customAdUrl);
                    Bitmap bmImg = BitmapFactory.decodeStream(httpEntity.getContent());
                    FileOutputStream fos = this.c.getContext().openFileOutput(String.valueOf(s) + "_mc_cached_custom_ad.png", 0);
                    bmImg.compress(Bitmap.CompressFormat.PNG, 90, fos);
                    fos.close();
                    Mobclix.addPref(String.valueOf(s) + "CustomAdUrl", settings.getCustomAdUrl());
                    settings.setCustomAdSet(true);
                } catch (Exception e) {
                }
            } else {
                settings.setCustomAdSet(true);
            }
        }
    }
}
