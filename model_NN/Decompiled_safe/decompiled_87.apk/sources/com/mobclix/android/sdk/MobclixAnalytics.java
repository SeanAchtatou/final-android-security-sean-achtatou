package com.mobclix.android.sdk;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URLEncoder;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.json.JSONObject;

public class MobclixAnalytics {
    /* access modifiers changed from: private */
    public static String MC_ANALYTICS_DIRECTORY = "analytics";
    /* access modifiers changed from: private */
    public static String MC_DIRECTORY = "mobclix";
    private static final String MC_KEY_APPLICATION_ID = "a";
    private static final String MC_KEY_APPLICATION_VERSION = "v";
    private static final String MC_KEY_DEVICE_HARDWARE_MODEL = "hwdm";
    private static final String MC_KEY_DEVICE_ID = "d";
    private static final String MC_KEY_DEVICE_MODEL = "dm";
    private static final String MC_KEY_DEVICE_SYSTEM_VERSION = "dv";
    private static final String MC_KEY_MOBCLIX_LIBRARY_VERSION = "m";
    private static final String MC_KEY_PLATFORM_ID = "p";
    private static final String MC_KEY_USER_LANGUAGE_PREFERENCE = "lg";
    private static final String MC_KEY_USER_LOCALE_PREFERENCE = "lo";
    private static int MC_MAX_ANALYTICS_FILES = 100;
    /* access modifiers changed from: private */
    public static int MC_MAX_EVENTS_PER_FILE = 5;
    static int SYNC_ERROR = -1;
    static int SYNC_READY = 0;
    static int SYNC_RUNNING = 1;
    private static final String TAG = "MobclixAnalytics";
    /* access modifiers changed from: private */
    public static Mobclix controller = Mobclix.getInstance();
    /* access modifiers changed from: private */
    public static File currentFile = null;
    /* access modifiers changed from: private */
    public static boolean fileCreated = false;
    /* access modifiers changed from: private */
    public static boolean loggingEvent = false;
    /* access modifiers changed from: private */
    public static int numLinesWritten = 0;
    /* access modifiers changed from: private */
    public static String syncContents = null;
    /* access modifiers changed from: private */
    public static int syncStatus = 0;

    static int getSyncStatus() {
        return syncStatus;
    }

    static class LogEvent implements Runnable {
        JSONObject event;

        public LogEvent(JSONObject e) {
            this.event = e;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
        public void run() {
            while (true) {
                try {
                    if (!MobclixAnalytics.loggingEvent && MobclixAnalytics.syncStatus == MobclixAnalytics.SYNC_READY) {
                        break;
                    }
                } catch (Exception e) {
                    MobclixAnalytics.loggingEvent = false;
                    return;
                }
            }
            MobclixAnalytics.loggingEvent = true;
            if (MobclixAnalytics.fileCreated || MobclixAnalytics.OpenAnalyticsFile()) {
                do {
                } while (!MobclixAnalytics.fileCreated);
                MobclixAnalytics.controller.updateSession();
                try {
                    FileOutputStream fos = new FileOutputStream(MobclixAnalytics.currentFile, true);
                    if (MobclixAnalytics.numLinesWritten > 1) {
                        fos.write(",".getBytes());
                    }
                    fos.write(this.event.toString().getBytes());
                    fos.close();
                    MobclixAnalytics.numLinesWritten = MobclixAnalytics.numLinesWritten + 1;
                    if (MobclixAnalytics.numLinesWritten > MobclixAnalytics.MC_MAX_EVENTS_PER_FILE) {
                        MobclixAnalytics.fileCreated = false;
                        boolean unused = MobclixAnalytics.OpenAnalyticsFile();
                    }
                } catch (Exception e2) {
                }
                MobclixAnalytics.loggingEvent = false;
                return;
            }
            MobclixAnalytics.loggingEvent = false;
        }
    }

    /* access modifiers changed from: private */
    public static boolean OpenAnalyticsFile() {
        numLinesWritten = 1;
        controller.updateSession();
        try {
            JSONObject header = new JSONObject(controller.session, new String[]{"ll", "g", TMXConstants.TAG_TILE_ATTRIBUTE_ID});
            header.put(MC_KEY_APPLICATION_ID, URLEncoder.encode(controller.getApplicationId(), "UTF-8"));
            header.put(MC_KEY_PLATFORM_ID, URLEncoder.encode(controller.getPlatform(), "UTF-8"));
            header.put("m", URLEncoder.encode(controller.getMobclixVersion()));
            header.put(MC_KEY_APPLICATION_VERSION, URLEncoder.encode(controller.getApplicationVersion(), "UTF-8"));
            header.put(MC_KEY_DEVICE_ID, URLEncoder.encode(controller.getDeviceId(), "UTF-8"));
            header.put(MC_KEY_DEVICE_MODEL, URLEncoder.encode(controller.getDeviceModel(), "UTF-8"));
            header.put(MC_KEY_DEVICE_SYSTEM_VERSION, URLEncoder.encode(controller.getAndroidVersion(), "UTF-8"));
            header.put(MC_KEY_DEVICE_HARDWARE_MODEL, URLEncoder.encode(controller.getDeviceHardwareModel(), "UTF-8"));
            header.put("m", URLEncoder.encode(Mobclix.MC_LIBRARY_VERSION, "UTF-8"));
            header.put(MC_KEY_USER_LANGUAGE_PREFERENCE, URLEncoder.encode(controller.getLanguage(), "UTF-8"));
            header.put(MC_KEY_USER_LOCALE_PREFERENCE, URLEncoder.encode(controller.getLocale(), "UTF-8"));
            File anDir = new File(String.valueOf(controller.getContext().getDir(MC_DIRECTORY, 0).getAbsolutePath()) + "/" + MC_ANALYTICS_DIRECTORY);
            anDir.mkdir();
            try {
                if (anDir.listFiles().length >= MC_MAX_ANALYTICS_FILES) {
                    return false;
                }
            } catch (Exception e) {
            }
            currentFile = new File(anDir.getAbsoluteFile() + "/" + System.currentTimeMillis() + ".log");
            currentFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(currentFile);
            fos.write("[{\"hb\":".getBytes());
            fos.write(header.toString().getBytes());
            fos.write(",\"ev\":[".getBytes());
            fos.close();
            fileCreated = true;
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    static class Sync implements Runnable {
        Sync() {
        }

        /* JADX WARNING: Removed duplicated region for block: B:49:0x01cf A[Catch:{ Exception -> 0x01a7 }] */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x01a0 A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public synchronized void run() {
            /*
                r22 = this;
                monitor-enter(r22)
            L_0x0001:
                boolean r19 = com.mobclix.android.sdk.MobclixAnalytics.loggingEvent     // Catch:{ all -> 0x01b6 }
                if (r19 != 0) goto L_0x0001
                int r19 = com.mobclix.android.sdk.MobclixAnalytics.SYNC_RUNNING     // Catch:{ all -> 0x01b6 }
                com.mobclix.android.sdk.MobclixAnalytics.syncStatus = r19     // Catch:{ all -> 0x01b6 }
                com.mobclix.android.sdk.Mobclix r19 = com.mobclix.android.sdk.MobclixAnalytics.controller     // Catch:{ Exception -> 0x01a7 }
                android.content.Context r19 = r19.getContext()     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r20 = com.mobclix.android.sdk.MobclixAnalytics.MC_DIRECTORY     // Catch:{ Exception -> 0x01a7 }
                r21 = 0
                java.io.File r12 = r19.getDir(r20, r21)     // Catch:{ Exception -> 0x01a7 }
                java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x01a7 }
                java.lang.StringBuilder r19 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r20 = r12.getAbsolutePath()     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r20 = java.lang.String.valueOf(r20)     // Catch:{ Exception -> 0x01a7 }
                r19.<init>(r20)     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r20 = "/"
                java.lang.StringBuilder r19 = r19.append(r20)     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r20 = com.mobclix.android.sdk.MobclixAnalytics.MC_ANALYTICS_DIRECTORY     // Catch:{ Exception -> 0x01a7 }
                java.lang.StringBuilder r19 = r19.append(r20)     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r19 = r19.toString()     // Catch:{ Exception -> 0x01a7 }
                r0 = r2
                r1 = r19
                r0.<init>(r1)     // Catch:{ Exception -> 0x01a7 }
                r2.mkdir()     // Catch:{ Exception -> 0x01a7 }
                java.io.File[] r8 = r2.listFiles()     // Catch:{ Exception -> 0x01a7 }
                r10 = 0
            L_0x004d:
                r0 = r8
                int r0 = r0.length     // Catch:{ Exception -> 0x01a7 }
                r19 = r0
                r0 = r10
                r1 = r19
                if (r0 < r1) goto L_0x006c
            L_0x0056:
                r19 = 0
                com.mobclix.android.sdk.MobclixAnalytics.syncContents = r19     // Catch:{ all -> 0x01b6 }
                r19 = 0
                com.mobclix.android.sdk.MobclixAnalytics.numLinesWritten = r19     // Catch:{ all -> 0x01b6 }
                r19 = 0
                com.mobclix.android.sdk.MobclixAnalytics.fileCreated = r19     // Catch:{ all -> 0x01b6 }
                int r19 = com.mobclix.android.sdk.MobclixAnalytics.SYNC_READY     // Catch:{ all -> 0x01b6 }
                com.mobclix.android.sdk.MobclixAnalytics.syncStatus = r19     // Catch:{ all -> 0x01b6 }
            L_0x006a:
                monitor-exit(r22)
                return
            L_0x006c:
                java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x01a7 }
                r5.<init>()     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r19 = "p=android"
                r0 = r5
                r1 = r19
                r0.append(r1)     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r19 = "&a="
                r0 = r5
                r1 = r19
                java.lang.StringBuffer r19 = r0.append(r1)     // Catch:{ Exception -> 0x01a7 }
                com.mobclix.android.sdk.Mobclix r20 = com.mobclix.android.sdk.MobclixAnalytics.controller     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r20 = r20.getApplicationId()     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r21 = "UTF-8"
                java.lang.String r20 = java.net.URLEncoder.encode(r20, r21)     // Catch:{ Exception -> 0x01a7 }
                r19.append(r20)     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r19 = "&m="
                r0 = r5
                r1 = r19
                java.lang.StringBuffer r19 = r0.append(r1)     // Catch:{ Exception -> 0x01a7 }
                com.mobclix.android.sdk.Mobclix r20 = com.mobclix.android.sdk.MobclixAnalytics.controller     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r20 = r20.getMobclixVersion()     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r20 = java.net.URLEncoder.encode(r20)     // Catch:{ Exception -> 0x01a7 }
                r19.append(r20)     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r19 = "&d="
                r0 = r5
                r1 = r19
                java.lang.StringBuffer r19 = r0.append(r1)     // Catch:{ Exception -> 0x01a7 }
                com.mobclix.android.sdk.Mobclix r20 = com.mobclix.android.sdk.MobclixAnalytics.controller     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r20 = r20.getDeviceId()     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r21 = "UTF-8"
                java.lang.String r20 = java.net.URLEncoder.encode(r20, r21)     // Catch:{ Exception -> 0x01a7 }
                r19.append(r20)     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r19 = "&v="
                r0 = r5
                r1 = r19
                java.lang.StringBuffer r19 = r0.append(r1)     // Catch:{ Exception -> 0x01a7 }
                com.mobclix.android.sdk.Mobclix r20 = com.mobclix.android.sdk.MobclixAnalytics.controller     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r20 = r20.getApplicationVersion()     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r21 = "UTF-8"
                java.lang.String r20 = java.net.URLEncoder.encode(r20, r21)     // Catch:{ Exception -> 0x01a7 }
                r19.append(r20)     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r19 = "&j="
                r0 = r5
                r1 = r19
                r0.append(r1)     // Catch:{ Exception -> 0x01a7 }
                java.io.FileInputStream r9 = new java.io.FileInputStream     // Catch:{ Exception -> 0x01a7 }
                r19 = r8[r10]     // Catch:{ Exception -> 0x01a7 }
                r0 = r9
                r1 = r19
                r0.<init>(r1)     // Catch:{ Exception -> 0x01a7 }
                java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x01a7 }
                r3.<init>(r9)     // Catch:{ Exception -> 0x01a7 }
                java.io.DataInputStream r6 = new java.io.DataInputStream     // Catch:{ Exception -> 0x01a7 }
                r6.<init>(r3)     // Catch:{ Exception -> 0x01a7 }
            L_0x00fb:
                int r19 = r6.available()     // Catch:{ Exception -> 0x01a7 }
                if (r19 != 0) goto L_0x01aa
                r6.close()     // Catch:{ Exception -> 0x01a7 }
                r3.close()     // Catch:{ Exception -> 0x01a7 }
                r9.close()     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r19 = "]}]"
                r0 = r5
                r1 = r19
                r0.append(r1)     // Catch:{ Exception -> 0x01a7 }
                java.lang.String r19 = r5.toString()     // Catch:{ Exception -> 0x01a7 }
                com.mobclix.android.sdk.MobclixAnalytics.syncContents = r19     // Catch:{ Exception -> 0x01a7 }
                r17 = 0
                r4 = 0
                java.net.URL r18 = new java.net.URL     // Catch:{ Exception -> 0x01d8 }
                com.mobclix.android.sdk.Mobclix r19 = com.mobclix.android.sdk.MobclixAnalytics.controller     // Catch:{ Exception -> 0x01d8 }
                r0 = r19
                java.lang.String r0 = r0.analyticsServer     // Catch:{ Exception -> 0x01d8 }
                r19 = r0
                r18.<init>(r19)     // Catch:{ Exception -> 0x01d8 }
                java.net.URLConnection r5 = r18.openConnection()     // Catch:{ Exception -> 0x01c4 }
                r0 = r5
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x01c4 }
                r4 = r0
                r19 = 1
                r0 = r4
                r1 = r19
                r0.setDoOutput(r1)     // Catch:{ Exception -> 0x01c4 }
                r19 = 1
                r0 = r4
                r1 = r19
                r0.setDoInput(r1)     // Catch:{ Exception -> 0x01c4 }
                r19 = 0
                r0 = r4
                r1 = r19
                r0.setUseCaches(r1)     // Catch:{ Exception -> 0x01c4 }
                java.lang.String r19 = "POST"
                r0 = r4
                r1 = r19
                r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x01c4 }
                java.io.OutputStream r13 = r4.getOutputStream()     // Catch:{ Exception -> 0x01c4 }
                java.io.PrintWriter r14 = new java.io.PrintWriter     // Catch:{ Exception -> 0x01c4 }
                r14.<init>(r13)     // Catch:{ Exception -> 0x01c4 }
                java.lang.String r19 = com.mobclix.android.sdk.MobclixAnalytics.syncContents     // Catch:{ Exception -> 0x01c4 }
                r0 = r14
                r1 = r19
                r0.print(r1)     // Catch:{ Exception -> 0x01c4 }
                r14.flush()     // Catch:{ Exception -> 0x01c4 }
                r14.close()     // Catch:{ Exception -> 0x01c4 }
                java.io.BufferedReader r15 = new java.io.BufferedReader     // Catch:{ Exception -> 0x01c4 }
                java.io.InputStreamReader r19 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x01c4 }
                java.io.InputStream r20 = r4.getInputStream()     // Catch:{ Exception -> 0x01c4 }
                r19.<init>(r20)     // Catch:{ Exception -> 0x01c4 }
                r0 = r15
                r1 = r19
                r0.<init>(r1)     // Catch:{ Exception -> 0x01c4 }
                r11 = 0
            L_0x017e:
                java.lang.String r16 = r15.readLine()     // Catch:{ Exception -> 0x01c4 }
                if (r16 != 0) goto L_0x01b9
                java.lang.String r19 = "1"
                r0 = r11
                r1 = r19
                boolean r19 = r0.equals(r1)     // Catch:{ Exception -> 0x01c4 }
                if (r19 == 0) goto L_0x01be
            L_0x018f:
                r15.close()     // Catch:{ Exception -> 0x01c4 }
                r17 = r18
            L_0x0194:
                int r19 = com.mobclix.android.sdk.MobclixAnalytics.syncStatus     // Catch:{ Exception -> 0x01a7 }
                int r20 = com.mobclix.android.sdk.MobclixAnalytics.SYNC_ERROR     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                r1 = r20
                if (r0 != r1) goto L_0x01cf
                int r19 = com.mobclix.android.sdk.MobclixAnalytics.SYNC_READY     // Catch:{ Exception -> 0x01a7 }
                com.mobclix.android.sdk.MobclixAnalytics.syncStatus = r19     // Catch:{ Exception -> 0x01a7 }
                goto L_0x006a
            L_0x01a7:
                r19 = move-exception
                goto L_0x0056
            L_0x01aa:
                java.lang.String r19 = r6.readLine()     // Catch:{ Exception -> 0x01a7 }
                r0 = r5
                r1 = r19
                r0.append(r1)     // Catch:{ Exception -> 0x01a7 }
                goto L_0x00fb
            L_0x01b6:
                r19 = move-exception
                monitor-exit(r22)
                throw r19
            L_0x01b9:
                if (r11 != 0) goto L_0x017e
                r11 = r16
                goto L_0x017e
            L_0x01be:
                int r19 = com.mobclix.android.sdk.MobclixAnalytics.SYNC_ERROR     // Catch:{ Exception -> 0x01c4 }
                com.mobclix.android.sdk.MobclixAnalytics.syncStatus = r19     // Catch:{ Exception -> 0x01c4 }
                goto L_0x018f
            L_0x01c4:
                r19 = move-exception
                r7 = r19
                r17 = r18
            L_0x01c9:
                int r19 = com.mobclix.android.sdk.MobclixAnalytics.SYNC_ERROR     // Catch:{ Exception -> 0x01a7 }
                com.mobclix.android.sdk.MobclixAnalytics.syncStatus = r19     // Catch:{ Exception -> 0x01a7 }
                goto L_0x0194
            L_0x01cf:
                r19 = r8[r10]     // Catch:{ Exception -> 0x01a7 }
                r19.delete()     // Catch:{ Exception -> 0x01a7 }
                int r10 = r10 + 1
                goto L_0x004d
            L_0x01d8:
                r19 = move-exception
                r7 = r19
                goto L_0x01c9
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixAnalytics.Sync.run():void");
        }
    }
}
