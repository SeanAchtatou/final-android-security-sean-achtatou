package ru.aeradeve.utils.rating.view;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.List;
import ru.aeradeve.games.gravityclumps2.R;
import ru.aeradeve.utils.rating.entity.FilterRating;
import ru.aeradeve.utils.rating.entity.ScoreEntity;

public class SwitcherTableRatingView extends LinearLayout {
    private LinearLayout mEmpty;
    /* access modifiers changed from: private */
    public FilterRating mFilter;
    private LinearLayout mRetry;
    private TableRatingView mTable = new TableRatingView(getContext(), this.mFilter);

    public SwitcherTableRatingView(Context context, FilterRating pFilter) {
        super(context);
        this.mFilter = pFilter;
        showLoading();
        this.mRetry = new LinearLayout(getContext());
        this.mRetry.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.mRetry.setGravity(17);
        this.mRetry.setOrientation(1);
        TextView textRetry = new TextView(getContext());
        textRetry.setGravity(17);
        textRetry.setText((int) R.string.ratingDialogRatingLoadedWithError);
        textRetry.setTextColor(-1);
        this.mRetry.addView(textRetry);
        Button button = new Button(getContext());
        button.setText((int) R.string.ratingButtonRetry);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SwitcherTableRatingView.this.mFilter.setPeriod(SwitcherTableRatingView.this.mFilter.getPeriod());
            }
        });
        this.mRetry.addView(button);
        this.mEmpty = new LinearLayout(getContext());
        this.mEmpty.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.mEmpty.setGravity(17);
        TextView textEmpty = new TextView(getContext());
        textEmpty.setText((int) R.string.ratingDialogRatingEmpty);
        textEmpty.setTextColor(-1);
        this.mEmpty.addView(textEmpty);
    }

    private void show(View pView) {
        removeAllViews();
        addView(pView);
    }

    public void showTable(List<ScoreEntity> scores) {
        this.mTable.fillTable(scores);
        show(this.mTable);
    }

    public void showLoading() {
        LinearLayout pLoading = new LinearLayout(getContext());
        pLoading.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        pLoading.setGravity(17);
        ProgressBar pb = new ProgressBar(getContext());
        pb.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        pLoading.addView(pb);
        show(pLoading);
    }

    public void showRetry() {
        show(this.mRetry);
    }

    public void showEmpty() {
        show(this.mEmpty);
    }
}
