package ru.aeradeve.utils.rating.utils;

import android.content.Context;
import java.io.PrintStream;
import java.util.Scanner;
import ru.aeradeve.utils.rating.entity.FilterRating;

public interface IRatingInfoGame {
    Context getContext();

    int getGameId();

    String getNickName();

    String getParams4URL(FilterRating filterRating);

    String getSCode();

    String getURL();

    boolean isNeedSendScore();

    boolean isNickNoname();

    void load();

    void onLoad(Scanner scanner);

    void onSave(PrintStream printStream);

    void resetScore4Send();

    void save();

    void setNickName(String str);

    void setScore4Send(long j);
}
