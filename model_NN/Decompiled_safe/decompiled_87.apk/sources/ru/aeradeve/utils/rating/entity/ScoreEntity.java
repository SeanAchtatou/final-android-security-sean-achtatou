package ru.aeradeve.utils.rating.entity;

import java.io.Serializable;

public class ScoreEntity implements Serializable {
    private String country;
    private String flagIso;
    private String name;
    private int rank;
    private String score;

    public int getRank() {
        return this.rank;
    }

    public void setRank(int rank2) {
        this.rank = rank2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getFlagIso() {
        return this.flagIso;
    }

    public void setFlagIso(String flagIso2) {
        if (flagIso2 == null || flagIso2.length() < 1) {
            flagIso2 = "pirate";
        }
        this.flagIso = flagIso2;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country2) {
        if (country2 == null || country2.trim().length() < 1) {
            country2 = "Yo-ho-ho!";
        }
        this.country = country2;
    }

    public String getScore() {
        return this.score;
    }

    public void setScore(String score2) {
        this.score = score2;
    }
}
