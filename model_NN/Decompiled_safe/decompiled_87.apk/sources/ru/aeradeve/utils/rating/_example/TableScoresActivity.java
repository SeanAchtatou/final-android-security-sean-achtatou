package ru.aeradeve.utils.rating._example;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import ru.aeradeve.utils.ad.EmptyAdvertising;
import ru.aeradeve.utils.rating.view.RatingView;

public class TableScoresActivity extends Activity {
    private InfoGame mInfoGame = null;

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(savedInstanceState);
        this.mInfoGame = new InfoGame(this);
        LinearLayout mainLinearLayout = new LinearLayout(this);
        mainLinearLayout.setOrientation(1);
        mainLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        View adView = new EmptyAdvertising(this);
        adView.setLayoutParams(new FrameLayout.LayoutParams(-1, 50));
        mainLinearLayout.addView(adView);
        RatingView rating = new RatingView(this);
        mainLinearLayout.addView(rating);
        setContentView(mainLinearLayout);
        rating.start(this.mInfoGame);
    }
}
