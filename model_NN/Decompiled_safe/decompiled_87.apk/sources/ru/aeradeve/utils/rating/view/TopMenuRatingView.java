package ru.aeradeve.utils.rating.view;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import ru.aeradeve.games.gravityclumps2.R;
import ru.aeradeve.utils.Util;
import ru.aeradeve.utils.rating.entity.FilterRating;
import ru.aeradeve.utils.rating.widget.SButton;

public class TopMenuRatingView extends LinearLayout {
    private SButton mAllPeriod;
    /* access modifiers changed from: private */
    public FilterRating mFilter;
    private SButton mMonthPeriod;
    private SButton mPlayerScore;
    private SButton mTopScores;
    private SButton mWeekPeriod;

    public TopMenuRatingView(Context context, FilterRating pFilter) {
        super(context);
        this.mFilter = pFilter;
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(-1, (int) TypedValue.applyDimension(1, 33.0f, Util.getDisplayMetrics(context)));
        setOrientation(0);
        setLayoutParams(lp);
        this.mAllPeriod = new SButton(context, R.string.ratingTabPeriodAll, R.drawable.btn_n_d_right, R.drawable.btn_f_d_right, -1);
        this.mMonthPeriod = new SButton(context, R.string.ratingTabPeriodMonth, R.drawable.btn_n_d_right, R.drawable.btn_f_d_right, -1);
        this.mWeekPeriod = new SButton(context, R.string.ratingTabPeriodWeek, R.drawable.btn_n_rd, R.drawable.btn_f_rd, -1);
        this.mTopScores = new SButton(context, R.string.ratingTabFragmentTop100, R.drawable.btn_n_ld, R.drawable.btn_f_ld, -1);
        this.mPlayerScore = new SButton(context, R.string.ratingTabFragmentYourScore, R.drawable.btn_n_d_left, R.drawable.btn_f_d_left, -1);
        addView(this.mAllPeriod);
        addView(this.mMonthPeriod);
        addView(this.mWeekPeriod);
        View empty = new View(context);
        empty.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 1.0f));
        addView(empty);
        addView(this.mTopScores);
        addView(this.mPlayerScore);
        refresh();
        this.mFilter.addListener(new Runnable() {
            public void run() {
                TopMenuRatingView.this.refresh();
            }
        });
        this.mAllPeriod.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                TopMenuRatingView.this.mFilter.setPeriod(0);
            }
        });
        this.mMonthPeriod.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                TopMenuRatingView.this.mFilter.setPeriod(2);
            }
        });
        this.mWeekPeriod.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                TopMenuRatingView.this.mFilter.setPeriod(1);
            }
        });
        this.mTopScores.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                TopMenuRatingView.this.mFilter.setFragment(1);
            }
        });
        this.mPlayerScore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                TopMenuRatingView.this.mFilter.setFragment(2);
            }
        });
    }

    /* access modifiers changed from: private */
    public void refresh() {
        this.mAllPeriod.unselect();
        this.mMonthPeriod.unselect();
        this.mWeekPeriod.unselect();
        if (this.mFilter.getPeriod() == 0) {
            this.mAllPeriod.select();
        }
        if (this.mFilter.getPeriod() == 2) {
            this.mMonthPeriod.select();
        }
        if (this.mFilter.getPeriod() == 1) {
            this.mWeekPeriod.select();
        }
        this.mTopScores.unselect();
        this.mPlayerScore.unselect();
        if (this.mFilter.getFragment() == 1) {
            this.mTopScores.select();
        }
        if (this.mFilter.getFragment() == 2) {
            this.mPlayerScore.select();
        }
        if (this.mFilter.getType() == 1) {
        }
    }
}
