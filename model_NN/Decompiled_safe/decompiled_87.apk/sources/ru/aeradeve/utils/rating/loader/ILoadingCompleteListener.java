package ru.aeradeve.utils.rating.loader;

import java.util.List;
import ru.aeradeve.utils.rating.entity.ScoreEntity;

public interface ILoadingCompleteListener {
    void onError();

    void onSuccessful(List<ScoreEntity> list);
}
