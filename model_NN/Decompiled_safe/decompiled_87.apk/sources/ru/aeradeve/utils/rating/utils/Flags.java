package ru.aeradeve.utils.rating.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import java.util.HashMap;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import ru.aeradeve.games.gravityclumps2.R;
import ru.aeradeve.utils.Util;

public class Flags {
    public static HashMap<String, Bitmap> flags = null;
    public static String[] flagsIso = {"ad", "ae", "af", "ag", "ai", "al", "am", "an", "ao", "ar", "as", "at", "au", "aw", "ax", "az", "ba", "bb", "bd", "be", "bf", "bg", "bh", "bi", "bj", "bm", "bn", "bo", "br", "bs", "bt", "bv", "bw", "by", "bz", "ca", "cc", "cd", "cf", "cg", "ch", "ci", "ck", "cl", "cm", "cn", "co", "cr", "cs", "cu", "cv", "cx", "cy", "cz", "de", "dj", "dk", "dm", "do", "dz", "ec", "ee", "eg", "eh", "er", "es", "et", "fi", "fj", "fk", "fm", "fo", "fr", "ga", "gb", "gd", "ge", "gf", "gh", "gi", "gl", "gm", "gn", "gp", "gq", "gr", "gs", "gt", "gu", "gw", "gy", "hk", "hm", "hn", "hr", "ht", "hu", TMXConstants.TAG_TILE_ATTRIBUTE_ID, "ie", "il", "in", "io", "iq", "ir", "is", "it", "jm", "jo", "jp", "ke", "kg", "kh", "ki", "km", "kn", "kp", "kr", "kw", "ky", "kz", "la", "lb", "lc", "li", "lk", "lr", "ls", "lt", "lu", "lv", "ly", "ma", "mc", "md", "me", "mg", "mh", "mk", "ml", "mm", "mn", "mo", "mp", "mq", "mr", "ms", "mt", "mu", "mv", "mw", "mx", "my", "mz", "na", "nc", "ne", "nf", "ng", "ni", "nl", "no", "np", "nr", "nu", "nz", "om", "pa", "pe", "pf", "pg", "ph", "pk", "pl", "pm", "pn", "pr", "ps", "pt", "pw", "py", "qa", "re", "ro", "rs", "ru", "rw", "sa", "sb", "sc", "sd", "se", "sg", "sh", "si", "sj", "sk", "sl", "sm", "sn", "so", "sr", "st", "sv", "sy", "sz", "tc", "td", "tf", "tg", "th", "tj", "tk", "tl", "tm", "tn", "to", "tr", "tt", "tv", "tw", "tz", "ua", "ug", "um", "us", "uy", "uz", "va", "vc", "ve", "vg", "vi", "vn", "vu", "wf", "ws", "ye", "yt", "za", "zm", "zw", "eu", "pirate"};

    /* JADX INFO: Multiple debug info for r23v1 int: [D('context' android.content.Context), D('flagsHeight' int)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static void loadFlags(Context context) {
        if (flags == null) {
            flags = new HashMap<>();
            DisplayMetrics metrics = Util.getDisplayMetrics(context);
            Bitmap flagsBitmap = Util.getBitmap(context, R.drawable.flags);
            int flagsWidth = flagsBitmap.getWidth();
            int flagWidth = flagsWidth / 10;
            int flagHeight = flagsBitmap.getHeight() / 25;
            int x = 0;
            int y = 0;
            Matrix matrix = new Matrix();
            matrix.setScale(metrics.density, metrics.density);
            Paint whitePaint = new Paint();
            whitePaint.setColor(-1);
            String[] arr$ = flagsIso;
            int len$ = arr$.length;
            int i$ = 0;
            while (true) {
                int i$2 = i$;
                if (i$2 < len$) {
                    String aFlagsISO = arr$[i$2];
                    if (x + flagWidth > flagsWidth) {
                        x = 0;
                        y += flagHeight;
                    }
                    Bitmap flagBitmap = Bitmap.createBitmap(flagsBitmap, x, y, flagWidth, flagHeight, matrix, true);
                    Bitmap newFlagBitmap = Bitmap.createBitmap(flagBitmap.getWidth() + 2, flagBitmap.getHeight() + 2, Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(newFlagBitmap);
                    canvas.drawColor(0);
                    canvas.drawLine(0.0f, 0.0f, (float) (flagBitmap.getWidth() + 1), 0.0f, whitePaint);
                    canvas.drawLine(0.0f, 0.0f, 0.0f, (float) (flagBitmap.getHeight() + 1), whitePaint);
                    canvas.drawLine((float) (flagBitmap.getWidth() + 1), 0.0f, (float) (flagBitmap.getWidth() + 2), (float) (flagBitmap.getHeight() + 2), whitePaint);
                    canvas.drawLine(0.0f, (float) (flagBitmap.getHeight() + 1), (float) (flagBitmap.getWidth() + 2), (float) (flagBitmap.getHeight() + 2), whitePaint);
                    canvas.drawBitmap(flagBitmap, 1.0f, 1.0f, (Paint) null);
                    flags.put(aFlagsISO, newFlagBitmap);
                    x += flagWidth;
                    i$ = i$2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public static Bitmap getFlag(String flagIso) {
        String flagIso2 = flagIso.trim().toLowerCase();
        if (flags.get(flagIso2) == null) {
            return flags.get("pirate");
        }
        return flags.get(flagIso2);
    }
}
