package ru.aeradeve.utils.rating;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771969;
        public static final int bannerType = 2130771973;
        public static final int deliverOnlyText = 2130771974;
        public static final int isTestMode = 2130771972;
        public static final int secondsToRefresh = 2130771968;
        public static final int textColor = 2130771971;
        public static final int textSize = 2130771970;
    }

    public static final class drawable {
        public static final int aeralogo = 2130837504;
        public static final int bg_main = 2130837505;
        public static final int btn_f_d_left = 2130837506;
        public static final int btn_f_d_right = 2130837507;
        public static final int btn_f_ld = 2130837508;
        public static final int btn_f_rd = 2130837509;
        public static final int btn_n_d_left = 2130837510;
        public static final int btn_n_d_right = 2130837511;
        public static final int btn_n_ld = 2130837512;
        public static final int btn_n_rd = 2130837513;
        public static final int change_name = 2130837514;
        public static final int exit = 2130837515;
        public static final int flags = 2130837516;
        public static final int gradient = 2130837517;
        public static final int icon = 2130837518;
        public static final int logo_for_ad = 2130837519;
        public static final int menu_logo = 2130837520;
        public static final int stars = 2130837521;
        public static final int start_game = 2130837522;
        public static final int top100 = 2130837523;
        public static final int your_score = 2130837524;
    }

    public static final class id {
        public static final int changeNameButton = 2131034115;
        public static final int exitButton = 2131034118;
        public static final int gameRenderSurfaceView = 2131034113;
        public static final int mainAdView = 2131034112;
        public static final int startGameButton = 2131034114;
        public static final int tableLayout = 2131034119;
        public static final int top100Button = 2131034116;
        public static final int yourScoreButton = 2131034117;
    }

    public static final class layout {
        public static final int ad = 2130903040;
        public static final int game = 2130903041;
        public static final int main = 2130903042;
        public static final int rating = 2130903043;
    }

    public static final class string {
        public static final int app_name = 2130968594;
        public static final int bonusScore = 2130968615;
        public static final int buttonApply = 2130968597;
        public static final int buttonCancel = 2130968598;
        public static final int changeNameButton = 2130968605;
        public static final int exitButton = 2130968607;
        public static final int gameMenuContinue = 2130968609;
        public static final int gameMenuMainMenu = 2130968611;
        public static final int gameMenuRestartGame = 2130968610;
        public static final int giveFiveDialogLater = 2130968592;
        public static final int giveFiveDialogNo = 2130968593;
        public static final int giveFiveDialogQuestion = 2130968590;
        public static final int giveFiveDialogYes = 2130968591;
        public static final int invalidNickname = 2130968601;
        public static final int labelCallKeyboard = 2130968596;
        public static final int labelNickname = 2130968595;
        public static final int loading = 2130968616;
        public static final int newRecord = 2130968612;
        public static final int noname = 2130968600;
        public static final int ratingButtonApply = 2130968582;
        public static final int ratingButtonCancel = 2130968583;
        public static final int ratingButtonRetry = 2130968581;
        public static final int ratingDialogPleaseWait = 2130968576;
        public static final int ratingDialogRatingEmpty = 2130968578;
        public static final int ratingDialogRatingLoadedWithError = 2130968577;
        public static final int ratingLabelNickname = 2130968580;
        public static final int ratingNoname = 2130968589;
        public static final int ratingTabFragmentTop100 = 2130968587;
        public static final int ratingTabFragmentYourScore = 2130968588;
        public static final int ratingTabPeriodAll = 2130968584;
        public static final int ratingTabPeriodMonth = 2130968585;
        public static final int ratingTabPeriodWeek = 2130968586;
        public static final int ratingTitleEnterNickname = 2130968579;
        public static final int score = 2130968613;
        public static final int startGameButton = 2130968603;
        public static final int success = 2130968602;
        public static final int tableScoreTitle = 2130968617;
        public static final int titleEnterNickname = 2130968599;
        public static final int top100 = 2130968604;
        public static final int waitPlease = 2130968608;
        public static final int yourBest = 2130968614;
        public static final int yourScoreButton = 2130968606;
    }

    public static final class styleable {
        public static final int[] BaseAdView = {ru.aeradeve.games.gravityclumps2.R.attr.secondsToRefresh, ru.aeradeve.games.gravityclumps2.R.attr.backgroundColor, ru.aeradeve.games.gravityclumps2.R.attr.textSize, ru.aeradeve.games.gravityclumps2.R.attr.textColor, ru.aeradeve.games.gravityclumps2.R.attr.isTestMode, ru.aeradeve.games.gravityclumps2.R.attr.bannerType, ru.aeradeve.games.gravityclumps2.R.attr.deliverOnlyText};
        public static final int BaseAdView_backgroundColor = 1;
        public static final int BaseAdView_bannerType = 5;
        public static final int BaseAdView_deliverOnlyText = 6;
        public static final int BaseAdView_isTestMode = 4;
        public static final int BaseAdView_secondsToRefresh = 0;
        public static final int BaseAdView_textColor = 3;
        public static final int BaseAdView_textSize = 2;
    }
}
