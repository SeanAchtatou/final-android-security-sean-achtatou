package ru.aeradeve.utils.rating.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import java.util.List;
import ru.aeradeve.utils.rating.entity.FilterRating;
import ru.aeradeve.utils.rating.entity.ScoreEntity;
import ru.aeradeve.utils.rating.loader.ILoadingCompleteListener;
import ru.aeradeve.utils.rating.loader.LoaderRatingThread;
import ru.aeradeve.utils.rating.utils.IRatingInfoGame;

public class RatingView extends LinearLayout {
    private FilterRating mFilter;
    /* access modifiers changed from: private */
    public IRatingInfoGame mInfoGame;
    /* access modifiers changed from: private */
    public LoaderRatingThread mScoreLoadingThread;
    /* access modifiers changed from: private */
    public SwitcherTableRatingView mTableRatingView;
    private TopMenuRatingView mTopMenuRatingView;

    public RatingView(Context context) {
        this(context, null);
    }

    public RatingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mFilter = new FilterRating();
        setOrientation(1);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        TopMenuRatingView topMenuRatingView = new TopMenuRatingView(context, this.mFilter);
        this.mTopMenuRatingView = topMenuRatingView;
        addView(topMenuRatingView);
        SwitcherTableRatingView switcherTableRatingView = new SwitcherTableRatingView(context, this.mFilter);
        this.mTableRatingView = switcherTableRatingView;
        addView(switcherTableRatingView);
        this.mFilter.addListener(new Runnable() {
            public void run() {
                if (RatingView.this.mInfoGame != null) {
                    RatingView.this.mTableRatingView.showLoading();
                    RatingView.this.mScoreLoadingThread.start();
                }
            }
        });
    }

    public void start(IRatingInfoGame pInfoGame) {
        this.mInfoGame = pInfoGame;
        this.mScoreLoadingThread = new LoaderRatingThread(getContext(), this.mInfoGame, this.mFilter, new ILoadingCompleteListener() {
            public void onSuccessful(final List<ScoreEntity> pScores) {
                RatingView.this.post(new Runnable() {
                    public void run() {
                        if (pScores.size() == 0) {
                            RatingView.this.mTableRatingView.showEmpty();
                        } else {
                            RatingView.this.mTableRatingView.showTable(pScores);
                        }
                    }
                });
            }

            public void onError() {
                RatingView.this.post(new Runnable() {
                    public void run() {
                        RatingView.this.mTableRatingView.showRetry();
                    }
                });
            }
        });
        this.mScoreLoadingThread.start();
    }

    public FilterRating getFilter() {
        return this.mFilter;
    }
}
