package ru.aeradeve.utils.rating.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import ru.aeradeve.utils.Util;

public class SButton extends Button implements View.OnTouchListener {
    private int mBackground;
    private int mColor;
    private int mDisabledColor;
    private boolean mSelect = false;
    private int mSelectedBackground;

    public SButton(Context context, int pText, int pBackground, int pSelectedBackground, int pColor) {
        super(context);
        this.mBackground = pBackground;
        this.mColor = pColor;
        this.mSelectedBackground = pSelectedBackground;
        setText(pText);
        setTextColor(this.mColor);
        setBackgroundResource(this.mBackground);
        this.mDisabledColor = Color.argb(Color.alpha(this.mColor), Color.red(this.mColor) / 2, Color.green(this.mColor) / 2, Color.blue(this.mColor) / 2);
        setOnTouchListener(this);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.mSelect) {
            return false;
        }
        if (motionEvent.getAction() == 0) {
            setBackgroundResource(this.mSelectedBackground);
        } else if (1 != motionEvent.getAction()) {
            return true;
        } else {
            setBackgroundResource(this.mBackground);
        }
        return false;
    }

    public void select() {
        if (!this.mSelect) {
            this.mSelect = true;
            setBackgroundResource(this.mSelectedBackground);
            setEnabled(isEnabled());
        }
    }

    public void unselect() {
        if (this.mSelect) {
            this.mSelect = false;
            setBackgroundResource(this.mBackground);
            setEnabled(isEnabled());
        }
    }

    public void setBackgroundResource(int resid) {
        super.setBackgroundResource(resid);
        setPadding(getPaddingLeft() + ((int) TypedValue.applyDimension(1, 7.0f, Util.getDisplayMetrics(getContext()))), getPaddingTop(), getPaddingRight() + ((int) TypedValue.applyDimension(1, 7.0f, Util.getDisplayMetrics(getContext()))), getPaddingBottom());
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        setTextColor((enabled || this.mSelect) ? this.mColor : this.mDisabledColor);
    }
}
