package ru.aeradeve.utils.rating.utils;

import android.content.Context;
import android.provider.Settings;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import ru.aeradeve.games.gravityclumps2.R;
import ru.aeradeve.utils.Util;
import ru.aeradeve.utils.rating.entity.FilterRating;

public abstract class BaseRatingInfoGame implements IRatingInfoGame {
    private static final Object blocker = new Object();
    protected Context mContext;
    protected String mFileName;
    protected String mNickName;
    private long mScore4Send = Long.MIN_VALUE;
    protected String mSpecialCode;

    /* access modifiers changed from: protected */
    public abstract void onInitData();

    protected BaseRatingInfoGame(Context pContext, String pFileName) {
        this.mFileName = pFileName;
        this.mContext = pContext;
    }

    public String getParams4URL(FilterRating pFilter) {
        String result = ("?gameid=" + getGameId()) + "&scode=" + getSCode();
        String nick = getNickName();
        try {
            nick = URLEncoder.encode(nick, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        String result2 = result + "&setnickname=" + nick;
        if (isNeedSendScore()) {
            result2 = result2 + "&setscore=" + this.mScore4Send;
        }
        return ((result2 + "&type=" + pFilter.getType()) + "&period=" + pFilter.getPeriod()) + "&fragment=" + pFilter.getFragment();
    }

    public String getURL() {
        return "http://aeratools.ru/api/rating/v3/rating.php";
    }

    /* access modifiers changed from: protected */
    public final void initData() {
        this.mSpecialCode = Settings.Secure.getString(this.mContext.getContentResolver(), "android_id");
        if (this.mSpecialCode == null) {
            this.mSpecialCode = "";
        }
        this.mSpecialCode += "!" + Util.getRandomString(64);
        this.mNickName = "";
        this.mScore4Send = Long.MIN_VALUE;
        onInitData();
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0056 A[SYNTHETIC, Splitter:B:27:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x005e A[SYNTHETIC, Splitter:B:30:0x005e] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x006b A[SYNTHETIC, Splitter:B:41:0x006b] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0073 A[SYNTHETIC, Splitter:B:44:0x0073] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void load() {
        /*
            r9 = this;
            monitor-enter(r9)
            java.lang.Object r6 = ru.aeradeve.utils.rating.utils.BaseRatingInfoGame.blocker     // Catch:{ all -> 0x0065 }
            monitor-enter(r6)     // Catch:{ all -> 0x0065 }
            r1 = 0
            r4 = 0
            r3 = 0
            android.content.Context r7 = r9.mContext     // Catch:{ Exception -> 0x004c }
            java.lang.String r8 = r9.mFileName     // Catch:{ Exception -> 0x004c }
            java.io.FileInputStream r3 = r7.openFileInput(r8)     // Catch:{ Exception -> 0x004c }
            ru.aeradeve.utils.io.SFileInputStream r5 = new ru.aeradeve.utils.io.SFileInputStream     // Catch:{ Exception -> 0x004c }
            java.io.FileDescriptor r7 = r3.getFD()     // Catch:{ Exception -> 0x004c }
            r5.<init>(r7)     // Catch:{ Exception -> 0x004c }
            java.util.Scanner r2 = new java.util.Scanner     // Catch:{ Exception -> 0x0086, all -> 0x007d }
            r2.<init>(r5)     // Catch:{ Exception -> 0x0086, all -> 0x007d }
            java.lang.String r7 = r2.nextLine()     // Catch:{ Exception -> 0x008a, all -> 0x0080 }
            r9.mSpecialCode = r7     // Catch:{ Exception -> 0x008a, all -> 0x0080 }
            java.lang.String r7 = r2.nextLine()     // Catch:{ Exception -> 0x008a, all -> 0x0080 }
            r9.mNickName = r7     // Catch:{ Exception -> 0x008a, all -> 0x0080 }
            java.lang.String r7 = r2.nextLine()     // Catch:{ Exception -> 0x008a, all -> 0x0080 }
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ Exception -> 0x008a, all -> 0x0080 }
            long r7 = r7.longValue()     // Catch:{ Exception -> 0x008a, all -> 0x0080 }
            r9.mScore4Send = r7     // Catch:{ Exception -> 0x008a, all -> 0x0080 }
            r9.onLoad(r2)     // Catch:{ Exception -> 0x008a, all -> 0x0080 }
            if (r3 == 0) goto L_0x0042
            r3.close()     // Catch:{ IOException -> 0x008f }
            r5.close()     // Catch:{ IOException -> 0x008f }
        L_0x0042:
            if (r2 == 0) goto L_0x0091
            r2.close()     // Catch:{ all -> 0x0077 }
            r4 = r5
            r1 = r2
        L_0x0049:
            monitor-exit(r6)     // Catch:{ all -> 0x0062 }
            monitor-exit(r9)
            return
        L_0x004c:
            r7 = move-exception
            r0 = r7
        L_0x004e:
            r9.initData()     // Catch:{ all -> 0x0068 }
            r9.save()     // Catch:{ all -> 0x0068 }
            if (r3 == 0) goto L_0x005c
            r3.close()     // Catch:{ IOException -> 0x0084 }
            r4.close()     // Catch:{ IOException -> 0x0084 }
        L_0x005c:
            if (r1 == 0) goto L_0x0049
            r1.close()     // Catch:{ all -> 0x0062 }
            goto L_0x0049
        L_0x0062:
            r7 = move-exception
        L_0x0063:
            monitor-exit(r6)     // Catch:{ all -> 0x0062 }
            throw r7     // Catch:{ all -> 0x0065 }
        L_0x0065:
            r6 = move-exception
            monitor-exit(r9)
            throw r6
        L_0x0068:
            r7 = move-exception
        L_0x0069:
            if (r3 == 0) goto L_0x0071
            r3.close()     // Catch:{ IOException -> 0x007b }
            r4.close()     // Catch:{ IOException -> 0x007b }
        L_0x0071:
            if (r1 == 0) goto L_0x0076
            r1.close()     // Catch:{ all -> 0x0062 }
        L_0x0076:
            throw r7     // Catch:{ all -> 0x0062 }
        L_0x0077:
            r7 = move-exception
            r4 = r5
            r1 = r2
            goto L_0x0063
        L_0x007b:
            r8 = move-exception
            goto L_0x0071
        L_0x007d:
            r7 = move-exception
            r4 = r5
            goto L_0x0069
        L_0x0080:
            r7 = move-exception
            r4 = r5
            r1 = r2
            goto L_0x0069
        L_0x0084:
            r7 = move-exception
            goto L_0x005c
        L_0x0086:
            r7 = move-exception
            r0 = r7
            r4 = r5
            goto L_0x004e
        L_0x008a:
            r7 = move-exception
            r0 = r7
            r4 = r5
            r1 = r2
            goto L_0x004e
        L_0x008f:
            r7 = move-exception
            goto L_0x0042
        L_0x0091:
            r4 = r5
            r1 = r2
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: ru.aeradeve.utils.rating.utils.BaseRatingInfoGame.load():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0031 A[SYNTHETIC, Splitter:B:19:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x003e A[SYNTHETIC, Splitter:B:30:0x003e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void save() {
        /*
            r6 = this;
            monitor-enter(r6)
            java.lang.Object r2 = ru.aeradeve.utils.rating.utils.BaseRatingInfoGame.blocker     // Catch:{ all -> 0x0038 }
            monitor-enter(r2)     // Catch:{ all -> 0x0038 }
            r0 = 0
            ru.aeradeve.utils.io.SPrintStream r1 = new ru.aeradeve.utils.io.SPrintStream     // Catch:{ FileNotFoundException -> 0x002e, all -> 0x003b }
            android.content.Context r3 = r6.mContext     // Catch:{ FileNotFoundException -> 0x002e, all -> 0x003b }
            java.lang.String r4 = r6.mFileName     // Catch:{ FileNotFoundException -> 0x002e, all -> 0x003b }
            r5 = 0
            java.io.FileOutputStream r3 = r3.openFileOutput(r4, r5)     // Catch:{ FileNotFoundException -> 0x002e, all -> 0x003b }
            r1.<init>(r3)     // Catch:{ FileNotFoundException -> 0x002e, all -> 0x003b }
            java.lang.String r3 = r6.mSpecialCode     // Catch:{ FileNotFoundException -> 0x0048, all -> 0x0045 }
            r1.println(r3)     // Catch:{ FileNotFoundException -> 0x0048, all -> 0x0045 }
            java.lang.String r3 = r6.mNickName     // Catch:{ FileNotFoundException -> 0x0048, all -> 0x0045 }
            r1.println(r3)     // Catch:{ FileNotFoundException -> 0x0048, all -> 0x0045 }
            long r3 = r6.mScore4Send     // Catch:{ FileNotFoundException -> 0x0048, all -> 0x0045 }
            r1.println(r3)     // Catch:{ FileNotFoundException -> 0x0048, all -> 0x0045 }
            r6.onSave(r1)     // Catch:{ FileNotFoundException -> 0x0048, all -> 0x0045 }
            if (r1 == 0) goto L_0x004b
            r1.close()     // Catch:{ all -> 0x0042 }
            r0 = r1
        L_0x002b:
            monitor-exit(r2)     // Catch:{ all -> 0x0035 }
            monitor-exit(r6)
            return
        L_0x002e:
            r3 = move-exception
        L_0x002f:
            if (r0 == 0) goto L_0x002b
            r0.close()     // Catch:{ all -> 0x0035 }
            goto L_0x002b
        L_0x0035:
            r3 = move-exception
        L_0x0036:
            monitor-exit(r2)     // Catch:{ all -> 0x0035 }
            throw r3     // Catch:{ all -> 0x0038 }
        L_0x0038:
            r2 = move-exception
            monitor-exit(r6)
            throw r2
        L_0x003b:
            r3 = move-exception
        L_0x003c:
            if (r0 == 0) goto L_0x0041
            r0.close()     // Catch:{ all -> 0x0035 }
        L_0x0041:
            throw r3     // Catch:{ all -> 0x0035 }
        L_0x0042:
            r3 = move-exception
            r0 = r1
            goto L_0x0036
        L_0x0045:
            r3 = move-exception
            r0 = r1
            goto L_0x003c
        L_0x0048:
            r3 = move-exception
            r0 = r1
            goto L_0x002f
        L_0x004b:
            r0 = r1
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: ru.aeradeve.utils.rating.utils.BaseRatingInfoGame.save():void");
    }

    public String getNickName() {
        return this.mNickName;
    }

    public void setNickName(String pNickName) {
        if (pNickName == null || pNickName.length() == 0) {
            pNickName = Util.getString(this.mContext, R.string.ratingNoname);
        }
        this.mNickName = pNickName;
    }

    public String getSCode() {
        return this.mSpecialCode;
    }

    public void setScore4Send(long pScore4Send) {
        this.mScore4Send = Math.max(pScore4Send, this.mScore4Send);
    }

    public void resetScore4Send() {
        this.mScore4Send = Long.MIN_VALUE;
    }

    public boolean isNeedSendScore() {
        return this.mScore4Send != Long.MIN_VALUE;
    }

    public boolean isNickNoname() {
        return Util.getString(this.mContext, R.string.ratingNoname).equals(this.mNickName);
    }

    public Context getContext() {
        return this.mContext;
    }

    public long getScore4Send() {
        return this.mScore4Send;
    }
}
