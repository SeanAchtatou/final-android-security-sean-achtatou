package ru.aeradeve.utils.rating.loader;

import android.content.Context;
import android.net.http.AndroidHttpClient;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import ru.aeradeve.utils.io.StringLineReader;
import ru.aeradeve.utils.rating.entity.FilterRating;
import ru.aeradeve.utils.rating.entity.ScoreEntity;
import ru.aeradeve.utils.rating.utils.Flags;
import ru.aeradeve.utils.rating.utils.IRatingInfoGame;

public class LoaderRatingThread implements Runnable {
    private Context mContext;
    private HttpUriRequest mCurrentRequest = null;
    private Thread mCurrentThread = null;
    private FilterRating mFilter = null;
    private IRatingInfoGame mInfoGame;
    private ILoadingCompleteListener mListener = null;

    public LoaderRatingThread(Context pContext, IRatingInfoGame pInfoGame, FilterRating pFilter, ILoadingCompleteListener pListener) {
        this.mInfoGame = pInfoGame;
        this.mFilter = pFilter;
        this.mListener = pListener;
        this.mContext = pContext;
    }

    public void start() {
        if (this.mCurrentThread != null && this.mCurrentThread.isAlive()) {
            this.mCurrentThread.interrupt();
            if (this.mCurrentRequest != null) {
                this.mCurrentRequest.abort();
            }
        }
        this.mCurrentThread = new Thread(this);
        this.mCurrentThread.start();
    }

    public void run() {
        Flags.loadFlags(this.mContext);
        ArrayList arrayList = new ArrayList();
        String uri = this.mInfoGame.getURL() + this.mInfoGame.getParams4URL(this.mFilter);
        if (uri != null && !"".equals(uri)) {
            AndroidHttpClient httpClient = AndroidHttpClient.newInstance("aera gameid=" + this.mInfoGame.getGameId());
            HttpUriRequest httpGet = new HttpGet(uri);
            this.mCurrentRequest = httpGet;
            Thread current = Thread.currentThread();
            byte[] result = new byte[0];
            try {
                byte[] b = new byte[1024];
                InputStream in = httpClient.execute(httpGet).getEntity().getContent();
                while (!current.isInterrupted()) {
                    int size = in.read(b);
                    if (size > 0) {
                        byte[] nr = new byte[(result.length + size)];
                        System.arraycopy(result, 0, nr, 0, result.length);
                        System.arraycopy(b, 0, nr, result.length, size);
                        result = nr;
                        continue;
                    }
                    if (size <= 0) {
                        in.close();
                        try {
                            StringLineReader stringLineReader = new StringLineReader(result, result.length);
                            stringLineReader.nextLine();
                            while (stringLineReader.hasNextLine()) {
                                if (!current.isInterrupted()) {
                                    ScoreEntity scoreEntity = new ScoreEntity();
                                    scoreEntity.setRank(Integer.parseInt(stringLineReader.nextLine().trim()));
                                    scoreEntity.setName(stringLineReader.nextLine());
                                    scoreEntity.setScore(stringLineReader.nextLine());
                                    scoreEntity.setCountry(stringLineReader.nextLine());
                                    scoreEntity.setFlagIso(stringLineReader.nextLine());
                                    stringLineReader.nextLine();
                                    arrayList.add(scoreEntity);
                                } else {
                                    return;
                                }
                            }
                            synchronized (this.mInfoGame) {
                                if (this.mInfoGame.isNeedSendScore()) {
                                    this.mInfoGame.resetScore4Send();
                                    this.mInfoGame.save();
                                }
                            }
                            if (this.mListener != null && current == this.mCurrentThread) {
                                this.mListener.onSuccessful(arrayList);
                                return;
                            }
                            return;
                        } catch (Exception e) {
                            Exception e2 = e;
                            if (this.mListener != null) {
                                this.mListener.onError();
                            }
                            e2.printStackTrace();
                            return;
                        }
                    }
                }
                httpClient.close();
            } catch (IOException e3) {
                if (this.mListener != null) {
                    this.mListener.onError();
                }
            } finally {
                httpClient.close();
            }
        } else if (this.mListener != null && Thread.currentThread() == this.mCurrentThread) {
            this.mListener.onSuccessful(arrayList);
        }
    }
}
