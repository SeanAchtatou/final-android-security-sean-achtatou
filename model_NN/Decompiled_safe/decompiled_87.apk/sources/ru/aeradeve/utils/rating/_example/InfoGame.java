package ru.aeradeve.utils.rating._example;

import android.content.Context;
import android.provider.Settings;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;
import ru.aeradeve.utils.Util;
import ru.aeradeve.utils.rating.utils.BaseRatingInfoGame;

public class InfoGame extends BaseRatingInfoGame {
    private long bestScore = 0;
    private int countBlocks = 0;
    private int lives = 3;
    private long score = 0;
    private long sumScore = 0;

    public InfoGame(Context pContext) {
        super(pContext, "savefilefortower_");
        load();
    }

    public int getGameId() {
        return 6;
    }

    public void reset() {
        this.score = 0;
        this.countBlocks = 0;
        this.lives = 3;
    }

    public long getScore() {
        return this.score;
    }

    public long getBestScore() {
        return this.bestScore;
    }

    public int getLives() {
        return this.lives;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public void addScore(long add) {
        this.score += add;
        this.score = Math.max(this.score, 0L);
        this.score = Math.min(this.score, 2000000000L);
        this.sumScore = Math.min(this.sumScore, 2000000000L);
        this.bestScore = Math.max(this.bestScore, this.score);
    }

    public void addLives(int add) {
        this.lives += add;
    }

    public void incCountBlocks() {
        this.countBlocks++;
    }

    /* access modifiers changed from: protected */
    public void onInitData() {
        this.score = 0;
        this.bestScore = 0;
        this.sumScore = 0;
        try {
            Scanner fin = new Scanner(this.mContext.openFileInput(Const.fileSave));
            this.mSpecialCode = fin.nextLine();
            this.mNickName = fin.nextLine();
            this.sumScore = fin.nextLong();
            this.bestScore = fin.nextLong();
            fin.close();
            this.mContext.openFileOutput(Const.fileSave, 0).close();
        } catch (FileNotFoundException e) {
            this.mSpecialCode = Settings.Secure.getString(this.mContext.getContentResolver(), "android_id");
            if (this.mSpecialCode == null) {
                this.mSpecialCode = "";
            }
            this.mSpecialCode += "!" + Util.getRandomString(64);
        } catch (IOException e2) {
        }
    }

    public void onSave(PrintStream out) {
    }

    public void onLoad(Scanner in) {
    }

    public int getCountBlocks() {
        return this.countBlocks;
    }
}
