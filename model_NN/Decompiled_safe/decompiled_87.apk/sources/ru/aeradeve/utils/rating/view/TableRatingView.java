package ru.aeradeve.utils.rating.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.util.List;
import ru.aeradeve.utils.Util;
import ru.aeradeve.utils.rating.entity.FilterRating;
import ru.aeradeve.utils.rating.entity.ScoreEntity;
import ru.aeradeve.utils.rating.utils.Flags;

public class TableRatingView extends ScrollView {
    private FilterRating mFilter;
    private TableLayout mTable;

    public TableRatingView(Context context, FilterRating pFilter) {
        this(context, pFilter, null);
    }

    public TableRatingView(Context context, FilterRating pFilter, AttributeSet attrs) {
        super(context, attrs);
        this.mFilter = pFilter;
        setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.mTable = new TableLayout(context);
        this.mTable.setLayoutParams(new TableLayout.LayoutParams(-1, -2));
        this.mTable.setPadding(5, 5, 5, 5);
        this.mTable.setStretchAllColumns(true);
        addView(this.mTable);
    }

    public void fillTable(List<ScoreEntity> scores) {
        this.mTable.removeAllViews();
        TableRow playerRow = null;
        for (ScoreEntity scoreEntity : scores) {
            TableRow tableRow = new TableRow(getContext());
            tableRow.setLayoutParams(new TableRow.LayoutParams(-1, -2));
            if (playerRow == null) {
                playerRow = tableRow;
            }
            int rank = scoreEntity.getRank();
            int color = -1;
            if (rank < 0) {
                rank = -rank;
                color = Color.rgb(123, 255, 72);
                playerRow = tableRow;
            }
            TextView textView = new TextView(getContext());
            String text = rank + ". " + Util.censor(scoreEntity.getName(), 17);
            textView.setText(text);
            textView.setTextSize(1, text.length() > 14 ? 12.0f : 13.0f);
            textView.setTypeface(Typeface.DEFAULT_BOLD);
            textView.setTextColor(color);
            LinearLayout linearLayout = new LinearLayout(getContext());
            linearLayout.setGravity(16);
            ImageView imageView = new ImageView(getContext());
            imageView.setImageBitmap(Flags.getFlag(scoreEntity.getFlagIso()));
            TextView textView2 = new TextView(getContext());
            textView2.setText(Util.censor(scoreEntity.getCountry(), 17));
            textView2.setTextSize(1, 13.0f);
            textView2.setTypeface(Typeface.DEFAULT_BOLD);
            textView2.setTextColor(color);
            linearLayout.addView(imageView);
            linearLayout.addView(textView2);
            TextView textView3 = new TextView(getContext());
            textView3.setGravity(5);
            textView3.setText(scoreEntity.getScore());
            textView3.setTextSize(1, 13.0f);
            textView3.setTypeface(Typeface.DEFAULT_BOLD);
            textView3.setTextColor(color);
            tableRow.addView(textView);
            tableRow.addView(linearLayout);
            tableRow.addView(textView3);
            tableRow.setPadding(0, 0, 0, 1);
            this.mTable.addView(tableRow);
        }
        if (playerRow != null) {
            final TableRow tableRow2 = playerRow;
            post(new Runnable() {
                public void run() {
                    TableRatingView.this.scrollTo(tableRow2.getLeft(), Math.max(0, tableRow2.getTop() - (TableRatingView.this.getHeight() / 2)));
                }
            });
        }
    }
}
