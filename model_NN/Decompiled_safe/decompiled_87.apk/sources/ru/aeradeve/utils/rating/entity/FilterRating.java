package ru.aeradeve.utils.rating.entity;

import java.util.ArrayList;
import java.util.Iterator;

public class FilterRating {
    public static final int FRAGMENT_AROUND_PLAYER = 2;
    public static final int FRAGMENT_TOP100 = 1;
    public static final int PERIOD_ALL = 0;
    public static final int PERIOD_MONTH = 2;
    public static final int PERIOD_WEEK = 1;
    public static final int TYPE_ALL = 1;
    public static final int TYPE_ONE_COUNTRY = 3;
    public static final int TYPE_ONE_PLAYER = 2;
    private ArrayList<Runnable> listeners = new ArrayList<>();
    private String mCountryIso = "";
    private int mFragment = 1;
    private int mPeriod = 0;
    private int mType = 1;

    public void addListener(Runnable pRunnable) {
        this.listeners.add(pRunnable);
    }

    public void init(int pType, int pPeriod, int pFragment, String pCountryIso) {
        this.mType = pType;
        this.mPeriod = pPeriod;
        this.mFragment = pFragment;
        this.mCountryIso = pCountryIso;
        wasChanged();
    }

    private void wasChanged() {
        Iterator i$ = this.listeners.iterator();
        while (i$.hasNext()) {
            i$.next().run();
        }
    }

    public String getCountryIso() {
        return this.mCountryIso;
    }

    public void setCountryIso(String mCountryIso2) {
        this.mCountryIso = mCountryIso2;
        wasChanged();
    }

    public int getPeriod() {
        return this.mPeriod;
    }

    public void setPeriod(int mPeriod2) {
        this.mPeriod = mPeriod2;
        wasChanged();
    }

    public int getType() {
        return this.mType;
    }

    public void setType(int mType2) {
        this.mType = mType2;
        wasChanged();
    }

    public int getFragment() {
        return this.mFragment;
    }

    public void setFragment(int mFragment2) {
        this.mFragment = mFragment2;
        wasChanged();
    }
}
