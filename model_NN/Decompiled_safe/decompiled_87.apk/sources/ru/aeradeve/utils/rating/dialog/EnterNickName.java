package ru.aeradeve.utils.rating.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import ru.aeradeve.games.gravityclumps2.R;
import ru.aeradeve.utils.Util;
import ru.aeradeve.utils.rating.utils.IRatingInfoGame;

public class EnterNickName extends AlertDialog {
    private Context context;
    private EditText editText;
    /* access modifiers changed from: private */
    public IRatingInfoGame mInfogame;

    public EnterNickName(Context context2, int pIcon, IRatingInfoGame pInfogame, final Runnable pOk, final Runnable pCancel) {
        super(context2);
        this.context = context2;
        this.mInfogame = pInfogame;
        LinearLayout linearLayout = new LinearLayout(context2);
        linearLayout.setPadding(33, 11, 33, 11);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.setOrientation(1);
        this.editText = new EditText(context2);
        this.editText.setSingleLine();
        if (!pInfogame.isNickNoname()) {
            this.editText.setText(pInfogame.getNickName());
        }
        TextView label = new TextView(context2);
        label.setText((int) R.string.ratingLabelNickname);
        linearLayout.addView(label);
        linearLayout.addView(this.editText);
        setView(linearLayout);
        setButton(Util.getString(context2, R.string.ratingButtonApply), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                String value = EnterNickName.this.getValue();
                if (value.length() > 0) {
                    EnterNickName.this.mInfogame.setNickName(value);
                    EnterNickName.this.mInfogame.save();
                }
                if (pOk != null) {
                    pOk.run();
                }
                dialogInterface.cancel();
            }
        });
        setButton2(Util.getString(context2, R.string.ratingButtonCancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (pCancel != null) {
                    pCancel.run();
                }
                dialogInterface.cancel();
            }
        });
        setTitle((int) R.string.ratingTitleEnterNickname);
        setIcon(pIcon);
    }

    public void show() {
        super.show();
        showKeybord();
    }

    public void showKeybord() {
        getWindow().setSoftInputMode(5);
    }

    public String getValue() {
        return this.editText.getText().toString();
    }
}
