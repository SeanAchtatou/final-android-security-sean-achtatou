package ru.aeradeve.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import java.io.IOException;
import java.io.InputStream;

public class Util {
    public static String getRandomString(int len) {
        char[] result = new char[len];
        for (int i = 0; i < len; i++) {
            result[i] = (char) (((int) (Math.random() * 26.0d)) + 97);
        }
        return new String(result);
    }

    public static String getString(Context context, int rid) {
        return context.getResources().getString(rid);
    }

    public static Bitmap getBitmap(Context context, int id) {
        InputStream is = context.getResources().openRawResource(id);
        try {
            return BitmapFactory.decodeStream(is);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
    }

    public static String censor(String src, int length) {
        String result = src;
        for (String word : new String[]{"хуй", "пизд", "fuck"}) {
            if (src.toLowerCase().indexOf(word) >= 0) {
                result = "*** censored ***";
            }
        }
        if (result.length() > length) {
            return result.substring(0, length - 3) + "...";
        }
        return result;
    }

    public static DisplayMetrics getDisplayMetrics(Context context) {
        return context.getResources().getDisplayMetrics();
    }
}
