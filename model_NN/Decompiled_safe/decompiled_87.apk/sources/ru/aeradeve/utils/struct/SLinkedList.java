package ru.aeradeve.utils.struct;

public class SLinkedList<T> {
    private SLinkedList<T>.ElementLinkedList mHead = null;
    private SLinkedList<T>.ElementLinkedList mTail = null;

    public class ElementLinkedList {
        /* access modifiers changed from: private */
        public SLinkedList<T>.ElementLinkedList mNext = null;
        /* access modifiers changed from: private */
        public SLinkedList<T>.ElementLinkedList mPrev = null;
        /* access modifiers changed from: private */
        public boolean mRemoved = false;
        private T mValue;

        public ElementLinkedList(T pValue) {
            this.mValue = pValue;
        }

        public T get() {
            return this.mValue;
        }

        public SLinkedList<T>.ElementLinkedList next() {
            SLinkedList<T>.ElementLinkedList current = this.mNext;
            while (current != null && current.mRemoved) {
                current = current.mNext;
            }
            return current;
        }

        public SLinkedList<T>.ElementLinkedList prev() {
            SLinkedList<T>.ElementLinkedList current = this.mPrev;
            while (current != null && current.mRemoved) {
                current = current.mPrev;
            }
            return current;
        }

        /* access modifiers changed from: private */
        public void insertAfter(SLinkedList<T>.ElementLinkedList pElement) {
            pElement.mNext = this.mNext;
            pElement.mPrev = this;
            this.mNext = pElement;
        }
    }

    public void add(T element) {
        SLinkedList<T>.ElementLinkedList current = new ElementLinkedList(element);
        if (this.mTail == null) {
            this.mTail = current;
            this.mHead = current;
            return;
        }
        this.mTail.insertAfter(current);
        this.mTail = current;
    }

    public SLinkedList<T>.ElementLinkedList getFirst() {
        if (this.mHead == null || !this.mHead.mRemoved) {
            return this.mHead;
        }
        return this.mHead.next();
    }

    public SLinkedList<T>.ElementLinkedList getLast() {
        if (this.mTail == null || !this.mTail.mRemoved) {
            return this.mTail;
        }
        return this.mTail.prev();
    }

    public synchronized void remove(T pElement) {
        for (SLinkedList<T>.ElementLinkedList current = getFirst(); current != null; current = current.next()) {
            if (current.get() == pElement) {
                remove(current);
            }
        }
    }

    public synchronized void remove(SLinkedList<T>.ElementLinkedList pElement) {
        if (!pElement.mRemoved) {
            SLinkedList<T>.ElementLinkedList next = pElement.next();
            SLinkedList<T>.ElementLinkedList prev = pElement.prev();
            if (pElement == this.mHead) {
                this.mHead = next;
            }
            if (pElement == this.mTail) {
                this.mTail = prev;
            }
            if (next != null) {
                ElementLinkedList unused = next.mPrev = prev;
            }
            if (prev != null) {
                ElementLinkedList unused2 = prev.mNext = next;
            }
            boolean unused3 = pElement.mRemoved = true;
        }
    }
}
