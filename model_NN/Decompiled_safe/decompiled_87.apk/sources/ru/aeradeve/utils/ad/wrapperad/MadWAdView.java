package ru.aeradeve.utils.ad.wrapperad;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import de.madvertise.android.sdk.MadView;

public class MadWAdView extends BaseWAdView implements MadView.MadViewCallbackListener {
    private MadView mMad;

    public MadWAdView(Context pContext, AttributeSet pAttrs) {
        super(pContext);
        this.mMad = new MadView(this.mContext, pAttrs);
        this.mMad.setMadViewCallbackListener(this);
    }

    public View getView() {
        return this.mMad;
    }

    public void stop() {
        this.mMad.stopLoading();
    }

    public void start() {
        this.mMad.startLoadingAd();
    }

    public void refresh() {
        this.mMad.startLoadingAd();
    }

    public void onLoaded(boolean succeed, MadView madView) {
        if (this.mListener != null) {
            this.mListener.onLoaded(succeed, this);
        }
    }
}
