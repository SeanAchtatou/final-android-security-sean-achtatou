package ru.aeradeve.utils.ad.wrapperad;

import android.content.Context;
import android.view.View;

public abstract class BaseWAdView {
    protected Context mContext;
    protected IWAdListener mListener = null;

    public abstract View getView();

    public abstract void refresh();

    public abstract void start();

    public abstract void stop();

    protected BaseWAdView(Context pContext) {
        this.mContext = pContext;
    }

    public void setListener(IWAdListener mListener2) {
        this.mListener = mListener2;
    }
}
