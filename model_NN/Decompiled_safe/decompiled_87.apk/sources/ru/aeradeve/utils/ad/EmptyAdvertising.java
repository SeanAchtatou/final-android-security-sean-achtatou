package ru.aeradeve.utils.ad;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import ru.aeradeve.games.gravityclumps2.R;

public class EmptyAdvertising extends View {
    private Bitmap logo = null;
    private Paint mBodyAd = new Paint();
    private Paint mBorderAd = new Paint();
    private Paint mEmptyPaint = new Paint();

    public EmptyAdvertising(Context context) {
        super(context);
        this.logo = BitmapFactory.decodeResource(context.getResources(), R.drawable.logo_for_ad);
        this.mBorderAd.setColor(-7829368);
        this.mBorderAd.setStyle(Paint.Style.STROKE);
        this.mBodyAd.setColor(-16777216);
        this.mBodyAd.setStyle(Paint.Style.FILL);
    }

    public void draw(Canvas canvas) {
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), this.mBodyAd);
        canvas.drawRect(0.0f, 1.0f, (float) (getWidth() - 1), (float) (getHeight() - 1), this.mBorderAd);
        canvas.drawBitmap(this.logo, (float) ((getWidth() - this.logo.getWidth()) / 2), (float) ((getHeight() - this.logo.getHeight()) / 2), this.mEmptyPaint);
    }
}
