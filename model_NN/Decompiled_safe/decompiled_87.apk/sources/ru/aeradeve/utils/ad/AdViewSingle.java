package ru.aeradeve.utils.ad;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ViewSwitcher;
import ru.aeradeve.utils.ad.wrapperad.BaseWAdView;
import ru.aeradeve.utils.ad.wrapperad.IWAdListener;

public class AdViewSingle extends ViewSwitcher implements IWAdListener {
    private static AdViewSingle mView = null;
    private int mCurrentWAdView = -1;
    private int mHeight;
    private BaseAdView mOwner = null;
    private BaseWAdView[] mWAdView;

    private AdViewSingle(Context context, BaseWAdView... pWAdView) {
        super(context);
        this.mHeight = context.getResources().getDisplayMetrics().widthPixels / 6;
        setLayoutParams(new FrameLayout.LayoutParams(-1, this.mHeight, 17));
        addView(new EmptyAdvertising(context), new FrameLayout.LayoutParams(-1, this.mHeight));
        this.mWAdView = pWAdView;
        loadNext();
    }

    private void showView(View pView) {
        removeAllViews();
        addView(pView, new FrameLayout.LayoutParams(-2, -2, 17));
    }

    private BaseWAdView getCurrentBaseWAdView() {
        if (this.mCurrentWAdView >= 0) {
            return this.mWAdView[this.mCurrentWAdView];
        }
        return null;
    }

    public void stop() {
        BaseWAdView current = getCurrentBaseWAdView();
        if (current != null) {
            current.stop();
        }
    }

    public void start() {
        BaseWAdView current = getCurrentBaseWAdView();
        if (current != null) {
            current.start();
        }
    }

    public void refresh() {
        BaseWAdView current = getCurrentBaseWAdView();
        if (current != null) {
            current.refresh();
        }
    }

    private void loadNext() {
        if (this.mWAdView != null && this.mWAdView.length > this.mCurrentWAdView + 1) {
            if (this.mCurrentWAdView >= 0) {
                this.mWAdView[this.mCurrentWAdView].stop();
            }
            this.mCurrentWAdView++;
            BaseWAdView currentAdView = this.mWAdView[this.mCurrentWAdView];
            currentAdView.setListener(this);
            Log.d("WAd", "start loading WAd " + currentAdView);
            currentAdView.start();
        }
    }

    public static void createInstance(Context context, BaseWAdView... pWAdView) {
        if (mView == null) {
            mView = new AdViewSingle(context, pWAdView);
        }
    }

    public static AdViewSingle getInstance() {
        return mView;
    }

    public static void onDestroy() {
        if (!(mView == null || mView.mWAdView == null)) {
            for (BaseWAdView stop : mView.mWAdView) {
                stop.stop();
            }
        }
        mView = null;
    }

    public void insertInto(BaseAdView pOwner) {
        if (this.mOwner != null) {
            this.mOwner.hideAd();
        }
        this.mOwner = pOwner;
        if (this.mOwner != null) {
            this.mOwner.removeAllViews();
            this.mOwner.addView(this);
        }
    }

    public void onLoaded(boolean pSucceed, BaseWAdView pWAdView) {
        Log.d("WAd", "loading complete, pSucceed=" + pSucceed + ", pWadView=" + pWAdView);
        if (pSucceed) {
            showView(this.mWAdView[this.mCurrentWAdView].getView());
        } else {
            loadNext();
        }
    }
}
