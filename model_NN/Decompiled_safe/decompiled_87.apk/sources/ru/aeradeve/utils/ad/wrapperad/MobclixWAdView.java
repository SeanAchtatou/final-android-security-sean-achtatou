package ru.aeradeve.utils.ad.wrapperad;

import android.content.Context;
import android.view.View;
import com.mobclix.android.sdk.MobclixAdView;
import com.mobclix.android.sdk.MobclixAdViewListener;
import com.mobclix.android.sdk.MobclixMMABannerXLAdView;

public class MobclixWAdView extends BaseWAdView implements MobclixAdViewListener {
    private MobclixMMABannerXLAdView mMobclix;

    public MobclixWAdView(Context pContext) {
        super(pContext);
    }

    public View getView() {
        return this.mMobclix;
    }

    public void stop() {
        if (this.mMobclix != null) {
            this.mMobclix.pause();
        }
    }

    public void start() {
        if (this.mMobclix == null) {
            this.mMobclix = new MobclixMMABannerXLAdView(this.mContext);
            this.mMobclix.addMobclixAdViewListener(this);
            return;
        }
        this.mMobclix.getAd();
    }

    public void refresh() {
        this.mMobclix.getAd();
    }

    public void onSuccessfulLoad(MobclixAdView mobclixAdView) {
        if (this.mListener != null) {
            this.mListener.onLoaded(true, this);
        }
    }

    public void onFailedLoad(MobclixAdView mobclixAdView, int i) {
        if (this.mListener != null) {
            this.mListener.onLoaded(false, this);
        }
    }

    public void onAdClick(MobclixAdView mobclixAdView) {
    }

    public boolean onOpenAllocationLoad(MobclixAdView mobclixAdView, int i) {
        return false;
    }

    public void onCustomAdTouchThrough(MobclixAdView mobclixAdView, String s) {
    }

    public String keywords() {
        return null;
    }

    public String query() {
        return null;
    }
}
