package ru.aeradeve.utils.ad;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import ru.aeradeve.utils.ad.wrapperad.BaseWAdView;

public abstract class BaseAdView extends LinearLayout {
    private int mHeight;
    private View mHideAd;

    protected BaseAdView(Context pContext, AttributeSet pAttrs, BaseWAdView... pBaseViews) {
        super(pContext, pAttrs);
        this.mHeight = pContext.getResources().getDisplayMetrics().widthPixels / 6;
        setLayoutParams(new FrameLayout.LayoutParams(-1, this.mHeight));
        this.mHideAd = new EmptyAdvertising(pContext);
        AdViewSingle.createInstance(getContext(), pBaseViews);
    }

    public void hideAd() {
        removeAllViews();
        addView(this.mHideAd, new FrameLayout.LayoutParams(-1, this.mHeight));
    }

    public void showAd() {
        removeAllViews();
        AdViewSingle.getInstance().insertInto(this);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        if (visibility == 0) {
            showAd();
        }
    }
}
