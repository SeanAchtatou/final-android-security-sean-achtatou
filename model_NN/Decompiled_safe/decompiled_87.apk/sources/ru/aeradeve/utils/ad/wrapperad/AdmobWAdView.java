package ru.aeradeve.utils.ad.wrapperad;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class AdmobWAdView extends BaseWAdView implements AdListener {
    private AdView mAdmob;
    private String mCode;

    public AdmobWAdView(Context pContext, String pCode) {
        super(pContext);
        this.mCode = pCode;
    }

    public View getView() {
        return this.mAdmob;
    }

    public void stop() {
        if (this.mAdmob != null) {
            this.mAdmob.stopLoading();
        }
    }

    public void start() {
        if (this.mAdmob == null) {
            this.mAdmob = new AdView((Activity) this.mContext, AdSize.BANNER, this.mCode);
            this.mAdmob.setAdListener(this);
        } else {
            this.mAdmob.stopLoading();
        }
        this.mAdmob.loadAd(new AdRequest());
    }

    public void refresh() {
        start();
    }

    public void onReceiveAd(Ad ad) {
        if (this.mListener != null) {
            this.mListener.onLoaded(true, this);
        }
    }

    public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode errorCode) {
        if (this.mListener != null) {
            this.mListener.onLoaded(false, this);
        }
    }

    public void onPresentScreen(Ad ad) {
    }

    public void onDismissScreen(Ad ad) {
    }

    public void onLeaveApplication(Ad ad) {
    }
}
