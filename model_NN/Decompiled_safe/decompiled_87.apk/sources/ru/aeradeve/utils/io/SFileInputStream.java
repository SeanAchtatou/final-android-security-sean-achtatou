package ru.aeradeve.utils.io;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class SFileInputStream extends FileInputStream {
    private byte[] buffer = new byte[17];
    private int count = 0;
    private int indMove = -1;

    public SFileInputStream(File file) throws FileNotFoundException {
        super(file);
    }

    public SFileInputStream(FileDescriptor fdObj) {
        super(fdObj);
    }

    private int flushBuffer(byte[] b, int off, int len) throws IOException {
        int i;
        byte currentSum = 0;
        for (int i2 = 0; i2 < this.count - 1; i2++) {
            currentSum = (byte) ((this.buffer[i2] ^ -1) ^ currentSum);
        }
        if (this.buffer[this.count - 1] != currentSum) {
            throw new IOException("File was changed");
        }
        int nlen = Math.min(this.count - 1, len);
        int i3 = 0;
        int off2 = off;
        while (i3 < nlen) {
            b[off2] = this.buffer[i3];
            i3++;
            off2++;
        }
        if (nlen == 16) {
            i = -1;
        } else {
            i = nlen;
        }
        this.indMove = i;
        this.count = 0;
        return nlen;
    }

    public int read(byte[] b, int off, int len) throws IOException {
        int z = super.read(b, off, len);
        int newLen = 0;
        if (z != -1) {
            int ind = 0;
            while (ind < z) {
                while (this.count < 17 && ind < z) {
                    this.buffer[this.count] = (byte) (b[off + ind] ^ -1);
                    if (this.indMove >= 0 && this.indMove < 16) {
                        byte[] bArr = this.buffer;
                        int i = this.indMove;
                        this.indMove = i + 1;
                        b[off + ind] = bArr[i];
                        newLen++;
                    }
                    this.count++;
                    ind++;
                }
                if (this.count == 17 || z < len) {
                    newLen += flushBuffer(b, off + newLen, ind - newLen);
                }
            }
            return newLen;
        } else if (this.count == 0) {
            return -1;
        } else {
            return flushBuffer(b, off, len);
        }
    }
}
