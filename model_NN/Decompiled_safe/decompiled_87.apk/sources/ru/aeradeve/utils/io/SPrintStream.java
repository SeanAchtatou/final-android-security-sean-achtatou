package ru.aeradeve.utils.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.ByteBuffer;

public class SPrintStream extends PrintStream {
    private ByteBuffer buffer = ByteBuffer.allocate(16);
    private int count = 0;
    private byte currentSum = 0;

    public SPrintStream(File file) throws FileNotFoundException {
        super(file);
    }

    public SPrintStream(OutputStream out) {
        super(out);
    }

    private void flushBuffer() {
        if (this.count > 0) {
            super.write(this.buffer.array(), 0, this.count);
            super.write(new byte[]{(byte) (this.currentSum ^ -1)}, 0, 1);
        }
        this.buffer.clear();
        this.count = 0;
        this.currentSum = 0;
    }

    public void write(byte[] buf, int off, int len) {
        int ind = 0;
        while (ind < len) {
            while (this.count < 16 && ind < len) {
                this.buffer.put((byte) (buf[off + ind] ^ -1));
                this.currentSum = (byte) (this.currentSum ^ (buf[off + ind] ^ -1));
                this.count++;
                ind++;
            }
            if (this.count == 16) {
                flushBuffer();
            }
        }
    }

    public void close() {
        flush();
        flushBuffer();
        super.close();
    }
}
