package ru.aeradeve.utils.io;

public class StringLineReader {
    private String buffer = null;
    private int position = 0;

    public StringLineReader(byte[] bytes, int length) {
        this.buffer = new String(bytes, 0, length);
    }

    public boolean hasNextLine() {
        return this.position < this.buffer.length();
    }

    public String nextLine() {
        int pos = this.buffer.indexOf(10, this.position);
        if (pos == -1) {
            pos = this.buffer.length();
        }
        String result = this.buffer.substring(this.position, pos);
        this.position = pos + 1;
        return result;
    }
}
