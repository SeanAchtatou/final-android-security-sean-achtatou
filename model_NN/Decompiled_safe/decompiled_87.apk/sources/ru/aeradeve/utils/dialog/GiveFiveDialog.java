package ru.aeradeve.utils.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;
import ru.aeradeve.games.gravityclumps2.R;
import ru.aeradeve.utils.Util;

public class GiveFiveDialog extends AlertDialog {
    private static final int ANSWER_LATER = 1;
    private static final int ANSWER_NO = 2;
    private static final int ANSWER_YES = 0;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public String mPackageId;

    public GiveFiveDialog(Context context, String pPackageId) {
        super(context);
        this.mContext = context;
        this.mPackageId = pPackageId;
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.setOrientation(0);
        TextView label = new TextView(context);
        label.setPadding(11, 11, 11, 11);
        label.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        label.setText((int) R.string.giveFiveDialogQuestion);
        label.setGravity(17);
        ImageView icon = new ImageView(this.mContext);
        icon.setPadding(11, 11, 11, 11);
        icon.setImageResource(R.drawable.stars);
        icon.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        linearLayout.addView(icon);
        linearLayout.addView(label);
        setView(linearLayout);
        setButton(Util.getString(context, R.string.giveFiveDialogYes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                GiveFiveDialog.this.saveAnswer(0);
                try {
                    GiveFiveDialog.this.mContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + GiveFiveDialog.this.mPackageId)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialogInterface.cancel();
            }
        });
        setButton2(Util.getString(context, R.string.giveFiveDialogNo), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                GiveFiveDialog.this.saveAnswer(2);
                dialogInterface.cancel();
            }
        });
        setButton3(Util.getString(context, R.string.giveFiveDialogLater), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                GiveFiveDialog.this.saveAnswer(1);
                dialogInterface.cancel();
            }
        });
    }

    private static boolean isNeedShow(Context pContext, String pPackageId) {
        try {
            Scanner in = new Scanner(pContext.openFileInput("givefive" + pPackageId.hashCode()));
            int answer = in.nextInt();
            long time = in.nextLong();
            in.close();
            if (answer != 1 || System.currentTimeMillis() - time <= 86400000) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void saveAnswer(int pAnswer) {
        try {
            PrintStream fout = new PrintStream(this.mContext.openFileOutput("givefive" + this.mPackageId.hashCode(), 0));
            fout.println(pAnswer);
            fout.println(System.currentTimeMillis());
            fout.close();
        } catch (FileNotFoundException e) {
        }
    }

    public static void showGiveFiveDialog(final Context pContext, final String pPackageId) {
        if (isNeedShow(pContext, pPackageId)) {
            ((Activity) pContext).runOnUiThread(new Runnable() {
                public void run() {
                    new GiveFiveDialog(pContext, pPackageId).show();
                }
            });
        }
    }
}
