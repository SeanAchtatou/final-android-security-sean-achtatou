package ru.aeradeve.games.gravityclumps2.entity.line;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import ru.aeradeve.games.gravityclumps2.entity.ElementType;

public class BitmapLineFactory {
    private static final String LINE_BLUE_LEFT = "gfx/objects/line_blue_left.png";
    private static final String LINE_BLUE_MIDDLE = "gfx/objects/line_blue_middle.png";
    private static final String LINE_BLUE_RIGHT = "gfx/objects/line_blue_right.png";
    private static final String LINE_GREEN_LEFT = "gfx/objects/line_green_left.png";
    private static final String LINE_GREEN_MIDDLE = "gfx/objects/line_green_middle.png";
    private static final String LINE_GREEN_RIGHT = "gfx/objects/line_green_right.png";
    private static final String LINE_PURPLE_LEFT = "gfx/objects/line_purple_left.png";
    private static final String LINE_PURPLE_MIDDLE = "gfx/objects/line_purple_middle.png";
    private static final String LINE_PURPLE_RIGHT = "gfx/objects/line_purple_right.png";
    private static final String LINE_YELLOW_LEFT = "gfx/objects/line_yellow_left.png";
    private static final String LINE_YELLOW_MIDDLE = "gfx/objects/line_yellow_middle.png";
    private static final String LINE_YELLOW_RIGHT = "gfx/objects/line_yellow_right.png";
    private static BitmapLineFactory ourInstance = new BitmapLineFactory();

    public static BitmapLineFactory getInstance() {
        return ourInstance;
    }

    private BitmapLineFactory() {
    }

    public Bitmap createLine(ElementType pType, int pWidth, Context pContext, boolean withEnds) {
        Bitmap middlePart;
        int i;
        int pWidth2 = Math.max(100, pWidth);
        Bitmap leftPart = getLineLeft(pType, pContext);
        Bitmap middlePart2 = getLineMiddle(pType, pContext);
        Bitmap rightPart = getLineRight(pType, pContext);
        if (!withEnds) {
            middlePart = Bitmap.createScaledBitmap(middlePart2, pWidth2, middlePart2.getHeight(), true);
        } else if ((pWidth2 - leftPart.getWidth()) - rightPart.getWidth() > 0) {
            middlePart = Bitmap.createScaledBitmap(middlePart2, (pWidth2 - leftPart.getWidth()) - rightPart.getWidth(), middlePart2.getHeight(), true);
        } else {
            middlePart = null;
        }
        if (middlePart == null) {
            i = leftPart.getWidth() + rightPart.getWidth();
        } else {
            i = pWidth2;
        }
        Bitmap resultBitmap = Bitmap.createBitmap(i, leftPart.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(resultBitmap);
        if (withEnds) {
            canvas.drawBitmap(leftPart, 0.0f, 0.0f, (Paint) null);
            if (middlePart != null) {
                canvas.drawBitmap(middlePart, (float) leftPart.getWidth(), 0.0f, (Paint) null);
                canvas.drawBitmap(rightPart, (float) (leftPart.getWidth() + middlePart.getWidth()), 0.0f, (Paint) null);
            } else {
                canvas.drawBitmap(rightPart, (float) leftPart.getWidth(), 0.0f, (Paint) null);
            }
        } else {
            canvas.drawBitmap(middlePart, 0.0f, 0.0f, (Paint) null);
        }
        return resultBitmap;
    }

    private Bitmap getLineLeft(ElementType pType, Context pContext) {
        try {
            switch (pType) {
                case SIMPLE:
                    return BitmapFactory.decodeStream(pContext.getAssets().open(LINE_YELLOW_LEFT));
                case ELASTIC:
                    return BitmapFactory.decodeStream(pContext.getAssets().open(LINE_GREEN_LEFT));
                case SLIPPERY:
                    return BitmapFactory.decodeStream(pContext.getAssets().open(LINE_BLUE_LEFT));
                case BASKET:
                    return BitmapFactory.decodeStream(pContext.getAssets().open(LINE_PURPLE_LEFT));
            }
        } catch (Exception e) {
        }
        return null;
    }

    private Bitmap getLineMiddle(ElementType pType, Context pContext) {
        try {
            switch (pType) {
                case SIMPLE:
                    return BitmapFactory.decodeStream(pContext.getAssets().open(LINE_YELLOW_MIDDLE));
                case ELASTIC:
                    return BitmapFactory.decodeStream(pContext.getAssets().open(LINE_GREEN_MIDDLE));
                case SLIPPERY:
                    return BitmapFactory.decodeStream(pContext.getAssets().open(LINE_BLUE_MIDDLE));
                case BASKET:
                    return BitmapFactory.decodeStream(pContext.getAssets().open(LINE_PURPLE_MIDDLE));
            }
        } catch (Exception e) {
        }
        return null;
    }

    private Bitmap getLineRight(ElementType pType, Context pContext) {
        try {
            switch (pType) {
                case SIMPLE:
                    return BitmapFactory.decodeStream(pContext.getAssets().open(LINE_YELLOW_RIGHT));
                case ELASTIC:
                    return BitmapFactory.decodeStream(pContext.getAssets().open(LINE_GREEN_RIGHT));
                case SLIPPERY:
                    return BitmapFactory.decodeStream(pContext.getAssets().open(LINE_BLUE_RIGHT));
                case BASKET:
                    return BitmapFactory.decodeStream(pContext.getAssets().open(LINE_PURPLE_RIGHT));
            }
        } catch (Exception e) {
        }
        return null;
    }
}
