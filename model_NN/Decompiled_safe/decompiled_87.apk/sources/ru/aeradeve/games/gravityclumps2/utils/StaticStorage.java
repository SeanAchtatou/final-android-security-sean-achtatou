package ru.aeradeve.games.gravityclumps2.utils;

import android.graphics.Bitmap;

public class StaticStorage {
    public static int height;
    public static int[] pixels;
    public static int width;

    public static void savePixels(Bitmap pBitmap) {
        width = pBitmap.getWidth();
        height = pBitmap.getHeight();
        pixels = new int[(width * height)];
        pBitmap.getPixels(pixels, 0, width, 0, 0, width, height);
    }
}
