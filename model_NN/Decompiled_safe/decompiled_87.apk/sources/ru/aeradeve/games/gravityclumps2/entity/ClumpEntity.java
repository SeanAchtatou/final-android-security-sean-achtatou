package ru.aeradeve.games.gravityclumps2.entity;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class ClumpEntity extends AnimatedSprite {
    private static final float ADDED_RADIUS = 2.0f;
    private static final float MAX_OVERLAP_RADIUS = 5.0f;
    private ClumpType mClumpType;
    private boolean mDie;

    public ClumpEntity(float pX, float pY, TiledTextureRegion pTiledTextureRegion, ClumpType clumpType) {
        super(pX, pY, pTiledTextureRegion);
        this.mClumpType = clumpType;
    }

    public boolean collidesWith(IShape pOtherShape) {
        if (!(pOtherShape instanceof ClumpEntity)) {
            return super.collidesWith(pOtherShape);
        }
        ClumpEntity otherCircle = (ClumpEntity) pOtherShape;
        double d = (double) (((getCenterX() - otherCircle.getCenterX()) * (getCenterX() - otherCircle.getCenterX())) + ((getCenterY() - otherCircle.getCenterY()) * (getCenterY() - otherCircle.getCenterY())));
        return ((double) ((getRadius() + otherCircle.getRadius()) * (getRadius() + otherCircle.getRadius()))) > d && ((double) ((getOverlapRadius() + otherCircle.getOverlapRadius()) * (getOverlapRadius() + otherCircle.getOverlapRadius()))) < d;
    }

    public float getCenterX() {
        return getX() + getScaleCenterX();
    }

    public float getCenterY() {
        return getY() + getScaleCenterY();
    }

    public float getRadius() {
        return (getWidthScaled() / ADDED_RADIUS) + ADDED_RADIUS;
    }

    public float getOverlapRadius() {
        return (getWidthScaled() / ADDED_RADIUS) - MAX_OVERLAP_RADIUS;
    }

    public ClumpType getClumpType() {
        return this.mClumpType;
    }

    public void setClumpType(ClumpType mClumpType2) {
        this.mClumpType = mClumpType2;
    }

    public boolean isDie() {
        return this.mDie;
    }

    public void setDie(boolean mDie2) {
        this.mDie = mDie2;
    }
}
