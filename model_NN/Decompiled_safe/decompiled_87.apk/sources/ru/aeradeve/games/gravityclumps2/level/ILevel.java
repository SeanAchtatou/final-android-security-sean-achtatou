package ru.aeradeve.games.gravityclumps2.level;

import android.content.Context;
import com.badlogic.gdx.physics.box2d.Body;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
import ru.aeradeve.games.gravityclumps2.entity.WorldEntity;
import ru.aeradeve.games.gravityclumps2.utils.Resources;

public interface ILevel {
    void createLines(Scene scene, IEntity iEntity, boolean z);

    void destroy(IEntity iEntity);

    WorldEntity getBarrierByBody(Body body);

    void initialize(String str, Resources resources, PhysicsWorld physicsWorld, Context context, float f, float f2);
}
