package ru.aeradeve.games.gravityclumps2.service;

import android.content.Context;

public interface LevelService {
    int loadBubbleCount(Context context, int i);

    String loadDescription(Context context, int i);

    int loadNeedScore(Context context, int i);

    boolean save(String str, String str2);
}
