package ru.aeradeve.games.gravityclumps2.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import ru.aeradeve.games.gravityclumps2.R;
import ru.aeradeve.games.gravityclumps2.rating.GameInfo;
import ru.aeradeve.games.gravityclumps2.service.GlobalFunctions;
import ru.aeradeve.utils.ad.AdViewSingle;

public class MainMenuActivity extends Activity {
    private static final int ENTER_NAME_DIALOG = 0;
    private DialogInterface.OnClickListener onClickApply = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialogInterface, int i) {
            String nickname = ((EnterNickNameDialog) dialogInterface).getValue().trim();
            if (nickname == null || nickname.length() <= 0 || nickname.length() > 10) {
                Toast.makeText(MainMenuActivity.this, MainMenuActivity.this.getResources().getString(R.string.invalidNickname), 1).show();
                return;
            }
            GameInfo gameInfo = new GameInfo(MainMenuActivity.this);
            if (gameInfo.getNickName() == null || gameInfo.getNickName().length() <= 0 || gameInfo.isNickNoname()) {
                gameInfo.setNickName(nickname);
                gameInfo.save();
                MainMenuActivity.this.startActivityForResult(new Intent(MainMenuActivity.this, SelectLevelActivity.class), 0);
                return;
            }
            gameInfo.setNickName(nickname);
            gameInfo.save();
            Toast.makeText(MainMenuActivity.this, MainMenuActivity.this.getString(R.string.success), 1).show();
        }
    };
    private DialogInterface.OnClickListener onClickCancel = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialogInterface, int i) {
        }
    };

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        if (id != 0) {
            return null;
        }
        String nickname = getResources().getString(R.string.noname);
        GameInfo gameInfo = new GameInfo(this);
        if (!gameInfo.isNickNoname()) {
            nickname = gameInfo.getNickName();
        }
        if (gameInfo.getNickName() == null || gameInfo.getNickName().length() == 0) {
            nickname = getString(R.string.noname);
        }
        return new EnterNickNameDialog(this, nickname, this.onClickApply, this.onClickCancel);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        GlobalFunctions.init(this);
        ((ImageView) findViewById(R.id.startGameButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    GameInfo gameInfo = new GameInfo(MainMenuActivity.this);
                    if (gameInfo.getNickName() == null || gameInfo.getNickName().length() <= 0 || gameInfo.isNickNoname()) {
                        MainMenuActivity.this.showDialog(0);
                        return;
                    }
                    MainMenuActivity.this.startActivityForResult(new Intent(MainMenuActivity.this, SelectLevelActivity.class), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        ((ImageView) findViewById(R.id.changeNameButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MainMenuActivity.this.showDialog(0);
            }
        });
        ((ImageView) findViewById(R.id.top100Button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MainMenuActivity.this.openTable(true);
            }
        });
        ((ImageView) findViewById(R.id.yourScoreButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MainMenuActivity.this.openTable(false);
            }
        });
        ((ImageView) findViewById(R.id.exitButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MainMenuActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: private */
    public void openTable(boolean top) {
        Intent startTableScoreIntent = new Intent(this, TableScoresActivity.class);
        startTableScoreIntent.putExtra("top", top);
        startActivityForResult(startTableScoreIntent, 0);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        AdViewSingle.onDestroy();
        super.onDestroy();
    }
}
