package ru.aeradeve.games.gravityclumps2.source;

import android.content.Context;
import android.graphics.Bitmap;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import ru.aeradeve.games.gravityclumps2.entity.ElementType;
import ru.aeradeve.games.gravityclumps2.entity.line.BitmapLineFactory;

public class LineTextureSource implements ITextureSource {
    private Bitmap mBitmap = BitmapLineFactory.getInstance().createLine(this.mType, this.mWidth, this.mContext, true);
    private Context mContext;
    private ElementType mType;
    private int mWidth;
    private boolean withEnds;

    public LineTextureSource(ElementType pType, int pWidth, Context pContext, boolean withEnds2) {
        this.mContext = pContext;
        this.mType = pType;
        this.mWidth = pWidth;
        this.withEnds = withEnds2;
    }

    public int getWidth() {
        return this.mBitmap.getWidth();
    }

    public int getHeight() {
        return this.mBitmap.getHeight();
    }

    public ITextureSource clone() {
        return null;
    }

    public Bitmap onLoadBitmap() {
        this.mBitmap = BitmapLineFactory.getInstance().createLine(this.mType, this.mWidth, this.mContext, true);
        return this.mBitmap;
    }
}
