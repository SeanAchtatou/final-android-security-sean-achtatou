package ru.aeradeve.games.gravityclumps2.entity;

import android.content.Intent;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.input.touch.TouchEvent;
import ru.aeradeve.games.gravityclumps2.activity.GameActivity;
import ru.aeradeve.games.gravityclumps2.scene.SelectScene;

public class LevelButton extends Entity implements Scene.ITouchArea {
    private IShape mActiveBackground;
    private float mCenterX;
    private float mCenterY;
    private IShape mErrorBackground;
    private float mLeft;
    /* access modifiers changed from: private */
    public int mLevel;
    /* access modifiers changed from: private */
    public SelectScene mScene;
    private int mScore;
    private IShape mScoreText;
    private boolean mSelected = false;
    private IShape mTextOrLock;
    private float mTop;
    private IShape mUnactiveBackground;

    public LevelButton(SelectScene pScene, int pLevel, int pScore) {
        this.mScene = pScene;
        this.mScore = pScore;
        this.mActiveBackground = new Sprite(0.0f, 0.0f, this.mScene.getTEnv().mButtons[1]);
        this.mUnactiveBackground = new Sprite(0.0f, 0.0f, this.mScene.getTEnv().mButtons[0]);
        this.mErrorBackground = new Sprite(0.0f, 0.0f, this.mScene.getTEnv().mButtons[2]);
        this.mErrorBackground.setAlpha(0.0f);
        this.mLevel = pLevel;
        if (pScore >= 0) {
            this.mTextOrLock = new ChangeableText(0.0f, 0.0f, this.mScene.getTEnv().mFontButton, String.valueOf(this.mLevel), 5);
            this.mScoreText = new ChangeableText(0.0f, 0.0f, this.mScene.getTEnv().mFontScore, String.valueOf(this.mScore), 5);
            return;
        }
        this.mTextOrLock = new Sprite(0.0f, 0.0f, this.mScene.getTEnv().mButtons[3]);
    }

    public void setCenter(float pX, float pY) {
        this.mCenterX = pX;
        this.mCenterY = pY;
        this.mLeft = this.mCenterX - (getWidth() / 2.0f);
        this.mTop = this.mCenterY - (getHeight() / 2.0f);
        this.mActiveBackground.setPosition(this.mLeft, this.mTop);
        this.mUnactiveBackground.setPosition(this.mLeft, this.mTop);
        this.mErrorBackground.setPosition(this.mLeft, this.mTop);
        updatePositionCount();
    }

    public float getWidth() {
        return this.mActiveBackground.getWidth();
    }

    public float getHeight() {
        return this.mActiveBackground.getHeight();
    }

    public void setSelected(boolean pSelected) {
        this.mSelected = pSelected;
    }

    public boolean isSelected() {
        return this.mSelected;
    }

    public void showError() {
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 gl10, Camera camera) {
        if (!this.mSelected) {
            this.mUnactiveBackground.onDraw(gl10, camera);
        } else {
            this.mActiveBackground.onDraw(gl10, camera);
        }
        this.mErrorBackground.onDraw(gl10, camera);
        this.mTextOrLock.onDraw(gl10, camera);
        if (this.mScoreText != null) {
            this.mScoreText.onDraw(gl10, camera);
        }
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float pSeconds) {
        this.mActiveBackground.onUpdate(pSeconds);
        this.mUnactiveBackground.onUpdate(pSeconds);
        this.mErrorBackground.onUpdate(pSeconds);
        this.mTextOrLock.onUpdate(pSeconds);
        if (this.mScoreText != null) {
            this.mScoreText.onUpdate(pSeconds);
        }
    }

    public boolean contains(float pX, float pY) {
        return Math.abs(pX - this.mCenterX) * 2.0f <= getWidth() && Math.abs(pY - this.mCenterY) * 2.0f <= getHeight();
    }

    public float[] convertSceneToLocalCoordinates(float pX, float pY) {
        return new float[]{pX - this.mLeft, pY - this.mTop};
    }

    public float[] convertLocalToSceneCoordinates(float pLocalX, float pLocalY) {
        return new float[]{this.mLeft + pLocalX, this.mTop + pLocalY};
    }

    public boolean onAreaTouched(TouchEvent pTouchEvent, float pLocalX, float pLocalY) {
        if (pTouchEvent.getAction() == 0 && this.mScore >= 0) {
            this.mSelected = true;
        }
        if (pTouchEvent.getAction() == 1) {
            if (this.mSelected) {
                this.mScene.hide(new Runnable() {
                    public void run() {
                        Intent startGameIntent = new Intent(LevelButton.this.mScene.getContext(), GameActivity.class);
                        startGameIntent.putExtra(GameActivity.EXT_LEVEL_ID, LevelButton.this.mLevel);
                        LevelButton.this.mScene.getContext().startActivity(startGameIntent);
                    }
                });
            }
            this.mSelected = true;
        }
        return true;
    }

    private void updatePositionCount() {
        this.mTextOrLock.setPosition(this.mLeft + ((getWidth() - this.mTextOrLock.getWidth()) / 2.0f), this.mTop + ((getHeight() - this.mTextOrLock.getHeight()) / 2.0f));
        if (this.mScoreText != null) {
            this.mScoreText.setPosition(this.mLeft + ((getWidth() - this.mScoreText.getWidth()) / 2.0f), this.mTop + getHeight());
        }
    }

    public void hide() {
    }
}
