package ru.aeradeve.games.gravityclumps2.utils;

import java.util.Random;

public class GlobalVariables {
    public static final String ADMOB_ID = "a14e2da0fba1b34";
    public static final float FLY_TEXT_SPEED = 50.0f;
    public static final String playerInfoFileName = "playerInfo.txt";
    public static final Random random = new Random();
    public static final String scoreFileName = "score.txt";
}
