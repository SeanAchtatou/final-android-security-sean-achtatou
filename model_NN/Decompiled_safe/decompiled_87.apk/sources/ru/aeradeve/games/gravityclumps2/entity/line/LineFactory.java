package ru.aeradeve.games.gravityclumps2.entity.line;

import android.content.Context;
import android.graphics.Bitmap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import ru.aeradeve.games.gravityclumps2.entity.DitherSprite;
import ru.aeradeve.games.gravityclumps2.entity.ElementType;
import ru.aeradeve.games.gravityclumps2.math.MathFunctions;
import ru.aeradeve.games.gravityclumps2.source.LineTextureSource;
import ru.aeradeve.games.gravityclumps2.utils.Resources;

public class LineFactory {
    private static final FixtureDef ELASTIC_BLOCK_FIXTURE_DEF = PhysicsFactory.createFixtureDef(2.0f, 1.3f, 0.0f);
    private static final FixtureDef SIMPLE_BLOCK_FIXTURE_DEF = PhysicsFactory.createFixtureDef(2.0f, 0.3f, 0.0f);
    private static final FixtureDef SLIPPERY_BLOCK_FIXTURE_DEF = PhysicsFactory.createFixtureDef(2.0f, 0.3f, 0.0f);
    private static LineFactory ourInstance = new LineFactory();

    public static LineFactory getInstance() {
        return ourInstance;
    }

    private LineFactory() {
    }

    public Sprite createLineSprite(Vector2 startPoint, Vector2 finalPoint, TextureRegion textureRegion, Resources pResource, Context pContext) {
        DitherSprite sprite = new DitherSprite(startPoint.x, startPoint.y, textureRegion);
        sprite.setScaleX(startPoint.dst(finalPoint) / sprite.getWidth());
        rotateBlock(sprite, startPoint, finalPoint);
        sprite.setScaleY(2.0f);
        return sprite;
    }

    private Body createLineBody(PhysicsWorld pPhysicsWorld, Sprite pSprite, ElementType pType, float pFactor) {
        float angle = pSprite.getRotation();
        float hwidth = ((pSprite.getWidthScaled() * 0.5f) - (15.0f * pFactor)) / 32.0f;
        Body body = PhysicsFactory.createPolygonBody(pPhysicsWorld, pSprite, new Vector2[]{new Vector2(-hwidth, -0.21875f), new Vector2(hwidth, -0.21875f), new Vector2(hwidth, 0.21875f), new Vector2(-hwidth, 0.21875f)}, BodyDef.BodyType.StaticBody, createFixtureDef(pType));
        body.setTransform(body.getPosition(), (float) Math.toRadians((double) angle));
        return body;
    }

    public LineEntity createLine(Vector2 startPoint, Vector2 finalPoint, TextureRegion textureRegion, ElementType pType, float scaleY, PhysicsWorld pPhysicsWorld, float pPhysFactor) {
        LineEntity lineEntity = new LineEntity();
        lineEntity.setType(pType);
        DitherSprite sprite = new DitherSprite(startPoint.x, startPoint.y, textureRegion);
        lineEntity.setSprite(sprite);
        sprite.setScaleX((float) (MathFunctions.getInstance().distance(startPoint, finalPoint) / ((double) sprite.getWidth())));
        rotateBlock(sprite, startPoint, finalPoint);
        sprite.setScaleY(scaleY);
        if (pPhysicsWorld != null) {
            Body body = createLineBody(pPhysicsWorld, sprite, pType, pPhysFactor);
            pPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(sprite, body, true, true));
            lineEntity.setBody(body);
        }
        return lineEntity;
    }

    public LineEntity createLine(Vector2 startPoint, Vector2 finalPoint, ElementType pType, Resources pResource, Context pContext, PhysicsWorld pPhysicsWorld, float pScale, boolean pEdit) {
        TextureRegion textureRegion = TextureRegionFactory.createFromSource(pResource.mBuildableTexture, new LineTextureSource(pType, (int) startPoint.dst(finalPoint), pContext, true));
        float scaleFactor = startPoint.dst(finalPoint) / ((float) (textureRegion.getWidth() - (pType == ElementType.BASKET ? 32 : 32)));
        Vector2 v = new Vector2(finalPoint.x - startPoint.x, finalPoint.y - startPoint.y).nor().mul(20.0f * scaleFactor);
        Vector2 vector2 = new Vector2(finalPoint);
        vector2.x += v.x;
        vector2.y += v.y;
        Vector2 vector22 = new Vector2(startPoint);
        vector22.x -= v.x;
        vector22.y -= v.y;
        return createLine(vector22, vector2, textureRegion, pType, pScale, pPhysicsWorld, scaleFactor);
    }

    /* access modifiers changed from: protected */
    public void rotateBlock(Sprite sprite, Vector2 startPoint, Vector2 finalPoint) {
        float spriteWidth = sprite.getWidth();
        float littleRadius = (sprite.getHeight() * sprite.getScaleY()) / 2.0f;
        float rotateRadius = (spriteWidth / 2.0f) - littleRadius;
        double rotateAngle = MathFunctions.getInstance().angleBetweenVectors(new Vector2(1.0f, 0.0f), new Vector2(finalPoint.x - startPoint.x, -(finalPoint.y - startPoint.y)));
        if (new Double(rotateAngle).isNaN()) {
            rotateAngle = 0.0d;
        }
        Vector2 rotatedVector = MathFunctions.getInstance().rotateVector(new Vector2(-rotateRadius, 0.0f), rotateAngle);
        rotatedVector.x += rotateRadius;
        rotatedVector.y = -rotatedVector.y;
        sprite.setRotation((float) rotateAngle);
        float scaledChangeWidth = ((spriteWidth - (sprite.getScaleX() * spriteWidth)) / 2.0f) - littleRadius;
        sprite.setPosition(((startPoint.x - rotatedVector.x) - littleRadius) - ((float) (Math.cos(Math.toRadians(rotateAngle)) * ((double) scaledChangeWidth))), ((startPoint.y - rotatedVector.y) - littleRadius) - ((float) (Math.sin(Math.toRadians(rotateAngle)) * ((double) scaledChangeWidth))));
    }

    public FixtureDef createFixtureDef(ElementType pType) {
        switch (pType) {
            case SIMPLE:
                return SIMPLE_BLOCK_FIXTURE_DEF;
            case ELASTIC:
                return ELASTIC_BLOCK_FIXTURE_DEF;
            case SLIPPERY:
                return SLIPPERY_BLOCK_FIXTURE_DEF;
            default:
                return SLIPPERY_BLOCK_FIXTURE_DEF;
        }
    }

    public static class TextureSource implements ITextureSource {
        private Bitmap mBitmap;

        public TextureSource(Bitmap pBitmap) {
            this.mBitmap = pBitmap;
        }

        public int getWidth() {
            return this.mBitmap.getWidth();
        }

        public int getHeight() {
            return this.mBitmap.getHeight();
        }

        public ITextureSource clone() {
            return new TextureSource(Bitmap.createBitmap(this.mBitmap));
        }

        public Bitmap onLoadBitmap() {
            return this.mBitmap;
        }
    }
}
