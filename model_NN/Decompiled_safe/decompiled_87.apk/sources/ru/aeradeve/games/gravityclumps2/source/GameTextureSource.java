package ru.aeradeve.games.gravityclumps2.source;

import android.graphics.Bitmap;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import ru.aeradeve.games.gravityclumps2.utils.StaticStorage;

public class GameTextureSource implements ITextureSource {
    public int getWidth() {
        return StaticStorage.width;
    }

    public int getHeight() {
        return StaticStorage.height;
    }

    public ITextureSource clone() {
        return null;
    }

    public Bitmap onLoadBitmap() {
        return Bitmap.createBitmap((int[]) StaticStorage.pixels.clone(), 0, StaticStorage.width, StaticStorage.width, StaticStorage.height, Bitmap.Config.ARGB_8888);
    }
}
