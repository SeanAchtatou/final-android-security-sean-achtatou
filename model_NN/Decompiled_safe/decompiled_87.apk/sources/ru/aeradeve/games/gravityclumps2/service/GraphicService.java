package ru.aeradeve.games.gravityclumps2.service;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.ui.activity.BaseGameActivity;
import ru.aeradeve.games.gravityclumps2.entity.DitherSprite;

public class GraphicService {
    private static GraphicService ourInstance = new GraphicService();

    public static GraphicService getInstance() {
        return ourInstance;
    }

    private GraphicService() {
    }

    public void repeatTileLayer(IEntity pLayer, int pWidth, int pHeight, TextureRegion pTexture) {
        float x = 0.0f;
        float y = 0.0f;
        while (y < ((float) pHeight)) {
            if (x > ((float) pWidth)) {
                x = 0.0f;
                y += (float) pTexture.getHeight();
            }
            pLayer.attachChild(new DitherSprite(x, y, pTexture));
            x += (float) pTexture.getWidth();
        }
    }

    public void clearLayer(final BaseGameActivity pGameActivity, final IEntity pLayer, final Runnable pAfterClear) {
        pGameActivity.runOnUpdateThread(new Runnable() {
            public void run() {
                for (int i = pLayer.getChildCount() - 1; i >= 0; i--) {
                    IEntity entity = pLayer.getChild(i);
                    if (entity instanceof Scene.ITouchArea) {
                        pGameActivity.getEngine().getScene().unregisterTouchArea((Scene.ITouchArea) entity);
                    }
                    pLayer.detachChild(pLayer.getChild(i));
                }
                if (pAfterClear != null) {
                    pAfterClear.run();
                }
            }
        });
    }

    public void clearLayer(BaseGameActivity pGameActivity, IEntity pLayer) {
        clearLayer(pGameActivity, pLayer, null);
    }
}
