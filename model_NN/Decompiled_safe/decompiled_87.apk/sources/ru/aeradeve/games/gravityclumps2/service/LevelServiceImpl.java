package ru.aeradeve.games.gravityclumps2.service;

import android.content.Context;
import java.io.IOException;
import java.util.Scanner;

public class LevelServiceImpl implements LevelService {
    public static final String LEVELS_PATH = "levels";
    public static final String LEVEL_FILE_EXTENSION = ".level";
    private static LevelServiceImpl ourInstance = new LevelServiceImpl();

    public static LevelServiceImpl getInstance() {
        return ourInstance;
    }

    private LevelServiceImpl() {
    }

    public boolean save(String pLevelName, String pLevelBody) {
        return true;
    }

    public String loadDescription(Context pContext, int pLevelId) {
        try {
            Scanner in = new Scanner(pContext.getAssets().open("levels/" + pLevelId + LEVEL_FILE_EXTENSION));
            in.nextLine();
            in.nextLine();
            return in.nextLine();
        } catch (IOException e) {
            return "";
        }
    }

    public int loadNeedScore(Context pContext, int pLevelId) {
        try {
            Scanner in = new Scanner(pContext.getAssets().open("levels/" + pLevelId + LEVEL_FILE_EXTENSION));
            in.nextLine();
            return Integer.parseInt(in.nextLine());
        } catch (IOException e) {
            return 444;
        }
    }

    public int loadBubbleCount(Context pContext, int pLevelId) {
        try {
            return Integer.parseInt(new Scanner(pContext.getAssets().open("levels/" + pLevelId + LEVEL_FILE_EXTENSION)).nextLine());
        } catch (IOException e) {
            return 70;
        }
    }
}
