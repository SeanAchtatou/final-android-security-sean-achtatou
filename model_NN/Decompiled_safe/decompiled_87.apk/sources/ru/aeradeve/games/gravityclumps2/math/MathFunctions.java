package ru.aeradeve.games.gravityclumps2.math;

import com.badlogic.gdx.math.Vector2;
import org.anddev.andengine.entity.sprite.Sprite;

public class MathFunctions {
    private static final float EPS = 1.0E-5f;
    private static MathFunctions ourInstance = new MathFunctions();

    public static MathFunctions getInstance() {
        return ourInstance;
    }

    private MathFunctions() {
    }

    public double angleBetweenVectors(Vector2 firstVector, Vector2 secondVector) {
        double alpha = Math.acos(((double) ((firstVector.x * secondVector.x) + (firstVector.y * secondVector.y))) / (Math.sqrt((double) ((firstVector.x * firstVector.x) + (firstVector.y * firstVector.y))) * Math.sqrt((double) ((secondVector.x * secondVector.x) + (secondVector.y * secondVector.y)))));
        if (((double) ((firstVector.x * secondVector.y) - (secondVector.x * firstVector.y))) < 0.0d) {
            return Math.toDegrees(alpha);
        }
        return 360.0d - Math.toDegrees(alpha);
    }

    public Vector2 rotateVector(Vector2 vector, double angle) {
        double angleRadians = Math.toRadians(angle);
        double cosAlpha = Math.cos(angleRadians);
        double sinAlpha = Math.sin(angleRadians);
        Vector2 rotatedVector = new Vector2();
        rotatedVector.x = (float) ((((double) vector.x) * cosAlpha) + (((double) vector.y) * sinAlpha));
        rotatedVector.y = (float) ((((double) vector.x) * (-sinAlpha)) + (((double) vector.y) * cosAlpha));
        return rotatedVector;
    }

    public double distance(Vector2 firstPoint, Vector2 secondPoint) {
        return Math.sqrt((double) (((firstPoint.x - secondPoint.x) * (firstPoint.x - secondPoint.x)) + ((firstPoint.y - secondPoint.y) * (firstPoint.y - secondPoint.y))));
    }

    public Vector2 getSpriteCenter(Sprite pSprite) {
        return new Vector2(pSprite.getX() + (pSprite.getWidth() / 2.0f), pSprite.getY() + (pSprite.getHeight() / 2.0f));
    }

    public boolean equals(float x, float y, float eps) {
        return Math.abs(x - y) < eps;
    }

    public boolean equals(float x, float y) {
        return equals(x, y, EPS);
    }

    public boolean isPointInside(Vector2 pPoint, Rectangle pRectangle) {
        if (pPoint == null || pRectangle == null) {
            return false;
        }
        return (pPoint.x > pRectangle.getLeftTopPoint().x || equals(pRectangle.getLeftTopPoint().x, pPoint.x)) && (pPoint.x < pRectangle.getRightBottomPoint().x || equals(pRectangle.getRightBottomPoint().x, pPoint.x)) && ((pPoint.y > pRectangle.getLeftTopPoint().y || equals(pRectangle.getLeftTopPoint().y, pPoint.y)) && (pPoint.y < pRectangle.getRightBottomPoint().y || equals(pRectangle.getRightBottomPoint().y, pPoint.y)));
    }

    public Vector2 normalizeVector(Vector2 pVector) {
        double vectorLength = distance(new Vector2(0.0f, 0.0f), pVector);
        Vector2 normalizeVector = new Vector2(pVector);
        normalizeVector.x = (float) (((double) normalizeVector.x) / vectorLength);
        normalizeVector.y = (float) (((double) normalizeVector.y) / vectorLength);
        return normalizeVector;
    }

    public boolean isCirclesIntersect(Vector2 pFirstCenter, float pFirstRadius, Vector2 pSecondCenter, float pSecondRadius) {
        return pFirstCenter.dst(pSecondCenter) < pFirstRadius + pSecondRadius;
    }
}
