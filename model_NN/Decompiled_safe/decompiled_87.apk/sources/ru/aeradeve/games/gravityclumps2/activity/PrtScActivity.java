package ru.aeradeve.games.gravityclumps2.activity;

import android.view.KeyEvent;
import android.widget.Toast;
import org.anddev.andengine.entity.util.ScreenCapture;

public abstract class PrtScActivity extends AdsGameActivity {
    final ScreenCapture screenCapture = new ScreenCapture();

    public void onLoadComplete() {
        this.mEngine.getScene().attachChild(this.screenCapture);
    }

    public boolean onKeyDown(int pKeyCode, KeyEvent pEvent) {
        if (pKeyCode != 82 || pEvent.getAction() != 0) {
            return super.onKeyDown(pKeyCode, pEvent);
        }
        this.screenCapture.capture(this.mRenderSurfaceView.getWidth(), this.mRenderSurfaceView.getHeight(), "/sdcard/Screen_" + System.currentTimeMillis() + ".png", new ScreenCapture.IScreenCaptureCallback() {
            public void onScreenCaptured(final String pFilePath) {
                PrtScActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(PrtScActivity.this, "Screenshot: " + pFilePath + " taken!", 0).show();
                    }
                });
            }

            public void onScreenCaptureFailed(final String pFilePath, Exception pException) {
                PrtScActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(PrtScActivity.this, "FAILED capturing Screenshot: " + pFilePath + " !", 0).show();
                    }
                });
            }
        });
        return true;
    }
}
