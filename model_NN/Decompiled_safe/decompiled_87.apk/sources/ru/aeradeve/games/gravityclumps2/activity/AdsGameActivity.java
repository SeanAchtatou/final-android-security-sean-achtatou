package ru.aeradeve.games.gravityclumps2.activity;

import org.anddev.andengine.opengl.view.RenderSurfaceView;
import org.anddev.andengine.ui.activity.LayoutGameActivity;
import ru.aeradeve.games.gravityclumps2.R;

public abstract class AdsGameActivity extends LayoutGameActivity {
    public abstract boolean isLandscape();

    /* access modifiers changed from: protected */
    public int getLayoutID() {
        return R.layout.game;
    }

    /* access modifiers changed from: protected */
    public int getRenderSurfaceViewID() {
        return R.id.gameRenderSurfaceView;
    }

    /* access modifiers changed from: protected */
    public void onSetContentView() {
        super.setContentView(getLayoutID());
        this.mRenderSurfaceView = (RenderSurfaceView) findViewById(getRenderSurfaceViewID());
        this.mRenderSurfaceView.setEGLConfigChooser(false);
        this.mRenderSurfaceView.setRenderer(this.mEngine);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
