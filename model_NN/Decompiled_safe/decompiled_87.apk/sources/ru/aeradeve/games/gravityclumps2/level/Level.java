package ru.aeradeve.games.gravityclumps2.level;

import android.content.Context;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
import org.anddev.andengine.opengl.texture.builder.BlackPawnTextureBuilder;
import org.anddev.andengine.opengl.texture.builder.ITextureBuilder;
import ru.aeradeve.games.gravityclumps2.entity.ElementType;
import ru.aeradeve.games.gravityclumps2.entity.WorldEntity;
import ru.aeradeve.games.gravityclumps2.entity.line.LineEntity;
import ru.aeradeve.games.gravityclumps2.entity.line.LineFactory;
import ru.aeradeve.games.gravityclumps2.utils.Resources;

public class Level implements ILevel {
    public static final int LEVEL_COUNT = 30;
    private List<String[]> mBallsDescription = new ArrayList();
    private HashMap<Body, WorldEntity> mBarrierMap;
    private Context mContext;
    private IEntity mLayer;
    private ArrayList<LineEntity> mLines;
    private PhysicsWorld mPhysicsWorld;
    private Resources mResources;
    private Scene mScene;

    public void initialize(String pData, Resources pResources, PhysicsWorld pPhysicsWorld, Context pContext, float pCameraWidth, float pCameraHeight) {
        this.mResources = pResources;
        this.mPhysicsWorld = pPhysicsWorld;
        this.mContext = pContext;
        this.mLines = new ArrayList<>();
        this.mBarrierMap = new HashMap<>();
        LineFactory lineFactory = LineFactory.getInstance();
        this.mLines.add(lineFactory.createLine(new Vector2(0.0f, pCameraHeight), new Vector2(pCameraWidth, pCameraHeight), ElementType.SIMPLE, this.mResources, pContext, this.mPhysicsWorld, 2.0f, false));
        this.mLines.add(lineFactory.createLine(new Vector2(0.0f, 0.0f), new Vector2(pCameraWidth, 0.0f), ElementType.SIMPLE, this.mResources, pContext, this.mPhysicsWorld, 2.0f, false));
        this.mLines.add(lineFactory.createLine(new Vector2(0.0f, 0.0f), new Vector2(0.0f, pCameraHeight), ElementType.SIMPLE, this.mResources, pContext, this.mPhysicsWorld, 2.0f, false));
        this.mLines.add(lineFactory.createLine(new Vector2(pCameraWidth, 0.0f), new Vector2(pCameraWidth, pCameraHeight), ElementType.SIMPLE, this.mResources, pContext, this.mPhysicsWorld, 2.0f, false));
        for (String object : pData.split(";")) {
            if (!(object == null || object.length() == 0)) {
                String[] description = object.split(" ");
                if ("L".equals(description[0])) {
                    parseLine(description);
                }
            }
        }
        try {
            this.mResources.mBuildableTexture.build(new BlackPawnTextureBuilder(1));
        } catch (ITextureBuilder.TextureSourcePackingException e) {
        }
    }

    private void parseLine(String[] pDescription) {
        this.mLines.add(LineFactory.getInstance().createLine(new Vector2((float) Integer.parseInt(pDescription[1]), (float) Integer.parseInt(pDescription[2])), new Vector2((float) Integer.parseInt(pDescription[3]), (float) Integer.parseInt(pDescription[4])), ElementType.valueOf(pDescription[5]), this.mResources, this.mContext, this.mPhysicsWorld, 2.0f, false));
    }

    public void createLines(Scene pScene, IEntity pLayer, boolean pAddSprite) {
        this.mScene = pScene;
        this.mLayer = pLayer;
        if (this.mLines != null) {
            Iterator i$ = this.mLines.iterator();
            while (i$.hasNext()) {
                LineEntity lineEntity = i$.next();
                if (pAddSprite) {
                    createWorldEntity(lineEntity, false);
                }
                this.mBarrierMap.put(lineEntity.getBody(), lineEntity);
            }
        }
    }

    public void destroy(IEntity pBallLayer) {
        if (this.mLines != null) {
            Iterator i$ = this.mLines.iterator();
            while (i$.hasNext()) {
                removeWorldEntity(i$.next());
            }
            this.mLines.clear();
        }
    }

    public WorldEntity getBarrierByBody(Body body) {
        return this.mBarrierMap.get(body);
    }

    private void createWorldEntity(WorldEntity pWorldEntity, boolean pIsTouchable) {
        createWorldEntity(pWorldEntity, pIsTouchable, this.mLayer);
    }

    private void createWorldEntity(WorldEntity pWorldEntity, boolean pIsTouchable, IEntity pLayer) {
        if (pLayer == null) {
            pLayer = this.mLayer;
        }
        pLayer.attachChild(pWorldEntity.getSprite());
        if (pIsTouchable) {
            this.mScene.registerTouchArea(pWorldEntity.getSprite());
        }
    }

    private void removeWorldEntity(WorldEntity pWorldEntity) {
        removeWorldEntity(pWorldEntity, this.mLayer);
    }

    private void removeWorldEntity(WorldEntity pWorldEntity, IEntity pLayer) {
        if (pLayer == null) {
            pLayer = this.mLayer;
        }
        this.mScene.unregisterTouchArea(pWorldEntity.getSprite());
        pLayer.detachChild(pWorldEntity.getSprite());
        PhysicsConnector spritePhysicsConnector = this.mPhysicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(pWorldEntity.getSprite());
        if (spritePhysicsConnector != null) {
            this.mPhysicsWorld.unregisterPhysicsConnector(spritePhysicsConnector);
            this.mPhysicsWorld.destroyBody(spritePhysicsConnector.getBody());
        }
    }

    public ArrayList<LineEntity> getLines() {
        return this.mLines;
    }
}
