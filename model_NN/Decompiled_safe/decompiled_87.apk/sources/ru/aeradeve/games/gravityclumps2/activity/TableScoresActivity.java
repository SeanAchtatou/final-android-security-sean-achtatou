package ru.aeradeve.games.gravityclumps2.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import ru.aeradeve.games.gravityclumps2.R;
import ru.aeradeve.games.gravityclumps2.rating.GameInfo;
import ru.aeradeve.utils.rating.view.RatingView;

public class TableScoresActivity extends Activity {
    private GameInfo mInfoGame;

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(savedInstanceState);
        this.mInfoGame = new GameInfo(this);
        setContentView((int) R.layout.rating);
        boolean top = getIntent().getExtras().getBoolean("top");
        RatingView rating = new RatingView(this);
        ((LinearLayout) findViewById(R.id.tableLayout)).addView(rating);
        if (!top) {
            rating.getFilter().setFragment(2);
        }
        rating.start(this.mInfoGame);
    }
}
