package ru.aeradeve.games.gravityclumps2.entity;

public enum ClumpType {
    RED,
    GREEN,
    BLUE,
    YELLOW;

    public static int getIndex(ClumpType clumpType) {
        for (int index = 0; index < values().length; index++) {
            if (values()[index] == clumpType) {
                return index;
            }
        }
        return -1;
    }
}
