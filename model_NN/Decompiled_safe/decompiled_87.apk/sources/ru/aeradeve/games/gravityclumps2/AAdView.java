package ru.aeradeve.games.gravityclumps2;

import android.content.Context;
import android.util.AttributeSet;
import ru.aeradeve.games.gravityclumps2.utils.GlobalVariables;
import ru.aeradeve.utils.ad.BaseAdView;
import ru.aeradeve.utils.ad.wrapperad.AdmobWAdView;
import ru.aeradeve.utils.ad.wrapperad.MadWAdView;
import ru.aeradeve.utils.ad.wrapperad.MobclixWAdView;

public class AAdView extends BaseAdView {
    public AAdView(Context pContext, AttributeSet pAttrs) {
        super(pContext, pAttrs, new MadWAdView(pContext, pAttrs), new MobclixWAdView(pContext), new AdmobWAdView(pContext, GlobalVariables.ADMOB_ID));
    }
}
