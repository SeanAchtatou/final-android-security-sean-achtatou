package ru.aeradeve.games.gravityclumps2.activity;

import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.util.FPSLogger;
import ru.aeradeve.games.gravityclumps2.rating.GameInfo;
import ru.aeradeve.games.gravityclumps2.scene.SelectScene;
import ru.aeradeve.games.gravityclumps2.utils.Resources;

public class SelectLevelActivity extends AdsGameActivity {
    private static final int CAMERA_HEIGHT = 640;
    private static final int CAMERA_WIDTH = 480;
    private Camera mCamera;
    private GameInfo mInfoGame;
    private SelectScene mSelectScene;
    private Resources mTEnv = null;

    public Engine onLoadEngine() {
        this.mCamera = new Camera(0.0f, 0.0f, 480.0f, 640.0f);
        return new Engine(new EngineOptions(true, EngineOptions.ScreenOrientation.PORTRAIT, new RatioResolutionPolicy(480.0f, 640.0f), this.mCamera).setNeedsSound(true));
    }

    public void onLoadResources() {
        this.mTEnv = new Resources(this.mEngine, this);
        this.mTEnv.loadSelectLevelResource();
    }

    public Scene onLoadScene() {
        this.mEngine.registerUpdateHandler(new FPSLogger());
        this.mInfoGame = new GameInfo(this);
        this.mSelectScene = new SelectScene(this, this.mTEnv, this.mCamera, this.mInfoGame);
        return this.mSelectScene;
    }

    public void onLoadComplete() {
        this.mSelectScene.show();
    }

    public boolean isLandscape() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mSelectScene != null) {
            this.mSelectScene.show();
        }
    }
}
