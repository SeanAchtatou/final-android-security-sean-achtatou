package ru.aeradeve.games.gravityclumps2.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import ru.aeradeve.games.gravityclumps2.R;

public class EnterNickNameDialog extends AlertDialog {
    private Context context;
    private EditText editText;

    public EnterNickNameDialog(Context context2, String value, DialogInterface.OnClickListener ok, DialogInterface.OnClickListener cancel) {
        super(context2);
        this.context = context2;
        LinearLayout linearLayout = new LinearLayout(context2);
        linearLayout.setPadding(33, 11, 33, 11);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.setOrientation(1);
        this.editText = new EditText(context2);
        this.editText.setSingleLine();
        this.editText.setText(value);
        TextView label = new TextView(context2);
        label.setText((int) R.string.labelNickname);
        TextView label2 = new TextView(context2);
        label2.setText((int) R.string.labelCallKeyboard);
        label2.setTextColor(-65536);
        label2.setTextSize(10.0f);
        label2.setGravity(17);
        linearLayout.addView(label);
        linearLayout.addView(this.editText);
        setView(linearLayout);
        setButton(context2.getResources().getText(R.string.buttonApply), ok);
        setButton2(context2.getResources().getText(R.string.buttonCancel), cancel);
        setTitle((int) R.string.titleEnterNickname);
        setIcon((int) R.drawable.icon);
    }

    public void show() {
        super.show();
        showKeyboard();
    }

    public void showKeyboard() {
        getWindow().setSoftInputMode(5);
    }

    public String getValue() {
        return this.editText.getText().toString();
    }
}
