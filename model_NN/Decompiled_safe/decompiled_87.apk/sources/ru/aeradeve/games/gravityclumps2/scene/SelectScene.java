package ru.aeradeve.games.gravityclumps2.scene;

import android.content.Context;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.entity.modifier.ScaleModifier;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.util.modifier.IModifier;
import ru.aeradeve.games.gravityclumps2.entity.LevelButton;
import ru.aeradeve.games.gravityclumps2.rating.GameInfo;
import ru.aeradeve.games.gravityclumps2.utils.Resources;

public class SelectScene extends BaseScene {
    private Sprite mArrowNext;
    private Sprite mArrowPrev;
    private IShape mBackground;
    private LevelButton[] mButtons;
    private Context mContext;
    private GameInfo mInfoGame;
    private IShape mLoading;
    /* access modifiers changed from: private */
    public int mStartLevel = -1;

    public SelectScene(Context pContext, Resources pEnv, Camera pCamera, GameInfo pInfoGame) {
        super(2, pCamera, pEnv);
        this.mContext = pContext;
        this.mInfoGame = pInfoGame;
        this.mBackground = new Rectangle(pCamera.getMinX(), pCamera.getMinY(), pCamera.getWidth(), pCamera.getHeight());
        this.mBackground.setColor(0.0f, 0.0f, 0.0f, 0.33f);
        IEntity layer = getFirstChild();
        layer.attachChild(this.mBackground);
        this.mLoading = new Text(0.0f, 0.0f, pEnv.mFontButton, "Loading...");
        this.mLoading.setScale(0.0f);
        layer.attachChild(this.mLoading);
        this.mArrowPrev = new Sprite(pCamera.getMinX() + 17.0f, pCamera.getMaxY() - 55.0f, pEnv.mArrowTextureRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                SelectScene.this.hide(new Runnable() {
                    public void run() {
                        SelectScene.this.show(SelectScene.this.mStartLevel - 16);
                    }
                });
                return true;
            }
        };
        layer.attachChild(this.mArrowPrev);
        this.mArrowNext = new Sprite((pCamera.getMaxX() - 17.0f) - this.mArrowPrev.getWidth(), pCamera.getMaxY() - 55.0f, pEnv.mArrowTextureRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                SelectScene.this.hide(new Runnable() {
                    public void run() {
                        SelectScene.this.show(SelectScene.this.mStartLevel + 16);
                    }
                });
                return true;
            }
        };
        this.mArrowNext.setRotation(180.0f);
        layer.attachChild(this.mArrowNext);
    }

    public void show() {
        this.mInfoGame.load();
        this.mStartLevel = 1;
        while (this.mStartLevel + 16 <= 30 && this.mInfoGame.currentScore[(this.mStartLevel + 16) - 1] >= 0) {
            this.mStartLevel += 16;
        }
        show(this.mStartLevel);
    }

    public void show(int pStartLevel) {
        clearTouchAreas();
        IEntity layer = getLastChild();
        layer.detachChildren();
        this.mButtons = new LevelButton[16];
        int countButtons = 0;
        int padding = 120 / 2;
        this.mStartLevel = pStartLevel;
        int level = pStartLevel;
        for (int iCol = 0; iCol < 4; iCol++) {
            for (int iRow = 0; iRow < 4; iRow++) {
                if (level <= 30) {
                    this.mButtons[countButtons] = new LevelButton(this, level, this.mInfoGame.currentScore[level - 1]);
                    this.mButtons[countButtons].setCenter(this.mCamera.getCenterX() + ((float) ((iRow - 2) * 120)) + ((float) padding), this.mCamera.getCenterY() + ((float) ((iCol - 2) * 120)) + ((float) padding));
                    registerTouchArea(this.mButtons[countButtons]);
                    layer.attachChild(this.mButtons[countButtons]);
                    countButtons++;
                    level++;
                }
            }
        }
        this.mLoading.setScale(0.0f);
        this.mLoading.setPosition(this.mCamera.getCenterX() - (this.mLoading.getWidth() / 2.0f), this.mCamera.getCenterY() + 112.0f);
        if (this.mStartLevel - 1 > 30 || this.mStartLevel - 1 <= 0) {
            this.mArrowPrev.setVisible(false);
        } else {
            this.mArrowPrev.setVisible(true);
            registerTouchArea(this.mArrowPrev);
        }
        if (this.mStartLevel + 16 <= 30) {
            this.mArrowNext.setVisible(true);
            registerTouchArea(this.mArrowNext);
            return;
        }
        this.mArrowNext.setVisible(false);
    }

    public void hide(final Runnable pRun) {
        clearTouchAreas();
        for (LevelButton button : this.mButtons) {
            if (button != null) {
                button.hide();
            }
        }
        this.mArrowPrev.setVisible(false);
        this.mArrowNext.setVisible(false);
        this.mLoading.registerEntityModifier(new ScaleModifier(0.33f, 0.0f, 1.0f, new IEntityModifier.IEntityModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                onModifierFinished((IModifier<IEntity>) x0, (IEntity) x1);
            }

            public void onModifierFinished(IModifier<IEntity> iModifier, IEntity iEntity) {
                SelectScene.this.postRunnable(pRun);
            }
        }));
    }

    public boolean onSceneTouchEvent(TouchEvent pSceneTouchEvent) {
        super.onSceneTouchEvent(pSceneTouchEvent);
        if (pSceneTouchEvent.getAction() == 1) {
            for (LevelButton button : this.mButtons) {
                if (button != null) {
                    button.setSelected(false);
                }
            }
        }
        return true;
    }

    public Context getContext() {
        return this.mContext;
    }
}
