package ru.aeradeve.games.gravityclumps2.rating;

import android.content.Context;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Scanner;
import ru.aeradeve.utils.rating.utils.BaseRatingInfoGame;

public class GameInfo extends BaseRatingInfoGame {
    private static final String FILE_NAME = "new_info";
    public int[] currentScore = null;
    public int lastOpenLevel = 1;

    public GameInfo(Context pContext) {
        super(pContext, FILE_NAME);
        load();
    }

    /* access modifiers changed from: protected */
    public void onInitData() {
    }

    public String getURL() {
        return "http://aeratools.appspot.com/api/rating/v3/rating";
    }

    public int getGameId() {
        return 17;
    }

    public void onSave(PrintStream out) {
        if (this.currentScore == null) {
            this.currentScore = new int[30];
            Arrays.fill(this.currentScore, -1);
            this.currentScore[0] = 0;
        }
        out.println(this.lastOpenLevel);
        for (int score : this.currentScore) {
            out.println(score);
        }
    }

    public void onLoad(Scanner in) {
        try {
            this.lastOpenLevel = Integer.parseInt(in.nextLine());
        } catch (Exception e) {
        }
        if (this.currentScore == null) {
            this.currentScore = new int[30];
        }
        Arrays.fill(this.currentScore, -1);
        this.currentScore[0] = 0;
        for (int i = 0; i < this.currentScore.length; i++) {
            try {
                this.currentScore[i] = Integer.parseInt(in.nextLine());
            } catch (Exception e2) {
            }
        }
    }

    public void recalcScores() {
        int sumScore = 0;
        for (int score : this.currentScore) {
            if (score > 0) {
                sumScore += score;
            }
        }
        setScore4Send((long) sumScore);
    }
}
