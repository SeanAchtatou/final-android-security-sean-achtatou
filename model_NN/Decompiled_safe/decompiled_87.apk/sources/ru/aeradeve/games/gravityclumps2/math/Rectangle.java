package ru.aeradeve.games.gravityclumps2.math;

import com.badlogic.gdx.math.Vector2;

public class Rectangle {
    private Vector2 mLeftTopPoint;
    private Vector2 mRightBottomPoint;

    public Rectangle() {
    }

    public Rectangle(Vector2 pLeftTopPoint, Vector2 pRightBottomPoint) {
        this.mLeftTopPoint = pLeftTopPoint;
        this.mRightBottomPoint = pRightBottomPoint;
    }

    public Vector2 getLeftTopPoint() {
        return this.mLeftTopPoint;
    }

    public void setLeftTopPoint(Vector2 pLeftTopPoint) {
        this.mLeftTopPoint = pLeftTopPoint;
    }

    public Vector2 getRightBottomPoint() {
        return this.mRightBottomPoint;
    }

    public void setRightBottomPoint(Vector2 pRightBottomPoint) {
        this.mRightBottomPoint = pRightBottomPoint;
    }

    public Vector2 getCenter() {
        return new Vector2((this.mLeftTopPoint.x + this.mRightBottomPoint.x) / 2.0f, (this.mLeftTopPoint.y + this.mRightBottomPoint.y) / 2.0f);
    }

    public float getWidth() {
        return this.mRightBottomPoint.x - this.mLeftTopPoint.x;
    }

    public float getHeight() {
        return this.mRightBottomPoint.y - this.mLeftTopPoint.y;
    }
}
