package ru.aeradeve.games.gravityclumps2.service;

import android.content.Context;
import ru.aeradeve.games.gravityclumps2.utils.Flags;

public class GlobalFunctions {
    public static String generateDeviceId(Context context) {
        StringBuffer deviceIdBuffer = new StringBuffer();
        deviceIdBuffer.append(System.currentTimeMillis());
        deviceIdBuffer.append(Math.random());
        return deviceIdBuffer.toString();
    }

    public static void init(Context context) {
        Flags.loadFlags(context);
    }
}
