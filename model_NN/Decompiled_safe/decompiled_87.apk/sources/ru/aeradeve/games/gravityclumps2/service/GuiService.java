package ru.aeradeve.games.gravityclumps2.service;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import ru.aeradeve.games.gravityclumps2.R;

public class GuiService {
    public LinearLayout getAdLayout(Context context, int width) {
        LinearLayout linerLinearLayout = new LinearLayout(context);
        linerLinearLayout.setGravity(17);
        linerLinearLayout.setOrientation(1);
        linerLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, width / 6));
        linerLinearLayout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.gradient));
        ImageView logoImageView = new ImageView(context);
        logoImageView.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        logoImageView.setImageDrawable(context.getResources().getDrawable(R.drawable.aeralogo));
        linerLinearLayout.addView(logoImageView);
        return linerLinearLayout;
    }
}
