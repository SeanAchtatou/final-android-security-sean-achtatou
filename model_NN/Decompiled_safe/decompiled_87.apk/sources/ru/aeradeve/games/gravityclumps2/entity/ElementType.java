package ru.aeradeve.games.gravityclumps2.entity;

public enum ElementType {
    SIMPLE,
    ELASTIC,
    SLIPPERY,
    BASKET,
    BALL
}
