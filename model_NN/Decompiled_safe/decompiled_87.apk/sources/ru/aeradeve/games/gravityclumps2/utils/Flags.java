package ru.aeradeve.games.gravityclumps2.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import java.util.HashMap;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import ru.aeradeve.games.gravityclumps2.R;

public class Flags {
    public static HashMap<String, Bitmap> flags = new HashMap<>();
    public static String[] flagsIso = {"ad", "ae", "af", "ag", "ai", "al", "am", "an", "ao", "ar", "as", "at", "au", "aw", "ax", "az", "ba", "bb", "bd", "be", "bf", "bg", "bh", "bi", "bj", "bm", "bn", "bo", "br", "bs", "bt", "bv", "bw", "by", "bz", "ca", "cc", "cd", "cf", "cg", "ch", "ci", "ck", "cl", "cm", "cn", "co", "cr", "cs", "cu", "cv", "cx", "cy", "cz", "de", "dj", "dk", "dm", "do", "dz", "ec", "ee", "eg", "eh", "er", "es", "et", "fi", "fj", "fk", "fm", "fo", "fr", "ga", "gb", "gd", "ge", "gf", "gh", "gi", "gl", "gm", "gn", "gp", "gq", "gr", "gs", "gt", "gu", "gw", "gy", "hk", "hm", "hn", "hr", "ht", "hu", TMXConstants.TAG_TILE_ATTRIBUTE_ID, "ie", "il", "in", "io", "iq", "ir", "is", "it", "jm", "jo", "jp", "ke", "kg", "kh", "ki", "km", "kn", "kp", "kr", "kw", "ky", "kz", "la", "lb", "lc", "li", "lk", "lr", "ls", "lt", "lu", "lv", "ly", "ma", "mc", "md", "me", "mg", "mh", "mk", "ml", "mm", "mn", "mo", "mp", "mq", "mr", "ms", "mt", "mu", "mv", "mw", "mx", "my", "mz", "na", "nc", "ne", "nf", "ng", "ni", "nl", "no", "np", "nr", "nu", "nz", "om", "pa", "pe", "pf", "pg", "ph", "pk", "pl", "pm", "pn", "pr", "ps", "pt", "pw", "py", "qa", "re", "ro", "rs", "ru", "rw", "sa", "sb", "sc", "sd", "se", "sg", "sh", "si", "sj", "sk", "sl", "sm", "sn", "so", "sr", "st", "sv", "sy", "sz", "tc", "td", "tf", "tg", "th", "tj", "tk", "tl", "tm", "tn", "to", "tr", "tt", "tv", "tw", "tz", "ua", "ug", "um", "us", "uy", "uz", "va", "vc", "ve", "vg", "vi", "vn", "vu", "wf", "ws", "ye", "yt", "za", "zm", "zw", "eu", "pirate"};

    /* JADX INFO: Multiple debug info for r22v3 android.graphics.Matrix: [D('flagsHeight' int), D('matrix' android.graphics.Matrix)] */
    /* JADX INFO: Multiple debug info for r22v7 int: [D('i$' int), D('aFlagsISO' java.lang.String)] */
    public static void loadFlags(Context context) {
        Bitmap flagsBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.flags);
        int flagsWidth = flagsBitmap.getWidth();
        int flagWidth = flagsWidth / 10;
        int flagHeight = flagsBitmap.getHeight() / 25;
        int x = 0;
        int y = 0;
        new Matrix().setScale(1.3f, 1.3f);
        Paint whitePaint = new Paint();
        whitePaint.setColor(-1);
        String[] arr$ = flagsIso;
        int len$ = arr$.length;
        int i$ = 0;
        while (i$ < len$) {
            String aFlagsISO = arr$[i$];
            if (x + flagWidth > flagsWidth) {
                x = 0;
                y += flagHeight;
            }
            int y2 = y;
            int x2 = x;
            Bitmap flagBitmap = Bitmap.createBitmap(flagsBitmap, x2, y2, flagWidth, flagHeight);
            Bitmap newFlagBitmap = Bitmap.createBitmap(flagBitmap.getWidth() + 2, flagBitmap.getHeight() + 2, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(newFlagBitmap);
            canvas.drawColor(0);
            canvas.drawLine(0.0f, 0.0f, (float) (flagBitmap.getWidth() + 1), 0.0f, whitePaint);
            canvas.drawLine(0.0f, 0.0f, 0.0f, (float) (flagBitmap.getHeight() + 1), whitePaint);
            canvas.drawLine((float) (flagBitmap.getWidth() + 1), 0.0f, (float) (flagBitmap.getWidth() + 2), (float) (flagBitmap.getHeight() + 2), whitePaint);
            canvas.drawLine(0.0f, (float) (flagBitmap.getHeight() + 1), (float) (flagBitmap.getWidth() + 2), (float) (flagBitmap.getHeight() + 2), whitePaint);
            canvas.drawBitmap(flagBitmap, 1.0f, 1.0f, (Paint) null);
            flags.put(aFlagsISO, newFlagBitmap);
            x = x2 + flagWidth;
            i$++;
            y = y2;
        }
    }

    public static Bitmap getFlag(String flagIso) {
        String flagIso2 = flagIso.trim().toLowerCase();
        if (flags.get(flagIso2) == null) {
            return flags.get("pirate");
        }
        return flags.get(flagIso2);
    }
}
