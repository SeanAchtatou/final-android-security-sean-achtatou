package ru.aeradeve.games.gravityclumps2.scene;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.scene.CameraScene;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.util.GLHelper;
import ru.aeradeve.games.gravityclumps2.utils.Resources;

public class BaseScene extends CameraScene {
    private Sprite mBackground = null;
    protected Resources mTEnv;

    public BaseScene(int pLayerCount, Camera pCamera, Resources pTEnv) {
        super(pLayerCount, pCamera);
        this.mTEnv = pTEnv;
        reset();
    }

    public void reset() {
        super.reset();
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            getChild(i).detachChildren();
        }
        this.mCamera.setCenter(this.mCamera.getWidth() / 2.0f, this.mCamera.getHeight() / 2.0f);
        setBackground(new ColorBackground(0.0f, 0.0f, 0.0f));
        if (this.mBackground == null) {
            this.mBackground = new Sprite(0.0f, 0.0f, this.mTEnv.getBg()) {
                /* access modifiers changed from: protected */
                public void onInitDraw(GL10 pGL) {
                    super.onInitDraw(pGL);
                    GLHelper.enableDither(pGL);
                }
            };
        }
        this.mBackground.setPosition(0.0f, this.mCamera.getHeight() - this.mBackground.getHeightScaled());
        getFirstChild().attachChild(this.mBackground);
    }

    public Resources getTEnv() {
        return this.mTEnv;
    }
}
