package ru.aeradeve.games.gravityclumps2.entity;

import com.badlogic.gdx.physics.box2d.Body;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;

public abstract class WorldEntity {
    protected Body mBody;
    protected IEntity mLayer;
    protected PhysicsWorld mPhysicsWorld;
    protected Sprite mSprite;
    protected ElementType mType;

    protected WorldEntity() {
    }

    public Sprite getSprite() {
        return this.mSprite;
    }

    public void setSprite(Sprite pSprite) {
        this.mSprite = pSprite;
    }

    public Body getBody() {
        return this.mBody;
    }

    public void setBody(Body pBody) {
        this.mBody = pBody;
    }

    public IEntity getLayer() {
        return this.mLayer;
    }

    public void setLayer(IEntity pLayer) {
        this.mLayer = pLayer;
    }

    public PhysicsWorld getPhysicsWorld() {
        return this.mPhysicsWorld;
    }

    public void setPhysicsWorld(PhysicsWorld pPhysicsWorld) {
        this.mPhysicsWorld = pPhysicsWorld;
    }

    public ElementType getType() {
        return this.mType;
    }

    public void setType(ElementType pType) {
        this.mType = pType;
    }
}
