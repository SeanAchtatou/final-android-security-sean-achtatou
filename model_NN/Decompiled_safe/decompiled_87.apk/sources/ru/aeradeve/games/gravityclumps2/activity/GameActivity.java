package ru.aeradeve.games.gravityclumps2.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.physics.PhysicsHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.AlphaModifier;
import org.anddev.andengine.entity.modifier.DelayModifier;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.entity.modifier.ParallelEntityModifier;
import org.anddev.andengine.entity.modifier.ScaleModifier;
import org.anddev.andengine.entity.modifier.SequenceEntityModifier;
import org.anddev.andengine.entity.scene.CameraScene;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.scene.background.SpriteBackground;
import org.anddev.andengine.entity.scene.menu.MenuScene;
import org.anddev.andengine.entity.scene.menu.item.IMenuItem;
import org.anddev.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.anddev.andengine.entity.scene.menu.item.TextMenuItem;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.entity.util.ScreenGrabber;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.opengl.view.RenderSurfaceView;
import org.anddev.andengine.sensor.accelerometer.AccelerometerData;
import org.anddev.andengine.sensor.accelerometer.IAccelerometerListener;
import org.anddev.andengine.util.modifier.IModifier;
import ru.aeradeve.games.gravityclumps2.R;
import ru.aeradeve.games.gravityclumps2.entity.ClumpEntity;
import ru.aeradeve.games.gravityclumps2.entity.ClumpType;
import ru.aeradeve.games.gravityclumps2.entity.DitherSprite;
import ru.aeradeve.games.gravityclumps2.level.Level;
import ru.aeradeve.games.gravityclumps2.rating.GameInfo;
import ru.aeradeve.games.gravityclumps2.service.LevelService;
import ru.aeradeve.games.gravityclumps2.service.LevelServiceImpl;
import ru.aeradeve.games.gravityclumps2.source.GameTextureSource;
import ru.aeradeve.games.gravityclumps2.utils.Resources;
import ru.aeradeve.games.gravityclumps2.utils.StaticStorage;
import ru.aeradeve.utils.dialog.GiveFiveDialog;

public class GameActivity extends PrtScActivity implements IAccelerometerListener, Scene.IOnSceneTouchListener, Scene.IOnAreaTouchListener, MenuScene.IOnMenuItemClickListener {
    private static final int BONUS_SCORE = 100;
    public static final int CAMERA_HEIGHT = 640;
    public static final int CAMERA_WIDTH = 480;
    public static int CLUMPS_COUNT = 80;
    public static final String EXT_LEVEL_ID = "levelId";
    private static final FixtureDef FIXTURE_DEF = PhysicsFactory.createFixtureDef(1.0f, 0.2f, 0.1f);
    private static final int MENU_CONTINUE = 0;
    private static final int MENU_EXIT = 2;
    private static final int MENU_NEW_RECORD = -1;
    private static final int MENU_NEXT_LEVEL = 3;
    private static final int MENU_RESET = 1;
    public static final int MINIMUM_GROUP_SIZE = 2;
    public static final Vector2 ONE_VECTOR = new Vector2(0.0f, 10.0f);
    public static final Vector2 ZERO_VECTOR = new Vector2(0.0f, 0.0f);
    private SpriteMenuItem continueMenuItem;
    private Vector2 gravityVector;
    /* access modifiers changed from: private */
    public Texture mBgTexture;
    private Sprite mFadeBgSprite;
    private int mFlyTextCount;
    /* access modifiers changed from: private */
    public GameInfo mGameInfo;
    private boolean mGameOver;
    private boolean mGameRun;
    /* access modifiers changed from: private */
    public Level mLevel;
    private String mLevelDescription;
    /* access modifiers changed from: private */
    public int mLevelId = 1;
    private LevelService mLevelService = LevelServiceImpl.getInstance();
    private boolean mMenuOpen;
    /* access modifiers changed from: private */
    public MenuScene mMenuScene;
    /* access modifiers changed from: private */
    public int mNeedScore;
    private PhysicsWorld mPhysicsWorld;
    private Random mRandom = new Random();
    private Resources mResources;
    /* access modifiers changed from: private */
    public Scene mScene;
    /* access modifiers changed from: private */
    public int mScores;
    private int[] mUsedClumpsCount;
    private SpriteMenuItem mainMenuItem;
    private TextMenuItem newRecordMenuItem;
    private SpriteMenuItem nextLevelMenuItem;
    private SpriteMenuItem resetMenuItem;
    private ChangeableText scoreInfo;
    private ScreenGrabber screenCapture;
    private ChangeableText yourBestScoreInfo;

    private interface OnFlyTextFinishedListener {
        void onFlyTextFinished();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle pSavedInstanceState) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(pSavedInstanceState);
        this.mGameInfo = new GameInfo(this);
        this.mLevelId = getIntent().getIntExtra(EXT_LEVEL_ID, 1);
    }

    public Engine onLoadEngine() {
        return new Engine(new EngineOptions(false, EngineOptions.ScreenOrientation.PORTRAIT, new RatioResolutionPolicy(480.0f, 640.0f), new Camera(0.0f, 0.0f, 480.0f, 640.0f)));
    }

    public void onLoadResources() {
        this.mResources = new Resources(this.mEngine, this);
        this.mResources.loadResources();
        enableAccelerometerSensor(this);
    }

    public Scene onLoadScene() {
        Scene scene = new Scene(2);
        this.mScene = scene;
        this.screenCapture = new ScreenGrabber();
        scene.attachChild(this.screenCapture);
        scene.setOnAreaTouchListener(this);
        this.mPhysicsWorld = new PhysicsWorld(new Vector2(0.0f, 9.80665f), false);
        scene.registerUpdateHandler(this.mPhysicsWorld);
        this.mFadeBgSprite = new Sprite(0.0f, 0.0f, this.mResources.mFadeBgTexture);
        return scene;
    }

    public void onLoadComplete() {
        super.onLoadComplete();
        startLevel();
    }

    /* access modifiers changed from: private */
    public void startLevel() {
        final Scene mScene2 = this.mEngine.getScene();
        this.mPhysicsWorld.setGravity(ZERO_VECTOR);
        mScene2.setBackground(new ColorBackground(0.0f, 0.0f, 0.0f));
        this.mResources.mBuildableTexture.clearTextureSources();
        this.mLevelDescription = this.mLevelService.loadDescription(this, this.mLevelId);
        this.mNeedScore = this.mLevelService.loadNeedScore(this, this.mLevelId);
        this.mLevel = new Level();
        this.mLevel.initialize(this.mLevelDescription, this.mResources, this.mPhysicsWorld, this, 480.0f, 640.0f);
        this.mLevel.createLines(mScene2, getGameLayer(), true);
        this.screenCapture.grab(this.mRenderSurfaceView.getWidth(), this.mRenderSurfaceView.getHeight(), new ScreenGrabber.IScreenGrabberCallback() {
            public void onScreenGrabbed(final Bitmap bitmap) {
                mScene2.postRunnable(new Runnable() {
                    public void run() {
                        StaticStorage.savePixels(Bitmap.createScaledBitmap(bitmap, GameActivity.CAMERA_WIDTH, GameActivity.CAMERA_HEIGHT, true));
                        Texture unused = GameActivity.this.mBgTexture = new Texture(512, 1024, TextureOptions.DEFAULT);
                        TextureRegion textureRegion = TextureRegionFactory.createFromSource(GameActivity.this.mBgTexture, new GameTextureSource(), 0, 0);
                        GameActivity.this.mEngine.getTextureManager().loadTexture(GameActivity.this.mBgTexture);
                        Iterator i$ = GameActivity.this.mLevel.getLines().iterator();
                        while (i$.hasNext()) {
                            GameActivity.this.getGameLayer().detachChild(i$.next().getSprite());
                        }
                        mScene2.setBackground(new SpriteBackground(new DitherSprite(0.0f, 0.0f, textureRegion)));
                        GameActivity.this.initialization();
                        GameActivity.this.initMenuScene();
                    }
                });
            }

            public void onScreenGrabFailed(Exception e) {
            }
        });
    }

    public boolean onSceneTouchEvent(Scene pScene, final TouchEvent pSceneTouchEvent) {
        if (this.mPhysicsWorld == null || pSceneTouchEvent.getAction() != 0) {
            return false;
        }
        runOnUpdateThread(new Runnable() {
            public void run() {
                GameActivity.this.addFace(pSceneTouchEvent.getX(), pSceneTouchEvent.getY());
            }
        });
        return true;
    }

    public void onAccelerometerChanged(AccelerometerData pAccelerometerData) {
        this.gravityVector = new Vector2(-pAccelerometerData.getX(), pAccelerometerData.getY());
        this.mPhysicsWorld.setGravity(this.gravityVector);
    }

    public boolean onKeyDown(int pKeyCode, KeyEvent pEvent) {
        if (pKeyCode != 82 || pEvent.getAction() != 0) {
            return super.onKeyDown(pKeyCode, pEvent);
        }
        if (this.mGameOver) {
            return false;
        }
        if (!this.mMenuOpen) {
            buildMenuScene(true);
            pause(this.mMenuScene, false);
        } else {
            noPause(false);
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void initialization() {
        CLUMPS_COUNT = LevelServiceImpl.getInstance().loadBubbleCount(this, this.mLevelId);
        this.mGameOver = false;
        IEntity gameLayer = getGameLayer();
        for (int i = gameLayer.getChildCount() - 1; i >= 0; i--) {
            if (gameLayer.getChild(i) instanceof ClumpEntity) {
                removeClump((ClumpEntity) gameLayer.getChild(i));
            } else {
                gameLayer.detachChild(gameLayer.getChild(i));
            }
        }
        this.mRandom = new Random((long) this.mLevelId);
        this.mUsedClumpsCount = new int[ClumpType.values().length];
        int x = 0;
        int y = 570;
        for (int addedClumpsCount = 0; addedClumpsCount < CLUMPS_COUNT; addedClumpsCount++) {
            if (x + 70 > 480) {
                x = 0;
                y -= 70;
                if (y < 0) {
                    y = 0;
                }
            }
            addFace((float) x, (float) y);
            x += 70;
        }
        addInfoText();
        this.mScores = 0;
        this.mFlyTextCount = 0;
        addScores(0.0f);
    }

    /* access modifiers changed from: private */
    public void addFace(float pX, float pY) {
        TiledTextureRegion texture;
        ClumpType clumpType;
        Scene scene = this.mEngine.getScene();
        switch (this.mRandom.nextInt(4)) {
            case 0:
                texture = this.mResources.mRedClumpTextureRegion;
                clumpType = ClumpType.RED;
                break;
            case 1:
                texture = this.mResources.mGreenClumpTextureRegion;
                clumpType = ClumpType.GREEN;
                break;
            case 2:
                texture = this.mResources.mBlueClumpTextureRegion;
                clumpType = ClumpType.BLUE;
                break;
            case 3:
                texture = this.mResources.mYellowClumpTextureRegion;
                clumpType = ClumpType.YELLOW;
                break;
            default:
                texture = this.mResources.mRedClumpTextureRegion;
                clumpType = ClumpType.RED;
                break;
        }
        int[] iArr = this.mUsedClumpsCount;
        int index = ClumpType.getIndex(clumpType);
        iArr[index] = iArr[index] + 1;
        ClumpEntity face = new ClumpEntity(pX, pY, texture, clumpType);
        Body body = PhysicsFactory.createCircleBody(this.mPhysicsWorld, face, BodyDef.BodyType.DynamicBody, FIXTURE_DEF);
        scene.registerTouchArea(face);
        getGameLayer().attachChild(face);
        this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(face, body, true, false));
    }

    public boolean onAreaTouched(TouchEvent touchEvent, final Scene.ITouchArea iTouchArea, float v, float v1) {
        if (touchEvent.getAction() != 0 || !(iTouchArea instanceof ClumpEntity)) {
            return true;
        }
        runOnUpdateThread(new Runnable() {
            public void run() {
                GameActivity.this.onClumpClick((ClumpEntity) iTouchArea);
            }
        });
        return true;
    }

    /* access modifiers changed from: protected */
    public int getLayoutID() {
        return R.layout.game;
    }

    /* access modifiers changed from: protected */
    public int getRenderSurfaceViewID() {
        return R.id.gameRenderSurfaceView;
    }

    public boolean isLandscape() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onSetContentView() {
        super.setContentView(getLayoutID());
        this.mRenderSurfaceView = (RenderSurfaceView) findViewById(getRenderSurfaceViewID());
        this.mRenderSurfaceView.setEGLConfigChooser(false);
        this.mRenderSurfaceView.setRenderer(this.mEngine);
    }

    /* access modifiers changed from: private */
    public void removeClump(ClumpEntity clump) {
        clump.setDie(true);
        Scene scene = this.mEngine.getScene();
        try {
            PhysicsConnector facePhysicsConnector = this.mPhysicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(clump);
            if (facePhysicsConnector == null) {
                removeClumpFromScene(scene, clump);
                return;
            }
            this.mPhysicsWorld.unregisterPhysicsConnector(facePhysicsConnector);
            this.mPhysicsWorld.destroyBody(facePhysicsConnector.getBody());
            removeClumpFromScene(scene, clump);
        } catch (NullPointerException e) {
            removeClumpFromScene(scene, clump);
        }
    }

    private void removeClumpFromScene(Scene scene, ClumpEntity clump) {
        scene.unregisterTouchArea(clump);
        getGameLayer().detachChild(clump);
    }

    private void removeClumpRunOnUpdate(final ClumpEntity clump) {
        runOnUpdateThread(new Runnable() {
            public void run() {
                GameActivity.this.removeClump(clump);
            }
        });
    }

    public void buildMenuScene(boolean continueGame) {
        this.mMenuScene = new MenuScene(this.mEngine.getCamera());
        if (this.mLevelId < this.mGameInfo.lastOpenLevel) {
            this.mMenuScene.addMenuItem(this.nextLevelMenuItem);
        }
        if (continueGame) {
            this.mMenuScene.addMenuItem(this.continueMenuItem);
        }
        this.mMenuScene.addMenuItem(this.resetMenuItem);
        this.mMenuScene.addMenuItem(this.mainMenuItem);
        this.mMenuScene.buildAnimations();
        this.mMenuScene.setBackgroundEnabled(false);
        this.mMenuScene.setOnMenuItemClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void initMenuScene() {
        this.mMenuScene = new MenuScene(this.mEngine.getCamera());
        this.nextLevelMenuItem = new SpriteMenuItem(3, this.mResources.mNextLevelMenuItem);
        this.nextLevelMenuItem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        this.continueMenuItem = new SpriteMenuItem(0, this.mResources.mContinueMenuItem);
        this.continueMenuItem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        this.resetMenuItem = new SpriteMenuItem(1, this.mResources.mRestartMenuItem);
        this.resetMenuItem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        this.mainMenuItem = new SpriteMenuItem(2, this.mResources.mExitMenuItem);
        this.mainMenuItem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        this.mMenuScene.buildAnimations();
        this.mMenuScene.setBackgroundEnabled(false);
        this.mMenuScene.setOnMenuItemClickListener(this);
    }

    private void addInfoText() {
        String scoreInitText = getResources().getString(R.string.score) + " " + this.mScores;
        this.scoreInfo = new ChangeableText(5.0f, 5.0f, this.mResources.mComboTextFont, scoreInitText, scoreInitText.length() + 4);
        String scorePreviousInitText = getResources().getString(R.string.yourBest) + " " + this.mNeedScore;
        this.yourBestScoreInfo = new ChangeableText(15.0f, 10.0f, this.mResources.mComboTextFont, scorePreviousInitText, scorePreviousInitText.length() + 4);
        this.yourBestScoreInfo.setPosition((480.0f - this.yourBestScoreInfo.getWidth()) - 15.0f, 10.0f);
        this.scoreInfo.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        this.yourBestScoreInfo.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        getGameLayer().attachChild(this.scoreInfo);
        getGameLayer().attachChild(this.yourBestScoreInfo);
    }

    public void pause(CameraScene cameraScene, boolean stopEngine) {
        repeatTileLayer(getInfoLayer(), this.mResources.mFadeBgTexture);
        this.mMenuOpen = true;
        this.mScene.setChildScene(cameraScene, false, true, true);
        if (stopEngine) {
            this.mEngine.stop();
        }
        this.mGameRun = false;
        if (this.mLevelId == 6) {
            GiveFiveDialog.showGiveFiveDialog(this, "ru.aeradeve.games.gravityclumps2");
        }
    }

    public void noPause(boolean startEngine) {
        clearLayer(getInfoLayer());
        this.mMenuOpen = false;
        this.mScene.clearChildScene();
        if (startEngine) {
            this.mEngine.start();
        }
        this.mGameRun = true;
    }

    public boolean onMenuItemClicked(MenuScene menuScene, IMenuItem iMenuItem, float v, float v1) {
        switch (iMenuItem.getID()) {
            case -1:
                return false;
            case 0:
                noPause(false);
                return true;
            case 1:
                levelComplete();
                noPause(false);
                return true;
            case 2:
                finish();
                return true;
            case 3:
                this.mLevelId++;
                levelComplete();
                noPause(false);
                return true;
            default:
                return false;
        }
    }

    private void addScores(float scores) {
        this.mScores = (int) (((float) this.mScores) + scores);
        this.scoreInfo.setText(getResources().getString(R.string.score) + " " + this.mScores);
        this.scoreInfo.setPosition(15.0f, 10.0f);
    }

    private void flyText(float pCenterX, float pY, String pText, OnFlyTextFinishedListener pOnFlyTextFinishedListener) {
        Text text = new Text(pCenterX, pY, this.mResources.mComboTextFont, pText);
        text.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        text.setPosition(pCenterX - (text.getWidth() / 2.0f), pY);
        PhysicsHandler physicsHandler = new PhysicsHandler(text);
        text.registerUpdateHandler(physicsHandler);
        physicsHandler.setVelocity(0.0f, -50.0f);
        final OnFlyTextFinishedListener onFlyTextFinishedListener = pOnFlyTextFinishedListener;
        text.registerEntityModifier(new SequenceEntityModifier(new IEntityModifier.IEntityModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                onModifierFinished((IModifier<IEntity>) x0, (IEntity) x1);
            }

            public void onModifierFinished(IModifier<IEntity> iModifier, IEntity entity) {
                if (onFlyTextFinishedListener != null) {
                    onFlyTextFinishedListener.onFlyTextFinished();
                }
                GameActivity.this.removeEntity(GameActivity.this.getInfoLayer(), entity);
            }
        }, new ParallelEntityModifier(new AlphaModifier(1.0f, 0.0f, 1.0f), new ScaleModifier(1.0f, 0.5f, 1.0f)), new DelayModifier(1.0f), new ParallelEntityModifier(new AlphaModifier(1.0f, 1.0f, 0.0f), new ScaleModifier(1.0f, 1.0f, 0.5f))));
        getInfoLayer().attachChild(text);
        this.mFlyTextCount = this.mFlyTextCount + 1;
    }

    public void removeEntity(final IEntity layer, final IEntity entity) {
        this.mScene.postRunnable(new Runnable() {
            public void run() {
                layer.detachChild(entity);
            }
        });
    }

    public void clearLayer(final IEntity layer) {
        this.mScene.postRunnable(new Runnable() {
            public void run() {
                for (int i = layer.getChildCount() - 1; i >= 0; i--) {
                    layer.detachChild(layer.getChild(i));
                }
            }
        });
    }

    public void repeatTileLayer(IEntity layer, TextureRegion texture) {
        float x = 0.0f;
        float y = 0.0f;
        while (y < 640.0f) {
            if (x > 480.0f) {
                x = 0.0f;
                y += (float) texture.getHeight();
            }
            layer.attachChild(new Sprite(x, y, texture));
            x += (float) texture.getWidth();
        }
    }

    public void onClumpClick(ClumpEntity clumpEntity) {
        if (!clumpEntity.isDie()) {
            IEntity layer = getGameLayer();
            int entityCount = layer.getChildCount();
            HashSet<ClumpEntity> deletedClumps = new HashSet<>();
            LinkedList<ClumpEntity> queue = new LinkedList<>();
            queue.offer(clumpEntity);
            deletedClumps.add(clumpEntity);
            ClumpType clumpType = clumpEntity.getClumpType();
            while (!queue.isEmpty()) {
                ClumpEntity currentClumpEntity = (ClumpEntity) queue.poll();
                for (int layerEntityIndex = 0; layerEntityIndex < entityCount; layerEntityIndex++) {
                    if (layer.getChild(layerEntityIndex) instanceof ClumpEntity) {
                        ClumpEntity checksClumpEntity = (ClumpEntity) layer.getChild(layerEntityIndex);
                        if (checksClumpEntity.getClumpType() == clumpType && !deletedClumps.contains(checksClumpEntity) && currentClumpEntity.collidesWith(checksClumpEntity) && !checksClumpEntity.isDie()) {
                            queue.offer(checksClumpEntity);
                            deletedClumps.add(checksClumpEntity);
                        }
                    }
                }
            }
            if (deletedClumps.size() >= 2) {
                addScores((float) (deletedClumps.size() * (deletedClumps.size() - 1)));
                String scoreForOneClumpS = "+" + String.valueOf(deletedClumps.size() - 1);
                Iterator i$ = deletedClumps.iterator();
                while (i$.hasNext()) {
                    ClumpEntity aClumpEntity = (ClumpEntity) i$.next();
                    flyText(aClumpEntity.getCenterX(), aClumpEntity.getCenterY(), scoreForOneClumpS, null);
                    removeClump(aClumpEntity);
                }
                int[] iArr = this.mUsedClumpsCount;
                int index = ClumpType.getIndex(clumpType);
                iArr[index] = iArr[index] - deletedClumps.size();
                boolean gameOver = true;
                int[] arr$ = this.mUsedClumpsCount;
                int len$ = arr$.length;
                int i$2 = 0;
                while (true) {
                    if (i$2 >= len$) {
                        break;
                    } else if (arr$[i$2] > 1) {
                        gameOver = false;
                        break;
                    } else {
                        i$2++;
                    }
                }
                if (gameOver) {
                    int lostClumpsCount = 0;
                    int[] arr$2 = this.mUsedClumpsCount;
                    int len$2 = arr$2.length;
                    for (int i$3 = 0; i$3 < len$2; i$3++) {
                        lostClumpsCount += arr$2[i$3];
                    }
                    int bonusScore = BONUS_SCORE / (lostClumpsCount + 1);
                    addScores((float) bonusScore);
                    flyText(240.0f, 320.0f, "+" + bonusScore + " " + getString(R.string.bonusScore), new OnFlyTextFinishedListener() {
                        public void onFlyTextFinished() {
                            if (GameActivity.this.mGameInfo.currentScore[GameActivity.this.mLevelId - 1] < GameActivity.this.mScores) {
                                GameActivity.this.mGameInfo.currentScore[GameActivity.this.mLevelId - 1] = GameActivity.this.mScores;
                                GameActivity.this.mGameInfo.recalcScores();
                                GameActivity.this.mGameInfo.save();
                            }
                            if (GameActivity.this.mScores >= GameActivity.this.mNeedScore) {
                                GameActivity.this.mGameInfo.lastOpenLevel = Math.min(Math.max(GameActivity.this.mGameInfo.lastOpenLevel, GameActivity.this.mLevelId + 1), 30);
                                GameActivity.this.mGameInfo.currentScore[GameActivity.this.mGameInfo.lastOpenLevel - 1] = Math.max(0, GameActivity.this.mGameInfo.currentScore[GameActivity.this.mGameInfo.lastOpenLevel - 1]);
                                GameActivity.this.mGameInfo.save();
                            }
                            GameActivity.this.buildMenuScene(false);
                            GameActivity.this.pause(GameActivity.this.mMenuScene, false);
                        }
                    });
                    this.mGameOver = true;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void levelComplete() {
        if (this.mLevelId > 30) {
            Intent openTableIntent = new Intent(this, TableScoresActivity.class);
            openTableIntent.putExtra("top", false);
            startActivity(openTableIntent);
            finish();
        }
        this.mLevelDescription = this.mLevelService.loadDescription(this, this.mLevelId);
        this.mPhysicsWorld.postRunnable(new Runnable() {
            public void run() {
                GameActivity.this.mLevel.destroy(null);
                for (int i = GameActivity.this.getGameLayer().getChildCount() - 1; i >= 0; i--) {
                    GameActivity.this.removeEntity(GameActivity.this.getGameLayer(), GameActivity.this.getGameLayer().getChild(i));
                }
                for (int i2 = GameActivity.this.getInfoLayer().getChildCount() - 1; i2 >= 0; i2--) {
                    GameActivity.this.removeEntity(GameActivity.this.getInfoLayer(), GameActivity.this.getInfoLayer().getChild(i2));
                }
                GameActivity.this.mScene.clearTouchAreas();
                GameActivity.this.mScene.clearChildScene();
                Scene unused = GameActivity.this.mScene = GameActivity.this.onLoadScene();
                GameActivity.this.mEngine.setScene(GameActivity.this.mScene);
                GameActivity.this.startLevel();
            }
        });
    }

    /* access modifiers changed from: private */
    public IEntity getGameLayer() {
        return this.mScene.getChild(0);
    }

    public IEntity getInfoLayer() {
        return this.mScene.getChild(1);
    }
}
