package ru.aeradeve.games.gravityclumps2.utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.font.StrokeFont;
import org.anddev.andengine.opengl.texture.BuildableTexture;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class Resources {
    public static final int BIG_INFO_TEXT_COLOR = -65281;
    public static final float BIG_INFO_TEXT_SIZE = 76.0f;
    public static final int COMBO_TEXT_COLOR = -256;
    public static final int COMBO_TEXT_SIZE = 32;
    public static final int COMBO_TEXT_STROKE_COLOR = -65536;
    public static final int COMBO_TEXT_STROKE_SIZE = 2;
    public static final int SCORE_TEXT_COLOR = -256;
    public static final int SCORE_TEXT_SIZE = 21;
    public static final int SCORE_TEXT_STROKE_COLOR = -65536;
    public static final int SCORE_TEXT_STROKE_SIZE = 1;
    public static final int SHADOW_STROKE_TEXT_COLOR = -7829368;
    public static final int SHADOW_STROKE_TEXT_STROKE_COLOR = -7829368;
    public static final int STROKE_TEXT_COLOR = -256;
    public static final int STROKE_TEXT_SIZE = 36;
    public static final int STROKE_TEXT_STROKE_COLOR = -65536;
    public static final int STROKE_TEXT_STROKE_SIZE = 2;
    public TextureRegion mArrowTextureRegion;
    public TextureRegion mBgTexture;
    public Font mBigInfoTextFont;
    public TiledTextureRegion mBlueClumpTextureRegion;
    public BuildableTexture mBuildableTexture;
    public TextureRegion[] mButtons = new TextureRegion[4];
    private Texture mButtonsTexture;
    public Font mComboTextFont;
    public Context mContext;
    public TextureRegion mContinueMenuItem;
    private Texture mDeskTexture;
    public Texture mDownBackgroundTexture;
    public Engine mEngine;
    public TextureRegion mExitMenuItem;
    public TextureRegion mFadeBgTexture;
    public Font mFont;
    public Font mFontButton;
    public Texture mFontButtonTexture;
    public StrokeFont mFontScore;
    public Font mFontSelected;
    public Texture mFontSelectedTexture;
    public Texture mFontTexture;
    public TiledTextureRegion mGreenClumpTextureRegion;
    private Texture mLaserTexture;
    private TextureRegion mLevelPreviewTexture;
    public TextureRegion mNextLevelMenuItem;
    public TiledTextureRegion mRedClumpTextureRegion;
    public TextureRegion mRestartMenuItem;
    private StrokeFont mShadowStrokeTextFont;
    private Texture mStarsTexture;
    private StrokeFont mStrokeTextFont;
    public Texture mTexture;
    public TiledTextureRegion mYellowClumpTextureRegion;

    public Resources(Engine engine, Context context) {
        this.mEngine = engine;
        this.mContext = context;
        TextureRegionFactory.setAssetBasePath("gfx/");
    }

    public void loadResources() {
        this.mTexture = new Texture(128, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mRedClumpTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this.mContext, "red_clump.png", 0, 0, 1, 1);
        this.mGreenClumpTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this.mContext, "green_clump.png", 0, 64, 1, 1);
        this.mBlueClumpTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this.mContext, "blue_clump.png", 64, 0, 1, 1);
        this.mYellowClumpTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this.mContext, "yellow_clump.png", 64, 64, 1, 1);
        Texture comboTextFontTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        FontFactory.setAssetBasePath("font/");
        this.mComboTextFont = new StrokeFont(comboTextFontTexture, Typeface.create(Typeface.DEFAULT, 1), 32.0f, true, -256, 2.0f, -65536);
        Texture menuItemsTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mContinueMenuItem = TextureRegionFactory.createFromAsset(menuItemsTexture, this.mContext, "continue_menu_item.png", 0, 0);
        this.mRestartMenuItem = TextureRegionFactory.createFromAsset(menuItemsTexture, this.mContext, "restart_menu_item.png", 0, 75);
        this.mExitMenuItem = TextureRegionFactory.createFromAsset(menuItemsTexture, this.mContext, "exit_menu_item.png", 0, 150);
        this.mNextLevelMenuItem = TextureRegionFactory.createFromAsset(menuItemsTexture, this.mContext, "next_level_menu_item.png", 0, 225);
        Texture fadeBgTexture = new Texture(128, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFadeBgTexture = TextureRegionFactory.createFromAsset(fadeBgTexture, this.mContext, "fade_bg.png", 0, 0);
        loadBackgroundResources();
        this.mEngine.getTextureManager().loadTextures(this.mTexture, comboTextFontTexture, menuItemsTexture, fadeBgTexture);
        this.mEngine.getFontManager().loadFonts(this.mComboTextFont);
        this.mBuildableTexture = new BuildableTexture(1024, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mEngine.getTextureManager().loadTextures(this.mBuildableTexture);
    }

    private void loadBackgroundResources() {
        Texture bgTexture = new Texture(512, 1024, TextureOptions.DEFAULT);
        this.mBgTexture = TextureRegionFactory.createFromAsset(bgTexture, this.mContext, "bg.png", 0, 0);
        this.mEngine.getTextureManager().loadTextures(bgTexture);
    }

    private void loadLevelPreviews() {
        Texture levelPreviewsTexture = new Texture(128, 128, TextureOptions.DEFAULT);
        this.mLevelPreviewTexture = TextureRegionFactory.createFromAsset(levelPreviewsTexture, this.mContext, "background_block.png", 0, 0);
        this.mEngine.getTextureManager().loadTexture(levelPreviewsTexture);
    }

    private void loadFontResource(boolean pLoadShadow) {
        FontFactory.setAssetBasePath("font/");
        Texture strokeTextFontTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mStrokeTextFont = new StrokeFont(strokeTextFontTexture, Typeface.create(Typeface.DEFAULT, 1), 36.0f, true, -256, 2.0f, -65536);
        this.mEngine.getTextureManager().loadTextures(strokeTextFontTexture);
        this.mEngine.getFontManager().loadFont(this.mStrokeTextFont);
        if (pLoadShadow) {
            Texture shadowStrokeTextFontTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
            this.mShadowStrokeTextFont = new StrokeFont(shadowStrokeTextFontTexture, Typeface.create(Typeface.DEFAULT, 1), 36.0f, true, -7829368, 2.0f, -7829368);
            this.mEngine.getTextureManager().loadTextures(shadowStrokeTextFontTexture);
            this.mEngine.getFontManager().loadFont(this.mShadowStrokeTextFont);
        }
    }

    private void loadSelectLevelControlResource() {
    }

    public void loadSelectLevelResource() {
        loadBackgroundResources();
        loadLevelPreviews();
        loadFontResource(true);
        loadSelectLevelControlResource();
        this.mButtonsTexture = new Texture(512, 128, TextureOptions.BILINEAR);
        TextureRegionFactory.createFromAsset(this.mButtonsTexture, this.mContext, "buttons.png", 0, 0);
        for (int iButton = 0; iButton < 4; iButton++) {
            this.mButtons[iButton] = new TextureRegion(this.mButtonsTexture, iButton * 75, 0, 75, 75);
        }
        this.mFontButtonTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFontButton = new StrokeFont(this.mFontButtonTexture, Typeface.create(Typeface.DEFAULT, 1), 37.0f, true, Color.argb(128, 255, 255, 255), 2.0f, -1);
        this.mEngine.getTextureManager().loadTextures(this.mButtonsTexture, this.mFontButtonTexture);
        this.mEngine.getFontManager().loadFont(this.mFontButton);
        this.mFontButtonTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFontScore = new StrokeFont(this.mFontButtonTexture, Typeface.create(Typeface.DEFAULT, 1), 21.0f, true, -256, 1.0f, -65536);
        this.mEngine.getTextureManager().loadTextures(this.mFontButtonTexture);
        this.mEngine.getFontManager().loadFont(this.mFontScore);
        this.mTexture = new Texture(128, 64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mArrowTextureRegion = TextureRegionFactory.createFromAsset(this.mTexture, this.mContext, "arrow.png", 0, 0);
        this.mEngine.getTextureManager().loadTexture(this.mTexture);
    }

    public TextureRegion getLevelPreviewTexture() {
        return this.mLevelPreviewTexture;
    }

    public StrokeFont getStrokeTextFont() {
        return this.mStrokeTextFont;
    }

    public StrokeFont getShadowStrokeTextFont() {
        return this.mShadowStrokeTextFont;
    }

    public TextureRegion getBg() {
        return this.mBgTexture;
    }
}
