package ru.aeradeve.games.effects;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import ru.aeradeve.games.effects.tower.TowerGameActivity;
import ru.aeradeve.games.gravityclumps2.R;

public class Effects extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.ad);
        ((Button) findViewById(R.id.mainAdView)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Effects.this.startActivityForResult(new Intent(Effects.this, TowerGameActivity.class), 0);
            }
        });
    }
}
