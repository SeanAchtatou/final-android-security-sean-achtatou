package ru.aeradeve.games.effects.tower.particle;

import org.anddev.andengine.entity.particle.ParticleSystem;
import org.anddev.andengine.entity.particle.emitter.IParticleEmitter;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class OneStepParticleSystem extends ParticleSystem {
    private static final float EXISTS_TIME = 5.0f;
    private float mAliveTime = 0.0f;
    private OnParticleSystemDieListener mParticleSystemDieListener;

    public OneStepParticleSystem(IParticleEmitter pParticleEmitter, float pMinRate, float pMaxRate, int pMaxParticles, TextureRegion pTextureRegion, OnParticleSystemDieListener particleSystemDieListener) {
        super(pParticleEmitter, pMinRate, pMaxRate, pMaxParticles, pTextureRegion);
        this.mParticleSystemDieListener = particleSystemDieListener;
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float pSecondsElapsed) {
        super.onManagedUpdate(pSecondsElapsed);
        if (isParticlesSpawnEnabled()) {
            setParticlesSpawnEnabled(false);
        }
        this.mAliveTime += pSecondsElapsed;
        if (this.mAliveTime > EXISTS_TIME) {
            this.mParticleSystemDieListener.onParticleSystemDie(this);
        }
    }
}
