package ru.aeradeve.games.effects.tower;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.ui.activity.BaseGameActivity;
import ru.aeradeve.games.effects.tower.PerfectDropEffectBuilder;
import ru.aeradeve.games.effects.tower.particle.RectangleSideParticleEmitter;

public class TowerGameActivity extends BaseGameActivity implements Scene.IOnSceneTouchListener, IUpdateHandler {
    private static final int CAMERA_HEIGHT = 720;
    private static final int CAMERA_WIDTH = 480;
    private PerfectDropEffectBuilder builder;
    private Camera camera;
    private int count = 0;
    private TextureRegion explodeParticleTexture;
    private ChangeableText fpsText;
    private Font mFont;
    private Texture mFontTexture;
    private float prevTime = 0.0f;
    private Scene scene;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Engine onLoadEngine() {
        this.camera = new Camera(0.0f, 0.0f, 480.0f, 720.0f);
        EngineOptions engineOptions = new EngineOptions(true, EngineOptions.ScreenOrientation.PORTRAIT, new RatioResolutionPolicy(480.0f, 720.0f), this.camera);
        engineOptions.getTouchOptions().setRunOnUpdateThread(true);
        return new Engine(engineOptions);
    }

    public void onLoadResources() {
        Texture particleTexture = new Texture(32, 32, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.explodeParticleTexture = TextureRegionFactory.createFromAsset(particleTexture, this, "gfx/particle_point.png", 0, 0);
        this.mFontTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFont = new Font(this.mFontTexture, Typeface.create(Typeface.DEFAULT, 1), 32.0f, true, -1);
        this.mEngine.getTextureManager().loadTextures(particleTexture, this.mFontTexture);
        this.mEngine.getFontManager().loadFont(this.mFont);
    }

    public Scene onLoadScene() {
        this.scene = new Scene(2);
        this.scene.setBackground(new ColorBackground(0.01f, 0.01f, 0.01f));
        this.scene.setOnSceneTouchListener(this);
        addInfoText();
        this.builder = new PerfectDropEffectBuilder(this.scene, this.explodeParticleTexture);
        this.scene.registerUpdateHandler(this);
        return this.scene;
    }

    private void addInfoText() {
        this.fpsText = new ChangeableText(5.0f, 75.0f, this.mFont, "asdasd", 10);
        this.fpsText.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        this.scene.getTopLayer().attachChild(this.fpsText);
    }

    public void onLoadComplete() {
    }

    public boolean onSceneTouchEvent(Scene scene2, TouchEvent touchEvent) {
        if (touchEvent.getAction() != 1) {
            return true;
        }
        this.builder.createEffect(this.mEngine.getScene().getTopLayer(), touchEvent.getX(), touchEvent.getY(), 64.0f, 64.0f, new PerfectDropEffectBuilder.PerfectDropEffectSettings(Color.rgb(0, 255, 0), Color.rgb(147, 210, 13), Color.rgb(0, 0, 0), 50, 80, 50, 2.0f, new RectangleSideParticleEmitter.EnableSides(true, true, true, true)));
        return true;
    }

    public void removeEntity(final IEntity layer, final IEntity entity) {
        runOnUpdateThread(new Runnable() {
            public void run() {
                layer.detachChild(entity);
            }
        });
    }

    public Scene getScene() {
        return this.scene;
    }

    public void setScene(Scene scene2) {
        this.scene = scene2;
    }

    public void onUpdate(float v) {
        this.prevTime += v;
        if (this.prevTime > 0.3f) {
            int i = this.count + 1;
            this.count = i;
            if (i % 100 == 0) {
                Log.i("!", String.valueOf(this.count));
            }
            this.builder.createEffect(this.mEngine.getScene().getTopLayer(), 200.0f, 200.0f, 64.0f, 64.0f, new PerfectDropEffectBuilder.PerfectDropEffectSettings(Color.rgb(253, 234, 0), Color.rgb(236, 159, 0), Color.rgb(0, 0, 0), 5, 50, 100, 0.77f, new RectangleSideParticleEmitter.EnableSides(true, true, true, true)));
            this.prevTime = 0.0f;
        }
        this.fpsText.setText(String.valueOf(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()));
    }

    public void reset() {
    }
}
