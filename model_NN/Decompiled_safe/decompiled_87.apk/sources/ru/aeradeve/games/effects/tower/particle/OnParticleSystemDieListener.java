package ru.aeradeve.games.effects.tower.particle;

import org.anddev.andengine.entity.particle.ParticleSystem;

public interface OnParticleSystemDieListener {
    void onParticleSystemDie(ParticleSystem particleSystem);
}
