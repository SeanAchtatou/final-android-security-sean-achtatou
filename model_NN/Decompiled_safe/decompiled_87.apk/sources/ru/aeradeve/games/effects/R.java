package ru.aeradeve.games.effects;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int aeralogo = 2130837504;
        public static final int bg_main = 2130837505;
        public static final int btn_f_d_left = 2130837506;
        public static final int btn_f_d_right = 2130837507;
        public static final int btn_f_ld = 2130837508;
        public static final int btn_f_rd = 2130837509;
        public static final int btn_n_d_left = 2130837510;
        public static final int btn_n_d_right = 2130837511;
        public static final int btn_n_ld = 2130837512;
        public static final int btn_n_rd = 2130837513;
        public static final int change_name = 2130837514;
        public static final int continue_game = 2130837515;
        public static final int continue_game_disable = 2130837516;
        public static final int edit_arrow = 2130837517;
        public static final int edit_box = 2130837518;
        public static final int edit_elastic = 2130837519;
        public static final int edit_eraser = 2130837520;
        public static final int edit_line = 2130837521;
        public static final int edit_play = 2130837522;
        public static final int edit_save = 2130837523;
        public static final int edit_simple = 2130837524;
        public static final int edit_slippery = 2130837525;
        public static final int flags = 2130837526;
        public static final int game_logo = 2130837527;
        public static final int gradient = 2130837528;
        public static final int icon = 2130837529;
        public static final int line = 2130837530;
        public static final int logo_for_ad = 2130837531;
        public static final int new_game = 2130837532;
        public static final int retry = 2130837533;
        public static final int score = 2130837534;
    }

    public static final class id {
        public static final int changeNameButton = 2131034127;
        public static final int continueGameButton = 2131034126;
        public static final int edit_arrow = 2131034119;
        public static final int edit_box = 2131034122;
        public static final int edit_elastic = 2131034117;
        public static final int edit_eraser = 2131034120;
        public static final int edit_line = 2131034121;
        public static final int edit_play = 2131034115;
        public static final int edit_save = 2131034123;
        public static final int edit_simple = 2131034116;
        public static final int edit_slippery = 2131034118;
        public static final int gameRenderSurfaceView = 2131034114;
        public static final int levelEditRenderSurfaceView = 2131034124;
        public static final int mainAdView = 2131034113;
        public static final int newGameButton = 2131034125;
        public static final int scoreButton = 2131034128;
        public static final int tableLayout = 2131034129;
        public static final int test = 2131034130;
        public static final int towerPerfectDropEffect = 2131034112;
    }

    public static final class layout {
        public static final int effect_main = 2130903040;
        public static final int game = 2130903041;
        public static final int level_edit = 2130903042;
        public static final int main = 2130903043;
        public static final int rating = 2130903044;
        public static final int text = 2130903045;
    }

    public static final class string {
        public static final int app_name = 2130968590;
        public static final int buttonApply = 2130968593;
        public static final int buttonCancel = 2130968594;
        public static final int failTry = 2130968609;
        public static final int gameFPS = 2130968607;
        public static final int gameLevel = 2130968605;
        public static final int gameRetry = 2130968603;
        public static final int gameScore = 2130968604;
        public static final int gameTry = 2130968606;
        public static final int invalidNickname = 2130968597;
        public static final int labelCallKeyboard = 2130968592;
        public static final int labelFilename = 2130968599;
        public static final int labelNickname = 2130968591;
        public static final int levelbuild_save_fail = 2130968602;
        public static final int levelbuild_save_success = 2130968601;
        public static final int noname = 2130968596;
        public static final int ratingButtonApply = 2130968582;
        public static final int ratingButtonCancel = 2130968583;
        public static final int ratingButtonRetry = 2130968581;
        public static final int ratingDialogPleaseWait = 2130968576;
        public static final int ratingDialogRatingEmpty = 2130968578;
        public static final int ratingDialogRatingLoadedWithError = 2130968577;
        public static final int ratingLabelNickname = 2130968580;
        public static final int ratingNoname = 2130968589;
        public static final int ratingTabFragmentTop100 = 2130968587;
        public static final int ratingTabFragmentYourScore = 2130968588;
        public static final int ratingTabPeriodAll = 2130968584;
        public static final int ratingTabPeriodMonth = 2130968585;
        public static final int ratingTabPeriodWeek = 2130968586;
        public static final int ratingTitleEnterNickname = 2130968579;
        public static final int settings_effects_off = 2130968611;
        public static final int settings_effects_on = 2130968612;
        public static final int settings_sounds_off = 2130968613;
        public static final int settings_sounds_on = 2130968614;
        public static final int settings_title = 2130968610;
        public static final int success = 2130968598;
        public static final int tableTitle = 2130968608;
        public static final int titleEnterFilename = 2130968600;
        public static final int titleEnterNickname = 2130968595;
    }
}
