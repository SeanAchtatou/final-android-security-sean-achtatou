package ru.aeradeve.games.effects.tower.particle;

import org.anddev.andengine.entity.particle.emitter.BaseRectangleParticleEmitter;
import org.anddev.andengine.util.MathUtils;

public class RectangleSideParticleEmitter extends BaseRectangleParticleEmitter {
    private EnableSides mEnableSides;

    public static class EnableSides {
        public static final int BOTTOM_ID = 3;
        public static final int LEFT_ID = 0;
        public static final int RIGHT_ID = 1;
        public static final int TOP_ID = 2;
        public boolean bottom;
        public boolean left;
        public boolean right;
        public boolean top;

        public EnableSides(boolean left2, boolean right2, boolean top2, boolean bottom2) {
            this.left = left2;
            this.right = right2;
            this.top = top2;
            this.bottom = bottom2;
        }

        public boolean isAllSidesDisable() {
            return !this.left && !this.right && !this.top && !this.bottom;
        }

        public boolean isEnabled(int sideId) {
            switch (sideId) {
                case 0:
                    return this.left;
                case 1:
                    return this.right;
                case 2:
                    return this.top;
                case 3:
                    return this.bottom;
                default:
                    return false;
            }
        }
    }

    public RectangleSideParticleEmitter(float pCenterX, float pCenterY, float pWidth, float pHeight, EnableSides enableSides) {
        super(pCenterX, pCenterY, pWidth, pHeight);
        this.mEnableSides = enableSides;
    }

    public void getPositionOffset(float[] pOffset) {
        float y;
        float x;
        if (!this.mEnableSides.isAllSidesDisable()) {
            int sideId = MathUtils.RANDOM.nextInt(4);
            while (!this.mEnableSides.isEnabled(sideId)) {
                sideId = MathUtils.RANDOM.nextInt(4);
            }
            if (sideId == 0 || sideId == 1) {
                if (sideId == 0) {
                    x = this.mCenterX - this.mWidthHalf;
                } else {
                    x = this.mCenterX + this.mWidthHalf;
                }
                y = this.mCenterY + (((float) MathUtils.randomSign()) * MathUtils.RANDOM.nextFloat() * this.mHeightHalf);
            } else {
                x = this.mCenterX + (((float) MathUtils.randomSign()) * MathUtils.RANDOM.nextFloat() * this.mWidthHalf);
                if (sideId == 2) {
                    y = this.mCenterY - this.mHeightHalf;
                } else {
                    y = this.mCenterY + this.mHeightHalf;
                }
            }
            pOffset[0] = x;
            pOffset[1] = y;
        }
    }
}
