package ru.aeradeve.games.effects.tower;

import android.graphics.Color;
import java.util.Random;
import org.anddev.andengine.engine.handler.physics.PhysicsHandler;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.particle.Particle;
import org.anddev.andengine.entity.particle.ParticleSystem;
import org.anddev.andengine.entity.particle.emitter.CircleParticleEmitter;
import org.anddev.andengine.entity.particle.initializer.AlphaInitializer;
import org.anddev.andengine.entity.particle.initializer.ColorInitializer;
import org.anddev.andengine.entity.particle.modifier.AlphaModifier;
import org.anddev.andengine.entity.particle.modifier.ColorModifier;
import org.anddev.andengine.entity.particle.modifier.ExpireModifier;
import org.anddev.andengine.entity.particle.modifier.IParticleModifier;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.MathUtils;
import ru.aeradeve.games.effects.tower.particle.OnParticleSystemDieListener;
import ru.aeradeve.games.effects.tower.particle.OneStepParticleSystem;
import ru.aeradeve.games.effects.tower.particle.RectangleSideParticleEmitter;

public class PerfectDropEffectBuilder {
    private static final float BIG_SPAWN_RATE = 300000.0f;
    private static final float EPS = 0.01f;
    private static final float FIELD_BIG_HALF_SIZE = 500.0f;
    private static final float FIELD_BIG_SIZE = 1000.0f;
    /* access modifiers changed from: private */
    public TextureRegion mParticleTextureRegion;
    /* access modifiers changed from: private */
    public Random mRandom = new Random();
    /* access modifiers changed from: private */
    public Scene mScene;

    public static class PerfectDropEffectSettings {
        public RectangleSideParticleEmitter.EnableSides enableSides;
        public int finishColor;
        public int maxParticleCount;
        public int middleColor;
        public float particleAliveTime;
        public int speedConstantPart;
        public int speedRandomPart;
        public int startColor;

        public PerfectDropEffectSettings(int startColor2, int middleColor2, int finishColor2, int speedConstantPart2, int speedRandomPart2, int maxParticleCount2, float particleAliveTime2, RectangleSideParticleEmitter.EnableSides enableSides2) {
            this.startColor = startColor2;
            this.middleColor = middleColor2;
            this.finishColor = finishColor2;
            this.speedConstantPart = speedConstantPart2;
            this.speedRandomPart = speedRandomPart2;
            this.maxParticleCount = maxParticleCount2;
            this.particleAliveTime = particleAliveTime2;
            this.enableSides = enableSides2;
        }
    }

    public PerfectDropEffectBuilder(Scene scene, TextureRegion particleTextureRegion) {
        this.mScene = scene;
        this.mParticleTextureRegion = particleTextureRegion;
    }

    public void createEffect(IEntity layer, float centerX, float centerY, float rectangleWidth, float rectangleHeight, PerfectDropEffectSettings settings) {
        final IEntity iEntity = layer;
        OneStepParticleSystem particleSystem = new OneStepParticleSystem(new CircleParticleEmitter(centerX, centerY, 5.0f), BIG_SPAWN_RATE, BIG_SPAWN_RATE, settings.maxParticleCount, this.mParticleTextureRegion, new OnParticleSystemDieListener() {
            public void onParticleSystemDie(final ParticleSystem particleSystem) {
                PerfectDropEffectBuilder.this.mScene.postRunnable(new Runnable() {
                    public void run() {
                        particleSystem.setParticlesSpawnEnabled(false);
                        iEntity.detachChild(particleSystem);
                    }
                });
            }
        });
        int startColor = settings.startColor;
        int middleColor = settings.middleColor;
        int finishColor = settings.finishColor;
        float middleTime = settings.particleAliveTime * 0.5f;
        float finishTime = settings.particleAliveTime;
        particleSystem.addParticleInitializer(new ColorInitializer((1.0f * ((float) Color.red(startColor))) / 255.0f, (1.0f * ((float) Color.green(startColor))) / 255.0f, (1.0f * ((float) Color.blue(startColor))) / 255.0f));
        particleSystem.addParticleInitializer(new AlphaInitializer(0.0f));
        particleSystem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 1);
        particleSystem.addParticleModifier(new ColorModifier((1.0f * ((float) Color.red(startColor))) / 255.0f, (1.0f * ((float) Color.red(middleColor))) / 255.0f, (1.0f * ((float) Color.green(startColor))) / 255.0f, (1.0f * ((float) Color.green(middleColor))) / 255.0f, (1.0f * ((float) Color.blue(startColor))) / 255.0f, (1.0f * ((float) Color.blue(middleColor))) / 255.0f, 0.0f, middleTime));
        particleSystem.addParticleModifier(new ColorModifier((1.0f * ((float) Color.red(middleColor))) / 255.0f, (1.0f * ((float) Color.red(finishColor))) / 255.0f, (1.0f * ((float) Color.green(middleColor))) / 255.0f, (1.0f * ((float) Color.green(finishColor))) / 255.0f, (1.0f * ((float) Color.blue(middleColor))) / 255.0f, (1.0f * ((float) Color.blue(finishColor))) / 255.0f, middleTime, finishTime));
        particleSystem.addParticleModifier(new AlphaModifier(0.0f, 1.0f, 0.0f, 0.15f * finishTime));
        particleSystem.addParticleModifier(new AlphaModifier(1.0f, 0.0f, middleTime, finishTime));
        particleSystem.addParticleModifier(new ExpireModifier(finishTime));
        final PerfectDropEffectSettings perfectDropEffectSettings = settings;
        particleSystem.addParticleModifier(new IParticleModifier() {
            public void onUpdateParticle(Particle particle) {
                particle.getPhysicsHandler().setVelocityY(particle.getPhysicsHandler().getVelocityY() + 1.0f);
            }

            public void onInitializeParticle(Particle particle) {
                float x = particle.getX() + (((float) MathUtils.randomSign()) * MathUtils.RANDOM.nextFloat() * PerfectDropEffectBuilder.FIELD_BIG_HALF_SIZE);
                float y = particle.getY() + (((float) MathUtils.randomSign()) * MathUtils.RANDOM.nextFloat() * PerfectDropEffectBuilder.FIELD_BIG_HALF_SIZE);
                float x2 = x - particle.getX();
                float y2 = y - particle.getY();
                float d = (float) Math.sqrt((double) ((x2 * x2) + (y2 * y2)));
                float x3 = (x2 / d) * ((float) generateSpeed(perfectDropEffectSettings));
                float y3 = (y2 / d) * ((float) generateSpeed(perfectDropEffectSettings));
                particle.setPosition(particle.getX() - (((float) PerfectDropEffectBuilder.this.mParticleTextureRegion.getWidth()) / 2.0f), particle.getY() - (((float) PerfectDropEffectBuilder.this.mParticleTextureRegion.getHeight()) / 2.0f));
                PhysicsHandler physicsHandler = new PhysicsHandler(particle);
                particle.registerUpdateHandler(physicsHandler);
                physicsHandler.setVelocity(x3, y3);
            }

            private int generateSpeed(PerfectDropEffectSettings settings) {
                return PerfectDropEffectBuilder.this.mRandom.nextInt(settings.speedConstantPart) + settings.speedRandomPart;
            }
        });
        layer.attachChild(particleSystem);
    }

    public TextureRegion getParticleTextureRegion() {
        return this.mParticleTextureRegion;
    }

    public void setParticleTextureRegion(TextureRegion mParticleTextureRegion2) {
        this.mParticleTextureRegion = mParticleTextureRegion2;
    }

    public Scene getScene() {
        return this.mScene;
    }

    public void setScene(Scene scene) {
        this.mScene = scene;
    }
}
