package de.madvertise.android.sdk;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

public class StaticBannerView extends ImageView {
    public StaticBannerView(Context context, Bitmap bannerBitmap) {
        super(context);
        if (bannerBitmap != null) {
            setImageBitmap(bannerBitmap);
        }
    }
}
