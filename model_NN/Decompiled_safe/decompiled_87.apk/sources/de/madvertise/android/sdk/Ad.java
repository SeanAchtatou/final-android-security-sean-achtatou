package de.madvertise.android.sdk;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Ad {
    private final String BANNER_URL_CODE = "banner_url";
    private final String CLICK_URL_CODE = "click_url";
    private final String HAS_BANNER_CODE = "has_banner";
    private final String TEXT_CODE = "text";
    private String bannerURL;
    private String clickURL;
    private Context context;
    private boolean hasBanner;
    private boolean hasBannerLink;
    private byte[] imageByteArray;
    private JSONArray jsonNames;
    private JSONArray jsonValues;
    private String text;

    protected Ad(Context context2, JSONObject json) {
        this.context = context2;
        MadUtil.logMessage(null, 3, "Creating ad");
        this.jsonNames = json.names();
        try {
            this.jsonValues = json.toJSONArray(this.jsonNames);
            for (int i = 0; i < this.jsonNames.length(); i++) {
                MadUtil.logMessage(null, 3, "Key => " + this.jsonNames.getString(i) + " Value => " + this.jsonValues.getString(i));
            }
            this.clickURL = json.isNull("click_url") ? "" : json.getString("click_url");
            this.bannerURL = json.isNull("banner_url") ? "" : json.getString("banner_url");
            this.text = json.isNull("text") ? "" : json.getString("text");
            this.hasBannerLink = Boolean.parseBoolean(json.isNull("has_banner") ? "true" : json.getString("has_banner"));
        } catch (JSONException e) {
            MadUtil.logMessage(null, 3, "Error in json string");
            e.printStackTrace();
        }
        if (this.hasBannerLink) {
            this.imageByteArray = downloadImage(this.bannerURL);
        } else {
            MadUtil.logMessage(null, 3, "No banner link in json found");
        }
        if (this.imageByteArray != null) {
            this.hasBanner = true;
        } else {
            this.hasBanner = false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00ed A[SYNTHETIC, Splitter:B:40:0x00ed] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f2 A[SYNTHETIC, Splitter:B:43:0x00f2] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] downloadImage(java.lang.String r20) {
        /*
            r19 = this;
            r10 = 0
            r2 = 0
            org.apache.http.impl.client.DefaultHttpClient r4 = new org.apache.http.impl.client.DefaultHttpClient
            r4.<init>()
            org.apache.http.client.methods.HttpGet r8 = new org.apache.http.client.methods.HttpGet
            r0 = r8
            r1 = r20
            r0.<init>(r1)
            r11 = 0
            r13 = 0
            if (r20 != 0) goto L_0x0015
            r14 = r13
        L_0x0014:
            return r14
        L_0x0015:
            org.apache.http.params.HttpParams r5 = r4.getParams()
            java.lang.Integer r15 = de.madvertise.android.sdk.MadUtil.CONNECTION_TIMEOUT
            int r15 = r15.intValue()
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r5, r15)
            java.lang.Integer r15 = de.madvertise.android.sdk.MadUtil.CONNECTION_TIMEOUT
            int r15 = r15.intValue()
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r5, r15)
            r15 = 0
            r16 = 3
            java.lang.StringBuilder r17 = new java.lang.StringBuilder
            r17.<init>()
            java.lang.String r18 = "Try to download banner: "
            java.lang.StringBuilder r17 = r17.append(r18)
            r0 = r17
            r1 = r20
            java.lang.StringBuilder r17 = r0.append(r1)
            java.lang.String r17 = r17.toString()
            de.madvertise.android.sdk.MadUtil.logMessage(r15, r16, r17)
            org.apache.http.HttpResponse r11 = r4.execute(r8)     // Catch:{ IOException -> 0x00d0 }
            r15 = 0
            r16 = 3
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00d0 }
            r17.<init>()     // Catch:{ IOException -> 0x00d0 }
            java.lang.String r18 = "Response Code=> "
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ IOException -> 0x00d0 }
            org.apache.http.StatusLine r18 = r11.getStatusLine()     // Catch:{ IOException -> 0x00d0 }
            int r18 = r18.getStatusCode()     // Catch:{ IOException -> 0x00d0 }
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ IOException -> 0x00d0 }
            java.lang.String r17 = r17.toString()     // Catch:{ IOException -> 0x00d0 }
            de.madvertise.android.sdk.MadUtil.logMessage(r15, r16, r17)     // Catch:{ IOException -> 0x00d0 }
            org.apache.http.HttpEntity r7 = r11.getEntity()     // Catch:{ IOException -> 0x00d0 }
            org.apache.http.StatusLine r15 = r11.getStatusLine()     // Catch:{ IOException -> 0x00d0 }
            int r12 = r15.getStatusCode()     // Catch:{ IOException -> 0x00d0 }
            r15 = 200(0xc8, float:2.8E-43)
            if (r12 != r15) goto L_0x00ad
            if (r7 == 0) goto L_0x00ad
            org.apache.http.HttpEntity r15 = r11.getEntity()     // Catch:{ IOException -> 0x00d0 }
            java.io.InputStream r10 = r15.getContent()     // Catch:{ IOException -> 0x00d0 }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x00d0 }
            r3.<init>()     // Catch:{ IOException -> 0x00d0 }
            int r9 = r10.read()     // Catch:{ IOException -> 0x0103, all -> 0x0100 }
        L_0x0090:
            r15 = -1
            if (r9 == r15) goto L_0x009b
            r3.write(r9)     // Catch:{ IOException -> 0x0103, all -> 0x0100 }
            int r9 = r10.read()     // Catch:{ IOException -> 0x0103, all -> 0x0100 }
            goto L_0x0090
        L_0x009b:
            byte[] r13 = r3.toByteArray()     // Catch:{ IOException -> 0x0103, all -> 0x0100 }
            r2 = r3
        L_0x00a0:
            if (r10 == 0) goto L_0x00a5
            r10.close()     // Catch:{ IOException -> 0x00f6 }
        L_0x00a5:
            if (r2 == 0) goto L_0x00aa
            r2.close()     // Catch:{ IOException -> 0x00f8 }
        L_0x00aa:
            r14 = r13
            goto L_0x0014
        L_0x00ad:
            r15 = 0
            r16 = 3
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00d0 }
            r17.<init>()     // Catch:{ IOException -> 0x00d0 }
            java.lang.String r18 = "Could not download banner because response code is "
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ IOException -> 0x00d0 }
            r0 = r17
            r1 = r12
            java.lang.StringBuilder r17 = r0.append(r1)     // Catch:{ IOException -> 0x00d0 }
            java.lang.String r18 = " (expected 200) or empty body"
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ IOException -> 0x00d0 }
            java.lang.String r17 = r17.toString()     // Catch:{ IOException -> 0x00d0 }
            de.madvertise.android.sdk.MadUtil.logMessage(r15, r16, r17)     // Catch:{ IOException -> 0x00d0 }
            goto L_0x00a0
        L_0x00d0:
            r15 = move-exception
            r6 = r15
        L_0x00d2:
            r15 = 0
            r16 = 3
            java.lang.String r17 = "Cannot fetch banner or icon from server"
            de.madvertise.android.sdk.MadUtil.logMessage(r15, r16, r17)     // Catch:{ all -> 0x00ea }
            r6.printStackTrace()     // Catch:{ all -> 0x00ea }
            if (r10 == 0) goto L_0x00e2
            r10.close()     // Catch:{ IOException -> 0x00fa }
        L_0x00e2:
            if (r2 == 0) goto L_0x00aa
            r2.close()     // Catch:{ IOException -> 0x00e8 }
            goto L_0x00aa
        L_0x00e8:
            r15 = move-exception
            goto L_0x00aa
        L_0x00ea:
            r15 = move-exception
        L_0x00eb:
            if (r10 == 0) goto L_0x00f0
            r10.close()     // Catch:{ IOException -> 0x00fc }
        L_0x00f0:
            if (r2 == 0) goto L_0x00f5
            r2.close()     // Catch:{ IOException -> 0x00fe }
        L_0x00f5:
            throw r15
        L_0x00f6:
            r15 = move-exception
            goto L_0x00a5
        L_0x00f8:
            r15 = move-exception
            goto L_0x00aa
        L_0x00fa:
            r15 = move-exception
            goto L_0x00e2
        L_0x00fc:
            r16 = move-exception
            goto L_0x00f0
        L_0x00fe:
            r16 = move-exception
            goto L_0x00f5
        L_0x0100:
            r15 = move-exception
            r2 = r3
            goto L_0x00eb
        L_0x0103:
            r15 = move-exception
            r6 = r15
            r2 = r3
            goto L_0x00d2
        */
        throw new UnsupportedOperationException("Method not decompiled: de.madvertise.android.sdk.Ad.downloadImage(java.lang.String):byte[]");
    }

    /* access modifiers changed from: protected */
    public void handleClick() {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.clickURL));
        intent.addFlags(268435456);
        try {
            this.context.startActivity(intent);
        } catch (Exception e) {
            MadUtil.logMessage(null, 3, "Failed to open URL : " + this.clickURL);
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public String getClickURL() {
        return this.clickURL;
    }

    /* access modifiers changed from: protected */
    public String getBannerURL() {
        return this.bannerURL;
    }

    /* access modifiers changed from: protected */
    public String getText() {
        return this.text;
    }

    /* access modifiers changed from: protected */
    public boolean hasBanner() {
        return this.hasBanner;
    }

    /* access modifiers changed from: protected */
    public byte[] getImageByteArray() {
        return this.imageByteArray;
    }
}
