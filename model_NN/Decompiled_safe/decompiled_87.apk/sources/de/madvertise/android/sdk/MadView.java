package de.madvertise.android.sdk;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import org.anddev.andengine.level.util.constants.LevelConstants;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

public class MadView extends LinearLayout {
    private static final boolean IS_TESTMODE_DEFAULT = false;
    private final double GRADIENT_STOP;
    private final int GRADIENT_TOP_ALPHA;
    private Timer adTimer;
    private int backgroundColor;
    private int bannerHeight;
    /* access modifiers changed from: private */
    public String bannerType;
    private MadViewCallbackListener callbackListener;
    /* access modifiers changed from: private */
    public Ad currentAd;
    /* access modifiers changed from: private */
    public boolean deliverOnlyText;
    private Drawable initialBackground;
    /* access modifiers changed from: private */
    public final Handler mHandler;
    /* access modifiers changed from: private */
    public final Runnable mUpdateResults;
    private boolean runningRefreshAd;
    private int secondsToRefreshAd;
    /* access modifiers changed from: private */
    public boolean testMode;
    private BitmapDrawable textBannerBackground;
    private int textColor;
    private int textSize;

    public interface MadViewCallbackListener {
        void onLoaded(boolean z, MadView madView);
    }

    public MadView(Context context) {
        this(context, null);
    }

    public MadView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.GRADIENT_TOP_ALPHA = 127;
        this.GRADIENT_STOP = 0.7375d;
        this.textColor = -1;
        this.backgroundColor = 0;
        this.secondsToRefreshAd = 30;
        this.testMode = false;
        this.bannerType = "mma";
        this.deliverOnlyText = false;
        this.textSize = 18;
        this.bannerHeight = 53;
        this.callbackListener = null;
        this.adTimer = null;
        this.mHandler = new Handler();
        this.runningRefreshAd = false;
        this.initialBackground = null;
        this.mUpdateResults = new Runnable() {
            public void run() {
                MadView.this.refreshView();
            }
        };
        MadUtil.logMessage(null, 3, "** Constructor for mad view called **");
        setVisibility(4);
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") == -1) {
            MadUtil.logMessage(null, 3, " *** ----------------------------- *** ");
            MadUtil.logMessage(null, 3, " *** Missing internet permissions! *** ");
            MadUtil.logMessage(null, 3, " *** ----------------------------- *** ");
            throw new IllegalArgumentException();
        }
        initParameters(attrs);
        Display display = ((WindowManager) getContext().getSystemService("window")).getDefaultDisplay();
        MadUtil.logMessage(null, 3, "Display values: Width = " + display.getWidth() + " ; Height = " + display.getHeight());
        setGravity(17);
        this.initialBackground = getBackground();
        this.textBannerBackground = generateBackgroundDrawable(new Rect(0, 0, display.getWidth(), display.getHeight()), this.backgroundColor, 16777215);
        setClickable(true);
        setFocusable(true);
        setDescendantFocusability(131072);
    }

    public boolean onTouchEvent(MotionEvent event) {
        MadUtil.logMessage(null, 3, "onTouchEvent(MotionEvent event) fired");
        if (this.currentAd == null) {
            return true;
        }
        this.currentAd.handleClick();
        return true;
    }

    /* access modifiers changed from: private */
    public void refreshView() {
        setBackgroundDrawable(this.initialBackground);
        if (this.currentAd != null) {
            if (!this.currentAd.hasBanner() || this.deliverOnlyText) {
                showTextBannerView();
            } else {
                showStaticBannerView();
            }
            notifyListener(true);
            return;
        }
        removeAllViews();
        notifyListener(false);
    }

    private void showStaticBannerView() {
        MadUtil.logMessage(null, 3, "Add static banner");
        StaticBannerView staticBannerView = new StaticBannerView(getContext(), BitmapFactory.decodeByteArray(this.currentAd.getImageByteArray(), 0, this.currentAd.getImageByteArray().length));
        removeAllViews();
        addView(staticBannerView);
    }

    private void showTextBannerView() {
        MadUtil.logMessage(null, 3, "Add text banner");
        TextView textView = new TextView(getContext());
        textView.setGravity(17);
        textView.setText(this.currentAd.getText());
        textView.setTextSize((float) this.textSize);
        textView.setTextColor(this.textColor);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        setBackgroundDrawable(this.textBannerBackground);
        removeAllViews();
        addView(textView);
    }

    private void notifyListener(boolean succeed) {
        if (this.callbackListener != null) {
            this.callbackListener.onLoaded(succeed, this);
        } else {
            MadUtil.logMessage(null, 3, "Callback Listener not set");
        }
        if (getWindowVisibility() == 0) {
            refreshAdTimer(true);
        }
    }

    public void setVisibility(int visibility) {
        if (super.getVisibility() != visibility) {
            synchronized (this) {
                int childViewCounter = getChildCount();
                for (int i = 0; i < childViewCounter; i++) {
                    getChildAt(i).setVisibility(visibility);
                }
                super.setVisibility(visibility);
            }
        }
    }

    private void initParameters(AttributeSet attrs) {
        if (attrs != null) {
            String packageName = "http://schemas.android.com/apk/res/" + getContext().getPackageName();
            if (packageName != null) {
                MadUtil.logMessage(null, 3, "namespace = " + packageName);
            }
            this.testMode = attrs.getAttributeBooleanValue(packageName, "isTestMode", false);
            this.textColor = attrs.getAttributeIntValue(packageName, "textColor", -1);
            this.backgroundColor = attrs.getAttributeIntValue(packageName, "backgroundColor", 0);
            this.secondsToRefreshAd = attrs.getAttributeIntValue(packageName, "secondsToRefresh", 30);
            this.bannerType = attrs.getAttributeValue(packageName, "bannerType");
            if (this.bannerType == null) {
                this.bannerType = "mma";
            }
            this.deliverOnlyText = attrs.getAttributeBooleanValue(packageName, "deliverOnlyText", false);
            this.textSize = attrs.getAttributeIntValue(packageName, "textSize", 18);
        } else {
            MadUtil.logMessage(null, 3, "AttributeSet is null!");
        }
        if (this.secondsToRefreshAd != 0 && this.secondsToRefreshAd < 60) {
            this.secondsToRefreshAd = 30;
        }
        if (this.bannerType.equals("iab")) {
            this.bannerHeight = 250;
        }
        MadUtil.logMessage(null, 3, "Using following attributes values:");
        MadUtil.logMessage(null, 3, " testMode = " + this.testMode);
        MadUtil.logMessage(null, 3, " textColor = " + this.textColor);
        MadUtil.logMessage(null, 3, " backgroundColor = " + this.backgroundColor);
        MadUtil.logMessage(null, 3, " secondsToRefreshAd = " + this.secondsToRefreshAd);
        MadUtil.logMessage(null, 3, " bannerType = " + this.bannerType);
        MadUtil.logMessage(null, 3, " deliverOnlyText = " + this.deliverOnlyText);
        MadUtil.logMessage(null, 3, " textSize = " + this.textSize);
        MadUtil.logMessage(null, 3, " bannerHeight = " + this.bannerHeight);
    }

    /* access modifiers changed from: private */
    public void requestNewAd() {
        MadUtil.logMessage(null, 3, "Trying to fetch a new ad");
        if (this.runningRefreshAd) {
            MadUtil.logMessage(null, 3, "Another request is still in progress ...");
        } else {
            new Thread() {
                public void run() {
                    String uid;
                    String siteToken = MadUtil.getToken(MadView.this.getContext());
                    if (siteToken == null) {
                        siteToken = "";
                        MadUtil.logMessage(null, 3, "Cannot show ads, since the appID ist null");
                    } else {
                        MadUtil.logMessage(null, 3, "appID = " + siteToken);
                    }
                    String uid2 = Settings.Secure.getString(MadView.this.getContext().getContentResolver(), "android_id");
                    if (uid2 == null) {
                        uid = "";
                    } else {
                        uid = MadView.this.getMD5Hash(uid2);
                    }
                    MadUtil.logMessage(null, 3, "uid = " + uid);
                    Display display = ((WindowManager) MadView.this.getContext().getSystemService("window")).getDefaultDisplay();
                    int displayHeight = display.getHeight();
                    int displayWidth = display.getWidth();
                    MadUtil.logMessage(null, 3, "Display height = " + Integer.toString(displayHeight));
                    MadUtil.logMessage(null, 3, "Display width = " + Integer.toString(displayWidth));
                    HttpPost httpPost = new HttpPost("http://ad.madvertise.de/site/" + siteToken);
                    httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(new BasicNameValuePair("ua", MadUtil.getUA()));
                    arrayList.add(new BasicNameValuePair("app", "true"));
                    arrayList.add(new BasicNameValuePair("debug", Boolean.toString(MadView.this.testMode)));
                    arrayList.add(new BasicNameValuePair("ip", MadUtil.getLocalIpAddress()));
                    arrayList.add(new BasicNameValuePair("format", "json"));
                    arrayList.add(new BasicNameValuePair("requester", "android_sdk"));
                    arrayList.add(new BasicNameValuePair("version", "1.1"));
                    arrayList.add(new BasicNameValuePair(LevelConstants.TAG_LEVEL_ATTRIBUTE_UID, uid));
                    arrayList.add(new BasicNameValuePair("banner_type", MadView.this.bannerType));
                    arrayList.add(new BasicNameValuePair("deliver_only_text", Boolean.toString(MadView.this.deliverOnlyText)));
                    MadUtil.refreshCoordinates(MadView.this.getContext());
                    if (MadUtil.getLocation() != null) {
                        arrayList.add(new BasicNameValuePair("lat", Double.toString(MadUtil.getLocation().getLatitude())));
                        arrayList.add(new BasicNameValuePair("lng", Double.toString(MadUtil.getLocation().getLongitude())));
                    }
                    HttpEntity httpEntity = null;
                    try {
                        httpEntity = new UrlEncodedFormEntity(arrayList);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    httpPost.setEntity(httpEntity);
                    MadUtil.logMessage(null, 3, "Post request created");
                    MadUtil.logMessage(null, 3, "Uri : " + httpPost.getURI().toASCIIString());
                    MadUtil.logMessage(null, 3, "All headers : " + MadUtil.getAllHeadersAsString(httpPost.getAllHeaders()));
                    MadUtil.logMessage(null, 3, "All request parameters :" + MadUtil.printRequestParameters(arrayList));
                    synchronized (this) {
                        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                        InputStream inputStream = null;
                        boolean jsonFetched = false;
                        JSONObject json = null;
                        try {
                            HttpParams clientParams = defaultHttpClient.getParams();
                            HttpConnectionParams.setConnectionTimeout(clientParams, MadUtil.CONNECTION_TIMEOUT.intValue());
                            HttpConnectionParams.setSoTimeout(clientParams, MadUtil.CONNECTION_TIMEOUT.intValue());
                            MadUtil.logMessage(null, 3, "Sending request");
                            HttpResponse httpResponse = defaultHttpClient.execute(httpPost);
                            MadUtil.logMessage(null, 3, "Response Code => " + httpResponse.getStatusLine().getStatusCode());
                            if (MadView.this.testMode) {
                                MadUtil.logMessage(null, 3, "Madvertise Debug Response: " + httpResponse.getLastHeader("X-Madvertise-Debug"));
                            }
                            int responseCode = httpResponse.getStatusLine().getStatusCode();
                            HttpEntity entity = httpResponse.getEntity();
                            if (responseCode == 200 && entity != null) {
                                inputStream = entity.getContent();
                                String resultString = MadUtil.convertStreamToString(inputStream);
                                MadUtil.logMessage(null, 3, "Response => " + resultString);
                                jsonFetched = true;
                                json = new JSONObject(resultString);
                            }
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException e2) {
                                }
                            }
                        } catch (ClientProtocolException e3) {
                            e3.printStackTrace();
                            MadUtil.logMessage(null, 3, "Error in HTTP request / protocol");
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException e4) {
                                }
                            }
                        } catch (IOException e5) {
                            e5.printStackTrace();
                            MadUtil.logMessage(null, 3, "Could not receive a http response on an ad reqeust");
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException e6) {
                                }
                            }
                        } catch (JSONException e7) {
                            e7.printStackTrace();
                            MadUtil.logMessage(null, 3, "Could not parse json object");
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException e8) {
                                }
                            }
                        } catch (Throwable th) {
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException e9) {
                                }
                            }
                            throw th;
                        }
                        if (jsonFetched) {
                            Ad unused = MadView.this.currentAd = new Ad(MadView.this.getContext(), json);
                        }
                    }
                    MadView.this.mHandler.post(MadView.this.mUpdateResults);
                }
            }.start();
        }
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        MadUtil.logMessage(null, 3, "#### onWindowFocusChanged fired ####");
        refreshAdTimer(hasWindowFocus);
        super.onWindowFocusChanged(hasWindowFocus);
    }

    public void startLoadingAd() {
        refreshAdTimer(true, true);
    }

    public void stopLoading() {
        refreshAdTimer(false);
    }

    private BitmapDrawable generateBackgroundDrawable(Rect rect, int backgroundColor2, int shineColor) {
        try {
            Bitmap bitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            drawTextBannerBackground(new Canvas(bitmap), rect, backgroundColor2, shineColor);
            return new BitmapDrawable(bitmap);
        } catch (Throwable th) {
            return null;
        }
    }

    private void drawTextBannerBackground(Canvas canvas, Rect rectangle, int backgroundColor2, int shineColor) {
        Paint paint = new Paint();
        paint.setColor(backgroundColor2);
        paint.setAntiAlias(true);
        canvas.drawRect(rectangle, paint);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(127, Color.red(shineColor), Color.green(shineColor), Color.blue(shineColor)), shineColor});
        int stop = ((int) (((double) rectangle.height()) * 0.7375d)) + rectangle.top;
        gradientDrawable.setBounds(rectangle.left, rectangle.top, rectangle.right, stop);
        gradientDrawable.draw(canvas);
        Rect shadowRect = new Rect(rectangle.left, stop, rectangle.right, rectangle.bottom);
        Paint shadowPaint = new Paint();
        shadowPaint.setColor(shineColor);
        canvas.drawRect(shadowRect, shadowPaint);
    }

    private void refreshAdTimer(boolean starting) {
        refreshAdTimer(starting, false);
    }

    private void refreshAdTimer(boolean starting, boolean fastRun) {
        Log.d("WAd", "run refreshAdTimer starting=" + starting + " fastRun=" + fastRun + " this=" + this + " seconds=" + this.secondsToRefreshAd);
        synchronized (this) {
            if (starting) {
                if (this.adTimer != null) {
                    this.adTimer.cancel();
                }
                this.adTimer = new Timer();
            } else if (this.adTimer != null) {
                MadUtil.logMessage(null, 3, "Stopping refresh timer ...");
                this.adTimer.cancel();
                this.adTimer = null;
            }
            if (this.adTimer != null) {
                this.adTimer.schedule(new TimerTask() {
                    public void run() {
                        MadUtil.logMessage(null, 3, "Refreshing ad ...");
                        MadView.this.requestNewAd();
                    }
                }, fastRun ? 0 : ((long) this.secondsToRefreshAd) * 1000);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), this.bannerHeight);
    }

    /* access modifiers changed from: private */
    public String getMD5Hash(String input) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(input.getBytes());
            byte[] digest = messageDigest.digest();
            StringBuffer hexString = new StringBuffer();
            for (byte b : digest) {
                String temp = Integer.toHexString(b & 255);
                if (temp.length() < 2) {
                    temp = "0" + temp;
                }
                hexString.append(temp);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            MadUtil.logMessage(null, 3, "Could not create hash value");
            return "";
        }
    }

    public void removeMadViewCallbackListener() {
        this.callbackListener = null;
    }

    public MadViewCallbackListener getCallbackListener() {
        return this.callbackListener;
    }

    public void setMadViewCallbackListener(MadViewCallbackListener listener) {
        this.callbackListener = listener;
    }
}
