package de.madvertise.android.sdk;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import org.anddev.andengine.util.constants.TimeConstants;
import org.apache.http.Header;
import org.apache.http.NameValuePair;

public class MadUtil {
    protected static final int BACKGROUND_COLOR_DEFAULT = 0;
    protected static final String BANNER_TYPE_DEFAULT = "mma";
    protected static final Integer CONNECTION_TIMEOUT = new Integer((int) TimeConstants.MILLISECONDSPERSECOND);
    protected static final boolean DELIVER_ONLY_TEXT_DEFAULT = false;
    protected static final String ENCODING = "UTF-8";
    protected static final int IAB_BANNER_HEIGHT_DEFAULT = 250;
    protected static final String LOG = "MAD_LOG";
    protected static final String MAD_SERVER = "http://ad.madvertise.de";
    protected static final int MMA_BANNER_HEIGHT_DEFAULT = 53;
    protected static final boolean PRINT_LOG = true;
    protected static final int SECONDS_TO_REFRESH_AD_DEFAULT = 30;
    protected static final int SECONDS_TO_REFRESH_LOCATION = 900;
    protected static final int TEXT_COLOR_DEFAULT = -1;
    protected static final int TEXT_SIZE_DEFAULT = 18;
    private static String UA;
    /* access modifiers changed from: private */
    public static Location currentLocation = null;
    /* access modifiers changed from: private */
    public static long locationUpdateTimestamp = 0;

    protected static String getToken(Context context) {
        String madvertiseToken = null;
        try {
            madvertiseToken = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString("madvertise_site_token");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (madvertiseToken == null) {
            Log.d(LOG, "Could not fetch \"madvertise_site_token\" from AndroidManifest.xml");
        }
        return madvertiseToken;
    }

    protected static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            Log.d(LOG, e.toString());
        }
        return "";
    }

    protected static String getAllHeadersAsString(Header[] headers) {
        String returnString = "";
        for (int i = 0; i < headers.length; i++) {
            returnString = returnString + "<< " + headers[i].getName() + " : " + headers[i].getValue() + " >>";
        }
        return returnString;
    }

    protected static byte[] convertStreamToByteArray(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
        byte[] buffer = new byte[1024];
        while (true) {
            try {
                int len = inputStream.read(buffer);
                if (len < 0) {
                    break;
                }
                byteArrayOutputStream.write(buffer, 0, len);
            } catch (IOException e) {
            }
        }
        return byteArrayOutputStream.toByteArray();
    }

    protected static boolean compareTwoStreams(InputStream inputStream1, InputStream inputStream2) throws IOException {
        boolean error;
        boolean z;
        try {
            byte[] buffer1 = new byte[1024];
            byte[] buffer2 = new byte[1024];
            do {
                int numRead1 = inputStream1.read(buffer1);
                int numRead2 = inputStream2.read(buffer2);
                if (numRead1 <= -1) {
                    if (numRead2 < 0) {
                        z = PRINT_LOG;
                    } else {
                        z = false;
                    }
                    inputStream1.close();
                    try {
                        inputStream2.close();
                        return z;
                    } catch (IOException e) {
                        if (0 != 0) {
                            return z;
                        }
                        throw e;
                    }
                } else if (numRead2 != numRead1) {
                    inputStream1.close();
                    try {
                        inputStream2.close();
                    } catch (IOException e2) {
                        if (0 == 0) {
                            throw e2;
                        }
                    }
                    return DELIVER_ONLY_TEXT_DEFAULT;
                }
            } while (Arrays.equals(buffer1, buffer2));
            inputStream1.close();
            try {
                inputStream2.close();
            } catch (IOException e3) {
                if (0 == 0) {
                    throw e3;
                }
            }
            return DELIVER_ONLY_TEXT_DEFAULT;
        } catch (IOException e4) {
            IOException e5 = e4;
            error = PRINT_LOG;
            try {
                throw e5;
            } catch (Throwable th) {
                try {
                    inputStream2.close();
                } catch (IOException e6) {
                    if (!error) {
                        throw e6;
                    }
                }
                throw th;
            }
        } catch (RuntimeException e7) {
            RuntimeException e8 = e7;
            error = PRINT_LOG;
            throw e8;
        } catch (Throwable th2) {
            inputStream1.close();
            throw th2;
        }
    }

    protected static String convertStreamToString(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        while (true) {
            try {
                String line = bufferedReader.readLine();
                if (line != null) {
                    stringBuilder.append(line + "\n");
                } else {
                    try {
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    inputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        }
        inputStream.close();
        return stringBuilder.toString();
    }

    protected static Location getLocation() {
        return currentLocation;
    }

    protected static void refreshCoordinates(Context context) {
        boolean permissionCoarseLocation;
        boolean permissionFineLocation;
        Log.d(LOG, "Trying to refresh location");
        if (context == null) {
            Log.d(LOG, "Context not set - quit location refresh");
        } else if (locationUpdateTimestamp + 900000 > System.currentTimeMillis()) {
            Log.d(LOG, "It's not time yet for refreshing the location");
        } else {
            synchronized (context) {
                if (locationUpdateTimestamp + 900000 > System.currentTimeMillis()) {
                    Log.d(LOG, "Another thread updated the loation already");
                    return;
                }
                if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                    permissionCoarseLocation = true;
                } else {
                    permissionCoarseLocation = false;
                }
                if (context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
                    permissionFineLocation = true;
                } else {
                    permissionFineLocation = false;
                }
                if (permissionCoarseLocation || permissionFineLocation) {
                    LocationManager locationManager = (LocationManager) context.getSystemService("location");
                    if (locationManager == null) {
                        Log.d(LOG, "Unable to fetch a location manger");
                        return;
                    }
                    String provider = null;
                    Criteria criteria = new Criteria();
                    criteria.setCostAllowed(DELIVER_ONLY_TEXT_DEFAULT);
                    if (permissionCoarseLocation) {
                        criteria.setAccuracy(2);
                        provider = locationManager.getBestProvider(criteria, PRINT_LOG);
                    }
                    if (provider == null && permissionFineLocation) {
                        criteria.setAccuracy(1);
                        provider = locationManager.getBestProvider(criteria, PRINT_LOG);
                    }
                    if (provider == null) {
                        Log.d(LOG, "Unable to fetch a location provider");
                        return;
                    }
                    final LocationManager finalizedLocationManager = locationManager;
                    locationUpdateTimestamp = System.currentTimeMillis();
                    locationManager.requestLocationUpdates(provider, 0, 0.0f, new LocationListener() {
                        public void onLocationChanged(Location location) {
                            Log.d(MadUtil.LOG, "Refreshing location");
                            Location unused = MadUtil.currentLocation = location;
                            long unused2 = MadUtil.locationUpdateTimestamp = System.currentTimeMillis();
                            finalizedLocationManager.removeUpdates(this);
                        }

                        public void onProviderDisabled(String provider) {
                        }

                        public void onProviderEnabled(String provider) {
                        }

                        public void onStatusChanged(String provider, int status, Bundle extras) {
                        }
                    }, context.getMainLooper());
                    return;
                }
                Log.d(LOG, "No permissions for requesting the location");
            }
        }
    }

    protected static String printRequestParameters(List<NameValuePair> parameterList) {
        StringBuilder stringBuilder = new StringBuilder();
        for (NameValuePair pair : parameterList) {
            stringBuilder.append(pair.getName() + "=" + pair.getValue() + "||");
        }
        return stringBuilder.toString();
    }

    protected static String getUA() {
        if (UA != null) {
            return UA;
        }
        StringBuffer arg = new StringBuffer();
        String version = Build.VERSION.RELEASE;
        if (version.length() > 0) {
            arg.append(version);
        } else {
            arg.append("1.0");
        }
        arg.append("; ");
        Locale l = Locale.getDefault();
        String language = l.getLanguage();
        if (language != null) {
            arg.append(language.toLowerCase());
            String country = l.getCountry();
            if (country != null) {
                arg.append("-");
                arg.append(country.toLowerCase());
            }
        } else {
            arg.append("de");
        }
        String model = Build.MODEL;
        if (model.length() > 0) {
            arg.append("; ");
            arg.append(model);
        }
        String id = Build.ID;
        if (id.length() > 0) {
            arg.append(" Build/");
            arg.append(id);
        }
        UA = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2", arg);
        return UA;
    }

    protected static void logMessage(String tag, int level, String message) {
        if (tag == null) {
            tag = LOG;
        }
        Log.println(level, tag, message);
    }
}
