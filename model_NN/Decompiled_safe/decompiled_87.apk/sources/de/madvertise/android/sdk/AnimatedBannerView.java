package de.madvertise.android.sdk;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

class AnimatedBannerView extends View {
    private static final String CACHED_BANNER_FILE = "cachedBanner.gif";
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    private Movie movie;

    public AnimatedBannerView(Context context) {
        super(context);
        setFocusable(true);
        FileInputStream file = null;
        try {
            file = context.openFileInput(CACHED_BANNER_FILE);
        } catch (FileNotFoundException e) {
            Log.d("MAD_LOG", "Could not load cached banner");
            e.printStackTrace();
        }
        byte[] byteArray = MadUtil.convertStreamToByteArray(file);
        this.movie = Movie.decodeByteArray(byteArray, 0, byteArray.length);
        if (this.movie == null) {
            Log.d("MAD_LOG", "Movie is null");
        }
        new Thread() {
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                    AnimatedBannerView.this.handler.post(new Runnable() {
                        public void run() {
                            AnimatedBannerView.this.invalidate();
                        }
                    });
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.movie != null) {
            this.movie.setTime((int) (SystemClock.uptimeMillis() % ((long) Math.max(this.movie.duration(), 1))));
            this.movie.draw(canvas, (float) (getWidth() - this.movie.width()), (float) (getHeight() - this.movie.height()));
        }
    }
}
