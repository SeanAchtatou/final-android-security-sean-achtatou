package a;

import java.util.Date;

public class c {
    public static long a(Date date) {
        if (date == null) {
            return 0;
        }
        return (System.currentTimeMillis() - date.getTime()) / 1000;
    }
}
