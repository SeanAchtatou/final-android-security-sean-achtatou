package com.scoreloop.android.coreui;

import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.ProgressBar;
import com.feasy.jewels.QQCakes.R;

abstract class BaseActivity extends ActivityGroup {
    static final int DIALOG_ERROR_EMAIL_ALREADY_TAKEN = 8;
    static final int DIALOG_ERROR_INVALID_EMAIL_FORMAT = 10;
    static final int DIALOG_ERROR_NAME_ALREADY_TAKEN = 11;
    static final int DIALOG_ERROR_NETWORK = 3;
    static final int DIALOG_ERROR_NOT_ON_HIGHSCORE_LIST = 1;

    BaseActivity() {
    }

    private Dialog createErrorDialog(int resId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(resId).setCancelable(true);
        Dialog dialog = builder.create();
        dialog.getWindow().requestFeature(DIALOG_ERROR_NOT_ON_HIGHSCORE_LIST);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(DIALOG_ERROR_NOT_ON_HIGHSCORE_LIST);
        getWindow().setFlags(1024, 1024);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_ERROR_NOT_ON_HIGHSCORE_LIST /*1*/:
                return createErrorDialog(R.string.sl_error_message_not_on_highscore_list);
            case 2:
            case 4:
            case 5:
            case 6:
            case 7:
            case 9:
            default:
                return null;
            case DIALOG_ERROR_NETWORK /*3*/:
                return createErrorDialog(R.string.sl_error_message_network);
            case DIALOG_ERROR_EMAIL_ALREADY_TAKEN /*8*/:
                return createErrorDialog(R.string.sl_error_message_email_already_taken);
            case DIALOG_ERROR_INVALID_EMAIL_FORMAT /*10*/:
                return createErrorDialog(R.string.sl_error_message_invalid_email_format);
            case DIALOG_ERROR_NAME_ALREADY_TAKEN /*11*/:
                return createErrorDialog(R.string.sl_error_message_name_already_taken);
        }
    }

    /* access modifiers changed from: package-private */
    public void setProgressIndicator(boolean visible) {
        ((ProgressBar) findViewById(R.id.progress_indicator)).setVisibility(visible ? 0 : 4);
    }
}
