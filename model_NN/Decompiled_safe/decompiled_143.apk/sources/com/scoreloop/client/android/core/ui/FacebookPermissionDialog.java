package com.scoreloop.client.android.core.ui;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import com.a.a.g;
import com.a.a.o;
import com.a.a.p;

public class FacebookPermissionDialog extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    private o f126a;
    private p b;

    public FacebookPermissionDialog(Context context, p pVar) {
        super(context, 16973841);
        this.b = pVar;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            this.b.b(this.f126a);
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.f126a = new o(getContext(), g.a(), new String[]{"publish_stream"});
        this.f126a.a(this.b);
        this.f126a.c();
        setContentView(this.f126a);
    }
}
