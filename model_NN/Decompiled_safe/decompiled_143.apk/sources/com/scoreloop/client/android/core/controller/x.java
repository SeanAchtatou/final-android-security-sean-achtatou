package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.utils.Logger;

class x implements RequestControllerObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RequestController f85a;

    private x(RequestController requestController) {
        this.f85a = requestController;
    }

    public void requestControllerDidFail(RequestController requestController, Exception exc) {
        Logger.a("RequestController", "Session authentication failed, failing _request");
        this.f85a.d.a(exc);
        this.f85a.d.g().a(this.f85a.d);
        U unused = this.f85a.f = (U) null;
    }

    public void requestControllerDidReceiveResponse(RequestController requestController) {
        U unused = this.f85a.f = (U) null;
    }
}
