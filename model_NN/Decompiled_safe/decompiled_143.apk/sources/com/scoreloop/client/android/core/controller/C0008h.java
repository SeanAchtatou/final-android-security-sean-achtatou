package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.PaymentCredential;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import org.json.JSONObject;

/* renamed from: com.scoreloop.client.android.core.controller.h  reason: case insensitive filesystem */
class C0008h extends G {

    /* renamed from: a  reason: collision with root package name */
    protected final String f74a;
    final /* synthetic */ PaymentController b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C0008h(PaymentController paymentController, RequestCompletionCallback requestCompletionCallback, Game game, User user, PaymentCredential paymentCredential, JSONObject jSONObject, String str) {
        super(paymentController, requestCompletionCallback, game, user, paymentCredential, jSONObject);
        this.b = paymentController;
        this.f74a = str;
    }

    public RequestMethod b() {
        return RequestMethod.PUT;
    }

    public String c() {
        return String.format("/service/games/%s/users/%s/payments/%s", this.c.getIdentifier(), this.e.getIdentifier(), this.f74a);
    }

    /* access modifiers changed from: protected */
    public JSONObject d() {
        return this.f.a();
    }

    /* access modifiers changed from: protected */
    public C0013m e() {
        return C0013m.SubmitPayment;
    }
}
