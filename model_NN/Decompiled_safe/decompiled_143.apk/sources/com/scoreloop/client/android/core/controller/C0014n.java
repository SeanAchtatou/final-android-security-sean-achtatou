package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;

/* renamed from: com.scoreloop.client.android.core.controller.n  reason: case insensitive filesystem */
class C0014n extends I {
    private final Game c;

    public C0014n(RequestCompletionCallback requestCompletionCallback, User user, Game game) {
        super(requestCompletionCallback, game, user);
        this.c = game;
    }

    public RequestMethod b() {
        return RequestMethod.GET;
    }

    public String c() {
        return String.format("/service/games/%s/users/%s/context", this.c.getIdentifier(), this.b.getIdentifier());
    }
}
