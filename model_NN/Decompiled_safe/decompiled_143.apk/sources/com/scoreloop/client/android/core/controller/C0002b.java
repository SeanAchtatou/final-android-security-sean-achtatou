package com.scoreloop.client.android.core.controller;

/* renamed from: com.scoreloop.client.android.core.controller.b  reason: case insensitive filesystem */
enum C0002b {
    EXACT(null, "exact match"),
    LIKE("like", "pattern match"),
    BEGINS_WITH("begins_with", "string begins with given value"),
    IS("equals", "is"),
    IS_GREATER("greater_than", "is greater than"),
    IS_LESS("less_than", "is less than"),
    IS_NOT("does_not_equal", "is not"),
    EQUALS_ANY("equals_any", "equals any");
    
    private String i;
    private String j;

    private C0002b(String str, String str2) {
        this.j = str;
        this.i = str2;
    }

    public String a() {
        return this.j;
    }
}
