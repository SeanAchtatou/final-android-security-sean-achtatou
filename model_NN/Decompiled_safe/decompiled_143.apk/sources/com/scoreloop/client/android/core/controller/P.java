package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Session;

class P implements SocialProviderControllerObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MySpaceSocialProviderController f59a;

    P(MySpaceSocialProviderController mySpaceSocialProviderController) {
        this.f59a = mySpaceSocialProviderController;
    }

    public void didEnterInvalidCredentials() {
        this.f59a.d().didEnterInvalidCredentials();
    }

    public void didFail(Throwable th) {
        this.f59a.d().didFail(th);
    }

    public void didSucceed() {
        if (this.f59a.c == null) {
            UserController unused = this.f59a.c = new UserController(Session.getCurrentSession(), this.f59a);
        }
        this.f59a.c.setUser(Session.getCurrentSession().getUser());
        this.f59a.c.updateUser();
    }

    public void userDidCancel() {
        this.f59a.d().userDidCancel();
    }
}
