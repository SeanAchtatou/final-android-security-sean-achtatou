package com.scoreloop.client.android.core.ui;

import android.content.DialogInterface;
import com.scoreloop.client.android.core.utils.Logger;

class j implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TwitterAuthViewController f143a;

    j(TwitterAuthViewController twitterAuthViewController) {
        this.f143a = twitterAuthViewController;
    }

    public void onCancel(DialogInterface dialogInterface) {
        Logger.a("twitter auth view controller", "dialog cancelled");
    }
}
