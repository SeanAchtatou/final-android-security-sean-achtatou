package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Device;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.utils.Logger;
import org.json.JSONObject;

/* renamed from: com.scoreloop.client.android.core.controller.u  reason: case insensitive filesystem */
class C0021u extends RequestController {
    private Request c;
    private Device d;
    private Request e;

    C0021u(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.d = session.a();
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        int f = response.f();
        JSONObject optJSONObject = response.e().optJSONObject("device");
        if (((C0010j) request).d() == J.VERIFY) {
            if (f == 404) {
                return false;
            }
            if (this.c != null) {
                this.c.n();
            }
            this.c = null;
        }
        if ((f == 200 || f == 201) && optJSONObject != null) {
            this.d.a(optJSONObject.getString("id"));
            if ("freed".equalsIgnoreCase(optJSONObject.optString("state"))) {
                this.d.a(Device.State.FREED);
            } else {
                this.d.a(f == 200 ? Device.State.VERIFIED : Device.State.CREATED);
            }
            return true;
        }
        throw new Exception("Request failed with status: " + f);
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void h() {
        Logger.a("DeviceController", "reset()");
        super.h();
        if (this.e != null) {
            if (!this.e.m()) {
                Logger.a("DeviceController", "reset() - canceling verify request");
                e().b().b(this.e);
            }
            this.e = null;
        }
    }

    /* access modifiers changed from: package-private */
    public Device i() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void j() {
        h();
        this.e = new C0010j(d(), i(), J.VERIFY);
        a(this.e);
        this.c = new C0010j(d(), i(), J.CREATE);
        a(this.c);
    }
}
