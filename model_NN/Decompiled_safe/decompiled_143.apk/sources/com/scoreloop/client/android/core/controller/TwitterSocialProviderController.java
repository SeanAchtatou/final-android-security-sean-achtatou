package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.AuthRequestDelegate;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.TwitterAuthRequest;
import com.scoreloop.client.android.core.ui.TwitterAuthViewController;

public class TwitterSocialProviderController extends SocialProviderController implements SocialProviderControllerObserver, UserControllerObserver, AuthRequestDelegate {
    private TwitterAuthRequest c;
    private TwitterAuthViewController d;
    private TwitterAuthRequest e;
    private UserController f;

    public TwitterSocialProviderController(Session session, SocialProviderControllerObserver socialProviderControllerObserver) {
        super(session, socialProviderControllerObserver);
    }

    public void a(TwitterAuthRequest twitterAuthRequest) {
        if (twitterAuthRequest == this.c) {
            this.d = new TwitterAuthViewController(this);
            this.d.a(this.c.b());
            a(c(), this.d);
        } else if (twitterAuthRequest == this.e) {
            f().a(e().getUser(), this.e.b(), this.e.c(), this.e.d());
            if (this.f != null) {
                throw new IllegalStateException("_userController must be null at this point");
            }
            this.f = new UserController(this);
            this.f.setUser(e().getUser());
            this.f.updateUser();
        } else {
            throw new IllegalStateException("unexpected request");
        }
    }

    public void a(TwitterAuthRequest twitterAuthRequest, Throwable th) {
        d().didFail(th);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.c = new TwitterAuthRequest(this);
        this.c.a();
    }

    public void didEnterInvalidCredentials() {
        d().didEnterInvalidCredentials();
    }

    public void didFail(Throwable th) {
        d().didFail(th);
    }

    public void didSucceed() {
        this.e = new TwitterAuthRequest(this);
        this.e.a(this.c.b(), this.c.c());
    }

    public void onEmailAlreadyTaken(UserController userController) {
        d().didFail(new IllegalStateException("twitter: SL user's email already taken"));
    }

    public void onEmailInvalidFormat(UserController userController) {
        d().didFail(new IllegalStateException("twitter: SL user's email invalid format"));
    }

    public void onUsernameAlreadyTaken(UserController userController) {
        d().didFail(new IllegalStateException("twitter: SL username already taken"));
    }

    public void requestControllerDidFail(RequestController requestController, Exception exc) {
        d().didFail(exc);
    }

    public void requestControllerDidReceiveResponse(RequestController requestController) {
        d().didSucceed();
    }

    public void userDidCancel() {
        d().userDidCancel();
    }
}
