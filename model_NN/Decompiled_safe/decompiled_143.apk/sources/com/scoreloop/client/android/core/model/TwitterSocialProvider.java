package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.controller.TwitterSocialProviderController;

public class TwitterSocialProvider extends SocialProvider {
    public static String IDENTIFIER = "com.twitter.v1";

    public Class a() {
        return TwitterSocialProviderController.class;
    }

    public String getIdentifier() {
        return IDENTIFIER;
    }

    public boolean isUserConnected(User user) {
        return user.h() != null;
    }
}
