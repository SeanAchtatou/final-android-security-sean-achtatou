package com.scoreloop.client.android.core.server;

import org.apache.http.client.methods.HttpPost;
import org.json.JSONObject;

class i {

    /* renamed from: a  reason: collision with root package name */
    protected int f121a;
    protected String b;
    protected JSONObject c;
    protected JSONObject d;
    final /* synthetic */ c e;
    private HttpPost f;

    private i(c cVar, g gVar) {
        this.e = cVar;
        this.f = null;
    }

    /* access modifiers changed from: private */
    public HttpPost b() {
        HttpPost httpPost;
        synchronized (this) {
            this.f = this.e.c.b();
            httpPost = this.f;
        }
        return httpPost;
    }

    public void a() {
        synchronized (this) {
            if (this.f != null) {
                this.f.abort();
            }
        }
    }

    public void a(int i, String str, JSONObject jSONObject, JSONObject jSONObject2) {
        this.f121a = i;
        this.b = str;
        this.c = jSONObject;
        this.d = jSONObject2;
    }
}
