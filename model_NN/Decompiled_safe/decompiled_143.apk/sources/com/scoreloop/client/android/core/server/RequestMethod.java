package com.scoreloop.client.android.core.server;

public enum RequestMethod {
    DELETE("delete"),
    GET("get"),
    POST("post"),
    PUT("put");
    
    private final String e;

    private RequestMethod(String str) {
        this.e = str;
    }

    public static RequestMethod[] a() {
        return (RequestMethod[]) f.clone();
    }
}
