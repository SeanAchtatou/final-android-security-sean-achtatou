package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Range;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;

public class ScoresController extends RequestController {
    private Range c;
    private Integer d;
    private Range e;
    private RankingController f;
    private int g;
    private List h;
    /* access modifiers changed from: private */
    public Score i;
    private SearchList j;

    public ScoresController(RequestControllerObserver requestControllerObserver) {
        this(Session.getCurrentSession(), requestControllerObserver);
    }

    public ScoresController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.h = new ArrayList();
        this.j = SearchList.defaultScoreSearchList();
        this.d = 0;
        this.e = new Range(0, 25);
        this.c = new Range(0, 0);
    }

    private List a(JSONArray jSONArray, int i2) {
        ArrayList arrayList = new ArrayList();
        for (int i3 = 0; i3 < i2; i3++) {
            arrayList.add(new Score(jSONArray.getJSONObject(i3).getJSONObject("score")));
        }
        return arrayList;
    }

    private void a(Range range) {
        this.c = range;
    }

    /* access modifiers changed from: private */
    public void a(Integer num) {
        if (num != null) {
            int length = getRange().getLength() / 2;
            setRange(new Range(num.intValue() - 1 < length ? 0 : (num.intValue() - 1) - length, getRange().getLength()));
        }
        i();
    }

    private void i() {
        Range loadedRange = getLoadedRange();
        loadedRange.b(getRange().getLocation());
        loadedRange.a(0);
        C0004d dVar = new C0004d(d(), b(), this.j, e().getUser(), this.d, Integer.valueOf(getRange().getLength() + 1), Integer.valueOf(loadedRange.getLocation()));
        h();
        a(dVar);
    }

    private RankingController j() {
        if (this.f == null) {
            this.f = new RankingController(e(), new C0011k(this));
        }
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        if (response.f() != 200) {
            throw new Exception("Request failed, returned status: " + response.f());
        }
        JSONArray jSONArray = response.e().getJSONArray("scores");
        Range range = getRange();
        this.g = jSONArray.length();
        int min = Math.min(this.g, this.e.getLength());
        this.h.clear();
        this.h = a(jSONArray, min);
        if (this.i != null && !this.h.contains(this.i)) {
            this.h.add((this.i.getRank().intValue() - this.e.getLocation()) - 1, this.i);
            this.h.remove(this.i.getRank().intValue() >= this.e.getLocation() + this.h.size() ? 0 : this.h.size() - 1);
        }
        int location = this.e.getLocation() + 1;
        for (Score a2 : this.h) {
            a2.a(Integer.valueOf(location));
            location++;
        }
        a(new Range(range.getLocation(), this.h.size()));
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    public SearchList getBuddiesSearchList() {
        return SearchList.buddiesScoreSearchList();
    }

    public SearchList getDefaultSearchList() {
        return SearchList.defaultScoreSearchList();
    }

    public SearchList getGlobalSearchList() {
        return SearchList.globalScoreSearchList();
    }

    public Range getLoadedRange() {
        return this.c;
    }

    public Integer getMode() {
        return this.d;
    }

    public Range getRange() {
        return this.e;
    }

    public Score getScoreForIndex(int i2) {
        if (getScores() == null) {
            return null;
        }
        return (Score) getScores().get(i2);
    }

    public List getScores() {
        return this.h;
    }

    public SearchList getSearchList() {
        return this.j;
    }

    public SearchList getTwentyFourHourSearchList() {
        return SearchList.twentyFourHourScoreSearchList();
    }

    public SearchList getUserCountrySearchList() {
        return SearchList.userCountryScoreSearchList();
    }

    public boolean hasNextRange() {
        return this.g > getRange().getLength();
    }

    public boolean hasPreviousRange() {
        return getLoadedRange().getLocation() > 0 && getRange().getLength() > 0;
    }

    public void loadNextRange() {
        if (!hasNextRange()) {
            throw new IllegalStateException("There's no next range");
        }
        Range range = getRange();
        range.b(getLoadedRange().getLocation() + range.getLength());
        loadRange();
    }

    public void loadPreviousRange() {
        if (!hasPreviousRange()) {
            throw new IllegalStateException("There's no previous range");
        }
        Range range = getRange();
        int location = getLoadedRange().getLocation();
        range.b(location > range.getLength() ? location - range.getLength() : 0);
        loadRange();
    }

    public void loadRange() {
        this.i = null;
        i();
    }

    public void loadRangeForScore(Score score) {
        RankingController j2 = j();
        j2.setSearchList(getSearchList());
        this.i = score;
        this.i.a((Integer) null);
        j2.requestRankingForScore(score);
    }

    public void loadRangeForScoreResult(Double d2, Map map) {
        loadRangeForScore(new Score(d2, map, f()));
    }

    public void loadRangeForUser(User user) {
        RankingController j2 = j();
        j2.setSearchList(getSearchList());
        j2.requestRankingForUserInGameMode(user, getMode().intValue());
        this.i = null;
    }

    public void setMode(Integer num) {
        this.d = num;
    }

    public void setRange(Range range) {
        this.e = range;
    }

    public void setSearchList(SearchList searchList) {
        if (this.j != searchList) {
            this.j = searchList;
            if (this.i != null) {
                this.i.a((Integer) null);
            }
        }
    }
}
