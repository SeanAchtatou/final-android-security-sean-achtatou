package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import org.json.JSONException;
import org.json.JSONObject;

class T extends Request {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MessageController f66a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public T(MessageController messageController, RequestCompletionCallback requestCompletionCallback) {
        super(requestCompletionCallback);
        this.f66a = messageController;
    }

    public JSONObject a() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("message", this.f66a.i().d());
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    public RequestMethod b() {
        return RequestMethod.POST;
    }

    public String c() {
        return String.format("/service/games/%s/users/%s/message", this.f66a.b().getIdentifier(), this.f66a.f().getIdentifier());
    }
}
