package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import org.json.JSONException;
import org.json.JSONObject;

class K extends I {
    protected User c;

    public K(RequestCompletionCallback requestCompletionCallback, Game game, User user, User user2) {
        super(requestCompletionCallback, game, user);
        this.c = user2;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.c != null) {
                jSONObject.put("reference_user_id", this.c.getIdentifier());
            }
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException("Invalid user data");
        }
    }

    public String c() {
        if (this.f54a == null || this.f54a.getIdentifier() == null) {
            return String.format("/service/users/%s/detail", this.b.getIdentifier());
        }
        return String.format("/service/games/%s/users/%s/detail", this.f54a.getIdentifier(), this.b.getIdentifier());
    }
}
