package com.scoreloop.client.android.core.ui;

import com.a.a.d;
import com.a.a.q;

final class g extends d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FacebookAuthViewController f140a;

    private g(FacebookAuthViewController facebookAuthViewController) {
        this.f140a = facebookAuthViewController;
    }

    /* synthetic */ g(FacebookAuthViewController facebookAuthViewController, b bVar) {
        this(facebookAuthViewController);
    }

    public void a(q qVar, Throwable th) {
        this.f140a.a(false);
        this.f140a.a().didFail(th);
    }

    public void b(q qVar) {
        this.f140a.a(false);
        this.f140a.a().userDidCancel();
    }
}
