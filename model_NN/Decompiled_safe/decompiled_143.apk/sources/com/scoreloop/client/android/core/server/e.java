package com.scoreloop.client.android.core.server;

import android.os.Handler;
import android.os.Message;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.utils.Logger;
import org.json.JSONException;
import org.json.JSONObject;

class e extends Thread implements BayeuxConnectionObserver {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f119a = (!e.class.desiredAssertionStatus());
    private static int i = 0;
    private boolean b;
    private final Handler c;
    private c d;
    private Game e;
    private volatile Request f;
    private volatile i g;
    private volatile boolean h = true;

    e(Handler handler) {
        this.c = handler;
        StringBuilder append = new StringBuilder().append(getClass().getSimpleName());
        int i2 = i;
        i = i2 + 1;
        setName(append.append(i2).toString());
        setDaemon(true);
    }

    private void a(Response response, int i2, Exception exc) {
        synchronized (this) {
            this.f = null;
            this.g = null;
        }
        Message obtainMessage = this.c.obtainMessage(i2);
        if (i2 == 1) {
            obtainMessage.obj = response;
        } else {
            obtainMessage.obj = exc;
        }
        obtainMessage.sendToTarget();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        synchronized (this) {
            this.h = false;
            if (this.g != null) {
                this.g.a();
            } else {
                notify();
            }
        }
        try {
            join();
        } catch (InterruptedException e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Game game) {
        this.e = game;
    }

    /* access modifiers changed from: package-private */
    public void a(Request request) {
        boolean z = false;
        synchronized (this) {
            if (this.f != null) {
                throw new IllegalStateException("Previous request not finished yet");
            } else if (!request.m()) {
                this.f = request;
                this.g = this.d.a();
                this.g.a(this.f.j(), this.f.c(), this.f.a(), this.f.i());
                notify();
            } else {
                z = true;
            }
        }
        if (z) {
            Logger.a("ServerCommThread", "startRequest(): Request was cancelled early");
            a(null, 2, null);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar) {
        this.d = cVar;
    }

    public void a(c cVar, Integer num, Object obj, String str, int i2) {
        this.b = true;
        a(new Response(obj, str, i2, num), 1, null);
    }

    public void a(c cVar, JSONObject jSONObject) {
    }

    public JSONObject b(c cVar, JSONObject jSONObject) {
        try {
            if (this.e != null) {
                JSONObject jSONObject2 = jSONObject.getJSONObject("ext");
                JSONObject jSONObject3 = new JSONObject();
                jSONObject2.put("game", jSONObject3);
                jSONObject3.put("id", this.e.getIdentifier());
                jSONObject3.put("secret", this.e.a());
                jSONObject3.put("version", this.e.getVersion());
            }
            return jSONObject;
        } catch (JSONException e2) {
            throw new IllegalStateException("Should never happen");
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        Logger.a("ServerCommThread", "abortCurrentRequest() called");
        synchronized (this) {
            if (this.g != null) {
                this.g.a();
            }
        }
    }

    public void run() {
        Logger.a("ServerCommThread", "run(): entering loop");
        while (this.h) {
            try {
                synchronized (this) {
                    if (this.g == null && this.h) {
                        wait();
                    }
                    if (this.g != null) {
                        try {
                            Logger.a("ServerCommThread", "run(): got bayeux request to process");
                            this.b = false;
                            this.d.a(this.g);
                            if (!f119a && !this.b) {
                                throw new AssertionError();
                            }
                        } catch (h e2) {
                            Logger.a("ServerCommThread", "run(): Request processing was interrupted\n" + e2);
                            a(null, 2, e2);
                        } catch (d e3) {
                            Logger.a("ServerCommThread", "run(): publish() failed\n" + e3);
                            a(null, 3, e3);
                        } catch (Exception e4) {
                            Logger.a("ServerCommThread", "run(): publish() failed\n" + e4);
                            a(null, 3, e4);
                        }
                    }
                }
            } catch (InterruptedException e5) {
                Logger.a("ServerCommThread", "run(): interrupted while waiting for request");
            }
        }
        Logger.a("ServerCommThread", "run(): exitting loop");
    }
}
