package com.scoreloop.client.android.core.controller;

public interface SocialProviderControllerObserver {
    void didEnterInvalidCredentials();

    void didFail(Throwable th);

    void didSucceed();

    void userDidCancel();
}
