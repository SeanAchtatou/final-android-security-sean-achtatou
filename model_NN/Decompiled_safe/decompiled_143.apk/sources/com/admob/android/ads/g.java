package com.admob.android.ads;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.admob.android.ads.b;
import com.admob.android.ads.d;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: AdContainer */
final class g extends RelativeLayout implements Animation.AnimationListener, b.a, d.b, s {
    private static float i = -1.0f;
    private static d j = null;

    /* renamed from: a  reason: collision with root package name */
    protected d f34a;
    protected ProgressBar b;
    /* access modifiers changed from: private */
    public final AdView c;
    private Vector<String> d;
    private View e;
    private long f = -1;
    private boolean g;
    private View h;

    public g(d dVar, Context context, AdView adView) {
        super(context);
        this.c = adView;
        setId(1);
        i = getResources().getDisplayMetrics().density;
        setBackgroundDrawable(context.getResources().getDrawable(17301602));
        Drawable drawable = context.getResources().getDrawable(17301602);
        drawable.setAlpha(128);
        this.e = new View(context);
        this.e.setBackgroundDrawable(drawable);
        this.e.setVisibility(4);
        addView(this.e, new RelativeLayout.LayoutParams(-1, -1));
        a((d) null);
    }

    /* access modifiers changed from: package-private */
    public final void a(d dVar) {
        this.f34a = dVar;
        if (dVar == null) {
            setFocusable(false);
            setClickable(false);
            return;
        }
        dVar.a((d.b) this);
        setFocusable(true);
        setClickable(true);
    }

    /* access modifiers changed from: protected */
    public final d b() {
        return this.f34a;
    }

    static float c() {
        return i;
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        if (this.f34a != null) {
            this.f34a.h();
            this.f34a = null;
        }
    }

    public final void a(View view, RelativeLayout.LayoutParams layoutParams) {
        if (view != null && view != this.h) {
            this.h = view;
            this.b = new ProgressBar(getContext());
            this.b.setIndeterminate(true);
            this.b.setId(2);
            if (layoutParams != null) {
                this.b.setLayoutParams(layoutParams);
            }
            this.b.setVisibility(4);
            post(new b(this));
        }
    }

    /* compiled from: AdContainer */
    private static class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private WeakReference<g> f36a;

        public b(g gVar) {
            this.f36a = new WeakReference<>(gVar);
        }

        public final void run() {
            try {
                g gVar = this.f36a.get();
                if (gVar != null) {
                    gVar.addView(gVar.b);
                }
            } catch (Exception e) {
                if (Log.isLoggable(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "exception caught in AdContainer post run(), " + e.getMessage());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        if (isPressed() || isFocused()) {
            canvas.clipRect(3, 3, getWidth() - 3, getHeight() - 3);
        }
        super.onDraw(canvas);
        if (this.f == -1) {
            this.f = SystemClock.uptimeMillis();
            if (this.f34a != null) {
                this.f34a.i();
            }
        }
    }

    public final void a() {
        post(new c(this));
    }

    /* compiled from: AdContainer */
    private static class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private WeakReference<g> f37a;

        public c(g gVar) {
            this.f37a = new WeakReference<>(gVar);
        }

        public final void run() {
            try {
                g gVar = this.f37a.get();
                if (gVar != null) {
                    gVar.e();
                }
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        this.g = false;
        if (this.b != null) {
            this.b.setVisibility(4);
        }
        if (this.h != null) {
            this.h.setVisibility(0);
        }
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (Log.isLoggable(AdManager.LOG, 2)) {
            Log.v(AdManager.LOG, "onKeyDown: keyCode=" + i2);
        }
        if (i2 == 66 || i2 == 23) {
            this.d = a(keyEvent, this.d);
            setPressed(true);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public final boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (Log.isLoggable(AdManager.LOG, 2)) {
            Log.v(AdManager.LOG, "onKeyUp: keyCode=" + i2);
        }
        if (j() && (i2 == 66 || i2 == 23)) {
            this.d = a(keyEvent, this.d);
            k();
        }
        setPressed(false);
        return super.onKeyUp(i2, keyEvent);
    }

    public final void f() {
        Vector vector = new Vector();
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            vector.add(getChildAt(i2));
        }
        if (j == null) {
            j = new d();
        }
        Collections.sort(vector, j);
        for (int size = vector.size() - 1; size >= 0; size--) {
            if (indexOfChild((View) vector.elementAt(size)) != size) {
                bringChildToFront((View) vector.elementAt(size));
            }
        }
        this.e.bringToFront();
    }

    /* compiled from: AdContainer */
    static class d implements Comparator<View> {
        d() {
        }

        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            float a2 = f.a((View) obj);
            float a3 = f.a((View) obj2);
            if (a2 < a3) {
                return -1;
            }
            return a2 > a3 ? 1 : 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x007c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean dispatchTouchEvent(android.view.MotionEvent r8) {
        /*
            r7 = this;
            r6 = 2
            r5 = 0
            r4 = 1
            java.lang.String r3 = "AdMobSDK"
            int r0 = r8.getAction()
            java.lang.String r1 = "AdMobSDK"
            boolean r1 = android.util.Log.isLoggable(r3, r6)
            if (r1 == 0) goto L_0x0045
            java.lang.String r1 = "AdMobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "dispatchTouchEvent: action="
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.String r2 = " x="
            java.lang.StringBuilder r1 = r1.append(r2)
            float r2 = r8.getX()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " y="
            java.lang.StringBuilder r1 = r1.append(r2)
            float r2 = r8.getY()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r3, r1)
        L_0x0045:
            boolean r1 = r7.j()
            if (r1 == 0) goto L_0x007b
            com.admob.android.ads.d r1 = r7.f34a
            if (r1 == 0) goto L_0x009a
            com.admob.android.ads.d r1 = r7.f34a
            android.graphics.Rect r1 = r1.g()
            com.admob.android.ads.d r2 = r7.f34a
            android.graphics.Rect r1 = r2.a(r1)
            float r2 = r8.getX()
            int r2 = (int) r2
            float r3 = r8.getY()
            int r3 = (int) r3
            boolean r1 = r1.contains(r2, r3)
            if (r1 != 0) goto L_0x009a
            r1 = r5
        L_0x006c:
            if (r1 == 0) goto L_0x0076
            java.util.Vector<java.lang.String> r2 = r7.d
            java.util.Vector r2 = r7.a(r8, r4, r2)
            r7.d = r2
        L_0x0076:
            if (r0 != 0) goto L_0x007c
            r7.setPressed(r1)
        L_0x007b:
            return r4
        L_0x007c:
            if (r0 != r6) goto L_0x0082
            r7.setPressed(r1)
            goto L_0x007b
        L_0x0082:
            if (r0 != r4) goto L_0x0093
            boolean r0 = r7.isPressed()
            if (r0 == 0) goto L_0x008f
            if (r1 == 0) goto L_0x008f
            r7.k()
        L_0x008f:
            r7.setPressed(r5)
            goto L_0x007b
        L_0x0093:
            r1 = 3
            if (r0 != r1) goto L_0x007b
            r7.setPressed(r5)
            goto L_0x007b
        L_0x009a:
            r1 = r4
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.g.dispatchTouchEvent(android.view.MotionEvent):boolean");
    }

    private boolean j() {
        if (this.f34a == null || SystemClock.uptimeMillis() - this.f <= this.f34a.c()) {
            return false;
        }
        return true;
    }

    public final long g() {
        long uptimeMillis = SystemClock.uptimeMillis() - this.f;
        if (this.f < 0 || uptimeMillis < 0 || uptimeMillis > 10000000) {
            return 0;
        }
        return uptimeMillis;
    }

    public final boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (Log.isLoggable(AdManager.LOG, 2)) {
            Log.v(AdManager.LOG, "dispatchTrackballEvent: action=" + motionEvent.getAction());
        }
        if (j()) {
            this.d = a(motionEvent, true, this.d);
            if (motionEvent.getAction() == 0) {
                setPressed(true);
            } else if (motionEvent.getAction() == 1) {
                if (hasFocus()) {
                    k();
                }
                setPressed(false);
            }
        }
        return super.onTrackballEvent(motionEvent);
    }

    private void k() {
        if (this.f34a != null && isPressed()) {
            setPressed(false);
            if (!this.g) {
                this.g = true;
                JSONObject l = l();
                if (this.h != null) {
                    AnimationSet animationSet = new AnimationSet(true);
                    float width = ((float) this.h.getWidth()) / 2.0f;
                    float height = ((float) this.h.getHeight()) / 2.0f;
                    ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, width, height);
                    scaleAnimation.setDuration(200);
                    animationSet.addAnimation(scaleAnimation);
                    ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.2f, 0.001f, 1.2f, 0.001f, width, height);
                    scaleAnimation2.setDuration(299);
                    scaleAnimation2.setStartOffset(200);
                    scaleAnimation2.setAnimationListener(this);
                    animationSet.addAnimation(scaleAnimation2);
                    postDelayed(new a(l, this), 500);
                    this.h.startAnimation(animationSet);
                    return;
                }
                this.f34a.a(l);
                if (this.c != null) {
                    this.c.performClick();
                }
            }
        }
    }

    /* compiled from: AdContainer */
    protected static class a extends Thread {

        /* renamed from: a  reason: collision with root package name */
        private JSONObject f35a;
        private WeakReference<g> b;

        public a(JSONObject jSONObject, g gVar) {
            this.f35a = jSONObject;
            this.b = new WeakReference<>(gVar);
        }

        public final void run() {
            try {
                g gVar = this.b.get();
                if (gVar != null && gVar.f34a != null) {
                    gVar.f34a.a(this.f35a);
                    if (gVar.c != null) {
                        gVar.c.performClick();
                    }
                }
            } catch (Exception e) {
                if (Log.isLoggable(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "exception caught in AdClickThread.run(), " + e.getMessage());
                }
            }
        }
    }

    private JSONObject l() {
        JSONObject jSONObject;
        Exception e2;
        try {
            JSONObject jSONObject2 = new JSONObject();
            a(this, jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            try {
                jSONObject3.put("interactions", jSONObject2);
                return jSONObject3;
            } catch (Exception e3) {
                e2 = e3;
                jSONObject = jSONObject3;
                Log.w(AdManager.LOG, "Exception while processing interaction history.", e2);
                return jSONObject;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            jSONObject = null;
            e2 = exc;
            Log.w(AdManager.LOG, "Exception while processing interaction history.", e2);
            return jSONObject;
        }
    }

    private static void a(View view, JSONObject jSONObject) {
        if (view instanceof s) {
            s sVar = (s) view;
            JSONObject i2 = sVar.i();
            String h2 = sVar.h();
            if (!(i2 == null || h2 == null)) {
                try {
                    jSONObject.put(h2, i2);
                } catch (Exception e2) {
                }
            }
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i3 = 0; i3 < viewGroup.getChildCount(); i3++) {
                a(viewGroup.getChildAt(i3), jSONObject);
            }
        }
    }

    private static Vector<String> a(int i2, int i3, int i4, long j2, Vector<String> vector) {
        Vector<String> vector2;
        String format;
        if (vector == null) {
            vector2 = new Vector<>();
        } else {
            vector2 = vector;
        }
        float f2 = ((float) j2) / 1000.0f;
        if (i3 == -1 || i4 == -1) {
            format = String.format("{%d,%f}", Integer.valueOf(i2), Float.valueOf(f2));
        } else {
            format = String.format("{%d,%d,%d,%f}", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Float.valueOf(f2));
        }
        vector2.add(format);
        if (Log.isLoggable(AdManager.LOG, 2)) {
            Log.v(AdManager.LOG, "recordEvent:" + format);
        }
        return vector2;
    }

    private Vector<String> a(MotionEvent motionEvent, boolean z, Vector<String> vector) {
        int action = motionEvent.getAction();
        long eventTime = motionEvent.getEventTime() - this.f;
        if (action != 0 && action != 1) {
            return vector;
        }
        return a(action == 1 ? 1 : 0, (int) motionEvent.getX(), (int) motionEvent.getY(), eventTime, vector);
    }

    private Vector<String> a(KeyEvent keyEvent, Vector<String> vector) {
        int action = keyEvent.getAction();
        long eventTime = keyEvent.getEventTime() - this.f;
        if (action != 0 && action != 1) {
            return vector;
        }
        return a(action == 1 ? 1 : 0, -1, -1, eventTime, vector);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
    }

    public final String h() {
        return "container";
    }

    public final JSONObject i() {
        JSONObject jSONObject = null;
        if (this.d != null) {
            jSONObject = new JSONObject();
            try {
                jSONObject.put("touches", new JSONArray((Collection) this.d));
            } catch (Exception e2) {
            }
        }
        return jSONObject;
    }

    public final void setPressed(boolean z) {
        if ((!this.g || !z) && isPressed() != z) {
            if (z) {
                if (this.e != null) {
                    this.e.bringToFront();
                }
                this.e.setVisibility(0);
            } else {
                this.e.setVisibility(4);
            }
            super.setPressed(z);
            invalidate();
        }
    }
}
