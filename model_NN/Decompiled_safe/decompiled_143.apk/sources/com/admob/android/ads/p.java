package com.admob.android.ads;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* compiled from: SkewAnimation */
public final class p extends Animation {

    /* renamed from: a  reason: collision with root package name */
    private float[] f41a;
    private float[] b;

    public p(float[] fArr, float[] fArr2, PointF pointF) {
        this.f41a = fArr;
        this.b = fArr2;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        if (((double) f) < 0.0d || ((double) f) > 1.0d) {
            transformation.setTransformationType(Transformation.TYPE_IDENTITY);
            return;
        }
        Matrix matrix = transformation.getMatrix();
        float[] fArr = new float[this.f41a.length];
        for (int i = 0; i < this.f41a.length; i++) {
            fArr[i] = this.f41a[i] + ((this.b[i] - this.f41a[i]) * f);
        }
        matrix.setSkew(this.f41a[0], this.f41a[1]);
        transformation.setTransformationType(Transformation.TYPE_MATRIX);
    }
}
