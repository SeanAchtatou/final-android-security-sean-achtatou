package com.a.a;

import android.content.Context;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

public class o extends q {
    private String[] d;

    public o(Context context, g gVar, String[] strArr) {
        super(context, gVar);
        this.d = strArr;
    }

    private void g() {
        HashMap hashMap = new HashMap();
        hashMap.put("fbconnect", "1");
        hashMap.put("connect_display", "touch");
        hashMap.put("api_key", this.f9a.e());
        hashMap.put("next", "fbconnect://success");
        hashMap.put("cancel", "fbconnect://cancel");
        int length = this.d.length;
        int i = 0;
        String str = "";
        while (i < length) {
            str = str + this.d[i] + (i == length - 1 ? "" : ",");
            i++;
        }
        hashMap.put("ext_perm", str);
        try {
            a("http://www.facebook.com/connect/prompt_permissions.php", "GET", hashMap, (Map) null);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        g();
    }
}
