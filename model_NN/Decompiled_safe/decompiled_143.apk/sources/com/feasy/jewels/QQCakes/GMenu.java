package com.feasy.jewels.QQCakes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import com.scoreloop.android.coreui.HighscoresActivity;
import com.scoreloop.android.coreui.ProfileActivity;
import com.scoreloop.android.coreui.ScoreloopManager;

public class GMenu extends Activity {
    public static String PREFS_NAME = "com.feasy.jewels.QQCakes";
    public static String SL_GAME_ID = "ca31f465-ba55-4eaf-aab1-bb4eefeb65d8";
    public static String SL_GAME_SECRET = "uLCJoPUy+Ea3JddAlJBQFL5OlJhLswxbfceChDOr7RpYY4GoIOMpSg==";
    private final int REQUEST_CODE = 2;
    /* access modifiers changed from: private */
    public int mGameSaveLevel;
    /* access modifiers changed from: private */
    public int mGameSaveLife;
    /* access modifiers changed from: private */
    public boolean mGameSavePause;
    /* access modifiers changed from: private */
    public int mGameSaveScore;
    private boolean mIsMusic;
    private boolean mIsSound;
    private boolean mIsVibrate;
    private MediaPlayer mMusicMP;
    private MediaPlayer mSoundMP;
    private Window window;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.window = getWindow();
        this.window.setFlags(1024, 1024);
        setContentView((int) R.layout.menu);
        this.mGameSavePause = false;
        this.mGameSaveLevel = 0;
        this.mGameSaveScore = 0;
        loadGameParam();
        Bundle b = getIntent().getExtras();
        if (b != null) {
            Log.v("GMenu", "onCreate(), b != null");
            this.mGameSavePause = b.getBoolean("gamePause", false);
            this.mGameSaveScore = b.getInt("gameScore", 0);
            this.mGameSaveLevel = b.getInt("gameLevel", 0);
            if (this.mGameSaveScore > 0) {
                updateHighScore(this.mGameSaveScore);
            }
            Log.v("GMenu", "onCreate(), Life=" + Integer.toString(this.mGameSaveLife) + ", level=" + Integer.toString(this.mGameSaveLevel));
        } else {
            Log.v("GMenu", "onCreate(), saveInstanceState=null");
        }
        ScoreloopManager.init(this, SL_GAME_ID, SL_GAME_SECRET);
        ScoreloopManager.setNumberOfModes(2);
        handleMenuClick();
        playMusic();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.v("GMenu", "onActivityResult(), requestCode=" + Integer.toString(requestCode) + ", resultCode=" + resultCode);
        if (requestCode == 2 && resultCode == -1) {
            boolean bMusic = this.mIsMusic;
            this.mIsMusic = data.getBooleanExtra("isMusic", true);
            this.mIsSound = data.getBooleanExtra("isSound", true);
            this.mIsVibrate = data.getBooleanExtra("isVibrate", true);
            saveGameParam();
            if (bMusic && !this.mIsMusic) {
                stopMusic();
            } else if (this.mIsMusic && !bMusic) {
                playMusic();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        pauseMusic();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        resumeMusic();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    private void handleMenuClick() {
        ((ImageView) findViewById(R.id.btn_newgame)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GMenu.this.playMenuSound();
                GMenu.this.stopMusic();
                Log.v("GMenu", "New Game...called, new itMain");
                Intent itMain = new Intent();
                itMain.setClass(GMenu.this, GameActivity.class);
                GMenu.this.startActivity(itMain);
                GMenu.this.finish();
            }
        });
        ((ImageView) findViewById(R.id.btn_resume)).setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View v) {
                GMenu.this.playMenuSound();
                Intent itMain = new Intent();
                itMain.setClass(GMenu.this, GameActivity.class);
                GMenu.this.mGameSavePause = true;
                if (GMenu.this.mGameSavePause) {
                    Log.v("GMenu", "Resume, mGameSavePause= true");
                    itMain.putExtra("gamePause", true);
                    itMain.putExtra("gameScore", GMenu.this.mGameSaveScore);
                    itMain.putExtra("gameLevel", GMenu.this.mGameSaveLevel);
                    Log.v("GMenu", "Resume, Life=" + Integer.toString(GMenu.this.mGameSaveLife) + ", level=" + Integer.toString(GMenu.this.mGameSaveLevel));
                } else {
                    Log.v("GMenu", "Resume, mGameSavePause=false!");
                }
                GMenu.this.stopMusic();
                GMenu.this.startActivity(itMain);
                GMenu.this.finish();
            }
        });
        ((ImageView) findViewById(R.id.btn_option)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GMenu.this.playMenuSound();
                GMenu.this.showOptionDlg();
            }
        });
        ((ImageView) findViewById(R.id.btn_hiscore)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GMenu.this.playMenuSound();
                GMenu.this.startActivity(new Intent(GMenu.this, HighscoresActivity.class));
            }
        });
        ((ImageView) findViewById(R.id.btn_sl_profile)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GMenu.this.playMenuSound();
                GMenu.this.startActivity(new Intent(GMenu.this, ProfileActivity.class));
            }
        });
        ((ImageView) findViewById(R.id.btn_exit)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GMenu.this.playMenuSound();
                GMenu.this.showExitDlg();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showExitDlg() {
        new AlertDialog.Builder(this).setMessage("Do you want to exit?").setTitle("Exit Game").setIcon((int) R.drawable.icon).setPositiveButton("More Games", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                new MoreGame(GMenu.this).show();
            }
        }).setNeutralButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                GMenu.this.exitApp();
            }
        }).create().show();
    }

    /* access modifiers changed from: private */
    public void exitApp() {
        saveGameParam();
        stopMusic();
        finish();
        Process.killProcess(Process.myPid());
    }

    public void showOptionDlg() {
        Intent intent = new Intent();
        intent.setClass(this, OptionActivity.class);
        intent.putExtra("isMusic", this.mIsMusic);
        intent.putExtra("isSound", this.mIsSound);
        intent.putExtra("isVibrate", this.mIsVibrate);
        startActivityForResult(intent, 2);
    }

    public void showScoreList() {
        Intent intent = new Intent();
        intent.setClass(this, ScoreList.class);
        startActivity(intent);
    }

    public void loadGameParam() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        this.mIsMusic = settings.getBoolean("isMusic", true);
        this.mIsSound = settings.getBoolean("isSound", true);
        this.mIsVibrate = settings.getBoolean("isVibrate", true);
    }

    public void saveGameParam() {
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, 0).edit();
        editor.putBoolean("isMusic", this.mIsMusic);
        editor.putBoolean("isSound", this.mIsSound);
        editor.putBoolean("isVibrate", this.mIsVibrate);
        editor.commit();
    }

    public void updateHighScore(int highScore) {
        Log.v("GMenu", "updateHighScore(),highScore=" + Integer.toString(highScore));
        if (highScore > 0) {
            SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
            int[] scoreArray = new int[10];
            for (int i = 0; i < 9; i++) {
                scoreArray[i] = settings.getInt("highScore" + Integer.toString(i), 0);
            }
            SharedPreferences.Editor editor = settings.edit();
            int i2 = 0;
            while (true) {
                if (i2 >= 9) {
                    break;
                } else if (highScore > scoreArray[i2]) {
                    Log.v("GMenu", "updateHighScore(), highScore>scoreArray[i], i=" + Integer.toString(i2));
                    for (int j = i2 + 1; j < 9; j++) {
                        editor.putInt("highScore" + Integer.toString(j), scoreArray[j - 1]);
                    }
                    editor.putInt("highScore" + Integer.toString(i2), highScore);
                } else {
                    i2++;
                }
            }
            editor.commit();
        }
    }

    public void playMusic() {
        if (this.mIsMusic) {
            stopMusic();
            if (this.mMusicMP == null) {
                this.mMusicMP = MediaPlayer.create(getBaseContext(), (int) R.raw.menu_music);
            }
            this.mMusicMP.start();
            this.mMusicMP.setLooping(true);
        }
    }

    /* access modifiers changed from: private */
    public void stopMusic() {
        if (this.mMusicMP != null) {
            if (this.mMusicMP.isPlaying()) {
                this.mMusicMP.stop();
            }
            this.mMusicMP.release();
            this.mMusicMP = null;
        }
    }

    private void pauseMusic() {
        if (this.mMusicMP != null) {
            this.mMusicMP.pause();
        }
    }

    private void resumeMusic() {
        if (this.mMusicMP != null && this.mIsMusic) {
            this.mMusicMP.start();
        }
    }

    /* access modifiers changed from: private */
    public void playMenuSound() {
        if (this.mSoundMP == null) {
            this.mSoundMP = MediaPlayer.create(getBaseContext(), (int) R.raw.click);
        }
        if (this.mSoundMP.isPlaying()) {
            this.mSoundMP.stop();
        }
        this.mSoundMP.start();
    }

    private void FreeMenuSound() {
        if (this.mSoundMP != null) {
            if (this.mSoundMP.isPlaying()) {
                this.mSoundMP.stop();
            }
            this.mSoundMP.release();
        }
    }
}
