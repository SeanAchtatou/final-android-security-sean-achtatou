package com.feasy.jewels.QQCakes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.ProgressBar;
import com.scoreloop.android.coreui.ScoreloopManager;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import java.util.HashMap;

public class GameActivity extends Activity {
    public final String PREFS_NAME = GMenu.PREFS_NAME;
    /* access modifiers changed from: private */
    public GameView gameView;
    private ProgressBar mBar;
    public GameSound mGameSound;
    private MediaPlayer mMusicMP;
    private HashMap<Integer, Integer> mSoundMap;
    private SoundPool mSoundPool;
    private int retCode;
    private ProgressDialog waitDlg = null;
    private Window window;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.gc();
        Log.v("GameActivity", "onCreate().....now");
        requestWindowFeature(1);
        this.window = getWindow();
        this.window.setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        this.gameView = (GameView) findViewById(R.id.game_view);
        Display display = getWindowManager().getDefaultDisplay();
        this.gameView.setParent(this);
        this.gameView.setScreen(display.getWidth(), display.getHeight());
        boolean isNewGame = true;
        Bundle b = getIntent().getExtras();
        if (b != null && b.getBoolean("gamePause", false)) {
            isNewGame = false;
        }
        this.gameView.init(isNewGame);
        this.mBar = (ProgressBar) findViewById(R.id.scorebar);
        this.mBar.setMax(100);
        updateBar();
        this.mGameSound = new GameSound(false, true, true);
        SharedPreferences settings = getSharedPreferences(this.PREFS_NAME, 0);
        this.mGameSound.mMusicOn = settings.getBoolean("isMusic", true);
        this.mGameSound.mSoundOn = settings.getBoolean("isSound", true);
        this.mGameSound.mVibrateOn = settings.getBoolean("isVibrate", true);
        initSP();
        playMusic();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.gameView.removeCallbacks(this.gameView);
        pauseMusic();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.gameView.run();
        resumeMusic();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        showSaveDlg();
        return false;
    }

    private void showSaveDlg() {
        new AlertDialog.Builder(this).setMessage("Are you want to save game before exit?").setTitle("Exit Game").setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                GameActivity.this.saveResumeData();
                GameActivity.this.showSumbitScoreDlg(true);
            }
        }).setNeutralButton("Discard", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                GameActivity.this.showSumbitScoreDlg(true);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create().show();
    }

    /* access modifiers changed from: protected */
    public void savePreferences() {
        SharedPreferences.Editor editor = getSharedPreferences(this.PREFS_NAME, 1).edit();
        editor.putInt("score", this.gameView.gameScore);
        editor.commit();
    }

    public void loadPreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences(this.PREFS_NAME, 1);
    }

    public void loadResumeData() {
        SharedPreferences gamePreferences = getSharedPreferences(this.PREFS_NAME, 1);
        this.gameView.gameScore = gamePreferences.getInt("score", 0);
        this.gameView.curScore = gamePreferences.getInt("curScore", 0);
        this.gameView.mBonus = gamePreferences.getInt("bonus", 1);
        new String("");
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                int blockId = gamePreferences.getInt("body_" + i + "_" + j, this.gameView.getRandomBlockId());
                if (blockId <= 0 || blockId > 9) {
                    blockId = this.gameView.getRandomBlockId();
                }
                this.gameView.body[i][j] = blockId;
            }
        }
    }

    public void saveResumeData() {
        SharedPreferences.Editor editor = getSharedPreferences(this.PREFS_NAME, 1).edit();
        if (this.gameView.body != null) {
            editor.putInt("score", this.gameView.gameScore);
            editor.putInt("curScore", this.gameView.curScore);
            editor.putInt("bonus", this.gameView.mBonus);
            new String("");
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    editor.putInt("body_" + i + "_" + j, this.gameView.body[i][j]);
                }
            }
            editor.commit();
        }
    }

    public void exitMain() {
        Log.v("Main", "exitMain(): GamePause: show Menu");
        doShowMenu(true, this.gameView.gameScore, this.gameView.mBonus);
        stopMusic();
        freeSP();
        this.mMusicMP = null;
        this.gameView.isRunning = false;
        this.gameView.removeCallbacks(this.gameView);
        finish();
    }

    /* access modifiers changed from: private */
    public void waitDlgShow() {
        this.waitDlg = ProgressDialog.show(this, "Ranking", "Please wait,Submit Score to Server", true);
        this.waitDlg.setCancelable(true);
    }

    private void waitDlgClose() {
        this.waitDlg.dismiss();
    }

    /* access modifiers changed from: private */
    public void showSumbitScoreDlg(boolean isFirst) {
        String sMsg;
        if (isFirst) {
            sMsg = "Do you want to submit your score (" + this.gameView.gameScore + ") to the global high scores list?";
        } else {
            sMsg = "Network error, submit failed, Try again?";
        }
        new AlertDialog.Builder(this).setMessage(sMsg).setTitle("Submit Score?").setIcon((int) R.drawable.icon).setPositiveButton("Submit Score", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                GameActivity.this.waitDlgShow();
                ScoreloopManager.submitScore(GameActivity.this.gameView.gameScore, 0, new ScoreSubmitObserver(GameActivity.this, null));
            }
        }).setNeutralButton("Discard", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                GameActivity.this.exitMain();
            }
        }).create().show();
    }

    public void doShowMenu(boolean gamePause, int gameScore, int gameLevel) {
        Log.v("Main", "doShowMenu(),gamePause=" + (gamePause ? "TRUE" : "FALSE"));
        Intent itMenu = new Intent();
        itMenu.setClass(this, GMenu.class);
        itMenu.putExtra("gamePause", gamePause);
        itMenu.putExtra("gameScore", gameScore);
        itMenu.putExtra("gameLevel", gameLevel);
        Log.v("Main", "doShowMenu(), level=" + Integer.toString(gameLevel) + ", mScore=" + Integer.toString(gameScore));
        startActivity(itMenu);
    }

    private void initSP() {
        this.mSoundPool = new SoundPool(30, 3, 100);
        this.mSoundMap = new HashMap<>();
        this.mGameSound.getClass();
        int i = 2;
        while (true) {
            this.mGameSound.getClass();
            if (i < 7) {
                this.mSoundMap.put(Integer.valueOf(i), Integer.valueOf(this.mSoundPool.load(getBaseContext(), this.mGameSound.getSoundResIdByType(i), 0)));
                i++;
            } else {
                return;
            }
        }
    }

    public void playSP(int soundId) {
        this.mGameSound.getClass();
        if (soundId >= 2) {
            this.mGameSound.getClass();
            if (soundId < 7 && this.mGameSound.mSoundOn) {
                this.mSoundPool.play(this.mSoundMap.get(Integer.valueOf(soundId)).intValue(), (float) 1, (float) 1, 0, 0, 1.0f);
            }
        }
    }

    private void freeSP() {
        if (this.mSoundPool != null) {
            this.mSoundPool.release();
            this.mSoundPool = null;
        }
        if (this.mSoundMap != null) {
            this.mSoundMap.clear();
            this.mSoundMap = null;
        }
    }

    public void playMusic() {
        if (this.mGameSound.mMusicOn) {
            stopMusic();
            if (this.mMusicMP == null) {
                GameSound gameSound = this.mGameSound;
                this.mGameSound.getClass();
                this.mMusicMP = MediaPlayer.create(getBaseContext(), gameSound.getSoundResIdByType(1));
            }
            this.mMusicMP.start();
            this.mMusicMP.setLooping(true);
        }
    }

    private void pauseMusic() {
        if (this.mMusicMP != null && this.mMusicMP.isPlaying()) {
            this.mMusicMP.pause();
        }
    }

    private void resumeMusic() {
        if (this.mMusicMP != null) {
            this.mMusicMP.start();
        }
    }

    private void stopMusic() {
        if (this.mMusicMP != null) {
            if (this.mMusicMP.isPlaying()) {
                this.mMusicMP.stop();
            }
            this.mMusicMP.release();
            this.mMusicMP = null;
        }
    }

    public void playVibrate() {
        if (this.mGameSound.mVibrateOn) {
            try {
                ((Vibrator) getSystemService("vibrator")).vibrate(new long[]{10, 30, 20, 10}, -1);
            } catch (Exception e) {
            }
        }
    }

    private void mySleep(long n) {
        try {
            Thread.sleep(n);
        } catch (Exception e) {
        }
    }

    public void updateBar() {
        this.mBar.setProgress((int) (((100.0f * ((float) this.gameView.curScore)) * 1.0f) / ((float) this.gameView.getCurLevelScore())));
    }

    private class ScoreSubmitObserver implements RequestControllerObserver {
        private ScoreSubmitObserver() {
        }

        /* synthetic */ ScoreSubmitObserver(GameActivity gameActivity, ScoreSubmitObserver scoreSubmitObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            GameActivity.this.showSumbitScoreDlg(false);
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            GameActivity.this.exitMain();
        }
    }
}
