package com.android.vending.licensing;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public abstract class f extends Binder implements ILicensingService {
    public f() {
        attachInterface(this, "com.android.vending.licensing.ILicensingService");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        g iVar;
        switch (i) {
            case 1:
                parcel.enforceInterface("com.android.vending.licensing.ILicensingService");
                long readLong = parcel.readLong();
                String readString = parcel.readString();
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    iVar = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.android.vending.licensing.ILicenseResultListener");
                    iVar = (queryLocalInterface == null || !(queryLocalInterface instanceof g)) ? new i(readStrongBinder) : (g) queryLocalInterface;
                }
                a(readLong, readString, iVar);
                return true;
            case 1598968902:
                parcel2.writeString("com.android.vending.licensing.ILicensingService");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
