package com.android.vending.licensing;

import android.text.TextUtils;
import android.util.Log;
import com.android.vending.licensing.LicenseCheckerCallback;
import com.android.vending.licensing.Policy;
import com.android.vending.licensing.util.Base64DecoderException;
import com.android.vending.licensing.util.a;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Iterator;
import java.util.regex.Pattern;

final class e {
    private final Policy a;
    private final LicenseCheckerCallback b;
    private final int c;
    private final String d;
    private final String e;
    private final a f;

    e(Policy policy, a aVar, LicenseCheckerCallback licenseCheckerCallback, int i, String str, String str2) {
        this.a = policy;
        this.f = aVar;
        this.b = licenseCheckerCallback;
        this.c = i;
        this.d = str;
        this.e = str2;
    }

    private void a(LicenseCheckerCallback.ApplicationErrorCode applicationErrorCode) {
        this.b.a(applicationErrorCode);
    }

    private void a(Policy.LicenseResponse licenseResponse) {
        this.a.a(licenseResponse);
        if (this.a.a()) {
            this.b.a();
        } else {
            this.b.b();
        }
    }

    private void d() {
        this.b.b();
    }

    public final LicenseCheckerCallback a() {
        return this.b;
    }

    public final void a(PublicKey publicKey, int i, String str, String str2) {
        if (i == 0 || i == 1 || i == 2) {
            try {
                Signature instance = Signature.getInstance("SHA1withRSA");
                instance.initVerify(publicKey);
                instance.update(str.getBytes());
                if (!instance.verify(a.a(str2))) {
                    Log.e("LicenseValidator", "Signature verification failed.");
                    d();
                    return;
                }
                try {
                    TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(':');
                    simpleStringSplitter.setString(str);
                    Iterator it = simpleStringSplitter.iterator();
                    if (!it.hasNext()) {
                        throw new IllegalArgumentException("Blank response.");
                    }
                    String str3 = (String) it.next();
                    String str4 = it.hasNext() ? (String) it.next() : "";
                    String[] split = TextUtils.split(str3, Pattern.quote("|"));
                    if (split.length < 6) {
                        throw new IllegalArgumentException("Wrong number of fields.");
                    }
                    l lVar = new l();
                    lVar.g = str4;
                    lVar.a = Integer.parseInt(split[0]);
                    lVar.b = Integer.parseInt(split[1]);
                    lVar.c = split[2];
                    lVar.d = split[3];
                    lVar.e = split[4];
                    lVar.f = Long.parseLong(split[5]);
                    if (lVar.a != i) {
                        Log.e("LicenseValidator", "Response codes don't match.");
                        d();
                        return;
                    } else if (lVar.b != this.c) {
                        Log.e("LicenseValidator", "Nonce doesn't match.");
                        d();
                        return;
                    } else if (!lVar.c.equals(this.d)) {
                        Log.e("LicenseValidator", "Package name doesn't match.");
                        d();
                        return;
                    } else if (!lVar.d.equals(this.e)) {
                        Log.e("LicenseValidator", "Version codes don't match.");
                        d();
                        return;
                    } else if (TextUtils.isEmpty(lVar.e)) {
                        Log.e("LicenseValidator", "User identifier is empty.");
                        d();
                        return;
                    }
                } catch (IllegalArgumentException e2) {
                    Log.e("LicenseValidator", "Could not parse response.");
                    d();
                    return;
                }
            } catch (NoSuchAlgorithmException e3) {
                throw new RuntimeException(e3);
            } catch (InvalidKeyException e4) {
                a(LicenseCheckerCallback.ApplicationErrorCode.INVALID_PUBLIC_KEY);
                return;
            } catch (SignatureException e5) {
                throw new RuntimeException(e5);
            } catch (Base64DecoderException e6) {
                Log.e("LicenseValidator", "Could not Base64-decode signature.");
                d();
                return;
            }
        }
        switch (i) {
            case 0:
            case 2:
                a(this.f.a());
                return;
            case 1:
                a(Policy.LicenseResponse.NOT_LICENSED);
                return;
            case 3:
                a(LicenseCheckerCallback.ApplicationErrorCode.NOT_MARKET_MANAGED);
                return;
            case 4:
                Log.w("LicenseValidator", "An error has occurred on the licensing server.");
                a(Policy.LicenseResponse.RETRY);
                return;
            case 5:
                Log.w("LicenseValidator", "Licensing server is refusing to talk to this device, over quota.");
                a(Policy.LicenseResponse.RETRY);
                return;
            case 257:
                Log.w("LicenseValidator", "Error contacting licensing server.");
                a(Policy.LicenseResponse.RETRY);
                return;
            case 258:
                a(LicenseCheckerCallback.ApplicationErrorCode.INVALID_PACKAGE_NAME);
                return;
            case 259:
                a(LicenseCheckerCallback.ApplicationErrorCode.NON_MATCHING_UID);
                return;
            default:
                Log.e("LicenseValidator", "Unknown response code for license check.");
                d();
                return;
        }
    }

    public final int b() {
        return this.c;
    }

    public final String c() {
        return this.d;
    }
}
