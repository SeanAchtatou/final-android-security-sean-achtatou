package com.android.vending.licensing;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.util.Log;
import com.android.vending.licensing.LicenseCheckerCallback;
import com.android.vending.licensing.Policy;
import com.android.vending.licensing.util.Base64DecoderException;
import com.android.vending.licensing.util.a;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public final class m implements ServiceConnection {
    private static final SecureRandom a = new SecureRandom();
    private ILicensingService b;
    /* access modifiers changed from: private */
    public PublicKey c;
    private final Context d;
    private final Policy e;
    /* access modifiers changed from: private */
    public Handler f;
    private final String g;
    private final String h;
    /* access modifiers changed from: private */
    public final Set i = new HashSet();
    private final Queue j = new LinkedList();

    public m(Context context, Policy policy, String str) {
        this.d = context;
        this.e = policy;
        this.c = a(str);
        this.g = this.d.getPackageName();
        this.h = a(context, this.g);
        HandlerThread handlerThread = new HandlerThread("background thread");
        handlerThread.start();
        this.f = new Handler(handlerThread.getLooper());
    }

    private static String a(Context context, String str) {
        try {
            return String.valueOf(context.getPackageManager().getPackageInfo(str, 0).versionCode);
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e("LicenseChecker", "Package not found. could not get version code.");
            return "";
        }
    }

    private static PublicKey a(String str) {
        try {
            return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(a.a(str)));
        } catch (NoSuchAlgorithmException e2) {
            throw new RuntimeException(e2);
        } catch (Base64DecoderException e3) {
            Log.e("LicenseChecker", "Could not decode from Base64.");
            throw new IllegalArgumentException(e3);
        } catch (InvalidKeySpecException e4) {
            Log.e("LicenseChecker", "Invalid key specification.");
            throw new IllegalArgumentException(e4);
        }
    }

    private void a() {
        while (true) {
            e eVar = (e) this.j.poll();
            if (eVar != null) {
                try {
                    Log.i("LicenseChecker", "Calling checkLicense on service for " + eVar.c());
                    this.b.a((long) eVar.b(), eVar.c(), new b(this, eVar));
                    this.i.add(eVar);
                } catch (RemoteException e2) {
                    Log.w("LicenseChecker", "RemoteException in checkLicense call.", e2);
                    b(eVar);
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(e eVar) {
        this.i.remove(eVar);
        if (this.i.isEmpty() && this.b != null) {
            try {
                this.d.unbindService(this);
            } catch (IllegalArgumentException e2) {
                Log.e("LicenseChecker", "Unable to unbind from licensing service (already unbound)");
            }
            this.b = null;
        }
        return;
    }

    /* access modifiers changed from: private */
    public synchronized void b(e eVar) {
        this.e.a(Policy.LicenseResponse.RETRY);
        if (this.e.a()) {
            eVar.a().a();
        } else {
            eVar.a().b();
        }
    }

    public final synchronized void a(LicenseCheckerCallback licenseCheckerCallback) {
        if (this.e.a()) {
            Log.i("LicenseChecker", "Using cached license response");
            licenseCheckerCallback.a();
        } else {
            e eVar = new e(this.e, new c(), licenseCheckerCallback, a.nextInt(), this.g, this.h);
            if (this.b == null) {
                Log.i("LicenseChecker", "Binding to licensing service.");
                try {
                    if (this.d.bindService(new Intent(ILicensingService.class.getName()), this, 1)) {
                        this.j.offer(eVar);
                    } else {
                        Log.e("LicenseChecker", "Could not bind to service.");
                        b(eVar);
                    }
                } catch (SecurityException e2) {
                    licenseCheckerCallback.a(LicenseCheckerCallback.ApplicationErrorCode.MISSING_PERMISSION);
                }
            } else {
                this.j.offer(eVar);
                a();
            }
        }
        return;
    }

    public final synchronized void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        ILicensingService hVar;
        if (iBinder == null) {
            hVar = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.android.vending.licensing.ILicensingService");
            hVar = (queryLocalInterface == null || !(queryLocalInterface instanceof ILicensingService)) ? new h(iBinder) : (ILicensingService) queryLocalInterface;
        }
        this.b = hVar;
        a();
    }

    public final synchronized void onServiceDisconnected(ComponentName componentName) {
        Log.w("LicenseChecker", "Service unexpectedly disconnected.");
        this.b = null;
    }
}
