package com.android.vending.licensing;

import android.util.Log;

final class k implements Runnable {
    private /* synthetic */ b a;
    private final /* synthetic */ int b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;

    k(b bVar, int i, String str, String str2) {
        this.a = bVar;
        this.b = i;
        this.c = str;
        this.d = str2;
    }

    public final void run() {
        Log.i("LicenseChecker", "Received response.");
        if (this.a.a.i.contains(this.a.b)) {
            b.b(this.a);
            this.a.b.a(this.a.a.c, this.b, this.c, this.d);
            this.a.a.a(this.a.b);
        }
    }
}
