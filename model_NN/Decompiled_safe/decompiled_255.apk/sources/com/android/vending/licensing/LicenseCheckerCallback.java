package com.android.vending.licensing;

public interface LicenseCheckerCallback {

    public enum ApplicationErrorCode {
        INVALID_PACKAGE_NAME,
        NON_MATCHING_UID,
        NOT_MARKET_MANAGED,
        CHECK_IN_PROGRESS,
        INVALID_PUBLIC_KEY,
        MISSING_PERMISSION
    }

    void a();

    void a(ApplicationErrorCode applicationErrorCode);

    void b();
}
