package com.android.vending.licensing;

import android.os.IBinder;
import android.os.Parcel;

final class i implements g {
    private IBinder a;

    i(IBinder iBinder) {
        this.a = iBinder;
    }

    public final void a(int i, String str, String str2) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.android.vending.licensing.ILicenseResultListener");
            obtain.writeInt(i);
            obtain.writeString(str);
            obtain.writeString(str2);
            this.a.transact(1, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    public final IBinder asBinder() {
        return this.a;
    }
}
