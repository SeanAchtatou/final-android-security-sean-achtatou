package com.android.vending.licensing;

import android.os.IBinder;
import android.os.Parcel;

final class h implements ILicensingService {
    private IBinder a;

    h(IBinder iBinder) {
        this.a = iBinder;
    }

    public final void a(long j, String str, g gVar) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.android.vending.licensing.ILicensingService");
            obtain.writeLong(j);
            obtain.writeString(str);
            obtain.writeStrongBinder(gVar != null ? gVar.asBinder() : null);
            this.a.transact(1, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    public final IBinder asBinder() {
        return this.a;
    }
}
