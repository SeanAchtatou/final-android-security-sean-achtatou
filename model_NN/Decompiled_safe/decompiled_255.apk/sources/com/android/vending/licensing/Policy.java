package com.android.vending.licensing;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:39)
    */
public interface Policy {

    public enum LicenseResponse {
        LICENSED,
        NOT_LICENSED,
        RETRY
    }

    void a(LicenseResponse licenseResponse);

    boolean a();
}
