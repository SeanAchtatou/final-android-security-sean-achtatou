package com.android.vending.licensing;

import android.util.Log;

final class b extends d {
    final /* synthetic */ m a;
    /* access modifiers changed from: private */
    public final e b;
    private Runnable c = new j(this);

    public b(m mVar, e eVar) {
        this.a = mVar;
        this.b = eVar;
        Log.i("LicenseChecker", "Start monitoring timeout.");
        this.a.f.postDelayed(this.c, 10000);
    }

    static /* synthetic */ void b(b bVar) {
        Log.i("LicenseChecker", "Clearing timeout.");
        bVar.a.f.removeCallbacks(bVar.c);
    }

    public final void a(int i, String str, String str2) {
        this.a.f.post(new k(this, i, str, str2));
    }
}
