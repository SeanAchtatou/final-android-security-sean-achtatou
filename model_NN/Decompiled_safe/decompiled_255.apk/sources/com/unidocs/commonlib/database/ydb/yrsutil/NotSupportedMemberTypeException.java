package com.unidocs.commonlib.database.ydb.yrsutil;

public class NotSupportedMemberTypeException extends Exception {
    public NotSupportedMemberTypeException(String str) {
        super(str);
    }
}
