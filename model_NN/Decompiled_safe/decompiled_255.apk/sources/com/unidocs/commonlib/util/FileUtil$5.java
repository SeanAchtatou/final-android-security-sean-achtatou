package com.unidocs.commonlib.util;

import java.io.File;

class FileUtil$5 extends File {
    private static final long serialVersionUID = -3747187948721544803L;

    FileUtil$5(String str) {
        super(str);
    }

    public int compareTo(File file) {
        return length() > file.length() ? 1 : -1;
    }
}
