package com.unidocs.commonlib.util.b;

import com.unidocs.commonlib.util.i;
import java.io.ByteArrayInputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class a {
    private Document a;

    public a(String str, String str2) {
        this(str.getBytes(str2));
    }

    private a(byte[] bArr) {
        ByteArrayInputStream byteArrayInputStream;
        Throwable th;
        try {
            byteArrayInputStream = new ByteArrayInputStream(bArr);
            try {
                this.a = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(byteArrayInputStream);
                i.a(byteArrayInputStream);
            } catch (Throwable th2) {
                th = th2;
                i.a(byteArrayInputStream);
                throw th;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            byteArrayInputStream = null;
            th = th4;
            i.a(byteArrayInputStream);
            throw th;
        }
    }

    public static String a(Element element) {
        String str = null;
        if (element == null) {
            return "";
        }
        NodeList childNodes = element.getChildNodes();
        String str2 = null;
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node item = childNodes.item(i);
            if (item.getNodeType() == 3) {
                str2 = item.getNodeValue();
            } else if (item.getNodeType() == 4) {
                str = item.getNodeValue();
            }
        }
        return str != null ? str : str2;
    }

    public static Element[] a(Element element, String str) {
        if (element == null) {
            return null;
        }
        NodeList elementsByTagName = element.getElementsByTagName(str);
        int length = elementsByTagName.getLength();
        if (length <= 0) {
            return null;
        }
        Element[] elementArr = new Element[length];
        for (int i = 0; i < elementsByTagName.getLength(); i++) {
            elementArr[i] = (Element) elementsByTagName.item(i);
        }
        return elementArr;
    }

    public final Element a() {
        return this.a.getDocumentElement();
    }
}
