package com.unidocs.commonlib.util;

import java.lang.reflect.Method;
import java.util.Collection;

public final class b {
    private static final char[] a = {12593, 12594, 12596, 12599, 12600, 12601, 12609, 12610, 12611, 12613, 12614, 12615, 12616, 12617, 12618, 12619, 12620, 12621, 12622};
    private static Class b;
    private static Class c;

    public static boolean a(Object obj) {
        return obj != null;
    }

    public static boolean a(Object obj, Object[] objArr) {
        if (b(obj) || b(objArr)) {
            return false;
        }
        for (Object equals : objArr) {
            if (obj.equals(equals)) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(String str) {
        return str != null && str.trim().length() > 0 && !str.equalsIgnoreCase("null");
    }

    public static boolean a(String str, String[] strArr, boolean z) {
        try {
            return b(str, strArr, z);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean a(Collection collection) {
        return collection != null && collection.size() > 0;
    }

    public static boolean a(Object[] objArr) {
        if (objArr == null || objArr.length == 0) {
            return false;
        }
        for (Object b2 : objArr) {
            if (b(b2)) {
                return false;
            }
        }
        return true;
    }

    public static boolean a(String[] strArr) {
        if (strArr == null || strArr.length == 0) {
            return false;
        }
        for (String b2 : strArr) {
            if (b(b2)) {
                return false;
            }
        }
        return true;
    }

    public static boolean b(Object obj) {
        return !(obj != null);
    }

    public static boolean b(String str) {
        return !a(str);
    }

    private static boolean b(String str, String[] strArr, boolean z) {
        Method method;
        if (b(str) || b(strArr)) {
            return false;
        }
        if (z) {
            Class<?> cls = b;
            if (cls == null) {
                try {
                    cls = Class.forName("java.lang.String");
                    b = cls;
                } catch (ClassNotFoundException e) {
                    throw new NoClassDefFoundError(e.getMessage());
                }
            }
            Class[] clsArr = new Class[1];
            Class<?> cls2 = c;
            if (cls2 == null) {
                try {
                    cls2 = Class.forName("java.lang.Object");
                    c = cls2;
                } catch (ClassNotFoundException e2) {
                    throw new NoClassDefFoundError(e2.getMessage());
                }
            }
            clsArr[0] = cls2;
            method = cls.getMethod("equals", clsArr);
        } else {
            Class<?> cls3 = b;
            if (cls3 == null) {
                try {
                    cls3 = Class.forName("java.lang.String");
                    b = cls3;
                } catch (ClassNotFoundException e3) {
                    throw new NoClassDefFoundError(e3.getMessage());
                }
            }
            Class[] clsArr2 = new Class[1];
            Class<?> cls4 = b;
            if (cls4 == null) {
                try {
                    cls4 = Class.forName("java.lang.String");
                    b = cls4;
                } catch (ClassNotFoundException e4) {
                    throw new NoClassDefFoundError(e4.getMessage());
                }
            }
            clsArr2[0] = cls4;
            method = cls3.getMethod("equalsIgnoreCase", clsArr2);
        }
        for (int i = 0; i < strArr.length; i++) {
            if (((Boolean) method.invoke(str, strArr[i])).booleanValue()) {
                return true;
            }
        }
        return false;
    }

    public static boolean b(Collection collection) {
        return !a(collection);
    }

    public static boolean b(Object[] objArr) {
        return !a(objArr);
    }

    public static boolean b(String[] strArr) {
        return !a(strArr);
    }
}
