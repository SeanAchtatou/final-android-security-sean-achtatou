package com.unidocs.commonlib.util;

public final class f {
    /* JADX WARNING: Removed duplicated region for block: B:11:0x001b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.io.File r8, java.io.File r9) {
        /*
            r0 = 0
            boolean r1 = r8.exists()     // Catch:{ all -> 0x0015 }
            if (r1 == 0) goto L_0x000d
            boolean r1 = r8.isFile()     // Catch:{ all -> 0x0015 }
            if (r1 != 0) goto L_0x001f
        L_0x000d:
            java.lang.Exception r1 = new java.lang.Exception     // Catch:{ all -> 0x0015 }
            java.lang.String r2 = "원본파일이 존재하지 않습니다"
            r1.<init>(r2)     // Catch:{ all -> 0x0015 }
            throw r1     // Catch:{ all -> 0x0015 }
        L_0x0015:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x0019:
            if (r1 == 0) goto L_0x001e
            r1.close()
        L_0x001e:
            throw r0
        L_0x001f:
            boolean r1 = r9.exists()     // Catch:{ all -> 0x0015 }
            if (r1 != 0) goto L_0x0028
            r9.mkdirs()     // Catch:{ all -> 0x0015 }
        L_0x0028:
            java.util.zip.ZipFile r1 = new java.util.zip.ZipFile     // Catch:{ all -> 0x0015 }
            r1.<init>(r8)     // Catch:{ all -> 0x0015 }
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ all -> 0x0093 }
            java.lang.String r2 = r9.getAbsolutePath()     // Catch:{ all -> 0x0093 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ all -> 0x0093 }
            r0.<init>(r2)     // Catch:{ all -> 0x0093 }
            java.lang.String r2 = java.io.File.separator     // Catch:{ all -> 0x0093 }
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ all -> 0x0093 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0093 }
            java.lang.String r2 = java.io.File.separator     // Catch:{ all -> 0x0093 }
            java.lang.String r3 = "\\"
            boolean r2 = r2.equals(r3)     // Catch:{ all -> 0x0093 }
            if (r2 == 0) goto L_0x005e
            java.lang.String r2 = "\\\\"
        L_0x0050:
            java.util.Enumeration r3 = r1.entries()     // Catch:{ all -> 0x0093 }
        L_0x0054:
            boolean r4 = r3.hasMoreElements()     // Catch:{ all -> 0x0093 }
            if (r4 != 0) goto L_0x0061
            r1.close()
            return
        L_0x005e:
            java.lang.String r2 = java.io.File.separator     // Catch:{ all -> 0x0093 }
            goto L_0x0050
        L_0x0061:
            java.lang.Object r8 = r3.nextElement()     // Catch:{ all -> 0x0093 }
            java.util.zip.ZipEntry r8 = (java.util.zip.ZipEntry) r8     // Catch:{ all -> 0x0093 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ all -> 0x0093 }
            java.lang.String r5 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0093 }
            r4.<init>(r5)     // Catch:{ all -> 0x0093 }
            java.lang.String r5 = r8.getName()     // Catch:{ all -> 0x0093 }
            java.lang.String r6 = "[\\\\/]"
            java.lang.String r5 = r5.replaceAll(r6, r2)     // Catch:{ all -> 0x0093 }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ all -> 0x0093 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0093 }
            java.lang.String r5 = java.io.File.separator     // Catch:{ all -> 0x0093 }
            boolean r5 = r4.endsWith(r5)     // Catch:{ all -> 0x0093 }
            if (r5 == 0) goto L_0x0095
            java.io.File r5 = new java.io.File     // Catch:{ all -> 0x0093 }
            r5.<init>(r4)     // Catch:{ all -> 0x0093 }
            r5.mkdirs()     // Catch:{ all -> 0x0093 }
            goto L_0x0054
        L_0x0093:
            r0 = move-exception
            goto L_0x0019
        L_0x0095:
            java.io.InputStream r5 = r1.getInputStream(r8)     // Catch:{ all -> 0x0093 }
            java.io.File r6 = new java.io.File     // Catch:{ all -> 0x0093 }
            r6.<init>(r4)     // Catch:{ all -> 0x0093 }
            com.unidocs.commonlib.util.j.a(r6, r5)     // Catch:{ all -> 0x0093 }
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.unidocs.commonlib.util.f.a(java.io.File, java.io.File):void");
    }
}
