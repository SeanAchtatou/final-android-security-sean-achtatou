package com.unidocs.commonlib.util.web;

import com.unidocs.commonlib.util.a.b;
import com.unidocs.commonlib.util.i;
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;

public class RequestController extends HttpServlet {
    static Class class$0;
    static Class class$1;
    private Map methodMap;

    public void finalizeJsonResponse(HttpServletResponse httpServletResponse, b bVar) {
        String a = bVar.a();
        httpServletResponse.setHeader("Content-Type", "text/html;charset=UTF-8");
        PrintWriter printWriter = null;
        try {
            printWriter = httpServletResponse.getWriter();
            printWriter.print(a);
            printWriter.flush();
        } finally {
            i.a(printWriter);
        }
    }

    public void finalizeStreamResponse(HttpServletResponse httpServletResponse, File file, String str) {
        ServletOutputStream servletOutputStream;
        FileInputStream fileInputStream;
        try {
            httpServletResponse.setHeader("Content-Type", str);
            httpServletResponse.setHeader("Content-Length", new StringBuffer().append(file.length()).toString());
            FileInputStream fileInputStream2 = new FileInputStream(file);
            try {
                servletOutputStream = httpServletResponse.getOutputStream();
                try {
                    byte[] bArr = new byte[1048576];
                    while (true) {
                        int read = fileInputStream2.read(bArr);
                        if (read == -1) {
                            servletOutputStream.flush();
                            i.a(new Object[]{fileInputStream2, servletOutputStream});
                            return;
                        }
                        servletOutputStream.write(bArr, 0, read);
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    fileInputStream = fileInputStream2;
                    th = th2;
                }
            } catch (Throwable th3) {
                fileInputStream = fileInputStream2;
                th = th3;
                servletOutputStream = null;
                i.a(new Object[]{fileInputStream, servletOutputStream});
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            servletOutputStream = null;
            fileInputStream = null;
            i.a(new Object[]{fileInputStream, servletOutputStream});
            throw th;
        }
    }

    public Object getRequestParameter(HttpServletRequest httpServletRequest, Class cls) {
        Object newInstance = cls.newInstance();
        BeanUtils.populate(newInstance, httpServletRequest.getParameterMap());
        return newInstance;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0081 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void init() {
        /*
            r8 = this;
            r7 = 1
            r6 = 0
            java.lang.Class r0 = r8.getClass()
            java.lang.reflect.Method[] r0 = r0.getMethods()
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            r8.methodMap = r1
            r1 = r6
        L_0x0012:
            int r2 = r0.length
            if (r1 < r2) goto L_0x003d
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.String r1 = "## RequestController 초기화되었습니다"
            r0.println(r1)
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            java.lang.String r2 = "## "
            r1.<init>(r2)
            java.util.Map r2 = r8.methodMap
            int r2 = r2.size()
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = " 종류의 서비스프로세스가 준비되었습니다"
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.println(r1)
            return
        L_0x003d:
            r2 = r0[r1]
            java.lang.Class[] r3 = r2.getParameterTypes()
            int r4 = r2.getModifiers()
            if (r4 != r7) goto L_0x009a
            int r4 = r3.length
            r5 = 2
            if (r4 != r5) goto L_0x009a
            r4 = r3[r6]
            java.lang.Class r5 = com.unidocs.commonlib.util.web.RequestController.class$0
            if (r5 != 0) goto L_0x005b
            java.lang.String r5 = "javax.servlet.http.HttpServletRequest"
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ ClassNotFoundException -> 0x0084 }
            com.unidocs.commonlib.util.web.RequestController.class$0 = r5
        L_0x005b:
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x009a
            r3 = r3[r7]
            java.lang.Class r4 = com.unidocs.commonlib.util.web.RequestController.class$1
            if (r4 != 0) goto L_0x006f
            java.lang.String r4 = "javax.servlet.http.HttpServletResponse"
            java.lang.Class r4 = java.lang.Class.forName(r4)     // Catch:{ ClassNotFoundException -> 0x008f }
            com.unidocs.commonlib.util.web.RequestController.class$1 = r4
        L_0x006f:
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x009a
            r3 = r7
        L_0x0076:
            if (r3 == 0) goto L_0x0081
            java.util.Map r3 = r8.methodMap
            java.lang.String r4 = r2.getName()
            r3.put(r4, r2)
        L_0x0081:
            int r1 = r1 + 1
            goto L_0x0012
        L_0x0084:
            r0 = move-exception
            java.lang.NoClassDefFoundError r1 = new java.lang.NoClassDefFoundError
            java.lang.String r0 = r0.getMessage()
            r1.<init>(r0)
            throw r1
        L_0x008f:
            r0 = move-exception
            java.lang.NoClassDefFoundError r1 = new java.lang.NoClassDefFoundError
            java.lang.String r0 = r0.getMessage()
            r1.<init>(r0)
            throw r1
        L_0x009a:
            r3 = r6
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.unidocs.commonlib.util.web.RequestController.init():void");
    }

    public void printServiceList(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        ServletOutputStream servletOutputStream;
        Throwable th;
        try {
            ServletOutputStream outputStream = httpServletResponse.getOutputStream();
            try {
                outputStream.print("## SERVICE LIST<br><br>");
                for (Object append : new TreeMap(this.methodMap).keySet()) {
                    outputStream.print(new StringBuffer().append(append).append("<br>").toString());
                }
                outputStream.flush();
                i.a(outputStream);
            } catch (Throwable th2) {
                Throwable th3 = th2;
                servletOutputStream = outputStream;
                th = th3;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            servletOutputStream = null;
            th = th5;
            i.a(servletOutputStream);
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void service(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        httpServletRequest.setCharacterEncoding("UTF-8");
        httpServletResponse.setCharacterEncoding("UTF-8");
        String str = "";
        String parameter = httpServletRequest.getParameter("cmd");
        if (parameter != null && parameter.length() > 0 && !parameter.toUpperCase().equals("NULL")) {
            str = parameter;
        }
        System.out.println("## RequestController called");
        PrintStream printStream = System.out;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("\n");
        stringBuffer.append("## PARAMETER SUMMARY\n");
        Enumeration parameterNames = httpServletRequest.getParameterNames();
        int i = -1;
        while (parameterNames.hasMoreElements()) {
            String str2 = (String) parameterNames.nextElement();
            i++;
            stringBuffer.append(new StringBuffer("# ").append(i).toString());
            stringBuffer.append(" KEY : ");
            stringBuffer.append(str2);
            stringBuffer.append("\n");
            String[] parameterValues = httpServletRequest.getParameterValues(str2);
            for (String append : parameterValues) {
                stringBuffer.append(new StringBuffer("# ").append(i).toString());
                stringBuffer.append(" VALUE : ");
                stringBuffer.append(append);
                stringBuffer.append("\n");
            }
        }
        printStream.println(stringBuffer.toString());
        Method method = (Method) this.methodMap.get(str);
        if (method != null) {
            try {
                method.invoke(this, httpServletRequest, httpServletResponse);
            } catch (Exception e) {
                e.printStackTrace();
                throw new ServletException(e);
            }
        } else {
            System.out.println("## 규약을 지키지 않은 요청 - 무시합니다");
        }
    }
}
