package com.unidocs.commonlib.util;

import java.io.File;

class FileUtil$1 extends File {
    private static final long serialVersionUID = -4838685290778453582L;

    FileUtil$1(String str) {
        super(str);
    }

    public int compareTo(File file) {
        String lowerCase = getAbsolutePath().toLowerCase();
        String lowerCase2 = file.getAbsolutePath().toLowerCase();
        int min = Math.min(lowerCase.length(), lowerCase2.length());
        for (int i = 0; i < min; i++) {
            if (lowerCase.charAt(i) < lowerCase2.charAt(i)) {
                return -1;
            }
            if (lowerCase.charAt(i) > lowerCase2.charAt(i)) {
                return 1;
            }
        }
        return lowerCase.length() > lowerCase2.length() ? 1 : -1;
    }
}
