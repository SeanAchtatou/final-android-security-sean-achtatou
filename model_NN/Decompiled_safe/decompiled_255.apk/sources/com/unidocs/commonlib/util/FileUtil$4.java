package com.unidocs.commonlib.util;

import java.io.File;

class FileUtil$4 extends File {
    private static final long serialVersionUID = -1670532984100445951L;

    FileUtil$4(String str) {
        super(str);
    }

    public int compareTo(File file) {
        return lastModified() > file.lastModified() ? -1 : 1;
    }
}
