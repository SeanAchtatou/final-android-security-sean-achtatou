package com.unidocs.commonlib.util;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public final class a {
    private HashMap a = new HashMap();
    private String b;

    public a(String str) {
        this.b = str;
        c.a(str);
        List a2 = j.a(new File(str));
        if (b.a((Collection) a2)) {
            for (int i = 0; i < a2.size(); i++) {
                String trim = ((String) a2.get(i)).trim();
                if (!trim.startsWith("#") && trim.indexOf("=") > 0) {
                    this.a.put(trim.substring(0, trim.indexOf("=")).trim(), trim.substring(trim.indexOf("=") + 1).trim());
                }
            }
        }
    }

    public final float a(String str, float f) {
        try {
            return Float.parseFloat((String) this.a.get(str));
        } catch (Exception e) {
            return f;
        }
    }

    public final int a(String str, int i) {
        try {
            return Integer.parseInt((String) this.a.get(str));
        } catch (Exception e) {
            return i;
        }
    }

    public final String a(String str, String str2) {
        String str3 = (String) this.a.get(str);
        return str3 != null ? str3 : str2;
    }

    public final void a(String str) {
        if (this.b != null) {
            File parentFile = new File(this.b).getParentFile();
            if (!parentFile.exists() || !parentFile.isDirectory()) {
                parentFile.mkdirs();
            }
            StringBuffer stringBuffer = new StringBuffer();
            for (Object next : this.a.keySet()) {
                stringBuffer.append(new StringBuffer().append(next).append("=").append(this.a.get(next)).append("\n").toString());
            }
            String str2 = this.b;
            j.a(new File(str2), stringBuffer.toString(), str);
        }
    }

    public final void a(String str, Object obj) {
        this.a.put(str, obj.toString());
    }

    public final boolean a(String str, boolean z) {
        String str2 = (String) this.a.get(str);
        if ("true".equalsIgnoreCase(str2)) {
            return true;
        }
        if ("false".equalsIgnoreCase(str2)) {
            return false;
        }
        return z;
    }
}
