package com.unidocs.commonlib.util;

import java.io.File;

class FileUtil$3 extends File {
    private static final long serialVersionUID = -6543950233203340595L;

    FileUtil$3(String str) {
        super(str);
    }

    public int compareTo(File file) {
        return lastModified() > file.lastModified() ? 1 : -1;
    }
}
