package com.unidocs.commonlib.util;

public final class d {
    private static Class a;
    private static Class b;
    private static Class c;
    private static Class d;
    private static Class e;

    public static boolean a(Class cls) {
        Class<?> cls2 = a;
        if (cls2 == null) {
            try {
                cls2 = Class.forName("java.lang.Integer");
                a = cls2;
            } catch (ClassNotFoundException e2) {
                throw new NoClassDefFoundError(e2.getMessage());
            }
        }
        if (cls != cls2) {
            Class<?> cls3 = b;
            if (cls3 == null) {
                try {
                    cls3 = Class.forName("java.lang.Long");
                    b = cls3;
                } catch (ClassNotFoundException e3) {
                    throw new NoClassDefFoundError(e3.getMessage());
                }
            }
            if (cls != cls3) {
                Class<?> cls4 = c;
                if (cls4 == null) {
                    try {
                        cls4 = Class.forName("java.lang.Double");
                        c = cls4;
                    } catch (ClassNotFoundException e4) {
                        throw new NoClassDefFoundError(e4.getMessage());
                    }
                }
                if (cls != cls4) {
                    Class<?> cls5 = d;
                    if (cls5 == null) {
                        try {
                            cls5 = Class.forName("java.lang.Short");
                            d = cls5;
                        } catch (ClassNotFoundException e5) {
                            throw new NoClassDefFoundError(e5.getMessage());
                        }
                    }
                    if (cls != cls5) {
                        Class<?> cls6 = e;
                        if (cls6 == null) {
                            try {
                                cls6 = Class.forName("java.lang.Float");
                                e = cls6;
                            } catch (ClassNotFoundException e6) {
                                throw new NoClassDefFoundError(e6.getMessage());
                            }
                        }
                        return cls == cls6 || cls == Integer.TYPE || cls == Long.TYPE || cls == Double.TYPE || cls == Short.TYPE || cls == Float.TYPE;
                    }
                }
            }
        }
    }
}
