package com.unidocs.commonlib.util;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class h {
    private static final ThreadLocal a = new g();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.unidocs.commonlib.util.h.a(java.lang.String, java.lang.CharSequence):java.util.regex.Matcher
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.unidocs.commonlib.util.h.a(java.lang.String, java.lang.String):boolean
      com.unidocs.commonlib.util.h.a(java.lang.String, java.lang.CharSequence):java.util.regex.Matcher */
    public static String a(String str, String str2, int i) {
        Matcher a2 = a(str2, (CharSequence) str);
        String str3 = null;
        if (a2.find()) {
            str3 = i <= 0 ? a2.group() : a2.group(i);
        }
        a(a2);
        return str3;
    }

    public static Matcher a(String str, CharSequence charSequence) {
        if (str == null) {
            throw new IllegalArgumentException("String 'pattern' must not be null");
        }
        Map map = (Map) a.get();
        Matcher matcher = (Matcher) map.get(str);
        if (matcher == null) {
            return Pattern.compile(str).matcher(charSequence);
        }
        map.put(str, null);
        matcher.reset(charSequence);
        return matcher;
    }

    public static void a(Matcher matcher) {
        ((Map) a.get()).put(matcher.pattern().pattern(), matcher);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.unidocs.commonlib.util.h.a(java.lang.String, java.lang.CharSequence):java.util.regex.Matcher
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.unidocs.commonlib.util.h.a(java.lang.String, java.lang.String):boolean
      com.unidocs.commonlib.util.h.a(java.lang.String, java.lang.CharSequence):java.util.regex.Matcher */
    public static boolean a(String str, String str2) {
        Matcher a2 = a(str2, (CharSequence) str);
        boolean find = a2.find();
        a(a2);
        return find;
    }

    public static String b(String str, String str2) {
        return a(str, new StringBuffer("(^|\\&)(").append(str2).append("=)(.+?)($|\\&)").toString(), 3);
    }
}
