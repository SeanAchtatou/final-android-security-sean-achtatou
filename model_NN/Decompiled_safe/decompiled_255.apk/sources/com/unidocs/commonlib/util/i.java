package com.unidocs.commonlib.util;

import java.lang.reflect.Method;

public final class i {
    private i() {
    }

    public static void a(Object obj) {
        if (obj != null) {
            try {
                Method method = obj.getClass().getMethod("close", null);
                method.setAccessible(true);
                method.invoke(obj, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void a(Object[] objArr) {
        for (Object a : objArr) {
            a(a);
        }
    }
}
