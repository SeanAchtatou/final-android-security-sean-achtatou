package com.unidocs.commonlib.util;

public final class e {
    private static String[] a = {"가", "까", "나", "다", "따", "라", "마", "바", "빠", "사", "싸", "아", "자", "짜", "차", "카", "타", "파", "하"};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuffer.insert(int, char):java.lang.StringBuffer}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuffer.insert(int, float):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, boolean):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, double):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.Object):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, long):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, int):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.CharSequence):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.String):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, char[]):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, char):java.lang.StringBuffer} */
    public static String a(String str) {
        String str2 = str == null ? "" : str;
        StringBuffer stringBuffer = new StringBuffer();
        int min = Math.min(str2.length(), 4);
        stringBuffer.append(str2.substring(0, min));
        for (int i = 4 - min; i > 0; i--) {
            stringBuffer.insert(0, '0');
        }
        return stringBuffer.toString();
    }
}
