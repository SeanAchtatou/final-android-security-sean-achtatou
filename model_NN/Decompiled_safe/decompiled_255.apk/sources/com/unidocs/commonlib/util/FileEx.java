package com.unidocs.commonlib.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;

public class FileEx implements Serializable {
    private byte[] byteStream = null;

    public byte[] getFile() {
        return this.byteStream;
    }

    public void read(File file) {
        FileInputStream fileInputStream;
        Throwable th;
        Exception e;
        try {
            byte[] bArr = new byte[new Integer((int) file.length()).intValue()];
            FileInputStream fileInputStream2 = new FileInputStream(file);
            try {
                fileInputStream2.read(bArr);
                this.byteStream = bArr;
                fileInputStream2.close();
            } catch (Exception e2) {
                e = e2;
                fileInputStream = fileInputStream2;
                try {
                    e.printStackTrace();
                    fileInputStream.close();
                } catch (Throwable th2) {
                    th = th2;
                    fileInputStream.close();
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                fileInputStream = fileInputStream2;
                fileInputStream.close();
                throw th;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            fileInputStream = null;
            e = exc;
            e.printStackTrace();
            fileInputStream.close();
        } catch (Throwable th4) {
            Throwable th5 = th4;
            fileInputStream = null;
            th = th5;
            fileInputStream.close();
            throw th;
        }
    }
}
