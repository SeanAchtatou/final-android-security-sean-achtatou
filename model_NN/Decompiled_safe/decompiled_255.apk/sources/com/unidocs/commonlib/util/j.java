package com.unidocs.commonlib.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public final class j {
    public static List a(File file) {
        ArrayList arrayList = new ArrayList();
        BufferedReader bufferedReader = null;
        try {
            BufferedReader bufferedReader2 = new BufferedReader(new FileReader(file));
            while (true) {
                try {
                    String readLine = bufferedReader2.readLine();
                    if (readLine == null) {
                        bufferedReader2.close();
                        return arrayList;
                    }
                    arrayList.add(readLine);
                } catch (Throwable th) {
                    th = th;
                    bufferedReader = bufferedReader2;
                }
            }
        } catch (Throwable th2) {
            th = th2;
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x001f  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.io.File r5, java.io.InputStream r6) {
        /*
            r3 = 0
            java.io.BufferedInputStream r0 = new java.io.BufferedInputStream     // Catch:{ all -> 0x001a }
            r0.<init>(r6)     // Catch:{ all -> 0x001a }
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x0028 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ all -> 0x0028 }
            r2.<init>(r5)     // Catch:{ all -> 0x0028 }
            r1.<init>(r2)     // Catch:{ all -> 0x0028 }
            a(r0, r1)     // Catch:{ all -> 0x002d }
            r0.close()
            r1.close()
            return
        L_0x001a:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x001d:
            if (r2 == 0) goto L_0x0022
            r2.close()
        L_0x0022:
            if (r1 == 0) goto L_0x0027
            r1.close()
        L_0x0027:
            throw r0
        L_0x0028:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x001d
        L_0x002d:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.unidocs.commonlib.util.j.a(java.io.File, java.io.InputStream):void");
    }

    public static void a(File file, String str, String str2) {
        if (str2 == null) {
            a(file, str.getBytes());
        } else {
            a(file, str.getBytes(str2));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x001f  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(java.io.File r5, byte[] r6) {
        /*
            r3 = 0
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream     // Catch:{ all -> 0x001a }
            r0.<init>(r6)     // Catch:{ all -> 0x001a }
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x0028 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ all -> 0x0028 }
            r2.<init>(r5)     // Catch:{ all -> 0x0028 }
            r1.<init>(r2)     // Catch:{ all -> 0x0028 }
            a(r0, r1)     // Catch:{ all -> 0x002d }
            r1.close()
            r0.close()
            return
        L_0x001a:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x001d:
            if (r1 == 0) goto L_0x0022
            r1.close()
        L_0x0022:
            if (r2 == 0) goto L_0x0027
            r2.close()
        L_0x0027:
            throw r0
        L_0x0028:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x001d
        L_0x002d:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.unidocs.commonlib.util.j.a(java.io.File, byte[]):void");
    }

    private static void a(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[1048576];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                outputStream.flush();
                return;
            }
            outputStream.write(bArr, 0, read);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x001d  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(java.io.InputStream r4) {
        /*
            r2 = 0
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x0019 }
            r0.<init>()     // Catch:{ all -> 0x0019 }
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ all -> 0x0026 }
            r1.<init>(r4)     // Catch:{ all -> 0x0026 }
            a(r4, r0)     // Catch:{ all -> 0x002c }
            byte[] r2 = r0.toByteArray()     // Catch:{ all -> 0x002c }
            r0.close()
            r1.close()
            return r2
        L_0x0019:
            r0 = move-exception
            r1 = r2
        L_0x001b:
            if (r2 == 0) goto L_0x0020
            r2.close()
        L_0x0020:
            if (r1 == 0) goto L_0x0025
            r1.close()
        L_0x0025:
            throw r0
        L_0x0026:
            r1 = move-exception
            r3 = r1
            r1 = r2
            r2 = r0
            r0 = r3
            goto L_0x001b
        L_0x002c:
            r2 = move-exception
            r3 = r2
            r2 = r0
            r0 = r3
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.unidocs.commonlib.util.j.a(java.io.InputStream):byte[]");
    }
}
