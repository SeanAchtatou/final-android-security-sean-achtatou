package com.unidocs.commonlib.util;

import java.io.File;
import java.io.FileOutputStream;
import java.util.TreeSet;

public final class c {
    private static Class a;

    public static void a(String str) {
        File file = new File(str);
        if (!file.exists() || file.isDirectory()) {
            FileOutputStream fileOutputStream = null;
            try {
                if (!file.exists() || file.isDirectory()) {
                    File parentFile = file.getParentFile();
                    if (!parentFile.exists()) {
                        parentFile.mkdirs();
                    }
                }
                fileOutputStream = new FileOutputStream(file);
            } finally {
                i.a(fileOutputStream);
            }
        }
        file.setLastModified(System.currentTimeMillis());
    }

    public static boolean a(File file) {
        if (!file.exists()) {
            return true;
        }
        File file2 = file;
        while (true) {
            File[] listFiles = file2.listFiles();
            if (listFiles == null || listFiles.length == 0) {
                file2.delete();
                if (file2.equals(file)) {
                    return true;
                }
                file2 = file;
            } else {
                file2 = listFiles[0];
            }
        }
    }

    public static File[] a(int i, File[] fileArr) {
        Class<?> cls = null;
        switch (i) {
            case 1:
                cls = new FileUtil$1("").getClass();
                break;
            case 2:
                cls = new FileUtil$2("").getClass();
                break;
            case 3:
                cls = new FileUtil$3("").getClass();
                break;
            case 4:
                cls = new FileUtil$4("").getClass();
                break;
            case 5:
                cls = new FileUtil$5("").getClass();
                break;
            case 6:
                cls = new FileUtil$6("").getClass();
                break;
        }
        return a(cls, fileArr);
    }

    private static File[] a(Class cls, File[] fileArr) {
        TreeSet treeSet = new TreeSet();
        for (int i = 0; i < fileArr.length; i++) {
            Class[] clsArr = new Class[1];
            Class<?> cls2 = a;
            if (cls2 == null) {
                try {
                    cls2 = Class.forName("java.lang.String");
                    a = cls2;
                } catch (ClassNotFoundException e) {
                    throw new NoClassDefFoundError(e.getMessage());
                }
            }
            clsArr[0] = cls2;
            treeSet.add(cls.getDeclaredConstructor(clsArr).newInstance(fileArr[i].getAbsolutePath()));
        }
        return (File[]) treeSet.toArray(new File[0]);
    }
}
