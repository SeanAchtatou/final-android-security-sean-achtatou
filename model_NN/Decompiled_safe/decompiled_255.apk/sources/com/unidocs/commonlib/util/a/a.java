package com.unidocs.commonlib.util.a;

import com.unidocs.commonlib.util.d;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

public final class a {
    private static Class a;

    public static String a(Object obj) {
        if (obj == null) {
            return "null";
        }
        Class<?> cls = obj.getClass();
        if (cls.isPrimitive()) {
            return new StringBuffer().append(obj).toString();
        }
        if (cls != Boolean.TYPE) {
            Class<?> cls2 = a;
            if (cls2 == null) {
                try {
                    cls2 = Class.forName("java.lang.Boolean");
                    a = cls2;
                } catch (ClassNotFoundException e) {
                    throw new NoClassDefFoundError(e.getMessage());
                }
            }
            if (cls != cls2) {
                return d.a(cls) ? new StringBuffer().append(obj).toString() : obj instanceof Collection ? a((Collection) obj) : obj instanceof Map ? a((Map) obj) : cls.isArray() ? a(obj, cls.getComponentType()) : obj instanceof c ? ((c) obj).a() : new StringBuffer("\"").append(obj.toString().replaceAll("\\\"", "\\\\\\\"").replaceAll("\\r", "\\\\r").replaceAll("\\n", "\\\\n").replaceAll("<", "&lt;").replaceAll(">", "&gt;")).append("\"").toString();
            }
        }
        return new StringBuffer().append(obj).toString();
    }

    private static String a(Object obj, Class cls) {
        if (obj == null) {
            return "null";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[");
        if (cls.isPrimitive()) {
            String name = cls.getName();
            if (name.equals("int")) {
                int[] iArr = (int[]) obj;
                for (int i = 0; i < iArr.length; i++) {
                    stringBuffer.append(iArr[i]);
                    if (i < iArr.length - 1) {
                        stringBuffer.append(",");
                    }
                }
            } else if (name.equals("boolean")) {
                boolean[] zArr = (boolean[]) obj;
                for (int i2 = 0; i2 < zArr.length; i2++) {
                    stringBuffer.append(zArr[i2]);
                    if (i2 < zArr.length - 1) {
                        stringBuffer.append(",");
                    }
                }
            } else if (name.equals("long")) {
                long[] jArr = (long[]) obj;
                for (int i3 = 0; i3 < jArr.length; i3++) {
                    stringBuffer.append(jArr[i3]);
                    if (i3 < jArr.length - 1) {
                        stringBuffer.append(",");
                    }
                }
            } else if (name.equals("short")) {
                short[] sArr = (short[]) obj;
                for (int i4 = 0; i4 < sArr.length; i4++) {
                    stringBuffer.append((int) sArr[i4]);
                    if (i4 < sArr.length - 1) {
                        stringBuffer.append(",");
                    }
                }
            } else if (name.equals("double")) {
                double[] dArr = (double[]) obj;
                for (int i5 = 0; i5 < dArr.length; i5++) {
                    stringBuffer.append(dArr[i5]);
                    if (i5 < dArr.length - 1) {
                        stringBuffer.append(",");
                    }
                }
            } else if (name.equals("float")) {
                float[] fArr = (float[]) obj;
                for (int i6 = 0; i6 < fArr.length; i6++) {
                    stringBuffer.append(fArr[i6]);
                    if (i6 < fArr.length - 1) {
                        stringBuffer.append(",");
                    }
                }
            } else if (name.equals("char")) {
                char[] cArr = (char[]) obj;
                for (int i7 = 0; i7 < cArr.length; i7++) {
                    stringBuffer.append(cArr[i7]);
                    if (i7 < cArr.length - 1) {
                        stringBuffer.append(",");
                    }
                }
            } else if (name.equals("byte")) {
                byte[] bArr = (byte[]) obj;
                for (int i8 = 0; i8 < bArr.length; i8++) {
                    stringBuffer.append((int) bArr[i8]);
                    if (i8 < bArr.length - 1) {
                        stringBuffer.append(",");
                    }
                }
            }
        } else {
            Object[] objArr = (Object[]) obj;
            for (int i9 = 0; i9 < objArr.length; i9++) {
                stringBuffer.append(a(objArr[i9]));
                if (i9 < objArr.length - 1) {
                    stringBuffer.append(",");
                }
            }
        }
        stringBuffer.append("]");
        return stringBuffer.toString();
    }

    private static String a(Collection collection) {
        if (collection == null) {
            return "null";
        }
        if (collection.size() <= 0) {
            return "[]";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[");
        Iterator it = collection.iterator();
        while (true) {
            stringBuffer.append(a(it.next()));
            if (it.hasNext()) {
                stringBuffer.append(",");
            } else {
                stringBuffer.append("]");
                return stringBuffer.toString();
            }
        }
    }

    private static String a(Map map) {
        if (map == null) {
            return "null";
        }
        if (map.size() <= 0) {
            return "{}";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("{");
        Iterator it = map.keySet().iterator();
        while (true) {
            Object next = it.next();
            Object obj = map.get(next);
            stringBuffer.append(new StringBuffer("\"").append(next).append("\":").toString());
            stringBuffer.append(a(obj));
            if (it.hasNext()) {
                stringBuffer.append(",");
            } else {
                stringBuffer.append("}");
                return stringBuffer.toString();
            }
        }
    }
}
