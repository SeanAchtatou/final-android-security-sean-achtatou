package com.unidocs.commonlib.util.a;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class d implements c {
    public final String a() {
        if (this == null) {
            return "null";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("{");
        Method[] methods = getClass().getMethods();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < methods.length; i++) {
            Method method = methods[i];
            if (method.getName().startsWith("get") && method.getParameterTypes().length == 0 && method.getModifiers() == 1 && method.getReturnType() != Void.TYPE) {
                arrayList.add(methods[i]);
            }
        }
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            Method method2 = (Method) arrayList.get(i2);
            String substring = method2.getName().substring(3);
            String stringBuffer2 = new StringBuffer(String.valueOf(substring.substring(0, 1).toLowerCase())).append(substring.substring(1)).toString();
            Object invoke = method2.invoke(this, new Class[0]);
            stringBuffer.append(new StringBuffer("\"").append(stringBuffer2).append("\":").toString());
            stringBuffer.append(a.a(invoke));
            if (i2 < size - 1) {
                stringBuffer.append(",");
            }
        }
        stringBuffer.append("}");
        return stringBuffer.toString();
    }
}
