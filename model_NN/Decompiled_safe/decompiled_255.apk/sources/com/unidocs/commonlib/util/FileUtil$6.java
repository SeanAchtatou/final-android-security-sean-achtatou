package com.unidocs.commonlib.util;

import java.io.File;

class FileUtil$6 extends File {
    private static final long serialVersionUID = -5889627805990582608L;

    FileUtil$6(String str) {
        super(str);
    }

    public int compareTo(File file) {
        return length() > file.length() ? -1 : 1;
    }
}
