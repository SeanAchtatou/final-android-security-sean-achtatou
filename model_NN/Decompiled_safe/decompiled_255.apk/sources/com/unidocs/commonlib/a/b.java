package com.unidocs.commonlib.a;

public final class b {
    private static long a = 0;
    private static int b = 0;

    public static synchronized String a(int i) {
        String stringBuffer;
        synchronized (b.class) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis == a) {
                b++;
            } else {
                b = 0;
            }
            a = currentTimeMillis;
            String stringBuffer2 = new StringBuffer(String.valueOf(b)).toString();
            while (stringBuffer2.length() < 3) {
                stringBuffer2 = new StringBuffer("0").append(stringBuffer2).toString();
            }
            stringBuffer = new StringBuffer(String.valueOf(currentTimeMillis)).append(stringBuffer2).toString();
            while (stringBuffer.length() < i) {
                stringBuffer = new StringBuffer("0").append(stringBuffer).toString();
            }
        }
        return stringBuffer;
    }
}
