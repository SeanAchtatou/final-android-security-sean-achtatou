package com.unidocs.commonlib.a;

import java.security.MessageDigest;

public final class a {
    public static String a(String str, String str2) {
        byte[] digest = MessageDigest.getInstance("SHA-1").digest(str.getBytes(str2));
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : digest) {
            byte b2 = b & 255;
            if (b2 < 16) {
                stringBuffer.append("0");
            }
            stringBuffer.append(Integer.toHexString(b2).toUpperCase());
        }
        return stringBuffer.toString();
    }
}
