package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;

public class CanWriteFileFilter extends a implements Serializable {
    public static final b CANNOT_WRITE = new NotFileFilter(CAN_WRITE);
    public static final b CAN_WRITE = new CanWriteFileFilter();

    protected CanWriteFileFilter() {
    }

    public boolean accept(File file) {
        return file.canWrite();
    }
}
