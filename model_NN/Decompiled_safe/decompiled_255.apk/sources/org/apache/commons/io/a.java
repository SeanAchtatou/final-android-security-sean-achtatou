package org.apache.commons.io;

import java.io.File;
import java.util.ArrayList;
import java.util.Stack;

public final class a {
    private static String a = new Character('.').toString();
    private static final char b = File.separatorChar;
    private static final char c;

    static {
        if (a()) {
            c = '/';
        } else {
            c = '\\';
        }
    }

    public static String a(String str) {
        int lastIndexOf;
        if (str == null) {
            return null;
        }
        if (str == null) {
            lastIndexOf = -1;
        } else {
            lastIndexOf = str.lastIndexOf(46);
            if ((str == null ? -1 : Math.max(str.lastIndexOf(47), str.lastIndexOf(92))) > lastIndexOf) {
                lastIndexOf = -1;
            }
        }
        return lastIndexOf == -1 ? "" : str.substring(lastIndexOf + 1);
    }

    static boolean a() {
        return b == '\\';
    }

    public static boolean a(String str, String str2) {
        return a(str, str2, IOCase.SENSITIVE);
    }

    public static boolean a(String str, String str2, IOCase iOCase) {
        String[] strArr;
        if (str == null && str2 == null) {
            return true;
        }
        if (str == null || str2 == null) {
            return false;
        }
        IOCase iOCase2 = iOCase == null ? IOCase.SENSITIVE : iOCase;
        String convertCase = iOCase2.convertCase(str);
        String convertCase2 = iOCase2.convertCase(str2);
        if (convertCase2.indexOf("?") == -1 && convertCase2.indexOf("*") == -1) {
            strArr = new String[]{convertCase2};
        } else {
            char[] charArray = convertCase2.toCharArray();
            ArrayList arrayList = new ArrayList();
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < charArray.length; i++) {
                if (charArray[i] == '?' || charArray[i] == '*') {
                    if (stringBuffer.length() != 0) {
                        arrayList.add(stringBuffer.toString());
                        stringBuffer.setLength(0);
                    }
                    if (charArray[i] == '?') {
                        arrayList.add("?");
                    } else if (arrayList.size() == 0 || (i > 0 && !arrayList.get(arrayList.size() - 1).equals("*"))) {
                        arrayList.add("*");
                    }
                } else {
                    stringBuffer.append(charArray[i]);
                }
            }
            if (stringBuffer.length() != 0) {
                arrayList.add(stringBuffer.toString());
            }
            strArr = (String[]) arrayList.toArray(new String[arrayList.size()]);
        }
        Stack stack = new Stack();
        int i2 = 0;
        int i3 = 0;
        boolean z = false;
        do {
            if (stack.size() > 0) {
                int[] iArr = (int[]) stack.pop();
                i2 = iArr[0];
                i3 = iArr[1];
                z = true;
            }
            while (i2 < strArr.length) {
                if (strArr[i2].equals("?")) {
                    i3++;
                    z = false;
                } else if (!strArr[i2].equals("*")) {
                    if (!z) {
                        if (!convertCase.startsWith(strArr[i2], i3)) {
                            break;
                        }
                    } else {
                        i3 = convertCase.indexOf(strArr[i2], i3);
                        if (i3 == -1) {
                            break;
                        }
                        int indexOf = convertCase.indexOf(strArr[i2], i3 + 1);
                        if (indexOf >= 0) {
                            stack.push(new int[]{i2, indexOf});
                        }
                    }
                    i3 += strArr[i2].length();
                    z = false;
                } else if (i2 == strArr.length - 1) {
                    i3 = convertCase.length();
                    z = true;
                } else {
                    z = true;
                }
                i2++;
            }
            if (i2 == strArr.length && i3 == convertCase.length()) {
                return true;
            }
        } while (stack.size() > 0);
        return false;
    }
}
