package org.apache.commons.io.comparator;

import java.io.File;
import java.io.Serializable;
import java.util.Comparator;
import org.apache.commons.io.b;

public class SizeFileComparator implements Serializable, Comparator {
    public static final Comparator SIZE_COMPARATOR = new SizeFileComparator();
    public static final Comparator SIZE_REVERSE = new ReverseComparator(SIZE_COMPARATOR);
    public static final Comparator SIZE_SUMDIR_COMPARATOR = new SizeFileComparator(true);
    public static final Comparator SIZE_SUMDIR_REVERSE = new ReverseComparator(SIZE_SUMDIR_COMPARATOR);
    private final boolean sumDirectoryContents;

    public SizeFileComparator() {
        this.sumDirectoryContents = false;
    }

    public SizeFileComparator(boolean z) {
        this.sumDirectoryContents = z;
    }

    public int compare(Object obj, Object obj2) {
        File file = (File) obj;
        File file2 = (File) obj2;
        long b = (file.isDirectory() ? (!this.sumDirectoryContents || !file.exists()) ? 0 : b.b(file) : file.length()) - (file2.isDirectory() ? (!this.sumDirectoryContents || !file2.exists()) ? 0 : b.b(file2) : file2.length());
        if (b < 0) {
            return -1;
        }
        return b > 0 ? 1 : 0;
    }
}
