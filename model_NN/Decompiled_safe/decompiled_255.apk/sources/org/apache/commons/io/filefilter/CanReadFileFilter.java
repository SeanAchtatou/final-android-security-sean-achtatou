package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;

public class CanReadFileFilter extends a implements Serializable {
    public static final b CANNOT_READ = new NotFileFilter(CAN_READ);
    public static final b CAN_READ = new CanReadFileFilter();
    public static final b READ_ONLY = new AndFileFilter(CAN_READ, CanWriteFileFilter.CANNOT_WRITE);

    protected CanReadFileFilter() {
    }

    public boolean accept(File file) {
        return file.canRead();
    }
}
