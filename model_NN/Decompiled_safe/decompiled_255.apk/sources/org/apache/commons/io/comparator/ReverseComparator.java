package org.apache.commons.io.comparator;

import java.io.Serializable;
import java.util.Comparator;

class ReverseComparator implements Serializable, Comparator {
    private final Comparator delegate;

    public ReverseComparator(Comparator comparator) {
        if (comparator == null) {
            throw new IllegalArgumentException("Delegate comparator is missing");
        }
        this.delegate = comparator;
    }

    public int compare(Object obj, Object obj2) {
        return this.delegate.compare(obj2, obj);
    }
}
