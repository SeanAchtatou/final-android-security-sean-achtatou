package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;

public class NotFileFilter extends a implements Serializable {
    private final b filter;

    public NotFileFilter(b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("The filter must not be null");
        }
        this.filter = bVar;
    }

    public boolean accept(File file) {
        return !this.filter.accept(file);
    }

    public boolean accept(File file, String str) {
        return !this.filter.accept(file, str);
    }

    public String toString() {
        return new StringBuffer().append(super.toString()).append("(").append(this.filter.toString()).append(")").toString();
    }
}
