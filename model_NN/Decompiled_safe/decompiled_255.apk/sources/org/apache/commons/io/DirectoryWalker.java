package org.apache.commons.io;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

public abstract class DirectoryWalker {
    private final FileFilter a = null;
    private final int b = -1;

    public class CancelException extends IOException {
        private static final long serialVersionUID = 1347339620135041008L;
        private int depth;
        private File file;

        public CancelException(File file2, int i) {
            this("Operation Cancelled", file2, i);
        }

        public CancelException(String str, File file2, int i) {
            super(str);
            this.depth = -1;
            this.file = file2;
            this.depth = i;
        }

        public int getDepth() {
            return this.depth;
        }

        public File getFile() {
            return this.file;
        }
    }

    private DirectoryWalker() {
    }
}
