package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;

public class HiddenFileFilter extends a implements Serializable {
    public static final b HIDDEN = new HiddenFileFilter();
    public static final b VISIBLE = new NotFileFilter(HIDDEN);

    protected HiddenFileFilter() {
    }

    public boolean accept(File file) {
        return file.isHidden();
    }
}
