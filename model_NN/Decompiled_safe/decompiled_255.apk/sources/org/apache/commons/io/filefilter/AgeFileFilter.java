package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import org.apache.commons.io.b;

public class AgeFileFilter extends a implements Serializable {
    private final boolean acceptOlder;
    private final long cutoff;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.filefilter.AgeFileFilter.<init>(long, boolean):void
     arg types: [long, int]
     candidates:
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.io.File, boolean):void
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.util.Date, boolean):void
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(long, boolean):void */
    public AgeFileFilter(long j) {
        this(j, true);
    }

    public AgeFileFilter(long j, boolean z) {
        this.acceptOlder = z;
        this.cutoff = j;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.io.File, boolean):void
     arg types: [java.io.File, int]
     candidates:
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(long, boolean):void
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.util.Date, boolean):void
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.io.File, boolean):void */
    public AgeFileFilter(File file) {
        this(file, true);
    }

    public AgeFileFilter(File file, boolean z) {
        this(file.lastModified(), z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.util.Date, boolean):void
     arg types: [java.util.Date, int]
     candidates:
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(long, boolean):void
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.io.File, boolean):void
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.util.Date, boolean):void */
    public AgeFileFilter(Date date) {
        this(date, true);
    }

    public AgeFileFilter(Date date, boolean z) {
        this(date.getTime(), z);
    }

    public boolean accept(File file) {
        boolean a = b.a(file, this.cutoff);
        return this.acceptOlder ? !a : a;
    }

    public String toString() {
        return new StringBuffer().append(super.toString()).append("(").append(this.acceptOlder ? "<=" : ">").append(this.cutoff).append(")").toString();
    }
}
