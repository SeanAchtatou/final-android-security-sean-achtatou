package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AndFileFilter extends a implements Serializable {
    private List fileFilters;

    public AndFileFilter() {
        this.fileFilters = new ArrayList();
    }

    public AndFileFilter(List list) {
        if (list == null) {
            this.fileFilters = new ArrayList();
        } else {
            this.fileFilters = new ArrayList(list);
        }
    }

    public AndFileFilter(b bVar, b bVar2) {
        if (bVar == null || bVar2 == null) {
            throw new IllegalArgumentException("The filters must not be null");
        }
        this.fileFilters = new ArrayList();
        addFileFilter(bVar);
        addFileFilter(bVar2);
    }

    public boolean accept(File file) {
        if (this.fileFilters.size() == 0) {
            return false;
        }
        for (b accept : this.fileFilters) {
            if (!accept.accept(file)) {
                return false;
            }
        }
        return true;
    }

    public boolean accept(File file, String str) {
        if (this.fileFilters.size() == 0) {
            return false;
        }
        for (b accept : this.fileFilters) {
            if (!accept.accept(file, str)) {
                return false;
            }
        }
        return true;
    }

    public void addFileFilter(b bVar) {
        this.fileFilters.add(bVar);
    }

    public List getFileFilters() {
        return Collections.unmodifiableList(this.fileFilters);
    }

    public boolean removeFileFilter(b bVar) {
        return this.fileFilters.remove(bVar);
    }

    public void setFileFilters(List list) {
        this.fileFilters = new ArrayList(list);
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(super.toString());
        stringBuffer.append("(");
        if (this.fileFilters != null) {
            for (int i = 0; i < this.fileFilters.size(); i++) {
                if (i > 0) {
                    stringBuffer.append(",");
                }
                Object obj = this.fileFilters.get(i);
                stringBuffer.append(obj == null ? "null" : obj.toString());
            }
        }
        stringBuffer.append(")");
        return stringBuffer.toString();
    }
}
