package org.apache.commons.io.comparator;

import java.io.File;
import java.io.Serializable;
import java.util.Comparator;

public class DefaultFileComparator implements Serializable, Comparator {
    public static final Comparator DEFAULT_COMPARATOR = new DefaultFileComparator();
    public static final Comparator DEFAULT_REVERSE = new ReverseComparator(DEFAULT_COMPARATOR);

    public int compare(Object obj, Object obj2) {
        return ((File) obj).compareTo((File) obj2);
    }
}
