package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.Serializable;

public class DelegateFileFilter extends a implements Serializable {
    private final FileFilter fileFilter;
    private final FilenameFilter filenameFilter;

    public DelegateFileFilter(FileFilter fileFilter2) {
        if (fileFilter2 == null) {
            throw new IllegalArgumentException("The FileFilter must not be null");
        }
        this.fileFilter = fileFilter2;
        this.filenameFilter = null;
    }

    public DelegateFileFilter(FilenameFilter filenameFilter2) {
        if (filenameFilter2 == null) {
            throw new IllegalArgumentException("The FilenameFilter must not be null");
        }
        this.filenameFilter = filenameFilter2;
        this.fileFilter = null;
    }

    public boolean accept(File file) {
        return this.fileFilter != null ? this.fileFilter.accept(file) : super.accept(file);
    }

    public boolean accept(File file, String str) {
        return this.filenameFilter != null ? this.filenameFilter.accept(file, str) : super.accept(file, str);
    }

    public String toString() {
        return new StringBuffer().append(super.toString()).append("(").append(this.fileFilter != null ? this.fileFilter.toString() : this.filenameFilter.toString()).append(")").toString();
    }
}
