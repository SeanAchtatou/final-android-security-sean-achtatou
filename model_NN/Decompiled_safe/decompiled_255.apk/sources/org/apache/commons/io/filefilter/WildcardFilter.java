package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.io.a;

public class WildcardFilter extends a implements Serializable {
    private final String[] wildcards;

    public WildcardFilter(String str) {
        if (str == null) {
            throw new IllegalArgumentException("The wildcard must not be null");
        }
        this.wildcards = new String[]{str};
    }

    public WildcardFilter(List list) {
        if (list == null) {
            throw new IllegalArgumentException("The wildcard list must not be null");
        }
        this.wildcards = (String[]) list.toArray(new String[list.size()]);
    }

    public WildcardFilter(String[] strArr) {
        if (strArr == null) {
            throw new IllegalArgumentException("The wildcard array must not be null");
        }
        this.wildcards = strArr;
    }

    public boolean accept(File file) {
        if (file.isDirectory()) {
            return false;
        }
        for (String a : this.wildcards) {
            if (a.a(file.getName(), a)) {
                return true;
            }
        }
        return false;
    }

    public boolean accept(File file, String str) {
        if (file != null && new File(file, str).isDirectory()) {
            return false;
        }
        for (String a : this.wildcards) {
            if (a.a(str, a)) {
                return true;
            }
        }
        return false;
    }
}
