package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;

public interface b extends FileFilter, FilenameFilter {
    boolean accept(File file);

    boolean accept(File file, String str);
}
