package org.apache.commons.io;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class b {
    private static File[] a = new File[0];

    public static String a(long j) {
        return j / 1073741824 > 0 ? new StringBuffer().append(String.valueOf(j / 1073741824)).append(" GB").toString() : j / 1048576 > 0 ? new StringBuffer().append(String.valueOf(j / 1048576)).append(" MB").toString() : j / 1024 > 0 ? new StringBuffer().append(String.valueOf(j / 1024)).append(" KB").toString() : new StringBuffer().append(String.valueOf(j)).append(" bytes").toString();
    }

    public static void a(File file) {
        if (!file.exists()) {
            if (!file.exists()) {
                File parentFile = file.getParentFile();
                if (parentFile != null && !parentFile.exists() && !parentFile.mkdirs()) {
                    throw new IOException(new StringBuffer().append("File '").append(file).append("' could not be created").toString());
                }
            } else if (file.isDirectory()) {
                throw new IOException(new StringBuffer().append("File '").append(file).append("' exists but is a directory").toString());
            } else if (!file.canWrite()) {
                throw new IOException(new StringBuffer().append("File '").append(file).append("' cannot be written to").toString());
            }
            c.a(new FileOutputStream(file));
        }
        if (!file.setLastModified(System.currentTimeMillis())) {
            throw new IOException(new StringBuffer().append("Unable to set the last modification time for ").append(file).toString());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.b.a(java.io.File, java.io.File, boolean):void
     arg types: [java.io.File, java.io.File, int]
     candidates:
      org.apache.commons.io.b.a(java.io.File, java.io.File, java.io.FileFilter):void
      org.apache.commons.io.b.a(java.io.File, java.io.File, boolean):void */
    public static void a(File file, File file2) {
        if (file == null) {
            throw new NullPointerException("Source must not be null");
        } else if (file2 == null) {
            throw new NullPointerException("Destination must not be null");
        } else if (!file.exists()) {
            throw new FileNotFoundException(new StringBuffer().append("Source '").append(file).append("' does not exist").toString());
        } else if (file.isDirectory()) {
            throw new IOException(new StringBuffer().append("Source '").append(file).append("' exists but is a directory").toString());
        } else if (file.getCanonicalPath().equals(file2.getCanonicalPath())) {
            throw new IOException(new StringBuffer().append("Source '").append(file).append("' and destination '").append(file2).append("' are the same").toString());
        } else if (file2.getParentFile() != null && !file2.getParentFile().exists() && !file2.getParentFile().mkdirs()) {
            throw new IOException(new StringBuffer().append("Destination '").append(file2).append("' directory cannot be created").toString());
        } else if (!file2.exists() || file2.canWrite()) {
            a(file, file2, true);
        } else {
            throw new IOException(new StringBuffer().append("Destination '").append(file2).append("' exists but is read-only").toString());
        }
    }

    public static void a(File file, File file2, FileFilter fileFilter) {
        b(file, file2, fileFilter);
    }

    private static void a(File file, File file2, FileFilter fileFilter, boolean z, List list) {
        if (file2.exists()) {
            if (!file2.isDirectory()) {
                throw new IOException(new StringBuffer().append("Destination '").append(file2).append("' exists but is not a directory").toString());
            }
        } else if (!file2.mkdirs()) {
            throw new IOException(new StringBuffer().append("Destination '").append(file2).append("' directory cannot be created").toString());
        } else if (z) {
            file2.setLastModified(file.lastModified());
        }
        if (!file2.canWrite()) {
            throw new IOException(new StringBuffer().append("Destination '").append(file2).append("' cannot be written to").toString());
        }
        File[] listFiles = fileFilter == null ? file.listFiles() : file.listFiles(fileFilter);
        if (listFiles == null) {
            throw new IOException(new StringBuffer().append("Failed to list contents of ").append(file).toString());
        }
        for (int i = 0; i < listFiles.length; i++) {
            File file3 = new File(file2, listFiles[i].getName());
            if (list == null || !list.contains(listFiles[i].getCanonicalPath())) {
                if (listFiles[i].isDirectory()) {
                    a(listFiles[i], file3, fileFilter, z, list);
                } else {
                    a(listFiles[i], file3, z);
                }
            }
        }
    }

    private static void a(File file, File file2, boolean z) {
        FileOutputStream fileOutputStream;
        if (!file2.exists() || !file2.isDirectory()) {
            FileInputStream fileInputStream = new FileInputStream(file);
            try {
                fileOutputStream = new FileOutputStream(file2);
                c.a(fileInputStream, fileOutputStream);
                c.a(fileOutputStream);
                c.a(fileInputStream);
                if (file.length() != file2.length()) {
                    throw new IOException(new StringBuffer().append("Failed to copy full contents from '").append(file).append("' to '").append(file2).append("'").toString());
                } else if (z) {
                    file2.setLastModified(file.lastModified());
                }
            } catch (Throwable th) {
                c.a(fileInputStream);
                throw th;
            }
        } else {
            throw new IOException(new StringBuffer().append("Destination '").append(file2).append("' exists but is a directory").toString());
        }
    }

    public static boolean a(File file, long j) {
        if (file == null) {
            throw new IllegalArgumentException("No specified file");
        } else if (!file.exists()) {
            return false;
        } else {
            return file.lastModified() > j;
        }
    }

    public static long b(File file) {
        long j = 0;
        if (!file.exists()) {
            throw new IllegalArgumentException(new StringBuffer().append(file).append(" does not exist").toString());
        } else if (!file.isDirectory()) {
            throw new IllegalArgumentException(new StringBuffer().append(file).append(" is not a directory").toString());
        } else {
            File[] listFiles = file.listFiles();
            if (listFiles == null) {
                return 0;
            }
            for (File file2 : listFiles) {
                j += file2.isDirectory() ? b(file2) : file2.length();
            }
            return j;
        }
    }

    public static void b(File file, File file2) {
        b(file, file2, null);
    }

    private static void b(File file, File file2, FileFilter fileFilter) {
        if (file == null) {
            throw new NullPointerException("Source must not be null");
        } else if (file2 == null) {
            throw new NullPointerException("Destination must not be null");
        } else if (!file.exists()) {
            throw new FileNotFoundException(new StringBuffer().append("Source '").append(file).append("' does not exist").toString());
        } else if (!file.isDirectory()) {
            throw new IOException(new StringBuffer().append("Source '").append(file).append("' exists but is not a directory").toString());
        } else if (file.getCanonicalPath().equals(file2.getCanonicalPath())) {
            throw new IOException(new StringBuffer().append("Source '").append(file).append("' and destination '").append(file2).append("' are the same").toString());
        } else {
            ArrayList arrayList = null;
            if (file2.getCanonicalPath().startsWith(file.getCanonicalPath())) {
                File[] listFiles = fileFilter == null ? file.listFiles() : file.listFiles(fileFilter);
                if (listFiles != null && listFiles.length > 0) {
                    arrayList = new ArrayList(listFiles.length);
                    for (File name : listFiles) {
                        arrayList.add(new File(file2, name.getName()).getCanonicalPath());
                    }
                }
            }
            a(file, file2, fileFilter, true, arrayList);
        }
    }
}
