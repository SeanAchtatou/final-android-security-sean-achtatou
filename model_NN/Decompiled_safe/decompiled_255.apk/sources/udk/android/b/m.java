package udk.android.b;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Build;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.ClipboardManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import com.unidocs.commonlib.util.b;

public final class m {
    private static boolean a = false;

    public static int a(Context context, int i) {
        float f = f(context);
        return f != 1.0f ? (int) (((double) (f * ((float) i))) + 0.5d) : i;
    }

    public static String a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    public static void a(Activity activity, View view, String str) {
        view.post(new a(activity, str));
    }

    public static void a(View view) {
        ((InputMethodManager) view.getContext().getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 0, null);
    }

    public static void a(View view, ResultReceiver resultReceiver) {
        ((InputMethodManager) view.getContext().getSystemService("input_method")).showSoftInput(view, 2, resultReceiver);
    }

    public static void a(View view, String str, String str2) {
        ((ClipboardManager) view.getContext().getSystemService("clipboard")).setText(str);
        if (b.a(str2)) {
            view.post(new b(view, str2));
        }
    }

    public static boolean a() {
        if (a) {
            return true;
        }
        return "zoom2".equals(Build.DEVICE) && ("LogicPD".equals(Build.MANUFACTURER) || "BarnesAndNoble".equals(Build.MANUFACTURER));
    }

    public static int b(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return -1;
        }
    }

    public static String c(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    public static String d(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    public static Display e(Context context) {
        return ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
    }

    public static float f(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    public static Rect g(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
    }
}
