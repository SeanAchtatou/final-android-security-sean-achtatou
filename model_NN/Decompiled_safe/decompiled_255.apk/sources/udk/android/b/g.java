package udk.android.b;

import android.view.View;
import android.view.animation.AlphaAnimation;

final class g implements Runnable {
    private final /* synthetic */ float a;
    private final /* synthetic */ View b;

    g(float f, View view) {
        this.a = f;
        this.b = view;
    }

    public final void run() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, this.a);
        alphaAnimation.setDuration(0);
        alphaAnimation.setFillAfter(true);
        this.b.startAnimation(alphaAnimation);
    }
}
