package udk.android.b;

import android.view.View;
import android.widget.Toast;

final class b implements Runnable {
    private final /* synthetic */ View a;
    private final /* synthetic */ String b;

    b(View view, String str) {
        this.a = view;
        this.b = str;
    }

    public final void run() {
        Toast.makeText(this.a.getContext(), this.b, 0).show();
    }
}
