package udk.android.b;

import android.graphics.PointF;
import android.graphics.RectF;

public final class c {
    public static double a = 6.283185307179586d;

    public static double a(double d) {
        return (180.0d * d) / 3.141592653589793d;
    }

    private static float a(double d, float f) {
        return (f / ((float) Math.cos(d))) * ((float) Math.sin(d));
    }

    public static float a(float f, float f2, float f3, float f4) {
        float f5 = f3 - f;
        float f6 = f4 - f2;
        return (float) Math.sqrt((double) ((f5 * f5) + (f6 * f6)));
    }

    public static float a(PointF pointF, PointF pointF2) {
        return a(pointF.x, pointF.y, pointF2.x, pointF2.y);
    }

    public static float a(RectF rectF, RectF rectF2) {
        return a(rectF2.left, rectF2.top, rectF2.right, rectF2.bottom) / a(rectF.left, rectF.top, rectF.right, rectF.bottom);
    }

    public static int a(int i) {
        return (i >> 24) & 255;
    }

    public static int a(int i, int i2) {
        return a(i2, b(i), c(i), i & 255);
    }

    public static int a(int i, int i2, int i3, int i4) {
        return (i << 24) | (i2 << 16) | (i3 << 8) | i4;
    }

    public static PointF a(PointF pointF, PointF pointF2, PointF pointF3) {
        PointF pointF4 = new PointF();
        float f = pointF2.x - pointF.x;
        float f2 = pointF2.y - pointF.y;
        if (f == 0.0f && f2 == 0.0f) {
            pointF4.x = pointF.x;
            pointF4.y = pointF.y;
        } else {
            float f3 = (((pointF3.x - pointF.x) * f) + ((pointF3.y - pointF.y) * f2)) / ((f * f) + (f2 * f2));
            pointF4.x = (f * f3) + pointF.x;
            pointF4.y = pointF.y + (f2 * f3);
        }
        return pointF4;
    }

    public static PointF a(PointF pointF, PointF pointF2, PointF pointF3, double d) {
        float f;
        float f2;
        if (pointF.x <= pointF3.x || pointF2.x <= pointF3.x) {
            if (pointF.x < pointF2.x) {
                f = pointF.x;
                f2 = pointF2.x;
            } else {
                f = pointF2.x;
                f2 = pointF.x;
            }
        } else if (pointF.x > pointF2.x) {
            f = pointF.x;
            f2 = pointF2.x;
        } else {
            f = pointF2.x;
            f2 = pointF.x;
        }
        PointF a2 = a(pointF, pointF2, pointF3, new PointF(f, a(d, f - pointF3.x) + pointF3.y));
        return a2 != null ? a2 : a(pointF, pointF2, pointF3, new PointF(f2, a(d, f2 - pointF3.x) + pointF3.y));
    }

    public static PointF a(PointF pointF, PointF pointF2, PointF pointF3, PointF pointF4) {
        PointF pointF5 = new PointF();
        float f = pointF2.y - pointF.y;
        float f2 = pointF.x - pointF2.x;
        float f3 = (pointF2.x * pointF.y) - (pointF.x * pointF2.y);
        float f4 = pointF4.y - pointF3.y;
        float f5 = pointF3.x - pointF4.x;
        float f6 = (pointF4.x * pointF3.y) - (pointF3.x * pointF4.y);
        float f7 = (f * f5) - (f4 * f2);
        if (f7 == 0.0f) {
            return null;
        }
        pointF5.x = ((f2 * f6) - (f5 * f3)) / f7;
        pointF5.y = ((f4 * f3) - (f * f6)) / f7;
        return pointF5;
    }

    public static PointF a(RectF rectF, float f, float f2) {
        PointF pointF = new PointF();
        if (rectF.width() <= f) {
            pointF.x = (f / 2.0f) - (rectF.width() / 2.0f);
        } else if (rectF.left > 0.0f) {
            pointF.x = 0.0f;
        } else if (rectF.left + rectF.width() < f) {
            pointF.x = f - rectF.width();
        } else {
            pointF.x = rectF.left;
        }
        if (rectF.height() <= f2) {
            pointF.y = (f2 / 2.0f) - (rectF.height() / 2.0f);
        } else if (rectF.top > 0.0f) {
            pointF.y = 0.0f;
        } else if (rectF.top + rectF.height() < f2) {
            pointF.y = f2 - rectF.height();
        } else {
            pointF.y = rectF.top;
        }
        return pointF;
    }

    public static RectF a(RectF rectF, float f) {
        return new RectF(rectF.left - f, rectF.top - f, rectF.right + f, rectF.bottom + f);
    }

    public static RectF a(RectF rectF, PointF pointF, PointF pointF2, float f) {
        float f2 = rectF.right - rectF.left;
        float f3 = rectF.bottom - rectF.top;
        float f4 = (pointF.x - rectF.left) / f2;
        float f5 = (pointF.y - rectF.top) / f3;
        float f6 = f2 * f;
        float f7 = f3 * f;
        float f8 = pointF2.x - (f4 * f6);
        float f9 = pointF2.y - (f5 * f7);
        return new RectF(f8, f9, f6 + f8, f7 + f9);
    }

    public static int b(int i) {
        return (i >> 16) & 255;
    }

    public static PointF b(float f, float f2, float f3, float f4) {
        return new PointF(((f3 - f) / 2.0f) + f, ((f4 - f2) / 2.0f) + f2);
    }

    public static PointF b(PointF pointF, PointF pointF2) {
        float f = pointF2.x - pointF.x;
        float f2 = pointF2.y - pointF.y;
        PointF pointF3 = new PointF();
        pointF3.x = (f * 0.5f) + pointF.x;
        pointF3.y = pointF.y + (f2 * 0.5f);
        return pointF3;
    }

    public static PointF b(RectF rectF, RectF rectF2) {
        PointF pointF = new PointF();
        if (rectF.width() <= rectF2.width()) {
            pointF.x = rectF2.centerX() - (rectF.width() / 2.0f);
        } else if (rectF.left > rectF2.left) {
            pointF.x = rectF2.left;
        } else if (rectF.right < rectF2.right) {
            pointF.x = rectF2.right - rectF.width();
        } else {
            pointF.x = rectF.left;
        }
        if (rectF.height() <= rectF2.height()) {
            pointF.y = rectF2.centerY() - (rectF.height() / 2.0f);
        } else if (rectF.top > rectF2.top) {
            pointF.y = rectF2.top;
        } else if (rectF.bottom < rectF2.bottom) {
            pointF.y = rectF2.bottom - rectF.height();
        } else {
            pointF.y = rectF.top;
        }
        return pointF;
    }

    public static RectF b(RectF rectF, float f) {
        float width = ((rectF.width() * f) - rectF.width()) / 2.0f;
        float height = ((rectF.height() * f) - rectF.height()) / 2.0f;
        return new RectF(rectF.left - width, rectF.top - height, width + rectF.right, height + rectF.bottom);
    }

    public static float c(RectF rectF, RectF rectF2) {
        return rectF2.width() / rectF.width() > rectF2.height() / rectF.height() ? rectF2.height() / rectF.height() : rectF2.width() / rectF.width();
    }

    public static int c(int i) {
        return (i >> 8) & 255;
    }

    public static void c(RectF rectF, float f) {
        rectF.left *= f;
        rectF.top *= f;
        rectF.right *= f;
        rectF.bottom *= f;
    }

    public static int d(int i) {
        return i & 255;
    }
}
