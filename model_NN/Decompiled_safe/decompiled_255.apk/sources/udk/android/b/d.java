package udk.android.b;

import android.graphics.drawable.Drawable;
import android.view.View;

public final class d {
    public static void a(View view, float f) {
        view.post(new g(f, view));
    }

    public static void a(View view, int i, int i2) {
        Drawable background = view.getBackground();
        view.setBackgroundColor(i);
        view.postDelayed(new h(view, background), (long) i2);
    }
}
