package udk.android.b;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class l {
    private static final SimpleDateFormat a = new SimpleDateFormat("'D:'yyyyMMddHHmmssZ");
    private static final SimpleDateFormat b = new SimpleDateFormat("'D:'yyyyMMddHHmmss");

    public static Date a(String str) {
        return (str.indexOf(43) >= 0 || str.indexOf(45) >= 0) ? a.parse(str.replace("'", "")) : b.parse(str);
    }
}
