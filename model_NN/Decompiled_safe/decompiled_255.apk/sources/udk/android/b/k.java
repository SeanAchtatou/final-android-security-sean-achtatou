package udk.android.b;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;

public final class k {
    public static BitmapShader a(int i) {
        Bitmap createBitmap = Bitmap.createBitmap(i * 2, i * 2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        paint.setColor(-1);
        Paint paint2 = new Paint();
        paint2.setColor(-2236963);
        int i2 = 0;
        while (i2 < 2) {
            int i3 = 0;
            while (i3 < 2) {
                canvas.drawRect((float) (i3 * i), (float) (i2 * i), (float) ((i3 * i) + i), (float) ((i2 * i) + i), i2 != i3 ? paint : paint2);
                i3++;
            }
            i2++;
        }
        return new BitmapShader(createBitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
    }
}
