package udk.android.b;

import android.graphics.drawable.Drawable;
import android.view.View;

final class h implements Runnable {
    private final /* synthetic */ View a;
    private final /* synthetic */ Drawable b;

    h(View view, Drawable drawable) {
        this.a = view;
        this.b = drawable;
    }

    public final void run() {
        this.a.setBackgroundDrawable(this.b);
    }
}
