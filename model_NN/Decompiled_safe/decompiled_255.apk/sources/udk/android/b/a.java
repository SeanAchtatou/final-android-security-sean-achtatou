package udk.android.b;

import android.app.Activity;
import android.app.AlertDialog;

final class a implements Runnable {
    private final /* synthetic */ Activity a;
    private final /* synthetic */ String b;

    a(Activity activity, String str) {
        this.a = activity;
        this.b = str;
    }

    public final void run() {
        new AlertDialog.Builder(this.a).setMessage(this.b).setCancelable(false).setPositiveButton("OK", new e(this, this.a)).show();
    }
}
