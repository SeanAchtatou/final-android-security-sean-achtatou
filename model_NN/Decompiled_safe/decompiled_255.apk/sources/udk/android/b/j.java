package udk.android.b;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;
import com.unidocs.commonlib.util.b;
import udk.android.reader.b.c;

public final class j {
    public static void a(Activity activity, boolean z) {
        if (z) {
            activity.getWindow().setFlags(1024, 1024);
        } else {
            activity.getWindow().clearFlags(1024);
        }
    }

    public static void a(Context context, String str, String str2) {
        try {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (Exception e) {
            c.a("## THREATED ERROR " + e.getMessage(), e);
            try {
                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://" + str)));
            } catch (Exception e2) {
                c.a((Throwable) e2);
                if (b.a(str2)) {
                    Toast.makeText(context, str2, 0).show();
                }
            }
        }
    }

    public static boolean a(Context context, Intent intent) {
        return context.getPackageManager().queryIntentActivities(intent, 65536).size() > 0;
    }
}
