package udk.android.reader;

import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;
import udk.android.b.j;
import udk.android.b.m;
import udk.android.reader.a.a;
import udk.android.reader.b.b;
import udk.android.reader.pdf.PDF;

final class ab implements Runnable {
    private /* synthetic */ PDFReaderActivity a;

    ab(PDFReaderActivity pDFReaderActivity) {
        this.a = pDFReaderActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void run() {
        if (!PDF.a().okToCopy()) {
            Toast.makeText(this.a, b.m, 0).show();
            return;
        }
        String G = this.a.d.G();
        int a2 = m.a(this.a, 5);
        Intent intent = new Intent("colordict.intent.action.SEARCH");
        intent.putExtra("EXTRA_QUERY", G);
        intent.putExtra("EXTRA_FULLSCREEN", false);
        intent.putExtra("EXTRA_HEIGHT", this.a.d.getHeight() / 2);
        intent.putExtra("EXTRA_GRAVITY", 80);
        intent.putExtra("EXTRA_MARGIN_LEFT", a2);
        intent.putExtra("EXTRA_MARGIN_TOP", a2);
        intent.putExtra("EXTRA_MARGIN_RIGHT", a2);
        intent.putExtra("EXTRA_MARGIN_BOTTOM", a2);
        if (j.a(this.a, intent)) {
            this.a.startActivity(intent);
        } else {
            Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + "com.socialnmobile.colordict"));
            if (j.a(this.a, intent2)) {
                this.a.startActivity(intent2);
            } else {
                a.b(this.a, this.a.d, "http://market.android.com/details?id=com.socialnmobile.colordict");
            }
        }
        this.a.d.H();
    }
}
