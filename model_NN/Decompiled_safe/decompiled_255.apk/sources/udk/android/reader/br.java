package udk.android.reader;

import android.widget.Toast;
import udk.android.b.m;
import udk.android.reader.b.b;
import udk.android.reader.pdf.PDF;

final class br implements Runnable {
    private /* synthetic */ y a;

    br(y yVar) {
        this.a = yVar;
    }

    public final void run() {
        if (!PDF.a().okToCopy()) {
            Toast.makeText(this.a.a, b.m, 0).show();
            return;
        }
        m.a(this.a.a.d, this.a.a.d.G(), this.a.a.getString(C0000R.string.f168));
        this.a.a.d.H();
    }
}
