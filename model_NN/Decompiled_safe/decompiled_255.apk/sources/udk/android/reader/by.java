package udk.android.reader;

import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class by implements Runnable {
    private /* synthetic */ ContentsManagerActivity a;
    private final /* synthetic */ EditText b;

    by(ContentsManagerActivity contentsManagerActivity, EditText editText) {
        this.a = contentsManagerActivity;
        this.b = editText;
    }

    public final void run() {
        ((InputMethodManager) this.a.getSystemService("input_method")).showSoftInput(this.b, 1);
    }
}
