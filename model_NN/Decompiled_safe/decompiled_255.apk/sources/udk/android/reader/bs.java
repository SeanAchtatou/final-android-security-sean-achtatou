package udk.android.reader;

import android.content.DialogInterface;

final class bs implements DialogInterface.OnClickListener {
    private /* synthetic */ bt a;
    private final /* synthetic */ int b;

    bs(bt btVar, int i) {
        this.a = btVar;
        this.b = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.PDFView.a(int, boolean):void
     arg types: [int, int]
     candidates:
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.pdf.annotation.s, udk.android.reader.pdf.annotation.Annotation):java.lang.Runnable
      udk.android.reader.view.pdf.PDFView.a(android.content.Context, udk.android.reader.pdf.y):void
      udk.android.reader.view.pdf.PDFView.a(java.lang.String, java.lang.Runnable):void
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.view.pdf.PDFView, android.app.AlertDialog):void
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.view.pdf.PDFView, android.app.ProgressDialog):void
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.view.pdf.PDFView, android.view.View):void
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.view.pdf.PDFView, java.lang.Runnable):void
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.view.pdf.PDFView, boolean):void
      udk.android.reader.view.pdf.PDFView.a(float, float):void
      udk.android.reader.view.pdf.PDFView.a(int, int):void
      udk.android.reader.view.pdf.PDFView.a(android.view.MotionEvent, udk.android.reader.pdf.annotation.e):void
      udk.android.reader.view.pdf.PDFView.a(java.lang.String, java.lang.String):void
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.pdf.c, android.view.MotionEvent):void
      udk.android.reader.view.pdf.PDFView.a(int, boolean):void */
    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i != this.b) {
            int K = this.a.a.a.d.K();
            this.a.a.a.d.a(i, true);
            if (this.a.a.a.d.K() != K) {
                int z = this.a.a.a.d.z();
                if (z > 1) {
                    this.a.a.a.d.a(z - 1);
                    this.a.a.a.d.o();
                } else if (z < this.a.a.a.d.A()) {
                    this.a.a.a.d.a(z + 1);
                    this.a.a.a.d.n();
                }
            }
            this.a.a.a.n.dismiss();
        }
    }
}
