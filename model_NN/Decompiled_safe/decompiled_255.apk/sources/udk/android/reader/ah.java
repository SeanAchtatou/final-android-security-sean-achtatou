package udk.android.reader;

import udk.android.reader.view.pdf.PDFView;

final class ah implements Runnable {
    private /* synthetic */ PDFReaderActivity a;

    ah(PDFReaderActivity pDFReaderActivity) {
        this.a = pDFReaderActivity;
    }

    public final void run() {
        if (this.a.d.F() == PDFView.ViewMode.PDF) {
            this.a.d.a(PDFView.ViewMode.TEXTREFLOW);
        } else {
            this.a.d.a(PDFView.ViewMode.PDF);
        }
    }
}
