package udk.android.reader;

import java.io.File;
import java.io.FileFilter;

final class bu implements FileFilter {
    private /* synthetic */ ae a;

    bu(ae aeVar) {
        this.a = aeVar;
    }

    public final boolean accept(File file) {
        return !file.isDirectory() && file.getName().startsWith("updatemsg_");
    }
}
