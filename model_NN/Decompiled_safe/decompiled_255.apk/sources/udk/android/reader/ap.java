package udk.android.reader;

import android.app.AlertDialog;
import android.view.View;
import java.util.ArrayList;
import udk.android.reader.b.a;
import udk.android.reader.b.e;
import udk.android.reader.pdf.b.d;
import udk.android.reader.view.pdf.PDFView;
import udk.android.reader.view.pdf.dc;

final class ap implements View.OnClickListener {
    final /* synthetic */ PDFReaderActivity a;

    ap(PDFReaderActivity pDFReaderActivity) {
        this.a = pDFReaderActivity;
    }

    public final void onClick(View view) {
        ArrayList arrayList = new ArrayList();
        PDFView.ViewMode F = this.a.d.F();
        if (a.m && this.a.d.F() != PDFView.ViewMode.TEXTREFLOW) {
            arrayList.add(this.a.getString(C0000R.string.f1));
        }
        if (a.m && this.a.d.F() != PDFView.ViewMode.THUMBNAIL) {
            arrayList.add(this.a.getString(C0000R.string.f2));
        }
        if (this.a.d.F() != PDFView.ViewMode.PDF) {
            arrayList.add(this.a.getString(C0000R.string.f0));
        }
        arrayList.add(this.a.getString(C0000R.string.f3));
        if (F == PDFView.ViewMode.PDF) {
            if (a.l && !this.a.d.J()) {
                arrayList.add(this.a.getString(C0000R.string.f4));
            }
            if (e.n) {
                if (this.a.d.U().b()) {
                    arrayList.add(this.a.getString(C0000R.string.f8));
                } else if (!this.a.d.y()) {
                    arrayList.add(this.a.getString(C0000R.string.f7));
                }
            }
            if (e.o && this.a.d.A() > 1) {
                arrayList.add(this.a.getString(C0000R.string.f18));
            }
        } else if (F == PDFView.ViewMode.TEXTREFLOW) {
            if (a.ar) {
                arrayList.add(this.a.getString(C0000R.string.f5));
            } else {
                arrayList.add(this.a.getString(C0000R.string.f6));
            }
        }
        if (F == PDFView.ViewMode.PDF || F == PDFView.ViewMode.TEXTREFLOW) {
            if (a.n) {
                if (this.a.d.O()) {
                    arrayList.add(this.a.getString(C0000R.string.f10));
                } else {
                    arrayList.add(this.a.getString(C0000R.string.f9));
                }
            }
            if (dc.a().b()) {
                arrayList.add(this.a.getString(C0000R.string.f13));
            } else {
                arrayList.add(this.a.getString(C0000R.string.f12));
            }
        }
        if (e.p) {
            arrayList.add(this.a.getString(C0000R.string.f14));
        }
        if (a.g) {
            if (!a.g ? false : d.a().b()) {
                arrayList.add(this.a.getString(C0000R.string.f15));
            }
        }
        if (e.q) {
            arrayList.add(this.a.getString(C0000R.string.f16));
        }
        if (e.r) {
            arrayList.add(this.a.getString(C0000R.string.f17));
        }
        if (e.s) {
            arrayList.add(this.a.getString(C0000R.string.about));
        }
        String[] strArr = (String[]) arrayList.toArray(new String[0]);
        new AlertDialog.Builder(this.a).setTitle("更多").setItems(strArr, new bt(this, strArr)).show();
    }
}
