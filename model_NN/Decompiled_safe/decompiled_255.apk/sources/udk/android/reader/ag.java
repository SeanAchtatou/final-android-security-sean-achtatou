package udk.android.reader;

import android.view.MotionEvent;
import android.view.View;
import udk.android.reader.b.e;

final class ag implements View.OnTouchListener {
    private /* synthetic */ PDFReaderActivity a;
    private final /* synthetic */ Runnable b;
    private final /* synthetic */ Runnable c;

    ag(PDFReaderActivity pDFReaderActivity, Runnable runnable, Runnable runnable2) {
        this.a = pDFReaderActivity;
        this.b = runnable;
        this.c = runnable2;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (e.z && motionEvent.getAction() == 1) {
            if (((double) motionEvent.getX()) < ((double) this.a.f.getWidth()) * 0.3d) {
                this.b.run();
            } else if (((double) motionEvent.getX()) > ((double) this.a.f.getWidth()) * 0.7d) {
                this.c.run();
            }
        }
        return true;
    }
}
