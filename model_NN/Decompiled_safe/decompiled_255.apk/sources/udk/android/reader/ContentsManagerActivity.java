package udk.android.reader;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import udk.android.reader.b.a;
import udk.android.reader.b.e;
import udk.android.reader.contents.ai;
import udk.android.reader.contents.an;
import udk.android.reader.contents.b;
import udk.android.reader.contents.l;
import udk.android.reader.contents.m;
import udk.android.reader.view.contents.c;
import udk.android.reader.view.contents.j;

public class ContentsManagerActivity extends Activity implements m {
    /* access modifiers changed from: private */
    public an a;
    /* access modifiers changed from: private */
    public c b;
    private View c;
    private ListView d;
    private View e;

    /* access modifiers changed from: private */
    public void a(EditText editText) {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
        ArrayList arrayList = new ArrayList();
        b.a().a(this.d, a.b(this), arrayList, (File[]) null, editText.getText().toString(), (l) null);
        if (com.unidocs.commonlib.util.b.b((Collection) arrayList)) {
            new AlertDialog.Builder(this).setTitle((int) C0000R.string.f49).setMessage((int) C0000R.string.f149).setPositiveButton((int) C0000R.string.f50, (DialogInterface.OnClickListener) null).show();
            return;
        }
        ExpandableListView expandableListView = new ExpandableListView(this);
        expandableListView.setCacheColorHint(0);
        expandableListView.setAdapter(new j(arrayList));
        for (int i = 0; i < arrayList.size(); i++) {
            expandableListView.expandGroup(i);
        }
        new AlertDialog.Builder(this).setTitle((int) C0000R.string.f150).setNegativeButton((int) C0000R.string.f54, (DialogInterface.OnClickListener) null).setView(expandableListView).show();
    }

    /* access modifiers changed from: private */
    public void b() {
        View findViewById = findViewById(C0000R.id.option_menubar);
        if (findViewById.getVisibility() == 0) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this, C0000R.anim.slide_down_out);
            loadAnimation.setAnimationListener(new cc(this, findViewById));
            findViewById.startAnimation(loadAnimation);
            return;
        }
        findViewById.setVisibility(0);
    }

    public final void a() {
        this.d.post(new bb(this));
    }

    public final void a(udk.android.reader.contents.c cVar) {
        this.d.post(new ba(this, cVar));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void
     arg types: [udk.android.reader.ContentsManagerActivity, int]
     candidates:
      udk.android.reader.ApplicationActivity.a(udk.android.reader.ApplicationActivity, java.io.File):void
      udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void */
    public void onBackPressed() {
        if (this.a.d(this)) {
            return;
        }
        if (this.a.d()) {
            this.a.a(false);
        } else if (ApplicationActivity.a(this)) {
            ApplicationActivity.a((Activity) this, false);
        } else {
            finish();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        udk.android.reader.b.c.a("## ON CONTENTS MANAGER CREATED");
        this.b = new c(this);
        this.a = an.c();
        this.a.a((m) this);
        this.c = View.inflate(this, C0000R.layout.contents_manager, null);
        setContentView(this.c);
        findViewById(C0000R.id.btn_menu_align).setOnClickListener(new bx(this));
        findViewById(C0000R.id.btn_menu_edit).setOnClickListener(new ca(this));
        if (e.s) {
            findViewById(C0000R.id.btn_menu_about).setOnClickListener(new bz(this));
        } else {
            findViewById(C0000R.id.btn_menu_about).setVisibility(8);
            findViewById(C0000R.id.bg_menu_about).setVisibility(8);
        }
        findViewById(C0000R.id.btn_edit_createdir).setOnClickListener(new cb(this));
        findViewById(C0000R.id.btn_edit_copy).setOnClickListener(new cd(this));
        findViewById(C0000R.id.btn_edit_cut).setOnClickListener(new be(this));
        findViewById(C0000R.id.btn_edit_paste).setOnClickListener(new bf(this));
        findViewById(C0000R.id.btn_edit_delete).setOnClickListener(new bg(this));
        this.d = (ListView) findViewById(C0000R.id.list);
        this.d.setDivider(getResources().getDrawable(C0000R.drawable.line));
        this.e = View.inflate(this, C0000R.layout.contents_search, null);
        EditText editText = (EditText) this.e.findViewById(C0000R.id.search_input);
        View findViewById = this.e.findViewById(C0000R.id.btn_search);
        editText.setInputType(1);
        editText.setImeOptions(3);
        editText.setOnEditorActionListener(new bw(this, editText));
        editText.setSingleLine();
        findViewById.setOnClickListener(new bv(this, editText));
        this.d.addHeaderView(this.e);
        this.d.setAdapter((ListAdapter) this.b);
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        if (view.getId() == C0000R.id.content) {
            ai aiVar = (ai) this.d.getItemAtPosition(this.d.getPositionForView(view));
            if (aiVar.f() != 11 || !aiVar.c().equals("..")) {
                contextMenu.setHeaderTitle((int) C0000R.string.f89);
                if (aiVar.f() == 21) {
                    contextMenu.add(0, 1, 0, getString(C0000R.string.f91_)).setOnMenuItemClickListener(new bh(this, aiVar));
                }
                contextMenu.add(0, 2, 0, getString(C0000R.string.f96__)).setOnMenuItemClickListener(new bi(this, aiVar));
                contextMenu.add(0, 3, 0, getString(C0000R.string.f93_)).setOnMenuItemClickListener(new bj(this, aiVar));
                if (aiVar.f() == 21 && e.y) {
                    contextMenu.add(0, 4, 0, getString(C0000R.string.f163)).setOnMenuItemClickListener(new bc(this, aiVar));
                }
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.a.b((m) this);
        super.onDestroy();
        udk.android.reader.b.c.a("## ContentsManager DESTROYED");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void
     arg types: [udk.android.reader.ContentsManagerActivity, int]
     candidates:
      udk.android.reader.ApplicationActivity.a(udk.android.reader.ApplicationActivity, java.io.File):void
      udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void */
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (ApplicationActivity.a(this)) {
            ApplicationActivity.a((Activity) this, false);
            return true;
        }
        b();
        return true;
    }

    public boolean onSearchRequested() {
        EditText editText = (EditText) this.e.findViewById(C0000R.id.search_input);
        if (com.unidocs.commonlib.util.b.a(editText.getText().toString())) {
            a(editText);
            return true;
        }
        editText.requestFocus();
        editText.postDelayed(new by(this, editText), 100);
        return true;
    }
}
