package udk.android.reader;

import udk.android.reader.contents.c;

final class ba implements Runnable {
    private /* synthetic */ ContentsManagerActivity a;
    private final /* synthetic */ c b;

    ba(ContentsManagerActivity contentsManagerActivity, c cVar) {
        this.a = contentsManagerActivity;
        this.b = cVar;
    }

    public final void run() {
        this.a.findViewById(C0000R.id.edit_menubar).setVisibility(this.b.a ? 0 : 8);
        this.a.b.notifyDataSetInvalidated();
    }
}
