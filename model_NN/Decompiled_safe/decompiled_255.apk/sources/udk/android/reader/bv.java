package udk.android.reader;

import android.view.View;
import android.widget.EditText;
import com.unidocs.commonlib.util.b;

final class bv implements View.OnClickListener {
    final /* synthetic */ ContentsManagerActivity a;
    private final /* synthetic */ EditText b;

    bv(ContentsManagerActivity contentsManagerActivity, EditText editText) {
        this.a = contentsManagerActivity;
        this.b = editText;
    }

    public final void onClick(View view) {
        if (b.a(this.b.getText().toString())) {
            this.a.a(this.b);
            return;
        }
        this.b.requestFocus();
        this.b.postDelayed(new a(this, this.b), 100);
    }
}
