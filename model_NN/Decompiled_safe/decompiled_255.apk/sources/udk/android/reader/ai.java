package udk.android.reader;

import android.content.Intent;

final class ai implements Runnable {
    private /* synthetic */ PDFReaderActivity a;

    ai(PDFReaderActivity pDFReaderActivity) {
        this.a = pDFReaderActivity;
    }

    public final void run() {
        this.a.a();
        if (!this.a.getIntent().getBooleanExtra("calledFromAppInner", false)) {
            this.a.startActivity(new Intent(this.a, ApplicationActivity.class));
        }
    }
}
