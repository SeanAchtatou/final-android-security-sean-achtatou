package udk.android.reader;

import android.app.ActivityManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.view.Display;
import com.unidocs.commonlib.util.i;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import udk.android.b.m;
import udk.android.reader.b.c;

final class bk implements DialogInterface.OnClickListener {
    private /* synthetic */ v a;
    private final /* synthetic */ Throwable b;

    bk(v vVar, Throwable th) {
        this.a = vVar;
        this.b = th;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.PrintWriter.<init>(java.io.Writer, boolean):void}
     arg types: [java.io.StringWriter, int]
     candidates:
      ClspMth{java.io.PrintWriter.<init>(java.io.File, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
      ClspMth{java.io.PrintWriter.<init>(java.io.OutputStream, boolean):void}
      ClspMth{java.io.PrintWriter.<init>(java.lang.String, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
      ClspMth{java.io.PrintWriter.<init>(java.io.Writer, boolean):void} */
    public final void onClick(DialogInterface dialogInterface, int i) {
        PrintWriter printWriter;
        Throwable th;
        try {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter2 = new PrintWriter((Writer) stringWriter, true);
            try {
                this.b.printStackTrace(printWriter2);
                printWriter2.flush();
                Display defaultDisplay = this.a.a.a.getWindowManager().getDefaultDisplay();
                ActivityManager activityManager = (ActivityManager) this.a.a.a.getSystemService("activity");
                ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
                activityManager.getMemoryInfo(memoryInfo);
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("## ezPDF Reader VERSION : " + m.a(this.a.a.a) + "\n");
                stringBuffer.append("## SYSTEM AVAIL MEMORY : " + memoryInfo.availMem + "\n");
                stringBuffer.append("## MEMORY : " + Runtime.getRuntime().freeMemory() + " / " + Runtime.getRuntime().maxMemory() + "\n");
                stringBuffer.append("## MEMORY CLASS : " + activityManager.getMemoryClass() + "\n");
                stringBuffer.append("## RESOLUTION : " + defaultDisplay.getWidth() + " * " + defaultDisplay.getHeight() + "\n");
                stringBuffer.append("## DEVICE SDK VERSION : " + Build.VERSION.SDK + "\n");
                stringBuffer.append("## BOARD : " + Build.BOARD + "\n");
                stringBuffer.append("## BRAND : " + Build.BRAND + "\n");
                stringBuffer.append("## CPU_ABI : " + Build.CPU_ABI + "\n");
                stringBuffer.append("## DEVICE : " + Build.DEVICE + "\n");
                stringBuffer.append("## MANUFACTURER : " + Build.MANUFACTURER + "\n");
                stringBuffer.append("## MODEL : " + Build.MODEL + "\n");
                stringBuffer.append("## PRODUCT : " + Build.PRODUCT + "\n");
                stringBuffer.append("\n\n");
                stringBuffer.append(stringWriter.toString());
                PDFReaderActivity pDFReaderActivity = this.a.a.a;
                String[] strArr = {"android@unidocs.com"};
                String stringBuffer2 = stringBuffer.toString();
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("message/rfc822");
                intent.putExtra("android.intent.extra.EMAIL", strArr);
                intent.putExtra("android.intent.extra.SUBJECT", "Error Report");
                intent.putExtra("android.intent.extra.TEXT", stringBuffer2);
                pDFReaderActivity.startActivity(intent);
            } catch (Exception e) {
                c.a((Throwable) e);
            } catch (Throwable th2) {
                th = th2;
                printWriter = printWriter2;
            }
            this.a.a.a.finish();
            i.a(printWriter2);
        } catch (Throwable th3) {
            Throwable th4 = th3;
            printWriter = null;
            th = th4;
            i.a(printWriter);
            throw th;
        }
    }
}
