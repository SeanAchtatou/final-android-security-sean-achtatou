package udk.android.reader.jni;

import com.unidocs.commonlib.util.i;
import java.io.InputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import udk.android.reader.b.c;

public class HttpConnection {
    private InputStream is;

    public void close() {
        i.a(this.is);
    }

    public long open(String str) {
        try {
            HttpResponse execute = new DefaultHttpClient().execute(new HttpGet(str));
            HttpEntity entity = execute.getEntity();
            this.is = entity.getContent();
            long contentLength = entity.getContentLength();
            Header firstHeader = execute.getFirstHeader("Accept-Ranges");
            return (firstHeader == null || !"bytes".equals(firstHeader.getValue())) ? contentLength * -1 : contentLength;
        } catch (Exception e) {
            c.a((Throwable) e);
            return 0;
        }
    }

    public long open(String str, int i, int i2) {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(str);
            httpGet.setHeader("Range", "bytes=" + i + "-" + i2);
            HttpEntity entity = defaultHttpClient.execute(httpGet).getEntity();
            this.is = entity.getContent();
            return entity.getContentLength();
        } catch (Exception e) {
            c.a((Throwable) e);
            return 0;
        }
    }

    public int read(byte[] bArr) {
        int i = 0;
        do {
            try {
                int read = this.is.read(bArr, i, bArr.length - i);
                if (read < 0) {
                    return i;
                }
                i += read;
            } catch (Exception e) {
                Exception exc = e;
                int i2 = i;
                c.a((Throwable) exc);
                return i2;
            }
        } while (i < bArr.length);
        return i;
    }
}
