package udk.android.reader;

import android.app.AlertDialog;
import android.content.DialogInterface;
import udk.android.reader.b.e;
import udk.android.reader.pdf.c;

final class ay implements Runnable {
    final /* synthetic */ PDFReaderActivity a;
    private final /* synthetic */ c b;

    ay(PDFReaderActivity pDFReaderActivity, c cVar) {
        this.a = pDFReaderActivity;
        this.b = cVar;
    }

    public final void run() {
        if (e.D) {
            new AlertDialog.Builder(this.a).setTitle((int) C0000R.string.f49).setMessage(this.a.getString(C0000R.string.f159X, new Object[]{Integer.valueOf(this.b.g())})).setPositiveButton((int) C0000R.string.f50, new ar(this, this.b)).setNegativeButton((int) C0000R.string.f53, (DialogInterface.OnClickListener) null).show();
            return;
        }
        this.a.d.b(this.b.g());
    }
}
