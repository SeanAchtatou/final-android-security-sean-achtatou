package udk.android.reader;

import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;

final class bw implements TextView.OnEditorActionListener {
    private /* synthetic */ ContentsManagerActivity a;
    private final /* synthetic */ EditText b;

    bw(ContentsManagerActivity contentsManagerActivity, EditText editText) {
        this.a = contentsManagerActivity;
        this.b = editText;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (keyEvent != null && keyEvent.getAction() != 1) {
            return true;
        }
        this.a.a(this.b);
        return true;
    }
}
