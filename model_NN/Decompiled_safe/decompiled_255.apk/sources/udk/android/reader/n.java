package udk.android.reader;

import android.os.Environment;
import android.view.View;
import udk.android.a.b;
import udk.android.reader.b.e;

final class n implements View.OnClickListener {
    final /* synthetic */ ApplicationActivity a;

    n(ApplicationActivity applicationActivity) {
        this.a = applicationActivity;
    }

    public final void onClick(View view) {
        b bVar = new b(this.a, (this.a.b == null || this.a.b.getParentFile() == null) ? Environment.getExternalStorageDirectory() : this.a.b.getParentFile(), e.w ? Environment.getExternalStorageDirectory() : null, new String[]{"pdf"}, false);
        bVar.setTitle((int) C0000R.string.f99__);
        bVar.setButton(this.a.getString(C0000R.string.f50), new k(this, bVar));
        bVar.show();
    }
}
