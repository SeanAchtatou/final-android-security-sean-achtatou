package udk.android.reader;

import android.app.Activity;
import android.app.TabActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabWidget;
import java.io.File;
import udk.android.b.d;
import udk.android.reader.b.a;
import udk.android.reader.b.c;
import udk.android.reader.contents.an;
import udk.android.reader.contents.b;

public class ApplicationActivity extends TabActivity {
    /* access modifiers changed from: private */
    public View a;
    /* access modifiers changed from: private */
    public File b;

    /* access modifiers changed from: private */
    public void a() {
        String currentTabTag = getTabHost().getCurrentTabTag();
        if ("contents_manager".equals(currentTabTag)) {
            an.c().f(this);
        } else if ("all_pdf_list".equals(currentTabTag)) {
            b.a().a(this.a);
        }
    }

    public static void a(Activity activity, boolean z) {
        ApplicationActivity applicationActivity = null;
        if (activity instanceof ApplicationActivity) {
            applicationActivity = (ApplicationActivity) activity;
        } else if (activity.getParent() instanceof ApplicationActivity) {
            applicationActivity = (ApplicationActivity) activity.getParent();
        }
        if (applicationActivity != null) {
            applicationActivity.findViewById(C0000R.id.app_title).setVisibility(z ? 8 : 0);
            applicationActivity.getTabWidget().setVisibility(z ? 8 : 0);
        }
    }

    public static boolean a(Activity activity) {
        ApplicationActivity applicationActivity = null;
        if (activity instanceof ApplicationActivity) {
            applicationActivity = (ApplicationActivity) activity;
        } else if (activity.getParent() instanceof ApplicationActivity) {
            applicationActivity = (ApplicationActivity) activity.getParent();
        }
        if (applicationActivity == null) {
            return false;
        }
        return applicationActivity.findViewById(C0000R.id.app_title).getVisibility() != 0;
    }

    /* access modifiers changed from: private */
    public void b() {
        TabHost tabHost = getTabHost();
        TabWidget tabWidget = tabHost.getTabWidget();
        int tabCount = tabWidget.getTabCount();
        if ("contents_manager".equals(tabHost.getCurrentTabTag())) {
            findViewById(C0000R.id.app_title).findViewById(C0000R.id.btn_edit).setVisibility(0);
            findViewById(C0000R.id.app_title).findViewById(C0000R.id.btn_import).setVisibility(0);
            File b2 = a.b(this);
            if (b2.exists()) {
                an.c().a(this, b2.getAbsolutePath());
            }
        } else {
            findViewById(C0000R.id.app_title).findViewById(C0000R.id.btn_edit).setVisibility(8);
            findViewById(C0000R.id.app_title).findViewById(C0000R.id.btn_import).setVisibility(8);
            an.c().a(false);
        }
        if ("all_pdf_list".equals(tabHost.getCurrentTabTag())) {
            findViewById(C0000R.id.app_title).findViewById(C0000R.id.btn_refresh).setVisibility(0);
        } else {
            findViewById(C0000R.id.app_title).findViewById(C0000R.id.btn_refresh).setVisibility(8);
        }
        for (int i = 0; i < tabCount; i++) {
            View childTabViewAt = tabWidget.getChildTabViewAt(i);
            if (tabHost.getCurrentTab() == i) {
                childTabViewAt.findViewById(C0000R.id.bg).setVisibility(8);
                childTabViewAt.findViewById(C0000R.id.bg_selected).setVisibility(0);
                d.a(childTabViewAt.findViewById(C0000R.id.title), 1.0f);
            } else {
                childTabViewAt.findViewById(C0000R.id.bg).setVisibility(0);
                childTabViewAt.findViewById(C0000R.id.bg_selected).setVisibility(8);
                d.a(childTabViewAt.findViewById(C0000R.id.title), 0.5f);
            }
        }
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: android.graphics.drawable.PaintDrawable.setShape(android.graphics.drawable.shapes.Shape):void in method: udk.android.reader.ApplicationActivity.onCreate(android.os.Bundle):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:59)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: android.graphics.drawable.PaintDrawable.setShape(android.graphics.drawable.shapes.Shape):void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:540)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 6 more
        */
    public void onCreate(android.os.Bundle r1) {
        /*
            r7 = this;
            r6 = 2131427410(0x7f0b0052, float:1.8476435E38)
            r5 = 0
            r4 = 0
            udk.android.reader.a.a.a(r7)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "## ON CREATE Application : "
            r0.<init>(r1)
            int r1 = android.os.Process.myPid()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            udk.android.reader.b.c.a(r0)
            super.onCreate(r8)
            udk.android.reader.b.e.c(r7)
            udk.android.reader.contents.an r0 = udk.android.reader.contents.an.c()
            java.io.File r1 = udk.android.reader.b.a.b(r7)
            r0.a(r1)
            r0 = 2130903042(0x7f030002, float:1.741289E38)
            android.view.View r0 = android.view.View.inflate(r7, r0, r4)
            r7.a = r0
            android.view.View r0 = r7.a
            r7.setContentView(r0)
            android.graphics.drawable.PaintDrawable r0 = new android.graphics.drawable.PaintDrawable
            r0.<init>()
            android.graphics.drawable.shapes.RectShape r1 = new android.graphics.drawable.shapes.RectShape
            r1.<init>()
            r0.setShape(r1)
            udk.android.reader.p r1 = new udk.android.reader.p
            r1.<init>(r7)
            r0.setShaderFactory(r1)
            r1 = 2131427333(0x7f0b0005, float:1.847628E38)
            android.view.View r1 = r7.findViewById(r1)
            r1.setBackgroundDrawable(r0)
            android.widget.TabHost r0 = r7.getTabHost()
            udk.android.reader.m r1 = new udk.android.reader.m
            r1.<init>(r7)
            r0.setOnTabChangedListener(r1)
            java.lang.String r1 = "contents_manager"
            android.widget.TabHost$TabSpec r1 = r0.newTabSpec(r1)
            r2 = 2130903064(0x7f030018, float:1.7412935E38)
            android.view.View r2 = android.view.View.inflate(r7, r2, r4)
            android.widget.TabHost$TabSpec r1 = r1.setIndicator(r2)
            android.content.Intent r2 = new android.content.Intent
            java.lang.Class<udk.android.reader.ContentsManagerActivity> r3 = udk.android.reader.ContentsManagerActivity.class
            r2.<init>(r7, r3)
            android.widget.TabHost$TabSpec r1 = r1.setContent(r2)
            r0.addTab(r1)
            java.lang.String r1 = "recent_pdf_list"
            android.widget.TabHost$TabSpec r1 = r0.newTabSpec(r1)
            r2 = 2130903065(0x7f030019, float:1.7412937E38)
            android.view.View r2 = android.view.View.inflate(r7, r2, r4)
            android.widget.TabHost$TabSpec r1 = r1.setIndicator(r2)
            android.content.Intent r2 = new android.content.Intent
            java.lang.Class<udk.android.reader.RecentPDFListActivity> r3 = udk.android.reader.RecentPDFListActivity.class
            r2.<init>(r7, r3)
            android.widget.TabHost$TabSpec r1 = r1.setContent(r2)
            r0.addTab(r1)
            boolean r1 = udk.android.reader.b.e.a
            if (r1 == 0) goto L_0x00c9
            java.lang.String r1 = "all_pdf_list"
            android.widget.TabHost$TabSpec r1 = r0.newTabSpec(r1)
            r2 = 2130903063(0x7f030017, float:1.7412933E38)
            android.view.View r2 = android.view.View.inflate(r7, r2, r4)
            android.widget.TabHost$TabSpec r1 = r1.setIndicator(r2)
            android.content.Intent r2 = new android.content.Intent
            java.lang.Class<udk.android.reader.AllPDFListActivity> r3 = udk.android.reader.AllPDFListActivity.class
            r2.<init>(r7, r3)
            android.widget.TabHost$TabSpec r1 = r1.setContent(r2)
            r0.addTab(r1)
        L_0x00c9:
            boolean r1 = udk.android.reader.b.e.e
            if (r1 == 0) goto L_0x00ec
            java.lang.String r1 = "web_contents_manager"
            android.widget.TabHost$TabSpec r1 = r0.newTabSpec(r1)
            r2 = 2130903066(0x7f03001a, float:1.741294E38)
            android.view.View r2 = android.view.View.inflate(r7, r2, r4)
            android.widget.TabHost$TabSpec r1 = r1.setIndicator(r2)
            android.content.Intent r2 = new android.content.Intent
            java.lang.Class<udk.android.reader.WebContentsManagerActivity> r3 = udk.android.reader.WebContentsManagerActivity.class
            r2.<init>(r7, r3)
            android.widget.TabHost$TabSpec r1 = r1.setContent(r2)
            r0.addTab(r1)
        L_0x00ec:
            java.lang.String r1 = "contents_manager"
            java.lang.String r2 = r0.getCurrentTabTag()
            boolean r2 = r2.equals(r1)
            if (r2 != 0) goto L_0x01a1
            r0.setCurrentTabByTag(r1)
        L_0x00fb:
            r0 = 2131427406(0x7f0b004e, float:1.8476427E38)
            android.view.View r1 = r7.findViewById(r0)
            r1.setVisibility(r5)
            android.view.View r0 = r1.findViewById(r6)
            r2 = 8
            r0.setVisibility(r2)
            r0 = 2131427409(0x7f0b0051, float:1.8476433E38)
            android.view.View r0 = r1.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r0.setVisibility(r5)
            r2 = 2130837572(0x7f020044, float:1.7280102E38)
            r0.setImageResource(r2)
            r0 = 2131427407(0x7f0b004f, float:1.847643E38)
            android.view.View r0 = r1.findViewById(r0)
            udk.android.reader.o r2 = new udk.android.reader.o
            r2.<init>(r7)
            r0.setOnTouchListener(r2)
            r0 = 2131427413(0x7f0b0055, float:1.8476442E38)
            android.view.View r0 = r1.findViewById(r0)
            r0.setVisibility(r5)
            udk.android.reader.n r2 = new udk.android.reader.n
            r2.<init>(r7)
            r0.setOnClickListener(r2)
            r0 = 2131427414(0x7f0b0056, float:1.8476444E38)
            android.view.View r0 = r1.findViewById(r0)
            udk.android.reader.r r2 = new udk.android.reader.r
            r2.<init>(r7)
            r0.setOnClickListener(r2)
            r0 = 2131427415(0x7f0b0057, float:1.8476446E38)
            android.view.View r0 = r1.findViewById(r0)
            r0.setVisibility(r5)
            udk.android.reader.q r2 = new udk.android.reader.q
            r2.<init>(r7)
            r0.setOnClickListener(r2)
            r0 = 2131427412(0x7f0b0054, float:1.847644E38)
            android.view.View r0 = r1.findViewById(r0)
            udk.android.reader.s r2 = new udk.android.reader.s
            r2.<init>(r7)
            r0.setOnClickListener(r2)
            java.lang.String r0 = r7.getPackageName()
            java.lang.String r2 = "udk.android.reader"
            boolean r0 = r0.startsWith(r2)
            if (r0 == 0) goto L_0x0188
            android.view.View r0 = r1.findViewById(r6)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.String r1 = "ezPDF Reader"
            r0.setText(r1)
        L_0x0188:
            android.view.View r0 = r7.a
            r1 = 2131165277(0x7f07005d, float:1.7944767E38)
            java.lang.String r1 = r7.getString(r1)
            udk.android.reader.l r2 = new udk.android.reader.l
            r2.<init>(r7)
            r3 = 2131165372(0x7f0700bc, float:1.794496E38)
            java.lang.String r3 = r7.getString(r3)
            udk.android.reader.view.pdf.PDFView.a(r7, r0, r1, r2, r3)
            return
        L_0x01a1:
            r7.b()
            goto L_0x00fb
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.ApplicationActivity.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        b.a().a(this);
        System.exit(0);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        c.a("## ApplicationActivity - ON RESTORE INSTANCE STATE");
        try {
            super.onRestoreInstanceState(bundle);
        } catch (Exception e) {
            c.a((Throwable) e);
            a();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
