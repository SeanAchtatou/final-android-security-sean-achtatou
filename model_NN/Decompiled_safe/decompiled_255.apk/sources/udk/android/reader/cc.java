package udk.android.reader;

import android.view.View;
import android.view.animation.Animation;

final class cc implements Animation.AnimationListener {
    private /* synthetic */ ContentsManagerActivity a;
    private final /* synthetic */ View b;

    cc(ContentsManagerActivity contentsManagerActivity, View view) {
        this.a = contentsManagerActivity;
        this.b = view;
    }

    public final void onAnimationEnd(Animation animation) {
        this.b.setVisibility(8);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
