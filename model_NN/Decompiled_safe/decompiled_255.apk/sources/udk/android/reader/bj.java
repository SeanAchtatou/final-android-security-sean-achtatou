package udk.android.reader;

import android.view.MenuItem;
import java.io.File;
import udk.android.reader.contents.ai;

final class bj implements MenuItem.OnMenuItemClickListener {
    private /* synthetic */ ContentsManagerActivity a;
    private final /* synthetic */ ai b;

    bj(ContentsManagerActivity contentsManagerActivity, ai aiVar) {
        this.a = contentsManagerActivity;
        this.b = aiVar;
    }

    public final boolean onMenuItemClick(MenuItem menuItem) {
        this.a.a.a(this.a, new File(this.b.d()));
        return true;
    }
}
