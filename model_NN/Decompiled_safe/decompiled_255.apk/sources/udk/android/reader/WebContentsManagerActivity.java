package udk.android.reader;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import udk.android.reader.a.a;
import udk.android.reader.b.c;
import udk.android.reader.b.e;
import udk.android.reader.view.contents.web.g;

public class WebContentsManagerActivity extends Activity {
    private g a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void
     arg types: [udk.android.reader.WebContentsManagerActivity, int]
     candidates:
      udk.android.reader.ApplicationActivity.a(udk.android.reader.ApplicationActivity, java.io.File):void
      udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void */
    public void onBackPressed() {
        if (this.a.a()) {
            this.a.b();
        } else if (ApplicationActivity.a(this)) {
            ApplicationActivity.a((Activity) this, false);
        } else {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        a.a(this);
        super.onCreate(bundle);
        try {
            this.a = (g) e.g.getConstructor(Context.class).newInstance(this);
        } catch (Exception e) {
            c.a((Throwable) e);
            this.a = new g(this);
        }
        setContentView(this.a);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void
     arg types: [udk.android.reader.WebContentsManagerActivity, int]
     candidates:
      udk.android.reader.ApplicationActivity.a(udk.android.reader.ApplicationActivity, java.io.File):void
      udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void */
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!ApplicationActivity.a(this)) {
            return true;
        }
        ApplicationActivity.a((Activity) this, false);
        return true;
    }
}
