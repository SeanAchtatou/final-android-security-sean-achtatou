package udk.android.reader;

import android.preference.Preference;
import udk.android.reader.b.c;

final class d implements Preference.OnPreferenceChangeListener {
    private /* synthetic */ PDFReaderConfigurationActivity a;

    d(PDFReaderConfigurationActivity pDFReaderConfigurationActivity) {
        this.a = pDFReaderConfigurationActivity;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        try {
            this.a.a(Integer.parseInt((String) obj) != 1);
        } catch (Exception e) {
            c.a((Throwable) e);
        }
        return true;
    }
}
