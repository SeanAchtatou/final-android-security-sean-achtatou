package udk.android.reader;

import android.app.AlertDialog;
import android.widget.TextView;
import com.unidocs.commonlib.util.b;
import udk.android.reader.b.c;

final class v implements Runnable {
    final /* synthetic */ w a;
    private final /* synthetic */ Throwable b;

    v(w wVar, Throwable th) {
        this.a = wVar;
        this.b = th;
    }

    public final void run() {
        c.a(this.b.getMessage(), this.b);
        TextView textView = null;
        if (b.a(this.b.getMessage())) {
            textView = new TextView(this.a.a);
            textView.setText(this.b.getMessage());
        }
        new AlertDialog.Builder(this.a.a).setTitle((int) C0000R.string.f173).setView(textView).setPositiveButton((int) C0000R.string.f50, new bl(this)).setNegativeButton((int) C0000R.string.f163, new bk(this, this.b)).show().setOnDismissListener(new bm(this));
    }
}
