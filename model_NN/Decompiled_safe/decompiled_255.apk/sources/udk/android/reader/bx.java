package udk.android.reader;

import android.app.AlertDialog;
import android.view.View;

final class bx implements View.OnClickListener {
    final /* synthetic */ ContentsManagerActivity a;

    bx(ContentsManagerActivity contentsManagerActivity) {
        this.a = contentsManagerActivity;
    }

    public final void onClick(View view) {
        new AlertDialog.Builder(this.a).setTitle(this.a.getString(C0000R.string.sort)).setItems(new String[]{this.a.getString(C0000R.string.sort_with_date), this.a.getString(C0000R.string.sort_with_name), this.a.getString(C0000R.string.sort_with_size)}, new ce(this)).show();
        this.a.b();
    }
}
