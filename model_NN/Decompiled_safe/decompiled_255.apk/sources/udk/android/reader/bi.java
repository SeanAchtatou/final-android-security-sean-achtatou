package udk.android.reader;

import android.content.Context;
import android.view.MenuItem;
import java.io.File;
import udk.android.reader.contents.ai;
import udk.android.reader.contents.b;

final class bi implements MenuItem.OnMenuItemClickListener {
    private /* synthetic */ ContentsManagerActivity a;
    private final /* synthetic */ ai b;

    bi(ContentsManagerActivity contentsManagerActivity, ai aiVar) {
        this.a = contentsManagerActivity;
        this.b = aiVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.contents.b.a(android.content.Context, java.io.File[]):void
     arg types: [udk.android.reader.ContentsManagerActivity, java.io.File[]]
     candidates:
      udk.android.reader.contents.b.a(udk.android.reader.contents.b, java.util.List):void
      udk.android.reader.contents.b.a(udk.android.reader.contents.b, boolean):void
      udk.android.reader.contents.b.a(android.app.Activity, java.io.File[]):void
      udk.android.reader.contents.b.a(android.content.Context, java.io.File):void
      udk.android.reader.contents.b.a(android.content.Context, java.io.File[]):void */
    public final boolean onMenuItemClick(MenuItem menuItem) {
        b.a().a((Context) this.a, new File[]{new File(this.b.d())});
        return true;
    }
}
