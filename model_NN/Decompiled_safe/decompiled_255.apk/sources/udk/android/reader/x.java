package udk.android.reader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import udk.android.reader.view.pdf.v;

final class x implements v {
    private /* synthetic */ PDFReaderActivity a;

    x(PDFReaderActivity pDFReaderActivity) {
        this.a = pDFReaderActivity;
    }

    public final Bitmap a() {
        return BitmapFactory.decodeResource(this.a.getResources(), C0000R.drawable.bookmark);
    }
}
