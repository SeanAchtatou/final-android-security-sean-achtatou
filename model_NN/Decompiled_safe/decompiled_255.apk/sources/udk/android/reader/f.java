package udk.android.reader;

import android.view.MenuItem;
import udk.android.reader.pdf.ae;

final class f implements MenuItem.OnMenuItemClickListener {
    private /* synthetic */ RecentPDFListActivity a;
    private final /* synthetic */ String b;

    f(RecentPDFListActivity recentPDFListActivity, String str) {
        this.a = recentPDFListActivity;
        this.b = str;
    }

    public final boolean onMenuItemClick(MenuItem menuItem) {
        ae.a().a(this.a, this.b);
        return true;
    }
}
