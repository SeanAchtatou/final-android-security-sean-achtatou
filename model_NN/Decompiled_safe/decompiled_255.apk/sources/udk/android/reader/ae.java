package udk.android.reader;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.unidocs.commonlib.util.j;
import java.io.File;
import org.apache.commons.io.b;
import udk.android.b.m;
import udk.android.reader.b.a;
import udk.android.reader.b.c;
import udk.android.reader.contents.an;
import udk.android.reader.pdf.y;

final class ae implements Runnable {
    private /* synthetic */ l a;

    ae(l lVar) {
        this.a = lVar;
    }

    public final void run() {
        y c = a.c(this.a.a);
        if (this.a.a.getPackageName().equals("udk.android.reader")) {
            File file = new File(c.f());
            boolean z = false;
            for (File file2 : file.listFiles(new bu(this))) {
                if (file2.getName().indexOf(new StringBuilder().append(m.b(this.a.a)).toString()) >= 0) {
                    z = true;
                } else {
                    file2.delete();
                }
            }
            if (!z) {
                try {
                    b.a(new File(String.valueOf(file.getAbsolutePath()) + "/updatemsg_" + m.b(this.a.a)));
                } catch (Exception e) {
                    c.a(e.getMessage(), e);
                }
                new AlertDialog.Builder(this.a.a).setTitle((int) C0000R.string.f180).setMessage((int) C0000R.string.f181).setPositiveButton((int) C0000R.string.f54, (DialogInterface.OnClickListener) null).show();
            }
        }
        if (this.a.a.getPackageName().startsWith("udk.android.reader")) {
            File file3 = new File(String.valueOf(c.f()) + "/present.1");
            File file4 = new File(a.b(this.a.a) + "/ThankYou");
            File file5 = new File(String.valueOf(file4.getAbsolutePath()) + "/Free_ezPDF_Builder_for_Mobile.pdf");
            if (!file3.exists() && !file5.exists()) {
                file4.mkdirs();
                try {
                    b.a(file3);
                    j.a(file5, this.a.a.getAssets().open("Free_ezPDF_Builder_for_Mobile.pdf"));
                } catch (Exception e2) {
                    c.a((Throwable) e2);
                }
                an.c().f(this.a.a);
            }
        }
    }
}
