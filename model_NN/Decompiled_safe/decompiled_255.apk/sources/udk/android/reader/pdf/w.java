package udk.android.reader.pdf;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public final class w implements ar {
    private static w a;
    private as b;
    private CharSequence c;
    private AlertDialog d;
    /* access modifiers changed from: private */
    public ak e;
    /* access modifiers changed from: private */
    public List f;

    private w() {
        PDF.a().a(this);
    }

    static /* synthetic */ List a(w wVar) {
        if (wVar.f == null) {
            ArrayList arrayList = new ArrayList();
            wVar.a(arrayList, wVar.e);
            wVar.f = arrayList;
        }
        return wVar.f;
    }

    public static w a() {
        if (a == null) {
            a = new w();
        }
        return a;
    }

    private void a(List list, ak akVar) {
        if (!akVar.a()) {
            list.add(akVar);
        }
        if (akVar.g() && akVar.h()) {
            if (akVar.d()) {
                PDF.a().a(akVar);
            }
            for (ak a2 : akVar.f()) {
                a(list, a2);
            }
        }
    }

    static /* synthetic */ void a(w wVar, int i) {
        wVar.d.dismiss();
        wVar.b.a(i);
    }

    private boolean b() {
        return this.e != null;
    }

    static /* synthetic */ void c(w wVar) {
        if (!wVar.b()) {
            wVar.e = new ak(null);
            wVar.e.a(PDF.a().m() > 0);
            if (wVar.e.g()) {
                wVar.e.b(true);
                PDF.a().a(wVar.e);
            }
        }
    }

    public final void a(Activity activity, as asVar, Drawable drawable, Drawable drawable2, Drawable drawable3, String str, String str2, String str3, String str4, String str5, String str6) {
        ad adVar = new ad(this, activity, drawable2, drawable, drawable3);
        LinearLayout linearLayout = new LinearLayout(activity);
        linearLayout.setOrientation(1);
        linearLayout.setGravity(17);
        ProgressBar progressBar = new ProgressBar(activity);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(8);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.weight = 0.0f;
        linearLayout.addView(progressBar, layoutParams);
        ListView listView = new ListView(activity);
        listView.setCacheColorHint(0);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1);
        layoutParams2.weight = 0.0f;
        linearLayout.addView(listView, layoutParams2);
        View view = new View(activity);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -1);
        layoutParams3.weight = 1.0f;
        linearLayout.addView(view, layoutParams3);
        AlertDialog create = new AlertDialog.Builder(activity).setView(linearLayout).setTitle(str2).setNegativeButton(str3, (DialogInterface.OnClickListener) null).create();
        listView.setOnItemClickListener(new z(this, adVar, create, asVar));
        listView.setOnItemLongClickListener(new aa(this, adVar, str6, str5, str4, activity, asVar, create));
        if (!b()) {
            progressBar.setVisibility(0);
            create.show();
            ab abVar = new ab(this, activity, str, create, progressBar, listView, adVar);
            abVar.setDaemon(true);
            abVar.start();
        } else if (!this.e.g()) {
            Toast.makeText(activity, str, 0).show();
            create.dismiss();
        } else {
            listView.setAdapter((ListAdapter) adVar);
            create.show();
        }
    }

    public final void a(ah ahVar) {
    }

    public final void b(ah ahVar) {
    }

    public final void f() {
    }

    public final void g() {
        this.c = null;
        this.e = null;
        this.f = null;
    }

    public final void h() {
    }
}
