package udk.android.reader.pdf.annotation;

import android.graphics.Paint;
import android.graphics.RectF;
import udk.android.b.c;

public abstract class q extends Annotation {
    private RectF a;
    private Paint b = new Paint(1);
    private Paint c;

    public q(int i, double[] dArr) {
        super(i, dArr);
        this.b.setStyle(Paint.Style.STROKE);
        this.c = new Paint(1);
        this.c.setStyle(Paint.Style.FILL);
        f(0);
    }

    public final void a(float f) {
        super.a(f);
        if (this.b != null) {
            this.b.setStrokeWidth(f);
        }
    }

    public void a(int i) {
        super.a(i);
        if (this.b != null) {
            this.b.setColor(i);
        }
    }

    /* access modifiers changed from: package-private */
    public final Paint ad() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final Paint ae() {
        return this.c;
    }

    public final RectF b(float f) {
        RectF rectF = null;
        if (af() != null) {
            rectF = super.b(f);
        } else if (this.a != null) {
            rectF = new RectF(this.a);
            c.c(rectF, f);
        }
        if (rectF != null) {
            rectF.right = rectF.left + (rectF.width() * X());
            rectF.bottom = rectF.top + (rectF.height() * Y());
            rectF.offset((V() * f) / 1.0f, (W() * f) / 1.0f);
        }
        return rectF;
    }

    public void b(float f, float f2, float f3) {
        this.a = new RectF(f / f3, f2 / f3, f / f3, f2 / f3);
    }

    public void d(float f, float f2, float f3) {
    }

    public void e(float f, float f2, float f3) {
        this.a.right = f / f3;
        this.a.bottom = f2 / f3;
    }

    public final void f(int i) {
        super.f(i);
        if (this.c != null) {
            this.c.setColor(i);
        }
    }

    public final boolean h() {
        return false;
    }

    public boolean i() {
        return true;
    }

    public boolean j() {
        return true;
    }

    public final boolean l() {
        return true;
    }

    public final boolean s() {
        return true;
    }

    public final boolean t() {
        return true;
    }
}
