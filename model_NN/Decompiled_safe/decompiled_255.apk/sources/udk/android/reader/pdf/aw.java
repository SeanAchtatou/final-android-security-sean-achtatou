package udk.android.reader.pdf;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.BaseAdapter;
import java.util.List;

final class aw implements DialogInterface.OnClickListener {
    private /* synthetic */ aa a;
    private final /* synthetic */ List b;
    private final /* synthetic */ String c;
    private final /* synthetic */ ak d;
    private final /* synthetic */ BaseAdapter e;
    private final /* synthetic */ String f;
    private final /* synthetic */ String g;
    private final /* synthetic */ ak h;
    private final /* synthetic */ as i;
    private final /* synthetic */ AlertDialog j;

    aw(aa aaVar, List list, String str, ak akVar, BaseAdapter baseAdapter, String str2, String str3, ak akVar2, as asVar, AlertDialog alertDialog) {
        this.a = aaVar;
        this.b = list;
        this.c = str;
        this.d = akVar;
        this.e = baseAdapter;
        this.f = str2;
        this.g = str3;
        this.h = akVar2;
        this.i = asVar;
        this.j = alertDialog;
    }

    public final void onClick(DialogInterface dialogInterface, int i2) {
        String str = (String) this.b.get(i2);
        if (str == this.c) {
            this.d.b(true);
            this.a.a.f = null;
            this.e.notifyDataSetInvalidated();
        } else if (str == this.f) {
            this.d.b(false);
            this.a.a.f = null;
            this.e.notifyDataSetInvalidated();
        } else if (str == this.g && this.h.j() == 1) {
            this.i.a(this.h.k());
            this.j.dismiss();
        }
        dialogInterface.dismiss();
    }
}
