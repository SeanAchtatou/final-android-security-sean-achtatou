package udk.android.reader.pdf;

import udk.android.reader.pdf.a.b;

public final class x {
    private int a;
    private int b;
    private int c;
    private boolean d;
    private String e;
    private b f;

    public final String a() {
        if (!this.d) {
            PDF.a().a(this);
            this.d = true;
        }
        return this.e;
    }

    public final void a(int i) {
        this.a = i;
    }

    public final void a(String str) {
        this.e = str;
    }

    public final void a(b bVar) {
        this.f = bVar;
    }

    public final int b() {
        return this.a;
    }

    public final void b(int i) {
        this.b = i;
    }

    public final int c() {
        return this.b;
    }

    public final void c(int i) {
        this.c = i;
    }

    public final b d() {
        return this.f;
    }

    public final int e() {
        return this.c;
    }
}
