package udk.android.reader.pdf;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import udk.android.b.m;
import udk.android.reader.b.a;

final class ad extends BaseAdapter {
    private /* synthetic */ w a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ Drawable c;
    private final /* synthetic */ Drawable d;
    private final /* synthetic */ Drawable e;

    ad(w wVar, Activity activity, Drawable drawable, Drawable drawable2, Drawable drawable3) {
        this.a = wVar;
        this.b = activity;
        this.c = drawable;
        this.d = drawable2;
        this.e = drawable3;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public ak getItem(int i) {
        return (ak) w.a(this.a).get(i);
    }

    public final int getCount() {
        return w.a(this.a).size();
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        LinearLayout linearLayout;
        if (view == null) {
            LinearLayout linearLayout2 = new LinearLayout(this.b);
            linearLayout2.setOrientation(0);
            linearLayout2.setGravity(16);
            ImageView imageView = new ImageView(this.b);
            imageView.setId(2);
            linearLayout2.addView(imageView, new LinearLayout.LayoutParams(-2, -2));
            LinearLayout linearLayout3 = new LinearLayout(this.b);
            new LinearLayout.LayoutParams(-2, -2);
            linearLayout3.setOrientation(1);
            linearLayout3.setGravity(16);
            TextView textView = new TextView(this.b);
            textView.setTextColor(a.aU);
            textView.setId(3);
            linearLayout3.addView(textView, new LinearLayout.LayoutParams(-2, -2));
            TextView textView2 = new TextView(this.b);
            textView2.setId(4);
            textView2.setTextSize((float) m.a(this.b, 5));
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            linearLayout3.addView(textView2, layoutParams);
            linearLayout2.addView(linearLayout3, layoutParams);
            linearLayout = linearLayout2;
        } else {
            linearLayout = view;
        }
        ak a2 = getItem(i);
        linearLayout.setPadding(a2.b() * m.a(this.b, 20), 0, 0, 0);
        ImageView imageView2 = (ImageView) linearLayout.findViewById(2);
        if (a2.g()) {
            imageView2.setImageDrawable(a2.h() ? this.c : this.d);
        } else {
            imageView2.setImageDrawable(this.e);
        }
        ((TextView) linearLayout.findViewById(3)).setText(a2.i());
        TextView textView3 = (TextView) linearLayout.findViewById(4);
        if (a2.j() == 1) {
            textView3.setText(String.valueOf(a2.k()) + " page");
        }
        return linearLayout;
    }
}
