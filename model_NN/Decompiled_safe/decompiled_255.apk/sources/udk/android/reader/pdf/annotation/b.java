package udk.android.reader.pdf.annotation;

import android.graphics.Canvas;
import udk.android.reader.pdf.b.e;
import udk.android.reader.pdf.b.q;

public final class b extends Annotation implements e {
    private q a;

    public b(int i, double[] dArr, q qVar) {
        super(i, dArr);
        this.a = qVar;
    }

    public final q a() {
        return this.a;
    }

    public final void a(Canvas canvas, float f) {
    }

    public final String k() {
        return "Widget";
    }
}
