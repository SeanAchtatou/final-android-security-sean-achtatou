package udk.android.reader.pdf.annotation;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.unidocs.commonlib.util.b;
import udk.android.b.c;
import udk.android.reader.pdf.c.a;

public final class p extends Annotation {
    public static final String[] a = {"Comment", "Check", "Circle", "Cross", "Help", "Insert", "Key", "NewParagraph", "Note", "Paragraph", "RightArrow", "RightPointer", "Star", "UpArrow", "UpLeftArrow"};
    private String b;

    public p(int i, double[] dArr, String str) {
        super(i, dArr);
        this.b = b.b(str) ? "Note" : str;
    }

    public final String a() {
        return this.b;
    }

    public final void a(Canvas canvas, float f) {
        Bitmap a2 = a.a().a(this.b, L());
        if (a2 != null) {
            RectF b2 = b(f);
            canvas.drawBitmap(a2, b2.left, b2.top, (Paint) null);
        }
    }

    public final RectF b(float f) {
        RectF b2 = super.b(f);
        if (b2 != null) {
            b2.offset((V() * f) / 1.0f, (W() * f) / 1.0f);
        }
        return b2;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final boolean c(float f, float f2, float f3) {
        RectF b2 = b(f);
        return b2 != null && c.a(b2, 10.0f).contains(f2, f3);
    }

    public final boolean h() {
        return false;
    }

    public final boolean i() {
        return true;
    }

    public final String k() {
        return "Text";
    }
}
