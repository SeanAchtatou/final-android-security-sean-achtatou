package udk.android.reader.pdf;

import android.text.TextPaint;
import android.text.style.URLSpan;
import android.view.View;
import udk.android.reader.b.a;

class OutlineService$6 extends URLSpan {
    private /* synthetic */ w a;
    private final /* synthetic */ int b;

    public void onClick(View view) {
        w.a(this.a, this.b);
    }

    public void updateDrawState(TextPaint textPaint) {
        textPaint.linkColor = a.aU;
        super.updateDrawState(textPaint);
    }
}
