package udk.android.reader.pdf;

import android.view.View;
import android.widget.CheckBox;

final class j implements View.OnClickListener {
    private /* synthetic */ b a;
    private final /* synthetic */ CheckBox b;

    j(b bVar, CheckBox checkBox) {
        this.a = bVar;
        this.b = checkBox;
    }

    public final void onClick(View view) {
        this.b.setChecked(!this.b.isChecked());
    }
}
