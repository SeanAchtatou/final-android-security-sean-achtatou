package udk.android.reader.pdf;

import com.unidocs.commonlib.util.b;
import org.w3c.dom.Element;
import udk.android.reader.b.a;
import udk.android.reader.b.c;

public final class an {
    private static an a;
    private PDF b = PDF.a();

    private an() {
    }

    private String a(int i, boolean z, int i2) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            String c = this.b.c(i);
            if (b.b(c)) {
                return "<div style='color:rgb(" + (z ? String.valueOf(a.at) + "," + a.au + "," + a.av : "0,0,0") + ")'>this page has no text</div>";
            }
            for (Element a2 : com.unidocs.commonlib.util.b.a.a(new com.unidocs.commonlib.util.b.a(c.replaceAll("<Paragraph>", "<Paragraph><![CDATA[").replaceAll("</Paragraph>", "]]></Paragraph>"), "UTF-8").a(), "TextBlock")) {
                stringBuffer.append(a(a2, z, i2));
            }
            return stringBuffer.toString();
        } catch (Exception e) {
            c.a((Throwable) e);
        }
    }

    private static String a(Element element, boolean z, int i) {
        StringBuffer stringBuffer = new StringBuffer();
        String str = String.valueOf(a.at) + "," + a.au + "," + a.av;
        if (!z) {
            str = element.getAttribute("color");
            if ("255,255,255".equals(str)) {
                str = "0,0,0";
            }
        }
        float f = 1.0f;
        try {
            f = Float.parseFloat(element.getAttribute("fontsize")) * 0.1f;
        } catch (Exception e) {
            c.a((Throwable) e);
        }
        String str2 = "line-height:" + i + "%25;";
        if ("yes".equalsIgnoreCase(element.getAttribute("fontbold"))) {
            str2 = String.valueOf(str2) + "font-weight:bold;";
        }
        if ("yes".equalsIgnoreCase(element.getAttribute("fontitalic"))) {
            str2 = String.valueOf(str2) + "font-style:italic;";
        }
        stringBuffer.append("<div style='padding-top:5pt;padding-bottom:5pt;color:rgb(" + str + ");font-size:" + f + "em;" + str2 + "'>");
        for (Element a2 : com.unidocs.commonlib.util.b.a.a(element, "Paragraph")) {
            String a3 = com.unidocs.commonlib.util.b.a.a(a2);
            if (a3 == null) {
                a3 = "";
            }
            stringBuffer.append(String.valueOf(a3.replaceAll("%", "%25")) + "<br>");
        }
        stringBuffer.append("</div>");
        return stringBuffer.toString();
    }

    public static an a() {
        if (a == null) {
            a = new an();
        }
        return a;
    }

    public final String a(float f, boolean z, int i) {
        int E = this.b.E();
        String str = z ? String.valueOf(a.aw) + "," + a.ax + "," + a.ay : "255,255,255";
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<html>");
        stringBuffer.append("<head>");
        stringBuffer.append("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\"/>");
        stringBuffer.append("<style type=\"text/css\">");
        stringBuffer.append("body {");
        stringBuffer.append("background : rgb(" + str + ");");
        stringBuffer.append("text-align : left;");
        stringBuffer.append("}");
        stringBuffer.append("</style>");
        stringBuffer.append("<script language='javascript'>function zoom( ratio ){ var z = document.body.style.fontSize.replace( '%25', '' ); document.body.style.fontSize = ( z * ratio ) + '%25'; }</script>");
        stringBuffer.append("</head>");
        stringBuffer.append("<body style=\"font-size:" + (75.0f * f) + "%25;\">");
        stringBuffer.append(a(E, z, i));
        stringBuffer.append("</body>");
        stringBuffer.append("</html>");
        return stringBuffer.toString();
    }
}
