package udk.android.reader.pdf;

import android.graphics.RectF;
import android.net.Uri;
import com.unidocs.commonlib.util.b;
import java.io.File;

public final class c {
    private int a;
    private int b;
    private int c;
    private int d;
    private String e;
    private String f;
    private String g;
    private int h;
    private int i;
    private double[] j;

    public c(int i2, int i3, int i4) {
        this.a = i4;
        this.i = i2;
        this.b = i3;
    }

    public final void a(int i2) {
        this.c = i2;
    }

    public final void a(String str) {
        this.e = str;
    }

    /* access modifiers changed from: package-private */
    public final void a(double[] dArr) {
        this.j = dArr;
    }

    public final double[] a() {
        return this.j;
    }

    public final RectF b() {
        int[] a2 = PDF.a().a(this.i, 1.0f, this.j);
        return new RectF((float) a2[0], (float) a2[3], (float) a2[2], (float) a2[1]);
    }

    public final void b(int i2) {
        this.d = i2;
    }

    public final void b(String str) {
        this.f = str;
    }

    public final Uri c() {
        if (this.c == 6 || this.c == 7 || this.c == 8) {
            return b.a(this.f) ? Uri.fromFile(new File(this.f)) : Uri.parse(this.e);
        }
        return null;
    }

    public final void c(int i2) {
        this.h = i2;
    }

    public final void c(String str) {
        this.g = str;
    }

    public final int d() {
        return this.i;
    }

    public final int e() {
        return this.b;
    }

    public final int f() {
        return this.c;
    }

    public final int g() {
        return this.d;
    }

    public final String h() {
        return this.e;
    }

    public final int i() {
        return this.a;
    }

    public final String j() {
        return this.g;
    }

    public final int k() {
        return this.h;
    }
}
