package udk.android.reader.pdf.annotation;

import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import com.unidocs.commonlib.util.b;
import java.util.Collection;
import java.util.List;
import udk.android.reader.pdf.a.a;

public abstract class ac extends Annotation {
    private List a;
    private Paint b;

    public ac(int i, double[] dArr, List list) {
        super(i, dArr);
        a(list);
        if (!h()) {
            this.b = new Paint(1);
            this.b.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DARKEN));
        }
    }

    /* access modifiers changed from: protected */
    public final Paint a() {
        return this.b;
    }

    public final void a(int i) {
        super.a(i);
        if (this.b != null) {
            this.b.setColor(L());
        }
    }

    public void a(List list) {
        this.a = list;
    }

    public final List c() {
        return this.a;
    }

    public final boolean c(float f, float f2, float f3) {
        if (b.b((Collection) this.a)) {
            return false;
        }
        for (a c : this.a) {
            if (c.c(f, f2, f3)) {
                return true;
            }
        }
        return false;
    }
}
