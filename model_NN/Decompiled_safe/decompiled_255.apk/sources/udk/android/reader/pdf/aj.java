package udk.android.reader.pdf;

import com.unidocs.commonlib.util.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.x;
import udk.android.reader.view.pdf.hx;

public final class aj {
    private static aj a;
    private PDF b = PDF.a();
    private String c;
    private HashMap d = new HashMap();
    private List e = new ArrayList();

    private aj() {
    }

    public static aj a() {
        if (a == null) {
            a = new aj();
        }
        return a;
    }

    public final c a(int i, int i2) {
        List<Annotation> list = (List) this.d.get(Integer.valueOf(i));
        if (b.a((Collection) list)) {
            for (Annotation J : list) {
                c cVar = (c) J.J();
                if (cVar.i() == i2) {
                    return cVar;
                }
            }
        }
        return null;
    }

    public final void a(c cVar, boolean z) {
        this.b.a(cVar, z);
    }

    public final boolean a(int i) {
        return b.a((Collection) this.d.get(Integer.valueOf(i)));
    }

    public final boolean a(c cVar) {
        return this.b.a(cVar);
    }

    public final void b(c cVar) {
        List<Annotation> list = (List) this.d.get(Integer.valueOf(cVar.d()));
        if (!b.b((Collection) list)) {
            for (Annotation annotation : list) {
                if (annotation.J() == cVar) {
                    ((x) annotation).b(579338325);
                    hx.a().p();
                    return;
                }
            }
        }
    }

    public final boolean b(int i) {
        if (!this.b.z().equals(this.c)) {
            this.d.clear();
            this.e.clear();
            this.c = this.b.z();
        }
        List list = null;
        if (this.d.containsKey(Integer.valueOf(i))) {
            list = (List) this.d.get(Integer.valueOf(i));
        } else {
            List<c> b2 = this.b.b(i);
            if (b.a((Collection) b2)) {
                ArrayList arrayList = new ArrayList();
                for (c cVar : b2) {
                    x xVar = new x(i, cVar.a());
                    xVar.a(cVar);
                    xVar.b(0);
                    arrayList.add(xVar);
                }
                list = arrayList;
            }
            this.d.put(Integer.valueOf(i), list);
        }
        if (!b.a(list)) {
            return false;
        }
        hx.a().p();
        return true;
    }

    public final List c(int i) {
        return (List) this.d.get(Integer.valueOf(i));
    }

    public final void d(int i) {
        if (!this.b.z().equals(this.c)) {
            this.d.clear();
            this.e.clear();
            this.c = this.b.z();
        }
        boolean z = !this.e.contains(Integer.valueOf(i));
        List<Annotation> list = (List) this.d.get(Integer.valueOf(i));
        if (!b.b((Collection) list)) {
            for (Annotation annotation : list) {
                ((x) annotation).b(z ? 579338325 : 0);
            }
            if (!z) {
                this.e.remove(new Integer(i));
            } else if (!this.e.contains(Integer.valueOf(i))) {
                this.e.add(Integer.valueOf(i));
            }
            hx.a().p();
        }
    }
}
