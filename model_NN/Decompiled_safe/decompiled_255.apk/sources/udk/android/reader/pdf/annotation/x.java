package udk.android.reader.pdf.annotation;

import android.graphics.Canvas;
import android.graphics.Paint;

public final class x extends Annotation {
    private Paint a;

    public x(int i, double[] dArr) {
        super(i, dArr);
    }

    public final void a(Canvas canvas, float f) {
        if (this.a == null) {
            this.a = new Paint();
        }
        canvas.drawRect(b(f), this.a);
    }

    public final void b(int i) {
        if (this.a == null) {
            this.a = new Paint();
        }
        this.a.setColor(i);
    }

    public final String k() {
        return null;
    }
}
