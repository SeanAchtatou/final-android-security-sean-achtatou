package udk.android.reader.pdf;

import android.view.View;
import android.widget.AdapterView;
import udk.android.b.m;

final class ap implements AdapterView.OnItemClickListener {
    private /* synthetic */ av a;
    private final /* synthetic */ String b;

    ap(av avVar, String str) {
        this.a = avVar;
        this.b = str;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        String[] strArr = (String[]) adapterView.getAdapter().getItem(i);
        m.a(adapterView, String.valueOf(strArr[0]) + " - " + strArr[1], this.b);
    }
}
