package udk.android.reader.pdf.annotation;

import android.app.AlertDialog;
import android.content.Context;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.unidocs.commonlib.util.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import udk.android.b.c;
import udk.android.reader.b.a;
import udk.android.reader.b.d;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.ah;
import udk.android.reader.pdf.ai;
import udk.android.reader.pdf.ar;

public final class s implements ar {
    private static s a;
    /* access modifiers changed from: private */
    public PDF b = PDF.a();
    private String c;
    private HashMap d;
    private Annotation e;
    private List f;

    private s() {
        this.b.a(this);
        this.d = new HashMap();
        this.f = new ArrayList();
    }

    public static s a() {
        if (a == null) {
            a = new s();
        }
        return a;
    }

    private void a(ac acVar, ai aiVar, ai aiVar2) {
        f(acVar);
        if (this.b.a(acVar, aiVar, aiVar2)) {
            g(acVar);
            ab abVar = new ab();
            abVar.a = acVar;
            c(abVar);
            if (a.r) {
                k();
            }
        }
    }

    private void b(q qVar) {
        f(qVar);
        g(qVar);
    }

    private boolean b(Annotation annotation, String str) {
        if (!annotation.Q()) {
            d(annotation);
        }
        String lowerCase = str.toLowerCase();
        return (annotation.N() != null && annotation.N().toLowerCase().indexOf(lowerCase) >= 0) || (annotation.P() != null && annotation.P().toLowerCase().indexOf(lowerCase) >= 0) || (annotation.M() != null && annotation.M().toLowerCase().indexOf(lowerCase) >= 0);
    }

    private void c(ab abVar) {
        for (aa a2 : this.f) {
            a2.a(abVar);
        }
    }

    private static void f(Annotation annotation) {
        annotation.R();
        annotation.c(annotation.k());
        annotation.d(UUID.randomUUID().toString());
        annotation.e(a.t);
    }

    private void g(Annotation annotation) {
        if (!this.b.z().equals(this.c)) {
            synchronized (this.d) {
                this.d.clear();
            }
            this.c = this.b.z();
        }
        Object obj = (List) this.d.get(Integer.valueOf(annotation.ag()));
        if (obj == null) {
            obj = new ArrayList();
            synchronized (this.d) {
                this.d.put(Integer.valueOf(annotation.ag()), obj);
            }
        }
        synchronized (obj) {
            obj.add(annotation);
        }
    }

    private void h(Annotation annotation) {
        List list = (List) this.d.get(Integer.valueOf(annotation.ag()));
        synchronized (list) {
            list.remove(annotation);
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        new m(this).start();
    }

    public final List a(int i, String str) {
        List<Annotation> h = h(i);
        if (b.b((Collection) h)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (Annotation annotation : h) {
            if (b(annotation, str)) {
                arrayList.add(annotation);
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List a(java.lang.String r6) {
        /*
            r5 = this;
            r4 = 0
            java.util.List r1 = r5.j()
            r2 = r4
        L_0x0006:
            int r0 = r1.size()
            if (r2 < r0) goto L_0x000d
            return r1
        L_0x000d:
            java.lang.Object r0 = r1.get(r2)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            java.util.HashMap r3 = r5.d
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Object r0 = r3.get(r0)
            java.util.List r0 = (java.util.List) r0
            boolean r3 = com.unidocs.commonlib.util.b.a(r0)
            if (r3 == 0) goto L_0x0033
            java.util.Iterator r3 = r0.iterator()
        L_0x002d:
            boolean r0 = r3.hasNext()
            if (r0 != 0) goto L_0x003f
        L_0x0033:
            r0 = r4
        L_0x0034:
            if (r0 != 0) goto L_0x004d
            r1.remove(r2)
            int r0 = r2 + -1
        L_0x003b:
            int r0 = r0 + 1
            r2 = r0
            goto L_0x0006
        L_0x003f:
            java.lang.Object r0 = r3.next()
            udk.android.reader.pdf.annotation.Annotation r0 = (udk.android.reader.pdf.annotation.Annotation) r0
            boolean r0 = r5.b(r0, r6)
            if (r0 == 0) goto L_0x002d
            r0 = 1
            goto L_0x0034
        L_0x004d:
            r0 = r2
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.pdf.annotation.s.a(java.lang.String):java.util.List");
    }

    public final Annotation a(int i, int i2) {
        List<Annotation> h = h(i);
        if (b.a((Collection) h)) {
            for (Annotation annotation : h) {
                if (annotation.ag() == i && annotation.K() == i2) {
                    return annotation;
                }
            }
        }
        return null;
    }

    public final a a(int i) {
        a aVar = new a(i, null, false);
        aVar.a(a.w);
        aVar.a(a.x);
        f(aVar);
        g(aVar);
        return aVar;
    }

    public final a a(a aVar) {
        aVar.b(true);
        if (!this.b.a(aVar)) {
            h(aVar);
            return null;
        }
        ab abVar = new ab();
        abVar.a = aVar;
        c(abVar);
        if (a.r) {
            k();
        }
        return aVar;
    }

    public final p a(int i, float f2, int i2, int i3) {
        p pVar = new p(i, null, a.u);
        pVar.a(a.v);
        f(pVar);
        if (!this.b.a(pVar, f2, i2, i3)) {
            return null;
        }
        g(pVar);
        ab abVar = new ab();
        abVar.a = pVar;
        c(abVar);
        if (!a.r) {
            return pVar;
        }
        k();
        return pVar;
    }

    public final z a(Context context, Annotation annotation, String str, String str2) {
        z zVar = new z(annotation.ag());
        f(zVar);
        zVar.e(str);
        zVar.a(str2);
        if (!this.b.a(annotation, zVar)) {
            return null;
        }
        a.t = str;
        a.a(context, d.b, a.t);
        if (!a.r) {
            return zVar;
        }
        k();
        return zVar;
    }

    public final void a(int i, ai aiVar, ai aiVar2) {
        t tVar = new t(i, null, null);
        tVar.a(a.H);
        a(tVar, aiVar, aiVar2);
    }

    public final void a(Context context, Annotation annotation, float f2, int i) {
        annotation.a(f2);
        annotation.e(i);
        this.b.e(annotation);
        if (annotation instanceof a) {
            a.x = f2;
            a.a(context, d.h, Float.valueOf(a.x));
        } else if (annotation instanceof o) {
            a.B = f2;
            a.a(context, d.l, Float.valueOf(a.B));
        } else if (annotation instanceof n) {
            a.B = f2;
            a.a(context, d.l, Float.valueOf(a.B));
        } else if (annotation instanceof e) {
            a.F = f2;
            a.a(context, d.p, Float.valueOf(a.F));
        }
        if (a.r) {
            k();
        }
        ab abVar = new ab();
        abVar.a = annotation;
        a(abVar);
    }

    public final void a(Context context, Annotation annotation, int i) {
        annotation.a(i);
        this.b.f(annotation);
        if (annotation instanceof t) {
            a.H = i;
            a.a(context, d.c, Integer.valueOf(a.H));
        } else if (annotation instanceof u) {
            a.I = i;
            a.a(context, d.d, Integer.valueOf(a.I));
        } else if (annotation instanceof ad) {
            a.J = i;
            a.a(context, d.e, Integer.valueOf(a.J));
        } else if (annotation instanceof p) {
            a.v = i;
            a.a(context, d.f, Integer.valueOf(a.v));
        } else if (annotation instanceof a) {
            a.w = i;
            a.a(context, d.g, Integer.valueOf(a.w));
        } else if (annotation instanceof o) {
            a.y = i;
            a.a(context, d.i, Integer.valueOf(a.y));
        } else if (annotation instanceof n) {
            a.y = i;
            a.a(context, d.i, Integer.valueOf(a.y));
        } else if (annotation instanceof e) {
            a.C = i;
            a.a(context, d.m, Integer.valueOf(a.C));
        }
        if (a.r) {
            k();
        }
        ab abVar = new ab();
        abVar.a = annotation;
        a(abVar);
    }

    public final void a(Context context, Annotation annotation, Runnable runnable) {
        int a2 = (int) a.a(5.0f);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(a2, a2, a2, a2);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        EditText editText = new EditText(context);
        editText.setHint(udk.android.reader.b.b.Y);
        editText.setText(a.t);
        editText.setSingleLine(true);
        linearLayout.addView(editText, layoutParams);
        EditText editText2 = new EditText(context);
        editText2.setHint(udk.android.reader.b.b.ac);
        linearLayout.addView(editText2, layoutParams);
        new AlertDialog.Builder(context).setTitle(udk.android.reader.b.b.g).setView(linearLayout).setPositiveButton(udk.android.reader.b.b.r, new k(this, editText, editText2, context, annotation, runnable)).show();
        editText2.requestFocus();
    }

    public final void a(Context context, Annotation annotation, String str) {
        annotation.e(str);
        this.b.i(annotation);
        a.t = str;
        a.a(context, d.b, a.t);
        if (a.r) {
            k();
        }
    }

    public final void a(Context context, Annotation annotation, boolean z, int i) {
        annotation.c(z);
        annotation.f(c.a(i, annotation.A()));
        this.b.g(annotation);
        if (annotation instanceof o) {
            a.z = z;
            a.A = i;
            a.a(context, d.j, Boolean.valueOf(a.z));
            a.a(context, d.k, Integer.valueOf(a.A));
        } else if (annotation instanceof n) {
            a.z = z;
            a.A = i;
            a.a(context, d.j, Boolean.valueOf(a.z));
            a.a(context, d.k, Integer.valueOf(a.A));
        } else if (annotation instanceof e) {
            a.D = z;
            a.E = i;
            a.a(context, d.n, Boolean.valueOf(a.D));
            a.a(context, d.o, Integer.valueOf(a.E));
        }
        if (a.r) {
            k();
        }
        ab abVar = new ab();
        abVar.a = annotation;
        a(abVar);
    }

    public final void a(Context context, e eVar, float f2) {
        eVar.e(f2);
        this.b.a(eVar);
        a.G = f2;
        a.a(context, d.q, Float.valueOf(a.G));
        if (a.r) {
            k();
        }
        ab abVar = new ab();
        abVar.a = eVar;
        a(abVar);
    }

    public final void a(Context context, p pVar, String str) {
        pVar.b(str);
        this.b.a(pVar);
        a.u = str;
        a.a(context, d.r, a.u);
        if (a.r) {
            k();
        }
        ab abVar = new ab();
        abVar.a = pVar;
        a(abVar);
    }

    public final void a(Context context, z zVar, Runnable runnable) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(udk.android.reader.b.b.g);
        if (!zVar.a()) {
            arrayList.add(udk.android.reader.b.b.f);
        }
        new AlertDialog.Builder(context).setItems((CharSequence[]) arrayList.toArray(new String[0]), new j(this, arrayList, context, zVar, runnable)).show();
    }

    public final void a(ah ahVar) {
        if (ahVar.c && b()) {
            d();
        }
    }

    public final void a(Annotation annotation) {
        if (this.e != annotation) {
            if (annotation != null && !annotation.Q()) {
                d(annotation);
            }
            ab abVar = new ab();
            abVar.c = this.e;
            this.e = annotation;
            abVar.a = this.e;
            for (aa e2 : this.f) {
                e2.e(abVar);
            }
        }
    }

    public final void a(Annotation annotation, String str) {
        annotation.c(str);
        this.b.h(annotation);
        if (a.r) {
            k();
        }
    }

    public final void a(Annotation annotation, String str, boolean z) {
        annotation.a(str);
        this.b.j(annotation);
        if (annotation instanceof e) {
            float F = this.b.F();
            if (((e) annotation).c(F)) {
                annotation.b(this.b.a(annotation.ag(), F, ((e) annotation).d(F)));
                this.b.d(annotation);
            }
        }
        if (a.r) {
            k();
        }
        if (z) {
            ab abVar = new ab();
            abVar.a = annotation;
            a(abVar);
        }
    }

    public final void a(aa aaVar) {
        if (!this.f.contains(aaVar)) {
            this.f.add(aaVar);
        }
    }

    public final void a(ab abVar) {
        for (aa b2 : this.f) {
            b2.b(abVar);
        }
    }

    public final void a(q qVar) {
        if (!this.b.a(qVar)) {
            h(qVar);
            return;
        }
        ab abVar = new ab();
        abVar.a = qVar;
        c(abVar);
        if (a.r) {
            k();
        }
    }

    public final void a(w wVar, String str, String str2) {
        wVar.i(str);
        wVar.j(str2);
        this.b.a(wVar);
        if (a.r) {
            k();
        }
        ab abVar = new ab();
        abVar.a = wVar;
        a(abVar);
    }

    public final int b(int i, String str) {
        return ((Integer) a(str).get(i)).intValue();
    }

    public final o b(int i) {
        o oVar = new o(i, null);
        oVar.a(a.y);
        oVar.c(a.z);
        oVar.f(c.a(a.A, oVar.A()));
        oVar.a(a.B);
        b((q) oVar);
        return oVar;
    }

    public final void b(int i, ai aiVar, ai aiVar2) {
        u uVar = new u(i, null, null);
        uVar.a(a.I);
        a(uVar, aiVar, aiVar2);
    }

    public final void b(ah ahVar) {
    }

    public final void b(Annotation annotation) {
        if (annotation == this.e) {
            d();
        }
        h(annotation);
        this.b.b(annotation);
        ab abVar = new ab();
        abVar.a = annotation;
        for (aa c2 : this.f) {
            c2.c(abVar);
        }
        if (a.r) {
            k();
        }
    }

    public final void b(a aVar) {
        h(aVar);
    }

    public final void b(aa aaVar) {
        this.f.remove(aaVar);
    }

    public final void b(ab abVar) {
        for (aa d2 : this.f) {
            d2.d(abVar);
        }
    }

    public final boolean b() {
        return this.e != null;
    }

    public final Annotation c() {
        return this.e;
    }

    public final n c(int i) {
        n nVar = new n(i, null);
        nVar.a(a.y);
        nVar.c(a.z);
        nVar.f(c.a(a.A, nVar.A()));
        nVar.a(a.B);
        b((q) nVar);
        return nVar;
    }

    public final void c(int i, ai aiVar, ai aiVar2) {
        ad adVar = new ad(i, null, null);
        adVar.a(a.J);
        a(adVar, aiVar, aiVar2);
    }

    public final void c(Annotation annotation) {
        if (annotation instanceof a) {
            this.b.b((a) annotation);
        } else if (annotation instanceof w) {
            this.b.b((w) annotation);
        } else {
            this.b.c(annotation);
        }
        annotation.I();
        if (a.r) {
            k();
        }
        ab abVar = new ab();
        abVar.a = annotation;
        a(abVar);
    }

    public final w d(int i) {
        w wVar = new w(i, null, null, "None", "None");
        wVar.a(a.y);
        wVar.c(a.z);
        wVar.f(c.a(a.A, wVar.A()));
        wVar.a(a.B);
        b((q) wVar);
        return wVar;
    }

    public final void d() {
        if (b()) {
            a((Annotation) null);
        }
    }

    public final void d(Annotation annotation) {
        this.b.k(annotation);
    }

    public final List e(Annotation annotation) {
        return this.b.a(annotation);
    }

    public final e e(int i) {
        e eVar = new e(i, null);
        eVar.e(a.G);
        eVar.a(a.C);
        eVar.c(a.D);
        eVar.f(c.a(a.E, eVar.A()));
        eVar.a(a.F);
        b((q) eVar);
        return eVar;
    }

    public final boolean e() {
        return this.b.checkPrivatePieceInfo();
    }

    public final void f() {
    }

    public final boolean f(int i) {
        if (!this.b.z().equals(this.c)) {
            synchronized (this.d) {
                this.d.clear();
            }
            this.c = this.b.z();
        }
        return this.d.containsKey(Integer.valueOf(i));
    }

    public final void g() {
        synchronized (this.d) {
            this.d.clear();
        }
        this.e = null;
    }

    public final boolean g(int i) {
        List a2;
        if (!this.b.z().equals(this.c)) {
            synchronized (this.d) {
                this.d.clear();
            }
            this.c = this.b.z();
        }
        if (this.d.containsKey(Integer.valueOf(i))) {
            a2 = (List) this.d.get(Integer.valueOf(i));
        } else {
            a2 = this.b.a(i);
            synchronized (this.d) {
                this.d.put(Integer.valueOf(i), a2);
            }
        }
        if (!b.a((Collection) a2)) {
            return false;
        }
        for (aa a3 : this.f) {
            a3.a();
        }
        return true;
    }

    public final List h(int i) {
        return (List) this.d.get(Integer.valueOf(i));
    }

    public final void h() {
    }

    public final int i(int i) {
        return ((Integer) j().get(i)).intValue();
    }

    public final void i() {
        l lVar = new l(this);
        lVar.setDaemon(true);
        lVar.start();
    }

    public final List j() {
        ArrayList arrayList = new ArrayList();
        synchronized (this.d) {
            for (Integer intValue : this.d.keySet()) {
                int intValue2 = intValue.intValue();
                if (this.d.get(Integer.valueOf(intValue2)) != null) {
                    arrayList.add(Integer.valueOf(intValue2));
                }
            }
        }
        Collections.sort(arrayList);
        return arrayList;
    }
}
