package udk.android.reader.pdf.annotation;

import android.content.Context;
import android.content.DialogInterface;
import java.util.List;
import udk.android.reader.b.a;
import udk.android.reader.b.b;

final class j implements DialogInterface.OnClickListener {
    private /* synthetic */ s a;
    private final /* synthetic */ List b;
    private final /* synthetic */ Context c;
    private final /* synthetic */ z d;
    private final /* synthetic */ Runnable e;

    j(s sVar, List list, Context context, z zVar, Runnable runnable) {
        this.a = sVar;
        this.b = list;
        this.c = context;
        this.d = zVar;
        this.e = runnable;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.annotation.s.a(android.content.Context, udk.android.reader.pdf.annotation.Annotation, java.lang.Runnable):void
     arg types: [android.content.Context, udk.android.reader.pdf.annotation.z, java.lang.Runnable]
     candidates:
      udk.android.reader.pdf.annotation.s.a(udk.android.reader.pdf.annotation.ac, udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):void
      udk.android.reader.pdf.annotation.s.a(int, udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):void
      udk.android.reader.pdf.annotation.s.a(android.content.Context, udk.android.reader.pdf.annotation.Annotation, int):void
      udk.android.reader.pdf.annotation.s.a(android.content.Context, udk.android.reader.pdf.annotation.Annotation, java.lang.String):void
      udk.android.reader.pdf.annotation.s.a(android.content.Context, udk.android.reader.pdf.annotation.e, float):void
      udk.android.reader.pdf.annotation.s.a(android.content.Context, udk.android.reader.pdf.annotation.p, java.lang.String):void
      udk.android.reader.pdf.annotation.s.a(android.content.Context, udk.android.reader.pdf.annotation.z, java.lang.Runnable):void
      udk.android.reader.pdf.annotation.s.a(udk.android.reader.pdf.annotation.Annotation, java.lang.String, boolean):void
      udk.android.reader.pdf.annotation.s.a(udk.android.reader.pdf.annotation.w, java.lang.String, java.lang.String):void
      udk.android.reader.pdf.annotation.s.a(android.content.Context, udk.android.reader.pdf.annotation.Annotation, java.lang.Runnable):void */
    public final void onClick(DialogInterface dialogInterface, int i) {
        String str = (String) this.b.get(i);
        if (str.equals(b.g)) {
            this.a.a(this.c, (Annotation) this.d, this.e);
        } else if (str.equals(b.f)) {
            this.a.b.b(this.d);
            if (a.r) {
                this.a.k();
            }
            if (this.e != null) {
                this.e.run();
            }
        }
    }
}
