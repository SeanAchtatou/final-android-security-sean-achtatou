package udk.android.reader.pdf.annotation;

import android.graphics.Canvas;
import android.graphics.Paint;
import udk.android.reader.pdf.b.e;
import udk.android.reader.pdf.b.i;
import udk.android.reader.pdf.b.q;

public final class d extends e implements e {
    private q a;
    private Paint b;

    public d(int i, double[] dArr, q qVar) {
        super(i, dArr);
        this.a = qVar;
        p();
    }

    public final q a() {
        return this.a;
    }

    public final void a(Canvas canvas, float f) {
        super.a(canvas, f);
        if (ab()) {
            if (this.b == null) {
                this.b = new Paint(1);
                this.b.setColor(L());
                this.b.setStyle(Paint.Style.STROKE);
                this.b.setStrokeWidth(2.0f);
            }
            canvas.drawRect(b(f), this.b);
        }
    }

    public final boolean a_() {
        return true;
    }

    public final boolean c() {
        return ((i) this.a).b();
    }

    public final boolean d() {
        return ((i) this.a).a();
    }

    public final int e() {
        return ((i) this.a).c();
    }

    public final boolean i() {
        return false;
    }

    public final boolean j() {
        return false;
    }

    public final String k() {
        return "Widget";
    }
}
