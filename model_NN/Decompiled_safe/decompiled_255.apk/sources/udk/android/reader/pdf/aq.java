package udk.android.reader.pdf;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
import udk.android.reader.C0000R;

final class aq extends BaseAdapter {
    private /* synthetic */ av a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ int c = C0000R.layout.property;
    private final /* synthetic */ int d = C0000R.id.key;
    private final /* synthetic */ int e = C0000R.id.value;
    private final /* synthetic */ List f;

    aq(av avVar, Context context, List list) {
        this.a = avVar;
        this.b = context;
        this.f = list;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String[] getItem(int i) {
        return (String[]) this.f.get(i);
    }

    public final int getCount() {
        return this.f.size();
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        String[] a2 = getItem(i);
        View inflate = View.inflate(this.b, this.c, null);
        ((TextView) inflate.findViewById(this.d)).setText(a2[0]);
        ((TextView) inflate.findViewById(this.e)).setText(a2[1]);
        return inflate;
    }
}
