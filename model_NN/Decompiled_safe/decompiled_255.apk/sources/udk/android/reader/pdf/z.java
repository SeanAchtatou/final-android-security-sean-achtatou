package udk.android.reader.pdf;

import android.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;

final class z implements AdapterView.OnItemClickListener {
    private /* synthetic */ w a;
    private final /* synthetic */ BaseAdapter b;
    private final /* synthetic */ AlertDialog c;
    private final /* synthetic */ as d;

    z(w wVar, BaseAdapter baseAdapter, AlertDialog alertDialog, as asVar) {
        this.a = wVar;
        this.b = baseAdapter;
        this.c = alertDialog;
        this.d = asVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        ak akVar = (ak) this.b.getItem(i);
        if (akVar.g()) {
            akVar.b(!akVar.h());
            this.a.f = null;
            this.b.notifyDataSetInvalidated();
        } else if (akVar.j() == 1) {
            this.c.dismiss();
            this.d.a(akVar.k());
        }
    }
}
