package udk.android.reader.pdf;

import java.io.File;
import java.io.FileFilter;
import udk.android.reader.b.c;

final class e implements FileFilter {
    private /* synthetic */ PDF a;

    e(PDF pdf) {
        this.a = pdf;
    }

    public final boolean accept(File file) {
        long j;
        String name = file.getName();
        if (!file.isDirectory() || !name.startsWith("p_")) {
            return false;
        }
        try {
            j = Long.parseLong(name.replaceAll("p_", ""));
        } catch (Exception e) {
            c.a((Throwable) e);
            j = Long.MAX_VALUE;
        }
        return System.currentTimeMillis() - j > 3600000;
    }
}
