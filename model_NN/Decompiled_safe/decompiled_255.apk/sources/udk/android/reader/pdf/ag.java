package udk.android.reader.pdf;

import com.unidocs.commonlib.util.a;
import udk.android.reader.b.c;

public final class ag {
    private String a;
    private a b;

    ag(y yVar, String str) {
        this.a = String.valueOf(udk.android.reader.b.a.a(yVar, str)) + "/configuration";
        try {
            this.b = new a(this.a);
        } catch (Exception e) {
            c.a("## INITIALIZE PDF CONFIGURATION : FAILURE", e);
        }
    }

    public final float a(String str, float f) {
        return this.b.a(str, f);
    }

    public final int a(String str, int i) {
        return this.b.a(str, i);
    }

    public final void a() {
        try {
            this.b.a("UTF-8");
        } catch (Exception e) {
            c.a("## COMMIT PDF CONFIGURATION : FAILURE", e);
        }
    }

    public final void a(String str, Object obj) {
        this.b.a(str, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.unidocs.commonlib.util.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.unidocs.commonlib.util.a.a(java.lang.String, float):float
      com.unidocs.commonlib.util.a.a(java.lang.String, int):int
      com.unidocs.commonlib.util.a.a(java.lang.String, java.lang.String):java.lang.String
      com.unidocs.commonlib.util.a.a(java.lang.String, java.lang.Object):void
      com.unidocs.commonlib.util.a.a(java.lang.String, boolean):boolean */
    public final boolean a(String str) {
        return this.b.a(str, false);
    }

    public final String b(String str) {
        return this.b.a(str, (String) null);
    }

    public final void b(String str, Object obj) {
        a(str, obj);
        a();
    }
}
