package udk.android.reader.pdf.annotation;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import com.unidocs.commonlib.util.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import udk.android.b.c;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.a.a;

public final class t extends ac {
    private List a;

    public t(int i, double[] dArr, List list) {
        super(i, dArr, list);
    }

    public final void a(Canvas canvas, float f) {
        if (!b.b((Collection) c())) {
            Paint a2 = a();
            for (f fVar : this.a) {
                canvas.save();
                canvas.translate(fVar.a() * f, fVar.b() * f);
                canvas.scale(f, f);
                canvas.rotate(fVar.c());
                canvas.drawPath(fVar.d(), a2);
                canvas.restore();
            }
        }
    }

    public final void a(List list) {
        super.a(list);
        if (!h() && list != null) {
            ArrayList arrayList = new ArrayList();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                int[] a2 = PDF.a().a(ag(), 1.0f, ((a) it.next()).af());
                PointF pointF = new PointF((float) a2[0], (float) a2[1]);
                PointF pointF2 = new PointF((float) a2[2], (float) a2[3]);
                PointF pointF3 = new PointF((float) a2[4], (float) a2[5]);
                double atan2 = Math.atan2((double) (pointF2.y - pointF.y), (double) (pointF2.x - pointF.x));
                RectF rectF = new RectF(0.0f, 0.0f, c.a(pointF, pointF2), c.a(pointF, pointF3));
                float height = rectF.height() / 4.0f;
                RectF rectF2 = new RectF(rectF.left - height, rectF.top, rectF.left + height, rectF.bottom);
                RectF rectF3 = new RectF(rectF.right - height, rectF.top, height + rectF.right, rectF.bottom);
                Path path = new Path();
                path.addRect(rectF, Path.Direction.CW);
                path.addOval(rectF2, Path.Direction.CW);
                path.addOval(rectF3, Path.Direction.CW);
                f fVar = new f(this);
                fVar.c((float) c.a(atan2));
                fVar.a(pointF.x);
                fVar.b(pointF.y);
                fVar.a(path);
                arrayList.add(fVar);
            }
            this.a = arrayList;
        }
    }

    public final String k() {
        return "Highlight";
    }
}
