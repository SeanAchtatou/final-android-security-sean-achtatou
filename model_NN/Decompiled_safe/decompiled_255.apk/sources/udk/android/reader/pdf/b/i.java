package udk.android.reader.pdf.b;

public final class i extends q {
    private boolean a;
    private boolean b;
    private int c;

    public i(int i, String str, int i2, String str2) {
        super(i, str, i2, str2);
    }

    public final void a(int i) {
        this.c = i;
    }

    public final void a(boolean z) {
        this.a = z;
    }

    public final boolean a() {
        return this.a;
    }

    public final void b(boolean z) {
        this.b = z;
    }

    public final boolean b() {
        return this.b;
    }

    public final int c() {
        return this.c;
    }
}
