package udk.android.reader.pdf;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.unidocs.commonlib.util.b;
import java.util.ArrayList;
import java.util.List;
import udk.android.b.l;
import udk.android.reader.b.c;

public final class av {
    private static av a;

    private av() {
    }

    public static av a() {
        if (a == null) {
            a = new av();
        }
        return a;
    }

    private static List b() {
        ArrayList arrayList = new ArrayList();
        PDF a2 = PDF.a();
        if (a2.f()) {
            for (String str : new String[]{"Title", "Author", "Creator", "CreationDate", "ModDate", "Producer"}) {
                String b = a2.b(str);
                if (b.b(b)) {
                    b = "";
                }
                if (str.toLowerCase().indexOf("date") >= 0) {
                    try {
                        b = l.a(b).toString();
                    } catch (Exception e) {
                        c.a((Throwable) e);
                    }
                }
                arrayList.add(new String[]{str, b});
            }
            String[] strArr = new String[2];
            strArr[0] = "Security Method";
            strArr[1] = !a2.h() ? "No Security" : a2.i();
            arrayList.add(strArr);
        }
        return arrayList;
    }

    public final void a(Context context, Drawable drawable, String str, String str2, String str3) {
        List b = b();
        ListView listView = new ListView(context);
        listView.setCacheColorHint(0);
        listView.setDivider(drawable);
        listView.setOnItemClickListener(new ap(this, str3));
        listView.setAdapter((ListAdapter) new aq(this, context, b));
        new AlertDialog.Builder(context).setTitle(str).setView(listView).setPositiveButton(str2, (DialogInterface.OnClickListener) null).show();
    }
}
