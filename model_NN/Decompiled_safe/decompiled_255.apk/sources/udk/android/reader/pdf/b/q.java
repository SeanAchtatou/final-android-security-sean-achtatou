package udk.android.reader.pdf.b;

import com.unidocs.commonlib.util.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class q implements Comparable {
    private int a;
    private String b;
    private int c;
    private String d;
    private boolean e;
    private boolean f;
    private q g;
    private List h = new ArrayList();

    public q(int i, String str, int i2, String str2) {
        this.a = i;
        this.b = str;
        this.c = i2;
        this.d = str2;
    }

    public final void a(String str) {
        this.d = str;
    }

    public final void a(q qVar) {
        this.g = qVar;
    }

    public final q b(int i) {
        if (i == this.a) {
            return this;
        }
        q qVar = null;
        for (q b2 : this.h) {
            qVar = b2.b(i);
            if (qVar != null) {
                return qVar;
            }
        }
        return qVar;
    }

    public final void b(q qVar) {
        this.h.add(qVar);
    }

    public final void c(boolean z) {
        this.e = z;
    }

    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return 0 - ((q) obj).b.compareTo(this.b);
    }

    public final void d(boolean z) {
        this.f = z;
    }

    public final boolean d() {
        return b.a((Collection) this.h);
    }

    public final String e() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass().equals(getClass())) {
            return false;
        }
        q qVar = (q) obj;
        return qVar.a == this.a && qVar.b.equals(this.b);
    }

    public final int f() {
        return this.a;
    }

    public final String g() {
        return this.d;
    }

    public final boolean h() {
        return this.e;
    }

    public final boolean i() {
        return this.f;
    }
}
