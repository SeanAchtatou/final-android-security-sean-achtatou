package udk.android.reader.pdf.annotation;

import android.graphics.Canvas;
import android.graphics.RectF;
import udk.android.b.c;

public final class n extends q {
    public n(int i, double[] dArr) {
        super(i, dArr);
    }

    public final void a(Canvas canvas, float f) {
        RectF b = b(1.0f);
        if (b != null) {
            canvas.save();
            canvas.scale(f, f);
            if (aa()) {
                canvas.drawOval(c.a(b, 0.0f - S()), ae());
            }
            canvas.drawOval(c.a(b, 0.0f - (S() / 2.0f)), ad());
            canvas.restore();
        }
    }

    public final String k() {
        return "Circle";
    }
}
