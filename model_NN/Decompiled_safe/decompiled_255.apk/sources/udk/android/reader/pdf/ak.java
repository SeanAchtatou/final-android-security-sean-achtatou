package udk.android.reader.pdf;

import java.util.ArrayList;
import java.util.List;

public final class ak {
    private ak a;
    private List b;
    private boolean c;
    private boolean d;
    private String e;
    private int f;
    private int g;
    private String h;

    public ak(ak akVar) {
        this.a = akVar;
    }

    public final void a(int i) {
        this.f = i;
    }

    public final void a(String str) {
        this.e = str;
    }

    public final void a(ak akVar) {
        if (this.b == null) {
            this.b = new ArrayList();
        }
        this.b.add(akVar);
    }

    public final void a(boolean z) {
        this.c = z;
    }

    public final boolean a() {
        return this.a == null;
    }

    public final int b() {
        if (a()) {
            return -1;
        }
        return this.a.b() + 1;
    }

    public final void b(int i) {
        this.g = i;
    }

    public final void b(String str) {
        this.h = str;
    }

    public final void b(boolean z) {
        this.d = z;
    }

    public final List c() {
        if (a()) {
            return new ArrayList();
        }
        ak akVar = this.a;
        List c2 = akVar.c();
        c2.add(Integer.valueOf(akVar.b.indexOf(this)));
        return c2;
    }

    public final boolean d() {
        return this.c && this.b == null;
    }

    public final ak e() {
        return this.a;
    }

    public final List f() {
        return this.b;
    }

    public final boolean g() {
        return this.c;
    }

    public final boolean h() {
        return this.d;
    }

    public final String i() {
        return this.e;
    }

    public final int j() {
        return this.f;
    }

    public final int k() {
        return this.g;
    }
}
