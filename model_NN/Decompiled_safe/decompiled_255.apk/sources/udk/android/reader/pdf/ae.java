package udk.android.reader.pdf;

import android.content.Context;
import com.unidocs.commonlib.util.i;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import udk.android.reader.b.a;
import udk.android.reader.b.c;

public final class ae {
    private static ae a;
    private LinkedList b;
    private List c = new ArrayList();

    private ae() {
    }

    public static ae a() {
        if (a == null) {
            a = new ae();
        }
        return a;
    }

    private void b() {
        for (au a2 : this.c) {
            a2.a();
        }
    }

    private void c(Context context) {
        ObjectOutputStream objectOutputStream = null;
        try {
            ObjectOutputStream objectOutputStream2 = new ObjectOutputStream(new FileOutputStream(new File(a.d(context))));
            try {
                objectOutputStream2.writeObject(this.b);
                objectOutputStream2.flush();
                i.a(objectOutputStream2);
            } catch (Exception e) {
                e = e;
                objectOutputStream = objectOutputStream2;
                try {
                    c.a((Throwable) e);
                    i.a(objectOutputStream);
                } catch (Throwable th) {
                    th = th;
                    i.a(objectOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                objectOutputStream = objectOutputStream2;
                i.a(objectOutputStream);
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            c.a((Throwable) e);
            i.a(objectOutputStream);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0037  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void d(android.content.Context r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x0048 }
            java.lang.String r1 = udk.android.reader.b.a.d(r5)     // Catch:{ all -> 0x0048 }
            r0.<init>(r1)     // Catch:{ all -> 0x0048 }
            java.io.File r1 = r0.getParentFile()     // Catch:{ all -> 0x0048 }
            boolean r2 = r1.exists()     // Catch:{ all -> 0x0048 }
            if (r2 != 0) goto L_0x0017
            r1.mkdirs()     // Catch:{ all -> 0x0048 }
        L_0x0017:
            boolean r1 = r0.exists()     // Catch:{ all -> 0x0048 }
            if (r1 == 0) goto L_0x0033
            r1 = 0
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ Throwable -> 0x0040 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0040 }
            r3.<init>(r0)     // Catch:{ Throwable -> 0x0040 }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x0040 }
            java.lang.Object r0 = r2.readObject()     // Catch:{ Throwable -> 0x0053, all -> 0x0050 }
            java.util.LinkedList r0 = (java.util.LinkedList) r0     // Catch:{ Throwable -> 0x0053, all -> 0x0050 }
            r4.b = r0     // Catch:{ Throwable -> 0x0053, all -> 0x0050 }
            com.unidocs.commonlib.util.i.a(r2)     // Catch:{ all -> 0x0048 }
        L_0x0033:
            java.util.LinkedList r0 = r4.b     // Catch:{ all -> 0x0048 }
            if (r0 != 0) goto L_0x003e
            java.util.LinkedList r0 = new java.util.LinkedList     // Catch:{ all -> 0x0048 }
            r0.<init>()     // Catch:{ all -> 0x0048 }
            r4.b = r0     // Catch:{ all -> 0x0048 }
        L_0x003e:
            monitor-exit(r4)
            return
        L_0x0040:
            r0 = move-exception
        L_0x0041:
            udk.android.reader.b.c.a(r0)     // Catch:{ all -> 0x004b }
            com.unidocs.commonlib.util.i.a(r1)     // Catch:{ all -> 0x0048 }
            goto L_0x0033
        L_0x0048:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x004b:
            r0 = move-exception
        L_0x004c:
            com.unidocs.commonlib.util.i.a(r1)     // Catch:{ all -> 0x0048 }
            throw r0     // Catch:{ all -> 0x0048 }
        L_0x0050:
            r0 = move-exception
            r1 = r2
            goto L_0x004c
        L_0x0053:
            r0 = move-exception
            r1 = r2
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.pdf.ae.d(android.content.Context):void");
    }

    public final String a(int i) {
        return (String) this.b.get(i);
    }

    public final synchronized void a(Context context) {
        new File(a.d(context)).delete();
        this.b.clear();
        b();
    }

    public final void a(Context context, String str) {
        if (this.b == null) {
            d(context);
        }
        if (this.b.contains(str)) {
            this.b.remove(str);
        }
        c(context);
        b();
    }

    public final void a(au auVar) {
        if (!this.c.contains(auVar)) {
            this.c.add(auVar);
        }
    }

    public final int b(Context context) {
        if (this.b == null) {
            d(context);
        }
        return this.b.size();
    }

    public final void b(Context context, String str) {
        if (this.b == null) {
            d(context);
        }
        if (this.b.contains(str)) {
            this.b.remove(str);
        }
        this.b.addFirst(str);
        if (this.b.size() > 30) {
            this.b.removeLast();
        }
        c(context);
        b();
    }

    public final void b(au auVar) {
        this.c.remove(auVar);
    }
}
