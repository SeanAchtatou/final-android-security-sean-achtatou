package udk.android.reader.pdf;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.Toast;

final class u implements DialogInterface.OnClickListener {
    private /* synthetic */ a a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ Context c;
    private final /* synthetic */ String d;

    u(a aVar, EditText editText, Context context, String str) {
        this.a = aVar;
        this.b = editText;
        this.c = context;
        this.d = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Bookmark bookmark = new Bookmark();
        bookmark.setPage(PDF.a().E());
        bookmark.setDesc(this.b.getText().toString());
        this.a.a(this.c, bookmark);
        Toast.makeText(this.c, this.d, 0).show();
        this.a.c = null;
    }
}
