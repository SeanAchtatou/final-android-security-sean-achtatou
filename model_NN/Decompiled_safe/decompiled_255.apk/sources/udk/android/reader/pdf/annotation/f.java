package udk.android.reader.pdf.annotation;

import android.graphics.Path;

final class f {
    private float a;
    private float b;
    private float c;
    private Path d;
    private /* synthetic */ t e;

    f(t tVar) {
        this.e = tVar;
    }

    public final float a() {
        return this.a;
    }

    public final void a(float f) {
        this.a = f;
    }

    public final void a(Path path) {
        this.d = path;
    }

    public final float b() {
        return this.b;
    }

    public final void b(float f) {
        this.b = f;
    }

    public final float c() {
        return this.c;
    }

    public final void c(float f) {
        this.c = f;
    }

    public final Path d() {
        return this.d;
    }
}
