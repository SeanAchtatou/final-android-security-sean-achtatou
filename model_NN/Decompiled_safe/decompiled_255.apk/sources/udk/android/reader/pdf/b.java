package udk.android.reader.pdf;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import udk.android.b.m;
import udk.android.reader.b.c;
import udk.android.reader.view.pdf.hx;

public final class b implements ar {
    private static b a;
    /* access modifiers changed from: private */
    public ao b;
    private List c = new ArrayList();
    /* access modifiers changed from: private */
    public Map d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public List f;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public AlertDialog h;
    /* access modifiers changed from: private */
    public AlertDialog i;
    /* access modifiers changed from: private */
    public boolean j;
    private String k;
    private boolean l;
    private boolean m;
    private int n;

    private b() {
        PDF.a().a(this);
    }

    public static b a() {
        if (a == null) {
            a = new b();
        }
        return a;
    }

    static /* synthetic */ void a(b bVar) {
        for (at o : bVar.c) {
            o.o();
        }
    }

    static /* synthetic */ void a(b bVar, String str, boolean z, boolean z2, int i2) {
        bVar.k();
        bVar.k = str;
        bVar.l = z;
        bVar.m = z2;
        bVar.n = i2;
        bVar.d = new HashMap();
        bVar.e = 0;
        bVar.f = new ArrayList();
        bVar.g = 0;
        bVar.b = new ao(bVar, str, z, z2, i2);
        bVar.b.setDaemon(true);
        bVar.b.setPriority(2);
        bVar.b.start();
    }

    static /* synthetic */ void a(b bVar, d dVar) {
        for (at a2 : bVar.c) {
            a2.a(dVar);
        }
    }

    static /* synthetic */ void d(b bVar) {
        for (at p : bVar.c) {
            p.p();
        }
    }

    private void k() {
        if (this.b != null) {
            this.b.a = false;
            try {
                this.b.join();
            } catch (Throwable th) {
                c.a(th);
            }
        }
    }

    public final List a(int i2) {
        if (this.f == null || i2 < 0 || i2 > this.f.size() - 1) {
            return null;
        }
        return b(((Integer) this.f.get(i2)).intValue());
    }

    public final void a(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, Drawable drawable) {
        this.j = true;
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        ArrayList arrayList = new ArrayList();
        arrayList.add(str4);
        arrayList.add(str5);
        arrayList.add(str6);
        CheckBox checkBox = new CheckBox(context);
        CheckBox checkBox2 = new CheckBox(context);
        CheckBox checkBox3 = new CheckBox(context);
        LinearLayout linearLayout2 = new LinearLayout(context);
        LinearLayout linearLayout3 = new LinearLayout(context);
        LinearLayout linearLayout4 = new LinearLayout(context);
        TextView textView = new TextView(context);
        boolean z = str == null && str2 == null && str3 == null && str4 == null && str5 == null && str6 == null && drawable == null;
        if (z) {
            linearLayout2.setVisibility(8);
            linearLayout3.setVisibility(8);
            linearLayout4.setVisibility(8);
        }
        EditText editText = new EditText(context);
        editText.setInputType(1);
        editText.setImeOptions(3);
        editText.setOnEditorActionListener(new n(this, editText, z, checkBox, checkBox2, arrayList, textView, str3));
        editText.setSingleLine();
        if (com.unidocs.commonlib.util.b.a(this.k)) {
            editText.setText(this.k);
            editText.setSelectAllOnFocus(true);
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(m.a(context, 5), m.a(context, 5), m.a(context, 5), 0);
        linearLayout.addView(editText, layoutParams);
        linearLayout2.setGravity(16);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        linearLayout2.setOrientation(0);
        linearLayout.addView(linearLayout2, layoutParams2);
        checkBox.setChecked(this.l);
        linearLayout2.addView(checkBox, new LinearLayout.LayoutParams(-2, -2));
        TextView textView2 = new TextView(context);
        textView2.setOnClickListener(new j(this, checkBox));
        textView2.setText(str);
        linearLayout2.addView(textView2, new LinearLayout.LayoutParams(-2, -2));
        linearLayout3.setGravity(16);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
        linearLayout3.setOrientation(0);
        linearLayout.addView(linearLayout3, layoutParams3);
        checkBox2.setChecked(this.m);
        linearLayout3.addView(checkBox2, new LinearLayout.LayoutParams(-2, -2));
        TextView textView3 = new TextView(context);
        textView3.setOnClickListener(new k(this, checkBox2));
        textView3.setText(str2);
        linearLayout3.addView(textView3, new LinearLayout.LayoutParams(-2, -2));
        l lVar = new l(this, textView, str3, context, arrayList);
        linearLayout4.setGravity(16);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, -2);
        linearLayout4.setOrientation(0);
        linearLayout.addView(linearLayout4, layoutParams4);
        checkBox3.setOnTouchListener(new m(this, lVar));
        if (drawable != null) {
            checkBox3.setButtonDrawable(drawable);
        }
        checkBox3.setChecked(true);
        linearLayout4.addView(checkBox3, new LinearLayout.LayoutParams(-2, -2));
        textView.setOnClickListener(new f(this, lVar));
        textView.setText(String.valueOf(str3) + " " + ((String) arrayList.get(this.n)));
        linearLayout4.addView(textView, new LinearLayout.LayoutParams(-2, -2));
        ScrollView scrollView = new ScrollView(context);
        scrollView.addView(linearLayout);
        this.h = new AlertDialog.Builder(context).setTitle(str7).setView(scrollView).setPositiveButton(str8, new g(this, editText, z, checkBox, checkBox2, arrayList, textView, str3)).setNegativeButton(str9, new h(this)).show();
        this.h.setOnDismissListener(new i(this));
        editText.postDelayed(new v(this, context, editText), 100);
    }

    public final void a(ah ahVar) {
    }

    public final void a(at atVar) {
        if (!this.c.contains(atVar)) {
            this.c.add(atVar);
        }
    }

    public final List b(int i2) {
        if (this.d == null) {
            return null;
        }
        return (List) this.d.get(Integer.valueOf(i2));
    }

    public final void b(ah ahVar) {
    }

    public final void b(at atVar) {
        this.c.remove(atVar);
    }

    public final boolean b() {
        return this.j;
    }

    public final void c() {
        k();
        this.d = null;
        this.f = null;
        this.g = 0;
        for (at q : this.c) {
            q.q();
        }
        hx.a().p();
    }

    public final boolean d() {
        return this.b != null;
    }

    public final int e() {
        return this.g;
    }

    public final void f() {
    }

    public final void g() {
        c();
    }

    public final void h() {
    }

    public final int i() {
        if (com.unidocs.commonlib.util.b.b((Collection) this.f)) {
            return 0;
        }
        return this.f.size();
    }

    public final int j() {
        if (this.d == null) {
            return 0;
        }
        return this.d.size();
    }
}
