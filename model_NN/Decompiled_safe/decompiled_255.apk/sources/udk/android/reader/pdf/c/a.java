package udk.android.reader.pdf.c;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import com.unidocs.commonlib.util.b;
import java.util.HashMap;
import udk.android.reader.b.c;
import udk.android.reader.pdf.annotation.p;

public class a {
    private static a a;
    private Paint b = new Paint();
    private HashMap c;

    private a() {
        this.b.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY));
        this.c = new HashMap();
    }

    private Bitmap a(String str) {
        Bitmap bitmap = (Bitmap) this.c.get(str);
        if (bitmap != null) {
            return bitmap;
        }
        try {
            bitmap = BitmapFactory.decodeStream(a.class.getResource("/udk/android/reader/pdf/appearance/" + str + ".png").openStream());
            this.c.put(str, bitmap);
            return bitmap;
        } catch (Exception e) {
            Exception exc = e;
            Bitmap bitmap2 = bitmap;
            c.a((Throwable) exc);
            return bitmap2;
        }
    }

    public static a a() {
        if (a == null) {
            a = new a();
        }
        return a;
    }

    public final Bitmap a(String str, int i) {
        if (!b.a(str, p.a, true)) {
            return null;
        }
        Bitmap a2 = a(str);
        if (a2 == null) {
            return null;
        }
        Bitmap copy = a2.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(copy);
        this.b.setColor(i);
        canvas.drawRect(new Rect(0, 0, a2.getWidth(), a2.getHeight()), this.b);
        return copy;
    }
}
