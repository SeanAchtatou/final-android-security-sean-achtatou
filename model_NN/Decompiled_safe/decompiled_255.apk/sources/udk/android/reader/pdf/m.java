package udk.android.reader.pdf;

import android.view.MotionEvent;
import android.view.View;

final class m implements View.OnTouchListener {
    private /* synthetic */ b a;
    private final /* synthetic */ Runnable b;

    m(b bVar, Runnable runnable) {
        this.a = bVar;
        this.b = runnable;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 1) {
            this.b.run();
        }
        return true;
    }
}
