package udk.android.reader.pdf.annotation;

import android.graphics.Canvas;
import com.unidocs.commonlib.util.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class z extends Annotation {
    private List a = new ArrayList();

    public z(int i) {
        super(i, null);
    }

    public final void a(Canvas canvas, float f) {
    }

    public final void a(z zVar) {
        this.a.add(zVar);
    }

    public final boolean a() {
        return b.a((Collection) this.a);
    }

    public final List c() {
        return this.a;
    }

    public final String k() {
        return "Reply";
    }
}
