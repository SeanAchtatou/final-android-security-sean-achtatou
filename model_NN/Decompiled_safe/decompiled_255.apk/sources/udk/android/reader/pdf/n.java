package udk.android.reader.pdf;

import android.view.KeyEvent;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import java.util.List;

final class n implements TextView.OnEditorActionListener {
    private /* synthetic */ b a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ boolean c;
    private final /* synthetic */ CheckBox d;
    private final /* synthetic */ CheckBox e;
    private final /* synthetic */ List f;
    private final /* synthetic */ TextView g;
    private final /* synthetic */ String h;

    n(b bVar, EditText editText, boolean z, CheckBox checkBox, CheckBox checkBox2, List list, TextView textView, String str) {
        this.a = bVar;
        this.b = editText;
        this.c = z;
        this.d = checkBox;
        this.e = checkBox2;
        this.f = list;
        this.g = textView;
        this.h = str;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (keyEvent != null && keyEvent.getAction() != 1) {
            return true;
        }
        if (this.a.h != null && this.a.h.isShowing()) {
            this.a.h.dismiss();
        }
        String editable = this.b.getText().toString();
        if (this.c) {
            b.a(this.a, editable, false, false, 0);
        } else {
            b.a(this.a, editable, this.d.isChecked(), this.e.isChecked(), this.f.indexOf(this.g.getText().toString().replaceAll(this.h, "").trim()));
        }
        this.a.h = null;
        this.a.j = false;
        return true;
    }
}
