package udk.android.reader.pdf;

import android.app.Activity;
import android.app.AlertDialog;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

final class o implements Runnable {
    private /* synthetic */ ab a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ String c;
    private final /* synthetic */ AlertDialog d;
    private final /* synthetic */ ProgressBar e;
    private final /* synthetic */ ListView f;
    private final /* synthetic */ BaseAdapter g;

    o(ab abVar, Activity activity, String str, AlertDialog alertDialog, ProgressBar progressBar, ListView listView, BaseAdapter baseAdapter) {
        this.a = abVar;
        this.b = activity;
        this.c = str;
        this.d = alertDialog;
        this.e = progressBar;
        this.f = listView;
        this.g = baseAdapter;
    }

    public final void run() {
        if (!this.a.a.e.g()) {
            Toast.makeText(this.b, this.c, 0).show();
            this.d.dismiss();
        } else if (this.d.isShowing()) {
            this.e.setVisibility(8);
            this.f.setAdapter((ListAdapter) this.g);
        }
    }
}
