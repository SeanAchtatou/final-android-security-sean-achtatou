package udk.android.reader.pdf;

import android.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;

final class q implements AdapterView.OnItemClickListener {
    private /* synthetic */ a a;
    private final /* synthetic */ AlertDialog b;
    private final /* synthetic */ ListAdapter c;
    private final /* synthetic */ af d;

    q(a aVar, AlertDialog alertDialog, ListAdapter listAdapter, af afVar) {
        this.a = aVar;
        this.b = alertDialog;
        this.c = listAdapter;
        this.d = afVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.b.dismiss();
        ac acVar = new ac();
        acVar.a = (Bookmark) this.c.getItem(i);
        this.d.a(acVar);
    }
}
