package udk.android.reader.pdf.b;

import android.app.AlertDialog;
import android.content.Context;
import com.unidocs.commonlib.util.b;
import com.unidocs.commonlib.util.b.a;
import java.util.Collection;
import java.util.List;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.ah;
import udk.android.reader.pdf.annotation.ab;
import udk.android.reader.pdf.annotation.i;
import udk.android.reader.pdf.annotation.s;
import udk.android.reader.pdf.annotation.v;
import udk.android.reader.pdf.ar;

public final class d implements ar {
    private static d a;
    /* access modifiers changed from: private */
    public PDF b = PDF.a();
    private k c;
    private a d;

    private d() {
        this.b.a(this);
    }

    public static d a() {
        if (a == null) {
            a = new d();
        }
        return a;
    }

    /* access modifiers changed from: private */
    public void d() {
        new p(this).start();
    }

    public final q a(int i) {
        return this.c.b(i);
    }

    public final void a(ah ahVar) {
    }

    public final void a(udk.android.reader.pdf.annotation.d dVar) {
        ab abVar = new ab();
        abVar.b = this.b.a(dVar);
        if (b.a((Collection) abVar.b)) {
            s.a().a(abVar);
        }
        if (udk.android.reader.b.a.L) {
            d();
        }
    }

    public final void a(i iVar, Context context) {
        l lVar = (l) iVar.a();
        List a2 = lVar.a();
        String[] strArr = new String[a2.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= strArr.length) {
                new AlertDialog.Builder(context).setItems(strArr, new o(this, a2, lVar)).show();
                return;
            } else {
                strArr[i2] = ((f) a2.get(i2)).a();
                i = i2 + 1;
            }
        }
    }

    public final void a(v vVar) {
        ab abVar = new ab();
        abVar.b = this.b.a(vVar);
        if (b.a((Collection) abVar.b)) {
            s.a().a(abVar);
        }
        if (udk.android.reader.b.a.L) {
            d();
        }
    }

    public final void b(ah ahVar) {
    }

    public final boolean b() {
        return this.c != null && this.c.d();
    }

    public final void c() {
        this.c = this.b.n();
    }

    public final void f() {
    }

    public final void g() {
    }

    public final void h() {
        this.c = null;
        this.d = null;
    }
}
