package udk.android.reader.pdf.annotation;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import java.io.File;
import udk.android.reader.b.c;
import udk.android.reader.pdf.b.e;
import udk.android.reader.pdf.b.q;

public class v extends Annotation implements e {
    private static final Paint a;
    private int b;
    private int c;
    private q d;
    private Paint e;

    static {
        Paint paint = new Paint(1);
        a = paint;
        paint.setColor(-16777216);
        a.setStrokeWidth(2.0f);
        a.setTextSize(40.0f);
        try {
            File file = new File("/system/fonts/DroidSansFallback.ttf");
            if (file.exists()) {
                a.setTypeface(Typeface.createFromFile(file));
            }
        } catch (Throwable th) {
            c.a(th);
        }
    }

    public v(int i, double[] dArr, q qVar) {
        super(i, dArr);
        this.d = qVar;
    }

    public final q a() {
        return this.d;
    }

    public final void a(Canvas canvas, float f) {
        float f2;
        String str;
        float f3;
        if (ab()) {
            if (this.e == null) {
                this.e = new Paint(1);
                this.e.setColor(L());
                this.e.setStyle(Paint.Style.STROKE);
                this.e.setStrokeWidth(2.0f);
            }
            canvas.drawRect(b(f), this.e);
        }
        if (this.b > 0) {
            int i = this.c;
            if (i == 3) {
                RectF b2 = udk.android.b.c.b(b(f), 0.9f);
                Rect rect = new Rect(0, 0, 40, 40);
                float c2 = udk.android.b.c.c(new RectF(rect), b2);
                canvas.save();
                canvas.translate(b2.centerX() - ((((float) rect.width()) * c2) / 2.0f), b2.centerY() - ((((float) rect.height()) * c2) / 2.0f));
                canvas.scale(c2, c2);
                canvas.drawLine((float) rect.left, (float) rect.top, (float) rect.right, (float) rect.bottom, a);
                canvas.drawLine((float) rect.left, (float) rect.bottom, (float) rect.right, (float) rect.top, a);
                canvas.restore();
                return;
            }
            float f4 = 0.0f;
            switch (i) {
                case 0:
                    f4 = -9.0f;
                    str = "✓";
                    f3 = 0.9f;
                    f2 = -3.0f;
                    break;
                case 1:
                    f4 = -7.0f;
                    str = "✔";
                    f3 = 0.9f;
                    f2 = -3.0f;
                    break;
                case 2:
                    f2 = 6.0f;
                    f4 = -3.0f;
                    str = "●";
                    f3 = 0.8f;
                    break;
                case 3:
                default:
                    f3 = 1.0f;
                    str = null;
                    f2 = 0.0f;
                    break;
                case 4:
                    str = "◆";
                    f3 = 0.8f;
                    f2 = 5.0f;
                    f4 = -2.0f;
                    break;
                case 5:
                    f2 = 6.0f;
                    f4 = -3.0f;
                    str = "■";
                    f3 = 0.8f;
                    break;
                case 6:
                    f2 = -2.0f;
                    f4 = -3.0f;
                    str = "★";
                    f3 = 0.8f;
                    break;
            }
            if (str != null) {
                RectF b3 = udk.android.b.c.b(b(f), f3);
                Rect rect2 = new Rect();
                a.getTextBounds(str, 0, 1, rect2);
                float c3 = udk.android.b.c.c(new RectF(rect2), b3);
                canvas.save();
                canvas.translate(b3.centerX() - ((((float) rect2.width()) * c3) / 2.0f), b3.centerY() - ((((float) rect2.height()) * c3) / 2.0f));
                canvas.scale(c3, c3);
                canvas.drawText(str, f4, ((float) rect2.height()) + f2, a);
                canvas.restore();
            }
        }
    }

    public final void b(int i) {
        this.b = i;
    }

    public final void c(int i) {
        this.c = i;
    }

    public boolean h() {
        return true;
    }

    public final String k() {
        return "Widget";
    }

    public final boolean l() {
        return true;
    }
}
