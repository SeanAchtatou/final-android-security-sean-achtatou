package udk.android.reader.pdf.annotation;

import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;
import udk.android.b.c;
import udk.android.reader.b.b;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.annotation.Annotation;

public final class w extends q {
    private double[] a;
    private PointF b;
    private PointF c;
    private String d;
    private String e;

    public w(int i, double[] dArr, double[] dArr2, String str, String str2) {
        super(i, dArr);
        a(dArr2);
        this.d = str;
        this.e = str2;
    }

    private void a(Canvas canvas, String str, PointF pointF, PointF pointF2) {
        if ("OpenArrow".equals(str)) {
            canvas.save();
            float a2 = (float) c.a(Math.atan2((double) (pointF2.y - pointF.y), (double) (pointF2.x - pointF.x)));
            canvas.translate(pointF.x, pointF.y);
            canvas.rotate(a2 - 30.0f);
            canvas.drawLine(0.0f, 0.0f, S() * 6.0f, 0.0f, ad());
            canvas.rotate(60.0f);
            canvas.drawLine(0.0f, 0.0f, S() * 6.0f, 0.0f, ad());
            canvas.restore();
        } else if ("Circle".equals(str)) {
            float S = (S() * 6.0f) / 2.0f;
            if (aa()) {
                canvas.drawCircle(pointF.x, pointF.y, S - (S() / 2.0f), ae());
            }
            canvas.drawCircle(pointF.x, pointF.y, S, ad());
        } else if ("Square".equals(str)) {
            float S2 = (S() * 6.0f) / 2.0f;
            RectF rectF = new RectF(pointF.x - S2, pointF.y - S2, pointF.x + S2, S2 + pointF.y);
            if (aa()) {
                canvas.drawRect(c.a(rectF, -(S() / 2.0f)), ae());
            }
            canvas.drawRect(rectF, ad());
        }
    }

    public static String b(String str) {
        if ("None".equals(str)) {
            return b.af;
        }
        if ("Square".equals(str)) {
            return b.ag;
        }
        if ("Circle".equals(str)) {
            return b.ah;
        }
        if ("OpenArrow".equals(str)) {
            return b.ai;
        }
        return null;
    }

    public static String h(String str) {
        if (b.af.equals(str)) {
            return "None";
        }
        if (b.ag.equals(str)) {
            return "Square";
        }
        if (b.ah.equals(str)) {
            return "Circle";
        }
        if (b.ai.equals(str)) {
            return "OpenArrow";
        }
        return null;
    }

    public final Annotation.TransformingType a(PointF pointF, float f) {
        float u = u();
        RectF b2 = b(f);
        if (j()) {
            if (c.a(pointF, c(f)) <= u) {
                return Annotation.TransformingType.START_HEAD;
            }
            if (c.a(pointF, d(f)) <= u) {
                return Annotation.TransformingType.END_HEAD;
            }
        }
        if (!i() || !b2.contains(pointF.x, pointF.y)) {
            return null;
        }
        return Annotation.TransformingType.MOVE;
    }

    public final void a(Canvas canvas, float f) {
        if (this.b != null && this.c != null) {
            canvas.save();
            canvas.scale(f, f);
            canvas.drawLine(this.b.x, this.b.y, this.c.x, this.c.y, ad());
            a(canvas, this.d, this.b, this.c);
            a(canvas, this.e, this.c, this.b);
            canvas.restore();
        }
    }

    public final void a(PointF pointF) {
        this.b = new PointF(pointF.x / 1.0f, pointF.y / 1.0f);
    }

    public final void a(double[] dArr) {
        this.a = dArr;
        if (dArr != null) {
            int[] a2 = PDF.a().a(ag(), 1.0f, dArr);
            this.b = new PointF((float) a2[0], (float) a2[1]);
            this.c = new PointF((float) a2[2], (float) a2[3]);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.PDF.a(int, float, int[]):double[]
     arg types: [int, int, int[]]
     candidates:
      udk.android.reader.pdf.PDF.a(int, float, int):java.util.List
      udk.android.reader.pdf.PDF.a(android.graphics.Canvas, int, float):void
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.b.k, java.util.Map, udk.android.reader.pdf.b.q):void
      udk.android.reader.pdf.PDF.a(int, int, int):int
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.annotation.ac, udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):boolean
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.y, java.lang.String, java.io.InputStream):boolean
      udk.android.reader.pdf.PDF.a(int, float, android.graphics.RectF):double[]
      udk.android.reader.pdf.PDF.a(int, float, double[]):int[]
      udk.android.reader.pdf.PDF.a(int, float, int[]):double[] */
    public final double[] a() {
        if (this.a != null) {
            return this.a;
        }
        return PDF.a().a(ag(), 1.0f, new int[]{(int) this.b.x, (int) this.b.y, (int) this.c.x, (int) this.c.y});
    }

    public final void b(float f, float f2, float f3) {
        super.b(f, f2, f3);
        this.b = new PointF(f / f3, f2 / f3);
        this.c = new PointF(f / f3, f2 / f3);
    }

    public final void b(PointF pointF) {
        this.c = new PointF(pointF.x / 1.0f, pointF.y / 1.0f);
    }

    public final PointF c(float f) {
        return new PointF(this.b.x * f, this.b.y * f);
    }

    public final String c() {
        return this.d;
    }

    public final PointF d(float f) {
        return new PointF(this.c.x * f, this.c.y * f);
    }

    public final String d() {
        return this.e;
    }

    public final void d(float f, float f2, float f3) {
        super.d(f, f2, f3);
    }

    public final void e(float f, float f2, float f3) {
        super.e(f, f2, f3);
        this.c = new PointF(f / f3, f2 / f3);
    }

    public final void i(String str) {
        this.d = str;
    }

    public final void j(String str) {
        this.e = str;
    }

    public final String k() {
        return "Line";
    }
}
