package udk.android.reader.pdf.annotation;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import java.util.Collection;
import java.util.List;
import udk.android.b.c;
import udk.android.e.a;
import udk.android.reader.c.a.b;
import udk.android.reader.c.a.d;
import udk.android.reader.pdf.PDF;

public class e extends q implements a {
    private b a;
    private Paint b = new Paint(1);
    private Paint c;
    private float d = this.b.getTextSize();
    private float e = 1.3f;
    private int f;
    private boolean g;
    private int h;
    private boolean i;

    public e(int i2, double[] dArr) {
        super(i2, dArr);
        if (udk.android.reader.b.a.aK) {
            this.c = new Paint(1);
            this.c.setColor(-7829368);
        }
        this.f = 0;
        this.g = false;
        b(true);
    }

    private Paint.Align a() {
        switch (this.f) {
            case 0:
                return Paint.Align.LEFT;
            case 1:
                return Paint.Align.CENTER;
            case 2:
                return Paint.Align.RIGHT;
            default:
                return Paint.Align.LEFT;
        }
    }

    private float ah() {
        RectF k = k(1.0f);
        if (k == null) {
            return 1.0f;
        }
        return k.width();
    }

    private RectF h(int i2) {
        float f2;
        List a2 = this.a.a();
        for (int i3 = 0; i3 < a2.size(); i3++) {
            float a3 = this.a.a(i3, this.e);
            float textSize = a3 - this.b.getTextSize();
            d dVar = (d) a2.get(i3);
            if (i2 >= dVar.a() && i2 < dVar.b()) {
                List c2 = dVar.c();
                if (com.unidocs.commonlib.util.b.b((Collection) c2)) {
                    float[] fArr = new float[1];
                    this.b.getTextWidths(" ", fArr);
                    float a4 = ((float) (i2 - dVar.a())) * fArr[0];
                    return new RectF(a4, textSize, fArr[0] + a4, a3);
                }
                int i4 = 0;
                while (i4 < c2.size()) {
                    udk.android.reader.c.a.a aVar = (udk.android.reader.c.a.a) c2.get(i4);
                    if (i2 >= aVar.a() && i2 < aVar.b()) {
                        float c3 = aVar.c();
                        if (i2 - aVar.a() > 0) {
                            float[] fArr2 = new float[(i2 - aVar.a())];
                            int textWidths = this.b.getTextWidths(this.a.b(), aVar.a(), i2, fArr2);
                            float f3 = c3;
                            for (int i5 = 0; i5 < textWidths; i5++) {
                                f3 += fArr2[i5];
                            }
                            f2 = f3;
                        } else {
                            f2 = c3;
                        }
                        float[] fArr3 = new float[1];
                        this.b.getTextWidths(this.a.b(), i2, i2 + 1, fArr3);
                        return new RectF(f2, textSize, fArr3[0] + f2, a3);
                    } else if (i2 < aVar.a()) {
                        float[] fArr4 = new float[1];
                        this.b.getTextWidths(" ", fArr4);
                        float c4 = aVar.c() - (((float) (aVar.a() - i2)) * fArr4[0]);
                        return new RectF(c4, textSize, fArr4[0] + c4, a3);
                    } else if (i4 >= c2.size() - 1) {
                        float c5 = aVar.c();
                        if (aVar.b() - aVar.a() > 0) {
                            float[] fArr5 = new float[(i2 - aVar.a())];
                            int textWidths2 = this.b.getTextWidths(this.a.b(), aVar.a(), aVar.b(), fArr5);
                            float f4 = c5;
                            for (int i6 = 0; i6 < textWidths2; i6++) {
                                f4 += fArr5[i6];
                            }
                            c5 = f4;
                        }
                        float[] fArr6 = new float[1];
                        this.b.getTextWidths(" ", fArr6);
                        float b2 = (((float) (i2 - aVar.b())) * fArr6[0]) + c5;
                        return new RectF(b2, textSize, fArr6[0] + b2, a3);
                    } else {
                        i4++;
                    }
                }
                continue;
            } else if (i2 < dVar.a()) {
                return new RectF(0.0f, textSize, 1.0f, a3);
            } else {
                if (i3 >= a2.size() - 1) {
                    float a5 = this.a.a(i3 + 1, this.e);
                    return new RectF(0.0f, a5 - this.b.getTextSize(), 1.0f, a5);
                }
            }
        }
        return null;
    }

    private static String h(String str) {
        return str.length() < 2 ? str : String.valueOf(str.substring(0, str.length() - 1).replaceAll("[\\S]", "*")) + str.charAt(str.length() - 1);
    }

    private float j(float f2) {
        float S = S() * f2;
        return S + S;
    }

    private RectF k(float f2) {
        RectF b2 = b(f2);
        if (b2 != null) {
            return c.a(b2, 0.0f - j(f2));
        }
        return null;
    }

    public final int a(float f2, float f3, float f4) {
        RectF b2 = b(1.0f);
        int length = M().length();
        int i2 = -1;
        float f5 = Float.MAX_VALUE;
        for (int i3 = 0; i3 < length; i3++) {
            RectF h2 = h(i3);
            h2.offset(b2.left, b2.top);
            c.c(h2, f2);
            if (h2.contains(f3, f4)) {
                return i3;
            }
            float a2 = c.a(h2.centerX(), h2.centerY(), f3, f4);
            if (a2 < f5) {
                i2 = i3;
                f5 = a2;
            }
        }
        return i2;
    }

    public final void a(int i2) {
        super.a(i2);
        this.b.setColor(i2);
    }

    public void a(Canvas canvas, float f2) {
        String str;
        boolean z;
        RectF rectF;
        RectF rectF2;
        RectF b2 = b(1.0f);
        if (b2 != null) {
            canvas.save();
            canvas.scale(f2, f2);
            if (aa()) {
                canvas.drawRect(S() > 0.0f ? c.a(b2, 0.0f - S()) : b2, ae());
            }
            if (S() > 0.0f || af() == null) {
                canvas.drawRect(c.a(b2, 0.0f - (S() / 2.0f)), ad());
            }
            String M = M();
            if (com.unidocs.commonlib.util.b.a(M)) {
                RectF k = k(1.0f);
                boolean j = j();
                if (c()) {
                    str = h(M);
                    z = true;
                } else {
                    boolean z2 = j;
                    str = M;
                    z = z2;
                }
                if (z && s.a().c() == this) {
                    this.a = new b(str, this.b, k.width(), a());
                }
                canvas.save();
                canvas.clipRect(k);
                if (!udk.android.reader.b.a.aK || !this.i) {
                    rectF = null;
                } else {
                    if (M().length() - this.h != 0) {
                        rectF2 = h((M().length() - this.h) - 1);
                        rectF2.left = rectF2.right - 1.0f;
                    } else {
                        float a2 = this.a.a(0, this.e);
                        rectF2 = new RectF(0.0f, a2 - this.b.getTextSize(), 1.0f, a2);
                    }
                    rectF2.bottom += rectF2.height() / 6.0f;
                    rectF2.offset(k.left, k.top);
                    rectF = rectF2;
                }
                if (this.g && !d()) {
                    canvas.translate(0.0f, (k.height() - this.a.a(this.e)) / 2.0f);
                } else if (rectF != null && rectF.bottom > k.bottom) {
                    canvas.translate(0.0f, k.bottom - rectF.bottom);
                }
                List a3 = this.a.a();
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= a3.size()) {
                        break;
                    }
                    float f3 = k.left;
                    float a4 = k.top + this.a.a(i3, this.e);
                    for (udk.android.reader.c.a.a aVar : ((d) a3.get(i3)).c()) {
                        canvas.drawText(str, aVar.a(), aVar.b(), f3 + aVar.c(), a4, this.b);
                    }
                    i2 = i3 + 1;
                }
                if (rectF != null) {
                    try {
                        canvas.drawRect(rectF, this.c);
                        float height = rectF.height() / 5.0f;
                        canvas.drawRect(new RectF(rectF.left - (height / 2.0f), rectF.top, rectF.right + (height / 2.0f), rectF.top + 1.0f), this.c);
                        canvas.drawRect(new RectF(rectF.left - (height / 2.0f), rectF.bottom - 1.0f, (height / 2.0f) + rectF.right, rectF.bottom), this.c);
                    } catch (Exception e2) {
                        udk.android.reader.b.c.a((Throwable) e2);
                    }
                }
                canvas.restore();
            }
            canvas.restore();
        }
    }

    public final void a(String str) {
        String str2 = str == null ? "" : str;
        super.a(str2);
        if (c()) {
            str2 = h(str2);
        }
        float ah = ah();
        this.a = new b(str2, this.b, ah, a());
        if (this.g) {
            float F = PDF.a().F();
            boolean z = false;
            while (true) {
                if (c(F) || (!d() && this.a.a().size() > 1)) {
                    this.b.setTextSize(this.b.getTextSize() - 0.1f);
                    this.a = new b(str2, this.b, ah, a());
                    z = true;
                }
            }
            if (!z) {
                while (!c(F) && this.b.getTextSize() < this.d && (d() || this.a.a().size() <= 1)) {
                    this.b.setTextSize(this.b.getTextSize() + 0.1f);
                    this.a = new b(str2, this.b, ah, a());
                }
                while (true) {
                    if (c(F) || (!d() && this.a.a().size() > 1)) {
                        this.b.setTextSize(this.b.getTextSize() - 0.1f);
                        this.a = new b(str2, this.b, ah, a());
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public final void a(double[] dArr) {
        this.b.setColor(c.a(255, (int) (dArr[0] * 255.0d), (int) (dArr[1] * 255.0d), (int) (dArr[2] * 255.0d)));
    }

    public final void b(float f2, float f3, float f4) {
        b(false);
        super.b(f2, f3, f4);
    }

    public final void b(int i2) {
        this.h = i2;
    }

    public final void b(String str) {
        a(str);
    }

    public final void c(int i2) {
        this.f = i2;
    }

    public boolean c() {
        return false;
    }

    public final boolean c(float f2) {
        return d(f2).height() > b(f2).height();
    }

    public final RectF d(float f2) {
        RectF k = k(f2);
        k.bottom = (this.a.a(this.e) * f2) + k.top + 0.0f;
        return c.a(k, j(f2));
    }

    public final void d(float f2, float f3, float f4) {
        super.d(f2, f3, f4);
        b(true);
    }

    public boolean d() {
        return true;
    }

    public int e() {
        return 0;
    }

    public final void e(float f2) {
        this.d = f2;
        this.b.setTextSize(f2);
        this.a = new b(M(), this.b, ah(), a());
    }

    public final float f() {
        return this.d;
    }

    public final String g() {
        return M();
    }

    public String k() {
        return "FreeText";
    }

    public final int m() {
        return this.h;
    }

    public final void n() {
        this.i = true;
    }

    public final void o() {
        this.i = false;
    }

    public final void p() {
        this.g = true;
    }
}
