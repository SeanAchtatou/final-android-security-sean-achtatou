package udk.android.reader.pdf.a;

import android.graphics.Path;
import android.graphics.RectF;
import com.unidocs.commonlib.util.b;
import udk.android.reader.pdf.PDF;

public final class a extends c {
    public a(int i, double[] dArr) {
        g(i);
        b(dArr);
    }

    public final Path a(float f) {
        double[] af = af();
        if (b.b(af)) {
            return null;
        }
        int[] a = PDF.a().a(ag(), f, af);
        Path path = new Path();
        path.moveTo((float) a[0], (float) a[1]);
        for (int i = 1; i < a.length / 2; i++) {
            path.lineTo((float) a[i * 2], (float) a[(i * 2) + 1]);
        }
        path.lineTo((float) a[0], (float) a[1]);
        return path;
    }

    public final RectF b(float f) {
        Path a = a(f);
        if (a == null) {
            return null;
        }
        RectF rectF = new RectF();
        a.computeBounds(rectF, true);
        return rectF;
    }
}
