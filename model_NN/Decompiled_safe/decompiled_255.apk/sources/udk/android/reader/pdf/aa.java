package udk.android.reader.pdf;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import java.util.ArrayList;

final class aa implements AdapterView.OnItemLongClickListener {
    final /* synthetic */ w a;
    private final /* synthetic */ BaseAdapter b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ String e;
    private final /* synthetic */ Activity f;
    private final /* synthetic */ as g;
    private final /* synthetic */ AlertDialog h;

    aa(w wVar, BaseAdapter baseAdapter, String str, String str2, String str3, Activity activity, as asVar, AlertDialog alertDialog) {
        this.a = wVar;
        this.b = baseAdapter;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = activity;
        this.g = asVar;
        this.h = alertDialog;
    }

    public final boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        ak akVar = (ak) this.b.getItem(i);
        ak e2 = akVar.g() ? akVar : !akVar.e().a() ? akVar.e() : null;
        ArrayList arrayList = new ArrayList();
        arrayList.add(this.c);
        if (e2 != null) {
            if (e2.h()) {
                arrayList.add(this.d);
            } else {
                arrayList.add(this.e);
            }
        }
        new AlertDialog.Builder(this.f).setTitle(akVar.i()).setItems((CharSequence[]) arrayList.toArray(new String[0]), new aw(this, arrayList, this.e, e2, this.b, this.d, this.c, akVar, this.g, this.h)).show();
        return true;
    }
}
