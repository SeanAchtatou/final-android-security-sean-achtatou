package udk.android.reader.pdf;

import android.app.Activity;
import android.app.AlertDialog;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

final class ab extends Thread {
    final /* synthetic */ w a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ String c;
    private final /* synthetic */ AlertDialog d;
    private final /* synthetic */ ProgressBar e;
    private final /* synthetic */ ListView f;
    private final /* synthetic */ BaseAdapter g;

    ab(w wVar, Activity activity, String str, AlertDialog alertDialog, ProgressBar progressBar, ListView listView, BaseAdapter baseAdapter) {
        this.a = wVar;
        this.b = activity;
        this.c = str;
        this.d = alertDialog;
        this.e = progressBar;
        this.f = listView;
        this.g = baseAdapter;
    }

    public final void run() {
        w.c(this.a);
        this.b.runOnUiThread(new o(this, this.b, this.c, this.d, this.e, this.f, this.g));
    }
}
