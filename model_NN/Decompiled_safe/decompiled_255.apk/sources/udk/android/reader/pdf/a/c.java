package udk.android.reader.pdf.a;

import android.graphics.RectF;

public abstract class c {
    private int a;
    private double[] b;

    public final double[] af() {
        return this.b;
    }

    public final int ag() {
        return this.a;
    }

    public abstract RectF b(float f);

    public final void b(double[] dArr) {
        this.b = dArr;
    }

    public boolean c(float f, float f2, float f3) {
        RectF b2 = b(f);
        return b2 != null && b2.contains(f2, f3);
    }

    public final void g(int i) {
        this.a = i;
    }
}
