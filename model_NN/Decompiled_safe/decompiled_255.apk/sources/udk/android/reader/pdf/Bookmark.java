package udk.android.reader.pdf;

import java.io.Serializable;

public class Bookmark implements Serializable, Comparable {
    private static final long serialVersionUID = -1748867557092783074L;
    private int a;
    private String b;
    private String desc;
    private int page;

    public int compareTo(Bookmark bookmark) {
        if (bookmark.getPage() == getPage()) {
            return 0;
        }
        return bookmark.getPage() > getPage() ? -1 : 1;
    }

    public String getDesc() {
        return this.desc != null ? this.desc : this.b;
    }

    public int getPage() {
        return this.page > 0 ? this.page : this.a;
    }

    public void setDesc(String str) {
        this.desc = str;
    }

    public void setPage(int i) {
        this.page = i;
    }
}
