package udk.android.reader.pdf;

import android.content.Context;

public final class y {
    private int a;
    private int b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private Context j;

    public final String a() {
        return this.c;
    }

    public final void a(int i2) {
        this.a = i2;
    }

    public final void a(Context context) {
        this.j = context;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final String b() {
        return this.h;
    }

    public final void b(int i2) {
        this.b = i2;
    }

    public final void b(String str) {
        this.h = str;
    }

    public final String c() {
        return this.i;
    }

    public final void c(String str) {
        this.i = str;
    }

    public final String d() {
        return this.g;
    }

    public final void d(String str) {
        this.g = str;
    }

    public final String e() {
        return this.f;
    }

    public final void e(String str) {
        this.f = str;
    }

    public final String f() {
        return this.e;
    }

    public final void f(String str) {
        this.e = str;
    }

    public final Context g() {
        return this.j;
    }

    public final void g(String str) {
        this.d = str;
    }

    public final String h() {
        return this.d;
    }
}
