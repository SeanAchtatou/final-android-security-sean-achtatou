package udk.android.reader.pdf;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.unidocs.commonlib.util.i;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import udk.android.reader.b.c;

public final class a implements ar {
    private static a a;
    private boolean b;
    /* access modifiers changed from: private */
    public AlertDialog c;
    private List d;
    private Map e;
    private List f = new ArrayList();

    private a() {
        PDF.a().a(this);
    }

    public static a a() {
        if (a == null) {
            a = new a();
        }
        return a;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0062 A[LOOP:0: B:4:0x0030->B:20:0x0062, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(android.content.Context r5) {
        /*
            r4 = this;
            java.io.File r0 = new java.io.File
            java.io.File r1 = new java.io.File
            udk.android.reader.pdf.PDF r2 = udk.android.reader.pdf.PDF.a()
            java.lang.String r2 = r2.y()
            r1.<init>(r2)
            java.lang.String r1 = udk.android.reader.b.a.d(r5, r1)
            r0.<init>(r1)
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x0037
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r4.d = r0
        L_0x0023:
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r4.e = r0
            java.util.List r0 = r4.d
            java.util.Iterator r1 = r0.iterator()
        L_0x0030:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x0062
            return
        L_0x0037:
            r1 = 0
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x004e }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x004e }
            r3.<init>(r0)     // Catch:{ Exception -> 0x004e }
            r2.<init>(r3)     // Catch:{ Exception -> 0x004e }
            java.lang.Object r0 = r2.readObject()     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            java.util.List r0 = (java.util.List) r0     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            r4.d = r0     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            com.unidocs.commonlib.util.i.a(r2)
            goto L_0x0023
        L_0x004e:
            r0 = move-exception
        L_0x004f:
            udk.android.reader.b.c.a(r0)     // Catch:{ all -> 0x005d }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x005d }
            r0.<init>()     // Catch:{ all -> 0x005d }
            r4.d = r0     // Catch:{ all -> 0x005d }
            com.unidocs.commonlib.util.i.a(r1)
            goto L_0x0023
        L_0x005d:
            r0 = move-exception
        L_0x005e:
            com.unidocs.commonlib.util.i.a(r1)
            throw r0
        L_0x0062:
            java.lang.Object r0 = r1.next()
            udk.android.reader.pdf.Bookmark r0 = (udk.android.reader.pdf.Bookmark) r0
            java.util.Map r2 = r4.e
            int r3 = r0.getPage()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2.put(r3, r0)
            goto L_0x0030
        L_0x0076:
            r0 = move-exception
            r1 = r2
            goto L_0x005e
        L_0x0079:
            r0 = move-exception
            r1 = r2
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.pdf.a.b(android.content.Context):void");
    }

    private void c(Context context) {
        ObjectOutputStream objectOutputStream;
        Throwable th;
        Exception e2;
        File file = new File(udk.android.reader.b.a.d(context, new File(PDF.a().y())));
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        try {
            ObjectOutputStream objectOutputStream2 = new ObjectOutputStream(new FileOutputStream(file));
            try {
                objectOutputStream2.writeObject(this.d);
                objectOutputStream2.flush();
                i.a(objectOutputStream2);
            } catch (Exception e3) {
                e2 = e3;
                objectOutputStream = objectOutputStream2;
                try {
                    c.a((Throwable) e2);
                    i.a(objectOutputStream);
                } catch (Throwable th2) {
                    th = th2;
                    i.a(objectOutputStream);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                objectOutputStream = objectOutputStream2;
                i.a(objectOutputStream);
                throw th;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            objectOutputStream = null;
            e2 = exc;
            c.a((Throwable) e2);
            i.a(objectOutputStream);
        } catch (Throwable th4) {
            Throwable th5 = th4;
            objectOutputStream = null;
            th = th5;
            i.a(objectOutputStream);
            throw th;
        }
    }

    private void e() {
        for (af d2 : this.f) {
            d2.d();
        }
    }

    public final int a(Context context) {
        PDF a2 = PDF.a();
        if (!PDF.a().f() || a2.y() == null) {
            return 0;
        }
        if (this.d == null) {
            b(context);
        }
        return this.d.size();
    }

    public final Bookmark a(Context context, int i) {
        if (this.d == null) {
            b(context);
        }
        return (Bookmark) this.d.get(i);
    }

    public final void a(Context context, String str, String str2, String str3, String str4, String str5) {
        Bookmark b2 = b(context, PDF.a().E());
        if (b2 != null) {
            if (this.d == null) {
                b(context);
            }
            this.d.remove(b2);
            c(context);
            this.e.remove(Integer.valueOf(b2.getPage()));
            e();
            Toast.makeText(context, str3, 0).show();
            return;
        }
        EditText editText = new EditText(context);
        editText.setInputType(1);
        editText.setImeOptions(6);
        editText.setOnEditorActionListener(new t(this, editText, context, str2));
        editText.setSingleLine();
        this.c = new AlertDialog.Builder(context).setTitle(str).setView(editText).setPositiveButton(str4, new u(this, editText, context, str2)).setNegativeButton(str5, new r(this)).show();
        editText.postDelayed(new s(this, context, editText), 100);
    }

    public final void a(Context context, Bookmark bookmark) {
        if (this.d == null) {
            b(context);
        }
        this.d.add(bookmark);
        Collections.sort(this.d);
        c(context);
        this.e.put(Integer.valueOf(bookmark.getPage()), bookmark);
        e();
    }

    public final void a(af afVar) {
        if (!this.f.contains(afVar)) {
            this.f.add(afVar);
        }
    }

    public final void a(af afVar, Context context, Drawable drawable, String str, String str2) {
        if (this.d == null) {
            b(context);
        }
        List list = this.d;
        ListView listView = new ListView(context);
        listView.setCacheColorHint(0);
        listView.setDivider(drawable);
        p pVar = new p(this, context, list);
        listView.setAdapter((ListAdapter) pVar);
        listView.setOnItemClickListener(new q(this, new AlertDialog.Builder(context).setTitle(str).setView(listView).setPositiveButton(str2, (DialogInterface.OnClickListener) null).show(), pVar, afVar));
    }

    public final void a(ah ahVar) {
    }

    public final boolean a(int i) {
        if (this.d == null) {
            return false;
        }
        return this.e.get(Integer.valueOf(i)) != null;
    }

    public final Bookmark b(Context context, int i) {
        if (this.d == null) {
            b(context);
        }
        return (Bookmark) this.e.get(Integer.valueOf(i));
    }

    public final void b() {
        this.b = true;
        for (af b2 : this.f) {
            b2.b();
        }
    }

    public final void b(af afVar) {
        this.f.remove(afVar);
    }

    public final void b(ah ahVar) {
    }

    public final void c() {
        this.b = false;
        for (af c2 : this.f) {
            c2.c();
        }
    }

    public final boolean d() {
        return this.b;
    }

    public final void f() {
    }

    public final void g() {
        this.b = false;
        this.d = null;
        e();
    }

    public final void h() {
    }
}
