package udk.android.reader.pdf;

import android.content.Context;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

final class t implements TextView.OnEditorActionListener {
    private /* synthetic */ a a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ Context c;
    private final /* synthetic */ String d;

    t(a aVar, EditText editText, Context context, String str) {
        this.a = aVar;
        this.b = editText;
        this.c = context;
        this.d = str;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (keyEvent != null && keyEvent.getAction() != 1) {
            return true;
        }
        if (this.a.c != null && this.a.c.isShowing()) {
            this.a.c.dismiss();
        }
        Bookmark bookmark = new Bookmark();
        bookmark.setPage(PDF.a().E());
        bookmark.setDesc(this.b.getText().toString());
        this.a.a(this.c, bookmark);
        Toast.makeText(this.c, this.d, 0).show();
        this.a.c = null;
        return true;
    }
}
