package udk.android.reader.pdf;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class v implements Runnable {
    private /* synthetic */ b a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ EditText c;

    v(b bVar, Context context, EditText editText) {
        this.a = bVar;
        this.b = context;
        this.c = editText;
    }

    public final void run() {
        ((InputMethodManager) this.b.getSystemService("input_method")).showSoftInput(this.c, 1);
    }
}
