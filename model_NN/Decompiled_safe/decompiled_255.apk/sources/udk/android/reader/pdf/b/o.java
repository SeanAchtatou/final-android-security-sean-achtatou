package udk.android.reader.pdf.b;

import android.content.DialogInterface;
import com.unidocs.commonlib.util.b;
import java.util.Collection;
import java.util.List;
import udk.android.reader.b.a;
import udk.android.reader.pdf.annotation.ab;
import udk.android.reader.pdf.annotation.s;

final class o implements DialogInterface.OnClickListener {
    private /* synthetic */ d a;
    private final /* synthetic */ List b;
    private final /* synthetic */ l c;

    o(d dVar, List list, l lVar) {
        this.a = dVar;
        this.b = list;
        this.c = lVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String b2 = ((f) this.b.get(i)).b();
        if (!b2.equals(this.c.g())) {
            ab abVar = new ab();
            abVar.b = this.a.b.a(this.c, b2);
            if (b.a((Collection) abVar.b)) {
                s.a().a(abVar);
            }
            if (a.L) {
                this.a.d();
            }
        }
    }
}
